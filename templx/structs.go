package templx

type PageHomeParm struct {
	Navbar            NavbarParm
	Title             string
	Cloud             bool
	MustLogin         bool
	LoggedIn          bool
	MustSelectProject bool
	Projects          []string
}
type AlertParm struct {
	Status   string
	Messages []string
}

var AlertStatusPrimary = "primary"
var AlertStatusSecondary = "secondary"
var AlertStatusSuccess = "success"
var AlertStatusDanger = "danger"
var AlertStatusWarning = "warning"
var AlertStatusInfo = "info"
var AlertStatusLight = "light"
var AlertStatusDark = "dark"

type NavbarParm struct {
	Admin               bool
	Cloud               bool
	InternetIcon        string
	InternetIconColor   string
	InternetStatusMsg   string
	Editor              bool // if true, additional editor specific menu items will be added to the navbar
	IncludeFull         bool
	FallbackEnabled     bool
	Gitlab              string
	GitlabGroupName     string
	GitlabIcon          string
	GitlabIconColor     string
	GitlabStatusMsg     string
	LML                 bool // indicates whether the editor is for the Liturgical Markup Language
	LocalTestSiteUrl    string
	LocalPublicSiteUrl  string
	LoggedIn            bool
	PublicSitePort      string
	TestSitePort        string
	PublishedTestSite   string
	PublishedPublicSite string
	Title               string
}
type PageBackupsParm struct {
	Navbar         NavbarParm
	Title          string
	Git            bool
	GitLabGroupUrl string
}
type PageLibrariesParm struct {
	Navbar NavbarParm
	Title  string
}
type PageMergeParm struct {
	Navbar NavbarParm
	Title  string
}
type PageSubscriptionsParm struct {
	Navbar NavbarParm
	Title  string
}
type PageSyncParm struct {
	Navbar NavbarParm
	Title  string
}
type PageStatisticsParm struct {
	Navbar NavbarParm
	Title  string
}
type NavbarViewParm struct {
	Gitlab              string
	GitlabGroupName     string
	PublishedTestSite   string
	PublishedPublicSite string
	LocalTestSiteUrl    string
	LocalPublicSiteUrl  string
}
type NavbarUserParm struct {
	Cloud    bool
	LoggedIn bool
}
