#!/bin/bash

# Check if Bun is installed
if ! command -v bun &> /dev/null; then
    echo "Bun is not installed. Please install it using:"
    echo "curl -fsSL https://bun.sh/install | bash"
    exit 1
fi

# Set development flag so Go app knows to use the dev server
export DOXA_DEV_MODE=true

# Start the Bun dev server in the background
echo "Starting Bun dev server for static files..."
bun run dev-server.js &
BUN_PID=$!

# Give it a moment to start
sleep 1

# Build and run the Go app
echo "Building and running doxa-tools..."
go build -o bin/doxa .
./bin/doxa

# When the Go app exits, kill the Bun server
kill $BUN_PID