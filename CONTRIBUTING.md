Contributing to the Doxa Project

You are welcome to volunteer time to work on adding enhancements or maintaining the code if you meet the following requirements:

- are an Orthodox Christian,
- are familiar with Orthodox liturgical services,
- have at least four years professional experience as a software developer,
- know how to use git,
- are willing to learn gitlab and golang if you do not already know them.

If you wish to contribute time, please contact Michael Colburn (michael.colburn@liml.org). Provide your name, the location and name of your parish (with a link to its website if it has one), and information about your prior experience as a software developer.

Also, please read the contributor pages on the project Wiki (see the menu bar on the left).

