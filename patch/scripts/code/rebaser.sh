#!/bin/bash

     ###
    #   #
   #     #
  #   !   #
 #         #
#############

# this creates irreversable damage
machname=$(hostname)
machname=${machname%%.local}
username=$(whoami)

cd $3
if ls -d .git; then 
	git checkout -f main
	git branch -D $username-$machname-work
	git branch $username-$machname-work
	git checkout $username-$machname-work
	git push --force
fi
