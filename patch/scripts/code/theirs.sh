#!/bin/bash

machname=$(hostname)
machname=${machname%%.local}
username=$(whoami)

cd $3
if ls -d .git; then 
	git checkout $username-$machname-work
	git merge --abort
	git merge main --allow-unreated-histories --strategy-option=theirs
	git commit -m "merged using 'theirs' strategy"
fi
