#!/bin/bash

machname=$(hostname)
machname=${machname%%.local}
for repo in $(cat $3); do
	dirpath=${repo#*/*/*/*/}
	dirpath=${dirpath%%.git}
	if ls -d $dirpath/.git >/dev/null ; then
		echo already exists
	else
		mkdir -p $dirpath
		cd $dirpath/..
		if git clone https://$1:$2@${repo#https://}; then
			cd -
		else
			cd -
			rmdir $dirpath
		fi
	fi
	echo $dirpath
	cd $dirpath
	pwd
	git branch $1-$machname-work
	git checkout $1-$machname-work
	cd -
done
