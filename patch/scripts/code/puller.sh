#!/bin/bash

machname=$(hostname)
for repo in $(cat $3); do
	dirpath=${repo#*/*/*/*/}
	dirpath=${dirpath%%.git}
	if ls -d $dirpath >/dev/null ; then
		cd $dirpath
		git checkout main || git checkout master
		git pull https://$1:$2@${repo#https://}
		git checkout $1-$machname-work
		git pull https://$1:$2@${repo#https://}
		cd -
	else
		mkdir -p $dirpath
		cd $dirpath/..
		if git clone https://$1:$2@${repo#https://}; then
			cd -
		else
			cd -
			rmdir $dirpath
		fi
	fi
done
