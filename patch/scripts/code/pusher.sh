#!/bin/bash

machname=$(hostname)
machname=${machname%%.local}
localuser=$(whoami)
for repo in $(cat $3); do
	dirpath=${repo#*/*/*/*/}
	dirpath=${dirpath%%.git}
	if ls -d $dirpath >/dev/null ; then
		cd $dirpath
		if git status; then
			git checkout main || git checkout master
			git pull https://$1:$2@${repo#https://}
			git checkout $localuser-$machname-work || (git branch $localuser-$machname-work && git checkout $localuser-$machname-work)
			git push https://$1:$2@${repo#https://}
			git pull https://$1:$2@${repo#https://}
			git add .
			git commit -am "Automatically commited by doxa pusher script"
			git checkout main || git checkout master
			git pull https://$1:$2@${repo#https://}
			git checkout $localuser-$machname-work
 			if (git merge main --ff-only) ; then # || git merge master --ff-only) ; then
				echo NO ISSUES
				git commit -am "doxa-pusher automatically merged from main"
			fi
			git push https://$1:$2@${repo#https://}

		fi
		cd -
	fi
done
exit 0
