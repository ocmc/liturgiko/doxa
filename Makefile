# Variables
MODULE = $(shell go list -m)
OUTPUT_DIR = ./licenses
NOTICE_FILE = NOTICE
GO_LICENSES = go-licenses

# Default target
all: build notice

# Build the Go project
build:
	@echo "Building the project..."
	go build -o bin/doxa .

# Generate license information
licenses:
	@echo "Generating license information..."
	$(GO_LICENSES) save $(MODULE) --save_path=$(OUTPUT_DIR) --force --include_tests=false

# Generate the NOTICE file
notice:
	@echo "Generating the NOTICE file..."
	@echo "# NOTICE" > $(NOTICE_FILE)
	@echo "" >> $(NOTICE_FILE)
	@echo "This project includes third-party libraries that are licensed under their respective licenses." >> $(NOTICE_FILE)
	@echo "" >> $(NOTICE_FILE)
	$(GO_LICENSES) report $(MODULE) | sed -n '/^## /,/^$$/p' >> $(NOTICE_FILE)
	@echo "" >> $(NOTICE_FILE)
	@echo "For full license texts, see $(OUTPUT_DIR) directory." >> $(NOTICE_FILE)

# Clean up generated files
clean:
	@echo "Cleaning up..."
	rm -rf bin
	rm -rf $(OUTPUT_DIR)
	rm -f $(NOTICE_FILE)

# Phony targets
.PHONY: all build licenses notice clean