package servers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"net/http"
)

func (s *server) wsRoutes(router *mux.Router) {
	router.HandleFunc("/ws/gen", s.genWsEndpoint)
	router.HandleFunc("/ws/publish", s.publishWsEndpoint)
	router.HandleFunc("/ws/rosync", s.roSyncWsEndpoint)
	router.HandleFunc("/ws/rwsync", s.rwSyncWsEndpoint)
	// TODO: the following appears to be unused. consider deleting it. and static/wc/sse-client.js
	router.HandleFunc("/sse", s.sseEndpoint())
}

func (s *server) genWsEndpoint(w http.ResponseWriter, r *http.Request) {
	var err error
	genUpgrader.CheckOrigin = func(r *http.Request) bool { return true }
	if err != nil {
		doxlog.Errorf("%v", err)
	}
	genWs, err = genUpgrader.Upgrade(w, r, nil)
	if err != nil {
		doxlog.Infof("%v", err)
		return
	}
	err = genWs.WriteMessage(1, []byte("WS: connection established"))
	if err != nil {
		return
	}
	genSocketWriter(genWs, s.genMsgChan)
}
func (s *server) publishWsEndpoint(w http.ResponseWriter, r *http.Request) {
	var err error
	publishUpgrader.CheckOrigin = func(r *http.Request) bool { return true }
	if err != nil {
		doxlog.Errorf("%v", err)
	}
	publishWs, err = publishUpgrader.Upgrade(w, r, nil)
	if err != nil {
		doxlog.Infof("%v", err)
		return
	}
	err = publishWs.WriteMessage(1, []byte("WS: connection established"))
	if err != nil {
		return
	}
	publishSocketWriter(publishWs, s.publishMsgChan)
}
func (s *server) roSyncWsEndpoint(w http.ResponseWriter, r *http.Request) {
	var err error
	roSyncUpgrader.CheckOrigin = func(r *http.Request) bool { return true }
	if err != nil {
		doxlog.Errorf("%v", err)
	}
	roSyncWs, err = roSyncUpgrader.Upgrade(w, r, nil)
	if err != nil {
		doxlog.Infof("%v", err)
		return
	}
	err = roSyncWs.WriteMessage(1, []byte("WS: connection established"))
	if err != nil {
		return
	}
	roSyncSocketWriter(roSyncWs, s.roSyncMsgChan)
}
func (s *server) rwSyncWsEndpoint(w http.ResponseWriter, r *http.Request) {
	var err error
	rwSyncUpgrader.CheckOrigin = func(r *http.Request) bool { return true }
	if err != nil {
		doxlog.Errorf("%v", err)
	}
	rwSyncWs, err = rwSyncUpgrader.Upgrade(w, r, nil)
	if err != nil {
		doxlog.Infof("%v", err)
		return
	}
	err = rwSyncWs.WriteMessage(1, []byte("WS: connection established"))
	if err != nil {
		return
	}
	rwSyncSocketWriter(rwSyncWs, s.rwSyncMsgChan)
}
func (s *server) sseEndpoint() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		//client := &Client{name: r.RemoteAddr, events: make(chan *DashBoard, 10)}
		//go updateDashboard(client)

		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		w.Header().Set("Content-Type", "text/event-stream")
		w.Header().Set("Cache-Control", "no-cache")
		w.Header().Set("Connection", "keep-alive")

		//timeout := time.After(5 * time.Minute)

		for ev := range s.genMsgChan {
			var buf bytes.Buffer
			enc := json.NewEncoder(&buf)
			enc.Encode(ev)
			fmt.Fprintf(w, "data: %v\n\n", buf.String())
			if f, ok := w.(http.Flusher); ok {
				f.Flush()
			}
			fmt.Printf("data: %v\n", buf.String())
		}
		//select {
		//case ev := <-s.messageChannel:
		//	var buf bytes.Buffer
		//	enc := json.NewEncoder(&buf)
		//	enc.Encode(ev)
		//	fmt.Fprintf(w, "data: %v\n\n", buf.String())
		//	fmt.Printf("data: %v\n", buf.String())
		//	//case <-timeout:
		//	//	fmt.Fprintf(w, ": nothing to sent\n\n")
		//}

		if f, ok := w.(http.Flusher); ok {
			f.Flush()
		}
	}
}
