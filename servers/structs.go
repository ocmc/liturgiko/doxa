package servers

import (
	"encoding/json"
	"github.com/liturgiko/doxa/pkg/atempl"
	"github.com/liturgiko/doxa/pkg/dvcs"
	"github.com/liturgiko/doxa/pkg/ldp"
	"github.com/liturgiko/doxa/pkg/parser"
	"github.com/liturgiko/doxa/pkg/queries"
	"github.com/liturgiko/doxa/pkg/synch"
	"github.com/liturgiko/doxa/pkg/table"
	"github.com/liturgiko/doxa/pkg/users"
	"github.com/liturgiko/doxa/pkg/utils/ltUnicode"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/stack"
	picoWidget "github.com/liturgiko/doxa/templates/webApp/structs/widget"
	"html/template"
)

/**
Note: each struct added here needs a corresponding ToJson Method.
*/

type JsonKeyPathResponse struct {
	Path    *stack.StringStack
	Message string
	Status  string
	Values  []*IDKV
}

func (r *JsonKeyPathResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonFileListResponse struct {
	Dir     string
	Message string
	Status  string
	Files   []string
}

func (r *JsonFileListResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonLibraryListResponse struct {
	Library  string
	Message  string
	Status   string
	Matching []string
}

func (r *JsonLibraryListResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonFileImportResponse struct {
	Count   int
	Message string
	Status  string
	Errors  []string
}

func (r *JsonFileImportResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonRecordSearchResponse struct {
	Path                 *stack.StringStack
	Message              string
	Status               string
	PatternOriginal      string
	PatternExpanded      string
	Values               []*IDKV
	Count                int
	Time                 string
	CaseInsensitive      bool
	DiacriticInsensitive bool
	WholeWord            bool
	NFD                  bool
}

func (r *JsonRecordSearchResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonTemplateSearchResponse struct {
	Path    *stack.StringStack
	Message string
	Status  string
	Values  []*ltfile.MatchedLine
}

func (r *JsonTemplateSearchResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonStatusResponse struct {
	Message string
	Status  string
}

func (r *JsonStatusResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonCssUpdateResponse struct {
	ID      string
	Status  string
	Message string
}

func (r *JsonCssUpdateResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonJsUpdateResponse struct {
	ID     string
	Status string
}

func (r *JsonJsUpdateResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonLmlUpdateResponse struct {
	ID       string // json array
	FilePath string
	Message  string
	Status   string
}

func (r *JsonLmlUpdateResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type BackupStatus struct {
	Title   string
	Navbar  Navbar
	Message string
	DbPath  string
	TsvPath string
}

type JsonKeyValueResponse struct {
	Status string
	Values []*KV
}

func (r *JsonKeyValueResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonRecordResponse struct {
	ID      string
	Message string
	Status  string
	Title   string
	Value   string
}

func (r *JsonRecordResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonSrcCodeResponse struct {
	ID       string // json array
	FilePath string
	Message  string
	ReadOnly bool
	Status   string
	Title    string
	Value    string
}

func (r *JsonSrcCodeResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonDbRedirectsToResponse struct {
	ID        string
	IsDir     bool
	Message   string
	Status    string
	Redirects []*Redirect
	Value     string
}

func (r *JsonDbRedirectsToResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type Redirect struct {
	From string
	To   string
}

type JsonDbIdTraceResponse struct {
	ID        string
	Message   string
	Status    string
	Redirects []string
	Value     string
}

func (r *JsonDbIdTraceResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonRegExResponse struct {
	CaseInsensitive      bool
	Count                int
	DiacriticInsensitive bool
	ExpandedPattern      string
	GoFlags              string
	Message              string
	NFD                  bool
	Pattern              string
	Status               string
	Text                 string
	Time                 string
	WholeWord            bool
}

func (r *JsonRegExResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonUnicodeResponse struct {
	Message            string
	Status             string
	SrcText            string
	Nfc                string // Unicode NFC
	NfcLenBytes        int
	Nfd                string // Unicode NFD
	NfdLenBytes        int
	NfcNd              string // NFC with diacritics removed
	NfcNdLenBytes      int
	NfdNd              string // NFD with diacritics removed
	NfdNdLenBytes      int
	NfcNdNpLow         string // NDC with diacritics, punctuation removed and to lower case}
	NfcNdNpLowLenBytes int
	NfdNdNpLow         string // NDF with diacritics, punctuation removed and to lower case}
	NfdNdNpLowLenBytes int
	GlyphMeta          []ltUnicode.GlyphMeta
}

func (r *JsonUnicodeResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type LangDesc struct {
	Code  string
	Title string
}
type JsonLayoutsResponse struct {
	Layouts       []string
	ColumnLayouts map[string]*atempl.TableLayout
	Status        string
}

func (r *JsonLayoutsResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonLayoutResponse struct {
	Acronym string
	Layout  *atempl.TableLayout
	Status  string
}

func (r *JsonLayoutResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonFallbackResponse struct {
	FallbackEnabled        bool
	FallbackSelected       string
	AvailableSubscriptions []string
	Status                 string
	Message                string
}

func (r *JsonFallbackResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonFallbackUpdateResponse struct {
	Message string
	Status  string
}

func (r *JsonFallbackUpdateResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonProfileResponse struct {
	Message string
	Profile *users.Profile
	Status  string
}

func (r *JsonProfileResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonStringValuesResponse struct {
	Message string
	Status  string
	Values  []string
}

func (r *JsonStringValuesResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonTemplatePatternsResponse struct {
	Message    string
	Status     string
	Patterns   []string
	Selections []string
}

func (r *JsonTemplatePatternsResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonTemplateStatusTypesResponse struct {
	Message     string
	Status      string
	StatusTypes []string
}

func (r *JsonTemplateStatusTypesResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonLdpResponse struct {
	Calendar          string
	Date              string
	Exp               string
	ExpParseErrors    []parser.ParseError
	ExpTrue           bool
	Message           string
	PentecostarionMcd []*queries.MCDInfo
	Properties        ldp.LiturgicalProperties
	Status            string
	TriodionMcd       []*queries.MCDInfo
}

func (r *JsonLdpResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonDirContentsResponse struct {
	Path            *stack.StringStack
	Message         string
	Status          string
	InFallbackDir   bool
	FallbackEnabled bool
	Values          []*DirItem
}

func (r *JsonDirContentsResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonDirTopListResponse struct {
	Message string
	Status  string
	Values  []string
}

func (r *JsonDirTopListResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonLmlParseResponse struct {
	Date               string
	FilePath           string // of the template
	HrefHost           string
	HtmlPath           string
	ID                 string // json array
	IsBlock            bool
	Message            string
	ParseErrors        []parser.ParseError
	Status             string
	TemplatesByNbr     map[int]string
	TemplatesTableData *table.Data
}

func (r *JsonLmlParseResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type JsonLogFileResponse struct {
	FilePath  string // of the template
	Message   string
	Status    string
	TableData *table.Data
}

func (r *JsonLogFileResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type GitlabGroupResponse struct {
	GitlabRootGroup *dvcs.Group
	Status          string
	Message         string
}

func (r *GitlabGroupResponse) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type RepositoryStatuses struct {
	Repos   []*synch.RepoMeta
	Status  string
	Message string
}

func (r *RepositoryStatuses) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type EnumAbrName struct {
	Abr  string
	Desc string
	Name string
}
type EnumAbrNames struct {
	Enums   []*EnumAbrName
	Status  string
	Message string
}

func (r *EnumAbrNames) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

type DirItem struct {
	IsDir bool
	Name  string
}
type GenData struct {
	Navbar   Navbar
	Host     string
	Endpoint string
	Title    string
}

type HomeData struct {
	Navbar      Navbar
	Host        string
	Title       string
	DoxaVersion string
}

type MergeData struct {
	Navbar   Navbar
	Title    string
	RepoData []*dvcs.MergeData
}

type TemplateExplorerData struct {
	Navbar          Navbar
	Host            string
	Title           string
	FallbackEnabled bool
}

type LdpData struct {
	Navbar          Navbar
	Calendar        string
	Date            string
	FallbackEnabled bool
	Host            string
	Title           string
}
type ToolsData struct {
	Navbar   Navbar
	Calendar string
	Host     string
	Title    string
}

type CatalogManagerData struct {
	Navbar Navbar
	Host   string
}
type SiteComparisonPageData struct {
	Navbar Navbar
	Host   string
	Title  string
}

type SynchManagerData struct {
	Navbar          Navbar
	Host            string
	ResourcesDir    string
	GitlabReachable bool
	GitlabURL       string
	ProfileExists   bool
	Git             string
	Title           string
}
type AboutData struct {
	Navbar   Navbar
	Host     string
	Title    string
	Version  string
	Project  string
	DoxaHome string
}
type IndexHtmlData struct {
	UrlPath           template.HTMLAttr
	Navbar            Navbar // used to create the navbar
	Title             string // used in the html head title tag
	Body              *picoWidget.Body
	NeedsModalConfirm bool
}

func NewIndexHtmlData(urlPath, title string, navbar Navbar) *IndexHtmlData {
	return &IndexHtmlData{
		UrlPath: template.HTMLAttr(urlPath),
		Title:   title,
		Navbar:  navbar,
		Body:    new(picoWidget.Body),
	}
}

type EditorData struct {
	Completions string
	Date        string
	Host        string
	Lang        string
	Navbar      Navbar
	Role        string
	Src         string
	Text        string
	Title       string
}
type LoginData struct {
	Navbar Navbar
	Host   string
	Title  string
}

type UserProfileData struct {
	Navbar      Navbar
	Host        string
	UserProfile map[string]interface{}
	VaultPath   string
	Title       string
}
type Navbar struct {
	Admin               bool
	DoxaHandlesGit      bool
	Cloud               bool
	Editor              bool // if true, additional editor specific menu items will be added to the navbar
	FallbackEnabled     bool
	Gitlab              string
	GitlabGroupName     string
	LML                 bool // indicates whether the editor is for the Liturgical Markup Language
	LoggedIn            bool
	ProjectName         string
	ProjectID           string
	ProjectUrl          string
	PublicSitePort      string
	TestSitePort        string
	PublishedTestSite   string
	PublishedPublicSite string
	Title               string
	TokenStatus         TokenStatus
}
type TokenStatus struct {
	Exists bool
	Locked bool
}
type RegExData struct {
	Navbar Navbar
	Host   string
	Title  string
}
type UnicodeData struct {
	Navbar Navbar
	Host   string
	Title  string
}

type SettingsData struct {
	Navbar           Navbar
	Host             string
	CurrentConfig    string
	AvailableConfigs string
	Title            string
}

// KV is used for simple key-value data
type KV struct {
	ID    string
	Value string
}

// IDKV is used for simple key-value data
type IDKV struct {
	ID    string
	Last  string
	Value string
}
