package servers

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/liturgiko/doxa/api"
	picoWidget "github.com/liturgiko/doxa/templates/webApp/structs/widget"
	"html/template"
	"net/http"
	"strings"
)

// catalogRoutes registers all catalog-related routes
func (s *server) catalogRoutes(r *mux.Router) {
	s.router.HandleFunc("/prepareCatalog", s.servePrepareCatalogPage()).Methods(http.MethodGet)

	r.HandleFunc("/htmx/catalog/getMeta", s.getCatalogProperties).Methods(http.MethodGet)
	r.HandleFunc("/htmx/catalog/listEntries", s.getCatalogEntryList).Methods(http.MethodGet)
	r.HandleFunc("/htmx/catalog/getEntryForm", s.getEntryProperties).Methods(http.MethodGet)
	r.HandleFunc("/htmx/catalog/update", s.updateCatalog).Methods(http.MethodPost)
	r.HandleFunc("/htmx/catalog/publish", s.publishCatalog).Methods(http.MethodPost)
	r.HandleFunc("/htmx/catalog/exclude", s.excludeEntry).Methods(http.MethodPost)
	r.HandleFunc("/htmx/catalog/include", s.includeEntry).Methods(http.MethodPost)
	r.HandleFunc("/htmx/catalog/setInclusion", s.handleCatalogSetInclusion).Methods(http.MethodPost)
	r.HandleFunc("/htmx/catalog/rebuild", s.rebuildCatalog).Methods(http.MethodPost)
	r.HandleFunc("/htmx/catalog/pollRebuildProgress", s.pollCatalogProgress).Methods(http.MethodGet)
}

func (s *server) servePrepareCatalogPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")

		data := NewIndexHtmlData(
			r.URL.Path,
			"Prepare Catalog",
			s.getNavbar(w, r),
		)
		data.Body.Main = picoWidget.NewMain(
			"Prepare Catalog for publication",
			"Note that in order to publish, you must unlock your token",
		)
		data.Body.Main.ID = "prepareCatalogMain"
		data.Body.Main.Help = `
    <style>
        .tip {
            background: #f8f9fa;
            border-left: 4px solid #4CAF50;
            padding: 15px;
            margin: 15px 0;
        }
        .warning {
            background: #fff3cd;
            border-left: 4px solid #ffc107;
            padding: 15px;
            margin: 15px 0;
        }
    </style>
    <h1>Catalog Service User Guide</h1>
    
    <h2>Main Catalog Settings</h2>
    <p>The main catalog settings page allows you to configure the overall catalog properties:</p>
    <ul>
        <li><strong>Project:</strong> (Read-only) Displays your current project name</li>
        <li><strong>Description:</strong> Add a general description of your catalog's purpose and contents</li>
        <li><strong>Version:</strong> Set the version number for your catalog</li>
        <li><strong>Maintained By:</strong> Select an existing maintainer or type a new name</li>
    </ul>

    <div class="tip">
        <strong>Tip:</strong> New maintainer names are automatically saved when you enter them and will appear in the dropdown list for future use.
    </div>

    <h2>Managing Catalog Entries</h2>
    <p>Click the "Manage Entries" button to view and configure individual entries in your catalog:</p>
    <ul>
        <li>Entries are displayed in an alphabetical list</li>
        <li>Use checkboxes to include/exclude entries from your catalog</li>
        <li>Media-related entries are marked with "(media)"</li>
        <li>Click the gear icon (<i class="bi bi-gear"></i>) next to any entry to edit its details</li>
    </ul>

    <h2>Entry Settings</h2>
    <p>When editing an individual entry, you can configure:</p>
    <ul>
        <li><strong>Entry:</strong> (Read-only) The entry name</li>
        <li><strong>Relative Path:</strong> (Read-only) The entry's location in the project</li>
        <li><strong>Description:</strong> Detailed information about this entry</li>
        <li><strong>Maintained By:</strong> Select or enter who maintains this entry</li>
        <li><strong>License:</strong> Specify the license for this entry</li>
        <li><strong>Update Message:</strong> Notes about recent updates</li>
        <li><strong>Dependencies:</strong> List any dependencies (one per line)</li>
    </ul>

    <h2>Saving and Publishing</h2>
    <p>The catalog interface provides two main actions:</p>
    <ul>
        <li><strong>Save Catalog:</strong> Saves your changes locally</li>
        <li><strong>Publish Catalog:</strong> Makes your catalog available to others (requires proper authentication)</li>
    </ul>

    <div class="warning">
        <strong>Important:</strong> The "Publish Catalog" button will be disabled if you don't have the required authentication credentials.
    </div>

    <h2>Tips for Effective Catalog Management</h2>
    <ul>
        <li>Always save your changes before navigating away from any form</li>
        <li>Use clear, descriptive names in the "Maintained By" field to help track ownership</li>
        <li>Keep descriptions concise but informative</li>
        <li>Regular publishing helps keep your catalog up-to-date for other users</li>
    </ul>
`
		section := new(picoWidget.Section)
		grid := new(picoWidget.Grid)
		catForm := s.appApi.Catalog.GetCatalogForm(nil, "")
		grid.AddWidget(catForm)
		section.AddGrid(grid)
		data.Body.Main.AddSection(section)
		s.RenderTemplate(w, "index", data, false)
	}
}

// getCatalogProperties handles getting catalog metadata properties
func (s *server) getCatalogProperties(w http.ResponseWriter, r *http.Request) {
	s.logMe("getCatalogProperties")
	f := s.appApi.Catalog.GetCatalogForm(nil, "")
	s.RenderTemplate(w, "bodyMainSectionGridArticleWidget", f, true)
}

// getCatalogEntryList handles getting the list of catalog entries
func (s *server) getCatalogEntryList(w http.ResponseWriter, r *http.Request) {
	s.logMe("getCatalogEntryList")
	f := s.appApi.Catalog.GetForm(nil, "")
	s.RenderTemplate(w, "bodyMainSectionGridArticleWidget", f, true)
}

// getEntryProperties handles getting properties for a specific catalog entry
func (s *server) getEntryProperties(w http.ResponseWriter, r *http.Request) {
	s.logMe("getEntryProperties")
	tgt := r.Header.Get("DL-Target")
	f := s.appApi.Catalog.GetEntryForm(nil, "", strings.ReplaceAll(tgt, "-", "/"))
	s.RenderTemplate(w, "bodyMainSectionGridArticleWidget", f, true)
}

// updateCatalog handles updating catalog metadata or entry properties
func (s *server) updateCatalog(w http.ResponseWriter, r *http.Request) {
	s.logMe("updateCatalog")
	if updateClass := r.Header.Get("UpdateTarget"); updateClass != "" {
		switch updateClass {
		case "meta":
			//go over form
			valmap := map[string]string{
				"Description": "",
				"Version":     "",
				"Maintainer":  "",
			}
			for k, _ := range valmap {
				valmap[k] = r.FormValue(k)
			}
			s.appApi.Catalog.UpdateCatalogDesc(nil, "", api.CatalogMetaUpdate{
				Description: valmap["Description"],
				Version:     valmap["Version"],
				Maintainer:  valmap["Maintainer"],
			})
		case "entry":
			valmap := map[string]string{
				"EntryID":       "",
				"License":       "",
				"Description":   "",
				"UpdateMessage": "",
				"Maintainer":    "",
				"Dependencies":  "",
				"EntryType":     "",
			}
			for k, _ := range valmap {
				valmap[k] = r.FormValue(k)
			}
			go s.appApi.Catalog.UpdateEntry(nil, "", api.CatalogEntryUpdate{
				EntryID:       valmap["EntryID"],
				License:       valmap["License"],
				Description:   valmap["Description"],
				UpdateMessage: valmap["UpdateMessage"],
				Maintainer:    valmap["Maintainer"],
				Dependencies:  valmap["Dependencies"],
				EntryType:     valmap["EntryType"],
			})
		}
		f := s.appApi.Catalog.GetForm(nil, "")
		s.RenderTemplate(w, "bodyMainSectionGridArticleWidget", f, true)
	}
}

// publishCatalog handles publishing the catalog
// TODO: causes abend, should return 401 Unauthorized
func (s *server) publishCatalog(w http.ResponseWriter, r *http.Request) {
	s.logMe("publishCatalog")
	//check for token
	stat, resp, err := s.appApi.TokenService.GetTokenStatus(ctx, "")
	if err != nil {
		w.WriteHeader(500)
		for _, msg := range resp.RequestMessages {
			fmt.Fprint(w, msg)
		}
		return
	}
	if stat == api.PasswordNeeded || stat == api.TokenNeeded {
		w.WriteHeader(401)
		for _, msg := range resp.RequestMessages {
			fmt.Fprint(w, msg)
		}
		return
	}
	go s.appApi.Catalog.Publish(nil, "")
	w.Header().Set("HX-Refresh", "true")
}

// excludeEntry handles excluding an entry from the catalog
func (s *server) excludeEntry(w http.ResponseWriter, r *http.Request) {
	s.logMe("excludeEntry")
	entryID := r.FormValue("entryID")
	if entryID == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	resp, err := s.appApi.Catalog.ExcludeEntry(nil, "", entryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if resp.RequestStatus != api.OK {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	w.Header().Set("HX-Refresh", "true")
}

// includeEntry handles including an entry in the catalog
func (s *server) includeEntry(w http.ResponseWriter, r *http.Request) {
	s.logMe("includeEntry")
	entryID := r.FormValue("entryID")
	if entryID == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	resp, err := s.appApi.Catalog.IncludeEntry(nil, "", entryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if resp.RequestStatus != api.OK {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	w.Header().Set("HX-Refresh", "true")
}

// handleCatalogSetInclusion handles setting catalog entry inclusion status
func (s *server) handleCatalogSetInclusion(w http.ResponseWriter, r *http.Request) {
	s.logMe("handleCatalogSetInclusion")

	sel := r.Header.Get("DL-Select")
	desel := r.Header.Get("DL-Deselect")
	var resp api.RequestResponse
	var err error

	if sel != "" {
		sel = strings.ReplaceAll(sel, "-", "/")
		resp, err = s.appApi.Catalog.IncludeEntry(nil, "", sel)
	} else if desel != "" {
		desel = strings.ReplaceAll(desel, "-", "/")
		resp, err = s.appApi.Catalog.ExcludeEntry(nil, "", desel)
	} else {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if resp.RequestStatus != api.OK {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	f := s.appApi.Catalog.GetEntryList(nil, "")
	s.RenderTemplate(w, "DynamicList", f, true)
}

func (s *server) rebuildCatalog(w http.ResponseWriter, r *http.Request) {
	resp, err := s.appApi.Catalog.Rebuild()
	if err != nil {
		for _, m := range resp.RequestMessages {
			fmt.Fprint(w, m)
		}
		w.WriteHeader(500)
		return
	}
	s.pollCatalogProgress(w, r)
}

func (s *server) pollCatalogProgress(w http.ResponseWriter, r *http.Request) {
	if catalogsAreRefresing {
		//send progressbar
		pb := picoWidget.NewProgressBar()
		pb.HtmxFields = []template.HTMLAttr{
			`hx-get="api/htmx/subscriptions/pollRebuildProgress"`,
			`hx-trigger="every 1s"`,
			`hx-target="this"`,
		}
		s.RenderTemplate(w, "picoWidgetFormProgress", pb, true)
		return
	}
	//refresh page
	w.Header().Set("HX-Refresh", "true")
	w.WriteHeader(205)
}
