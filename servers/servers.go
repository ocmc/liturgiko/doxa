// Package servers provides a web app interface for Doxa.
// It also provides a REST API for liturgical text searching and processing.
//
// Static files (e.g. templates) are embedded using embed.FS.
// They are served by the path /static/ and
// contains the required CSS, JS, images, and
// web components (in the wc directory).  The web components
// use vanilla JS to provide react.js like components.
// The doxa web app home page and all pages served by the main menu
// links are generated dynamically using go html templates, stored in the
// templates directory.  These generated pages use the web components.
//
// There are separate files in the app package:
// routes.go declares the various routes and which handler to call for each.
// handlers.go provides the handler methods for the server.
// structs.go provides the interfaces passed to the templates when executed.
package servers

import (
	"context"
	"embed"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/liturgiko/doxa/api"
	"github.com/liturgiko/doxa/app"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/enums/properties"
	"github.com/liturgiko/doxa/pkg/gitlocal"
	"github.com/liturgiko/doxa/pkg/keyring"
	"github.com/liturgiko/doxa/pkg/kvsw"
	"github.com/liturgiko/doxa/pkg/queries"
	"github.com/liturgiko/doxa/pkg/site"
	"github.com/liturgiko/doxa/pkg/ssesvr"
	"github.com/liturgiko/doxa/pkg/syncsrvc"
	"github.com/liturgiko/doxa/pkg/tmplfunc"
	"github.com/liturgiko/doxa/pkg/users"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/wes"
	picoWidget "github.com/liturgiko/doxa/templates/webApp/structs/widget"
	"html/template"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path"
	"runtime"
	"sort"
	"strings"
	"sync"
	"time"
)

const CloudAddress = "doxa.ocmc.org"
const LocalAddress = "localhost"
const DockerAddress = "0.0.0.0"

type server struct {
	api                 *mux.Router
	backupMutex         sync.Mutex // used to serialize records to tsv for git
	templateMutex       sync.Mutex // used for git add / commit of a template
	cloud               bool
	debug               bool
	docker              bool
	appApi              *api.API
	embeddedStaticFiles embed.FS
	embeddedTemplates   embed.FS
	http                *http.Server
	mapper              *kvs.KVS
	kvsWrapper          *kvsw.Wrapper
	keyRing             *keyring.KeyRing
	linux               bool
	/**
	The channels messages are sent to ws-client.js,
	and are the means by which a long-running
	function can give notifications to the user
	regarding the progress.  It is important
	to prefix each message with a topic and
	colon, e.g.
	    GM:
	The topic must be kept the same by all
	sub-functions called by the top function.
	It is critical that the switch statement
	in ws-client.js has a handler for this "topic".
	*/
	genMsgChan      chan string
	gitManager      *gitlocal.Manager // temporary quick method to push db backups to gitlab
	host            string
	publishMsgChan  chan string
	syncManager     *syncsrvc.Manager
	roSyncMsgChan   chan string
	rwSyncMsgChan   chan string
	ParseMutex      sync.Mutex
	Mutex           sync.Mutex
	Navbar          Navbar
	qm              *queries.QueryManager
	router          *mux.Router
	settingsManager *config.SettingsManager
	siteBuilder     *site.Builder
	// msgServer uses the Server-Sent Event protocol.
	// This is pub/sub technique.  It works fine if
	// the client is a go program, but I have not
	// been able to get a javascript client to work.
	// I have prototyped two approaches:
	// 1) using a 3rd party library (see pkg/ssesvr)
	// 2) using a server endpoint sse (see the routes and the handler)
	// These are not currently being used, since I can't get a javascript
	// client to work with them.
	msgServer      *ssesvr.Server
	ProjectName    string
	ProjectID      string
	ProjectPath    string
	ProjectUrl     string
	sessionManager *wes.SessionManager
	store          *sessions.CookieStore
	templates      *template.Template
	userManager    *users.Manager
	Version        string
	ws             *mux.Router
}

var ctx context.Context
var cancel context.CancelFunc
var mediaRedirectUrl string

func Serve(version string,
	doxaApi *api.API,
	mapper *kvs.KVS,
	keyRing *keyring.KeyRing,
	settingsManager *config.SettingsManager,
	syncManager *syncsrvc.Manager,
	userManager *users.Manager,
	dbname,
	projectName,
	projectId,
	projectPath,
	projectUrl string,
	appPort string,
	publicSitePort string,
	testSitePort string,
	mediaPort string,
	messagePort string,
	cloud bool,
	debug bool,
	siteBuilder *site.Builder,
	embeddedTemplates embed.FS,
	embeddedStaticFiles embed.FS,
	quit func()) {
	var err error
	ctx, cancel = context.WithCancel(context.Background())
	srv := server{}
	srv.appApi = doxaApi
	srv.Version = version
	srv.ProjectName = projectName
	srv.ProjectID = projectId
	srv.ProjectPath = projectPath
	srv.ProjectUrl = projectUrl
	srv.cloud = cloud
	strDocker := os.Getenv("IN_DOCKER")
	if len(strDocker) > 0 && strDocker == "true" {
		srv.docker = true
		srv.host = DockerAddress
	} else {
		srv.host = "" // LocalAddress SAR-101 MAC 2025-02-13
		// use `curl -v localhost:8080 | head` to confirm
		// IPv6: ::1
		// IPv4: 127.0.0.1
	}

	// Check if we're running in development mode
	if os.Getenv("DOXA_DEV_MODE") == "true" {
		doxlog.Info("Running in development mode - using Bun dev server for static files")
	}
	srv.syncManager = syncManager
	srv.userManager = userManager
	srv.embeddedTemplates = embeddedTemplates
	srv.embeddedStaticFiles = embeddedStaticFiles
	srv.siteBuilder = siteBuilder
	srv.settingsManager = settingsManager
	srv.store = sessions.NewCookieStore([]byte(os.Getenv("SESSION_KEY")))
	var gitlabUrl string
	var gitlabGroupName string
	srv.sessionManager = wes.NewManager()
	if cloud {
		// Start a goroutine to periodically check and remove expired web sessions
		doxlog.Infof("starting session timeout checker")
		go func() {
			for {
				time.Sleep(15 * time.Minute) // Adjust the frequency of the checks as needed
				srv.sessionManager.RemoveExpired()
			}
		}()
		srv.debug = debug
		// set up git to push the exported database to gitlab
		var token string
		if srv.cloud {
			token = os.Getenv("TOKEN")
			doxlog.Info("got gitlab token from environmental variable")
		}
		if len(token) == 0 {
			doxlog.Info("did not find gitlab token as environmental variable")
			token, err = srv.userManager.GetToken(app.DoxaSys)
			if err != nil {
				return
			}
			if len(token) > 0 {
				doxlog.Info("got token from user manager")
			}
		}
		dbExportDir := path.Join(config.DoxaPaths.BackupPath, "exports")
		srv.gitManager = gitlocal.NewManager(dbExportDir, token)
		doxlog.Infof("starting periodic database exports")
		doxlog.Infof("export frequency is %v", app.Ctx.BackupFrequency)
		doxlog.Infof("git enabled == %b", app.Ctx.GitEnabled)
		// Start a goroutine to periodically export
		// the entire database to tsv files and push them to gitlab.
		go func() {
			for {
				time.Sleep(time.Duration(app.Ctx.BackupFrequency) * time.Minute)
				doxlog.Infof("exporting to %s", dbExportDir)
				err = srv.kvsWrapper.ExportAll(dbExportDir)
				if err != nil {
					doxlog.Errorf("error exporting database to %s: %v", dbExportDir, err)
					continue
				}
				doxlog.Infof("committing changes in %s", dbExportDir)
				// commit the changes
				if app.Ctx.GitEnabled {
					err = srv.gitManager.Git.CommitAll(dbExportDir, fmt.Sprintf("db export %s", time.Now().Format(time.RFC3339)))
					if err != nil {
						doxlog.Errorf(fmt.Sprintf("git commit error for repo %s: %v", dbExportDir, err))
						continue
					}
					doxlog.Infof("pushing commits in %s", dbExportDir)
					err = srv.gitManager.Git.Push(dbExportDir, "doxasys", srv.gitManager.RemoteToken)
					if err != nil {
						doxlog.Errorf(fmt.Sprintf("git push error for repo %s: %v", dbExportDir, err))
						if strings.Contains(err.Error(), "remote") {
							doxlog.Info("perhaps the remote name doxa-origin has not been set.  Check using git remote -v")
						}
						continue
					}
				}
			}
		}()
	} else {
		gitlabGroupName, gitlabUrl = srv.userManager.GetGroupNameAndUrl(users.LocalUser)
	}
	srv.Navbar = Navbar{Cloud: cloud,
		LoggedIn:            false,
		Gitlab:              gitlabUrl,
		GitlabGroupName:     gitlabGroupName,
		ProjectName:         projectName,
		ProjectID:           projectId,
		ProjectUrl:          projectUrl,
		PublicSitePort:      publicSitePort,
		TestSitePort:        testSitePort,
		PublishedTestSite:   srv.settingsManager.StringProps[properties.SiteBuildPublishTestUrl],
		PublishedPublicSite: srv.settingsManager.StringProps[properties.SiteBuildPublishPublicUrl]}
	srv.loadTemplates()
	srv.mapper = mapper
	srv.kvsWrapper, _ = kvsw.NewWrapper(mapper)
	srv.keyRing = keyRing
	srv.router = mux.NewRouter()
	srv.router.Use(srv.authMiddleware)
	srv.qm = queries.NewQueryManager(mapper)
	srv.api = srv.router.PathPrefix("/api").Subrouter()
	srv.genMsgChan = make(chan string, 10000)
	srv.publishMsgChan = make(chan string, 10000)
	srv.roSyncMsgChan = make(chan string, 10000)
	srv.rwSyncMsgChan = make(chan string, 10000)
	if srv.syncManager != nil {
		srv.syncManager.RoSyncer.SetMsgChannel(srv.roSyncMsgChan)
		srv.syncManager.RwSyncer.SetMsgChannel(srv.rwSyncMsgChan)
	}
	srv.siteBuilder.MsgChan = srv.genMsgChan
	srv.msgServer, err = ssesvr.NewServer(messagePort)
	if err != nil {
		doxlog.Errorf("%v", err)
	}
	doxlog.Infof("Doxa sse server is broadcasting on port %s", messagePort)
	// If auto backup is on, every time we start up, do a backup of the database
	if srv.settingsManager.BoolProps[properties.SysDbAutoBackupOn] {
		go func(path string) {
			_, err = srv.kvsWrapper.Backup(config.DoxaPaths.BackupPath, 1)
			if err != nil {
				doxlog.Errorf("error backing up database to %s: %v", config.DoxaPaths.BackupPath, err)
			}
		}(config.DoxaPaths.BackupPath)
	}

	srv.router.HandleFunc("/quit", func(w http.ResponseWriter, r *http.Request) {
		if cloud {
			// redirect to home page instead of quitting
			w.Header().Set("Content-Type", "text/html")
			w.Header().Set("charset", "utf-8")
			homeData := new(HomeData)
			homeData.DoxaVersion = ""
			homeData.Navbar = srv.Navbar
			srv.templates.ExecuteTemplate(w, "home", homeData)
			return
		}
		go func() {
			go time.Sleep(2 * time.Second)
			quit()
		}()
		w.Write([]byte("<html><head></head><body><h1 style='margin: auto; width: 400px;'>Doxa has shut down.</h1></body></html>"))
		return
	})

	srv.routes() // set the routes for the router

	srv.http = &http.Server{
		Handler:      srv.router,
		Addr:         srv.host + ":" + appPort, // Changes
		WriteTimeout: 15000 * time.Second,      // 15
		ReadTimeout:  15000 * time.Second,      // 15
	}
	if !srv.docker && !srv.cloud {
		hostn := srv.host
		if hostn == "" {
			hostn = "localhost"
		}
		openBrowser(fmt.Sprintf("http://%s:%s", hostn, appPort))
	}
	doxlog.Infof("Doxa web app is available at %s:%s", srv.host, appPort)
	doxlog.Infof("Doxa db is at %s", dbname)
	log.Fatal(srv.http.ListenAndServe())
}
func ServeGeneratedPublicSite(path, baseHref, port, mediaUrl, pdfViewer string) {
	// add a handler for the local generated website
	if _, err := os.Stat(path); os.IsNotExist(err) {
		err = ltfile.CreateDir(path)
		if err != nil {
			doxlog.Errorf("could not create %s: %v", path, err)
		}
	}
	serverMux := http.NewServeMux()
	if len(mediaUrl) > 0 {
		mediaRedirectUrl = mediaUrl
		serverMux.HandleFunc("/a/", MediaRedirect)
		serverMux.HandleFunc("/m/", MediaRedirect)
		if len(pdfViewer) > 0 { // the user has provided a path to a pdf viewer
			// We assume the viewer path has /viewer in it.
			// If we ever switch viewer libraries, that might not be the case
			// and this code will break.  So it is fragile.
			// We need a better way to handle this.
			parts := strings.Split(pdfViewer, "/viewer")
			viewer := parts[0] + "/viewer/"
			if !strings.HasPrefix(viewer, "/") {
				viewer = "/" + viewer
			}
			serverMux.HandleFunc(viewer, MediaRedirect)
		}
	}
	fs := http.FileServer(http.Dir(path))
	serverMux.Handle("/", fs)
	doxlog.Infof("Generated public site is at http://127.0.0.1:%s", port)
	doxlog.Infof("Serving from local directory %s", path)
	stripBaseHref := baseHref
	if len(baseHref) > 0 {
		if !strings.HasSuffix(stripBaseHref, "/") {
			stripBaseHref = stripBaseHref + "/"
		}
		serverMux.Handle(baseHref, http.StripPrefix(stripBaseHref, addHeaders(fs)))
	}
	err := http.ListenAndServe(":"+port, serverMux)
	if err != nil {
		log.Fatal(err)
	}
}

func ServeGeneratedTestSite(sitePath, baseHref, port, mediaUrl, pdfViewer string) {
	// add a handler for the local generated website
	if _, err := os.Stat(sitePath); os.IsNotExist(err) {
		err = ltfile.CreateDir(sitePath)
		if err != nil {
			doxlog.Errorf("could not create %s: %v", sitePath, err)
		}
	}
	serverMux := http.NewServeMux()
	if len(mediaUrl) > 0 {
		mediaRedirectUrl = mediaUrl
		serverMux.HandleFunc("/a/", MediaRedirect)
		serverMux.HandleFunc("/m/", MediaRedirect)
		if len(pdfViewer) > 0 { // the user has provided a path to a pdf viewer
			// We assume the viewer path has /viewer in it.
			// If we ever switch viewer libraries, that might not be the case
			// and this code will break.  So it is fragile.
			// We need a better way to handle this.
			parts := strings.Split(pdfViewer, "/viewer")
			viewer := parts[0] + "/viewer/"
			if !strings.HasPrefix(viewer, "/") {
				viewer = "/" + viewer
			}
			serverMux.HandleFunc(viewer, MediaRedirect)
		}
	}
	fs := http.FileServer(http.Dir(sitePath))
	serverMux.Handle("/", fs)
	doxlog.Infof("Generated test site is at http://127.0.0.1:%s", port)
	stripBaseHref := baseHref
	if len(baseHref) > 0 {
		if !strings.HasSuffix(stripBaseHref, "/") {
			stripBaseHref = stripBaseHref + "/"
		}
		serverMux.Handle(baseHref, http.StripPrefix(stripBaseHref, addHeaders(fs)))
	}
	err := http.ListenAndServe(":"+port, serverMux)
	if err != nil {
		log.Fatal(err)
	}
}

// addHeaders This function was added to add headers to requests to serve
// the generated DCS (test and public) on the user's machine.
// For example, when a user is working on templates or makes changes
// to a css, we want to use headers that prevent the browser from
// caching files, which makes it look like changes are not taking effect
// in the generated services and books.
func addHeaders(fs http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// Cache-Control: no-cache means ok to cache, but always
		//                check with server to see if new version exists.
		// Cache-Control: no-store means not allowed to cache at all.
		w.Header().Add("Cache-Control", "no-store")
		fs.ServeHTTP(w, r)
	}
}
func MediaRedirect(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, mediaRedirectUrl+r.RequestURI, 302)
}
func ServeMedia(path, baseHref, port string) {
	// add a handler for the local generated website
	if _, err := os.Stat(path); os.IsNotExist(err) {
		doxlog.Errorf("Local media path does not exist: %s", path)
	} else {
		serverMux := http.NewServeMux()
		fs := http.FileServer(http.Dir(path))
		serverMux.Handle("/", fs)
		doxlog.Infof("media files are served from http://127.0.0.1:%s", port)
		stripBaseHref := baseHref
		if !strings.HasSuffix(stripBaseHref, "/") {
			stripBaseHref = stripBaseHref + "/"
		}
		if len(baseHref) > 0 {
			serverMux.Handle(baseHref, http.StripPrefix(stripBaseHref, fs))
		}
		err := http.ListenAndServe(":"+port, serverMux)
		if err != nil {
			log.Fatal(err)
		}
	}
}
func openBrowser(url string) {
	var err error

	switch runtime.GOOS {
	case "linux":
	case "windows":
		err = exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	case "darwin":
		err = exec.Command("open", url).Start()
	default:
		err = fmt.Errorf("unsupported platform")
	}
	if err != nil {
		doxlog.Errorf("error opening browser to doxa: %v", err)
	}

}

func (s *server) loadTemplates() {
	var err error
	// Create a new FuncMap with predefined functions
	funcMap := tmplfunc.New().Map()

	// custom sort method for various ui components
	funcMap["sort"] = func(items interface{}, field string) interface{} {
		switch items := items.(type) {
		case []picoWidget.Group:
			sort.Slice(items, func(i, j int) bool {
				return items[i].Name < items[j].Name
			})
			return items
		case []picoWidget.Project:
			sort.Slice(items, func(i, j int) bool {
				return items[i].DbName < items[j].DbName
			})
			return items
		default:
			return items
		}
	}

	// Create a new template and add the custom functions
	t := template.New("").Funcs(funcMap)
	s.templates, err = t.ParseFS(
		s.embeddedTemplates,
		"templates/webApp/bootstrap/*.gohtml",
		"templates/webApp/pico/body/*.gohtml",
		"templates/webApp/pico/head/*.gohtml",
		"templates/webApp/pico/index/*.gohtml",
		"templates/webApp/pico/modal/*.gohtml",
		"templates/webApp/pico/nav/*.gohtml",
		"templates/webApp/pico/page/*.gohtml",
		"templates/webApp/pico/prototype/*.gohtml",
		"templates/webApp/pico/widget/form/*.gohtml",
		"templates/webApp/pico/widget/gitlab/*.gohtml",
		"templates/webApp/pico/widget/page/*.gohtml",
		"templates/webApp/pico/widget/poller/*.gohtml",
		"templates/webApp/pico/widget/semantic/*.gohtml",
		"templates/webApp/pico/widget/table/*.gohtml",
		"templates/webApp/pico/widget/generic/*.gohtml",
	)
	if err != nil {
		log.Fatal(err)
	}
}
