package servers

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"net/http"
	"path"
	"strings"
)

func (s *server) jsRoutes(router *mux.Router) {
	router.HandleFunc("/js/read", s.handleJsRead()).Methods(http.MethodGet)
	router.HandleFunc("/js/update", s.handleJsUpdate()).Methods(http.MethodPut)
}

// handleJsRead returns the content of the site app.js file
func (s *server) handleJsRead() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonSrcCodeResponse)
		src := r.URL.Query().Get("src")
		if len(src) == 0 {
			resp.Status = "missing path"
			respond(w, http.StatusBadRequest, resp)
			return
		}

		var filename string

		if strings.HasSuffix(src, "app.js") {
			filename = path.Join(config.DoxaPaths.AssetsPath, "js", "app.js")
		} else {
			resp.Status = "unknown js file"
			respond(w, http.StatusBadRequest, resp)
			return
		}
		var err error
		resp.Value, err = ltfile.GetFileContent(filename)
		if err != nil {
			resp.Status = "could not read js file"
			respond(w, http.StatusBadRequest, resp)
		}
		resp.Status = "OK"
		resp.ID = filename
		resp.FilePath = filename
		resp.Title = "Javascript"
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleJsUpdate updates the content of the site app.js file
func (s *server) handleJsUpdate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonCssUpdateResponse)
		text := r.URL.Query().Get("text")
		if len(text) == 0 {
			resp.Status = "missing text"
			respond(w, http.StatusBadRequest, resp)
			return
		}
		fileSource := path.Join(config.DoxaPaths.AssetsPath, "js", "app.js")
		doxlog.Infof(fileSource)
		err := s.siteBuilder.WriteJsFile(fileSource, text)
		if err != nil {
			resp.Status = fmt.Sprintf("%v", err)
		} else {
			resp.Status = "OK"
			resp.Message = "updated"
		}
		respond(w, http.StatusOK, resp)
		return
	}
}
