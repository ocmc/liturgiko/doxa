package servers

import (
	"bytes"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/liturgiko/doxa/app"
	"github.com/liturgiko/doxa/pkg/atempl"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/enums/calendarTypes"
	"github.com/liturgiko/doxa/pkg/enums/fileSystemActions"
	"github.com/liturgiko/doxa/pkg/enums/templateTypes"
	"github.com/liturgiko/doxa/pkg/enums/versions"
	"github.com/liturgiko/doxa/pkg/layouts"
	"github.com/liturgiko/doxa/pkg/ldp"
	"github.com/liturgiko/doxa/pkg/lmlFormatter"
	"github.com/liturgiko/doxa/pkg/ltm"
	"github.com/liturgiko/doxa/pkg/parser"
	"github.com/liturgiko/doxa/pkg/table"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"github.com/liturgiko/doxa/pkg/utils/stack"
	"github.com/liturgiko/doxa/pkg/utils/stamp"
	"github.com/liturgiko/doxa/pkg/valuemap"
	"html/template"
	"net/http"
	"net/url"
	"os"
	"path"
	"runtime"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

func (s *server) templateRoutes(router *mux.Router) {
	router.HandleFunc("/template/parse", s.handleTemplateParse()).Methods(http.MethodGet)
	router.HandleFunc("/template/read", s.handleTemplateRead()).Methods(http.MethodGet)
	router.HandleFunc("/template/update", s.handleTemplateUpdate()).Methods(http.MethodPut)
	router.HandleFunc("/template/delete", s.handleTemplateDelete()).Methods(http.MethodDelete)
	router.HandleFunc("/template/dir/delete", s.handleTemplateDirDelete()).Methods(http.MethodDelete)
	router.HandleFunc("/template/copy", s.handleTemplateCopy()).Methods(http.MethodPut)
	router.HandleFunc("/template/dir/browse", s.handleTemplateDirBrowse()).Methods(http.MethodGet)
	router.HandleFunc("/template/dir/add", s.handleTemplateDirAdd()).Methods(http.MethodPut)
	router.HandleFunc("/template/dir/rename", s.handleTemplateDirRename()).Methods(http.MethodPut)
	router.HandleFunc("/template/dir/list/top", s.handleTemplateDirListTop()).Methods(http.MethodGet)
	router.HandleFunc("/template/search", s.handleTemplateSearch()).Methods(http.MethodGet)
	router.HandleFunc("/template/patterns", s.handleLmlPatterns()).Methods(http.MethodGet, http.MethodPut)
	router.HandleFunc("/template/status/types", s.handleLmlStatusTypes()).Methods(http.MethodGet)
}

// handleTemplateRead returns the content of the specified LML (template) file
func (s *server) handleTemplateRead() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonSrcCodeResponse)
		var err error
		src := r.URL.Query().Get("src")
		src, err = url.QueryUnescape(src)
		filePath, title, err := s.canonicalTemplateFilePath(src, fileSystemActions.Read)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("%v", err)
			respond(w, http.StatusBadRequest, resp)
			return
		}
		primary := s.siteBuilder.TemplateMapper.GetPrimary()
		fallback := s.siteBuilder.TemplateMapper.GetFallback()
		resp.ID = src
		if s.siteBuilder.Config.TemplatesFallbackEnabled {
			if strings.HasPrefix(filePath, primary) {
				if !strings.HasPrefix(src, "[\"primary") {
					resp.ID = fmt.Sprintf("[\"primary\",%s", src[1:])
				}
			} else if strings.HasPrefix(filePath, fallback) {
				if !strings.HasPrefix(src, "[\"fallback") {
					resp.ID = fmt.Sprintf("[\"fallback\",%s", src[1:])
				}
			}
		}
		resp.FilePath = filePath
		resp.Title = title
		resp.ReadOnly = s.siteBuilder.TemplateMapper.IsReadOnly(resp.FilePath)
		resp.Value, err = s.siteBuilder.TemplateMapper.GetContent(filePath)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = "invalid template file path"
			respond(w, http.StatusBadRequest, resp)
			return
		}
		resp.Status = "OK"
		resp.Message = "OK"
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleTemplateDelete deletes the specified LML (template) file
func (s *server) handleTemplateDelete() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonLmlUpdateResponse)

		src := r.URL.Query().Get("src")
		filePath, _, err := s.canonicalTemplateFilePath(src, fileSystemActions.Delete)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("%v", err)
			respond(w, http.StatusBadRequest, resp)
			return
		}
		resp.ID = src
		resp.FilePath = filePath
		err = os.Remove(filePath)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("%s: %v", src, err)
		} else {
			resp.Status = "OK"
			resp.Message = "deleted"
		}
		respond(w, http.StatusOK, resp)
		//commented out to avoid git conflict
		/*
			err = s.removeTemplate(filePath)
			if err != nil {
				doxlog.Errorf("error committing removal of %s: %v", filePath, err)
			}*/
		return
	}
}

// handleTemplateDirDelete deletes the specified directory in the templates folder and
// all contents recursively
func (s *server) handleTemplateDirDelete() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonKeyPathResponse)
		var err error
		src := r.URL.Query().Get("path")
		resp.Path, err = stack.NewStringStack(src)
		if err != nil {
			resp.Status = "BadRequest"
			resp.Message = fmt.Sprintf("%v", err)
			respond(w, http.StatusOK, resp)
		}
		var subPathStack *stack.StringStack
		if resp.Path.First() == "templates" {
			subPathStack = resp.Path.Remove(0)
		} else {
			subPathStack = resp.Path
			resp.Path = new(stack.StringStack)
			resp.Path = resp.Path.Enqueue("templates")
		}
		resp.Path.Pop() // so we do not return the path to a deleted directory
		dirPath, _, err := s.canonicalTemplateDirPath(subPathStack.Json(false), fileSystemActions.Delete)
		if err != nil {
			resp.Status = "BadRequest"
			resp.Message = fmt.Sprintf("%v", err)
			respond(w, http.StatusOK, resp)
		}
		err = os.RemoveAll(dirPath)
		if err != nil {
			resp.Status = "BadRequest"
			resp.Message = fmt.Sprintf("%s: %v", dirPath, err)
			respond(w, http.StatusOK, resp)
		} else {
			resp.Status = "OK"
			resp.Message = fmt.Sprintf("deleted %s", dirPath)
		}
		respond(w, http.StatusOK, resp)
		/*err = s.removeTemplate(dirPath)
		if err != nil {
			doxlog.Errorf("error committing removal of %s: %v", dirPath, err)
		}
		*/

		return
	}
}

// handleTemplateCopy copies the content of the specified LML (template) file
// to the primary directory, preserving the subdirectory path.
func (s *server) handleTemplateCopy() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonLmlUpdateResponse)
		if !s.siteBuilder.Config.TemplatesFallbackEnabled {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = "fallback is not enabled"
			respond(w, http.StatusOK, resp)
			return
		}
		src := r.URL.Query().Get("src")
		if !strings.Contains(src, "fallback") {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = "invalid request"
			respond(w, http.StatusOK, resp)
			return
		}
		src = strings.Replace(src, "fallback", "primary", 1)
		filePath, _, err := s.canonicalTemplateFilePath(src, fileSystemActions.Copy)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("%v", err)
			respond(w, http.StatusBadRequest, resp)
			return
		}
		text := r.URL.Query().Get("text")
		if len(text) == 0 {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("no template content to copy")
			respond(w, http.StatusOK, resp)
			return
		}
		// format contents to canonical format for LML
		var formattedLines []string
		formattedLines, err = lmlFormatter.FormatLines(strings.Split(text, "\n"))
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("error formatting template: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		err = ltfile.WriteLinesToFile(filePath, formattedLines)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("error writing template to %s: %v", filePath, err)
			respond(w, http.StatusOK, resp)
			return
		}
		resp.ID = src
		resp.FilePath = filePath
		resp.Status = "OK"
		resp.Message = "updated"
		respond(w, http.StatusOK, resp)
		//disabled to avoid conflict with git remote
		/*go func() {
			err = s.commitTemplate(filePath)
			if err != nil {
				doxlog.Errorf("error committing template %s: %v", filePath, err)
			}
		}()*/

		return
	}
}

// handleTemplateUpdate updates the content of the specified LML (template) file
func (s *server) handleTemplateUpdate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonLmlUpdateResponse)
		text := r.URL.Query().Get("text")
		src := r.URL.Query().Get("src")
		filePath, _, err := s.canonicalTemplateFilePath(src, fileSystemActions.Update)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("%v", err)
			respond(w, http.StatusBadRequest, resp)
			return
		}
		if len(text) == 0 {
			text, err = getTemplateContents(filePath)
			if err != nil {
				resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
				resp.Message = fmt.Sprintf("error initializing template %s: %v", src, err)
				respond(w, http.StatusBadRequest, resp)
				return
			}
		}
		// format contents to canonical format for LML
		var formattedLines []string
		formattedLines, err = lmlFormatter.FormatLines(strings.Split(text, "\n"))
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("error formatting template: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		err = ltfile.WriteLinesToFile(filePath, formattedLines)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("error writing template to %s: %v", filePath, err)
			respond(w, http.StatusOK, resp)
			return
		}
		resp.ID = src
		resp.FilePath = filePath
		resp.Status = "OK"
		resp.Message = "updated"
		respond(w, http.StatusOK, resp)
		if _, ok := app.Ctx.SyncManager.RwSyncer.GetFromMap(app.Ctx.Paths.PrimaryTemplatesPath); !ok {
			//disabled to prevent git conflict
			/*	go func() {
				err = s.commitTemplate(filePath)
				if err != nil {
					doxlog.Errorf("error committing template %s: %v", filePath, err)
				}
				return
			}()*/
		}
		return
	}
}

// handleTemplateParse parses the specified template and returns any errors found.
// This function is called by the GUI to display errors and a preview of a template in the LML Editor.
func (s *server) handleTemplateParse() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		timeStamp := stamp.NewStamp("parse")
		timeStamp.Start()
		resp := new(JsonLmlParseResponse)
		resp.HrefHost = ""
		s.siteBuilder.RelayMuted = false
		s.siteBuilder.RelayUsesStandardOut = false
		src := r.URL.Query().Get("src")
		filePath, _, err := s.canonicalTemplateFilePath(src, fileSystemActions.Read)
		if err != nil {
			doxlog.Error(fmt.Sprintf("%s > error getting template filepath: %v", handlerData(r), err))
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("%v", err)
			respond(w, http.StatusBadRequest, resp)
			return
		}
		// get template content
		var content string
		content, err = ltfile.GetFileContent(filePath)
		if err != nil {
			doxlog.Error(fmt.Sprintf("%s > error getting template file content: %v", handlerData(r), err))
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = "error getting template file content"
			respond(w, http.StatusBadRequest, resp)
			return
		}
		// set primary and backup libraries to use for generation
		// genLangs holds a slice of GenLib.
		var genLangs []*atempl.TableLayout
		strLibraryCombo := r.URL.Query().Get("libraryCombo")
		if len(strLibraryCombo) == 0 {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = "missing library combo index"
			doxlog.Errorf(fmt.Sprintf("%s > %s", handlerData(r), resp.Message))
			respond(w, http.StatusOK, resp)
			return
		}
		// get the requested layout
		var layout *atempl.TableLayout
		layout, err = layouts.Manager.GetSiteBuildLayout(strLibraryCombo)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("%s > error getting layout %s: %v", handlerData(r), strLibraryCombo, err)
			doxlog.Errorf(fmt.Sprintf("%s", resp.Message))
			respond(w, http.StatusOK, resp)
			return
		}
		// we set this because the database holds the selected layouts set from the app Generate page.
		// We set PreviewLayoutAcronym, so it will process this layout irrespective of what the database says is selected.
		s.siteBuilder.Config.PreviewLayoutAcronymn = layout.Acronym

		genLangs = append(genLangs, layout)
		base := path.Base(filePath)

		var strDate string

		// we do not set the date for books,
		// but do so for services (se.) and blocks (bl.)
		if !strings.HasPrefix(base, "bk.") {
			strDate = r.URL.Query().Get("date")
		}
		var theLdp ldp.LDP
		if len(strDate) > 0 {
			var myDate time.Time
			myDate, err = time.Parse("2006-01-02", strDate)
			if err != nil {
				resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
				resp.Message = fmt.Sprintf("error parsing date %s", strDate)
				doxlog.Errorf(fmt.Sprintf("%s > %s: %v", handlerData(r), resp.Message, err))
				respond(w, http.StatusOK, resp)
				return
			}
			theLdp, err = ldp.NewLDPYMD(myDate.Year(), int(myDate.Month()), myDate.Day(), calendarTypes.Gregorian)
			if err != nil {
				resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
				resp.Message = fmt.Sprintf("error creating LDP for date %s", strDate)
				doxlog.Errorf(fmt.Sprintf("%s > %s: %v", handlerData(r), resp.Message, err))
				respond(w, http.StatusOK, resp)
				return
			}
		} else {
			theLdp, err = ldp.NewLDP()
			if err != nil {
				resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
				resp.Message = "error creating LDP for today's date"
				doxlog.Errorf(fmt.Sprintf("%s > %s: %v", handlerData(r), resp.Message, err))
				respond(w, http.StatusOK, resp)
				return
			}
		}
		// reset the resolver
		s.siteBuilder.Resolver.Reset()

		// set builder to show topic-key of missing values
		s.siteBuilder.Resolver.SetShowUnresolvedTopicKey(true)
		// let the build process know this is a build for the preview pane
		parms := parser.Parms{
			TemplatePath:        filePath,
			TemplateContent:     content,
			Realm:               s.siteBuilder.Config.Realm,
			TheVersion:          versions.All,
			GenLangs:            genLangs,
			TheResolver:         s.siteBuilder.Resolver,
			MediaReverseLookup:  &app.Ctx.Keyring.MediaReverseLookup,
			TheMedia:            valuemap.NewValueMapper(),
			Values:              valuemap.NewValueMapper(),
			HtmlCss:             s.siteBuilder.Config.HtmlCss,
			PdfCss:              s.siteBuilder.Config.PdfCss,
			TheCalendarType:     s.siteBuilder.Config.Calendar,
			TheLdp:              &theLdp,
			TemplateDir:         s.siteBuilder.Config.TemplatesPrimaryPath,
			YearOverride:        s.siteBuilder.Config.YearOverride,
			FlagMissingRidTk:    s.siteBuilder.Config.FlagMissingRidTk,
			FlagMissingRidValue: s.siteBuilder.Config.FlagMissingRidValue,
			IncludeCovers:       s.siteBuilder.Config.IncludeCovers,
		}
		var lml *parser.LML
		lml, err = parser.NewLMLParser(&parms)
		if err != nil {
			s.siteBuilder.Resolver.SetShowUnresolvedTopicKey(false)
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = "error creating LML Parser"
			doxlog.Error(fmt.Sprintf("%s > %s", handlerData(r), resp.Message))
			respond(w, http.StatusOK, resp)
			return
		}
		if strings.HasPrefix(base, "bl") { // block
			lml.Listener.IsBlockTemplate = true
		} else if strings.HasPrefix(base, "bk") { // book
			lml.Listener.IsBlockTemplate = false
		} else if strings.HasPrefix(base, "se") { // dated service
			lml.Listener.IsBlockTemplate = false
		} else {
			s.siteBuilder.Resolver.SetShowUnresolvedTopicKey(false)
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = "invalid file prefix: expected bl, bk, or se"
			doxlog.Errorf("%s > %s", handlerData(r), resp.Message)
			respond(w, http.StatusOK, resp)
			return
		}
		resp.ID = src
		resp.FilePath = filePath
		resp.Status = "OK"
		resp.Message = "No errors"
		var atem *atempl.ATEM
		atem, resp.ParseErrors = lml.WalkTemplate()
		s.siteBuilder.Resolver.SetShowUnresolvedTopicKey(false)

		noErrors := len(resp.ParseErrors) == 0

		// Serialize the parse data to the database for future use.
		// The next time a user opens a template in the editor,
		// we can load using the serialized version rather than
		// parsing it again.  This will save time.
		// 2023/10/15 MAC.  Commented out for now due to
		// suspicions it increases the size of the database
		// too much.
		//prettyPrint := true
		//go s.siteBuilder.SaveParseData(atem, noErrors, prettyPrint)

		if noErrors { // write the preview html file
			doxlog.Info("preview - no errors in template")
			resp.Date = fmt.Sprintf("%d-%02d-%02d", lml.Listener.ATEM.Year, lml.Listener.ATEM.Month, lml.Listener.ATEM.Day)
			var writeErrorMessages []string
			msgChan := make(chan string, 10000) // we use this to get the url
			errorChan := make(chan error, 10000)
			year := strconv.Itoa(atem.Year)
			idPath, title := s.siteBuilder.PathAndTitleFromTemplateID(atem.ID)
			htmlPathOut, baseHref := s.siteBuilder.GetHtmlTempPathOut(atem.Type, year, idPath)
			s.siteBuilder.CopyAppDoxaCssJs()
			doxlog.Infof("writing preview html to %s", htmlPathOut)
			atem.Title = title
			var writerWg sync.WaitGroup
			writerWg.Add(1)
			go s.siteBuilder.WriteHTML(&writerWg,
				atem,
				s.siteBuilder.Config.DocNavbar,
				s.siteBuilder.Config.Footer,
				htmlPathOut,
				baseHref,
				s.siteBuilder.Config.Links,
				s.siteBuilder.Config.Scripts,
				s.siteBuilder.Config.Templates,
				false,
				true,
				true,
				s.siteBuilder.Config.PreviewLayoutAcronymn,
				msgChan,
				errorChan)

			select {
			case err := <-errorChan:
				writeErrorMessages = append(writeErrorMessages, fmt.Sprintf("%v", err))
				doxlog.Error(err.Error())
			case msg := <-msgChan:
				// if msg says the html file was written, set the htmlPath for the response,
				// so it can be loaded into the preview iFrame.
				if strings.HasPrefix(msg, "wrote HTML to") {
					i := strings.Index(msg, app.Ctx.Paths.SiteGenTestPath)
					if i == -1 {
						resp.Status = "OK"
						resp.Message = fmt.Sprintf("unexpected path %s", msg)
						doxlog.Error(resp.Message)
						respond(w, http.StatusOK, resp)
						return
					}
					resp.HtmlPath = path.Join("test", msg[(len(app.Ctx.Paths.SiteGenTestPath))+15:])
				}
			}

			writerWg.Wait()
			close(errorChan)
			close(msgChan)
			s.siteBuilder.Config.PreviewLayoutAcronymn = ""
			if len(writeErrorMessages) > 0 { // just send back the first error
				resp.Message = fmt.Sprintf("writer error: %s", writeErrorMessages[0])
			} else {
				resp.Status = "OK"
				resp.Message = "No errors"
			}
		} else {
			resp.Status = fmt.Sprintf("%d", http.StatusOK)
			if len(resp.ParseErrors) == 1 {
				resp.Message = "there was one error"
			} else {
				resp.Message = fmt.Sprintf("there were %d errors", len(resp.ParseErrors))
			}
			// write an error.html file to be loaded into the preview iFrame
			errorPath := path.Join(config.DoxaPaths.PreviewPath, "errors.html")
			err = WriteErrorHtml(errorPath, resp.ParseErrors)
			if err != nil {
				resp.Status = fmt.Sprintf("%d", http.StatusInternalServerError)
				resp.Message = "error writing errors.html"
				doxlog.Errorf(err.Error())
				respond(w, http.StatusOK, resp)
				return
			}
			resp.HtmlPath = "test/t/errors.html"
		}
		resp.TemplatesByNbr = atem.TemplatesByNbr
		// send template table data
		resp.TemplatesTableData = new(table.Data)
		resp.TemplatesTableData.Title = "Templates Used"
		resp.TemplatesTableData.Desc = "This table lists each template called in sequence starting from the top template.  A template can be called more than once, but it will only show once in this table.  If you double-click a template ID, it will open the template in another tab."
		resp.TemplatesTableData.Headings = []string{"Nbr", "Template"}
		resp.TemplatesTableData.Filter = 1
		resp.TemplatesTableData.Widths = []int{5, 96}
		// create the data to display the templates used as a table
		// They are numbered according to which was called first,
		// but are not in sequence in a map.
		// So, get the keys
		var keys []int
		for k := range atem.TemplatesByNbr {
			keys = append(keys, k)
		}
		sort.Ints(keys)
		// now build the table rows in the sorted order
		delimiter := app.Ctx.Paths.PathSeparator
		for k := range keys {
			v := atem.TemplatesByNbr[k]
			if len(v) == 0 {
				continue
			}
			var row []*table.Cell
			ck := new(table.Cell)
			ck.Value = fmt.Sprintf("%03d", k)
			row = append(row, ck)
			cv := new(table.Cell)
			cv.Value = strings.ReplaceAll(v, delimiter, fmt.Sprintf(" %s ", delimiter))
			if k != 1 { // these templates are for #1, so can't reopen it.  It is already open.
				cv.Target = "_blank"
				cv.Href = fmt.Sprintf("ide?id=%s&date=%s", url.QueryEscape(v), url.QueryEscape(resp.Date))
			}
			row = append(row, cv)
			resp.TemplatesTableData.Rows = append(resp.TemplatesTableData.Rows, row)
		}
		if atem.Type == templateTypes.Block {
			resp.IsBlock = true
		}
		doxlog.Infof("%d cores available", runtime.NumCPU())
		doxlog.Info(timeStamp.FinishMillis())
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleTemplateDirBrowse provides content info for specified templates directory in the current site
func (s *server) handleTemplateDirBrowse() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonDirContentsResponse)
		doxlog.Debugf("received request")
		resp.FallbackEnabled = s.siteBuilder.Config.TemplatesFallbackEnabled
		resp.Path = new(stack.StringStack)
		var thePath string
		var err error
		jsonArray := r.URL.Query().Get("path")
		if jsonArray == "[]" || jsonArray == `["templates"]` {
			resp.Path.Push("templates")
			if s.siteBuilder.Config.TemplatesFallbackEnabled {
				i := new(DirItem)
				i.IsDir = true
				i.Name = ltm.PRIMARY
				resp.Values = append(resp.Values, i)
				i = new(DirItem)
				i.IsDir = true
				i.Name = ltm.FALLBACK
				resp.InFallbackDir = true // not necessarily true, but it gives desired result
				resp.Values = append(resp.Values, i)
				resp.Status = "OK"
				respond(w, http.StatusOK, resp)
				return
			} else {
				thePath = s.siteBuilder.TemplateMapper.GetRoot()
			}
		} else {
			if !strings.HasPrefix(jsonArray, `["templates"`) {
				jsonArray = strings.Replace(jsonArray, `[`, `["templates",`, 1)
			}
			resp.Path, err = stack.NewStringStack(jsonArray)
			if err != nil {
				resp.Status = "BadRequest"
				resp.Message = "invalid template file path"
				respond(w, http.StatusOK, resp)
				return
			}
			var subDirStack *stack.StringStack
			if resp.Path.First() == "templates" {
				subDirStack = resp.Path.Remove(0)
			} else {
				subDirStack = resp.Path
				resp.Path = new(stack.StringStack)
				resp.Path = resp.Path.Enqueue("templates")
			}
			thePath, _, err = s.canonicalTemplateDirPath(subDirStack.Json(false), fileSystemActions.Read)
			if err != nil {
				resp.Status = "BadRequest"
				resp.Message = "invalid template file path"
				respond(w, http.StatusOK, resp)
				return
			}
		}
		resp.InFallbackDir = strings.HasPrefix(thePath, s.siteBuilder.TemplateMapper.GetFallback())
		entries, err := ltfile.EntriesInDir(thePath)
		if err != nil {
			resp.Status = "BadRequest"
			resp.Message = "invalid template file path"
			respond(w, http.StatusOK, resp)
			return
		}
		for _, entry := range entries {
			if entry.Name() == "LICENSE" ||
				entry.Name() == "README.md" ||
				strings.HasPrefix(entry.Name(), ".") {
				continue
			}
			i := new(DirItem)
			i.IsDir = entry.IsDir()
			i.Name = entry.Name()
			resp.Values = append(resp.Values, i)
		}
		resp.Status = "OK"
		resp.Message = "Retrieved directory contents"
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleTemplateDirAdd provides content info for specified templates directory in the current site.
// It returns the new contents of the parent directory.
func (s *server) handleTemplateDirAdd() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonDirContentsResponse)
		resp.FallbackEnabled = s.siteBuilder.Config.TemplatesFallbackEnabled
		var err error
		src := r.URL.Query().Get("path")
		resp.Path, err = stack.NewStringStack(src)
		if err != nil {
			resp.Status = "BadRequest"
			resp.Message = fmt.Sprintf("bad path %s", src)
			respond(w, http.StatusOK, resp)
		}
		var subPathStack *stack.StringStack
		if resp.Path.First() == "templates" {
			subPathStack = resp.Path.Remove(0)
		} else {
			subPathStack = resp.Path
			resp.Path = new(stack.StringStack)
			resp.Path = resp.Path.Enqueue("templates")
		}
		dirPath, _, err := s.canonicalTemplateDirPath(subPathStack.Json(false), fileSystemActions.Read)
		if err != nil {
			resp.Status = "BadRequest"
			resp.Message = fmt.Sprintf("%v", err)
			respond(w, http.StatusOK, resp)
		}
		// get the new directory name
		dirName := r.URL.Query().Get("dir")
		if len(dirName) == 0 {
			resp.Status = "BadRequest"
			resp.Message = "missing new directory name"
			respond(w, http.StatusOK, resp)
			return
		}
		newDirPath := path.Join(dirPath, dirName)
		err = ltfile.CreateDir(newDirPath)
		if err != nil {
			resp.Status = "BadRequest"
			resp.Message = fmt.Sprintf("error creating dir %s: %v", dirPath, err)
			respond(w, http.StatusOK, resp)
			return
		}

		// get the contents of the parent directory, which will include the newly added directory
		entries, err := ltfile.EntriesInDir(dirPath)
		if err != nil {
			resp.Status = "BadRequest"
			resp.Message = "invalid template file path"
			respond(w, http.StatusOK, resp)
			return
		}
		for _, entry := range entries {
			if entry.Name() == ".DS_Store" {
				continue
			}
			i := new(DirItem)
			i.IsDir = entry.IsDir()
			i.Name = entry.Name()
			resp.Values = append(resp.Values, i)
		}
		resp.Status = "OK"
		resp.Message = "OK"
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleTemplateDirRename provides content info for specified templates directory in the current site.
// It returns the new contents of the parent directory.
func (s *server) handleTemplateDirRename() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonDirContentsResponse)
		resp.FallbackEnabled = s.siteBuilder.Config.TemplatesFallbackEnabled
		// TODO: Not implemented. Needs to be reworked
		//src := r.URL.Query().Get("path")
		//dirPath, _, err := s.canonicalTemplateDirPath(src, fileSystemActions.Read)
		//if err != nil {
		//	resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
		//	resp.Message = fmt.Sprintf("%v", err)
		//	respond(w, http.StatusBadRequest, resp)
		//}
		//resp.Path = new(stack.StringStack)
		//resp.Path.Push("doxa")
		//resp.Path.Push("sites")
		//resp.Path.Push(config.DoxaPaths.Project)
		//resp.Path.Push("templates")
		////oldPath := resp.Path.Join(string(filepath.Separator)) // this is wrong for sure, need to drop last folder name
		//// create the new directory
		//dirName := r.URL.Query().Get("dir")
		//if len(dirName) == 0 {
		//	resp.Status = "missing new directory name"
		//	respond(w, http.StatusBadRequest, resp)
		//	return
		//}
		//newPath := filepath.Join(path, dirName)
		//err = ltfile.RenameDir(oldPath, newPath)
		//if err != nil {
		//	resp.Status = fmt.Sprintf("error creating dir %s: %v", newPath, err)
		//	respond(w, http.StatusBadRequest, resp)
		//	return
		//}
		//// block actions within the subscriptions directory
		//if strings.HasPrefix(oldPath, config.DoxaPaths.SubscriptionsPath) {
		//	resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
		//	respond(w, http.StatusOK, resp)
		//	return
		//}
		//
		//// get the contents of the parent directory, which will include the newly added directory
		//entries, err := ltfile.EntriesInDir(path)
		//if err != nil {
		//	resp.Status = "invalid template file path"
		//	respond(w, http.StatusBadRequest, resp)
		//	return
		//}
		//for _, entry := range entries {
		//	if entry.Name() == ".DS_Store" {
		//		continue
		//	}
		//	i := new(DirItem)
		//	i.IsDir = entry.IsDir()
		//	i.Name = entry.Name()
		//	resp.Values = append(resp.Values, i)
		//}
		// TODO
		// resp.Path
		resp.Status = "OK"
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleTemplateDirListTop provides an array of the immediate child folders of the templates directory.
func (s *server) handleTemplateDirListTop() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonDirTopListResponse)
		var err error
		resp.Values, err = ltfile.DirsInDir(config.DoxaPaths.PrimaryTemplatesPath, true)
		if err != nil {
			resp.Status = "BadRequest"
			resp.Message = fmt.Sprintf("error reading dirs in %s: %v", config.DoxaPaths.PrimaryTemplatesPath, err)
			respond(w, http.StatusOK, resp)
			return
		}
		resp.Status = "OK"
		resp.Message = "OK"
		respond(w, http.StatusOK, resp)
		return
	}
}

// getTemplateContents gives the initial contents for a template.
// templatePath must be a full path to the template, and have a directory
// called templates.
func getTemplateContents(templatePath string) (string, error) {
	var err error
	dirs, base := ltstring.ToDirSegmentsAndBase(templatePath)
	var index int
	if index = ltstring.IndexInSlice(dirs, "templates"); index < 0 {
		return "", fmt.Errorf("template not in a templates directory")
	}
	separator := "/"
	templateId := strings.Join(dirs[index+1:], separator)
	if len(templateId) == 0 {
		templateId = base
	} else {
		templateId = templateId + base
	}
	templateIdBase := base
	if strings.HasSuffix(templateIdBase, ".lml") {
		templateIdBase = templateIdBase[:len(templateIdBase)-4]
	}
	parts := strings.Split(templateIdBase, ".")
	var templateType templateTypes.TemplateType
	templateType = templateTypes.TypeForAcronym(parts[0])
	if templateType == -1 {
		return "", fmt.Errorf("invalid template type %s", parts[0])
	}

	parts = parts[1:]

	var data NewTemplate
	data.ID = templateId

	var templateToParse string
	switch templateType {
	case templateTypes.Block:
		templateToParse = BlockTemplate
	case templateTypes.Book:
		data.IndexTitleCodes = strings.Join(parts, ", ")
		data.LookupSid = fmt.Sprintf("%s.pdf.header", strings.Join(parts, "."))
		templateToParse = BookTemplate
	case templateTypes.Service:
		templateToParse = ServiceTemplate
		data.Month, data.Day, err = ltstring.MonthDayFromServiceTemplateId(templateIdBase)
		if err != nil {
			return "", err
		}
		data.LookupSid = fmt.Sprintf("%s.pdf.header", parts[len(parts)-1])
	default:
		return "", fmt.Errorf("invalid template type")
	}
	t := template.New("action")
	t, err = t.Parse(templateToParse)
	if err != nil {
		return "", err
	}
	var tpl bytes.Buffer
	if err = t.Execute(&tpl, data); err != nil {
		return "", err
	}

	return tpl.String(), nil
}

type NewTemplate struct {
	ID              string
	IndexTitleCodes string // populated if a book template. empty otherwise. Must be a delimited string
	LookupSid       string
	Month           int // if a service template
	Day             int // if a service template
}

const BlockTemplate = `id = "{{.ID}}"
type = "block"
status = "draft"

`
const BookTemplate = `id = "{{.ID}}"
type = "book"
status = "draft"

indexLastTitleOverride = "" // if you fill this in, instead of looking up the last part of the ID in the titles map (e.g. baptism) , this text will appear in the index.
indexTitleCodes = "{{.IndexTitleCodes}}" // these codes are used in the books index and must exist in the config for site/build/pages/titles/map

pdfSettings { 
// TODO: specify sid lookups and other header / footer information
// Each library (language) must have a key in its template.titles for the lookup, e.g. {{.LookupSid}}.
// When you uncomment pageHeaderEven or pageHeaderOdd, you might see this error:
// sid topic:key 'template.titles:{{.LookupSid}}' does not exist in database.
// To fix the error, you need to add the key {{.LookupSid}} to each library's template.titles.
// Or, change the key to another one already existing in template.titles.
// When you have done this, you may remove this and the above comment lines.
//  pageHeaderEven = center @lookup sid "template.titles:{{.LookupSid}}" ver 1 
//  pageHeaderOdd = center @lookup sid "template.titles:{{.LookupSid}}" ver 2 
  pageFooterEven = center @pageNbr
  pageFooterOdd = center @pageNbr
  pageNbr = 1
}

pdfPreface { // TODO: insert cover and credits)
  // pageBreak
}

`
const ServiceTemplate = `id = "{{.ID}}"
type = "service"
status = "draft"
month = {{.Month}}
day = {{.Day}}

indexLastTitleOverride = "" // if you fill this in, instead of looking up the service type (e.g. li or ma) in the titles map, this text will appear in the index.

pdfSettings {
// TODO: specify sid lookups and other header / footer information
// Each library (language) must have a key in its template.titles for the lookup, e.g. {{.LookupSid}}.
// When you uncomment pageHeaderEven or pageHeaderOdd, you might see this error:
// sid topic:key 'template.titles:{{.LookupSid}}' does not exist in database.
// To fix the error, you need to add the key {{.LookupSid}} to each library's template.titles.
// Or, change the key to another one already existing in template.titles.
// When you have done this, you may remove this and the above comment lines.
//  pageHeaderEven = center @lookup sid "template.titles:{{.LookupSid}}" sid "template.titles:d.on" ver 1 @date ver 1
//  pageHeaderOdd = center @lookup sid "template.titles:{{.LookupSid}}" sid "template.titles:d.on" ver 2 @date ver 2
  pageFooterEven = center @pageNbr
  pageFooterOdd = center @pageNbr
  pageNbr = 1
}

pdfPreface { // TODO: insert cover and credits)
  // pageBreak
}

`
