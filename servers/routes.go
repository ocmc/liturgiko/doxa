package servers

import (
	"github.com/gorilla/mux"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"io"
	"net/http"
	"os"
	"strings"
)

// routes initializes all the routes for the server.
func (s *server) routes() {
	// Check if we're in development mode
	devMode := os.Getenv("DOXA_DEV_MODE") == "true"
	
	// Configure static file handling
	var staticHandler http.Handler
	if devMode {
		// In dev mode, proxy requests to the Bun dev server
		doxlog.Info("Using Bun development server for static files")
		staticHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// Rewrite the URL to point to the Bun server
			path := strings.TrimPrefix(r.URL.Path, "/static")
			targetURL := "http://localhost:3000" + path
			
			// Create a new request to the Bun server
			req, err := http.NewRequest(r.Method, targetURL, r.Body)
			if err != nil {
				http.Error(w, "Error creating proxy request", http.StatusInternalServerError)
				return
			}
			
			// Copy headers from the original request
			for name, values := range r.Header {
				for _, value := range values {
					req.Header.Add(name, value)
				}
			}
			
			// Send the request to the Bun server
			client := &http.Client{}
			resp, err := client.Do(req)
			if err != nil {
				http.Error(w, "Error proxying to dev server: "+err.Error(), http.StatusBadGateway)
				return
			}
			defer resp.Body.Close()
			
			// Copy headers from the response
			for name, values := range resp.Header {
				for _, value := range values {
					w.Header().Add(name, value)
				}
			}
			
			// Set the status code
			w.WriteHeader(resp.StatusCode)
			
			// Copy the body
			io.Copy(w, resp.Body)
		})
	} else {
		// In production mode, use the embedded files
		var staticFS = http.FS(s.embeddedStaticFiles)
		staticHandler = http.FileServer(staticFS)
	}
	
	// Register the static file handler
	s.router.Handle("/static/{path:.*}", CacheControlWrapper(s.cloud, s.linux, staticHandler))
	
	// test website
	if !ltfile.DirExists(config.DoxaPaths.SiteGenTestPath) {
		err := ltfile.CreateDirs(config.DoxaPaths.SiteGenTestPath)
		if err != nil {
			doxlog.Errorf("Error creating test website directory %s: %v", err)
		}
	}
	
	// For test static files, we use the same handler as the main static files
	s.router.PathPrefix("/test/static/").Handler(http.StripPrefix("/test/", staticHandler))
	s.router.PathPrefix("/test/").Handler(http.StripPrefix("/test/", http.FileServer(http.Dir(config.DoxaPaths.SiteGenTestPath))))

	// Add route groups
	s.pageRoutes(s.router)
	s.templateRoutes(s.router)
	s.wsRoutes(s.router)
	// the modals
	s.router.HandleFunc("/modal/token", s.serveProtectTokenModal()).Methods(http.MethodGet)
	// api is already prefixed to the following routes
	// Route groups
	s.catalogRoutes(s.api)
	s.cssRoutes(s.api)
	s.dbRoutes(s.api)
	s.dvcsRoutes(s.api)
	s.jsRoutes(s.api)
	s.layoutRoutes(s.api)
	s.lmlRoutes(s.api)
	s.roSyncRoutes(s.api)
	s.rwSyncRoutes(s.api)
	s.siteRoutes(s.api)
	s.statusRoutes(s.api)
	s.subscriptionRoutes(s.api)
	s.tkRoutes(s.api)
	s.tokenRoutes(s.api)
	s.userRoutes(s.api)

	// Individual API routes
	s.api.HandleFunc("/cmp", s.handleCompare()).Methods(http.MethodGet)
	s.api.HandleFunc("/css", s.handleCssRead()).Methods(http.MethodGet)
	s.api.HandleFunc("/css", s.handleCssUpdate()).Methods(http.MethodPut)
	s.api.HandleFunc("/enum/books", s.handleEnumBooks()).Methods(http.MethodGet)
	s.api.HandleFunc("/generate", s.handleGeneration()).Methods(http.MethodPut)
	s.api.HandleFunc("/git/merge", s.handleGitMerge()).Methods(http.MethodPost)
	s.api.HandleFunc("/id", s.handleApiHome()).Methods(http.MethodGet)
	s.api.HandleFunc("/index/nav", s.handleIndexNav()).Methods(http.MethodPut)
	s.api.HandleFunc("/index/search", s.handleIndexSearch()).Methods(http.MethodPut)
	s.api.HandleFunc("/js", s.handleJsRead()).Methods(http.MethodGet)
	s.api.HandleFunc("/js", s.handleJsUpdate()).Methods(http.MethodPut)
	s.api.HandleFunc("/ldp", s.handleLdpRequest()).Methods(http.MethodGet)
	s.api.HandleFunc("/list", s.handleList()).Methods(http.MethodGet)
	s.api.HandleFunc("/log", s.handleLogRequest()).Methods(http.MethodGet)
	s.api.HandleFunc("/props/reload", s.handlePropertiesReloadRequest()).Methods(http.MethodGet)
	s.api.HandleFunc("/publish", s.handlePublication()).Methods(http.MethodPut)
	s.api.HandleFunc("/regex", s.handleRegEx()).Methods(http.MethodGet)
	s.api.HandleFunc("/search", s.handleSearch()).Methods(http.MethodGet, http.MethodOptions)
	s.api.HandleFunc("/synch", s.handleSynch()).Methods(http.MethodPut)
	s.api.HandleFunc("/unicode", s.handleUnicode()).Methods(http.MethodGet)
	s.api.Use(mux.CORSMethodMiddleware(s.api))
}
