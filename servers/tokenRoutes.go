package servers

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/liturgiko/doxa/api"
	"github.com/liturgiko/doxa/pkg/doxlog"
	picoWidget "github.com/liturgiko/doxa/templates/webApp/structs/widget"
	"net/http"
)

func (s *server) tokenRoutes(router *mux.Router) {

	s.router.HandleFunc("/protectToken", s.serveProtectTokenPage()).Methods(http.MethodGet)

	router.HandleFunc("/htmx/token/decrypt", s.decryptToken).Methods(http.MethodPost)
	router.HandleFunc("/htmx/token/encrypt", s.encryptToken).Methods(http.MethodPost)
	router.HandleFunc("/htmx/token/set", s.setToken).Methods(http.MethodPut)
	router.HandleFunc("/htmx/token/delete", s.deleteToken).Methods(http.MethodDelete)
	router.HandleFunc("/htmx/token/validate", s.validateToken).Methods(http.MethodPost)
	router.HandleFunc("/htmx/token/status", s.getTokenStatus).Methods(http.MethodGet)
}

func (s *server) serveProtectTokenPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")

		data := NewIndexHtmlData(
			r.URL.Path,
			"Protect Token",
			s.getNavbar(w, r),
		)
		data.Body.Main = picoWidget.NewMain(
			"Protect Gitlab Access Token",
			"Control DOXA's use of your Gitlab personal access token.",
		)
		grid := new(picoWidget.Grid)
		widget := s.appApi.TokenService.GetWidget(nil, "api/htmx/token/", "token", "")
		grid.AddWidget(widget)
		section := new(picoWidget.Section)
		section.AddGrid(grid)
		data.Body.Main.AddSection(section)
		data.NeedsModalConfirm = true
		s.RenderTemplate(w, "index", data, false)
	}
}

func (s *server) decryptToken(w http.ResponseWriter, r *http.Request) {
	s.logMe("decryptToken")
	// Parse the form data
	err := r.ParseForm()
	if err != nil {
		http.Error(w, "Unable to parse form", http.StatusBadRequest)
		return
	}

	// Extract the password field
	password := r.FormValue("password")

	_, err = s.appApi.TokenService.DecryptToken(nil, "", password)
	if r.Header.Get("X-No-HX-Refresh") != "true" {
		w.Header().Set("HX-Refresh", "true")
		w.WriteHeader(205)
		return
	}
	s.serveProtectTokenModal()(w, r)
}
func (s *server) encryptToken(w http.ResponseWriter, r *http.Request) {
	s.logMe("encryptToken")
	/*resp, err :=*/ s.appApi.TokenService.EncryptToken(nil, "")
	if r.Header.Get("X-No-HX-Refresh") == "true" {
		s.serveProtectTokenModal()(w, r)
		return
	}
	if r.Header.Get("X-Expect-Icon") == "true" {
		w.Header().Set("HX-Trigger", "tokenStatusUpdated")
		s.getTokenStatus(w, r)
		return
	}
	w.Header().Set("HX-Refresh", "true")
	w.WriteHeader(205)
	return
	s.getTokenStatus(w, r)
	return
}
func (s *server) setToken(w http.ResponseWriter, r *http.Request) {
	s.logMe("setToken")
	// Parse the form data
	err := r.ParseForm()
	if err != nil {
		http.Error(w, "Unable to parse form", http.StatusBadRequest)
		return
	}

	// Extract the password field
	password := r.FormValue("password")

	// Extract the password field
	token := r.FormValue("token")
	var resp api.RequestResponse
	resp, err = s.appApi.TokenService.SetToken(nil, "", password, token)
	if err != nil {
		if resp.RequestStatus == api.OK {
			// TODO: should not happen
		} else {
			doxlog.Errorf(err.Error())
			if len(resp.RequestMessages) > 0 {
				_, err = fmt.Fprint(w, `<i class="bi bi-wifi-off"d></i>`)
				if err != nil {
					doxlog.Errorf(err.Error())
					return
				}
			} else {
				_, err = fmt.Fprint(w, `<i class="bi bi-wifi-off"d></i>`)
				if err != nil {
					doxlog.Errorf(err.Error())
				}
			}
		}
		return
	}
	if resp.RequestStatus == api.OK {
		w.Header().Set("HX-Refresh", "true")
		w.WriteHeader(205)
		// Redirect to /protectToken handler
		// http.Redirect(w, r, "/protectToken", http.StatusSeeOther)
		return
	} else {
		_, err = fmt.Fprint(w, `<i class="bi bi-wifi-off"d></i>`)
		if err != nil {
			doxlog.Errorf(err.Error())
			return
		}
	}
	return
}

func (s *server) deleteToken(w http.ResponseWriter, r *http.Request) {
	s.logMe("deleteToken")
	_, err := s.appApi.TokenService.DeleteToken(nil, "", "")
	if err != nil {
		_, err = fmt.Fprint(w, "An error occurred while deleting the token. See the logs for more information.")
		doxlog.Errorf(err.Error())
		return
	}
	w.Header().Set("HX-Refresh", "true")
	w.WriteHeader(205)
}
func (s *server) validateToken(w http.ResponseWriter, r *http.Request) {
	s.logMe("validateToken")
	//		hx-post="/validate-field"
	//       hx-trigger="blur"
	//       hx-target="this"
	//       hx-swap="outerHTML"
	token := r.FormValue("token")
	validity, _, err := s.appApi.TokenService.ValidateToken(nil, "", token)
	//TODO: get token field more efficiently?
	wholeForm := s.appApi.TokenService.GetWidget(nil, "api/htmx/token/", "token", "")
	// TODO: this does not belong here--should be done by GetWidget.  MAC.
	for _, field := range wholeForm.Fields {
		f := field.(*picoWidget.InputField)
		if f.Name == "token" {
			//this is the chosen one
			f.Value = token
			switch validity {
			case api.ValidToken:
				f.ValidationFeedback = &picoWidget.ValidationFeedback{
					Class:     "",
					Message:   "token active",
					IsInvalid: false,
				}
			case api.InvalidToken:
				f.ValidationFeedback = &picoWidget.ValidationFeedback{
					Class:     "",
					Message:   "Token is not a valid gitlab token. A valid gitlab token beings with 'gl-pat'",
					IsInvalid: true,
				}
			case api.ExpiredToken:
				f.ValidationFeedback = &picoWidget.ValidationFeedback{
					Class:     "",
					Message:   "Your token has expired. Please go to gitlab and generate a new one",
					IsInvalid: true,
				}
			case api.RevokedToken:
				f.ValidationFeedback = &picoWidget.ValidationFeedback{
					Class:     "",
					Message:   "Token has been revoked",
					IsInvalid: true,
				}
			case api.UnknownError:
				f.ValidationFeedback = &picoWidget.ValidationFeedback{
					Class:     "",
					Message:   err.Error(),
					IsInvalid: true,
				}
			}
			s.RenderTemplate(w, "picoWidgetFormField", f, false)
			w.Header().Set("Content-Type", "text/html")
			break
		}
	}
	return
}

func (s *server) getTokenStatus(w http.ResponseWriter, r *http.Request) {
	s.logMe("getTokenStatus")
	status, _, _ := s.appApi.TokenService.GetTokenStatus(nil, "")
	switch status {
	case api.TokenNeeded:
		fmt.Fprint(w, `<!-- this page intentionally left blank -->`)
	case api.PasswordNeeded:
		fmt.Fprint(w, `<i class="bi bi-lock" hx-get="/modal/token" hx-target="#token-modal-content" hx-trigger="click"></i>`)
	case api.Decrypted:
		fmt.Fprint(w, `<i class="bi bi-unlock" hx-post="/api/htmx/token/encrypt" hx-trigger="click consume" hx-headers='{"X-Expect-Icon": "true"}' hx-target="closest i" hx-swap="outerHTML"></i>`)
	case api.DecryptionError:
		fmt.Fprint(w, `<i class="bi bi-lock" hx-get="/modal/token" hx-target="#token-modal-content" hx-trigger="click"></i>`)
	default:
		fmt.Fprint(w, `<!-- this page intentionally left blank -->`)
	}
}
