package servers

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/liturgiko/doxa/pkg/atempl"
	"github.com/liturgiko/doxa/pkg/layouts"
	"net/http"
	"strings"
)

func (s *server) layoutRoutes(router *mux.Router) {
	router.HandleFunc("/layout", s.handleLayoutRequest()).Methods(http.MethodGet)
	router.HandleFunc("/layout", s.handleLayoutUpdate()).Methods(http.MethodPut)
	router.HandleFunc("/layout/delete", s.handleLayoutDelete()).Methods(http.MethodPut)
	router.HandleFunc("/layouts", s.handleLayoutsRequest()).Methods(http.MethodGet)
}

// handleLayoutRequest returns the specified layout, used for generating html and pdf
func (s *server) handleLayoutRequest() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonLayoutResponse)
		var err error
		acronym := r.URL.Query().Get("acronym")
		if len(acronym) == 0 || acronym == "undefined" {
			layouts, err := layouts.Manager.GetSiteBuildLayoutsAcronyms()
			if err == nil && len(layouts) > 0 {
				acronym = layouts[0]
			} else {
				resp.Status = "missing generation layout acronym"
				respond(w, http.StatusBadRequest, resp)
				return
			}
		}
		resp.Layout, err = layouts.Manager.GetSiteBuildLayout(acronym)
		if err != nil {
			resp.Status = fmt.Sprintf("error: %v", err)
			respond(w, http.StatusNotFound, resp)
		}
		resp.Acronym = resp.Layout.Acronym
		resp.Status = "OK"
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleLayoutUpdate updates the specified layout, used for generating html and pdf
func (s *server) handleLayoutUpdate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonLayoutResponse)
		var err error
		// get the layout matrix as a json string
		strMatrix := r.URL.Query().Get("matrix")
		if len(strMatrix) == 0 {
			resp.Status = "missing generation library combo table layouts"
			respond(w, http.StatusBadRequest, resp)
			return
		}
		s.Mutex.Lock()
		defer s.Mutex.Unlock()
		// convert the json string into a go 2-d array
		var matrix [][]string
		err = json.Unmarshal([]byte(strMatrix), &matrix)
		if err != nil {
			resp.Status = fmt.Sprintf("Error unmarshalling matrix: %v", err)
			respond(w, http.StatusBadRequest, resp)
			return
		}
		// convert the 2-d array into a TableLayout object
		var layout *atempl.TableLayout
		layout, err = atempl.NewTableLayoutFromMatrix(matrix)
		if err != nil {
			resp.Status = fmt.Sprintf("Error converting matrix to layout: %v", err)
			respond(w, http.StatusBadRequest, resp)
			return
		}
		// save to configs a column layout for each language
		acronyms := strings.Split(layout.Acronym, "-")
		for i := 0; i < len(acronyms); i++ {
			cl := new(atempl.TableLayout) // cl = column layout
			cl.Acronym = acronyms[i]
			cl.LiturgicalLibs = append(cl.LiturgicalLibs, layout.LiturgicalLibs[i])
			cl.GospelLibs = append(cl.GospelLibs, layout.GospelLibs[i])
			cl.EpistleLibs = append(cl.EpistleLibs, layout.EpistleLibs[i])
			cl.PsalterLibs = append(cl.PsalterLibs, layout.PsalterLibs[i])
			cl.ProphetLibs = append(cl.ProphetLibs, layout.ProphetLibs[i])
			cl.DateFormat = layout.DateFormat
			cl.WeekDayFormat = layout.WeekDayFormat
			// save the layout object to the database configs
			err = layouts.Manager.SetSiteBuildLayout(cl)
			if err != nil {
				resp.Status = fmt.Sprintf("Error saving layout to database: %v", err)
				respond(w, http.StatusBadRequest, resp)
				return
			}
		}
		// update the acronym rows
		err = layouts.Manager.AddSiteBuildLayoutsAcronym(layout.Acronym)
		if err != nil {
			resp.Status = fmt.Sprintf("Error saving layout acronyms %s to database: %v", layout.Acronym, err)
			respond(w, http.StatusBadRequest, resp)
			return
		}
		err = s.settingsManager.ReadConfiguration()
		if err != nil {
			resp.Status = fmt.Sprintf("Error s.settingsManager.ReadConfiguration: %v", err)
			respond(w, http.StatusBadRequest, resp)
			return
		}
		resp.Status = "OK"
		resp.Acronym = layout.Acronym
		resp.Layout = layout
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleLayoutDelete deletes the specified layout, used for generating html and pdf
func (s *server) handleLayoutDelete() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonLayoutResponse)
		var err error
		acronym := r.URL.Query().Get("acronym")
		if len(acronym) == 0 {
			resp.Status = "handleLayoutDelete: missing generation layout acronym"
			respond(w, http.StatusOK, resp)
			return
		}
		theLayouts, err := layouts.Manager.GetSiteBuildLayouts()
		if err != nil {
			resp.Status = fmt.Sprintf("handleLayoutDelete: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		if len(theLayouts) == 1 {
			resp.Status = "handleLayoutDelete: can't delete the last remaining layout"
			respond(w, http.StatusOK, resp)
			return
		}
		s.Mutex.Lock()
		// delete the layout
		err = layouts.Manager.DeleteSiteBuildLayout(acronym)
		s.Mutex.Unlock()
		if err != nil {
			resp.Status = fmt.Sprintf("error deleting %s: %v", acronym, err)
			respond(w, http.StatusOK, resp)
			return
		}
		// reload the configuration
		err = s.settingsManager.ReadConfiguration()
		if err != nil {
			resp.Status = fmt.Sprintf("Error s.settingsManager.ReadConfiguration: %v.", err)
			respond(w, http.StatusOK, resp)
			return
		}
		resp.Status = "OK"
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleLayoutsRequest returns the user defined layouts for generating html and pdf
func (s *server) handleLayoutsRequest() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonLayoutsResponse)
		var err error
		resp.Layouts, err = layouts.Manager.GetSiteBuildLayoutsAcronyms()
		if err != nil {
			resp.Status = fmt.Sprintf("GetSiteBuildLayoutsAcronyms error: %v", err)
			respond(w, http.StatusOK, resp)
		}
		resp.ColumnLayouts, err = layouts.Manager.GetSiteBuildColumnMap()
		if err != nil {
			resp.Status = fmt.Sprintf("GetSiteBuildColumnMap error: %v", err)
			respond(w, http.StatusOK, resp)
		}
		resp.Status = "OK"
		respond(w, http.StatusOK, resp)
		return
	}
}
