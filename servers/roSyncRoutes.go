package servers

import (
    "github.com/gorilla/mux"
    "net/http"
    "github.com/liturgiko/doxa/api"
    "github.com/liturgiko/doxa/pkg/doxlog"
    picoWidget "github.com/liturgiko/doxa/templates/webApp/structs/widget"
)

func (s *server) roSyncRoutes(router *mux.Router) {
    router.HandleFunc("/htmx/ro/startSync", s.startRoSync).Methods(http.MethodPost)
    router.HandleFunc("/htmx/ro/reset", s.clearRoSyncResults).Methods(http.MethodPost)
    router.HandleFunc("/htmx/ro/pollUpdated", s.getRoLastUpdated).Methods(http.MethodGet)
    router.HandleFunc("/htmx/ro/pollProgress", s.getRoSyncProgress).Methods(http.MethodGet)
    router.HandleFunc("/htmx/ro/cancelSync", s.cancelRoSync).Methods(http.MethodPost)
    router.HandleFunc("/htmx/ro/syncUI", s.getRoListCombo).Methods(http.MethodGet)
    router.HandleFunc("/htmx/ro/info", s.getRoInfo).Methods(http.MethodGet)
}

func (s *server) startRoSync(w http.ResponseWriter, r *http.Request) {
    s.logMe("startRoSync")
    s.appApi.RoSync.SyncAll(nil, "")
    w.Header().Set("HX-Refresh", "true")
    w.WriteHeader(205)
    return
}

func (s *server) clearRoSyncResults(w http.ResponseWriter, r *http.Request) {
    s.appApi.RoSync.ClearInfo()
    w.Header().Set("HX-Refresh", "true")
    w.WriteHeader(205)
    return
}

func (s *server) getRoLastUpdated(w http.ResponseWriter, r *http.Request) {
    inf, _, err := s.appApi.RoSync.Info(nil, "")
    if err != nil {
        doxlog.Errorf("Error getting ro info: %v", err)
    }
    form := s.appApi.RoSync.GetForm(nil, "", "/api/htmx/ro", "ro")
    if (r.Header.Get("X-Sync-Ongoing") == "true" && !inf.Syncing) || (r.Header.Get("X-Up-To-Date") == "true" && inf.VersionAvailable != inf.VersionInstalled) {
        w.Header().Set("HX-Refresh", "true")
        w.WriteHeader(205)
    }
    for _, f := range form.Fields {
        switch f.(type) {
        case *picoWidget.InputField:
            if f.(*picoWidget.InputField) != nil && f.(*picoWidget.InputField).Name == "last-updated" {
                s.RenderTemplate(w, "picoWidgetFormField", f, false)
                break
            }
        }
    }
}

func (s *server) getRoSyncProgress(w http.ResponseWriter, r *http.Request) {
    s.logMe("getRoSyncProgress")

    // Get current state and progress
    state, refresh := s.appApi.RoSync.GetProgressBar()
    if refresh {
        w.Header().Set("HX-Refresh", "true")
        w.WriteHeader(205)
        return
    }
    if state == nil {
        return
    }

    s.RenderTemplate(w, "picoWidgetFormProgress", state, false)
    return
}

func (s *server) cancelRoSync(w http.ResponseWriter, r *http.Request) {
    s.logMe("cancelRoSync")
    s.appApi.RoSync.Cancel(nil, "")
    w.Header().Set("HX-Refresh", "true")
    w.WriteHeader(205)
    return
}

func (s *server) getRoListCombo(w http.ResponseWriter, r *http.Request) {
    s.logMe("getRoListCombo")
    // process headers
    dlId := r.Header.Get("DL-ID")
    dlSelection := r.Header.Get("DL-Selected")
    if dlId != "" {
        if dlSelection != "" {
            s.appApi.RoSync.UpdateSelection(dlId, dlSelection)
        }
    }
    if h := r.Header.Get("DL-List"); h != "" {
        switch h {
        case api.ROUICatalogListId:
            s.RenderTemplate(w, "Element", s.appApi.RoSync.GetCatalogList("ro"), true)
            return
        case api.ROUIEntryListID:
            s.RenderTemplate(w, "Element", s.appApi.RoSync.GetEntryList("ro"), true)
            return
        default:
            w.WriteHeader(428)
        }
    }
    roForm := s.appApi.RoSync.GetForm(nil, "api/htmx/ro", "ro", "")
    s.RenderTemplate(w, "picoWidgetForm", roForm, true)
    return
}

func (s *server) getRoInfo(w http.ResponseWriter, r *http.Request) {
    s.logMe("getRoInfo")
    // process headers
    dlId := r.Header.Get("DL-ID")
    dlInfo := r.Header.Get("DL-Info")
    switch dlId {
    case api.ROUICatalogListId:
        //s.appApi.RoSync.GetCatalogInfoForm(dlInfo)
    case api.ROUIEntryListID:
        s.RenderTemplate(w, "Element", s.appApi.RoSync.GetEntryInfoForm(dlInfo), true)
    default:
        w.WriteHeader(428)
    }
    return
}
