package servers

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/inOut"
	"github.com/liturgiko/doxa/pkg/utils/ltRegEx"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"github.com/liturgiko/doxa/pkg/utils/stack"
	"github.com/liturgiko/doxa/pkg/utils/stamp"
	"github.com/liturgiko/doxa/templx"
	"net/http"
	"path"
	"sort"
	"strconv"
	"strings"
)

func (s *server) dbRoutes(router *mux.Router) {
	router.HandleFunc("/db/configs", s.handleConfigs()).Methods(http.MethodGet, http.MethodOptions)
	router.HandleFunc("/db/create", s.handleRecordCreate()).Methods(http.MethodPost, http.MethodOptions)
	router.HandleFunc("/db/delete", s.handleDelete()).Methods(http.MethodPut)
	router.HandleFunc("/db/export", s.handleExport()).Methods(http.MethodPost)
	router.HandleFunc("/db/import", s.handleImportRequest()).Methods(http.MethodPut)
	router.HandleFunc("/db/import/list", s.handleImportListRequest()).Methods(http.MethodGet)
	router.HandleFunc("/db/library/matching/language", s.handleGetLibrariesWithMatchingLanguageCode()).Methods(http.MethodGet)
	router.HandleFunc("/db/backup", s.handleBackup()).Methods(http.MethodPost)
	router.HandleFunc("/db/mkdir", s.handleDirCreate()).Methods(http.MethodPost)
	router.HandleFunc("/db/read", s.handleRecordRead()).Methods(http.MethodGet)
	router.HandleFunc("/db/redirects/to", s.handleDbIdTrace()).Methods(http.MethodGet)
	router.HandleFunc("/db/redirects/from", s.handleDbIdTrace()).Methods(http.MethodGet)
	router.HandleFunc("/db/trace", s.handleDbIdTrace()).Methods(http.MethodGet)
	router.HandleFunc("/db/update", s.handleRecordUpdate()).Methods(http.MethodPut)
	router.HandleFunc("/db/list", s.handleList()).Methods(http.MethodGet)
	router.HandleFunc("/db/compare", s.handleCompare()).Methods(http.MethodGet)
	router.HandleFunc("/db/search", s.handleSearch()).Methods(http.MethodGet)
	router.HandleFunc("/db/redirects/analysis", s.handleLibraryRedirectsAnalysis()).Methods(http.MethodGet)
	router.HandleFunc("/db/redirects/to", s.handleDbRedirectsTo()).Methods(http.MethodGet)
}

// handleList returns the directories or records for the specified database path
func (s *server) handleList() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		resp := new(JsonKeyPathResponse)
		path := r.URL.Query().Get("path")
		kp := kvs.NewKeyPath()
		var err error
		if len(path) > 0 {
			err = kp.ParsePath(path)
		}
		if err != nil {
			resp.Status = "bad path"
			respond(w, http.StatusBadRequest, resp)
			return
		}
		keys, _, _, err := s.mapper.Db.Keys(kp)
		if err == nil {
			if len(path) > 0 {
				resp.Path = kp.Dirs
			} else {
				resp.Path = new(stack.StringStack)
				resp.Path.Push("")
			}
			var recs []*IDKV
			for _, k := range keys {
				// filter out the -nnp directories
				if strings.HasSuffix(k.Dirs.Last(), "-nnp") {
					continue
				}
				r := new(IDKV)
				r.ID = k.Path()
				if k.IsRecord() {
					r.Last = k.Key()
					rec, err := s.mapper.Db.Get(k)
					if err != nil {
						w.WriteHeader(http.StatusBadRequest)
						jsonResp, err := json.Marshal(resp)
						if err != nil {
							doxlog.Infof("Error happened in JSON marshal. Err: %s", err)
							return
						}
						w.Write(jsonResp)
						return
					}
					r.Value = rec.Value
				} else {
					r.Last = fmt.Sprintf("%s/", k.Dirs.Last())
				}
				recs = append(recs, r)
			}
			resp.Values = recs
			resp.Status = "OK"
			respond(w, http.StatusOK, resp)
			return
		} else {
			resp.Status = "not found"
			respond(w, http.StatusNotFound, resp)
			return
		}
	}
}

// handleCompare returns the ltx records matching the specified topic and key
func (s *server) handleCompare() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		resp := new(JsonKeyValueResponse)
		path := r.URL.Query().Get("path")
		kp := kvs.NewKeyPath()
		var err error
		if len(path) > 0 {
			err = kp.ParsePath(path)
		}
		if err != nil || !kp.IsRecord() {
			resp.Status = "bad path"
			respond(w, http.StatusBadRequest, resp)
			return
		}
		var includeEmptyValue bool
		if r.URL.Query().Has("empty") {
			includeEmptyValue = r.URL.Query().Get("empty") == "true"
		}
		var showRedirects bool
		if r.URL.Query().Has("showRedirects") {
			showRedirects = r.URL.Query().Get("showRedirects") == "true"
		}
		matcher := kvs.NewMatcher()
		matcher.KP = kp.Copy()
		matcher = matcher.TopicKeyMatcher()
		matcher.IncludeEmpty = includeEmptyValue
		recs, _, err := s.mapper.Db.GetMatching(*matcher)
		if err != nil {
			doxlog.Infof("error comparing %s: %v", path, err)
			resp.Status = "error comparing topic-keys"
			return
		}
		var respRecs []*KV

		for _, rec := range recs {
			r := new(KV)
			r.ID = rec.KP.Path()
			r.Value = rec.Value
			if rec.IsRedirect() && !showRedirects {
				continue
			}
			respRecs = append(respRecs, r)
		}
		resp.Values = respRecs
		resp.Status = "OK"
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleSearch returns matching records for the specified database path
func (s *server) handleSearch() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		var err error
		var searchForIds, caseInsensitive, diacriticInsensitive, greekNfc, wholeWord bool
		resp := new(JsonRecordSearchResponse)
		// get path
		path := r.URL.Query().Get("path")
		kp := kvs.NewKeyPath()
		if len(path) > 0 {
			err = kp.ParsePath(path)
		}
		if err != nil {
			resp.Status = "bad path"
			respond(w, http.StatusBadRequest, resp)
			return
		}
		resp.Path = kp.Dirs.Copy()
		// initialize matcher
		matcher := kvs.NewMatcher()
		matcher.KP = kp.Copy()
		matcher.Recursive = true
		// get the pattern for the search
		pattern := r.URL.Query().Get("pattern")
		// check to see if se are searching for IDs or values
		searchIds := r.URL.Query().Get("searchIds")
		matcher.IdSearch = searchIds == "true"

		if matcher.IdSearch {
			matcher.ValuePattern = path + pattern
			matcher.NNP = false
			matcher.NFD = false
		} else {
			caseInsensitive, err = strconv.ParseBool(r.URL.Query().Get("caseInsensitive"))
			if err != nil {
				resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
				resp.Message = fmt.Sprintf("%s", err)
				respond(w, http.StatusOK, resp)
				return
			}
			diacriticInsensitive, err = strconv.ParseBool(r.URL.Query().Get("diacriticInsensitive"))
			if err != nil {
				resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
				resp.Message = fmt.Sprintf("%s", err)
				respond(w, http.StatusOK, resp)
				return
			}
			greekNfc, err = strconv.ParseBool(r.URL.Query().Get("greek"))
			if err != nil {
				resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
				resp.Message = fmt.Sprintf("%s", err)
				respond(w, http.StatusOK, resp)
				return
			}
			wholeWord, err = strconv.ParseBool(r.URL.Query().Get("wholeWord"))
			if err != nil {
				resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
				resp.Message = fmt.Sprintf("%s", err)
				respond(w, http.StatusOK, resp)
				return
			}
			resp.CaseInsensitive = caseInsensitive
			resp.DiacriticInsensitive = diacriticInsensitive
			resp.WholeWord = wholeWord

			// only allow greek to be true if library is for gr
			// We don't automatically set it true because if the
			// library is gr and
			// greek is false:
			//     we handle diacritics like any other unicode block
			//     text has to be converted to NFD for search
			// greek is true:
			//     we use letter expansion instead.
			//     we use nfc for search which is faster than using nfd
			//
			if kp.Dirs.Size() > 1 && strings.HasPrefix(kp.Dirs.Get(1), "gr_") {
				// no action
			} else {
				// if we are not searching Greek,
				// greekNfc must be set to false
				// no matter what the user said.
				greekNfc = false
			}
			matcher.WholeWord = wholeWord
			matcher.CaseInsensitive = caseInsensitive
			matcher.DiacriticInsensitive = diacriticInsensitive
			matcher.ValuePattern, matcher.NFD, err = ltRegEx.ExpandedRegEx(pattern, caseInsensitive, diacriticInsensitive, greekNfc)
			if err != nil {
				resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
				resp.Message = fmt.Sprintf("%v", err)
				respond(w, http.StatusOK, resp)
				return
			}
		}

		resp.NFD = matcher.NFD
		err = matcher.UseAsRegEx()
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("%s", err)
			respond(w, http.StatusOK, resp)
			return
		}
		var dbRecs []*kvs.DbR

		timeStamp := stamp.NewStamp("find")
		timeStamp.Start()

		if searchForIds {
			dbRecs, err = s.mapper.Db.GetMatchingIDs(*matcher)
			resp.Count = len(dbRecs)
		} else {
			dbRecs, resp.Count, err = s.mapper.Db.GetMatching(*matcher)
		}
		resp.Time = timeStamp.FinishNanosecondsNoMessage()
		if err == nil {
			var recs []*IDKV
			for _, rec := range dbRecs {
				r := new(IDKV)
				r.ID = rec.KP.Path()
				r.Last = rec.KP.Dirs.Last()
				r.Value = rec.Value
				recs = append(recs, r)
			}
			resp.Status = "OK"
			resp.Message = fmt.Sprintf("%d matches", len(recs))
			resp.PatternOriginal = pattern
			resp.PatternExpanded = matcher.ValueRegEx.String()
			resp.Values = recs

			respond(w, http.StatusOK, resp)
			return
		} else {
			resp.Status = "no matches"
			respond(w, http.StatusNotFound, resp)
			return
		}
	}
}

// handleDbRedirectsTo returns the records that redirect to the given directory or record
func (s *server) handleDbRedirectsTo() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonDbRedirectsToResponse)
		id := r.URL.Query().Get("id")
		if len(id) == 0 {
			resp.Status = "missing id"
			respond(w, http.StatusBadRequest, resp)
			return
		}
		resp.ID = id
		kp := kvs.NewKeyPath()
		err := kp.ParsePath(id)
		if err != nil {
			resp.Status = "bad request"
			resp.Message = fmt.Sprintf("%s not a valid database directory or record ID", id)
			respond(w, http.StatusOK, resp)
			return
		}
		targetDbr, err := s.mapper.Db.Get(kp)
		if err != nil {
			w.WriteHeader(http.StatusOK)
			resp.Status = "not found"
			resp.Message = fmt.Sprintf("%s not found in database", id)
			respond(w, http.StatusOK, resp)
			return
		}
		// if we get here, we found the requested dir or record
		var redirects []*kvs.DbR
		if targetDbr.KP.IsRecord() {
			redirects, err = s.mapper.Db.GetRedirectsToRecord(targetDbr.KP)
		} else {
			redirects, err = s.mapper.Db.GetRedirectsToDir(targetDbr.KP)
		}
		if err != nil {
			w.WriteHeader(http.StatusOK)
			resp.Status = "not found"
			resp.Message = fmt.Sprintf("error finding redirects to %s: %v", id, err)
			respond(w, http.StatusOK, resp)
			return
		}
		if len(redirects) == 0 {
			w.WriteHeader(http.StatusOK)
			resp.Status = "not found"
			resp.Message = fmt.Sprintf("there are no redirects to %s", id)
			respond(w, http.StatusOK, resp)
			return
		}
		// if we get this far, we have redirects to process
		for _, dbr := range redirects {
			redirect := new(Redirect)
			redirect.From = dbr.KP.Path()
			redirect.To = dbr.Redirect.Path()
			resp.Redirects = append(resp.Redirects, redirect)
		}
		w.WriteHeader(http.StatusOK)
		resp.Status = "OK"
		resp.Message = fmt.Sprintf("there are %d redirects to %s", len(redirects), id)
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleLibraryRedirectsAnalysis() provides an analysis of a specified library's use of redirects
func (s *server) handleLibraryRedirectsAnalysis() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonDbIdTraceResponse)
		id := r.URL.Query().Get("id")
		if len(id) == 0 {
			resp.Status = "missing id"
			respond(w, http.StatusBadRequest, resp)
			return
		}
		resp.ID = id
		kp := kvs.NewKeyPath()
		err := kp.ParsePath(id)
		if err != nil {
			resp.Status = "malformed id"
			respond(w, http.StatusBadRequest, resp)
			return
		}
		rec, redirects, err := s.mapper.Db.GetResolved(kp)
		if err == nil {
			w.WriteHeader(http.StatusOK)
			resp.Status = "OK"
			resp.Message = "Traced"
			resp.Redirects = redirects
			resp.Value = rec.Value
			jsonResp, err := json.Marshal(resp)
			if err != nil {
				doxlog.Infof("JSON marshal Error: %s", err)
				return
			}
			w.Write(jsonResp)
			return
		} else {
			w.WriteHeader(http.StatusOK)
			resp.Status = "not found"
			jsonResp, err := json.Marshal(resp)
			if err != nil {
				doxlog.Infof("Error happened in JSON marshal. Err: %s", err)
				return
			}
			w.Write(jsonResp)
			return
		}
	}
}

// handleConfigs returns all the property records for the specified config
func (s *server) handleConfigs() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonKeyPathResponse)
		id := r.URL.Query().Get("config")
		if len(id) == 0 {
			id = s.settingsManager.ConfigPath
		}
		matcher := kvs.NewMatcher()
		err := matcher.KP.ParsePath(id)
		if err != nil {
			resp.Status = "malformed id"
			respond(w, http.StatusBadRequest, resp)
			return
		}
		matcher.Recursive = true
		matcher.IncludeEmpty = true
		dbRecs, _, err := s.mapper.Db.GetMatching(*matcher)
		if err == nil {
			w.WriteHeader(http.StatusOK)
			var recs []*IDKV
			for _, rec := range dbRecs {
				r := new(IDKV)
				r.ID = rec.KP.Path()
				r.Last = rec.KP.Dirs.Last() // don't really need this for properties, but so be it.
				r.Value = rec.Value
				recs = append(recs, r)
			}
			resp.Values = recs
			jsonResp, err := json.Marshal(resp)
			if err != nil {
				doxlog.Infof("JSON marshal Error: %s", err)
				return
			}
			w.Write(jsonResp)
			return
		} else {
			w.WriteHeader(http.StatusOK)
			resp.Status = "not found"
			jsonResp, err := json.Marshal(resp)
			if err != nil {
				doxlog.Infof("Error happened in JSON marshal. Err: %s", err)
				return
			}
			w.Write(jsonResp)
			return
		}
	}
}

// handleRecordCreate creates a record at the specified database path
func (s *server) handleRecordCreate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		uid, _ := s.getSessionUsername(w, r)
		resp := new(JsonKeyPathResponse)
		id := r.URL.Query().Get("id")
		if len(id) == 0 {
			resp.Status = "missing path"
			respond(w, http.StatusOK, resp)
			return
		}
		kp := kvs.NewKeyPath()
		err := kp.ParsePath(id)
		if err != nil {
			resp.Status = "malformed id"
			respond(w, http.StatusOK, resp)
			return
		}
		resp.Path = kp.Dirs.Copy()
		// return error if record already exists
		if s.mapper.Db.Exists(kp) {
			resp.Status = "record already exists"
			respond(w, http.StatusOK, resp)
			return
		}
		dbr := kvs.NewDbR()
		dbr.KP = kp
		if strings.HasSuffix(id, "/titles/map") {
			// all keys must be lowercase
			key := strings.ToLower(dbr.KP.KeyParts.First())
			err = dbr.KP.KeyParts.Set(0, key)
			if err != nil {
				resp.Status = fmt.Sprintf("error setting key parts for %s", key)
				doxlog.Errorf("%s:%v", resp.Status, err)
				respond(w, http.StatusOK, resp)
				return
			}
		}
		dbr.Value = r.URL.Query().Get("value")
		// prevent a recursive redirect
		if strings.HasPrefix(dbr.Value, "@") {
			if dbr.Value[1:] == dbr.KP.Path() {
				resp.Status = "redirecting back to the same record is not allowed"
				respond(w, http.StatusOK, resp)
				return
			}
		}
		err = s.mapper.Db.Put(dbr)
		if err != nil {
			doxlog.Infof("Error creating record: %s", err)
			resp.Status = fmt.Sprintf("error creating record: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		if s.cloud {
			doxlog.Infof("user %s created record %s = %s", uid, dbr.KP.Path(), dbr.Value)
		}
		if dbr.KP.Dirs.First() == "ltx" {
			err = s.keyRing.Add(dbr.KP)
		}
		resp.Status = "created"
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleDelete deletes the directory or record at the specified database path
func (s *server) handleDelete() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		uid, _ := s.getSessionUsername(w, r)
		resp := new(JsonKeyPathResponse)
		id := r.URL.Query().Get("id")
		if len(id) == 0 {
			resp.Status = "missing path"
			respond(w, http.StatusBadRequest, resp)
			return
		}
		kp := kvs.NewKeyPath()
		err := kp.ParsePath(id)
		if err != nil {
			resp.Status = "malformed id"
			respond(w, http.StatusBadRequest, resp)
			return
		}
		resp.Path = kp.Dirs.Copy()
		// return error if record does not exist
		if !s.mapper.Db.Exists(kp) {
			resp.Status = "record does not exist"
			respond(w, http.StatusBadRequest, resp)
			return
		}
		err = s.mapper.Db.Delete(*kp)
		if err != nil {
			doxlog.Infof("Error deleting record: %s", err)
			resp.Status = "error deleting record"
			respond(w, http.StatusBadRequest, resp)
			return
		}
		if s.cloud {
			doxlog.Infof("user %s deleted %s", uid, id)
		}

		resp.Status = "deleted"
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleExport recursively exports all records at the specified database path
func (s *server) handleExport() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		resp := new(JsonKeyPathResponse)
		id := r.URL.Query().Get("id")
		if len(id) == 0 {
			resp.Status = "missing path"
			respond(w, http.StatusOK, resp)
			return
		}
		kp := kvs.NewKeyPath()
		err := kp.ParsePath(id)
		if err != nil {
			resp.Status = "malformed id"
			respond(w, http.StatusOK, resp)
			return
		}
		resp.Path = kp.Dirs.Copy()
		// return error if directory already exists
		if !s.mapper.Db.Exists(kp) {
			resp.Status = "path does not exist"
			respond(w, http.StatusOK, resp)
			return
		}

		// get export type (records as lines in tsv files vs each record as json)
		var exportType kvs.ExportType
		var pathOut string
		var strType string
		strType = r.URL.Query().Get("type")
		switch strType {
		case "1":
			exportType = kvs.ExportAsJson
			pathOut = path.Join(config.DoxaPaths.ExportPath, "json")
		default:
			exportType = kvs.ExportAsLine
			pathOut = path.Join(config.DoxaPaths.ExportPath, kp.DirPath()+"."+kvs.FileExtension)
		}

		err = s.mapper.Db.Export(kp, pathOut, exportType)
		if err != nil {
			resp.Status = fmt.Sprintf("%v", err)
			respond(w, http.StatusOK, resp)
			return
		} else {
			resp.Status = "OK"
			resp.Message = fmt.Sprintf("Records exported to %s\n", pathOut)
		}
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleImportRequest imports the indicated file contents into the database
func (s *server) handleImportRequest() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		resp := new(JsonFileImportResponse)
		filePath := strings.TrimSpace(r.URL.Query().Get("file"))
		if len(filePath) == 0 {
			resp.Status = "missing file path"
			resp.Message = "missing file path"
			respond(w, http.StatusOK, resp)
			return
		}
		removeQuotesStr := r.URL.Query().Get("removeQuotes")
		removeQuotes := removeQuotesStr == "true"
		allOrNoneStr := r.URL.Query().Get("allOrNone")
		allOrNone := allOrNoneStr == "true"
		skipObsoleteConfigs := true
		var errors []error
		resp.Count, errors = s.mapper.Db.Import("", filePath, inOut.LineParser, removeQuotes, allOrNone, skipObsoleteConfigs)
		var newErrors = inOut.ConsolidateErrors(errors)
		maxErrors := 25
		if len(newErrors) > 0 {
			for i, e := range newErrors {
				if i <= maxErrors {
					resp.Errors = append(resp.Errors, fmt.Sprintf("%s", e))
				}
			}
			resp.Status = "ERRORS"
			var errorCount string
			if len(errors) == 1 {
				errorCount = "1 line"
			} else {
				errorCount = fmt.Sprintf("%d lines", len(errors))
			}
			var maxMsg string
			if len(newErrors) > maxErrors {
				maxMsg = fmt.Sprintf(" Only the first %d errors will be shown.", maxErrors)
			}
			if allOrNone {
				resp.Message = fmt.Sprintf("No lines in the file were imported into the database, due to errors.")
			} else {
				resp.Message = fmt.Sprintf("The file was imported into the database, except for %s due to errors.%s", errorCount, maxMsg)
			}
		} else {
			resp.Status = "OK"
			resp.Message = "The file was imported into the database. There were no errors."
		}
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleImportListRequest returns files (with path) found in the imports directory
func (s *server) handleImportListRequest() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		resp := new(JsonFileListResponse)
		resp.Dir = config.DoxaPaths.ImportPath
		var err error
		// get the list of files
		if resp.Files, err = ltfile.FileMatcher(config.DoxaPaths.ImportPath, kvs.FileExtension, nil); err != nil {
			resp.Status = fmt.Sprintf("%v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		if len(resp.Files) == 0 {
			resp.Message = fmt.Sprintf("No files found in %s", config.DoxaPaths.ImportPath)
		}
		sort.Strings(resp.Files)
		resp.Status = "OK"
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleGetLibrariesWithMatchingLanguageCode returns the libraries that have the same language code as the one provided as query string
func (s *server) handleGetLibrariesWithMatchingLanguageCode() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		resp := new(JsonLibraryListResponse)
		resp.Library = r.URL.Query().Get("library")
		if len(resp.Library) == 0 {
			resp.Status = "BadRequest"
			resp.Message = "library to match not provided"
			respond(w, http.StatusOK, resp)
			return
		}
		// get all the libraries in ltx.  They are the topmost directories.
		kp := kvs.NewKeyPath()
		kp.Dirs.Push("ltx")
		var libs []string
		var err error
		if libs, err = s.mapper.Db.DirNames(*kp); err != nil {
			resp.Status = "BadRequest"
			resp.Message = fmt.Sprintf("%v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		// extract the requested language code from the library sent as a query string
		var requestedLangCode string
		if requestedLangCode, err = ltstring.LangFromLibrary(resp.Library); err != nil {
			resp.Status = "BadRequest"
			resp.Message = fmt.Sprintf("%v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		requestedLangCode = requestedLangCode + "_"

		// filter the libs, so we only return those whose language code matches
		for _, lib := range libs {
			if strings.HasPrefix(lib, requestedLangCode) {
				resp.Matching = append(resp.Matching, lib)
			}
		}
		resp.Status = "OK"
		resp.Message = fmt.Sprintf("%d libraries found", len(resp.Matching))
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleRecordRead returns the record at the specified database path
func (s *server) handleRecordRead() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonRecordResponse)
		id := r.URL.Query().Get("id")
		if len(id) == 0 {
			resp.Status = "missing id"
			respond(w, http.StatusBadRequest, resp)
			return
		}
		resp.ID = id
		kp := kvs.NewKeyPath()
		err := kp.ParsePath(id)
		if err != nil {
			resp.Status = "malformed id"
			respond(w, http.StatusBadRequest, resp)
			return
		}
		rec, err := s.mapper.Db.Get(kp)
		if err == nil {
			w.WriteHeader(http.StatusOK)
			resp.Value = rec.Value
			jsonResp, err := json.Marshal(resp)
			if err != nil {
				doxlog.Infof("JSON marshal Error: %s", err)
				return
			}
			w.Write(jsonResp)
			return
		} else {
			w.WriteHeader(http.StatusOK)
			resp.Status = "not found"
			jsonResp, err := json.Marshal(resp)
			if err != nil {
				doxlog.Infof("Error happened in JSON marshal. Err: %s", err)
				return
			}
			w.Write(jsonResp)
			return
		}
	}
}

// handleRecordUpdate updates the record at the specified database path
func (s *server) handleRecordUpdate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		uid, _ := s.getSessionUsername(w, r)
		resp := new(JsonKeyPathResponse)
		id := r.URL.Query().Get("id")
		if len(id) == 0 {
			resp.Status = "missing path"
			respond(w, http.StatusOK, resp)
			return
		}
		kp := kvs.NewKeyPath()
		err := kp.ParsePath(id)
		if err != nil {
			resp.Status = "malformed id"
			respond(w, http.StatusOK, resp)
			return
		}
		resp.Path = kp.Dirs.Copy()
		dbr := kvs.NewDbR()
		dbr.KP = kp
		if strings.HasSuffix(id, "/titles/map") {
			// all keys must be lowercase
			key := strings.ToLower(dbr.KP.KeyParts.First())
			err = dbr.KP.KeyParts.Set(0, key)
			if err != nil {
				resp.Status = fmt.Sprintf("error setting key parts for %s", key)
				respond(w, http.StatusOK, resp)
				return
			}
		}
		dbr.Value = r.URL.Query().Get("value")
		// prevent a recursive redirect
		if strings.HasPrefix(dbr.Value, "@") {
			if dbr.Value[1:] == dbr.KP.Path() {
				resp.Status = "Recursive redirects are not allowed. You are redirecting back to the same record."
				respond(w, http.StatusOK, resp)
				return
			}
		}
		err = s.mapper.Db.Put(dbr)
		if err != nil {
			doxlog.Infof("Error updating record: %s", err)
			resp.Status = fmt.Sprintf("error creating record: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		if s.cloud {
			doxlog.Infof("user %s changed record %s to %s", uid, dbr.KP.Path(), dbr.Value)
		}

		resp.Status = "updated"
		// if the record that was updated is a configuration setting
		// update the settings manager settings.
		if dbr.KP.Dirs.First() == "configs" {
			err = s.settingsManager.UpdateConfiguration(dbr.KP.DirPath())
		}
		go func() {
			s.Mutex.Lock()
			defer s.Mutex.Unlock()
			err := s.settingsManager.ReadConfiguration()
			if err != nil {
				doxlog.Infof("error reading configuration: %v", err)
			}
			err = s.siteBuilder.Config.Load()
			if err != nil {
				doxlog.Infof("error reloading properties: %v", err)
			}
		}()
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleDirCreate creates a directory at the specified database path
func (s *server) handleDirCreate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		uid, _ := s.getSessionUsername(w, r)
		resp := new(JsonKeyPathResponse)
		id := r.URL.Query().Get("id")
		if len(id) == 0 {
			resp.Status = "missing path"
			respond(w, http.StatusOK, resp)
			return
		}
		kp := kvs.NewKeyPath()
		err := kp.ParsePath(id)
		if err != nil {
			resp.Status = "malformed id"
			respond(w, http.StatusOK, resp)
			return
		}
		if kp.IsRecord() {
			resp.Status = "not a directory path"
			respond(w, http.StatusOK, resp)
			return
		}
		resp.Path = kp.Dirs.Copy()
		// return error if directory already exists
		if s.mapper.Db.Exists(kp) {
			resp.Status = "directory already exists"
			respond(w, http.StatusOK, resp)
			return
		}
		err = s.mapper.Db.MakeDir(kp)
		if err != nil {
			doxlog.Infof("Error creating directory: %s", err)
			resp.Status = "error creating directory"
			respond(w, http.StatusOK, resp)
			return
		}
		if s.cloud {
			doxlog.Infof("user %s created directory %s", uid, kp.Path())
		}
		resp.Status = "created"
		respond(w, http.StatusOK, resp)
		go func() {
			err = s.repoCreate(kp)
			if err != nil {
				doxlog.Errorf("error serializing tsp file for %s: %v", kp.Path(), err)
			}
		}()

		return
	}
}

// handleDbIdTrace returns the redirects encountered while resolving the specified ID
func (s *server) handleDbIdTrace() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonDbIdTraceResponse)
		id := r.URL.Query().Get("id")
		if len(id) == 0 {
			resp.Status = "missing id"
			respond(w, http.StatusBadRequest, resp)
			return
		}
		resp.ID = id
		kp := kvs.NewKeyPath()
		err := kp.ParsePath(id)
		if err != nil {
			resp.Status = "malformed id"
			respond(w, http.StatusBadRequest, resp)
			return
		}
		rec, redirects, err := s.mapper.Db.GetResolved(kp)
		if err == nil {
			w.WriteHeader(http.StatusOK)
			resp.Status = "OK"
			resp.Message = "Traced"
			resp.Redirects = redirects
			resp.Value = rec.Value
			jsonResp, err := json.Marshal(resp)
			if err != nil {
				doxlog.Infof("JSON marshal Error: %s", err)
				return
			}
			w.Write(jsonResp)
			return
		} else {
			w.WriteHeader(http.StatusOK)
			resp.Status = "not found"
			jsonResp, err := json.Marshal(resp)
			if err != nil {
				doxlog.Infof("Error happened in JSON marshal. Err: %s", err)
				return
			}
			w.Write(jsonResp)
			return
		}
	}
}

// handleBackup makes a copy of the database with the current date and time as its filename and places it in the doxa/backups directory
func (s *server) handleBackup() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		var msg, status string
		var messages []string
		dbBackupPath, err := s.kvsWrapper.Backup(path.Join(config.DoxaPaths.BackupPath, "db"), 1)
		if err != nil {
			msg = fmt.Sprintf("error backing up database: %v", err)
			status = "danger"
			messages = append(messages, msg)
			doxlog.Errorf(msg)
			s.rwSyncMsgChan <- msg
		} else {
			msg = fmt.Sprintf("Database backed up to %s", dbBackupPath)
			messages = append(messages, msg)
			s.rwSyncMsgChan <- msg
		}
		//clear it
		backup := s.kvsWrapper.ProjectPushData
		s.kvsWrapper.SetExportFilter(nil)
		dbrExportPath := path.Join(config.DoxaPaths.BackupPath, config.ResourcesDir)
		err = s.kvsWrapper.ExportAll(dbrExportPath)
		if err != nil {
			status = "danger"
			msg = fmt.Sprintf("Error exporting DB records: %v", err)
			messages = append(messages, msg)
			doxlog.Errorf(msg)
			s.rwSyncMsgChan <- msg
		} else {
			msg = fmt.Sprintf("Records exported to %s", dbrExportPath)
			messages = append(messages, msg)
			s.rwSyncMsgChan <- msg
		}
		s.kvsWrapper.SetExportFilter(backup)

		templateBackupPath := path.Join(config.DoxaPaths.BackupPath, config.TemplatesDir)
		err = ltfile.ConcurrentCopyDir(config.DoxaPaths.PrimaryTemplatesPath, templateBackupPath, 128)
		if err != nil {
			status = "danger"
			msg = fmt.Sprintf("Error copying templates: %v", err)
			messages = append(messages, msg)
			doxlog.Errorf(msg)
			s.rwSyncMsgChan <- msg
		} else {
			msg = fmt.Sprintf("Templates copied to %s", templateBackupPath)
			messages = append(messages, msg)
			s.rwSyncMsgChan <- msg
			// it is possible that something else set it to danger
			if status != "danger" {
				status = "success"
			}
		}

		parm := new(templx.AlertParm)
		parm.Status = status
		parm.Messages = messages
		component := templx.Alert(parm)
		err = component.Render(r.Context(), w)
		if err != nil {
			doxlog.Errorf(err.Error())
		}
	}
}
