package servers

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/enums/properties"
	"github.com/liturgiko/doxa/pkg/enums/statuses"
	"github.com/liturgiko/doxa/pkg/parser"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

func (s *server) lmlRoutes(router *mux.Router) {
	router.HandleFunc("/lml", s.handleTemplateRead()).Methods(http.MethodGet)
	router.HandleFunc("/lml", s.handleTemplateUpdate()).Methods(http.MethodPut)
	router.HandleFunc("/lml/copy", s.handleTemplateCopy()).Methods(http.MethodPut)
	router.HandleFunc("/lml/delete", s.handleTemplateDelete()).Methods(http.MethodPut)
	router.HandleFunc("/lml/dir/browse", s.handleTemplateDirBrowse()).Methods(http.MethodGet)
	router.HandleFunc("/lml/dir/delete", s.handleTemplateDirDelete()).Methods(http.MethodPut)
	router.HandleFunc("/lml/dir/new", s.handleTemplateDirAdd()).Methods(http.MethodPut)
	router.HandleFunc("/lml/dir/rename", s.handleTemplateDirRename()).Methods(http.MethodPut)
	router.HandleFunc("/lml/dir/top", s.handleTemplateDirListTop()).Methods(http.MethodGet)
	router.HandleFunc("/lml/fallback", s.handleFallbackSettings()).Methods(http.MethodGet)
	router.HandleFunc("/lml/fallback", s.handleFallbackSettingsUpdate()).Methods(http.MethodPut)
	router.HandleFunc("/lml/match", s.handleLmlMatch()).Methods(http.MethodGet, http.MethodOptions)
	router.HandleFunc("/lml/parse", s.handleTemplateParse()).Methods(http.MethodGet)
	router.HandleFunc("/lml/patterns", s.handleLmlPatterns()).Methods(http.MethodGet, http.MethodPut, http.MethodOptions)
	router.HandleFunc("/lml/search", s.handleTemplateSearch()).Methods(http.MethodGet)
	router.HandleFunc("/lml/statuses", s.handleLmlStatusTypes()).Methods(http.MethodGet)
}

// handleTemplateSearch returns matching content from templates
func (s *server) handleTemplateSearch() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonTemplateSearchResponse)
		pattern := r.URL.Query().Get("pattern")
		if len(pattern) < 1 {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("missing search pattern")
			respond(w, http.StatusOK, resp)
			return
		}
		pattern = strings.TrimSpace(pattern)
		modifiedPattern := pattern

		wholeWord := r.URL.Query().Get("whole")
		var useWholeWord = wholeWord == "true"

		primary := r.URL.Query().Get("primary")
		var usePrimary = primary == "true"

		fallback := r.URL.Query().Get("fallback")
		var useFallback = fallback == "true"

		if useWholeWord {
			// set the regEx whole word option. It should not already be set, but defensively check anyway
			if !strings.HasPrefix(modifiedPattern, "\\b") {
				modifiedPattern = fmt.Sprintf("\\b%s", modifiedPattern)
			}
			if !strings.HasSuffix(modifiedPattern, "\\b") {
				modifiedPattern = fmt.Sprintf("%s\\b", modifiedPattern)
			}
		}
		exact := r.URL.Query().Get("exact")
		var useExact = exact == "true"
		if !useExact {
			// set the regEx case-insensitive option. It should not already be set, but defensively check anyway
			if !strings.HasPrefix(modifiedPattern, "(?i)") {
				modifiedPattern = fmt.Sprintf("(?i)%s", modifiedPattern)
			}
		}
		var err error
		resultWidth := 0 // 80 // use this to control the size of the left context + match + right context. zero returns whole line
		var dirsToSearch []string
		var sources []string
		if s.siteBuilder.Config.TemplatesFallbackEnabled {
			if usePrimary && useFallback {
				dirsToSearch = []string{s.siteBuilder.TemplateMapper.GetPrimary(), s.siteBuilder.TemplateMapper.GetFallback()}
				sources = []string{s.siteBuilder.Config.SiteName, s.siteBuilder.Config.TemplatesFallbackGitLabGroupName}
			} else if usePrimary {
				dirsToSearch = []string{s.siteBuilder.TemplateMapper.GetPrimary()}
				sources = []string{s.siteBuilder.Config.SiteName}
			} else {
				dirsToSearch = []string{s.siteBuilder.TemplateMapper.GetFallback()}
				sources = []string{s.siteBuilder.Config.TemplatesFallbackGitLabGroupName}
			}
		} else {
			dirsToSearch = []string{config.DoxaPaths.PrimaryTemplatesPath}
			sources = []string{s.siteBuilder.Config.SiteName}
		}
		resp.Values, err = ltfile.FileContentMatcher(dirsToSearch, sources, "lml", modifiedPattern, resultWidth)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusNotFound)
			resp.Message = fmt.Sprintf("%v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		if resp.Values == nil || len(resp.Values) == 0 {
			resp.Status = fmt.Sprintf("%d", http.StatusNotFound)
			resp.Message = fmt.Sprintf("no matches for %s", pattern)
			respond(w, http.StatusOK, resp)
			return
		}
		resp.Status = "OK"
		resp.Message = fmt.Sprintf("%d matches", len(resp.Values))
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleLmlMatch returns LML templates IDs matching the specified regex pattern
func (s *server) handleLmlMatch() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonStringValuesResponse)
		pattern := r.URL.Query().Get("pattern")
		if len(pattern) == 0 {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = "pattern missing"
			respond(w, http.StatusOK, resp)
			return
		}
		if strings.HasPrefix(pattern, "bk") ||
			strings.HasPrefix(pattern, "(bk") ||
			strings.HasPrefix(pattern, "se") ||
			strings.HasPrefix(pattern, "(se)") {
			// ok
		} else {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("bad template pattern %s: must start with bk or se", pattern)
			respond(w, http.StatusOK, resp)
			return
		}
		var err error
		// TODO: make this a parameter from the user
		osDelimiter := "/"
		dirExclusionPatterns := []string{fmt.Sprintf("%ss-temp%s", osDelimiter, osDelimiter),
			fmt.Sprintf("%ss-tests%s", osDelimiter, osDelimiter),
			fmt.Sprintf("%sa-templates%sMedia_Lists%s", osDelimiter, osDelimiter, osDelimiter)}
		resp.Values, err = s.siteBuilder.TemplateMapper.GetMatchingIDs([]string{pattern}, dirExclusionPatterns)
		if err != nil {
			return
		}
		resp.Status = "OK"
		if len(resp.Values) == 0 {
			resp.Message = "no matching templates"
		} else if len(resp.Values) == 1 {
			resp.Message = fmt.Sprintf("1 matching template")
		} else {
			resp.Message = fmt.Sprintf("%d matching templates", len(resp.Values))
		}
		respond(w, http.StatusOK, resp)
		return
	}
}

func (s *server) handleLmlPatterns() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		// if GET
		if r.Method == http.MethodGet {
			resp := new(JsonTemplatePatternsResponse)
			resp.Patterns = s.siteBuilder.Config.FilePatterns
			resp.Selections = s.siteBuilder.Config.FilePatternSelections
			resp.Status = "OK"
			respond(w, http.StatusOK, resp)
			return
		}
		// handle PUT
		resp := new(JsonStatusResponse)
		resp.Status = "OK"

		// parameter: patterns
		patternJson := r.URL.Query().Get("patterns")
		if len(patternJson) == 0 {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = "missing template patterns"
			respond(w, http.StatusOK, resp)
			return
		}
		var patterns []string
		var err error
		// convert query parm into patterns string slice
		err = json.Unmarshal([]byte(patternJson), &patterns)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("malformed template patterns: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		// validate patterns
		if err = validatePatterns(patterns); err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("%v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		// parameter selections
		selectionsJson := r.URL.Query().Get("selections")
		if len(selectionsJson) == 0 || selectionsJson == "[]" || selectionsJson == "[ ]" {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = "at least one pattern must be selected"
			respond(w, http.StatusOK, resp)
			return
		}
		// convert query parm into indexes string slice
		var selections []string
		err = json.Unmarshal([]byte(selectionsJson), &selections)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("malformed template pattern selections: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		// validate indexes in relation to patterns
		if err = validatePatternsAndIndexes(patterns, selections); err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("%v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		// for selected patterns, check for overlaps
		//if err = s.checkForOverlaps(patterns, selections); err != nil {
		//	//resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
		//	resp.Message = fmt.Sprintf("Warning: you will generate the same templates more than once because %v", err)
		//	//			respond(w, http.StatusOK, resp)
		//	//			return
		//}

		// concatenate selections into delimited string
		sb := strings.Builder{}
		for _, index := range selections {
			if sb.Len() > 0 {
				sb.WriteString(",")
			}
			sb.WriteString(strings.TrimSpace(index))
		}

		// save patterns to the database
		if err = s.settingsManager.SetStringSliceByType(properties.SiteBuildTemplatesPatterns, patterns); err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("error saving template name patterns %s: %v", patternJson, err)
			respond(w, http.StatusOK, resp)
			return
		}
		// save selections to the database
		if err = s.settingsManager.SetValueByType(properties.SiteBuildTemplatesPatternsSelected, sb.String()); err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("error saving template name pattern indexes %s: %v", selections, err)
			respond(w, http.StatusOK, resp)
			return
		}
		// we have updated the database, but we need to also update the in-memory config
		s.siteBuilder.Config.FilePatterns = patterns
		s.siteBuilder.Config.FilePatternSelections = selections

		resp.Message = fmt.Sprintf("Template file patterns saved. %s", resp.Message)
		respond(w, http.StatusOK, resp)
		return
	}
}

func (s *server) handleLmlStatusTypes() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonTemplateStatusTypesResponse)
		resp.Status = "OK"
		resp.Message = "Received template status types"
		resp.StatusTypes = statuses.StatusTypeNames()
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleFallbackSettings returns info about the user's template Fallback options
func (s *server) handleFallbackSettings() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonFallbackResponse)

		var err error
		resp.FallbackEnabled, resp.FallbackSelected,
			resp.AvailableSubscriptions, err = s.siteBuilder.GetFallbackSettings()

		if err == nil {
			if resp.FallbackEnabled {
				if len(resp.AvailableSubscriptions) == 0 { // If the user has no subscriptions or none with templates
					resp.Status = fmt.Sprintf("%d", http.StatusNotFound)
					resp.Message = "No template subscriptions found"
					respond(w, http.StatusOK, resp)
					return
				} else {
					resp.Status = "OK"
					resp.Message = "Subscriptions found containing templates"
					respond(w, http.StatusOK, resp)
					return
				}
			} else {
				resp.Status = "OK"
				resp.Message = "Fallbacks are not enabled"
				respond(w, http.StatusOK, resp)
				return
			}
		} else { // In case of error
			resp.Status = "Bad Request"
			resp.Message = fmt.Sprintf("%v", err)
			respond(w, http.StatusBadRequest, resp)
			return
		}
	}
}

func (s *server) handleFallbackSettingsUpdate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonFallbackUpdateResponse)
		var err error
		// Store the values for selected and enabled from HTTP GET
		selected := r.URL.Query().Get("selected")
		enabled, err := strconv.ParseBool(r.URL.Query().Get("enabled"))
		if err != nil { // If there's an error retrieving the Bool value from GET enabled
			resp.Status = "Enabled is not a Bool value"
			resp.Message = fmt.Sprintf("%v", err)
			respond(w, http.StatusBadRequest, resp)
			return
		}
		// Set the fallback settings
		err = s.siteBuilder.SetFallbackSettings(enabled, selected)

		if err != nil {
			resp.Status = "Bad Request"
			resp.Message = fmt.Sprintf("%v", err)
			respond(w, http.StatusBadRequest, resp)
			return
		} else {
			resp.Status = "OK"
			resp.Message = "updated"
			respond(w, http.StatusOK, resp)
			return
		}
	}
}

func WriteErrorHtml(path string, errors []parser.ParseError) error {
	sb := strings.Builder{}
	sb.WriteString(`
<!DOCTYPE html>
<html>
<head>
	<title>Template Errors</title>
</head>
<body>
 	<h1 style="color: red;">Your Template Has Errors</h1>
    <p>
       In the template editor, a line with an error has a red square to the left of the line number. Place your mouse cursor over the red square to read the error message. The errors are also shown below in a table. Note that sometimes several lines next to each other might show errors, but it is just the first marked line in the group that actually has an error. To open an inserted template that is the source of an error click on its link.  That will open a new template editor in another browser tab.
    </p>
    <table>
      <thead>
       <tr>
         <td>Line</td><td>Column</td><td>Error Message</td>
       </tr>
      </thead>
      <tbody>
      <style>
        table, td, th {
           border: 1px solid #ddd;
           padding: 8px;
        }
      </style>
`)
	for _, e := range errors {
		msg := e.Message
		if strings.Contains(msg, "expecting {") {
			parts := strings.Split(msg, "expecting {")
			msg = parts[0]
		}
		if strings.Contains(msg, "extraneous input") {
			msg = strings.ReplaceAll(msg, "extraneous input", "unknown keyword:")
		}
		if strings.Contains(msg, "no viable alternative at input 'case") {
			msg = "invalid case value"
		}
		parts := strings.Split(msg, "{")
		if len(parts) > 1 {
			// make the templateID containing the source of the error
			// a link that when clicked will open in a new tab
			// the ide and load the content of that template.
			// To envision what happens below,
			// here is an example of what last can look like.
			// a-templates/Lectionary/LectionaryParts/LectionaryGospelTitle/Gospel_Title/DaysByNumber/bl.Defaults 9 26 parse error: missing delimiter : in topic:key path}}}}}}
			id := strings.TrimSpace(parts[len(parts)-1])
			// chop off the last part which is the line and column number and the error itself,
			// so we have the id to use in the hyperlink.
			parts = strings.Split(id, " ")
			if len(parts) > 1 {
				id = parts[0]
				asb := strings.Builder{}
				asb.WriteString(" <a href=\"/ide?id=")
				asb.WriteString(id)
				asb.WriteString("\" target=\"_blank\">")
				asb.WriteString(id)
				asb.WriteString("</a>")
				msg = strings.ReplaceAll(msg, id, asb.String())
			}
		}
		sb.WriteString(fmt.Sprintf("<tr><td>%d</td><td>%d</td><td>%s</td></tr>", e.Line, e.Column+1, msg))
	}
	sb.WriteString(`
    </tbody>
  </table>
</body>
</html>
`)
	err := ltfile.WriteFile(path, sb.String())
	if err != nil {
		msg := fmt.Sprintf("Error writing errors to %s: %v\n", path, err)
		doxlog.Errorf(msg)
		return fmt.Errorf(msg)
	}
	return nil
}

func toJsonArrayString(templateId string) (string, error) {
	var r []string
	parts := strings.Split(templateId, "/")
	for _, p := range parts {
		r = append(r, p)
	}
	j, err := json.Marshal(r)
	if err != nil {
		return "", fmt.Errorf("could not convert %s into json array", templateId)
	}
	return url.QueryEscape(string(j)), nil
}
