package servers

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/websocket"
	"github.com/liturgiko/doxa/api"
	"github.com/liturgiko/doxa/app"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/completer"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/dvcs"
	"github.com/liturgiko/doxa/pkg/enums/books"
	"github.com/liturgiko/doxa/pkg/enums/calendarTypes"
	"github.com/liturgiko/doxa/pkg/enums/fileSystemActions"
	"github.com/liturgiko/doxa/pkg/enums/gitStatuses"
	"github.com/liturgiko/doxa/pkg/enums/properties"
	"github.com/liturgiko/doxa/pkg/enums/syncStrategyTypes"
	"github.com/liturgiko/doxa/pkg/ldp"
	"github.com/liturgiko/doxa/pkg/parser"
	"github.com/liturgiko/doxa/pkg/resolver"
	"github.com/liturgiko/doxa/pkg/scraper"
	"github.com/liturgiko/doxa/pkg/synch"
	"github.com/liturgiko/doxa/pkg/table"
	"github.com/liturgiko/doxa/pkg/users"
	"github.com/liturgiko/doxa/pkg/utils/ltRegEx"
	"github.com/liturgiko/doxa/pkg/utils/ltUnicode"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"github.com/liturgiko/doxa/pkg/utils/stamp"
	picoWidget "github.com/liturgiko/doxa/templates/webApp/structs/widget"
	"github.com/liturgiko/doxa/templx"
	"log"
	"log/slog"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"
)

const sessionCookie = "sessionId"

// authMiddleware intercepts every request and checks to see if the user is logged in if running in the cloud
func (s *server) authMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.HasPrefix(r.RequestURI, "/static") ||
			strings.HasPrefix(r.RequestURI, "/login") ||
			strings.HasPrefix(r.RequestURI, "/api/user/login") {
			next.ServeHTTP(w, r)
			return
		}

		// Check if user is authenticated
		if !s.userIsAuthenticated(r) {
			http.Redirect(w, r, "/login", http.StatusSeeOther)
			return
		}
		next.ServeHTTP(w, r)
	})
}
func (s *server) userIsAuthenticated(r *http.Request) bool {
	if !s.cloud {
		return true
	} else {
		cookie, err := r.Cookie(sessionCookie)
		if err != nil {
			return false
		}
		if _, ok := s.sessionManager.GetSessionById(cookie.Value); ok {
			return true
		}
		return false
	}
}
func CacheControlWrapper(cloud, linux bool, h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if cloud || linux {
			w.Header().Set("Cache-Control", "max-age=2592000") // 30 days
		} else {
			w.Header().Set("Cache-Control", "no-store")
		}
		h.ServeHTTP(w, r)
	})
}
func (s *server) getNavbar(w http.ResponseWriter, r *http.Request) Navbar {
	var sessionIdCookie *http.Cookie
	var err error
	navbar := new(Navbar)
	navbar.Cloud = s.Navbar.Cloud
	navbar.DoxaHandlesGit = app.Ctx.GitScriptManager != nil
	navbar.Title = s.Navbar.Title
	// TODO: these should be set through properties of the user's profile
	navbar.Gitlab = s.Navbar.Gitlab
	navbar.Editor = s.Navbar.Editor
	navbar.FallbackEnabled = s.Navbar.FallbackEnabled
	navbar.Admin = s.Navbar.Admin
	navbar.GitlabGroupName = s.Navbar.GitlabGroupName
	navbar.LML = s.Navbar.LML
	navbar.ProjectName = s.Navbar.ProjectName
	navbar.ProjectID = s.Navbar.ProjectID
	navbar.ProjectUrl = s.Navbar.ProjectUrl
	navbar.PublicSitePort = s.Navbar.PublicSitePort
	navbar.PublishedPublicSite = s.Navbar.PublishedPublicSite
	navbar.PublishedTestSite = s.Navbar.PublishedTestSite
	navbar.TestSitePort = s.Navbar.TestSitePort
	if s.cloud {
		sessionIdCookie, err = r.Cookie(sessionCookie)
		if err != nil {
			doxlog.Errorf("could not read session cookie: %v", err)
			http.Redirect(w, r, "/login", http.StatusSeeOther)
		}
		if len(sessionIdCookie.Value) == 0 {
			doxlog.Errorf("session cookie value len == 0")
			http.Redirect(w, r, "/login", http.StatusSeeOther)
		}
		if _, ok := s.sessionManager.GetSessionById(sessionIdCookie.Value); ok {
			navbar.LoggedIn = true
		} else {
			http.Redirect(w, r, "/login", http.StatusSeeOther)
		}
	}
	ts, _, _ := s.appApi.TokenService.GetTokenStatus(nil, "")
	navbar.TokenStatus = TokenStatus{Exists: ts != api.TokenNeeded, Locked: ts == api.PasswordNeeded}
	return *navbar
}

// RenderTemplate executes a given template with the provided data and applies minification
func (s *server) RenderTemplate(w http.ResponseWriter, tmpl string, data interface{}, mini bool) {
	w.Header().Set("Content-Type", "text/html")
	w.Header().Set("charset", "utf-8")

	// minify import breaks doxa's pkg/css.go.
	// can't use until figure out fix.
	if mini {
		//// Create a buffer to hold the template output
		//var buf bytes.Buffer
		//
		//// Execute the template into the buffer instead of directly writing to ResponseWriter
		//err := s.templates.ExecuteTemplate(&buf, tmpl, data)
		//if err != nil {
		//	http.Error(w, err.Error(), http.StatusInternalServerError)
		//	doxlog.Errorf("error rendering template %s: %v", tmpl, err.Error())
		//	return
		//}
		//
		//// Create a new minifier instance
		//m := minify.New()
		//
		//// Minify the buffer content (HTML)
		//minifiedHTML, err := m.String("text/html", buf.String())
		//if err != nil {
		//	http.Error(w, err.Error(), http.StatusInternalServerError)
		//	doxlog.Errorf("error minifying template %s: %v", tmpl, err.Error())
		//	return
		//}
		//
		//// Write the minified HTML to ResponseWriter
		//_, err = w.Write([]byte(minifiedHTML))
		//if err != nil {
		//	http.Error(w, err.Error(), http.StatusInternalServerError)
		//	doxlog.Errorf("error writing response for template %s: %v", tmpl, err.Error())
		//	return
		//}
		err := s.templates.ExecuteTemplate(w, tmpl, data)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			doxlog.Errorf("error rendering template %s: %v", tmpl, err.Error())
		}
	} else {
		err := s.templates.ExecuteTemplate(w, tmpl, data)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			doxlog.Errorf("error rendering template %s: %v", tmpl, err.Error())
		}
		return
	}
}

// handleHome serves the app home page
func (s *server) handleHome() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		data := new(HomeData)
		data.Navbar = s.getNavbar(w, r)
		data.Title = ""
		s.RenderTemplate(w, "home", data, false)
	}
}

// serveDbExplorerPage serves the database management page
func (s *server) serveDbExplorerPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		data := new(HomeData)
		data.Host = "" // s.host
		data.Navbar = s.getNavbar(w, r)
		data.Title = "DB Explorer"
		s.templates.ExecuteTemplate(w, "dbExplorer", data)
	}
}

// serveDbImportPage serves a page for users to select a file for import into the database
func (s *server) serveDbImportPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		data := new(HomeData)
		data.Host = "" // s.host
		data.Navbar = s.getNavbar(w, r)
		data.Title = "Database Importer"
		s.RenderTemplate(w, "dbImporter", data, false)
	}
}

// serveDbIdTracePage serves a page for users to trace the redirects of an ID
func (s *server) serveDbIdTracePage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		data := new(HomeData)
		data.Navbar = s.getNavbar(w, r)
		data.Host = "" // s.host

		data.Title = "DB ID Tracer"
		s.RenderTemplate(w, "idTrace", data, false)
	}
}

// serveTemplateExplorerPage serves the template search page
func (s *server) serveTemplateExplorerPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		data := new(TemplateExplorerData)
		data.Host = "" // s.host
		data.Navbar = s.getNavbar(w, r)
		data.Title = "Template Explorer"
		data.FallbackEnabled = s.siteBuilder.Config.TemplatesFallbackEnabled
		s.RenderTemplate(w, "templateExplorer", data, false)
	}
}

// serveIdeToolsPage serves the database management page and ldp tool without a navbar for use as a plug-in to an iframe
func (s *server) serveIdeToolsPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		var err error
		data := new(LdpData)
		data.Host = "" // s.host
		data.Title = "Editor Tools"
		data.Calendar = s.settingsManager.StringProps[properties.SiteBuildCalendar]
		data.Date = r.URL.Query().Get("date")
		if len(data.Date) > 0 {
			data.Date, err = ltstring.PadDate(data.Date, "-")
			if err != nil {
				data.Date = "" // silently reset
			}
		}
		data.FallbackEnabled = s.siteBuilder.Config.TemplatesFallbackEnabled
		s.RenderTemplate(w, "ideTools", data, false)
	}
}
func (s *server) serveAboutPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		data := new(AboutData)
		data.Host = "" // s.host
		data.Title = "About Doxa"
		data.Version = s.Version
		data.Project = s.ProjectName
		data.DoxaHome = s.ProjectPath

		s.RenderTemplate(w, "about", data, false)
	}
}

func (s *server) serveProtectTokenModal() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")

		data := new(IndexHtmlData)
		data.Title = "Protect Token"
		data.Navbar = s.getNavbar(w, r)
		data.Body = picoWidget.NewBody()
		data.Body.Main = picoWidget.NewMain(
			"Protect Gitlab Access Token",
			"Control DOXA's use of your Gitlab personal access token.",
		)
		grid := new(picoWidget.Grid)
		tokenForm := s.appApi.TokenService.GetWidget(nil, "api/htmx/token/", "token", "")
		grid.AddWidget(tokenForm)
		section := new(picoWidget.Section)
		section.AddGrid(grid)
		data.Body.Main.AddSection(section)
		s.RenderTemplate(w, "modalUnlockTokenContent", tokenForm, false)
	}
}

// serveBackupPage serves the backup page.
func (s *server) serveBackupPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		data := new(HomeData)
		data.Title = "Backup"
		data.Navbar = s.getNavbar(w, r)

		s.RenderTemplate(w, "backup", data, false)
	}
}

// serveMergePage serves the merge page.
func (s *server) serveMergePage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		s.backupMutex.Lock()
		defer s.backupMutex.Unlock()
		data := new(MergeData)
		data.Title = "Backup"
		data.Navbar = s.getNavbar(w, r)
		data.RepoData = app.Ctx.GitScriptManager.PushList.GetAllPushReposAsMergeData()

		s.RenderTemplate(w, "merge", data, false)
	}
}

// serveCssEditorPage serves the css editor page
// If role == html, serves /assets/css/app.css
// If role == pdf, serves assets/pdf/css/pdf.css
func (s *server) serveCssEditorPage(role string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		data := new(EditorData)
		data.Role = role
		data.Host = "" // s.host
		data.Navbar = s.getNavbar(w, r)
		data.Navbar.Editor = true
		data.Lang = "css"
		if role == "html" {
			data.Src = path.Join(config.AssetsDir, "css", "app.css")
			data.Title = "HTML Stylesheet"
		} else if role == "pdf" {
			data.Src = path.Join(config.AssetsDir, "pdf", "css", "pdf.css")
			data.Title = "PDF Stylesheet"
		} else {
			data.Title = "CSS"
			data.Text = fmt.Sprintf("unknown role: %s", data.Role)
		}
		var err error
		fileSource := path.Join(config.DoxaPaths.ProjectDirPath, data.Src)
		data.Text, err = ltfile.GetFileContent(fileSource)
		if err != nil {
			data.Text = fmt.Sprintf("%s, %v", fileSource, err)
		}
		s.RenderTemplate(w, "csseditor", data, false)
		if err != nil {
			doxlog.Error(err.Error())
		}
	}
}

// serveJsEditorPage serves the Javascript editor page
func (s *server) serveJsEditorPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		data := new(EditorData)
		data.Role = "app"
		data.Host = "" // s.host
		data.Navbar = s.getNavbar(w, r)
		data.Navbar.Editor = true
		data.Lang = "js"
		data.Title = "Javascript"
		data.Src = path.Join(config.DoxaPaths.AssetsPath, "js", "app.js")
		var err error
		data.Text, err = ltfile.GetFileContent(data.Src)
		if err != nil {
			data.Text = fmt.Sprintf("%s, %v", data.Src, err)
		}
		s.RenderTemplate(w, "jseditor", data, false)
	}
}

// serveLmlEditorPage serves the Liturgical Markup Language (template) editor page
func (s *server) serveLmlEditorPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		var err error
		data := new(EditorData)
		data.Title = "Template Editor"
		data.Host = "" // s.host
		data.Navbar = s.getNavbar(w, r)
		data.Navbar.FallbackEnabled = s.siteBuilder.Config.TemplatesFallbackEnabled
		data.Lang = "lml"
		data.Src = r.URL.Query().Get("id")
		var useKeyRing = true
		lookups, err := completer.NewCompleter(config.DoxaPaths.HtmlCss,
			config.DoxaPaths.PdfCss,
			config.DoxaPaths.PrimaryTemplatesPath,
			s.mapper,
			useKeyRing)
		if err != nil {
			doxlog.Errorf("%v", err)
		}
		data.Completions = lookups.ToJson()
		s.RenderTemplate(w, "lmleditor", data, false)
		if err != nil {
			doxlog.Errorf(err.Error())
		}
	}
}

// serveIdePage serves an Integrated Development Environment for editing templates
func (s *server) serveIdePage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		data := new(EditorData)
		data.Host = "" // s.host
		data.Navbar = s.getNavbar(w, r)
		data.Navbar.Editor = true
		data.Navbar.LML = true
		data.Navbar.FallbackEnabled = s.siteBuilder.Config.TemplatesFallbackEnabled
		data.Title = "Templates"
		data.Lang = "lml"
		data.Date = r.URL.Query().Get("date")
		var err error
		if len(data.Date) > 0 {
			data.Date, err = ltstring.PadDate(data.Date, "-")
			if err != nil {
				data.Date = "" // silently reset
			}
		}
		data.Src = r.URL.Query().Get("id")
		if len(data.Src) > 0 {
			parts := strings.Split(data.Src, "/")
			var src []string
			for _, p := range parts {
				src = append(src, p)
			}
			j, err := json.Marshal(&src)
			if err != nil {
				return
			}
			data.Title = fmt.Sprintf("%s: %s", data.Title, parts[len(parts)-1])
			data.Src = string(j)
		}
		s.RenderTemplate(w, "ide", data, false)
	}
}

// serveMediaEditorPage serves the media editor page
func (s *server) serveMediaEditorPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		data := new(EditorData)
		data.Navbar = s.getNavbar(w, r)
		data.Host = "" // s.host
		data.Title = "Media"
		data.Lang = "lml"
		data.Src = path.Join(config.DoxaPaths.PrimaryTemplatesPath, "Books", "Euchologion", "bk.eu.smallwaterblessing.lml")
		var err error
		data.Text, err = ltfile.GetFileContent(data.Src)
		if err != nil {
			data.Text = fmt.Sprintf("%s, %v", data.Src, err)
		}
		s.RenderTemplate(w, "mediaeditor", data, false)
	}
}

func (s *server) serveLoginPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		data := new(LoginData)
		data.Host = "" // s.host
		data.Navbar = s.Navbar
		data.Title = "Doxa Login"
		s.RenderTemplate(w, "login", data, false)
	}
}

// serveUserProfilePage serves the user profile page
func (s *server) serveUserProfilePage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		// TODO the server needs a user manager.  Use it to load the profile data
		data := new(UserProfileData)
		data.Navbar = s.getNavbar(w, r)
		data.Host = "" // s.host
		data.Title = "Profile"
		var err error
		var profile *users.Profile
		if s.cloud {
			// TODO
		} else {
			profile, err = s.userManager.GetProfile(users.LocalUser)
		}
		var profileJson string
		profileJson, err = profile.ToString()
		if err != nil {
			// TODO
		}
		var profileMap map[string]interface{}
		if err = json.Unmarshal([]byte(profileJson), &profileMap); err != nil {
			// TODO
		}
		data.UserProfile = profileMap
		data.VaultPath = config.DoxaPaths.VaultPath
		s.RenderTemplate(w, "userprofile", data, false)
	}
}

// serveLayoutPage serves the layouts editor
func (s *server) serveLayoutPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		data := new(HomeData)
		data.Host = "" // s.host
		data.Title = "Layout LayoutManager"
		data.Navbar = s.getNavbar(w, r)
		s.RenderTemplate(w, "layout", data, false)
	}
}

// serveLdpPage serves the Liturgical Day Properties page
func (s *server) serveLdpPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		data := new(LdpData)
		data.Host = "" // s.host
		data.Title = "LDP"
		data.Navbar = s.getNavbar(w, r)
		data.Calendar = s.settingsManager.StringProps[properties.SiteBuildCalendar]
		s.RenderTemplate(w, "ldp", data, false)
	}
}

// serveSiteComparisonPage serves the Site Comparison page
func (s *server) serveSiteComparisonPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		data := new(SiteComparisonPageData)
		data.Host = "" // s.host
		data.Navbar = s.getNavbar(w, r)
		data.Title = "Site Comparison"
		s.RenderTemplate(w, "siteCmp", data, false)
	}
}

// serveSynchManagerPage serves the Synchronization LayoutManager page
func (s *server) serveSynchManagerPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		var profileExists bool
		var gitlabURL string
		git := "user" // changed to "doxa" if user wants doxa to handle git
		if s.cloud {
			// TODO need session
		} else {
			profile, err := s.userManager.GetProfile(users.LocalUser)
			profileExists = err == nil && profile != nil
			if profileExists {
				if profile.Gitlab.DoxaHandlesGitlab {
					git = "doxa"
					if profile.Gitlab.Verified && profile.Gitlab.GitlabRootGroup != nil {
						gitlabURL = profile.Gitlab.GitlabRootGroup.WebURL
					}
				}
			}
		}
		data := new(SynchManagerData)
		data.Host = "" // s.host

		data.Navbar = s.getNavbar(w, r)
		data.Title = "Synchronization LayoutManager"
		data.Git = git
		data.GitlabURL = gitlabURL
		data.ProfileExists = profileExists
		if data.ProfileExists {
			// verify we have an internet connection to gitlab.com
			data.GitlabReachable = scraper.IsAvailable(dvcs.BaseUrl)
		}
		data.ResourcesDir = config.DoxaPaths.ResourcesPath
		s.RenderTemplate(w, "synch", data, false)
	}
}

// serveGeneratorPage serves the site generator page
func (s *server) serveGeneratorPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		data := new(GenData)
		data.Navbar = s.getNavbar(w, r)
		data.Host = "" // s.host
		data.Title = "Generate"
		// reload the properties
		go func() {
			s.Mutex.Lock()
			defer s.Mutex.Unlock()
			err := s.settingsManager.ReadConfiguration()
			if err != nil {
				doxlog.Infof("error reloading properties: %v", err)
			}
		}()
		s.RenderTemplate(w, "generator", data, false)
	}
}

// servePublicationPage serves the site publication page
func (s *server) servePublicationPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		data := new(GenData)
		data.Host = "" // s.host
		data.Navbar = s.getNavbar(w, r)
		data.Title = "Publish"
		// reload the properties
		go func() {
			s.Mutex.Lock()
			defer s.Mutex.Unlock()
			err := s.settingsManager.ReadConfiguration()
			if err != nil {
				doxlog.Infof("error reloading properties: %v", err)
			}
		}()
		s.RenderTemplate(w, "publisher", data, false)
	}
}

// handleGeneration generates requested websites (test and/or public)
func (s *server) handleGeneration() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		resp := new(JsonStatusResponse)

		// only allow one request at a time for generation
		s.Mutex.Lock()
		defer s.Mutex.Unlock()
		s.siteBuilder.RelayMuted = false
		s.siteBuilder.RelayUsesStandardOut = false

		var err error

		// get the build public parameter
		// and save it to the database
		val := r.URL.Query().Get("public")
		if len(val) == 0 || val == "null" || val == "undefined" {
			val = "false"
		}
		// save it
		if err = s.settingsManager.SetValueByType(properties.SiteBuildPublic, val); err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("build public: could not convert %s to bool", val)
			respond(w, http.StatusOK, resp)
			return
		}
		// read it back.
		buildPublic := s.settingsManager.BoolProps[properties.SiteBuildPublic]

		// get the pre-generation bool parameter values
		// and save them to the database.
		// parameter: copy assets
		val = r.URL.Query().Get("copyAssets")
		if len(val) == 0 || val == "null" || val == "undefined" {
			val = "false"
		}
		if err = s.settingsManager.SetValueByType(properties.SiteBuildAssetsCopyAssets, val); err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("copy assets: could not convert %s to bool", val)
			respond(w, http.StatusOK, resp)
			return
		}
		// parameter: delete Html
		val = r.URL.Query().Get("deleteHtml")
		if len(val) == 0 || val == "null" || val == "undefined" {
			val = "false"
		}
		if err = s.settingsManager.SetValueByType(properties.SiteBuildPregenDeleteHtmlDir, val); err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("delete HTML files: could not convert %s to bool", val)
			respond(w, http.StatusOK, resp)
			return
		}
		// parameter: delete Pdf
		val = r.URL.Query().Get("deletePdf")
		if len(val) == 0 || val == "null" || val == "undefined" {
			val = "false"
		}
		if err = s.settingsManager.SetValueByType(properties.SiteBuildPregenDeletePdfDir, val); err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("delete PDF files: could not convert %s to bool", val)
			respond(w, http.StatusOK, resp)
			return
		}
		// parameter: index site for navigation
		val = r.URL.Query().Get("indexSiteForNav")
		if len(val) == 0 || val == "null" || val == "undefined" {
			val = "false"
		}
		if err = s.settingsManager.SetValueByType(properties.SiteBuildPostgenIndexForNav, val); err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("index for navigation: could not convert %s to bool", val)
			respond(w, http.StatusOK, resp)
			return
		}
		// parameter: index site for search
		val = r.URL.Query().Get("indexSiteForSearch")
		if len(val) == 0 || val == "null" || val == "undefined" {
			val = "false"
		}
		if err = s.settingsManager.SetValueByType(properties.SiteBuildPostgenIndexForSearch, val); err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("index for search: could not convert %s to bool", val)
			respond(w, http.StatusOK, resp)
			return
		}

		// get the file types to generate
		// parameter: gen Html
		val = r.URL.Query().Get("genHtml")
		if len(val) == 0 || val == "null" || val == "undefined" {
			val = "false"
		}
		if err = s.settingsManager.SetValueByType(properties.SiteBuildOutputHtml, val); err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("generate HTML files: could not convert %s to bool", val)
			respond(w, http.StatusOK, resp)
			return
		}
		// parameter: gen Pdf
		val = r.URL.Query().Get("genPdf")
		if len(val) == 0 || val == "null" || val == "undefined" {
			val = "false"
		}
		if err = s.settingsManager.SetValueByType(properties.SiteBuildOutputPdf, val); err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("generate PDF files: could not convert %s to bool", val)
			respond(w, http.StatusOK, resp)
			return
		}
		// parameter: layouts
		val = r.URL.Query().Get("layouts")
		if len(val) == 0 || val == "null" || val == "undefined" {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = "no layout selected"
			respond(w, http.StatusOK, resp)
			return
		}
		if err := s.settingsManager.SetValueByType(properties.SiteBuildLayoutsSelected, val); err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("selected layouts: could not save %s to properties", val)
			respond(w, http.StatusOK, resp)
			return
		}
		// parameter: selected template statuses
		val = r.URL.Query().Get("statuses")
		if len(val) == 0 && val != "null" || val == "undefined" {
			val = "na"
		}
		if err = s.settingsManager.SetValueByType(properties.SiteBuildTemplatesStatusesSelected, val); err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("selected template statuses: could not save %s to properties", val)
			respond(w, http.StatusOK, resp)
			return
		}
		// parameter: selected days of the week
		val = r.URL.Query().Get("dow") // days of week
		if len(val) == 0 || val == "null" || val == "undefined" {
			val = "*"
		}
		if err := s.settingsManager.SetValueByType(properties.SiteBuildTemplatesDaysOfWeekSelected, val); err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("selected template statuses: could not save %s to properties", val)
			respond(w, http.StatusOK, resp)
			return
		}
		// get the user profile
		var uid string
		if s.cloud {
			// TODO: get user from session
		} else {
			uid = users.LocalUser
		}
		// get the user profile
		var userProfile *users.Profile
		userProfile, err = s.userManager.GetProfile(uid)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusNotFound)
			resp.Message = fmt.Sprintf("error getting profile for %s: %v", uid, err)
			respond(w, http.StatusOK, resp)
			return
		}
		// send message to client to clear log
		Clear(s.siteBuilder.MsgChan)

		// if doxa handles git for user, pull from remote prior to generating
		if userProfile.Gitlab != nil {
			if userProfile.Gitlab.DoxaHandlesGitlab {
				sitePath := config.DoxaPaths.SiteGenTestPath
				if buildPublic {
					sitePath = config.DoxaPaths.SiteGenPublicPath
				}
				sitePath = path.Dir(sitePath) // drop off the dcs folder, so we are in the repo root dir
				s.siteBuilder.MsgChan <- "GM: pulling remote repo prior to generating"
				gClient := dvcs.NewGitClient(sitePath)
				if gClient.LocalExists(sitePath) {
					status, err := gClient.PullDoxa(sitePath, true)
					switch status {
					case gitStatuses.AlreadyUpToDate:
						s.siteBuilder.MsgChan <- "GM: already up to date"
					case gitStatuses.UnstagedChanges:
						s.siteBuilder.MsgChan <- "unstaged changes"
					default:
						if err == nil {
							s.siteBuilder.MsgChan <- fmt.Sprintf("GM: unhandled result from pulling repo, will continue with generation")
						} else {
							s.siteBuilder.MsgChan <- fmt.Sprintf("GM: could not pull repo, will continue with generation: %v", err)
						}
					}
				} else {
					s.siteBuilder.MsgChan <- fmt.Sprintf("GM: %s is not a git repo", sitePath)
				}
			}
		}

		s.siteBuilder.CancelIndexers() // this is the search indexer that runs as a go routine at the end of this handler.
		s.siteBuilder.Resolver.Reset()

		s.siteBuilder.MsgChan <- fmt.Sprintf("GM: reloading the configuration settings")
		err = s.settingsManager.ReadConfiguration()
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusInternalServerError)
			resp.Message = fmt.Sprintf("error reloading configuration settings: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		s.siteBuilder.MsgChan <- fmt.Sprintf("GM: finished reloading the configuration settings")

		s.siteBuilder.RelayUsesStandardOut = false // build messages will use the msgChan instead
		var finishTime string
		var thereWereErrors bool
		var wg sync.WaitGroup
		wg.Add(1)
		go func() {
			defer wg.Done()
			timeStamp := stamp.NewStamp("site generation")
			s.siteBuilder.MsgChan <- fmt.Sprintf("GM: %s", timeStamp.Start())
			// call the builder
			var parseErrors []parser.ParseError
			err, parseErrors = s.siteBuilder.Build(buildPublic, false)
			if err != nil {
				if !thereWereErrors {
					thereWereErrors = true
				}
				s.siteBuilder.MsgChan <- fmt.Sprintf("GM: %v", err)
			}
			if parseErrors != nil && len(parseErrors) > 0 {
				if !thereWereErrors {
					thereWereErrors = true
				}
			}
			if err == nil {
				if s.siteBuilder.Config.IndexForNav {
					s.genMsgChan <- "GM: creating html files (e.g. index, help, dcs, blank, booksindex, servicesindex)"
					err = s.siteBuilder.WriteAuxPages()
					if err != nil {
						s.siteBuilder.MsgChan <- fmt.Sprintf("GM: %v", err)
					}
				}
			}
			finishTime = timeStamp.Finish()
			s.siteBuilder.MsgChan <- fmt.Sprintf("GM: %s", finishTime)
		}()
		wg.Wait()
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusInternalServerError) // need a better status
			resp.Message = fmt.Sprintf("%v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		if s.siteBuilder.Config.IndexForSearch {
			if err != nil {
				resp.Status = fmt.Sprintf("%d", http.StatusInternalServerError) // need a better status
				resp.Message = fmt.Sprintf("%v", err)
				respond(w, http.StatusOK, resp)
				return
			}
			// Disabled by Michael Colburn 2024-08-12.  Takes up too much disk space in generated website.
			//go s.siteBuilder.WriteSearchPages(s.siteBuilder.AddContext())
		}
		if thereWereErrors {
			resp.Status = fmt.Sprintf("%d", http.StatusInternalServerError)
			resp.Message = fmt.Sprintf("Generation took %s. There were errors generating the site.", finishTime)
			respond(w, http.StatusOK, resp)
			return
		}
		resp.Status = "OK"
		resp.Message = fmt.Sprintf("Generation finished.  Took %s.  No errors.", finishTime)
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleIndexNav creates the navigation index pages for the requested websites (test and/or public)
func (s *server) handleIndexNav() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		resp := new(JsonStatusResponse)

		// only allow one request at a time for generation
		s.Mutex.Lock()
		defer s.Mutex.Unlock()
		s.siteBuilder.RelayMuted = false
		s.siteBuilder.RelayUsesStandardOut = false

		// parameter: build public
		buildPublic := r.URL.Query().Get("public") == "true"

		// get the user profile
		var uid string
		if s.cloud {
			// TODO: get user from session
		} else {
			uid = users.LocalUser
		}
		var err error
		// get the user profile
		var userProfile *users.Profile
		userProfile, err = s.userManager.GetProfile(uid)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusNotFound)
			resp.Message = fmt.Sprintf("error getting profile for %s: %v", uid, err)
			respond(w, http.StatusOK, resp)
			return
		}
		// send message to client to clear log
		Clear(s.siteBuilder.MsgChan)

		// if doxa handles git for user, pull from remote prior to generating
		if userProfile.Gitlab != nil {
			if userProfile.Gitlab.DoxaHandlesGitlab {
				sitePath := config.DoxaPaths.SiteGenTestPath
				if buildPublic {
					sitePath = config.DoxaPaths.SiteGenPublicPath
				}
				sitePath = path.Dir(sitePath) // drop off the dcs folder, so we are in the repo root dir
				s.siteBuilder.MsgChan <- "GM: pulling remote repo prior to generating"
				gClient := dvcs.NewGitClient(sitePath)
				if gClient.LocalExists(sitePath) {
					status, err := gClient.PullDoxa(sitePath, true)
					switch status {
					case gitStatuses.AlreadyUpToDate:
						s.siteBuilder.MsgChan <- "GM: already up to date"
					case gitStatuses.UnstagedChanges:
						s.siteBuilder.MsgChan <- "unstaged changes"
					default:
						if err == nil {
							s.siteBuilder.MsgChan <- fmt.Sprintf("GM: unhandled result from pulling repo, will continue with generation")
						} else {
							s.siteBuilder.MsgChan <- fmt.Sprintf("GM: could not pull repo, will continue with generation: %v", err)
						}
					}
				} else {
					s.siteBuilder.MsgChan <- fmt.Sprintf("GM: %s is not a git repo", sitePath)
				}
			}
		}

		s.siteBuilder.CancelIndexers() // this is the search indexer that runs as a go routine at the end of this handler.
		s.siteBuilder.Resolver.Reset()

		s.siteBuilder.MsgChan <- fmt.Sprintf("GM: reloading settings manager")
		err = s.settingsManager.ReadConfiguration()
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusInternalServerError)
			resp.Message = fmt.Sprintf("error reloading settings manager: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		s.siteBuilder.MsgChan <- fmt.Sprintf("GM: finished reloading settings manager")

		// reload the config
		err = s.siteBuilder.LoadConfig()
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusInternalServerError)
			resp.Message = fmt.Sprintf("error reloading sitebuilder configuration: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		s.siteBuilder.RelayUsesStandardOut = false // build messages will use the msgChan instead
		var finishTime string
		var wg sync.WaitGroup
		wg.Add(1)
		go func() {
			defer wg.Done()
			timeStamp := stamp.NewStamp("site auxiliary files creation")
			s.siteBuilder.MsgChan <- fmt.Sprintf("GM: %s", timeStamp.Start())
			s.genMsgChan <- "GM: creating auxiliary html pages (e.g. blank, booksindex, dcs, help, servicesindex)"
			err = s.siteBuilder.WriteAuxPages()
			if err != nil {
				s.siteBuilder.MsgChan <- fmt.Sprintf("GM: %v", err)
			}
			finishTime = timeStamp.Finish()
			s.siteBuilder.MsgChan <- fmt.Sprintf("GM: %s", finishTime)
		}()
		wg.Wait()
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusInternalServerError)
			resp.Message = fmt.Sprintf("%v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		resp.Status = "OK"
		resp.Message = fmt.Sprintf("Generation finished.  Took %s.  No errors.", finishTime)
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleIndexSearch creates the index json for searching the requested websites (test and/or public)
func (s *server) handleIndexSearch() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		resp := new(JsonStatusResponse)

		// only allow one request at a time for generation
		s.Mutex.Lock()
		defer s.Mutex.Unlock()
		s.siteBuilder.RelayMuted = false
		s.siteBuilder.RelayUsesStandardOut = false

		// parameter: build public
		buildPublic := r.URL.Query().Get("public") == "true"

		// get the user profile
		var uid string
		if s.cloud {
			// TODO: get user from session
		} else {
			uid = users.LocalUser
		}
		var err error
		// get the user profile
		var userProfile *users.Profile
		userProfile, err = s.userManager.GetProfile(uid)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusNotFound)
			resp.Message = fmt.Sprintf("error getting profile for %s: %v", uid, err)
			respond(w, http.StatusOK, resp)
			return
		}
		// send message to client to clear log
		Clear(s.siteBuilder.MsgChan)

		// if doxa handles git for user, pull from remote prior to generating
		if userProfile.Gitlab != nil {
			if userProfile.Gitlab.DoxaHandlesGitlab {
				sitePath := config.DoxaPaths.SiteGenTestPath
				if buildPublic {
					sitePath = config.DoxaPaths.SiteGenPublicPath
				}
				sitePath = path.Dir(sitePath) // drop off the dcs folder, so we are in the repo root dir
				s.siteBuilder.MsgChan <- "GM: pulling remote repo prior to generating"
				gClient := dvcs.NewGitClient(sitePath)
				if gClient.LocalExists(sitePath) {
					status, err := gClient.PullDoxa(sitePath, true)
					switch status {
					case gitStatuses.AlreadyUpToDate:
						s.siteBuilder.MsgChan <- "GM: already up to date"
					case gitStatuses.UnstagedChanges:
						s.siteBuilder.MsgChan <- "unstaged changes"
					default:
						if err == nil {
							s.siteBuilder.MsgChan <- fmt.Sprintf("GM: unhandled result from pulling repo, will continue with generation")
						} else {
							s.siteBuilder.MsgChan <- fmt.Sprintf("GM: could not pull repo, will continue with generation: %v", err)
						}
					}
				} else {
					s.siteBuilder.MsgChan <- fmt.Sprintf("GM: %s is not a git repo", sitePath)
				}
			}
		}

		s.siteBuilder.CancelIndexers() // this is the search indexer that runs as a go routine at the end of this handler.
		s.siteBuilder.Resolver.Reset()

		s.siteBuilder.MsgChan <- fmt.Sprintf("GM: reloading the configuration settings")
		err = s.settingsManager.ReadConfiguration()
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusInternalServerError)
			resp.Message = fmt.Sprintf("error reloading configuration settings: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		s.siteBuilder.MsgChan <- fmt.Sprintf("GM: finished reloading the configuration settings")

		// reload the config
		err = s.siteBuilder.LoadConfig()
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusInternalServerError)
			resp.Message = fmt.Sprintf("error reloading sitebuilder configuration: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}

		s.siteBuilder.RelayUsesStandardOut = false // build messages will use the msgChan instead
		var finishTime string
		var wg sync.WaitGroup
		wg.Add(1)
		go func() {
			defer wg.Done()
			timeStamp := stamp.NewStamp("site search indexes creation")
			s.siteBuilder.MsgChan <- fmt.Sprintf("GM: %s", timeStamp.Start())
			s.genMsgChan <- "GM: creating search indexes)"
			s.siteBuilder.WriteSearchPages(s.siteBuilder.AddContext())
			finishTime = timeStamp.Finish()
			s.siteBuilder.MsgChan <- fmt.Sprintf("GM: %s", finishTime)
		}()
		wg.Wait()
		resp.Status = "OK"
		resp.Message = fmt.Sprintf("Generation finished.  Took %s.  No errors.", finishTime)
		respond(w, http.StatusOK, resp)
		return
	}
}

// Clear sends a message to the web socket client to clear its message log
// A timer runs in order to give the client time to clear before messages start coming
func Clear(msgChan chan string) {
	msgChan <- "clear"
	timer := time.NewTimer(1 * time.Second)
	<-timer.C
}

// handlePublication publishes requested websites (test or public)
func (s *server) handlePublication() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		resp := new(JsonStatusResponse)

		// only allow one request at a time for generation
		s.Mutex.Lock()
		defer s.Mutex.Unlock()

		// parameter: build public
		buildPublic := r.URL.Query().Get("public") == "true"
		siteType := "test"
		if buildPublic {
			siteType = "public"
		}
		// get the user profile
		var uid string
		if s.cloud {
			// TODO: get user from session
		} else {
			uid = users.LocalUser
		}
		var err error
		// get the user profile
		var userProfile *users.Profile
		userProfile, err = s.userManager.GetProfile(uid)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusNotFound)
			resp.Message = fmt.Sprintf("error getting profile for %s: %v", uid, err)
			respond(w, http.StatusOK, resp)
			return
		}
		Clear(s.publishMsgChan)

		// if doxa handles git for user, commit and push the repo
		if userProfile.Gitlab != nil {
			if userProfile.Gitlab.DoxaHandlesGitlab {
				// get the token from the user manager.
				var token string
				token, err = s.userManager.GetToken(uid)
				if err != nil {
					resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
					resp.Message = fmt.Sprintf("could not retrieve token from vault: %v", err)
					respond(w, http.StatusOK, resp)
					return
				}
				sitePath := config.DoxaPaths.SiteGenTestPath
				if buildPublic {
					sitePath = config.DoxaPaths.SiteGenPublicPath
				}
				sitePath = path.Dir(sitePath) // drop off the dcs folder, so we are in the repo root dir
				s.publishMsgChan <- "PM: starting publication"
				gClient := dvcs.NewGitClient(sitePath)
				// TODO: if local is not a repo, initialize the remote and local
				if gClient.LocalExists(sitePath) {
					//var upToDate bool
					//upToDate, err = gClient.UpToDate(sitePath)
					//if upToDate {
					//	resp.Status = "OK"
					//	s.publishMsgChan <- fmt.Sprintf("%s has no changes. Already up-to-date.", siteType)
					//	s.publishMsgChan <- fmt.Sprintf("canceled publication of %s website", siteType)
					//	respond(w, http.StatusOK, resp)
					//	return
					//}
					s.publishMsgChan <- "adding files to git index"
					// TODO: go-git not working to add deleted files.  Executing bash instead, which means user must install git.
					cmd := exec.Command("git", "add", "-A")
					cmd.Dir = sitePath
					var out []byte
					out, err = cmd.Output()
					if err != nil {
						if strings.Contains(string(out), "") {

						}
						resp.Status = fmt.Sprintf("%d", http.StatusOK)
						resp.Message = fmt.Sprintf("error adding files to git index: %v", err)
						s.publishMsgChan <- resp.Message
						respond(w, http.StatusOK, resp)
						return
					}
					if strings.Contains(string(out), "") {

					}

					s.publishMsgChan <- "committing files"
					// TODO: go-git not working to commit files.  Executing bash instead, which means user must install git.
					cmd = exec.Command("git", "commit", "-m", fmt.Sprintf("doxa %s", time.Now().Format(time.RFC3339)))
					cmd.Dir = sitePath
					out, err = cmd.Output()
					if err != nil {
						if strings.Contains(string(out), "nothing to commit") {
							resp.Status = "OK"
							s.publishMsgChan <- fmt.Sprintf("%s has no changes. Already up-to-date.", siteType)
							s.publishMsgChan <- fmt.Sprintf("canceled publication of %s website", siteType)
							respond(w, http.StatusOK, resp)
							return
						}
						resp.Status = fmt.Sprintf("%d", http.StatusOK)
						resp.Message = fmt.Sprintf("error committing files to git: %v", err)
						s.publishMsgChan <- resp.Message
						respond(w, http.StatusOK, resp)
						return
					}
					// TODO: figure out how to get go-git to work
					//err = gClient.CommitAll(sitePath, "doxa publisher")
					//if err != nil {
					//	resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
					//	resp.Message = fmt.Sprintf("error commiting files: %v", err)
					//	s.publishMsgChan <- resp.Message
					//	respond(w, http.StatusOK, resp)
					//	return
					//}
					s.publishMsgChan <- "pushing files"
					err = gClient.Push(sitePath, dvcs.Username, token)
					if err != nil {
						resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
						resp.Message = fmt.Sprintf("error pushing files: %v", err)
						s.publishMsgChan <- resp.Message
						respond(w, http.StatusOK, resp)
						return
					}
				} else {
					s.publishMsgChan <- fmt.Sprintf("PM: %s is not a git repo", sitePath)
				}
				s.publishMsgChan <- "PM: completed publication"
			} else {
				s.publishMsgChan <- "PM: according to your user profile, you want to handle git yourself"
			}
		} else {
			s.publishMsgChan <- "PM: gitlab not set in user profile"
		}
		resp.Status = "OK"
		resp.Message = fmt.Sprintf("published %s website", siteType)
		s.publishMsgChan <- fmt.Sprintf("published %s website", siteType)
		respond(w, http.StatusOK, resp)
		return
	}
}

// serveRegExPage serves the regular expression tools page
func (s *server) serveRegExPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		data := new(RegExData)
		data.Host = "" // s.host
		data.Title = "RegEx Tester"
		data.Navbar = s.getNavbar(w, r)
		s.RenderTemplate(w, "regex", data, false)
	}
}

// serveLogsPage provides a page for the user to view logs
func (s *server) serveLogsPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		data := new(RegExData)
		data.Title = "Log Viewer"
		data.Host = "" // s.host
		data.Navbar = s.getNavbar(w, r)
		s.RenderTemplate(w, "logs", data, false)
	}
}

// handleLogRequest provides the entries in the currently open log.
func (s *server) handleLogRequest() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonLogFileResponse)
		// create table data
		resp.TableData = new(table.Data)
		resp.TableData.Title = "Log Entries"
		resp.TableData.Desc = "This table lists each entry in the current log."
		resp.TableData.Headings = []string{"Time", "Level", "Message", "Function", "File", "Line"}
		resp.TableData.Filter = 1
		resp.TableData.FilterValue = "ERROR"
		resp.TableData.Widths = []int{20, 5, 90, 10, 20, 5}
		var entries []*doxlog.MetaError
		var err error
		entries, err = doxlog.LogEntries()
		if err != nil {
			resp.Status = "500"
			resp.Message = fmt.Sprintf("error getting entries from log: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		//{"time":"2023-09-27T15:54:39.766555-07:00","level":"INFO","msg":"Starting...","function":"main","file":"main.go","line":118}
		for _, e := range entries {
			var row []*table.Cell
			// time
			ck := new(table.Cell)
			ck.Value = e.Time
			row = append(row, ck)
			// level
			ck = new(table.Cell)
			ck.Value = e.Level
			row = append(row, ck)
			// message
			ck = new(table.Cell)
			ck.Value = e.Msg
			row = append(row, ck)
			// function
			ck = new(table.Cell)
			ck.Value = e.Function
			row = append(row, ck)
			// file
			ck = new(table.Cell)
			ck.Value = e.File
			row = append(row, ck)
			// line
			ck = new(table.Cell)
			ck.Value = fmt.Sprintf("%09d", e.Line)
			row = append(row, ck)
			resp.TableData.Rows = append(resp.TableData.Rows, row)
		}
		resp.Status = "OK"
		resp.Message = fmt.Sprintf("received %d log file entries from %s...", len(resp.TableData.Rows), doxlog.LogFile.Name())
		respond(w, http.StatusOK, resp)
		return
	}
}

// serveUnicodePage serves the unicode tools page
func (s *server) serveUnicodePage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		data := new(RegExData)
		data.Host = "" // s.host
		data.Title = "Unicode Tools"
		data.Navbar = s.getNavbar(w, r)
		s.RenderTemplate(w, "unicode", data, false)
	}
}

// serveUnicodePage serves the unicode tools page
func (s *server) serveOauthCallback() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		// if all ok, serve home page
		//data := new(HomeData)
		//data.Navbar = s.getNavbar(w, r)
		//data.Title = "Home"
		//s.RenderTemplate(w, "home",  data, false)
		//if err != nil {
		//	doxlog.Error(err.Error())
		//}
		code := r.URL.Query().Get("code")
		state := r.URL.Query().Get("state")
		fmt.Printf("code: %s state: %s\n", code, state)
		//r.URL.Host = "127.0.0.1:4180"
		//r.URL.RawQuery = ""
		//r.RequestURI = r.URL.String()
		//http.Redirect(w, r, "127.0.0.1:4180/", http.StatusMovedPermanently)
		resp := new(JsonStatusResponse)
		resp.Status = fmt.Sprintf("%d", http.StatusOK)
		respond(w, http.StatusOK, resp)
	}
}

// serveSettingsPage serves the settings management page
func (s *server) serveSettingsPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		var configs []string
		var err error
		data := new(SettingsData)
		data.Host = "" // s.host
		data.Title = "Settings"
		data.Navbar = s.getNavbar(w, r)
		data.CurrentConfig = s.settingsManager.ConfigPath
		configs, err = s.settingsManager.AvailableConfigs()
		if err != nil {
			doxlog.Errorf("%v", err)
		}
		data.AvailableConfigs = strings.Join(configs, ",")
		s.RenderTemplate(w, "settings", data, false)
		if err != nil {
			doxlog.Errorf("error executing settings template")
		}
	}
}

type Client struct {
	name   string
	events chan *DashBoard
}
type DashBoard struct {
	User uint
}

func updateDashboard(client *Client) {
	for {
		db := &DashBoard{
			User: uint(rand.Uint32()),
		}
		client.events <- db
	}
}

// sseEndpoint handles Server Sent Events
// MAC: This is not being used. It works OK
//
//	     if it is go client, but
//	     not if it is a javascript client.
//	     Here is what the javascript should be:
//				this.evtSource = new EventSource(":8080/sse");
//				this.evtSource.onmessage = function(ev) {
//	  			console.log(ev.data)
//				};
//				this.evtSource.onopen = e => {
//	  			console.log(e);
//				}
//				this.evtSource.addEventListener('clear', e => {
//	  			console.log(e);
//				});
//				this.evtSource.addEventListener('gm', e => {
//	  			console.log(e);
//				});
//				this.evtSource.addEventListener('pm', e => {
//	  			console.log(e);
//				});

var genWs *websocket.Conn
var genUpgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 2048,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

func genSocketWriter(conn *websocket.Conn, msgChannel chan string) {
	for {
		select {
		case msg := <-msgChannel:
			{
				if msg != "clear" {
					first := strings.ToLower(msg[0:2])
					if !strings.HasPrefix(first, "gm") {
						msg = fmt.Sprintf("GM: %s", msg)
					}
				}
				//				fmt.Println(msg)
				if err := conn.WriteMessage(1, []byte(msg)); err != nil {
					doxlog.Errorf("%v", err)
				}
			}
		default:
		}
	}
}
func genReader(conn *websocket.Conn, buildChannel chan<- string) {
	for {
		messageType, p, err := conn.ReadMessage()

		if err != nil {
			doxlog.Errorf("%v", err)
			return
		}
		log.Println(string(p))

		if err := conn.WriteMessage(messageType, p); err != nil {
			doxlog.Errorf("%v", err)
			return
		}
	}
}

var publishWs *websocket.Conn
var publishUpgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 2048,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

func publishSocketWriter(conn *websocket.Conn, msgChannel chan string) {
	for {
		select {
		case msg := <-msgChannel:
			{
				if msg != "clear" {
					first := strings.ToLower(msg[0:2])
					if !strings.HasPrefix(first, "pm") {
						msg = fmt.Sprintf("PM: %s", msg)
					}
				}
				//fmt.Println(msg)
				if err := conn.WriteMessage(1, []byte(msg)); err != nil {
					doxlog.Errorf("%v", err)
				}
			}
		default:
		}
	}
}

var roSyncWs *websocket.Conn
var roSyncUpgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 2048,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

func roSyncSocketWriter(conn *websocket.Conn, msgChannel chan string) {
	for {
		select {
		case msg := <-msgChannel:
			{
				//fmt.Println(msg)
				if err := conn.WriteMessage(1, []byte(msg)); err != nil {
					doxlog.Errorf("%v", err)
				}
			}
		default:
		}
	}
}
func roSyncReader(conn *websocket.Conn, buildChannel chan<- string) {
	for {
		messageType, p, err := conn.ReadMessage()

		if err != nil {
			doxlog.Errorf("%v", err)
			return
		}
		log.Println(string(p))

		if err := conn.WriteMessage(messageType, p); err != nil {
			doxlog.Errorf("%v", err)
			return
		}
	}
}

var rwSyncWs *websocket.Conn
var rwSyncUpgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 2048,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

func rwSyncSocketWriter(conn *websocket.Conn, msgChannel chan string) {
	for {
		select {
		case msg := <-msgChannel:
			{
				//fmt.Println(msg)
				if err := conn.WriteMessage(1, []byte(msg)); err != nil {
					doxlog.Errorf("%v", err)
				}
			}
		default:
		}
	}
}
func rwSyncReader(conn *websocket.Conn, buildChannel chan<- string) {
	for {
		messageType, p, err := conn.ReadMessage()

		if err != nil {
			doxlog.Errorf("%v", err)
			return
		}
		log.Println(string(p))

		if err := conn.WriteMessage(messageType, p); err != nil {
			doxlog.Errorf("%v", err)
			return
		}
	}
}

// handleApiHome provides path information in the event that the requester fails
// to provide a library, topic, key or a topic and key.
func (s *server) handleApiHome() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "AddPathToMap /id/{type}/{library}/{topic}/{key} to %s, e.g., id/ltx/gr_gr_cog/actors/Priest to view for specific key.", s.http.Addr)
		fmt.Fprintf(w, "\nor\nAddPathToMap /id/{type}/{topic}/{key} to %s, e.g., id/ltx/actors/Priest to view for all libraries.", s.http.Addr)
		fmt.Fprintf(w, "\nor\nAddPathToMap /topic/{type}/{library}/{topic} to %s, e.g., topic/ltx/gr_gr_cog/actors to view all keys for that library and topic.", s.http.Addr)
	}
}

// dbDirCreate serializes create, update, or delete and commits it to git.
func (s *server) repoCreate(kp *kvs.KeyPath) error {
	if kp == nil {
		return fmt.Errorf("kp is nil")
	}
	if kp.Dirs.Size() < 2 {
		return fmt.Errorf("kp is too small")
	}
	doxlog.Infof("simulating creation of repo for %s", kp.Path())
	if app.Ctx.Cloud {
		return nil
	}
	if !app.Ctx.LocalUserCanUpdateGroup {
		return nil
	}
	// TODO Create local and remote repo
	// TODO Add the url to the projects{project group name}/scriptData/pullUrls.txt file
	switch kp.Dirs.First() {
	case "ltx":
	case "media":
	default:
		return nil
	}

	return nil
}

// backup serializes create, update, or delete and commits it to git.
func (s *server) backup(kp *kvs.KeyPath) error {
	if kp == nil {
		return fmt.Errorf("kp is nil")
	}
	if kp.Dirs.Size() < 2 {
		return fmt.Errorf("kp is too small")
	}
	s.backupMutex.Lock()
	defer s.backupMutex.Unlock()
	backupKp := kvs.NewKeyPath()
	backupDir := app.Ctx.Paths.ResourcesPath
	var backupFile string
	firstType := kp.Dirs.First()
	switch firstType {
	case "config":
		backupKp.Dirs.Push("config")
		backupDir = path.Join(backupDir, "config")
		backupFile = path.Join(backupDir, "config", "config")
	case "ltx":
		if kp.Dirs.Size() < 3 {
			return fmt.Errorf("kp is too small")
		}
		backupKp.Dirs.Push("ltx")
		backupKp.Dirs.Push(kp.Dirs.Get(1)) // library
		backupKp.Dirs.Push(kp.Dirs.Get(2)) // topic
		backupDir = path.Join(backupDir, "ltx", kp.Dirs.Get(1))
		backupFile = path.Join(backupDir, kp.Dirs.Get(2))
	case "media":
		backupKp.Dirs.Push("media")
		backupKp.Dirs.Push(kp.Dirs.Get(1))
		backupDir = path.Join(backupDir, "media")
		backupDir = path.Join(backupDir, kp.Dirs.Get(1))
	}
	backupFile += ".tsv"
	err := s.mapper.Db.Export(backupKp, backupFile, kvs.ExportAsLine)
	if err != nil {
		msg := fmt.Sprintf("error backing up %s: %v", backupKp.Path(), err)
		doxlog.Error(msg)
		return fmt.Errorf(msg)
	}
	if _, ok := app.Ctx.SyncManager.RwSyncer.GetFromMap(backupDir); ok {
		git := dvcs.NewGitClient(backupDir)
		err = git.Commit(backupFile, fmt.Sprintf("doxa auto commit"))
		if err != nil {
			msg := fmt.Sprintf("error commiting tsv file %s: %v", backupFile, err)
			doxlog.Error(msg)
			return fmt.Errorf(msg)
		}
	}
	return nil
}
func (s *server) commitTemplate(tmplPath string) error {
	dirPath := app.Ctx.Paths.PrimaryTemplatesPath
	if app.Ctx.ProjectPushData == nil {
		return errors.New("ProjectPushData ia nil")
	}
	if repoData, ok := app.Ctx.ProjectPushData.FindRepoByRepoPath(dirPath); ok {
		if len(repoData.FsRepoPath) > 0 {
			s.templateMutex.Lock()
			defer s.templateMutex.Unlock()
			git := dvcs.NewGitClient(dirPath)
			err := git.Commit(tmplPath, fmt.Sprintf("doxa auto commit"))
			if err != nil {
				msg := fmt.Sprintf("error commiting tsv file %s: %v", tmplPath, err)
				doxlog.Error(msg)
				return fmt.Errorf(msg)
			}
		}
	}
	return nil
}

// removeTemplate does a git rm and commit.
// tmplPath can be a directory or file in the templates folder
func (s *server) removeTemplate(tmplPath string) error {
	dirPath := app.Ctx.Paths.PrimaryTemplatesPath
	if repoData, ok := app.Ctx.ProjectPushData.FindRepoByRepoPath(dirPath); ok {
		if len(repoData.FsRepoPath) > 0 {
			s.templateMutex.Lock()
			defer s.templateMutex.Unlock()
			git := dvcs.NewGitClient(dirPath)
			err := git.Remove(tmplPath, fmt.Sprintf("doxa auto commit"), false)
			if err != nil {
				msg := fmt.Sprintf("error commiting delete of %s: %v", tmplPath, err)
				doxlog.Error(msg)
				return fmt.Errorf(msg)
			}
		}
	}
	return nil
}

func (s *server) getSessionUsername(w http.ResponseWriter, r *http.Request) (string, error) {
	if !s.cloud {
		return "", nil
	}
	var sessionId string
	cookie, err := r.Cookie(sessionCookie)
	if err != nil {
		msg := fmt.Sprintf("could not retrieve session cookie: %v", err)
		doxlog.Error(msg)
		http.Redirect(w, r, "/login", http.StatusSeeOther)
	}
	if len(cookie.Value) == 0 {
		msg := fmt.Sprintf("len(cookie.Value) == 0")
		doxlog.Error(msg)
		http.Redirect(w, r, "/login", http.StatusSeeOther)
	}
	sessionId = cookie.Value
	if session, ok := s.sessionManager.GetSessionById(sessionId); ok {
		return session.Username, nil
	}
	msg := fmt.Sprintf("could not retrieve session for %s", sessionId)
	doxlog.Error(msg)
	http.Redirect(w, r, "/login", http.StatusSeeOther)
	return "", fmt.Errorf(msg)
}

// handleRegEx returns the result of applying a regular expression to a text
func (s *server) handleRegEx() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonRegExResponse)
		text := r.URL.Query().Get("text")
		if len(text) == 0 {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = "missing text"
			respond(w, http.StatusOK, resp)
			return
		}
		pattern := r.URL.Query().Get("pattern")
		if len(pattern) == 0 {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = "missing pattern"
			respond(w, http.StatusOK, resp)
			return
		}
		resp.Pattern = pattern
		var err error
		var caseInsensitive, diacriticInsensitive, greek, wholeWord bool
		caseInsensitive, err = strconv.ParseBool(r.URL.Query().Get("caseInsensitive"))
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("%s", err)
			respond(w, http.StatusOK, resp)
			return
		}
		diacriticInsensitive, err = strconv.ParseBool(r.URL.Query().Get("diacriticInsensitive"))
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("%s", err)
			respond(w, http.StatusOK, resp)
			return
		}
		if diacriticInsensitive {
			resp.Text = ltstring.ToUnicodeNFD(resp.Text)
		} else {
			resp.Text = ltstring.ToUnicodeNFC(resp.Text)
		}
		greek, err = strconv.ParseBool(r.URL.Query().Get("greek"))
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("%s", err)
			respond(w, http.StatusOK, resp)
			return
		}
		wholeWord, err = strconv.ParseBool(r.URL.Query().Get("wholeWord"))
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("%s", err)
			respond(w, http.StatusOK, resp)
			return
		}

		resp.ExpandedPattern, resp.NFD, err = ltRegEx.ExpandedRegEx(pattern, caseInsensitive, diacriticInsensitive, greek)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("%s", err)
			respond(w, http.StatusOK, resp)
			return
		}
		resp.CaseInsensitive = caseInsensitive
		resp.DiacriticInsensitive = diacriticInsensitive
		resp.WholeWord = wholeWord

		if caseInsensitive {
			resp.GoFlags = ltRegEx.CaseInsensitive
		}
		if wholeWord {
			resp.ExpandedPattern = ltRegEx.WholeWord(resp.ExpandedPattern)
		}

		re, err := regexp.Compile(resp.GoFlags + resp.ExpandedPattern)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("%s", err)
			respond(w, http.StatusOK, resp)
			return
		}
		timeStamp := stamp.NewStamp("load")
		timeStamp.Start()
		indexes := re.FindAllStringIndex(resp.Text, -1)
		resp.Time = timeStamp.FinishNanosecondsNoMessage()

		resp.Count = len(indexes)
		resp.Status = "OK"
		resp.Message = fmt.Sprintf("go: %d matches", resp.Count)
		respond(w, http.StatusOK, resp)
	}
}

// handleUnicode returns the result of converting text to Unicode NFC, NFD, etc.
func (s *server) handleUnicode() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		resp := new(JsonUnicodeResponse)
		text := r.URL.Query().Get("srcText")
		if len(text) == 0 {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = "missing text"
			respond(w, http.StatusOK, resp)
			return
		}
		var useNfc bool
		form := r.URL.Query().Get("inlineRadioOptions")
		if len(form) == 0 {
			useNfc = true
		} else {
			useNfc = form == "nfc"
		}
		resp.Nfc = ltUnicode.ToUnicodeNFC(text)
		resp.NfcLenBytes = len(resp.Nfc)
		resp.Nfd = ltUnicode.ToUnicodeNFD(text)
		resp.NfdLenBytes = len(resp.Nfd)
		resp.NfcNd = ltUnicode.ToNfcNoDiacritics(text)
		resp.NfcNdLenBytes = len(resp.NfcNd)
		resp.NfdNd = ltUnicode.ToNfdNoDiacritics(text)
		resp.NfdNdLenBytes = len(resp.NfdNd)
		resp.NfcNdNpLow = strings.ToLower(ltstring.RemovePunctuation(resp.NfcNd))
		resp.NfcNdNpLowLenBytes = len(resp.NfcNdNpLow)
		resp.NfdNdNpLow = strings.ToLower(ltstring.RemovePunctuation(resp.NfdNd))
		resp.NfdNdNpLowLenBytes = len(resp.NfdNdNpLow)
		if useNfc {
			resp.GlyphMeta = ltUnicode.GlyphsRaw(resp.Nfc)
		} else {
			resp.GlyphMeta = ltUnicode.GlyphsRaw(resp.Nfd)
		}
		s.RenderTemplate(w, "unicodeResults", resp, false)
	}
}

// handleRegEx returns the result of applying a regular expression to a text
func (s *server) handleGitMerge() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		var msg, status string
		var messages []string
		var hadErrors bool
		parm := new(templx.AlertParm)

		err := r.ParseForm()
		if err != nil {
			msg = fmt.Sprintf("failed to parse form: %v", err)
			status = "danger"
			messages = append(messages, msg)
			doxlog.Errorf(msg)
		}
		messages = append(messages, "THIS IS A SIMULATION")

		if err == nil {
			for i := range r.Form {
				localPath := r.Form[i]
				action := r.Form[i]
				if err != nil {
					msg = fmt.Sprintf("error merging %s using action %s: %v", localPath, action, err)
					messages = append(messages, msg)
					doxlog.Errorf(msg)
					hadErrors = true
				} else {
					msg = fmt.Sprintf("merged %s using action %s", localPath, action)
					messages = append(messages, msg)
				}
			}
		}
		if hadErrors {
			status = "danger"
		} else {
			status = "success"
		}
		parm.Status = status
		parm.Messages = messages
		component := templx.Alert(parm)
		err = component.Render(r.Context(), w)
		if err != nil {
			doxlog.Errorf(err.Error())
		}
	}
}

type ResponseInterface interface {
	ToJson() ([]byte, error)
}

func respond(w http.ResponseWriter, statusCode int, resp ResponseInterface) {
	jsonResp, err := resp.ToJson()
	if err != nil {
		doxlog.Infof("Error happened in JSON marshal. Err: %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(nil)
		return
	}
	if doxlog.Level.Level() == slog.LevelDebug {
		doxlog.Debugf(string(jsonResp))
	}
	w.WriteHeader(statusCode)
	w.Write(jsonResp)
}

func (s *server) handleHomeV1() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Greetings from version 1 of the api")
	}
}
func (s *server) handleHomeV2() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Greetings from version 2 of the api")
	}
}

// handleLdpRequest returns the liturgical day properties for the requested date and the result of evaluating an expression
func (s *server) handleLdpRequest() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		var err error
		resp := new(JsonLdpResponse)
		// expression to be evaluated (optional)
		exp := r.URL.Query().Get("exp")
		resp.Exp = exp
		// calendar
		cal := r.URL.Query().Get("cal")
		// default to Gregorian
		var calendarType = calendarTypes.Gregorian
		if strings.ToLower(cal) == "julian" {
			calendarType = calendarTypes.Julian
		}
		resp.Calendar = cal
		// date
		var year, month, day int
		strDate := r.URL.Query().Get("date")
		if len(strDate) == 0 { // set date to today
			now := time.Now()
			if year == 0 {
				year = now.Year()
			}
			if month == 0 {
				month = int(now.Month())
			}
			if day == 0 {
				day = now.Day()
			}
			strDate = fmt.Sprintf("%d-%d-%d", year, month, day)
		} else { // set the date from the parameter
			parts := strings.Split(strDate, "-")
			if len(parts) != 3 {
				resp.Status = "bad date"
				respond(w, http.StatusOK, resp)
				return
			}
			year, err = strconv.Atoi(parts[0])
			if err != nil {
				resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
				resp.Message = fmt.Sprintf("year %s is not an integer", parts[0])
				respond(w, http.StatusOK, resp)
			}
			month, err = strconv.Atoi(parts[1])
			if err != nil {
				resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
				resp.Message = fmt.Sprintf("month %s is not an integer", parts[1])
				respond(w, http.StatusOK, resp)
			}
			day, err = strconv.Atoi(parts[2])
			if err != nil {
				resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
				resp.Message = fmt.Sprintf("day %s is not an integer", parts[2])
				respond(w, http.StatusOK, resp)
			}
		}
		l, err := ldp.NewLDPYMD(year, month, day, calendarType)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusInternalServerError)
			resp.Status = fmt.Sprintf("ldp initialization error: %v", err)
			respond(w, http.StatusOK, resp)
		}
		resp.Date = l.TheDay.Format("2006/01/02")
		resp.Properties = l.LiturgicalPropertiesSummary()
		// evaluate expression if not an empty string
		if len(exp) > 0 {
			ltkResolver := resolver.NewDbResolver(s.mapper, s.keyRing, s.siteBuilder.TemplateMapper, false)
			var model = "gr_gr_cog"
			resp.ExpTrue, resp.ExpParseErrors, err = parser.ExpressionTrue(exp, year, month, day, calendarType, ltkResolver, model)
			if err != nil {
				resp.Status = fmt.Sprintf("%d", http.StatusInternalServerError)
				resp.Message = fmt.Sprintf("expression evaluator initialization error: %v", err)
				respond(w, http.StatusOK, resp)
				return
			}
		}
		// set the triodion and pentecostarion movable cycle day info
		resp.TriodionMcd, resp.PentecostarionMcd, err = s.qm.MCDCommemoration(l.TriodionStartDateThisYear, l.PaschaDateThisYear, []string{"gr_gr_cog", "en_us_dedes"})
		if err != nil {
		}
		resp.Status = "OK"
		resp.Message = "LDP properties calculated"
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleTemplateRead returns the content of the specified LML (template) file

func (s *server) canonicalTemplateDirPath(src string, action fileSystemActions.FileSystemAction) (dirPath string, title string, err error) {
	dirPath, _, err = s.canonicalTemplatePath(src, action)
	if err != nil {
		return dirPath, title, err
	}
	if strings.HasSuffix(dirPath, ".lml") {
		dirPath = dirPath[:len(dirPath)-4]
	}
	switch action {
	case fileSystemActions.Delete, fileSystemActions.Read, fileSystemActions.Rename:
		{
			if !ltfile.DirExists(dirPath) {
				return dirPath, title, fmt.Errorf("%s does not exist or is not a directory", dirPath)
			}
		}
	}
	base := path.Base(dirPath)
	if action == fileSystemActions.Delete && (base == "primary" || base == "fallback") {
		return dirPath, title, fmt.Errorf("delete of primary or fallback directory not allowed")
	}
	return dirPath, title, nil
}
func (s *server) canonicalTemplateFilePath(src string, action fileSystemActions.FileSystemAction) (filePath string, title string, err error) {
	// we reuse the canonicalTemplatePath code, but strip off the .lml to get a directory path
	filePath, _, err = s.canonicalTemplatePath(src, action)
	if err != nil {
		return filePath, title, err
	}
	if !strings.HasSuffix(filePath, ".lml") {
		filePath = filePath + ".lml"
	}
	switch action {
	case fileSystemActions.Delete, fileSystemActions.Read, fileSystemActions.Rename:
		{
			if !ltfile.FileExists(filePath) {
				return filePath, title, fmt.Errorf("%s does not exist or is not a file", filePath)
			}
		}
	}
	title = path.Base(filePath)
	return filePath, title, nil
}
func (s *server) canonicalTemplatePath(src string, action fileSystemActions.FileSystemAction) (filePath string, title string, err error) {
	src, err = url.QueryUnescape(src)
	if err != nil {
		return "", "", fmt.Errorf("invalid template file path %s", src)
	}
	var pathSlice []string
	if strings.HasPrefix(src, "[") {
		err = json.Unmarshal([]byte(src), &pathSlice)
		if err != nil {
			return filePath, title, fmt.Errorf("invalid template file path %s", src)
		}
		if s.siteBuilder.Config.TemplatesFallbackEnabled {
			if pathSlice[0] == "fallback" && !(action == fileSystemActions.Read || action == fileSystemActions.Copy) {
				return filePath, title, fmt.Errorf("only copy or read action allowed against a fallback (subscription) directory")
			}
		}
		title = pathSlice[len(pathSlice)-1]
		filePath = path.Join(pathSlice...)
		filePath = s.siteBuilder.TemplateMapper.ToCanonicalPath(filePath)
	} else {
		return filePath, title, fmt.Errorf("invalid template file path %s", src)
	}
	return filePath, title, nil
}

func handlerData(r *http.Request) string {
	sb := strings.Builder{}
	sb.WriteString(fmt.Sprintf("%s %s", r.Method, r.URL))
	return sb.String()
}

func prefixBlock(tPath, id string) (string, error) {
	splitter := ""
	if strings.Contains(tPath, "Blocks") {
		splitter = fmt.Sprintf("Blocks%s", "/")
	} else if strings.Contains(tPath, "Books") {
		splitter = fmt.Sprintf("Books%s", "/")
	} else if strings.Contains(tPath, "Dated-CompareServices") {
		splitter = fmt.Sprintf("Dated-CompareServices%s", "/")
	} else {
		return "", fmt.Errorf("path %s does not have either Blocks, Books, or Dated-CompareServices as a directory", tPath)
	}
	parts := strings.Split(tPath, splitter)
	if len(parts) != 2 {
		return "", fmt.Errorf("%s could not split using %s", tPath, splitter)
	}
	parts = strings.Split(parts[1], string(os.PathSeparator))
	j := len(parts) - 1
	newId := ""
	for i := 0; i < j; i++ {
		newId = newId + parts[i] + "/"
	}
	newId = newId + id
	return newId, nil
}

func validatePatterns(patterns []string) error {
	var err error
	pMap := make(map[string]string)
	for _, v := range patterns {
		val := strings.TrimSpace(v)
		if _, ok := pMap[val]; ok {
			return fmt.Errorf("duplicate pattern %s not allowed", val)
		} else {
			pMap[val] = val
		}
		// be sure it starts with bk or se.  We only allow book or service templates.  No blocks.
		if !(strings.HasPrefix(val, "bk") || strings.HasPrefix(val, "se") || strings.HasPrefix(val, "(bk|se") || strings.HasPrefix(val, "(se|bk")) {
			return fmt.Errorf("bad template pattern %s: must start with bk or se", val)
		}
		// ensure it is a valid regular expression
		_, err = regexp.Compile(v)
		if err != nil {
			return fmt.Errorf("%s is not a valid regular expression: %v", val, err)
		}
	}
	return nil
}
func (s *server) checkForOverlaps(patterns, indexes []string) error {
	var err error
	tMap := make(map[string]string)
	for _, index := range indexes {
		var i int
		i, err = strconv.Atoi(index)
		if err != nil {
			return fmt.Errorf("index %s not an integer: %v", index, err)
		}
		if i > len(patterns) {
			return fmt.Errorf("index %s exceeds number of patterns", index)
		}
		var p = patterns[i]
		var matches []string
		// TODO: make this a parameter from the user
		osDelimiter := "/"
		dirExclusionPatterns := []string{fmt.Sprintf("%ss-temp%s", osDelimiter, osDelimiter),
			fmt.Sprintf("%ss-tests%s", osDelimiter, osDelimiter),
			fmt.Sprintf("%sa-templates%sMedia_Lists%s", osDelimiter, osDelimiter, osDelimiter)}

		if matches, err = s.siteBuilder.TemplateMapper.GetMatchingIDs([]string{p}, dirExclusionPatterns); err != nil {
			return fmt.Errorf("error matching template IDs to pattern %s: %v", p, err)
		}
		for _, m := range matches {
			if v, ok := tMap[m]; ok {
				// TODO: give a list of all overlapping files
				return fmt.Errorf("templates selected by pattern %s overlap with ones selected by pattern %s, e.g. %s", p, v, m)
			}
			tMap[m] = p
		}
	}
	return nil
}

const patternAllBooks = "bk\\..*"
const patternAllServices = "se\\..*"
const patternAllBooksAndServices = "(bk|se)\\.(.*)"

func validatePatternsAndIndexes(patterns []string, indexes []string) error {
	var allBooksSelected bool
	var allServicesSelected bool
	var allBooksAndServicesSelected bool
	var additionalBookPatternSelected bool
	var additionalServicePatternSelected bool

	if len(patterns) == 0 {
		return fmt.Errorf("there must be at least one pattern")
	}
	if len(indexes) == 0 {
		return fmt.Errorf("at least one pattern must be selected")
	}
	if len(indexes) > len(patterns) {
		return fmt.Errorf("number of selected patterns cannot be greater than the number of patterns")
	}
	for _, index := range indexes {
		i, err := strconv.Atoi(strings.TrimSpace(index))
		if err != nil {
			return fmt.Errorf("invalid index: %s is not an integer", index)
		}
		if i > len(patterns) {
			return fmt.Errorf("index %s is greater than the number of patterns", index)
		}
		switch patterns[i] {
		case patternAllBooks:
			allBooksSelected = true
		case patternAllServices:
			allServicesSelected = true
		case patternAllBooksAndServices:
			allBooksAndServicesSelected = true
		default:
			{
				if strings.HasPrefix(patterns[i], "bk") || strings.HasPrefix(patterns[i], "(bk") {
					additionalBookPatternSelected = true
				}
				if strings.HasPrefix(patterns[i], "se") || strings.HasPrefix(patterns[i], "(se") {
					additionalServicePatternSelected = true
				}

			}
		}
	}
	if allBooksAndServicesSelected && len(indexes) > 1 {
		return fmt.Errorf("if pattern %s is selected, no other patterns may be selected", patternAllBooksAndServices)
	}
	if allBooksSelected && additionalBookPatternSelected {
		return fmt.Errorf("if pattern %s is selected, no other bk patterns may be selected", patternAllBooks)
	}
	if allServicesSelected && additionalServicePatternSelected {
		return fmt.Errorf("if pattern %s is selected, no other se patterns may be selected", patternAllServices)
	}
	return nil
}

// handlePropertiesReloadRequest reloads the properties
func (s *server) handlePropertiesReloadRequest() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		resp := new(JsonStatusResponse)
		resp.Status = "OK"
		resp.Message = "properties reload initiated"
		respond(w, http.StatusOK, resp)
	}
}

func (s *server) handleEnumBooks() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(EnumAbrNames)
		titles := s.settingsManager.StringMapStringProps[properties.SiteBuildPagesTitles]
		entries := books.Desc()
		for _, e := range entries {
			ean := new(EnumAbrName)
			ean.Abr = e.Abr
			ean.Desc = e.Name
			ean.Name = titles[e.Abr]
			if len(ean.Name) == 0 {
				ean.Name = "missing"
			}
			resp.Enums = append(resp.Enums, ean)
		}
		resp.Status = "OK"
		resp.Message = "got enums"
		respond(w, http.StatusOK, resp)
		return
	}
}

func (s *server) logMe(me string) {
	if s.debug {
		doxlog.Infof("rest server: %s", me)
	}
}

func (s *server) requestRoUnsusbcribe(w http.ResponseWriter, r *http.Request) {
	s.logMe("requestRoUnsusbcribe")
	s.appApi.Subscription.Unsubscribe(nil, "")
	w.Header().Set("HX-Refresh", "true")
	w.WriteHeader(205)
}

func (s *server) requestSubscribeBrowseCatalogs(w http.ResponseWriter, r *http.Request) {
	s.logMe("requestSubscribeBrowseCatalogs")
	selection := r.Header.Get("DL-Selected")
	selectionObj := api.SubscribeUISelection{}
	if selection != "" {
		selectionObj.Name = selection
		selectionObj.ListID = r.Header.Get("DL-ID")
	}
	s.RenderTemplate(w, "picoWidgetGeneric", s.appApi.Subscription.GetForm(nil, "", "subscriptionsCatalogBrowserWizard", "", false, selectionObj), true)
}

func (s *server) requestSubscribeBrowseInventories(w http.ResponseWriter, r *http.Request) {
	s.logMe("requestSubscribeBrowseInventories")
	selection := r.Header.Get("DL-Selected")
	selectionObj := api.SubscribeUISelection{}
	if selection != "" {
		selectionObj.Name = selection
		selectionObj.ListID = r.Header.Get("DL-ID")
	}
	s.RenderTemplate(w, "picoWidgetGeneric", s.appApi.Subscription.GetForm(nil, "", "subscriptionsInventoryBrowserWizard", "", true, selectionObj), true)
}

func (s *server) handleSynch() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonStatusResponse)

		Clear(s.roSyncMsgChan)

		timeStamp := stamp.NewStamp("synchronization")
		// s.msgServer.Publish(topic, fmt.Sprintf(timeStamp.Start()))
		s.roSyncMsgChan <- fmt.Sprintf("SM: %s", timeStamp.Start())
		// s.msgServer.Publish(topic, "preparing for synchronization")
		s.roSyncMsgChan <- "SM: preparing for synchronization"
		syncStrategy := r.URL.Query().Get("strategy")
		if len(syncStrategy) == 0 {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			msg := "SM: error - missing synchronization strategy"
			s.roSyncMsgChan <- msg
			resp.Message = msg
			respond(w, http.StatusOK, resp)
			return
		}
		var uid string
		if s.cloud {
			// TODO: get user from session
		} else {
			uid = users.LocalUser
		}
		// s.msgServer.Publish(topic, "getting user profile")
		s.roSyncMsgChan <- "SM: getting user profile"
		// get the user profile
		userProfile, err := s.userManager.GetProfile(uid)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusNotFound)
			msg := fmt.Sprintf("SM: error getting profile for %s: %v", uid, err)
			s.roSyncMsgChan <- msg
			resp.Message = msg
			respond(w, http.StatusOK, resp)
			return
		}
		if userProfile == nil {
			resp.Status = fmt.Sprintf("%d", http.StatusNotFound)
			msg := "SM: user profile has not been created"
			s.roSyncMsgChan <- msg
			resp.Message = msg
			respond(w, http.StatusOK, resp)
			return
		}
		if !userProfile.Gitlab.DoxaHandlesGitlab {
			if syncStrategy != syncStrategyTypes.CodeForType(syncStrategyTypes.Export) {
				resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
				msg := fmt.Sprintf("SM: synchronization strategy %s not allowed if user handles git", syncStrategy)
				s.roSyncMsgChan <- msg
				resp.Message = msg
				respond(w, http.StatusOK, resp)
				return
			}
		}
		jsonArray := r.URL.Query().Get("libraries")
		var libraries []string
		if len(jsonArray) == 0 {
			libraries = append(libraries, "all")
		} else {
			err = json.Unmarshal([]byte(jsonArray), &libraries)
			if err != nil {
				resp.Status = fmt.Sprintf("invalid libraries list: %v", err)
				msg := fmt.Sprintf("SM: invalid libraries list: %v", err)
				s.roSyncMsgChan <- msg
				resp.Message = msg
				respond(w, http.StatusOK, resp)
				return
			}
		}
		// TODO: handle situation where user wants to handle git themselves
		// get the token from the user manager.
		var token string
		token, err = s.userManager.GetToken(uid)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("could not retrieve token from vault: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		s.roSyncMsgChan <- fmt.Sprintf("SM: reloading the configuration settings")
		err = s.settingsManager.ReadConfiguration()
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusInternalServerError)
			resp.Message = fmt.Sprintf("error reloading configuration settings: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		s.roSyncMsgChan <- fmt.Sprintf("SM: finished reloading the configuration settings")

		//		s.msgServer.Publish(topic, "initializing manager for Distributed Version Control System")
		s.roSyncMsgChan <- "SM: initializing manager for Distributed Version Control System"
		var dvcsClient *dvcs.DVCS
		// TODO: the next func call takes many seconds to complete
		// because it creates a new Gitlab client and loads maps of
		// the remote groups and repos.  Figure out how to speed it up.
		dvcsClient, err = dvcs.NewDvcsClientWithGitlab(config.DoxaPaths.ProjectDirPath,
			"",
			token,
			userProfile.Gitlab.GitlabRootGroup.ID,
			"",
			true)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("group ID and/or token invalid: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		//		s.msgServer.Publish(topic, "initializing the Synch LayoutManager")
		s.rwSyncMsgChan <- "SM: initializing the Synch LayoutManager"
		syncher, err := synch.NewSyncher(syncStrategy,
			config.DoxaPaths.HomePath,
			config.DoxaPaths.ResourcesPath,
			config.DoxaPaths.PrimaryTemplatesPath,
			libraries,
			s.mapper,
			dvcsClient,
			s.settingsManager,
			userProfile,
			s.roSyncMsgChan)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("error initializing sync manager: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		// TODO: remove
		syncher.Simulate = true
		if syncher.Simulate {
			//			s.msgServer.Publish(topic, "THIS IS A SIMULATION!!!!")
			s.roSyncMsgChan <- "SM: THIS IS A SIMULATION!!!!"
		}
		//s.msgServers.msgServer.Publish(topic, "starting synchronization")
		s.roSyncMsgChan <- "SM: starting synchronization"

		var writerWg sync.WaitGroup
		writerWg.Add(1)
		go func() {
			defer writerWg.Done()
			err = syncher.Synch()
		}()
		writerWg.Wait()
		//		err = syncher.Export(s.messageChannel)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("error synching database: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		//		s.msgServer.Publish(topic, fmt.Sprintf(timeStamp.FinishMillis()))
		s.roSyncMsgChan <- fmt.Sprintf("SM: %s", timeStamp.FinishMillis())
		resp.Status = "OK"
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleUserProfileGet returns the profile for the specified user
func (s *server) handleUserProfileGet() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonProfileResponse)
		// TODO: get uid from session.  For now we are just supporting the local user
		uid := users.LocalUser
		profile, err := s.userManager.GetProfile(uid)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusNotFound)
			resp.Message = fmt.Sprintf("error getting profile for %s: %v", uid, err)
			respond(w, http.StatusOK, resp)
			return
		}
		resp.Profile = profile
		resp.Status = "OK"
		resp.Message = fmt.Sprintf("profile for %s", uid)
		respond(w, http.StatusOK, resp)
		return
	}
}
