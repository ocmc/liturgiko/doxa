package servers

import (
	"github.com/gorilla/mux"
	"net/http"
)

func (s *server) pageRoutes(router *mux.Router) {
	router.HandleFunc("/", s.handleHome()).Methods(http.MethodGet)
	router.HandleFunc("/about", s.serveAboutPage()).Methods(http.MethodGet)
	router.HandleFunc("/backup", s.serveBackupPage()).Methods(http.MethodGet)
	router.HandleFunc("/css", s.serveCssEditorPage("html")).Methods(http.MethodGet)
	router.HandleFunc("/db", s.serveDbExplorerPage()).Methods(http.MethodGet)
	router.HandleFunc("/generate", s.serveGeneratorPage()).Methods(http.MethodGet)
	router.HandleFunc("/ide", s.serveIdePage()).Methods(http.MethodGet)
	router.HandleFunc("/ideTools", s.serveIdeToolsPage()).Methods(http.MethodGet)
	router.HandleFunc("/import", s.serveDbImportPage()).Methods(http.MethodGet)
	router.HandleFunc("/js", s.serveJsEditorPage()).Methods(http.MethodGet)
	router.HandleFunc("/layout", s.serveLayoutPage()).Methods(http.MethodGet)
	router.HandleFunc("/ldp", s.serveLdpPage()).Methods(http.MethodGet)
	router.HandleFunc("/lml", s.serveLmlEditorPage()).Methods(http.MethodGet)
	router.HandleFunc("/login", s.serveLoginPage()).Methods(http.MethodGet)
	router.HandleFunc("/logs", s.serveLogsPage()).Methods(http.MethodGet)
	router.HandleFunc("/media", s.serveMediaEditorPage()).Methods(http.MethodGet)
	router.HandleFunc("/merge", s.serveMergePage()).Methods(http.MethodGet)
	router.HandleFunc("/oauth2/callback", s.serveOauthCallback()).Methods(http.MethodGet)
	router.HandleFunc("/pdfcss", s.serveCssEditorPage("pdf")).Methods(http.MethodGet)
	router.HandleFunc("/profile", s.serveUserProfilePage()).Methods(http.MethodGet)
	router.HandleFunc("/publish", s.servePublicationPage()).Methods(http.MethodGet)
	router.HandleFunc("/regex", s.serveRegExPage()).Methods(http.MethodGet)
	router.HandleFunc("/settings", s.serveSettingsPage()).Methods(http.MethodGet)
	router.HandleFunc("/sse", s.sseEndpoint()).Methods(http.MethodGet)
	router.HandleFunc("/siteCmp", s.serveSiteComparisonPage()).Methods(http.MethodGet)
	router.HandleFunc("/synch", s.serveSynchManagerPage()).Methods(http.MethodGet)
	router.HandleFunc("/template", s.serveTemplateExplorerPage()).Methods(http.MethodGet)
	router.HandleFunc("/trace", s.serveDbIdTracePage()).Methods(http.MethodGet)
	router.HandleFunc("/unicode", s.serveUnicodePage()).Methods(http.MethodGet)
	router.HandleFunc("/ws/generate", s.genWsEndpoint)
	router.HandleFunc("/ws/publish", s.publishWsEndpoint)
	router.HandleFunc("/ws/roSync", s.roSyncWsEndpoint)
	router.HandleFunc("/ws/rwSync", s.rwSyncWsEndpoint)

}
