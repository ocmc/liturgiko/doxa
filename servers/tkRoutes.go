package servers

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"html/template"
	"net/http"
	"sync"
)

// TODO: prefix with db
func (s *server) tkRoutes(router *mux.Router) {
	router.HandleFunc("/cite/{type}/{library}/{topic}/{key}", s.handleCite("templates/webApp/table.gohtml")).Methods(http.MethodGet)
	router.HandleFunc("/cmp/{type}/{topic}/{key}", s.handleTK("templates/webApp/table.gohtml")).Methods(http.MethodGet)
	router.HandleFunc("/cmp/{type}/{topic}/{key}", s.handleTK("templates/webApp/table.gohtml")).Queries("empty", "{empty}").Methods(http.MethodGet)
	router.HandleFunc("/id/{type}/{library}/{topic}/{key}", s.handleID()).Methods(http.MethodGet)
	router.HandleFunc("/id/{type}/{topic}/{key}", s.handleTK()).Methods(http.MethodGet)
	router.HandleFunc("/id/{type}/{topic}/{key}", s.handleTK()).Queries("empty", "{empty}").Methods(http.MethodGet)
	router.HandleFunc("/topic/{type}/{library}/{topic}", s.handleTopic("templates/webApp/table.gohtml")).Methods(http.MethodGet)
	router.HandleFunc("/topic/{type}/{library}/{topic}", s.handleTopic("templates/webApp/table.gohtml")).Queries("empty", "{empty}").Methods(http.MethodGet)
}

// handleID returns the liturgical text that matches the requested library, topic, and key.
func (s *server) handleID() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json; charset=utf-8")

		vars := mux.Vars(r)

		kp := kvs.NewKeyPath()
		kp.Dirs.Push(vars["type"])
		kp.Dirs.Push(vars["library"])
		kp.Dirs.Push(vars["topic"])
		kp.KeyParts.Push(vars["key"])

		resp := new(JsonKeyValueResponse)
		rec, err := s.mapper.Db.Get(kp)
		if err == nil {
			w.WriteHeader(http.StatusOK)
			var recs []*KV
			recs = append(recs, &KV{rec.KP.Path(), rec.Value})
			resp.Values = recs
			jsonResp, err := json.Marshal(resp)
			if err != nil {
				doxlog.Infof("JSON marshal Error: %s", err)
				return
			}
			w.Write(jsonResp)
			return
		} else {
			w.WriteHeader(http.StatusNotFound)
			jsonResp, err := json.Marshal(resp)
			if err != nil {
				doxlog.Infof("Error happened in JSON marshal. Err: %s", err)
				return
			}
			w.Write(jsonResp)
			return
		}
	}
}

// handleCite returns the liturgical text that matches the requested library, topic, and key.
func (s *server) handleCite(files ...string) http.HandlerFunc {
	var (
		init sync.Once
		tpl  *template.Template
		err  error
	)
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")

		init.Do(func() { // one time only, parse the template files
			tpl, err = template.ParseFiles(files...)
		})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		vars := mux.Vars(r)

		kp := kvs.NewKeyPath()
		kp.Dirs.Push(vars["type"])
		kp.Dirs.Push(vars["library"])
		kp.Dirs.Push(vars["topic"])
		kp.KeyParts.Push(vars["key"])

		rec, err := s.mapper.Db.Get(kp)
		if err == nil {
			var recs []*KV
			recs = append(recs, &KV{rec.KP.Path(), rec.Value})
			tpl.Execute(w, recs)
		} else {
			doxlog.Errorf("%v", err)
			fmt.Fprintf(w, "%s", "Not found")
		}
	}
}

// TKHandler returns liturgical texts for all libraries that have the requested topic and key.
// If the query ?empty=true is set, then it will include records with empty values.
func (s *server) handleTK(files ...string) http.HandlerFunc {
	var (
		init sync.Once
		tpl  *template.Template
		err  error
	)
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		vars := mux.Vars(r)
		init.Do(func() { // one time only, parse the template files
			tpl, err = template.ParseFiles(files...)
		})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		matcher := kvs.NewMatcher()
		matcher.KP.Dirs.Push(vars["type"])
		matcher.KP.Dirs.Push(vars["topic"])
		matcher.KP.KeyParts.Push(vars["key"])
		matcher = matcher.TopicKeyMatcher()
		dbrs, _, err := s.mapper.Db.GetMatching(*matcher)
		var recs []*KV
		for _, r := range dbrs {
			recs = append(recs, &KV{r.KP.Path(), r.Value})
		}
		if err == nil {
			tpl.Execute(w, recs)
		} else {
			doxlog.Errorf("%v", err)
			fmt.Fprintf(w, "%s", "Not found")
		}
	}
}

// TopicHandler returns liturgical texts for the requested library and topic.
func (s *server) handleTopic(files ...string) http.HandlerFunc {
	var (
		init sync.Once
		tpl  *template.Template
		err  error
	)
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")
		vars := mux.Vars(r)
		init.Do(func() { // one time only, parse the template files
			tpl, err = template.ParseFiles(files...)
		})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		matcher := kvs.NewMatcher()
		matcher.KP.Dirs.Push(vars["type"])
		matcher.KP.Dirs.Push(vars["library"])
		matcher.KP.Dirs.Push(vars["topic"])
		dbrs, _, err := s.mapper.Db.GetMatching(*matcher)
		var recs []*KV
		for _, r := range dbrs {
			recs = append(recs, &KV{r.KP.Path(), r.Value})
		}
		if err == nil {
			tpl.Execute(w, recs)
		} else {
			doxlog.Errorf("%v", err)
			fmt.Fprintf(w, "%s", "Not found")
		}
	}
}
