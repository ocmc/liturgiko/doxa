package servers

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/liturgiko/doxa/pkg/ages/goarchdcs"
	"github.com/liturgiko/doxa/pkg/atempl"
	"github.com/liturgiko/doxa/pkg/enums/properties"
	"github.com/liturgiko/doxa/pkg/layouts"
	"github.com/liturgiko/doxa/pkg/site"
	"net/http"
	"strconv"
	"time"
)

func (s *server) siteRoutes(router *mux.Router) {
	router.HandleFunc("/site/cmp", s.handleSiteCmp()).Methods(http.MethodGet)
	router.HandleFunc("/site/tk", s.handleSiteTk()).Methods(http.MethodGet)
}

// handleSiteCmp returns a report that compares the contents of two dcs websites
func (s *server) handleSiteCmp() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonStatusResponse)
		url1 := r.URL.Query().Get("url1")
		if len(url1) == 0 {
			resp.Status = "BadRequest"
			resp.Message = "missing URL for site 1"
			respond(w, http.StatusOK, resp)
			return
		}
		url2 := r.URL.Query().Get("url2")
		if len(url2) == 0 {
			resp.Status = "BadRequest"
			resp.Message = "missing URL for site 2"
			respond(w, http.StatusOK, resp)
			return
		}
		htmlPathRegEx := r.URL.Query().Get("regex")
		if len(htmlPathRegEx) == 0 {
			htmlPathRegEx = ".*"
		}
		var intersect bool
		var err error
		if intersect, err = strconv.ParseBool(r.URL.Query().Get("intersect")); err != nil {
			resp.Status = "BadRequest"
			resp.Message = fmt.Sprintf("intersect bool error: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		var siteComparer *site.Comparer
		if siteComparer, err = site.NewComparer(url1, url2, htmlPathRegEx, intersect); err != nil {
			resp.Status = "BadRequest"
			resp.Message = fmt.Sprintf("%v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		if err = siteComparer.CompareServices(); err != nil {
			resp.Status = "BadRequest"
			resp.Message = fmt.Sprintf("%v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		resp.Message = fmt.Sprintf("site %s compared to %s", url1, url2)
		resp.Status = "OK"
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleSiteTk returns a report of topic-keys used in dcs website
func (s *server) handleSiteTk() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonStatusResponse)
		var err error
		var acronyms = []string{"es", "en", "gr"}
		var theLayouts = make(map[string]*atempl.TableLayout)
		var layout *atempl.TableLayout
		var genLangs []*atempl.TableLayout
		for _, acronym := range acronyms {
			layout, err = layouts.Manager.GetSiteBuildLayout(acronym)
			if err != nil {
				resp.Message = fmt.Sprintf("no layout found for %s: %v", acronym, err)
				resp.Status = "OK"
				respond(w, http.StatusOK, resp)
				return
			}
			theLayouts[acronym] = layout
			genLangs = append(genLangs, layout)
		}
		//source := "http://127.0.0.1:8081"
		source := "https://dcs.goarch.org/goa/dcs/"
		var scraper *goarchdcs.Scraper
		// 2006-01-02
		var fromDateStr, toDateStr string
		//fromDateStr = "2021-05-01"
		//toDateStr = "2021-06-28"
		fromDateStr = "2023-01-01"
		toDateStr = "2023-08-31"
		var fromDate, toDate time.Time
		fromDate, err = time.Parse("2006-01-02", fromDateStr)
		if err != nil {
			resp.Message = fmt.Sprintf("error parsing from date %s: %v", fromDate, err)
			resp.Status = "OK"
			respond(w, http.StatusOK, resp)
			return
		}
		toDate, err = time.Parse("2006-01-02", toDateStr)
		if err != nil {
			resp.Message = fmt.Sprintf("error parsing from date %s: %v", fromDate, err)
			resp.Status = "OK"
			respond(w, http.StatusOK, resp)
			return
		}
		titles := s.settingsManager.StringMapStringProps[properties.SiteBuildPagesTitles]
		var serviceTypes = []string{"li"}

		scraper, err = goarchdcs.NewScraper(source, "servicesindex.json", s.siteBuilder.Resolver, theLayouts, genLangs, fromDate, toDate, titles, serviceTypes)
		if err != nil {
			resp.Message = fmt.Sprintf("error creating scraper for %s: %v", source, err)
			resp.Status = "OK"
			respond(w, http.StatusOK, resp)
			return
		}
		err = scraper.GetMetaTemplates()
		if err != nil {
			resp.Message = fmt.Sprintf("error creating scraper for %s: %v", source, err)
			resp.Status = "OK"
			respond(w, http.StatusOK, resp)
			return
		}
		pathOut := "/Users/mac002/git/liturgiko/doxa/pkg/ages/goarchdcs/comingSoon.tsv"
		err = scraper.SerializeToTabSeparatedValues(pathOut)
		if err != nil {
			resp.Message = fmt.Sprintf("error serializing scraper results for %s: %v", source, err)
			resp.Status = "OK"
			respond(w, http.StatusOK, resp)
		}
		resp.Message = fmt.Sprintf("site %s data serialized to %s", source, pathOut)
		resp.Status = "OK"
		respond(w, http.StatusOK, resp)
		return
	}
}
