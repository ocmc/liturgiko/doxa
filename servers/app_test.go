package servers

import "testing"

func TestGetTemplateContentsForBook(t *testing.T) {
	templateId := "someone/somewhere/doxa/project/someProject/templates/someFolder/bk.me.m01.d30.lml"
	content, err := getTemplateContents(templateId)
	if err != nil {
		t.Error(err)
	}
	if len(content) == 0 {
		t.Errorf("no content")
	}
}
func TestGetTemplateContentsForService(t *testing.T) {
	templateId := "someone/somewhere/doxa/project/someProject/templates/someFolder/se.m01.d30.li.lml"
	content, err := getTemplateContents(templateId)
	if err != nil {
		t.Error(err)
	}
	if len(content) == 0 {
		t.Errorf("no content")
	}
}
