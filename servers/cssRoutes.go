package servers

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"net/http"
	"path"
	"strings"
)

func (s *server) cssRoutes(router *mux.Router) {
	router.HandleFunc("/css/read", s.handleCssRead()).Methods(http.MethodGet)
	router.HandleFunc("/css/update", s.handleCssUpdate()).Methods(http.MethodPut)
}

// handleCssRead returns the content of the site app.css file
func (s *server) handleCssRead() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonSrcCodeResponse)
		src := r.URL.Query().Get("src")
		if len(src) == 0 {
			resp.Status = "missing path"
			respond(w, http.StatusBadRequest, resp)
			return
		}
		var filename string
		if strings.HasSuffix(src, "app.css") {
			resp.Title = "HTML CSS"
			filename = path.Join(config.DoxaPaths.AssetsPath, "css", "app.css")
		} else if strings.HasSuffix(src, "pdf.css") {
			resp.Title = "PDF CSS"
			filename = path.Join(config.DoxaPaths.AssetsPath, "pdf", "css", "pdf.css")
		} else {
			resp.Status = "unknown css file"
			respond(w, http.StatusBadRequest, resp)
			return
		}
		var err error
		resp.Value, err = ltfile.GetFileContent(filename)
		if err != nil {
			resp.Status = "could not read css file"
			respond(w, http.StatusBadRequest, resp)
		}
		resp.ID = filename
		resp.FilePath = filename
		resp.Status = "OK"
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleCssUpdate updates the content of the site app.css file
func (s *server) handleCssUpdate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonCssUpdateResponse)
		src := r.URL.Query().Get("src")
		if len(src) == 0 {
			resp.Status = "error"
			resp.Message = "missing path"
			respond(w, http.StatusOK, resp)
			return
		}
		text := r.URL.Query().Get("text")
		if len(text) == 0 {
			resp.Status = "error"
			resp.Message = "missing text"
			respond(w, http.StatusOK, resp)
			return
		}
		fileSource := path.Join(config.DoxaPaths.AssetsPath, src)
		doxlog.Infof(fileSource)
		err := s.siteBuilder.WriteCssFile(fileSource, text)
		if err != nil {
			resp.Status = "error"
			resp.Message = fmt.Sprintf("%s: %v", fileSource, err)
		} else {
			resp.Status = "OK"
			resp.Message = "updated"
		}
		resp.ID = src
		respond(w, http.StatusOK, resp)
		return
	}
}
