package servers

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/dvcs"
	"github.com/liturgiko/doxa/pkg/enums/syncStrategyTypes"
	"github.com/liturgiko/doxa/pkg/synch"
	"github.com/liturgiko/doxa/pkg/users"
	"net/http"
	"strconv"
	"strings"
)

func (s *server) dvcsRoutes(router *mux.Router) {
	router.HandleFunc("/dvcs/credentials/validate", s.handleDvcsCredentialsValidate()).Methods(http.MethodGet)
	router.HandleFunc("/dvcs/status", s.handleDvcsStatus()).Methods(http.MethodGet)
	router.HandleFunc("/dvcs/clear", s.handleDvcsStatus()).Methods(http.MethodGet)
}

// handleDvcsCredentialsValidate verifies the supplied gitlab group id and personal access token are valid.
// If they are, it returns gitlab group information (see DvcsGroupData).
func (s *server) handleDvcsCredentialsValidate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(GitlabGroupResponse)
		//var uid string
		if s.cloud {
			// TODO: get user from session
		} else {
			//uid = users.LocalUser
		}
		// get the user profile
		//userProfile, err := s.userManager.GetProfile(uid)
		//if err != nil {
		//	resp.Status = fmt.Sprintf("%d", http.StatusNotFound)
		//	resp.Message = fmt.Sprintf("error getting profile for %s: %v", uid, err)
		//	respond(w, http.StatusOK, resp)
		//	return
		//}

		groupIdStr := r.URL.Query().Get("groupId")
		groupIdStr = strings.TrimSpace(groupIdStr)
		if len(groupIdStr) == 0 {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = "missing required parameter: groupId"
			respond(w, http.StatusOK, resp)
			return
		}
		groupId, err := strconv.Atoi(groupIdStr)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("error converting groupId from string to integer: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		token := r.URL.Query().Get("token")
		token = strings.TrimSpace(token)
		if len(token) == 0 {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = "missing required parameter: personal access token"
			respond(w, http.StatusOK, resp)
			return
		}
		client, err := dvcs.NewGitlabClient(token,
			groupId,
			config.DoxaPaths.ProjectDirPath,
			"", false,
		)
		if err != nil {
			var msg string
			if strings.Contains(err.Error(), "401") {
				msg = "unauthorized: invalid token"
			} else if strings.Contains(err.Error(), "403") {
				msg = fmt.Sprintf("you are not authorized to access group %d", groupId)
			} else if strings.Contains(err.Error(), "404") {
				msg = "group not found"
			} else {
				msg = fmt.Sprintf("%v", err)
			}
			resp.Status = fmt.Sprintf("%d", http.StatusUnauthorized)
			resp.Message = msg
			respond(w, http.StatusOK, resp)
			return
		}
		resp.GitlabRootGroup = client.RemoteRootGroup
		resp.Status = "OK"
		respond(w, http.StatusOK, resp)
		return
	}
}

// handleDvcsStatus returns the status of all local git repositories managed by Doxa.
// This includes both subscriptions and repositories within the user's Gitlab Group.
func (s *server) handleDvcsStatus() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(RepositoryStatuses)
		var uid string
		if s.cloud {
			// TODO: get user from session
		} else {
			uid = users.LocalUser
		}
		// get the user profile
		userProfile, err := s.userManager.GetProfile(uid)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusNotFound)
			resp.Message = fmt.Sprintf("error getting profile for %s: %v", uid, err)
			respond(w, http.StatusOK, resp)
			return
		}
		jsonArray := r.URL.Query().Get("libraries")
		var libraries []string
		if len(jsonArray) == 0 {
			libraries = append(libraries, "all")
		} else {
			err = json.Unmarshal([]byte(jsonArray), &libraries)
			if err != nil {
				resp.Status = fmt.Sprintf("invalid libraries list: %v", err)
				respond(w, http.StatusOK, resp)
				return
			}
		}
		// TODO: handle situation where user wants to handle git themselves
		// get the token from the user manager.
		var token string
		token, err = s.userManager.GetToken(uid)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("could not retrieve token from vault: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		var dvcsClient *dvcs.DVCS
		dvcsClient, err = dvcs.NewDvcsClientWithGitlab(config.DoxaPaths.ProjectDirPath,
			"",
			token,
			userProfile.Gitlab.GitlabRootGroup.ID, "", true)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("group ID and/or token invalid: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		syncher, err := synch.NewSyncher(syncStrategyTypes.CodeForType(syncStrategyTypes.Status),
			config.DoxaPaths.HomePath,
			config.DoxaPaths.ResourcesPath,
			config.DoxaPaths.PrimaryTemplatesPath,
			libraries,
			s.mapper,
			dvcsClient,
			s.settingsManager,
			userProfile,
			nil)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("could not initialize sync manager: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		repos, err := syncher.RepoDirs(config.DoxaPaths.ResourcesPath)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("could not get status of repositories: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		resp.Repos = repos
		resp.Status = "OK"
		respond(w, http.StatusOK, resp)
		return
	}
}
