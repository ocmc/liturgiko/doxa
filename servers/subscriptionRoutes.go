package servers

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/liturgiko/doxa/api"
	"github.com/liturgiko/doxa/pkg/doxlog"
	picoWidget "github.com/liturgiko/doxa/templates/webApp/structs/widget"
	"html/template"
	"net/http"
)

func (s *server) subscriptionRoutes(router *mux.Router) {

	s.router.HandleFunc("/subscriptionManager", s.serveSubscriptionManagerPage()).Methods(http.MethodGet)

	router.HandleFunc("/htmx/subscriptions/subscribe/{mode}/{catalog}/{path:.+}", s.handleSelectSubscribe).Methods(http.MethodPost)
	router.HandleFunc("/htmx/subscriptions/unsubscribe/{mode}/{catalog}/{path:.+}", s.handleSelectUnsubscribe).Methods(http.MethodPost)
	router.HandleFunc("/htmx/subscriptions/add-catalog", s.handleAddCatalogInventory).Methods(http.MethodPost)
	router.HandleFunc("/htmx/subscriptions/delete/{catalog}", s.handleDeleteCatalogInventory).Methods(http.MethodDelete)
	router.HandleFunc("/htmx/subscriptions/info/{catalog}", s.requestSubscriptionsInfo).Methods(http.MethodGet)
	router.HandleFunc("/htmx/subscriptions/groups", s.requestSubscriptionGroups).Methods(http.MethodGet)
	router.HandleFunc("/htmx/subscriptions/groups/{name}", s.requestSubscriptionGroupDetails).Methods(http.MethodGet)
	router.HandleFunc("/htmx/subscriptions/pollRefreshProgress", s.requestRefreshProgress).Methods(http.MethodGet)
	router.HandleFunc("/htmx/subscriptions/projects", s.requestSubscriptionProjects).Methods(http.MethodGet)
	router.HandleFunc("/htmx/subscriptions/projects/{catalog}/{stability}/{name:.+}", s.requestSubscriptionProjectDetails).Methods(http.MethodGet)
	router.HandleFunc("/htmx/subscriptions/refreshCatalogs", s.requestRefreshCatalogs).Methods(http.MethodPost)
	router.HandleFunc("/htmx/subscriptions/updateFallback", s.requestRebuildFallback).Methods(http.MethodPost)
	router.HandleFunc("/htmx/subscriptions/templateFallbackPriority", s.requestTemplateFallbackPriority).Methods(http.MethodGet)
	router.HandleFunc("/htmx/subscriptions/templateFallbackPriority", s.handleTemplateFallbackPriorityUpdate).Methods(http.MethodPut)
	router.HandleFunc("/htmx/subscriptions/templateFallbackPriority/{node}", s.getFallbackPriorityStatus).Methods(http.MethodGet)
	router.HandleFunc("/htmx/subscriptions/templateFallbackPriority/{node}", s.setFallbackPriorityStatus).Methods(http.MethodPost)

	// Legacy subscription endpoints
	router.HandleFunc("/subs/catalogs/custom", s.handleAddCustomCatalog()).Methods(http.MethodPost)
	router.HandleFunc("/subs/catalogs", s.handleListCatalogs()).Methods(http.MethodGet)
	router.HandleFunc("/subs/catalogs/custom/{id}", s.handleRemoveCustomCatalog()).Methods(http.MethodDelete)
	router.HandleFunc("/subs/info", s.handleGetInfo()).Methods(http.MethodGet)
	router.HandleFunc("/subs/versions", s.handleListVersions()).Methods(http.MethodGet)
	router.HandleFunc("/subs/versions/{id}/revert", s.handleRevertVersion()).Methods(http.MethodPost)
	router.HandleFunc("/subs/catalogs/{id}", s.handleSubscribe()).Methods(http.MethodPost)
	router.HandleFunc("/subs", s.handleUnsubscribe()).Methods(http.MethodDelete)
}

func (s *server) serveSubscriptionManagerPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")

		data := NewIndexHtmlData(
			r.URL.Path,
			"Manage Subscriptions",
			s.getNavbar(w, r),
		)
		data.Body.Main = picoWidget.NewMain(
			"Manage Subscriptions",
			"Update your subscription... or unsubscribe from it",
		)
		section := new(picoWidget.Section)
		grid := new(picoWidget.Grid)
		//roForm := s.appApi.RoSync.GetForm(nil, "api/htmx/ro", "ro", "")
		//grid.AddWidget(roForm)
		selectionObj := api.SubscribeUISelection{}
		groupLevel := false
		if cata := r.URL.Query().Get("group"); cata != "" {
			groupLevel = true
			selectionObj = api.SubscribeUISelection{
				Name:   cata,
				ListID: api.SSUICatalogListId,
			}
		}
		catWdgt := s.appApi.Subscription.GetForm(nil, "", "subscriptionsCatalogBrowserWizard", "", false, selectionObj)
		catWdgt.Class = "container doxa-widget-wide"
		catWdgt.ShowGroups = !groupLevel
		catWdgt.ShowProjects = groupLevel
		grid.AddWidget(catWdgt)
		section.AddGrid(grid)
		grid = new(picoWidget.Grid)
		data.Body.Main.AddSection(section)
		s.RenderTemplate(w, "index", data, false)
	}
}

func (s *server) handleSelectSubscribe(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	mode := vars["mode"] // stable or unstable
	catalog := vars["catalog"]
	path := vars["path"]

	isInventory := mode == "unstable"

	content := api.CatalogSelectionContents{
		Name: catalog,
		Contents: []struct {
			Name   string
			Action api.SubscriptionSelectionAction
		}{
			{
				Name:   path,
				Action: api.SelectSubscribe,
			},
		},
	}

	selData := api.SubscriptionSelectionData{}
	if isInventory {
		selData.Inventories = []api.CatalogSelectionContents{content}
	} else {
		selData.Catalogs = []api.CatalogSelectionContents{content}
	}

	s.appApi.Subscription.ChangeSubscription(r.Context(), "", selData)
	sm := s.appApi.Subscription.GetForm(nil, "", "subscription-content", "", false, api.SubscribeUISelection{})
	sm.ShowGroups = false
	sm.ShowProjects = true

	s.RenderTemplate(w, "subscriptionsManager", sm, true)
}

func (s *server) handleSelectUnsubscribe(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	mode := vars["mode"] // stable or unstable
	catalog := vars["catalog"]
	path := vars["path"]

	isInventory := mode == "unstable"

	content := api.CatalogSelectionContents{
		Name: catalog,
		Contents: []struct {
			Name   string
			Action api.SubscriptionSelectionAction
		}{
			{
				Name:   path,
				Action: api.SelectUnsubscribe,
			},
		},
	}

	selData := api.SubscriptionSelectionData{}
	if isInventory {
		selData.Inventories = []api.CatalogSelectionContents{content}
	} else {
		selData.Catalogs = []api.CatalogSelectionContents{content}
	}

	s.appApi.Subscription.ChangeSubscription(r.Context(), "", selData)
	sm := s.appApi.Subscription.GetForm(nil, "", "subscription-content", "", false, api.SubscribeUISelection{})
	sm.ShowGroups = false
	sm.ShowProjects = true

	s.RenderTemplate(w, "subscriptionsManager", sm, true)
}

func (s *server) handleAddCatalogInventory(w http.ResponseWriter, r *http.Request) {
	s.logMe("handleAddCatalogInventory")

	// Parse the form data first
	err := r.ParseForm()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	catUrl := r.Form.Get("URL")
	if catUrl == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	_, catStat, catErr := s.appApi.Subscription.AddCustomCatalog(nil, "", catUrl)
	_, invStat, invErr := s.appApi.Subscription.AddCustomInventory(nil, "", catUrl)
	if invErr != nil && catErr != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, catStat.RequestMessages)
		fmt.Fprint(w, invStat.RequestMessages)
		return
	}
	sm := s.appApi.Subscription.GetForm(nil, "", "subscription-content", "", false, api.SubscribeUISelection{})
	sm.ShowGroups = true
	sm.ShowProjects = false

	s.RenderTemplate(w, "subscriptionsManager", sm, true)
}

func (s *server) handleDeleteCatalogInventory(w http.ResponseWriter, r *http.Request) {
	s.logMe("handleDeleteCatalogInventory")
	vars := mux.Vars(r)
	catalog := vars["catalog"]
	if catalog == "" {
		w.WriteHeader(404)
		return
	}
	resp, err := s.appApi.Subscription.RemoveCustomCatalog(nil, "", catalog)
	resp2, err2 := s.appApi.Subscription.RemoveCustomInventory(nil, "", catalog)
	if err != nil && err2 != nil {
		metaMsg := make([]string, len(resp.RequestMessages)+len(resp2.RequestMessages))
		copy(metaMsg, resp.RequestMessages)
		metaMsg = append(metaMsg, resp2.RequestMessages...)
		w.WriteHeader(500)
		for _, msg := range metaMsg {
			fmt.Fprint(w, msg)
		}
	}
	w.Header().Set("HX-Refresh", "true")
	w.WriteHeader(205)
}

func (s *server) requestSubscriptionsInfo(w http.ResponseWriter, r *http.Request) {
	s.logMe("requestSubscriptionsInfo")
	vars := mux.Vars(r)
	catalog := vars["catalog"]
	//first of all, we look at the headers to see what list is updated
	// if we don't have a DL-ID we should return a
	elem := s.appApi.Subscription.GetCatalogInfo(nil, "", catalog, "", true)
	if elem == nil {
		w.WriteHeader(404)
		return
	}
	s.RenderTemplate(w, "Element", elem, true)
}

func (s *server) requestSubscriptionGroups(w http.ResponseWriter, r *http.Request) {
	sm := s.appApi.Subscription.GetForm(nil, "", "subscription-content", "", false, api.SubscribeUISelection{})
	sm.ShowGroups = true
	sm.ShowProjects = false

	s.RenderTemplate(w, "subscriptionsManager", sm, true)
}

func (s *server) requestSubscriptionGroupDetails(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	groupName := vars["name"]
	ctx := r.Context()

	sm := s.appApi.Subscription.GetForm(ctx, "", "subscription-content", "", false, api.SubscribeUISelection{
		Name:   groupName,
		ListID: api.SSUICatalogListId,
	})

	s.RenderTemplate(w, "subscriptionsManager", sm, true)
}

func (s *server) requestSubscriptionProjects(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	// Get currently selected catalog/group
	catalogName, isInventory := s.appApi.Subscription.GetSelectedUICatalog(ctx, "")
	if catalogName == "" {
		http.Error(w, "No catalog selected", http.StatusBadRequest)
		return
	}

	sm := s.appApi.Subscription.GetForm(ctx, "", "subscription-content", "", isInventory, api.SubscribeUISelection{
		Name:   catalogName,
		ListID: api.SSUIEntryListID,
	})

	s.RenderTemplate(w, "subscriptionsManager", sm, true)
}

func (s *server) requestSubscriptionProjectDetails(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	projectName := vars["name"]
	catalogName := vars["catalog"]
	isInventory := vars["stability"] == "unstable"
	ctx := r.Context()

	// Get currently selected catalog/group
	// catalogName, isInventory := s.appApi.Subscription.GetSelectedUICatalog(ctx, "")
	if catalogName == "" {
		http.Error(w, "No catalog selected", http.StatusBadRequest)
		return
	}

	// Get project details
	var element picoWidget.Element
	if catalogName != "" {
		element = s.appApi.Subscription.GetEntryInfo(ctx, "", catalogName, projectName, "subscription-content",
			isInventory)
	} else {
		element = s.appApi.Subscription.GetCatalogInfo(ctx, "", projectName, "subscription-content", isInventory)
		if element == nil {
			w.WriteHeader(404)
			return
		}
	}

	s.RenderTemplate(w, "Element", element, true)
}

func (s *server) requestRebuildFallback(w http.ResponseWriter, r *http.Request) {
	s.logMe("requestRebuildFallback")
	s.appApi.Subscription.SetFallbackTemplates(false)
}

func (s *server) handleAddCustomCatalog() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonStatusResponse)
		resp.Status = "OK"
		resp.Message = "added custom catalog"
		respond(w, http.StatusOK, resp)
	}
}

func (s *server) handleListCatalogs() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonStatusResponse)
		resp.Status = "OK"
		resp.Message = "here is some info"
		respond(w, http.StatusOK, resp)
	}
}

func (s *server) handleRemoveCustomCatalog() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonStatusResponse)
		resp.Status = "OK"
		resp.Message = "added custom catalog"
		respond(w, http.StatusOK, resp)
	}
}

func (s *server) handleGetInfo() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonStatusResponse)
		resp.Status = "OK"
		resp.Message = "here is some info"
		respond(w, http.StatusOK, resp)
	}
}

func (s *server) handleListVersions() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonStatusResponse)
		resp.Status = "OK"
		resp.Message = "here is some info"
		respond(w, http.StatusOK, resp)
	}
}

func (s *server) handleRevertVersion() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonStatusResponse)
		resp.Status = "OK"
		resp.Message = "here is some info"
		respond(w, http.StatusOK, resp)
	}
}

func (s *server) handleSubscribe() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonStatusResponse)
		resp.Status = "OK"
		resp.Message = "here is some info"
		respond(w, http.StatusOK, resp)
	}
}

func (s *server) handleUnsubscribe() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		resp := new(JsonStatusResponse)
		resp.Status = "OK"
		resp.Message = "here is some info"
		respond(w, http.StatusOK, resp)
	}
}
func (s *server) requestTemplateFallbackPriority(w http.ResponseWriter, r *http.Request) {
	s.logMe("requestTemplateFallbackPriority")
	fallbackPaths := s.appApi.Subscription.GetFallbackPaths()
	if fallbackPaths == nil {
		http.Error(w, "No fallback paths available", http.StatusNotFound)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(fallbackPaths)
}

func (s *server) handleTemplateFallbackPriorityUpdate(w http.ResponseWriter, r *http.Request) {
	s.logMe("handleTemplateFallbackPriorityUpdate")
	var newPaths []string
	err := json.NewDecoder(r.Body).Decode(&newPaths)
	if err != nil {
		http.Error(w, "Invalid request body", http.StatusBadRequest)
		return
	}
	s.appApi.Subscription.SetFallbackPaths(newPaths)
	w.WriteHeader(http.StatusOK)
}

func (s *server) getFallbackPriorityStatus(w http.ResponseWriter, r *http.Request) {
	s.logMe("getFallbackPriorityStatus")
	vars := mux.Vars(r)
	node := vars["node"]
	status := s.appApi.Subscription.GetFallbackPriorityStatus(node)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(status)
}

func (s *server) setFallbackPriorityStatus(w http.ResponseWriter, r *http.Request) {
	s.logMe("setFallbackPriorityStatus")
	vars := mux.Vars(r)
	node := vars["node"]
	var status bool
	err := json.NewDecoder(r.Body).Decode(&status)
	if err != nil {
		http.Error(w, "Invalid request body", http.StatusBadRequest)
		return
	}
	s.appApi.Subscription.SetFallbackPriorityStatus(node, status)
	w.WriteHeader(http.StatusOK)
}

var catalogsAreRefresing bool

func (s *server) requestRefreshProgress(w http.ResponseWriter, r *http.Request) {
	if catalogsAreRefresing {
		//send progressbar
		pb := picoWidget.NewProgressBar()
		pb.HtmxFields = []template.HTMLAttr{
			`hx-get="api/htmx/subscriptions/pollRefreshProgress"`,
			`hx-trigger="every 1s"`,
			`hx-target="this"`,
		}
		s.RenderTemplate(w, "picoWidgetFormProgress", pb, true)
		return
	}
	//refresh page
	w.Header().Set("HX-Refresh", "true")
	w.WriteHeader(205)
}
func (s *server) requestRefreshCatalogs(w http.ResponseWriter, r *http.Request) {
	if !catalogsAreRefresing {
		catalogsAreRefresing = true
		go func() {
			resp, err := s.appApi.Subscription.RefreshCatalogs()
			if err != nil {
				doxlog.Errorf("error refreshing catalogs: %v, %v", err, resp.RequestMessages)
			}
			catalogsAreRefresing = false
		}()
	}
	s.requestRefreshProgress(w, r)
}
