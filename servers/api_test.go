package servers

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"testing"
)

//
/*
These tests must be executed with the Doxa server running.
TODO: add more tests, and load them up to detect concurrent access issues
*/
const LocalHost = "http://localhost:8080/api"

func TestApiLdp(t *testing.T) {

	url := LocalHost +
		`/ldp?` +
		`date=2022-08-28` +
		`&exp=` +
		`&cal=gregorian`

	response, err := http.Get(url)
	if err != nil {
		t.Error(err)
	}
	if response.StatusCode != http.StatusOK {
		t.Errorf("%s", response.Status)
		return
	}
	responseData, err := io.ReadAll(response.Body)
	if err != nil {
		t.Error(err)
		return
	}
	strResponseData := string(responseData)
	fmt.Println(string(strResponseData))
	data := new(JsonLdpResponse)
	err = json.Unmarshal([]byte(strResponseData), &data)
	if err != nil {
		t.Error(err)
		return
	}
	if data.Status != "OK" {
		t.Errorf("%s", data.Message)
	}
	expectedDate := "Sunday, August 28, 2022"
	expectedDOW := "Sun"
	expectedModeOfWeek := 2
	expectedEothinon := 11
	if data.Properties.Date != expectedDate {
		t.Errorf("expected date == %s but got %s", expectedDate, data.Properties.Date)
	}
	if data.Properties.DayOfWeek != expectedDOW {
		t.Errorf("expected day of week == %s but got %s", expectedDOW, data.Properties.DayOfWeek)
	}
	if data.Properties.ModeOfWeek != expectedModeOfWeek {
		t.Errorf("expected mode of week == %d but got %d", expectedModeOfWeek, data.Properties.ModeOfWeek)
	}
	if data.Properties.Eothinon != expectedEothinon {
		t.Errorf("expected eothinon == %d but got %d", expectedEothinon, data.Properties.Eothinon)
	}
}

func TestFallbackSettings(t *testing.T) {

	url := LocalHost + "/lml/fallback"

	response, err := http.Get(url)
	if err != nil {
		t.Error(err)
	}
	if response.StatusCode != http.StatusOK {
		t.Errorf("%s", response.Status)
		return
	}
	responseData, err := io.ReadAll(response.Body)
	if err != nil {
		t.Error(err)
		return
	}
	strResponseData := string(responseData)
	fmt.Println(string(strResponseData))
	data := new(JsonFallbackResponse)
	err = json.Unmarshal([]byte(strResponseData), &data)
	if err != nil {
		t.Error(err)
		return
	}
	if data.Status != "OK" {
		t.Errorf("%s", data.Message)
	}
	expectedSubscriptions := "doxa-seraphimdedes"
	if data.AvailableSubscriptions[0] != expectedSubscriptions {
		t.Errorf("expected subscription == %s but got %s", expectedSubscriptions, data.AvailableSubscriptions[0])
	}
}

func TestSetFallbackSettingsSelectedExists(t *testing.T) {
	enabled := true
	selected := "doxa-seraphimdedes"

	putUrl := LocalHost + "/lml/fallback" +
		fmt.Sprintf("?enabled=%s&selected=%s", fmt.Sprintf("%v", enabled), selected)
	putStatus, putRespBody, err := httpPut(putUrl)
	if err != nil {
		t.Error(err)
		return
	}
	if putStatus != http.StatusOK {
		t.Error(fmt.Errorf("expected status %d, but got %d", http.StatusOK, putStatus))
	}
	data := new(JsonStatusResponse)
	err = json.Unmarshal(putRespBody, &data)
	if err != nil {
		t.Error(err)
		return
	}
	if data.Status != "OK" {
		t.Errorf("expected response Status == OK, but got %s: %s", data.Status, data.Message)
	}
	getUrl := LocalHost + "/lml/fallback"
	getStatus, getRespBody, getErr := httpGet(getUrl)
	if getErr != nil {
		t.Error(getErr)
		return
	}
	if getStatus != http.StatusOK {
		t.Error(fmt.Errorf("expected status %d, but got %d", http.StatusOK, getStatus))
	}
	getData := new(JsonFallbackResponse)
	err = json.Unmarshal(getRespBody, &getData)
	if err != nil {
		t.Error(err)
		return
	}
	if getData.FallbackEnabled != enabled {
		t.Errorf("expected %v, got %v", enabled, getData.FallbackEnabled)
	}
	if getData.FallbackSelected != selected {
		t.Errorf("expected %s, got %s", selected, getData.FallbackSelected)
	}
}
func TestSetFallbackSettingsSelectedWithoutTemplates(t *testing.T) {
	enabled := true
	selected := "xyz"

	putUrl := LocalHost + "/lml/fallback" +
		fmt.Sprintf("?enabled=%s&selected=%s", fmt.Sprintf("%v", enabled), selected)
	putStatus, putRespBody, err := httpPut(putUrl)
	if err != nil {
		t.Error(err)
		return
	}
	if putStatus != http.StatusBadRequest {
		t.Error(fmt.Errorf("expected status %d, but got %d", http.StatusBadRequest, putStatus))
	}
	data := new(JsonStatusResponse)
	err = json.Unmarshal(putRespBody, &data)
	if err != nil {
		t.Error(err)
		return
	}
	if !strings.HasSuffix(data.Message, "does not have a templates subdirectory") {
		t.Errorf("expected response Status ending == does not have a templates subdirectory, but got %s: %s", data.Status, data.Message)
	}
}

func TestSetFallbackSettingsSelectedNonexistent(t *testing.T) {
	enabled := true
	selected := "abcd"

	putUrl := LocalHost + "/lml/fallback" +
		fmt.Sprintf("?enabled=%s&selected=%s", fmt.Sprintf("%v", enabled), selected)
	putStatus, putRespBody, err := httpPut(putUrl)
	if err != nil {
		t.Error(err)
		return
	}
	if putStatus != http.StatusBadRequest {
		t.Error(fmt.Errorf("expected status %d, but got %d", http.StatusBadRequest, putStatus))
	}
	data := new(JsonStatusResponse)
	err = json.Unmarshal(putRespBody, &data)
	if err != nil {
		t.Error(err)
		return
	}
	if !strings.HasSuffix(data.Message, "does not exist") {
		t.Errorf("expected response Status ending == does not exist, but got %s: %s", data.Status, data.Message)
	}
}
func TestGetMatchingLangCodes(t *testing.T) {
	getUrl := LocalHost + "/db/library/matching/language?library=en_us_public"
	expect := "en_"
	getStatus, getRespBody, getErr := httpGet(getUrl)
	if getErr != nil {
		t.Error(getErr)
		return
	}
	if getStatus != http.StatusOK {
		t.Error(fmt.Errorf("expected status %d, but got %d", http.StatusOK, getStatus))
	}
	getData := new(JsonLibraryListResponse)
	err := json.Unmarshal(getRespBody, &getData)
	if err != nil {
		t.Error(err)
		return
	}
	if len(getData.Matching) == 0 {
		t.Errorf("expected matching libraries but got zero")
	}
	for _, l := range getData.Matching {
		if !strings.HasPrefix(l, expect) {
			t.Errorf("expected lang code %s, got %s", expect, l)
			return
		}
	}
}

// httpGet must only be called when Doxa is running
func httpGet(url string) (int, []byte, error) {
	return httpRequest(http.MethodGet, url)
}

// httpPut must only be called when Doxa is running
func httpPut(url string) (int, []byte, error) {
	return httpRequest(http.MethodPut, url)
}
func httpRequest(method, url string) (int, []byte, error) {
	req, err := http.NewRequest(method, url, nil)
	if err != nil {
		return http.StatusBadRequest, nil, err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return http.StatusBadRequest, nil, err
	}
	defer resp.Body.Close()

	// read response body
	var body []byte
	body, err = io.ReadAll(resp.Body)
	if err != nil {
		return http.StatusBadRequest, nil, err
	}
	// close response body
	resp.Body.Close()
	return resp.StatusCode, body, nil
}
