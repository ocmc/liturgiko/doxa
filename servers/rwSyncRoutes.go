package servers

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/liturgiko/doxa/api"
	"github.com/liturgiko/doxa/templates/webApp/structs/widget"
	"html/template"
	"net/http"
)

func (s *server) rwSyncRoutes(router *mux.Router) {
	s.router.HandleFunc("/protectFiles", s.serveProtectProjectFilesPage()).Methods(http.MethodGet)

	router.HandleFunc("/htmx/rw/reset", s.resetRwSyncState).Methods(http.MethodPost)
	router.HandleFunc("/htmx/rw/start", s.startRwSync).Methods(http.MethodPost)
	router.HandleFunc("/htmx/rw/pollProgress", s.getRwSyncProgress).Methods(http.MethodGet)
}

func (s *server) serveProtectProjectFilesPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		w.Header().Set("Content-Type", "text/html")
		w.Header().Set("charset", "utf-8")

		data := NewIndexHtmlData(
			r.URL.Path,
			"Protect Your DOXA Project",
			s.getNavbar(w, r),
		)
		data.Body.Main = picoWidget.NewMain(
			"Protect Your DOXA Project",
			"Secure your DOXA project by uploading your files to your GitLab Project Group. Additionally, maintain control over your project’s integrity by managing when you download files from other users’ DOXA Projects that you subscribed to. This ensures both your work and the content you incorporate remain protected.",
		)
		data.Body.Main.Help = `
<style>
    .tip {
        background: #f8f9fa;
        border-left: 4px solid #4CAF50;
        padding: 15px;
        margin: 15px 0;
    }
    .warning {
        background: #fff3cd;
        border-left: 4px solid #ffc107;
        padding: 15px;
        margin: 15px 0;
    }
</style>
<div class="container">
    <h1>Sync Service User Guide</h1>
    
    <h2>Download Subscriptions</h2>
    <p>The download interface allows you to manage and update your library subscriptions:</p>
    <ul>
        <li><strong>Status:</strong> Shows current state of your libraries</li>
        <li><strong>Available Updates:</strong> Lists any new versions ready to download</li>
        <li><strong>Last Download:</strong> Shows when you last updated your libraries</li>
        <li><strong>Progress:</strong> Displays real-time download status</li>
    </ul>

    <div class="tip">
        <strong>Tip:</strong> The status icon in the title bar quickly shows if updates are available (<i class="bi bi-cloud-download"></i>) or if everything is up to date (<i class="bi bi-cloud-check"></i>).
    </div>

    <h2>Library Types</h2>
    <p>There are two types of library subscriptions you can manage:</p>
    <ul>
        <li><i class="bi bi-book"></i> <strong>Stable:</strong> Regular, tested library versions</li>
        <li><i class="bi bi-cone-striped"></i> <strong>Unstable:</strong> Development versions with latest changes</li>
    </ul>

    <h2>Status Indicators</h2>
    <p>The interface uses these icons to show library status:</p>
    <ul>
        <li><i class="bi bi-cloud-check"></i> <strong>Up to date</strong> - Your libraries are current</li>
        <li><i class="bi bi-cloud-download"></i> <strong>Updates available</strong> - New versions ready to download</li>
        <li><i class="bi bi-arrow-repeat"></i> <strong>In progress</strong> - Download is running</li>
        <li><i class="bi bi-exclamation-triangle-fill"></i> <strong>Warning</strong> - Minor issues detected</li>
        <li><i class="bi bi-exclamation-octagon-fill"></i> <strong>Error</strong> - Serious problems found</li>
    </ul>

    <h2>Upload Changes</h2>
    <p>The upload interface helps you share your local changes:</p>
    <ul>
        <li><strong>Start:</strong> Begins the upload process</li>
        <li><strong>View in GitLab:</strong> Opens your repository in the browser</li>
        <li><strong>Clear:</strong> Dismisses completed upload information</li>
    </ul>

    <div class="warning">
        <strong>Important:</strong> You must unlock your GitLab token and have an internet connection before uploading changes.
    </div>

    <h2>Tips for Effective Sync Management</h2>
    <ul>
        <li>Keep your libraries up to date by checking for updates regularly</li>
        <li>Review any warnings or errors in the status display</li>
        <li>Wait for downloads to complete before closing the application</li>
        <li>Use "View in GitLab" to verify your uploads</li>
    </ul>
</div>
`
		grid := new(picoWidget.Grid)
		widget := s.appApi.RwSync.GetWidget(nil, "api/htmx/rw/", "rw", "")
		if status, _, _ := s.appApi.TokenService.GetTokenStatus(nil, ""); status != api.Decrypted {
			widget = s.appApi.TokenService.GetWidget(ctx, "api/htmx/token/", "token", "")
		}
		roWdgt := s.appApi.RoSync.GetForm(nil, "api/htmx/ro/", "ro", "")
		grid.AddWidget(widget)
		grid.AddWidget(roWdgt)
		section := new(picoWidget.Section)
		section.AddGrid(grid)
		data.Body.Main.AddSection(section)
		data.NeedsModalConfirm = true
		s.RenderTemplate(w, "index", data, false)
	}
}

func (s *server) startRwSync(w http.ResponseWriter, r *http.Request) {
	s.logMe("startRwSync")
	if resp, err := s.appApi.RwSync.SyncAll(nil, ""); err != nil {
		w.WriteHeader(404)
		for _, msg := range resp.RequestMessages {
			fmt.Fprint(w, msg)
		}
	}
	w.Header().Set("HX-Refresh", "true")
	w.WriteHeader(205)
	return
}

func (s *server) resetRwSyncState(w http.ResponseWriter, r *http.Request) {
	s.logMe("resetRwSyncState")
	s.appApi.RwSync.ResetPage(nil, "")
	w.Header().Set("HX-Refresh", "true")
	w.WriteHeader(205)
	return
}

func (s *server) getRwSyncProgress(w http.ResponseWriter, r *http.Request) {
	s.logMe("getRwSycnProgress")
	deets, _, _ := s.appApi.RwSync.Details(nil, "")
	bar := picoWidget.NewProgressBar()
	bar.Max = deets.NumTotal
	bar.Value = deets.NumCompleted
	if deets.Done {
		w.Header().Set("HX-Refresh", "true")
		w.WriteHeader(205)
		return
	}
	bar.HtmxFields = []template.HTMLAttr{
		`hx-get="/api/htmx/rw/pollProgress"`,
		`hx-trigger="every 1s"`,
		`hx-swap="outerHTML"`,
	}
	s.RenderTemplate(w, "picoWidgetFormProgress", bar, false)
	return
}
