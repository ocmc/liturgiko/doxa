package servers

import (
    "fmt"
    "github.com/gorilla/mux"
    "github.com/liturgiko/doxa/pkg/doxlog"
    "net/http"
)

func (s *server) statusRoutes(router *mux.Router) {
    router.HandleFunc("/htmx/alerts/status", s.getAlertsStatus).Methods(http.MethodGet)
    router.HandleFunc("/htmx/messages/status", s.getMessagesStatus).Methods(http.MethodGet)
    router.HandleFunc("/htmx/wifi/status", s.getWifiStatus).Methods(http.MethodGet)
}

func (s *server) getAlertsStatus(w http.ResponseWriter, r *http.Request) {
    s.logMe("getAlertStatus")
    var err error
    var count int
    _, err = fmt.Fprint(w, fmt.Sprintf("<i class=%s>%d</i>", `"bi bi-bell""`, count))
    if err != nil {
        doxlog.Errorf(err.Error())
        return
    }
    return
}

func (s *server) getMessagesStatus(w http.ResponseWriter, r *http.Request) {
    var err error
    var count int
    _, err = fmt.Fprint(w, fmt.Sprintf("<i class=%s>%d</i>", `"bi bi-envelope""`, count))
    if err != nil {
        doxlog.Errorf(err.Error())
        return
    }
    return
}

func (s *server) getWifiStatus(w http.ResponseWriter, r *http.Request) {
    var err error
    internetAvailable := s.appApi.Status.InternetOk()
    if internetAvailable {
        _, err = fmt.Fprint(w, `<i class="bi bi-wifi"></i>`)
        if err != nil {
            doxlog.Errorf(err.Error())
            return
        }
    } else {
        _, err = fmt.Fprint(w, `<i class="bi bi-wifi-off"d></i>`)
        if err != nil {
            doxlog.Errorf(err.Error())
            return
        }
    }
    return
}
