package servers

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/dvcs"
	"github.com/liturgiko/doxa/pkg/users"
	"net/http"
	"strconv"
	"strings"
)

func (s *server) userRoutes(router *mux.Router) {
	router.HandleFunc("/user/login", s.handleUserLogin()).Methods(http.MethodPost)
	//TODO: prefix with /user if still in use
	router.HandleFunc("/logout", s.handleUserLogout()).Methods(http.MethodGet, http.MethodPost, http.MethodPut)
	router.HandleFunc("/user/profile", s.handleUserProfile()).Methods(http.MethodGet, http.MethodPut)
}

func (s *server) handleUserLogin() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		uid := r.FormValue("username")
		pwd := r.FormValue("code")
		if s.userManager.IsValidToken(uid, pwd) {
			sessionId := s.sessionManager.Add(uid)
			// Create a new cookie
			cookie := &http.Cookie{
				Name:  "sessionId",
				Value: sessionId,
				Path:  "/",
			}
			// Add the cookie to the response
			http.SetCookie(w, cookie)
			http.Redirect(w, r, "/", http.StatusSeeOther)
		} else {
			http.Redirect(w, r, "/login", http.StatusSeeOther)
		}
		return
	}
}

func (s *server) handleUserLogout() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		if s.cloud {
			cookie, err := r.Cookie(sessionCookie)
			if err != nil {
				doxlog.Errorf("no session cookie found when user requested logout: %v", err)
			}
			if len(cookie.Value) > 0 {
				if session, ok := s.sessionManager.GetSessionById(cookie.Value); ok {
					s.sessionManager.Delete(session.ID)
				}
			}
			http.Redirect(w, r, "/login", http.StatusSeeOther)
		}
		return
	}
}

// handleUserProfile handles GET and PUT for user profiles
func (s *server) handleUserProfile() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("charset", "utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		// if GET
		if r.Method == http.MethodGet {
			resp := new(JsonProfileResponse)
			// TODO: get uid from session.  For now, we are just supporting the local user
			uid := users.LocalUser
			profile, err := s.userManager.GetProfile(uid)
			if err != nil {
				resp.Status = fmt.Sprintf("%d", http.StatusNotFound)
				resp.Message = fmt.Sprintf("error getting profile for %s: %v", uid, err)
				respond(w, http.StatusOK, resp)
				return
			}
			resp.Profile = profile
			resp.Status = "OK"
			resp.Message = fmt.Sprintf("profile for %s", uid)
			respond(w, http.StatusOK, resp)
			return
		}
		// else handle PUT
		resp := new(JsonStatusResponse)
		var uid string
		if s.cloud {
			// TODO: get uid from session
		} else {
			uid = users.LocalUser
		}
		var err error
		profile := users.NewProfile(uid)
		profile.UserName = uid
		doxaHandlesGit := r.URL.Query().Get("doxaHandlesGit")
		if len(doxaHandlesGit) == 0 {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = "missing information about whether Doxa will handle Git and Gitlab"
			respond(w, http.StatusOK, resp)
			return
		}
		profile.Gitlab.DoxaHandlesGitlab = doxaHandlesGit == "true"
		if profile.Gitlab.DoxaHandlesGitlab {
			gitlabPrivate := r.URL.Query().Get("gitlabPrivate")
			if len(gitlabPrivate) == 0 {
				resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
				resp.Message = "missing information about whether Gitlab projects are to be private or public"
				respond(w, http.StatusOK, resp)
				return
			}
			profile.Gitlab.GitlabPublic = gitlabPrivate != "true"
			// get text parameters
			profile.Gitlab.Contact = r.URL.Query().Get("contact")
			profile.Gitlab.Copyright = r.URL.Query().Get("copyright")
			profile.Gitlab.License = r.URL.Query().Get("license")
			profile.Gitlab.Jurisdiction = r.URL.Query().Get("jurisdiction")

			// validate the gitlab group ID and the token
			groupIdStr := r.URL.Query().Get("groupId")
			groupIdStr = strings.TrimSpace(groupIdStr)
			if profile.Gitlab.DoxaHandlesGitlab && len(groupIdStr) == 0 {
				resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
				resp.Message = "missing Gitlab group ID"
				respond(w, http.StatusOK, resp)
			}
			var groupId int
			groupId, err = strconv.Atoi(groupIdStr)
			if err != nil {
				resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
				resp.Message = fmt.Sprintf("error converting %s to integer: %v", groupIdStr, err)
				respond(w, http.StatusOK, resp)
				return
			}
			token := r.URL.Query().Get("token")
			token = strings.TrimSpace(token)
			if len(token) == 0 || strings.HasPrefix(token, "*") {
				// get the token from the user manager.
				token, err = s.userManager.GetToken(uid)
				if err != nil {
					resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
					resp.Message = fmt.Sprintf("could not retrieve token from vault: %v", err)
					respond(w, http.StatusOK, resp)
					return
				}
			} // else the user has entered a new token
			var client *dvcs.Gitlab
			client, err = dvcs.NewGitlabClient(token,
				groupId,
				config.DoxaPaths.ProjectDirPath,
				"", true)
			if err != nil {
				resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
				resp.Message = fmt.Sprintf("group ID and/or token invalid: %v", err)
				respond(w, http.StatusOK, resp)
				return
			}
			// set the gitlab root group information
			var group *dvcs.Group
			group, _, err = client.GetGroup(groupId)
			if err != nil {
				resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
				resp.Message = fmt.Sprintf("could not read Gitlab group %d: %v", groupId, err)
				respond(w, http.StatusOK, resp)
				return
			}
			profile.Gitlab.GitlabRootGroup = group
			profile.Gitlab.Verified = true
			// save the token
			err = s.userManager.SetToken(uid, token)
			if err != nil {
				resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
				resp.Message = fmt.Sprintf("could not save token: %v", err)
				respond(w, http.StatusOK, resp)
				return
			}
		}
		// validate the profile properties
		if valid, errors := profile.Valid(); !valid {
			resp.Status = fmt.Sprintf("%d", http.StatusBadRequest)
			resp.Message = fmt.Sprintf("%v", errors)
			respond(w, http.StatusOK, resp)
			return
		}
		err = s.userManager.SetProfile(profile)
		if err != nil {
			resp.Status = fmt.Sprintf("%d", http.StatusInternalServerError)
			resp.Message = fmt.Sprintf("error saving profile: %v", err)
			respond(w, http.StatusOK, resp)
			return
		}
		// update the Navbar info
		if profile.Gitlab.DoxaHandlesGitlab {
			s.Navbar.Gitlab = profile.Gitlab.GitlabRootGroup.WebURL
		}
		resp.Status = "OK"
		resp.Message = fmt.Sprintf("updated profile for %s", uid)
		respond(w, http.StatusOK, resp)
		return
	}
}
