FROM golang:alpine as builder
COPY . $GOPATH/src
WORKDIR $GOPATH/src
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GOARCH_VERSION=v8 go build -a -installsuffix cgo -o /go/bin/doxa main.go
FROM golang:alpine
RUN apk update && apk add --no-cache git
COPY --from=builder /go/bin/doxa /doxa
RUN addgroup -g 1000 doxasys && adduser -D -u 1000 -G doxasys doxasys
USER doxasys
EXPOSE 8080
ENTRYPOINT ["/doxa"]