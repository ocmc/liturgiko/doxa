import * as utils from "./utils.js";

/**
 * @tagname code-editor
 * @description A full-featured code editor component for liturgical template files (.lml). Provides editing, preview, template insertion tracking, and error highlighting capabilities. It also supports editing of css (.css) and javascript (.js), as indicated by the lang attribute.
 * 
 * @inputs {Attribute} host - Base URL for API endpoints
 * @inputs {Attribute} completions - JSON string of code completions for the editor
 * @inputs {Attribute} lang - Language mode for syntax highlighting (lml, css, js)
 * @inputs {Attribute} src - Source file path/identifier
 * @inputs {Attribute} text - Initial content for the editor
 * @inputs {Attribute} role - Role-based permissions
 * 
 * @outputs {Event} None - This component doesn't emit custom events directly
 * 
 * @api {GET} /api/{lang} - Fetches file content
 * @api {PUT} /api/{lang} - Saves file content
 * @api {GET} /api/lml/parse - Parses LML content for preview
 * @api {PUT} /api/lml/copy - Creates a copy of the current file
 * @api {PUT} /api/lml/delete - Deletes the current file
 * 
 * @watches {Attribute} src - Loads new content when changed
 * 
 * @examples
 * 
 * Basic usage:
 * ~~~html
 * <code-editor
 *   id="liturgy-editor"
 *   host="/api/"
 *   lang="lml"
 *   src="liturgics/services/vespers">
 * </code-editor>
 * ~~~
 * 
 * With completions:
 * ~~~html
 * <code-editor
 *   id="advanced-editor"
 *   host="/api/"
 *   lang="lml"
 *   src="liturgics/services/vespers"
 *   completions='[{"name":"insert","value":"insert","meta":"keyword"}]'>
 * </code-editor>
 * ~~~
 */
class CodeEditor extends HTMLElement {
  // handles custom event comboSelectionChanged (raised by generation-layouts.js)
  // handles custom event openTemplate (raised in preview pane by code inserted by this class code-editor.js)
  constructor() {
    super();
    console.log("static/wc/code-editor.js")
    this.ideLeftPaneDiv = null;
    this.toolsVisibilityToggleBtn = null;
    // handles for the preview elements
    this.previewVisibilityToggleBtn = null;
    this.previewGenPageNavbarIcons = null;
    this.previewGenPageNavbarMissingNav = null;
    this.previewGenPageNotFoundIndex = 0;
    this.previewGenPageNoValueIndex = 0;
    this.previewGenPageCurrentMissing = null;
    this.previewGenPageShowingSpan = null;
    this.previewGenPageShowingIndex = null;
    this.previewGenPageShowingTotal = null;
    this.highlight = "#F2BB05";
    this.scrollY = 0;
    this.rowToScrollTo = "";

    // iframe separator handles
    this.ideHorizontalSeparator = null;
    this.ideHorizontalSeparatorVisible = true;
    this.ideVerticalSeparator = null;

    this.currentDirPath = []; // set when template is loaded.  Passed to modal-file-select.
    this.date = "";
    this.defaultFontSize = 17;
    this.defaultLineHeight = "1.5em";
    this.editor = null;
    this.filePath = "";
    this.font = this.defaultFontSize;
    this.handlingSaveAs = false;
    this.hide = "Hide";
    this.show = "Show";
    this.isBlock = false;
    this.lineheight = this.defaultLineHeight;
    this.modalFileSelect = null;
    this.previewBtn = null;
    this.previewDate = "";
    this.previewDiv = null; // container for iframe
    this.previewDivVisible = true;
    this.previewIframe = null;
    this.previewLibraryCombo = "0";
    this.previewShowMissing = false;
    this.previewShowMissingOn = "Hide Missing Values";
    this.previewShowMissingOff = "Show Missing Values";
    this.readOnly = false;
    this.readOnlyMessage = " (read only)";
    this.previewShowTemplatesBtn = null;
    this.selectedLibraryComboKey = "selectedLibraryCombo"
    this.showTemplatesBtnOn = "Hide Inserted Templates";
    this.showTemplatesBtnOff = "Show Inserted Templates";
    this.previewShowTemplates = false;
    this.templatesByNbr = null;
    this.templatesTableData = null;
    this.theme = "ace/theme/twilight";
    this.toolsDiv = null; // container for iframe
    this.toolsDivVisible = true;
    this.toolsIframe = null;
    this.toolsIframeLdpTool = null;
    this.toolsIframeLibraryComboTool = null;
    this.toolsIframeTemplatesUsedTable = null;
    this.waitingForParseResult = false;
    this.wrap = false;
    this.wrapOff = "Turn Line Wrap Off";
    this.wrapOn = "Turn Line Wrap On";
    // Navbar File Menu Buttons
    this.closeBtn = null;
    this.copyBtn = null;
    this.deleteBtn = null;
    this.newBtn = null;
    this.openBtn = null;
    this.previewShowMissingBtn = null;
    this.saveAsBtn = null;
    this.saveBtn = null;

    // stats for missing values
    this.nbrNotFound = 0;
    this.nbrNoValue = 0;
    let tmpScroll = localStorage.getItem(this.filePath+".scrollY");
    if (tmpScroll != null) {
      this.previewDiv.scrollY = tmpScroll;
    }
  }

  /**
   * Web component lifecycle method that runs when the element is added to the DOM
   * Sets up the editor, binds event listeners, and initializes the UI
   */
  connectedCallback() {
    let html = `
    <style> 
      @import url('static/css/bootstrap.min.css');
      @import url('static/img/bootstrap-icons.css');
    </style>
    <style>
        br {
          padding-top: 0px;
        }
        .ace_editor.ace_autocomplete {
            border: 1px solid rgba(0,0,0,.2)!important;
            height: 30% !important;
            width: 100% !important;
            position:absolute;
            top: 0;
            left: 0; 
            z-index:9999;
            overflow-x: scroll;
            overflow-y: scroll;
        }
        .sticky-search {
          position: -webkit-sticky !important; /* Safari & IE */
          position: sticky !important;
          top: 0 !important;
        }
        body {
         margin-top: 0 !important;
         height: 100%;
         overflow-x: scroll;
         overflow-y: scroll;
        }
        .ace-scroller {
            overflow-x: scroll !important;
            overflow-y: scroll !important;
        }
        .editor-wrapper, .code-editor {
            display: flex;
            flex-direction: row;
            flex-grow: 1;
            height: 100%;
            width: 100%;
            overflow-x: scroll !important;
            overflow-y: scroll !important;
        }
        #editor {
          flex-grow: 1;
        }
        .readOnly {
          color: red;
          font-weight: bold;
        }
        .lowerEditor {
          padding-top: 100px;
        }

    </style>
  <!-- Source filepath -->
      <div class="row">
        <span class="code-editor-alert-non-lml" id="code-editor-alert" style="width:100% !important; height:10% !important; overflow: visible !important;"></span>
      </div>
  <!-- Editor -->
      <div id="editor-wrapper" class="row p-1 editor-wrapper">
        <div id="editor" class="code-editor code-editor-non-lml">${this.text}</div>
      </div>
  <!-- Placeholder for modal file selector -->
        <modal-file-select 
        id="modal-file-select" 
        class="d-none"
        path=""
        title="" 
        src="" 
        host="" 
        fileSuffix="lml"
        currentFile="${this.src}"></modal-file-select>
  <!-- Placeholder for confirmation modal -->
      <div id="div-modal-confirm"></div>
  `
    this.innerHTML = html;
    // set the Navbar Edit menu item button handles
    this.closeBtn = window.parent.document.querySelector("#close-btn");
    this.copyBtn = window.parent.document.querySelector("#copy-btn");
    this.deleteBtn = window.parent.document.querySelector("#delete-btn");
    this.newBtn = window.parent.document.querySelector("#new-btn");
    this.openBtn = window.parent.document.querySelector("#open-btn");
    this.previewBtn = window.parent.document.querySelector("#preview-btn");
    this.previewShowTemplatesBtn = window.parent.document.querySelector("#show-templates-btn");
    this.previewShowMissingBtn = window.parent.document.querySelector("#show-missing-btn");
    this.saveAsBtn = window.parent.document.querySelector("#saveAs-btn");
    this.saveBtn = window.parent.document.querySelector("#save-btn");

    this.modalConfirmDiv = this.querySelector("#div-modal-confirm")
    this.modalFileSelect = this.querySelector("#modal-file-select")

    this.readPreferences();
    this.initAceEditor();

    if (this.lang === "lml") {
      this.closeBtn.addEventListener('click', () => this.handleCloseFile());
      if (this.copyBtn) {
        this.copyBtn.addEventListener('click', () => this.handleFileCopy());
      }
      this.toolsVisibilityToggleBtn = window.parent.document.querySelector("#itemToggleToolsPane");
      this.toolsVisibilityToggleBtn.addEventListener('click', (ev) => this.toggleRightTopVisible(ev));
      this.previewVisibilityToggleBtn = window.parent.document.querySelector("#itemTogglePreviewPane");
      this.previewVisibilityToggleBtn.addEventListener('click', (ev) => this.toggleRightBottomVisible(ev));
      this.ideHorizontalSeparator = window.parent.document.getElementById("ide-horizontal-separator");
      this.ideVerticalSeparator = window.parent.document.getElementById("ide-vertical-separator");
      this.deleteBtn.addEventListener('click', () => this.handleDelete());
      this.newBtn.addEventListener('click', () => this.handleNewFile());
      this.openBtn.addEventListener('click', () => this.showModalFileSelect("Template File Explorer"));
      this.previewBtn.addEventListener('click', () => this.handlePreview());
      this.previewShowTemplatesBtn.addEventListener('click', () => this.togglePreviewShowTemplates());
      this.previewShowMissingBtn.addEventListener('click', () => this.togglePreviewShowMissing());
      this.saveAsBtn.addEventListener('click', () => this.handleSaveAs());
      this.previewDiv = window.parent.document.getElementById("ide-right-bottom-div");
      this.previewIframe = window.parent.document.getElementById("iframeBottom");
      this.previewIframe.addEventListener('load', () => this.handlePreviewLoad());
      this.previewIframe.addEventListener('openTemplate', (event) => this.handleTemplateSelected(event));
      this.ideLeftPaneDiv = window.parent.document.getElementById("ide-left-pane");
      this.toolsVisibilityToggleBtn = null;
      this.previewVisibilityToggleBtn = null;
      this.toolsDiv = window.parent.document.getElementById("ide-right-top-div");
      this.toolsIframe = window.parent.document.getElementById("iframeTop");
      window.parent.document.addEventListener('openEditor', (event) => this.showEditor(event));
      this.toolsIframeLdpTool = this.toolsIframe.contentWindow.document.querySelector("#ldp-tool");
      this.toolsIframeTemplatesUsedTable = this.toolsIframe.contentWindow.document.querySelector("#templatesUsed");
      this.toolsIframeLibraryComboTool = this.toolsIframe.contentWindow.document.querySelector("#library-combo-tool");
      this.toolsIframeLibraryComboTool.addEventListener('combosaved', () => this.handleComboToolSave());
      this.toolsIframeLibraryComboTool.addEventListener('comboSelectionChanged', () => this.handleComboToolClick());
      this.modalFileSelect.addEventListener('modal-file-select:canceled', () => this.handleModalFileSelectCancel());
      this.modalFileSelect.addEventListener('modal-file-select:selected', () => this.handleModalFileSelectFileSelected());
      if (this.toolsIframeLdpTool) {
        this.toolsIframeLdpTool.addEventListener("ldpchanged", (ev) => this.handleLdpDateChange(ev));
      }
      this.querySelector("#code-editor-alert").classList.remove("code-editor-alert-non-lml");
      this.querySelector("#code-editor-alert").classList.add("code-editor-alert-lml");
      this.querySelector("#editor").classList.remove("code-editor-non-lml");
      this.querySelector("#editor").classList.add("code-editor-lml");
    }

    window.parent.document.querySelector("#editor-dropdown").addEventListener('click', (event) => this.changeEditor(event));
    this.saveBtn.addEventListener('click', (ev) => this.handleFileUpdate(ev));

    if (this.src && this.src.length > 0) {
      this.loadContent();
    }
  }

  /**
   * Web component lifecycle method that runs when the element is removed from the DOM
   * Cleans up by saving preferences and removing event listeners
   */
  disconnectedCallback() {
    this.savePreferences();
    if (this.lang === "lml") {
      this.closeBtn.removeEventListener('click', () => this.handleCloseFile());
      if (this.copyBtn) {
        this.copyBtn.removeEventListener('click', () => this.handleFileCopy());
      }
      this.deleteBtn.removeEventListener('click', () => this.handleDelete());
      this.newBtn.removeEventListener('click', () => this.handleNewFile());
      this.openBtn.removeEventListener('click', () => this.showModalFileSelect("Template File Explorer"));
      this.previewShowMissingBtn.removeEventListener('click', () => this.handlePreview());
      this.saveAsBtn.removeEventListener('click', () => this.handleSaveAs());
      this.previewIframe.removeEventListener('load', () => this.handlePreviewLoad());
      this.toolsIframeLibraryComboTool.removeEventListener('combosaved', () => this.handleComboToolSave());
      this.toolsIframeLibraryComboTool.removeEventListener('comboSelectionChanged', () => this.handleComboToolClick());
      this.modalFileSelect.removeEventListener('modal-file-select:canceled', () => this.handleModalFileSelectCancel());
      this.modalFileSelect.removeEventListener('modal-file-select:selected', () => this.handleModalFileSelectFileSelected());
      if (this.toolsIframeLdpTool) {
        this.toolsIframeLdpTool.removeEventListener("ldpchanged", () => this.handleLdpDateChange());
      }
    }

    window.parent.document.querySelector("#editor-dropdown").removeEventListener('click', (event) => this.changeEditor(event));
    this.saveBtn.removeEventListener('click', (ev) => this.handleFileUpdate(ev));
  }

  togglePreviewShowTemplates() {
    if (!this.previewShowTemplatesBtn) {
      return;
    }
    this.previewShowTemplates = !this.previewShowTemplates;
    if (this.previewShowTemplates) {
      this.previewShowTemplatesBtn.innerText = this.showTemplatesBtnOn;
      this.showTemplatesInPreview();
    } else {
      this.previewShowTemplatesBtn.innerText = this.showTemplatesBtnOff;
      this.hideTemplatesInPreview();
    }
//    localStorage.setItem("showTemplates",this.previewShowTemplates);
  }
  handleLdpDateChange(ev) {
    if (this.isBlock) {
      this.previewDate = this.toolsIframeLdpTool.getAttribute("date")
      if (this.date && this.date === this.previewDate) {
        return;
      }
      this.date = this.previewDate;
      localStorage.setItem("previewDate", this.previewDate);
      this.handlePreview();
    }
  }
  handleTemplateSelected(ev) {
    if (ev && ev.detail && ev.detail.href) {
      this.openInNewTab(ev.detail.href)
    }
  }
  handlePreviewLoad() {
    const contentWindow = this.previewIframe.contentWindow;
    const doc = contentWindow.document;
    let theSpans = doc.getElementsByClassName("kvp");
    if (theSpans) {
      Array.from(theSpans).forEach(function(theSpan) {
        theSpan.addEventListener('dblclick', (event) => handleDblClick(event));
        function handleDblClick(ev) {
          ev.target.setAttribute("id", "recordBeingEdited");
          window.parent.document.dispatchEvent(
              new CustomEvent("openEditor", {
                bubbles: true,
                detail: { value: ev.target.innerText,
                  lang: ev.target.getAttribute("data-lang"),
                  primary: ev.target.getAttribute("data-primary"),
                  fallback: ev.target.getAttribute("data-fallback"),
                  topicKey: ev.target.getAttribute("data-topic-key"),
                  redirects: ev.target.getAttribute("data-id-redirects"),
                  dbStatus: ev.target.getAttribute("data-status"),
                  used: ev.target.getAttribute("data-id-used"),
                  dummy: ev.target.classList.contains("wasDummy")
                },
              })
          );
        }
      });
    }
    let lastTemplateSeenId = "";
    let lastTemplateSeenNbr = "";

    this.previewIframe.contentWindow.document.querySelectorAll(".versions-container").forEach((theRow) => {
      if (!theRow.hasAttribute("data-template-id")) {
        return;
      }
      const rowId = theRow.getAttribute("id");
      if (!rowId) {
        return;
      }
      let templateUsed = theRow.getAttribute("data-template-id");
      let templateUsedLine = theRow.getAttribute("data-template-line");
      let callStack = theRow.getAttribute("data-template-lines");
      let insertedById = "";
      let insertedByLine = "";
      if (callStack && callStack.length > 0) {
        const parts = callStack.split(",");
        if (parts.length > 1) {
          const insertParts = parts[parts.length-2].split(":")
          if (insertParts && insertParts.length === 2) {
            insertedById = this.templatesByNbr[insertParts[0]];
            insertedByLine = insertParts[1];
          }
        }
      }
      if (templateUsed !== lastTemplateSeenNbr) {
        lastTemplateSeenNbr = templateUsed;
        lastTemplateSeenId = this.templatesByNbr[lastTemplateSeenNbr];
        let templateId = this.templatesByNbr[templateUsed];
        if (templateId && templateId.length > 0) {
          const row = doc.createElement("div");
          row.classList.add("templateUsed");
          row.classList.add("d-none"); // we will remove / add back d-none to make visible, invisible
          row.setAttribute("style", "border-top:5px solid;");
          let srcId = "";
          if (this.src) {
            try {
              srcId = JSON.parse(this.src).join("/");
            } catch (error) {
              alert(`347 ${error?.message}`)
            }
          }
          if (insertedById && insertedById.length > 0) {
            if (srcId.endsWith(insertedById)) {
              row.innerHTML = `<span style="">This template&nbsp;line ${insertedByLine}&nbsp;<br>inserted:<br><a href="ide?id=${encodeURIComponent(templateId)}&date=${this.date}" target="_self">${templateId}</a>&nbsp;line&nbsp;${templateUsedLine}<br>inserted:</br></span>`
            } else {
              row.innerHTML = `<span style=""><a href="ide?id=${encodeURIComponent(insertedById)}&date=${this.date}" target="_self">${insertedById}</a>&nbsp;line ${insertedByLine}&nbsp;<br>inserted:<br><a href="${this.host}ide?id=${encodeURIComponent(templateId)}&date=${this.date}" target="_self">${templateId}</a>&nbsp;line&nbsp;${templateUsedLine}<br>inserted:</br></span>`
            }
          } else {
            if (srcId.endsWith(templateId)) {
              row.innerHTML = `<span style="" >This template&nbsp;line&nbsp;${templateUsedLine}<br>inserted:<br></span>`
            } else {
              row.innerHTML = `<span style="" ><a href="ide?id=${encodeURIComponent(templateId)}&date=${this.date}" target="_self">${templateId}</a>&nbsp;line&nbsp;${templateUsedLine}<br>inserted:<br></span>`
            }
          }
          // Next we attach an event handler to each of the anchors in the row.
          // The event handler will dispatch a custom event from
          // the preview pane iframe, so that the code editor pane can
          // detect it and submit a request to the Doxa api to open
          // the selected template in a new browser tab.
          // We have to do this for security reasons.
          // If a regular anchor href was used, it uses the host name
          // that is in the preview pane for the call, and it is disallowed
          // because it treats it as a different host.
          let anchors = row.querySelectorAll('span > a');
          // iterate the anchors
          for (let i = 0; i < anchors.length; i++) {
            let anchor = anchors[i];
            anchor.addEventListener('click', function(event) {
              // Prevent the default action,
              // which for an anchor is to open whatever
              // the href is set to.
              event.preventDefault();

              // Get the href value from the event target (the anchor element)
              let hrefValue = event.target.getAttribute("href");
              // Create a new custom event and pass to it the href
              // of the template the user wants to open.
              let openTemplateEvent = new CustomEvent('openTemplate', {
                bubbles: true, // The event should bubble up through the DOM
                cancelable: false, // The event's default action can be prevented
                detail: { href: hrefValue } // Pass the href value as data in the custom event
              });

              // Dispatch the custom event
              let frame = window.parent.document.getElementById("iframeBottom");
              if (frame) {
                frame.dispatchEvent(openTemplateEvent);
              } else {
                console.log("code-editor.handlePreviewLoad() could not get reference to iframe for the preview pane to dispatch a custom event openTemplate")
              }
            });
          }
          const target = doc.getElementById(rowId);
          target.parentNode.insertBefore(row, target);
        }
      }
  });
    this.showMissingValuesInPreview();
    if (this.rowToScrollTo && this.rowToScrollTo.length > 0) {
      this.scrollToRow(this.rowToScrollTo);
    }
    this.rowToScrollTo = "";
  }
  showEditor(ev) {
    if (ev && ev.detail && ev.detail) {
      let notExists = "false";
      let value = ev.detail.value;
      // the value could be temporarily set to the requested ID,
      // if so, for the editor, set it back to empty string.
      if (ev.detail.dummy && ev.detail.dummy === true) {
        value = "";
      }
      let dataLang = ev.detail.lang;
      let dataPrimary = ev.detail.primary;
      let dataFallback = ev.detail.fallback;
      let dataTopicKey = ev.detail.topicKey;
      let redirects = ev.detail.redirects;
      let dbStatus = ev.detail.dbStatus;
      let id = ev.detail.used;
      if (!id || id.length === 0) {
        if (redirects && redirects.length > 0) {
          id = utils.getLastFromTrace(redirects);
          notExists = "true";
          value = "";
        }
      }
      const component = document.createElement("div");
      component.className = "div-modal-edit-record";
      component.innerHTML = `
        <modal-edit-record
            id="preview-modal-edit-record"
            title="Edit Record"
            host=""
            trace="${redirects}"
            recNotExists="${notExists}"
            recId="${id}"
            recValue="${value}"
            showRedirects="true"
            status="${dbStatus}"
            cancelBtn="Close"
            okBtn="Save">
        </modal-edit-record>`
      component.addEventListener('click', (ev) => {
        const contentWindow = this.previewIframe.contentWindow;
        const doc = contentWindow.document;
        let theSpans = doc.getElementsByClassName("kvp");

        this.scrollY = contentWindow.scrollY;
        let theModal = component.querySelector("#preview-modal-edit-record");
        let theStatus = theModal.getAttribute("status");
        // get a handle for the span whose record was being edited
        let theRec = doc.getElementById("recordBeingEdited");
        if (theStatus === "updated") {
          this.setRowFor(theRec);
          this.handlePreview();
        } else {
          theRec.removeAttribute("id");
        }
        component.querySelector("#preview-modal-edit-record").setAttribute("visible", "false");
        doc.querySelector("#div-modal-edit").innerHTML = "";
//        contentWindow.scrollTo({top: scrollY, behavior: 'auto'});
      });
      component.querySelector("#preview-modal-edit-record").setAttribute("visible", "true");
      window.parent.document.querySelector("body").appendChild(component);
      }
  }

  scroll() {
    if (this.scrollTo && this.scrollTo > -1) {
      const contentWindow = this.previewIframe.contentWindow;
      if (contentWindow) {
        contentWindow.scrollTo({top: this.scrollY, behavior: 'auto'});
      }
    }
  }
  hideMissingValuesInPreview() {
    if (this.previewIframe && this.previewIframe.contentWindow) {
      const contentWindow = this.previewIframe.contentWindow;
      const doc = contentWindow.document;
      if (doc) {
        const scrollY = contentWindow.scrollY;
        let theDummies = doc.getElementsByClassName("wasDummy");
        if (theDummies) {
          Array.from(theDummies).forEach(function(d) {
            d.classList.add("dummy");
            d.classList.remove("wasDummy");
            d.innerText = "";
          });
          contentWindow.scrollTo({top: scrollY, behavior: 'auto'});
        }
        this.previewGenPageNavbarMissingNav?.classList.add("d-none");
        doc.querySelector(".liml-navbar-title").classList.remove("d-none")
      }
    }
  }
  setCountsForMissingValues() {
    this.nbrNoValue = 0;
    this.nbrNotFound = 0;
    if (this.previewIframe && this.previewIframe.contentWindow) {
      const contentWindow = this.previewIframe.contentWindow;
      const doc = contentWindow.document;
      if (doc) {
        const noValue = doc.querySelectorAll('[data-status="NoValue"]');
        if (noValue) {
          this.nbrNoValue = noValue.length;
        }
        const notFound = doc.querySelectorAll('[data-status="NotFound"]');
        if (notFound) {
          this.nbrNotFound = notFound.length;
        }
        this.previewGenPageNavbarIcons = doc.querySelector("body > div.liml-navbar > span.inlineIcons");
        this.previewGenPageNavbarMissingNav = doc.getElementById("preview-missing");
        if ((noValue && noValue.length > 0) || (notFound && notFound.length > 0)) {
          if (this.previewGenPageNavbarIcons) {
            if (this.previewGenPageNavbarMissingNav) {
              const previewNotFound = doc.getElementById("preview-nbr-not-found");
              if (previewNotFound) {
                previewNotFound.innerText = this.nbrNotFound;
              }
              const previewNoValue = doc.getElementById("preview-nbr-no-value");
              if (previewNoValue) {
                previewNoValue.innerText = this.nbrNoValue;
              }
              this.previewGenPageNavbarMissingNav?.classList.remove("d-none");
            } else {
              const missingSpanNav = doc.createElement("span");
              missingSpanNav.setAttribute("id", "preview-missing");
              missingSpanNav.innerHTML = `
                <span id=preview-not-found>
                     <span style="color: gold;">Not Found: <span id="preview-nbr-not-found">${this.nbrNotFound}</span></span>&nbsp;
                     <svg id="previewNotFoundScrollDown" class="bi text-light" width="22" height="22" fill="currentColor" role="img"><use xlink:href="img/bootstrap-icons.svg#arrow-down-square"></use></svg>
                &nbsp;
                     <svg id="previewNotFoundScrollUp" class="bi text-light" width="22" height="22" fill="currentColor" role="img"><use xlink:href="img/bootstrap-icons.svg#arrow-up-square"></use></svg>
                </span>
                &nbsp;
                <span id=preview-no-value>
                     <span style="color: gold;">No Value: <span id="preview-nbr-no-value">${this.nbrNoValue}</span></span>&nbsp;
                     <svg id="previewNoValueScrollDown" class="bi text-light" width="22" height="22" fill="currentColor" role="img"><use xlink:href="img/bootstrap-icons.svg#arrow-down-square"></use></svg>
                &nbsp;
                     <svg id="previewNoValueScrollUp" class="bi text-light" width="22" height="22" fill="currentColor" role="img"><use xlink:href="img/bootstrap-icons.svg#arrow-up-square"></use></svg>
                </span>
                <span id="preview-showing" class="d-none">
                     <span style="color: gold;">Showing: <span id="preview-nbr-index"></span>&nbsp;of&nbsp;<span id="preview-nbr-total"></span></span>&nbsp;
                </span>
              `;
              this.previewGenPageNavbarIcons?.insertAdjacentElement("afterend", missingSpanNav);
              this.previewGenPageNavbarMissingNav = doc.getElementById("preview-missing");
            }
            let nfDown = doc.getElementById("previewNotFoundScrollDown");
            if (nfDown) {
              nfDown.addEventListener("click", () => this.scrollPreviewNotFoundDown());
            }
            let nfUp = doc.getElementById("previewNotFoundScrollUp");
            if (nfUp) {
              nfUp.addEventListener("click", () => this.scrollPreviewNotFoundUp());
            }
            let nvDown = doc.getElementById("previewNoValueScrollDown");
            if (nvDown) {
              nvDown.addEventListener("click", () => this.scrollPreviewNoValueDown());
            }
            let nvUp = doc.getElementById("previewNoValueScrollUp");
            if (nvUp) {
              nvUp.addEventListener("click", () => this.scrollPreviewNoValueUp());
            }
            this.previewGenPageNavbarMissingNav?.classList.remove("d-none");
            this.previewGenPageShowingSpan = doc.getElementById("preview-showing");
            this.previewGenPageShowingIndex = doc.getElementById("preview-nbr-index");
            this.previewGenPageShowingTotal = doc.getElementById("preview-nbr-total");
          }
        } else {
          if (this.previewGenPageNavbarMissingNav) {
            this.previewGenPageNavbarMissingNav?.classList.add("d-none");
          }
        }
      }
    }
  }
  setRowFor(span) {
    this.rowToScrollTo = "";
    if (span) {
      let theRow = span.closest("div.versions-container");
      if (theRow) {
        let id = theRow.getAttribute("id");
        if (id) {
          this.rowToScrollTo = id;
        }
      }
    }
  }
  scrollToRow(id) {
    if (this.previewIframe && this.previewIframe.contentWindow) {
      const contentWindow = this.previewIframe.contentWindow;
      const doc = contentWindow.document;
      if (doc) {
        let theRow = doc.getElementById(id);
        if (theRow) {
          const offset = this.getCoords(theRow);
          if (offset) {
            this.previewIframe.contentWindow.scrollTo({
              left: offset.left,
              top: offset.top - 25, //the 25 should compensate for the bar
              behavior: 'instant'
            });
          }
        } else {
          console.log(`scrollToRow - row not found`);
        }
      }
    } else {
      console.log(`scrollToRow - this.previewIframe null`);
    }
 }
  getCoords(elem) {
    if (this.previewIframe && this.previewIframe.contentWindow) {
      let box = elem.getBoundingClientRect();
      return {
        top: box.top + this.previewIframe.contentWindow.pageYOffset,
        right: box.right + this.previewIframe.contentWindow.pageXOffset,
        bottom: box.bottom + this.previewIframe.contentWindow.pageYOffset,
        left: box.left + this.previewIframe.contentWindow.pageXOffset
      };
    }
  }
  setCurrentMissingHighlight(c) {
    if (this.previewGenPageCurrentMissing) {
       this.previewGenPageCurrentMissing.style.backgroundColor = c;
    }
  }
  scrollPreviewNotFoundDown() {
    if (this.previewIframe && this.previewIframe.contentWindow) {
      const doc = this.previewIframe.contentWindow.document;
      if (doc) {
        if (this.previewGenPageNavbarMissingNav) {
          this.setCurrentMissingHighlight("");
          let missingId = "";
          if (this.previewGenPageNotFoundIndex < this.nbrNotFound) {
            this.previewGenPageNotFoundIndex = this.previewGenPageNotFoundIndex + 1;
          } else { // wrap to top
            this.previewGenPageNotFoundIndex = 1;
          }
          missingId = `preview-not-found-${this.previewGenPageNotFoundIndex}`;
          let missingSpan = doc.getElementById(missingId);
          if (missingSpan) {
            let parentRow = missingSpan.closest("div.versions-container");
            if (parentRow) {
              parentRow.style.display = "flex";
            }
            this.previewGenPageCurrentMissing = missingSpan;
           missingSpan.style.backgroundColor = this.highlight;
            const offset = this.getCoords(missingSpan);
            if (offset) {
              this.previewIframe.contentWindow.scrollTo(offset.left, offset.top-100);
            }
            this.showIndex("NotFound");
          }
        }
      }
    }
  }
  scrollPreviewNotFoundUp() {
    if (this.previewIframe && this.previewIframe.contentWindow) {
      const doc = this.previewIframe.contentWindow.document;
      if (doc) {
        if (this.previewGenPageNavbarMissingNav) {
          this.setCurrentMissingHighlight("");
          let missingId = "";
          if (this.previewGenPageNotFoundIndex < 2) {
            this.previewGenPageNotFoundIndex = this.nbrNotFound; // wrap back to bottom
          } else {
            this.previewGenPageNotFoundIndex = this.previewGenPageNotFoundIndex - 1;
          }
          missingId = `preview-not-found-${this.previewGenPageNotFoundIndex}`;
          let missingSpan = doc.getElementById(missingId);
          if (missingSpan) {
            let parentRow = missingSpan.closest("div.versions-container");
            if (parentRow) {
              parentRow.style.display = "flex";
            }
            this.previewGenPageCurrentMissing = missingSpan;
            missingSpan.style.backgroundColor = this.highlight;
            const offset = this.getCoords(missingSpan);
            if (offset) {
              this.previewIframe.contentWindow.scrollTo(offset.left, offset.top-100);
            }
            this.showIndex("NotFound");
          }
        }
      }
    }
  }
  scrollPreviewNoValueDown() {
    if (this.previewIframe && this.previewIframe.contentWindow) {
      const doc = this.previewIframe.contentWindow.document;
      if (doc) {
        if (this.previewGenPageNavbarMissingNav) {
          this.setCurrentMissingHighlight("");
          let missingId = "";
          if (this.previewGenPageNoValueIndex < this.nbrNoValue) {
            this.previewGenPageNoValueIndex = this.previewGenPageNoValueIndex + 1;
          } else { // wrap to top
            this.previewGenPageNoValueIndex = 1;
          }
          missingId = `preview-no-value-${this.previewGenPageNoValueIndex}`;
          let missingSpan = doc.getElementById(missingId);
          if (missingSpan) {
            let parentRow = missingSpan.closest("div.versions-container");
            if (parentRow) {
              parentRow.style.display = "flex";
            }
            this.previewGenPageCurrentMissing = missingSpan;
            missingSpan.style.backgroundColor = this.highlight;
            const offset = this.getCoords(missingSpan);
            if (offset) {
              this.previewIframe.contentWindow.scrollTo(offset.left, offset.top-100);
            }
            this.showIndex("NoValue");
          }
        }
      }
    }
  }
  scrollPreviewNoValueUp() {
    if (this.previewIframe && this.previewIframe.contentWindow) {
      const doc = this.previewIframe.contentWindow.document;
      if (doc) {
        if (this.previewGenPageNavbarMissingNav) {
          this.setCurrentMissingHighlight("");
          let missingId = "";
          if (this.previewGenPageNoValueIndex < 2) {
            this.previewGenPageNoValueIndex = this.nbrNoValue; // wrap back to bottom
          } else {
            this.previewGenPageNoValueIndex = this.previewGenPageNoValueIndex - 1;
          }
          missingId = `preview-no-value-${this.previewGenPageNoValueIndex}`;
          let missingSpan = doc.getElementById(missingId);
          if (missingSpan) {
            let parentRow = missingSpan.closest("div.versions-container");
            if (parentRow) {
              parentRow.style.display = "flex";
            }
            this.previewGenPageCurrentMissing = missingSpan;
            missingSpan.style.backgroundColor = this.highlight;
            const offset = this.getCoords(missingSpan);
            if (offset) {
              this.previewIframe.contentWindow.scrollTo(offset.left, offset.top-100);
            }
            this.showIndex("NoValue");
          }
        }
      }
    }
  }
  showIndex(status) {
    if (this.previewGenPageShowingIndex) {
      if (status === "NotFound") {
        this.previewGenPageShowingIndex.innerText = this.previewGenPageNotFoundIndex;
      } else {
        this.previewGenPageShowingIndex.innerText = this.previewGenPageNoValueIndex;
      }
    }
    if (this.previewGenPageShowingTotal) {
      if (status === "NotFound") {
        this.previewGenPageShowingTotal.innerText = this.nbrNotFound;
      } else {
        this.previewGenPageShowingTotal.innerText = this.nbrNoValue;
      }
    }
    if (this.previewGenPageShowingSpan) {
      this.previewGenPageShowingSpan?.classList.remove("d-none");
    }
  }
  hideIndex() {
    if (this.previewGenPageShowingSpan) {
      this.previewGenPageShowingSpan?.classList.add("d-none");
    }
  }
  showMissingValuesInPreview() {
    if (!this.previewShowMissing) {
      return;
    }
    if (this.previewIframe && this.previewIframe.contentWindow) {
      const contentWindow = this.previewIframe.contentWindow;
      const doc = contentWindow.document;
      if (doc) {
        let theDummies = doc.getElementsByClassName("dummy");
        if (theDummies) {
          const scrollY = contentWindow.scrollY;
          let dummiesArray = Array.from(theDummies);
          const l = dummiesArray.length;
          let nbrNotFound = 0;
          let nbrNoValue = 0;
          for (let i = 0; i < l; ++i) {
            let d = dummiesArray[i];
            d.classList.add("wasDummy");
            d.classList.remove("dummy");
            let status = d.getAttribute("data-status");
            let id = d.getAttribute("data-id-used");
            if (id && id.length > 0) {
              // do nothing
            } else {
              let redirects = d.getAttribute("data-id-redirects");
              if (redirects && redirects.length > 0) {
                id = utils.getLastFromTrace(redirects);
              } else {
                id = "unknown"
              }
            }
            if (status === "NotFound") {
              nbrNotFound = nbrNotFound + 1;
              d.setAttribute("id", `preview-not-found-${nbrNotFound}`)
            } else if (status === "NoValue") {
              nbrNoValue = nbrNoValue + 1;
              d.setAttribute("id", `preview-no-value-${nbrNoValue}`)
            }
            d.innerText = status + ": " + id;
          }
          contentWindow.scrollTo({top: scrollY, behavior: 'auto'});
          this.setCountsForMissingValues();
          let title = doc.querySelector("span.liml-navbar-title")
          if (title) {
            title.classList.add("d-none")
          }
        }
      }
    }
  }

  previewGetScrolledToDiv() {
    const contentWindow = this.previewIframe.contentWindow;
    const doc = contentWindow.document;
    const divs = doc.querySelectorAll('.versions-container');
    let closestDiv = null;
    let closestDistance = Infinity;

    divs.forEach(div => {
      const rect = div.getBoundingClientRect();
      // Check the distance from the top of the viewport
      if (rect.top >= 0 && rect.top < closestDistance) {
        closestDistance = rect.top;
        closestDiv = div;
      }
    });

return closestDiv;
  }

  hideTemplatesInPreview() {
    if (this.previewIframe && this.previewIframe.contentWindow) {
      const contentWindow = this.previewIframe.contentWindow;
      const doc = contentWindow.document;
      const nearest = this.previewGetScrolledToDiv()
      if (doc) {
        let theTemplateRows = doc.getElementsByClassName("templateUsed");
        if (theTemplateRows) {
          const scrollY = contentWindow.scrollY;
          Array.from(theTemplateRows).forEach(function(d) {
            d.classList.add("d-none");
          });
          //contentWindow.scrollTo({top: scrollY, behavior: 'auto'});
          nearest.scrollIntoView();
        }
      }
    }
  }
  showTemplatesInPreview() {
    if (this.previewIframe && this.previewIframe.contentWindow) {
      const contentWindow = this.previewIframe.contentWindow;
      const doc = contentWindow.document;
      const nearestDiv = this.previewGetScrolledToDiv()
      if (doc) {
        let theTemplateRows = doc.getElementsByClassName("templateUsed");
        if (theTemplateRows) {
          //const scrollY = contentWindow.scrollY;
          Array.from(theTemplateRows).forEach(function(d) {
            d.classList.remove("d-none");
          });
          nearestDiv.scrollIntoView()
        }
      }
    }
  }
  previewScrollToTemplateLine() {
    let loc = this.editor.getSelection().getCursor();
    let line = loc.row
    const contentWindow = this.previewIframe.contentWindow;
    const doc = contentWindow.document;
    let div = doc.querySelector('div.versions-container[data-template-id="1"][data-template-line="'+line+'"]');
    if (!div){
      //TODO: extend to support 'insert statements' by using the meta
      let rawLine = this.editor.session.getLine(line)
      let magic = /^insert\s+"([^"]+)"$/;
      // Test if the line matches the pattern
      let match = rawLine.match(magic);
      if (!match) {
        return;
      }
      let path = match[1]
      let correspondingMeta = doc.querySelectorAll('meta[name="lml"]');
      const metaTag = Array.from(correspondingMeta)
          .find(meta => meta.content.includes(path));
      if (!metaTag) {
        return;
      }
      // Extract the content
      let content = metaTag.content;
      // Split the content by colon and get the first part
      let prefix = content.split(':')[0].trim();

      div = doc.querySelector('div.versions-container[data-template-id="'+parseInt(prefix)+'"]');
      if (!div) {
        return;
      }

      div.scrollIntoView();
    }
    div.scrollIntoView();
  }
  // handleComboToolClick renders the preview if the combo index changed
  handleComboToolClick() {
    // this gets called through propagation of click event
    // in generation-layouts.js when a layout is selected from a list.
    if (this.toolsIframeLibraryComboTool) {
      const libraryComboIndex = this.toolsIframeLibraryComboTool.getAttribute("selected");
      if (libraryComboIndex && libraryComboIndex.length > 0) {
        if (this.previewLibraryCombo !== libraryComboIndex) {
          this.previewLibraryCombo = libraryComboIndex;
          localStorage.setItem(this.selectedLibraryComboKey, this.previewLibraryCombo);
          if (this.src && this.src.length > 0) {
            this.handlePreview();
          }
        }
      }
    }
  }
  // handleComboToolSave re-renders
  handleComboToolSave() {
    if (this.toolsIframeLibraryComboTool) {
      this.previewLibraryCombo = this.toolsIframeLibraryComboTool.getAttribute("selected");
      localStorage.setItem(this.selectedLibraryComboKey, this.previewLibraryCombo);
      if (this.src && this.src.length > 0) {
        this.handlePreview();
      }
    }
  }
  savePreferences() {
    localStorage.setItem('previewDate', this.previewDate);
    localStorage.setItem('previewLibraryCombo', this.previewLibraryCombo);
    localStorage.setItem('theme', this.theme);
    localStorage.setItem('wrap', this.wrap);
  }
  readPreferences() {
    // font
    let value = null;
    value = localStorage.getItem('font');
    if (!value) {
      this.font = 14;
    } else {
      try {
        this.font = JSON.parse(value);
      } catch (error) {
        alert(`866 ${error?.message}`)
      }
    }
    // preview date
    this.previewDate = localStorage.getItem('previewDate');
    if (! this.previewDate || this.previewDate.length === 0) {
      this.previewDate = utils.getToday();
    }
    if (this.toolsIframeLdpTool) {
      this.toolsIframeLdpTool.setAttribute("date", this.previewDate);
    }
    // preview library combo index
    this.previewLibraryCombo = localStorage.getItem('previewLibraryCombo');
    if (! this.previewLibraryCombo || this.previewLibraryCombo.length === 0) {
      this.previewLibraryCombo = "0";
    }
    if (this.toolsIframeLibraryComboTool) {
      this.toolsIframeLibraryComboTool.setAttribute("selected", this.previewLibraryCombo);
    }

    // editor theme
    this.theme = localStorage.getItem('theme');
    if (! this.theme || this.theme.length === 0) {
      this.theme = "ace/theme/twilight";
    } else if (this.theme === "light") {
      // Convert simple "light" theme to a valid Ace theme
      this.theme = "ace/theme/eclipse";
    } else if (this.theme === "dark") {
      // Convert simple "dark" theme to a valid Ace theme
      this.theme = "ace/theme/twilight";
    } else if (!this.theme.startsWith("ace/theme/")) {
      // If theme doesn't have the proper prefix, default to twilight
      this.theme = "ace/theme/twilight";
    }
    // editor wrap
    value = localStorage.getItem('wrap');
    if (!value) {
      this.wrap = false;
    } else {
      try {
        this.wrap = JSON.parse(value) === true;
      } catch (error) {
        alert(`900 ${error?.message}`)
      }
    }
    this.toggleWrapMenuItem();
  }

  /**
   * Initializes the Ace code editor with appropriate settings
   * Sets up syntax highlighting, keyboard shortcuts, and auto-completion
   */
  initAceEditor() {
    if (this.editor !== null) {
      this.editor.destroy();
    }
    this.editor = ace.edit("editor");
    console.log(`initAceEditor - this.editor theme: ${this.theme}`);
    this.editor.setTheme(this.theme);
    console.log(`initAceEditor - this.editor theme set to: ${this.editor.getTheme()}`);
    this.editor.container.style.lineHeight = this.lineheight;
    switch (this.lang) {
      case "css":
        this.editor.session.setMode("ace/mode/css");
        break;
      case "js":
        this.editor.session.setMode("ace/mode/javascript");
        break;
      case "lml":
        this.editor.session.setMode("ace/mode/lml");
        break;
      default:
        alert(`invalid language type: ${this.lang}`)
    }
    this.editor.setOptions({
      fontSize: this.font,
      maxLines: 30,
      minLines: 10,
      enableSnippets: true,
      tabSize: 2,
      useSoftTabs: true
    });
    this.editor.container.style.height = "100vh";
    this.editor.setOption("wrap", this.wrap);
    this.editor.setAutoScrollEditorIntoView(true);
    this.editor.setShowPrintMargin(false);
    this.editor.renderer.updateFontSize();
    const snippetManager = ace.require('ace/snippets').snippetManager;
    // Below does not seem to work.
    // Research this instead: https://codesandbox.io/s/react-acesnippets-sk2uq?file=/src/App.js
    // See also https://cloud9-sdk.readme.io/docs/snippets
    const snippetContent = `
        # scope: lml
        snippet hello
        <p>Hello, \${1:name}!</p>
    `;
    const snippets = snippetManager.parseSnippetFile(snippetContent);
    snippetManager.register(snippets, 'lml');

    // bind keyboard shortcut for jump to preview
    this.editor.commands.addCommand({
      name: 'jump',
      bindKey: {win: "Ctrl-J", "mac": "Cmd-J"},
      exec: (editor) => {
        this.previewScrollToTemplateLine();
      },
      readOnly: true
    });
    // bind keyboard shortcut for save
    this.editor.commands.addCommand({
      name: 'save',
      bindKey: {win: "Ctrl-S", "mac": "Cmd-S"},
      exec: (editor) => {
        this.handleFileUpdate();
      }
    });
    // bind keyboard shortcut for preview
    this.editor.commands.addCommand({
      name: 'preview',
      bindKey: {win: "Ctrl-P", "mac": "Cmd-P"},
      exec: (editor) => {
        this.handlePreview();
      }
    });

    if (this.lang === "lml") {
      this.editor.commands.addCommand({
        name: "triggerAutoComplete",
        bindKey: { win: "Ctrl-;", mac: "Ctrl-;" },
        exec: function (editor) {
          editor.execCommand('startAutocomplete');
        }
      });
      this.editor.setOption("enableBasicAutocompletion", [{
        getCompletions: (editor, session, pos, prefix, callback) => {
          callback(null, JSON.parse(this.completions));
        },
      }]);
      this.editor.setOption("enableLiveAutocompletion", true);
    } else {
      this.editor.setOption("enableBasicAutocompletion", true);
      this.editor.setOption("enableLiveAutocompletion", true);
    }
    this.editor.on("input", () => {
      if (this.editor.session.getUndoManager().isClean()) {
        this.saveBtn.classList.add("disabled");
      } else {
        this.saveBtn.classList.remove("disabled");
      }
    });
    this.editor.on("dblclick", () => {
      if (this.editor) {
        if (this.editor.session) {
          this.handleLoadInsertedTemplate(this.editor.session.getLine(this.editor.getSelectionRange().start.row));
        }
      }
    });
    this.editor.resize();
  }

  handleLoadInsertedTemplate(id) {
    id = id.trim(id)
    if (id.startsWith("insert ")) {
      if (id.length > 7) {
        id = id.substring(7);
      }
      if (id.startsWith("\"")) {
        id = id.substring(1);
      }
      if (id.endsWith("\"")) {
        id = id.substring(0, id.length-1);
      }
      let date = "";
      if (this.date && this.date.length > 0 && this.date !== "null") {
        date = this.date;
      }
      const url = this.host
           + `ide`
           + `?id=` + encodeURIComponent(id)
          + `&date=` + date
      ;
     this.openInNewTab(url);
    }
  }
  openInNewTab(href) {
    Object.assign(document.createElement('a'), {
      target: '_blank',
      rel: 'noopener noreferrer',
      href: href,
    }).click();
  }
  
  /**
   * Forces the editor to completely refresh to properly apply theme changes
   * This addresses the visual glitch that occurs when changing themes
   */
  refreshEditor() {
    // Store current content and cursor position
    const content = this.editor.getValue();
    const cursorPosition = this.editor.getCursorPosition();
    const scrollTop = this.editor.session.getScrollTop();
    
    // Force a complete re-render of the editor
    this.editor.renderer.updateFull(true);
    
    // Additional force refresh using setTimeout to ensure it's applied after the current event loop
    setTimeout(() => {
      this.editor.resize(true);
      this.editor.renderer.updateFull(true);
      
      // Restore cursor position and scroll position
      this.editor.moveCursorToPosition(cursorPosition);
      this.editor.session.setScrollTop(scrollTop);
      
      // Force focus to trigger a complete redraw
      this.editor.focus();
    }, 10);
  }
  handleNewFile() {
    if (this.hasUnsavedChanges()) {
      if (confirm("You have unsaved changes.  Click OK to discard them.")) {
        // fall through
      } else {
        return;
      }
    }
    this.setAlert("");
    this.initAceEditor();
    this.showModalFileSelect("Template File Explorer")
  }
  handleCloseFile() {
    if (this.hasUnsavedChanges()) {
      if (confirm("You have unsaved changes.  Click OK to discard them.")) {
        // fall through
      } else {
        return;
      }
    }
    this.setAlert("");
    this.disableFileActions();
    this.resetIde();
  }
  handleFileCopy(ev) {
    // do not do this -> ev.stopPropagation();
    this.editor.session.getUndoManager().markClean();
    this.saveBtn.disabled = this.editor.session.getUndoManager().isClean();
    this.copyFileContent().then(response => {
      this.src = response.ID;
      this.filePath = response.FilePath;
      alert(`copied to ${response.FilePath}`)
      this.loadContent();
    }).catch(error => {
      this.setAlert(error.message);
    });
    this.editor.session.getUndoManager().markClean();
    this.saveBtn.classList.add("disabled");
  }

  hasUnsavedChanges() {
    return ! this.editor.session.getUndoManager().isClean();
  }
  resetIde() {
    this.src = [];
    this.filePath = "";
    this.text = "";
    this.previewIframe.setAttribute("src", "");
    this.initAceEditor();
  }
  // saveWithNewId uses the template file src (its path) to replace the ID in the template code, then saves it.
  saveWithNewId(src) {
    let text = this.editor.getValue();
    let lines = [];
    lines = text.split("\n");
    if (lines.length === 0) {
      return;
    }
    // src is a json string array
    // convert it to an object
    let srcArray = null;
    try {
      srcArray = JSON.parse(src);
    } catch (error) {
      alert(`1122 ${error?.message}`)
    }

    // replace the current id in source code with the new one
    // TODO: see if there is a way to replace the first line directly in the editor...
    lines[0] = `id = "${srcArray.join("/")}"`
    text = lines.join("\n");
    this.editor.getSession().setValue(text, -1);
    // save it under the new name
    this.handleFileUpdate();
  }
  handleDelete() {
    if (confirm("Are you sure you want to delete this template.  Click OK to delete it.")) {
      if (this.src.length > 0) {
        this.deleteFile().then(response => {
          if (response.Status && response.Status !== "OK") {
            alert(`401 ${response.Status}: ${response.Message}`)
          }
          this.setAlert(response.Message);
          this.resetIde();
        }).catch(error => {
          this.setAlert(error.message);
        });
      }
    }
  }
  handleModalFileSelectCancel() {
    this.modalFileSelect.setAttribute("title", "");
    this.modalFileSelect.setAttribute("text", "");
    // hide the modal by adding class name d-none, which makes it invisible
    this.modalFileSelect.classList.add("d-none");
    this.handlingSaveAs = false;
  }

  handleModalFileSelectFileSelected() {
    let src = this.modalFileSelect.getAttribute("src");
    if (src && src.length > 0) {
      this.src = src;
    }
    // hide the modal by adding class name d-none, which makes it invisible
    this.modalFileSelect.classList.add("d-none");
    this.loadContent();
  }
  setReadOnlyMessage() {
    if (this.readOnly) {
      this.readOnlyMessage = "Read only: ";
    } else {
      this.readOnlyMessage = "";
    }
  }
  loadContent() {
    this.disableFileActions();
    if (this.src.length > 0) {
      if (this.handlingSaveAs) {
        // update the selected file with current file contents
        // after modifying the id line
        this.saveWithNewId(this.src)
      } else { // open the selected file
        this.fetchFileContent(this.src).then(response => {
          if (response.Status === "OK") {
            if (response.ReadOnly) {
              this.readOnly = response.ReadOnly;
            } else {
              this.readOnly = false;
            }
            this.setReadOnlyMessage();
            this.enableFileActions();
            this.editor.setReadOnly(this.readOnly);
            this.text = response.Value;
            if (response.ID && response.ID.length > 0) {
              this.src = response.ID;
              try {
                this.currentDirPath = this.src.split(utils.PathSeparator());
              } catch (error) {
                alert(`1192 ${error?.message}`)
              }
              this.currentDirPath.pop();
            } else {
              this.currentDirPath = [];
            }
            this.filePath = response.FilePath;
            window.parent.document.title = response.Title
            this.editor.getSession().setValue(this.text, -1);
            this.editor.scrollToLine(localStorage.getItem(this.filePath+".line"));
            this.previewDiv?.scrollTo(localStorage.getItem(this.filePath+"previewDiv"));
            this.editor.resize();
            this.handlingSaveAs = false;
            let msg = this.readOnlyMessage;
            if (this.filePath) {
              msg = msg + this.filePath
            }
            this.setAlert(msg);
            this.handlePreview();
          } else {
            if (response && response.Status) {
              alert(`445 ${response.Status}: ${response.Message}`)
              this.setAlert(response.Status);
            }
          }
        }).catch(error => {
          this.setAlert(error.message);
        });
      }
      setTimeout(() => {
        let msg = this.readOnlyMessage;
        if (this.filePath) {
          msg = msg + this.filePath
        }
        this.setAlert(msg);

      }, 5000);
    } else { // this file was deleted
      this.text = "";
      this.src = [];
      this.filePath = "";
      this.initAceEditor();
    }
    this.handlingSaveAs = false;
    this.editor.resize();
}
  showModalFileSelect(title) {
    this.modalFileSelect.setAttribute("currentFile", this.src);
    if (this.currentDirPath && this.currentDirPath.length > 0) {
      this.modalFileSelect.setAttribute("path", JSON.stringify(this.currentDirPath));
    }
    this.modalFileSelect.setAttribute("src", this.src);
    this.modalFileSelect.setAttribute("title", title);
    this.modalFileSelect.setAttribute("text", "this.modalFileSelectText");
    // show the modal by removing class name d-none, which had made it invisible
    this.modalFileSelect.classList.remove("d-none");
  }

  handleSaveAs() {
    this.handlingSaveAs = true;
    // we are going to pass the template contents to the modal file select.
    // if the user creates a new template through the modal, the current template's contents will be saved in it.
    this.modalFileSelectText = this.editor.getValue();
    this.showModalFileSelect("Template File Explorer");
    this.editor.session.getUndoManager().markClean();
  }

  /**
   * Handles the preview generation by parsing the current editor content
   * Creates a preview in the preview pane and updates template information
   * Only works with LML content and requires a valid source file
   */
  handlePreview() {
    if (this.waitingForParseResult) {
      return
    }

    if (this.lang !== "lml") {
      return;
    }
    if (!this.toolsIframeLdpTool || this.src.length === 0) {
      return;
    }
    // const ldpDate = this.toolsIframeLdpTool.getAttribute("date");
    // if (ldpDate && ldpDate.length > 0) {
    //   this.previewDate = ldpDate;
    //   localStorage.setItem("previewDate", this.previewDate);
    // }
    if (this.toolsIframeLibraryComboTool) {
      const libraryComboIndex = this.toolsIframeLibraryComboTool.getAttribute("selected");
      if (libraryComboIndex && libraryComboIndex.length > 0) {
        this.previewLibraryCombo = libraryComboIndex;
        localStorage.setItem(this.selectedLibraryComboKey, this.previewLibraryCombo);
      }
    }
    this.editor.getSession().clearAnnotations();
    this.waitingForParseResult = true
    this.fetchFileParse().then(response => {
      if (response.Status !== "OK") {
        alert(`502 ${response.Status}: ${response.Message}`)
      }
      this.setAlert(response.Message);
      this.previewIframe.setAttribute("src", response.HtmlPath);
      if (response.ParseErrors) {
        this.setAlert("There were errors.  Check lines with red box.")
        let errors = [];
        response.ParseErrors.forEach((item, index) => {
          const line = item.Line - 1;
          const col = item.Column + 1;
          let msg = `     ${item.Line}:${col} - `;
          if (item.Message.startsWith("mismatched input")) {
            msg += "syntax error";
          } else if (item.Message.includes("mismatched input")) {
            const parts = item.Message.split("mismatched input");
            msg += parts[0].replaceAll("{", "");
          } else {
            msg += item.Message
          }
          errors.push({row: line, column: col, text: msg, type: "error"})
        });
        this.editor.getSession().setAnnotations(errors);
      } else {
        if (response.IsBlock) {
          this.isBlock = response.IsBlock;
        }

        this.templatesByNbr = response.TemplatesByNbr;
        this.templatesTableData = response.TemplatesTableData;
        if (this.templatesTableData) {
         this.toolsIframeTemplatesUsedTable.classList.remove("d-none");
         this.toolsIframeTemplatesUsedTable.setAttribute("data", JSON.stringify(this.templatesTableData));
        } else {
          this.toolsIframeTemplatesUsedTable.setAttribute("data", "");
        }
        if (response.Date && response.Date.length > 0) {
          this.date = response.Date;
          this.previewDate = this.date;
          localStorage.setItem("previewDate", this.previewDate);
          this.toolsIframeLdpTool.setAttribute("date", this.date);
        }
        this.setAlert(response.Message);
      }
      setTimeout(() => {
        let msg = this.readOnlyMessage;
        if (this.filePath) {
          msg = msg + this.filePath
        }
        this.setAlert(msg);
      }, 5000)
    }).catch(error => {
      this.setAlert(error.message);
    });
    this.waitingForParseResult = false
  }
  disableFileActions() {
    if (this.lang === "lml") {
      this.closeBtn.classList.add("disabled");
      if (this.copyBtn) {
        this.copyBtn.classList.add("disabled");
      }
      this.deleteBtn.classList.add("disabled");
      this.saveBtn.classList.add("disabled");
      this.saveAsBtn.classList.add("disabled");
      this.previewShowTemplatesBtn.classList.add("disabled");
      this.previewShowMissingBtn.classList.add("disabled");
    }
  }
  enableFileActions() {
    if (this.lang === "lml") {
      this.closeBtn.classList.remove("disabled");
      if (this.readOnly) {
        if (this.copyBtn) {
          this.copyBtn.classList.remove("disabled");
        }
      } else {
        this.deleteBtn.classList.remove("disabled");
        this.saveBtn.classList.remove("disabled");
        this.saveAsBtn.classList.remove("disabled");
      }
      this.previewShowTemplatesBtn.classList.remove("disabled");
      this.previewShowMissingBtn.classList.remove("disabled");
    }
  }
  changeEditor(evt) {
    evt.preventDefault();
    let item = evt.target.innerText;
    switch (item) {
      case "Decrease Font Size": {
        this.updateFont(this.font-2, this.defaultLineHeight);
        break;
      }
      case "Normal-Sized Font": {
        this.updateFont(this.defaultFontSize, this.defaultLineHeight);
        break;
      }
      case "Increase Font Size": {
        this.updateFont(this.font+2, this.defaultLineHeight);
        break;
      }
      case "Dark Theme" : {
        this.theme = "ace/theme/twilight";
        localStorage.setItem("theme", this.theme);
        this.editor.setTheme(this.theme);
        this.refreshEditor();
        break;
      }
      case "Light Theme" : {
        this.theme = "ace/theme/eclipse";
        localStorage.setItem("theme", this.theme);
        this.editor.setTheme(this.theme);
        this.refreshEditor();
        break;
      }
      case this.wrapOn : {
        this.wrap = true;
        this.editor.setOption("wrap", this.wrap);
        localStorage.setItem("wrap",this.wrap);
        this.toggleWrapMenuItem();
        break;
      }
      case this.wrapOff : {
        this.wrap = false;
        this.editor.setOption("wrap", this.wrap);
        localStorage.setItem("wrap",this.wrap);
        this.toggleWrapMenuItem();
        break;
      }
      case this.showTemplatesBtnOn : {
        this.togglePreviewShowTemplates();
        break;
      }
      case this.showTemplatesBtnOff : {
        this.togglePreviewShowTemplates();
        break;
      }
      case this.previewShowMissingOn : {
        this.togglePreviewShowMissing();
        break;
      }
      case this.previewShowMissingOff : {
        this.togglePreviewShowMissing();
        break;
      }
      case "Fold All" : {
        window.parent.document.querySelector("#foldBtn").innerText = "Unfold All";
        this.editor.execCommand("foldall");
        break;
      }
      case "Unfold All" : {
        window.parent.document.querySelector("#foldBtn").innerText = "Fold All";
        this.editor.execCommand("unfoldall");
        break;
      }
      case "Scroll Preview to Line": {
        this.previewScrollToTemplateLine();
        break;
      }
    }
  }
  updateFont(size, height) {
    this.font = size;
    this.lineheight = height;
    this.editor.container.style.lineHeight = this.lineheight;
    this.editor.setFontSize(this.font);
    localStorage.setItem("font", this.font);
  }
  toggleWrapMenuItem() {
    if (this.wrap) {
      window.parent.document.querySelector("#lineWrapBtn").innerText = this.wrapOff;
    } else {
      window.parent.document.querySelector("#lineWrapBtn").innerText = this.wrapOn;
    }
  }
  toggleRightTopVisible(ev) {
    this.toolsDivVisible = !this.toolsDivVisible;
    if (this.toolsDivVisible) {
      this.toolsDiv.classList.remove("d-none");
      if (ev) {
        ev.target.innerText = this.hide;
      }
    } else {
      this.toolsDiv.classList.add("d-none");
      if (ev) {
        ev.target.innerText = this.show;
      }
    }
    this.toggleIdeSeparators();
  }
  toggleRightBottomVisible(ev) {
    this.previewDivVisible = !this.previewDivVisible;
    if (this.previewDivVisible) {
      this.previewDiv.classList.remove("d-none");
      if (ev) {
        ev.target.innerText = this.hide;
      }
    } else {
      this.previewDiv.classList.add("d-none");
      if (ev) {
        ev.target.innerText = this.show;
      }
    }
    this.toggleIdeSeparators();
  }
  toggleIdeSeparators() {
    if (this.toolsDivVisible && this.previewDivVisible) {
      this.toolsDiv.style.height = "50vh";
      this.previewDiv.style.height = "50vh";
      this.ideHorizontalSeparator.classList.remove("d-none");
    } else {
      if (this.toolsDivVisible) {
        this.toolsDiv.style.height = "100vh";
      }
      if (this.previewDivVisible) {
        this.previewDiv.style.height = "100vh";
      }
      this.ideHorizontalSeparator.classList.add("d-none");
    }
    if (!this.toolsDivVisible && !this.previewDivVisible) {
      this.ideLeftPaneDiv.style.width = "100%";
      this.ideVerticalSeparator.classList.add("d-none");
    } else {
      this.ideLeftPaneDiv.style.width = "50%";
      this.ideVerticalSeparator.classList.remove("d-none");
    }
  }
  togglePreviewShowMissing() {
    if (!this.previewShowMissingBtn) {
      return;
    }
    this.previewShowMissing = !this.previewShowMissing;
    if (this.previewShowMissing) {
      this.previewShowMissingBtn.innerText = this.previewShowMissingOn;
      this.showMissingValuesInPreview();
    } else {
      this.previewShowMissingBtn.innerText = this.previewShowMissingOff;
      this.hideMissingValuesInPreview();
    }
  }
  static get observedAttributes() {
    return ['host', 'completions', 'lang', 'src', 'text'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
  }

  adoptedCallback() {
  }
  get host() {
    return this.getAttribute("host");
  }

  set host(value) {
    if (!value.endsWith("/")) {
      this.setAttribute("host", value + "/");
    } else {
      this.setAttribute("host", value);
    }
  }
  get completions() {
    return this.getAttribute("completions");
  }

  set completions(value) {
    this.setAttribute("completions", value);
  }
  get lang() {
    return this.getAttribute("lang");
  }

  set lang(value) {
    this.setAttribute("lang", value);
  }
  get role() {
    return this.getAttribute("role");
  }
  set role(value) {
    this.setAttribute("role", value);
  }
  get src() {
    return this.getAttribute("src");
  }
  set src(value) {
    this.setAttribute("src", value);
  }
  get text() {
    return this.getAttribute("text");
  }

  set text(value) {
    this.setAttribute("text", value);
  }

  /**
   * Handles saving the current file content to the server
   * Marks the editor as clean after saving and updates the preview
   * @param {Event} ev - The event that triggered this action
   */
  handleFileUpdate(ev) {
    localStorage.setItem(this.filePath+".line", this.editor.getSelection().cursor.row)
    this.editor.session.getUndoManager().markClean();
    this.saveBtn.disabled = this.editor.session.getUndoManager().isClean();
    this.saveFileContent().then(response => {
        this.src = response.ID;
        this.filePath = response.FilePath;
        if (response.ReadOnly) {
          this.readOnly = response.ReadOnly;
        } else {
          this.readOnly = false;
        }
        this.setReadOnlyMessage();
        // TODO: the api handler should return OK not updated and the message should be updated
        if (response.Status !== "OK" && response.Status !== "updated") {
          alert(`${response.Status}: ${response.Message}`);
        } else {
          this.setAlert(response.Message);
        }
        setTimeout(() => {
          let msg = this.readOnlyMessage;
          if (this.filePath) {
            msg = msg + this.filePath
          }
          this.setAlert(msg);
        }, 5000)
      this.handlePreview();
    }).catch(error => {
      this.setAlert(error.message);
    });
    this.editor.session.getUndoManager().markClean();
    this.saveBtn.classList.add("disabled");
  }

  /**
   * Fetches file content from the server for editing
   * @param {string} src - The file identifier/path to fetch
   * @returns {Promise<Object>} Response object with file content and metadata
   */
  async fetchFileContent(src) {

    const url = this.host
        + `api/${this.lang}`
        + `?src=` + encodeURIComponent(src)
    ;
    let fetchData = {
      method: 'GET'
    }

    const response = await fetch(url, fetchData);
    if (!response.ok) {
      let message = "";
      switch (response.status) {
        case 404: {
          message += "not found";
          break;
        }
        case 500: {
          message += "doxa server error";
          break;
        }
        default: {
          message += `An error has occurred: ${response.status}`;
        }
      }
      this.setAlert(message);
      throw new Error(message);
    }
    const responseJson = await response.json();
    return responseJson;
  }

  async copyFileContent() {

    const url = this.host
        + `api/lml/copy?`
        + `src=` + encodeURIComponent(this.src)
        + `&text=` + encodeURIComponent(this.editor.getValue())
    ;
    let fetchData = {
      method: 'PUT'
    }
    const response = await fetch(url, fetchData);
    if (!response.ok) {
      let message = "";
      switch (response.status) {
        case 404: {
          message += "not found";
          break;
        }
        case 500: {
          message += "doxa server error";
          break;
        }
        default: {
          message += `An error has occurred: ${response.status}`;
        }
      }
      throw new Error(message);
    }
    const responseJson = await response.json();
    return responseJson;
  }

  async saveFileContent() {
    const url = this.host
        + `api/${this.lang}?`
        + `src=` + encodeURIComponent(this.src)
        + `&text=` + encodeURIComponent(this.editor.getValue())
    ;
    let fetchData = {
      method: 'PUT'
    }
    const response = await fetch(url, fetchData);
    if (!response.ok) {
      let message = "";
      switch (response.status) {
        case 404: {
          message += "not found";
          break;
        }
        case 500: {
          message += "doxa server error";
          break;
        }
        default: {
          message += `An error has occurred: ${response.status}`;
        }
      }
      throw new Error(message);
    }
    const responseJson = await response.json();
    return responseJson;
  }

  async fetchFileParse() {
    const url = this.host
        + `api/lml/parse`
        + `?src=` + encodeURIComponent(this.src)
        + `&date=` + encodeURIComponent(this.previewDate)
        + `&libraryCombo=` + encodeURIComponent(this.previewLibraryCombo)
    ;
    let fetchData = {
      method: 'GET'
    }
    let message = "";
    try {
      const response = await fetch(url, fetchData);
      if (!response.ok) {
        switch (response.status) {
          case 404: {
            message += "not found";
            break;
          }
          case 500: {
            message += "doxa server error";
            break;
          }
          default: {
            message += `An error has occurred: ${response.status}`;
          }
        }
      }
      this.setAlert(message);
      return await response.json();
    } catch (error) {
      message = `${error.message}: this can happen if the LML parser encounters an unexpected letter or keyword in your template and it goes into an endless loop reporting it over and over.  A timeout will occur and this error will appear.  To see the line and column where the error occurred, open the Log tab in the upper right.`;
      this.setAlert(message);
      alert(message);
    }
  }


  async deleteFile() {
    const url = this.host
        + `api/lml/delete?`
        + `src=` + encodeURIComponent(this.src)
    ;
    let fetchData = {
      method: 'PUT'
    }

    const response = await fetch(url, fetchData);
    let message = "";
    if (!response.ok) {
      switch (response.status) {
        case 404: {
          message += "not found";
          break;
        }
        case 500: {
          message += "doxa server error";
          break;
        }
        default: {
          message += `An error has occurred: ${response.status}`;
        }
      }
      this.setAlert(message);
      throw new Error(message);
    }
    const responseJson = await response.json();
    return responseJson;
  }

  /**
   * Sets the alert message displayed at the top of the editor component
   * @param {string} msg - The message to display
   */
  setAlert(msg) {
    this.querySelector("#code-editor-alert").innerText = msg;
  }
  
  /**
   * Inserts text at the current cursor position in the editor
   * @param {string} txt - The text to insert at current cursor position
   */
  insert(txt) {
    let pos = this.editor.selection.getCursor()
    let end = this.editor.session.insert(pos, txt)
    this.editor.selection.setRange({start:pos,end:end})
  }
}

window.customElements.define('code-editor', CodeEditor);
