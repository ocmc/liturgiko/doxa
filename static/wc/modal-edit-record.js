
import * as utils from "./utils.js";

/**
 * @tagname modal-edit-record
 * @description A modal dialog component for editing database records with specialized handling for different record types.
 * Features a full-screen editor with comparison tables for liturgical and biblical texts, help information,
 * and support for redirects. The component handles saving changes back to the database with validation.
 *
 * @dependencies {WebComponent} edit-tools - Used for liturgical text editing tools (Grammar, Music, Notes, PDF)
 * @dependencies {WebComponent} topic-key-compare - Used to compare values across different libraries, listens for redirectClick events
 * @dependencies {WebComponent} help-info - Used to display contextual help
 * @dependencies {WebComponent} prop-desc - Used to display property descriptions for configuration records
 * @inputs {Attribute} recId - The ID of the record to edit
 * @inputs {Attribute} recValue - The initial value of the record (optional)
 * @inputs {Attribute} recNotExists - Whether the record exists in the database (true/false)
 * @inputs {Attribute} host - API endpoint host URL (optional)
 * @inputs {Attribute} title - Title for the modal dialog
 * @inputs {Attribute} okBtn - Text for the save button
 * @inputs {Attribute} cancelBtn - Text for the cancel button
 * @inputs {Attribute} trace - Trace information for redirects
 * @inputs {Attribute} showRedirects - Whether to show redirects (true/false)
 * @inputs {Attribute} visible - Controls modal visibility (true/false)
 * @outputs {Attribute} status - Status message from database operations
 * @api {GET} /api/db/read - Fetches record data
 * @api {PUT} /api/db/update - Updates existing records
 * @api {POST} /api/db/create - Creates new records
 * @watches {Attribute} status - Updates UI when operation status changes
 * @watches {Attribute} visible - Shows or hides the modal when changed
 * 
 * @examples
 * 
 * Basic usage:
 * ~~~html
 * <modal-edit-record
 *   id="record-editor"
 *   recId="configs/general/settings"
 *   title="Edit Settings"
 *   okBtn="Save"
 *   cancelBtn="Cancel">
 * </modal-edit-record>
 * ~~~
 * 
 * Editing liturgical text:
 * ~~~html
 * <modal-edit-record
 *   id="liturgical-editor"
 *   recId="ltx/en_us_dedes/ho.ho23"
 *   showRedirects="true"
 *   title="Edit Liturgical Text"
 *   okBtn="Save"
 *   cancelBtn="Cancel">
 * </modal-edit-record>
 * ~~~
 * 
 * Creating a new record:
 * ~~~html
 * <modal-edit-record
 *   id="new-record-editor"
 *   recId="configs/user/preferences"
 *   recNotExists="true"
 *   title="Create User Preferences"
 *   okBtn="Create"
 *   cancelBtn="Cancel">
 * </modal-edit-record>
 * ~~~
 * 
 * Controlling visibility programmatically:
 * ~~~javascript
 * const editor = document.getElementById('record-editor');
 * 
 * // Show the editor
 * editor.setAttribute('visible', 'true');
 * 
 * // Monitor status updates
 * const observer = new MutationObserver((mutations) => {
 *   mutations.forEach((mutation) => {
 *     if (mutation.attributeName === 'status') {
 *       console.log('Operation status:', editor.getAttribute('status'));
 *       if (editor.getAttribute('status') === 'updated') {
 *         // Handle successful update
 *       }
 *     }
 *   });
 * });
 * observer.observe(editor, { attributes: true });
 * ~~~
 */
class ModalEditRecord extends HTMLElement {
  constructor() {
    super();
    console.log("static/wc/modal-edit-record.js")
    this.originalValue = ""
    this.myModal = {}
  }

  connectedCallback() {
    this.innerHTML = `
      <style> 
		    @import url('static/css/bootstrap.min.css');
            @import url('static/img/bootstrap-icons.css');
      </style>
      <style>
        .textarea-autosize {
          padding-top:10px;
          padding-bottom:25px; /* increased! */
          width:100%;
          display:block;
  }
      .control-label {
        color: #5bc0de;
      }
       .modal {
          display:block;
       }
       .modal-header{
          text-transform: uppercase;
          color:#fff;
          background-color: #428bca;
        }
       .modal-body {
           padding: 40px !important;
       }
      </style>
      <div class="modal"  id="modal-dialog-edit-record" data-bs-backdrop="static" data-bs-keyboard="false">
       <div class="modal-dialog  modal-fullscreen">
         <div class="modal-content">
           <div class="modal-header">
              <h4 class="modal-title">${this.title}</h4>
              <button type="button" class="close" id="closeBtn" data-dismiss="modal" aria-hidden="true">×</button>
           </div>
           <div class="modal-body">
                <div id="edit-help"></div>
                <div class="row">
                  <div class="col"></div>
                  <div class="col col-10">
                    <div class="row" id="topic-key-table"></div>
                    <div class="row" id="redirectsDiv"></div>                    
                    <div class="row" id="notExistsDiv"></div>                    
                    <div class="row">
                      <div class="control-label">Value for: ${this.recId}</div>
                      <textarea id="recordValue" type="text" class="form-control textarea-autosize" tabindex="2" style="height:100%;">${this.recValue}</textarea>
                    </div>
                  </div>
                  <div class="col"></div>
                </div>
                <div class="row">
                  <div class="col">
                  </div>
                  <div class="col col-10" style="margin-top: 10px;">
                      <div class="align-middle">
                      <button type="button" class="btn btn-outline-primary" data-dismiss="modal"  id="cancelBtn" tabindex="2">${this.cancelBtn}</button>
                      <button type="button" class="btn btn-outline-primary"  id="undoBtn" disabled tabindex="3">Undo</button>
                      <button type="button" class="btn btn-primary" id="okBtn" disabled  tabindex="4">${this.okBtn}</button>
                      &nbsp;<span id="updateStatus" class="control-label"></span>
                      </div>
                  </div>
                  <div class="col"></div>
                </div>
                <div class="row">
                  <div class="col">
                  </div>
                  <div class="col col-10" style="margin-top: 10px;">
                  </div>
                  <div class="col"></div>
                </div>
                <div id="tab-row" class="row"></div>
           </div>
           <div class="modal-footer">
              <button type="button" class="btn btn-outline-primary" data-dismiss="modal"  id="cancelBtn" tabindex="4">${this.cancelBtn}</button>
           </div>
        </div><!-- /.modal-content -->
       </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
     </div>
  `;
    this.querySelector("#modal-dialog-edit-record").addEventListener("click", e => {
      const targetId = e.target.id;
      switch (targetId) {
        case "cancelBtn": {
          // Allow propagation of event.
          // In case the user changed the value but did not save it,
          // revert to original value.
          this.recValue = this.originalValue;
          break;
        }
        case "closeBtn": {
          // Allow propagation of event.
          // In case the user changed the value but did not save it,
          // revert to original value.
          this.recValue = this.originalValue;
          break;
        }
        case "okBtn": {
          // normalize the value to NFC
          let val = this.querySelector("#recordValue").value.normalize("NFC");
//          val = utils.Underline(val); // experimental for OCA
          // set the html control to the normalized value
          this.querySelector("#recordValue").value = val;
          // check to see if the user has set the value to
          // a recursive redirect
          let v = val.trim()
          if (v.length > 0 && v[0] === "@") {
            if (v.slice(1) === this.recId) {
              this.setUpdateStatusMessage(" Recursive redirects are not allowed. You are redirecting back to the same record.");
              e.stopPropagation();
              break;
            }
          }

          this.updateDbRecord(this.recId,val).then(r => {
            this.status = `${r}`;
          });
          e.stopPropagation();
          this.setAttribute("recValue", val);
          this.recValue = val
          this.originalValue = this.recValue;
          this.toggleAddButton();
          break;
        }
        case "undoBtn": {
          this.recValue = this.originalValue;
          this.querySelector("#recordValue").value = this.recValue;
          this.toggleAddButton();
          e.stopPropagation();
          break;
        }
        default: { e.stopPropagation();}
      }
    });
    // keyup recordValue
    this.querySelector("#recordValue").addEventListener('keyup', ev => {
      ev.stopPropagation();
      this.toggleAddButton();
    })
    this.originalValue = this.recValue;

    if (this.recId.startsWith("configs/")) {
      const recIdDesc = this.recIdToDesc();
      if (recIdDesc.length > 0) {
        this.querySelector("#topic-key-table").innerHTML = `<prop-desc recId="${recIdDesc}"></prop-desc>`;
      }
    }
    if (this.recId.startsWith("ltx/")) {
      this.querySelector("#edit-help").innerHTML = `<help-info text="The text editor has three sections. The first section is a table that shows the Greek text and translations of the text. The second section has a text box for you to enter your translation. When you are ready, click the Save button to update the database with your changes. The third section has tabs for a Grammar Explorer, Notes, and a PDF generator. Click on a tab title to view its contents. Close this window by using the X at the upper right, or by clicking the Close button. To set the record value to a redirect, click the @ symbol for the library you wish to use."></help-info>`;
      this.querySelector("#tab-row").innerHTML = `<p></p><hr><edit-tools></edit-tools>`;
    }
    // if this is a record for biblical or liturgical text, add a table comparing values
    if (this.recId.startsWith("btx/") || this.recId.startsWith("ltx/")) {
      this.querySelector("#topic-key-table").innerHTML = `<topic-key-compare id="topic-key-compare-id" recId="${this.recId}"  idForLookup="" showRedirects="${this.showRedirects}"></topic-key-compare>`
      // add listeners
      const cmp = this.querySelector("#topic-key-compare-id");
      
      // Listen for the custom redirectClick event
      cmp.addEventListener('redirectClick', (event) => {
        const redirect = event.detail.redirectId;
        if (redirect) {
          this.recValue = redirect;
          this.querySelector("#recordValue").value = this.recValue;
          this.toggleAddButton();
        }
      });
      
      // Keep old click listener for backward compatibility
      cmp.addEventListener('click', (event) => {
        let redirect = cmp.getAttribute("idForLookup") || cmp.getAttribute("idForRedirect");
        if (redirect) {
          this.recValue = redirect;
          this.querySelector("#recordValue").value = this.recValue;
          this.toggleAddButton();
        }
      });
    }
    if (this.recNotExists && this.recNotExists==="true") {
      this.querySelector("#notExistsDiv").innerHTML = `<p style="color: red">This record does not exist in the database.  If you enter a value (or an empty space) and click the Save button, it will be created.  If you need a record to redirect to it, also add that.</p><p></p>`
    }
    // determine whether we need to trace the redirects that resulted in this record being used
    if (this.trace && this.trace.length > 0 && this.trace !== this.recId) {
      this.querySelector("#redirectsDiv").innerHTML = `<div class="control-label">Trace: ${this.trace}</div><p></p>`
    }
    if (!this.recValue) {
      this.readDbRecord(this.recId).then(r => {
        this.status = `${r}`;
      });
    }
    // set focus to first input
    this.querySelector("#recordValue").focus();

    this.myModal = bootstrap.Modal.getOrCreateInstance(this.querySelector("#modal-dialog-edit-record"), {});
    this.myModal.show();
  }

  disconnectedCallback() {
  }

  adoptedCallback() {
  }

  recIdToDesc() {
    const parts = this.recId.split(":")
    if (parts.length > 1) {
      return parts[0] + ":desc";
    }
    return "";
  }
  setUpdateStatusMessage(value) {
    if (!this.querySelector("#updateStatus")) {
      return;
    }
    this.querySelector("#updateStatus").innerText = value;
  }
  toggleAddButton() {
    if (!this.querySelector("#recordValue")) {
      return;
    }
    const undoBtn = this.querySelector("#undoBtn")
    const okBtn = this.querySelector("#okBtn")
    if (this.originalValue !== this.querySelector("#recordValue").value) {
      okBtn.disabled = false;
      undoBtn.disabled = false;
      this.setUpdateStatusMessage("");
    } else {
      undoBtn.disabled = true;
      okBtn.disabled = true;
    }
  }
  async createDbRecord(id, value) {
    const url = this.host
        + 'api/db/create?id='
        + encodeURIComponent(id)
        + "&value=" + encodeURIComponent(value)
    ;
    let fetchData = {
      method: 'POST'
    }
    return new Promise(async (res, rej) => {
      await fetch(url, fetchData)
          .then(data => data.json())
          .then((json) => {
            if (json.status === "created") {
              if (json.status === "updated") {
                this.querySelector("#notExistsDiv").innerHTML = "";
              }
            }
            this.setUpdateStatusMessage(json.status);
            res(json.Status);
            return res;
          })
          .catch((error) => rej(error));
    })
  }

  async updateDbRecord(id, value) {
    const url = this.host
        + 'api/db/update?id='
        + encodeURIComponent(id)
        + "&value=" + encodeURIComponent(value)
    ;
    let fetchData = {
      method: 'PUT'
    }
    return new Promise(async (res, rej) => {
      await fetch(url, fetchData)
          .then(data => data.json())
          .then((json) => {
            if (json.status === "record does not exist") {
              this.createDbRecord(id, value);
            } else {
              if (json.status === "updated") {
                this.querySelector("#notExistsDiv").innerHTML = "";
              }
              this.setUpdateStatusMessage(json.status);
              res(json.Status);
              return res;
            }
          })
          .catch((error) => rej(error));
    });
  }

  async readDbRecord(id) {
    const url = this.host
        + 'api/db/read?id='
        + encodeURIComponent(id)
    ;
    let fetchData = {
      method: 'GET'
    }
    return new Promise(async (res, rej) => {
      await fetch(url, fetchData)
          .then(data => data.json())
          .then((json) => {
            if (json.status === "record does not exist") {
            } else {
              this.recValue = json.Value;
              this.querySelector("#recordValue").value = this.recValue;
              this.setUpdateStatusMessage(json.status);
              res(json.Status);
              return res;
            }
          })
          .catch((error) => rej(error));
    });
  }

  static get observedAttributes() {
    return ['recId', 'recValue', 'status', 'visible'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
    switch (name) {
      case "status": {
        this.originalValue = this.recValue;
        this.toggleAddButton();
        this.setUpdateStatusMessage(newVal);
        break;
      }
      case "visible": {
        if (this.myModal && this.myModal.show) {
          if (newVal === "true") {
            this.myModal.show();
          } else {
            this.myModal.hide();
          }
        }
      }
    }
  }
  // getters and setters
  get cancelBtn() {
    return this.getAttribute("cancelBtn");
  }
  set cancelBtn(value) {
    this.setAttribute("cancelBtn", value);
  }
  get host() {
    return this.getAttribute("host");
  }
  set host(value) {
    this.setAttribute("host", value);
  }
  get recId() {
    return this.getAttribute("recId");
  }
  set recId(value) {
    this.setAttribute("recId", value);
  }
  get recNotExists() {
    return this.getAttribute("recNotExists");
  }
  set recNotExists(value) {
    this.setAttribute("recNotExists", value);
  }
  get recValue() {
    return this.getAttribute("recValue");
  }
  set recValue(value) {
    this.setAttribute("recValue", value);
  }

  get visible() {
    return this.getAttribute("visible");
  }
  set visible(value) {
    this.setAttribute("visible", value);
  }
  get okBtn() {
    return this.getAttribute("okBtn");
  }
  set okBtn(value) {
    this.setAttribute("okBtn", value);
  }
  get title() {
    return this.getAttribute("title");
  }
  set title(value) {
    this.setAttribute("title", value);
  }
  get trace() {
    return this.getAttribute("trace");
  }
  set trace(value) {
    this.setAttribute("trace", value);
  }

  get showRedirects() {
    return this.getAttribute("showRedirects");
  }
  set showRedirects(value) {
    this.setAttribute("showRedirects", value);
  }

  get status() {
    return this.getAttribute("status");
  }
  set status(value) {
    this.setAttribute("status", value);
  }
}

window.customElements.define('modal-edit-record', ModalEditRecord);