/**
 * @tagname edit-tools
 * @description A tabbed interface component that provides specialized editing tools for liturgical content.
 * The component displays a set of tabs (Grammar, Music, Notes, PDF) with corresponding content panels
 * for analyzing and working with liturgical texts.
 *
 * @dependencies {WebComponent} topic-key-compare - Used indirectly via modal-edit-record parent
 * @dependencies {WebComponent} help-info - Used indirectly via modal-edit-record parent
 * @inputs {Attribute} active-tab - The ID of the initially active tab (default: "nav-grammar-tab")
 * @outputs {Event} tab-changed - Fires when a tab is selected with detail: {tabId: String}
 * @watches {Attribute} content-updated - Triggers a refresh of the content when this attribute changes
 * 
 * @examples
 * 
 * Basic usage:
 * ~~~html
 * <edit-tools id="liturgical-editor-tools"></edit-tools>
 * ~~~
 * 
 * With specific active tab:
 * ~~~html
 * <edit-tools id="liturgical-editor-tools" active-tab="nav-music-tab"></edit-tools>
 * ~~~
 * 
 * Listening for tab changes:
 * ~~~javascript
 * const editTools = document.getElementById('liturgical-editor-tools');
 * editTools.addEventListener('tab-changed', (event) => {
 *   console.log('Selected tab:', event.detail.tabId);
 *   // Perform actions based on selected tab
 * });
 * ~~~
 * 
 * Used within modal-edit-record (typical use case):
 * ~~~javascript
 * // The component is typically used within modal-edit-record for liturgical text editing
 * if (this.recId.startsWith("ltx/")) {
 *   this.querySelector("#tab-row").innerHTML = `<p></p><hr><edit-tools></edit-tools>`;
 * }
 * ~~~
 */
class EditTools extends HTMLElement {
  constructor() {
    super();
    console.log("static/wc/edit-tools.js")
  }

  connectedCallback() {
    this.innerHTML = `
    <style> 
      @import url('static/css/bootstrap.min.css');
      @import url('static/img/bootstrap-icons.css');
    </style>
    <style>
        .alert-info {
            color: #31708f;
            background-color: #d9edf7;
            border-color: #bce8f1;
        }
        body {
         height: 100%;
         overflow-y: hidden;
        }
    </style>
    <div class="container-fluid">
        <div class="row p-1">
        <nav>
          <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <button class="nav-link active" id="nav-grammar-tab" data-bs-toggle="tab" data-bs-target="#nav-grammar" type="button" role="tab" aria-controls="nav-grammar" aria-selected="true">Grammar</button>
            <button class="nav-link" id="nav-music-tab" data-bs-toggle="tab" data-bs-target="#nav-music" type="button" role="tab" aria-controls="nav-music" aria-selected="false">Music</button>
            <button class="nav-link" id="nav-notes-tab" data-bs-toggle="tab" data-bs-target="#nav-notes" type="button" role="tab" aria-controls="nav-notes" aria-selected="false">Notes</button>
            <button class="nav-link" id="nav-pdf-tab" data-bs-toggle="tab" data-bs-target="#nav-pdf" type="button" role="tab" aria-controls="nav-pdf" aria-selected="false">PDF</button>
          </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
          <div class="tab-pane fade show active" id="nav-grammar" role="tabpanel" aria-labelledby="nav-grammar-tab" tabindex="0">Grammar content</div>
          <div class="tab-pane fade" id="nav-music" role="tabpanel" aria-labelledby="nav-music-tab" tabindex="0">Music content</div>
          <div class="tab-pane fade" id="nav-notes" role="tabpanel" aria-labelledby="nav-notes-tab" tabindex="0">Notes content</div>
          <div class="tab-pane fade" id="nav-pdf" role="tabpanel" aria-labelledby="nav-pdf-tab" tabindex="0">PDF content</div>
        </div>
        </div>
    </div>
    `
  }

  disconnectedCallback() {
  }

  static get observedAttributes() {
    return ['active-tab', 'content-updated'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
    if (name === 'active-tab' && newVal) {
      // Activate the specified tab
      const tabElement = this.querySelector(`#${newVal}`);
      if (tabElement) {
        tabElement.click();
      }
    } else if (name === 'content-updated') {
      // Refresh the content
      this.refreshContent();
    }
  }
  
  /**
   * Refreshes the content of the currently active tab
   * @private
   */
  refreshContent() {
    // Implementation for refreshing content
    console.log('Content refreshed');
  }

  adoptedCallback() {
  }
}

window.customElements.define('edit-tools', EditTools);