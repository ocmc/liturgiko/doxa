import * as utils from "./utils.js";

// InputWithDropdown provides an input text field with a dropdown list
// When the user make a selection from a list, the select is inserted at the end
// of the current value of the input and a custom event, inputChanged is generated.
// When the action button is clicked a custom event will be generated.
class RegexInput extends HTMLElement {
    // generates custom event "inputChanged". ev.detail.value contains the value of the input component.
    // generates custom event "enterKeyPressed" when user has pressed and released the Enter key.ev.detail.value contains the value of the input component.

    /**
     * TAG ATTRIBUTES:
     *   gid: a unique id for the list so if the tag is used multiple times on a page, they will be treated as separate groups
     *   items: a json stringified object array as described below.
     *   active: the numeric index of the item to set as selected.
     * USAGE: see how used in synch-manager.
     * 1. In constructor, set an array object with radio options:
     *    e.g.,
     *    this.strategyOptions = [
     *       {Key: "combine", Desc: "Make remote repos, local repos and database content identical, but keep changes I made since the last synch.", Selected: true, Class: "alert-success", Icon: "", IconColor: ""},
     *       {Key: "resetToRemote", Desc: "Throw away database and local repo changes. Make the local repos and database identical to the remote repos.", Selected: false, Class: "alert-warning", Icon: "", IconColor: ""},
     *       {Key: "resetToDB", Desc: "Throw away remote repo changes. Make the remote repos identical to local repos and the database contents.", Selected: false, Class: "alert-warning", Icon: "", IconColor: ""},
     *     ];
     *     The option object attributes are:
     *       Key - the unique ID that will be returned when the item is selected
     *       Desc - what the user sees
     *       Class - (optional) the Bootstrap Alert class. Used to set the font color and background color of the item
     *       Icon - (optional) the Bootstrap icon to prefix to the Desc value
     *       IconColor - (optional) the color of the icon
     * 2. Add <dropdown-list it="someIdYouCreate" items="[]" active="0" title="My list"></dropdown-list>
     *       items - use JSON.stringify() to turn the array of option objects into string to pass as tag
     *       active - the index number of the selected item
     * 3. Add an event handler to get notified when someone selects an option:
     *      this.querySelector("#someIdYouCreate").addEventListener("change", (ev) => this.yourHandlerFunction(ev));     * To Get Selected Item, in the using javascript:
     * 4. Create the handler function:
     *   myHandlerFunction(ev) {
     *     // to get value selected
     *     let selectedItem = ev.target.getAttribute("value");
     *    // to get value selected
     *    let selectedItemIndex = ev.target.getAttribute("index");
     *   }
     */

    /*
        Design issues:
        - This web component provides a single input for the user to enter
          a regular expression.
        - this.currentPosition keeps track of where the cursor is in the input.
        - As an aid to the user, there are two ways for the user to see
          a list of regular expression keywords,
          to select a value from the list,
          and have it inserted into the input.
        - 1. This component provides an autocomplete that is triggered when the user enters a backslash.
             The code for this is the function below named autocomplete.  It is a closure,
             and therefore cannot set outer scope variables, specifically, this.currentPosition.
       - 2. At the end of the input, there is a lookup that is triggered by clicking on it.
            If the user clicks a value in the list, the regEx is inserted at this.currentPosition
            in the input.
       - Because the autocomplete changes the position of the cursor after an insert,
         the outer variable this.currentPosition needs to be updated.  This is done by
         using a custom event 'inputPositionChange'.

     */


    constructor() {
        super();
        console.log("static/wc/dropdown-list.js")
        this.listArray = null;
        this.listComponent = null;
        this.listComponentPreviousRxPatterns = null;
        this.dropdownItemDiA = null; // Diacritic Insensitive All
        this.dropdownItemDiG = null; // Diacritic Insensitive Greek
        this.inputComponent = null;
        this.inputPosition = 0;
        this.inputCaseInsensitive = null;
        this.inputDiacriticInsensitive = null;
        this.inputWholeWord = null;
        this.inputGreek = null;

        this.iconRegExHelp = null;
        this.modalRegExHelp = null;

        // variables used for saving user choices to browser storage.
        this.lsPrefixBase = "rxInput"
        this.lsPrefix = ""; // gets set when attribute gid changes
        this.previousRegExPatterns = [];
        this.lsPreviousRegExPatterns = "previousPatterns"
        this.lsCaseInsensitive = "caseInsensitive";
        this.lsDiacriticInsenstive = "diacriticsInsensitive";
        this.lsWholeWord = "wholeWord";
        // passed back to using web component via
        // custom event regExFlagsChanged
        this.caseInsensitive = false;
        this.diacriticInsensitiveAll = false;
        this.wholeWord = false;
        this.diacriticInsensitiveGreek = false;
    }

    connectedCallback() {
        this.innerHTML =
            `
<style> 
  @import url('static/css/bootstrap.min.css');
  @import url('static/img/bootstrap-icons.css');
</style>
<style>
.input-with-dropdown-menu {
  max-height: 20vh;
  overflow-y: scroll;
  max-width: 50vw;
  overflow-x: scroll;
}
.autocomplete {
  /*the container must be positioned relative:*/
  position: relative;
  display: inline-block;
}
input {
  border: 1px solid transparent;
  background-color: #f1f1f1;
  padding: 10px;
  font-size: 16px;
}
input[type=text] {
  background-color: #f1f1f1;
  width: 100%;
}
input[type=submit] {
  background-color: DodgerBlue;
  color: #fff;
}
.autocomplete-items {
  position: absolute;
  border: 1px solid #d4d4d4;
  border-bottom: none;
  border-top: none;
  z-index: 99;
  /*position the autocomplete items to be the same width as the container:*/
  top: 100%;
  left: 0;
  right: 0;
  max-height: 20vh;
  overflow-y: scroll;
  max-width: 50vw;
  overflow-x: scroll;
}
.autocomplete-items div {
  padding: 10px;
  cursor: pointer;
  background-color: #fff; 
  border-bottom: 1px solid #d4d4d4; 
}
.autocomplete-items div:hover {
  /*when hovering an item:*/
  background-color: #e9e9e9; 
}
.autocomplete-active {
  /*when navigating through the items using the arrow keys:*/
  background-color: DodgerBlue !important; 
  color: #ffffff; 
}


</style>

<div class="input-group">
  <button class="btn btn-outline-secondary dropdown-toggle" style="background-color: #e9ecef;" type="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="bi-clock-history" style="font-weight: 900; font-size: 1rem; color: cornflowerblue;"></i></button>
    <ul id="listComponent_${this.gid}_previousRxPatterns" class="dropdown-menu">
    </ul>
  <input id="inputComponent_${this.gid}" type="text" class="form-control" 
    placeholder="search for value, e.g. joy.*" 
    aria-label="enter regular expression">
  <button class="btn btn-outline-secondary dropdown-toggle"  style="background-color: #e9ecef;" type="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="bi-info-circle" style="font-size: 1rem; color: cornflowerblue;"></i></button>
  <ul id="listComponent_${this.gid}" class="dropdown-menu dropdown-menu{-sm|-md|-lg|-xl|-xxl}-end input-with-dropdown-menu"></ul>
  <button class="btn btn-outline-secondary dropdown-toggle"  style="background-color: #e9ecef;" type="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="bi-globe" style="font-size: 1rem; color: cornflowerblue;"></i></button>
  <ul id="listComponent_${this.gid}_global" class="dropdown-menu dropdown-menu{-sm|-md|-lg|-xl|-xxl}-end input-with-dropdown-menu">
  <li class="dropdown-item"> 
      <input class="form-check-input" type="checkbox" value="" id="inputCaseInsensitive">
      <label class="form-check-label" for="inputCaseInsensitive">
      Case-Insensitive
      </label>
    </li>
    <li class="dropdown-item" id="dropdownItemDiA"> 
      <input class="form-check-input" type="checkbox" value="" id="inputDiacriticInsensitive">
      <label class="form-check-label" for="inputDiacriticInsensitive">
      Diacritic-Insensitive (any language)
      </label>
    </li>
    <li class="dropdown-item" id="dropdownItemDiG"> 
      <input class="form-check-input" type="checkbox" value="" id="inputGreek">
      <label class="form-check-label" for="inputGreek">
        Diacritic-insensitive (Greek only)
      </label>
    </li>
    <li class="dropdown-item"> 
      <input class="form-check-input" type="checkbox" value="" id="inputWholeWord">
      <label class="form-check-label" for="inputWholeWord">
        Whole Word
      </label>
    </li>
  </ul>
  <i id="iconRegExHelp" class="bi-question-circle" style="padding: 0.5em; color: cornflowerblue;"></i>
  <p id="idHelpBlockRegEx" class="form-text d-none">
  Enter the regular expression and press the Enter or Return key. Click <i class="bi-clock-history" style="font-weight: 900; font-size: 1rem; color: cornflowerblue;"></i> to view and select one of your last five regular expressions.  When typing, when you enter a backslash, it will trigger autocomplete. You can also view this by clicking <i class="bi-info-circle" style="font-size: 1rem; color: cornflowerblue;"></i>. Click <i class="bi-globe" style="font-size: 1rem; color: cornflowerblue;"></i> to turn on/off global settings.
  </p>
  <!-- Modal Help -->
    <modal-icon-help
        id="regex-input-modal-help"
        class="d-none"
        
        firstDivText="To search text in the database you can type whole or partial words.  Select global flags (see below) to apply to the whole pattern you entered.  You can also use regular expressions.  However, you need to be aware that Doxa is written in the go programming language, which conforms to the re2 standard.  You should look on the Internet for re2 syntax.  Also, be aware that the web browser uses javascript, which  implements Perl-style regular expressions, which is part of the ECMA-262 standard for the language.  There are some incompatibilities between it and re2. Also, Doxa has added two global flags not available elsewhere for diacritic-insensitive searches.  It has also added a unique escape sequence \\m and \\μ for letter specific diacritic-insensitive matches.  See below. When you enter a backslash, it will trigger auto-complete. Doxa has a regular expression tester.  Go to Run > Regular Expression Tester."
        lastDivText="The Doxa directive \\m() supports any language. If the library you are searching is for a language that has diacritics for certain letters, and you want to find all occurrences for all variants, use this directive.  For example, to search Spanish words matching either 'su' or 'sú', use this pattern: s\\m(u).  This means, match any text with the letter 's' followed by a variant of the letter 'u'.  This also works for Greek.  However, Doxa has a special directive to match Greek vowels and ρ for all variants.  Enter the pattern using a polytonic Greek keyboard and type \\μ instead of \\m.  For example, \\μ(α)γ\\μ(α)π\\μ(η) will match ἀγάπης, ἀγαπητοῦ, ἀγάπην, etc. Note: regular expression Unicode properties use braces, e.g. \\p{L}, which matches any Unicode letter. Doxa uses parentheses instead of braces because in polytonic Greek keyboards the braces are usually mapped to a Greek diacritic instead of a brace. By using parentheses instead, it is not necesary to switch back to a Latin keyboard.  Also, note that for Greek, using \\m works, but is slower than using \\μ.  That is because \\m and global diacritic-insensitive flags require converting the text to NFD before the search, then back to NFC.  The Greek \\μ searches against NFC only."
        title="Regular Expression Help">
    </modal-icon-help>

`;
        if (this.gid) { // make the prefix unique across uses of this component
            this.lsPrefix = this.lsPrefixBase + this.gid;
        }
        if (this.help && this.help === "true") {
            this.querySelector(`#idHelpBlockRegEx`).classList.remove("d-none");
        }
        // modal regEx help
        this.iconRegExHelp = this.querySelector('#iconRegExHelp');
        this.iconRegExHelp.addEventListener('click', (ev) => this.showModalRegExHelp(ev));
        this.modalRegExHelp = this.querySelector("#regex-input-modal-help");
        this.modalRegExHelp.addEventListener('click', (ev) => this.handleModalRegExClick(ev));


        this.dropdownItemDiA = this.querySelector("#dropdownItemDiA");
        this.dropdownItemDiG = this.querySelector("#dropdownItemDiG");
        this.inputComponent = this.querySelector(`#inputComponent_${this.gid}`);
        this.inputComponent.addEventListener("change", (e) => this.handleInputChange(e));
        this.inputComponent.addEventListener("click", (e) => this.setInputPosition(e));
        this.inputComponent.addEventListener("keyup", (e) => this.setInputPosition(e));
        this.inputComponent.addEventListener("inputPositionChange", (e) => this.handleInputPositionChange(e));
        this.listComponent = this.querySelector(`#listComponent_${this.gid}`);
        this.listComponent.addEventListener("click", (e) => this.handleItemClick(e));
        if (this.listArray && this.listArray.length > 0) {
            this.renderList()
        }
        if (this.listArray) {
            this.autocomplete(this.inputComponent, this.listArray);
        }
        this.listComponentPreviousRxPatterns = this.querySelector(`#listComponent_${this.gid}_previousRxPatterns`);
        this.listComponentPreviousRxPatterns.addEventListener("click", (e) => this.handlePreviousRegExClick(e));
        this.getPreviousRegExPatterns();

        this.inputCaseInsensitive = this.querySelector(`#inputCaseInsensitive`);
        this.inputDiacriticInsensitive = this.querySelector(`#inputDiacriticInsensitive`);
        this.inputGreek = this.querySelector(`#inputGreek`);
        this.inputWholeWord = this.querySelector(`#inputWholeWord`);
        this.inputCaseInsensitive.addEventListener("change", (e) => this.toggleCaseInsensitive(e));
        this.inputDiacriticInsensitive.addEventListener("change", (e) => this.toggleDiacriticInsensitive(e));
        this.inputGreek.addEventListener("click", (e) => this.toggleGreek(e));
        this.inputWholeWord.addEventListener("click", (e) => this.toggleWholeWord(e));

        this.setRegExFlags();
    }
    // for every event handler added above, below remove it
    disconnectedCallback() {
        this.querySelector('#iconRegExHelp').removeEventListener('click', (ev) => this.showModalRegExHelp(ev));
        this.querySelector('#regex-input-modal-help').removeEventListener('click', (ev) => this.showModalRegExHelp(ev));
        this.listComponent.removeEventListener("click", (e) => this.handleItemClick(e));
        this.inputComponent.removeEventListener("change", (e) => this.handleInputChange(e));
        this.inputComponent.removeEventListener("keyup", (e) => this.setInputPosition(e));
        this.inputComponent.removeEventListener("click", (e) => this.setInputPosition(e));
        this.inputGreek.removeEventListener("click", (e) => this.toggleGreek(e));
        this.inputWholeWord.removeEventListener("change", (e) => this.toggleWholeWord(e));
        this.inputCaseInsensitive.removeEventListener("change", (e) => this.toggleCaseInsensitive(e));
        this.inputDiacriticInsensitive.removeEventListener("change", (e) => this.toggleDiacriticInsensitive(e));
    }

    showModalRegExHelp(ev) {
        ev.stopPropagation();
        this.scrollY = window.scrollY;
        let help = JSON.stringify([
            {
                "icon": "bi-clock-history",
                "desc": "Select one of your last five search patterns.",
            },
            {
                "icon": "bi-info-circle",
                "desc": "Select or read about Doxa and Unicode Properties.",
            },
            {
                "icon": "bi-globe",
                "desc": "Select global flags. Globals apply to the entire pattern.",
            },
            {
                "icon": "NFC",
                "desc": "Unicode normal form C. Doxa stores all text in NFC.",
            },
            {
                "icon": "NFD",
                "desc": "Unicode normal form D.  Used for global diacritic-insensitive searches.",
            },
            {
                "icon": "Case-Insensitive",
                "desc": "A global flag that says to match both upper and lower-case.",
            },
            {
                "icon": "Diacritic-Insensitive (any language)",
                "desc": "For all languages, diacritics (aka marks) will be ignored. The text is converted to NFD for the search, then back to NFC.",
            },
            {
                "icon": "Diacritic-Sensitive (Greek only)",
                "desc": "For Greek only, ignores diacritics by using all variants of vowels and ρ.  Uses NFC.",
            },
            {
                "icon": "Whole Word",
                "desc": "Only selects matching text if it is a whole word.",
            },
            {
                "icon": "\\m(a letter)",
                "desc": "A directive to match all variants of the Unicode letter appearing between the parentheses. Uses NFD. Using this in a pattern switches off the global Diacritic-Insensitive flags.",
            },
            {
                "icon": "\\μ(a Greek letter)",
                "desc": "A directive to match all variants of the Greek letter appearing between the parentheses. Uses NFC.  Using this in a pattern switches off the global Diacritic-Insensitive flags.",
            },
        ]);
        this.modalRegExHelp.setAttribute("data", help);
        this.modalRegExHelp.classList.remove("d-none");
    }

    handleModalRegExClick(ev) {
        ev.stopPropagation();
        const btn = ev.composedPath()[0].id;
        if (btn === "closeBtn" || btn === "cancelBtn") {
            this.modalRegExHelp.classList.add("d-none");
            this.scrollWindowTo(this.scrollY);
        }
    }

    setRegExFlags() {
        this.inputCaseInsensitive.checked = this.caseInsensitive;
        this.inputDiacriticInsensitive.checked = this.diacriticInsensitiveAll;
        this.inputGreek.checked = this.diacriticInsensitiveGreek;
        this.inputWholeWord.checked = this.wholeWord;
    }

    toggleCaseInsensitive() {
        this.caseInsensitive = !this.caseInsensitive;
        this.flagCi = JSON.stringify(this.caseInsensitive);
        this.dispatchEvent(new CustomEvent('flagCiChange', {
            detail: {
                name: 'flagCiChange', // this is the name to use in handler
                value: this.flagCi,
            },
            bubbles: true
        }));
    }

    toggleDiacriticInsensitive() {
        this.diacriticInsensitiveAll = !this.diacriticInsensitiveAll;
        this.flagDi = JSON.stringify(this.diacriticInsensitiveAll);
        this.dispatchEvent(new CustomEvent('flagDiChange', {
            detail: {
                name: 'flagDiChange', // this is the name to use in handler
                value: this.flagDi,
            },
            bubbles: true
        }));
        if (this.diacriticInsensitiveAll && this.diacriticInsensitiveGreek) {
            this.toggleGreek();
            this.inputGreek.checked = this.diacriticInsensitiveGreek;
        }
    }

    toggleWholeWord() {
        this.wholeWord = !this.wholeWord;
        this.flagWw = JSON.stringify(this.wholeWord);
        this.dispatchEvent(new CustomEvent('flagWwChange', {
            detail: {
                name: 'flagWwChange', // this is the name to use in handler
                value: this.flagWw,
            },
            bubbles: true
        }));
    }
    toggleGreek() {
        this.diacriticInsensitiveGreek = !this.diacriticInsensitiveGreek;
        this.flagGr = JSON.stringify(this.diacriticInsensitiveGreek);
        this.dispatchEvent(new CustomEvent('flagGrChange', {
            detail: {
                name: 'flagGrChange', // this is the name to use in handler
                value: this.flagGr,
            },
            bubbles: true
        }));
        if (this.diacriticInsensitiveAll && this.diacriticInsensitiveGreek) {
            this.toggleDiacriticInsensitive();
            this.inputDiacriticInsensitive.checked = this.diacriticInsensitiveAll;
        }
    }
     getPreviousRegExPatterns() {
        if (this.gid) {
            this.previousRegExPatterns =  JSON.parse(utils.lsGet(this.lsPrefix, this.lsPreviousRegExPatterns));
            if (this.previousRegExPatterns) {
                this.renderPreviousRxPatternList();
            } else {
                this.previousRegExPatterns = [];
            }
        }
    }
    putPreviousRegExPatterns(pattern) {
        if (this.gid) {
            if (this.previousRegExPatterns.length >= 5) {
                this.previousRegExPatterns = this.previousRegExPatterns.slice(1);
            }
            // check to see if we already have this pattern
            let found = false;
            this.previousRegExPatterns.forEach((item) => {
                if (item === pattern) {
                    found = true;
                }
            });
            if (!found) {
                this.previousRegExPatterns.push(pattern);
                utils.lsSet(this.lsPrefix, this.lsPreviousRegExPatterns, JSON.stringify(this.previousRegExPatterns));
                this.renderPreviousRxPatternList();
            }
        }
    }
    renderPreviousRxPatternList() {
        if (this.previousRegExPatterns && this.listComponentPreviousRxPatterns) {
            this.listComponentPreviousRxPatterns.innerHTML = "";
            const l = this.previousRegExPatterns.length - 1;
            // display the list in the order of
            // newest to oldest
            for (let i = l; i >= 0; i--) {
                let theLi = document.createElement("li");
                theLi.classList.add("dropdown-item");
                theLi.innerText = this.previousRegExPatterns[i];
                this.listComponentPreviousRxPatterns.appendChild(theLi);
            }
        }
    }
    autocomplete(inp, arr) {
        // source: https://www.w3schools.com/howto/howto_js_autocomplete.asp
        /*the autocomplete function takes two arguments,
        the text field element and an array of possible autocompleted values:*/
        if (!inp || arr.length === 0) {
            return;
        }
        let currentFocus;
        let inputPosition = 0;
        let lastLength = 0;
        let stack = [];
        /*execute a function when someone writes in the text field:*/
        inp.addEventListener("input", function(e) {
            inputPosition = e.target.selectionStart;
            let c = this.value.slice(inputPosition-1, inputPosition);
            if (c === `\\` && inputPosition > lastLength) {
               stack = [];
            } else if (inputPosition < lastLength) {
                    const j = lastLength - inputPosition + 1;
                    for (let i = 0; i < j; i++) {
                        if (stack.length > 0) {
                            stack.pop();
                        }
                    }
                    lastLength = inputPosition;
            }

            if (c.length > 0) {
                stack.push(c);
            }
            lastLength = this.value.length;
            let a, b, i, val = this.value; // i.e., inp.value (what has been entered into the field)
            val = stack.join('');
            /* close any already open lists of autocompleted values*/
            closeAllLists();
            if (!val) { return false;}
            currentFocus = -1;
            /* create a DIV element that will contain the items (values):*/
            a = document.createElement("DIV");
            a.setAttribute("id", this.id + "autocomplete-list");
            a.setAttribute("class", "autocomplete-items");
            /*append the DIV element as a child of the autocomplete container:*/
            this.parentNode.appendChild(a);
            /* for each item in the array...*/
            for (i = 0; i < arr.length; i++) {
                /* check if the item starts with the same letters as the text field value:*/
                if (arr[i].Key.substr(0, val.length).toUpperCase() === val.toUpperCase()) {
                    /* create a DIV element for each matching element:*/
                    b = document.createElement("DIV");
                    /* make the matching letters bold:*/
                    b.innerHTML = "<strong>" + arr[i].Key.substr(0, val.length) + "</strong>";
                    b.innerHTML += arr[i].Desc.substr(val.length);
                    /* insert an input field that will hold the current array item's value:*/
                    b.innerHTML += "<input type='hidden' value='" + arr[i].Key + "'>";
                    /* execute a function when someone clicks on the item value (DIV element):*/
                    b.addEventListener("click", function(e) {
                        /* insert the value for the autocomplete text field:*/
                        const selectedItem = this.getElementsByTagName("input")[0].value;
                        let l = inp.value.length;
                        if (inputPosition < l) {
                            let before = inp.value.slice(0, inputPosition-stack.length);
                            let after = inp.value.slice(inputPosition);
                            inp.value = before + selectedItem + after;
                        } else {
                            let before = inp.value.slice(0, inputPosition-stack.length);
                            inp.value = before + selectedItem;
                        }
                        stack = [];
                        this.dispatchEvent(new CustomEvent('inputChange', {
                            detail: {
                                name: 'inputChange', // this is the name to use in handler
                                value: inp.value,
                            },
                            bubbles: true
                        }));
                        /*close the list of autocompleted values,
                        (or any other open lists of autocompleted values:*/
                        closeAllLists();
                        // set focus to input after the inserted value
                        l = inputPosition + selectedItem.length - 1;
                        if (inp.setSelectionRange) {
                            inp.focus();
                            inp.selectionStart = l;
                            inp.selectionEnd = l;
                        }
                        // within the closure, we know what the current cursor
                        // position is for the input.  But it is not known
                        // outside the closure.  So, we need to dispatch an event
                        // that will be handled by handleInputPositionChange(e)
                        // which will set this.inputPosition.
                        inp.dispatchEvent(new CustomEvent('inputPositionChange', {
                            detail: {
                                name: 'inputPositionChange', // this is the name to use in handler
                                value: l,
                            },
                            bubbles: true
                        }));
                    });
                    a.appendChild(b);
                }
            }
        });
        inp.addEventListener("keyup", function(e) {
            let x = document.getElementById(this.id + "autocomplete-list");
            if (x) x = x.getElementsByTagName("div");
            switch (e.keyCode) {
                case 13: { // Enter / Return key.  Close list.
                    let x = document.getElementsByClassName("autocomplete-items");
                    for (let i = 0; i < x.length; i++) {
                        x[i].parentNode.removeChild(x[i]);
                    }
                    return;
                }
            }
        });
                    /* execute a function presses a key on the keyboard:*/
        inp.addEventListener("keydown", function(e) {
            let x = document.getElementById(this.id + "autocomplete-list");
            if (x) x = x.getElementsByTagName("div");
            switch (e.keyCode) {
                case 13: {
                    /*If the ENTER key is pressed, prevent the form from being submitted,*/
                   e.preventDefault();
                    if (currentFocus > -1) {
                        /*and simulate a click on the "active" item:*/
                        if (x) x[currentFocus].click();
                    }
                    return;
                }
                case 38: {
                    /*If the arrow UP key is pressed,
                    decrease the currentFocus variable:*/
                    currentFocus--;
                    /* and make the current item more visible:*/
                    addActive(x);
                    return;
                }
                case 40: {
                    /*If the arrow DOWN key is pressed,
                    increase the currentFocus variable:*/
                    currentFocus++;
                    /* and make the current item more visible:*/
                    addActive(x);
                    return;
                }
            }
        });

        function addActive(x) {
            /*a function to classify an item as "active":*/
            if (!x) return false;
            /*start by removing the "active" class on all items:*/
            removeActive(x);
            if (currentFocus >= x.length) currentFocus = 0;
            if (currentFocus < 0) currentFocus = (x.length - 1);
            /*add class "autocomplete-active":*/
            x[currentFocus].classList.add("autocomplete-active");
        }
        function removeActive(x) {
            /*a function to remove the "active" class from all autocomplete items:*/
            for (let i = 0; i < x.length; i++) {
                x[i].classList.remove("autocomplete-active");
            }
        }
        function closeAllLists(element) {
            /*close all autocomplete lists in the document,
            except the one passed as an argument:*/
            let x = document.getElementsByClassName("autocomplete-items");
            for (let i = 0; i < x.length; i++) {
                if (element !== x[i] && element !== inp) {
                    x[i].parentNode.removeChild(x[i]);
                }
            }
        }
        /*execute a function when someone clicks in the document:*/
        document.addEventListener("click", function (e) {
            closeAllLists(e.target);
        });
    }
    handleInputPositionChange(e) {
        e.preventDefault;
        this.inputPosition = e.detail.value;
    }
    checkForDoxaMarkDirectives() {
        if (this.inputValue.includes(`\\m(`) || this.inputValue.includes(`\\μ(`)) {
            this.deselectDiacriticInsensitive();
            this.dropdownItemDiA.classList.add("d-none");
            this.dropdownItemDiG.classList.add("d-none");
        } else {
            this.dropdownItemDiA.classList.remove("d-none");
            this.dropdownItemDiG.classList.remove("d-none");
        }
    }
    setInputPosition(ev) {
        this.inputPosition = ev.target.selectionStart;
        this.inputValue = this.inputComponent.value;
        this.checkForDoxaMarkDirectives()
        switch (ev.key) {
            case "Enter": {
                // the event below will be handled by the user of the component
                this.dispatchEvent(new CustomEvent('enterKeyPressed', {
                    detail: {
                        name: 'enterKeyPressed',
                        value: this.inputComponent.value,
                        caseInsensitive: this.caseInsensitive,
                        diacriticInsensitive: this.diacriticInsensitiveAll,
                        wholeWord: this.wholeWord,
                        greek: this.diacriticInsensitiveGreek,
                    },
                    bubbles: true
                }));
                this.putPreviousRegExPatterns(this.inputComponent.value);
                break;
            }
        }
    }
    handleInputChange(ev) {
        if (ev && ev.target) {
            ev.preventDefault();
            ev.stopPropagation();
            this.inputValue = this.inputComponent.value;
            this.dispatchEvent(new CustomEvent('inputChange', {
                detail: {
                    name: 'inputChange', // this is the name to use in handler
                    value: this.inputComponent.value,
                },
                bubbles: true
            }));
        }
        return false; // to stop window scrolling
    }
    deselectDiacriticInsensitive() {
        if (this.diacriticInsensitiveAll) {
            this.toggleDiacriticInsensitive();
            this.inputDiacriticInsensitive.checked = this.diacriticInsensitiveAll;
        }
        if (this.diacriticInsensitiveGreek) {
            this.toggleGreek();
            this.inputGreek.checked = this.diacriticInsensitiveGreek;
        }
    }
    handlePreviousRegExClick(ev) {
        if (ev && ev.target) {
            ev.preventDefault();
            let selectedItem = ev.target.innerText;
            if (selectedItem) {
                this.inputComponent.value = selectedItem;
                this.inputValue = selectedItem;
                this.checkForDoxaMarkDirectives();
                this.inputPosition = this.inputComponent.value.length;
                // set focus to input
                if (this.inputComponent.setSelectionRange) {
                    this.inputComponent.focus();
                    this.inputComponent.selectionStart = this.inputPosition;
                    this.inputComponent.selectionEnd = this.inputPosition;
                    this.inputComponent.focus();
                }
            }
        }
    }
    handleItemClick(ev) {
        if (ev && ev.target) {
            ev.preventDefault();

            let selectedItem = ev.target.getAttribute("data-key");
            const l = this.inputComponent.value.length;
            if (this.inputPosition < l) {
                let before = this.inputComponent.value.slice(0, this.inputPosition);
                let after = this.inputComponent.value.slice(this.inputPosition);
                this.inputComponent.value = before + selectedItem + after;
            } else {
                let before = this.inputComponent.value.slice(0, this.inputPosition);
                this.inputComponent.value = before + selectedItem;
            }

            this.dispatchEvent(new CustomEvent('inputChange', {
                detail: {
                    name: 'inputChange', // this is the name to use in handler
                    value: this.inputComponent.value,
                },
                bubbles: true
            }));
            // // close list
            let x = ev.currentTarget.parentElement;
            for (let i = 0; i < x.length; i++) {
                x[i].parentNode.removeChild(x[i]);
            }
            this.inputPosition = this.inputPosition + selectedItem.length;
            // set focus to input
            if (this.inputComponent.setSelectionRange) {
                this.inputComponent.focus();
                this.inputComponent.selectionStart = this.inputPosition;
                this.inputComponent.selectionEnd = this.inputPosition;
                this.inputComponent.focus();
            }
        }
        return false; // to stop window scrolling
    }

    renderItem(item) {
        if (!item) {
            return
        }
        // <li><a className="dropdown-item" href="#">Action</a></li>
        let theLi = document.createElement("li");
        if (item.Key === "dropdown-divider") {
            theLi.classList.add("dropdown-divider")
        } else {
            theLi.innerHTML = `<a class="dropdown-item" data-key="${item.Key}" href="#">${item.Desc}</a>`
        }
        this.listComponent.appendChild(theLi);
    }

    renderList() {
        if (! this.listComponent) {
            return;
        }
        this.listComponent.innerHTML = "";
        if (this.listComponent) {
            this.listArray.forEach((item, index) => {
                item.Selected = index === this.active;
                this.renderItem(item, index);
            });
        }
    }

    static get observedAttributes() {
        return ['gid', 'items', 'active', 'title', 'form', 'help', 'ci', 'di', 'ww', "gr"];
    }

    //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
        switch (name) {
            case "gid": {
                if (newVal !== null && oldVal !== newVal) { // make the prefix unique across uses of this component
                    this.lsPrefix = this.lsPrefixBase + newVal;
                    this.getPreviousRegExPatterns();
                }
                break;
            }
            case "items": {
                if (newVal !== null && oldVal !== newVal) {
                    this.listArray = JSON.parse(newVal);
                    this.renderList();
                    this.autocomplete(this.inputComponent, this.listArray);
                }
                break;
            }
            case "active": {
                if (newVal !== null && oldVal !== newVal) {
                    this.renderList();
                }
                break;
            }
            case "ci": {
                if (newVal !== null && oldVal !== newVal) {
                    const l = newVal.toLowerCase();
                    this.caseInsensitive = l === "true" || l === "t" || l === "y";
                    if (this.inputCaseInsensitive) {
                        this.inputCaseInsensitive.checked = this.caseInsensitive;
                    }
                }
                break;
            }
            case "di": {
                if (newVal !== null && oldVal !== newVal) {
                    const l = newVal.toLowerCase();
                    this.diacriticInsensitiveAll = l === "true" || l === "t" || l === "y";
                    if (this.inputDiacriticInsensitive) {
                        this.inputDiacriticInsensitive.checked = this.diacriticInsensitiveAll;
                    }
                }
                break;
            }
            case "gr": {
                if (newVal !== null && oldVal !== newVal) {
                    const l = newVal.toLowerCase();
                    this.diacriticInsensitiveGreek = l === "true" || l === "t" || l === "y";
                    if (this.inputGreek) {
                        this.inputGreek.checked = this.diacriticInsensitiveGreek;
                    }
                }
                break;
            }
            case "ww": {
                if (newVal !== null && oldVal !== newVal) {
                    const l = newVal.toLowerCase();
                    this.wholeWord = l === "true" || l === "t" || l === "y";
                    if (this.inputWholeWord) {
                        this.inputWholeWord.checked = this.wholeWord;
                    }
                }
                break;
            }
            case "help": {
                if (newVal !== null && (newVal === "true" || newVal === "yes" || newVal === "y")) {
                    this.querySelector(`#idHelpBlockRegEx`).classList.remove("d-none");
                } else {
                    this.querySelector(`#idHelpBlockRegEx`).classList.add("d-none");
                }
                break;
            }
        }
    }

    adoptedCallback() {
    }

    get active() {
        return this.getAttribute("active");
    }
    set active(value) {
        this.setAttribute("active", value);
    }
    get ci() { // flag Case insensitive
        return this.getAttribute("ci");
    }
    set ci(value) {
        this.setAttribute("ci", value);
    }
    get di() { // flag Diacritic insensitive
        return this.getAttribute("di");
    }
    set di(value) {
        this.setAttribute("di", value);
    }
    get form() {
        return this.getAttribute("form");
    }
    set form(value) {
        this.setAttribute("form", value);
    }
    get gid() { // group id
        return this.getAttribute("gid");
    }
    set gid(value) {
        this.setAttribute("gid", value);
    }
    get gr() { // flag Whole word
        return this.getAttribute("gr");
    }
    set gr(value) {
        this.setAttribute("gr", value);
    }

    get help() {
        return this.getAttribute("help");
    }
    set help(value) {
        this.setAttribute("help", value);
    }

    get inputValue() {
        return this.getAttribute("inputValue");
    }
    set inputValue(value) {
        this.setAttribute("inputValue", value);
    }

    get items() {
        return this.getAttribute("items");
    }
    set items(value) {
        this.setAttribute("items", value);
    }

    get title() { // group id
        return this.getAttribute("title");
    }
    set title(value) {
        this.setAttribute("title", value);
    }
    get ww() { // flag Whole word
        return this.getAttribute("ww");
    }
    set ww(value) {
        this.setAttribute("ww", value);
    }
}

window.customElements.define('regex-input', RegexInput);