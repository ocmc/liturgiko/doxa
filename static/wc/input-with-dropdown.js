// InputWithDropdown provides an input text field with a dropdown list
// When the user make a selection from a list, the select is inserted at the end
// of the current value of the input and a custom event, inputChanged is generated.
// When the action button is clicked a custom event will be generated.
class InputWithDropdown extends HTMLElement {
    // generates custom event "inputChanged". ev.detail.value contains the value of the input component.
    /**
     * TAG ATTRIBUTES:
     *   gid: a unique id for the list so if the tag is used multiple times on a page, they will be treated as separate groups
     *   items: a json stringified object array as described below.
     *   active: the numeric index of the item to set as selected.
     * USAGE: see how used in synch-manager.
     * 1. In constructor, set an array object with radio options:
     *    e.g.,
     *    this.strategyOptions = [
     *       {Key: "combine", Desc: "Make remote repos, local repos and database content identical, but keep changes I made since the last synch.", Selected: true, Class: "alert-success", Icon: "", IconColor: ""},
     *       {Key: "resetToRemote", Desc: "Throw away database and local repo changes. Make the local repos and database identical to the remote repos.", Selected: false, Class: "alert-warning", Icon: "", IconColor: ""},
     *       {Key: "resetToDB", Desc: "Throw away remote repo changes. Make the remote repos identical to local repos and the database contents.", Selected: false, Class: "alert-warning", Icon: "", IconColor: ""},
     *     ];
     *     The option object attributes are:
     *       Key - the unique ID that will be returned when the item is selected
     *       Desc - what the user sees
     *       Class - (optional) the Bootstrap Alert class. Used to set the font color and background color of the item
     *       Icon - (optional) the Bootstrap icon to prefix to the Desc value
     *       IconColor - (optional) the color of the icon
     * 2. Add <dropdown-list it="someIdYouCreate" items="[]" active="0" title="My list"></dropdown-list>
     *       items - use JSON.stringify() to turn the array of option objects into string to pass as tag
     *       active - the index number of the selected item
     * 3. Add an event handler to get notified when someone selects an option:
     *      this.querySelector("#someIdYouCreate").addEventListener("change", (ev) => this.yourHandlerFunction(ev));     * To Get Selected Item, in the using javascript:
     * 4. Create the handler function:
     *   myHandlerFunction(ev) {
     *     // to get value selected
     *     let selectedItem = ev.target.getAttribute("value");
     *    // to get value selected
     *    let selectedItemIndex = ev.target.getAttribute("index");
     *   }
     */
    constructor() {
        super();
        console.log("static/wc/dropdown-list.js")
        this.listArray = null;
        this.listComponent = null;
        this.inputComponent = null;
        this.inputPosition = 0;
    }

    connectedCallback() {
        this.innerHTML =
            `
<style> 
  @import url('static/css/bootstrap.min.css');
  @import url('static/img/bootstrap-icons.css');
</style>
<style>
.input-with-dropdown-menu {
  max-height: 20vh;
  overflow-y: scroll;
  max-width: 50vw;
  overflow-x: scroll;
}
.autocomplete {
  /*the container must be positioned relative:*/
  position: relative;
  display: inline-block;
}
input {
  border: 1px solid transparent;
  background-color: #f1f1f1;
  padding: 10px;
  font-size: 16px;
}
input[type=text] {
  background-color: #f1f1f1;
  width: 100%;
}
input[type=submit] {
  background-color: DodgerBlue;
  color: #fff;
}
.autocomplete-items {
  position: absolute;
  border: 1px solid #d4d4d4;
  border-bottom: none;
  border-top: none;
  z-index: 99;
  /*position the autocomplete items to be the same width as the container:*/
  top: 100%;
  left: 0;
  right: 0;
  max-height: 20vh;
  overflow-y: scroll;
  max-width: 50vw;
  overflow-x: scroll;
}
.autocomplete-items div {
  padding: 10px;
  cursor: pointer;
  background-color: #fff; 
  border-bottom: 1px solid #d4d4d4; 
}
.autocomplete-items div:hover {
  /*when hovering an item:*/
  background-color: #e9e9e9; 
}
.autocomplete-active {
  /*when navigating through the items using the arrow keys:*/
  background-color: DodgerBlue !important; 
  color: #ffffff; 
}

</style>

<div class="input-group">
  <input id="inputComponent_${this.gid}" type="text" class="form-control" aria-label="enter regular expression">
  <button type="button" class="btn btn-outline-secondary">${this.title}</button>
  <button type="button" class="btn btn-outline-secondary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" data-bs-display="static" aria-expanded="false">
    <span class="visually-hidden">Toggle Dropdown</span>
  </button>
  <ul id="listComponent_${this.gid}" class="dropdown-menu dropdown-menu-end dropdown-menu{-sm|-md|-lg|-xl|-xxl}-end input-with-dropdown-menu">
  </ul>
</div>
`;

        this.inputComponent = this.querySelector(`#inputComponent_${this.gid}`);
        this.inputComponent.addEventListener("change", (e) => this.handleInputChange(e));
        this.inputComponent.addEventListener("keyup", (e) => this.setInputPosition(e));
        this.listComponent = this.querySelector(`#listComponent_${this.gid}`);
        this.listComponent.addEventListener("click", (e) => this.handleItemClick(e));
        if (this.listArray && this.listArray.length > 0) {
            this.renderList()
        }
        if (this.listArray) {
            this.autocomplete(this.inputComponent, this.listArray);
        }
    }
    // for every event handler added above, below remove it
    disconnectedCallback() {
        this.listComponent.removeEventListener("click", (e) => this.handleItemClick(e));
        this.inputComponent.removeEventListener("change", (e) => this.handleInputChange(e));
        this.inputComponent.removeEventListener("keyup", (e) => this.setInputPosition(e));
    }

    autocomplete(inp, arr) {
        // source: https://www.w3schools.com/howto/howto_js_autocomplete.asp
        /*the autocomplete function takes two arguments,
        the text field element and an array of possible autocompleted values:*/
        if (!inp || arr.length === 0) {
            return;
        }
        let currentFocus;
        let inputPosition = 0;
        let lastLength = 0;
        let stack = [];
        // inp.addEventListener("keyup", (e) =>  {
        //     inputPosition = e.target.selectionStart;
        //     console.log(`keyup inputPosition: ${inputPosition}`)
        // });
        /*execute a function when someone writes in the text field:*/
        inp.addEventListener("input", function(e) {
            inputPosition = e.target.selectionStart;
            if (inputPosition < lastLength) {
                const j = lastLength - inputPosition + 1;
                for (let i = 0; i < j; i++) {
                    if (stack.length > 0) {
                        stack.pop();
                    }
                }
                lastLength = inputPosition;
            }

            let c = this.value.slice(inputPosition-1, inputPosition);
            if (c.length > 0) {
                stack.push(c);
            }
            lastLength = this.value.length;
            let a, b, i, val = this.value; // i.e., inp.value (what has been entered into the field)
            val = stack.join('');
            console.log(`inputPosition: ${inputPosition} lastLength: ${lastLength} c: ${c} val: ${val} stack.length: ${stack.length} stack: ${JSON.stringify(stack)}`)
            /*close any already open lists of autocompleted values*/
            closeAllLists();
            if (!val) { return false;}
            currentFocus = -1;
            /*create a DIV element that will contain the items (values):*/
            a = document.createElement("DIV");
            a.setAttribute("id", this.id + "autocomplete-list");
            a.setAttribute("class", "autocomplete-items");
            /*append the DIV element as a child of the autocomplete container:*/
            this.parentNode.appendChild(a);
            /*for each item in the array...*/
            for (i = 0; i < arr.length; i++) {
                /*check if the item starts with the same letters as the text field value:*/
                if (arr[i].Key.substr(0, val.length).toUpperCase() === val.toUpperCase()) {
                    /*create a DIV element for each matching element:*/
                    b = document.createElement("DIV");
                    /*make the matching letters bold:*/
                    b.innerHTML = "<strong>" + arr[i].Key.substr(0, val.length) + "</strong>";
                    b.innerHTML += arr[i].Desc.substr(val.length);
                    /*insert an input field that will hold the current array item's value:*/
                    b.innerHTML += "<input type='hidden' value='" + arr[i].Key + "'>";
                    /*execute a function when someone clicks on the item value (DIV element):*/
                    b.addEventListener("click", function(e) {
                        /*insert the value for the autocomplete text field:*/
                        const selectedItem = this.getElementsByTagName("input")[0].value;
                        const l = inp.value.length;
                        if (inputPosition < l) {
                            let before = inp.value.slice(0, inputPosition-stack.length);
                            let after = inp.value.slice(inputPosition);
                            inp.value = before + selectedItem + after;
                        } else {
                            let before = inp.value.slice(0, inputPosition-stack.length);
                            inp.value = before + selectedItem;
                            //inp.value = inp.value.slice(l-stack.length-1) + selectedItem;
                        }
                        stack = [];
                        this.dispatchEvent(new CustomEvent('inputChange', {
                            detail: {
                                name: 'inputChange', // this is the name to use in handler
                                value: inp.value,
                            },
                            bubbles: true
                        }));
                        /*close the list of autocompleted values,
                        (or any other open lists of autocompleted values:*/
                        closeAllLists();
                    });
                    a.appendChild(b);
                }
            }
        });
        /* execute a function presses a key on the keyboard:*/
        inp.addEventListener("keydown", function(e) {
            let x = document.getElementById(this.id + "autocomplete-list");
            if (x) x = x.getElementsByTagName("div");
            switch (e.keyCode) {
                case 13: {
                    /*If the ENTER key is pressed, prevent the form from being submitted,*/
                    e.preventDefault();
                    if (currentFocus > -1) {
                        /*and simulate a click on the "active" item:*/
                        if (x) x[currentFocus].click();
                    }
                    return;
                }
                case 38: {
                    /*If the arrow UP key is pressed,
                    decrease the currentFocus variable:*/
                    currentFocus--;
                    /* and make the current item more visible:*/
                    addActive(x);
                    return;
                }
                case 40: {
                    /*If the arrow DOWN key is pressed,
                    increase the currentFocus variable:*/
                    currentFocus++;
                    /* and make the current item more visible:*/
                    addActive(x);
                    return;
                }
            }
        });

        function addActive(x) {
            /*a function to classify an item as "active":*/
            if (!x) return false;
            /*start by removing the "active" class on all items:*/
            removeActive(x);
            if (currentFocus >= x.length) currentFocus = 0;
            if (currentFocus < 0) currentFocus = (x.length - 1);
            /*add class "autocomplete-active":*/
            x[currentFocus].classList.add("autocomplete-active");
        }
        function removeActive(x) {
            /*a function to remove the "active" class from all autocomplete items:*/
            for (let i = 0; i < x.length; i++) {
                x[i].classList.remove("autocomplete-active");
            }
        }
        function closeAllLists(elmnt) {
            /*close all autocomplete lists in the document,
            except the one passed as an argument:*/
            var x = document.getElementsByClassName("autocomplete-items");
            for (let i = 0; i < x.length; i++) {
                if (elmnt !== x[i] && elmnt !== inp) {
                    x[i].parentNode.removeChild(x[i]);
                }
            }
        }
        /*execute a function when someone clicks in the document:*/
        document.addEventListener("click", function (e) {
            closeAllLists(e.target);
        });
    }
    setInputPosition(ev) {
        this.inputPosition = ev.target.selectionStart;
    }
    handleInputChange(ev) {
        if (ev && ev.target) {
            ev.preventDefault();
            ev.stopPropagation();
            this.inputValue = this.inputComponent.value;
            this.dispatchEvent(new CustomEvent('inputChange', {
                detail: {
                    name: 'inputChange', // this is the name to use in handler
                    value: this.inputComponent.value,
                },
                bubbles: true
            }));
        }
        return false; // to stop window scrolling
    }

    handleItemClick(ev) {
        if (ev && ev.target) {
            ev.preventDefault();
            ev.stopPropagation();
            let selectedItem = ev.target.getAttribute("data-key");
            const l = this.inputComponent.value.length;
            if (this.inputPosition < l) {
                let before = this.inputComponent.value.slice(0,this.inputPosition);
                let after = this.inputComponent.value.slice(this.inputPosition);
                this.inputComponent.value = before + selectedItem + after;
            } else {
                this.inputComponent.value = this.inputComponent.value + selectedItem;
            }
            this.dispatchEvent(new CustomEvent('inputChange', {
                detail: {
                    name: 'inputChange', // this is the name to use in handler
                    value: this.inputComponent.value,
                },
                bubbles: true
            }));

        }
        return false; // to stop window scrolling
    }

    renderItem(item) {
        if (!item) {
            return
        }
        // <li><a className="dropdown-item" href="#">Action</a></li>
        let theLi = document.createElement("li");
        if (item.Key === "dropdown-divider") {
            theLi.classList.add("dropdown-divider")
        } else {
            theLi.innerHTML = `<a class="dropdown-item" data-key="${item.Key}" href="#">${item.Desc}</a>`
        }
        this.listComponent.appendChild(theLi);
    }

    renderList() {
        if (! this.listComponent) {
            return;
        }
        this.listComponent.innerHTML = "";
        if (this.listComponent) {
            this.listArray.forEach((item, index) => {
                item.Selected = index === this.active;
                this.renderItem(item, index);
            });
        }
    }

    static get observedAttributes() {
        return ['gid', 'items', 'active', 'title'];
    }

    //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
        switch (name) {
            case "items": {
                if (newVal !== null && oldVal !== newVal) {
                    this.listArray = JSON.parse(newVal);
                    this.renderList();
                    this.autocomplete(this.inputComponent, this.listArray);
                }
                break;
            }
            case "active": {
                if (newVal !== null && oldVal !== newVal) {
                    this.renderList();
                }
                break;
            }
        }
    }

    adoptedCallback() {
    }

    get gid() { // group id
        return this.getAttribute("gid");
    }
    set gid(value) {
        this.setAttribute("gid", value);
    }

    get inputValue() {
        return this.getAttribute("inputValue");
    }
    set inputValue(value) {
        this.setAttribute("inputValue", value);
    }

    get items() {
        return this.getAttribute("items");
    }
    set items(value) {
        this.setAttribute("items", value);
    }

    get active() {
        return this.getAttribute("active");
    }
    set active(value) {
        this.setAttribute("active", value);
    }
    get title() { // group id
        return this.getAttribute("title");
    }
    set title(value) {
        this.setAttribute("title", value);
    }
}

window.customElements.define('input-with-dropdown', InputWithDropdown);