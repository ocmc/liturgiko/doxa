class ModalLibraryPicker extends HTMLElement {
  constructor() {
    // always call super() first
    super();
    console.log("static/wc/modal-lib-picker.js")
  }

  connectedCallback() {
    // Create component
    const component = document.createElement("div");
    component.className = "bootstrap-shadow-dialog";
    // Create shadowRoot
    const shadowRoot = this.attachShadow({mode: 'open'});
    // Set inner HTML
    shadowRoot.innerHTML = `
      <style> 
		    @import url('static/css/bootstrap.min.css');
            @import url('static/img/bootstrap-icons.css');
      </style>
      <style>
       .modal {
          display:block;
          position: fixed;
          top: 200px;
       }
       .modal-header{
          text-transform: uppercase;
          color:#fff;
          background-color: #428bca;
        }
      </style>
      
      <div class="modal"  id="modal-dialog" data-bs-backdrop="static" data-bs-keyboard="False">
       <div class="modal-dialog shadow">
         <div class="modal-content">
           <div class="modal-header">
              <h4 class="modal-title">${this.title}</h4>
              <button type="button" class="close" id="closeBtn" data-dismiss="modal" aria-hidden="true">×</button>
           </div>
           <div class="modal-body">
             <div class="row">
             </div>
           </div>
           <div class="modal-footer">
              <button type="button" class="btn btn-outline-primary" data-dismiss="modal"  id="cancelBtn">${this.cancelBtn}</button>
              <button type="button" class="btn btn-danger" id="okBtn">${this.okBtn}</button>
           </div>
        </div><!-- /.modal-content -->
       </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
     </div>
  `;
    shadowRoot.getElementById("modal-dialog").addEventListener("click", e => {
      const targetId = e.target.id;
      if (targetId === "closeBtn" || targetId === "cancelBtn" || targetId === "okBtn") {
        // allow propagation
      } else {
        e.stopPropagation();
      }
    })
    // set focus to cancel button
    shadowRoot.getElementById("cancelBtn").focus();

    // Save copy of shadow root to component
    component._shadowRoot = shadowRoot;
    // Attach component to dom
    document.getElementsByTagName("body")[0].appendChild(component);
  }

  disconnectedCallback() {
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
  }

  adoptedCallback() {
  }

  static get observedAttributes() {
    return ['cancelBtn', 'okBtn', 'title'];
  }

  get cancelBtn() {
    return this.getAttribute("cancelBtn");
  }
  set cancelBtn(value) {
    this.setAttribute("cancelBtn", value);
  }
  get okBtn() {
    return this.getAttribute("okBtn");
  }
  set okBtn(value) {
    this.setAttribute("okBtn", value);
  }
  get title() {
    return this.getAttribute("title");
  }
  set title(value) {
    this.setAttribute("title", value);
  }
  get libraries() {
    return this.getAttribute("libraries");
  }
  set libraries(value) {
    this.setAttribute("libraries", value);
  }

}

window.customElements.define('modal-lib-picker', ModalLibraryPicker);