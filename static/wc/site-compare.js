import * as utils from "./utils.js";

class SiteCompare extends HTMLElement {
  constructor() {
    super();
    console.log("static/wc/site-compare.js")
    this.siteUrlOne = null;
    this.siteUrlTwo = null;
    this.redirectItems = [];
    this.requestedId = "";
    this.divRequestStatus = "";
    this.redirectList = null;
    this.editor = null;
    this.lsPrefix = "siteCmp"; // used to prefix local storage keys
    // local storage keys
    this.keySiteUrlOne = "url1";
    this.keySiteUrlTwo = "url2";
  }

  connectedCallback() {
    this.innerHTML =
`
    <style> 
      @import url('static/css/bootstrap.min.css');
      @import url('static/img/bootstrap-icons.css');
    </style>
<style>body {background-color: #fff;}</style>
<br>
<div id="my-element-div" class="wc-title">Site Comparison</div>
<br>
<hr>
<div>This tool compares the content of two DCS websites.</div>
<br>
<hr>
<div style="display: flex; flex-direction: column; flex-wrap: wrap; gap: 1rem; max-width: 50vw;">
  <label for="siteUrlOne" class="form-label">URL of Site 1</label>
  <input type="text" id="siteUrlOne" placeholder="enter the 1st URL" class="form-control" aria-labelledby="idHelpBlock">
  <label for="siteUrlTwo" class="form-label">URL of Site 2</label>
  <input type="text" id="siteUrlTwo" placeholder="enter the 2nd URL" class="form-control" aria-labelledby="idHelpBlock">
  <div id="idHelpBlock" class="form-text">
  Enter the URL for each of the two sites you want to compare, then click the Compare button.  
  </div>
  <div style="display: flex; flex-direction: row; gap: 20px;">  <!-- Submit Button -->
      <span>
          <button id="btnRequest"  type="button" class="btn btn-primary" style="">Compare</button>
      </span>
      <br>
      <div class="alert alert-light message" role="alert" id="requestStatus" style="padding: 3px; width: max-content;"></div>
  </div> <!-- end Import Button -->
  </div>
<br>
<hr>
<br>
<! -- Redirect list-->
<div style="display: flex; flex-direction: row; flex-wrap: wrap; gap: 1rem;">
  <ol class="list-group list-group-numbered" id="redirectList"></ol>
</div>
<div id="editor"></div>
`;
    this.editor = this.querySelector("#editor");
    this.redirectList = this.querySelector("#redirectList");
    this.divRequestStatus = this.querySelector("#requestStatus");
    this.siteUrlOne = this.querySelector("#siteUrlOne");
    this.siteUrlTwo = this.querySelector("#siteUrlTwo");
    this.requestButton = this.querySelector("#btnRequest");
    // add listeners.  For each one added, remove it in disconnectedCallback.
    this.siteUrlOne.addEventListener("change", (ev) => this.setUrlOne(ev));
    this.siteUrlTwo.addEventListener("change", (ev) => this.setUrlTwo(ev));
    this.requestButton.addEventListener("click", (e) => this.compareSites(e))
    this.getLocalValues();
  }

  disconnectedCallback() {
    this.siteUrlOne.removeEventListener("change", (ev) => this.setUrlOne(ev));
    this.siteUrlTwo.removeEventListener("change", (ev) => this.setUrlTwo(ev));
    this.requestButton.removeEventListener("click", (e) => this.compareSites(e))
  }

  getLocalValues() {
    this.siteUrlOne.value = utils.lsGet(this.lsPrefix, this.keySiteUrlOne);
    this.siteUrlTwo.value = utils.lsGet(this.lsPrefix, this.keySiteUrlTwo);
  }
  setUrlOne(ev) {
    ev.stopPropagation();
    ev.preventDefault();
    utils.lsSet(this.lsPrefix, this.keySiteUrlOne, ev.target.value);
  }
  setUrlTwo(ev) {
    ev.stopPropagation();
    ev.preventDefault();
    utils.lsSet(this.lsPrefix, this.keySiteUrlTwo, ev.target.value);
  }

  compareSites(ev) {
    if (ev) {
      ev.stopPropagation();
      ev.preventDefault();
    }
    this.requestButton.disabled = true;
    this.requestStatusMessage(utils.alerts.Primary, "Comparing...")
    this.requestComparisobn().then(response => {
      this.requestButton.disabled = false;
      if (response.Status === "OK") {
        console.log(JSON.stringify(response));
        this.requestStatusMessage(utils.alerts.Success, response.Message);
      } else {
        this.requestStatusMessage(utils.alerts.Danger, `${response.Status}: ${response.Message}`)
      }
    }).catch(error => {
      this.requestButton.disabled = false;
      this.requestStatusMessage(utils.alerts.Danger, error)
    });
  }

  // ---- MESSAGE Handlers
  requestStatusMessage(type, message) {
    this.divRequestStatus.className = "";
    this.divRequestStatus.classList.add(type);
    this.divRequestStatus.innerHTML = utils.getMessageSpan(type, message);
  }
  // ---- REST API Handlers

  async requestComparisobn() {
    const url = this.host
        + `api/site/cmp?url1=${this.siteUrlOne.value}`
        +  `&url2=${this.siteUrlTwo.value}`
    ;
    let fetchData = {
      method: 'GET'
    }

    const response = await fetch(url, fetchData);
    if (!response.ok) {
      let message = "";
      switch (response.status) {
        case 404: {
          message += "not found";
          break;
        }
        case 500: {
          message += "doxa server error";
          break;
        }
        default: {
          message += `An error has occurred: ${response.status}`;
        }
      }
      throw new Error(message);
    }
    const responseJson = await response.json();
    return responseJson;
  }
// ---- End REST API Handlers

  static get observedAttributes() {
    return ['host'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
  }

  adoptedCallback() {
  }
  get host() {
    return this.getAttribute("host");
  }

  set host(value) {
    this.setAttribute("host", value);
  }
}

window.customElements.define('site-compare', SiteCompare);