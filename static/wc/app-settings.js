class AppSettings extends HTMLElement {
    constructor() {
        super();
        console.log("static/wc/app-settings.js")

        // this.theData holds the ids and values returned by an api call
        this.theData = {};
        // this.dataIndexArray is an array of integers, which are indexes into this.theData.
        // this.dataIndexArray is essentially a list of pointers to values that match a filter.
        // this.theData[this.dataIndexArray[i]].Value is the underlying value.
        this.dataIndexArray = [];

        this.curPage = 1;
        this.currentIdFilter = "";
        this.currentValueFilter = "";
        this.dataLength = 0;
        this.dataLength = 1;
        this.pageSize = 10;
        this.scrollY = "";
    }

    connectedCallback() {
        this.innerHTML =
            `
<style> 
  @import url('static/css/bootstrap.min.css');
  @import url('static/img/bootstrap-icons.css');
</style>
<style>
    td {
      white-space: normal !important; 
      word-wrap: break-word;  
    }
    table {
      table-layout: fixed;
    }
    .alert {
      line-height: 1px;
    }
    .filterColor {
      color: blue;
    }
    .page-title {
        color: blue;
        font-style: italic;
    }
</style>
<h3 class="text-center page-title">Settings Manager</h3>
<!-- Help -->
    <div class="row p-1">
      <div class="container-fluid" >
        <help-info text="You may have multiple named configurations (aka settings). Whichever one you select will be used to generate your web site.  IDs that end in :desc provide a description of a setting or group of settings. They are shown in bold italic and can't be edited by you. IDs that end in :value can be edited (changed) by clicking in the text.  When you tab out of the text, or click outside it, it will be updated in the database."></help-info>
      </div>
    </div>
<!-- Selector -->
    <div class="row p-1">
      <div class="container-fluid" >
            <div class="col">
                <select id="availableConfigs" class="form-select w-25" aria-label="Default select example"></select>
            </div>
            <div class="col">
            </div>
            <div class="col">
            </div>
      </div>
    </div>
<!-- Filters -->
    <div class="row p-1">
        <div class="col">
            <div class="input-group mb-3">
                <span class="input-group-text" id="basic-addon1">
                    <i class="bi-filter" style="color: cornflowerblue;"></i>
                </span>
                <input type="text" id="app-settings-filter-id" class="form-control w-50" placeholder="filter ID" aria-label="filter ID" aria-describedby="basic-addon1">
            </div>
        </div>
        <div class="col">
        </div>
        <div class="col">
            <div class="input-group mb-3">
                <span class="input-group-text" id="basic-addon1">
                    <i class="bi-filter" style="color: cornflowerblue;"></i>
                </span>
                <input type="text" id="app-settings-filter-value" class="form-control w-50" placeholder="filter value" aria-label="filter value" aria-describedby="basic-addon1">
            </div>
        </div>
    </div>
<!-- Status -->
    <div class="row p-1">
      <div class="container-fluid" >
            <div class="col">
                <div class="alert alert-primary" id="app-settings-alert" role="alert">
                    <span style:>Status:</span>
                </div>
            </div>
            <div class="col">
            </div>
            <div class="col">
            </div>
      </div>
    </div>
<!-- Table -->
    <div class="row p-1">
        <div class="col">
            <div class="table-responsive">
                <table id="app-settings-data-table" class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col" style="width: 60% !important;">ID</th>
                        <th scope="col" style="width: 40% !important;">Value</th>
                    </tr>
                    </thead>
                    <tbody id="app-settings-tbody">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<!-- Pagination -->
     <div class="row p-1">
        <div class="col col-4">
            <div class="control-label" style="padding-top: 12px;">Showing rows <span id="rowTop">0</span> to <span id="rowBottom">0</span> of <span id="rowCount">0</span></div>
        </div>
        <div class="col col-4"><div class="control-label" style="margin-top: 4px; text-align: center;"><span>Page size: </span><input id="pageSizeId" style="max-width: 3ch; text-align: center;"></div></div>
        <div class="col col-4">
            <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-end">
              <li class="page-item">
                <a id="firstButton" class="page-link d-none" href="#" aria-label="First">
                  <span aria-hidden="true"><i class="bi-chevron-double-left"></i></span>
                </a>
              </li>
              <li class="page-item">
                <a id="prevButton" class="page-link d-none" href="#" aria-label="Previous">
                  <span aria-hidden="true"><i class="bi-chevron-left"></i></span>
                </a>
              </li>
              <li class="page-item">
                <div class="control-label" style="margin-top: 4px;"> 
                  <span style="margin-left: 4px;">Page</span>
                  <input style="margin-left: 2px;max-width: 4ch; text-align: center;" id="currentPage">
                  <span style="margin-left: 2px;">of</span>
                  <span style="margin-left: 2px;margin-right: 4px;" id="pageMax"></span>
                </div>
              </li>
              <li class="page-item">
                <a id="nextButton" class="page-link" href="#" aria-label="Next">
                  <span aria-hidden="true"><i class="bi-chevron-right"></i></span>
                </a>
              </li>
              <li class="page-item">
                <a id="lastButton" class="page-link" href="#" aria-label="Last">
                  <span aria-hidden="true"><i class="bi-chevron-double-right"></i></span>
                </a>
              </li>
            </ul>
          </nav>
        </div>
        </div>
     </div>
`;
        // add listeners.  For each one added, remove it in disconnectedCallback.
        this.querySelector('#app-settings-filter-id').addEventListener('keyup', (event) => this.filterData(event));
        this.querySelector('#app-settings-filter-value').addEventListener('keyup', (event) => this.filterData(event));

        this.querySelector('#currentPage').addEventListener("keypress", (e) => {
            if (e.key === 'Enter') {
                this.curPage = document.getElementById("currentPage").value;
                this.renderTable();
            }
        });
        // listen for page size change
        this.querySelector("#pageSizeId").addEventListener("keypress", (e) => {
            if (e.key === 'Enter') {
                this.pageSize = this.querySelector('#pageSizeId').value;
                this.curPage = 1;
                this.renderTable();
            }
        });

        this.querySelector('#firstButton').addEventListener('click', () => this.firstPage(this));
        this.querySelector('#prevButton').addEventListener('click', () => this.previousPage(this));
        this.querySelector('#nextButton').addEventListener('click', () => this.nextPage(this));
        this.querySelector('#lastButton').addEventListener('click', () => this.lastPage(this));


        this.setConfigsDropdown();
        this.fetchRecords().then();
    }

    disconnectedCallback() {
        this.querySelector('#app-settings-filter-id').removeEventListener('keyup', (event) => this.filterData(event));
        this.querySelector('#app-settings-filter-value').removeEventListener('keyup', (event) => this.filterData(event));
        this.querySelector('#firstButton').removeEventListener('click', () => this.firstPage(this));
        this.querySelector('#prevButton').removeEventListener('click', () => this.previousPage(this));
        this.querySelector('#nextButton').removeEventListener('click', () => this.nextPage(this));
        this.querySelector('#lastButton').removeEventListener('click', () => this.lastPage(this));
    }

    setConfigsDropdown() {
        let html = ``
        this.availableConfigs.split(",").forEach((item) => {
            if (item === this.currentConfig) {
                html += `<option value="${item}" selected>${item}</option>`
            } else {
                html += `<option value="${item}">${item}</option>`
            }
        });
        this.querySelector("#availableConfigs").innerHTML = html;
    }

    static get observedAttributes() {
        return ["availableConfigs", "currentConfig"];
    }

    //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
        switch (name) {
            case "availableConfigs": {
                this.setConfigsDropdown();
            }
        }
    }

    adoptedCallback() {
    }

    get availableConfigs() {
        return this.getAttribute("availableConfigs");
    }

    set availableConfigs(value) {
        this.setAttribute("availableConfigs", value);
    }

    get currentConfig() {
        return this.getAttribute("currentConfig");
    }

    set currentConfig(value) {
        this.setAttribute("currentConfig", value);
    }

    get host() {
        return this.getAttribute("host");
    }

    set host(value) {
        this.setAttribute("host", value);
    }
    async fetchRecords() {
        const url = `api/db/configs?id=` + this.currentConfig;
        return new Promise(async (res, rej) => {
            await fetch(url)
                .then(data => data.json())
                .then((json) => {
                    this.theData = json;
                    this.currentIdFilter = "";
                    this.currentValueFilter = "";
                    this.setAlert(this.theData.Values.length + " found");
                    this.setDataIndexForAllRows();
                    this.renderTable();
                    res();
                })
                .catch((error) => rej(error));
        })
    }
    getIdFilterRegEx() {
        return new RegExp(this.currentIdFilter, "i");
    }
    getValueFilterRegEx() {
        return new RegExp(this.currentValueFilter, "i");
    }
    renderTable() {
        const table = this.querySelector('#app-settings-tbody');
        // clear rows
        while (table.firstChild) {
            table.removeChild(table.firstChild);
        }

        this.dataLength = this.dataIndexArray.length;
        if (this.dataLength === 0) {
            this.setAlert("0 found");
            return;
        }
        let displaying = 0;
        let start = (this.curPage - 1) * this.pageSize;
        let end = this.curPage * this.pageSize;

        // set up filter regEx to highlight matching filter words in value column only
        let filterRegEx = {};
        if (this.currentValueFilter) {
            filterRegEx = this.getValueFilterRegEx();
        }

        // add new rows
        this.dataIndexArray.filter((row, index) => {
            if (index >= start && index < end) return true;
        }).forEach((item, index) => {
            displaying++;
            const dataItem = this.theData.Values[item];
            const tr = document.createElement('tr');
            tr.setAttribute("id", "app-settings-table-row-" + index)
            tr.setAttribute("data-id", dataItem.ID);

            // ID Cell
            const tdLibrary = document.createElement('td');
            tdLibrary.innerText = dataItem.ID;
            // Value Cell
            const tdValue = document.createElement('td');
            const value = document.createTextNode(dataItem.Value);
            tdValue.setAttribute("data-id", dataItem.ID);
            tdValue.setAttribute("data-index", item);
            tdValue.appendChild(value);

            // highlight the words matching the filter pattern
            if (this.currentValueFilter) {
                const words = dataItem.Value.match(filterRegEx);
                if (words && words.length > 0) {
                    let span = "<span class='filterColor'>" + words[0] + "</span>";
                    tdValue.innerHTML = tdValue.innerHTML.split(words[0]).join(span);
                }
            }
            if (dataItem.ID.endsWith(":desc")) {
                tr.style.fontWeight = "bold";
                tr.style.fontStyle = "italic";
            }
            if (dataItem.ID.endsWith(":value")) {
                tdValue.setAttribute("contenteditable", "true");
                tdValue.addEventListener("focusout", (event) => this.saveProperty(event, value));
            }

            // add cells to row
            tr.appendChild(tdLibrary);
            tr.appendChild(tdValue);

            table.appendChild(tr);
        });
        this.setAlertInfo(this.dataLength.toLocaleString() + " found");
        this.setTableSizeNotice(start + 1, start + displaying);
    }
    saveProperty(event, value) {
        event.preventDefault();
        event.stopPropagation();
        let originalIsBool = false;
        let originalIsNumber = false;
        let newIsBool = false;
        let newIsNumber = false;
        const dataId = event.target.getAttribute("data-id");
        const dataIndex = event.target.getAttribute("data-index");
        const originalValue = this.theData.Values[dataIndex].Value;
        originalIsBool = originalValue === "true" || originalValue === "false";
        if (originalValue.length > 0) { // original val of string can be empty string
            originalIsNumber =  !Number.isNaN(Number(originalValue));
        }

        let newValue = event.target.innerText.trim();
        if (newValue.length < 1) {
            return;
        }
        newValue = newValue.replace(/(\r\n|\n|\r|\t)/gm, "");
        newIsBool = newValue === "true" || newValue === "false";
        if (newValue.length > 0) { //  val of string can be empty string
            newIsNumber =  !Number.isNaN(Number(newValue));
        }
        // if this is supposed to be a bool, verify the new value is
        if (originalIsBool && ! newIsBool) {
            alert("must be true or false");
            event.target.innerText = originalValue;
            return;
        }
        if (newIsBool && !originalIsBool) {
            alert("can not be true or false");
            event.target.innerText = originalValue;
            return;
        }
        // if this is supposed to be a number, verify the new value is a number
        if (originalIsNumber && ! newIsNumber) {
                alert("must be a number");
                event.target.innerText = originalValue;
                return;
        }
        if (newIsNumber && ! originalIsNumber ) {
            alert("can not be a number");
            event.target.innerText = originalValue;
            return;
        }
        event.target.innerText = newValue; // so it won't have line breaks
        if (newValue !== originalValue) { // update the database
            this.updateDbRecord(dataId, newValue).then(r => {
                this.setAlertInfo(r);
                if (r.trim() === "updated") { // update the underlying data in the browser
                    this.theData.Values[dataIndex].Value = newValue;
                }
            });
        }
    }
    async updateDbRecord(id, value) {
        const url = this.host
            + 'api/db/update?id='
            + encodeURIComponent(id)
            + "&value=" + encodeURIComponent(value)
        ;
        let fetchData = {
            method: 'PUT'
        }
        return new Promise(async (res, rej) => {
            await fetch(url, fetchData)
                .then(data => data.json())
                .then((json) => {
                    res(json.Status);
                    return res;
                })
                .catch((error) => rej(error));
        });
    }

    setAlertInfo(msg) {
        this.querySelector("#app-settings-alert").className = "alert alert-info";
        this.setAlert(msg);
    }

    setAlertSuccess(msg) {
        this.querySelector("#app-settings-alert").className = "alert alert-success";
        this.setAlert(msg);
    }

    setAlert(msg) {
        this.querySelector("#app-settings-alert").innerText = "Status: " + msg;
    }
    // Populates theDataIndexArray with indexes of this.theData.Values.
    // If pattern is null, all the indexes of this.theData.Values will be pushed onto the array.
    // If pattern is not null, only indexes of data IDs or Values that match will be pushed onto the array.
    filterData(evt) {
        // // ignore keypress unless user presses enter key
        // if (evt.keyCode !== 13) {
        //   return;
        // }
        this.currentIdFilter = this.querySelector("#app-settings-filter-id").value;
        this.currentValueFilter = this.querySelector("#app-settings-filter-value").value;
        this.dataIndexArray = [];
        if (this.currentIdFilter.length > 0 || this.currentValueFilter.length > 0) {
            // Even as recent as Jan 2023, there is an ECMAScript 3 Javascript bug such that
            // regex test only matches every other string.
            // See https://stackoverflow.com/questions/3891641/regex-test-only-works-every-other-time
            // This is why, below, we keep re-creating the regex and wrap it in parentheses.
            this.theData.Values.forEach((item, index) => {
                console.log(`${item.ID}`)
                // if both the ID and Value filter have a pattern, both must match
                if (this.currentIdFilter.length > 0 && this.currentValueFilter.length > 0) {
                    if ((this.getIdFilterRegEx()).test(item.ID) && (this.getValueFilterRegEx()).test(item.Value)) {
                        this.dataIndexArray.push(index);
                    } else {
                    }
                } else if (this.currentIdFilter.length > 0) { // only need to match ID filter
                    if ((this.getIdFilterRegEx()).test(item.ID)) {
                        this.dataIndexArray.push(index);
                    }
                } else if (this.currentValueFilter.length > 0) { // only need to match Value filter
                    if ((this.getValueFilterRegEx()).test(item.Value)) {
                        this.dataIndexArray.push(index);
                    }
                }
            });
        } else {
            this.setDataIndexForAllRows();
        }
        this.curPage = 1;
        this.renderTable();
    }

    setDataIndexForAllRows() {
        this.dataIndexArray = [];
        if (!this.theData || !this.theData.Values) {
            return;
        }
        const j = this.theData.Values.length;
        for (let i = 0; i < j; i++) {
            this.dataIndexArray.push(i);
        }
    }

    setTableSizeNotice(start, end) {
        this.querySelector("#rowTop").innerText = `${start.toLocaleString()}`;
        this.querySelector("#rowBottom").innerText = `${end.toLocaleString()}`;
        this.querySelector("#rowCount").innerText = `${this.dataLength.toLocaleString()}`;
        this.querySelector("#currentPage").value = `${this.curPage.toLocaleString()}`;
        this.querySelector("#pageMax").innerText = `${(Math.ceil(this.dataLength / this.pageSize)).toLocaleString()}`;
        this.querySelector("#pageSizeId").value = `${this.pageSize}`;

        if (this.curPage === 1) {
            this.querySelector("#firstButton").classList.add("d-none");
            this.querySelector("#prevButton").classList.add("d-none");
        } else {
            this.querySelector("#firstButton").classList.remove("d-none");
            this.querySelector("#prevButton").classList.remove("d-none");
        }
        if (this.curPage === Math.ceil(this.dataLength / this.pageSize)) {
            this.querySelector("#nextButton").classList.add("d-none");
            this.querySelector("#lastButton").classList.add("d-none");
        } else {
            this.querySelector("#nextButton").classList.remove("d-none");
            this.querySelector("#lastButton").classList.remove("d-none");
        }
    }
    scrollWindowTo(y) {
        window.scrollTo({top: y, behavior: 'auto'});
    }

    firstPage() {
        if (this.curPage > 1) this.curPage = 1;
        this.renderTable();
    }

    previousPage() {
        if (this.curPage > 1) this.curPage--;
        this.renderTable();
    }

    nextPage() {
        if ((this.curPage * this.pageSize) < this.dataLength) this.curPage++;
        this.renderTable();
    }

    lastPage() {
        if ((this.curPage * this.pageSize) < this.dataLength) {
            this.curPage = Math.floor(this.dataLength / this.pageSize) + 1;
            this.renderTable();
        }
    }

    resetTableSizeNotice() {
        document.getElementById("rowTop").innerText = "0";
        document.getElementById("rowBottom").innerText = "0";
        document.getElementById("rowCount").innerText = "0";
        document.getElementById("currentPage").value = "0";
        document.getElementById("pageMax").innerText = "0";
        document.getElementById("pageSizeId").value = "0";

    }


}

window.customElements.define('app-settings', AppSettings);