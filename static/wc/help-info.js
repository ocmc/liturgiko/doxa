class HelpInfo extends HTMLElement {
  constructor() {
    // always call super() first
    super();
    console.log("static/wc/help-info.js")
    this.shadowDOM = this.attachShadow({mode: 'open'});
  }

  connectedCallback() {
    this.shadowDOM.innerHTML = `
    <style> 
      @import url('static/css/bootstrap.min.css');
      @import url('static/img/bootstrap-icons.css');
    </style>
    <style>
        .alert-info {
            color: #31708f;
            background-color: #d9edf7;
            border-color: #bce8f1;
        }
    </style>
    <div class="alert alert-primary" role="alert">
        <i class="bi-info-circle" style="color: blue;"></i>
        ${this.text}
    </div>
    `
  }

  disconnectedCallback() {
  }

  static get observedAttributes() {
    return ['text'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
  }

  adoptedCallback() {
  }
  get text() {
    return this.getAttribute("text");
  }

  set text(value) {
    this.setAttribute("text", value);
  }

}

window.customElements.define('help-info', HelpInfo);