import * as utils from "./utils.js";

class PublicationManager extends HTMLElement {
  constructor() {
    super();
    console.log("static/wc/publication-manager.js")
    // site type
    this.selectedSiteType = "";
    this.lsSelectedSiteType = "siteType";
    this.siteTypeOptions = [
      {Key: "test", Desc: "Test website", Selected: true, Class: "", Icon: "", IconColor: ""},
      {Key: "public", Desc: "Public website", Selected: false, Class: "", Icon: "", IconColor: ""},
    ];
    this.siteTypeDefaultKey = "test";
    this.siteTypeRadioGroup = null;
  }

  connectedCallback() {
    this.innerHTML =
`
<style> 
  @import url('static/css/bootstrap.min.css');
  @import url('static/img/bootstrap-icons.css');
</style>
<div id="my-element-div" class="wc-title">Publication Manager</div>
<br>
<hr>
<div style="display: flex; flex-direction: column; flex-wrap: wrap; gap: 1rem;">
    <div>
        The publication manager publishes to the Internet two types of liturgical websites.
    </div>
    <ol>
       <li><em>Test</em> website: this has the next version of the website, 
              but you are not ready for the public to see it.
              A test website can be put on the Internet on a place only known to
              people who are going to review or test it and give you feedback 
              or approval.
       </li>
       <li><em>Public</em> website: this is the version of the liturgical website that you 
             intend to put in a place on the Internet where the public can see it.  
       </li>
    </ol>
    <div>
        The generator creates these two websites only on your computer.
        They are not visible to other people unless you use this Publication Manager.
    </div>
    <div>
        After you publish the website, to look at it, click on the View menu in the menu bar.
    </div>
</div>
<br>
<hr>
<br>
<div style="display: flex; flex-direction: row; flex-wrap: wrap; gap: 1rem;">
  <div><em>Select the website type:</em></div>
  <radio-group id="siteTypeRadioGroup" gid="siteType" items="[]" active="0"></radio-group>
</div>
<br>
<hr>
<br>
<div style="display: flex; flex-direction: column; flex-wrap: wrap; gap: 1rem; max-width: 50vw;">
  <div style="display: flex; flex-direction: row; gap: 20px;">  <!-- Publish Button -->
      <span>
          <button id="btnPublish"  type="button" class="btn btn-primary" style="">Publish</button>
      </span>
      <br>
      <div class="alert alert-light message" role="alert" id="publishStatus" style="padding: 3px; width: max-content;">&nbsp;</div>
  </div> <!-- end Generation Button -->
<div>
    <web-socket-client id="webSocketClient" class="d-none" endpoint="/ws/publish" title="Publication Messages" style="width: 100vw !important;"></web-socket-client>
</div>
<br>
`;

    this.divPublishStatus = this.querySelector("#publishStatus");
    this.publishButton = this.querySelector("#btnPublish");

    // add listeners.  For each one added, remove it in disconnectedCallback.
    this.publishButton.addEventListener("click", (e) => this.handlePublish(e))

    // site type group
    this.siteTypeRadioGroup = this.querySelector("#siteTypeRadioGroup");
    if (this.siteTypeRadioGroup) {
      this.siteTypeRadioGroup.setAttribute("items", JSON.stringify(this.siteTypeOptions));
      this.siteTypeRadioGroup.setAttribute("active", this.indexOfSelectedSiteType());
      this.siteTypeRadioGroup.addEventListener("change", (ev) => this.handleSiteTypeSelection(ev));
    }
  }

  disconnectedCallback() {
    this.publishButton.removeEventListener("click", (ev) => this.handlePublish(ev))
    if (this.siteTypeRadioGroup) {
      this.siteTypeRadioGroup.removeEventListener("change", (ev) => this.handleSiteTypeSelection(ev));
    }
  }

  handleSiteTypeSelection(ev) {
    this.selectedSiteType = ev.target.getAttribute("value");
    utils.lsSet(this.lsPrefix, this.lsSelectedSiteType, this.selectedSiteType);
  }
  // indexOfSelectedSiteType returns the siteTypeOptions index of the key found in local storage.
  // If not found, returns the index of the default key.  Local storage will also be set to the default.
  indexOfSelectedSiteType() {
    let defaultIndex = 0;
    this.selectedSiteType = utils.lsGet(this.lsPrefix, this.lsSelectedSiteType);
    if (!this.selectedSiteType) {
      this.selectedSiteType = this.siteTypeDefaultKey;
    }
    const j = this.siteTypeOptions.length;
    for(let i = 0; i < j; i++) {
      const key = this.siteTypeOptions[i].Key
      if (key === this.siteTypeDefaultKey) {
        defaultIndex = i;
      }
      if (key === this.selectedSiteType) {
        return i;
      }
    };
    utils.lsSet(this.lsPrefix, this.lsSelectedSiteType, this.siteTypeDefaultKey);
    return defaultIndex;
  }

  handlePublish(ev) {
    if (ev) {
      ev.stopPropagation();
      ev.preventDefault();
    }
    this.publishButton.disabled = true;
    this.publishStatusMessage(utils.alerts.Primary, "Publishing...")
    this.querySelector("#webSocketClient").classList.remove("d-none")
    this.requestPublication(this.libraries).then(response => {
      this.publishButton.disabled = false;
      if (response.Status === "OK") {
        this.publishStatusMessage(utils.alerts.Success, "Publication finished OK")
      } else {
        this.publishStatusMessage(utils.alerts.Danger, `${response.Status}: ${response.Message}`)
      }
    }).catch(error => {
      this.publishButton.disabled = false;
      this.publishStatusMessage(utils.alerts.Danger, error)
    });
  }
  // ---- MESSAGE Handlers
  publishStatusMessage(type, message) {
    this.divPublishStatus.className = "";
    this.divPublishStatus.classList.add(type);
    this.divPublishStatus.innerHTML = utils.getMessageSpan(type, message);
  }
  // ---- REST API Handlers

  async requestPublication(libraries) {
    const url = this.host
        + `api/publish?public=${this.selectedSiteType === "public"}`
    ;
    let fetchData = {
      method: 'PUT'
    }

    const response = await fetch(url, fetchData);
    if (!response.ok) {
      let message = "";
      switch (response.status) {
        case 404: {
          message += "not found";
          break;
        }
        case 500: {
          message += "doxa server error";
          break;
        }
        default: {
          message += `An error has occurred: ${response.status}`;
        }
      }
      throw new Error(message);
    }
    const responseJson = await response.json();
    return responseJson;
  }
// ---- End REST API Handlers

  static get observedAttributes() {
    return ['host'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
  }

  adoptedCallback() {
  }
  get host() {
    return this.getAttribute("host");
  }

  set host(value) {
    this.setAttribute("host", value);
  }
}

window.customElements.define('publication-manager', PublicationManager);