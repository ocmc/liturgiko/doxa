// RegExGroup allows the user make a multiple template regular expression selections using check boxes
import * as utils from "./utils.js";

class RegExGroup extends HTMLElement {
    constructor() {
        super();
        console.log("static/wc/regex-group.js")
        this.patternArray = [];
        this.listComponent = null;
        this.divMatched = null;
        this.divStatus = null;
        this.btnMatched = null;
        this.divMatchedCount = null;
        this.divMatchedPatterns = null;
        this.btnPattern = null;
        this.btnPatternCheck = null;
        this.inputPattern = null;
        this.updateIndex = -1;
        this.patternAllBooks = "bk\\..*.lml";
        this.patternAllServices = "se\\.m([0-9][0-9])\\.d([0-9][0-9])\\..*.lml";
        this.patternAllBooksAndServices = "bk|se\\.m([0-9][0-9])\\.d([0-9][0-9])\\.(.*).lml";
        this.iconHelp = null;
        this.modalPatternHelp = null;
    }

    connectedCallback() {
        this.innerHTML =
            `
<style> 
  @import url('static/css/bootstrap.min.css');
  @import url('static/img/bootstrap-icons.css');
</style>
<style>
.flex-container {
  display: flex;
  display: -webkit-flex;
  flex-direction: row;
  gap: 20px;
}
#inputPattern {
  width: 75%;
}
.fa-clickable {
    cursor:pointer;
    outline:none;
}
 div.scroll {
    margin-left:4px;
    margin-right:4px;
    padding:14px;
    width: 65vw;
    height: 220px;
    overflow-x: hidden;
    overflow-y: auto;
    text-align:justify;
}
</style>
<div><i id="iconHelp" class="bi-question-circle fa-clickable" style="color: cornflowerblue;"></i></div>
<div id="listComponent_${this.gid}"></div>
<br>
<div class="flex-container" style="flex-direction: row">
    <div class="input-group mb-3">
      <button id="btnPattern" class="btn btn-outline-primary" type="button" ><i class="bi bi-arrow-bar-up"></i></button>
      <input id="inputPattern" class="form-control" type="text" style="min-width: 20vw;" placeholder="enter new pattern" aria-describedby="btnPattern" aria-label="update">
      <button id="btnPatternCheck" class="btn btn-outline-primary" type="button"><i class="bi bi-search"></i></button>
    </div>
</div>
<br>
<div id="statusDiv"></div>
<br>
<div id="divMatched" class="d-none">
    <div class="row">
      <span><i id="btnMatched" class="bi bi-x-circle"></i></span>
      <span id="divMatchedCount"></span>
    </div>
    <div id="divMatchedPatterns" class="scroll"></div>
</div>
<br>
<!-- Modal Help -->
    <modal-icon-help
        id="regex-modal-icon-help"
        class="d-none"
        firstDivText="${this.getPatternHelpFirstDivText()}"
        lastDivText="${this.getPatternHelpLastDivText()}"
        title="Generation Manager: Template Pattern Help">
    </modal-icon-help>
`;

        this.iconHelp = this.querySelector("#iconHelp");
        this.divStatus = this.querySelector("#statusDiv");
        this.modalPatternHelp = this.querySelector("#regex-modal-icon-help");
        this.btnPattern = this.querySelector("#btnPattern");
        this.btnPatternCheck = this.querySelector("#btnPatternCheck");
        this.inputPattern = this.querySelector("#inputPattern");
        this.listComponent = this.querySelector(`#listComponent_${this.gid}`);

        this.btnPattern.addEventListener("click", (e) => this.handleBtnPatternClick(e));
        this.btnPatternCheck.addEventListener("click", (e) => this.handleBtnPatternCheckClick(e));
        this.iconHelp.addEventListener("click", (e) => this.showModalIconHelp(e));
        this.listComponent.addEventListener("change", (e) => this.handleChangeEvent(e));
        this.listComponent.addEventListener("click", (e) => this.handleActionIconClick(e));
        this.modalPatternHelp.addEventListener('click', (ev) => this.handleModalIconHelpClick(ev));

        this.getFilePatterns();
        if (this.items && this.items.length > 0) {
            this.patternArray = JSON.parse(this.items);
        } else {
            this.patternArray.push("bk|se)\\.(.*)")
        }
        if (!this.selectionArray || this.selectionArray.length === 1) {
            this.selectionArray = [];
            this.selectionArray.push("0")
        }

        if (this.patternArray && this.patternArray.length > 0) {
            this.renderList()
        }
        this.divMatched = this.querySelector("#divMatched")
        this.btnMatched = this.querySelector("#btnMatched")
        this.btnMatched.addEventListener("click", () => {
            this.divMatched.classList.add("d-none");
        });
        this.divMatchedCount = this.querySelector("#divMatchedCount")
        this.divMatchedPatterns = this.querySelector("#divMatchedPatterns")
    }
    // for every event handler added above, below remove it
    disconnectedCallback() {
        this.btnMatched.addEventListener("click", () => {
            this.divMatched.classList.add("d-none");
        });
        this.btnPattern.removeEventListener("click", (e) => this.handleBtnPatternClick(e));
        this.btnPatternCheck.removeEventListener("click", (e) => this.handleBtnPatternCheckClick(e));
        this.iconHelp.removeEventListener("click", (e) => this.showModalIconHelp(e));
        this.listComponent.removeEventListener("change", (e) => this.handleChangeEvent(e));
        this.listComponent.removeEventListener("click", (e) => this.handleActionIconClick(e));
        this.modalPatternHelp.removeEventListener('click', (ev) => this.handleModalIconHelpClick(ev));
    }

    handleBtnPatternClick(ev) {
        if (ev) {
            ev.stopPropagation();
            ev.preventDefault();
        }
        let pattern = this.inputPattern.value;
        if (pattern.length === 0) {
            return
        }
        if (!pattern.endsWith(".lml")) {
            pattern = pattern + ".lml";
            this.inputPattern.value = pattern;
        }
        // check to prevent duplicate regular expression patterns.
        if (this.patternArray.includes(pattern)) {
            this.statusMessage(utils.alerts.Danger, "duplicate regular expressions not allowed.");
            return;
        }
        if (this.updateIndex === -1) {
            // this is an add
            this.patternArray.push(pattern);
        } else {
            // this is an update
            this.patternArray[this.updateIndex] = pattern;
        }
        this.updateIndex = -1;
        this.renderList();
        this.updateFilePatterns();
    }
    handleBtnPatternCheckClick(ev) {
        if (ev) {
            ev.stopPropagation();
            ev.preventDefault();
        }
        let pattern = this.inputPattern.value;
        if (pattern.length === 0) {
            return
        }
        if (!pattern.endsWith(".lml")) {
            pattern = pattern + ".lml";
            this.inputPattern.value = pattern;
        }
        this.getTemplateMatches(this.inputPattern.value);
    }

    getPatternHelpFirstDivText() {
        return `The generator uses templates to generate a website of HTML and PDF files.  
                The last segment of a template ID indicates its type (bk for book, 
                bl for block, or se for service), followed by other acronyms, e.g.
                bk.eu.wedding.lml, meaning (book, euchologion, wedding) or se.m02.d03.li.lml
                (meaning service, month 2, day 3, liturgy). 
                The generator uses template ID patterns to determine which templates to use.
                The template patterns use regular expressions. Google the phrase 'regular expression'
                if you are not familiar with what it is. 
                Note that there are two parts to the pattern selection area.
                <ol>
                <li>A list of available patterns.  You can select one or more.</li>
                <li>An input field where you can enter a new pattern or edit an existing one.</li>
                </ol>
                Below is a description of what happens when you click each icon.
        `;
    }
    getPatternHelpLastDivText() {
        return `Pattern rules: 
        <ol>
          <li>A pattern can only match bk (book) or service (se) templates.</li>
          <li>A pattern must match one or more templates.</li>
          <li>A selected pattern is not allowed to match a template that is also matched by another pattern.</li>
        </ol>
        `;
    }

    showModalIconHelp(ev) {
        ev.stopPropagation();
        this.scrollY = window.scrollY;
        let help = JSON.stringify([
            {
                "icon": "bi-search",
                "desc": "See which templates match the pattern.",
            },
            {
                "icon": "bi-pencil-square",
                "desc": "Copy a pattern to the pattern input field so you can edit it.",
            },
            {
                "icon": "bi-file-x",
                "desc": "Delete a pattern. Note that the first three patterns cannot be deleted.",
            },
            {
                "icon": "bi-arrow-bar-up",
                "desc": "Add a new or edited pattern in the input field to the list of patterns above it.",
            },
        ]);
        this.modalPatternHelp.setAttribute("data", help);
        this.modalPatternHelp.className = "";
    }

    handleActionIconClick(ev) {
        this.statusMessage(utils.alerts.Clear);
        console.log("handleActionIconClick")
        if (!ev.target.hasAttribute("data-action")) {
            return;
        }
        ev.stopPropagation();
        ev.preventDefault();

        this.updateIndex = -1;
        const action = ev.target.getAttribute("data-action");
        const index = ev.target.getAttribute("data-index");

        if (action && index) {
            switch (action) {
                case "edit": {
                    this.inputPattern.value = this.patternArray[index];
                    this.updateIndex = index;
                    break;
                }
                case "search": {
                    this.getTemplateMatches(this.patternArray[index]);
                    break;
                }
                case "delete": {
                    if (this.patternArray.length === 1) {
                        this.statusMessage(utils.alerts.Danger, "there must be at least one pattern");
                        return;
                    }
                    let newArray = [];
                    this.patternArray.forEach((item, i) => {
                        const strI = `${i}`
                        if (strI !== index) {
                            newArray.push(item);
                        }
                    });
                    if (this.selectionArray.length === 1) {
                        this.selectionArray = [];
                        this.selectionArray.push("0")
                    }
                    this.patternArray = newArray;
                    this.renderList();
                    this.updateFilePatterns();
                    break;
                }
            }
        }
    }
    doNada() {}
    handleChangeEvent(ev) {
        if (ev) {
            if (ev.target.hasAttribute("data-action")) {
                ev.stopPropagation();
                ev.preventDefault();
            }
        }
        this.statusMessage(utils.alerts.Clear);
        const targetIndex = ev.target.value;
        const pattern = this.patternArray[targetIndex];
        if (pattern === this.patternAllBooksAndServices) {
            this.deselectAllExcept(targetIndex);
        } else {
            this.deselectDiscreteBooksServicesPattern();
        }
        if (pattern === this.patternAllBooks) {
            this.deselectAllBookPatterns();
        } else if (pattern.startsWith("bk"))  {
            this.deselectAllPatternAllBooks();
        }
        if (pattern === this.patternAllServices) {
            this.deselectDiscreteServicePatterns();
        } else if (pattern.startsWith("se"))  {
            this.deselectAllPatternAllServices();
        }
        this.selectionArray = [];
        const items = this.listComponent.querySelectorAll("input");
        items.forEach((item, i) => {
            if (item.checked) {
                this.selectionArray.push(i);
            }
        });
        this.updateFilePatterns();
    }

    deselectAllPatternAllBooks() {
        const items = this.listComponent.querySelectorAll("input");
        items.forEach((item) => {
            const pattern = this.patternArray[item.value]
            if (pattern === this.patternAllBooks) {
                item.checked = false;
            } else if (pattern === this.patternAllBooksAndServices) {
                    item.checked = false;
            } else {
                if (pattern.startsWith("bk")
                    || pattern.startsWith("(bk|se")
                    || pattern.startsWith("(se|bk")
                ) {
                    // ignore
                }
            }
        });
        this.newLayout = true;
    }
    deselectAllPatternAllServices() {
        const items = this.listComponent.querySelectorAll("input");
        items.forEach((item) => {
            const pattern = this.patternArray[item.value]
            if (pattern === this.patternAllServices) {
                item.checked = false;
            } else if (pattern === this.patternAllBooksAndServices) {
                item.checked = false;
            } else {
                if (pattern.startsWith("bk")
                    || pattern.startsWith("(bk|se)")
                    || pattern.startsWith("(se|bk)")
                ) {
                    // ignore
                }
            }
        });
        this.newLayout = true;
    }
    deselectAllBookPatterns() {
        const items = this.listComponent.querySelectorAll("input");
        items.forEach((item) => {
            const pattern = this.patternArray[item.value]
            if (pattern === this.patternAllBooks) {
                // do nothing
            } else {
                if (pattern.startsWith("bk") || pattern.startsWith("(bk")) {
                    item.checked = false;
                }
            }
        });
        this.newLayout = true;
    }
    deselectDiscreteServicePatterns() {
        const items = this.listComponent.querySelectorAll("input");
        items.forEach((item) => {
            const pattern = this.patternArray[item.value]
            if (pattern === this.patternAllServices) {
                // do nothing
            } else {
                if (pattern.startsWith("se")
                    || pattern.startsWith("(bk|se)")
                    || pattern.startsWith("(se|bk)")) {
                    item.checked = false;
                }
            }
        });
        this.newLayout = true;
    }

    deselectDiscreteBooksServicesPattern() {
        const items = this.listComponent.querySelectorAll("input");
        items.forEach((item) => {
            if (this.patternArray[item.value] === this.patternAllBooksAndServices) {
                item.checked = false;
            }
        });
        this.newLayout = true;
    }

    deselectAllExcept(except) {
        const items = this.listComponent.querySelectorAll("input");
        items.forEach((item) => {
            item.checked = item.value === except
        });
        this.newLayout = true;
    }

    /**
     <div class="form-check">
     <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
     <label class="form-check-label" for="flexCheckDefault">
     Default checkbox
     </label>
     </div>
     *
     */
    renderItem(item, index) {
        // containing div
        const theDiv = document.createElement('div');
        theDiv.classList.add("form-check");
        // input
        const theInput = document.createElement("input");
        theInput.classList.add("form-check-input");
        theInput.setAttribute("type", "checkbox");
        theInput.setAttribute("name", `flexCheckDefault_${this.gid}`);
        theInput.setAttribute("id", `flexCheckDefault_${this.gid}_${index}`);
        theInput.setAttribute("value", `${index}`);
        let j = 0;
        if (this.selectionArray) {
            j = this.selectionArray.length;
        }
        // see if this item is selected
        for (let i = 0; i < j; i++) {
            if (this.selectionArray[i] === "*") {
                theInput.checked = true;
                break;
            } else if (this.selectionArray[i] === `${index}`){
                theInput.checked = true;
                break;
            }
        }
        // label
        const theLabel = document.createElement('label');
        theLabel.classList.add("form-check-label");
        theLabel.setAttribute("For", `flexCheckDefault_${this.gid}_${index}`);
        switch (item) {
            case this.patternAllBooks: {
                theLabel.innerHTML = `<span>${item}</span>
            &nbsp;&nbsp;<em style="color: cornflowerblue">All books</em>
            &nbsp;&nbsp;
                <i class="bi bi-search fa-clickable" data-action="search" data-index="${index}" style="color: blue;"></i>
                `
                break;
            }
            case this.patternAllServices: {
                theLabel.innerHTML = `<span>${item}</span>
            &nbsp;&nbsp;<em style="color: cornflowerblue">All services</em>
            &nbsp;&nbsp;
                <i class="bi bi-search fa-clickable" data-action="search" data-index="${index}" style="color: blue;"></i>
                `
                break;
            }
            case this.patternAllBooksAndServices: {
                theLabel.innerHTML = `<span>${item}</span>
            &nbsp;&nbsp;<em style="color: cornflowerblue">All books and services</em>
            &nbsp;&nbsp;
                <i class="bi bi-search fa-clickable" data-action="search" data-index="${index}" style="color: blue;"></i>
                `
                break;
            }
            default: {
                theLabel.innerHTML = `<span>${item}</span>
            &nbsp;&nbsp;
                <i class="bi bi-pencil-square fa-clickable"  data-action="edit" data-index="${index}" style="color: blue;"></i>
            &nbsp;&nbsp;
                <i class="bi bi-search fa-clickable" data-action="search" data-index="${index}" style="color: blue;"></i>
            &nbsp;&nbsp;
                <i class="bi bi-file-x fa-clickable" data-action="delete" data-index="${index}" style="color: red;"></i>
                `
            }
        }
        // append components
        theDiv.appendChild(theInput);
        theDiv.appendChild(theLabel);
        this.listComponent.appendChild(theDiv);
        return true
    }

    handleModalIconHelpClick(ev) {
        ev.stopPropagation();
        const btn = ev.composedPath()[0].id;
        if (btn === "closeBtn" || btn === "cancelBtn") {
            this.modalPatternHelp.className = "d-none";
            this.scrollWindowTo(this.scrollY);
        }
    }

    renderList() {
        if (! this.listComponent) {
            return;
        }
        this.listComponent.innerHTML = "";
        let done = true;
        this.patternArray.sort();
        if (this.listComponent) {
            this.patternArray.forEach((item, index) => {
                done = this.renderItem(item, index);
            });
        }
    }

    selectionsToStringArray() {
        let s = [];
        this.selectionArray.forEach((item) => {
            s.push(`${item}`);
        });
        return s;
    }
    getFilePatterns() {
        this.requestFilePatterns().then(response => {
            if (response.Status === "OK") {
                this.patternArray = response.Patterns;
                this.selectionArray = response.Selections;
                this.renderList();
            } else {
                this.statusMessage(utils.alerts.Danger, `${response.Status}: ${response.Message}`)
            }
        }).catch(error => {
            this.statusMessage(utils.alerts.Danger, error)
        });
    }

    updateFilePatterns() {
        this.statusMessage(utils.alerts.Clear);
        this.requestFilePatternsUpdate().then(response => {
            if (response.Status === "OK") {
                this.inputPattern.value = "";
                if (response.Message.includes("Warning")) {
                    this.statusMessage(utils.alerts.Danger, `${response.Status}: ${response.Message}`)
                } else {
                    this.statusMessage(utils.alerts.Info, `${response.Status}: ${response.Message}`)
                }
            } else {
                this.getFilePatterns();
                this.statusMessage(utils.alerts.Danger, `${response.Status}: ${response.Message}`)
            }
        }).catch(error => {
            this.statusMessage(utils.alerts.Danger, error)
        });
    }

    getTemplateMatches(pattern) {
        this.statusMessage(utils.alerts.Clear);
        this.divMatched.classList.add("d-none");
        this.requestTemplateMatches(pattern).then(response => {
            if (response.Status === "OK") {
                if (!response.Values) {
                    this.statusMessage(utils.alerts.Danger, response.Message);
                    return;
                }
                if (response.Values && response.Values.length === 0) {
                    this.statusMessage(utils.alerts.Danger, response.Message);
                    return;
                }
                this.statusMessage(utils.alerts.Info, response.Message);
                let ol = document.createElement("ol")
                response.Values.forEach((item) => {
                    let li = document.createElement("li")
                    li.innerText = item;
                    ol.appendChild(li);
                });
                this.divMatchedPatterns.innerHTML = "";
                this.divMatchedPatterns.classList.add(utils.alerts.Info);
                this.divMatchedPatterns.classList.add("scroll");
                // if (response.Values.length === 1) {
                //     this.divMatchedCount.innerText = `${response.Values.length} template matches ${pattern}:`
                // } else {
                //     this.divMatchedCount.innerText = `${response.Values.length} templates match ${pattern}:`
                // }
                this.divMatchedPatterns.appendChild(ol);
                this.divMatched.classList.remove("d-none");
            } else {
                this.statusMessage(utils.alerts.Danger, `${response.Status}: ${response.Message}`)
            }
        }).catch(error => {
            this.statusMessage(utils.alerts.Danger, error)
        });
    }
    // ---- MESSAGE Handler
    statusMessage(type, message) {
        this.divStatus.className = "";
        this.divStatus.classList.add(type);
        this.divStatus.innerHTML = utils.getMessageSpan(type, message);
    }

    // ---- REST API Handlers

    async requestFilePatterns() {
        const url = this.host
            + `api/lml/patterns`
        ;
        let fetchData = {
            method: 'GET'
        }
        const response = await fetch(url, fetchData);
        if (!response.ok) {
            let message = "";
            switch (response.status) {
                case 404: {
                    message += "not found";
                    break;
                }
                case 500: {
                    message += "doxa server error";
                    break;
                }
                default: {
                    message += `An error has occurred: ${response.status}`;
                }
            }
            throw new Error(message);
        }
        return await response.json();
    }

    async requestFilePatternsUpdate() {
        const url = this.host
            + `api/lml/patterns`
            + `?patterns=${JSON.stringify(this.patternArray)}`
            + `&selections=${JSON.stringify(this.selectionsToStringArray())}`
        ;
        let fetchData = {
            method: 'PUT'
        }
        const response = await fetch(url, fetchData);
        if (!response.ok) {
            let message = "";
            switch (response.status) {
                case 404: {
                    message += "not found";
                    break;
                }
                case 500: {
                    message += "doxa server error";
                    break;
                }
                default: {
                    message += `An error has occurred: ${response.status}`;
                }
            }
            throw new Error(message);
        }
        return await response.json();
    }

    async requestTemplateMatches(pattern) {
        const url = this.host
            + `api/lml/match`
            + `?pattern=${pattern}`
        ;
        let fetchData = {
            method: 'GET'
        }
        const response = await fetch(url, fetchData);
        if (!response.ok) {
            let message = "";
            switch (response.status) {
                case 404: {
                    message += "not found";
                    break;
                }
                case 500: {
                    message += "doxa server error";
                    break;
                }
                default: {
                    message += `An error has occurred: ${response.status}`;
                }
            }
            throw new Error(message);
        }
        return await response.json();
    }

// ---- End REST API Handlers



    static get observedAttributes() {
        return ['gid', 'host'];
    }

    //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
    }

    // custom events
    selectionChanged = new CustomEvent('selectionchanged', {
        detail: {
            name: 'selectionchanged'
        },
        bubbles: true
    });

    adoptedCallback() {
    }

    get gid() { // group id
        return this.getAttribute("gid");
    }
    set gid(value) {
        this.setAttribute("gid", value);
    }
    get host() {
        return this.getAttribute("host");
    }

    set host(value) {
        this.setAttribute("host", value);
    }

}

window.customElements.define('regex-group', RegExGroup);