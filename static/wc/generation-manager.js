import * as utils from "./utils.js";

class GenerationManager extends HTMLElement {
  constructor() {
    super();
    console.log("static/wc/generation-manager.js")
    this.lsPrefix = "gm"; // used to prefix local storage keys
    // site type
    this.selectedSiteType = "";
    this.lsSelectedSiteType = "siteType";
    this.siteTypeOptions = [
      {Key: "test", Desc: "Test website", Selected: true, Class: "", Icon: "", IconColor: ""},
      {Key: "public", Desc: "Public website", Selected: false, Class: "", Icon: "", IconColor: ""},
    ];
    this.siteTypeDefaultKey = "test";
    this.siteTypeRadioGroup = null;
    // pre generate options
    // The options are set by function getPreGenerateOptions()
    this.keyCopyAssets = "copyAssets";
    this.keyDeleteHtml = "deleteHtml";
    this.keyDeletePdf = "deletePdf";
    this.copyAssets = false;
    this.deleteHtml = true;
    this.deletePdf = true;
    this.preGenerateCheckboxGroup = null;

    // post generate options
    // The options are set by function getPostGenerateOptions()
    this.keyIndexSiteForNav = "indexSiteForNav"; // provides the nav pages showing available books and services
    this.indexSiteForNav = false;
    this.keyIndexSiteForSearch = "indexSiteForSearch"; // provides the index to search entire website
    // this.indexSiteForSearch = false;
    this.postGenerateCheckboxGroup = null;

    // template regexes
    // The options are set by function getRegExOptions()
    this.keyCopyAssets = "copyAssets";
    this.keyDeleteHtml = "deleteHtml";
    this.keyDeletePdf = "deletePdf";
    this.copyAssets = false;
    this.deleteHtml = true;
    this.deletePdf = true;
    this.regExCheckboxGroup = null;

    // file type options
    this.genHtml = true;
    this.genPdf = true;
    this.keyGenBoth = "genBoth";
    this.keyGenHtml = "genHtml";
    this.keyGenPdf = "genPdf";
    this.selectedFileType = "";
    this.lsSelectedFileType = "fileType";
    this.fileTypeOptions = [
      {Key: this.keyGenHtml, Desc: "HTML", Selected: false, Class: "", Icon: "", IconColor: ""},
      {Key: this.keyGenPdf, Desc: "PDF", Selected: false, Class: "", Icon: "", IconColor: ""},
      {Key: this.keyGenBoth, Desc: "Both", Selected: true, Class: "", Icon: "", IconColor: ""},
    ];
    this.fileTypeDefaultKey = this.keyGenBoth;
    this.fileTypeRadioGroup = null;
    // layouts
    this.layoutsCheckboxGroup = null;
    this.layouts = [];
    this.selectedLayouts = [];
    this.lsSelectedLayouts = "layouts";
    // template statuses
    this.templateStatusesCheckboxGroup = null;
    this.templateStatuses = [];
    this.selectedTemplateStatuses = [];
    this.lsSelectedTemplateStatuses = "templateStatuses";
    // day of week options
    // The options are set by function getDayOfWeekOptions()
    this.lsSelectedDaysOfWeek = "dayOfWeek";
    this.selectedDaysOfWeek = [];
    this.sundaySelected = false;
    this.mondaySelected = false;
    this.tuesdaySelected = false;
    this.wednesdaySelected = false;
    this.thursdaySelected = false;
    this.fridaySelected = false;
    this.saturdaySelected = false;
    this.daysOfWeekCheckboxGroup = null;
  }

  connectedCallback() {
    this.innerHTML =
`
<style> 
  @import url('static/css/bootstrap.min.css');
  @import url('static/img/bootstrap-icons.css');
</style>
<style>
  #layoutsCheckboxGroup {
      margin-left: 6.8em;
  }
</style>
<div id="my-element-div" class="wc-title">Generation Manager</div>
<br>
<hr>
<div style="display: flex; flex-direction: column; flex-wrap: wrap; gap: 1rem;">
    <div>
        The generation manager creates two types of liturgical websites.
    </div>
    <ol>
       <li><em>Test</em> website: this has the next version of the website, 
              but you are not ready for the public to see it.
              A test website can be put on the Internet on a place only known to
              people who are going to review or test it and give you feedback 
              or approval.
       </li>
       <li><em>Public</em> website: this is the version of the liturgical website that you 
             intend to put in a place on the Internet where the public can see it.  
       </li>
    </ol>
    <div>
        The generator creates these two websites only on your computer.
        They are not visible to other people unless you have configured
        Doxa to publish your websites for you and use the Publish menu in the menu bar, 
        or you manually copy the files to a web server.
    </div>
    <div>
        After you generate the website(s), to look at them, click on the View menu in the menu bar.  Sometimes it is necessary to clear the browser cache to see updates on the website.
    </div>
</div>
<br>
<hr>
<br>
<div style="display: flex; flex-direction: row; flex-wrap: wrap; gap: 1rem;">
    <template-fallback id="fallback" host=""></template-fallback>
</div>
<br>
<div style="display: flex; flex-direction: row; flex-wrap: wrap; gap: 1rem;">
  <div><em>Select the website type:</em></div>
  <radio-group id="siteTypeRadioGroup" gid="siteType" items="[]" active="0"></radio-group>
  <div><em>File type:</em></div>
  <radio-group id="fileTypeRadioGroup" gid="fileType" items="[]" active="0"></radio-group>
</div>
<br>
<div style="display: flex; flex-direction: row; flex-wrap: wrap; gap: 1rem;">
  <div><em>Pre-Generate Options:</em>&nbsp;&nbsp;</div>
  <checkbox-group id="preGenerateCheckboxGroup" gid="preGen" items="[]" checked="[]"></checkbox-group>
</div>
<br>
<div style="display: flex; flex-direction: row; flex-wrap: wrap; gap: 1rem;">
  <div><em>Post-Generate Options:</em>&nbsp;&nbsp;</div>
  <checkbox-group id="postGenerateCheckboxGroup" gid="postGen" items="[]" checked="[]"></checkbox-group>
</div>
<br>
<div style="display: flex; flex-direction: row; flex-wrap: wrap; gap: 1rem;">
  <div><em>Layouts:</em></div>
  <checkbox-group id="layoutsCheckboxGroup" gid="layouts" items="[]" checked="[]"></checkbox-group>
</div>
<br>
<div style="display: flex; flex-direction: row; flex-wrap: wrap; gap: 1rem;">
  <div><em>Template ID patterns:</em>&nbsp;&nbsp;</div>
  <regex-group id="regexCheckboxGroup" gid="regEx" host=""></regex-group>
</div>
<div style="display: flex; flex-direction: row; flex-wrap: wrap; gap: 1rem;">
  <div><em>Template Statuses:</em></div>
  <checkbox-group id="templateStatusesCheckboxGroup" gid="statuses" items="[]" checked="[]"></checkbox-group>
  <div><em>Template Days of Week:</em></div>
  <checkbox-group id="daysOfWeekCheckboxGroup" gid="dow" items="[]" checked="[]"></checkbox-group>
</div>
<br>
<hr>
<br>
<div style="display: flex; flex-direction: column; flex-wrap: wrap; gap: 1rem; max-width: 50vw;">
  <div><span style="color: blue;"><i class="bi bi-info-circle">&nbsp;</i>After generating or creating auxiliary files or search indexes, you can view the results by selecting <em>View</em> from the main menu.  When the generated website loads, remember to clear browser cache and reload the page.</span></div>
  <div style="display: flex; flex-direction: row; gap: 20px;">  <!-- Buttons -->
      <span>
          <button id="btnGenerate"  type="button" class="btn btn-primary" style="">Generate</button>
      </span>
      <span>
          <button id="btnIndexForNav"  type="button" class="btn btn-primary" style="">Create Auxiliary Web Pages</button>
      </span>
<!--      <span>-->
<!--          <button id="btnIndexForSearch"  type="button" class="btn btn-primary" style="">Create Search Indexes</button>-->
<!--      </span>-->
  </div> <!-- end Buttons -->
  <br>
  <div class="alert alert-light message" role="alert" id="generateStatus" style="padding: 3px; width: max-content;">&nbsp;</div>
  <div class="alert alert-primary" role="alert" id="generateDelayNotice" style="display: none;">&nbsp;</div>
  <div>
    <web-socket-client id="webSocketClient" class="d-none" endpoint="/ws/generate" title="Messages" style="width: 100vw !important;"></web-socket-client>
</div>
<br>
`;

    this.divGenerateStatus = this.querySelector("#generateStatus");
    this.divGenerateDelayNotice = this.querySelector("#generateDelayNotice");
    this.generateButton = this.querySelector("#btnGenerate");
    this.indexForNavButton = this.querySelector("#btnIndexForNav");
    // this.indexForSearchButton = this.querySelector("#btnIndexForSearch");
    this.webSocketClient = this.querySelector("#webSocketClient");
    this.webSocketClient.addEventListener('wsMessage', (e) => this.handleWsMessage(e));
    this.webSocketClient.addEventListener('wsNotice', (e) => this.handleWsNotice(e));


    // add listeners.  For each one added, remove it in disconnectedCallback.
    this.generateButton.addEventListener("click", (e) => this.handleGenerate(e))
    this.indexForNavButton.addEventListener("click", (e) => this.handleIndexForNav(e))
    // this.indexForSearchButton.addEventListener("click", (e) => this.handleIndexForSearch(e))

    // site type group
    this.siteTypeRadioGroup = this.querySelector("#siteTypeRadioGroup");
    if (this.siteTypeRadioGroup) {
      this.siteTypeRadioGroup.setAttribute("items", JSON.stringify(this.siteTypeOptions));
      this.siteTypeRadioGroup.setAttribute("active", this.indexOfSelectedSiteType());
      this.siteTypeRadioGroup.addEventListener("change", (ev) => this.handleSiteTypeSelection(ev));
    }

    // pre-generate group
    this.preGenerateCheckboxGroup = this.querySelector("#preGenerateCheckboxGroup");
    if (this.preGenerateCheckboxGroup) {
      this.getPreGenerateValues();
      this.preGenerateCheckboxGroup.setAttribute("items", JSON.stringify(this.getPreGenerateOptions()));
      this.preGenerateCheckboxGroup.addEventListener("change", (ev) => this.handlePreGenerateChange(ev));
    }
    // post-generate group
    this.postGenerateCheckboxGroup = this.querySelector("#postGenerateCheckboxGroup");
    if (this.postGenerateCheckboxGroup) {
      this.getPostGenerateValues();
      this.postGenerateCheckboxGroup.setAttribute("items", JSON.stringify(this.getPostGenerateOptions()));
      this.postGenerateCheckboxGroup.addEventListener("change", (ev) => this.handlePostGenerateChange(ev));
    }
    // file type group
    this.fileTypeRadioGroup = this.querySelector("#fileTypeRadioGroup");
    if (this.fileTypeRadioGroup) {
      this.fileTypeRadioGroup.setAttribute("items", JSON.stringify(this.fileTypeOptions));
      this.fileTypeRadioGroup.setAttribute("active", this.indexOfSelectedFileType());
      this.fileTypeRadioGroup.addEventListener("change", (ev) => this.handleFileTypeSelection(ev));
    }
    this.layoutsCheckboxGroup = this.querySelector("#layoutsCheckboxGroup");
    if (this.layoutsCheckboxGroup) {
      this.selectedLayouts =  JSON.parse(utils.lsGet(this.lsPrefix, this.lsSelectedLayouts));
      this.layoutsCheckboxGroup.addEventListener("change", (ev) => this.handleLayoutSelection(ev));
      this.requestLayoutIndexes();
    }
    this.templateStatusesCheckboxGroup = this.querySelector("#templateStatusesCheckboxGroup");
    if (this.templateStatusesCheckboxGroup) {
      this.selectedTemplateStatuses =  JSON.parse(utils.lsGet(this.lsPrefix, this.lsSelectedTemplateStatuses));
      this.templateStatusesCheckboxGroup.addEventListener("change", (ev) => this.handleTemplateStatusesSelection(ev));
      this.requestTemplateStatusTypes();
    }
    this.daysOfWeekCheckboxGroup = this.querySelector("#daysOfWeekCheckboxGroup");
    if (this.daysOfWeekCheckboxGroup) {
      this.selectedDaysOfWeek =  this.getSelectedDaysOfWeek();
      this.daysOfWeekCheckboxGroup.setAttribute("items", JSON.stringify(this.getDaysOfWeekOptions()));
      this.daysOfWeekCheckboxGroup.addEventListener("change", (ev) => this.handleDaysOfWeekChange(ev));
    }
  }

  disconnectedCallback() {
    this.generateButton.removeEventListener("click", (ev) => this.handleGenerate(ev))
    if (this.siteTypeRadioGroup) {
      this.siteTypeRadioGroup.removeEventListener("change", (ev) => this.handleSiteTypeSelection(ev));
    }
    if (this.preGenerateCheckboxGroup) {
      this.preGenerateCheckboxGroup.removeEventListener("change", (ev) => this.handlePreGenerateChange(ev));
    }
    if (this.postGenerateCheckboxGroup) {
      this.postGenerateCheckboxGroup.removeEventListener("change", (ev) => this.handlePostGenerateChange(ev));
    }
    if (this.layoutsCheckboxGroup) {
      this.layoutsCheckboxGroup.removeEventListener("change", (ev) => this.handleLayoutSelection(ev));
    }
    if (this.templateStatusesCheckboxGroup) {
      this.templateStatusesCheckboxGroup.removeEventListener("change", (ev) => this.handleTemplateStatusesSelection(ev));
    }
    if (this.daysOfWeekCheckboxGroup) {
      this.daysOfWeekCheckboxGroup.removeEventListener("change", (ev) => this.handleDaysOfWeekChange(ev));
    }
  }

  getPreGenerateOptions() {
    return [
      {Key: this.keyCopyAssets, Desc: "Copy assets folder.", Selected: this.copyAssets,  Class: "", Icon: "", IconColor: ""},
      {Key: this.keyDeleteHtml, Desc: "Delete existing HTML files.", Selected: this.deleteHtml,  Class: "", Icon: "", IconColor: ""},
      {Key: this.keyDeletePdf, Desc: "Delete existing PDF files.", Selected: this.deletePdf,  Class: "", Icon: "", IconColor: ""},
    ];
  }
  getPreGenerateValues() {
    this.copyAssets = utils.lsGetBool(this.lsPrefix, this.keyCopyAssets);
    this.deleteHtml = utils.lsGetBool(this.lsPrefix, this.keyDeleteHtml);
    this.deletePdf = utils.lsGetBool(this.lsPrefix, this.keyDeletePdf);
  }
  handlePreGenerateChange(ev) {
    switch (ev.target.getAttribute("value")) {
      case this.keyCopyAssets:
        this.copyAssets = !this.copyAssets;
        utils.lsSetBool(this.lsPrefix, this.keyCopyAssets, this.copyAssets);
        break;
      case this.keyDeleteHtml:
        this.deleteHtml = !this.deleteHtml;
        utils.lsSetBool(this.lsPrefix, this.keyDeleteHtml, this.deleteHtml);
        break;
      case this.keyDeletePdf:
        this.deletePdf = !this.deletePdf;
        utils.lsSetBool(this.lsPrefix, this.keyDeletePdf, this.deletePdf);
        break;
    }
  }
  getPostGenerateOptions() {
    return [
      {Key: this.keyIndexSiteForNav, Desc: "Create auxiliary pages (blank, booksindex, dcs, help, servicesindex).", Selected: this.indexSiteForNav,  Class: "", Icon: "", IconColor: ""},
      // {Key: this.keyIndexSiteForSearch, Desc: "Create search indexes.", Selected: this.indexSiteForSearch,  Class: "", Icon: "", IconColor: ""},
    ];
  }
  getPostGenerateValues() {
    this.indexSiteForNav = utils.lsGetBool(this.lsPrefix, this.keyIndexSiteForNav);
    // this.indexSiteForSearch = utils.lsGetBool(this.lsPrefix, this.keyIndexSiteForSearch);
  }
  handlePostGenerateChange(ev) {
    switch (ev.target.getAttribute("value")) {
      case this.keyIndexSiteForNav:
        this.indexSiteForNav = !this.indexSiteForNav;
        utils.lsSetBool(this.lsPrefix, this.keyIndexSiteForNav, this.indexSiteForNav);
        break;
      // case this.keyIndexSiteForSearch:
      //   this.indexSiteForSearch = !this.indexSiteForSearch;
      //   utils.lsSetBool(this.lsPrefix, this.keyIndexSiteForSearch, this.indexSiteForSearch);
      //   break;
    }
  }

  getDaysOfWeekOptions() {
    return [
      {Key: "0", Desc: "Sunday", Selected: this.sundaySelected,  Class: "", Icon: "", IconColor: ""},
      {Key: "1", Desc: "Monday", Selected: this.mondaySelected,  Class: "", Icon: "", IconColor: ""},
      {Key: "2", Desc: "Tuesday", Selected: this.tuesdaySelected,  Class: "", Icon: "", IconColor: ""},
      {Key: "3", Desc: "Wednesday", Selected: this.wednesdaySelected,  Class: "", Icon: "", IconColor: ""},
      {Key: "4", Desc: "Thursday", Selected: this.thursdaySelected,  Class: "", Icon: "", IconColor: ""},
      {Key: "5", Desc: "Friday", Selected: this.fridaySelected,  Class: "", Icon: "", IconColor: ""},
      {Key: "6", Desc: "Saturday", Selected: this.saturdaySelected,  Class: "", Icon: "", IconColor: ""},
    ];
  }
  getSelectedDaysOfWeek() {
    this.selectedDaysOfWeek = JSON.parse(utils.lsGet(this.lsPrefix, this.lsSelectedDaysOfWeek));
    if (this.selectedDaysOfWeek === null) {
      this.selectedDaysOfWeek = [];
    }
    this.selectedDaysOfWeek.forEach((i) => {
      switch (i) {
        case 0:
          this.sundaySelected = true;
          break;
        case 1:
          this.mondaySelected = true;
          break;
        case 2:
          this.tuesdaySelected = true;
          break;
        case 3:
          this.wednesdaySelected = true;
          break;
        case 4:
          this.thursdaySelected = true;
          break;
        case 5:
          this.fridaySelected = true;
          break;
        case 6:
          this.saturdaySelected = true;
          break;
      }
    });
    return this.selectedDaysOfWeek
  }
  handleDaysOfWeekChange(ev) {
    const checked = utils.strToIntArray(this.daysOfWeekCheckboxGroup?.getAttribute("checked"));
    this.selectedDaysOfWeek = [];
    checked.forEach((i) => {
      this.selectedDaysOfWeek.push(i);
    });
    utils.lsSet(this.lsPrefix, this.lsSelectedDaysOfWeek, JSON.stringify(this.selectedDaysOfWeek));
  }
  handleLayoutSelection(ev) {
    const checked = utils.strToIntArray(this.layoutsCheckboxGroup?.getAttribute("checked"));
    this.selectedLayouts = [];
    checked.forEach((i) => {
      this.selectedLayouts.push(this.layouts[i]);
    });
    utils.lsSet(this.lsPrefix, this.lsSelectedLayouts, JSON.stringify(this.selectedLayouts));
  }
  handleTemplateStatusesSelection(ev) {
    const checked = utils.strToIntArray(this.templateStatusesCheckboxGroup?.getAttribute("checked"));
    this.selectedTemplateStatuses = [];
    checked.forEach((i) => {
      this.selectedTemplateStatuses.push(this.templateStatuses[i]);
    });
    utils.lsSet(this.lsPrefix, this.lsSelectedTemplateStatuses, JSON.stringify(this.selectedTemplateStatuses));
  }

  handleSiteTypeSelection(ev) {
    this.selectedSiteType = ev.target.getAttribute("value");
    utils.lsSet(this.lsPrefix, this.lsSelectedSiteType, this.selectedSiteType);
  }
  // indexOfSelectedSiteType returns the siteTypeOptions index of the key found in local storage.
  // If not found, returns the index of the default key.  Local storage will also be set to the default.
  indexOfSelectedSiteType() {
    let defaultIndex = 0;
    this.selectedSiteType = utils.lsGet(this.lsPrefix, this.lsSelectedSiteType);
    if (!this.selectedSiteType || this.selectedSiteType === "undefined") {
      this.selectedSiteType = this.siteTypeDefaultKey;
      utils.lsSet(this.lsPrefix, this.lsSelectedSiteType, this.selectedSiteType);
    }
    const j = this.siteTypeOptions.length;
    for(let i = 0; i < j; i++) {
      const key = this.siteTypeOptions[i].Key
      if (key === this.siteTypeDefaultKey) {
        defaultIndex = i;
      }
      if (key === this.selectedSiteType) {
        return i;
      }
    }
    utils.lsSet(this.lsPrefix, this.lsSelectedSiteType, this.siteTypeDefaultKey);
    return defaultIndex;
  }

  handleFileTypeSelection(ev) {
    this.selectedFileType = ev.target.getAttribute("value");
    utils.lsSet(this.lsPrefix, this.lsSelectedFileType, this.selectedFileType);
    this.setFileTypes();
  }
  setFileTypes() {
    this.genHtml = false;
    this.genPdf = false;
    switch (this.selectedFileType) {
      case this.keyGenBoth:
        this.genHtml = true;
        this.genPdf = true;
        break;
      case this.keyGenHtml:
        this.genHtml = true;
        this.genPdf = false;
        break;
      case this.keyGenPdf:
        this.genHtml = false;
        this.genPdf = true;
        break;
    }
  }
  // indexOfSelectedFileType returns the fileTypeOptions index of the key found in local storage.
  // If not found, returns the index of the default key.  Local storage will also be set to the default.
  indexOfSelectedFileType() {
    let defaultIndex = 0;
    this.selectedFileType = utils.lsGet(this.lsPrefix, this.lsSelectedFileType);
    if (!this.selectedFileType) {
      this.selectedFileType = this.fileTypeDefaultKey;
      utils.lsSet(this.lsPrefix, this.lsSelectedFileType, this.selectedFileType);
    }
    this.setFileTypes();
    const j = this.fileTypeOptions.length;
    for(let i = 0; i < j; i++) {
      const key = this.fileTypeOptions[i].Key
      if (key === this.fileTypeDefaultKey) {
        defaultIndex = i;
      }
      if (key === this.selectedFileType) {
        return i;
      }
    };
    return defaultIndex;
  }
  handleWsMessage(ev) {
    console.log(JSON.stringify(ev))
    if (ev && ev.detail && ev.detail.msg) {
      this.generateStatusMessage(utils.alerts.Info, ev.detail.msg)
    }
  }
  handleWsNotice(ev) {
    //get the
    if (ev.detail.hide) {
      this.divGenerateDelayNotice.style.display = "none";
    } else {
      this.divGenerateDelayNotice.className = "";
      this.divGenerateDelayNotice.classList.add(utils.alerts.Warning);
      this.divGenerateDelayNotice.innerHTML = utils.getMessageSpan(utils.alerts.Warning, ev.detail.msg);
      this.divGenerateDelayNotice.style.display = "block";
    }
  }

  disableButtons(value) {
    this.generateButton.disabled = value;
    this.indexForNavButton.disabled = value;
    // this.indexForSearchButton.disabled = value;
  }
  handleGenerate(ev) {
    if (ev) {
      ev.stopPropagation();
      ev.preventDefault();
    }
    this.disableButtons(true);
    this.generateStatusMessage(utils.alerts.Primary, "Generating...")
    this.querySelector("#webSocketClient").classList.remove("d-none")
    this.requestGeneration(this.libraries).then(response => {
      this.disableButtons(false);
      if (response.Status === "OK") {
        this.generateStatusMessage(utils.alerts.Success, "Generation finished OK. But, always check below for warnings or errors.")
      } else {
        this.generateStatusMessage(utils.alerts.Danger, `${response.Status}: ${response.Message}`)
      }
    }).catch(error => {
      this.disableButtons(false);
      this.generateStatusMessage(utils.alerts.Danger, error)
    });
  }
  handleIndexForNav(ev) {
    if (ev) {
      ev.stopPropagation();
      ev.preventDefault();
    }
    this.disableButtons(true);
    this.generateStatusMessage(utils.alerts.Primary, "Creating auxiliary pages and navigation indexes...")
    this.querySelector("#webSocketClient").classList.remove("d-none")
    this.requestIndexNav().then(response => {
      this.disableButtons(false);
      if (response.Status === "OK") {
        this.generateStatusMessage(utils.alerts.Success, "Creation of auxiliary pages finished OK. But, always check below for warnings or errors.")
      } else {
        this.generateStatusMessage(utils.alerts.Danger, `${response.Status}: ${response.Message}`)
      }
    }).catch(error => {
      this.disableButtons(false);
      this.generateStatusMessage(utils.alerts.Danger, error)
    });
  }
  handleIndexForSearch(ev) {
    if (ev) {
      ev.stopPropagation();
      ev.preventDefault();
    }
    this.disableButtons(true);
    this.generateStatusMessage(utils.alerts.Primary, "Indexing for search...")
    this.querySelector("#webSocketClient").classList.remove("d-none")
    this.requestIndexSearch().then(response => {
      this.disableButtons(false);
      if (response.Status === "OK") {
        this.generateStatusMessage(utils.alerts.Success, "Indexing finished OK. But, always check below for warnings or errors.")
      } else {
        this.generateStatusMessage(utils.alerts.Danger, `${response.Status}: ${response.Message}`)
      }
    }).catch(error => {
      this.disableButtons(false);
      this.generateStatusMessage(utils.alerts.Danger, error)
    });
  }
  // ---- MESSAGE Handlers
  generateStatusMessage(type, message) {
    this.divGenerateStatus.className = "";
    this.divGenerateStatus.classList.add(type);
    this.divGenerateStatus.innerHTML = utils.getMessageSpan(type, message);
  }
  // ---- REST API Handlers

  async requestGeneration() {
    const url = this.host
        + `api/generate?public=${this.selectedSiteType === "public"}`
        +  `&copyAssets=${this.copyAssets}`
        +  `&deleteHtml=${this.deleteHtml}`
        +  `&deletePdf=${this.deletePdf}`
        +  `&genHtml=${this.genHtml}`
        +  `&genPdf=${this.genPdf}`
        +  `&indexSiteForNav=${this.indexSiteForNav}`
        +  `&indexSiteForSearch=false`
      // +  `&indexSiteForSearch=${this.indexSiteForSearch}`
        +  `&layouts=${this.selectedLayouts}`
        +  `&statuses=${this.selectedTemplateStatuses}`
        +  `&dow=${this.selectedDaysOfWeek}`
    ;
    let fetchData = {
      method: 'PUT'
    }

    const response = await fetch(url, fetchData);
    if (!response.ok) {
      let message = "";
      switch (response.status) {
        case 404: {
          message += "not found";
          break;
        }
        case 500: {
          message += "doxa server error";
          break;
        }
        default: {
          message += `An error has occurred: ${response.status}`;
        }
      }
      throw new Error(message);
    }
    const responseJson = await response.json();
    return responseJson;
  }
  // Request that navigation index pages and other auxiliary pages be created
  async requestIndexNav() {
    const url = this.host
        + `api/index/nav?public=${this.selectedSiteType === "public"}`
    ;
    let fetchData = {
      method: 'PUT'
    }

    const response = await fetch(url, fetchData);
    if (!response.ok) {
      let message = "";
      switch (response.status) {
        case 404: {
          message += "not found";
          break;
        }
        case 500: {
          message += "doxa server error";
          break;
        }
        default: {
          message += `An error has occurred: ${response.status}`;
        }
      }
      throw new Error(message);
    }
    return await response.json();
  }
  // Request that the search indexes be created
  async requestIndexSearch() {
    const url = this.host
        + `api/index/search?public=${this.selectedSiteType === "public"}`
    ;
    let fetchData = {
      method: 'PUT'
    }

    const response = await fetch(url, fetchData);
    if (!response.ok) {
      let message = "";
      switch (response.status) {
        case 404: {
          message += "not found";
          break;
        }
        case 500: {
          message += "doxa server error";
          break;
        }
        default: {
          message += `An error has occurred: ${response.status}`;
        }
      }
      throw new Error(message);
    }
    const responseJson = await response.json();
    return responseJson;
  }

  // Get the layout indexes
  requestLayoutIndexes() {
    const url = this.host
        + `api/layouts?`
    ;

    utils.callApi(url, "GET").then(response => {
      if (response.Status === "OK") {
        if (! response.Layouts) {
          return;
        }
        this.layouts = response.Layouts
        let selectionMap = new Map();
        if (this.selectedLayouts && this.selectedLayouts?.length > 0) {
          this.selectedLayouts.forEach((i) => {
            selectionMap.set(i, true);
          });
        } else {
          this.selectedLayouts = [];
          this.layouts.forEach((layout) => {
            this.selectedLayouts.push(layout);
            selectionMap.set(layout, true);
          });
        }
        let items = [];
        this.layouts.forEach((layout, index) => {
          items.push({Key: index, Desc: layout, Selected: selectionMap.has(layout),  Class: "", Icon: "", IconColor: ""},
          )
        });
        this.layoutsCheckboxGroup.setAttribute("items", JSON.stringify(items));
      } else {
        alert(response.Status)
      }
    }).catch(error => {
      alert(error);
    });
  }

  // Get the Types of Statuses that can be set in a template
  requestTemplateStatusTypes() {
    const url = this.host
        + `api/lml/statuses`
    ;

    utils.callApi(url, "GET").then(response => {
      if (response.Status === "OK") {
        if (! response.StatusTypes) {
          return;
        }
        this.templateStatuses = response.StatusTypes
        let items = [];
        this.templateStatuses.forEach((statusType, index) => {
          items.push({Key: index, Desc: statusType, Selected: this.indexInSelectedTemplateStatuses(statusType),  Class: "", Icon: "", IconColor: ""},)
        });
        this.templateStatusesCheckboxGroup.setAttribute("items", JSON.stringify(items));
      } else {
        alert(response.Status)
      }
    }).catch(error => {
      alert(error);
    });
  }
  indexInSelectedTemplateStatuses(i) {
    let selected = false;
    if (this.selectedTemplateStatuses) {
      this.selectedTemplateStatuses.forEach((selection) => {
//      alert(`i: ${i} selection: ${selection}`)
        if (i === selection) {
          selected = true;
        }
      });
    } else if (i === 0) {
      selected = true;
    }
 //   alert(`indexInSelectedTemplateStatuses(${i}) selected === ${selected}`)
    return selected;
  }
// ---- End REST API Handlers

  static get observedAttributes() {
    return ['host'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
  }

  adoptedCallback() {
  }
  get host() {
    return this.getAttribute("host");
  }

  set host(value) {
    this.setAttribute("host", value);
  }
}

window.customElements.define('generation-manager', GenerationManager);