class GenLayoutPicker extends HTMLElement {
  constructor() {
    super();
    console.log("static/wc/gen-layout-picker.js")
    this.selectedLibraryComboKey = "selectedLibraryCombo"
  }

  connectedCallback() {

    // set the combo index value
    if (! this.selected || this.selected === null || this.selected === "null" || this.selected.length === 0) {
      this.selected = 1;
    }
    // see if there is a selected value in local storage
    const previewLibraryCombo = localStorage.getItem(this.selectedLibraryComboKey);
    if (previewLibraryCombo !== null && previewLibraryCombo.length > 0) {
      this.selected = previewLibraryCombo;
    }

    this.innerHTML =
`
<style> 
  @import url('static/css/bootstrap.min.css');
  @import url('static/img/bootstrap-icons.css');
</style>
<style>
  .library-combo-container {
    padding-top: 5vh;
    margin-left: 0 !important;
    padding-left: 0 !important;
  }
</style>
<div class="container library-combo-container">
<div class="row p-1">
    <generation-layouts id="genTableLayouts" host="" acronym="${this.selected}"></generation-layouts>
</div>
</div>
`;

    this.querySelector("#genTableLayouts").addEventListener('click', (ev) => this.handleListClick(ev));
  }

  handleListClick(event) {
    if (event && event.target && event.target.value)
      this.selected = event.target?.value
    console.log(`gen-layout-picker: handleListCLick : ${this.selected} ${event.target.textContent}`)
      localStorage.setItem(this.selectedLibraryComboKey, this.selected);
  }
  disconnectedCallback() {
  }

  static get observedAttributes() {
    return ['selected'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
    if (name === "selected") {

    }
  }

  adoptedCallback() {
  }
  get host() {
    return this.getAttribute("host");
  }

  set host(value) {
    this.setAttribute("host", value);
  }
  get selected() {
    return this.getAttribute("selected");
  }

  set selected(value) {
    this.setAttribute("selected", value);
  }

}

window.customElements.define('gen-layout-picker', GenLayoutPicker);