import * as utils from "./utils.js";

class SynchManager extends HTMLElement {
  constructor() {
    super();
    console.log("static/wc/synch-manager.js")
    // local storage keys.  ls = local storage
    this.wc = "sm:"; // synch-manager
    this.lsSelectedStrategy = this.wc + "strategy"
    this.evtSource = null;
    this.divSynchAllStatus = null;
    this.libraries = [];
    this.selectedStrategy = "";
    this.synchAllButton = null
    // Strategy option keys values must exactly match the CodeForType strings in pkg/enums/synchStrategies.go
    // An attempt will be made to set the selected item to the value in local storage (this.lsSelectedStrategy).
    // If not found, the default is specified below in this.strategyDefaultKey
    this.strategyOptions = [
      {Key: "combine", Desc: "Make remote repos, local repos and database content identical, but keep changes I made since the last synch.", Selected: true, Class: "alert-success", Icon: "", IconColor: ""},
      {Key: "resetToRemote", Desc: "Throw away database and local repo changes. Make the local repos and database identical to the remote repos.", Selected: false, Class: "alert-warning", Icon: "", IconColor: ""},
      {Key: "resetToDB", Desc: "Throw away remote repo changes. Make the remote repos identical to local repos and the database contents.", Selected: false, Class: "alert-warning", Icon: "", IconColor: ""},
    ];
    this.strategyDefaultKey = "combine";
    this.strategyRadioGroup = null;
  }

  connectedCallback() {
    this.innerHTML =
`
<style> 
  @import url('static/css/bootstrap.min.css');
  @import url('static/img/bootstrap-icons.css');
</style>
<div id="my-element-div" class="wc-title">Synchronization Manager</div>
<br>
<hr>
<div style="display: flex; flex-direction: column; flex-wrap: wrap; gap: 1rem; max-width: 50vw;">
    <div>
        The synchronization manager exports a copy of the database records to *.dkv (text) files in ${this.dir},
        so they can be kept in a safe place. 
        It also keeps the database up-to-date if you have subscriptions. 
    </div>
    <div>
        When you click the "Sync All" button, Doxa will do
        the following...
    </div>
    <div>
        <ul>
            <li style="padding-top: 20px;">
              If you subscribe to resources from someone else, Doxa
              will automatically pull updates from them and load
              them into the database for you. 
              </li>
            <li style="padding-top: 20px;">
              If there are libraries you manage yourself, 
              Doxa will export them from the database 
              to text files.  If in your user profile you indicated that you want Doxa to handle git and gitlab
              for you, Doxa will automatically push your data to your gitlab group. 
            </li>
        </ul>
    </div>
    <div>
        ${this.getProfileText()}
    </div>
    <br>
</div>
<div style="display: flex; gap: 20px;">  <!-- Strategy Selector -->
    ${this.getSynchronizationStrategy()}
</div>
<br>
<div style="display: flex; flex-direction: row; gap: 20px;">  <!-- Sync All Button -->
    <span>
        <button id="btnSynchAll"  type="button" class="btn btn-primary" style="">Sync All</button>
    </span>
    <br>
    <div class="alert alert-light message" role="alert" id="synchAllStatus" style="padding: 3px; width: max-content;">&nbsp;</div>
</div> <!-- end Sync All Button -->
<br>
<div>
    <web-socket-client id="webSocketClient" class="d-none" endpoint="/ws/synch" title=""></web-socket-client>
</div>
<br>

<hr/>
<div class="row">
    <div style="font-style: italic; color: #0d6efd;">Subscriptions</div>
</div>
<hr/>
<div class="row">
    <div style="font-style: italic; color: #0d6efd;">Your Git Repositories</div>
</div>
`;

    // message handles
    this.divSynchAllStatus = this.querySelector("#synchAllStatus");
    // other handles
    this.strategyRadioGroup = this.querySelector("#strategyRadioGroup");
    this.synchAllButton = this.querySelector("#btnSynchAll");

    // add listeners.  For each one added, remove it in disconnectedCallback.
    this.querySelector("#btnSynchAll").addEventListener("click", (ev) => this.handleSynchAll(ev))

    if (this.strategyRadioGroup) {
      this.strategyRadioGroup.setAttribute("items", JSON.stringify(this.strategyOptions));
      this.strategyRadioGroup.setAttribute("active", this.indexOfSelectedStrategy());
      this.strategyRadioGroup.addEventListener("change", (ev) => this.handleStrategySelection(ev));
    }

    if (this.internet === "false") {
      this.synchAllButton.innerText = "Export Database Records";
    }
  }

  disconnectedCallback() {
    this.querySelector("#btnSynchAll").removeEventListener("click", (ev) => this.handleSynchAll(ev))
    if (this.strategyRadioGroup) {
      this.strategyRadioGroup.removeEventListener("change", (event) => this.handleStrategySelection(event));
    }
  }

  handleStrategySelection(ev) {
    this.selectedStrategy = ev.target.getAttribute("value");
    localStorage.setItem(this.lsSelectedStrategy, this.selectedStrategy);
  }
  // indexOfSelectedStrategy returns the strategyOptions index of the key found in local storage.
  // If not found, returns the index of the default key.  Local storage will also be set to the default.
  indexOfSelectedStrategy() {
    let defaultIndex = 0;
    this.selectedStrategy = localStorage.getItem(this.lsSelectedStrategy);
    if (!this.selectedStrategy) {
      this.selectedStrategy = this.strategyDefaultKey;
    }
    const j = this.strategyOptions.length;
    for(let i = 0; i < j; i++) {
      const key = this.strategyOptions[i].Key
      if (key === this.strategyDefaultKey) {
        defaultIndex = i;
      }
      if (key === this.selectedStrategy) {
        return i;
      }
    };
    localStorage.setItem(this.lsSelectedStrategy, this.strategyDefaultKey);
    return defaultIndex;
  }
  getProfileText() {
    if (this.profile === "false") {
      const text = `
      Doxa does not know if you want to handle git yourself. 
      You have not set your profile yet.
      To set your profile, click <a href="profile">here.</a>
      `
      return utils.getAlert(utils.alerts.Warning, text);
    }
    if (this.git === "user") {
      const text = `
        In your profile, you indicated that you will handle git.
        Therefore, Doxa will not push your data for you.
        You have to handle that yourself.  This is not recommended 
        unless you are experienced and comfortable using git.
        Doxa will, however, pull data from any subscriptions you have 
        and load it into the database.  To change your User Profile, click <a href="profile">here.</a>
      `
      return utils.getAlert(utils.alerts.Warning, text);
    }
    if (this.gitlab.length === 0) {
      const text = `
        In your profile, you indicated that you want Doxa
        to handle git and Gitlab for you.
        However, you have not set your Gitlab Group ID
        or Personal Access Token.  Please update your
         <a href="profile">User Profile.</a>
      `
      return utils.getAlert(utils.alerts.Danger, text);
    }
    if (this.internet === "false") {
      const text = `
        In your profile, you indicated that you want Doxa
        to handle git and Gitlab for you.
        However, Gitlab can't be accessed.  Do you have Internet connectivity?
        If you do, wait and try synching again in a few minutes.
        You can go ahead and export your database records, but Doxa won't be able 
        to save them in Gitlab until connectivity to Gitlab is working.
        Also, if you have Subscriptions, you won't be able to get updates
        from them until you have Internet connectivity.</a>
      `
      return utils.getAlert(utils.alerts.Warning, text);
    }
    const text = `
        In your profile, you indicated that you want Doxa
        to handle git and Gitlab for you.  Doxa will save (push)
        your data to the <a href="${this.gitlab}" target="_blank">Gitlab Group</a> you specified in
        your <a href="profile">User Profile</a>.
    `
    return utils.getAlert(utils.alerts.Success, text);
  }
  getSynchronizationStrategy() {
    if (this.profile === "false") {
      return "";
    }
    if (this.git === "user") {
      return "";
    }
    if (this.gitlab.length === 0) {
      return "";
    }
    if (this.internet === "false") {
      return "";
    }
    let text =  `    <div>Synchronization Strategy:</div>
                      <radio-group id="strategyRadioGroup" items="[]" active="0"></radio-group>
`
    const info = `The word <em>repos</em> means <em>repositories</em>.
                  Local repos are repositories stored on your computer.
                  Remote repos are ones stored in the cloud (Internet) at Gitlab.com.
                  <br>
                  <br>
                  Normally, you should select the strategy with a green background: <em>Make remote repos, local repos and database content identical...</em>
                  <br>
                  <br>
                  The strategy options with a yellow background should only be used when you
                  are certain it is what you want to do.  For example, you have been using Doxa
                  on two computers and have made changes to Doxa content on both.  And, you have
                  synchronized the content on one of the computers.  This means the remote repo
                  has changes not on the second computer, but the second computer has changes the
                  remote does not. The strategies in yellow allow you to make a choice about what
                  to do.
`;
    text = text + utils.getAlert(utils.alerts.Info, info);
    return text;
  }
  handleListBtxReadSaveBtnClicked() {
    alert(this.listBtxRead.getAttribute("items"));
  }
  handleSynchAll(ev) {
    ev.stopPropagation();
    ev.preventDefault();
    this.synchAllButton.disabled = true;
    this.querySelector("#webSocketClient").classList.remove("d-none")
    this.synchAllMessage(utils.alerts.Primary, "Synching...")
    this.libraries = []
    this.libraries.push("all");
    this.requestSynch(this.libraries).then(response => {
      this.synchAllButton.disabled = false;
      if (response.Status === "OK") {
        this.synchAllMessage(utils.alerts.Success, "Synch finished OK")
      } else {
        this.synchAllMessage(utils.alerts.Danger, `${response.Status}: ${response.Message}`)
      }
    }).catch(error => {
      this.synchAllButton.disabled = false;
      this.synchAllMessage(utils.alerts.Danger, error)
    });
  }
  // ---- MESSAGE Handlers
  synchAllMessage(type, message) {
    this.divSynchAllStatus.className = "";
    this.divSynchAllStatus.classList.add(type);
    this.divSynchAllStatus.innerHTML = utils.getMessageSpan(type, message);
  }
  // ---- REST API Handlers

  async requestSynch(libraries) {
    const url = this.host
        + `api/synch?libraries=${JSON.stringify(libraries)}
        &strategy=${this.selectedStrategy}`
    ;
    let fetchData = {
      method: 'PUT'
    }

    const response = await fetch(url, fetchData);
    if (!response.ok) {
      let message = "";
      switch (response.status) {
        case 404: {
          message += "not found";
          break;
        }
        case 500: {
          message += "doxa server error";
          break;
        }
        default: {
          message += `An error has occurred: ${response.status}`;
        }
      }
      throw new Error(message);
    }
    const responseJson = await response.json();
    return responseJson;
  }
// ---- End REST API Handlers

  static get observedAttributes() {
    return ['host'];
  }

  attributeChangedCallback(host, oldVal, newVal) {
  }

  adoptedCallback() {
  }
  get host() {
    return this.getAttribute("host");
  }

  set host(value) {
    this.setAttribute("host", value);
  }
  get dir() {
    return this.getAttribute("dir");
  }

  set dir(value) {
    this.setAttribute("dir", value);
  }
  get git() {
    return this.getAttribute("git");
  }

  set git(value) {
    this.setAttribute("git", value);
  }
  get gitlab() {
    return this.getAttribute("gitlab");
  }

  set gitlab(value) {
    this.setAttribute("gitlab", value);
  }
  get internet() {
    return this.getAttribute("internet");
  }

  set internet(value) {
    this.setAttribute("internet", value);
  }
  get profile() {
    return this.getAttribute("profile");
  }

  set profile(value) {
    this.setAttribute("profile", value);
  }

}

window.customElements.define('synch-manager', SynchManager);