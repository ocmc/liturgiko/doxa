import * as utils from "./utils.js";

class DataTable extends HTMLElement {
  constructor() {
    super();

    // html element handles
    this.tableTitleSpan = null;
    this.divDesc = null;
    this.tableBody = null;
    this.tableHeadRow = null;

    this.rowTopSpan = null;
    this.rowBottomSpan = null;
    this.rowCountSpan = null;
    this.currentPageSpan = null;
    this.pageMaxSpan = null;
    this.pageSizeIdSpan = null;

    this.firstButton = null;
    this.prevButton = null;
    this.nextButton = null;
    this.lastButton = null;

    this.filterColumnSelect = null;

    this.curPage = 1;
    this.inputFilter = null;
    this.currentFilter = "";


    this.tableData = null;

    this.columnToFilter = 0;

    this.Values = [];
    this.dataLength = 0;
    this.searchPattern = "";
    this.colSourceWidth = "1%"
    this.colIdWidth = "28%";
    this.colLineColWidth = "1%";
    this.colMatchWidth = "69%";
    this.colOpenWidth = "1%"
    this.dataIndexArray = [];
    // for paging the data in the table
    this.curPage = 1;
    this.pageSize = 10;
    this.scrollY = "";
    this.useId = false;
    this.divAbout = null;
    this.iconAbout = null;
  }

  connectedCallback() {
    this.innerHTML =
`
<style> 
  @import url('static/css/bootstrap.min.css');
  @import url('static/img/bootstrap-icons.css');
</style>
<style>
.sticky-top { 
    background-color: #fff;
}
#data-table-Container {
    overflow-y:scroll !important;
}
table {
}
.filterColor {
  color: blue;
}
.searchColor {
  color: red;
}
.alert {
    line-height: 1px;
}
.match {
  color: red;
}
.leftContext {
}
.colMatch {
    text-align: center;
}
.rightContext {
}
.tableBody {
    
}
.clickable {
    cursor:pointer;
    outline:none;
}
</style>
<div id="${this.prefix}-data-table-Container" class="container container-fluid">
<div class="wc-title"><span id="${this.prefix}-tableTitleSpan"></span>&nbsp;<i id="${this.prefix}-iconAbout" class="bi bi-question-circle clickable" style="color: cornflowerblue;"></i></div>
<div id="${this.prefix}-divAbout" class="d-none" style="display: flex; flex-direction: column; flex-wrap: wrap; gap: 1rem; max-width: 95vw;">
    <br>
    <div id="${this.prefix}-divDesc">
    </div>
</div>
<hr>
<!-- filters-->
  <div class="row p-1">
      <div class="col">
          <div class="input-group mb-3">
              <span class="input-group-text" id="${this.prefix}-basic-addon1"><i class="bi-filter"
                                                                  style="color: cornflowerblue;"></i></span>
              <input type="text" id="${this.prefix}-data-table-filter-id" class="form-control" placeholder="filter"
                     aria-label="filter table" aria-describedby="${this.prefix}-basic-addon1">
          </div>
      </div>
      <div class="col">
          <select id="${this.prefix}-data-table-filter-column-select" class="form-select" aria-label="Select column to filter"></select>
      </div>
  </div> <!-- end filters-->
<!-- table -->
  <div class="table-responsive">
    <table class="table table-striped table-responsive">
        <thead>
            <tr id="${this.prefix}-tableHeadRow"></tr>
        </thead>
        <tbody id="${this.prefix}-tableBody" translate="no"></tbody>
    </table>  
  </div> <!-- end table div -->
  <!-- Pagination -->
     <div class="row p-1">
        <div class="col col-4">
            <div class="control-label" style="padding-top: 12px;">Showing rows <span id="${this.prefix}-rowTop">0</span> to <span id="${this.prefix}-rowBottom">0</span> of <span id="${this.prefix}-rowCount">0</span></div>
        </div>
        <div class="col col-4"><div class="control-label" style="margin-top: 4px; text-align: center;"><span>Page size: </span><input id="${this.prefix}-pageSizeId" style="max-width: 10ch; text-align: center;"></div></div>
        <div class="col col-4">
            <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-end">
              <li class="page-item">
                <a id="${this.prefix}-firstButton" class="page-link d-none" href="#" aria-label="First">
                  <span aria-hidden="true"><i class="bi-chevron-double-left"></i></span>
                </a>
              </li>
              <li class="page-item">
                <a id="${this.prefix}-prevButton" class="page-link d-none" href="#" aria-label="Previous">
                  <span aria-hidden="true"><i class="bi-chevron-left"></i></span>
                </a>
              </li>
              <li class="page-item">
                <div class="control-label" style="margin-top: 4px;"> 
                  <span style="margin-left: 4px;">Page</span>
                  <input style="margin-left: 2px; max-width: 10ch; text-align: center;" id="${this.prefix}-currentPage">
                  <span style="margin-left: 2px;">of</span>
                  <span style="margin-left: 2px;margin-right: 4px;" id="${this.prefix}-pageMax"></span>
                </div>
              </li>
              <li class="page-item">
                <a id="${this.prefix}-nextButton" class="page-link" href="#" aria-label="Next">
                  <span aria-hidden="true"><i class="bi-chevron-right"></i></span>
                </a>
              </li>
              <li class="page-item">
                <a id="${this.prefix}-lastButton" class="page-link" href="#" aria-label="Last">
                  <span aria-hidden="true"><i class="bi-chevron-double-right"></i></span>
                </a>
              </li>
            </ul>
          </nav>
        </div>
        </div>
     </div>

</div> <!-- container -->
`;

    this.currentPageSpan = document.getElementById(`${this.prefix}-currentPage`);
    this.divAbout = this.querySelector(`#${this.prefix}-divAbout`);
    this.divDesc = this.querySelector(`#${this.prefix}-divDesc`);
    this.filterColumnSelect = this.querySelector(`#${this.prefix}-data-table-filter-column-select`);
    this.firstButton = document.getElementById(`${this.prefix}-firstButton`);
    this.iconAbout = this.querySelector(`#${this.prefix}-iconAbout`);
    this.inputFilter = this.querySelector(`#${this.prefix}-data-table-filter-id`);
    this.lastButton = document.getElementById(`${this.prefix}-lastButton`);
    this.nextButton = document.getElementById(`${this.prefix}-nextButton`);
    this.pageMaxSpan = document.getElementById(`${this.prefix}-pageMax`);
    this.pageSizeIdSpan = document.getElementById(`${this.prefix}-pageSizeId`);
    this.prevButton = document.getElementById(`${this.prefix}-prevButton`);
    this.rowBottomSpan = document.getElementById(`${this.prefix}-rowBottom`);
    this.rowCountSpan = document.getElementById(`${this.prefix}-rowCount`);
    this.rowTopSpan = document.getElementById(`${this.prefix}-rowTop`);
    this.tableBody = this.querySelector(`#${this.prefix}-tableBody`);
    this.tableHeadRow = this.querySelector(`#${this.prefix}-tableHeadRow`);
    this.tableTitleSpan = this.querySelector(`#${this.prefix}-tableTitleSpan`);

    this.rowTopSpan.innerText = "0";
    this.rowBottomSpan.innerText = "0";
    this.rowCountSpan.innerText = "0";
    this.currentPageSpan.value = "0";
    this.pageMaxSpan.innerText = "0";
    this.pageSizeIdSpan.value = "0";

    this.firstButton.classList.remove("d-none");
    this.prevButton.classList.remove("d-none");
    this.nextButton.classList.add("d-none");
    this.lastButton.classList.add("d-none");


  // add listeners.  For each one added, remove it in disconnectedCallback.
    this.filterColumnSelect.addEventListener('change', (ev) => {
      this.columnToFilter = ev.target.selectedIndex;
    });

    this.inputFilter.addEventListener('keyup', () => this.filterData());
    this.iconAbout?.addEventListener('click', () => {
      if (this.divAbout?.classList.contains("d-none")) {
        this.divAbout.classList.remove("d-none");
      } else {
        this.divAbout.classList.add("d-none");
      }
    });
    this.currentPageSpan.addEventListener("keypress", (e) => {
      if (e.key === 'Enter') {
        this.curPage = document.getElementById(`${this.prefix}-currentPage`).value;
        this.renderTable();
      }
    });
    // listen for page size change
    this.pageSizeIdSpan.addEventListener("keypress", (e) => {
      if (e.key === 'Enter') {
        this.pageSize = document.getElementById(`${this.prefix}-pageSizeId`).value;
        this.curPage = 1;
        this.renderTable();
      }
    });
    this.firstButton.addEventListener('click', () => this.firstPage(this));
    this.prevButton.addEventListener('click', () => this.previousPage(this));
    this.nextButton.addEventListener('click', () => this.nextPage(this));
    this.lastButton.addEventListener('click', () => this.lastPage(this));

    if (this.data) {
      this.tableData = JSON.parse(this.data);
      if (this.tableData) {
        this.divDesc.innerText = this.tableData.Desc;
        this.tableTitleSpan.innerText = this.tableData.Title;
        this.renderTableHeaders();
        this.renderFilterColumnOptions();
        this.Values = this.tableData.Rows;
        this.dataLength = 0;
        if (this.Values) {
          this.dataLength = this.Values.length;
          if (this.dataLength > 0) {
            this.setDataIndexForAllRows();
            this.renderTable();
          }
        }
      }
    }
  }

  disconnectedCallback() {
  }

  renderFilterColumnOptions() {
    this.filterColumnSelect.innerHTML = "";
    this.tableData.Headings.forEach((title, index) => {
      const option = document.createElement("option");
      option.innerHTML = `<option value="${index}">${title}</option>`
      this.filterColumnSelect.appendChild(option);
      });
    this.filterColumnSelect.selectedIndex = this.columnToFilter;
    this.inputFilter.value = this.tableData.FilterValue;
    this.currentFilter = this.tableData.FilterValue;
  }

  renderTableHeaders() {
    this.tableHeadRow.innerHTML = "";
    this.tableData.Headings.forEach((title, index) => {
      const th = document.createElement("th");
      th.setAttribute("style", `width: ${this.tableData.Widths[index]} !important;`)
      th.innerText = title;
      this.tableHeadRow.appendChild(th);
    });
  }
  renderTable() {
    this.tableBody.innerHTML = "";
    this.dataLength = this.dataIndexArray.length;
    if (this.dataLength === 0) {
      return;
    }

    let displaying = 0;
    let start = (this.curPage - 1) * this.pageSize;
    let end = this.curPage * this.pageSize;

    this.dataIndexArray.filter((row, index) => {
      if (index >= start && index < end) return true;
    }).forEach((entry, index) => {
      displaying++;
      const cells = this.Values[entry];
      const row = document.createElement("tr");
      cells.forEach((cell) => {
        const td = document.createElement("td");
        if (cell.Href && cell.Href.length > 0) {
          let target = "_blank";
          if (cell.Target && cell.Target.length > 0) {
            target = cell.Target;
          }
          td.innerHTML = `<a href="${cell.Href}" target="${target}">${cell.Value}</a>`
        } else {
          td.innerText = cell.Value;
        }
        row.appendChild(td);
      });
      this.tableBody.appendChild(row);
    });
    this.setTableSizeNotice(start + 1, start + displaying);
  }
  setDataIndexForAllRows() {
    this.dataIndexArray = [];
    if (!this.Values) {
      return;
    }
    const j = this.Values.length;
    for (let i = 0; i < j; i++) {
      this.dataIndexArray.push(i);
    }
  }

  // Populates theDataIndexArray with indexes of this.Values.
  // If pattern is null, all the indexes of this.Values will be pushed onto the array.
  // If pattern is not null, only indexes of data IDs that match will be pushed onto the array.
  filterData() {
    this.currentFilter = this.inputFilter.value;
    this.dataIndexArray = [];

    if (this.currentFilter.length > 0) {
      this.Values.forEach((item, index) => {
        if ((this.getIdFilterRegEx()).test(item[this.columnToFilter].Value)) {
          this.dataIndexArray.push(index);
        }
      });
    } else {
      this.setDataIndexForAllRows();
    }
    this.curPage = 1;
    this.renderTable();
  }
  getIdFilterRegEx() {
    return new RegExp(this.currentFilter, "gi");
  }

  scrollWindowTo(y) {
    window.scrollTo({top: y, behavior: 'auto'});
  }

  firstPage() {
    if (this.curPage > 1) this.curPage = 1;
    this.renderTable();
  }

  previousPage() {
    if (this.curPage > 1) this.curPage--;
    this.renderTable();
  }

  nextPage() {
    if ((this.curPage * this.pageSize) < this.dataLength) this.curPage++;
    this.renderTable();
  }

  lastPage() {
    if ((this.curPage * this.pageSize) < this.dataLength) {
      this.curPage = Math.floor(this.dataLength / this.pageSize) + 1;
      this.renderTable();
    }
  }

  resetTableSizeNotice() {
    this.rowTopSpan.innerText = "0";
    this.rowBottomSpan.innerText = "0";
    this.rowCountSpan.innerText = "0";
    this.currentPageSpan.value = "0";
    this.pageMaxSpan.innerText = "0";
    this.pageSizeIdSpan.value = "0";
  }

  setTableSizeNotice(start, end) {
    this.rowTopSpan.innerText = `${start.toLocaleString()}`;
    this.rowBottomSpan.innerText = `${end.toLocaleString()}`;
    this.rowCountSpan.innerText = `${this.dataLength.toLocaleString()}`;
    this.currentPageSpan.value = `${this.curPage.toLocaleString()}`;
    this.pageMaxSpan.innerText = `${(Math.ceil(this.dataLength / this.pageSize)).toLocaleString()}`;
    this.pageSizeIdSpan.value = `${this.pageSize}`;

    if (this.curPage === 1) {
      this.firstButton.classList.add("d-none");
     this.prevButton.classList.add("d-none");
    } else {
      this.firstButton.classList.remove("d-none");
      this.prevButton.classList.remove("d-none");
    }
    if (this.curPage === Math.ceil(this.dataLength / this.pageSize)) {
      this.nextButton.classList.add("d-none");
      this.lastButton.classList.add("d-none");
    } else {
      this.nextButton.classList.remove("d-none");
      this.lastButton.classList.remove("d-none");
    }
  }

  // element attributes
  static get observedAttributes() {
    return ['data'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
    if (name === "data" && oldVal !== newVal && newVal !== null) {
      if (this.data) {
        this.tableData = JSON.parse(this.data);
        if (this.tableData) {
          this.divDesc.innerText = this.tableData.Desc;
          this.tableTitleSpan.innerText = this.tableData.Title;
          this.columnToFilter = this.tableData.Filter;
          this.renderTableHeaders();
          this.renderFilterColumnOptions();
          this.Values = this.tableData.Rows;
          if (this.Values) {
            this.dataLength = this.Values.length;
            if (this.dataLength > 0) {
              this.setDataIndexForAllRows();
              this.filterData(); // it will call this.renderTable();
            }
          }
        }
      }
    }
  }

  adoptedCallback() {
  }
  get data() {
    return this.getAttribute("data");
  }

  set data(value) {
    this.setAttribute("data", value);
  }
  get prefix() {
    return this.getAttribute("prefix");
  }

  set prefix(value) {
    this.setAttribute("prefix", value);
  }
}

window.customElements.define('data-table', DataTable);