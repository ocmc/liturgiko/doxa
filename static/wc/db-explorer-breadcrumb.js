import {KeyPath} from "./kvs.js";
import * as utils from "./utils.js";

/**
 * @tagname db-explorer-breadcrumb
 * @description A breadcrumb navigation component for the database explorer that displays and allows navigation of the current database path. Features a bucket selection dropdown for directory context.
 * 
 * @inputs {Attribute} path - The current path in the database
 * @inputs {Attribute} searching - Whether the component is in search mode, affects rendering of the current segment
 * @inputs {Attribute} host - API endpoint host URL (optional, defaults to global host)
 * @outputs {Event} path-change - Fires when a breadcrumb item is clicked with detail: {path: string}
 * @outputs {Event} context-change - Fires when bucket selection changes with detail: {type: 'path', path: string, bucket: string}
 * @api {GET} /api/list - Fetches directory contents for bucket dropdown
 * @watches {Attribute} path - Re-renders the breadcrumb when this attribute changes
 * @watches {Attribute} searching - Re-renders when search mode changes
 * @watches {Attribute} host - Re-initializes API endpoints when host changes
 * 
 * @examples
 * 
 * Basic usage:
 * ```html
 * <db-explorer-breadcrumb id="database-breadcrumb"></db-explorer-breadcrumb>
 * ```
 * 
 * With initial path:
 * ```html
 * <db-explorer-breadcrumb id="database-breadcrumb" path="ltx/en_us_dedes"></db-explorer-breadcrumb>
 * ```
 * 
 * With event handling:
 * ```javascript
 * const breadcrumb = document.getElementById('database-breadcrumb');
 * breadcrumb.addEventListener('path-change', (event) => {
 *   console.log('New path:', event.detail.path);
 *   // Handle navigation to the new path
 * });
 * 
 * breadcrumb.addEventListener('context-change', (event) => {
 *   console.log('Context changed to bucket:', event.detail.bucket);
 *   console.log('In path context:', event.detail.path);
 *   // Handle bucket context change
 * });
 * ```
 * 
 * Uses a simple state model with path segments parsed from the current path.
 * Renders breadcrumb links that dispatch events when clicked to trigger
 * navigation in the parent component. Includes a bucket dropdown for 
 * directory context selection.
 * 
 * State management:
 * - currentPath: Stores the complete path string
 * - pathSegments: Array of path segments derived from currentPath
 * - bucketList: Available buckets for context dropdown
 * - selectedBucket: Currently selected bucket for navigation context
 */
class DbExplorerBreadcrumb extends HTMLElement {
    /**
     * Creates a new breadcrumb navigation component and initializes Shadow DOM
     */
    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.currentPath = "";
        this.pathSegments = [];
        this.bucketList = [];
        this.selectedBucket = "Any";
        this.host = "";
        this.bucketSelect = null;
        
        // Bind event handlers and methods to this instance to ensure proper removal and external access
        this.handleLinkClick = this.handleLinkClick.bind(this);
        this.handleBucketChange = this.handleBucketChange.bind(this);
        this.fetchBuckets = this.fetchBuckets.bind(this);
    }

    /**
     * Specify which attributes should trigger attributeChangedCallback
     * @returns {string[]} Array of attribute names to observe
     */
    static get observedAttributes() {
        return ['path', 'searching', 'host'];
    }

    /**
     * Respond to changes in observed attributes
     * 
     * @param {string} name - The name of the attribute that changed
     * @param {string} oldValue - The previous value of the attribute
     * @param {string} newValue - The new value of the attribute
     */
    attributeChangedCallback(name, oldValue, newValue) {
        if (name === 'path' && oldValue !== newValue) {
            this.currentPath = newValue || "";
            this.pathSegments = this.currentPath ? this.currentPath.split('/') : [];
            this.fetchBuckets();
            this.render();
        } else if (name === 'searching') {
            // Just trigger a re-render when search mode changes
            this.render();
        } else if (name === 'host' && oldValue !== newValue) {
            this.host = newValue || "";
            // Re-fetch buckets if the host changes and we have a path
            if (this.currentPath) {
                this.fetchBuckets();
            }
        }
    }
    
    /**
     * Sets the API host for making requests
     * Detects the host from attributes or global context
     */
    setHost() {
        if (this.hasAttribute("host")) {
            this.host = this.getAttribute("host");
            if (!this.host.endsWith("/")) {
                this.host = this.host + "/";
            }
        } else {
            if (typeof globalHost !== 'undefined' && globalHost && globalHost.length > 0) {
                this.host = globalHost;
                if (!this.host.endsWith("/")) {
                    this.host = this.host + "/";
                }
            } else {
                this.host = "";
            }
        }
    }
    
    /**
     * Fetches the available buckets from the API for the dropdown
     * Populates the bucketList array with results
     */
    fetchBuckets() {
        // Ensure host is set
        this.setHost();
        
        // For root level, need to fetch root buckets
        const url = this.host + 'api/list?path=' + (this.currentPath || '');
        
        utils.callApi(url, "GET")
            .then(response => {
                if (response.Status === "OK" && response.Values) {
                    // Extract buckets (directories) from the response
                    const buckets = response.Values
                        .filter(item => item.Last.endsWith("/"))
                        .map(item => item.Last.replace("/", ""))
                        .sort();
                    
                    // Process buckets with periods to add wildcard options
                    let expandedBuckets = [...buckets]; // Keep original bucket names
                    
                    // For each bucket with periods, generate wildcard options
                    buckets.forEach(bucket => {
                        if (bucket.includes('.')) {
                            const segments = bucket.split('.');
                            
                            // Generate wildcard patterns for each level
                            for (let i = 1; i <= segments.length-1; i++) {
                                // Take first i segments and add wildcard
                                const wildcardPattern = segments.slice(0, i).join('.') + '.*';
                                
                                // Add to expanded buckets if not already present
                                if (!expandedBuckets.includes(wildcardPattern)) {
                                    expandedBuckets.push(wildcardPattern);
                                }
                            }
                        }
                    });
                    
                    // Sort the expanded buckets
                    expandedBuckets.sort();
                    
                    // Always have "Any" as the first option
                    this.bucketList = [".* (any)", ...expandedBuckets];
                    
                    // Re-render with the new bucket list
                    this.render();
                    
                    // Dispatch an event when buckets are loaded
                    this.dispatchEvent(new CustomEvent('buckets-loaded', {
                        bubbles: true,
                        composed: true
                    }));
                }
            })
            .catch(error => {
                console.error("Error fetching buckets:", error);
                this.bucketList = [".* (any)"];
                
                // Dispatch event even on error to prevent deadlocks
                this.dispatchEvent(new CustomEvent('buckets-loaded', {
                    bubbles: true,
                    composed: true,
                    detail: { error: error.message }
                }));
            });
    }

    /**
     * Component lifecycle method called when element is added to DOM
     * Initializes the component and renders the initial UI
     */
    connectedCallback() {
        this.setHost();
        this.fetchBuckets();
        this.render();
    }

    /**
     * Renders the component UI into the shadow DOM
     * Generates the breadcrumb navigation with clickable links
     */
    render() {
        this.shadowRoot.innerHTML = `
            <style>
                :host {
                    display: block;
                    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
                }
                .breadcrumb-container {
                    margin-bottom: 0.25rem;
                    font-weight: bold;
                }
                .breadcrumb {
                    display: flex;
                    flex-wrap: wrap;
                    align-items: center;
                    padding: 0.35rem 1rem;
                    margin-bottom: 0.25rem;
                    list-style: none;
                    background-color: #f8f9fa;
                    border-radius: 0.25rem;
                }
                .breadcrumb-item {
                    display: flex;
                    align-items: center;
                }
                .breadcrumb-item.active {
                    color: #6c757d;
                }
                .segment-delimiter {
                    display: inline-block;
                    padding-left: 0.2rem;
                    padding-right: 0.2rem;
                    color: #6c757d;
                }
                a {
                    color: #007bff;
                    text-decoration: none;
                    cursor: pointer;
                }
                a:hover {
                    color: #0056b3;
                    text-decoration: underline;
                }
                .breadcrumb-help {
                    margin-left: 0.5rem;
                    color: #6c757d;
                    font-size: 0.875rem;
                }
                .search-container {
                    display: flex;
                    align-items: center;
                    padding: 0.5rem 1rem;
                    background-color: #f8f9fa;
                    border-radius: 0.25rem;
                    margin-top: 0.5rem;
                }
                .search-title {
                    font-size: 0.875rem;
                    color: #6c757d;
                    margin-right: 0.5rem;
                }
                .bucket-select {
                    padding: 0.375rem 0.375rem;
                    font-size: 1.0rem;
                    line-height: 1.5;
                    color: #495057;
                    min-width: 100px;
                }
                .search-input {
                    flex: 1;
                    padding: 0.375rem 0.75rem;
                    font-size: 0.875rem;
                    line-height: 1.5;
                    color: #495057;
                    background-color: #fff;
                    border: 1px solid #ced4da;
                    border-radius: 0.25rem;
                    margin-right: 0.5rem;
                }
                .search-button {
                    padding: 0.375rem 0.75rem;
                    font-size: 0.875rem;
                    line-height: 1.5;
                    color: #fff;
                    background-color: #007bff;
                    border: 1px solid #007bff;
                    border-radius: 0.25rem;
                    cursor: pointer;
                }
                .search-button:hover {
                    background-color: #0069d9;
                    border-color: #0062cc;
                }
                .path-display {
                    color: #6c757d;
                    font-style: italic;
                    margin-right: 0.5rem;
                }
                .search-input-group {
                    display: flex;
                    flex: 1;
                }
                .prefix-display {
                    padding: 0.375rem 0.75rem;
                    font-size: 0.875rem;
                    line-height: 1.5;
                    color: #495057;
                    background-color: #e9ecef;
                    border: 1px solid #ced4da;
                    border-right: none;
                    border-radius: 0.25rem 0 0 0.25rem;
                    white-space: nowrap;
                }
                .search-input-with-prefix {
                    flex: 1;
                    padding: 0.375rem 0.75rem;
                    font-size: 0.875rem;
                    line-height: 1.5;
                    color: #495057;
                    background-color: #fff;
                    border: 1px solid #ced4da;
                    border-radius: 0 0.25rem 0.25rem 0;
                }
                .search-button-icon {
                    margin-right: 0.25rem;
                }
                .hidden {
                    display: none;
                }
                /* Loading state styles */
                :host([disabled]) a {
                    pointer-events: none;
                    opacity: 0.6;
                    cursor: not-allowed;
                }
                :host([disabled]) select {
                    pointer-events: none;
                    opacity: 0.6;
                }
                /* Add loading indicator using ::after on all links when disabled */
                :host([disabled]) .breadcrumb-container::after {
                    content: "Loading...";
                    display: inline-block;
                    margin-left: 8px;
                    color: #6c757d;
                    font-style: italic;
                    font-size: 0.8rem;
                }
            </style>
            <div class="breadcrumb-container">
                <nav aria-label="Database navigation breadcrumb">
                    <ol id="db-explorer-breadcrumb-list" class="breadcrumb" role="list">
                        ${this.isRootPath() ? 
                            `<li class="breadcrumb-item" role="listitem">
                                <a href="#" data-path="" role="link" aria-label="Navigate to database root">db</a><span class="segment-delimiter" aria-hidden="true">/</span>
                                ${this.renderBreadcrumbItems()}
                            </li>` : 
                            `<li class="breadcrumb-item" role="listitem">
                                <a href="#" data-path="" role="link" aria-label="Navigate to database root">db</a><span class="segment-delimiter" aria-hidden="true">/</span>
                            </li>
                            ${this.renderBreadcrumbItems()}`
                        }
                        <span class="breadcrumb-help" aria-hidden="true">(Use these links to change directory.)</span>
                    </ol>
                </nav>                
            </div>
        `;

        // Save references to key elements
        this.bucketSelect = this.shadowRoot.getElementById('db-explorer-breadcrumb-bucket-select');
        
        // Add event listeners to breadcrumb links
        this.shadowRoot.querySelectorAll('a').forEach(link => {
            link.addEventListener('click', this.handleLinkClick);
        });
        
        // Add event listener for bucket selection
        if (this.bucketSelect) {
            this.bucketSelect.addEventListener('change', this.handleBucketChange);
        }
    }
    
    /**
     * Handles bucket selection changes
     * Updates component state and dispatches a context-change event
     * 
     * @param {Event} event - The change event from bucket select
     */
    handleBucketChange(event) {
        this.selectedBucket = event.target.value;
        
        // Dispatch context-change event when bucket selection changes
        this.dispatchEvent(new CustomEvent('context-change', {
            detail: { 
                type: 'path',
                path: this.currentPath,
                bucket: this.selectedBucket
            },
            bubbles: true,
            composed: true
        }));
        
        // If not ".* (any)" bucket, refresh the component with the API call
        if (this.selectedBucket !== '.* (any)') {
            const url = this.host + 'api/list?path=' + 
                (this.currentPath ? `${this.currentPath}/${this.selectedBucket}` : this.selectedBucket);
                
            utils.callApi(url, "GET")
                .then(response => {
                    if (response.Status === "OK") {
                        // Just log success for now
                        console.log(`Bucket ${this.selectedBucket} context updated`);
                    }
                })
                .catch(error => {
                    console.error(`Error fetching bucket ${this.selectedBucket} contents:`, error);
                });
        }
    }

    /**
     * Generates the HTML for breadcrumb items based on path segments
     * 
     * @returns {string} HTML string for the breadcrumb items
     */
    renderBreadcrumbItems() {
        let html = '';
        let currentPath = '';

        // If we have no path segments, this is the root level
        // Display the bucket dropdown at the root level
        if (!this.pathSegments.length || (this.pathSegments.length === 1 && !this.pathSegments[0])) {
            // Root level navigation - show bucket dropdown
            return `<li class="breadcrumb-item active" role="listitem">
                <select id="db-explorer-breadcrumb-bucket-select" class="bucket-select" aria-label="Select root bucket">
                    ${this.renderBucketOptions()}
                </select>
            </li>`;
        }
        
        // Create breadcrumb items for each path segment
        for (let i = 0; i < this.pathSegments.length; i++) {
            const segment = this.pathSegments[i];
            if (!segment) continue;
            
            // Build the cumulative path
            currentPath = currentPath ? `${currentPath}/${segment}` : segment;

            // For the last item, include the bucket dropdown
            if (i === this.pathSegments.length - 1) {
                html += `<li class="breadcrumb-item active" role="listitem">
                    <a href="#" data-path="${currentPath}" role="link" aria-label="Navigate to ${currentPath}">${segment}</a><span class="segment-delimiter" aria-hidden="true">/</span>
                     <select id="db-explorer-breadcrumb-bucket-select" class="bucket-select" aria-label="Select directory context">
                        ${this.renderBucketOptions()}
                    </select>
                </li>`;
            } else {
                html += `<li class="breadcrumb-item" role="listitem">
                    <a href="#" data-path="${currentPath}" role="link" aria-label="Navigate to ${currentPath}">${segment}</a><span class="segment-delimiter" aria-hidden="true">/</span>
                </li>`;
            }
        }
        
        return html;
    }

    /**
     * Renders the bucket selection dropdown options
     * 
     * @returns {string} HTML string of option elements
     */
    renderBucketOptions() {
        return this.bucketList.map(bucket => 
            `<option value="${bucket}" ${bucket === this.selectedBucket ? 'selected' : ''} aria-label="Select bucket: ${bucket}">${bucket}</option>`
        ).join('');
    }

    /**
     * Handles click events on breadcrumb links
     * Dispatches a path-change event with the new path
     * 
     * @param {Event} event - The click event
     */
    handleLinkClick(event) {
        event.preventDefault();
        const path = event.target.getAttribute('data-path');
        
        // Dispatch custom event with the new path
        this.dispatchEvent(new CustomEvent('path-change', {
            detail: { path },
            bubbles: true,
            composed: true
        }));
    }
    
    // =====================================================================
    // PUBLIC API METHODS
    // =====================================================================
    
    /**
     * Set the breadcrumb's current path
     * Updates the component's path attribute and triggers a re-render
     * 
     * @param {string} path - The path to set
     */
    setPath(path) {
        this.setAttribute('path', path || '');
    }

    /**
     * Set the component's search mode
     * Controls whether the current segment is rendered as active or clickable
     * 
     * @param {boolean} isSearching - Whether the component is in search mode
     */
    setSearching(isSearching) {
        if (isSearching) {
            this.setAttribute('searching', '');
        } else {
            this.removeAttribute('searching');
        }
        this.render();
    }
    
    /**
     * Set the selected bucket for the dropdown
     * Updates dropdown selection and triggers change event
     * 
     * @param {string} bucket - The bucket name to select
     */
    setSelectedBucket(bucket) {
        if (this.bucketList.includes(bucket)) {
            this.selectedBucket = bucket;
            
            // Update select element if it exists
            if (this.bucketSelect) {
                this.bucketSelect.value = bucket;
                
                // Trigger change event to update UI
                const event = new Event('change');
                this.bucketSelect.dispatchEvent(event);
            }
        }
    }
    
    /**
     * Helper method to check if the current path is at the root level
     * 
     * @returns {boolean} True if at root level, false otherwise
     */
    isRootPath() {
        return !this.currentPath || this.currentPath === '' || 
            (this.pathSegments.length === 1 && !this.pathSegments[0]);
    }
    
    /**
     * Component lifecycle method called when the element is removed from the DOM
     * Cleans up event listeners to prevent memory leaks
     */
    disconnectedCallback() {
        // Remove event listeners when component is removed
        if (this.shadowRoot) {
            this.shadowRoot.querySelectorAll('a').forEach(link => {
                link.removeEventListener('click', this.handleLinkClick);
            });
            
            // Remove bucket select listener
            const bucketSelect = this.shadowRoot.getElementById('db-explorer-breadcrumb-bucket-select');
            if (bucketSelect) {
                bucketSelect.removeEventListener('change', this.handleBucketChange);
            }
        }
    }
}

window.customElements.define('db-explorer-breadcrumb', DbExplorerBreadcrumb);