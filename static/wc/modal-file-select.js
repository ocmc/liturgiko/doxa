import * as utils from "./utils.js";

class ModalFileSelect extends HTMLElement {
  // dispatches events modal-file-select:canceled
  //                   modal-file-select:selected
  //                   modal-file-select:template-invalidated // e.g. because it was deleted or parent dir was
  constructor() {
    // always call super() first
    super();
    console.log("static/wc/modal-file-select.js")
    this.addBtn = null;
    this.currentPath = [];
    this.data = null;
    this.divFooter = null;
    this.newItemName = "";
    this.selectedAddType = "";
    this.typeBlock = "bl.";
    this.typeBook = "bk.";
    this.typeDir = "dir";
    this.typeService = "se.";
    this.inlineRadioNewDir = null;
    this.inlineRadioNewBlock = null;
    this.inlineRadioNewBook = null;
    this.inlineRadioNewService = null;
    this.fallbackEnabled = false;
    this.inFallbackDir = false;
  }

  connectedCallback() {
    // Create component
    const component = document.createElement("div");
    component.className = "bootstrap-shadow-dialog";
    // Create shadowRoot
    const shadowRoot = this.attachShadow({mode: 'open'});
    // Set inner HTML
    shadowRoot.innerHTML = `
      <style> 
        @import url('static/css/bootstrap.min.css');
        @import url('static/img/bootstrap-icons.css');
      </style>
      <style>
       .modal {
             position: absolute;
             top: -3vh;
             left: 20px;
             display:block;
          /*margin-top: 10vh;*/
       }
       .modal-dialog {
         max-width: 75% !important;
       }
       .modal-header{
          text-transform: uppercase;
          color:#fff;
          background-color: #428bca;
        }
        .close {
            cursor: pointer;
        }
        .table {
            margin-left: 1px;
            margin-right: 1px;
        }
        .desc {
            margin-left: 1px;
            margin-right: 1px;
        }
        .bi-folder {
         padding-right: 10px;
         color: cornflowerblue;
        }
        .breadcrumb {
            cursor: pointer !important;
            color: #0d6efd !important;
            text-decoration: underline !important;            
        }
      </style>
      
      <div class="modal"  id="modal-dialog" data-bs-backdrop="static" data-bs-keyboard="False">
       <div class="modal-dialog shadow">
         <div class="modal-content">
           <div class="modal-header">
              <h4 class="modal-title">${this.title}</h4>
              <button type="button" class="close" id="closeBtn" data-dismiss="modal" aria-hidden="true">×</button>
           </div>
           <div class="modal-body">
             <div class="row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" id="modal-file-select-breadcrumbs">
                 </ol>
                </nav>
             </div>
             <div class="row">
              <table class="table table-striped">
                <thead><td>Item</td><td>Actions</td></thead>
                <tbody id="modal-file-select-tbody">
                </tbody>
              </table>
             </div>
           </div>
         <div id="divFooter" class="modal-footer">
         <div class="container">
          <div class="row">
            <div class="col col-6 align-self-center">
              <div class="input-group mb-3">
                <span class="input-group-text" id="newItemLabel"><i id="item-rename" class="bi-plus" style="color: cornflowerblue;"></i><button id="addBtn">Add</button></span>
                <input type="text"  id="newItemName" class="form-control" placeholder="Enter name..." aria-label="Itemname" aria-describedby="newItemLabel">
              </div>
            </div>
            <div class="col col-6 align-self-start">
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadioNewDir" value="option1">
              <label class="form-check-label" for="inlineRadioNewDir">Directory</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadioNewBlock" value="option2">
              <label class="form-check-label" for="inlineRadioNewBlock">Block</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadioNewBook" value="option3">
              <label class="form-check-label" for="inlineRadioNewBook">Book</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadioNewService" value="option4">
              <label class="form-check-label" for="inlineRadioNewService">Service</label>
            </div>
            </div>
            <div>${utils.getMessageSpan("", "Enter the name of the new item, select the type, e.g. directory, then click the Add button.")}</div>
          </div>
       </div><!-- /.modal-footer -->
        </div><!-- /.modal-content -->
       </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
     </div>
  `;
    shadowRoot.getElementById("modal-dialog").addEventListener("click", e => {
      const targetId = e.target.id;
      if (targetId === "closeBtn" || targetId === "cancelBtn") {
        this.dispatchEvent(this.cancelEvent);
      } else {
        if (e.target.hasAttribute("data-dir") && e.target.getAttribute("data-dir") === "false") {
          this.dispatchEvent(this.fileSelectedEvent);
          e.stopPropagation();
        } else { // it was a dir that was clicked or some inconsequential area of the modal
          e.stopPropagation();
        }
      }
    })
    if (this.path && this.path.length > 0) {
      this.currentPath = JSON.parse(this.path);
    }
    // set focus to close button
    shadowRoot.getElementById("closeBtn").focus();
    this.addBtn = shadowRoot.querySelector("#addBtn");
    this.divFooter = shadowRoot.querySelector("#divFooter");
    this.inlineRadioNewDir = shadowRoot.querySelector("#inlineRadioNewDir");
    this.inlineRadioNewBlock = shadowRoot.querySelector("#inlineRadioNewBlock");
    this.inlineRadioNewBook = shadowRoot.querySelector("#inlineRadioNewBook");
    this.inlineRadioNewService = shadowRoot.querySelector("#inlineRadioNewService");

    // add listeners
    this.addBtn.addEventListener('click', () => this.handleAdd());
    shadowRoot.querySelector("#modal-file-select-breadcrumbs").addEventListener('click', (event) => this.handleBreadCrumbClick(event));
    shadowRoot.querySelector("#modal-file-select-tbody").addEventListener('click', (event) => this.handleDirItemClick(event));
    this.inlineRadioNewDir.addEventListener('click', () => this.handleNewDir());
    this.inlineRadioNewBlock.addEventListener('click', () => this.handleNewBlock());
    this.inlineRadioNewBook.addEventListener('click', () => this.handleNewBook());
    this.inlineRadioNewService.addEventListener('click', () => this.handleNewService());

    this.newItemName = shadowRoot.querySelector("#newItemName");

    // Save copy of shadow root to component
    component._shadowRoot = shadowRoot;
    // Attach component to dom
    document.getElementsByTagName("body")[0].appendChild(component);

    this.fetchItemsInDir();

  }
  disconnectedCallback() {
    this.addBtn.removeEventListener('click', () => this.handleAdd());
    this.shadowRoot.querySelector("#modal-file-select-breadcrumbs").removeEventListener('click', (event) => this.handleBreadCrumbClick(event));
    this.shadowRoot.querySelector("#modal-file-select-tbody").removeEventListener('click', (event) => this.handleDirItemClick(event));
    this.inlineRadioNewDir.removeEventListener('click', () => this.handleNewDir());
    this.inlineRadioNewBlock.removeEventListener('click', () => this.handleNewBlock());
    this.inlineRadioNewBook.removeEventListener('click', () => this.handleNewBook());
    this.inlineRadioNewService.removeEventListener('click', () => this.handleNewService());
  }
  deselectAllRadio() {
    this.inlineRadioNewDir.checked = false;
    this.inlineRadioNewBlock.checked = false;
    this.inlineRadioNewBook.checked = false;
    this.inlineRadioNewService.checked = false;
  }
  fetchItemsInDir() {
    this.newItemName.value = ""
    const url = this.host
        + `api/lml/dir/browse?`
        + `path=` + JSON.stringify(this.currentPath)
    ;
    utils.callApi(url, "GET").then(response => {
      if (response.Status === "OK") {
        if (this.currentPath.length === 0) {
          this.currentPath.push("templates");
        }
        this.data = response;
        this.fallbackEnabled = response.FallbackEnabled;
        this.inFallbackDir = response.InFallbackDir;
        if (this.fallbackEnabled && this.inFallbackDir) {
          this.divFooter.classList.add("d-none");
        } else {
          this.divFooter.classList.remove("d-none");
        }
        this.renderData();
      } else {
        alert(response.Message)
      }
    }).catch(error => {
      alert(error);
    });
  }
  handleAdd() {
    if (!this.newItemName.value) {
      if (this.selectedAddType && this.selectedAddType.length > 0) {
        switch(this.selectedAddType) {
          case this.typeBlock:
            alert("You must enter the name of the block you want to add.");
            break;
          case this.typeBook:
            alert("You must enter the name of the book you want to add.");
            break;
          case this.typeDir:
            alert("You must enter the name of the directory you want to add.");
            break;
          case this.typeService:
            alert("You must enter the name of the service you want to add.");
            break;
          default:
            alert("You must enter the name of the item you want to add.");
        }
      } else {
        alert("You must enter the name of the item you want to add.");
        return;
      }
      return;
    }
    if (this.selectedAddType && this.selectedAddType.length > 0) {
      if (this.selectedAddType === this.typeDir) {
        this.requestNewDir();
      } else {
        this.saveNewTemplate(this.newItemName.value, this.selectedAddType);
      }
    } else {
      alert("You must select the type of item you want to add.")
    }
  }
  handleNewDir() {
    this.selectedAddType = this.typeDir;
  }
  requestNewDir() {
    if (!this.newItemName.value) {
      alert("You must enter the name of the new directory");
      this.deselectAllRadio();
      return;
    }
    const url = this.host
        + `api/lml/dir/new`
        + `?path=` + JSON.stringify(this.currentPath)
        + `&dir=` + this.newItemName.value
    ;

    utils.callApi(url, "PUT").then(response => {
      if (response.Status === "OK") {
        this.data = response;
        this.newItemName.value = "";
        this.deselectAllRadio();
        this.renderData();
      } else {
        alert(response.Message);
      }
    }).catch(error => {
      alert(error);
    });
  }
  // isCurrentFile is used to block action icons for a file that is already open
  isCurrentFile(fileName) {
    let macPath = this.currentPath.join("/") + `/${fileName}`;
    let winPath = this.currentPath.join("\\") + `\\${fileName}`;
    // TODO: this.currentFile is a property and is not used by code-editor.  So, what gives?
    return this.currentFile.endsWith(macPath) || this.currentFile.endsWith(winPath);
  }
  handleNewBlock() {
    this.selectedAddType = this.typeBlock;
  }
  handleNewBook() {
    this.selectedAddType = this.typeBook;
  }
  handleNewService() {
    this.selectedAddType = this.typeService;
  }
  saveNewTemplate (name, type) {
    // normalize the template file name
    if (! name.startsWith(type)) {
      name = type + name;
    }
    if (! name.endsWith(".lml")) {
      name += ".lml";
    }
    if (type === "se.") {
      // does the name include /mxx/dxx ?
      if (name.includes(".m") && name.includes(".d")) {
        let parts = name.split(".");
        // make sure the month and day segments have a length of 3
        if (parts[1].length !== 3 || parts[2].length !== 3) {
          alert(`invalid service template name: month and day must be three characters, (m or d), and a two digit number,  e.g. 'li' in se.m01.d02.li.lml`);
          return
        } else {
          if (parts[1].slice(0,1) !== "m") {
            alert(`invalid service template name: month number must start with the letter m,  e.g. 'li' in se.m01.d02.li.lml`);
            return
          }
          if (parts[2].slice(0,1) !== "d") {
            alert(`invalid service template name: day number must start with the letter d,  e.g. 'li' in se.m01.d02.li.lml`);
            return
          }
          // ensure these are valid month and day numbers
          let month = +parts[1].slice(1);
          let day = +parts[2].slice(1);
          if (month > 0 && month < 13) {
            if (day > 0 && day < 32) {
              // all ok
            } else {
              alert(`invalid service template name: day must be a number between 1 and 31,  e.g. 'li' in se.m01.d02.li.lml`);
              return
            }
          } else {
            alert(`invalid service template name: month must be a number between 1 and 12,  e.g. 'li' in se.m01.d02.li.lml`);
            return
          }
        }
        if (parts[parts.length - 2].startsWith("d")) { // could be a service type with a d?
          if (parts[parts.length - 3].startsWith("d")) { // so see if it is preceded by the day segment
            // assume ok
          } else {
            alert(`invalid service template name: ${name} must include service type, e.g. 'li' in se.m01.d02.li.lml`);
            return
          }
        }
      } else {
        alert(`invalid service template name: ${name} must include month and day, e.g. se.m01.d02.li.lml`);
        return
      }
      // does the name include the service type?
    }
    // clone the currentPath and add the template file name to it
    let idArray = [];
    this.currentPath.forEach((item) => {
      if (item !== "templates") {
        idArray.push(item)
      }
    });
    idArray.push(name);
    this.src = idArray.join("/");
    // call the api to create the file and save the contents
    const url = this.host
        + `api/lml?`
        + `src=` + encodeURIComponent(JSON.stringify(idArray))
        + `&text=`// + encodeURIComponent(this.getContent(type, name))
    ;
    utils.callApi(url, "PUT").then(response => {
      if (response.Status === "302") {
        alert(response.Message);
        this.newItemName.value = "";
        this.deselectAllRadio();
      } else {
        if (response.Status === "OK") {
          this.fetchItemsInDir();
        } else {
          alert(response.Message);
        }
      }
    }).catch(error => {
      alert(error);
    });
  }
  // getContent returns the first  lines of a template, filled in based on the template type
  getContent(type, name) {
    let text = "";
    text = `id = "${this.src}"`;

    switch (type) {
      case this.typeBlock: {
        text += `\ntype = "block"\nstatus = "draft"\n`;
        break;
      }
      case this.typeBook: {
        let parts = name.split(".");
        let newName = ""
        parts.forEach((item) => {
          if (item === "bk" || item === "lml") {
            return;
          }
          if (newName.length > 0) {
            newName = newName + ","
          }
          newName = newName + item
        });
        text += `\ntype = "book"\nstatus = "draft"\nindexTitleCodes = "${newName}"\n`;
        break;
      }
      case this.typeService: {
        text += `\ntype = "service"\noffice = ""\nstatus = "draft"\nmonth = \nday = \n`;
        break;
      }
    }
    return text;
  }
  static get observedAttributes() {
    return ['path', 'src', 'title'];
  }

  attributeChangedCallback(name, oldVal, newVal) {
    if (name === "path") {
      if (newVal && newVal.length > 0 && newVal !== oldVal) {
        this.currentPath = JSON.parse(newVal);
        this.fetchItemsInDir();
      }
    }
  }

  adoptedCallback() {
  }

  renderData() {
    if (this.data === null) {
      return;
    }
    const breadCrumbs = this.shadowRoot.querySelector("#modal-file-select-breadcrumbs")
    const table = this.shadowRoot.querySelector("#modal-file-select-tbody")

    // add breadcrumbs
    breadCrumbs.innerHTML = ``;
    let templateDirFound = false
    let dirSegments = [];
    this.data.Path.forEach((item) => {
      dirSegments.push(item);
      const li = document.createElement('li');
      li.classList.add("breadcrumb-item")
      if (item === "templates") {
        templateDirFound = true;
      }
      if (templateDirFound) {
        li.innerHTML = `<a href="#" data-path='${JSON.stringify(dirSegments)}'>${item}</a>`
      } else {
        li.innerHTML = `<span data-path='${JSON.stringify(dirSegments)}'>${item}</span>`
      }
      breadCrumbs.appendChild(li)
    });
    // add dir contents
    table.innerHTML = ``; // clear the list
    if (!this.data.Values) { // we have an empty directory
      return;
    }
    this.data.Values.forEach((item, index) => {
      if (item.Name.startsWith(".")) { // e.g. .git
        return;
      }
      if (! item.IsDir) {
        if (this.fileSuffix && this.fileSuffix.length > 0) {
          if (!item.Name.endsWith(this.fileSuffix)) {
            return
          }
        }
      }
      const tr = document.createElement('tr');
      const itemTd = document.createElement("td");
      const actionTd = document.createElement("td");
      if (item.IsDir) {
        itemTd.innerHTML = `<i class="bi-folder"></i><a href="#" data-dir="true">${item.Name}</a>`;
        if (item.Name === "primary" || item.Name === "fallback") {
          // no action allowed
          actionTd.innerHTML = "";
        } else {
          if (this.fallbackEnabled && this.inFallbackDir) {
            // no action allowed
            actionTd.innerHTML = "";
          } else {
            actionTd.innerHTML = `<i id="item-rename-${index}" class="bi-bootstrap-reboot" data-dir="true" data-action="rename" data-item="${item.Name}" style="color: cornflowerblue;"></i>&nbsp;&nbsp;&nbsp;<i id="item-delete-${index}" data-dir="true" data-action="delete"  data-item="${item.Name}" class="bi-trash" style="color: cornflowerblue;"></i>`;
          }
        }
      } else {
        itemTd.innerHTML = `<a href="#" data-dir="false">${item.Name}`;
        if (! this.isCurrentFile(item.Name)) {
          if (this.fallbackEnabled && this.inFallbackDir) {
            // no action allowed
            actionTd.innerHTML = "";
          } else {
            actionTd.innerHTML = `<i id="item-rename-${index}" class="bi-bootstrap-reboot" data-dir="false" data-action="rename" data-item="${item.Name}" style="color: cornflowerblue;"></i>&nbsp;&nbsp;&nbsp;<i id="item-delete-${index}" data-dir="false" data-action="delete"  data-item="${item.Name}" class="bi-trash" style="color: cornflowerblue;"></i>`;
          }
        }
      }
      tr.appendChild(itemTd);
      tr.appendChild(actionTd);
      table.appendChild(tr)
    });
  }

  handleBreadCrumbClick(event) {

    const id = event.target.getAttribute("data-path");
    if (! id) {
      return;
    }
    const idArray = JSON.parse(id);
    let templateDirFound = false;
    this.currentPath = [];
    idArray.forEach((item) => {
      if (item === "templates" || item === "fallback" || item === "primary") {
        templateDirFound = true;
      }
      if (templateDirFound) {
        this.currentPath.push(item)
      }
    });
    this.fetchItemsInDir();
  }
  handleDirItemClick(event) {
    const isDir = event.target.getAttribute("data-dir")
    const value = event.target.innerText;
    if (event.target.hasAttribute("data-action")) {
      const item = event.target.getAttribute("data-item");
      switch (event.target.getAttribute("data-action")) {
        case "rename": {
          this.handleRename();
          break;
        }
        case "delete": {
          event.stopPropagation();
          if (isDir === "true") {
            this.handleDirDelete(item);
          } else {
            this.handleTemplateDelete(item);
          }
          break;
        }
      }
      return;
    }
    // if we get here, we are just drilling into a dir or opening a template...
    if (isDir === "true") {
      if (this.currentPath[this.currentPath.length-1] !== value) {
        this.currentPath.push(value)
      }
      this.fetchItemsInDir();
    } else {
      this.src = "";
      let srcSegments = [];
      this.currentPath.forEach((item) => {
        // when the caller uses this.src to call the doxa web api,
        // it will append this.src to the templates path, so we
        // will leave it off here.
        if (item !== "templates") {
          srcSegments.push(item);
        }
      });
      srcSegments.push(value);
      this.src = JSON.stringify(srcSegments)
    }
  }

  handleRename() {
    let path = [];
    this.currentPath.forEach((item, index) => {
      if (index === 0 && item === "templates") {
        // ignore
      } else {
        path.push(item);
      }
    });
    path.push(dir);
    if (confirm(`OK to rename ${path.join("/")}?`)) {
      const url = this.host
          + `api/lml/dir/rename`
          + `?path=` + JSON.stringify(path)
      ;
      utils.callApi(url, "PUT").then(response => {
        if (response.Status === "OK") {
          this.fetchItemsInDir();
        } else {
          alert(response.Message);
        }
      }).catch(error => {
        alert(error);
      });
    }
  }
  handleDirDelete(dir) {
    if (dir === "primary" || dir === "fallback") {
      alert("You are not allowed to delete the primary or fallback directory.")
      return;
    }
    let path = [];
    this.currentPath.forEach((item, index) => {
      if (index === 0 && item === "templates") {
        // ignore
      } else {
        path.push(item);
      }
    });
    path.push(dir);
    if (confirm(`OK to delete ${path.join("/")}?`)) {
      const url = this.host
          + `api/lml/dir/delete`
          + `?path=` + JSON.stringify(path)
      ;
      utils.callApi(url, "PUT").then(response => {
        if (response.Status === "OK") {
          this.fetchItemsInDir();
        } else {
          alert(response.Message);
        }
      }).catch(error => {
        alert(error);
      });
    }
  }

  handleTemplateDelete(templateName) {
    let path = [];
    this.currentPath.forEach((item, index) => {
      if (index === 0 && item === "templates") {
        // ignore
      } else {
        path.push(item);
      }
    });
    path.push(templateName);
    if (confirm(`OK to delete ${path.join("/")}?`)) {
      const url = this.host
          + `api/lml/delete?`
          + `src=` + JSON.stringify(path)
      ;
      utils.callApi(url, "PUT").then(response => {
        if (response.Status === "OK") {
          this.fetchItemsInDir();
        } else {
          alert(response.Message);
        }
      }).catch(error => {
        alert(error);
      });
      this.src = "";
    }
  }
  // getters / setters
  get fileSuffix() {
    return this.getAttribute("fileSuffix");
  }
  set fileSuffix(value) {
    this.setAttribute("fileSuffix", value);
  }
  get host() {
    return this.getAttribute("host");
  }
  set host(value) {
    this.setAttribute("host", value);
  }
  get path() {
    return this.getAttribute("path");
  }
  set path(value) {
    this.setAttribute("path", value);
  }

  get currentFile() {
    return this.getAttribute("currentFile");
  }
  set currentFile(value) {
    this.setAttribute("currentFile", value);
  }
  // gets set when user selects a file rather than a directory.
  // Is then available to caller.
  get src() {
    return this.getAttribute("src");
  }
  set src(value) {
    this.setAttribute("src", value);
  }
  // custom events
  cancelEvent = new CustomEvent('modal-file-select:canceled', {
    detail: {
      name: 'modal-file-select:canceled'
    },
    bubbles: true
  });
  fileSelectedEvent = new CustomEvent('modal-file-select:selected', {
    detail: {
      name: 'modal-file-select:selected'
    },
    bubbles: true
  });
  templateInvalidatedEvent = new CustomEvent('modal-file-select:templateInvalidated', {
    detail: {
      name: 'modal-file-select:templateInvalidated'
    },
    bubbles: true
  });

}

window.customElements.define('modal-file-select', ModalFileSelect);