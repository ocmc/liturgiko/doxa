class ModalConfirm extends HTMLElement {
  constructor() {
    // always call super() first
    super();
    console.log("static/wc/modal-confirm.js")
    this.titleDiv = null;
    this.msg1Div = null;
    this.msg2Row = null;
    this.msg2Div = null;
    this.msg3Row = null;
    this.msg3Div = null;
    this.okButton = null;
  }

  connectedCallback() {
    // Create component
    const component = document.createElement("div");
    component.className = "bootstrap-shadow-dialog";
    // Create shadowRoot
    const shadowRoot = this.attachShadow({mode: 'open'});
    // Set inner HTML
    shadowRoot.innerHTML = `
      <style> 
		    @import url('static/css/bootstrap.min.css');
      </style>
      <style>
       .modal {
          display:block;
          position: fixed;
          top: 200px;
       }
       .modal-header{
          text-transform: uppercase;
          color:#fff;
          background-color: #428bca;
        }
      </style>
      
      <div class="modal"  id="modal-dialog" data-bs-backdrop="static" data-bs-keyboard="False">
       <div class="modal-dialog shadow">
         <div class="modal-content">
           <div class="modal-header">
              <h4 id = "title" class="modal-title">${this.title}</h4>
              <button type="button" class="close" id="closeBtn" data-dismiss="modal" aria-hidden="true">×</button>
           </div>
           <div class="modal-body">
             <div class="row">
               <div style='font-size: 20px;'><div id="msg1" class="alert" role="alert">${this.msg1}</div></div>
             </div>
             <div id="msg2row" class="row">
               <div style='font-size: 20px;'><div id="msg2" class="alert" role="alert">${this.msg2}</div></div>
             </div>
             <div id="msg3row" class="row">
               <div style='font-size: 20px;'><div id="msg3" class="alert" role="alert">${this.msg3}</div></div>
             </div>
           </div>
           <div class="modal-footer">
              <button type="button" class="btn btn-outline-primary" data-dismiss="modal"  id="cancelBtn">${this.cancelBtn}</button>
              <button type="button" class="btn btn-danger" id="okBtn">${this.okBtn}</button>
           </div>
        </div><!-- /.modal-content -->
       </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
     </div>
  `;
    this.titleDiv = shadowRoot.getElementById("title");
    this.msg1Div = shadowRoot.getElementById("msg1");
    this.setMessage("msg1", this.msg1)
    this.msg2Row = shadowRoot.getElementById("msg2Row");
    this.msg2Div = shadowRoot.getElementById("msg2");
    this.setMessage("msg2", this.msg2)
    this.msg3Row = shadowRoot.getElementById("msg3Row");
    this.msg3Div = shadowRoot.getElementById("msg3");
    this.setMessage("msg3", this.msg3)
    this.okButton = shadowRoot.getElementById("okBtn");
    shadowRoot.getElementById("modal-dialog").addEventListener("click", e => {
      const targetId = e.target.id;
      if (targetId === "closeBtn" || targetId === "cancelBtn" || targetId === "okBtn") {
        // allow propagation
      } else {
        e.stopPropagation();
      }
    })
    // set focus to cancel button
    shadowRoot.getElementById("cancelBtn").focus();

    // Save copy of shadow root to component
    component._shadowRoot = shadowRoot;
    // Attach component to dom
    document.getElementsByTagName("body")[0].appendChild(component);
  }

  disconnectedCallback() {
  }

  setMessage(name, newVal) {
    switch (name) {
      case "title": {
        if (this.titleDiv) {
          this.titleDiv.innerText = newVal;
        }
        break;
      }
      case "msg1": {
        if (this.msg1Div) {
          this.msg1Div.innerText = newVal;
        }
        break;
      }
      case "msg2": {
        if (this.msg2Div) {
          this.msg2Div.innerText = newVal;
        }
        break;
      }
      case "msg3": {
        if (this.msg3Div) {
          this.msg3Div.innerText = newVal;
        }
        break;
      }
      case "okBtn": {
        alert(`modal-confirm.attributeChangedCallback okBtn = ${newVal}`)
        this.okButton.value = newVal;
        break;
      }

    }

  }
  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
    if (oldVal === null) {
      return;
    }
    if (oldVal !== newVal) {
      this.setAttribute(name, newVal)
    }
  }

  adoptedCallback() {
  }

  static get observedAttributes() {
    return ['cancelBtn', 'okBtn', 'msg1', 'msg2', 'msg3', 'passThrough', 'title', ];
  }

  get cancelBtn() {
    return this.getAttribute("cancelBtn");
  }
  set cancelBtn(value) {
    this.setAttribute("cancelBtn", value);
  }
  get msg1() {
    return this.getAttribute("msg1");
  }
  set msg1(value) {
    this.setAttribute("msg1", value);
  }
  get msg2() {
    return this.getAttribute("msg2");
  }
  set msg2(value) {
    this.setAttribute("msg2", value);
  }
  get msg3() {
    return this.getAttribute("msg3");
  }
  set passThrough(value) {
    this.setAttribute("passThrough", value);
  }
  get passThrough() {
    return this.getAttribute("passThrough");
  }
  get okBtn() {
    return this.getAttribute("okBtn");
  }
  set okBtn(value) {
    this.setAttribute("okBtn", value);
  }
  get title() {
    return this.getAttribute("title");
  }
  set title(value) {
    this.setAttribute("title", value);
  }

}

window.customElements.define('modal-confirm', ModalConfirm);