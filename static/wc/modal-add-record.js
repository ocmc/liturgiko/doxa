class ModalAddRecord extends HTMLElement {
  constructor() {
    // always call super() first
    super();
    console.log("static/wc/modal-add-record.js")
  }

  connectedCallback() {
    // Create component
    const component = document.createElement("div");
    component.className = "bootstrap-shadow-dialog";
    // Create shadowRoot
    const shadowRoot = this.attachShadow({mode: 'open'});
    // Set inner HTML
    shadowRoot.innerHTML = `
      <style> 
		    @import url('static/css/bootstrap.min.css');
      </style>
      <style>
      .control-label {
        color: #5bc0de;
      }
       .modal {
          display:block;
       }
       .modal-header{
          text-transform: uppercase;
          color:#fff;
          background-color: #428bca;
        }
       .modal-body {
           padding: 40px !important;
       }

      </style>
      
      <div class="modal"  id="modal-dialog" data-bs-backdrop="static" data-bs-keyboard="False">
       <div class="modal-dialog shadow">
         <div class="modal-content">
           <div class="modal-header">
              <h4 class="modal-title">${this.title}</h4>
              <button type="button" class="close" id="closeBtn" data-dismiss="modal" aria-hidden="true">×</button>
           </div>
           <div class="modal-body">
                <div class="row">
                    <div class="control-label">Key</div>
                    <input id="recordKey" type="text" class="form-control" tabindex="1">   
                </div>
                <div class="row">
                    <div class="control-label">Value</div>
                    <textarea id="recordValue" type="text" class="form-control" tabindex="2"></textarea>
                </div>
                <div class="row">
                    <div id="idRow" class="alert alert-light control-label" role="alert">
                        ID: ${this.recPath}:<span id="recKeyAlert">${this.recKey}</span>
                    </div>
                </div>
           </div>
           <div class="modal-footer">
              <button type="button" class="btn btn-outline-primary" data-dismiss="modal"  id="cancelBtn" tabindex="4">${this.cancelBtn}</button>
              <button type="button" class="btn btn-primary" id="okBtn" disabled  tabindex="3">${this.okBtn}</button>
           </div>
        </div><!-- /.modal-content -->
       </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
     </div>
  `;
    shadowRoot.getElementById("modal-dialog").addEventListener("click", e => {
      const targetId = e.target.id;
      if (targetId === "closeBtn" || targetId === "cancelBtn" || targetId === "okBtn") {
        // allow propagation
      } else {
        e.stopPropagation();
      }
    })
    // keyup recordKey
    shadowRoot.getElementById("recordKey").addEventListener('keyup', ev => {
      ev.stopPropagation();
      const val = shadowRoot.getElementById("recordKey").value;
      let noPunctuation = val.replace(/[,\/#!$%\^&\*;:{}=\-`~()]/g,"");
      let finalString = noPunctuation.replaceAll(" ","");
      if (this.recPath.endsWith("build/pages/titles/map")) {
        finalString = finalString.toLowerCase();
      }
      this.setAttribute("recKey", finalString);
      shadowRoot.getElementById("recKeyAlert").innerText = finalString;
      this.toggleAddButton();
    })
    // keyup recordValue
    shadowRoot.getElementById("recordValue").addEventListener('keyup', ev => {
      ev.stopPropagation();
      const id = this.recPath + ":" + this.recKey;
      const val = shadowRoot.getElementById("recordValue").value;
      this.setAttribute("recValue", val);
      if (val.startsWith("@") && id === val.slice(1)) {
        shadowRoot.getElementById("idRow").innerText = "a record can't redirect back to itself";
        this.disableAddButton();
      } else {
        this.toggleAddButton();
      }
    })

    // set focus to first input
    shadowRoot.getElementById("recordKey").focus();

    // Save copy of shadow root to component
    component._shadowRoot = shadowRoot;
    // Attach component to dom
    document.getElementsByTagName("body")[0].appendChild(component);
  }

  disconnectedCallback() {
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
  }

  adoptedCallback() {
  }

  toggleAddButton() {
    const btn = this.shadowRoot.getElementById("okBtn")
    btn.disabled = !(this.shadowRoot.getElementById("recordKey").value.length > 0
        && this.shadowRoot.getElementById("recordValue").value.length > 0);
  }
  disableAddButton() {
    const btn = this.shadowRoot.getElementById("okBtn")
    btn.disabled = true;
  }

  get cancelBtn() {
    return this.getAttribute("cancelBtn");
  }
  set cancelBtn(value) {
    this.setAttribute("cancelBtn", value);
  }
  get recPath() {
    return this.getAttribute("recPath");
  }
  set recPath(value) {
    this.setAttribute("recPath", value);
  }
  get recKey() {
    return this.getAttribute("recKey");
  }
  set recKey(value) {
    this.setAttribute("recKey", value);
  }
  get recValue() {
    return this.getAttribute("recValue");
  }
  set recValue(value) {
    this.setAttribute("recValue", value);
  }

  get okBtn() {
    return this.getAttribute("okBtn");
  }
  set okBtn(value) {
    this.setAttribute("okBtn", value);
  }
  get title() {
    return this.getAttribute("title");
  }
  set title(value) {
    this.setAttribute("title", value);
  }

}

window.customElements.define('modal-add-record', ModalAddRecord);