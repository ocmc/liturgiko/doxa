class ModalIconHelp extends HTMLElement {
  constructor() {
    // always call super() first
    super();
    console.log("static/wc/icon-help.js")
  }

  connectedCallback() {
    // Create component
    const component = document.createElement("div");
    component.className = "bootstrap-shadow-dialog";
    // Create shadowRoot
    const shadowRoot = this.attachShadow({mode: 'open'});
    // Set inner HTML
    shadowRoot.innerHTML = `
      <style> 
        @import url('static/css/bootstrap.min.css');
        @import url('static/img/bootstrap-icons.css');
      </style>
      <style>
       .modal {
          display:block;
          top: 3em !important;
       }
       .modal-header{
          text-transform: uppercase;
          color:#fff;
          background-color: #428bca;
        }
        .close {
            cursor: pointer;
        }
        .table {
            margin-left: 1px;
            margin-right: 1px;
        }
        .desc {
            margin-left: 1px;
            margin-right: 1px;
        }
        ol, ul {
            padding-top: 20px;
            margin-left: 2vw;
        }
      </style>
      
      <div class="modal"  id="modal-dialog" data-bs-backdrop="static" data-bs-keyboard="False">
       <div class="modal-dialog modal-lg shadow">
         <div class="modal-content">
           <div class="modal-header">
              <h4 class="modal-title">${this.title}</h4>
              <button type="button" class="close" id="closeBtn" data-dismiss="modal" aria-hidden="true">×</button>
           </div>
           <div class="modal-body">
             <div class="row desc">
                 ${this.firstDivText}
            </div>
             <div class="row">
                <div class="col col-1"></div>
                <div class="col col-10">
                    <table class="table table-striped">
             <thead>
                <th>Icon</th>
                <th>Description</th>
             </thead>
             <tbody id="icon-help-tbody"></tbody>
             </table>
                </div>
                <div class="col col-1"></div>
             </div>
             <div class="row desc">
                 ${this.lastDivText}
            </div>
           </div>
         <div class="modal-footer">
          <button type="button" class="close" id="cancelBtn" data-dismiss="modal" aria-hidden="true">×</button>
       </div><!-- /.modal-footer -->
        </div><!-- /.modal-content -->
       </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
     </div>
  `;
    shadowRoot.getElementById("modal-dialog").addEventListener("click", e => {
      const targetId = e.target.id;
      if (targetId === "closeBtn" || targetId === "cancelBtn") {
        // allow propagation
      } else {
        e.stopPropagation();
      }
    })
    // set focus to close button
    shadowRoot.getElementById("closeBtn").focus();

    // Save copy of shadow root to component
    component._shadowRoot = shadowRoot;
    // Attach component to dom
    document.getElementsByTagName("body")[0].appendChild(component);
  }

  disconnectedCallback() {
  }

  static get observedAttributes() {
    return ['data'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
    if (name === "data") {
      if (newVal.length > 0) {
        const theData = JSON.parse(newVal);
        const table = this.shadowRoot.getElementById('icon-help-tbody');
        // clear rows
        while (table.firstChild) {
          table.removeChild(table.firstChild);
        }
        theData.forEach(item => {
          const tr = document.createElement('tr');
          // add icon
          const tdIcon = document.createElement('td');
          const icon = document.createTextNode(item.icon);
          if (item.icon.startsWith("bi-")) {
            tdIcon.innerHTML = `<i class="${item.icon}" style="color: cornflowerblue;"/>`;
          } else { // text only
            tdIcon.innerHTML = `<i style="color: cornflowerblue;">${item.icon}</i>`;
          }
          tr.appendChild(tdIcon);
          // add desc
          const tdDesc = document.createElement('td');
          const desc = document.createTextNode(item.desc);
          tdDesc.appendChild(desc);
          tr.appendChild(tdDesc);
          // add row to table
          table.appendChild(tr);
        });
      }
    }
  }

  adoptedCallback() {
  }

  get firstDivText() {
    return this.getAttribute("firstDivText");
  }
  set firstDivText(value) {
    this.setAttribute("firstDivText", value);
  }
  get lastDivText() {
    return this.getAttribute("lastDivText");
  }
  set lastDivText(value) {
    this.setAttribute("lastDivText", value);
  }
  get title() {
    return this.getAttribute("title");
  }
  set title(value) {
    this.setAttribute("title", value);
  }

}

window.customElements.define('modal-icon-help', ModalIconHelp);