// SseClient displays messages sent by the Doxa server, using the Server Sent Event protocol.
// The go code is in pkg/ssesvr
class SseClient extends HTMLElement {
  constructor() {
    super();
    console.log("static/wc/sse-client.js")
    this.shadowDOM = this.attachShadow({mode: 'open'});
    this.log = {}
    this.evtSource = null;
  }

  connectedCallback() {
    this.shadowDOM.innerHTML = `
    <style> 
      @import url('static/css/bootstrap.min.css');
      @import url('static/img/bootstrap-icons.css');
    </style>
    <style>
      .control-label {
        color: #5bc0de;
      }
      div.scroll {
        width: 50vw;
        height: 150px;
        overflow-x: hidden;
        overflow-y: auto;
        padding: 20px;
      }
      .log {
        font-family:courier, monospace;
        font-size: 0.25em !important;
        background-color: black;
        color: white;
        padding: 10px 10px 10px 10px;
        max-height: 50vw;
      }
    </style>
    <h3 style="text-align: center;">SSE: ${this.title}</h3>
    <div class="row p-1">
        <div class="col col-1"></div>
        <div class="col col-10">
            <div id="log" class="log scroll">
            </div>
        </div>
        <div class="col col-1"></div>
    </div>
    `
    this.log = this.querySelector("#log");
//    this.setEventSource();

    this.evtSource = new EventSource(this.endpoint);
    console.log(`set up EventSource for ${this.endpoint}`)
    this.evtSource.onmessage = function(ev) {
      console.log(ev.data)
    };
    this.evtSource.onopen = e => {
      console.log(e);
    }
    this.evtSource.addEventListener('ping', e => {
      console.log(e);
    });
    // clear events
    this.evtSource.addEventListener("clear", (e) => {
      console.log("got clear event")
    })
//    this.evtSource.addEventListener("clear", (ev) => this.handleClearEvent(ev));
    // generator events
    this.evtSource.addEventListener("gm", (ev) => this.handleGeneratorEvent(ev));
    // syncher events
    this.evtSource.addEventListener("sm", (ev) => this.handleSynchEvent(ev));
  }

  disconnectedCallback() {
    this.evtSource.close();
    // clear events
    this.evtSource.addEventListener("clear", (e) => {
      console.log("got clear event")
    })
//    this.evtSource.removeEventListener("clear", (ev) => this.handleClearEvent(ev));
    // generator events
    this.evtSource.removeEventListener("gm", (ev) => this.handleGeneratorEvent(ev));
    // syncher events
    this.evtSource.removeEventListener("sm", (ev) => this.handleSynchEvent(ev));
  }

  // setEventSource() {
  //   this.evtSource = new EventSource(this.endpoint);
  //   // clear events
  //   this.evtSource.addEventListener("clear", (event) => {
  //     this.log.innerHTML = "";
  //   });
  //   // generator events
  //   this.evtSource.addEventListener("gm", (event) => {
  //     console.log(JSON.stringify(event))
  //     const newElement = document.createElement("div");
  //     newElement.innerText = event.data;
  //     this.log.appendChild(newElement);
  //   });
  //   // syncher events
  //   this.evtSource.addEventListener("sm", (event) => {
  //     console.log(JSON.stringify(event))
  //     const newElement = document.createElement("div");
  //     newElement.innerText = event.data;
  //     this.log.appendChild(newElement);
  //   });
  // }

  handleClearEvent(ev) {
    console.log(JSON.stringify(ev))
      this.log.innerHTML = "";
  }
  handleGeneratorEvent(ev) {
    console.log(JSON.stringify(ev))
    const newElement = document.createElement("div");
    newElement.innerText = ev.data;
    this.log.appendChild(newElement);
  }
  handleSynchEvent(ev) {
    console.log(JSON.stringify(ev))
    const newElement = document.createElement("div");
    newElement.innerText = ev.data;
    this.log.appendChild(newElement);
  }
  static get observedAttributes() {
    return ['endpoint', 'title'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
    switch (name) {
      case "endpoint": {
          if (oldVal && oldVal !== newVal) {
            this.endpoint = newVal;
          }
      }
    }
  }

  adoptedCallback() {
  }

  get endpoint() {
    return this.getAttribute("endpoint");
  }

  set endpoint(value) {
    this.setAttribute("endpoint", value);
  }
  get title() {
    return this.getAttribute("title");
  }

  set title(value) {
    this.setAttribute("title", value);
  }
}
window.customElements.define('sse-client', SseClient);