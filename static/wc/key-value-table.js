class KeyValueTable extends HTMLElement {
  // based on code from https://codepen.io/cfjedimaster/pen/LYeEJRb
  // and https://www.taniarascia.com/front-end-tables-sort-filter-paginate/
  // TODO: [ ] implement the column filters, [ ] only show paginator if data exceeds page size

  /**
   * Example usage
   *
   *            <div class="row">
   *                 <key-value-table
   *                     id="the-key-value-table"
   *                     data=""
   *                     config="">
   *                 </key-value-table>
   *            </div>
   *
   *     const kvt = shadowRoot.getElementById("the-key-value-table")
   *     let fakeData = `[{"name":"Fluffy","age":9,"breed":"calico","gender":"male"},{"name":"Luna","age":10,"breed":"long hair","gender":"female"},{"name":"Cracker","age":8,"breed":"fat","gender":"male"},{"name":"Pig","age":6,"breed":"calico","gender":"female"},{"name":"Robin","age":7,"breed":"long hair","gender":"male"},{"name":"Sammy","age":13,"breed":"fat","gender":"male"},{"name":"Aliece","age":9,"breed":"long hair","gender":"female"},{"name":"Mehatable","age":5,"breed":"calico","gender":"female"},{"name":"Scorpia","age":6,"breed":"long hair","gender":"female"},{"name":"Zoomies","age":1,"breed":"fat","gender":"male"},{"name":"Zues","age":5,"breed":"long hair","gender":"male"},{"name":"Lord Kittybottom","age":9,"breed":"calico","gender":"male"},{"name":"Princess Furball","age":5,"breed":"calico","gender":"female"},{"name":"Delerium","age":4,"breed":"fat","gender":"female"}]`
   *     kvt.setAttribute("data", fakeData);
   *     let config = {};
   *     config.cols = [
   *       {property: "name", label: "Name", width: "col-3"},
   *       {property: "age", label: "Age", width: "col-2"},
   *       {property: "breed", label: "Breed", width: "col-3"},
   *       {property: "gender", label: "Gender", width: "col-2"},
   *     ];
   *     config.actions = [
   *       {name: "actionCopyID", icon: "", text: "ID", style: "color: cornflowerblue; font-style: italic;"},
   *       {name: "actionEdit", icon: "bi-pencil", text: "", style: "color: cornflowerblue;"},
   *       {name: "actionExport", icon: "bi-upload", text: "", style: "color: cornflowerblue;"},
   *       {name: "actionDelete", icon: "bi-trash", text: "", style: "color: cornflowerblue;"},
   *     ];
   *     config.pageSize = "4";
   *     kvt.setAttribute("config", JSON.stringify(config));
   */
  constructor() {
    super();
    console.log("static/wc/key-value-table.js")
    this.theData = [];
    this.theConfig = {};
    this.dataLength = 1;
    this.sortCol = {};
    this.sortAsc = false;
    this.pageSize = 3;
    this.curPage = 1;
    this.filters = {};
  }

  connectedCallback() {
    if (this.config) {
      this.theConfig = JSON.parse(this.config);
      if (this.theConfig.pageSize) this.pageSize = this.theConfig.pageSize
    }
    this.innerHTML = `
    <style> 
      @import url('static/css/bootstrap.min.css');
      @import url('static/img/bootstrap-icons.css');
    </style>
    <style>
      .control-label {
        color:  #0d6efd; 
      } 
      th {
            cursor: pointer;
        }
    </style>
    <div class="row p-1">
            <div class="container-fluid" >
                <table id="dataTable" class="table table-striped table-bordered  border-primary">
                  <thead>
                    <tr id="dataTheadTr" class="">
                    </tr>
                    <tr id="dataTheadTrFilter" class="">
                    </tr>
                  </thead>
                  <tbody id="dataTbody">
                    <tr><td><i>Loading...</i></td></tr>
                  </tbody>
                </table>
            </div>
    </div>
     <div class="row p-1">
        <div class="col col-4">
            <div class="control-label" style="padding-top: 12px;">Showing rows <span id="rowTop">0</span> to <span id="rowBottom">0</span> of <span id="rowCount">0</span></div>
        </div>
        <div class="col col-4"><div class="control-label" style="margin-top: 4px; text-align: center;"><span>Page size: </span><input id="pageSizeId" style="max-width: 3ch; text-align: center;"></div></div>
        <div class="col col-4">
            <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-end">
              <li class="page-item">
                <a id="firstButton" class="page-link d-none" href="#" aria-label="First">
                  <span aria-hidden="true"><i class="bi-chevron-double-left"></i></span>
                </a>
              </li>
              <li class="page-item">
                <a id="prevButton" class="page-link d-none" href="#" aria-label="Previous">
                  <span aria-hidden="true"><i class="bi-chevron-left"></i></span>
                </a>
              </li>
              <li class="page-item">
                <div class="control-label" style="margin-top: 4px;"> 
                  <span style="margin-left: 4px;">Page</span>
                  <input style="margin-left: 2px;max-width: 4ch; text-align: center;" id="currentPage">
                  <span style="margin-left: 2px;">of</span>
                  <span style="margin-left: 2px;margin-right: 4px;" id="pageMax"></span>
                </div>
              </li>
              <li class="page-item">
                <a id="nextButton" class="page-link" href="#" aria-label="Next">
                  <span aria-hidden="true"><i class="bi-chevron-right"></i></span>
                </a>
              </li>
              <li class="page-item">
                <a id="lastButton" class="page-link" href="#" aria-label="Last">
                  <span aria-hidden="true"><i class="bi-chevron-double-right"></i></span>
                </a>
              </li>
            </ul>
          </nav>
        </div>
        </div>
     </div>
    `

    this.querySelector("#currentPage").addEventListener("keypress", (e) => {
      if (e.key === 'Enter') {
        this.curPage = this.querySelector("#currentPage").value;
        this.renderTable();
      }
    });
    // listen for page size change
    this.querySelector("#pageSizeId").addEventListener("keypress", (e) => {
      if (e.key === 'Enter') {
        this.pageSize = this.querySelector("#pageSizeId").value;
        this.curPage = 1;
        this.renderTable();
      }
    });

    this.querySelector('#firstButton').addEventListener('click', () => this.firstPage(this));
    this.querySelector('#prevButton').addEventListener('click', () => this.previousPage(this));
    this.querySelector('#nextButton').addEventListener('click', () => this.nextPage(this));
    this.querySelector('#lastButton').addEventListener('click', () => this.lastPage(this));

  }

  disconnectedCallback() {
  }

  static get observedAttributes() {
    return ['data', 'config'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
    if (oldVal === null) {
      return;
    }
    switch (name) {
      case "config": {
        this.theConfig = JSON.parse(this.config);
        if (this.theConfig.pageSize) this.pageSize = this.theConfig.pageSize
        if (this.theData) {
          this.renderTable();
        }
        break;
      }
      case "data": {
        this.theData = JSON.parse(this.data);
        this.renderTable();
        break;
      }
    }
  }

  adoptedCallback() {
  }

  get data() {
    return this.getAttribute("data");
  }
  set data(value) {
    this.setAttribute("data", value);
  }
  get config() {
    return this.getAttribute("config");
  }
  set config(value) {
    this.setAttribute("config", value);
  }

  sort(e) {
    let thisSort = e.target.dataset.sort;
    if(this.sortCol === thisSort) this.sortAsc = !this.sortAsc;
    this.sortCol = thisSort;
    this.theData.sort((a, b) => {
      if(a[this.sortCol] < b[this.sortCol]) return this.sortAsc?1:-1;
      if(a[this.sortCol] > b[this.sortCol]) return this.sortAsc?-1:1;
      return 0;
    });
    this.curPage = 1;

    this.renderTable();
  }

  firstPage() {
    if(this.curPage > 1) this.curPage = 1;
    this.renderTable();
  }
  previousPage() {
    if(this.curPage > 1) this.curPage--;
    this.renderTable();
  }
  nextPage() {
    if((this.curPage * this.pageSize) < this.theData.length) this.curPage++;
    this.renderTable();
  }
  lastPage() {
    if((this.curPage * this.pageSize) < this.theData.length) {
      this.curPage = Math.floor(this.theData.length/this.pageSize) + 1;
      this.renderTable();
    }
  }

  renderTable() {
    if (! this.theConfig || ! this.theConfig.cols) {
      return;
    }
    if (! this.theData || this.theData.length === 0) {
      return;
    }
    let result = '';
    // render table header row 1 (column names)
    const theadTr = this.querySelector("#dataTheadTr");
    this.theConfig.cols.forEach(c => {
        result += `<th class="${c.width}"><span data-sort="${c.property}">${c.label}<i  data-sort="${c.property}" class="bi-arrow-down-up control-label" style="margin-left: 2px;"></i></span></th>`
    });
    if (this.theConfig.actions) {
      result += `<th style="text-align: center; vertical-align: middle;" colspan="${this.theConfig.actions.length}" scope="col">Actions</th>`
    }
    theadTr.innerHTML = result;
    result = "";
    // render table header row 2 (filters)
    const theadTrFilter = this.querySelector("#dataTheadTrFilter");
    this.theConfig.cols.forEach(c => {
      result += this.getColumnFilter(c);
    });
    if (this.theConfig.actions) {
      result += `<th style="text-align:center" colspan="${this.theConfig.actions.length}" scope="col"></th>`
    }
    theadTrFilter.innerHTML = result;
    result = "";
    // render table rows
    const tbody = this.querySelector("#dataTbody");
    // create table body html
    let displaying = 0;
    let start = (this.curPage-1)*this.pageSize;
    let end =this.curPage*this.pageSize;
    this.theData.filter((row, index) => {
      if(index >= start && index < end) return true;
    }).forEach(r => { // add rows
      displaying++;
      result += `<tr>`;
      this.theConfig.cols.forEach(c => { // add cells
        result += `<td class="${c.width}">${r[c.property]}</td>`
      });
      if (this.theConfig.actions) {
        this.theConfig.actions.forEach(a => { // add action cells
          result += `<td><a href="#" data-type="${a.name}" style="${a.style}" ><i class="${a.icon}" data-type="${a.name}"></i>${a.text}</a></td>`
        });
      }
      result += `</tr>`;
    });
    tbody.innerHTML = result;
    this.setTableSizeNotice(start+1, start+displaying);
    // listen for sort clicks
    this.querySelectorAll('#dataTable thead tr th span').forEach(t => {
      t.addEventListener('click', (e) => this.sort(e, this));
    });
  }
  getColumnFilter(column) {
    return `<td class="${column.width}"><input type="text" id="db-explorer-filter-${column.property}" class="form-control" placeholder="Filter" aria-label="filter table" style="font-size: 50%;"></td>`
  }
  setTableSizeNotice(start, end) {
    this.querySelector("#rowTop").innerText = `${start}`;
    this.querySelector("#rowBottom").innerText = `${end}`;
    this.querySelector("#rowCount").innerText = `${this.theData.length}`;
    this.querySelector("#currentPage").value = `${this.curPage}`;
    this.querySelector("#pageMax").innerText = `${Math.ceil(this.theData.length / this.pageSize)}`;
    this.querySelector("#pageSizeId").value = `${this.pageSize}`;

    if (this.curPage === 1) {
      this.querySelector("#firstButton").classList.add("d-none");
      this.querySelector("#prevButton").classList.add("d-none");
    } else {
      this.querySelector("#firstButton").classList.remove("d-none");
      this.querySelector("#prevButton").classList.remove("d-none");
    }
    if (this.curPage === Math.ceil(this.theData.length/this.pageSize)) {
      this.querySelector("#nextButton").classList.add("d-none");
      this.querySelector("#lastButton").classList.add("d-none");
    } else {
      this.querySelector("#nextButton").classList.remove("d-none");
      this.querySelector("#lastButton").classList.remove("d-none");
    }
  }

}
window.customElements.define('key-value-table', KeyValueTable);
