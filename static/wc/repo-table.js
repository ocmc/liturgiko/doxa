class RepoTable extends HTMLElement {
  constructor() {
    super();
    console.log("static/wc/repo-table.js")
    this.tableBody = null;
  }

  connectedCallback() {
    this.innerHTML =
`
<style> 
  @import url('static/css/bootstrap.min.css');
  @import url('static/img/bootstrap-icons.css');
</style>
<div id="repoTableContainer" class="container container-fluid">
  <table class="table table-striped">
      <thead>
          <tr>
          <th>Local Directory</th>
          <th>Remote URL</th>
          <th>Status</th>
          <th>Actions</th>
          </tr>
      </thead>
      <tbody id="tableBody"></tbody>
  </table>  
</div> <!-- container -->
`;
    this.tableBody = this.querySelector("#tableBody")
    // add listeners.  For each one added, remove it in disconnectedCallback.
    this.querySelector('#my-element-div').addEventListener('click', () => this.sayOuch());
  }

  disconnectedCallback() {
    this.querySelector('#my-element-div').removeEventListener('click', () => this.sayOuch());
  }

  sayOuch() {
    alert("ouch");
  }

  static get observedAttributes() {
    return ['name'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
    console.log(`Attribute: ${name} changed!`);
  }

  adoptedCallback() {
  }
  get name() {
    return this.getAttribute("name");
  }

  set name(value) {
    this.setAttribute("name", value);
  }

}

window.customElements.define('repo-table', RepoTable);