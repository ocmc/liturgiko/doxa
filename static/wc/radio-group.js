// RadioGroup allows the user make a selection using radio buttons
class RadioGroup extends HTMLElement {
    /**
     * TAG ATTRIBUTES:
     *   gid: a unique id for the group so if the tag is used multiple times on a page, they will be treated as separate groups
     *   items: a json stringified object array as described below.
     *   active: the numeric index of the item to set as selected.
     * USAGE: see how used in synch-manager.
     * 1. In constructor, set an array object with radio options:
     *    e.g.,
     *    this.strategyOptions = [
     *       {Key: "combine", Desc: "Make remote repos, local repos and database content identical, but keep changes I made since the last synch.", Selected: true, Class: "alert-success", Icon: "", IconColor: ""},
     *       {Key: "resetToRemote", Desc: "Throw away database and local repo changes. Make the local repos and database identical to the remote repos.", Selected: false, Class: "alert-warning", Icon: "", IconColor: ""},
     *       {Key: "resetToDB", Desc: "Throw away remote repo changes. Make the remote repos identical to local repos and the database contents.", Selected: false, Class: "alert-warning", Icon: "", IconColor: ""},
     *     ];
     *     The option object attributes are:
     *       Key - the unique ID that will be returned when the item is selected
     *       Desc - what the user sees
     *       Class - (optional) the Bootstrap Alert class. Used to set the font color and background color of the item
     *       Icon - (optional) the Bootstrap icon to prefix to the Desc value
     *       IconColor - (optional) the color of the icon
     * 2. Add <radio-group it="someIdYouCreate" items="[]" active="0"></radio-group>
     *       items - use JSON.stringify() to turn the array of option objects into string to pass as tag
     *       active - the index number of the selected item
     * 3. Add an event handler to get notified when someone clicks a radio button:
     *      this.querySelector("#someIdYouCreate").addEventListener("change", (ev) => this.yourHandlerFunction(ev));     * To Get Selected Item, in the using javascript:
     * 4. Create the handler function:
     *   myHandlerFunction(ev) {
     *     // to get value selected
     *     let selectedItem = ev.target.getAttribute("value");
     *    // to get value selected
     *    let selectedItemIndex = ev.target.getAttribute("index");
     *   }
     */
    constructor() {
        super();
        console.log("static/wc/radio-group.js")
        this.listArray = null;
        this.listComponent = null;
    }

    connectedCallback() {
        this.innerHTML =
            `
<style> 
  @import url('static/css/bootstrap.min.css');
  @import url('static/img/bootstrap-icons.css');
</style>
<style>
</style>
<div id="listComponent_${this.gid}"></div>
`;
        this.listComponent = this.querySelector(`#listComponent_${this.gid}`);
        this.listComponent.addEventListener("onchange", (e) => this.fireChangeEvent(e));
        if (this.listArray && this.listArray.length > 0) {
            this.renderList()
        }
    }
    // for every event handler added above, below remove it
    disconnectedCallback() {
        this.listComponent.removeEventListener("onchange", (e) => this.fireChangeEvent(e));
    }

    fireChangeEvent(ev) {
    }
    /**
     <div className="form-check">
     <input className="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" value="" index="">
     <label className="form-check-label" htmlFor="flexRadioDefault1">
     Default radio
     </label>
     </div>
     *
     */
    renderItem(item, index) {
        // containing div
        const theDiv = document.createElement('div');
        theDiv.classList.add("form-check");
        // input
        const theInput = document.createElement("input");
        theInput.classList.add("form-check-input");
        theInput.setAttribute("type", "radio")
        theInput.setAttribute("name", `flexRadioDefault_${this.gid}`)
        theInput.setAttribute("id", `flexRadioDefault_${this.gid}_${index}`)
        theInput.setAttribute("value", `${item.Key}`)
        theInput.setAttribute("index", `${index}`)
        theInput.checked = item.Selected;
        // label
        const theLabel = document.createElement('label');
        theLabel.classList.add("form-check-label");
        if (item.Class && item.Class.length > 0) {
            theLabel.classList.add(item.Class);
        }
        theLabel.setAttribute("for", `flexRadioDefault_${this.gid}_${index}`)
        if (item.Icon && item.Icon.length > 0) {
            let color = "black";
            if (item.IconColor && item.IconColor.length >= 0) {
                color = item.IconColor
            }
            theLabel.innerHTML = `<span><i class="bi bi-${item.Icon}" style="color: ${color};"></i>&nbsp;${item.Desc}</span>`
        } else {
            theLabel.innerText = item.Desc
        }
        // append components
        theDiv.appendChild(theInput);
        theDiv.appendChild(theLabel);
        this.listComponent.appendChild(theDiv);
    }

    renderList() {
        if (! this.listComponent) {
            return;
        }
        this.listComponent.innerHTML = "";
        if (this.listComponent) {
            this.listArray.forEach((item, index) => {
                item.Selected = `${index}` === this.active;
                this.renderItem(item, index);
            });
        }
    }

    static get observedAttributes() {
        return ['gid', 'items', 'active'];
    }

    //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
        switch (name) {
            case "items": {
                if (newVal !== null && oldVal !== newVal) {
                    this.listArray = JSON.parse(newVal);
                    this.renderList();
                }
                break;
            }
            case "active": {
                if (newVal !== null && oldVal !== newVal) {
                    this.renderList();
                }
                break;
            }
        }
    }

    adoptedCallback() {
    }

    get gid() { // group id
        return this.getAttribute("gid");
    }
    set gid(value) {
        this.setAttribute("gid", value);
    }

    get items() {
        return this.getAttribute("items");
    }
    set items(value) {
        this.setAttribute("items", value);
    }

    get active() {
        return this.getAttribute("active");
    }
    set active(value) {
        this.setAttribute("active", value);
    }

    // custom events
    selectionChanged = new CustomEvent('selectionchanged', {
        detail: {
            name: 'selectionchanged'
        },
        bubbles: true
    });
}

window.customElements.define('radio-group', RadioGroup);