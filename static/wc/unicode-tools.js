import * as utils from "./utils.js";

class UnicodeTools extends HTMLElement {
  constructor() {
    super();
    console.log("static/wc/unicode-tools.js")
    this.inputSrc = null;
    this.theResponse = null;
    // The response prototype class
    class TheResponse {
    }
    TheResponse.prototype.Message = "";
    TheResponse.prototype.Status = "";
    TheResponse.prototype.SrcText = ""; // source text
    TheResponse.prototype.Nfc = ""; // converted to Unicode NFC
    TheResponse.prototype.Nfd = ""; // to NFD
    TheResponse.prototype.NfdNd = ""; // to NFD no diacritics
    TheResponse.prototype.NfdNdNpLow = ""; // no diacritics,no punctuation, lowercase
  }

  connectedCallback() {
    this.innerHTML =
`
<style> 
  @import url('static/css/bootstrap.min.css');
  @import url('static/img/bootstrap-icons.css');
</style>
<br>
<div id="my-element-div" class="wc-title">Unicode Tools</div>
<br>
<hr>
<br>
        <div class="row">
            <div style="display: flex; flex-direction: column; flex-wrap: wrap; gap: 1rem; max-width: 90vw;">
                <!-- text input -->
                <label id="labelSrc" for="inputSrc" class="form-label">Text to Convert</label>
                <textarea id="inputSrc" rows="4" class="form-control" aria-labelledby="labelSrc">
                </textarea>
            </div>
        </div>
        <br>
<div style="display: flex; flex-direction: column; flex-wrap: wrap; gap: 1rem; max-width: 50vw;">
  <div style="display: flex; flex-direction: row; gap: 20px;">  <!-- Submit Button -->
      <span>
          <button id="btnRequest"  type="button" class="btn btn-primary" style="">Convert</button>
      </span>
      <br>
      <div class="alert alert-light message" role="alert" id="requestStatus" style="padding: 3px; width: max-content;">&nbsp;</div>
  </div> <!-- end Submit Button -->
<br>
        <div class="row">
            <div style="display: flex; flex-direction: column; flex-wrap: wrap; gap: 1rem; max-width: 90vw;">
                <label id="idLabelNfc" for="outputNfc" class="form-label">Unicode Normal Form Canonical Composition (NFC)</label>
                <textarea id="outputNfc" placeholder="text to convert" rows="4" class="form-control" aria-labelledby="idLabelNfc">
                </textarea>
            </div>
        </div>
        <div class="row">
            <div style="display: flex; flex-direction: column; flex-wrap: wrap; gap: 1rem; max-width: 90vw;">
                <label id="idLabelNfd" for="outputNfd" class="form-label">Unicode Normal Form Canonical Decomposition (NFD)</label>
                <textarea id="outputNfd" placeholder="text to convert" rows="4" class="form-control" aria-labelledby="idLabelNfd">
                </textarea>
            </div>
        </div>
        <div class="row">
            <div style="display: flex; flex-direction: column; flex-wrap: wrap; gap: 1rem; max-width: 90vw;">
                <label id="idLabelNfdNd" for="outputNfdNd" class="form-label">Unicode NDF - No Diacritics</label>
                <textarea id="outputNfdNd" placeholder="text to convert" rows="4" class="form-control" aria-labelledby="idLabelNfdNd">
                </textarea>
            </div>
        </div>
        <div class="row">
            <div style="display: flex; flex-direction: column; flex-wrap: wrap; gap: 1rem; max-width: 90vw;">
                <label id="idLabelNfdNdNpLow" for="outputNfdNdNpLow" class="form-label">Unicode NDF - Lower Case, No Diacritics or Punctuation</label>
                <textarea id="outputNfdNdNpLow" placeholder="text to convert" rows="4" class="form-control" aria-labelledby="idLabelNfdNdNpLow">
                </textarea>
            </div>
        </div>
`;
    this.inputSrc = this.querySelector("#inputSrc");
    this.divRequestStatus = this.querySelector("#requestStatus");
    this.requestButton = this.querySelector("#btnRequest");

    // add listeners.  For each one added, remove it in disconnectedCallback.
    this.requestButton.addEventListener("click", (e) => this.handleRequest(e))
  }

  disconnectedCallback() {
    this.requestButton.removeEventListener("click", (ev) => this.handleRequest(ev))
  }

  // ---- MESSAGE Handlers
  requestStatusMessage(type, message) {
    this.divRequestStatus.className = "";
    this.divRequestStatus.classList.add(type);
    this.divRequestStatus.innerHTML = utils.getMessageSpan(type, message);
  }
  // ---- REST API Handlers
  handleRequest() {
    const url = this.host
        + `api/unicode?srcText=${encodeURIComponent(this.inputSrc.value)}`
    ;
    utils.callApi(url, "GET").then(response => {
      if (response.Status === "OK") {
        alert(JSON.stringify(response))
        try {
          this.theResponse = Object.assign(new TheResponse(), response);
          console.log("could not cast response to class TheResponse")
        } catch {
          this.theResponse = response;
        }

        this.requestStatusMessage(utils.alerts.Info, response.Message)
      } else {
        this.requestStatusMessage(utils.alerts.Danger, response.Message)
      }
    }).catch(error => {
      this.requestStatusMessage(utils.alerts.Danger, error)
    });
  }
// ---- End REST API Handlers

  static get observedAttributes() {
    return ['host'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
  }

  adoptedCallback() {
  }
  get host() {
    return this.getAttribute("host");
  }

  set host(value) {
    this.setAttribute("host", value);
  }
}

window.customElements.define('unicode-tools', UnicodeTools);