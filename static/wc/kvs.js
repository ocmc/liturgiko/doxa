export class KeyPath {
    constructor(path) {
        console.log("static/wc/kvs.js")
        this.idDelimiter = ":";
        this.segmentDelimiter = "/";
        this.dirs = [];
        this.keyParts = [];
        this.parsePath(path);
    }
    // sid returns the ID as a sid for use in lml
    sid(id) {
        let parts = this.path().split(":");
        if (parts.length !== 2) {
            return id;
        }
        let key = parts[1];
        parts = parts[0].split("/");
        if (parts.length < 2) {
            return;
        }
        let topic = parts[parts.length - 1]
        return `sid "${topic}${this.idDelimiter}${key}"`
//        return "sid \"" + this.path().split("/").slice(-1) + "\"";
    }
    // rid returns the ID as a rid for use in lml
    rid(id) {
        let parts = this.path().split(":");
        if (parts.length !== 2) {
            return id;
        }
        let key = parts[1];
        parts = parts[0].split("/");
        if (parts.length < 2) {
            return;
        }
        let topic = parts[parts.length - 1]
        if (this.canBeRid(topic)) {
            return `rid "${this.relativeTopic(topic)}${this.idDelimiter}${key}"`;
        } else {
            return `Can't be used as a rid: ${this.topicKey()}`;
        }
    }
    topicKey() {
        return this.dirs.slice(-1) + this.idDelimiter + this.keyParts.join(this.segmentDelimiter);
    }
    // function relativeTopic converts the topic part of a Doxa database ID into
    // a relative topic.  The topic is the last directory of an ID path.  It is
    // only relative if the path starts with "ltx".
    relativeTopic (topic) {
        let parts = topic.split(".")
        let relativeTopic = parts[0];
        if (parts[0] === "le") {
            relativeTopic = relativeTopic + "." + parts[1] + "." + parts[2];
        }
        relativeTopic = relativeTopic + ".*";
        return relativeTopic;
    }

    canBeRid(topic) {
        return topic.startsWith("da.")
            || topic.startsWith("eo.")
            || topic.startsWith("le.")
            || topic.startsWith("me.")
            || topic.startsWith("oc.")
            || topic.startsWith("pe.")
            || topic.startsWith("sy.")
            || topic.startsWith("tr.")
            || topic.startsWith("ty.")
            ;
    }

    // Path returns the Dirs and KeyParts as a path, separated by the IdDelimiter
    // If the length of Dirs or KeyParts is > 1, the segments are separated by the SegmentDelimiter
    path() {
        let sb = "";
        if (this.dirs && this.dirs.length > 0) {
            sb = this.segmentString(this.dirs);
        }
        if (this.keyParts && this.keyParts.length > 0) {
            sb = sb + this.idDelimiter + this.segmentString(this.keyParts);
        }
        return sb;
    }
    // DirPath returns the Dirs as a path, separated by the IdDelimiter
    // If the length of Dirs is > 1, the segments are separated by the SegmentDelimiter
    dirPath() {
        if (this.dirs.length > 0) {
            return this.segmentString(this.dirs);
        } else {
            return "";
        }
    }
    parsePath(path) {
        const parts = this.parseParts(path);
        switch (parts.length) {
            case 1: { // this is a partial path, with just the dirs
                this.clearDirs();
                this.parseSegments(parts[0]).forEach((p,index) => {
                    this.dirs.push(p);
                });
                this.clearKeyParts();
                break;
            }
            case 2: case 3: {
                this.clearDirs();
                this.parseSegments(parts[0]).forEach((p,index) => {
                    if (p.length > 0) {
                        this.dirs.push(p);
                    }
                });
                this.clearKeyParts();
                this.parseSegments(parts[1]).forEach((p,index) => {
                    this.keyParts.push(p);
                });
                break;
            }
            default: {
                return null;
            }
        }
    }
    clearDirs() {
        this.dirs = [];
    }
    clearKeyParts() {
        this.keyParts = [];
    }
    // parseParts splits s into its parts using the IdDelimiter as the delimiter.
    parseParts(s) {
        return s.split(this.idDelimiter);
    }
    // parseSegments splits s into its segments using the SegmentDelimiter as the delimiter.
    parseSegments(s) {
        return s.split(this.segmentDelimiter);
    }

    // segmentString joins the parameter array into a segment using the SegmentDelimiter as the delimiter.
    segmentString(s) {
    return s.join(this.segmentDelimiter);
    }

    dirsSegmentString() {
        return this.dirs.join(this.segmentDelimiter);
    }
    keyPartsSegmentString() {
        return this.keyParts.join(this.segmentDelimiter);
    }
}