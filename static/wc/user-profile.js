import * as utils from "./utils.js";

class UserProfile extends HTMLElement {
    constructor() {
        super();
        console.log("static/wc/user-profile.js")
        // submit parameters
        this.user = "";
        this.copyright = ""; // must have value if this.gitlabPrivate === true
        this.contact = ""; // optional if this.gitlabPrivate === true
        this.doxaHandleGitlab = false;
        this.gitlabPrivate = true;
        this.jurisdiction = ""; // must have value if license Public Domain (within specified jurisdictions) is selected.
        this.license = ""; // must have value if this.gitlabPrivate === true
        this.token = "";  // must have value if this.gitlabPrivate === true
        // div handles to make hidden or visible
        this.gitLabSettingsDiv = null;
        this.groupInfo = null;
        this.publicDiv = null;
        this.divCopyright = null;
        this.divContact = null;
        this.inputJurisdictionDiv = null;
        this.uidPwdAccordionDiv = null;
        // input handles
        this.inputCopyright = null;
        this.inputContact = null;
        this.inputToken = null;
        this.inputGroupId = null;
        this.inputJurisdiction = null;
        this.inputDoxaHandlesGit = null;
        this.inputUserHandlesGit = null;
        this.inputGitlabPrivate = null;
        this.inputGitlabPublic = null;
        this.inputLicenseNotSet = null;
        this.inputLicenseCcBy = null;
        this.inputLicenseCcBySa = null;
        this.inputLicenseCcByNd = null;
        this.inputLicenseCcByNc = null;
        this.inputLicenseCcByNcNd = null;
        this.inputLicenseCcByNcSa = null;
        this.inputLicenseCco = null;
        this.inputLicensePdSj = null;
        this.inputLicensePdWw = null;
        this.inputLicenseValue = "";

        // button handles
        this.buttonVerifyCredentials = null;
        // output handles
        this.anchorGroupUrl = null;
        this.submitMsgDiv = null;
        this.verificationMsgDiv = null;

        // error spans
        this.spanErrorGroupId = null;
        this.spanErrorToken = null;
        this.spanErrorLicense = null;
        this.spanErrorCopyright = null;
        this.spanErrorJurisdication = null;

        // license enum.  Must match doxa golang pkg/enums/licenseTypes
        this.licenses = {
            By: 'BY',
            ByNc: 'BY-NC',
            ByNCNd: 'BY-NC-ND',
            ByNcSa: 'BY-NC-SA',
            ByNd: 'BY-ND',
            BySa: 'BY-SA',
            CCO: 'CCO',
            PdSj: 'PD-SJ',
            PdWw: 'PD-WW',
            NotSet: 'Not Set',
        }
    }

    connectedCallback() {
        if (!this.vault || this.vault.length === 0) {
            this.vault = "{your home directory}/doxa/.vault"
        }
        this.htmlTitle = `
            <div class="row flex"> <!-- title -->
                <p class="alert-info" style="margin-left: -20px !important;">User
                    Profile</p>
                <br>
                <div>Your user profile is encrypted and stored in ${this.vault}, not in the Doxa
                    database.
                </div>
            </div> <!-- end title -->
        ` // end this.title

        this.htmlInstructions = `
            <div class="row flex"> <!-- instructions about accordions -->
                <div>Your User Profile is divided into sections. Click on the name of a
                section to view or hide it. Each section has its own Save button.
                </div>
            </div> <!-- end instructions about accordions -->
        ` // end this.htmlInstructions

        this.htmlAcItGitBodyIntro = `
            <div class="row flex narrative">
                <div class="col col-8">Doxa can use <a
                        href="https://git-scm.com" target="_blank">git</a>
                    to save versions of your Doxa data on your computer. 
                    Git is a distributed version control system. You
                    do not need to install git. Doxa can create and
                    manage git repositories on your computer for
                    you. Doxa can also save your git repositories to
                    a cloud server provided by <a
                        href="https://gitlab.com" target="_blank">Gitlab</a>
                    at no cost to you.</div>
                <div class="col col-4"></div>
            </div>        
        ` // end this.htmlAcItGitBodyIntro

        this.htmlAcItGitBodyProfileNotInGit = `
            <div class="row flex narrative">
                <div class="col col-8">
                    <div>
                        Your profile is not stored in git. The data
                        Doxa stores in git are:
                    </div>
                    <ol class="">
                        <li class="">Your translations (exported
                            from the database).</li>
                        <li class="">Your media maps (exported from
                            the database).</li> 
                        <li class="">Your templates.</li>
                        <li class="">Your configuration settings
                            (exported from the database).</li>
                        <li class="">Your generated website.</li>
                        <li class="">Assets (css, javascript, etc.)
                            used by your website.</li>
                    </ol>
                </div>
                <div class="col col-4"></div>
            </div>        
        ` // end this.htmlAcItGitBodyProfileNotInGit


        this.htmlAcItGitBodyAdvantagesGit = `
            <div class="row flex narrative">
                <div class="col col-8">
                    <div>
                        The advantages of using git are:
                    </div>
                    <ol class="">
                        <li class="">It keeps a record of all
                            changes you make.</li>
                        <li class="">You can compare versions of
                            your data.</li>
                        <li class="">You can recover a past version.</li>
                    </ol>
                </div>
                <div class="col col-4"></div>
            </div>
        ` // end this.htmlAcItGitBodyAdvantagesGit

        this.htmlAcItGitBodyFormGitSelect = `
            <div class="form-check">
                <input class="form-check-input"
                    type="radio" name="gitPreference"
                    id="handleGitMyself" checked>
                <label class="form-check-label"
                    for="handleGitMyself">
                    I know how to use git and prefer to
                    do it myself.
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input"
                    type="radio" name="gitPreference"
                    id="doxaHandleGitlab">
                <label class="form-check-label"
                    for="doxaHandleGitlab">
                    I want Doxa to handle git for me and
                    save my data to Gitlab.
                </label>
            </div>
        ` // this.htmlAcItGitBodyFormGitSelect

        this.htmlAcItGitBodyFormDoxaHandlesGitCredentials = `
            <div class="row flex narrative">
                <div>Since you want Doxa
                    to handle backups for you, 
                    you need a free Gitlab account. 
                    You can sign up <a href="https://gitlab.com/users/sign_up" target="_blank">here.</a>
                    You must also create a Gitlab group ID 
                    and a personal access token, and
                    enter them below. Doxa will only create  
                    subgroups and projects in the Gitlab group
                    whose ID you provide. The personal access token 
                    is encrypted and stored only on your computer.
                </div>
            </div>
            <br>
            <div class="mb-3"> <!-- Group ID-->
                <label for="inputGroupId"
                    class="form-label">Gitlab Group ID &nbsp;<span class="errorFlag" id="errorGroupId"></span>
                </label>
                <input type="text"
                    class="form-control"
                    id="inputGroupId"
                    aria-describedby="groupIdHelp">
                <div id="groupIdHelp"
                    class="form-text">Enter the
                    numeric ID of the Gitlab group
                    you create for your Doxa data.
                    <a
                        href="https://docs.gitlab.com/ee/user/group/manage.html"
                        target="_blank">How to
                        Create a Group</a>. The group
                        name should either start or end
                        with the word doxa, e.g. doxa-{your name}. 
                        After you create the group, you will
                    see the numeric Group ID. Copy and paste
                    the group ID into the field above.</div>
            </div> <!-- end Group ID -->
            <div class="mb-3"> <!-- Token -->
                <label for="inputToken"
                    class="form-label">Personal Access Token &nbsp;<span class="errorFlag" id="errorToken"></span>
                </label>
                <input type="password"
                    class="form-control"
                    id="inputToken">
                <div id="tokenHelp"
                    class="form-text">Enter your
                    Gitlab personal access token in
                    the field above. It
                    will be encrypted and stored on
                    your computer in ${this.vault}. <a
                        href="https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html"
                        target="_blank">How to
                        Create a Personal Access
                        Token</a>. When you create
                    it, give the token the name
                    "doxa". After you create the
                    token, copy and paste it into
                    the field above. You should also
                    save a copy of the token in a
                    safe place, e.g. a password
                    manager. Do not share it with
                    anyone else. It is a kind of
                    password. Although Doxa only works 
                    within the GitLab group whose ID 
                    you provide, if someone else gets
                    your token, they can access all
                    your GitLab groups and projects.</div>
            </div>  <!-- end Token -->
            <br>
            <div style="display: flex; gap: 20px;">  <!-- Verify Button -->
                <span>
                    <button id="buttonVerifyCredentials" type="button" style="">Verify Group ID and Token</button>
                </span>
                <br>
                <div class="alert alert-light message" role="alert" id="verificationMsg" style="padding: 3px;">&nbsp;</div>
            </div> <!-- end Verify Button -->
            <div id="groupInfo" class=" row d-none" style="padding-top: 30px;">
                <div>Group URL: <a id="anchorGroupUrl" href="${this.groupUrl}"  target="_blank">${this.groupUrl}</a></div>
            </div>
        ` // end this.htmlAcItGitBodyFormDoxaHandlesGitCredentials

        this.htmlAcItGitBodyFormDoxaHandlesGitSelectPrivatePublic = `
                    <div class="form-check">
                        <input class="form-check-input"
                            type="radio" name="gitPrivate"
                            id="gitlabPrivate" checked>
                        <label class="form-check-label"
                            for="gitlabPrivate">
                            I want my Doxa data to be
                            private.
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input"
                            type="radio" name="gitPrivate"
                            id="gitlabPublic">
                        <label class="form-check-label"
                            for="gitlabPublic">
                            I want to share my Doxa data
                            with the public.
                        </label>
                    </div>
        ` // end this.htmlAcItGitBodyFormDoxaHandlesGitSelectPrivatePublic

        this.htmlAcItGitBodyFormDoxaHandlesGitPublic = `
                    <div class="d-none" id="publicDiv"> <!-- begin public div -->
                        <br>
                        <div>
                            When you provide your work to others, 
                            you can still retain the copyright. There are
                            a variety of copyright licenses available that 
                            allow you to require users of your work to 
                            acknowledge they obtained it from you (attribution).  
                            You can also indicate whether they are allowed to
                            modify your work (adaptation) and whether they are allowed to
                            use your work for monetary (commercial) purposes.  
                            Please review the licenses
                                available at <a
                                    href="https://creativecommons.org/choose/"
                                    target="_blank">Creative
                                    Commons</a>. For <em>No
                                    Rights Reserved</em>, see <a
                                    href="https://creativecommons.org/share-your-work/public-domain/cc0/"
                                    target="_blank">this</a>.
                                For <em>Public Domain</em> (both
                                types), see <a
                                    href="https://creativecommons.org/share-your-work/public-domain/pdm/"
                                    target="_blank">this</a>.
                                Below, select a license to make your work
                                available for the benefit of
                                other people.
                        </div>
                        <br>
                        <div>Selected license: &nbsp;<span class="errorFlag" id="errorLicense"></span></div>
                        <div class="form-check">
                            <input class="form-check-input"
                                type="radio"
                                name="licensePreference"
                                id="license-not-set"
                                checked>
                            <label class="form-check-label"
                                for="license-not-set">
                                Not Selected
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input"
                                type="radio"
                                name="licensePreference"
                                id="license-cc-by">
                            <label class="form-check-label"
                                for="license-cc-by">
                                Attribution CC BY
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input"
                                type="radio"
                                name="licensePreference"
                                id="license-cc-by-sa">
                            <label class="form-check-label"
                                for="license-cc-by-sa">
                                Attribution-ShareAlike CC
                                BY-SA
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input"
                                type="radio"
                                name="licensePreference"
                                id="license-cc-by-nd">
                            <label class="form-check-label"
                                for="license-cc-by-nd">
                                Attribution-NoDerivs CC
                                BY-ND
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input"
                                type="radio"
                                name="licensePreference"
                                id="license-cc-by-nc">
                            <label class="form-check-label"
                                for="license-cc-by-nc">
                                Attribution-NonCommerical CC
                                BY-NC
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input"
                                type="radio"
                                name="licensePreference"
                                id="license-cc-by-nc-sa">
                            <label class="form-check-label"
                                for="license-cc-by-nc-sa">
                                Attribution-NonCommerical-ShareAlike
                                CC BY-NC-SA
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input"
                                type="radio"
                                name="licensePreference"
                                id="license-cc-by-nc-nd">
                            <label class="form-check-label"
                                for="license-cc-by-nc-nd">
                                Attribution-NonCommercial-NoDerivatives
                                CC BY-NC-ND
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input"
                                type="radio"
                                name="licensePreference"
                                id="license-cco">
                            <label class="form-check-label"
                                for="license-cco">
                                No rights reserved CCO
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input"
                                type="radio"
                                name="licensePreference"
                                id="license-pd-ww">
                            <label class="form-check-label"
                                for="license-pd-ww">
                                Public Domain (world-wide)
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input"
                                type="radio"
                                name="licensePreference"
                                id="license-pd-sj">
                            <label class="form-check-label"
                                for="license-pd-sj">
                                Public Domain (within
                                specified jurisdictions)
                            </label>
                        </div>
                        <div class="d-none" id="inputJurisdictionDiv"> <!-- begin public domain jurisdictions -->
                            <br>
                            <label for="inputJurisdiction" class="form-label">Public Domain Jurisdiction(s) &nbsp;<span class="errorFlag" id="errorJurisdication"></span></label>
                            <input type="text" id="inputJurisdiction" class="form-control" aria-describedby="publicDomainJurisdictionHelpBlock">
                            <div id="publicDomainJurisdictionHelpBlock" class="form-text">
                            Either enter the countries in which you release your work to be public domain, or list the ones where it is not. If there are no restrictions, use <em>Public Domain (world-wide).</em>
                            </div>
                        </div> <!-- end public domain jurisdictions -->
                        <div id="divCopyright"> <!-- begin copyright -->
                            <br>
                            <label for="inputCopyright" class="form-label">Copyright &nbsp;<span class="errorFlag" id="errorCopyright"></span></label>
                            <input type="text" id="inputCopyright" class="form-control" aria-describedby="copyrightHelpBlock">
                            <div id="copyrightHelpBlock" class="form-text">
                            Either the name you want to show as the copyright owner of your work (e.g. your own name).
                            </div>
                        </div> <!-- end copyright -->
                        <div id="divContact"> <!-- begin contact -->
                            <br>
                            <label for="inputContact" class="form-label">Contact Email (Optional)</label>
                            <input type="email" id="inputContact" class="form-control" aria-describedby="contactHelpBlock">
                            <div id="contactHelpBlock" class="form-text">
                            Optionally, enter the email to contact the copyright owner (e.g. your email).
                            </div>
                        </div> <!-- end contact -->
                    </div> <!-- end public div -->
        ` // end this.htmlAcItGitBodyFormDoxaHandlesGitPublic

        this.htmlAcItGitBodyFormDoxaHandlesGit = `
                <div class="d-none" id="doxa-handles-git"> <!-- begin doxa-handles-git -->
                    <hr>
                    ${this.htmlAcItGitBodyFormDoxaHandlesGitCredentials}
                    <hr>
                    <div><a href="https://ocmc.org" target="_blank">OCMC</a> provides Doxa free to all.
                            In the same spirit, 
                            many in the community of Doxa 
                            users freely share with the public 
                            their translations, notes, etc., so
                            others can use them with Doxa.
                            However, we understand that you
                            might have valid reasons not 
                            to share your work. Those who do share
                            their work often use a <a
                                href="https://creativecommons.org/choose/"
                                target="_blank">Creative Commons</a> license.
                    </div>
                    <br>
                    ${this.htmlAcItGitBodyFormDoxaHandlesGitSelectPrivatePublic}
                    ${this.htmlAcItGitBodyFormDoxaHandlesGitPublic}
                </div>  <!-- end doxa-handles-git -->
        ` // end this.htmlAcItGitBodyFormDoxaHandlesGit

        this.htmlAcItGitBodyForm = `
            <div class="row flex narrative">
                <div class="col col-8">
                    ${this.htmlAcItGitBodyFormGitSelect}
                    ${this.htmlAcItGitBodyFormDoxaHandlesGit}
                    <br>
                    <div style="display: flex; gap: 20px;">
                        <span>
                            <button id="buttonSubmit" type="button" class="btn
                                btn-primary">Save Git and Gitlab Preferences</button>
                        </span>
                        <span class="alert alert-light message" role="alert" id="submitMsg" style="padding: 3px;"></span>
                    </div>
                </div>
                <div class="col col-4"></div>
                <br>
            </div>
        ` // end this.htmlAcItGitBodyForm

        this.htmlAcItGitBodyAdvantagesGitlab = `
            <div class="row flex narrative">
                <div class="col col-8">
                    <div>The advantages of using Gitlab are:</div>
                    <ol class="">
                        <li class="">
                            Your data is securely stored on
                            a cloud server. If something happens to
                            your computer, your data is safe.
                        </li>
                        <li class="">
                            You can easily load your data
                            on a different computer.
                        </li>
                        <li class="">
                            You can keep your data private,
                            or you can share your work with other
                            users of Doxa.
                        </li>
                    </ol>
                </div>
                <div class="col col-4"></div>
            </div>
        ` // end this.htmlAcItGitBodyAdvantagesGitlab

        this.htmlAcItGitBody = `
            ${this.htmlAcItGitBodyIntro}
            ${this.htmlAcItGitBodyProfileNotInGit}
            ${this.htmlAcItGitBodyAdvantagesGit}
            ${this.htmlAcItGitBodyAdvantagesGitlab}
            <hr>
            ${this.htmlAcItGitBodyForm}
        ` // end this.htmlAcItGitBody

        this.htmlAcItGit = `
                <div class="accordion-item"> <!-- Git and Gitlab -->
                    <h2 class="accordion-header" id="headingGitlab">
                        <button class="accordion-button collapsed" type="button"
                            data-bs-toggle="collapse"
                            data-bs-target="#collapseGitlab" aria-expanded="false"
                            aria-controls="collapseGitlab">
                            <em>Git and Gitlab (for saving versions of your data on
                                your computer and on a cloud server)</em>
                        </button>
                    </h2>
                    <div id="collapseGitlab" class="accordion-collapse collapse"
                        aria-labelledby="headingGitlab"
                        data-bs-parent="#accordionProfile">
                        <div class="accordion-body">
                            ${this.htmlAcItGitBody}
                        </div>
                    </div>
                </div> <!-- end accordion item -->

        ` // end this.htmlAcItGit

        this.htmlAcItUidPwdBody = `
            <div class="row flex"> <!-- instructions -->
                <div>
                    Doxa can run on a personal computer or on a cloud server.  
                    Since more than one person can share a personal computer
                    (for example, in a library, or a home), Doxa requires
                    a User ID and Password.  
                </div>
            </div> <!-- end instructions -->
            <br>
            <!-- User ID -->
            <div class="mb-3">
            <label for="inputUID" class="form-label">User ID</label>
            <input type="text" class="form-control" id="inputUID" aria-describedby="uidHelp">
            <div id="uidHelp" class="form-text"></div>
          </div>
            <!-- Password -->
            <div class="mb-3">
                <label for="inputPwd" class="form-label">Password</label>
                <input type="password" class="form-control" id="inputPwd" aria-describedby="pwdHelp">
                <div id="pwdHelp" class="form-text"></div>
            </div>
            <button id="btnUidPwd" type="button" class="btn btn-primary">Save</button>
        ` // end this.htmlAcItUidPwdBody

        this.htmlAcItUidPwd = `
          <div id="uidPwdAccordionDiv" class="accordion-item d-none">
            <h2 class="accordion-header" id="headingOne">
              <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                User ID and Password
              </button>
            </h2>
            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionProfile">
              <div class="accordion-body">
                ${this.htmlAcItUidPwdBody}
              </div>
            </div>
          </div>
        ` // this.htmlAcItUidPwd

        this.htmlAccordionGroup = `
            <div class="row flex"> <!-- begin accordion div -->
                <div class="accordion" id="accordionProfile"> <!-- begin accordion group -->
                    ${this.htmlAcItUidPwd}
                    ${this.htmlAcItGit}
                </div> <!-- end accordion group -->
            </div> <!-- begin accordion div -->
` // end this.accordion

        this.innerHTML =
            `
<style> 
  @import url('static/css/bootstrap.min.css');
  @import url('static/img/bootstrap-icons.css');
</style>
<style>
    .user-profile {
       margin-top: 1vh;
    }
    .narrative {
        padding-top: 1vh;
    }
    .message {
        padding: 30px !important;
        margin: 30px !important;
        align-items: center !important;
        justify-content: center !important;
        text-align: center !important;
        align-items: center !important;
    }
    .accordion-button {
        color: #0c63e4;
        font-style: italic;
    }
</style>
<div class="container-fluid user-profile">
    ${this.htmlTitle}
    <br>
    <hr>
    <br>
    ${this.htmlInstructions}
    <br>
    <hr>
    <br>
    ${this.htmlAccordionGroup}
</div>
`;

        // input handles
        this.inputGroupId = this.querySelector("#inputGroupId");
        this.inputToken = this.querySelector("#inputToken");
        this.inputContact = this.querySelector("#inputContact");
        this.inputCopyright = this.querySelector("#inputCopyright");
        this.inputJurisdiction = this.querySelector("#inputJurisdiction");
        this.inputDoxaHandlesGit = this.querySelector("#doxaHandleGitlab");
        this.inputUserHandlesGit = this.querySelector("#handleGitMyself");
        this.inputGitlabPrivate = this.querySelector("#gitlabPrivate");
        this.inputGitlabPublic = this.querySelector("#gitlabPublic");

        // button handles
        this.buttonVerifyCredentials = this.querySelector("#buttonVerifyCredentials");

        // div handles for making div hidden or visible
        this.gitLabSettingsDiv = this.querySelector("#doxa-handles-git");
        this.groupInfo = this.querySelector("#groupInfo");
        this.publicDiv = this.querySelector("#publicDiv");
        this.divCopyright = this.querySelector("#divCopyright");
        this.divContact = this.querySelector("#divContact");
        this.inputJurisdictionDiv = this.querySelector("#inputJurisdictionDiv");
        this.submitMsgDiv = this.querySelector("#submitMsg");
        this.verificationMsgDiv = this.querySelector("#verificationMsg");
        this.uidPwdAccordionDiv = this.querySelector("#uidPwdAccordionDiv");

        // if running in the cloud, enable user ID and password accordion for the profile
        if (this.host.length > 0 && ! (this.host.startsWith("localhost") || this.host.startsWith("http://localhost"))) {
            this.uidPwdAccordionDiv?.classList.remove("d-none");
        }

        // error flagging handlers
        this.spanErrorGroupId = this.querySelector("#errorGroupId");
        this.spanErrorToken = this.querySelector("#errorToken");
        this.spanErrorLicense = this.querySelector("#errorLicense");
        this.spanErrorCopyright = this.querySelector("#errorCopyright");
        this.spanErrorJurisdication = this.querySelector("#errorJurisdication");

        // input field handlers
        this.anchorGroupUrl = this.querySelector("#anchorGroupUrl");

        this.inputGroupId.addEventListener('keyup', () => this.handleGroupIdChange());
        this.inputToken.addEventListener('keyup', () => this.handleTokenChange());
        this.inputCopyright.addEventListener('keyup', () => this.handleCopyrightChange());
        this.inputJurisdiction.addEventListener('keyup', () => this.handleJurisdictionChange());

        // radio button handlers
        this.inputUserHandlesGit.addEventListener('click', (event) => this.handleGitOnlyClick());
        this.inputDoxaHandlesGit.addEventListener('click', (event) => this.handleGitAndGitlabClick());
        this.querySelector('#gitlabPrivate').addEventListener('click', (event) => this.handleGitlabPrivateClick());
        this.querySelector('#gitlabPublic').addEventListener('click', (event) => this.handleGitlabPublicClick());

        // button handlers
        this.buttonVerifyCredentials.addEventListener('click', (event) => this.handleVerifyClick(event));

        // license handlers
        this.inputLicenseNotSet = this.querySelector('#license-not-set')
        this.inputLicenseCcBy = this.querySelector('#license-cc-by')
        this.inputLicenseCcBySa = this.querySelector('#license-cc-by-sa')
        this.inputLicenseCcByNd = this.querySelector('#license-cc-by-nd')
        this.inputLicenseCcByNc = this.querySelector('#license-cc-by-nc')
        this.inputLicenseCcByNcNd = this.querySelector('#license-cc-by-nc-nd')
        this.inputLicenseCcByNcSa = this.querySelector('#license-cc-by-nc-sa')
        this.inputLicenseCco = this.querySelector('#license-cco')
        this.inputLicensePdSj = this.querySelector('#license-pd-sj')
        this.inputLicensePdWw = this.querySelector('#license-pd-ww')

        this.inputLicenseNotSet.addEventListener('click', (event) => this.handleLicenseClick(this.licenses.NotSet));
        this.inputLicenseCcBy.addEventListener('click', (event) => this.handleLicenseClick(this.licenses.By));
        this.inputLicenseCcBySa.addEventListener('click', (event) => this.handleLicenseClick(this.licenses.BySa));
        this.inputLicenseCcByNd.addEventListener('click', (event) => this.handleLicenseClick(this.licenses.ByNd));
        this.inputLicenseCcByNc.addEventListener('click', (event) => this.handleLicenseClick(this.licenses.ByNc));
        this.inputLicenseCcByNcNd.addEventListener('click', (event) => this.handleLicenseClick(this.licenses.ByNCNd));
        this.inputLicenseCcByNcSa.addEventListener('click', (event) => this.handleLicenseClick(this.licenses.ByNcSa));
        this.inputLicenseCco.addEventListener('click', (event) => this.handleLicenseClick(this.licenses.CCO));
        this.inputLicensePdSj.addEventListener('click', (event) => this.handleLicenseClick(this.licenses.PdSj));
        this.inputLicensePdWw.addEventListener('click', (event) => this.handleLicenseClick(this.licenses.PdWw));

        this.querySelector("#buttonSubmit").addEventListener('click', () => this.handleSubmitGit());
        this.requestGroup();
    }

    disconnectedCallback() {
        this.inputGroupId.removeEventListener('keyup', () => this.handleGroupIdChange());
        this.inputToken.removeEventListener('keyup', () => this.handleTokenChange());
        this.inputUserHandlesGit.removeEventListener('click', (event) => this.handleGitOnlyClick());
        this.inputDoxaHandlesGit.removeEventListener('click', (event) => this.handleGitAndGitlabClick());
        this.querySelector('#gitlabPrivate').removeEventListener('click', (event) => this.handleGitlabPrivateClick());
        this.querySelector('#gitlabPublic').removeEventListener('click', (event) => this.handleGitlabPublicClick());
        this.buttonVerifyCredentials.removeEventListener('click', (event) => this.handleVerifyClick(event));
        this.inputLicenseNotSet.removeEventListener('click', (event) => this.handleLicenseClick(this.licenses.NotSet));
        this.inputLicenseCcBy.removeEventListener('click', (event) => this.handleLicenseClick(this.licenses.By));
        this.inputLicenseCcBySa.removeEventListener('click', (event) => this.handleLicenseClick(this.licenses.BySa));
        this.inputLicenseCcByNd.removeEventListener('click', (event) => this.handleLicenseClick(this.licenses.ByNd));
        this.inputLicenseCcByNc.removeEventListener('click', (event) => this.handleLicenseClick(this.licenses.ByNc));
        this.inputLicenseCcByNcNd.removeEventListener('click', (event) => this.handleLicenseClick(this.licenses.ByNCNd));
        this.inputLicenseCcByNcSa.removeEventListener('click', (event) => this.handleLicenseClick(this.licenses.ByNcSa));
        this.inputLicenseCco.removeEventListener('click', (event) => this.handleLicenseClick(this.licenses.CCO));
        this.inputLicensePdSj.removeEventListener('click', (event) => this.handleLicenseClick(this.licenses.PdSj));
        this.inputLicensePdWw.removeEventListener('click', (event) => this.handleLicenseClick(this.licenses.PdWw));
        this.querySelector("#buttonSubmit").removeEventListener('click', () => this.handleSubmitGit());
    }

    setErrorSpan(span, msg) {
        if (msg && msg.length === 0) {
            this.clearErrorSpan(span);
            return;
        }
        span.innerHTML = `<span style="color: red;"><i class="bi bi-arrow-left"></i>&nbsp;${msg}</span>`;
    }
    clearErrorSpan(span) {
        span.innerHTML = ``;
    }
    clearErrorSpans() {
        this.spanErrorGroupId.innerHTML = "";
        this.spanErrorToken.innerHTML = "";
        this.spanErrorLicense.innerHTML = "";
        this.spanErrorCopyright.innerHTML = "";
        this.spanErrorJurisdication.innerHTML = "";
    }

    handleGroupIdChange() {
        this.groupIdValid();
        const disabled =this.inputToken.value === this.token && this.inputGroupId === this.groupId;
        this.buttonVerifyCredentials.disabled = disabled;
        if (! disabled) {
            this.inputToken.value = "" // force them to enter it
            this.setErrorSpan(this.spanErrorToken, "reenter token since group ID changed");
            this.verificationMessage(utils.alerts.Warning, `group ID and token not verified`);
        }
    }
    groupIdValid() {
        const val = this.inputGroupId.value;
        const span = this.spanErrorGroupId;
        if (! this.gitlabPrivate) {
            if (val.length === 0) {
                this.setErrorSpan(span, "required if you want to share your work with the public");
                return false;
            }
            if (!utils.isPositiveInteger(val)) {
                this.setErrorSpan(this.spanErrorGroupId, "must be a number");
                return false;
            }
        }
        this.clearErrorSpan(span, "")
        return true;
    }

    handleTokenChange() {
        this.tokenValid();
        const disabled =this.inputToken.value === this.token && this.inputGroupId === this.groupId;
        this.buttonVerifyCredentials.disabled = disabled;
        if (! disabled) {
            this.verificationMessage(utils.alerts.Warning, `group ID and token not verified`);
        }
    }
    tokenValid() {
        const val = this.inputToken.value;
        const span = this.spanErrorToken;
        if (! this.gitlabPrivate) {
            if (val.length === 0) {
                this.setErrorSpan(span, "required if you want to share your work with the public")
                return false;
            }
        }
        this.clearErrorSpan(span, "")
        return true;
    }

    handleCopyrightChange() {
        this.copyrightValid();
    }
    copyrightValid() {
        const val = this.inputCopyright.value;
        const span = this.spanErrorCopyright
        if (this.copyrightRequired()) {
            if (val.length === 0) {
                const span = this.spanErrorCopyright
                this.setErrorSpan(span, "required if you want to share your work with the public")
                return false;
            }
        }
        this.clearErrorSpan(span, "")
        return true;
    }
    copyrightRequired() {
        if (this.gitlabPrivate) {
            return false;
        }
        if (this.inputLicenseValue === this.licenses.NotSet
            || this.inputLicenseValue === this.licenses.PdSj
            || this.inputLicenseValue === this.licenses.PdWw) {
            console.log("copyright not required")
            return false;
        }
        console.log("copyright required")
        return true;
    }
    handleJurisdictionChange() {
        this.jurisdictionValid();
    }
    jurisdictionValid() {
        const val = this.inputJurisdiction.value;
        const span = this.spanErrorJurisdication
        if (this.jurisdictionRequired()) {
            if (val.length === 0) {
                this.setErrorSpan(span, "required for license Public Domain (within specified jurisdictions)")
                return false;
            }
        }
        this.clearErrorSpan(span, "")
        return true;
    }
    jurisdictionRequired() {
        if (this.gitlabPrivate) {
            return false;
        }
        if (this.inputLicenseValue === this.licenses.PdSj) {
            return true;
        }
        return false;
    }
    handleVerifyClick(ev) {
        ev.stopPropagation();
        this.groupId = this.inputGroupId.value;
        this.token = this.inputToken.value;
        if (this.groupId.length === 0 || this.token.length === 0) {
            this.verificationMessage(utils.alerts.Danger, "You must enter both the Gitlab Group ID and your Personal Access Token.")
            return;
        }
        this.requestVerification();
    }
    handleLicenseClick(name) {
        switch (name) {
            case this.licenses.NotSet: {
                this.clearCopyrightAndContact();
            }
            case this.licenses.PdWw: {
                this.clearCopyrightAndContact();
                break;
            }
            case this.licenses.PdSj: {
                this.clearCopyrightAndContact();
                this.inputJurisdictionDiv?.classList.remove("d-none");
                break;
            }
            default:
                this.inputJurisdictionDiv?.classList.add("d-none");
                this.inputJurisdiction.value = "";
                this.enableCopyrightAndContact();
        }
        this.inputLicenseValue = name
        this.licenseValid();
    }
    licenseValid() {
        let span = this.spanErrorLicense;
        if ((!this.gitlabPrivate) && this.inputLicenseValue === this.licenses.NotSet) {
            this.setErrorSpan(span, "you must select a license")
            return false;
        }
        this.clearErrorSpan(span, "")
        return true;
    }
    setLicenseSelection(name) {
        this.divCopyright.classList.remove("d-none");
        this.divContact?.classList.remove("d-none");
        this.inputLicenseValue = name;
        switch (name) {
            case this.licenses.By: {
                this.inputLicenseCcBy.checked = true;
                break;
            }
            case this.licenses.ByNc: {
                this.inputLicenseCcByNc.checked = true;
                break;
            }
            case this.licenses.ByNCNd: {
                this.inputLicenseCcByNcNd.checked = true;
                break;
            }
            case this.licenses.ByNcSa: {
                this.inputLicenseCcByNcSa.checked = true;
                break;
            }
            case this.licenses.ByNd: {
                this.inputLicenseCcByNd.checked = true;
                break;
            }
            case this.licenses.BySa: {
                this.inputLicenseCcBySa.checked = true;
                break;
            }
            case this.licenses.CCO: {
                this.inputLicenseCco.checked = true;
                break;
            }
            case this.licenses.NotSet: {
                this.divCopyright.classList.add("d-none");
                this.divContact.classList.add("d-none");
                this.inputLicenseNotSet.checked = true;
                break;
            }
            case this.licenses.PdSj: {
                this.clearCopyrightAndContact();
                this.inputLicensePdSj.checked = true;
                this.inputJurisdictionDiv?.classList.remove("d-none");
                this.divCopyright.classList.add("d-none");
                this.divContact?.classList.add("d-none");
                break;
            }
            case this.licenses.PdWw: {
                this.clearCopyrightAndContact();
                this.inputLicensePdWw.checked = true;
                this.divCopyright.classList.add("d-none");
                this.divContact?.classList.add("d-none");
                break;
            }
            default: {
                this.inputLicenseNotSet.checked = true;
                this.divCopyright.classList.add("d-none");
                this.divContact?.classList.add("d-none");
            }
        }
        this.licenseValid();
    }
    clearCopyrightAndContact() {
        this.inputCopyright.value = "";
        this.inputContact.value = "";
        this.divCopyright.classList.add("d-none");
        this.divContact?.classList.add("d-none");
        this.clearErrorSpan(this.spanErrorCopyright);
        this.clearErrorSpan(this.spanErrorJurisdication);
    }
    enableCopyrightAndContact() {
        this.divCopyright.classList.remove("d-none");
        this.divContact?.classList.remove("d-none");
        this.clearErrorSpan(this.spanErrorCopyright);
        this.clearErrorSpan(this.spanErrorJurisdication);
    }
    handleGitOnlyClick() {
        this.doxaHandleGitlab = false;
        this.gitLabSettingsDiv.classList.add("d-none");
    }

    handleGitAndGitlabClick() {
        this.doxaHandleGitlab = true;
        this.gitLabSettingsDiv.classList.remove("d-none");
    }

    handleGitlabPrivateClick() {
        this.clearErrorSpan(this.spanErrorCopyright);
        this.clearErrorSpan(this.spanErrorJurisdication);
        this.gitlabPrivate = true;
        this.inputJurisdiction.value = "";
        this.inputCopyright.value = "";
        this.inputContact.value = "";
        this.setLicenseSelection(this.licenses.NotSet)
        this.publicDiv.classList.add("d-none");
    }

    handleGitlabPublicClick() {
        this.clearErrorSpan(this.spanErrorCopyright);
        this.clearErrorSpan(this.spanErrorJurisdication);
        this.gitlabPrivate = false;
        this.publicDiv.classList.remove("d-none");
        this.setLicenseSelection(this.licenses.NotSet)
    }

    setProfileGit(profile) {
        if (profile?.Gitlab?.DoxaHandlesGitlab) {
            this.inputDoxaHandlesGit.checked = true;
            this.handleGitAndGitlabClick();
            const gitlab = profile?.Gitlab;
            if (gitlab !== null) {
                if (gitlab?.Verified) {
                    this.token = "************"
                    this.inputToken.value = this.token
                    this.buttonVerifyCredentials.disabled = true;
                    this.verificationMessage(utils.alerts.Success, `group ID and token verified. Don't forget to save your Git and Gitlab preferences using the button down below.`);
                }
                const group = profile?.Gitlab?.GitlabRootGroup
                if (group !== null) {
                    this.inputGroupId.value = group.ID;
                    this.setGroupUrl(group.WebURL);
                }
                if (profile?.Gitlab?.GitlabPublic) {
                    this.inputGitlabPublic.checked = true;
                    this.gitlabPrivate = false;
                    this.handleGitlabPublicClick();
                } else {
                    this.inputGitlabPrivate.checked = true;
                    this.gitlabPrivate = true;
                    this.handleGitlabPrivateClick();
                }
                if (profile?.Gitlab?.License) {
                    this.setLicenseSelection(profile.Gitlab.License);
                }
                if (profile?.Gitlab?.Copyright) {
                    this.inputCopyright.value = profile.Gitlab.Copyright;
                }
                if (profile?.Gitlab?.Contact) {
                    this.inputContact.value = profile.Gitlab.Contact;
                }
                if (profile?.Gitlab?.Jurisdiction) {
                    this.inputJurisdiction.value = profile.Gitlab.Jurisdiction;
                }
            }
        } else {
            this.inputUserHandlesGit.checked = true;
            this.handleGitOnlyClick();
        }
    }
    // gitFieldsValid returns true if all are valid.
    // it also flags each field that has an error.
    gitFieldsValid() {
        if (this.inputDoxaHandlesGit.checked) {
            let groupOk = this.groupIdValid();
            let tokenOk = this.tokenValid();
            let licenseOk = this.licenseValid();
            let copyrightOk = this.copyrightValid();
            let jurisdictionOk = this.jurisdictionValid();
            return groupOk && tokenOk && licenseOk && copyrightOk && jurisdictionOk
        }
        return true;
    }
    hideGroupUrl() {
        this.groupUrl = "";
        this.groupInfo.classList.add("d-none");
    }
    setGroupUrl(url) {
        this.groupUrl = url;
        this.anchorGroupUrl.href = this.groupUrl;
        this.anchorGroupUrl.innerText = this.groupUrl;
        this.groupInfo.classList.remove("d-none");
    }
    // ---- REST API Handlers

    requestGroup() {
        const url = this.host
            + `api/user/profile`
        ;

        utils.callApi(url, "GET").then(response => {
            if (response.Status === "OK") {
                this.setProfileGit(response.Profile)
            } else {
                alert(response.Message);
            }
        }).catch(error => {
            alert(`error: ${error}`);
        });
    }

    handleSubmitGit() {
        this.clearErrorSpans();
        if (! this.gitFieldsValid()) {
            this.submitMessage(utils.alerts.Danger, `Please correct the data fields above that are marked in red.`)
            return;
        }
        this.submitMessage(utils.alerts.Primary, "submitting profile...")
        const url = this.host
            + `api/user/profile?`
            + `copyright=${this.inputCopyright.value}`
            + `&contact=${this.inputContact.value}`
            + `&doxaHandlesGit=${this.inputDoxaHandlesGit.checked}`
            + `&gitlabPrivate=${this.inputGitlabPrivate.checked}`
            + `&groupId=${this.inputGroupId.value}`
            + `&jurisdiction=${this.inputJurisdiction.value}`
            + `&license=${this.inputLicenseValue}`
            + `&token=${this.inputToken.value}`
        ;

        utils.callApi(url, "PUT").then(response => {
            if (response.Status === "OK") {
                this.submitMessage(utils.alerts.Success, `your git and gitlab preferences have been saved`);
            } else {
                this.submitMessage(utils.alerts.Danger, `${response.Message}`)
            }
        }).catch(error => {
            this.submitMessage(utils.alerts.Danger, `error: ${error}`)
        });
    }

    requestVerification() {
        this.verificationMessage(utils.alerts.Primary, "requesting validation...")
        const url = this.host
            + `api/dvcs/credentials/validate?`
            + `groupId=${this.groupId}`
            + `&token=${this.token}`
        ;

        utils.callApi(url, "GET").then(response => {
            if (response.Status === "OK") {
                this.buttonVerifyCredentials.disabled = true;
                this.verificationMessage(utils.alerts.Success, `group ID and token verified.  Don't forget to save your Git and Gitlab preferences using the button down below.`);
                if (response?.GitlabRootGroup?.WebURL) {
                    this.setGroupUrl(response.GitlabRootGroup.WebURL);
                }
            } else {
                this.hideGroupUrl();
                this.verificationMessage(utils.alerts.Danger, `${response.Message}`)
            }
        }).catch(error => {
            this.verificationMessage(utils.alerts.Danger, `error: ${error}`)
        });
    }

    submitMessage(type, message) {
        this.submitMsgDiv.className = "";
        this.submitMsgDiv.classList.add(type);
        this.submitMsgDiv.innerHTML = utils.getMessageSpan(type, message);
    }
    verificationMessage(type, message) {
        this.verificationMsgDiv.className = "";
        this.verificationMsgDiv.classList.add(type);
        this.verificationMsgDiv.innerHTML = utils.getMessageSpan(type, message);
    }
// ---- End REST API Handlers

    static get observedAttributes() {
        return ['host'];
    }

    //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
    }

    adoptedCallback() {
    }

    get host() {
        return this.getAttribute("host");
    }

    set host(value) {
        this.setAttribute("host", value);
    }

    get vault() {
        return this.getAttribute("vault");
    }

    set vault(value) {
        this.setAttribute("vault", value);
    }

}

window.customElements.define('user-profile', UserProfile);