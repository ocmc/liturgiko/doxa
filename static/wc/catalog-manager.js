import * as utils from "./utils.js";

class CatalogManager extends HTMLElement {
  constructor() {
    super();
    console.log("static/wc/catalog-manager.js")
    this.catalog = {}
  }

  connectedCallback() {
    this.innerHTML =
`
<style> 
  @import url('static/css/bootstrap.min.css');
  @import url('static/img/bootstrap-icons.css');
</style>
<div id="my-element-div" class="wc-title">Catalog Manager</div>
</div>
`;
  }

  disconnectedCallback() {
  }

  handleCatalogRequest() {
    this.requestCatalog(this.libraries).then(response => {
      if (response.Status === "OK") {
        this.divSynchAllStatus.innerText = "Synch finished OK"
      } else {
        this.divSynchAllStatus.innerText = response.Status
      }
    }).catch(error => {
      alert(error);
    });
  }

  renderData() {

  }
  // ---- REST API Handlers

  requestCatalog() {
    const url = this.host
        + `api/catalog`
    ;

    utils.callApi(url, "GET").then(response => {
      if (response.Status === "OK") {
        if (! response.Catalog) {
          return;
        }
        this.catalog = response.Catalog
        this.renderData();
      } else {
        alert(response.Status)
      }
    }).catch(error => {
      alert(error);
    });
  }
// ---- End REST API Handlers

  static get observedAttributes() {
    return ['host'];
  }


//attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
attributeChangedCallback(host, oldVal, newVal) {
    console.log(`Attribute: ${host} changed!`);
  }

  adoptedCallback() {
  }
  get host() {
    return this.getAttribute("host");
  }

  set host(value) {
    this.setAttribute("host", value);
  }

}

window.customElements.define('catalog-manager', CatalogManager);