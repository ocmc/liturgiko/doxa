import * as utils from "./utils.js";

class TemplateFallback extends HTMLElement {
  constructor() {
    super();
    console.log("static/wc/template-fallback.js");
    this.spanEnabled = null;
    this.spanGroupName = null;
    this.enabled = false;
    this.GitLabGroupName = "";
  }

  connectedCallback() {
    this.innerHTML =
`
    <style> 
      @import url('static/css/bootstrap.min.css');
      @import url('static/img/bootstrap-icons.css');
    </style>
    <div style="font-weight: bold"><span id="spanEnabled"></span>&nbsp;<span id="spanGroupName"></span></div>
`;
    this.spanEnabled = this.querySelector("#spanEnabled");
    this.spanGroupName = this.querySelector("#spanGroupName");
    this.getFallback();
  }

  disconnectedCallback() {
  }

  getFallback() {
    this.requestFallback().then(response => {
      if (response.Status === "OK") {
        console.log("33 status OK")
          this.enabled = response.FallbackEnabled;
          if (this.enabled) {
            this.spanEnabled.innerText = "Fallback is enabled";
            this.GitLabGroupName = response.FallbackSelected;
            this.spanGroupName.innerText = `and uses your subscription to ${this.GitLabGroupName}.`;
          } else {
            this.GitLabGroupName = "";
            this.spanEnabled.innerText = "Fallback is disabled.";
        }
      } else {
        console.log("44 status Not OK")
        alert(`error retrieving fallback information: ${response.Status} ${response.Message}`);
      }
    }).catch(error => {
      console.log("caught error")
      alert(JSON.stringify(error));
    });
  }

  // ---- REST API Handlers

  async requestFallback() {
    const url = this.host
        + `api/lml/fallback`
    ;
    let fetchData = {
      method: 'GET'
    }

    const response = await fetch(url, fetchData);
    if (!response.ok) {
      let message = "";
      switch (response.status) {
        case 404: {
          message += "not found";
          break;
        }
        case 500: {
          message += "doxa server error";
          break;
        }
        default: {
          message += `An error has occurred: ${response.status}`;
        }
      }
      console.log("throwing error line 76")
      throw new Error(message);
    }
    const responseJson = await response.json();
    return responseJson;
  }
// ---- End REST API Handlers

  static get observedAttributes() {
    return ['host'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
  }

  adoptedCallback() {
  }
  get host() {
    return this.getAttribute("host");
  }

  set host(value) {
    this.setAttribute("host", value);
  }
}

window.customElements.define('template-fallback', TemplateFallback);