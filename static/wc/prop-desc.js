class PropDesc extends HTMLElement {
  constructor() {
    super();
    console.log("static/wc/prop-desc.js")
  }

  connectedCallback() {
    this.innerHTML =
`
<style> 
  @import url('static/css/bootstrap.min.css');
  @import url('static/img/bootstrap-icons.css');
</style>
    <div class="row p-1">
        <div id="property-desc"></div>
    </div>
`;
    this.fetchRecord().then();
  }

  disconnectedCallback() {
  }

  static get observedAttributes() {
    return ['recId'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
  }

  adoptedCallback() {
  }
  get recId() {
    return this.getAttribute("recId");
  }

  set recId(value) {
    this.setAttribute("recId", value);
  }

  async fetchRecord() {
    const url = `api/db/read?id=` + encodeURIComponent(this.recId);
    return new Promise(async (res, rej) => {
      await fetch(url)
          .then(data => data.json())
          .then((json) => {
            this.setDescription(json);
            res();
          })
          .catch((error) => rej(error));
    })
  }
  setDescription(json) {
    if (json.Value && json.Value.length > 0) {
      this.querySelector("#property-desc").innerHTML = `<help-info text="${json.Value}"></help-info>`;
    }
  }

}

window.customElements.define('prop-desc', PropDesc);