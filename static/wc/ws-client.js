class WebSocketClient extends HTMLElement {
  // generates custom event wsMessage
  constructor() {
    super();
    console.log("static/wc/ws-client.js")
    this.log = null;
    this.conn = new WebSocket("ws://" + document.location.host + this.endpoint);
  }

  connectedCallback() {
    this.innerHTML = `
    <style> 
      @import url('static/css/bootstrap.min.css');
      @import url('static/img/bootstrap-icons.css');
    </style>
    <style>
      .control-label {
        color: #5bc0de;
      }
     .content {
          flex: 1;
          display: flex;
          overflow: auto;
     }

      .box {
          display: flex;
          flex-direction: column;
          min-height: min-content; 
          font-family:courier, monospace;
          background-color: black;
          color: white;
          padding: 10px 10px 10px 10px;
      }
    </style>
    <div><em>${this.title}</em></div>
    <div class="content">
      <div id="log" class="box"></div>
    </div>
    `;

    this.log = this.querySelector("#log");
    this.listenToSocket();
  }

  disconnectedCallback() {
    this.conn.close();
  }

  static get observedAttributes() {
    return ['endpoint', 'title'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
    switch (name) {
      case "endpoint": {
        if (newVal) {
          if (oldVal && oldVal !== newVal) {
            this.endpoint = newVal;
          }
        }
        break;
      }
      case "title": {
        if (newVal && newVal !== null) {
          if (oldVal && oldVal !== newVal) {
            this.title = newVal;
          }
        }
      }
    }
  }

  adoptedCallback() {
  }

  appendMsg(msg) {
    const item = document.createElement("div");
    item.classList.add("message");
    item.innerText = msg;
    const msgLower = msg.toLowerCase();
    if (msgLower.includes("error")) {
      item.style.color = "red";
    } else if (msgLower.includes("warning")) {
      item.style.color = "gold";
    }
    this.log.appendChild(item);
  };

  listenToSocket() {
    if (window["WebSocket"]) {
      this.conn.onclose = function (evt) {
        const item = document.createElement("div");
        item.innerHTML = "<b>Connection closed.</b>";
        this.log.appendChild(item);
      };
      this.conn.onmessage =  (evt) => {
        const messages = evt.data.split('\n');
        for (let i = 0; i < messages.length; i++) {
          let theMessage = messages[i].trim();
          let theMessageLower = theMessage.toLowerCase()
          if (theMessageLower === "clear") {
            this.log.innerHTML = "";
          } else if (theMessageLower.startsWith("gm: parsing template")) {
            this.dispatchEvent(new CustomEvent('wsMessage', {
              detail: {
                name: 'wsMessage', // this is the name to use in handler
                msg: theMessage,
              },
              bubbles: true
            }));
          } else if (theMessageLower.startsWith("gm: sync")) {
            if (theMessageLower.endsWith("finished")) {
              this.dispatchEvent(new CustomEvent('wsNotice', {detail: {
                  name: 'wsNotice',
                  hide: true,
                }, bubbles: true}))
            }else {
              this.dispatchEvent(new CustomEvent('wsNotice', {
                detail: {
                  name: 'wsNotice', // this is the name to use in handler
                  msg: theMessage,
                  hide: false,
                },
                bubbles: true
              }));
            }
          } else if (theMessageLower.startsWith("gm: finished parsing")) {
            // ignore
          } else if (theMessageLower.startsWith("gm: wrote html to")) {
            // ignore
          } else if (theMessageLower.startsWith("gm: wrote pdf to")) {
            // ignore
          } else if (theMessageLower === "gm:") {
            // ignore
          } else if (theMessageLower.startsWith("gm: template ") &&
              (theMessageLower.endsWith("error")
                  || theMessageLower.endsWith("errors") )) {
            if (theMessage.length > 14) {
              let id = theMessage.slice(13, theMessage.length-1)
              let parts = id.split(" ")
              if (parts.length > 1) {
                id = parts[0];
                let theMessageFirstPart = `GM: template ${id}}`
                let theMessageLastPart = theMessage.slice(theMessageFirstPart.length-1)
                const item = document.createElement("div");
                item.innerHTML = `<span>GM: template <a style="color: red;" href="ide?id=${id}" target="_blank">${id}</a> ${theMessageLastPart}</span>`
                this.log.appendChild(item);
              }
            } else {
              this.appendMsg(theMessage)
            }
          } else {
            this.appendMsg(theMessage)
          }
        }
      };
    } else {
      const item = document.createElement("div");
      item.innerHTML = "<b>Your browser does not support WebSockets, so Doxa can't give you generation messages.</b>";
      this.log.appendChild(item);
    }
  }

  get endpoint() {
    return this.getAttribute("endpoint");
  }

  set endpoint(value) {
    this.setAttribute("endpoint", value);
  }
  get title() {
    return this.getAttribute("title");
  }

  set title(value) {
    this.setAttribute("title", value);
  }
}

window.customElements.define('web-socket-client', WebSocketClient);