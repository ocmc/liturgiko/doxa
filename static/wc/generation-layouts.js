import * as utils from "./utils.js";

class GenerationLayouts extends HTMLElement {
  // generates custom event "combosaved"
  // generates custom event "comboSelectionChanged"
  constructor() {
    super();
    console.log("static/wc/generation-layouts.js")
    this.acronymList = null;
    this.acronymSpan = null;
    this.colArrayLength = 10;
    this.columnLayoutMap = null;
    this.container = null;
    this.data = null;
    this.deleteBtn = null;
    this.indexTitleDiv = null;
    this.indexTitle = "" +
        "" +
        "Select a layout...";
    this.layoutAction = "";
    this.layoutMatrix = []; // will be 10 rows x 3 columns
    this.layouts = [];
    this.libraries = []; // for the library select component
    this.libraryUndoStack = [];
    this.modalConfirm = null;
    this.newLayout = false;
    this.rowTitles = [];
    this.rowTypes = new Array("Liturgical", "Epistle", "Gospel", "Prophet", "Psalter");
    this.saveBtn = null;
    this.selectedLibraryComboKey = "selectedLibraryCombo"
    this.table = null;
    this.undoAllBtn = null;
    this.modalHelp = null;
    this.helpBtn = null;
    this.selectCol1LangCode = null;
    this.selectCol2LangCode = null;
    this.selectCol3LangCode = null;
    this.selected = "";
  }

  connectedCallback() {
    this.innerHTML = `
    <style> 
      @import url('static/css/bootstrap.min.css');
      @import url('static/img/bootstrap-icons.css');
    </style>
    <style>
        li{
           cursor: pointer;
        }
        .info {
          color: cornflowerblue;
        }
        .list-group, .list-item {
        }
        th {
            text-align: center !important;
        }
        .wait {
            cursor: wait !important;
        }
        .layouts-title {
            text-align: right;
        }
        .flex-parent > * { 
            flex: 1; 
        }
        .fa-clickable {
            cursor:pointer;
            outline:none;
        }
        .tfoot > * {
            background-color: green;
            font-size: small !important;
        }
    </style>
    <div id="genLayoutsContainer" class="container container-fluid">
        <div class="row">
            <div class="col-2">
                <div id="layouts-title-div" class="info layouts-title">${this.indexTitle}</div>
            </div>
            <div class="col-4">
              <ol id="acronym-list" class="list-group  list-group-numbered"></ol>
            </div>
            <div class="col-6 alert-primary">
                The layout manager allows you to specify the high level layout of a generated book or service.
                A book or service can be generated with one, two, or three columns side-by-side.
                Note that the order (sequence) of language combinations in a layout, e.g. gr-en, must be unique. 
                You can't have two layouts with the same sequence of languages. For example, you can't add a duplicate
                gr-en, but you could add en-gr if it does not already exist. Note that changes you make to the column for a specific language code change it for all other layouts using that code.
            </div>
        </div>
        <hr/>
        <div class="row p-1">
          <p class="alert-info">Generation Layout for <span id="layoutAcronym"></span>&nbsp;<i id="helpBtn" class="bi-question-circle fa-clickable" style="color: black; font-weight: bold;"></i></p>
          <table class="table table-striped">
              <thead>
                  <tr>
                  <th colspan="2">Type</th>
                  <th>Column 1
                    <select class="form-select" id="col1LangCodeSelect" aria-label="Default select example">
                    </select>
                  </th>
                  <th>Column 2
                    <select class="form-select"  id="col2LangCodeSelect" aria-label="Default select example">
                    </select>
                  </th>
                  <th>Column 3
                    <select class="form-select"  id="col3LangCodeSelect" aria-label="Default select example">
                    </select>
                  </th>
                  </tr>
              </thead>
              <tbody id="layoutTable"></tbody>  
              <tfoot>
                  <tr>
                    <th colspan="2" style="font-size: xx-small !important; ">
                        <button id="saveBtn" type="button" class="btn btn-sm btn-primary disabled" data-bs-toggle="tooltip" data-bs-placement="top" title="            Save layout"><i class="bi-save" style="color: white;"></i></button>
                        <button id="undoAllBtn" type="button" class="btn btn-sm btn-warning disabled"data-bs-toggle="tooltip" data-bs-placement="top" title="            Undo all changes"></i><i class="bi-arrow-counterclockwise" style="color: black;"></i></button>
                        <button id="deleteBtn" type="button" class="btn btn-sm btn-danger "data-bs-toggle="tooltip" data-bs-placement="top" title="            Delete layout"><i class="bi-trash" style="color: white;"></i></button>
                        <button id="newBtn" type="button" class="btn btn-sm btn-primary "data-bs-toggle="tooltip" data-bs-placement="top" title="            New layout"><i class="bi-plus-lg" style="color: white;"></i></button>
                    </th>
                    <th>
                      <div class="btn-group" role="toolbar" aria-label="column one toolbar">
                        <button id="col0MoveLeft" type="button" class="btn btn-sm btn-outline-primary"><i class="bi-chevron-double-left" data-action="moveLeftCol0" data-bs-toggle="tooltip" data-bs-placement="top" title="            Move left"></i></button>
                        <button id="col0Clear" type="button" class="btn btn-sm btn-outline-primary"><i class="bi-x-square" data-action="clearCol1"data-bs-toggle="tooltip" data-bs-placement="top" title="            Clear libraries"></i></button>
                        <button id="col0MoveRight" type="button" class="btn btn-sm btn-outline-primary"><i class="bi-arrow-left-right" data-action="moveRightCol1"data-bs-toggle="tooltip" data-bs-placement="top" title="            Move right"></i></button>
                      </div>
                    </th>
                    <th>
                      <div class="btn-group" role="toolbar" aria-label="column two toolbar">
                        <button id="col1MoveLeft" type="button" class="btn btn-sm btn-outline-primary"><i class="bi-arrow-left-right" data-action="moveLeftCol2" data-bs-toggle="tooltip" data-bs-placement="top" title="            Move left"></i></button>
                        <button id="col1Clear" type="button" class="btn btn-sm btn-outline-primary"><i class="bi-x-square" data-action="clearCol1" data-bs-toggle="tooltip" data-bs-placement="top" title="            Clear libraries"></i></button>
                        <button id="col1MoveRight" type="button" class="btn btn-sm btn-outline-primary"><i class="bi-arrow-left-right" data-action="moveRightCol1" data-bs-toggle="tooltip" data-bs-placement="top" title="            Move right"></i></button>
                      </div>
                    </th>
                    <th>
                      <div class="btn-group" role="toolbar" aria-label="column three toolbar">
                        <button id="col2MoveLeft" type="button" class="btn btn-sm btn-outline-primary"><i class="bi-arrow-left-right" data-action="moveLeftCol2" data-bs-toggle="tooltip" data-bs-placement="top" title="            Move left"></i></button>
                        <button id="col2Clear" type="button" class="btn btn-sm btn-outline-primary"><i class="bi-x-square" data-action="clearCol1" data-bs-toggle="tooltip" data-bs-placement="top" title="            Clear libraries"></i></button>
                        <button id="col2MoveRight" type="button" class="btn btn-sm btn-outline-primary"><i class="bi-chevron-double-right" data-action="moveRightCol2" data-bs-toggle="tooltip" data-bs-placement="top" title="            Move right"></i></button>
                      </div>
                    </th>
                  </tr>
              </tfoot>      
          </table>
        </div>
    </div>
    <modal-confirm 
    class="d-none"
    id="gen-layout-modal-confirm"
    title=""
    msg1=""
    msg2=""
    msg3=""
    cancelBtn="Cancel"
    okBtn="OK"
    passThrough=""
    ></modal-confirm>
    <!-- Modal Help -->
    <modal-icon-help
        id="layouts-modal-icon-help"
        class="d-none"
        firstDivText="The page beneath this is the layout manager page.  The layout manager allows you to specify the high level layout of a generated book or service. A book or service can be generated with one, two, or three columns side-by-side."
        lastDivText="Note that the order (sequence) of language combinations in a layout, e.g. gr-en, must be unique. You can't have two layouts with the same sequence of languages. For example, you can't add a duplicate gr-en, but you could add en-gr if it does not already exist.   Note that changes you make to the column for a specific language code change it for all other layouts using that code."
        title="Layout Manager: Icon Help">
    </modal-icon-help>

    `
    // see if there is a selected acronym in local storage
    // if not found, a down-stream function will select the first item in the acronym list
    const selectedLayout = localStorage.getItem(this.selectedLibraryComboKey);
    if (selectedLayout !== null && selectedLayout.length > 0) {
      this.selected = selectedLayout;
    }


    this.helpBtn = this.querySelector("#helpBtn");
    this.modalHelp = this.querySelector("#layouts-modal-icon-help");
    this.container = this.querySelector("#genLayoutsContainer");
    this.acronymList = this.querySelector("#acronym-list");
    this.indexTitleDiv = this.querySelector("#layouts-title-div");

    // list of layouts
    this.acronymList.addEventListener('click', (ev) => this.handleListClick(ev));
    this.requestLayoutIndexes();

    // selected layout
    this.initRowTitles();
    this.initLayoutMatrix();
    this.table = this.querySelector("#layoutTable");
    this.deleteBtn = this.querySelector("#deleteBtn");
    this.saveBtn = this.querySelector("#saveBtn");
    this.undoAllBtn = this.querySelector("#undoAllBtn");
    this.acronymSpan = this.querySelector("#layoutAcronym");
    this.modalConfirm = this.querySelector("#gen-layout-modal-confirm");

    // column language code selectors
    this.selectCol1LangCode = this.querySelector("#col1LangCodeSelect");
    this.selectCol2LangCode = this.querySelector("#col2LangCodeSelect");
    this.selectCol3LangCode = this.querySelector("#col3LangCodeSelect");
    // column language event handlers
    this.selectCol1LangCode.addEventListener("change", (ev) => this.handleColumnLangCodeChange(ev, 1))
    this.selectCol2LangCode.addEventListener("change", (ev) => this.handleColumnLangCodeChange(ev, 2))
    this.selectCol3LangCode.addEventListener("change", (ev) => this.handleColumnLangCodeChange(ev, 3))

    this.modalHelp.addEventListener('click', (ev) => this.handleModalIconHelpClick(ev));
    this.helpBtn.addEventListener("click", (e) => this.showModalIconHelp(e));
    this.modalConfirm.addEventListener('click', ev => this.handleConfirmClick(ev));
    this.table.addEventListener("change", (ev) => this.handleTableChange(ev));
    this.deleteBtn.addEventListener("click", (ev) => this.confirmDelete(ev));
    this.saveBtn.addEventListener("click", (ev) => this.handleSave(ev));
    this.undoAllBtn.addEventListener("click", (ev) => this.handleUndoAll(ev));
    this.querySelector("#newBtn").addEventListener("click", (ev) => this.confirmNewLayout(ev));
    this.querySelector("#col0MoveLeft").addEventListener("click", (ev) => this.wrapColsLeft(ev));
    this.querySelector("#col0Clear").addEventListener("click", (ev) => this.colToNone(ev, 0));
    this.querySelector("#col0MoveRight").addEventListener("click", (ev) => this.swapCols(ev, 0,1));
    this.querySelector("#col1MoveLeft").addEventListener("click", (ev) => this.swapCols(ev,0,1));
    this.querySelector("#col1Clear").addEventListener("click", (ev) => this.colToNone(ev, 1));
    this.querySelector("#col1MoveRight").addEventListener("click", (ev) => this.swapCols(ev,1,2));
    this.querySelector("#col2MoveLeft").addEventListener("click", (ev) => this.swapCols(ev,1,2));
    this.querySelector("#col2Clear").addEventListener("click", (ev) => this.colToNone(ev,2));
    this.querySelector("#col2MoveRight").addEventListener("click", (ev) => this.wrapColsRight(ev));

    this.getLibraries();
  }
  disconnectedCallback() {
    this.modalHelp.removeEventListener('click', (ev) => this.handleModalIconHelpClick(ev));
    this.modalConfirm.removeEventListener('click', ev => this.handleConfirmClick(ev));
    this.table.removeEventListener("change", (ev) => this.handleTableChange(ev));
    this.deleteBtn.removeEventListener("click", (ev) => this.confirmDelete(ev));
    this.saveBtn.removeEventListener("click", (ev) => this.handleSave(ev));
    this.undoAllBtn.removeEventListener("click", (ev) => this.handleUndoAll(ev));
    this.querySelector("#newBtn").removeEventListener("click", (ev) => this.confirmNewLayout(ev));
    this.querySelector("#col0MoveLeft").removeEventListener("click", (ev) => this.wrapColsLeft(ev));
    this.querySelector("#col0Clear").removeEventListener("click", (ev) => this.colToNone(ev, 0));
    this.querySelector("#col0MoveRight").removeEventListener("click", (ev) => this.swapCols(ev,0,1));
    this.querySelector("#col1MoveLeft").removeEventListener("click", (ev) => this.swapCols(ev,0,1));
    this.querySelector("#col1Clear").removeEventListener("click", (ev) => this.colToNone(ev,1));
    this.querySelector("#col1MoveRight").removeEventListener("click", (ev) => this.swapCols(ev,1,2));
    this.querySelector("#col2MoveLeft").removeEventListener("click", (ev) => this.swapCols(ev,1,2));
    this.querySelector("#col2Clear").removeEventListener("click", (ev) => this.colToNone(ev,2));
    this.querySelector("#col2MoveRight").removeEventListener("click", (ev) => this.wrapColsRight(ev,));
    this.selectCol1LangCode.removeEventListener("change", (ev) => this.handleColumnLangCodeChange(ev, 1))
    this.selectCol2LangCode.removeEventListener("change", (ev) => this.handleColumnLangCodeChange(ev, 2))
    this.selectCol3LangCode.removeEventListener("change", (ev) => this.handleColumnLangCodeChange(ev, 3))
  }

  handleColumnLangCodeChange(ev, col) {
    if (ev) {
      ev.stopPropagation();
      ev.preventDefault();
      this.setCols(col-1, ev.target.value);
    } else {
      return;
    }
  }
  handleModalIconHelpClick(ev) {
    ev.stopPropagation();
    const btn = ev.composedPath()[0].id;
    if (btn === "closeBtn" || btn === "cancelBtn") {
      this.modalHelp.className = "d-none";
    }
  }

  showModalIconHelp(ev) {
    ev.stopPropagation();
    this.scrollY = window.scrollY;
    let help = JSON.stringify([
      {
        "icon": "bi-save",
        "desc": "Save the layout.",
      },
      {
        "icon": "bi-arrow-counterclockwise",
        "desc": "Undo all changes.",
      },
      {
        "icon": "bi-trash",
        "desc": "Delete the layout. Note that the last layout cannot be deleted. You will be asked to confirm the delete.",
      },
      {
        "icon": "bi-plus-lg",
        "desc": "Add a new layout.",
      },
      {
        "icon": "bi-question-circle",
        "desc": "Show this help window.",
      },
      {
        "icon": "bi-chevron-double-left",
        "desc": "Wrap columns left.",
      },
      {
        "icon": "bi-chevron-double-right",
        "desc": "Wrap columns right.",
      },
      {
        "icon": "bi-x-square",
        "desc": "Delete column.",
      },
      {
        "icon": "bi-arrow-left-right",
        "desc": "Swap adjacent columns.",
      },
    ]);
    this.modalHelp.setAttribute("data", help);
    this.modalHelp.className = "";
  }

  // handleListClick fires when user selects a combination of language acronyms (aka a layout)
  // It also dispatches event comboSelectionChanged
  // to code-editor.js handleComboToolClick()
  // to render a preview for the section.
  handleListClick(ev) {
    if (ev) {
      ev.stopPropagation(); // otherwise in code-editor.js preview is called twice
      ev.preventDefault();
    }
    this.newLayout = false;
    this.selected = ev.target.textContent;

    localStorage.setItem(this.selectedLibraryComboKey, this.selected);
    this.renderAcronymList();
    this.container.dispatchEvent(this.comboSelectionChanged);
    this.getLayout();
  }
  requestLayoutIndexes() {
    const url = this.host
        + `api/layouts?`
    ;
    utils.callApi(url, "GET").then(response => {
      if (response.Status === "OK") {
        if (! response.Layouts) {
          return;
        }
        this.columnLayoutMap = response.ColumnLayouts;
        this.layouts = response.Layouts // e.g., ["en","es","es-en","es-gr","es-gr-en"]
        this.renderAcronymList();
      } else {
        alert(response.Status)
      }
    }).catch(error => {
      alert(error);
    });
  }

  renderAcronymList() {
    const list = this.acronymList;
    if (! list) {
      return;
    }
    this.validateSelected();
    list.innerHTML = "";
    this.layouts.sort();
    this.layouts.forEach((element) => {
      const li = document.createElement('li');
      li.classList.add("list-group-item");
      if (element === this.selected) {
        li.classList.add("active");
        li.setAttribute("aria-current", "true");
      }
      li.innerText = element;
      list.appendChild(li);
    });
    this.getLayout();
  }

  deselectAll() {
    const items = this.acronymList.querySelectorAll("li");
    items.forEach((item) => {
      item.classList.remove("active");
    });
    this.newLayout = true;
  }
  handleConfirmClick(ev) {
    if (ev) {
      ev.stopPropagation();
      ev.preventDefault();
    }
    const btn = ev.composedPath()[0].id;
    this.modalConfirm.classList.add("d-none");
    if (btn === "okBtn") {
      switch (this.layoutAction) {
        case "delete": {
          this.handleDelete();
          break;
        }
        case "newLayout": {
          this.handleNewLayout();
          break;
        }
      }
      this.layoutAction = "";
    }
  }
  confirm(title, message, layoutAction) {
    this.layoutAction = layoutAction;
    this.modalConfirm.setAttribute("title", title);
    this.modalConfirm.setAttribute("msg1", message);
    this.modalConfirm.classList.remove("d-none");
  }
  confirmNewLayout(ev) {
    if (ev) {
      ev.stopPropagation();
      ev.preventDefault();
    }
    if (this.libraryUndoStack.length > 0) {
      this.confirm("New Layout Confirmation",
          "You have unsaved changes. Click OK to lose them and create a new layout.",
          "newLayout");
    } else {
      this.handleNewLayout();
    }
  }

  handleNewLayout() {
    for (let i = 0; i < 3; i++) {
      this.colToNone(null, i);
    }
    this.libraryUndoStack = [];
    if (this.layouts && this.layouts.length > 0) {
      this.selected = this.layouts[0];
    }
    this.buildTable();
    this.deselectAll();
  }
  confirmDelete(ev) {
    if (ev) {
      ev.stopPropagation();
      ev.preventDefault();
    }
    this.confirm("Delete Layout Confirmation",
        "Are you sure you want to delete the layout? Click OK to delete.",
        "delete");
  }

  setCursorWait() {
    // 2022-08-26 Had lots of trouble getting the wait style to work. MAC.
    //            Seems like it should work just by setting the document.body style....
    this.indexTitleDiv.innerText = "Please wait. Updating layout list....";
    this.container.classList.add("wait");
    this.deleteBtn.classList.add("wait");
    this.saveBtn.classList.add("wait");
    this.acronymList.classList.add("wait");
    this.table.classList.add("wait");
  }
  setCursorDefault() {
    this.indexTitleDiv.innerText = this.indexTitle;
    this.container.classList.remove("wait");
    this.deleteBtn.classList.remove("wait");
    this.saveBtn.classList.remove("wait");
    this.acronymList.classList.remove("wait");
    this.table.classList.remove("wait");
  }
  setCursor(style) {
    this.deleteBtn.style.cursor = style;
    this.saveBtn.style.cursor = style;
    this.container.style.cursor = style;
    this.acronymList.style.cursor = style;
    this.table.style.cursor = style;
  }
  handleDelete() {
    this.deleteGenConfig().then(response => {
      if (response.Status === "OK") {
        if (this.layouts && this.layouts.length > 0) {
          this.selected = this.layouts[0];
          localStorage.setItem(this.selectedLibraryComboKey, this.selected);
        }
        this.requestLayoutIndexes()
      } else {
        alert(response.Status)
      }
    }).catch(error => {
      alert(error);
    });
  }
  handleSave(ev) {
    if (ev) {
      ev.stopPropagation();
      ev.preventDefault();
    }
    if (! this.librariesSetOk()) {
      return;
    }
    this.shiftColumns();
    this.libraryUndoStack = [];
    this.toggleButtons();

    this.updateGenConfig().then(response => {
      if (response.Status === "OK") {
        this.selected = response.Acronym;
        localStorage.setItem(this.selectedLibraryComboKey, this.selected);
        this.requestLayoutIndexes();
        this.container.dispatchEvent(this.genComboSaved);
      } else {
        alert(response.Status)
      }
    }).catch(error => {
      alert(error);
    });
  }

  // shift a column left if the column to its left has liturgical.primary === none
  shiftColumns() {
    if (this.layoutMatrix[0][0] === "none" && this.layoutMatrix[0,1] !== "none") {
      this.swapCols(null,0, 1);
    }
    if (this.layoutMatrix[0][1] === "none" && this.layoutMatrix[0,2] !== "none") {
      this.swapCols(null,1, 2);
    }
  }
  librariesSetOk() {
    if (! this.liturgicalSet()) {
      alert("Can't Save layout: at least one column must have a liturgical primary library set.");
      return false;
    }
    if (! this.allPrimaryLibrariesSet()) {
      alert("Can't Save layout: if the liturgical primary library in a column is set, all the other primary libraries in the column must also be set.");
      return false;
    }
    return true;
  }
  liturgicalSet() {
    let liturgicalSet = false;
    for (let i = 0; i < 3; i++) {
      if (this.layoutMatrix[0][i] !== "none") {
        liturgicalSet = true;
      }
    }
    return liturgicalSet;
  }
  allPrimaryLibrariesSet()  {
    let primariesSet = true
    let col = 0;
    let row = 0;
    OuterLoop:
        for (col = 0; col < 3; col++) {
          if (this.layoutMatrix[0][col] !== "none") {
            for (row = 2; row < 10; row += 2) {
              if (this.layoutMatrix[row][col] === "none") {
                primariesSet = false
                break OuterLoop
              }
            }
          }
        }
    return primariesSet
  }

  // resets table values originally retrieved from the call to the api
  handleUndoAll(ev) {
    if (ev) {
      ev.stopPropagation();
      ev.preventDefault();
    }
    this.libraryUndoStack = [];
    this.initLayoutMatrix();
    this.addTypesToLayout();
    this.buildTable();
  }
  toggleButtons() {
    this.toggleSaveButton();
    this.toggleUndoAllButton();
    this.toggleDeleteButton();
  }

  toggleDeleteButton() {
    if (this.layoutMatrix[0][0] === "none") {
      this.deleteBtn.classList.add("disabled");
    } else {
      this.deleteBtn.classList.remove("disabled");
    }
  }
  toggleSaveButton() {
    if (this.saveBtn.classList.contains("disabled")) {
      if (this.libraryUndoStack.length > 0) {
        this.saveBtn.classList.remove("disabled");
      }
    } else {
      if (this.libraryUndoStack.length === 0) {
        this.saveBtn.classList.add("disabled");
      }
    }
  }
  toggleUndoAllButton() {
    if (this.undoAllBtn.classList.contains("disabled")) {
      if (this.libraryUndoStack.length > 0) {
        this.undoAllBtn.classList.remove("disabled");
      }
    } else {
      if (this.libraryUndoStack.length === 0) {
        this.undoAllBtn.classList.add("disabled");
      }
    }
  }
  getLibraries() {
    this.requestLibraries().then(response => {
      if (response.Status === "OK") {
          if (response.Values) {
            this.libraries = [];
            response.Values.forEach( (item) => {
              if (item.Last && item.Last.endsWith("/")) { // it is a directory
                this.libraries.push(item.Last.slice(0,-1));
              }
            });
            this.getLayout();
          } else {
            alert("response.Values is null");
          }
      } else {
        alert(response.Status)
      }
    }).catch(error => {
      alert(error);
    });
  }

  async requestLibraries() {
    const url = this.host
        + `api/list?path=ltx`
    ;
    let fetchData = {
      method: 'GET'
    }

    const response = await fetch(url, fetchData);
    if (!response.ok) {
      let message = "";
      switch (response.status) {
        case 404: {
          message += "not found";
          break;
        }
        case 500: {
          message += "doxa server error";
          break;
        }
        default: {
          message += `An error has occurred: ${response.status}`;
        }
      }
      throw new Error(message);
    }
    const responseJson = await response.json();
    return responseJson;
  }

  validateSelected() {
    if (!this.layouts) {
      return;
    }
    if (this.layouts.length === 0) {
      return;
    }
    if (this.selected === null || this.selected.length === 0) {
        this.selected = this.layouts[0];
      return;
    }
    let found = false;
    this.layouts.forEach((item) => {
      if (item === this.selected) {
        found = true;
      }
    });
    if (!found) {
      if (this.layouts !== null && this.layouts.length > 0) {
        this.selected = this.layouts[0];
        localStorage.setItem(this.selectedLibraryComboKey, this.selected);
      }
    }
  }
  getLayout() {
    // make sure the layout we are requesting exists in the layouts array
    // if not, it will be set to the first layout.
    this.validateSelected();
    this.requestGenConfig().then(response => {
      if (! response || response === null) {
        return;
      }
      if (! response.Layout) {
        return;
      }
      this.data = response.Layout
      if (this.data.Layout) {
        this.selected = this.data?.Layout.Acronym;
        localStorage.setItem(this.selectedLibraryComboKey, this.selected);
      }
      this.initLayoutMatrix();
      this.setLangCodeColumnSelectors(this.selected)
      this.addTypesToLayout();
      this.buildTable();
    }).catch(error => {
      if (error.message.includes("not found")) {
        this.selected = this.layouts[0];
        localStorage.setItem(this.selectedLibraryComboKey, this.selected);
        alert(`layout not found in the database.`)
        return;
      }
      alert(error);
    });
  }
  async requestGenConfig() {
    if (this.selected.length === 0) {
      alert("this.selected not set")
      return;
    }
    const url = this.host
        + `api/layout?`
        + `acronym=` + this.selected
    ;
    let fetchData = {
      method: 'GET'
    }
    const response = await fetch(url, fetchData);
    if (!response.ok) {
      let message = "";
      switch (response.status) {
        case 404: {
          message += "not found";
          break;
        }
        case 500: {
          message += "doxa server error";
          break;
        }
        default: {
          message += `An error has occurred: ${response.status}`;
        }
      }
      throw new Error(message);
    }
    const responseJson = await response.json();
    return responseJson;
  }

  async updateGenConfig() {
    const url = this.host
        + `api/layout`
        + `?matrix=` + encodeURIComponent(JSON.stringify(this.layoutMatrix))
    ;
    let fetchData = {
      method: 'PUT'
    }
    this.setCursorWait();
    const response = await fetch(url, fetchData);
    this.setCursorDefault();
    if (!response.ok) {
      let message = "";
      switch (response.status) {
        case 404: {
          message += "not found";
          break;
        }
        case 500: {
          message += "doxa server error";
          break;
        }
        default: {
          message += `An error has occurred: ${response.status}`;
        }
      }
      throw new Error(message);
    }
    const responseJson = await response.json();
    return responseJson;
  }

  async deleteGenConfig() {
    const url = this.host
        + `api/layout/delete`
        + `?acronym=` + this.selected
    ;
    let fetchData = {
      method: 'PUT'
    }
    this.setCursorWait();
    const response = await fetch(url, fetchData);
    this.setCursorDefault();
    if (!response.ok) {
      let message = "";
      switch (response.status) {
        case 404: {
          message += "not found";
          break;
        }
        case 500: {
          message += "doxa server error";
          break;
        }
        default: {
          message += `An error has occurred: ${response.status}}`;
        }
      }
      throw new Error(message);
    }
    const responseJson = await response.json();
    return responseJson;
  }

  initRowTitles() {
    this.rowTypes.forEach((t) => {
      let cols = [];
      cols.push(t);
      cols.push("Primary");
      this.rowTitles.push(cols);
      cols = [];
      cols.push("");
      cols.push("Fallback")
      this.rowTitles.push(cols);
    });
  }
  // Adds 10 rows to this.layoutMatrix and populates the columns with "none" as the value
  initLayoutMatrix () {
    this.layoutMatrix = [];
    for (let i = 0; i < this.colArrayLength; i++) {
      let row = [];
      for (let j = 0; j < 3; j++) {
        row.push("none");
      }
      this.layoutMatrix.push(row);
    }
  }

  // for each row in this.layoutMatrix, sets row[colNbr] to "none"
  colToNone(ev, colNbr) {
    if (ev) {
      ev.stopPropagation();
      ev.preventDefault();
    }
    this.layoutMatrix.forEach((row, rowIndex) => {
      const curVal = this.layoutMatrix[rowIndex][colNbr];
      if (curVal === "none") {
        return; // skip this iteration
      }
      this.setLibrary(rowIndex, colNbr, curVal, "none");
    });
    this.buildTable();
  }
  // for each row in this.layoutMatrix, set the values for the specified column and language codes
  setCols(colNbr, langCode) {
    const libKeys = ["LiturgicalLibs", "EpistleLibs", "GospelLibs", "ProphetLibs", "PsalterLibs"];
    const libTypes = ["Primary", "Fallbacks"];
    const langVals = this.columnLayoutMap[langCode];
    let k = -1;
    libKeys.forEach ((libKey, i) => {
      const val = langVals[libKey][0]
      libTypes.forEach((type, j) => {
        k++;
        let newVal = val[type]
        if (!newVal) {
          newVal = "none"
        }
        let oldVal = this.layoutMatrix[k][colNbr];
        this.setLibrary(k, colNbr, oldVal, newVal);
      })
    });
    this.buildTable();
  }

  // for each row in this.layoutMatrix, swaps the values for row[i] and row[j]
  swapCols(ev, i, j) {
    if (ev) {
      ev.stopPropagation();
      ev.preventDefault();
    }
    this.layoutMatrix.forEach((row, rowNbr) => {
      let iVal = this.layoutMatrix[rowNbr][i];
      let jVal = this.layoutMatrix[rowNbr][j];
      this.setLibrary(rowNbr, i, iVal, jVal);
      this.setLibrary(rowNbr, j, jVal, iVal);
    });
    this.buildTable();
  }
  wrapColsLeft(ev) {
    if (ev) {
      ev.stopPropagation();
      ev.preventDefault();
    }
    this.wrapCols("left");
  }
  wrapColsRight(ev) {
    if (ev) {
      ev.stopPropagation();
      ev.preventDefault();
    }
    this.wrapCols("right");
  }
  wrapCols(direction) {
    this.layoutMatrix.forEach((row, rowNbr) => {
      let col0Val = this.layoutMatrix[rowNbr][0];
      let col1Val = this.layoutMatrix[rowNbr][1];
      let col2Val = this.layoutMatrix[rowNbr][2];
      switch (direction) {
        case "left": {
          //  0 1 2 => 1 2 0
          this.layoutMatrix[rowNbr][0] = col1Val;
          this.layoutMatrix[rowNbr][1] = col2Val;
          this.layoutMatrix[rowNbr][2] = col0Val;
          break;
        }
        case "right": {
          // 0 1 2 => 2 0 1
          this.layoutMatrix[rowNbr][0] = col2Val;
          this.layoutMatrix[rowNbr][1] = col0Val;
          this.layoutMatrix[rowNbr][2] = col1Val;
          break;
        }
      }
      this.libraryUndoStack.push({"row": row, "col": 0, "value": col0Val});
      this.libraryUndoStack.push({"row": row, "col": 1, "value": col1Val});
      this.libraryUndoStack.push({"row": row, "col": 2, "value": col2Val});
    });
    this.buildTable();
  }

  // setAcronymSelectors creates a language code dropdown as the column header of the layout table,
  // one for each acronym in the layout set of acronyms.  If a column does not have a language acronym, it is set to none.
  setLangCodeColumnSelectors(layoutAcronym) {
    const acronyms = layoutAcronym.split("-");
    const l = acronyms.length;
    for (let i = 0; i < 3; i++) { // there are three columns to set to either an actual language code or none.
      if (i < l) { // this means we have an actual language code to use as the option value
        this.setAcronymDropdown(acronyms[i], i);
      } else { // this is an unused column, so we will set the option to none.
        this.setAcronymDropdown("none", i);
      }
    }
  }

  // setAcronymDropdown sets the dropdown for the specified column, populating the option list with each available column layout acronym.
  setAcronymDropdown(selectedAcronym, colIndex) {
    let optionsHtml = `<option value="none">none</option>`;
    Object.keys(this.columnLayoutMap).forEach((element) =>  {
      let selected = ""
      if (element === selectedAcronym) {
        selected = "selected"
      }
      optionsHtml += `<option ${selected} value="${element}">${element}</option>`
    });
    switch (colIndex) {
      case 0:
        this.selectCol1LangCode.innerHTML = optionsHtml;
        break;
      case 1:
        this.selectCol2LangCode.innerHTML = optionsHtml;
        break;
      default:
        this.selectCol3LangCode.innerHTML = optionsHtml;
    }
  }
  addTypesToLayout() {
    this.addTypeToLayout("LiturgicalLibs", 0);
    this.addTypeToLayout("EpistleLibs", 2);
    this.addTypeToLayout("GospelLibs", 4);
    this.addTypeToLayout("ProphetLibs", 6);
    this.addTypeToLayout("PsalterLibs", 8);
  }
  addTypeToLayout(type, r) {
    this.data[type].forEach( (item, index) => { // e.g. {"Primary":"en_us_public","FallBacks":null}
      this.layoutMatrix[r][index] = item.Primary;
      let fallback = "none";
      if (item.FallBacks && item.FallBacks[0]) {
        fallback = item.FallBacks[0];
      }
      this.layoutMatrix[r+1][index] = fallback;
    });
  }

  buildTable() {
    this.table.innerHTML = "";
    let rowIndex = 0;
    this.layoutMatrix.forEach((row, r) => {
      rowIndex = r;
      const tr = document.createElement("tr");
      let typeCell = document.createElement("td");
      let subTypeCell = document.createElement("td");
      typeCell.innerText = this.rowTitles[r][0];
      subTypeCell.innerText = this.rowTitles[r][1];
      tr.appendChild(typeCell);
      tr.appendChild(subTypeCell);

      row.forEach((library, col) => {
        let td = document.createElement("td");
        td.appendChild(this.getSelect(library, rowIndex, col));
        tr.appendChild(td);
      });
      this.table.appendChild(tr);
    });
    this.setAcronym();
    this.toggleButtons();
  }

  // setAcronym uses the iso lang code
  setAcronym() {
    let a = utils.langCodeFromLibrary(this.layoutMatrix[0][0]);
    if (this.layoutMatrix[0][1] !== "none") {
      a += `-${utils.langCodeFromLibrary(this.layoutMatrix[0][1])}`;
    }
    if (this.layoutMatrix[0][2] !== "none") {
      a += `-${utils.langCodeFromLibrary(this.layoutMatrix[0][2])}`;
    }
    this.acronymSpan.innerText = a;
  }
  // get a select component of available libraries.  selected is the currently selected library.
  getSelect(selected, rowIndex, colIndex) {
    const select = document.createElement("select");
    select.classList.add("form-select");
    select.setAttribute("aria-label", "Select Library");
    select.setAttribute("data-row", rowIndex);
    select.setAttribute("data-col", colIndex);
    select.setAttribute("data-selected", selected);
    // create none selected option
    const firstOption = document.createElement("option");
    firstOption.setAttribute("value", "none");
    firstOption.innerText = "none";
    select.appendChild(firstOption);
    // add libraries as options
    this.libraries.forEach((item) => {
      const option = document.createElement("option");
      option.setAttribute("value", item);
      if (item === selected) {
        option.selected = true;
      }
      option.innerText = item;
      select.appendChild(option);
    });
    return select;
  }

  handleTableChange(ev) {
    ev.stopPropagation();
    ev.preventDefault();
    const s = ev.target;
    if (s.hasAttribute("data-row")) {
      const row = s.getAttribute("data-row");
      const col = s.getAttribute("data-col");
      const previousSelection = s.getAttribute("data-selected");
      s.setAttribute("data-selected", s.value)
      this.setLibrary(row, col, previousSelection, s.value);
      this.setAcronym();
      this.toggleButtons();
    }
  }
  // sets this.layoutMatrix[row][col] = value
  // also pushes oldVal on the "undo" stack
  setLibrary(row, col, oldVal, newVal) {
    this.libraryUndoStack.push({"row": row, "col": col, "value": oldVal});
    this.layoutMatrix[row][col] = newVal;
  }


  static get observedAttributes() {
    return ["selected"];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
    if (name === "index") {
      // issue: when the component loads, this.getLibraries is called,
      // which then calls this.getLayout.  But, also attributeChangedCallback is
      // being called multiple times, the first time oldVal is null, then the
      // 2nd oldVal === newVal, which is crazy, but true.  Hence, this if statement.
      if (oldVal && oldVal !== null && oldVal !== newVal) {
        this.getLayout();
      }
    }
  }

  adoptedCallback() {
  }
  get host() {
    return this.getAttribute("host");
  }
  set host(value) {
    if (!value.endsWith("/")) {
      this.setAttribute("host", value + "/");
    } else {
      this.setAttribute("host", value);
    }
  }
  get selected() {
    return this.getAttribute("selected");
  }
  set selected(value) {
    this.setAttribute("selected", value);
  }
  genComboSaved = new CustomEvent('combosaved', {
    detail: {
      name: 'combosaved' // this is the name to use in handler
    },
    bubbles: true
  });
  comboSelectionChanged = new CustomEvent('comboSelectionChanged', {
    detail: {
      name: 'comboSelectionChanged'
    },
    bubbles: true
  });

}

window.customElements.define('generation-layouts', GenerationLayouts);