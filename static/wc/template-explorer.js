import * as utils from "./utils.js";

class TemplateExplorer extends HTMLElement {
  constructor() {
    super();
    // used for setting / getting browser stored options
    this.lsPrefix = "tx"; // used to prefix local storage keys
    this.lsExact = "exact";
    this.lsWhole = "wholeWord";
    this.lsPrimary = "searchPrimary";
    this.lsFallback = "searchFallback";

    this.divStatus = null;
    this.inputSearch = null;
    this.optionExact = null;
    this.optionWholeWord = null;
    this.optionPrimary = null;
    this.optionFallback = null;
    this.divOptionPrimary = null;
    this.divOptionFallback = null;
    this.curPage = 1;
    this.inputFilterId = null;
    this.currentIdFilter = "";
//    this.inputFilterValue = null;
    this.tableBody = null;
    this.Values = [];
    this.dataLength = 0;
    this.searchPattern = "";
    this.colSourceWidth = "1%"
    this.colIdWidth = "28%";
    this.colLineColWidth = "1%";
    this.colMatchWidth = "69%";
    this.colOpenWidth = "1%"
    this.dataIndexArray = [];
    // for paging the data in the table
    this.curPage = 1;
    this.pageSize = 10;
    this.scrollY = "";
    this.useId = false;
    this.divAbout = null;
    this.iconAbout = null;
  }

  connectedCallback() {
    this.innerHTML =
`
<style> 
  @import url('static/css/bootstrap.min.css');
  @import url('static/img/bootstrap-icons.css');
</style>
<style>
.sticky-top { 
    background-color: #fff;
}
#t-explorer-Container {
    overflow-y:scroll !important;
}
table {
}
.filterColor {
  color: blue;
}
.searchColor {
  color: red;
}
.alert {
    line-height: 1px;
}
.match {
  color: red;
}
.leftContext {
}
.colMatch {
    text-align: center;
}
.rightContext {
}
.tableBody {
    font-family: "Courier New";
}
.clickable {
    cursor:pointer;
    outline:none;
}
</style>
<div id="t-explorer-Container" class="container container-fluid">
<div id="my-element-div" class="wc-title">Template Explorer&nbsp;<i id="iconAbout" class="bi bi-question-circle clickable" style="color: cornflowerblue;"></i></div>
<div id="divAbout" class="d-none" style="display: flex; flex-direction: column; flex-wrap: wrap; gap: 1rem; max-width: 50vw;">
    <br>
    <div>
        The template explorer allows you to search the content of templates.
        Enter the search pattern and press the Enter key. Search patterns 
        are treated as regular expressions.  If the search results are not what
        you expect, it might be that you have used a reserved character
        such as: .  * (  )  when you mean the literal character.
        Try putting a backslash in front of them, e.g. \\. \\* \\(  \\) .
        After the results are displayed, you can filter them by the ID. 
        To hide this information, click the help icon.
    </div>
</div>
<hr>
<!-- search-->
  <div class="row p-1" id="t-explorer-search-row">
      <div class="col">
          <div class="input-group mb-3">
              <span class="input-group-text" id="basic-addon1"><i class="bi-search"
                                                                  style="color: cornflowerblue;"></i></span>
              <input type="text" id="t-explorer-search-input" class="form-control"
                     placeholder="search templates" aria-label="search templates"
                     aria-describedby="basic-addon1">
          </div>
      </div>
      <div class="col">
          <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" id="t-explorer-search-option-exact"
                     value="option1">
              <label class="form-check-label" for="inlineCheckbox1">Exact</label>
          </div>
          <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" id="t-explorer-search-option-whole-word"
                     value="option2">
              <label class="form-check-label" for="inlineCheckbox2">Whole Word</label>
          </div>
          <div id="divOptionPrimary" class="form-check form-check-inline d-none">
              <input class="form-check-input" type="checkbox" id="t-explorer-search-option-primary"
                     value="option3">
              <label class="form-check-label selected" for="inlineCheckbox3">Include Primary</label>
          </div>
          <div id="divOptionFallback" class="form-check form-check-inline d-none">
              <input class="form-check-input" type="checkbox" id="t-explorer-search-option-fallback"
                     value="option4">
              <label class="form-check-label" for="inlineCheckbox4">Include Fallback</label>
          </div>
      </div>
      <div class="col">
      </div>
  </div> <!-- end search-->
<!-- filters-->
  <div class="row p-1">
      <div class="col">
          <div class="input-group mb-3">
              <span class="input-group-text" id="basic-addon1"><i class="bi-filter"
                                                                  style="color: cornflowerblue;"></i></span>
              <input type="text" id="t-explorer-filter-id" class="form-control" placeholder="filter ID"
                     aria-label="filter table" aria-describedby="basic-addon1">
          </div>
      </div>
      <div class="col">
      </div>
  </div> <!-- end filters-->
<!-- status -->
  <div id="statusDiv"></div>
<!-- table -->
  <div class="table-responsive">
    <table class="table table-striped table-responsive">
        <thead>
            <tr>
            <th scope="col" style="width: ${this.colSourceWidth} !important;">Source</th>
            <th scope="col" style="width: ${this.colIdWidth} !important;">ID</th>
            <th scope="col" style="width: ${this.colLineColWidth} !important;">Line:Col</th>
            <th scope="col" style="width: ${this.colMatchWidth} !important;">Match</th>
            <th scope="col" style="width: ${this.colOpenWidth} !important;">Open</th>
            </tr>
        </thead>
        <tbody id="tableBody"></tbody>
    </table>  
  </div> <!-- end table div -->
  <!-- Pagination -->
     <div class="row p-1">
        <div class="col col-4">
            <div class="control-label" style="padding-top: 12px;">Showing rows <span id="rowTop">0</span> to <span id="rowBottom">0</span> of <span id="rowCount">0</span></div>
        </div>
        <div class="col col-4"><div class="control-label" style="margin-top: 4px; text-align: center;"><span>Page size: </span><input id="pageSizeId" style="max-width: 3ch; text-align: center;"></div></div>
        <div class="col col-4">
            <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-end">
              <li class="page-item">
                <a id="firstButton" class="page-link d-none" href="#" aria-label="First">
                  <span aria-hidden="true"><i class="bi-chevron-double-left"></i></span>
                </a>
              </li>
              <li class="page-item">
                <a id="prevButton" class="page-link d-none" href="#" aria-label="Previous">
                  <span aria-hidden="true"><i class="bi-chevron-left"></i></span>
                </a>
              </li>
              <li class="page-item">
                <div class="control-label" style="margin-top: 4px;"> 
                  <span style="margin-left: 4px;">Page</span>
                  <input style="margin-left: 2px;max-width: 4ch; text-align: center;" id="currentPage">
                  <span style="margin-left: 2px;">of</span>
                  <span style="margin-left: 2px;margin-right: 4px;" id="pageMax"></span>
                </div>
              </li>
              <li class="page-item">
                <a id="nextButton" class="page-link" href="#" aria-label="Next">
                  <span aria-hidden="true"><i class="bi-chevron-right"></i></span>
                </a>
              </li>
              <li class="page-item">
                <a id="lastButton" class="page-link" href="#" aria-label="Last">
                  <span aria-hidden="true"><i class="bi-chevron-double-right"></i></span>
                </a>
              </li>
            </ul>
          </nav>
        </div>
        </div>
     </div>

</div> <!-- container -->
`;
    this.divAbout = this.querySelector("#divAbout");
    this.divOptionPrimary = this.querySelector("#divOptionPrimary");
    this.divOptionFallback = this.querySelector("#divOptionFallback");
    this.iconAbout = this.querySelector("#iconAbout");
    this.divStatus = this.querySelector("#statusDiv");
    this.inputSearch = this.querySelector("#t-explorer-search-input");
    this.optionExact = this.querySelector("#t-explorer-search-option-exact");
    this.optionWholeWord = this.querySelector("#t-explorer-search-option-whole-word");
    if (this.fallbackEnabled && this.fallbackEnabled === "true") {
      this.optionPrimary = this.querySelector("#t-explorer-search-option-primary");
      this.optionFallback = this.querySelector("#t-explorer-search-option-fallback");
      this.divOptionPrimary.classList.remove("d-none")
      this.divOptionFallback.classList.remove("d-none");
    }
    this.inputFilterId = this.querySelector("#t-explorer-filter-id");
    this.tableBody = this.querySelector("#tableBody");

    // add listeners.  For each one added, remove it in disconnectedCallback.
    this.optionExact.checked =  utils.lsGetBool(this.lsPrefix, this.lsExact);
    this.optionExact.addEventListener("change", () => this.handleOptionExactChange());
    this.optionWholeWord.checked =  utils.lsGetBool(this.lsPrefix, this.lsWhole);
    this.optionWholeWord.addEventListener("change", () => this.handleOptionWholeWordChange());
    if (this.fallbackEnabled && this.fallbackEnabled === "true") {
      this.optionPrimary.checked =  utils.lsGetBool(this.lsPrefix, this.lsPrimary);
      this.optionPrimary.addEventListener("change", () => this.handleOptionSearchPrimaryChange());
      this.optionFallback.checked =  utils.lsGetBool(this.lsPrefix, this.lsFallback);
      this.optionFallback.addEventListener("change", () => this.handleOptionSearchFallback());
    }
    this.inputSearch.addEventListener('keyup', (event) => this.setSearch(event));
    this.inputFilterId.addEventListener('keyup', (event) => this.filterData(event));
    this.iconAbout?.addEventListener('click', () => {
      if (this.divAbout?.classList.contains("d-none")) {
        this.divAbout.classList.remove("d-none");
      } else {
        this.divAbout.classList.add("d-none");
      }
    });
    document.getElementById("currentPage").addEventListener("keypress", (e) => {
      if (e.key === 'Enter') {
        this.curPage = document.getElementById("currentPage").value;
        this.renderTable();
      }
    });
    // listen for page size change
    document.getElementById("pageSizeId").addEventListener("keypress", (e) => {
      if (e.key === 'Enter') {
        this.pageSize = document.getElementById("pageSizeId").value;
        this.curPage = 1;
        this.renderTable();
      }
    });
    this.querySelector('#firstButton').addEventListener('click', () => this.firstPage(this));
    this.querySelector('#prevButton').addEventListener('click', () => this.previousPage(this));
    this.querySelector('#nextButton').addEventListener('click', () => this.nextPage(this));
    this.querySelector('#lastButton').addEventListener('click', () => this.lastPage(this));

  }

  disconnectedCallback() {
    this.querySelector('#my-element-div').removeEventListener('click', () => this.sayOuch());
  }

  handleOptionExactChange() {
    utils.lsSet(this.lsPrefix, this.lsExact, JSON.stringify(this.optionExact.checked));
  }
  handleOptionWholeWordChange() {
    utils.lsSet(this.lsPrefix, this.lsWhole, JSON.stringify(this.optionWholeWord.checked));
  }
  handleOptionSearchPrimaryChange() {
    utils.lsSet(this.lsPrefix, this.lsPrimary, JSON.stringify(this.optionPrimary.checked));
  }
  handleOptionSearchFallback() {
    utils.lsSet(this.lsPrefix, this.lsFallback, JSON.stringify(this.optionFallback.checked));
  }

  setSearch(ev) {
    if (ev.keyCode !== 13) { // enter key
      return;
    }
    this.searchPattern = this.inputSearch.value;
    if (this.searchPattern.length > 0) {
      this.getTemplateMatches();
    }
  }
  renderTable() {
    this.tableBody.innerHTML = "";

    let displaying = 0;
    let start = (this.curPage - 1) * this.pageSize;
    let end = this.curPage * this.pageSize;

    this.dataIndexArray.filter((row, index) => {
      if (index >= start && index < end) return true;
    }).forEach((entry, index) => {
      displaying++;
      const item = this.Values[entry];
      // item.Value is from go struct:
      // type MatchedLine struct {
      //   Source       string
      //   Path         string
      //   RelativePath string
      //   FileName     string
      //   Line         int
      //   From         int
      //   To           int
      //   ContextLeft  string
      //   Match        string
      //   ContextRight string
      // }
        // Note: we put spaces around the path for the item.RelativePath so the browser will wrap it.
      const row = document.createElement("tr");
      row.innerHTML = `<td>${item.Source}</td>
                        <td style="width: ${this.colIdWidth} !important;">${item.RelativePath.replaceAll("/", " / ")}</td>
                        <td style="width: ${this.colLineColWidth} !important;">${item.Line}:${item.From}</td>
                        <td class="colMatch" style="width: ${this.colMatchWidth} !important;"><span class="leftContext">${item.ContextLeft}</span><span class="match">${item.Match}</span><span class="rightContext">${item.ContextRight}</span>
                        <td style="width: ${this.colOpenWidth} !important;"><a href="ide?id=${item.RelativePath}" target="_blank"><i class="bi bi-file-text"></i></a></td>
                        </td>
`
      this.tableBody.appendChild(row);
    });
    this.setTableSizeNotice(start + 1, start + displaying);
  }
  setDataIndexForAllRows() {
    this.dataIndexArray = [];
    if (!this.Values) {
      return;
    }
    const j = this.Values.length;
    for (let i = 0; i < j; i++) {
      this.dataIndexArray.push(i);
    }
  }

  // Populates theDataIndexArray with indexes of this.Values.
  // If pattern is null, all the indexes of this.Values will be pushed onto the array.
  // If pattern is not null, only indexes of data IDs that match will be pushed onto the array.
  filterData(evt) {
    // // ignore keypress unless user presses enter key
    // if (evt.keyCode !== 13) {
    //   return;
    // }
    this.currentIdFilter = this.inputFilterId.value;
    this.dataIndexArray = [];

    if (this.currentIdFilter.length > 0) {
      this.Values.forEach((item, index) => {
        if ((this.getIdFilterRegEx()).test(item.RelativePath)) {
          this.dataIndexArray.push(index);
        }
      });
    } else {
      this.setDataIndexForAllRows();
    }
    this.curPage = 1;
    this.renderTable();
  }
  getIdFilterRegEx() {
    return new RegExp(this.currentIdFilter, "gi");
  }

  scrollWindowTo(y) {
    window.scrollTo({top: y, behavior: 'auto'});
  }

  firstPage() {
    if (this.curPage > 1) this.curPage = 1;
    this.renderTable();
  }

  previousPage() {
    if (this.curPage > 1) this.curPage--;
    this.renderTable();
  }

  nextPage() {
    if ((this.curPage * this.pageSize) < this.dataLength) this.curPage++;
    this.renderTable();
  }

  lastPage() {
    if ((this.curPage * this.pageSize) < this.dataLength) {
      this.curPage = Math.floor(this.dataLength / this.pageSize) + 1;
      this.renderTable();
    }
  }

  resetTableSizeNotice() {
    document.getElementById("rowTop").innerText = "0";
    document.getElementById("rowBottom").innerText = "0";
    document.getElementById("rowCount").innerText = "0";
    document.getElementById("currentPage").value = "0";
    document.getElementById("pageMax").innerText = "0";
    document.getElementById("pageSizeId").value = "0";

  }

  setTableSizeNotice(start, end) {
    document.getElementById("rowTop").innerText = `${start.toLocaleString()}`;
    document.getElementById("rowBottom").innerText = `${end.toLocaleString()}`;
    document.getElementById("rowCount").innerText = `${this.dataLength.toLocaleString()}`;
    document.getElementById("currentPage").value = `${this.curPage.toLocaleString()}`;
    document.getElementById("pageMax").innerText = `${(Math.ceil(this.dataLength / this.pageSize)).toLocaleString()}`;
    document.getElementById("pageSizeId").value = `${this.pageSize}`;

    if (this.curPage === 1) {
      document.getElementById("firstButton").classList.add("d-none");
      document.getElementById("prevButton").classList.add("d-none");
    } else {
      document.getElementById("firstButton").classList.remove("d-none");
      document.getElementById("prevButton").classList.remove("d-none");
    }
    if (this.curPage === Math.ceil(this.dataLength / this.pageSize)) {
      document.getElementById("nextButton").classList.add("d-none");
      document.getElementById("lastButton").classList.add("d-none");
    } else {
      document.getElementById("nextButton").classList.remove("d-none");
      document.getElementById("lastButton").classList.remove("d-none");
    }
  }


  // ---- MESSAGE Handler
  statusMessage(type, message) {
    this.divStatus.className = "";
    this.divStatus.classList.add(type);
    this.divStatus.innerHTML = utils.getMessageSpan(type, message);
  }

  // Request handlers

  getTemplateMatches() {
    this.Values = [];
    this.dataLength = 0;
    this.statusMessage(utils.alerts.Info, "Searching templates. This can take a few seconds..")
    this.resetTableSizeNotice();
    this.requestMatches().then(response => {
      if (response.Status === "OK") {
        this.statusMessage(utils.alerts.Info, `${response.Status}: ${response.Message}`)
        this.Values = response.Values;
        this.dataLength = this.Values.length;
        this.currentIdFilter = "";
        this.setDataIndexForAllRows();
        this.renderTable();
      } else {
        this.statusMessage(utils.alerts.Danger, `${response.Status}: ${response.Message}`)
      }
    }).catch(error => {
      this.statusMessage(utils.alerts.Danger, error)
    });
  }

  // ---- REST API Handlers

  async requestMatches() {
    let searchPrimary = false;
    let searchFallback = false;
    if (this.fallbackEnabled && this.fallbackEnabled === "true") {
      searchPrimary = this.optionPrimary.checked;
      searchFallback = this.optionFallback.checked;
    }
    const url = this.host
        + `api/lml/search`
        + `?pattern=${this.searchPattern}`
        + `&exact=${this.optionExact.checked}`
        + `&whole=${this.optionWholeWord.checked}`
        + `&primary=${searchPrimary}`
        + `&fallback=${searchFallback}`
    ;
    let fetchData = {
      method: 'GET'
    }
    const response = await fetch(url, fetchData);
    if (!response.ok) {
      let message = "";
      switch (response.status) {
        case 404: {
          message += "not found";
          break;
        }
        case 500: {
          message += "doxa server error";
          break;
        }
        default: {
          message += `An error has occurred: ${response.status}`;
        }
      }
      throw new Error(message);
    }
    const responseJson = await response.json();
    return responseJson;
  }

  // element attributes
  static get observedAttributes() {
    return ['host'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
  }

  adoptedCallback() {
  }
  get fallbackEnabled() {
    return this.getAttribute("fallbackEnabled");
  }

  set fallbackEnabled(value) {
    this.setAttribute("fallbackEnabled", value);
  }

  get host() {
    return this.getAttribute("host");
  }

  set host(value) {
    this.setAttribute("host", value);
  }
}

window.customElements.define('template-explorer', TemplateExplorer);