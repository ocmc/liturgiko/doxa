import * as utils from "./utils.js";

class RichEditor extends HTMLElement {
  constructor() {
    super();
    console.log("static/wc/rich-editor.js")
    this.lsPrefix = "richEd";
    this.editor = null;
    this.divEditor = null;
  }

  connectedCallback() {
    this.innerHTML =
`
<style>
</style>
<div id="editor"></div>
`;
//     this.editor = new Quill('#editor', {
//       modules: {
//         toolbar: true
//       },
//       theme: 'snow'
//     });
//     this.editor.setContents(JSON.parse(`{
//    "ops": [
//       {
//          "insert": "Kaiye faiaita oruwa!\\nSome initial "
//       },
//       {
//          "insert": "bold",
//          "attributes": {
//             "bold": true
//          }
//       },
//       {
//          "insert": " text\\n\\n"
//       }
//    ]
// }`));
//     console.log("initial:")
//     console.log(JSON.stringify(this.editor.getContents(), null,3))
//     this.editor.on('editor-change', function(eventName, ...args) {
//       if (eventName === 'text-change') {
//         // args[0] is the changes
//         // args[1] is the delta before the changes
//         // args[2] is the source, e.g. user or api (programmatic)
//         const j = args.length;
//         console.log("")
//         for (let i = 0; i < j; i++) {
//           console.log(i);
//           console.log(JSON.stringify(args[i], null, 3));
//         }
//         // console.log("this.editor.getText()")
//         // console.log(JSON.stringify(this.editor.getText()))
//         // console.log("this.editor.getContents()")
//         // console.log(JSON.stringify(this.editor.getContents(), null, 3))
//       } else if (eventName === 'selection-change') {
//         // args[0] will be old range
//       }
//     });
  }

  disconnectedCallback() {
    //this.editor.off('editor-change');
  }

  handleRequest(ev) {
    if (ev) {
      ev.stopPropagation();
      ev.preventDefault();
    }
    this.resetResultsMeta();
    this.divResults.innerHTML = ""; // clear contents
    this.text = this.inputText.value;
    if (!this.pattern || this.pattern.length === 0) {
      this.requestStatusMessage(utils.alerts.Success, "missing regular expression");
      return
    }
    if (!this.pattern || this.pattern.length === 0) {
      this.requestStatusMessage(utils.alerts.Success, "missing text");
      return
    }
    this.requestStatusMessage(utils.alerts.Primary, "Testing...");
  }

  // ---- MESSAGE Handlers
  requestStatusMessage(type, message) {
    this.divRequestStatus.className = "";
    this.divRequestStatus.classList.add(type);
    this.divRequestStatus.innerHTML = utils.getMessageSpan(type, message);
  }
  // ---- REST API Handlers


// ---- End REST API Handlers

  static get observedAttributes() {
    return ['gid, host'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
  }

  adoptedCallback() {
  }
  get gid() {
    return this.getAttribute("gid");
  }

  set gid(value) {
    this.setAttribute("gid", value);
  }
  get host() {
    return this.getAttribute("host");
  }

  set host(value) {
    this.setAttribute("host", value);
  }
}

window.customElements.define('rich-editor', RichEditor);