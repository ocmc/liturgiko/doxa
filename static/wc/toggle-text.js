// ToggleText provides an easy way to add more info and let user decide to view it.
// Note: It is not finished.
// TODO: convert div infoSlot to <slot name="infoSlot"
// TODO: figure out why using this causes a problem when next to a bootstrap toggle.

class ToggleText extends HTMLElement {
  constructor() {
    super();
    console.log("static/wc/toggle-text.js")
    this.visible = false;
    this.slotHandle = null;
  }

  connectedCallback() {
    this.innerHTML =
        `
<style> 
  @import url('static/css/bootstrap.min.css');
  @import url('static/img/bootstrap-icons.css');
</style>
<div id="infoHeading">${this.heading}</div>
<div id="infoSlot" class="d-none">You found me!</div>
`;

    this.slotHandle = this.querySelector("#infoSlot");
    // add listeners.  For each one added, remove it in disconnectedCallback.
    this.querySelector('#infoHeading').addEventListener('click', () => this.toggle());
  }

  disconnectedCallback() {
    this.querySelector('#infoSlot').removeEventListener('click', () => this.toggle());
  }

  toggle() {
    if (this.visible) {
      this.visible = false;
      this.slotHandle.classList.add("d-none")
    } else {
      this.visible = true;
      this.slotHandle.classList.remove("d-none")
    }
  }

  static get observedAttributes() {
    return ['heading'];
  }

  attributeChangedCallback(heading, oldVal, newVal) {
    console.log(`Attribute: ${heading} changed!`);
  }

  adoptedCallback() {
  }
  get heading() {
    return this.getAttribute("heading");
  }

  set heading(value) {
    this.setAttribute("heading", value);
  }
}

window.customElements.define('toggle-text', ToggleText);