// DropdownList allows the user make a selection from a list.
class DropdownList extends HTMLElement {
    // generates custom event "listSelectionChange"
    /**
     * TAG ATTRIBUTES:
     *   gid: a unique id for the list so if the tag is used multiple times on a page, they will be treated as separate groups
     *   items: a json stringified object array as described below.
     *   active: the numeric index of the item to set as selected.
     * USAGE: see how used in synch-manager.
     * 1. In constructor, set an array object with radio options:
     *    e.g.,
     *    this.strategyOptions = [
     *       {Key: "combine", Desc: "Make remote repos, local repos and database content identical, but keep changes I made since the last synch.", Selected: true, Class: "alert-success", Icon: "", IconColor: ""},
     *       {Key: "resetToRemote", Desc: "Throw away database and local repo changes. Make the local repos and database identical to the remote repos.", Selected: false, Class: "alert-warning", Icon: "", IconColor: ""},
     *       {Key: "resetToDB", Desc: "Throw away remote repo changes. Make the remote repos identical to local repos and the database contents.", Selected: false, Class: "alert-warning", Icon: "", IconColor: ""},
     *     ];
     *     The option object attributes are:
     *       Key - the unique ID that will be returned when the item is selected
     *       Desc - what the user sees
     *       Class - (optional) the Bootstrap Alert class. Used to set the font color and background color of the item
     *       Icon - (optional) the Bootstrap icon to prefix to the Desc value
     *       IconColor - (optional) the color of the icon
     * 2. Add <dropdown-list it="someIdYouCreate" items="[]" active="0" title="My list"></dropdown-list>
     *       items - use JSON.stringify() to turn the array of option objects into string to pass as tag
     *       active - the index number of the selected item
     * 3. Add an event handler to get notified when someone selects an option:
     *      this.querySelector("#someIdYouCreate").addEventListener("change", (ev) => this.yourHandlerFunction(ev));     * To Get Selected Item, in the using javascript:
     * 4. Create the handler function:
     *   myHandlerFunction(ev) {
     *     // to get value selected
     *     let selectedItem = ev.target.getAttribute("value");
     *    // to get value selected
     *    let selectedItemIndex = ev.target.getAttribute("index");
     *   }
     */
    constructor() {
        super();
        console.log("static/wc/dropdown-list.js")
        this.listArray = null;
        this.listComponent = null;
    }

    connectedCallback() {
        this.innerHTML =
            `
<style> 
  @import url('static/css/bootstrap.min.css');
  @import url('static/img/bootstrap-icons.css');
</style>
<style>
.dropdown-menu {
  max-height: 100px;
  overflow-y: scroll;
}
</style>
 <div class="dropdown">
 <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" data-bs-display="static" aria-expanded="false">
 ${this.title}
 </button>
 <ul id="listComponent_${this.gid}" class="dropdown-menu dropdown-menu-dark .dropdown-menu{-sm|-md|-lg|-xl|-xxl}-end." aria-labelledby="dropdownMenuButton1">
 </ul>
 </div>
`;
        this.listComponent = this.querySelector(`#listComponent_${this.gid}`);
        this.listComponent.addEventListener("click", (e) => this.fireChangeEvent(e));
        if (this.listArray && this.listArray.length > 0) {
            this.renderList()
        }
    }
    // for every event handler added above, below remove it
    disconnectedCallback() {
        this.listComponent.removeEventListener("click", (e) => this.fireChangeEvent(e));
    }

    fireChangeEvent(ev) {
        if (ev && ev.target)
        this.dispatchEvent(new CustomEvent('listSelectionChange', {
            detail: {
                name: 'listSelectionChange', // this is the name to use in handler
                key: ev.target.getAttribute("data-key"),
            },
            bubbles: true
        }));
    }

    renderItem(item, index) {
        // <li><a className="dropdown-item" href="#">Action</a></li>
        let theLi = document.createElement("li");
        theLi.innerHTML = `<a class="dropdown-item" data-key="${item.Key}" href="#">${item.Desc}</a>`
        this.listComponent.appendChild(theLi);
    }

    renderList() {
        if (! this.listComponent) {
            return;
        }
        this.listComponent.innerHTML = "";
        if (this.listComponent) {
            this.listArray.forEach((item, index) => {
                item.Selected = index === this.active;
                this.renderItem(item, index);
            });
        }
    }

    static get observedAttributes() {
        return ['gid', 'items', 'active', 'title'];
    }

    //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
        switch (name) {
            case "items": {
                if (newVal !== null && oldVal !== newVal) {
                    this.listArray = JSON.parse(newVal);
                    this.renderList();
                }
                break;
            }
            case "active": {
                if (newVal !== null && oldVal !== newVal) {
                    this.renderList();
                }
                break;
            }
        }
    }

    adoptedCallback() {
    }

    get gid() { // group id
        return this.getAttribute("gid");
    }
    set gid(value) {
        this.setAttribute("gid", value);
    }

    get items() {
        return this.getAttribute("items");
    }
    set items(value) {
        this.setAttribute("items", value);
    }

    get active() {
        return this.getAttribute("active");
    }
    set active(value) {
        this.setAttribute("active", value);
    }
    get title() { // group id
        return this.getAttribute("title");
    }
    set title(value) {
        this.setAttribute("title", value);
    }
}

window.customElements.define('dropdown-list', DropdownList);