// LogViewer
//  Provides a data table view of entries in the current log file.
//  It calls the endpoint /api/logs
//  The handler pkg/server/handlers.go, func handleLogRequest()
//  calls pkg/dlog/dlog.go, func LogEntries to get the entries from the file.
//  At this time, it only reads the current log file.
//  TODO: add the ability for the user to select a log file from a list of available log files
import * as utils from "./utils.js";

class LogResponse {
}
LogResponse.prototype.FilePath = [];
LogResponse.prototype.Message = "";
LogResponse.prototype.Status = "";
LogResponse.prototype.TableData = "";

class LogViewer extends HTMLElement {
  constructor() {
    super();
    console.log("static/wc/log-viewer.js")
    this.dataTable = null;
  }

  connectedCallback() {
    this.innerHTML =
        `
<style> 
  @import url('static/css/bootstrap.min.css');
  @import url('static/img/bootstrap-icons.css');
</style>
<br>
<div id="my-element-div" class="wc-title">Log Viewer</div>
<br>
<hr>
<br>
<div style="display: flex; flex-direction: column; flex-wrap: wrap; gap: 1rem; max-width: 95vw;">
  <div style="display: flex; flex-direction: row; gap: 20px;">  <!-- Submit Button -->
      <span>
          <button id="btnRequest"  type="button" class="btn btn-primary" style="">Load / Reload</button>
      </span>
      <br>
      <div class="alert alert-light message" role="alert" id="requestStatus" style="padding: 3px; width: max-content;">&nbsp;</div>
  </div> <!-- end Submit Button -->
<br>
<data-table id="dataTable" class="d-none" prefix="${this.prefix}-table" data=""></data-table>
`;
    this.divRequestStatus = this.querySelector("#requestStatus");
    this.requestButton = this.querySelector("#btnRequest");
    this.dataTable = this.querySelector("#dataTable");

    // add listeners.  For each one added, remove it in disconnectedCallback.
    this.requestButton.addEventListener("click", (e) => this.handleLogRequest(e))
  }

  disconnectedCallback() {
    this.requestButton.removeEventListener("click", (ev) => this.handleLogRequest(ev))
  }

  // ---- MESSAGE Handlers
  requestStatusMessage(type, message) {
    this.divRequestStatus.className = "";
    this.divRequestStatus.classList.add(type);
    this.divRequestStatus.innerHTML = utils.getMessageSpan(type, message);
  }

  // event handler functions
  handleLogRequest(ev) {
    if (ev) {
      ev.stopPropagation();
      ev.preventDefault();
    }
    this.requestStatusMessage(utils.alerts.Primary, "Requesting log entries...");
    this.requestLogEntries();
  }

  // ---- REST API Handlers
  requestLogEntries() {
    const url = this.host
        + `api/log`
//        + `src=` + encodeURIComponent(this.src)
    ;

    utils.callApi(url, "GET").then(response => {
      let theResponse = null;
      try {
        theResponse = Object.assign(new LogResponse(), response);
      } catch (error) {
        this.requestStatusMessage(utils.alerts.Danger, `error converting response to object: ${error.message}`);
        return
      }
      if (theResponse.Status === "OK") {
        this.dataTable.classList.remove("d-none");
        this.dataTable.setAttribute("data", JSON.stringify(response.TableData));
        this.requestStatusMessage(utils.alerts.Info, `${theResponse.Message}`);
      } else {
        this.requestStatusMessage(utils.alerts.Danger, `${theResponse.Message}`);
      }
    }).catch(error => {
      this.requestStatusMessage(utils.alerts.Danger, error)
    });
  }
// ---- End REST API Handlers

  static get observedAttributes() {
    return ['host','prefix'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
  attributeChangedCallback(name, oldVal, newVal) {
  }

  adoptedCallback() {
  }
  get host() {
    return this.getAttribute("host");
  }

  set host(value) {
    this.setAttribute("host", value);
  }
  get prefix() {
    return this.getAttribute("host");
  }

  set prefix(value) {
    this.setAttribute("host", value);
  }
}

window.customElements.define('log-viewer', LogViewer);