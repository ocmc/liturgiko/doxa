import * as utils from "./utils.js";

class RegexTester extends HTMLElement {
  constructor() {
    super();
    console.log("static/wc/regex-tester.js")
    this.lsPrefix = "regXT";
    this.lsCaseInsensitive = "caseInsensitive";
    this.lsDiacriticInsenstive = "diacriticInsensitive";
    this.lsGreek = "greek";
    this.lsWholeWord = "wholeWord";
    this.useNfd = false;
    this.unicodeForm = "nfc";
    this.spanish = "Soberano filántropo, haz brillar en nuestros";
    this.greeklish = `    ΑαᾼΆᾺᾹᾸᾷᾶᾴᾳᾲᾱᾰᾏᾎᾍᾌᾋᾊᾉᾈᾇᾆᾅᾄᾃᾂᾁᾀάὰἏἎἍἌἋἊἉἈἇἆἅἄἃἂἁἀ βΒ γΓ δΔ ζΖ ΗηῌΉῊῇῆῄῃῂᾟᾞᾝᾜᾛᾚᾙᾘᾗᾖᾕᾔᾓᾒᾑᾐήὴἯἮἭἬἫἪἩἨἧἦἥἤἣἢἡἠ θΘ ΕΈῈεέὲἝἜἛἚἙἘἕἔἓἒἑἐ ΙιΊῚῙῘῗῖΐῒῑῐίὶἿἾἽἼἻἺἹἸἷἶἵἴἳἲἱἰ κΚ λΛ μΜ νΝ ξΞ ΟοΌῸόὸὍὌὋὊὉὈὅὄὃὂὁὀ πΠ ΡρῬῥῤ σΣς τΤ ΥυΎῪῩῨῧῦΰῢῡῠύὺὟὝὛὙὗὖὕὔὓὒὑὐ φΦ χΧ ψΨ ΩωῼΏῺῷῶῴῳῲᾯᾮᾭᾬᾫᾪᾩᾨᾧᾦᾥᾤᾣᾢᾡᾠώὼὯὮὭὬὫὪὩὨὧὦὥὤὣὢὡὠ ,.;·«» 
    aA bB cC dD eE fF gG hH iI jJ kK lL mM nN oO pP qQ rR sS tT uU vV wW xX yY zZ ~!@#$%^&*()_+-=-+{}[]\|;':"<>?,./1234567890 1.2 1.03 1,030 1,030.2
    
    Ἀγάπη μου, ἀγάπη μου, ἀγαπημένος.  Joy joy joyful.
`;

    // input handles
    this.inputRegEx = null;
    this.inputText = null;

    // output handles
    this.divResults = null;
    this.paraexpandedRegEx = null;
    this.paraGoFlags = null;
    this.divTime = null;
    this.divResultsMeta = null;

    this.divRequestStatus = "";

    // parameters
    this.text = "";
    this.pattern = "";
    this.caseInsensitive = false;
    this.diacriticInsensitive = false;
    this.greek = false;
    this.wholeWord = false;
  }

  connectedCallback() {
    this.innerHTML =
`
    <style> 
      @import url('static/css/bootstrap.min.css');
      @import url('static/img/bootstrap-icons.css');
    </style>
<style>
  body {background-color: #fff;}
</style>
<br>
<div id="my-element-div" class="wc-title">Regular Expression Tester</div>
<br>
<hr>
<div>Use this page to write and test a regular expression before you use in it in the Database Explorer.</div>
<br>
<div>For a tester with more features than the one offered here, search online, or use <a href="https://regex101.com">this one</a>. Set the <em>flavor</em> to golang. Copy the expression from there into the one here on this page.</div>
<br>
<div>If you use a regular expression tester on another website, be sure to also test it here to make sure Doxa will give the same results.</div>
<br>
<div>Caution: go implements regular expressions version 2 (<em>re2</em>), which is more restrictive than what javascript supports.  For information, see the <a href="https://github.com/google/re2/wiki/Syntax" target="_blank">re2 syntax</a>.  With Doxa, do not use anything marked in <em>re2</em> as NOT SUPPORTED.</div>
<br>
<hr>
<div style="display: flex; flex-direction: column; flex-wrap: wrap; gap: 1rem; max-width: 90vw;">
  <!-- text input -->
  <label for="inputText" class="form-label">Text</label>
  <textarea id="inputText" placeholder="text to test" rows="4" class="form-control" aria-labelledby="idHelpBlockText">
  ${this.spanish}
  </textarea>
  <!-- regex input -->
  <label for="inputRegEx" class="form-label">Regular Expression</label>
  <regex-input id="inputRegEx" items="[]" active="0" title="regEx" placeholder="regular expression" gid="rxTester" formEnabled="true" ci="false" di="false" gr="false" ww="false" helpEnabled="true"  aria-labelledby="idHelpBlockRegEx"></regex-input>
</div>
<!-- results metadata -->
<div id="resultsMeta" class="d-none">
  <br>
  <div id="expandedRegEx"></div>
  <div id="goFlags"></div>
  <div id="jsFlags"></div>
  <div id="unicodeForm"></div>
  <div id="time"></div>
</div>
<br>
  <!-- status -->
<div class="alert alert-light message" role="alert" id="requestStatus" style="padding: 3px; width: max-content;"></div>
<hr>
<! -- Results-->
<div id="divResults"></div>
<br>
<br>
<br>
`;
    this.getLocalStorageRegExFlags();
    this.divResultsMeta = this.querySelector("#resultsMeta");
    this.divTime = this.querySelector("#time");
    this.inputRegEx = this.querySelector("#inputRegEx");
    if (this.inputRegEx) {
      this.inputRegEx.setAttribute("items", JSON.stringify(utils.comboCategories));
      this.setWcRegExFlags();
    }
    this.inputText = this.querySelector("#inputText");
    this.divResults = this.querySelector("#divResults");
    this.divRequestStatus = this.querySelector("#requestStatus");
    this.paraexpandedRegEx = this.querySelector("#expandedRegEx");
    this.paraGoFlags = this.querySelector("#goFlags");
    this.paraJsFlags = this.querySelector("#jsFlags");
    this.paraForm = this.querySelector("#unicodeForm");
    // event handlers
    if (this.inputRegEx) {
      this.inputRegEx.addEventListener("inputChange", (e) => this.handeInputChange(e));
      this.inputRegEx.addEventListener("enterKeyPressed", (e) => this.handleEnterKeyPressed(e));
      this.inputRegEx.addEventListener("flagCiChange", (e) => this.handleFlagCiChange(e));
      this.inputRegEx.addEventListener("flagDiChange", (e) => this.handleFlagDiChange(e));
      this.inputRegEx.addEventListener("flagGrChange", (e) => this.handleFlagGrChange(e));
      this.inputRegEx.addEventListener("flagWwChange", (e) => this.handleFlagWwChange(e));
    }
  }

  disconnectedCallback() {
    if (this.inputRegEx) {
      this.inputRegEx.removeEventListener("inputChange", (e) => this.handeInputChange(e));
      this.inputRegEx.removeEventListener("enterKeyPressed", (e) => this.handleEnterKeyPressed(e));
      this.inputRegEx.removeEventListener("flagCiChange", (e) => this.handleFlagCiChange(e));
      this.inputRegEx.removeEventListener("flagDiChange", (e) => this.handleFlagDiChange(e));
      this.inputRegEx.removeEventListener("flagWwChange", (e) => this.handleFlagWwChange(e));
    }
  }

  handleFlagCiChange(e) {
    if (e && e.detail) {
      this.caseInsensitive = e.detail.value === "true";
      utils.lsSetBool(this.lsPrefix, this.lsCaseInsensitive, JSON.stringify(this.caseInsensitive));
    }
  }
  handleFlagDiChange(e) {
    if (e && e.target) {
      this.diacriticInsensitive = e.detail.value === "true";
      utils.lsSetBool(this.lsPrefix, this.lsDiacriticInsenstive, JSON.stringify(this.diacriticInsensitive));
    }
  }
  handleFlagGrChange(e) {
    if (e && e.target) {
      this.greek = e.detail.value === "true";
      utils.lsSetBool(this.lsPrefix, this.lsGreek, JSON.stringify(this.greek));
    }
  }
  handleFlagWwChange(e) {
    if (e && e.target) {
      this.wholeWord = e.detail.value === "true";
      utils.lsSetBool(this.lsPrefix, this.lsWholeWord, JSON.stringify(this.wholeWord));
    }
  }
  getLocalStorageRegExFlags() {
    this.caseInsensitive = utils.lsGetBool(this.lsPrefix, this.lsCaseInsensitive);
    this.diacriticInsensitive = utils.lsGetBool(this.lsPrefix, this.lsDiacriticInsenstive);
    this.greek = utils.lsGetBool(this.lsPrefix, this.lsGreek);
    this.wholeWord = utils.lsGetBool(this.lsPrefix, this.lsWholeWord);
  }
  setLocalStorageRegExFlags() {
    utils.lsSetBool(this.lsPrefix, this.lsCaseInsensitive, JSON.stringify(this.caseInsensitive));
    utils.lsSetBool(this.lsPrefix, this.lsDiacriticInsenstive, JSON.stringify(this.diacriticInsensitive));
    utils.lsSetBool(this.lsPrefix, this.lsGreek, JSON.stringify(this.greek));
    utils.lsSetBool(this.lsPrefix, this.lsWholeWord, JSON.stringify(this.wholeWord));
  }
  setWcRegExFlags() {
    this.inputRegEx.setAttribute("ci", JSON.stringify(this.caseInsensitive));
    this.inputRegEx.setAttribute("di", JSON.stringify(this.diacriticInsensitive));
    this.inputRegEx.setAttribute("gr", JSON.stringify(this.greek));
    this.inputRegEx.setAttribute("ww", JSON.stringify(this.wholeWord));
  }
  resetResultsMeta() {
    this.divResultsMeta.classList.add("d-none");
    this.paraexpandedRegEx.innerHTML = "";
    this.paraGoFlags.innerHTML = "";
    this.paraJsFlags.innerHTML = ""
    this.paraForm.innerHTML = "";
    this.divTime.innerHTML = "";
  }
  handeInputChange(ev) {
    // to get value selected
    if (ev && ev.detail && ev.detail.value) {
      this.pattern = ev.detail.value;
    }
  }
  handleEnterKeyPressed(ev) {
    if (ev && ev.detail && ev.detail.value) {
      this.pattern = ev.detail.value;
      this.setLocalStorageRegExFlags();
    }
    this.handleRequest(ev);
  }

  handleRequest(ev) {
    if (ev) {
      ev.stopPropagation();
      ev.preventDefault();
    }
    this.resetResultsMeta();
    this.divResults.innerHTML = ""; // clear contents
    this.text = this.inputText.value.normalize("NFC"); // before sending
    if (!this.pattern || this.pattern.length === 0) {
      this.requestStatusMessage(utils.alerts.Success, "missing regular expression");
      return
    }
    if (!this.pattern || this.pattern.length === 0) {
      this.requestStatusMessage(utils.alerts.Success, "missing text");
      return
    }
    this.requestStatusMessage(utils.alerts.Primary, "Testing...");
    this.requestRegExParse();
  }

  // ---- MESSAGE Handlers
  requestStatusMessage(type, message) {
    this.divRequestStatus.className = "";
    this.divRequestStatus.classList.add(type);
    this.divRequestStatus.innerHTML = utils.getMessageSpan(type, message);
  }
  // ---- REST API Handlers

  requestRegExParse() {
    const url = this.host
        + `api/regex?caseInsensitive=${this.caseInsensitive}`
        + `&diacriticInsensitive=${encodeURIComponent(this.diacriticInsensitive)}`
        + `&greek=${encodeURIComponent(this.greek)}`
        + `&wholeWord=${encodeURIComponent(this.wholeWord)}`
        + `&pattern=${encodeURIComponent(this.pattern)}`
        + `&text=${encodeURIComponent(this.text)}`
    ;
    utils.callApi(url, "GET").then(response => {
      if (response.Status === "OK") {
        this.requestStatusMessage(utils.alerts.Success, response.Message);
        this.useNfd = response.NFD;
        if (this.useNfd) {
          this.unicodeForm = "NFD";
          this.resultText = this.text.normalize('NFD');
        } else {
          this.unicodeForm = "NFC";
          this.resultText = this.text.normalize('NFC');
        }
        if (response.ExpandedPattern) {
          let flags = "gu";
          if (this.caseInsensitive) {
            flags = flags + `i`;
          }
          this.paraexpandedRegEx.innerHTML = `<span>Expanded regEx: ${response.ExpandedPattern}</span>`;
          this.paraGoFlags.innerHTML = `<span>Global go flags:&nbsp;${response.GoFlags}</span>`;
          this.paraJsFlags.innerHTML = `<span>Global js&nbsp;&nbsp;flags:&nbsp;${flags}</span>`;
          this.paraForm.innerHTML = `<span>Unicode form:&nbsp;${this.unicodeForm}</span>`;
          this.divTime.innerHTML = `<span>Elapsed time: ${response.Time}</span>`
          this.divResultsMeta.classList.remove("d-none");
          let filterRegEx = null;
          try {
            let i = 0;
            filterRegEx = new RegExp(response.ExpandedPattern, flags);
            // highlight the words matching the filter pattern
            this.divResults.innerHTML = this.resultText.replace(filterRegEx, (match) => {
              i++
              return `<span style="color: red;">${match}</span>`;
            });
            this.divResults.innerHTML = this.divResults.innerHTML.normalize('NFC');
            this.requestStatusMessage(utils.alerts.Success, `${response.Message} js: ${i} matches`);
          } catch (err) {
            this.requestStatusMessage(`js error parsing ${response.ExpandedPattern} with flags ${flags}: ${err.message}`)
          }
        }
      } else {
        this.requestStatusMessage(utils.alerts.Danger, response.Message)
      }
    }).catch(error => {
        this.requestStatusMessage(utils.alerts.Danger, error.message)
    });
  }

// ---- End REST API Handlers

  static get observedAttributes() {
    return ['host'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
  }

  adoptedCallback() {
  }
  get host() {
    return this.getAttribute("host");
  }

  set host(value) {
    this.setAttribute("host", value);
  }
}

window.customElements.define('regex-tester', RegexTester);