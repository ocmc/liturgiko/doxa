/**
 * @tagname topic-key-compare
 * @description A component that creates a comparative table of values for the same key across different libraries.
 * Particularly useful for comparing liturgical or biblical text translations across different sources.
 * Special handling is provided for liturgical texts (ltx/) with additional redirect capabilities.
 *
 * @dependencies None - Uses Bootstrap CSS and icons but no other web components
 * @inputs {Attribute} recId - The record ID to compare across libraries
 * @inputs {Attribute} showRedirects - Whether to show redirects in the comparison (true/false)
 * @outputs {Event} redirectClick - Fires when a redirect link is clicked with detail: {redirectId: String}
 * @outputs {Attribute} idForRedirect - Set when a redirect link is clicked, contains "@" + selected record ID (legacy support)
 * @api {GET} /api/cmp - Fetches comparative values for the given record ID
 * @watches {Attribute} recId - Re-fetches data when this attribute changes
 * 
 * @examples
 * 
 * Basic usage:
 * ~~~html
 * <topic-key-compare id="compare-table" recId="ltx/en_us_dedes/ho.ho23"></topic-key-compare>
 * ~~~
 * 
 * Showing redirects:
 * ~~~html
 * <topic-key-compare 
 *   id="compare-with-redirects" 
 *   recId="ltx/en_us_dedes/ho.ho23" 
 *   showRedirects="true">
 * </topic-key-compare>
 * ~~~
 * 
 * With event handling for redirects (recommended):
 * ~~~javascript
 * const cmp = document.querySelector("#compare-table");
 * // Listen for the redirectClick custom event
 * cmp.addEventListener('redirectClick', (event) => {
 *   const redirectTarget = event.detail.redirectId;
 *   console.log('Selected redirect:', redirectTarget);
 *   // Handle the redirect selection
 * });
 * ~~~
 * 
 * With legacy attribute-based redirect handling:
 * ~~~javascript
 * const cmp = document.querySelector("#compare-table");
 * // Listen for attribute changes to detect when a redirect is selected
 * const observer = new MutationObserver((mutations) => {
 *   mutations.forEach((mutation) => {
 *     if (mutation.attributeName === 'idForRedirect') {
 *       const redirectTarget = cmp.getAttribute('idForRedirect');
 *       console.log('Selected redirect:', redirectTarget);
 *       // Handle the redirect selection
 *     }
 *   });
 * });
 * observer.observe(cmp, { attributes: true });
 * ~~~
 * 
 * Used within modal-edit-record (typical use case):
 * ~~~javascript
 * // This component is typically used within modal-edit-record to show
 * // alternative versions of the same text across different libraries
 * if (this.recId.startsWith("btx/") || this.recId.startsWith("ltx/")) {
 *   this.querySelector("#topic-key-compare-id").innerHTML = 
 *     `<topic-key-compare id="topic-key-compare-id" 
 *       recId="${this.recId}" showRedirects="${this.showRedirects}">
 *     </topic-key-compare>`;
 * }
 * ~~~
 */
class TopicKeyCompare extends HTMLElement {
  constructor() {
    super();
    console.log("static/wc/topic-key-compare.js")
  }

  connectedCallback() {
    let actionHtml = "";
    if (this.recId.startsWith("ltx/")) {
      actionHtml = `<th scope="col" style="width: 5%;"></th>`
    }
    this.innerHTML = `
    <style> 
      @import url('static/css/bootstrap.min.css');
      @import url('static/img/bootstrap-icons.css');
    </style>
    <style>
      .control-label {
        color: #5bc0de;
      }
    </style>

    <div class="row p-1">
        <div class="col">
            <div class="table-responsive">
                <table id="topic-key-compare-table" class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col" style="width: 10%;">Library</th>
                        <th scope="col" style="width: 85%;">Value</th>
                        ${actionHtml}
                    </tr>
                    </thead>
                    <tbody id="topic-key-compare-tbody">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    `
    this.querySelector("#topic-key-compare-table").addEventListener('click', (ev) => this.handleTableClick(ev));
    this.fetchRecords().then();
  }

  disconnectedCallback() {
  }

  static get observedAttributes() {
    return ['recId'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
  }

  adoptedCallback() {
  }


  get idForRedirect() {
    return this.getAttribute("idForRedirect");
  }

  set idForRedirect(value) {
    this.setAttribute("idForRedirect", value);
  }
  get recId() {
    return this.getAttribute("recId");
  }

  set recId(value) {
    this.setAttribute("recId", value);
  }

  get showRedirects() {
    return this.getAttribute("showRedirects");
  }

  set showRedirects(value) {
    this.setAttribute("showRedirects", value);
  }
  async fetchRecords() {
    const url = `api/cmp?path=${this.recId}&empty=false&showRedirects=${this.showRedirects}`
    console.log(url)
    return new Promise(async (res, rej) => {
      await fetch(url)
          .then(data => data.json())
          .then((json) => {
            this.renderData(json);
            res();
          })
          .catch((error) => rej(error));
    })
  }

  renderData(data) {
    if (!data.Values) {
      return;
    }
    const table = this.querySelector('#topic-key-compare-tbody');
    let greekRow = {};

    // add new rows
    data.Values.forEach((item, index) => {
      if (this.recId === item.ID) {
        return; // equivalent of continue since this is a forEach function instead of a loop
      }
      const tr = document.createElement('tr');
      tr.setAttribute("id", "topic-key-compare-table-row-" + index)

      // library part of ID
      const tdLibrary = document.createElement('td');
      const parts = item.ID.split("/");
      if (parts.length > 1) {
        tdLibrary.innerText = parts[1];
      } else {
        tdLibrary.innerText = item.ID;
      }
      // value
      const tdValue = document.createElement('td');
      tdValue.innerText = item.Value;

      // lookup action column and set data-id with the ID for retrieval when clicked by user
      const tdAction = document.createElement('td');
      tdAction.innerHTML = `<span class="control-label" style="cursor: pointer;" data-type="makeLookup" data-id="${item.ID}">@</span>`;

      tr.appendChild(tdLibrary);
      tr.appendChild(tdValue);

      if (item.ID.startsWith("ltx/")) {
        tr.appendChild(tdAction);
      }

      // save for end of table
      if (parts[1] === "gr_gr_cog") {
        greekRow = tr;
        return;
      }
      table.appendChild(tr);
    });
    // add Greek last so easily seen during translation
    if (greekRow && greekRow instanceof Node) {
      table.appendChild(greekRow);
    }
  }
  handleTableClick(ev) {
    let theId = ev.target.getAttribute("data-id")
    if (!theId) {
      return;
    }
    
    // Dispatch a custom event with the redirect information
    const redirectEvent = new CustomEvent('redirectClick', {
      bubbles: true,
      composed: true,
      detail: { redirectId: "@" + theId }
    });
    this.dispatchEvent(redirectEvent);
    
    // Keep the old behavior for backward compatibility
    this.idForRedirect = "@" + theId;
  }
}
window.customElements.define('topic-key-compare', TopicKeyCompare);