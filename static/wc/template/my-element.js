import * as utils from "./utils.js";

class MyElement extends HTMLElement {
  constructor() {
    super();
    console.log("static/wc/db-importer.js")
  }

  connectedCallback() {
    this.innerHTML =
        `
<style> 
  @import url('static/css/bootstrap.min.css');
  @import url('static/img/bootstrap-icons.css');
</style>
<br>
<div id="my-element-div" class="wc-title">Unicode Tools</div>
<br>
<hr>
<br>
<div style="display: flex; flex-direction: column; flex-wrap: wrap; gap: 1rem; max-width: 50vw;">
  <div style="display: flex; flex-direction: row; gap: 20px;">  <!-- Submit Button -->
      <span>
          <button id="btnRequest"  type="button" class="btn btn-primary" style="">Import</button>
      </span>
      <br>
      <div class="alert alert-light message" role="alert" id="requestStatus" style="padding: 3px; width: max-content;">&nbsp;</div>
  </div> <!-- end Submit Button -->
<br>
`;
    this.divRequestStatus = this.querySelector("#requestStatus");
    this.requestButton = this.querySelector("#btnRequest");

    // add listeners.  For each one added, remove it in disconnectedCallback.
    this.requestButton.addEventListener("click", (e) => this.handleImport(e))
  }

  disconnectedCallback() {
    this.requestButton.removeEventListener("click", (ev) => this.handleImport(ev))
  }

  // ---- MESSAGE Handlers
  requestStatusMessage(type, message) {
    this.divRequestStatus.className = "";
    this.divRequestStatus.classList.add(type);
    this.divRequestStatus.innerHTML = utils.getMessageSpan(type, message);
  }
  // ---- REST API Handlers
  requestFileList() {
    const url = this.host
        + `api/db/import/list`
//        + `src=` + encodeURIComponent(this.src)
    ;

    utils.callApi(url, "GET").then(response => {
      if (response.Status === "OK") {
      } else {
        this.requestStatusMessage(utils.alerts.Danger, response.Status)
      }
    }).catch(error => {
      this.requestStatusMessage(utils.alerts.Danger, error)
    });
  }
// ---- End REST API Handlers

  static get observedAttributes() {
    return ['host'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
  attributeChangedCallback(name, oldVal, newVal) {
  }

  adoptedCallback() {
  }
  get host() {
    return this.getAttribute("host");
  }

  set host(value) {
    this.setAttribute("host", value);
  }
}

window.customElements.define('my-element', MyElement);