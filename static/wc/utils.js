// alert types enum.  Must match bootstrap 5 alert class types
export const alerts = {
    Clear: `clear`, // used to clear the message
    Danger: 'alert-danger',
    Dark: 'alert-dark',
    Info: 'alert-info',
    Light: 'alert-light',
    Primary: 'alert-primary',
    Secondary: 'alert-secondary',
    Success: 'alert-success',
    Warning: 'alert-warning',
}
export function getAlert(type, message) {
    return `<div class="alert ${type}" role="alert">
                ${getMessageSpan(type, message)}
            </div>`
}
// function GetToday returns the current date
// in the format YYYY-MM-DD
export function getToday() {
    const today = new Date();
    const day = today.getDate().toString().padStart(2, '0');
    const month = (today.getMonth() + 1).toString().padStart(2, '0');
    const year = today.getFullYear();
    return `${year}-${month}-${day}`;
}
// function isValidDateFormat checks to see if the supplied
// date has the format yyyy-mm-dd.
export function isValidDateFormat(d) {
    if (!d) {
        return true;
    }
    let parts = d.split("-")
    // if we split using -, we expect three parts
    if (parts.length !== 3) {
        return false;
    }
    // year
    if (parts[0].length !== 4) {
        return false;
    }
    // month
    if (parts[1].length !== 2) {
        return false;
    }
    // day
    return parts[2].length === 2;
}
export function getMessageSpan(type, message) {
    switch (type) {
        case alerts.Clear: {
            return "";
        }
        case alerts.Success: {
            return `<span><i class="bi bi-check-circle">&nbsp;</i>${message}</span>`
        }
        case alerts.Danger, alerts.Warning: {
            return `<span><i class="bi bi-exclamation-triangle">&nbsp;</i>${message}</span>`
        }
        default: {
            return `<span style="color: blue;"><i class="bi bi-info-circle">&nbsp;</i>${message}</span>`
        }
    }
}
export function toBytesArray(str) {
    const encoder = new TextEncoder();
    return encoder.encode(str);
}
export function lsGet(prefix, key) {
    return localStorage.getItem(`${prefix}:${key}`);
}
export function lsRemove(prefix, key) {
    return localStorage.removeItem(`${prefix}:${key}`);
}
export function lsSet(prefix, key, value) {
    localStorage.setItem(`${prefix}:${key}`, value);
}
export function lsGetBool(prefix, key) {
    const val = localStorage.getItem(`${prefix}:${key}`);
    return val === "true";
}
export function lsSetBool(prefix, key, value) {
    localStorage.setItem(`${prefix}:${key}`, value);
}

// remove diacritics (e.g. accents) from a string and convert to lower case
export function toNormalForm(str) {
    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
}
export function isPositiveInteger(str) {
    if (typeof str !== 'string') {
        return false;
    }
    const num = Number(str);
    if (Number.isInteger(num) && num > 0) {
        return true;
    }
    return false;
}

export async function  callApi(url, method) {

    let fetchData = {
        method: `${method}`
    }
    const response = await fetch(url, fetchData);
    if (!response.ok) {
        let message = "";
        switch (response.status) {
            case 302: {
                message += response.Message
                break;
            }
            case 404: {
                message += "not found";
                break;
            }
            case 500: {
                message += "doxa server error";
                break;
            }
            default: {
                message += `An error has occurred: ${response.status}`;
            }
        }
        throw new Error(message);
    }
    const responseJson = await response.json();
    return responseJson;
}
export function langCodeFromLibrary(library) {
    const parts = library.split("_");
    return parts[0];
}
export function strToIntArray(str) {
    const arr = str
        .split(',')
        .filter(element => {
            if (element.trim() === '') {
                return false;
            }
            return !isNaN(element);
        })
        .map(element => {
            return Number(element);
        });
    return arr;
}
export function topicKeyFromId(i) {
    let parts = i.split("/");
    return parts[parts.length-1]
}
export function getLastFromTrace(t) {
    let parts = t.split(">");
    return parts[parts.length-1].trim();
}
export const doxaCategories = [
    { Key: `\\μ(α)`, Desc: `\\μ(α): any lowercase variant of α.`},
    { Key: `\\μ(Α)`, Desc: `\\μ(Α): any uppercase variant of α.`},
    { Key: `\\μ(ε)`, Desc: `\\μ(ε): any lowercase variant of ε.`},
    { Key: `\\μ(Ε)`, Desc: `\\μ(Ε): any uppercase variant of ε.`},
    { Key: `\\μ(η)`, Desc: `\\μ(η): any lowercase variant of η.`},
    { Key: `\\μ(Η)`, Desc: `\\μ(Η): any uppercase variant of η.`},
    { Key: `\\μ(ι)`, Desc: `\\μ(ι): any lowercase variant of ι.`},
    { Key: `\\μ(Ι)`, Desc: `\\μ(Ι): any uppercase variant of ι.`},
    { Key: `\\μ(ο)`, Desc: `\\μ(ο): any lowercase variant of ο.`},
    { Key: `\\μ(Ο)`, Desc: `\\μ(Ο): any uppercase variant of ο.`},
    { Key: `\\μ(ω)`, Desc: `\\μ(ω): any lowercase variant of ω.`},
    { Key: `\\μ(Ω)`, Desc: `\\μ(Ω): any uppercase variant of ω.`},
    { Key: `\\μ(υ)`, Desc: `\\μ(υ): any lowercase variant of υ.`},
    { Key: `\\μ(Υ)`, Desc: `\\μ(Υ): any uppercase variant of υ.`},
    { Key: `\\μ(ρ)`, Desc: `\\μ(ρ): any lowercase variant of ρ.`},
    { Key: `\\μ(Ρ)`, Desc: `\\μ(Ρ): any uppercase variant of ρ.`},
];
export const unicodeCategories = [
    { Key: `\\p{L}`, Desc: `\\p{L} or \\p{Letter}: any kind of letter from any language.`},
    { Key: `\\p{Ll}`, Desc: `\\p{Ll} or \\p{Lowercase_Letter}: a lowercase letter that has an uppercase variant.`},
    { Key: `\\p{Lu}`, Desc: `\\p{Lu} or \\p{Uppercase_Letter}: an uppercase letter that has a lowercase variant.`},
    { Key: `\\p{Lt}`, Desc: `\\p{Lt} or \\p{Titlecase_Letter}: a letter that appears at the start of a word when only the first letter of the word is capitalized.`},
    { Key: `\\p{L&}`, Desc: `\\p{L&} or \\p{Cased_Letter}: a letter that exists in lowercase and uppercase variants (combination of Ll, Lu and Lt).`},
    { Key: `\\p{Lm}`, Desc: `\\p{Lm} or \\p{Modifier_Letter}: a special character that is used like a letter.`},
    { Key: `\\p{Lo}`, Desc: `\\p{Lo} or \\p{Other_Letter}: a letter or ideograph that does not have lowercase and uppercase variants.`},
    { Key: `\\p{M}`, Desc: `\\p{M} or \\p{Mark}: a character intended to be combined with another character (e.g. accents, umlauts, enclosing boxes, etc.).`},
    { Key: `\\p{Mn}`, Desc: `\\p{Mn} or \\p{Non_Spacing_Mark}: a character intended to be combined with another character without taking up extra space (e.g. accents, umlauts, etc.).`},
    { Key: `\\p{Mc}`, Desc: `\\p{Mc} or \\p{Spacing_Combining_Mark}: a character intended to be combined with another character that takes up extra space (vowel signs in many Eastern languages).`},
    { Key: `\\p{Me}`, Desc: `\\p{Me} or \\p{Enclosing_Mark}: a character that encloses the character it is combined with (circle, square, keycap, etc.).`},
    { Key: `\\p{Z}`, Desc: `\\p{Z} or \\p{Separator}: any kind of whitespace or invisible separator.`},
    { Key: `\\p{Zs}`, Desc: `\\p{Zs} or \\p{Space_Separator}: a whitespace character that is invisible, but does take up space.`},
    { Key: `\\p{Zl}`, Desc: `\\p{Zl} or \\p{Line_Separator}: line separator character U+2028.`},
    { Key: `\\p{Zp}`, Desc: `\\p{Zp} or \\p{Paragraph_Separator}: paragraph separator character U+2029.`},
    { Key: `\\p{S}`, Desc: `\\p{S} or \\p{Symbol}: math symbols, currency signs, dingbats, box-drawing characters, etc.`},
    { Key: `\\p{Sm}`, Desc: `\\p{Sm} or \\p{Math_Symbol}: any mathematical symbol.`},
    { Key: `\\p{Sc}`, Desc: `\\p{Sc} or \\p{Currency_Symbol}: any currency sign.`},
    { Key: `\\p{Sk}`, Desc: `\\p{Sk} or \\p{Modifier_Symbol}: a combining character (mark) as a full character on its own.`},
    { Key: `\\p{So}`, Desc: `\\p{So} or \\p{Other_Symbol}: various symbols that are not math symbols, currency signs, or combining characters.`},
    { Key: `\\p{N}`, Desc: `\\p{N} or \\p{Number}: any kind of numeric character in any script.`},
    { Key: `\\p{Nd}`, Desc: `\\p{Nd} or \\p{Decimal_Digit_Number}: a digit zero through nine in any script except ideographic scripts.`},
    { Key: `\\p{Nl}`, Desc: `\\p{Nl} or \\p{Letter_Number}: a number that looks like a letter, such as a Roman numeral.`},
    { Key: `\\p{No}`, Desc: `\\p{No} or \\p{Other_Number}: a superscript or subscript digit, or a number that is not a digit 0–9 (excluding numbers from ideographic scripts).`},
    { Key: `\\p{P}`, Desc: `\\p{P} or \\p{Punctuation}: any kind of punctuation character.`},
    { Key: `\\p{Pd}`, Desc: `\\p{Pd} or \\p{Dash_Punctuation}: any kind of hyphen or dash.`},
    { Key: `\\p{Ps}`, Desc: `\\p{Ps} or \\p{Open_Punctuation}: any kind of opening bracket.`},
    { Key: `\\p{Pe}`, Desc: `\\p{Pe} or \\p{Close_Punctuation}: any kind of closing bracket.`},
    { Key: `\\p{Pi}`, Desc: `\\p{Pi} or \\p{Initial_Punctuation}: any kind of opening quote.`},
    { Key: `\\p{Pf}`, Desc: `\\p{Pf} or \\p{Final_Punctuation}: any kind of closing quote.`},
    { Key: `\\p{Pc}`, Desc: `\\p{Pc} or \\p{Connector_Punctuation}: a punctuation character such as an underscore that connects words.`},
    { Key: `\\p{Po}`, Desc: `\\p{Po} or \\p{Other_Punctuation}: any kind of punctuation character that is not a dash, bracket, quote or connector.`},
    { Key: `\\p{C}`, Desc: `\\p{C} or \\p{Other}: invisible control characters and unused code points.`},
    { Key: `\\p{Cc}`, Desc: `\\p{Cc} or \\p{Control}: an ASCII or Latin-1 control character: 0x00–0x1F and 0x7F–0x9F.`},
    { Key: `\\p{Cf}`, Desc: `\\p{Cf} or \\p{Format}: invisible formatting indicator.`},
    { Key: `\\p{Co}`, Desc: `\\p{Co} or \\p{Private_Use}: any code point reserved for private use.`},
    { Key: `\\p{Cs}`, Desc: `\\p{Cs} or \\p{Surrogate}: one half of a surrogate pair in UTF-16 encoding.`},
    { Key: `\\p{Cn}`, Desc: `\\p{Cn} or \\p{Unassigned}: any code point to which no character has been assigned.`}
];
export const comboCategories = [
    { Key: `\\m(i)`, Desc: `\\m(l): any non-Greek lowercase variant of a letter, e.g. \\m(n) or \\m(o).`},
    { Key: `\\μ(α)`, Desc: `\\μ(α): any lowercase variant of α.`},
    { Key: `\\μ(Α)`, Desc: `\\μ(Α): any uppercase variant of α.`},
    { Key: `\\μ(ε)`, Desc: `\\μ(ε): any lowercase variant of ε.`},
    { Key: `\\μ(Ε)`, Desc: `\\μ(Ε): any uppercase variant of ε.`},
    { Key: `\\μ(η)`, Desc: `\\μ(η): any lowercase variant of η.`},
    { Key: `\\μ(Η)`, Desc: `\\μ(Η): any uppercase variant of η.`},
    { Key: `\\μ(ι)`, Desc: `\\μ(ι): any lowercase variant of ι.`},
    { Key: `\\μ(Ι)`, Desc: `\\μ(Ι): any uppercase variant of ι.`},
    { Key: `\\μ(ο)`, Desc: `\\μ(ο): any lowercase variant of ο.`},
    { Key: `\\μ(Ο)`, Desc: `\\μ(Ο): any uppercase variant of ο.`},
    { Key: `\\μ(ω)`, Desc: `\\μ(ω): any lowercase variant of ω.`},
    { Key: `\\μ(Ω)`, Desc: `\\μ(Ω): any uppercase variant of ω.`},
    { Key: `\\μ(υ)`, Desc: `\\μ(υ): any lowercase variant of υ.`},
    { Key: `\\μ(Υ)`, Desc: `\\μ(Υ): any uppercase variant of υ.`},
    { Key: `\\μ(ρ)`, Desc: `\\μ(ρ): any lowercase variant of ρ.`},
    { Key: `\\μ(Ρ)`, Desc: `\\μ(Ρ): any uppercase variant of ρ.`},
    { Key: `dropdown-divider`, Desc: ``},
    { Key: `\\p{L}`, Desc: `\\p{L} or \\p{Letter}: any kind of letter from any language.`},
    { Key: `\\p{Ll}`, Desc: `\\p{Ll} or \\p{Lowercase_Letter}: a lowercase letter that has an uppercase variant.`},
    { Key: `\\p{Lu}`, Desc: `\\p{Lu} or \\p{Uppercase_Letter}: an uppercase letter that has a lowercase variant.`},
    { Key: `\\p{Lt}`, Desc: `\\p{Lt} or \\p{Titlecase_Letter}: a letter that appears at the start of a word when only the first letter of the word is capitalized.`},
    { Key: `\\p{L&}`, Desc: `\\p{L&} or \\p{Cased_Letter}: a letter that exists in lowercase and uppercase variants (combination of Ll, Lu and Lt).`},
    { Key: `\\p{Lm}`, Desc: `\\p{Lm} or \\p{Modifier_Letter}: a special character that is used like a letter.`},
    { Key: `\\p{Lo}`, Desc: `\\p{Lo} or \\p{Other_Letter}: a letter or ideograph that does not have lowercase and uppercase variants.`},
    { Key: `\\p{M}`, Desc: `\\p{M} or \\p{Mark}: a character intended to be combined with another character (e.g. accents, umlauts, enclosing boxes, etc.).`},
    { Key: `\\p{Mn}`, Desc: `\\p{Mn} or \\p{Non_Spacing_Mark}: a character intended to be combined with another character without taking up extra space (e.g. accents, umlauts, etc.).`},
    { Key: `\\p{Mc}`, Desc: `\\p{Mc} or \\p{Spacing_Combining_Mark}: a character intended to be combined with another character that takes up extra space (vowel signs in many Eastern languages).`},
    { Key: `\\p{Me}`, Desc: `\\p{Me} or \\p{Enclosing_Mark}: a character that encloses the character it is combined with (circle, square, keycap, etc.).`},
    { Key: `\\p{Z}`, Desc: `\\p{Z} or \\p{Separator}: any kind of whitespace or invisible separator.`},
    { Key: `\\p{Zs}`, Desc: `\\p{Zs} or \\p{Space_Separator}: a whitespace character that is invisible, but does take up space.`},
    { Key: `\\p{Zl}`, Desc: `\\p{Zl} or \\p{Line_Separator}: line separator character U+2028.`},
    { Key: `\\p{Zp}`, Desc: `\\p{Zp} or \\p{Paragraph_Separator}: paragraph separator character U+2029.`},
    { Key: `\\p{S}`, Desc: `\\p{S} or \\p{Symbol}: math symbols, currency signs, dingbats, box-drawing characters, etc.`},
    { Key: `\\p{Sm}`, Desc: `\\p{Sm} or \\p{Math_Symbol}: any mathematical symbol.`},
    { Key: `\\p{Sc}`, Desc: `\\p{Sc} or \\p{Currency_Symbol}: any currency sign.`},
    { Key: `\\p{Sk}`, Desc: `\\p{Sk} or \\p{Modifier_Symbol}: a combining character (mark) as a full character on its own.`},
    { Key: `\\p{So}`, Desc: `\\p{So} or \\p{Other_Symbol}: various symbols that are not math symbols, currency signs, or combining characters.`},
    { Key: `\\p{N}`, Desc: `\\p{N} or \\p{Number}: any kind of numeric character in any script.`},
    { Key: `\\p{Nd}`, Desc: `\\p{Nd} or \\p{Decimal_Digit_Number}: a digit zero through nine in any script except ideographic scripts.`},
    { Key: `\\p{Nl}`, Desc: `\\p{Nl} or \\p{Letter_Number}: a number that looks like a letter, such as a Roman numeral.`},
    { Key: `\\p{No}`, Desc: `\\p{No} or \\p{Other_Number}: a superscript or subscript digit, or a number that is not a digit 0–9 (excluding numbers from ideographic scripts).`},
    { Key: `\\p{P}`, Desc: `\\p{P} or \\p{Punctuation}: any kind of punctuation character.`},
    { Key: `\\p{Pd}`, Desc: `\\p{Pd} or \\p{Dash_Punctuation}: any kind of hyphen or dash.`},
    { Key: `\\p{Ps}`, Desc: `\\p{Ps} or \\p{Open_Punctuation}: any kind of opening bracket.`},
    { Key: `\\p{Pe}`, Desc: `\\p{Pe} or \\p{Close_Punctuation}: any kind of closing bracket.`},
    { Key: `\\p{Pi}`, Desc: `\\p{Pi} or \\p{Initial_Punctuation}: any kind of opening quote.`},
    { Key: `\\p{Pf}`, Desc: `\\p{Pf} or \\p{Final_Punctuation}: any kind of closing quote.`},
    { Key: `\\p{Pc}`, Desc: `\\p{Pc} or \\p{Connector_Punctuation}: a punctuation character such as an underscore that connects words.`},
    { Key: `\\p{Po}`, Desc: `\\p{Po} or \\p{Other_Punctuation}: any kind of punctuation character that is not a dash, bracket, quote or connector.`},
    { Key: `\\p{C}`, Desc: `\\p{C} or \\p{Other}: invisible control characters and unused code points.`},
    { Key: `\\p{Cc}`, Desc: `\\p{Cc} or \\p{Control}: an ASCII or Latin-1 control character: 0x00–0x1F and 0x7F–0x9F.`},
    { Key: `\\p{Cf}`, Desc: `\\p{Cf} or \\p{Format}: invisible formatting indicator.`},
    { Key: `\\p{Co}`, Desc: `\\p{Co} or \\p{Private_Use}: any code point reserved for private use.`},
    { Key: `\\p{Cs}`, Desc: `\\p{Cs} or \\p{Surrogate}: one half of a surrogate pair in UTF-16 encoding.`},
    { Key: `\\p{Cn}`, Desc: `\\p{Cn} or \\p{Unassigned}: any code point to which no character has been assigned.`}
];
// Underline adds the unicode underline diacritic \u0332 to letters
// between _, e.g. Resur_rec_tion -> Resur̲r̲e̲ction.
// This is an experimental function to support OCA
// musical notation.  However, it does not address
// line breaks.
export function Underline(s) {
    let on = false;
    let u = "";

    for (let i in s) {
        let token = s[i];
        if (token === "_") {
            on = !on;
            continue;
        }
        u += token;
        if (on) {
            u += "\u0332";
        }
    }
    return u;
}
export function PathSeparator() {
// Get the user agent string
    let uaString = navigator.userAgent;

// Detect the operating system
    let osName = null;
    if (uaString.indexOf("Windows") !== -1) {
        osName = "Windows";
    } else {
        osName = "other";
    }

// Display a message depending on the detected operating system
    if (osName === "Windows") {
        return "\\";
    } else {
        return "/";
    }
}