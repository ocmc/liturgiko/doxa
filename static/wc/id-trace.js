import * as utils from "./utils.js";

class IdTrace extends HTMLElement {
  constructor() {
    super();
    console.log("static/wc/id-trace.js")
    this.dbIdInput = null;
    this.redirectItems = [];
    this.requestedId = "";
    this.divRequestStatus = "";
    this.redirectList = null;
    this.editor = null;
  }

  connectedCallback() {
    this.innerHTML =
`
    <style> 
      @import url('static/css/bootstrap.min.css');
      @import url('static/img/bootstrap-icons.css');
    </style>
<style>body {background-color: #fff;}</style>
<br>
<div id="my-element-div" class="wc-title">Database ID Tracer</div>
<br>
<hr>
<div>The tracer tool allows you to trace a requested ID through all its redirects to the final record and value.</div>
<br>
<hr>
<div style="display: flex; flex-direction: column; flex-wrap: wrap; gap: 1rem; max-width: 50vw;">
  <label for="inputDbId" class="form-label">ID to Trace</label>
  <input type="text" id="inputDbId" placeholder="enter the ID" class="form-control" aria-labelledby="idHelpBlock">
  <div id="idHelpBlock" class="form-text">
  Enter a Database record ID, then click the Trace button.  An example database record ID is ltx/en_us_public/pe.d110:peLI.Kontakion1.melody  
  </div>
  <div style="display: flex; flex-direction: row; gap: 20px;">  <!-- Submit Button -->
      <span>
          <button id="btnRequest"  type="button" class="btn btn-primary" style="">Trace</button>
      </span>
      <br>
      <div class="alert alert-light message" role="alert" id="requestStatus" style="padding: 3px; width: max-content;"></div>
  </div> <!-- end Import Button -->
  </div>
<br>
<hr>
<br>
<! -- Redirect list-->
<div style="display: flex; flex-direction: row; flex-wrap: wrap; gap: 1rem;">
  <ol class="list-group list-group-numbered" id="redirectList"></ol>
</div>
<div id="editor"></div>
`;
    this.editor = this.querySelector("#editor");
    this.redirectList = this.querySelector("#redirectList");
    this.divRequestStatus = this.querySelector("#requestStatus");
    this.dbIdInput = this.querySelector("#inputDbId");
    this.requestButton = this.querySelector("#btnRequest");
    // add listeners.  For each one added, remove it in disconnectedCallback.
    this.redirectList.addEventListener("click", (e) => this.showEditor(e));
    this.requestButton.addEventListener("click", (e) => this.handelTrace(e))
  }

  disconnectedCallback() {
    this.requestButton.removeEventListener("click", (ev) => this.handelTrace(ev))
    this.redirectList.removeEventListener("click", (e) => this.showEditor(e));
  }

  handelTrace(ev) {
    if (ev) {
      ev.stopPropagation();
      ev.preventDefault();
    }
    this.requestedId = this.dbIdInput.value;
    this.requestButton.disabled = true;
    this.redirectList.innerHTML = "";
    this.requestStatusMessage(utils.alerts.Primary, "Tracing...")
    this.requestTrace().then(response => {
      this.requestButton.disabled = false;
      if (response.Status === "OK") {
        this.requestStatusMessage(utils.alerts.Success, response.Message);
        this.redirectItems = response.Redirects;
        const l = this.redirectItems.length;
        this.redirectItems.forEach((i, n) => {
          const item = document.createElement("li");
          item.classList.add("list-group-item");
          item.setAttribute("data-redirect-id", i);
          if (n === l-1 && response.Value) {
            item.innerText = `${i} => ${response.Value}`;
          } else {
            item.innerText = `${i} =>`;
          }
          this.redirectList?.appendChild(item);
        });

      } else if (response.Status === "ERRORS") {
        if (response.Errors) {
          this.requestStatusMessage(utils.alerts.Warning, response.Message)
          response.Errors.forEach((item, index) => {
            const theItem = document.createElement('li');
            theItem.innerText = item;
            this.errorList?.appendChild(theItem);
          });
        } else {
          this.requestStatusMessage(utils.alerts.Warning, "Import finished, but one or more lines had errors. No further information is available.")
        }
      } else {
        this.requestStatusMessage(utils.alerts.Danger, `${response.Status}: ${response.Message}`)
      }
    }).catch(error => {
      this.requestButton.disabled = false;
      this.requestStatusMessage(utils.alerts.Danger, error)
    });
  }
  showEditor(ev) {
    if (ev && ev.target) {
      let value = ev.target.innerText;
      let id = ev.target.getAttribute("data-redirect-id");
      if (!id) {
        return;
      }
      const component = document.createElement("div");
      component.className = "div-modal-edit-record";
      component.innerHTML = `
    <modal-edit-record
        id="preview-modal-edit-record"
        title="Edit Record"
        host=""
        recId="${id}"
        recValue=""
        showRedirects="true"
        status=""
        cancelBtn="Close"
        okBtn="Save">
    </modal-edit-record>
  `
      component.addEventListener('click', ev => {
        alert("component.addEventListener")
        let theModal = component.querySelector("#preview-modal-edit-record");
        let theStatus = theModal.getAttribute("status");
        if (theStatus === "updated") {
        }
        component.querySelector("#preview-modal-edit-record").setAttribute("visible", "false");
        doc.querySelector("#div-modal-edit").innerHTML = "";
      });
      component.querySelector("#preview-modal-edit-record").setAttribute("visible", "true");
      this.editor.appendChild(component);
    }
  }

  // ---- MESSAGE Handlers
  requestStatusMessage(type, message) {
    this.divRequestStatus.className = "";
    this.divRequestStatus.classList.add(type);
    this.divRequestStatus.innerHTML = utils.getMessageSpan(type, message);
  }
  // ---- REST API Handlers

  async requestTrace() {
    const url = this.host
        + `api/db/trace?id=${this.requestedId}`
    ;
    let fetchData = {
      method: 'GET'
    }

    const response = await fetch(url, fetchData);
    if (!response.ok) {
      let message = "";
      switch (response.status) {
        case 404: {
          message += "not found";
          break;
        }
        case 500: {
          message += "doxa server error";
          break;
        }
        default: {
          message += `An error has occurred: ${response.status}`;
        }
      }
      throw new Error(message);
    }
    const responseJson = await response.json();
    return responseJson;
  }
// ---- End REST API Handlers

  static get observedAttributes() {
    return ['host'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
  }

  adoptedCallback() {
  }
  get host() {
    return this.getAttribute("host");
  }

  set host(value) {
    this.setAttribute("host", value);
  }
}

window.customElements.define('id-trace', IdTrace);