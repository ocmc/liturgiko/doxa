// CheckboxGroup allows the user make a multiple selections using check boxes
class CheckboxGroup extends HTMLElement {
    /**
     * TAG ATTRIBUTES:
     *   gid: a unique id for the group so if the tag is used multiple times on a page, they will be treated as separate groups
     *   items: a json stringified object array as described below.
     * USAGE: see how used in generation-manager.
     * 1. In constructor, set the tag items attribute to the result of calling a function
     *    e.g.,
     *      getPreGenerateOptions() {
     *     return [
     *       {Key: this.keyCopyAssets, Desc: "Copy assets folder. (Note: app.js, css.js are always copied.)", Selected: this.copyAssets, Icon: ""},
     *       {Key: this.keyDeleteHtml, Desc: "Delete existing HTML files", Selected: this.deleteHtml, Icon: ""},
     *       {Key: this.keyDeletePdf, Desc: "Delete existing PDF files", Selected: this.deletePdf, Icon: ""},
     *     ];
     *   }
     *       The option object attributes are:
     *       Key - the unique ID that will be returned when the item is selected
     *       Desc - what the user sees
     *       Selected - true or false. This is the boolean data type, not a string, e.g. not "true", but true.
     *       Class - (optional) the Bootstrap Alert class. Used to set the font color and background color of the item
     *       Icon - (optional) the Bootstrap icon to prefix to the Desc value
     *       IconColor - (optional) the color of the icon
     * 2. Add <checkbox-group it="someIdYouCreateSoItIsUnique" guid="someGroupIdYouCreateSoItIsUnique" items="[]" active="0" checked="[]" ></checkbox-group>
     *       items - use JSON.stringify() to turn the array of option objects into string to pass as tag
     *       gid   - a group ID that is unique
     *       active - the index number of the selected item
     *       checked - each time a box is changed, this will hold the index values of all checked items
     *                 Do not set it yourself.
     * 3. Add an event handler to get notified when someone clicks an option:
     *      this.querySelector("#someIdYouCreate").addEventListener("change", (ev) => this.yourHandlerFunction(ev));     * To Get Selected Item, in the using javascript:
     * 4. Create the handler function:
     *   myHandlerFunction(ev) {
     *     // to get value selected
     *     let selectedItem = ev.target.getAttribute("value");
     *    // to get value selected
     *    let selectedItemIndex = ev.target.getAttribute("index");
     *    // to get all selected items:
     *    const checked = ev.target.getAttribute("checked");
     *   }
     */
    constructor() {
        super();
        console.log("static/wc/checkbox-group.js")
        this.listArray = null;
        this.listComponent = null;
    }

    connectedCallback() {
        this.innerHTML =
            `
<style> 
  @import url('static/css/bootstrap.min.css');
  @import url('static/img/bootstrap-icons.css');
</style>
<style>
</style>
<div id="listComponent_${this.gid}"></div>
`;
        this.listComponent = this.querySelector(`#listComponent_${this.gid}`);
        this.listComponent.addEventListener("change", (e) => this.fireChangeEvent(e));
        if (this.items && this.items.length > 0) {
            this.listArray = JSON.parse(this.items);
        }
        if (this.listArray && this.listArray.length > 0) {
            this.renderList()
        }
    }
    // for every event handler added above, below remove it
    disconnectedCallback() {
        this.listComponent.removeEventListener("change", (e) => this.fireChangeEvent(e));
    }

    fireChangeEvent(ev) {
        const children = this.listComponent.querySelectorAll('input');
        let checked = [];
        children.forEach((element, index) => {
            if (element.checked) {
                checked.push(index);
            }
        });
        this.checked = checked;
    }
    /**
     <div class="form-check">
     <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
     <label class="form-check-label" for="flexCheckDefault">
     Default checkbox
     </label>
     </div>
     *
     */
    renderItem(item, index) {
        // containing div
        const theDiv = document.createElement('div');
        theDiv.classList.add("form-check");
        if (item.Class && item.Class.length > 0) {
            theDiv.classList.add(item.Class);
        }
        // input
        const theInput = document.createElement("input");
        theInput.classList.add("form-check-input");
        theInput.setAttribute("type", "checkbox")
        theInput.setAttribute("name", `flexCheckDefault_${this.gid}`)
        theInput.setAttribute("id", `flexCheckDefault_${this.gid}_${index}`)
        theInput.setAttribute("value", `${item.Key}`)
        theInput.setAttribute("index", `${index}`)
        theInput.checked = item.Selected;
        // label
        const theLabel = document.createElement('label');
        theLabel.classList.add("form-check-label");
        theLabel.setAttribute("For", `flexCheckDefault_${this.gid}_${index}`)
        if (item.Icon && item.Icon.length > 0) {
            theLabel.innerHTML = `<span><i class="bi bi-${item.Icon}"></i>&nbsp;${item.Desc}</span>`
        } else {
            theLabel.innerText = item.Desc
        }
        // append components
        theDiv.appendChild(theInput);
        theDiv.appendChild(theLabel);
        this.listComponent.appendChild(theDiv);
    }

    renderList() {
        if (! this.listComponent) {
            return;
        }
        this.listComponent.innerHTML = "";
        if (this.listComponent) {
            this.listArray.forEach((item, index) => {
                this.renderItem(item, index);
            });
        }
    }

    static get observedAttributes() {
        return ['gid', 'items'];
    }

    //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
        switch (name) {
            case "items": {
                if (newVal !== null && oldVal !== newVal) {
                    this.listArray = JSON.parse(newVal);
                    this.renderList();
                }
                break;
            }
        }
    }

    // custom events
    selectionChanged = new CustomEvent('selectionchanged', {
        detail: {
            name: 'selectionchanged'
        },
        bubbles: true
    });

    adoptedCallback() {
    }

    get gid() { // group id
        return this.getAttribute("gid");
    }
    set gid(value) {
        this.setAttribute("gid", value);
    }

    get items() {
        return this.getAttribute("items");
    }
    set items(value) {
        this.setAttribute("items", value);
    }
    get checked() {
        return this.getAttribute("checked");
    }
    set checked(value) {
        this.setAttribute("checked", value);
    }
}

window.customElements.define('checkbox-group', CheckboxGroup);