import * as utils from "./utils.js";

class DbImporter extends HTMLElement {
  constructor() {
    super();
    console.log("static/wc/db-importer.js")
    this.lsPrefix = "di"; // used to prefix local storage keys
    this.selectedFile = "0";
    this.fileList = [];
    this.errorList = null;
    // remove quotes option
    // The option is set by function getRecordTextProcessingOptions()
    this.keyRemoveQuotes = "removeQuotes"; // provides the nav pages showing available books and services
    this.removeQuotes = false;
    this.recordTextProcessingCheckboxGroup = null;
    // all or none option
    // The option is set by function getRecordErrorProcessingOptions()
    this.keyAllOrNone = "allOrNone"; // provides the nav pages showing available books and services
    this.allOrNone = false;
    this.recordErrorProcessingCheckboxGroup = null;

  }

  connectedCallback() {
    this.innerHTML =
`
<style> 
  @import url('static/css/bootstrap.min.css');
  @import url('static/img/bootstrap-icons.css');
</style>
<br>
<div id="my-element-div" class="wc-title">Database Import Manager</div>
<br>
<hr>
<div class="accordion" id="accordionExample">
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingOne">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
        <span style="color: cornflowerblue">Requirements for an Import File</span>&nbsp;(click to view or hide)
      </button>
    </h2>
    <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
      <div class="accordion-body">
<div style="display: flex; flex-direction: column; flex-wrap: wrap; gap: 1rem;">
    <div>
        An import file must meet the following conditions:
    </div>
    <ul>
       <li>The file must be in the imports directory of your project.</li>
       <li>The file extension must be <em>.tsv</em></li>
       <li>Each record must be its own line in the file.</li>
       <li>Each line of the file must use a tab to separate the parts.</li>
       <li>There are two ways to create an import file. You can use a text editor and separate each part in a line by pressing the tab button.  Or, you can enter the parts as columns in a spreadsheet and export the spreadsheet as a tab separated value file (tsv). The spreadsheet will insert the tabs for you when you export it. Note that Excel exports a tab delimited file with an .txt extension.  You need to rename it to .tsv.</li>
       <li>Each line must have exactly two parts separated by a tab: the first must be the <em>ID</em> and the second the <em>value</em>.</li>
       <li>The first line in the file can optionally be a header line: <em>ID\\tValue</em>.  The header line will not be loaded into the database. The first heading must be <em>ID</em> and the second the word <em>Value</em>.</li>
       <li>An ID has this format: <em>root dir/child dir/child dir{1:n}:topic</em></li>
       <li>All IDs have at least three directories (the root and at least 2 children).</li>
       <li>The directory part of an ID uses the forward slash character <em>/</em> as its delimiter.</li>
       <li>The last part of the ID is the key, and uses the colon character <em>:</em> as its separator.</li>
       <li>If the first directory of the ID is <em>btx</em> (biblical text):</li>
         <ul>
           <li>The 2nd directory must be a library, e.g. en_us_kjv.</li>
           <li>The 3rd directory must be the abbreviation of a Bible book, e.g. act (for acts)</li>
           <li>The 4th directory must be a chapter number, e.g. 23.  Doxa will prefix it with the letter c and zero pad the number and zero pad it to be three digits, e.g. c023</li>
           <li>The key must be a number, e.g. 3.  Doxa will zero pad it to three digits, e.g. 003.</li>  
           <li>Example, below:</li>
           <li>btx/en_uk_webbe/act/1:1\\tThe first book I wrote, Theophilus, concerned all that Jesus began both to do and to teach,</li>
           <li>Note: lectionary text is kept in the ltx directory, not the btx.  The btx is for researching the Biblical text in Greek and translations.</li>
           <li>Note: to see what Bible book abbreviations are valid, look at btx/en_uk_webbe.</li>
         </ul> 
        <li>If the first directory of the ID is <em>ltx</em> (liturgical text):</li>
          <ul>
            <li>The 2nd directory must be a library, e.g. gr_gr_cog.</li>
            <li>The 3rd directory must be the topic, e.g. actors.</li>
            <li>The next part must be the key, e.g. ac.Priest</li>
            <li>Then the value, e.g. ΛΑΟΣ</li>
            <li>Example line, below:</li>
            <li>ltx/gr_gr_cog/actors:ac.Priest\\tΛΑΟΣ</li>
          </ul>
       <li>If the first directory of the ID is <em>media</em>:</li>
         <ul>
           <li>The 2nd directory must be a library, e.g., gr_gr_cog.</li>
           <li>The 3rd directory must be an <em>ltx</em> topic, e.g., eo.e02</li>
           <li>The 4th directory must be a key from within that topic, e.g., eo.Doxasticon </li>
           <li>The 5th directory must be the composition ID (the letter c plus a number), e.g. c1011</li>
           <li>The 6th directory must be either the word <em>audio</em> or <em>core</em>s, e.g. score</li>
           <li>The 7th directory must be the letter <em>b</em> (for byzantine notation) or <em>w</em> (for western), e.g. w</li>
           <li>The 8th directory must be a sequence number, e.g. 1</li>
           <li>Then a colon</li>
           <li>Then either <em>link.info</em> or <em>link.path</em>, e.g. link.info</li>
           <li>Then a tab, e.g. \\t</li>
           <li>Then the value, e.g., Roubanis</li>
           <li>Example line, below:</li>
           <li>media/gr_gr_cog/eo.e02/eo.Doxasticon/c1011/score/w/1:link.info\\tRoubanis</li>
           <li>media/gr_gr_cog/eo.e02/eo.Doxasticon/c1011/score/w/1:link.path\\t/m/roubanis/gr/eo/e02/w/eodoxasticon.pdf</li>
           <li>Note: for each topic:key for which there is media, there must be both a link.info and a link.path.</li>
         </ul>   
      <li>If the first directory of the ID is <em>configs</em>, do not change the IDs, only the values.  The config IDs are defined in the Doxa program code and can only be changed by a programmer.  The property must be known to Doxa, or it will not be used to update the database.</li> 
      <li>You may not import a line that starts with <em>keyring</em>.  The keyring is generated every time Doxa starts.</li>
      <li>If a value is a redirect, it must start with the asperand character @ and the ID to redirect to, with no space between the asperand and the ID, e.g. @ltx/en_us_dedes/actors:Priest</li>
    </ul>
</div>
      </div>
    </div>
  </div>
</div>
<br>
<hr>
<br>
<div style="display: flex; flex-direction: row; flex-wrap: wrap; gap: 1rem;">
  <div><em>Select the file to import:</em></div>
  <radio-group id="fileRadioGroup" gid="siteType" items="[]" active="0"></radio-group>
</div>
<br>
<hr>
<br>
<div style="display: flex; flex-direction: row; flex-wrap: wrap; gap: 1rem;">
  <div><em>How You Created or Edited the Import File:</em>&nbsp;&nbsp;</div>
  <checkbox-group id="recordValueProcessingCheckboxGroup" gui="removeQuotes" items="[]" checked="[]"></checkbox-group>
  <br>
  <div>When you export a tab separated value file from a spreadsheet (e.g. Excel, Numbers), if text has quotes around it, the spreadsheet adds two more sets of enclosing quotes.  If the text is not quoted, but has a comma in it, it will be exported with a single set of quotation marks. In order to correctly remove quotation marks added by a spreadsheet, Doxa needs to know how you created or edited the import file.</div>
</div>
<hr>
<br>
<div style="display: flex; flex-direction: row; flex-wrap: wrap; gap: 1rem;">
  <div><em>What to do if a line has an error:</em>&nbsp;&nbsp;</div>
  <checkbox-group id="recordErrorProcessingCheckboxGroup" gui="removeQuotes" items="[]" checked="[]"></checkbox-group>
  <br>
  <div>If you leave the box above unchecked, lines in the import file that do not have an error will be written to the database.  If you check the box, nothing will be written unless all lines in the import file are free of errors. Either way, you will see a report of any lines that have an error.</div>
</div>
<hr>
<br>
<div style="display: flex; flex-direction: column; flex-wrap: wrap; gap: 1rem; max-width: 50vw;">
  <div style="display: flex; flex-direction: row; gap: 20px;">  <!-- Import Button -->
      <span>
          <button id="btnRequest"  type="button" class="btn btn-primary" style="">Import</button>
      </span>
      <br>
      <div class="alert alert-light message" role="alert" id="requestStatus" style="padding: 3px; width: max-content;">&nbsp;</div>
  </div> <!-- end Import Button -->
<br>
<! -- Error list-->
<div style="display: flex; flex-direction: row; flex-wrap: wrap; gap: 1rem;">
  <ul id="errorList"></ul>
</div>
`;

    // record processing group
    this.recordTextProcessingCheckboxGroup = this.querySelector("#recordValueProcessingCheckboxGroup");
    if (this.recordTextProcessingCheckboxGroup) {
      this.getRecordValueProcessingValues();
      this.recordTextProcessingCheckboxGroup.setAttribute("items", JSON.stringify(this.getRecordValueProcessingOptions()));
      this.recordTextProcessingCheckboxGroup.addEventListener("change", (ev) => this.handleRecordValueProcessingChange(ev));
    }
    // recordErrorProcessingCheckboxGroup
    this.recordErrorProcessingCheckboxGroup = this.querySelector("#recordErrorProcessingCheckboxGroup");
    if (this.recordErrorProcessingCheckboxGroup) {
      this.getRecordErrorProcessingValues();
      this.recordErrorProcessingCheckboxGroup.setAttribute("items", JSON.stringify(this.getRecordErrorProcessingOptions()));
      this.recordErrorProcessingCheckboxGroup.addEventListener("change", (ev) => this.handleRecordErrorProcessingChange(ev));
    }

    this.divRequestStatus = this.querySelector("#requestStatus");
    this.requestButton = this.querySelector("#btnRequest");
    this.errorList = this.querySelector("#errorList");

    // add listeners.  For each one added, remove it in disconnectedCallback.
    this.requestButton.addEventListener("click", (e) => this.handleImport(e))

    // site type group
    this.fileRadioGroup = this.querySelector("#fileRadioGroup");
    if (this.fileRadioGroup) {
      this.fileRadioGroup.setAttribute("items", JSON.stringify(this.fileList));
      this.fileRadioGroup.addEventListener("change", (ev) => this.handleFileSelection(ev));
    }
    this.requestFileList();
  }

  disconnectedCallback() {
    this.requestButton.removeEventListener("click", (ev) => this.handleImport(ev))
    if (this.fileRadioGroup) {
      this.fileRadioGroup.removeEventListener("change", (ev) => this.handleFileSelection(ev));
    }
  }

  handleFileSelection(ev) {
    this.selectedFile = ev.target.getAttribute("value");
  }
  handleImport(ev) {
    if (ev) {
      ev.stopPropagation();
      ev.preventDefault();
    }
    this.errorList.innerHTML = "";
    this.requestButton.disabled = true;
    this.requestStatusMessage(utils.alerts.Primary, "Importing...")
    this.requestImport().then(response => {
      this.requestButton.disabled = false;
      if (response.Status === "OK") {
        this.requestStatusMessage(utils.alerts.Success, "Import finished OK.")
      } else if (response.Status === "ERRORS") {
        if (response.Errors) {
          this.requestStatusMessage(utils.alerts.Warning, response.Message)
          response.Errors.forEach((item, index) => {
            const theItem = document.createElement('li');
            theItem.innerText = item;
            this.errorList?.appendChild(theItem);
          });
        } else {
          this.requestStatusMessage(utils.alerts.Warning, "Import finished, but one or more lines had errors. No further information is available.")
        }
      } else {
        this.requestStatusMessage(utils.alerts.Danger, `${response.Status}: ${response.Message}`)
      }
    }).catch(error => {
      this.requestButton.disabled = false;
      this.requestStatusMessage(utils.alerts.Danger, error)
    });
  }

  getRecordValueProcessingOptions() {
    return [
      {Key: this.keyRemoveQuotes, Desc: "I used a spreadsheet. ", Selected: this.removeQuotes,  Class: "", Icon: "", IconColor: ""},
    ];
  }
  getRecordValueProcessingValues() {
    this.removeQuotes = utils.lsGetBool(this.lsPrefix, this.keyRemoveQuotes);
    this.indexSiteForSearch = utils.lsGetBool(this.lsPrefix, this.keyIndexSiteForSearch);
  }
  handleRecordValueProcessingChange(ev) {
      this.removeQuotes = !this.removeQuotes;
      utils.lsSetBool(this.lsPrefix, this.keyRemoveQuotes, this.removeQuotes);
  }

  getRecordErrorProcessingOptions() {
    return [
      {Key: this.keyAllOrNone, Desc: "Do not write any records to the database.", Selected: this.allOrNone,  Class: "", Icon: "", IconColor: ""},
    ];
  }
  getRecordErrorProcessingValues() {
    this.allOrNone = utils.lsGetBool(this.lsPrefix, this.keyRemoveQuotes);
  }
  handleRecordErrorProcessingChange(ev) {
    if (ev) {
      this.allOrNone = !this.allOrNone;
      utils.lsSetBool(this.lsPrefix, this.keyAllOrNone, this.allOrNone);
    }
  }

  // ---- MESSAGE Handlers
  requestStatusMessage(type, message) {
    this.divRequestStatus.className = "";
    this.divRequestStatus.classList.add(type);
    this.divRequestStatus.innerHTML = utils.getMessageSpan(type, message);
  }
  // ---- REST API Handlers

  async requestImport() {
    const url = this.host
        + `api/db/import?file=${this.fileList[this.selectedFile]}`
        + `&removeQuotes=${this.removeQuotes}`
        + `&allOrNone=${this.allOrNone}`
    ;
    let fetchData = {
      method: 'PUT'
    }

    const response = await fetch(url, fetchData);
    if (!response.ok) {
      let message = "";
      switch (response.status) {
        case 404: {
          message += "not found";
          break;
        }
        case 500: {
          message += "doxa server error";
          break;
        }
        default: {
          message += `An error has occurred: ${response.status}`;
        }
      }
      throw new Error(message);
    }
    const responseJson = await response.json();
    return responseJson;
  }

  requestFileList() {
    const url = this.host
        + `api/db/import/list`
    ;

    utils.callApi(url, "GET").then(response => {
      if (response.Status === "OK") {
        if (response.Files) { // initialize the radio group with the file list
          this.fileList = response.Files
          let items = [];
          this.fileList.forEach((item, index) => {
            items.push({Key: index, Desc: item, Selected: false,  Class: "", Icon: "", IconColor: ""},
            )
          });
          this.fileRadioGroup.setAttribute("items", JSON.stringify(items));
        } else { // tell user there are no files in the import dir
          if (response.Dir) {
            this.requestStatusMessage(utils.alerts.Warning, `no files found in ${response.Dir}`)
          } else {
            this.requestStatusMessage(utils.alerts.Warning, `no files found in the import directory for your project`)
          }
        }
      } else {
        this.requestStatusMessage(utils.alerts.Danger, response.Status)
      }
    }).catch(error => {
      this.requestStatusMessage(utils.alerts.Danger, error)
    });
  }
// ---- End REST API Handlers

  static get observedAttributes() {
    return ['host'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
  }

  adoptedCallback() {
  }
  get host() {
    return this.getAttribute("host");
  }

  set host(value) {
    this.setAttribute("host", value);
  }
}

window.customElements.define('db-importer', DbImporter);