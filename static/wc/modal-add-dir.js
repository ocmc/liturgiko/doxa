class ModalAddDir extends HTMLElement {
  constructor() {
    // always call super() first
    super();
    console.log("static/wc/modal-add-dir.js")
  }

  connectedCallback() {
    // Create component
    const component = document.createElement("div");
    component.className = "bootstrap-shadow-dialog";
    // Create shadowRoot
    const shadowRoot = this.attachShadow({mode: 'open'});
    // Set inner HTML
    shadowRoot.innerHTML = `
      <style> 
		    @import url('static/css/bootstrap.min.css');
      </style>
      <style>
      .control-label {
        color: #5bc0de;
      }
       .modal {
          display:block;
       }
       .modal-body {
           padding: 40px !important;
       }
       .modal-header{
          text-transform: uppercase;
          color:#fff;
          background-color: #428bca;
        }
      </style>
      
      <div class="modal"  id="modal-dialog" data-bs-backdrop="static" data-bs-keyboard="False">
       <div class="modal-dialog shadow">
         <div class="modal-content">
           <div class="modal-header">
              <h4 class="modal-title">${this.title}</h4>
              <button type="button" class="close" id="closeBtn" data-dismiss="modal" aria-hidden="true">×</button>
           </div>
           <div class="modal-body">
                <div class="row">
                    <div class="control-label">Directory</div>
                    <input id="newDir" type="text" class="form-control">   
                </div>
                <div class="row">
                    <div class="alert alert-light control-label" role="alert">
                        ID: ${this.dirPath}/<span id="dirAlert">${this.dir}</span>
                    </div>
                </div>
           </div>
           <div class="modal-footer">
              <button type="button" class="btn btn-outline-primary" data-dismiss="modal"  id="cancelBtn">${this.cancelBtn}</button>
              <button type="button" class="btn btn-primary" id="okBtn" disabled>${this.okBtn}</button>
           </div>
        </div><!-- /.modal-content -->
       </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
     </div>
  `;
    shadowRoot.getElementById("modal-dialog").addEventListener("click", e => {
      const targetId = e.target.id;
      if (targetId === "closeBtn" || targetId === "cancelBtn" || targetId === "okBtn") {
        // allow propagation
      } else {
        e.stopPropagation();
      }
    })
    // keyup newDir
    shadowRoot.getElementById("newDir").addEventListener('keyup', ev => {
      ev.stopPropagation();
      const val = shadowRoot.getElementById("newDir").value;
      let noPunctuation = val.replace(/[,\/#!$%\^&\*;:{}=\-`~()]/g,"");
      const finalString = noPunctuation.replaceAll(" ","");
      this.setAttribute("dir", finalString);
      shadowRoot.getElementById("dirAlert").innerText = finalString;
      this.toggleAddButton();
    })
    // set focus to first input
    shadowRoot.getElementById("newDir").focus();

    // Save copy of shadow root to component
    component._shadowRoot = shadowRoot;
    // Attach component to dom
    document.getElementsByTagName("body")[0].appendChild(component);
  }

  disconnectedCallback() {
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
  }

  adoptedCallback() {
  }

  toggleAddButton() {
    const btn = this.shadowRoot.getElementById("okBtn")
    if (this.shadowRoot.getElementById("newDir").value.length > 0) {
      btn.disabled = false;
    } else {
      btn.disabled = true;
    }
  }

  get cancelBtn() {
    return this.getAttribute("cancelBtn");
  }
  set cancelBtn(value) {
    this.setAttribute("cancelBtn", value);
  }
  get dirPath() {
    return this.getAttribute("dirPath");
  }
  set dirPath(value) {
    this.setAttribute("dirPath", value);
  }
  get dir() {
    return this.getAttribute("dir");
  }
  set dir(value) {
    this.setAttribute("dir", value);
  }

  get okBtn() {
    return this.getAttribute("okBtn");
  }
  set okBtn(value) {
    this.setAttribute("okBtn", value);
  }
  get title() {
    return this.getAttribute("title");
  }
  set title(value) {
    this.setAttribute("title", value);
  }

}

window.customElements.define('modal-add-dir', ModalAddDir);