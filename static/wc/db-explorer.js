import {KeyPath} from "./kvs.js";
import * as utils from "./utils.js";
import "./db-explorer-breadcrumb.js";

class SearchResponse {
}
SearchResponse.prototype.Path = [];
SearchResponse.prototype.Message = "";
SearchResponse.prototype.Status = "";
SearchResponse.prototype.PatternOriginal = "";
SearchResponse.prototype.PatternExpanded = "";
SearchResponse.prototype.Count =  0;
SearchResponse.prototype.Time = "";
SearchResponse.prototype.CaseInsensitive = false;
SearchResponse.prototype.DiacriticInsensitive = false;
SearchResponse.prototype.WholeWord = false;
SearchResponse.prototype.Values = [];

class SearchResponseMatch {}
SearchResponseMatch.prototype.ID = "";
SearchResponseMatch.prototype.Last = "";
SearchResponseMatch.prototype.Value = "";


class DbExplorer extends HTMLElement {
    constructor() {
        super();
        console.log("static/wc/db-explorer.js")
        // local storage
        this.lsPrefix = "dbX";
        this.lsCaseInsensitive = "caseInsensitive";
        this.lsDiacriticInsenstive = "diacriticInsensitive";
        this.lsGreek = "greek";
        this.lsWholeWord = "wholeWord";

        // kvs provides functions for processing Doxa database record IDs.
        this.kvs = new KeyPath("");

        // There are three views of the data:
        // 1. this.theData, which always has the full set of data.
        // 2. this.dataIndexArray, containing the indexes in this.theData that match a filter
        // 3. the table displayed to the user, and is a paged subset of this.dataIndexArray.
        // this.theData holds the full set of data returned by an api call
        this.theData = {};
        // this.dataIndexArray is an array of integers (which are indexes pointing to values in this.theData).
        // It is initialized to point to all the values in this.theData (see func setDataIndexForAllRows)
        // It is subsequently reset each time a filter is applied (see func filterData).
        // When a filter is cleared, it is reset back to all the values in this.theData.
        this.dataIndexArray = [];
        this.dataLength = 0;
        // for paging the data in the table
        this.curPage = 1;
        this.pageSize = 30;

        // div handles
        this.divSearchRow = null;
        this.divReplaceRow = null;
        this.divRxRow = null;
        this.divRx = null;
        this.divRxFlags = null;

        // anchor handles
        this.menuExportAnchor = null;
        this.menuReplaceAnchor = null;

        // input handles
        this.inputSearchValue = null;
        this.inputReplaceId = null;
        this.inputReplaceValue = null;
        this.inputFilterId = null;
        this.inputFilterValue = null;
        this.inputToggleActionsIcon = null;
        this.inputToggleReplaceIcon = null;
        this.inputToggleRxIcon = null;
        this.searchButton = null; // Search button

        // current values read in from the inputs
        this.currentIdFilter = "";
        this.currentValueFilter = "";
        this.currentPath = "";
        this.currentSearch = "";
        this.responseSearchPattern = ""; // as received back from server
        this.currentIdReplace = "";
        this.currentValueReplace = "";

        this.actionsHidden = false;
        this.replaceEnabled = false;
        this.rxEnabled = false;

        // Value search options
        this.caseInsensitive = false;
        this.diacriticInsensitive = false;
        this.greek = false;
        this.wholeWord = false;

        // if a search is diacritic insensitive
        // either globally or for a specific
        // character, we need to use Unicode
        // Normal Form D (decomposed) when
        // we highlight the matches.
        this.useNfd = false;

        this.host = "";
        this.scrollY = "";
        this.useId = false;
        this.time = "";
    }

    connectedCallback() {

        this.innerHTML = `
    <style> 
      @import url('static/css/bootstrap.min.css');
      @import url('static/img/bootstrap-icons.css');
    </style>

<style>
body {background-color: #fff;}
.sticky-top { 
    background-color: #fff;
}
#doxa-db-explorer-container {
    overflow-y:scroll !important;
}
.db-explorer-copy-id, .db-explorer-copy-rid, .db-explorer-copy-sid {
    color: cornflowerblue;
    font-style: italic;
}
/* Loading state styles */
#db-explorer-breadcrumb-container.loading {
    position: relative;
}
#db-explorer-breadcrumb-container.loading::before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(255,255,255,0.4);
    z-index: 5;
}
table {
    padding-top: 20vh;
}
.bi-toggle-on, .bi-toggle-off {
    cursor: pointer;
}
.filterColor {
  color: blue;
}
.searchColor {
  color: red;
}
.alert {
    line-height: 1px;
}
.navbarNavDropdown {
}
#db-explorer-filter-id {
    min-width: 0;
}
th .input-group {
    min-width: 0;
}
th .input-group-text {
    padding: 0.25rem 0.5rem;
}
th .form-control-sm {
    font-size: 0.75rem;
    padding: 0.25rem 0.5rem;
}
</style>
<div id="doxa-db-explorer-container" class="container-fluid doxa-db-explorer">
    <div id="db-explorer-sticky-header" class="sticky-top" style="opacity: 1;">
<!-- db menu-->
        <div id="db-explorer-menu-row" class="row p-1">
            <nav class="navbar navbar-expand-sm">
                <div id="db-explorer-navbar-container" class="alert-info container-fluid">
                    <a class="wc-title" href="#">Database Explorer:</a>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                                   data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="bi-plus-lg" style="color: cornflowerblue;"></i>
                                    Add
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <li><a class="dropdown-item"  id="doxa-db-explorer-link-add-dir" href="#">Directory</a></li>
                                    <li><a class="dropdown-item" id="doxa-db-explorer-link-add-record" href="#">Record</a></li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link d-none" href="#" id="doxa-db-explorer-menu-export-id">
                                    <i class="bi-upload" style="color: cornflowerblue;"></i>
                                    Export
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" id="doxa-db-explorer-link-show-icon-help">
                                    <i class="bi-question-circle" style="color: cornflowerblue;"></i>
                                    Help
                                </a>
                            </li>
<!--                            replace toggle -->
                            <li class="nav-item">
                                <a class="nav-link d-none" href="#" id="doxa-db-explorer-link-toggle-replace">
                                    <i class="bi-toggle-on" id="doxa-db-explorer-link-toggle-replace-icon" style="color: cornflowerblue;"></i>
                                    Replace
                                </a>
                            </li>
<!--                            expanded RegEx pattern toggle -->
                            <li class="nav-item">
                                <a class="nav-link d-none" href="#" id="doxa-db-explorer-link-toggle-rx">
                                    <i class="bi-toggle-on" id="doxa-db-explorer-link-toggle-rx-icon" style="color: cornflowerblue;"></i>
                                    Rx
                                </a>
                            </li>
<!--                            actions toggle -->
                            <li class="nav-item">
                                <a class="nav-link" href="#" id="doxa-db-explorer-link-toggle-actions">
                                    <i class="bi-toggle-on" id="doxa-db-explorer-link-toggle-actions-icon" style="color: cornflowerblue;"></i>
                                    Actions
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
<!-- breadcrumb-->
        <div id="db-explorer-breadcrumb-row" class="row p-0">
            <div id="db-explorer-breadcrumb-container" class="col">
                <db-explorer-breadcrumb id="db-explorer-breadcrumb"></db-explorer-breadcrumb>
            </div>
        </div>
<div class="col-8 mt-1">
<!-- search by value -->
    <div class="input-group mb-2">
        <regex-input id="db-explorer-search-input" class="" items="[]" active="0" title="" placeholder="search for value (regular expression)" gid="DbX" formEnabled="true" helpEnabled="false"  ci="false" di="false" gr="false" ww="false" aria-labelledby="idHelpBlockRegEx"></regex-input>
    </div>
</div>        
<!-- Rx pattern as returned by Doxa -->
        <div id="db-explorer-rx-row" class="row p-1 d-none">
          <div id="db-explorer-rx-pattern-row" class="row p-1">
            <div id="db-explorer-rx-pattern-column" class="col">
                <div id="db-explorer-rx" style="align-content: center;">
                </div>
            </div>
          </div>
          <div id="db-explorer-rx-flags-row" class="row p-1">
            <div id="db-explorer-rx-flags-column" class="col">
                <div id="db-explorer-flags" style="align-content: center;">
                </div>
            </div>
          </div>
        </div>
<!-- replace -->
        <div id="db-explorer-replace-row" class="row p-1 d-none">
            <div id="db-explorer-replace-id-column" class="col">
                <div id="db-explorer-replace-id-group" class="input-group mb-3">
                    <span class="input-group-text" id="group-replace-id"><i class="bi-filter"
                                                                        style="color: cornflowerblue;"></i></span>
                    <input type="text" id="db-explorer-replace-id-input" class="form-control" placeholder="replace with ID"
                           aria-label="replace values" aria-describedby="group-replace-id">
                </div>
            </div>
            <div id="db-explorer-replace-value-column" class="col">
                <div id="db-explorer-replace-value-group" class="input-group mb-3">
                    <span class="input-group-text" id="group-replace-value"><i class="bi-filter"
                                                                        style="color: cornflowerblue;"></i></span>
                    <input type="text" id="db-explorer-replace-value-input" class="form-control" placeholder="replace with value"
                           aria-label="replace value" aria-describedby="group-replace-value">
                </div>
            </div>
            <div id="db-explorer-replace-empty-column" class="col">
            </div>
        </div>
<!-- status -->
        <div id="db-explorer-status-row" class="row p-0 mt-1">
            <div id="db-explorer-alert" class="alert alert-primary py-1" role="alert">
                <span id="db-explorer-status-label">Status:</span>
            </div>
        </div>
<!-- Filter instructions -->
        <div id="db-explorer-filter-instructions-row" class="row py-0 px-1">
            <span id="db-explorer-filter-instructions-text">&nbsp;Filter your search results by typing in the filter boxes in the table header.</span>
        </div>
    </div>
<!-- Table -->
    <div id="db-explorer-table-row" class="row p-1">
        <div id="db-explorer-table-column" class="col">
            <div id="db-explorer-table-responsive-container" class="table-responsive">
                <table id="db-explorer-data-table" class="table table-striped">
                    <thead>
                    <tr>
                        <!-- <th scope="col" style="width: 1% !important;"><input class="form-check-input" type="checkbox" value="" id="doxa-db-explorer-table-column-checkbox"></th>-->
                        <th scope="col" style="width: 1% !important; min-width: 250px;">
                            ID
                            <div id="db-explorer-filter-id-container" class="input-group mt-2">
                                <span class="input-group-text" style="white-space: nowrap;"><i class="bi-filter" style="color: cornflowerblue;"></i></span>
                                <input type="text" id="db-explorer-filter-id" class="form-control form-control-sm" placeholder="filter ID" aria-label="filter table" aria-describedby="basic-addon1">
                            </div>
                        </th>
                        <th scope="col" style="width: 98% !important;">
                            Value
                            <div id="db-explorer-filter-value-container" class="input-group mt-2">
                                <span class="input-group-text" style="white-space: nowrap;"><i class="bi-filter" style="color: cornflowerblue;"></i></span>
                                <input type="text" id="db-explorer-filter-value" class="form-control form-control-sm" placeholder="filter value" aria-label="filter table" aria-describedby="basic-addon1">
                            </div>
                        </th>
                        <th class="action-columns-on" id="actions-toggle-on" scope="col" style="text-align:center; width: 1% !important;" colspan="7">Actions</th>
                        <th class="action-columns-off d-none"  id="actions-toggle-off" scope="col" style="text-align:center; width: 1% !important;" ></th>
                    </tr>
                    </thead>
                    <tbody id="db-explorer-tbody" translate="no">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<!-- Modals -->
     <div id="db-explorer-modal-div">
       <div id="db-explorer-the-modal" class="modal" tabindex="-1">
     </div>
<!-- Pagination -->
     <div id="db-explorer-pagination-row" class="row p-1">
        <div id="db-explorer-row-count-column" class="col col-4">
            <div id="db-explorer-row-indicator" class="control-label" style="padding-top: 12px;">Showing rows <span id="db-explorer-row-top">0</span> to <span id="db-explorer-row-bottom">0</span> of <span id="db-explorer-row-count">0</span></div>
        </div>
        <div id="db-explorer-page-size-column" class="col col-4"><div id="db-explorer-page-size-container" class="control-label" style="margin-top: 4px; text-align: center;"><span id="db-explorer-page-size-label">Page size: </span><input id="db-explorer-page-size-input" style="width: 10ch; text-align: center;"></div></div>
        <div id="db-explorer-pagination-controls-column" class="col col-4">
            <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-end">
              <li class="page-item">
                <a id="db-explorer-first-button" class="page-link d-none" href="#" aria-label="First">
                  <span aria-hidden="true"><i class="bi-chevron-double-left"></i></span>
                </a>
              </li>
              <li class="page-item">
                <a id="db-explorer-prev-button" class="page-link d-none" href="#" aria-label="Previous">
                  <span aria-hidden="true"><i class="bi-chevron-left"></i></span>
                </a>
              </li>
              <li class="page-item">
                <div id="db-explorer-page-indicator" class="control-label" style="margin-top: 4px;"> 
                  <span id="db-explorer-page-label" style="margin-left: 4px;">Page</span>
                  <input id="db-explorer-current-page" style="margin-left: 2px; width:10ch; text-align: center;">
                  <span id="db-explorer-of-label" style="margin-left: 2px;">of</span>
                  <span id="db-explorer-page-max" style="margin-left: 2px;margin-right: 4px;"></span>
                </div>
              </li>
              <li class="page-item">
                <a id="db-explorer-next-button" class="page-link" href="#" aria-label="Next">
                  <span aria-hidden="true"><i class="bi-chevron-right"></i></span>
                </a>
              </li>
              <li class="page-item">
                <a id="db-explorer-last-button" class="page-link" href="#" aria-label="Last">
                  <span aria-hidden="true"><i class="bi-chevron-double-right"></i></span>
                </a>
              </li>
            </ul>
          </nav>
        </div>
        </div>
     </div>

<!-- Modal Help -->
    <modal-icon-help
        id="db-explorer-modal-icon-help"
        class="d-none"
        firstDivText="The page behind this one is the database explorer page. Use it to search, create, update, or delete Doxa database records. Note that both the database search and table filter accept regular expressions. When searching for IDs, the regular expression wildcard .* is automatically prefixed to the search pattern. If the search pattern does not end in a wildcard (.*), the word boundary (\\b) is automatically added as a suffix.  For example, actors:Priest is changed to .*actors:Priest\\b.  It will find keys that end in exactly the word Priest.  actors:Priest.* will find keys starting with Priest, e.g. actors:Priest, actors:Priests, actors:PriestOrDeacon. For searching values, note that \\b (word boundary) only works with ASCII, not Greek.  So do not use it.  Instead, check the whole word option. If you do not check the Exact option, your pattern will be converted to lower case, and diacritics removed for the search. After entering a filter or search pattern, press the ENTER (return) key. Below are descriptions of what happens when you click each icon."
        lastDivText="The results of a database search are displayed as a table with a preset number of rows.  Use the buttons at the bottom of the table to scroll through the pages of rows.  There is an input box to change the number of rows displayed.  There is another input box to enter a page number and jump directly to it.  After entering a value in the input box, press the ENTER (return) key."
        title="Database Explorer: Icon Help">
    </modal-icon-help>
`
        // set handles for divs
        this.divReplaceRow = this.querySelector("#db-explorer-replace-row");
        this.divRxRow = this.querySelector("#db-explorer-rx-row");
        this.divRx = this.querySelector("#db-explorer-rx");
        this.divRxFlags = this.querySelector("#db-explorer-flags");

        // set handles for inputs
        this.inputReplaceId = this.querySelector('#db-explorer-replace-id-input');
        this.inputReplaceValue = this.querySelector('#db-explorer-replace-value-input');
        this.inputFilterId = this.querySelector('#db-explorer-filter-id');
        this.inputFilterValue = this.querySelector('#db-explorer-filter-value');
        this.inputToggleActionsIcon = this.querySelector('#doxa-db-explorer-link-toggle-actions-icon');
        this.inputToggleReplaceIcon = this.querySelector('#doxa-db-explorer-link-toggle-replace-icon');
        this.inputToggleRxIcon = this.querySelector('#doxa-db-explorer-link-toggle-rx-icon');
        this.inputSearchValue = this.querySelector('#db-explorer-search-input');
        this.inputSearchValue.setAttribute("items", JSON.stringify(utils.comboCategories));

        // anchor handles
        this.menuExportAnchor = this.querySelector("#doxa-db-explorer-menu-export-id");
        this.menuReplaceAnchor = this.querySelector('#doxa-db-explorer-link-toggle-replace');
        this.menuRxAnchor = this.querySelector('#doxa-db-explorer-link-toggle-rx');

        this.querySelector('#db-explorer-modal-icon-help').addEventListener('click', (ev) => this.handleModalIconHelpClick(ev));

        // this.querySelector('#db-explorer-search-option-case-insensitive').addEventListener('change', (event) => this.handleOptionCaseInsensitive());
        // this.querySelector('#db-explorer-search-option-whole-word').addEventListener('change', (event) => this.handleOptionWholeWordChange());
        this.querySelector('#doxa-db-explorer-link-add-dir').addEventListener('click', () => this.handleNewDir());
        this.querySelector('#doxa-db-explorer-link-add-record').addEventListener('click', () => this.handleNewRecord());
        this.querySelector('#doxa-db-explorer-link-show-icon-help').addEventListener('click', (ev) => this.showModalIconHelp(ev));
        this.querySelector('#doxa-db-explorer-menu-export-id').addEventListener('click', () => this.handleRequestForExport());
        this.querySelector("#db-explorer-data-table").addEventListener("click", (ev) => this.handleTableClick(ev));

        // event listeners
        this.inputToggleActionsIcon.addEventListener('click', (ev) => this.toggleActionColumns(ev));
        this.inputToggleReplaceIcon.addEventListener('click', (ev) => this.toggleReplace(ev));
        this.inputToggleRxIcon.addEventListener('click', (ev) => this.toggleRx(ev));
        this.inputFilterId.addEventListener('keyup', (event) => this.filterData(event));
        this.inputFilterValue.addEventListener('keyup', (event) => this.filterData(event));
        
        // Breadcrumb component event listeners
        const breadcrumb = this.querySelector('#db-explorer-breadcrumb');
        breadcrumb.addEventListener('path-change', (event) => this.handleBreadcrumbChange(event));
        breadcrumb.addEventListener('id-search', (event) => this.handleIdSearch(event));
        breadcrumb.addEventListener('context-change', (event) => this.handleContextChange(event));
        this.querySelector('#db-explorer-first-button').addEventListener('click', () => this.firstPage(this));
        this.querySelector('#db-explorer-prev-button').addEventListener('click', () => this.previousPage(this));
        this.querySelector('#db-explorer-next-button').addEventListener('click', () => this.nextPage(this));
        this.querySelector('#db-explorer-last-button').addEventListener('click', () => this.lastPage(this));
        document.getElementById("db-explorer-current-page").addEventListener("keypress", (e) => {
            if (e.key === 'Enter') {
                this.curPage = document.getElementById("db-explorer-current-page").value;
                this.renderTable();
            }
        });
        // Search event listeners
        this.inputSearchValue.addEventListener("enterKeyPressed", (e) => this.handleEnterKeyPressed(e));
        this.inputSearchValue.addEventListener("flagCiChange", (e) => this.handleFlagCiChange(e));
        this.inputSearchValue.addEventListener("flagDiChange", (e) => this.handleFlagDiChange(e));
        this.inputSearchValue.addEventListener("flagGrChange", (e) => this.handleFlagGrChange(e));
        this.inputSearchValue.addEventListener("flagWwChange", (e) => this.handleFlagWwChange(e));


        // listen for page size change
        document.getElementById("db-explorer-page-size-input").addEventListener("keypress", (e) => {
            if (e.key === 'Enter') {
                this.pageSize = document.getElementById("db-explorer-page-size-input").value;
                this.curPage = 1;
                this.renderTable();
            }
        });

        // initial function calls
        this.setHost();
        this.getLocalStorageRegExFlags(); // get the regEx flags from local storage
        this.setWcRegExFlags(); // pass them to <regex-input>
        this.fetchDirectory("");
    }

    disconnectedCallback() {
        this.inputToggleActionsIcon.removeEventListener('click', (ev) => this.toggleActionColumns(ev));
        this.inputToggleReplaceIcon.removeEventListener('click', (ev) => this.toggleReplace(ev));
        this.inputToggleRxIcon.removeEventListener('click', (ev) => this.toggleRx(ev));
        this.inputFilterValue.removeEventListener('keyup', (event) => this.filterData(event));
        
        // Breadcrumb component event listeners
        const breadcrumb = this.querySelector('#db-explorer-breadcrumb');
        if (breadcrumb) {
            breadcrumb.removeEventListener('path-change', (event) => this.handleBreadcrumbChange(event));
            breadcrumb.removeEventListener('id-search', (event) => this.handleIdSearch(event));
            breadcrumb.removeEventListener('context-change', (event) => this.handleContextChange(event));
        }
        // Search component event listeners
        this.inputSearchValue.removeEventListener("enterKeyPressed", (e) => this.handleEnterKeyPressed(e));
        this.inputSearchValue.removeEventListener("flagCiChange", (e) => this.handleFlagCiChange(e));
        this.inputSearchValue.removeEventListener("flagDiChange", (e) => this.handleFlagDiChange(e));
        this.inputSearchValue.removeEventListener("flagWwChange", (e) => this.handleFlagWwChange(e));

        this.querySelector('#db-explorer-first-button').removeEventListener('click', () => this.firstPage(this));
        this.querySelector('#db-explorer-prev-button').removeEventListener('click', () => this.previousPage(this));
        this.querySelector('#db-explorer-next-button').removeEventListener('click', () => this.nextPage(this));
        this.querySelector('#db-explorer-last-button').removeEventListener('click', () => this.lastPage(this));
        document.getElementById("db-explorer-current-page").removeEventListener("keypress", (e) => {
            if (e.key === 'Enter') {
                this.curPage = document.getElementById("db-explorer-current-page").value;
                this.renderTable();
            }
        });
        // listen for page size change
        document.getElementById("db-explorer-page-size-input").removeEventListener("keypress", (e) => {
            if (e.key === 'Enter') {
                this.pageSize = document.getElementById("db-explorer-page-size-input").value;
                this.curPage = 1;
                this.renderTable();
            }
        });
    }

    getLocalStorageRegExFlags() {
        this.caseInsensitive = utils.lsGetBool(this.lsPrefix, this.lsCaseInsensitive);
        this.diacriticInsensitive = utils.lsGetBool(this.lsPrefix, this.lsDiacriticInsenstive);
        this.greek = utils.lsGetBool(this.lsPrefix, this.lsGreek);
        this.wholeWord = utils.lsGetBool(this.lsPrefix, this.lsWholeWord);
    }
    setWcRegExFlags() {
        // No longer needed - search input has been removed
    }

    toggleActionColumns(ev) {
        ev.stopImmediatePropagation();
        ev.preventDefault();
        this.scrollY = window.scrollY;

        if (this.actionsHidden) {
            this.actionsHidden = false;
            this.inputToggleActionsIcon.classList.remove("bi-toggle-off");
            this.inputToggleActionsIcon.classList.add("bi-toggle-on");
            this.showActionColumns();
        } else {
            this.actionsHidden = true;
            this.inputToggleActionsIcon.classList.remove("bi-toggle-on");
            this.inputToggleActionsIcon.classList.add("bi-toggle-off");
            this.hideActionColumns();
        }
        // we can't scroll to the exact place because the linebreaks change
        // when we toggle the actions.  But, this puts the screen at approximately the right place.
        this.scrollWindowTo(this.scrollY);
    }

    hideActionColumns() {
        document.querySelectorAll('.action-columns-on').forEach(function (td) {
            td.classList.add("d-none");
        });
        document.querySelectorAll('.action-columns-off').forEach(function (td) {
            td.classList.remove("d-none");
        });
    }

    showActionColumns() {
        document.querySelectorAll('.action-columns-on').forEach(function (td) {
            td.classList.remove("d-none");
        });
        document.querySelectorAll('.action-columns-off').forEach(function (td) {
            td.classList.add("d-none");
        });
    }

    pathShortenedId(id) {
        if (this.theData && this.theData.Path) {
            const l = this.theData.Path.join("/").length+1;
            if (id.length > l) {
                return id.slice(l) // returns the remainder of id
            }
        }
        return id;
    }
    // Populates theDataIndexArray with indexes of this.theData.Values.
    // If pattern is null, all the indexes of this.theData.Values will be pushed onto the array.
    // If pattern is not null, only indexes of data IDs or Values that match will be pushed onto the array.
    filterData(evt) {
        // // ignore keypress unless user presses enter key
        // if (evt.keyCode !== 13) {
        //   return;
        // }
        this.currentIdFilter = this.querySelector("#db-explorer-filter-id").value;
        this.currentValueFilter = this.querySelector("#db-explorer-filter-value").value;
        this.dataIndexArray = [];
        let l = this.currentValueFilter.length;
        // because we use the filters as used as regular expressions,
        // check to see if the user has entered a character
        // that is a reserved regex character.  It must be
        // escaped to use it as a literal....
        if ((this.currentValueFilter.endsWith(`(`)
                || this.currentValueFilter.endsWith(`)`)
            || this.currentValueFilter.endsWith(`{`)
            || this.currentValueFilter.endsWith(`}`)
            || this.currentValueFilter.endsWith(`[`)
            || this.currentValueFilter.endsWith(`]`))
            && this.currentValueFilter.charAt(l-2) !== `\\`) {
            this.setAlertWarning("In regular expressions, a literal (, ), {, }, [, or ] must be preceded by \\, e.g. \\(");
            return
        }
        if (this.currentIdFilter.length > 0 || this.currentValueFilter.length > 0) {
            // Even as recent as Jan 2023, there is an ECMAScript 3 Javascript bug such that
            // regex test only matches every other string.
            // See https://stackoverflow.com/questions/3891641/regex-test-only-works-every-other-time
            // This is why, below, we keep re-creating the regex and wrap it in parentheses.
            this.theData.Values.forEach((item, index) => {
                const subId = this.pathShortenedId(item.ID)

                // if both the ID and Value filter have a pattern, both must match
                if (this.currentIdFilter.length > 0 && this.currentValueFilter.length > 0) {
                    if ((this.testIdFilterRegEx(subId)) && (this.testValueFilterRegEx(item.Value))) {
                        this.dataIndexArray.push(index);
                    } else {
                        //
                    }
                } else if (this.currentIdFilter.length > 0) { // only need to match ID filter
                    if (this.testIdFilterRegEx(subId)) {
                        this.dataIndexArray.push(index);
                    }
                } else if (this.currentValueFilter.length > 0) { // only need to match Value filter
                    if (this.testValueFilterRegEx(item.Value)) {
                        this.dataIndexArray.push(index);
                    }
                }
            });
        } else {
            this.setDataIndexForAllRows();
        }
        this.curPage = 1;
        this.renderTable();
    }

    setDataIndexForAllRows() {
        this.dataIndexArray = [];
        if (!this.theData || !this.theData.Values) {
            return;
        }
        const j = this.theData.Values.length;
        for (let i = 0; i < j; i++) {
            this.dataIndexArray.push(i);
        }
    }

    setSearch() {
        // We now get the search value from handleEnterKeyPressed via this.currentSearch
        // No need to reference this.inputSearchValue directly
        if (!this.currentSearch || this.currentSearch.length === 0) {
            this.setAlertWarning("you must set a search pattern");
            return;
        }
        
        if (!this.currentPath.length > 0) {
            this.setAlertWarning("you must be at least one level deep");
            return;
        }
        
        // Store current table state before search
        this.storeCurrentTableState();
        
        this.setAlertInfo("searching...");
        this.searchDatabase(this.currentPath, this.currentSearch, false)
            .then(r => "")
            .catch(err => {
                this.setAlertWarning("Search failed: " + (err.message || "unknown error"));
                // Restore previous state on error
                this.restoreTableState();
            });
    }
    
    /**
     * Stores current table state before search
     * Used to restore if search has no results
     */
    storeCurrentTableState() {
        this.previousTableState = {
            theData: JSON.parse(JSON.stringify(this.theData)),
            dataIndexArray: [...this.dataIndexArray],
            dataLength: this.dataLength,
            currentPath: this.currentPath
        };
    }
    
    /**
     * Restores previous table state
     * Used when search has no results
     */
    restoreTableState() {
        if (this.previousTableState) {
            // Set flag to prevent "0 found" message during restore
            this.isRestoringState = true;
            
            // Restore previous state values
            this.theData = this.previousTableState.theData;
            this.dataIndexArray = this.previousTableState.dataIndexArray;
            this.dataLength = this.previousTableState.dataLength;
            
            // Render table with restored state
            this.renderTable();
            
            // Reset flag after rendering
            this.isRestoringState = false;
        }
    }

    async searchDatabase(path, pattern, searchIds) {
        if (path === "index.html" || path === "db") {
            path = "";
        }
        this.clearFilter();
        this.responseSearchPattern = "";
        this.divRx.innerText = "";
        this.divRxFlags.innerText = "";

        const url = this.host + 'api/search?path=' + path
            + "&pattern=" + encodeURIComponent(pattern)
            + "&caseInsensitive=" + this.caseInsensitive
            + "&diacriticInsensitive=" + this.diacriticInsensitive
            + "&greek=" + this.greek
            + "&wholeWord=" + this.wholeWord
            + "&searchIds=" + searchIds
        ;
        return new Promise(async (res, rej) => {
            await fetch(url)
                .then(data => data.json())
                .then((json) => {
                    try {
                        this.theData = Object.assign(new SearchResponse(), json);
                    } catch {
                        this.theData = json;
                    }
                    if (this.theData.Status === "OK") {
                        this.currentIdFilter = "";
                        this.setDataIndexForAllRows();
                        if (json.PatternExpanded) {
                            this.useNfd = json.NFD;
                            this.responseSearchPattern = json.PatternExpanded;
                            this.divRx.innerText = this.responseSearchPattern
                            let flags = "";
                            if (json.CaseInsensitive) {
                                flags = "Case insensitive. ";
                            }
                            if (json.DiacriticInsensitive) {
                                flags = flags + "Diacritics insensitive. ";
                            }
                            if (json.WholeWord) {
                                flags = flags + "Whole Word. ";
                            }
                            if (json.NFD) {
                                flags = flags + "Text converted to Unicode NFD for search, then back to NFC for display.";
                            } else {
                                flags = flags + "Text is in Unicode NFC for search and display.";
                            }
                            this.divRxFlags.innerText = flags;
                            this.toggleMenuRx();
                            this.renderTable(this.responseSearchPattern);
                        } else {
                            this.renderTable(pattern);
                        }
                        res();
                    } else {
                        if (this.theData && this.theData.Message) {
                            this.setAlertWarning(this.theData.Message);
                        } else {
                            this.setAlertWarning("unknown error")
                        }
                    }
                })
                .catch((error) => rej(error));
        })
    }



    getLocalStorageRegExFlags() {
        this.caseInsensitive = utils.lsGetBool(this.lsPrefix, this.lsCaseInsensitive);
        this.diacriticInsensitive = utils.lsGetBool(this.lsPrefix, this.lsDiacriticInsenstive);
        this.greek = utils.lsGetBool(this.lsPrefix, this.lsGreek);
        this.wholeWord = utils.lsGetBool(this.lsPrefix, this.lsWholeWord);
    }
    setWcRegExFlags() {
        // Refresh reference if needed
        if (!this.inputSearchValue) {
            this.inputSearchValue = this.querySelector('#db-explorer-search-input');
        }
        
        // Skip if element doesn't exist
        if (!this.inputSearchValue) {
            return;
        }
        
        this.inputSearchValue.setAttribute("ci", JSON.stringify(this.caseInsensitive));
        this.inputSearchValue.setAttribute("di", JSON.stringify(this.diacriticInsensitive));
        this.inputSearchValue.setAttribute("gr", JSON.stringify(this.greek));
        this.inputSearchValue.setAttribute("ww", JSON.stringify(this.wholeWord));
    }

    handleEnterKeyPressed(ev) {
        // Extract the search value if provided in the event
        let searchValue = '';
        if (ev && ev.detail && ev.detail.value) {
            searchValue = ev.detail.value;
        }
        
        // Store the search value
        this.currentSearch = searchValue;
        
        // Perform the search
        this.setSearch();
    }
    handleFlagCiChange(e) {
        if (e && e.detail) {
            this.caseInsensitive = e.detail.value === "true";
            utils.lsSetBool(this.lsPrefix, this.lsCaseInsensitive, JSON.stringify(this.caseInsensitive));
        }
    }
    handleFlagDiChange(e) {
        if (e && e.target) {
            this.diacriticInsensitive = e.detail.value === "true";
            utils.lsSetBool(this.lsPrefix, this.lsDiacriticInsenstive, JSON.stringify(this.diacriticInsensitive));
        }
    }
    handleFlagGrChange(e) {
        if (e && e.target) {
            this.greek = e.detail.value === "true";
            utils.lsSetBool(this.lsPrefix, this.lsGreek, JSON.stringify(this.greek));
        }
    }
    handleFlagWwChange(e) {
        if (e && e.target) {
            this.wholeWord = e.detail.value === "true";
            utils.lsSetBool(this.lsPrefix, this.lsWholeWord, JSON.stringify(this.wholeWord));
        }
    }


    showModalIconHelp(ev) {
        ev.stopPropagation();
        this.scrollY = window.scrollY;
        let help = JSON.stringify([
            {
                "icon": "bi-plus-lg",
                "desc": "Add a new directory (folder) or record to the database.",
            },
            {
                "icon": "ID",
                "desc": "Copy record ID to clipboard to paste elsewhere.",
            },
            {
                "icon": "RD",
                "desc": "Copy as an RID to clipboard to paste in template.",
            },
            {
                "icon": "SD",
                "desc": "Copy as a SID to clipboard to paste in template.",
            },
            {
                "icon": "bi-receipt",
                "desc": "Copy record ID and value to clipboard to paste elsewhere.",
            },
            {
                "icon": "bi-pencil",
                "desc": "Edit record value.",
            },
            {
                "icon": "bi-filter",
                "desc": "Filter the table based on part of a word, a whole word, a phrase, or regular expression.",
            },
            {
                "icon": "bi-download",
                "desc": "Import records from your Doxa exports folder.",
            },
            {
                "icon": "bi-toggle-off",
                "desc": "The named feature is turned off or hidden. Click to turn it on or view it.",
            },
            {
                "icon": "bi-toggle-on",
                "desc": "The named feature is turned on or visible. Click to turn it off or hide it.",
            },
            {
                "icon": "bi-search",
                "desc": "Search the database records using the regular expression pattern you entered.",
            },
        ]);
        this.querySelector('#db-explorer-modal-icon-help').setAttribute("data", help);
        this.querySelector('#db-explorer-modal-icon-help').classList.remove("d-none") ;
    }

    handleTableClick(ev) {
        let actionType = ev.target.getAttribute("data-action");
        if (!actionType) {
            return;
        }
        let theRow = ev.target.parentElement.closest("tr");
        if (!theRow) {
            console.log("handleTableClick: could not get row");
            return;
        }
        let theRowIndex = theRow.rowIndex;
        if (!theRowIndex || theRowIndex < 1) {
            console.log("handleTableClick: could not get row index");
            return;
        }
        let theId = theRow.getAttribute("data-id");
        if (!theId) {
            console.log("handleTableClick: could not get row data-id");
            return;
        }
        let table = document.getElementById("db-explorer-data-table");
        if (!table) {
            console.log("handleTableClick: could not get table");
            return;
        }
        ev.stopPropagation();
        ev.preventDefault();

        switch (actionType) {
            case "actionCopyId":
                navigator.clipboard.writeText(theId).then();
                break;
            case "actionCopySid":
                this.kvs.parsePath(theId);
                navigator.clipboard.writeText(this.kvs.sid()).then();
                break;
            case "actionCopyRid":
                this.kvs.parsePath(theId);
                navigator.clipboard.writeText(this.kvs.rid()).then();
                break;
            case "actionCopyRec":
                let theValue = theRow.getElementsByTagName("td")[1].innerText;
                navigator.clipboard.writeText(`${theId}\n${theValue}\n`).then();
                break;
            case "actionDelete":
                this.handleDelete(theId);
                break;
            case "actionDirList":
                let dataId = ev.target.getAttribute("data-id");
                if (!dataId) {
                    return;
                }
                this.fetchDirectory(dataId);
                break;
            case "actionEdit":
                if (theId.includes(":")) { // this is a record
                    let theValue = theRow.getElementsByTagName("td")[1].innerText;
                    this.handleUpdateRecord(theId, theValue, theRowIndex - 1);
                } else { // this is a directory
                    alert("actionEdit Directory Not Implemented Yet");
                }
                break;
            case "actionExport":
                alert("actionExport TBD");
                break;
            case "showActionCols":
                this.showActionColumns();
                break;
            default:
                alert(`Not implemented: ${actionType}`);
        }
    }

    getDataTableRecordId(index) {
        let row = this.getDataTableRow(index);
        if (!row) {
            return "";
        }
        if (row.hasAttribute("data-id")) {
            return row.getAttribute("data-id");
        }
        let td = row.querySelector('td:nth-child(1)') // ID.  Use 2 if add back checkbox col
        if (!td) {
            return "";
        }
        if (td.hasAttribute("data-id")) {
            return td.getAttribute("data-id");
        }
        return "";
    }

    getDataTableRecordValue(index) {
        let row = this.getDataTableRow(index);
        if (!row) {
            return "";
        }
        let td = row.querySelector('td:nth-child(2)') // Value. Use 3 if add back check box col
        if (!td) {
            return "";
        }
        return td.innerText;
    }

    searching = function () {
        return this.currentSearch.length > 0;
    };

    setDataTableRecordValue(index, value) {
        let row = this.getDataTableRow(index);
        if (!row) {
            return;
        }
        let td = row.querySelector('td:nth-child(2)') // Value. Use 3 if add back check box col
        if (!td) {
            return;
        }
        td.innerText = value;
    }

    getDataTableRow(index) {
        return document.getElementById("doxa-db-explorer-table-row-" + index);
    }

    handleModalIconHelpClick(ev) {
        ev.stopPropagation();
        const btn = ev.composedPath()[0].id;
        if (btn === "closeBtn" || btn === "cancelBtn") {
            this.querySelector('#db-explorer-modal-icon-help').classList.add("d-none");
            this.scrollWindowTo(this.scrollY);
        }
    }

    wordBoundary = `[ |\.|;|?|!|"|'|:|˙|$|^|\(|\)]`;

    // =====================================================================
    // UI RENDERING & DISPLAY
    // =====================================================================

    getPathDelimiter() {
        // Default to slash
        return "/";
    }

    renderTable() {
        this.currentPath = this.theData.Path.join("/");

        // Update breadcrumb component
        const breadcrumb = this.querySelector("#db-explorer-breadcrumb");
        breadcrumb.setPath(this.currentPath);
        breadcrumb.setSearching(this.searching());
        
        // Set the host attribute on the breadcrumb component
        if (this.host) {
            breadcrumb.setAttribute('host', this.host);
        }
        const table = document.getElementById('db-explorer-tbody');

        // clear rows
        while (table.firstChild) {
            table.removeChild(table.firstChild);
        }
        this.dataLength = this.dataIndexArray.length;
        if (this.dataLength === 0) {
            // Only set the "0 found" alert if we're not in the process of restoring state
            if (!this.isRestoringState) {
                this.setAlert("0 found");
            }
            return;
        }
        this.useId = this.currentSearch.length > 0;

        let displaying = 0;
        let start = (this.curPage - 1) * this.pageSize;
        let end = this.curPage * this.pageSize;

        // set up filter regEx to highlight matching filter words
        let filterRegEx = {};
        if (this.currentValueFilter) {
            try {
                filterRegEx = new RegExp(this.currentValueFilter, "gu");
            } catch (err) {
                this.setAlertWarning(`filter ${err}`)
            }
        }
        // set up search regEx to highlight matching words
        let searchRegEx = {};
        if (this.currentSearch) {
            let pattern = this.responseSearchPattern;
            let modifier = "";
            pattern = pattern.replace("(?i)", "");
            if (this.caseInsensitive) {
                modifier = "i";
            }
            // enable the Rx switch so user can see the
            // regEx returned by the search API handler.
            modifier += "gu";
            try {
                searchRegEx = new RegExp(pattern, modifier);
            } catch(err) {
                this.setAlertWarning(`javascript: ${err}`)
                return
            }
        }
        this.dataIndexArray.filter((row, index) => {
            if (index >= start && index < end) return true;
        }).forEach((item, index) => {
            displaying++;
            const dataItem = this.theData.Values[item];
            const isDirectory = dataItem.Last.endsWith("/");
            const isLiturgical = dataItem.ID.startsWith("ltx");
            const isKeyring = dataItem.ID.startsWith("keyring");
            const skipAllActions = isKeyring; // can add more later
            const tr = document.createElement('tr');
            tr.setAttribute("id", "doxa-db-explorer-table-row-" + index)
            tr.setAttribute("data-id", dataItem.ID);
            tr.setAttribute("data-index", item);

            // checkbox
            const tdCheck = document.createElement('td');
            tdCheck.innerHTML = '<div><input class="form-check-input doxa-db-explorer-row-check-input" type="checkbox" value="" aria-label="..."></div>';

            // columns for copy actions
            const tdCopyRid = document.createElement('td');
            const tdCopySid = document.createElement('td');
            const tdCopyId = document.createElement('td');
            const tdCopyRecord = document.createElement('td');

            // add class name
            if (this.actionsHidden) {
                tdCopyRid.classList.add("action-columns-on", "d-none");
                tdCopySid.classList.add("action-columns-on", "d-none");
                tdCopyId.classList.add("action-columns-on", "d-none");
                tdCopyRecord.classList.add("action-columns-on", "d-none");
            } else {
                tdCopyRid.classList.add("action-columns-on");
                tdCopySid.classList.add("action-columns-on");
                tdCopyId.classList.add("action-columns-on");
                tdCopyRecord.classList.add("action-columns-on");
            }

            // set up a hidden column for user to toggle actions on and off
            const tdHiddenActionsCol = document.createElement('td');
            if (this.actionsHidden) {
                tdHiddenActionsCol.classList.add("action-columns-off");
            } else {
                tdHiddenActionsCol.classList.add("action-columns-off", "d-none");
            }
            tdHiddenActionsCol.innerHTML = `<i class="" data-action="""showActionCols" style="color: cornflowerblue;"></i>`


            // only show 'copy' icons if this is a record (i.e. not a directory)
            // and, only show 'copy' as RID or SID if it is liturgical
            if (!isDirectory && !isKeyring) {
                if (isLiturgical) {
                    tdCopyRid.innerHTML = '<a href="#"  data-action="actionCopyRid" class="db-explorer-copy-rid">RD</a>';
                    tdCopySid.innerHTML = '<a href="#"  data-action="actionCopySid" class="db-explorer-copy-sid">SD</a>';
                    tdCopyId.innerHTML = '<a href="#" data-action="actionCopyId" class="db-explorer-copy-id">ID</a>';
                } else {
                    tdCopyId.innerHTML = '<a href="#" data-action="actionCopyId" class="db-explorer-copy-id">ID</a>';
                }
                tdCopyRecord.innerHTML = '<a href="#"  data-action="actionCopyRec"><i class="bi-receipt" style="color: cornflowerblue;"   data-action="actionCopyRec"></i></a>';
            }
            // edit icon
            const tdEdit = document.createElement('td');
            if (this.actionsHidden) {
                tdEdit.classList.add("action-columns-on", "d-none");
            } else {
                tdEdit.classList.add("action-columns-on");
            }
            tdEdit.innerHTML = '<a href="#" data-action="actionEdit"><i class="bi-pencil" style="color: cornflowerblue;" data-action="actionEdit"></i></a>';

            // export icon
            const tdExport = document.createElement('td');
            if (this.actionsHidden) {
                tdExport.classList.add("action-columns-on", "d-none");
            } else {
                tdExport.classList.add("action-columns-on");
            }
            tdExport.innerHTML = '<a href="#" data-action="actionExport"><i class="bi-upload" style="color: cornflowerblue;" data-action="actionExport"></i></a>';

            // delete icon
            const tdDelete = document.createElement('td');
            if (this.actionsHidden) {
                tdDelete.classList.add("action-columns-on", "d-none");
            } else {
                tdDelete.classList.add("action-columns-on");
            }
            tdDelete.innerHTML = '<a href="#" data-action="actionDelete"><i class="bi-trash" style="color: cornflowerblue;" data-action="actionDelete"></i></a>';

            const tdId = document.createElement('td');
            const tdVal = document.createElement('td');
            tdId.className = "text-wrap";
            tdId.setAttribute("data-id", dataItem.ID);

            if (isDirectory) {
                const a = document.createElement("a")
                a.setAttribute('href', dataItem.ID);
                if (this.useId) {
                    a.innerText = dataItem.ID.replace(this.currentPath + "/", " / ");
                } else {
                    a.innerText = dataItem.Last;
                }
                a.onclick = this.handleAnchorClick;
                tdId.appendChild(a);
            } else { // if it is a record, display the dirs as a hyperlink
                tdId.innerHTML = this.getIdAsBreadCrumb(this.currentPath, dataItem.ID);
            }
            // create value column
            if (this.currentSearch ||  this.currentValueFilter) {
                let searchString = dataItem.Value;
                if (this.useNfd) {
                    searchString = searchString.normalize("NFD");
                }
                // highlight the words matching the search pattern
                if (this.currentSearch) {
                    searchString = searchString.replace(searchRegEx, (match) => {
                        return `<span class='searchColor'>${match}</span>`;
                    });
                }
                if (this.useNfd) { // change to Unicode NFC
                    searchString = searchString.normalize("NFC");
                }
                // highlight the words matching the filter pattern
                if (this.currentValueFilter) {
                    searchString = searchString.replace(filterRegEx, (match) => {
                        return `<span class='filterColor'>${match}</span>`;
                    });
                }
                tdVal.innerHTML = searchString;
            } else {
                tdVal.innerText = dataItem.Value;
            }

//      tr.appendChild(tdCheck); // uncomment if in adding check box 1st column
            tr.appendChild(tdId);
            tr.appendChild(tdVal);
            tr.appendChild(tdCopyRid);
            tr.appendChild(tdCopySid);
            tr.appendChild(tdCopyId);
            tr.appendChild(tdCopyRecord);
            tr.appendChild(tdEdit);
            tr.appendChild(tdExport);
            tr.appendChild(tdDelete);
            tr.appendChild(tdHiddenActionsCol);

            table.appendChild(tr);
        })
        let alertMsg = "";
        if (this.theData && this.theData.Count) {
            // If true, we did a search.
            // A record can have more than one match in the text,
            // so the count can be > the number of records.
            // So, report both the total number of pattern matches
            // and the number of matching records.
            alertMsg = `found ${this.theData.Count.toLocaleString()} occurrences in ${this.dataLength.toLocaleString()} records (${this.theData.Time})`;
        } else {
            // if false, we were just browsing the directories in the db
            alertMsg = `${this.dataLength.toLocaleString()} found`;
        }
        this.setAlertInfo(alertMsg);
        this.toggleDbExplorerMenu();
        this.toggleSearchInput();
        this.setTableSizeNotice(start + 1, start + displaying);
    }

    toggleSearchInput() {
        if (!this.inputSearchValue) {
            // Refresh reference if it's undefined
            this.inputSearchValue = this.querySelector('#db-explorer-search-input');
            
            // If still undefined, exit early to prevent errors
            if (!this.inputSearchValue) {
                return;
            }
        }

        if (this.currentPath.length > 0) {
            this.inputSearchValue.style.display = "block";
        } else {
            this.inputSearchValue.style.display = "none";
        }
    }
    getIdFilterRegEx() {
        return new RegExp(this.currentIdFilter, "gi");
    }
    getValueFilterRegEx() {
        return new RegExp(this.currentValueFilter, "gu");
    }
    testIdFilterRegEx(s) {
        return this.testFilterRegEx(this.currentIdFilter, s, "gi")
    }
    testValueFilterRegEx(s) {
        return this.testFilterRegEx(this.currentValueFilter, s, "gu")
    }
    testFilterRegEx(pattern, txt, flags) {
        try {
            let re = new RegExp(pattern, flags);
            return re.test(txt)
        } catch (err) {
            if (err.message.includes("Unterminated group")) {
                this.setAlertWarning(`filter ${err}. Use \ before ( or [ or {.`)
            }
            this.setAlertWarning(`filter - ${err}`)
            return false
        }
    }

    toggleDbExplorerMenu() {
        this.toggleMenuExport();
        this.toggleMenuReplace();
    }
    toggleMenuExport() {
        if (this.currentPath.length === 0) {
            if (!this.menuExportAnchor.classList.contains("d-none")) {
                this.menuExportAnchor.classList.add("d-none");
            }
        } else {
            if (this.menuExportAnchor.classList.contains("d-none")) {
                this.menuExportAnchor.classList.remove("d-none");
            }
        }
    }
    toggleMenuReplace() {
        if (this.currentPath.length === 0) {
            if (!this.menuReplaceAnchor.classList.contains("d-none")) {
                this.menuReplaceAnchor.classList.add("d-none");
            }
        } else {
            if (this.menuReplaceAnchor.classList.contains("d-none")) {
                this.menuReplaceAnchor.classList.remove("d-none");
            }
        }
    }

    toggleMenuRx() {
        if (this.currentPath.length === 0) {
            if (!this.menuRxAnchor.classList.contains("d-none")) {
                this.menuRxAnchor.classList.add("d-none");
            }
        } else {
            if (this.menuRxAnchor.classList.contains("d-none")) {
                this.menuRxAnchor.classList.remove("d-none");
            }
        }
    }
    // toggleRx is called when the value
    // of the rx input is changed.
    // It changes the appearance of the input toggle
    toggleRx(ev) {
        ev.stopImmediatePropagation();
        ev.preventDefault();
        this.scrollY = window.scrollY;

        if (this.rxEnabled) {
            this.rxEnabled = false;
            this.inputToggleRxIcon.classList.remove("bi-toggle-on");
            this.inputToggleRxIcon.classList.add("bi-toggle-off");
        } else {
            this.rxEnabled = true;
            this.inputToggleRxIcon.classList.remove("bi-toggle-off");
            this.inputToggleRxIcon.classList.add("bi-toggle-on");
        }
        this.toggleRxDiv();
        // we can't scroll to the exact place because the linebreaks change
        // when we toggle the actions.  But, this puts the screen at approximately the right place.
        this.scrollWindowTo(this.scrollY);
    }
    toggleRxDiv() {
        if (this.currentPath.length === 0) {
            if (!this.divRxRow.classList.contains("d-none")) {
                this.divRxRow.classList.add("d-none");
            }
        } else {
            if (this.divRxRow.classList.contains("d-none")) {
                this.divRxRow.classList.remove("d-none");
            }
        }
    }

    // toggleReplace is called when the value
    // of the replace input is changed.
    // It changes the appearance of the input toggle
    toggleReplace(ev) {
        ev.stopImmediatePropagation();
        ev.preventDefault();
        this.scrollY = window.scrollY;

        if (this.replaceEnabled) {
            this.replaceEnabled = false;
            this.inputToggleReplaceIcon.classList.remove("bi-toggle-on");
            this.inputToggleReplaceIcon.classList.add("bi-toggle-off");
        } else {
            this.replaceEnabled = true;
            this.inputToggleReplaceIcon.classList.remove("bi-toggle-off");
            this.inputToggleReplaceIcon.classList.add("bi-toggle-on");
        }
        this.toggleReplaceDiv();
        // we can't scroll to the exact place because the linebreaks change
        // when we toggle the actions.  But, this puts the screen at approximately the right place.
        this.scrollWindowTo(this.scrollY);
    }
    toggleReplaceDiv() {
        if (this.currentPath.length > 0 && this.replaceEnabled) {
            if (this.divReplaceRow.classList.contains("d-none")) {
                this.divReplaceRow.classList.remove("d-none");
            }
        } else {
            if (!this.divReplaceRow.classList.contains("d-none")) {
                this.divReplaceRow.classList.add("d-none");
            }
        }
    }

    getIdAsBreadCrumb(path, id) {
        this.kvs.parsePath(id.replace(path, ""));
        let bc = `<nav aria-label="breadcrumb">
                <ol class="breadcrumb">`
        let dataId = this.currentPath;
        this.kvs.dirs.forEach((item, index) => {
            dataId += "/" + item;
            bc += `<li class="breadcrumb-item"><a href="#" data-action="actionDirList" data-id="${dataId}">${item}</a></li>`;
        });
        bc += `<li class="" aria-current="page">&nbsp;:&nbsp;${this.kvs.keyPartsSegmentString()}</li>
        </ol>
       </nav>`;
        return bc;
    }

    // =====================================================================
    // EVENT HANDLERS
    // =====================================================================

    handleAnchorClick = (evt) => {
        evt.preventDefault();
        evt.stopImmediatePropagation();
        this.clearFilter();
        this.clearSearch();
        this.curPage = 1;
        const url = new URL(evt.currentTarget.href);
        let path = url.pathname.slice(1);
        // when running in WebStorm, it adds doxa-web-components to path.  Remove it.
        path = path.replace("doxa-web-components/", "");
        // when running in Goland, it adds static to path.  Remove it.
        path = path.replace("static/", "");
        this.fetchDirectory(path);
    }

    handleBreadcrumbChange(event) {
        if (event && event.detail && event.detail.path !== undefined) {
            this.clearFilter();
            this.clearSearch();
            this.curPage = 1;
            
            // Disable navigation and show loading indicator
            this.disableNavigation();
            this.setAlertInfo("Loading directory...");
            
            // Special handling for root path to address timing issue
            if (event.detail.path === "") {
                // For root path, we need special handling to ensure buckets are refreshed
                this.fetchDirectory(event.detail.path)
                    .then(() => {
                        // After fetch completes, ensure breadcrumb is fully updated
                        const breadcrumb = this.querySelector("#db-explorer-breadcrumb");
                        if (breadcrumb) {
                            // Return the promise from fetchBuckets to chain properly
                            return new Promise((resolve) => {
                                // Set up one-time event listener for buckets-loaded
                                breadcrumb.addEventListener('buckets-loaded', function handler() {
                                    breadcrumb.removeEventListener('buckets-loaded', handler);
                                    resolve();
                                }, { once: true });
                                
                                // Force a refresh of the breadcrumb
                                breadcrumb.fetchBuckets();
                            });
                        }
                        return Promise.resolve();
                    })
                    .then(() => {
                        // Re-enable navigation after everything completes
                        this.enableNavigation();
                        
                        // Dispatch context change event after everything is updated
                        this.dispatchEvent(new CustomEvent('context-change', {
                            detail: { 
                                type: 'path',
                                path: event.detail.path
                            },
                            bubbles: true,
                            composed: true
                        }));
                    })
                    .catch(err => {
                        console.error("Navigation error:", err);
                        this.enableNavigation();
                        this.setAlertWarning("Error loading directory: " + err.message);
                    });
            } else {
                // For non-root paths, use similar approach but without bucket refresh
                this.fetchDirectory(event.detail.path)
                    .then(() => {
                        // Re-enable navigation after fetch completes
                        this.enableNavigation();
                        
                        // Dispatch a context-change event to notify parent components
                        this.dispatchEvent(new CustomEvent('context-change', {
                            detail: { 
                                type: 'path',
                                path: event.detail.path
                            },
                            bubbles: true,
                            composed: true
                        }));
                    })
                    .catch(err => {
                        console.error("Navigation error:", err);
                        this.enableNavigation();
                        this.setAlertWarning("Error loading directory: " + err.message);
                    });
            }
        }
    }
    
    /**
     * Disables navigation elements during directory loading
     * Prevents multiple clicks and race conditions
     */
    disableNavigation() {
        const breadcrumb = this.querySelector("#db-explorer-breadcrumb");
        if (breadcrumb) {
            // Add a visual indication that navigation is disabled
            breadcrumb.setAttribute('disabled', 'true');
            
            // Add a loading class to the container for styling
            const container = this.querySelector("#db-explorer-breadcrumb-container");
            if (container) {
                container.classList.add('loading');
            }
        }
    }
    
    /**
     * Re-enables navigation elements after directory loading completes
     */
    enableNavigation() {
        const breadcrumb = this.querySelector("#db-explorer-breadcrumb");
        if (breadcrumb) {
            // Remove the disabled attribute
            breadcrumb.removeAttribute('disabled');
            
            // Remove the loading class
            const container = this.querySelector("#db-explorer-breadcrumb-container");
            if (container) {
                container.classList.remove('loading');
            }
        }
    }
    handleIdSearch(event) {
        if (!event || !event.detail) {
            return;
        }
        
        const { path, pattern } = event.detail;
        
        // Set current search pattern
        this.currentSearch = pattern;
        
        // Set alert and prepare UI for search
        this.setAlertInfo("searching...");
        this.resetTableSizeNotice();
        
        // Perform the ID search
        this.searchDatabase(path, pattern, true)
            .then(() => {
                // Dispatch context-change event to notify parent components
                this.dispatchEvent(new CustomEvent('context-change', {
                    detail: { 
                        type: 'search',
                        path: path,
                        pattern: pattern,
                        searchIds: true
                    },
                    bubbles: true,
                    composed: true
                }));
            })
            .catch(error => {
                this.setAlertWarning("Search failed: " + error.message);
            });
    }

    handleContextChange(event) {
        if (!event || !event.detail) {
            return;
        }
        
        const { type, path, bucket } = event.detail;
        
        if (type === 'path' && bucket) {
            // Bucket selection changed - only act if not "Any"
            if (bucket !== '.* (any)') {
                // Fetch the contents for the selected bucket
                const bucketPath = path ? `${path}/${bucket}` : bucket;
                this.clearFilter();
                this.clearSearch();
                this.curPage = 1;
                this.fetchDirectory(bucketPath);
                
                // Dispatch a context-change event to notify parent components
                this.dispatchEvent(new CustomEvent('context-change', {
                    detail: { 
                        type: 'path',
                        path: bucketPath
                    },
                    bubbles: true,
                    composed: true
                }));
            }
        }
    }
    // setSearch() method removed - search functionality now handled by breadcrumb component

    // =====================================================================
    // API INTERACTIONS
    // =====================================================================

    fetchDirectory(path) {
        if (path === "index.html" || path === "db" || path === "ideTools") {
            path = "";
        }
        this.clearFilter();
        this.clearSearch();
        this.responseSearchPattern = "";
        this.divRx.innerText = "";
        this.divRxFlags.innerText = "";
        
        return this.fetchDirectoryWithoutClearing(path);
    }
    
    /**
     * Fetches directory contents without clearing search state
     * Used as a fallback when search returns no results
     * 
     * @param {string} path - Directory path to fetch
     * @returns {Promise} - Promise resolving when directory is fetched
     */
    fetchDirectoryWithoutClearing(path) {
        if (path === "index.html" || path === "db" || path === "ideTools") {
            path = "";
        }
        
        const url = this.host + 'api/list?path=' + path;
        
        return new Promise((resolve, reject) => {
            utils.callApi(url, "GET").then(response => {
                if (response.Status === "OK") {
                    this.theData = response;
                    this.setDataIndexForAllRows();
                    this.renderTable();
                    resolve(response);
                } else {
                    this.setAlertWarning(response.Message);
                    reject(new Error(response.Message));
                }
            }).catch(error => {
                this.setAlertWarning(error.message);
                reject(error);
            });
        });
    }

    async searchDatabase(path, pattern, searchIds) {
        if (path === "index.html" || path === "db") {
            path = "";
        }
        this.clearFilter();
        this.responseSearchPattern = "";
        this.divRx.innerText = "";
        this.divRxFlags.innerText = "";

        const url = this.host + 'api/search?path=' + path
            + "&pattern=" + encodeURIComponent(pattern)
            + "&caseInsensitive=" + this.caseInsensitive
            + "&diacriticInsensitive=" + this.diacriticInsensitive
            + "&greek=" + this.greek
            + "&wholeWord=" + this.wholeWord
            + "&searchIds=" + searchIds
        ;
        console.log(url)
        return new Promise(async (res, rej) => {
            await fetch(url)
                .then(data => data.json())
                .then((json) => {
                    try {
                        this.theData = Object.assign(new SearchResponse(), json);
                    } catch {
                        this.theData = json;
                    }
                    if (this.theData.Status === "OK") {
                        this.currentIdFilter = "";
                        this.setDataIndexForAllRows();
                        
                        // Check if we have any results
                        if (this.dataIndexArray.length === 0) {
                            // No search results found
                            const noResultsMessage = `No matches found for '${pattern}'`;
                            this.setAlertInfo(noResultsMessage);
                            
                            // Keep the current directory contents, but show "0 found" message
                            this.restoreTableState();
                            
                            // Set the "0 found" count in the alert area
                            document.getElementById("db-explorer-alert").innerText = "Status: 0 found";
                            
                            // Dispatch a context-change event to notify parent components
                            this.dispatchEvent(new CustomEvent('context-change', {
                                detail: { 
                                    type: 'search',
                                    path: path,
                                    pattern: pattern,
                                    searchIds: searchIds,
                                    noResults: true
                                },
                                bubbles: true,
                                composed: true
                            }));
                            res();
                            return;
                        }
                        
                        // Process search results
                        if (json.PatternExpanded) {
                            this.useNfd = json.NFD;
                            this.responseSearchPattern = json.PatternExpanded;
                            this.divRx.innerText = this.responseSearchPattern
                            let flags = "";
                            if (json.CaseInsensitive) {
                                flags = "Case insensitive. ";
                            }
                            if (json.DiacriticInsensitive) {
                                flags = flags + "Diacritics insensitive. ";
                            }
                            if (json.WholeWord) {
                                flags = flags + "Whole Word. ";
                            }
                            if (json.NFD) {
                                flags = flags + "Text converted to Unicode NFD for search, then back to NFC for display.";
                            } else {
                                flags = flags + "Text is in Unicode NFC for search and display.";
                            }
                            this.divRxFlags.innerText = flags;
                            this.toggleMenuRx();
                            this.renderTable(this.responseSearchPattern);
                            
                            // Dispatch a context-change event to notify parent components
                            this.dispatchEvent(new CustomEvent('context-change', {
                                detail: { 
                                    type: 'search',
                                    path: path,
                                    pattern: pattern,
                                    searchIds: searchIds
                                },
                                bubbles: true,
                                composed: true
                            }));
                        } else {
                            this.renderTable(pattern);
                        }
                        res();
                    } else {
                        // Handle error response
                        const errorMessage = (this.theData && this.theData.Message) ? 
                            this.theData.Message : "unknown error";
                        this.setAlertWarning(errorMessage);
                        
                        // On error, restore previous table state rather than fetching new data
                        this.restoreTableState();
                        res();
                    }
                })
                .catch((error) => rej(error));
        })
    }
    async createDbDir(id) {
        const url = this.host
            + 'api/db/mkdir?id='
            + id
        ;
        let fetchData = {
            method: 'POST'
        }
        return new Promise(async (res, rej) => {
            await fetch(url, fetchData)
                .then(data => data.json())
                .then((json) => {
                    switch (json.Status) {
                        case "created":
                            this.setAlertSuccess(json.Status);
                            this.fetchDirectory(this.currentPath);
                            break;
                        default:
                            this.setAlertWarning(json.Status);
                    }
                    res();
                })
                .catch((error) => rej(error));
        })
    }

    async createDbRecord(id, value) {
        const url = this.host
            + 'api/db/create?id='
            + encodeURIComponent(id)
            + "&value=" + encodeURIComponent(value)
        ;
        let fetchData = {
            method: 'POST'
        }
        return new Promise(async (res, rej) => {
            await fetch(url, fetchData)
                .then(data => data.json())
                .then((json) => {
                    switch (json.Status) {
                        case "created":
                            this.setAlertSuccess(json.Status);
                            this.fetchDirectory(this.currentPath);
                            break;
                        default:
                            this.setAlertWarning(json.Status);
                    }
                    res();
                })
                .catch((error) => rej(error));
        })
    }

    async dbDelete(id) {
        const url = this.host
            + 'api/db/delete?id='
            + id
        ;
        let fetchData = {
            method: 'PUT'
        }
        return new Promise(async (res, rej) => {
            await fetch(url, fetchData)
                .then(data => data.json())
                .then((json) => {
                    switch (json.Status) {
                        case "deleted":
                            this.setAlertSuccess(json.Status);
                            this.fetchDirectory(this.currentPath);
                            break;
                        default:
                            this.setAlertWarning(json.Status);
                    }
                    res();
                })
                .catch((error) => rej(error));
        })
    }

    dbExport(id) {
        const url = this.host
            + 'api/db/export?id='
            + id
        ;
        utils.callApi(url, "POST").then(response => {
            if (response.Status === "OK") {
                this.setAlertSuccess(response.Message);
            } else {
                this.setAlertWarning(response.Status);
            }
        }).catch(error => {
            this.setAlertWarning(error.message);
        });
    }


    setAlertInfo(msg) {
        document.getElementById("db-explorer-alert").className = "alert alert-info";
        this.setAlert(msg);
    }

    setAlertSuccess(msg) {
        document.getElementById("db-explorer-alert").className = "alert alert-success";
        this.setAlert(msg);
    }

    setAlertWarning(msg) {
        document.getElementById("db-explorer-alert").className = "alert alert-warning";
        this.setAlert(msg);
    }

    setAlert(msg) {
        document.getElementById("db-explorer-alert").innerText = "Status: " + msg;
    }

    clearFilter() {
        let input = document.getElementById("db-explorer-filter-id");
        input.value = "";
        this.currentIdFilter = "";
        input = document.getElementById("db-explorer-filter-value");
        input.value = "";
        this.currentValueFilter = "";
    }

    clearSearch() {
        this.currentSearch = "";
        this.dataLength = 0;
        this.currPage = 1;
        
        // Find the search input element if we don't have a reference to it
        if (!this.inputSearchValue) {
            this.inputSearchValue = this.querySelector('#db-explorer-search-input');
        }
        
        // If we have a reference to the search input, clear it
        if (this.inputSearchValue) {
            // Only manipulate if the element exists
            if (typeof this.inputSearchValue.setValue === 'function') {
                this.inputSearchValue.setValue('');
            }
        }
        
        // Reset breadcrumb state to not searching
        const breadcrumb = this.querySelector("#db-explorer-breadcrumb");
        if (breadcrumb) {
            breadcrumb.setSearching(false);
        }
    }

    handleTableColumnCheckboxChange(ev) {
        const isChecked = ev.currentTarget.checked;
        const grid = document.getElementById("db-explorer-tbody");

        //Reference the CheckBoxes in Table.
        const checkBoxes = grid.getElementsByTagName("INPUT");

        //Loop through the CheckBoxes.
        for (let i = 0; i < checkBoxes.length; i++) {
            checkBoxes[i].checked = isChecked;
        }
    }

    getSelectedDbIds() {
        let theIds = [];
        const grid = document.getElementById("db-explorer-tbody");

        //Reference the CheckBoxes in Table.
        const checkBoxes = grid.getElementsByTagName("INPUT");

        //Loop through the CheckBoxes.
        for (let i = 0; i < checkBoxes.length; i++) {
            if (checkBoxes[i].checked) {
                const row = checkBoxes[i].parentNode.parentNode.parentNode;
                theIds.push(row.cells[1].getAttribute("data-id"));
            }
        }
        return theIds;
    }

    getSelectedDbIdsAndValues() {
        let theResults = [];
        const grid = document.getElementById("db-explorer-tbody");

        //Reference the CheckBoxes in Table.
        const checkBoxes = grid.getElementsByTagName("INPUT");

        //Loop through the CheckBoxes.
        for (let i = 0; i < checkBoxes.length; i++) {
            if (checkBoxes[i].checked) {
                const row = checkBoxes[i].parentNode.parentNode.parentNode;
                const keyValue = `${row.cells[1].getAttribute("data-id")}\n${row.cells[2].innerText}\n`;
                theResults.push(keyValue);
            }
        }
        return theResults;
    }

    setHost() {
        if (this.hasAttribute("host")) {
            this.host = this.getAttribute("host");
            if (!this.host.endsWith("/")) {
                this.host = this.host + "/";
            }
        } else {
            if (globalHost && globalHost.length > 0) {
                this.host = globalHost;
                if (!this.host.endsWith("/")) {
                    this.host = this.host + "/";
                }
            } else {
                this.host = ""
            }
        }
    }

    handleDelete(id) {
        this.scrollY = window.scrollY;
        const component = document.createElement("div");
        component.className = "div-modal-confirm";
        const shadowRoot = component.attachShadow({mode: 'open'});
        shadowRoot.innerHTML = `
        <modal-confirm 
            id="db-explorer-modal-confirm"
            title="Confirm Delete"
            msg1=""
            msg2=""
            msg3=""
            cancelBtn="Cancel"
            okBtn="Delete"
            passThrough=""
            ></modal-confirm>
    `
        let msg1 = "Are you sure you want to delete:";
        let msg2 = id;
        // put spaces between path delimiters so the path will wrap in the modal
        msg2 = msg2.replaceAll("/", " / ");
        msg2 = msg2.replace(":", " : ");
        let msg3 = "You cannot undo a delete.";
        if (!msg2.includes(":")) {
            msg3 = "This directory and all its child directories and records will be deleted. " + msg3;
        }
        shadowRoot.getElementById("db-explorer-modal-confirm").setAttribute("msg1", msg1);
        shadowRoot.getElementById("db-explorer-modal-confirm").setAttribute("msg2", msg2);
        shadowRoot.getElementById("db-explorer-modal-confirm").setAttribute("msg3", msg3);
        component.addEventListener('click', ev => {
            const btn = ev.composedPath()[0].id;
            component.remove();
            this.scrollWindowTo(this.scrollY);
            if (btn === "okBtn") {
                if (id.length > 0) {
                    this.dbDelete(id).then(r => {
                    });
                }
            } else {
                this.setAlertSuccess("delete canceled");
            }
        })
        component._shadowRoot = shadowRoot;
        window.parent.document.querySelector("body").appendChild(component);
        //        document.getElementsByTagName("body")[0].appendChild(component);
        this.renderTable();
    }

    handleRequestForExport() {
        let id = this.theData.Path.join("/")
        this.dbExport(id);
    }
    handleNewRecord() {
        this.scrollY = window.scrollY;
        const component = document.createElement("div");
        component.className = "div-modal-add-record";
        const shadowRoot = component.attachShadow({mode: 'open'});
        shadowRoot.innerHTML = `
      <modal-add-record
          id="db-explorer-modal-add-record"
          class="d-none"
          title="Add New Record"
          recPath=""
          recKey=""
          recValue=""
          cancelBtn="Cancel"
          okBtn="Add">
      </modal-add-record>
    `
        shadowRoot.getElementById("db-explorer-modal-add-record").setAttribute("recPath", this.currentPath);
        component.addEventListener('click', ev => {
            const btn = ev.composedPath()[0].id;
            if (btn === "okBtn") {
                let modal = shadowRoot.getElementById("db-explorer-modal-add-record")
                let id = "";
                if (modal.recPath.length > 0) {
                    id = modal.recPath;
                }
                id = id + ":" + modal.recKey;
                id = id.trim();
                let value = modal.recValue;
                if (id.length > 0 && value.length > 0) {
                    this.createDbRecord(id, value).then(r => {
                    });
                }
            } else {
                this.setAlertSuccess("add record canceled");
            }
            component.remove();
            this.scrollWindowTo(this.scrollY);
        })
        component._shadowRoot = shadowRoot;
        if (window && window.parent && window.parent.document) {
            window.parent.document.querySelector("body").appendChild(component);
        } else if (window && window.document) {
            window.document.querySelector("body").appendChild(component);
        } else {
            document.getElementsByTagName("body")[0].appendChild(component);
        }
    }

    handleUpdateRecord(id, value, row) {
        // Note: row is a pointer to both the table row and an index of this.dataIndexArray.  They are the same.
        // But, the value of this.dataIndexArray[row] is an index of this.theData.
        // So, this.theData[this.dataIndexArray[row]].Value provides the underlying data value.
        this.scrollY = window.scrollY;
        const component = document.createElement("div");
        component.className = "div-modal-edit-record";
        component.innerHTML = `
      <modal-edit-record
          id="db-explorer-modal-edit-record"
          title="Edit Record"
          host=""
          recId=""
          recValue=""
          status=""
          cancelBtn="Close"
          okBtn="Save">
      </modal-edit-record>
    `
        component.addEventListener('click', ev => {
            const modal = window.parent.document.querySelector("#db-explorer-modal-edit-record");
            this.setAlertSuccess("edit window closed");
            this.theData.Values[this.dataIndexArray[row]].Value = modal.recValue;
            this.setDataTableRecordValue(row, modal.recValue)
            modal.setAttribute("visible", "false");
            this.querySelector("#modalDiv").innerHTML = ``;
            component.remove();
            this.scrollWindowTo(this.scrollY);
        })
        component.querySelector("#db-explorer-modal-edit-record").setAttribute("recId", id);
        component.querySelector("#db-explorer-modal-edit-record").setAttribute("recValue", value);
        component.querySelector("#db-explorer-modal-edit-record").setAttribute("visible", "true");
        window.parent.document.querySelector("body").appendChild(component);
    }

    handleNewDir() {
        this.scrollY = window.scrollY;
        const component = document.createElement("div");
        component.className = "div-modal-add-dir";
        const shadowRoot = component.attachShadow({mode: 'open'});
        shadowRoot.innerHTML = `
      <modal-add-dir
          id="db-explorer-modal-add-dir"
          title="Add New Directory"
          dirPath=""
          dir=""
          cancelBtn="Cancel"
          okBtn="Add">
      </modal-add-dir>
    `
        shadowRoot.getElementById("db-explorer-modal-add-dir").setAttribute("dirPath", this.currentPath);
        component.addEventListener('click', ev => {
            const btn = ev.composedPath()[0].id;
            component.remove();
            this.scrollWindowTo(this.scrollY);
            if (btn === "okBtn") {
                let modal = shadowRoot.getElementById("db-explorer-modal-add-dir")
                let id = ""
                if (modal.dirPath.length > 0) {
                    id = modal.dirPath + "/";
                }
                id = id + modal.dir;
                if (id.length > 0) {
                    this.createDbDir(id).then(r => {
                    });
                }
            } else {
                this.setAlertSuccess("add directory canceled");
            }
            component.remove();
            this.scrollWindowTo(this.scrollY);
        })
        component._shadowRoot = shadowRoot;
        window.parent.document.querySelector("body").appendChild(component);
    }

    scrollWindowTo(y) {
        window.scrollTo({top: y, behavior: 'auto'});
    }

    firstPage() {
        if (this.curPage > 1) this.curPage = 1;
        this.renderTable();
        
    }

    previousPage() {
        if (this.curPage > 1) this.curPage--;
        this.renderTable();
    }

    nextPage() {
        if ((this.curPage * this.pageSize) < this.dataLength) this.curPage++;
        this.renderTable();
    }

    lastPage() {
        if ((this.curPage * this.pageSize) < this.dataLength) {
            this.curPage = Math.floor(this.dataLength / this.pageSize) + 1;
            this.renderTable();
        }
    }

    resetTableSizeNotice() {
        if (document.getElementById("db-explorer-row-top")) {
            document.getElementById("db-explorer-row-top").innerText = "0";
        }
        if (document.getElementById("db-explorer-row-bottom")) {
            document.getElementById("db-explorer-row-bottom").innerText = "0";
        }
        if (document.getElementById("db-explorer-row-count")) {
            document.getElementById("db-explorer-row-count").innerText = "0";
        }
        if (document.getElementById("db-explorer-current-page")) {
            document.getElementById("db-explorer-current-page").value = "0";
        }
        if (document.getElementById("db-explorer-page-max")) {
            document.getElementById("db-explorer-page-max").innerText = "0";
        }
        if (document.getElementById("db-explorer-page-size-input")) {
            document.getElementById("db-explorer-page-size-input").value = "0";
        }
    }

    setTableSizeNotice(start, end) {
        if (document.getElementById("db-explorer-row-top")) {
            document.getElementById("db-explorer-row-top").innerText = `${start.toLocaleString()}`;
        }
        if (document.getElementById("db-explorer-row-bottom")) {
            document.getElementById("db-explorer-row-bottom").innerText = `${end.toLocaleString()}`;
        }
        if (document.getElementById("db-explorer-row-count")) {
            document.getElementById("db-explorer-row-count").innerText = `${this.dataLength.toLocaleString()}`;
        }
        if (document.getElementById("db-explorer-current-page")) {
            document.getElementById("db-explorer-current-page").value = `${this.curPage.toLocaleString()}`;
        }
        if (document.getElementById("db-explorer-page-max")) {
            document.getElementById("db-explorer-page-max").innerText = `${(Math.ceil(this.dataLength / this.pageSize)).toLocaleString()}`;
        }
        if (document.getElementById("db-explorer-page-size-input")) {
            document.getElementById("db-explorer-page-size-input").value = `${this.pageSize}`;
        }

        if (document.getElementById("db-explorer-first-button") && document.getElementById("db-explorer-prev-button")) {
            if (this.curPage === 1) {
                document.getElementById("db-explorer-first-button").classList.add("d-none");
                document.getElementById("db-explorer-prev-button").classList.add("d-none");
            } else {
                document.getElementById("db-explorer-first-button").classList.remove("d-none");
                document.getElementById("db-explorer-prev-button").classList.remove("d-none");
            }
        }
        if (document.getElementById("db-explorer-next-button") && document.getElementById("db-explorer-last-button")) {
            if (this.curPage === Math.ceil(this.dataLength / this.pageSize)) {
                document.getElementById("db-explorer-next-button").classList.add("d-none");
                document.getElementById("db-explorer-last-button").classList.add("d-none");
            } else {
                document.getElementById("db-explorer-next-button").classList.remove("d-none");
                document.getElementById("db-explorer-last-button").classList.remove("d-none");
            }
        }
    }

}

window.customElements.define('db-explorer', DbExplorer);