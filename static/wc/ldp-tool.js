import * as utils from "./utils.js";

class LdpTool extends HTMLElement {
  // generates custom event "ldpchange"
  constructor() {
    super();
    console.log("static/wc/ldp-tool.js")
    this.julian = "julian";
    this.gregorian = "gregorian";
    this.mcdTable = null;
    this.container = null;
    this.divAbout = null;
    this.iconAbout = null;
    this.date = "";
    this.lukanCycleDayMsg = "Sunday after Elevation of the Cross";
  }

  connectedCallback() {
    this.innerHTML = `
    <style> 
      @import url('static/css/bootstrap.min.css');
      @import url('static/img/bootstrap-icons.css');
    </style>
    <style>
        .alert-info {
            color: #31708f;
            background-color: #d9edf7;
            border-color: #bce8f1;
        }
        .input-group-append {
            cursor: pointer;
        }
        .ldp-date-label, .ldp-expression-label {
           width: 25px !important;
        }
        .ldp-date-input, .ldp-expression-input {
           width: 50px !important;
        }
        .clickable {
          cursor:pointer;
          outline:none;
        }
    </style>
    <div id="ldpContainer"class="container-fluid ldp-tool">
        <!-- title --> 
        <div class="row flex">
            <p class="wc-title" style="margin-left: -20px; !important;">Liturgical Day Properties Tool&nbsp;<i id="iconAbout" class="bi bi-question-circle clickable" style="color: cornflowerblue;"></i></p>
        </div>
        <div id="divAbout" class="d-none" style="display: flex; flex-direction: column; flex-wrap: wrap; gap: 1rem; max-width: 50vw;">
            <div>
              Date-based services (template IDs that start with 'se.', specifiy the date of the service.
              Expressions (if-statements, switches) and rid use the liturgical properties of the specified date.
              Liturgical properties include the mode of the week, the eothinon, the day of the week, the day of 
              the movable cycle day (aka the numeric day of the Triodion and Pentecostarion), etc. 
              Note that blocks (templates that start with 'bl.' can also have date-sensitive logic.
              You can use the Liturgical Day Properties (LDP) tool to see the liturgical properties of specific dates.
              Use the calendar to select a date.  If you are using the LDP tool in the template editor,
              when you select a date, the preview web page will automatically
              update.  If there are date-sensitive statements in your template, the content will change.
              Also, when you click on a date, the liturgical properties will be shown.
              You can enter expressions and test to see if they are true for the date you selected.
              For example, you could enter <em>modeOfWeek === 1</em> and then click the <em>Check</em> button.
              The LDP tool allows you to test the various conditions handled by the template.  
              To hide this information, click the help icon.
            </div>
        </div>
        <hr>
        <!-- date & calendar type--> 
        <div class="row mt-2 flex">
          <div class="col-4">
            <div id="ldp-date-div" class="input-group mb-3 flex">
              <span id="ldp-date-label" class="input-group-text date-label" >Date</span>
              <input id="ldp-date-input" type="date" class="form-control date-input" value="${this.date}" placeholder="Date" aria-label="Date" aria-describedby="ldp-date-label">
            </div>
          </div>
          <div class="col-8 mt-2">
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="gregorian" value="gregorian" checked>
                <label class="form-check-label" for="gregorian">New Calendar</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="julian" value="julian">
                <label class="form-check-label" for="julian">Old Calendar</label>
              </div>
          </div>
        </div>
        <!-- expression --> 
        <div class="row flex mt-2"
          <div class="col-2">
          </div>
          <div class="col-10">
            <div class="input-group">
              <span id="ldp-expression-label" class="input-group-text">Expression</span>
              <input  id="ldp-expression-input" type="text" placeholder="dayOfWeek == mon" class="form-control" aria-label="Expression" aria-describedby="ldp-expression-label">
              <button id="ldp-btn" type="button" class="btn btn-outline-primary">Check</button>
              <span id="ldp-expression-true" class="input-group-text">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            </div>
          </div>
        </div>
        <hr/>
        <!-- display results --> 
        <div class="row p-1">
          <p class="alert-info">Liturgical Properties for <span id="propsDate"></span> (<span id="propsCalendar"></span>)</p>
          <p>
            The following properties can be used in template if-statements and switch-statements...
          </p>
          <table class="table table-striped">
            <thead><td>Property</td><td>Value</td><td>Comment</td></thead>
            <tbody>
                <tr><td>date</td><td id="monthDay"></td><td></td></tr>
                <tr><td>dayOfWeek</td><td id="dayOfWeek"></td><td></td></tr>
                <tr><td>eothinon</td><td id="eothinon"></td><td>On Sundays, value is 1-11, 0 on all other days</td></tr>
                <tr><td>lukanCycleDay</td><td id="lukanCycleDay"></td><td>Number of days since the start of the <em>last</em>cycle. The Lukan cycle begins on the Monday following the Sunday after the Elevation of the Cross. If the current date is before then, the last cycle occurred last year.</td></tr>
                <tr><td>modeOfWeek</td><td id="modeOfWeek"></td><td></td></tr>
                <tr><td>movableCycleDay</td><td id="movableCycleDay"></td><td>Values are 1-127 if in the Triodion or Pentecostarion periods, otherwise the value is zero. Starts with the Sunday of the Publican and Pharisee and ends with the Sunday of All Saints. Note that day 71 is both the last day of the Triodion and the first day of the Pentecostarion.</td></tr>
                <tr><td>sundayAfterElevationOfCross</td><td id="sundayAfterElevationOfCross"></td><td id="sundayAfterElevationOfCrossMsg">${this.lukanCycleDayMsg}</td></tr>
                <tr><td>sundaysBeforeTriodion</td><td id="sundaysBeforeTriodion"></td><td>Number of Sundays between January 15 and the start of the triodion</td></tr>
                <tr><td>year</td><td id="year"></td><td></td></tr>
            </tbody>
          </table>
        </div>
        <hr/>
        <div class="row p-1">
          <p class="alert-info">Feasts for the Year <span id="feastsYear"></span></p>
          <table class="table table-striped">
            <thead><td>Date</td><td>Day of Week</td><td>Feast / Event</td></thead>
            <tbody>
                <tr><td id="theophany"></td><td id="theophanyDay"></td><td>Holy Theophany</td></tr>
                <tr><td id="meetingLord"></td><td id="meetingLordDay"></td><td>The Meeting of the Lord</td></tr>
                <tr><td id="triodionStart"></td><td id="triodionStartDay"></td><td>Triodion Start</td></tr>
                <tr><td id="annunciation"></td><td id="annunciationDay"></td><td>Annunciation of the Theotokos</td></tr>
                <tr><td id="palmSunday"></td><td id="palmSundayDay"></td><td>Palm Sunday</td></tr>
                <tr><td id="pascha"></td><td id="paschaDay"></td><td>Pascha</td></tr>
                <tr><td id="pentecost"></td><td id="pentecostDay"></td><td>Pentecost</td></tr>
                <tr><td id="allSaints"></td><td id="allSaintsDay"></td><td>All Saints</td></tr>
                <tr><td id="transfiguration"></td><td id="transfigurationDay"></td><td>Transfiguration of the Lord</td></tr>
                <tr><td id="dormition"></td><td id="dormitionDay"></td><td>Dormition of the Theotokos</td></tr>
                <tr><td id="nativityTheotokos"></td><td id="nativityTheotokosDay"></td><td>The Nativity of the Theotokos</td></tr>
                <tr><td id="elevation"></td><td id="elevationDay"></td><td>The Elevation of the Cross</td></tr>
                <tr><td id="lukan"></td><td id="lukanDay"></td><td>Lukan Cycle Start</td></tr>
                <tr><td id="entrance"></td><td id="entranceDay"></td><td>The Entry of the Theotokos</td></tr>
                <tr><td id="nativityChrist"></td><td id="nativityChristDay"></td><td>The Nativity of the Lord</td></tr>
            </tbody>
          </table>
        </div>
        <div class="row p-1">
          <p class="alert-info">Movable Cycle Dates for the Year <span id="mcdYear"></span></p>
          <table class="table table-striped">
            <thead><td>Day</td><td>Period</td><td>Date</td><td>Commemoration</td></thead>
            <tbody id="mcd">
            </tbody>
          </table>
        </div>
    </div>      
    `
    this.container = this.querySelector("#ldpContainer");
    this.mcdTable = this.querySelector("#mcd")
    if (!this.date || this.date.length === 0) {
      this.date = utils.getToday();
    }
    this.divAbout = this.querySelector("#divAbout");
    this.iconAbout = this.querySelector("#iconAbout");
    this.iconAbout?.addEventListener('click', () => {
      if (this.divAbout?.classList.contains("d-none")) {
        this.divAbout.classList.remove("d-none");
      } else {
        this.divAbout.classList.add("d-none");
      }
    });

    this.querySelector("#ldp-date-input").value = this.date;
    if (!utils.isValidDateFormat(this.Date)) {
      alert(`ldp-tools, connectedCallback: invalid date format ${this.date}.  Should be yyyy-mm-dd`);
    }
    this.querySelector("#ldp-btn").addEventListener('click', () => this.handleSubmit());
    this.querySelector("#ldp-date-input").addEventListener('input', ev => this.handleDateInput(ev));
    this.querySelector("#gregorian").addEventListener("change", (event) => this.handleRadioGregorianChange(event));
    this.querySelector("#julian").addEventListener("change", (event) => this.handleRadioJulianChange(event));
    this.handleSubmit();
  }

  disconnectedCallback() {
    this.querySelector("#ldp-btn").removeEventListener('click', () => this.handleSubmit());
    this.querySelector("#ldp-date-input").removeEventListener('input', ev => this.handleDateInput(ev));
    this.querySelector("#gregorian").removeEventListener("change", (event) => this.handleRadioGregorianChange(event));
    this.querySelector("#julian").removeEventListener("change", (event) => this.handleRadioJulianChange(event));
  }

  handleDateInput(ev) {
    ev.stopPropagation();
    this.date = this.querySelector("#ldp-date-input").value;
    if (!utils.isValidDateFormat(this.Date)) {
      alert(`ldp-tools.js, handleDateInput - invalid date format ${this.date}.  Should be yyyy-mm-dd`);
    }

    this.handleSubmit();
  }
  handleRadioJulianChange(ev) {
    if (ev.target.checked) {
      this.calendar = this.julian;
    } else {
      this.calendar = this.gregorian;
    }
    this.handleSubmit();
  }
  handleRadioGregorianChange(ev) {
    if (ev.target.checked) {
      this.calendar = this.gregorian;
    } else {
      this.calendar = this.julian;
    }
    this.handleSubmit();
  }

  handleSubmit() {
    const date = this.querySelector("#ldp-date-input").value;
    if (!utils.isValidDateFormat(this.Date)) {
      alert(`ldp-tool.js, handleSubmit - invalid date format ${this.date}.  Should be yyyy-mm-dd`);
    }
    const exp = this.querySelector("#ldp-expression-input").value;
    this.requestLdp(date, exp, this.calendar).then(response => {
      this.querySelector("#propsDate").innerText = response.Date;
      let cal = response.Calendar.toLowerCase();
      if (cal === "julian") {
        cal = "Old Calendar";
      } else {
        cal = "New Calendar";
      }
      this.querySelector("#propsCalendar").innerText = cal;
      this.querySelector("#feastsYear").innerText = response.Properties.Year;
      this.querySelector("#dayOfWeek").innerText = response.Properties.DayOfWeek.toLowerCase();
      this.querySelector("#ldp-expression-input").setAttribute("placeholder", `dayOfWeek == ${response.Properties.DayOfWeek.toLowerCase()}`);
      this.querySelector("#eothinon").innerText = response.Properties.Eothinon;
      this.querySelector("#lukanCycleDay").innerText = response.Properties.LukanCycleDay;
      this.querySelector("#modeOfWeek").innerText = response.Properties.ModeOfWeek;
      this.querySelector("#monthDay").innerText = response.Properties.MonthDay.toLowerCase();
      this.querySelector("#movableCycleDay").innerText = response.Properties.MovableCycleDay;
      if(response.Properties.Year === response.Properties.LukanCycleYear){
        this.lukanCycleDayMsg = `The date of the of the Elevation of the Cross for this year (${response.Properties.LukanCycleYear}).`;
      }else{
        this.lukanCycleDayMsg = `The date of the Elevation of the Cross for last year (${response.Properties.LukanCycleYear}). Last year's date is used until after the Elevation of Cross this year.`;
      }
      this.querySelector("#sundayAfterElevationOfCrossMsg").innerText = this.lukanCycleDayMsg
      this.querySelector("#sundayAfterElevationOfCross").innerText = response.Properties.SundayAfterElevationOfCrossMonthDay.toLowerCase();
      this.querySelector("#sundaysBeforeTriodion").innerText = response.Properties.SundaysBeforeTriodion;
      this.querySelector("#year").innerText = response.Properties.Year;

      this.querySelector("#theophany").innerText = response.Properties.DateTheophany;
      this.querySelector("#theophanyDay").innerText = response.Properties.DayTheophany;

      this.querySelector("#meetingLord").innerText = response.Properties.DateMeetingLord;
      this.querySelector("#meetingLordDay").innerText = response.Properties.DayMeetingLord;

      this.querySelector("#triodionStart").innerText = response.Properties.DateTriodionStart;
      this.querySelector("#triodionStartDay").innerText = response.Properties.DayTriodionStart;

      this.querySelector("#annunciation").innerText = response.Properties.DateAnnunciation;
      this.querySelector("#annunciationDay").innerText = response.Properties.DayAnnunciation;

      this.querySelector("#palmSunday").innerText = response.Properties.DatePalmSunday;
      this.querySelector("#palmSundayDay").innerText = response.Properties.DayPalmSunday;

      this.querySelector("#pascha").innerText = response.Properties.DatePascha;
      this.querySelector("#paschaDay").innerText = response.Properties.DayPascha;

      this.querySelector("#pentecost").innerText = response.Properties.DatePentecost;
      this.querySelector("#pentecostDay").innerText = response.Properties.DayPentecost;

      this.querySelector("#allSaints").innerText = response.Properties.DateAllSaints;
      this.querySelector("#allSaintsDay").innerText = response.Properties.DayAllSaints;

      this.querySelector("#transfiguration").innerText = response.Properties.DateTransfiguration;
      this.querySelector("#transfigurationDay").innerText = response.Properties.DayTransfiguration;

      this.querySelector("#dormition").innerText = response.Properties.DateDormitionTheotokos;
      this.querySelector("#dormitionDay").innerText = response.Properties.DayDormitionTheotokos;

      this.querySelector("#nativityTheotokos").innerText = response.Properties.DateNativityTheotokos;
      this.querySelector("#nativityTheotokosDay").innerText = response.Properties.DayNativityTheotokos;

      this.querySelector("#elevation").innerText = response.Properties.DateElevationCross;
      this.querySelector("#elevationDay").innerText = response.Properties.DayElevationCross;

      this.querySelector("#lukan").innerText = response.Properties.DateLukanCycleStart;
      this.querySelector("#lukanDay").innerText = response.Properties.DayLukanCycleStart;

      this.querySelector("#entrance").innerText = response.Properties.DateEntranceTheotokos;
      this.querySelector("#entranceDay").innerText = response.Properties.DayEntranceTheotokos;

      this.querySelector("#nativityChrist").innerText = response.Properties.DateNativityChrist;
      this.querySelector("#nativityChristDay").innerText = response.Properties.DayNativityChrist;

      // load Movable Cycle Table
      this.querySelector("#mcdYear").innerText = response.Properties.Year;
      this.mcdTable.innerHTML = ""; // reset the table
      if (response.TriodionMcd) {
        this.loadMcd(response.TriodionMcd, "Triodion")
      }
      if (response.PentecostarionMcd) {
        this.loadMcd(response.PentecostarionMcd, "Pentecostarion")
      }
      if (response.Exp && response.Exp.length > 0) {
        this.querySelector("#ldp-expression-true").innerText = response.ExpTrue;
        if (response.ExpParseErrors !== null && response.ExpParseErrors.length > 0) {
          alert(JSON.stringify(response.ExpParseErrors));
        }
      }
//      console.log(`ldp-tools line 291 dispatching event this.ldpChanged date === ${this.date}`)
      this.dispatchEvent(this.ldpChanged);
    }).catch(error => {
      alert(error);
    });

  }
  loadMcd(m, period) {
    m.forEach((mcd) => {
      let tr = document.createElement("tr")
      // add mcd number
      let tdMcd = document.createElement("td")
      tdMcd.innerText = mcd.MCD
      tr.appendChild(tdMcd)
      // add period
      let tdPeriod = document.createElement("td")
      tdPeriod.innerText = period
      tr.appendChild(tdPeriod)
      // add date
      let tdDate = document.createElement("td")
      tdDate.innerText = mcd.Date
      tr.appendChild(tdDate)
      // add commemoration
      if (mcd.Commemorations) {
        let tdCom = document.createElement("td")
        switch (mcd.Commemorations.length) {
          case 1: {
            tdCom.innerText = mcd.Commemorations[0]
            break;
          }
          case 2: {
            if (mcd.Commemorations[0] === "NA") {
              tdCom.innerText = mcd.Commemorations[0]
            } else {
              tdCom.innerText = `${mcd.Commemorations[1]} (${mcd.Commemorations[0]})`;
            }
            break;
          }
        }
        tr.appendChild(tdCom)
      }
      // add row to table
      this.mcdTable.appendChild(tr)
    });
  }
  async requestLdp(date, exp, cal) {
    const url = this.host
        + `api/ldp?`
        + `date=` + encodeURIComponent(date)
        + `&exp=` + encodeURIComponent(exp)
        + `&cal=` + encodeURIComponent(cal)
    ;
    let fetchData = {
      method: 'GET'
    }

    const response = await fetch(url, fetchData);
    if (!response.ok) {
      let message = "";
      switch (response.status) {
        case 404: {
          message += "not found";
          break;
        }
        case 500: {
          message += "doxa server error";
          break;
        }
        default: {
          message += `An error has occurred: ${response.status}`;
        }
      }
      throw new Error(message);
    }
    const responseJson = await response.json();
    return responseJson;
  }

  // custom events
  ldpChanged = new CustomEvent('ldpchanged', {
    detail: {
      name: 'ldpchanged',
      date: this.date,
    },
    bubbles: true
  });

  // getters and setters
  static get observedAttributes() {
    return ["date"];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
    if (name === "date" && oldVal !== null && oldVal.length > 0) {
      const input = this.querySelector("#ldp-date-input");
      if (input) {
        if (input.value !== this.date) {
          if (!utils.isValidDateFormat(this.Date)) {
            alert(`ldp-tool.js, attributeChangedCallback - invalid date format ${this.date}.  Should be yyyy-mm-dd`);
          }
          input.value = this.date;
          this.handleSubmit();
        }
      }
    }
  }

  adoptedCallback() {
  }
  // get set calendar
  get calendar() {
    return this.getAttribute("calendar");
  }
  set calendar(value) {
    this.setAttribute("calendar", value);
  }

  // get set date
  get date() {
    return this.getAttribute("date");
  }
  set date(value) {
    this.setAttribute("date", value);
  }

  // get set host
  get host() {
    return this.getAttribute("host");
  }
  set host(value) {
    if (!value.endsWith("/")) {
      this.setAttribute("host", value + "/");
    } else {
      this.setAttribute("host", value);
    }
  }

}

window.customElements.define('ldp-tool', LdpTool);