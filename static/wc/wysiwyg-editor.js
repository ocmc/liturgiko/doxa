class WysiwygEditor extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.innerHTML =
`
<style> 
  @import url('static/css/bootstrap.min.css');
  @import url('static/img/bootstrap-icons.css');
</style>
<div id="my-element-div" class="wc-title">${this.name}</div>
`;
    // add listeners.  For each one added, remove it in disconnectedCallback.
    this.querySelector('#my-element-div').addEventListener('click', () => this.sayOuch());
  }

  disconnectedCallback() {
    this.querySelector('#my-element-div').removeEventListener('click', () => this.sayOuch());
  }

  sayOuch() {
    alert("ouch");
  }

  static get observedAttributes() {
    return ['name'];
  }

  //attributeChangedCallback Warning: only works with attributes specified above, in the function observedAttributes
	attributeChangedCallback(name, oldVal, newVal) {
    console.log(`Attribute: ${name} changed!`);
  }

  adoptedCallback() {
  }
  get name() {
    return this.getAttribute("name");
  }

  set name(value) {
    this.setAttribute("name", value);
  }

}

window.customElements.define('wysiwyg-editor', WysiwygEditor);