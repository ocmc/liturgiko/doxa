document.addEventListener('DOMContentLoaded', function() {
    let themeToggle = document.getElementById('theme-toggle');

    function toggleTheme() {
        const currentTheme = document.documentElement.getAttribute('data-theme');
        const newTheme = currentTheme === 'dark' ? 'light' : 'dark';
        document.documentElement.setAttribute('data-theme', newTheme);
        localStorage.setItem('theme', newTheme);
        updateIcon(newTheme);
    }

// Apply the saved theme on page load
    document.addEventListener('DOMContentLoaded', function() {
        const savedTheme = localStorage.getItem('theme') || 'light';
        document.documentElement.setAttribute('data-theme', savedTheme);
        updateIcon(savedTheme);
    });
    function updateIcon(theme) {
        const themeIcon = document.getElementById('theme-icon');
        if (theme === 'dark') {
            themeIcon.className = 'bi bi-brightness-high';
        } else {
            themeIcon.className = 'bi bi-moon';
        }
    }

    if (themeToggle) {
        themeToggle.addEventListener('click', function(event) {
            event.preventDefault();
            toggleTheme();
        });

        updateIcon(document.documentElement.getAttribute('data-theme') || 'light');
    }

    const messageIndicator = document.getElementById('message-indicator');
    const messageModal = document.getElementById('message-modal');
    const alertIndicator = document.getElementById('alert-indicator');
    const alertModal = document.getElementById('alert-modal');

    function setupModal(indicator, modal, modalName) {
        if (indicator && modal) {
            indicator.addEventListener('click', (e) => {
                e.preventDefault();
                try {
                    modal.showModal();
                    document.documentElement.classList.add(`${modalName}-is-open`, `${modalName}-is-opening`);
                } catch (error) {
                    console.error(`Error opening ${modalName} modal:`, error);
                }
            });

            const closeButtons = document.querySelectorAll(`[data-target="${modalName}-modal"]`);
            closeButtons.forEach(button => {
                button.addEventListener('click', (e) => {
                    e.preventDefault();
                    document.documentElement.classList.add(`${modalName}-is-closing`);
                    setTimeout(() => {
                        modal.close();
                        document.documentElement.classList.remove(`${modalName}-is-open`, `${modalName}-is-opening`, `${modalName}-is-closing`);
                    }, 300);
                });
            });
        }
    }

    setupModal(messageIndicator, messageModal, 'message');
    setupModal(alertIndicator, alertModal, 'alert');});