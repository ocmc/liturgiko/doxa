/*
Example usage:
	return &picoWidget.InputField{
		ID:    id,
		Type:  "button",
		Class: "secondary button",
		HtmxFields: []template.HTMLAttr{
			`hx-delete="/api/htmx/token/delete"`,
			`hx-confirm="Are you sure? This cannot be undone. If you delete your Gitlab Token, you will have to manually handle git and Gitlab for your project."`,
			`hx-target="#formMessages"`,
		},
		ConfirmModal: &picoWidget.ConfirmModal{
			ID:          "tokenService-delete-modal",
			Title:       "Are you sure?",
			Message:     "This cannot be undone. If you delete your Gitlab Token, you will have to re-enter it before doxa can interact with your Gitlab Group. Features like Backup and Publish Catalog will not be available.",
			CancelText:  "Cancel",
			ConfirmText: "Delete Token",
		},
		Value: "Delete Token",
	}

Problem: when delete is clicked and the modal confirm is displayed, and its delete button is clicked, the native browser confirm modal appears on top of it.
         See https://htmx.org/examples/confirm/
 */

document.addEventListener('DOMContentLoaded', function() {
    (function () {
        htmx.on('htmx:confirm', function (evt) {

            // Get the target element that triggered the event
            const target = evt.target;

            // Check if the event target has an 'hx-confirm' attribute
            if (! (target.hasAttribute('hx-confirm') && target.hasAttribute('data-confirm-title'))) {
                // If not, proceed without showing the modal
                evt.detail.issueRequest(true);
                return;
            }

            // Prevent the default confirmation dialog
            evt.preventDefault();

            // Retrieve values from data attributes
            const title = target.getAttribute('data-confirm-title') || 'Are you sure?';
            const message = target.getAttribute('data-confirm-message') || 'This action cannot be undone.';
            const cancelText = target.getAttribute('data-confirm-cancel') || 'Cancel';
            const confirmText = target.getAttribute('data-confirm-confirm') || 'Confirm';

            // Get modal elements
            const modal = document.getElementById('confirmModalDialog');
            const titleElement = document.getElementById('confirmModalTitle');
            const messageElement = document.getElementById('confirmModalMessage');
            const confirmButton = document.getElementById('confirmModalConfirmButton');
            const cancelButton = document.getElementById('confirmModalCancelButton');

            // Update modal content with values from data attributes
            titleElement.textContent = title;
            messageElement.textContent = message;
            confirmButton.textContent = confirmText;
            cancelButton.textContent = cancelText;

            const close = () => {
                evt.preventDefault();
                evt.stopPropagation();
                modal.close();
                cleanup();
            }
            // Define confirm and cancel handlers
            const handleConfirm = () => {
                evt.preventDefault();
                evt.stopPropagation();
                close();
                evt.detail.issueRequest(true);  // Proceed with the request
            };

            const handleCancel = () => {
                close();
            };

            const cleanup = () => {
                confirmButton.removeEventListener('click', handleConfirm);
                cancelButton.removeEventListener('click', handleCancel);
            };

            // Attach event listeners for buttons
            confirmButton.addEventListener('click', handleConfirm);
            cancelButton.addEventListener('click', handleCancel);

            // Show the modal
            modal.showModal();

            // Return false to prevent HTMX from showing native confirm dialog
            return false;
        });
    })();
});
