package kvs

import (
	"fmt"
)

func Example() {
	fname := "test.db"
	// Create a new BoltDB Key-Value (data)Store (aka database)
	ds, err := NewBoltKVS(fname)
	handle(err)
	// always defer the closing
	defer ds.Db.Close()

	// create a new KVS manager using the BoltDB ds
	m, err := NewKVS(ds)
	handle(err)
	// now create a record
	// keys have the format:
	// Dirs/KeyParts
	// where Dirs and KeyParts can have segments delimited by the SegmentDelimiter.
	// e.g. ltx/en_us_dedes/actors:Priest
	// A DbR has a KeyPath.
	// The KeyPath has a slice for Dirs and for KeyParts.
	// In the database, each Dirs element becomes a bucket.
	// The Key() method joins the segments of KeyParts into a single string delimited by SegmentDelimiter.
	dbr := NewDbR()
	// the database always has a starting bucket named Doxa.  It is explicitly added for you.
	dbr.KP.Dirs.Push("ltx") // results in a bucket ltx in the Doxa bucket of the database
	// ltx == liturgical text
	dbr.KP.Dirs.Push("en_us_dedes") // adds an en_us_dedes bucket to the ltx bucket
	dbr.KP.Dirs.Push("actors")      // adds an actors bucket to the gr_gr_cog bucket.
	dbr.KP.KeyParts.Push("Priest")  // since we did not push a suffix, the key == Priest
	dbr.Value = "PRIEST"
	err = m.Db.Put(dbr)
	handle(err)

	// read the record
	rec, err := m.Db.Get(dbr.KP)
	fmt.Printf("%s = %s\n", rec.KP.Path(), rec.Value)

	// when writing a lot of records, you get better performance using Batch()
	// create a bunch of records:
	var records []*DbR // holds the records we are going to create

	dbr.KP.KeyParts.Pop() // remove "Priest". Alternatively dbr.KP.ClearID(), which removes all elements.
	dbr.KP.KeyParts.Push("Deacon")
	dbr.Value = "DEACON"
	records = append(records, dbr)

	dbr.KP.KeyParts.Pop() // remove "Deacon"
	dbr.KP.KeyParts.Push("PriestOrDeacon")
	dbr.Value = "PRIEST/DEACON"
	records = append(records, dbr)

	dbr.KP.KeyParts.Pop() // remove "Priest or Deacon"
	dbr.KP.KeyParts.Push("People")
	dbr.Value = "PEOPLE"
	records = append(records, dbr)

	// add the Greek equivalents
	dbr.KP.Clear() // clears prefix, id, and suffix
	dbr.KP.Dirs.Push("ltx")
	dbr.KP.Dirs.Push("gr_gr_cog")
	dbr.KP.Dirs.Push("actors")
	dbr.KP.KeyParts.Push("Priest")
	dbr.Value = "ΙΕΡΕΥΣ"
	records = append(records, dbr)

	dbr.KP.KeyParts.Pop() // remove "Priest". Alternatively dbr.KP.ClearID(), which removes all elements.
	dbr.KP.KeyParts.Push("Deacon")
	dbr.Value = "ΔΙΑΚΟΝΟΣ"
	records = append(records, dbr)

	dbr.KP.KeyParts.Pop() // remove "Deacon"
	dbr.KP.KeyParts.Push("PriestOrDeacon")
	dbr.Value = "ΙΕΡΕΥΣ/ΔΙΑΚΟΝΟΣ"
	records = append(records, dbr)

	dbr.KP.KeyParts.Pop() // remove "Priest or Deacon"
	dbr.KP.KeyParts.Push("People")
	dbr.Value = "ΛΑΟΣ"
	records = append(records, dbr)

	// normally you would not batch so few records, but this is an example
	err = m.Db.Batch(records)
	handle(err)
	/*
	  We now have a bucket hierarchy:
	    doxa
	      |- ltx <- bucket
	          |- en_us_dedes <- bucket
	               |- actors <- bucket
	                   - Deacon <- record key
	                   - Priest <- record key
	                   - PriestOrDeacon <- record key
	          |- gr_gr_cog <- bucket
	               |- actors <- bucket
	                   - Deacon <- record key
	                   - Priest <- record key
	                   - PriestOrDeacon <- record key
	*/

	// There is a means to search the database using a Matcher.
	// It can work as either string comparisons or regular expressions.
	// If regular expressions are used, they must be used for each element of the Matcher,
	// i.e., the Dirs, KeyParts, and value.

	// set up our matcher
	matcher := NewMatcher()
	// A matcher has a KeyPart field.  Use it to set the Dirs and/or KeyParts to be matched.
	matcher.KP.Dirs.Push("ltx")
	matcher.KP.KeyParts.Push("^Priest/") // ^ means beginning of text, / means end
	matcher.Recursive = true             // if true, will recursively search child buckets
	err = matcher.UseAsRegEx()           // be sure to call this AFTER you have set the prefix and id
	handle(err)
	// the matcher will find records in the ltx bucket recursively
	// that have a key that is exactly equal to Priest.
	recs, count, err := m.Db.GetMatching(*matcher)
	fmt.Printf("%d occurrences in %d records", count, len(recs))
	/*
	   should return
	     ltx/en_us_dedes/actors:Priest
	     ltx/gr_gr_cog/actors:Priest
	*/
	matcher.KP.KeyParts.Pop()
	matcher.KP.KeyParts.Push("^Priest") // ^ means beginning of text
	// the matcher will find records in the ltx bucket recursively
	// that have an ID that starts with Priest.
	fmt.Printf("%d occurrences in %d records", count, len(recs))
	/*
	   should return
	     ltx/en_us_dedes/actors:Priest
	     ltx/en_us_dedes/actors:PriestOrDeacon
	     ltx/gr_gr_cog/actors:Priest
	     ltx/gr_gr_cog/actors:PriestOrDeacon
	*/
	matcher = NewMatcher()
	matcher.KP.Dirs.Push("ltx")
	matcher.KP.Dirs.Push("en_us_dedes")
	matcher.KP.Dirs.Push("actors")
	// the matcher will get all records in the ltx/en_us_dedes/actors bucket
	recs, count, err = m.Db.GetMatching(*matcher)
	fmt.Printf("%d occurrences in %d records", count, len(recs))
	/*
	   should return
	     ltx/en_us_dedes/actors:Deacon
	     ltx/en_us_dedes/actors:Priest
	     ltx/en_us_dedes/actors:PriestOrDeacon
	     ltx/gr_gr_cog/actors:Deacon
	     ltx/gr_gr_cog/actors:Priest
	     ltx/gr_gr_cog/actors:PriestOrDeacon
	*/

	// now lets search for a substring in record values
	matcher = NewMatcher()
	matcher.KP.Dirs.Push("ltx")
	matcher.ValuePattern = "PRIEST"
	matcher.Recursive = true
	// the matcher will get recursively all records in ltx with a value containing PRIEST
	recs, count, err = m.Db.GetMatching(*matcher)
	fmt.Printf("%d occurrences in %d records", count, len(recs))
	/*
	   should return
	     ltx/en_us_dedes/actors:Priest
	     ltx/en_us_dedes/actors:PriestOrDeacon
	     ltx/gr_gr_cog/actors:Priest
	     ltx/gr_gr_cog/actors:PriestOrDeacon
	*/
}
func handle(err error) {
	// your code here
}
