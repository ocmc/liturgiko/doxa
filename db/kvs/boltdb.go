package kvs

import (
	"bufio"
	"bytes"
	"fmt"
	"github.com/emirpasic/gods/trees/btree"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	bolt "go.etcd.io/bbolt"
	"golang.org/x/text/unicode/norm"
	"log"
	"os"
	"path"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"
)

const Root = "doxa"
const BatchSize = 2000

// BoltKVS implements a Key-Value (data)store with the methods specified by KVI.
// Note that BoltDB supports the notion of buckets.
// Also, note that KeyPath (a parameter in many of the methods)
// can be implemented with deeply nested buckets (using Dirs), or
// a single bucket (or two) with complex keys (using KeyParts).
// Whichever method is used, it must be consistent with the method used to
// populate the database. Irrespective of which method you use,
// the database will have a single root bucket using the value in the const Root (see above).
// Do not add the Root to Dirs. It will be done for you.
type BoltKVS struct {
	Db *bolt.DB
	Rt []byte
}

// NewBoltKVS returns an instance of BoltKVS with the specified database opened.
// The database is opened in mode read-writeAsLines.
// Be sure to call the Close() method when finished.
// If the directories in the path to kvsPath do not exist, they will be created.
// A timeout error probably means someone has the database open.
// BoltDB only allows one process to have it open.
// The user needs to close the process (e.g. a program) that has it open.
func NewBoltKVS(kvsPath string) (*BoltKVS, error) {
	// create directories if needed
	err := ltfile.CreateDirs(path.Dir(kvsPath))
	if err != nil {
		msg := fmt.Sprintf("error creating directories for %s: %v", kvsPath, err)
		doxlog.Error(msg)
		return nil, fmt.Errorf(msg)
	}
	// set the database options
	options := new(bolt.Options)
	options.Timeout = 1 * time.Second
	db, err := bolt.Open(ltfile.ToSysPath(kvsPath), 0600, options)
	if err != nil {
		// A timeout error probably means the database is open.
		// Only one process is allowed to have it open.
		if db == nil && strings.Contains(fmt.Sprintf("%v", err), "timeout") {
			return nil, fmt.Errorf("timeout opening database %s. Please check to see if you already have the database open. If so close it", kvsPath)
		} else {
			return nil, err
		}
	}
	rt := []byte(Root)
	return &BoltKVS{db, rt}, nil
}

// Backup the data store to the specified directory in the file system.
// It will be saved with the current date and time in the file name.
func (b *BoltKVS) Backup(fpath string) (string, error) {
	err := b.Db.View(func(tx *bolt.Tx) error {
		if err := ltfile.CreateDirs(path.Dir(fpath)); err != nil {
			return err
		}
		f, err := ltfile.Create(fpath)
		defer func(f *os.File) {
			err = f.Close()
			if err != nil {
				doxlog.Errorf("error closing file: %s", err)
			}
		}(f)
		if err != nil {
			return err
		}
		w := bufio.NewWriter(f)
		_, err = tx.WriteTo(w)
		err = w.Flush()
		if err != nil {
			return err
		}
		return err
	})
	return fpath, err
}

// Batch puts each value into the database.
// The values are sorted first in order to optimize the writeAsLines-speed.
// The values are converted to Unicode Normal Form C before saving.
func (b *BoltKVS) Batch(values []*DbR) error {
	if len(values) == 0 {
		return fmt.Errorf("len(values) == 0")
	}
	b.Db.MaxBatchSize = 2000
	sort.Sort(KVPSorter(values))
	err := b.Db.Batch(func(tx *bolt.Tx) error {
		var err error
		var suppressedError error
		var bkt *bolt.Bucket
		for i, rec := range values {
			bkt, err = b.Bucket(*rec.KP, tx, true)
			if err != nil {
				log.Printf("bucket error line %d id %s value %s: %v\n", i, rec.KP.Path(), rec.Value, err)
				return err
			}
			if err = bkt.Put([]byte(rec.KP.Key()), []byte(norm.NFC.String(rec.Value))); err != nil {
				msg := fmt.Sprintf("put error line %d id %s value %s: %v\n", i, rec.KP.Path(), rec.Value, err)
				log.Println(msg)
				if suppressedError == nil {
					suppressedError = fmt.Errorf(msg)
				}
				continue
				//return err
			}
		}
		return suppressedError
	})
	if err != nil {
		return err
	}
	return nil
}

// Bucket returns the database bucket represented by the kp.Dirs slice.
// If kp.Dirs.Size() == 0, the root bucket is returned.
func (b *BoltKVS) Bucket(kp KeyPath, tx *bolt.Tx, create bool) (*bolt.Bucket, error) {
	var bkt *bolt.Bucket
	var err error
	if create {
		bkt, err = tx.CreateBucketIfNotExists(b.Rt)
		if err != nil {
			return nil, fmt.Errorf("error creating root bucket: %v", err)
		}
	} else {
		bkt = tx.Bucket(b.Rt)
		if bkt == nil {
			return nil, fmt.Errorf("error getting root bucket")
		}
	}
	if kp.Dirs.Size() == 0 {
		return bkt, nil
	}
	for _, p := range *kp.Dirs {
		if create {
			bkt, err = bkt.CreateBucketIfNotExists([]byte(p))
			if err != nil {
				return nil, fmt.Errorf("error creating bucket %s in %s: %v", p, kp.SegmentString(kp.Dirs), err)
			}
		} else {
			bkt = bkt.Bucket([]byte(strings.TrimSpace(p)))
			if bkt == nil {
				return nil, fmt.Errorf("error getting bucket %s in %s", p, kp.SegmentString(kp.Dirs))
			}
		}
	}
	return bkt, nil
}

// DirPaths returns the KeyPath for each bucket owned by the Bucket represented by kp.Dirs.
// If the kp.Dirs.Size() == 0, the buckets owned by the Root are returned.
// See the const Root (above) for the value.
func (b *BoltKVS) DirPaths(kp KeyPath) ([]*KeyPath, error) {
	var results []*KeyPath
	err := b.Db.View(func(tx *bolt.Tx) error {
		bkt, err := b.Bucket(kp, tx, false)
		if err != nil {
			return err
		}
		bkt.ForEach(func(k, v []byte) error {
			if v == nil { // the key is a bucket name
				kp := NewKeyPath()
				kp.Dirs.Push(string(k))
				results = append(results, kp)
			}
			return nil
		})
		return nil
	})
	return results, err
}

// DirNames returns the name of each bucket owned by the Bucket represented by kp.Dirs.
// If the kp.Dirs.Size() == 0, the buckets owned by the Root are returned.
// See the const Root (above) for the value.
func (b *BoltKVS) DirNames(kp KeyPath) ([]string, error) {
	var names []string
	buckets, err := b.DirPaths(kp)
	if err != nil {
		return nil, err
	}
	for _, keyPath := range buckets {
		names = append(names, keyPath.SegmentString(keyPath.Dirs))
	}
	return names, nil
}

func (b *BoltKVS) Close() error {
	return b.Db.Close()
}

func (b *BoltKVS) Copy(from, to KeyPath) error {
	// Important: Must take into account any corresponding -nnp directories
	panic("implement me")
	return nil
}

// Delete deletes a record if kp.KeyParts is not empty.
// Otherwise, it deletes the last dir in kp.Dirs.
func (b *BoltKVS) Delete(kp KeyPath) error {

	// copy kp, so we can check for existence of a corresponding -nnp dir
	nkp := kp.Copy()
	nkp.Dirs.Set(0, nkp.Dirs.First()+"-nnp")

	err := b.Db.Update(func(tx *bolt.Tx) error {
		if kp.KeyParts.Empty() {
			last := kp.Dirs.Pop()
			bkt, err := b.Bucket(kp, tx, false)
			if err != nil {
				return err
			}
			// delete the requested directory
			err = bkt.DeleteBucket([]byte(last))
			if err != nil {
				return err
			}
			// if there is a corresponding -nnp dir, delete it also
			if b.Exists(nkp) {
				last = nkp.Dirs.Pop()
				bkt, err = b.Bucket(*nkp, tx, false)
				if err != nil {
					return err
				}
				return bkt.DeleteBucket([]byte(last))
			}
		} else {
			key := kp.Key()
			kp.KeyParts = nil
			bkt, err := b.Bucket(kp, tx, false)
			if err != nil {
				return err
			}
			err = bkt.Delete([]byte(key))
			if err != nil {
				return err
			}
			// if there is a corresponding -nnp record, delete it also
			if b.Exists(nkp) {
				key := nkp.Key()
				nkp.KeyParts = nil
				bkt, err = b.Bucket(*nkp, tx, false)
				return bkt.Delete([]byte(key))
			}
			return nil
		}
		return nil
	})
	return err
}

func (b *BoltKVS) Exists(kp *KeyPath) bool {
	// check to see if kp is path to a record
	if kp.KeyParts != nil && len(kp.Key()) > 0 {
		dbr, err := b.Get(kp)
		if err != nil {
			return false
		}
		return dbr != nil
	} else { // check to see if kp is a path to a bucket
		var bkt *bolt.Bucket
		var err error
		err = b.Db.View(func(tx *bolt.Tx) error {
			bkt, err = b.Bucket(*kp, tx, false)
			if err != nil {
				return err
			}
			return nil
		})
		if err != nil {
			return false
		}
		return bkt != nil
	}
}

func (b *BoltKVS) ExistsMatching(matcher Matcher) (bool, error) {
	var exists bool
	var err error

	err = b.Db.View(func(tx *bolt.Tx) error {
		ckp := NewKeyPath() // current Key Path
		ckp = matcher.KeyPath(ckp)
		bkt, err := b.Bucket(*ckp, tx, false)
		if err != nil {
			return err
		}
		err = bkt.ForEach(func(k, v []byte) error {
			if !exists {
				if v == nil {
					// ignore bucket name if recursive is false
					if matcher.Recursive {
						matches := true // can become false in next if clause
						if matcher.RegEx {
							if len(matcher.DirsRegEx) > matcher.KP.Dirs.Size() {
								matches = matcher.DirsRegEx[matcher.KP.Dirs.Size()].Match(k)
							}
						}
						if matches {
							matcher.KP.Dirs.Push(string(k))
							exists, err = b.ExistsMatching(matcher)
							matcher.KP.Dirs.Pop()
							if err != nil {
								return err
							}
							if exists {
								return nil
							}
						}
					}
				} else {
					idMatches := true
					var valMatches bool
					if matcher.RegEx {
						j := len(matcher.IdRegEx)
						for i, p := range matcher.KP.ParseSegments(string(k)) {
							if i < j {
								idMatches = matcher.IdRegEx[i].Match([]byte(p))
							}
						}
						valMatches = matcher.ValueRegEx.Match(v)
					} else {
						if !matcher.KP.KeyParts.Empty() {
							idMatches = bytes.Equal(k, []byte(matcher.KP.SegmentString(matcher.KP.KeyParts)))
						}
						valMatches = bytes.Contains(v, []byte(matcher.ValuePattern))
					}
					if idMatches && valMatches {
						exists = true
						return nil
					}
				}
			}
			return nil
		})
		return err
	})
	return exists, err
}

// Export the database to the specified output path (normally a file) as text lines of key = value, starting from the supplied path.  If path is unpopulated, starts at root.
// If templates are being exported, they are written as a separate file for each record. The filepath must be the path to the Doxa site directory.
func (b *BoltKVS) Export(kp *KeyPath, pathOut string, exportType ExportType) error {
	if kp == nil {
		return fmt.Errorf("kp cannot be nil, but can be empty")
	}
	if exportType == ExportAsJson {
		p := path.Join(pathOut, kp.Path())
		if ltfile.DirExists(p) {
			err := ltfile.DeleteDirRecursively(p)
			if err != nil {
				return fmt.Errorf("error removing previous export %s: %v", p, err)
			}
		}
	} else if (exportType == ExportAsJson || kp.Dirs.First() == "templates") && ltfile.DirExists(pathOut) {
		err := ltfile.DeleteDirRecursively(pathOut)
		if err != nil {
			return fmt.Errorf("error removing previous export %s: %v", pathOut, err)
		}
	} else if ltfile.FileExists(pathOut) {
		err := ltfile.DeleteDirRecursively(pathOut)
		if err != nil {
			return fmt.Errorf("error removing previous export %s: %v", pathOut, err)
		}
	}
	err := ltfile.CreateDirs(path.Dir(pathOut))
	if err != nil {
		return fmt.Errorf("error creating directories for %s: %v", pathOut, err)
	}

	if exportType == ExportAsJson {
		err = b.writeJson(kp, pathOut)
		if err != nil {
			return fmt.Errorf("error exporting records to %s:%v", pathOut, err)
		}
		return nil
	}
	if kp.Dirs.First() == "templates" {
		if strings.HasSuffix(pathOut, "/templates") {
			pathOut = pathOut[:len(pathOut)-10]
		}
		err = b.writeTemplates(kp, pathOut)
		if err != nil {
			return fmt.Errorf("error exporting templates to %s:%v", pathOut, err)
		}
		return nil
	}
	// if we get here, this is not the templates' dir, and we are writing key-value pairs to a file
	f, err := ltfile.Create(pathOut)
	if err != nil {
		return fmt.Errorf("error creating file %s: %v", pathOut, err)
	}
	defer func(f *os.File) {
		err = f.Close()
		if err != nil {

		}
	}(f)
	w := bufio.NewWriter(f)
	err = b.writeAsLines(*kp, w)
	err = w.Flush()
	if err != nil {
		return err
	}
	return err
}

func (b *BoltKVS) Get(kp *KeyPath) (*DbR, error) {
	dbr := NewDbR()
	dbr.KP = kp
	err := b.Db.View(func(tx *bolt.Tx) error {
		bkt, err := b.Bucket(*kp, tx, false)
		if err != nil {
			return err
		}
		val := bkt.Get([]byte(kp.Key()))
		if val == nil {
			return fmt.Errorf("not found: %s", kp.Path())
		}
		dbr.Value = string(val)
		return nil
	})
	return dbr, err
}

// GetMatching returns all records meeting the criteria of the supplied Matcher.
// If matcher.Substring is not empty, it will be used to filter record values.
// If matcher.RegEx is true, the matcher.Substring is used as the pattern for a regular expression.
// If recursive is true, includes records in sub-buckets recursively.
// If kp.Dirs.Size() == 0, and recursive is true, all records in the database will be returned.
func (b *BoltKVS) GetMatching(matcher Matcher) ([]*DbR, int, error) {
	var recs []*DbR
	// a value pattern can occur multiple times in a record.
	// therefore, occurrences wil be >= len(recs)
	var occurrences int
	var err error
	var valRegEx *regexp.Regexp
	if matcher.ValueRegEx != nil {
		valRegEx = matcher.ValueRegEx
	} else if len(matcher.ValuePattern) > 0 {
		valRegEx, err = regexp.Compile(matcher.ValuePattern)
	}
	if err != nil {
		return nil, 0, err
	}
	err = b.Db.View(func(tx *bolt.Tx) error {
		ckp := NewKeyPath() // current Key Path
		// matcher.KeyPath
		ckp = matcher.KeyPath(ckp)
		bkt, err := b.Bucket(*ckp, tx, false)
		if err != nil {
			return err
		}
		err = bkt.ForEach(func(k, v []byte) error {
			if v == nil {
				// ignore bucket name if recursive is false
				if matcher.Recursive {
					matches := true // can become false in the next if-clause
					if matcher.RegEx {
						if len(matcher.DirsRegEx) > matcher.KP.Dirs.Size() {
							matches = matcher.DirsRegEx[matcher.KP.Dirs.Size()].Match(k)
						}
					}
					if matches {
						matcher.KP.Dirs.Push(string(k))
						subRecs, _, err := b.GetMatching(matcher)
						if err != nil {
							return err
						}
						for _, sRec := range subRecs {
							recs = append(recs, sRec)
						}
						matcher.KP.Dirs.Pop()
					}
				}
			} else {
				value := string(v) // already in Unicode NFC
				if matcher.NFD {
					value = norm.NFD.String(value)
				}
				idMatches := true
				var valMatches bool
				var indexes [][]int
				if matcher.RegEx {
					idMatches = matcher.IdRegEx[0].Match(k)
				} else {
					if !matcher.KP.KeyParts.Empty() {
						idMatches = bytes.Equal(k, []byte(matcher.KP.SegmentString(matcher.KP.KeyParts)))
					}
				}
				if matcher.ValueRegEx == nil {
					valMatches = true
				} else {
					indexes = valRegEx.FindAllStringIndex(value, -1)
					valMatches = indexes != nil && len(indexes) > 0
					occurrences = occurrences + len(indexes)
				}
				if idMatches && valMatches {
					kvp := NewDbR()
					for _, p := range *matcher.KP.Dirs {
						kvp.KP.Dirs.Push(p)
					}
					idParts := kvp.KP.ParseSegments(string(k))
					for _, part := range idParts {
						kvp.KP.KeyParts.Push(part)
					}
					// Always include a record with a non-empty value.
					// Conditionally include if value empty and caller requested inclusion of empties.
					if len(v) > 0 || (len(v) == 0 && matcher.IncludeEmpty) {
						//if !matcher.ForConcordance && kvp.KP.DirHasNnpSuffix() {
						//	// we will remove -nnp from the first dir
						//	// and do a database lookup to get the corresponding value
						//	newKvp := kvp.KP.Copy()
						//	newKvp.RemoveNnpSuffix()
						//	dbr, err := b.Get(newKvp)
						//	if err == nil {
						//		kvp.KP.RemoveNnpSuffix()
						//		value = dbr.Value
						//	}
						//}
						if matcher.NFD {
							// convert back to NFC to send back to user
							value = string(v)
						}
						kvp.Value = value
						if valMatches {
							kvp.Indexes = indexes
						}
						recs = append(recs, kvp)
					}
				}
			}
			return nil
		})
		return err
	})
	//	fmt.Printf("\nHits: %d Pattern: %v NNP: %v WholeWord: %v DirsRegEx: %v\n", len(recs), matcher.ValueRegEx, matcher.NNP, matcher.WholeWord, matcher.DirsRegEx)
	return recs, len(recs), err
}

// GetMatchingIDs returns all records whose KP.Path() matches the valuePattern.
// If recursive is true, will search recursively.
func (b *BoltKVS) GetMatchingIDs(matcher Matcher) ([]*DbR, error) {
	var recs []*DbR
	var err error

	if matcher.ValueRegEx == nil {
		return nil, fmt.Errorf("error: matcher value pattern not set for ID to find")
	}

	err = b.Db.View(func(tx *bolt.Tx) error {
		ckp := NewKeyPath() // current Key Path
		ckp = matcher.KeyPath(ckp)
		bkt, err := b.Bucket(*ckp, tx, false)
		if err != nil {
			return err
		}
		bkt.Cursor()
		err = bkt.ForEach(func(k, v []byte) error {
			if v == nil {
				// ignore bucket name if recursive is false
				if matcher.Recursive {
					matcher.KP.Dirs.Push(string(k))
					recsFound, err := b.GetMatchingIDs(matcher)
					matcher.KP.Dirs.Pop()
					if err != nil {
						return err
					}
					for _, rec := range recsFound {
						if !strings.HasSuffix(rec.KP.Dirs.First(), "-nnp") {
							recs = append(recs, rec)
						}
					}
				}
			} else {
				matcher.KP.KeyParts.Push(string(k))
				idMatches := matcher.ValueRegEx.Match([]byte(matcher.KP.Path()))
				matcher.KP.KeyParts.Pop()
				if idMatches {
					kvp := NewDbR()
					for _, p := range *matcher.KP.Dirs {
						kvp.KP.Dirs.Push(p)
					}
					idParts := kvp.KP.ParseSegments(string(k))
					for _, part := range idParts {
						kvp.KP.KeyParts.Push(part)
					}
					// Always include a record with a non-empty value.
					// Conditionally include if value empty and caller requested inclusion of empties.
					if len(v) > 0 || (len(v) == 0 && matcher.IncludeEmpty) {
						kvp.Value = string(v)
						// exclude the nnp records
						if !strings.HasSuffix(kvp.KP.Dirs.First(), "-nnp") {
							recs = append(recs, kvp)
						}
					}
				}
			}
			return nil
		})
		return err
	})
	return recs, err
}

func (b *BoltKVS) GetResolved(kp *KeyPath) (*DbR, []string, error) {
	visited := make(map[string]bool) // Map to track visited key paths
	return b.getResolvedInternal(kp, visited)
}

func (b *BoltKVS) getResolvedInternal(kp *KeyPath, visited map[string]bool) (*DbR, []string, error) {
	var redirectIds []string
	path := kp.Path()

	// Check if we've already visited this key path to avoid loops
	if visited[path] {
		err := fmt.Errorf("recursive redirect: %s redirects to itself", path)
		doxlog.Errorf("error getting redirect: %v", err)
		return nil, nil, err
	}
	visited[path] = true

	dbr, err := b.Get(kp)
	if err != nil {
		// Append the key path to indicate which record was not found
		redirectIds = append(redirectIds, path)
		return nil, redirectIds, err
	}

	if dbr.IsRedirect() {
		// Return error if this is a recursive redirect
		if strings.HasPrefix(dbr.Value, "@") {
			if dbr.Value[1:] == dbr.KP.Path() {
				err := fmt.Errorf("recursive redirect: %s == %s", dbr.KP.Path(), dbr.Value)
				doxlog.Errorf("error resolving redirect: %v", err)
				return nil, nil, err
			}
		}

		redirectIds = append(redirectIds, dbr.KP.Path())
		rkp, err := dbr.GetRedirect()
		if err != nil {
			redirectIds = append(redirectIds, path)
			return nil, redirectIds, fmt.Errorf("error getting redirect: %v", err)
		}

		// Recursively resolve the redirect
		rdb, rds, err := b.getResolvedInternal(rkp, visited)
		if rds != nil {
			redirectIds = append(redirectIds, rds...)
		}
		if err != nil {
			return nil, redirectIds, fmt.Errorf("error resolving redirect %s in %s", rkp.Path(), path)
		}

		// Update the redirect information in the original dbr
		dbr.Redirect = rdb.KP
		dbr.Value = rdb.Value
	} else {
		redirectIds = append(redirectIds, dbr.KP.Path())
	}

	// If there were multiple redirects, update the final redirect key path
	if len(redirectIds) > 1 {
		redirectKp := NewKeyPath()
		err = redirectKp.ParsePath(redirectIds[len(redirectIds)-1])
		if err == nil {
			dbr.Redirect = redirectKp
		}
	}

	return dbr, redirectIds, nil
}

// GetRedirectsMapper provides a two-way map between records redirected to and redirected from
func (b *BoltKVS) GetRedirectsMapper(kp *KeyPath) (*RedirectMapper, error) {
	m := new(RedirectMapper)
	m.pointedTo = make(map[string][]string)
	m.pointedFrom = make(map[string]string)
	matcher := NewMatcher()
	matcher.KP = kp
	matcher.Recursive = true
	recs, _, err := b.GetMatching(*matcher)
	if err != nil {
		return nil, err
	}
	for _, rec := range recs {
		if rec.IsRedirect() {
			redirectID := rec.Value[len(RedirectsTo):]
			m.pointedFrom[rec.KP.Path()] = redirectID
			if v, found := m.pointedTo[redirectID]; found {
				m.pointedTo[redirectID] = append(v, rec.KP.Path())
			} else {
				m.pointedTo[redirectID] = []string{rec.KP.Path()}
			}
		}
	}
	return m, nil
}

// GetRedirectsToDir checks all records in the directory to see if they are used outside the directory
func (b *BoltKVS) GetRedirectsToDir(kp *KeyPath) ([]*DbR, error) {
	var result []*DbR
	dirPath := kp.Path()
	m := NewMatcher()
	m.KP.Dirs.Push(kp.Dirs.First())
	m.ValuePattern = "@" + kp.Path() + ".*"
	m.Recursive = true
	m.UseAsRegEx()
	recs, _, err := b.GetMatching(*m)
	if err != nil {
		return nil, err
	}
	for _, r := range recs {
		if !strings.HasPrefix(r.KP.Path(), dirPath) {
			result = append(result, r)
		}
	}
	return result, nil
}

func (b *BoltKVS) GetRedirectsToRecord(kp *KeyPath) ([]*DbR, error) {
	m := NewMatcher()
	m.KP.Dirs.Push(kp.Dirs.First())
	m.ValuePattern = kp.Path()
	m.Recursive = true
	m.UseAsRegEx()
	recs, _, err := b.GetMatching(*m)
	return recs, err
}

// IsRedirect returns true if the value of the record is a redirect ID
func (b *BoltKVS) IsRedirect(kp *KeyPath) (bool, *DbR, error) {
	dbr, err := b.Get(kp)
	has := err == nil && dbr != nil && dbr.IsRedirect()
	return has, dbr, err
}

// Import file of text lines of format key = value
// into the database.
// The import file should normally have been created using the Export method
// but any file with lines having key = value can be imported.
// However, keys must use the IdDelimiter and SegmentDelimiter.
// See the constants for these in package kvs.
func (b *BoltKVS) Import(dirsPrefix, pathIn string, lineParser LineParser, removeQuotes, allOrNone, skipObsoleteConfigs bool) (int, []error) {
	var errors []error
	var err error
	var lines []string
	if len(dirsPrefix) > 0 {
		kp := NewKeyPath()
		err = kp.ParsePath(dirsPrefix)
		if err != nil {
			return 0, []error{fmt.Errorf("invalid dirsPrefix %s", dirsPrefix)}
		}
	}
	lines, err = ltfile.GetFileLines(pathIn)
	if err != nil {
		errors = append(errors, fmt.Errorf("error opening file %s: %v", pathIn, err))
		return 0, errors
	}
	cnt := 0
	var recordsToWrite []*DbR
	i := 0
	//	sort.Strings(lines)
	for _, txt := range lines {
		i++
		txt = strings.TrimSpace(txt)
		if len(txt) == 0 {
			continue
		}
		test := strings.ToLower(txt) // test to see if this is the header line
		if i == 1 && strings.HasPrefix(test, "id") && strings.HasSuffix(test, "value") {
			continue
		}

		// initialize variables for lineParser
		var kp *KeyPath
		var value []byte
		var err error

		if lineParser == nil {
			kp = NewKeyPath()
			parts := strings.SplitN(txt, "\t", 2)
			if len(parts) != 2 {
				errors = append(errors, fmt.Errorf("line %d: invalid key %s", i, txt))
				continue
			}
			kp.ParsePath(parts[0])
			value = []byte(parts[1])
		} else {
			kp, value, err = lineParser(txt, LineDelimiter, removeQuotes, nil)
			if err != nil {
				if !strings.HasPrefix(fmt.Sprintf("%v", err), "obsolete configuration property") {
					errors = append(errors, fmt.Errorf("line %d: %v", i, err))
				}
				continue
			}
			// skip this iteration
			if kp == nil && value == nil {
				continue
			}
		}
		if len(dirsPrefix) > 0 {
			if !strings.HasPrefix(kp.Path(), dirsPrefix) {
				continue
			}
		}
		if allOrNone && len(errors) > 0 {
			continue // do not writeAsLines any records
		}
		dbr := NewDbR()
		dbr.KP = kp
		dbr.Value = b.RemoveDisallowedFromString(string(value))
		recordsToWrite = append(recordsToWrite, dbr)
		cnt++
	}
	if allOrNone && len(errors) > 0 {
		return 0, errors
	}
	if len(recordsToWrite) > 0 {
		// we delayed the sort until now so that if there was
		// an error, we could report back the line number accurately.
		sort.SliceStable(recordsToWrite, func(i, j int) bool {
			return recordsToWrite[i].KP.Key() < recordsToWrite[j].KP.Key()
		})
		err = b.Batch(recordsToWrite)
		if err != nil {
			errors = append(errors, err)
		}
	}
	return len(recordsToWrite), errors
}

// Keys returns the key(path)s for Dirs and records (non-recursively)
// for the Dir specified in KeyPath.
// Dir keys will be suffixed with a forward slash
// The first int is the count of keys that are for Dirs.
// The second int is the count of keys that are for records.
func (b *BoltKVS) Keys(kp *KeyPath) ([]*KeyPath, int, int, error) {
	if !kp.KeyParts.Empty() {
		return nil, 0, 0, nil
	}
	var keys []*KeyPath
	bucketKeys := 0
	recordKeys := 0

	err := b.Db.View(func(tx *bolt.Tx) error {
		bkt, err := b.Bucket(*kp, tx, false)
		if err != nil {
			return err
		}
		err = bkt.ForEach(func(k, v []byte) error {
			keyPath := kp.Copy()
			if v == nil {
				keyPath.Dirs.Push(string(k))
				keys = append(keys, keyPath)
				bucketKeys++
			} else {
				keyPath.KeyParts.Push(string(k))
				keys = append(keys, keyPath)
				recordKeys++
			}
			return nil
		})
		return err
	})
	return keys, bucketKeys, recordKeys, err
}

func (b *BoltKVS) Move(from, to KeyPath) error {
	// Important: Must take into account any corresponding -nnp directories
	panic("implement me")
	return nil
}

func (b *BoltKVS) MakeDir(kp *KeyPath) error {
	err := b.Db.Update(func(tx *bolt.Tx) error {
		_, err := b.Bucket(*kp, tx, true)
		return err
	})
	return err
}
func (b *BoltKVS) MakeRedirects(fromKp, toKp *KeyPath) error {
	var err error
	// so we can batch up the records
	var valBatch []*DbR
	// get all the records matching the fromKp
	matcher := NewMatcher()
	matcher.KP = fromKp.Copy()
	matcher.Recursive = true
	matcher.ValuePattern = fmt.Sprintf("%s.*", fromKp.Path())
	err = matcher.UseAsRegEx()
	if err != nil {
		return err
	}
	var dbRecs []*DbR
	dbRecs, err = b.GetMatchingIDs(*matcher)
	if err != nil {
		return err
	}
	// convert them into redirect records and batch for writing toKp
	for _, rec := range dbRecs {
		newRec := NewDbR()
		newRec.KP = rec.KP.Copy()
		newRec.KP.Dirs.Set(1, toKp.Dirs.Get(1))
		newRec.Redirect = rec.KP.Copy()
		newRec.Value = fmt.Sprintf("%s%s", RedirectsTo, newRec.Redirect.Path())
		valBatch = append(valBatch, newRec)
		if len(valBatch) >= BatchSize {
			err = b.Batch(valBatch)
			if err != nil {
				return err
			}
			valBatch = nil
		}
	}
	// if there are left over records in valBatch, writeAsLines them as well.
	if valBatch != nil {
		err = b.Batch(valBatch)
		if err != nil {
			return err
		}
	}
	return nil
}

// Open opens the database.  If the database is already open, it will first close it,
// then reopen it.
func (b *BoltKVS) Open(filename string) error {
	if b.Db != nil {
		err := b.Db.Close()
		if err != nil {
			return err
		}
	}
	options := new(bolt.Options)
	options.Timeout = 1 * time.Second
	db, err := bolt.Open(filename, 0600, options)
	if err != nil {
		return err
	}
	b.Db = db
	return err
}

func (b *BoltKVS) Put(kvp *DbR) error {
	// record values are converted to Unicode Normal Form C before saving
	kvp.Value = norm.NFC.String(kvp.Value)
	kvp.Value = b.RemoveDisallowedFromString(kvp.Value)
	// Return error if this is a recursive redirect
	if strings.HasPrefix(kvp.Value, "@") {
		if kvp.Value[1:] == kvp.KP.Path() {
			return fmt.Errorf("redirecting back to the same record is not allowed")
		}
	}

	// TODO batch needs to also enforce rules about redirects
	if strings.HasPrefix(kvp.Value, RedirectsTo) {
		kvp.Redirect = NewKeyPath()
		err := kvp.Redirect.ParsePath(kvp.Value[1:])
		if err != nil {
			return err
		}
		if kvp.Redirect.Dirs.First() == "ltx" {
			// enforce rule that only _public can redirect outside itself
			if kvp.KP.Dirs.Get(1) != kvp.Redirect.Dirs.Get(1) {
				if !isRedirectDir(kvp.KP.Dirs.Get(1)) {
					return fmt.Errorf("only a library ending in _public, _redirects or containing _redirects_ is allowed to redirect outside itself")
				}
			}
		}
	} else if isRedirectDir(kvp.KP.Dirs.Get(1)) {
		// enforce rule that the value of a record in _public must be a redirect
		return fmt.Errorf("a library ending in _public, _redirects or containing _redirects_ may only have record values that are redirects")
	}
	if b.Db == nil {
		return fmt.Errorf("put error: db not open")
	}
	return b.Db.Update(func(tx *bolt.Tx) error {
		bkt, err := b.Bucket(*kvp.KP, tx, true)
		if err != nil {
			return err
		}
		err = bkt.Put([]byte(kvp.KP.Key()), []byte(kvp.Value))
		if err != nil {
			return err
		}
		first := kvp.KP.Dirs.First()
		// if the user adds or updates a ltx record, add a keyring record for it.
		if HasKeyRing(first) { // e.g. ltx/gr_gr_cog/actors:Priest will have record keyring/actors:Priest
			kp := kvp.KP.Copy()
			dbr := kp.KeyRingRec()
			get, getErr := b.Get(dbr.KP)
			if getErr == nil {
				if !strings.Contains(get.Value, kvp.KP.Dirs.Get(1)) {
					if len(get.Value) > 0 {
						dbr.Value = fmt.Sprintf("%s, %s", get.Value, kvp.KP.Dirs.Get(1))
					} else {
						dbr.Value = kvp.KP.Dirs.Get(1)
					}
				}
			} else {
				dbr.Value = kvp.KP.Dirs.Get(1)
			}
			bkt, err := b.Bucket(*dbr.KP, tx, true)
			if err != nil {
				return err
			}
			err = bkt.Put([]byte(dbr.KP.Key()), []byte(dbr.Value))
			if err != nil {
				return err
			}
		}
		return nil
	})
}
func isRedirectDir(d string) bool {
	if strings.HasSuffix(d, "_public") ||
		strings.HasSuffix(d, "_redirects") ||
		strings.Contains(d, "_redirects_") {
		return true
	} else {
		return false
	}
}

// Tree returns a tree representation of the database directories and records
// keyStart is the path to start with.
// dirOnly when true ignores records
// depth controls how deep the tree will be, -1 all, 0 current dir, 1..n 1 level...n levels
// width controls how many characters to return from value: -1 all, 0 none, 1...n 1 to n chars
// max controls how many keys will be returned
// Note that btree.Tree node key and value are type interface{}.
// The key should be converted to string and the value to DbR.
func (b *BoltKVS) Tree(kp *KeyPath,
	dirOnly bool,
	includeSystem,
	excludeRedirect,
	excludeEmpty,
	excludeText bool,
	depth int,
	width int) (*btree.Tree, error) {
	tree := btree.NewWithStringComparator(3)
	index := ""
	matcher := NewMatcher()
	matcher.KP = kp.Copy()
	matcher.Recursive = true
	err := b.loadTree(tree,
		index,
		matcher,
		dirOnly,
		includeSystem,
		excludeRedirect,
		excludeEmpty,
		excludeText,
		depth+kp.Dirs.Size(),
		width)
	return tree, err
}

// loadTree splits out the tree loading from method Tree, so it can be called recursively.
// Because the tree parm is a pointer, the parm is updated with the results.
func (b *BoltKVS) loadTree(tree *btree.Tree,
	index string,
	matcher *Matcher,
	dirOnly,
	includeSystem,
	excludeRedirect,
	excludeEmpty,
	excludeText bool,
	depth int,
	width int) error {
	var err error
	err = b.Db.View(func(tx *bolt.Tx) error {
		ckp := NewKeyPath() // current Key Path
		ckp = matcher.KeyPath(ckp)
		bkt, err := b.Bucket(*ckp, tx, false)
		if err != nil {
			return err
		}
		i := 0
		err = bkt.ForEach(func(k, v []byte) error {
			i++
			intIndex, _ := strconv.Atoi(index)
			currentIndex := fmt.Sprintf("%04d%04d", intIndex, i)
			key := string(k)
			treeValue := new(TreeValue)
			treeValue.KP = matcher.KP.Copy()
			treeValue.Leaf = key
			var isRecord, exclude bool
			if v == nil {
				matcher.KP.Dirs.Push(key)
				treeValue.KP.Dirs.Push(key)
				if depth > matcher.KP.Dirs.Size() {
					_ = b.loadTree(tree,
						currentIndex,
						matcher,
						dirOnly,
						includeSystem,
						excludeRedirect,
						excludeEmpty,
						excludeText,
						depth,
						width)
				}
				matcher.KP.Dirs.Pop()
			} else {
				isRecord = true
				treeValue.KP.KeyParts.Push(key)
				switch width {
				case -1:
					treeValue.Value = string(v)
				case 0: // ignore
				default:
					val := string(v)
					if len(val) > width {
						val = val[:width-1]
					}
					treeValue.Value = val
				}
			}
			// check to see if this key should be excluded
			if matcher.KP.Dirs.Empty() {
				if IsSystem(key) && !includeSystem {
					exclude = true
				}
			} else {
				// if we are in a lower level of the hierarchy of dirs
				// we check the top level dir instead of key.
				if IsSystem(matcher.KP.Dirs.First()) && !includeSystem {
					exclude = true
				}
			}
			if isRecord {
				if dirOnly {
					exclude = true
				}
				if strings.HasPrefix(treeValue.Value, "@") {
					exclude = excludeRedirect
				} else if excludeText && len(treeValue.Value) > 0 {
					exclude = true
				}
				if excludeEmpty && len(treeValue.Value) == 0 {
					exclude = true
				}
			}
			if !exclude {
				tree.Put(treeValue.KP.Path(), treeValue)
			}
			return nil
		})
		if err != nil {
			if strings.Contains(err.Error(), "break") {
				return nil
			}
		}
		return err
	})
	return err
}

func (b *BoltKVS) Range(from, to *KeyPath) ([]*DbR, error) {
	var recs []*DbR
	var err error
	err = b.Db.View(func(tx *bolt.Tx) error {
		bkt, err := b.Bucket(*from, tx, false)
		if err != nil {
			return err
		}
		c := bkt.Cursor()
		minKey := []byte(from.Key())
		maxKey := []byte(to.Key())

		for k, v := c.Seek(minKey); k != nil && bytes.Compare(k, maxKey) <= 0; k, v = c.Next() {
			kp := NewKeyPath()
			err = kp.ParsePath(string(k))
			if err != nil {
				return err
			}
			dbr := NewDbR()
			dbr.KP = kp
			if v != nil {
				dbr.Value = string(v)
			}
			recs = append(recs, dbr)
		}
		return nil
	})
	return recs, err
}
func (b *BoltKVS) RemoveDisallowedFromBytes(v []byte) []byte {
	s := string(v)
	for _, d := range Disallowed {
		s = strings.ReplaceAll(s, d, "")
	}
	return []byte(s)
}
func (b *BoltKVS) RemoveDisallowedFromString(v string) string {
	s := v
	for _, d := range Disallowed {
		s = strings.ReplaceAll(s, d, "")
	}
	return s
}

// writeAsLines write matching records to file as tab separated value lines
func (b *BoltKVS) writeAsLines(kp KeyPath, w *bufio.Writer) error {
	wn := norm.NFD.Writer(w)
	defer wn.Close()
	err := b.Db.View(func(tx *bolt.Tx) error {
		bkt, err := b.Bucket(kp, tx, false)
		if err != nil {
			return err
		}
		err = bkt.ForEach(func(k, v []byte) error {
			if v == nil { // the key is a bucket name
				kp.Dirs.Push(string(k))
				err = b.writeAsLines(kp, w)
				if err != nil {
					return err
				}
				kp.Dirs.Pop()
			} else {
				key := fmt.Sprintf("%s%s%s", kp.SegmentString(kp.Dirs), IdDelimiter, k)
				_, err := wn.Write([]byte(fmt.Sprintf("%s%s%s\n", key, LineDelimiter, b.RemoveDisallowedFromBytes(v))))
				if err != nil {
					return fmt.Errorf("error writing %s: %v", key, err)
				}
			}
			return nil
		})
		if err != nil {
			return err
		}
		return nil
	})
	w.Flush()
	return err
}

// writeTemplates write each matching template record as a separate file.
func (b *BoltKVS) writeTemplates(kp *KeyPath, siteDir string) error {
	if kp.Dirs.First() != "templates" {
		return fmt.Errorf("%s is not in the templates dir", kp.Path())
	}
	err := b.Db.View(func(tx *bolt.Tx) error {
		bkt, err := b.Bucket(*kp, tx, false)
		if err != nil {
			return err
		}
		bkt.ForEach(func(k, v []byte) error {
			if v == nil { // the key is a bucket name
				kp.Dirs.Push(string(k))
				b.writeTemplates(kp, siteDir)
				kp.Dirs.Pop()
			} else {
				kp.KeyParts.Push(string(k))
				path := path.Join(siteDir, kp.ToFileSystemPath())
				if !strings.HasSuffix(path, ".lml") {
					path = path + ".lml"
				}
				err = ltfile.WriteFile(path, string(v))
				kp.KeyParts.Pop()
				if err != nil {
					return fmt.Errorf("error writing %s: %v", path, err)
				}
			}
			return nil
		})
		return nil
	})
	return err
}

// writeAsJson write each matching record as a separate json file
func (b *BoltKVS) writeJson(kp *KeyPath, siteDir string) error {
	err := b.Db.View(func(tx *bolt.Tx) error {
		bkt, err := b.Bucket(*kp, tx, false)
		if err != nil {
			return err
		}
		bkt.ForEach(func(k, v []byte) error {
			if v == nil { // the key is a bucket name
				kp.Dirs.Push(string(k))
				b.writeJson(kp, siteDir)
				kp.Dirs.Pop()
			} else {
				kp.KeyParts.Push(string(k))
				path := path.Join(siteDir, kp.ToFileSystemPath())
				if !strings.HasSuffix(path, ".json") {
					path = path + ".json"
				}
				r := Record{kp.Path(), string(v)}
				var j []byte
				j, err = r.ToJson()
				if err != nil {
					kp.KeyParts.Pop()
					if err != nil {
						return fmt.Errorf("error writing %s: %v", path, err)
					}
				}
				err = ltfile.WriteFile(path, string(j))
				kp.KeyParts.Pop()
				if err != nil {
					return fmt.Errorf("error writing %s: %v", path, err)
				}
			}
			return nil
		})
		return nil
	})
	return err
}
func (b *BoltKVS) InverseKeyMap(kp *KeyPath) (*map[string][]*KeyPath, error) {
	theKp := kp.Copy()
	theMap := make(map[string][]*KeyPath)
	err := b.inverseKeyMap(theKp, &theMap)
	return &theMap, err
}
func (b *BoltKVS) inverseKeyMap(kp *KeyPath, keyMap *map[string][]*KeyPath) error {
	err := b.Db.View(func(tx *bolt.Tx) error {
		bkt, err := b.Bucket(*kp, tx, false)
		if err != nil {
			return err
		}
		err = bkt.ForEach(func(k, v []byte) error {
			kl, vl := k, v // local copies so won't be overwritten
			if vl == nil { // the key is a bucket name
				kp.Dirs.Push(string(kl))
				err = b.inverseKeyMap(kp, keyMap)
				if err != nil {
					return err
				}
				kp.Dirs.Pop()
			} else {
				key := string(kl)
				kp.KeyParts.Push(key)
				if val, exists := (*keyMap)[key]; exists {
					(*keyMap)[key] = append(val, kp.Copy())
				} else {
					(*keyMap)[key] = []*KeyPath{kp.Copy()}
				}
				kp.KeyParts.Pop()
			}
			return nil
		})
		if err != nil {
			return err
		}
		return nil
	})
	return err
}
func (b *BoltKVS) AvailableConfigs() ([]*DbR, error) {

	return nil, nil
}
