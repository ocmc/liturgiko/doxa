package kvs

import (
	"fmt"
	"os"
	"path/filepath"
	"sort"
	"testing"
)

func TestKVP_Export(t *testing.T) {
	dbPath := "/Volumes/ssd2/doxa/backups/liturgical.db"
	exportPath := "/Volumes/ssd2/doxa/exports/all.tsv"
	ds, err := NewBoltKVS(dbPath)
	if err != nil {
		t.Error(err)
		return
	}
	kvp := NewKeyPath()
	err = ds.Export(kvp, exportPath, ExportAsLine)
	if err != nil {
		t.Error(err)
		return
	}
}
func TestKeyPath(t *testing.T) {
	dbr := NewDbR()
	type Person struct {
		Name string
		DOB  string
	}
	err := dbr.KP.ParsePath("people/name:person/Name")
	if err != nil {
		return
	}
	dbr.Value = "Joe"
	// put
	err = dbr.KP.ParsePath("people/name:person/DOB")
	dbr.Value = "1/1/2000"
	// put
	kp := NewKeyPath()
	err = kp.ParsePath("people/name:person")
	kp.KeyParts.Push("*")
	// get
	// []*dbr
}
func TestKVP_ParsePath(t *testing.T) {
	rec := NewDbR()
	rec.KP.Dirs.Push("ltx")
	rec.KP.Dirs.Push("gr_gr_cog")
	rec.KP.Dirs.Push("actors")
	rec.KP.KeyParts.Push("Priest")
	rec.Value = "ΙΕΡΕΥΣ"
	expect := "ltx" + SegmentDelimiter + "gr_gr_cog" + SegmentDelimiter + "actors" + IdDelimiter + "Priest"
	got := rec.KP.Path()
	if expect != got {
		t.Errorf("kvp.Path: expected %s, got %s", expect, got)
	}
	err := rec.KP.ParsePath(got)
	if err != nil {
		t.Errorf("error parsing path: %v", err)
	}
	expect = "ltx" + SegmentDelimiter + "gr_gr_cog" + SegmentDelimiter + "actors"
	got = rec.KP.SegmentString(rec.KP.Dirs)
	if got != expect {
		t.Errorf("kvp.Prefix: expected %s, got %s", expect, got)
	}
	expect = "Priest"
	got = rec.KP.SegmentString(rec.KP.KeyParts)
	if got != expect {
		t.Errorf("kvp.Prefix: expected %s, got %s", expect, got)
	}

	rec2 := NewDbR()
	rec2.KP.Dirs.Push("ltx")
	rec2.KP.Dirs.Push("en_us_dedes")
	rec2.KP.Dirs.Push("actors")
	rec2.KP.KeyParts.Push("Priest")
	rec2.Value = "PRIEST"
	var recs []*DbR
	recs = append(recs, rec)
	recs = append(recs, rec2)
	sort.Sort(KVPSorter(recs))
	expect = "ltx" + SegmentDelimiter + "en_us_dedes" + SegmentDelimiter + "actors" + IdDelimiter + "Priest"
	got = recs[0].KP.Path()
	if got != expect {
		t.Errorf("sort.Sort(KVPSorter(kvps)): for kvsp[0].Path() expected %s, got %s", expect, got)
	}
	kp := NewKeyPath()
	kp.Dirs.Push("ltx")
	kp.Dirs.Push("gr_gr_cog")
	kp.Dirs.Push("actors")
	got = kp.Dirs.Pop()
	expect = "actors"
	if got != expect {
		t.Errorf("kp.Prefix.Pop: expected %s, got %s", expect, got)
	}

	got = kp.Dirs.Pop()
	expect = "gr_gr_cog"
	if got != expect {
		t.Errorf("kp.Prefix.Pop: expected %s, got %s", expect, got)
	}
	got = kp.Dirs.Pop()
	expect = "ltx"
	if got != expect {
		t.Errorf("kp.Prefix.Pop: expected %s, got %s", expect, got)
	}
	got = kp.Dirs.Pop()
	expect = ""
	if got != expect {
		t.Errorf("kp.Prefix.Pop: expected %s, got %s", expect, got)
	}
}

func TestDbR_SetRedirectFromValue(t *testing.T) {
	rDbr := NewDbR()
	rDbr.KP.Dirs.Push("ltx")
	rDbr.KP.Dirs.Push("en_us_dedes")
	rDbr.KP.Dirs.Push("actors")
	rDbr.KP.KeyParts.Push("Priest")

	dbr := NewDbR()
	// verify that dbr is not a redirect (yet)
	if dbr.IsRedirect() {
		t.Errorf("expected dbr.IsRedirect() == false")
	}
	// set value as a redirect
	dbr.Value = fmt.Sprintf("%s%s", RedirectsTo, rDbr.KP.Path())
	kp, err := dbr.GetRedirect()
	if err != nil {
		t.Error(err)
	}
	expect := "ltx/en_us_dedes/actors:Priest"
	got := kp.Path()
	if expect != got {
		t.Errorf("expected %s, got %s", expect, got)
	}
	if !dbr.IsRedirect() {
		t.Errorf("expected dbr.IsRedirect == true")
	}
}

func TestL(t *testing.T) {
	rec := NewDbR()
	rec.KP.Dirs.Push("ltx")
	rec.KP.Dirs.Push("gr_gr_cog")
	rec.KP.Dirs.Push("actors")
	rec.KP.KeyParts.Push("Priest")
	rec.KP.KeyParts.Push("Note")
	rec.Value = "Priest dialogs are sometimes done by a Deacon if serving."
	expect := "ltx" + SegmentDelimiter + "gr_gr_cog" + SegmentDelimiter + "actors" + IdDelimiter + "Priest" + SegmentDelimiter + "Note"
	got := rec.KP.Path()
	if expect != got {
		t.Errorf("kvp.Path: expected %s, got %s", expect, got)
	}
	err := rec.KP.ParsePath(got)
	if err != nil {
		t.Errorf("error parsing path: %v", err)
	}
	expect = "ltx" + SegmentDelimiter + "gr_gr_cog" + SegmentDelimiter + "actors" + IdDelimiter + "Priest" + SegmentDelimiter + "Note"
	got = rec.KP.SegmentString(rec.KP.Dirs) + IdDelimiter + rec.KP.SegmentString(rec.KP.KeyParts)
	if got != expect {
		t.Errorf("kvp.Prefix: expected %s, got %s", expect, got)
	}

}
func TestMatcher_UseAsRegEx(t *testing.T) {
	m := NewMatcher()
	m.KP.Dirs.Push("ltx")
	m.KP.Dirs.Push("en_us_dedes")
	m.ValuePattern = m.KP.Path() + ".*actors.*"
	m.UseAsRegEx()
	dbr := NewDbR()
	dbr.KP.Dirs.Push("ltx")
	dbr.KP.Dirs.Push("en_us_dedes")
	dbr.KP.Dirs.Push("actors")
	dbr.KP.KeyParts.Push("Priest")
	if !m.ValueRegEx.Match([]byte(dbr.KP.Path())) {
		t.Errorf("%s does not match %s", dbr.KP.Path(), m.ValueRegEx)
	}
	m = NewMatcher()
	m.KP.Dirs.Push("ltx")
	m.KP.Dirs.Push("en_us_dedes")
	m.KP.Dirs.Push("actors")
	m.ValuePattern = m.KP.Path() + ".*Priest.*"
	m.UseAsRegEx()
	if !m.ValueRegEx.Match([]byte(dbr.KP.Path())) {
		t.Errorf("%s does not match %s", dbr.KP.Path(), m.ValueRegEx)
	}
}

func TestBoltKVS_GetMatchingIDs(t *testing.T) {
	tsv := `
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C11.greekmelody	@ltx/gr_gr_cog/he.h.m2:heHE.VythouAnekalypse.greekname
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C11.incipit	Ἀδὰμ τὸν φθαρέντα…
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C11.melody	@ltx/gr_gr_cog/he.h.m2:heHE.VythouAnekalypse.name
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C11.mode	@ltx/gr_gr_cog/miscellanea:misc.Mode2
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C11.ode	@ltx/gr_gr_cog/miscellanea:misc.Ode1
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C11.text	Ἀδὰμ τὸν φθαρέντα ἀναπλάττει, ῥείθροις Ἰορδάνου καὶ δρακόντων, κεφαλὰς ἐμφωλευόντων διαθλάττει, ὁ Βασιλεὺς τῶν αἰώνων Κύριος· ὅτι δεδόξασται.
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C12.incipit	Πυρὶ τῆς Θεότητος…
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C12.melody	@ltx/gr_gr_cog/he.h.m2:heHE.VythouAnekalypse.name
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C12.mode	@ltx/gr_gr_cog/miscellanea:misc.Mode2
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C12.ode	@ltx/gr_gr_cog/miscellanea:misc.Ode1
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C12.text	Πυρὶ τῆς Θεότητος ἀΰλῳ, σάρκα ὑλικὴν ἠμφιεσμένος, Ἰορδάνου περιβάλλεται τὸ νᾶμα, ὁ σαρκωθεὶς ἐκ Παρθένου Κύριος· ὅτι δεδόξασται.
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C13.incipit	Τὸν ῥύπον ὁ σμήχων…
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C13.melody	@ltx/gr_gr_cog/he.h.m2:heHE.VythouAnekalypse.name
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C13.mode	@ltx/gr_gr_cog/miscellanea:misc.Mode2
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C13.ode	@ltx/gr_gr_cog/miscellanea:misc.Ode1
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C13.text	Τὸν ῥύπον ὁ σμήχων τῶν ἀνθρώπων, τούτοις καθαρθεὶς ἐν Ἰορδάνῃ, οἷς θελήσας ὡμοιώθη ὃ ἦν μείνας, τοὺς ἐν σκότει φωτίζει Κύριος· ὅτι δεδόξασται.
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C1H.greekmelody	@ltx/gr_gr_cog/he.h.m2:heHE.VythouAnekalypse.greekname
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C1H.incipit	@ltx/gr_gr_cog/he.h.m2:heHE.VythouAnekalypse.incipit
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C1H.melody	@ltx/gr_gr_cog/he.h.m2:heHE.VythouAnekalypse.melody
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C1H.mode	@ltx/gr_gr_cog/miscellanea:misc.Mode2
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C1H.ode	@ltx/gr_gr_cog/miscellanea:misc.Ode1
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C1H.text	@ltx/gr_gr_cog/he.h.m2:heHE.VythouAnekalypse.text
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C21.greekmelody	@ltx/gr_gr_cog/he.h.m2:heHE.SteiveiThalassis.greekname
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C21.incipit	Ὄρθρου φανέντος…
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C21.melody	@ltx/gr_gr_cog/he.h.m2:heHE.SteiveiThalassis.name
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C21.mode	@ltx/gr_gr_cog/miscellanea:misc.Mode2
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C21.ode	@ltx/gr_gr_cog/miscellanea:misc.Ode1
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C21.text	Ὄρθρου φανέντος, τοῖς βροτοῖς σελασφόρου, * Νῦν ἐξ ἐρήμου, πρὸς ῥοὰς Ἰορδάνου. * Ἄναξ ὑπέσχες, ἡλίου σὸν αὐχένα, * Χώρου ζοφώδους, τὸν Γενάρχην ἁρπάσαι, * Ῥύπου τε παντός, ἐκκαθᾶραι τὴν κτίσιν.
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C211.text	Ὄρθρου φανέντος, τοῖς βροτοῖς σελασφόρου,
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C211a.text	Ὄ
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C211b.text	ρθρου φανέντος, τοῖς βροτοῖς σελασφόρου,
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C212.text	Νῦν ἐξ ἐρήμου, πρὸς ῥοὰς Ἰορδάνου,
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C212a.text	Ν
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C212b.text	ῦν ἐξ ἐρήμου, πρὸς ῥοὰς Ἰορδάνου,
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C213.text	Ἄναξ ὑπέσχες, ἡλίου σὸν αὐχένα,
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C213a.text	Ἄ
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C213b.text	ναξ ὑπέσχες, ἡλίου σὸν αὐχένα,
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C214.text	Χώρου ζοφώδους, τὸν Γενάρχην ἁρπάσαι,
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C214a.text	Χ
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C214b.text	ώρου ζοφώδους, τὸν Γενάρχην ἁρπάσαι,
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C215.text	Ῥύπου τε παντός, ἐκκαθᾶραι τὴν κτίσιν.
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C215a.text	Ῥ
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C215b.text	ύπου τε παντός, ἐκκαθᾶραι τὴν κτίσιν.
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C22.incipit	Ἄναρχε ῥείθροις…
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C22.melody	@ltx/gr_gr_cog/he.h.m2:heHE.SteiveiThalassis.name
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C22.mode	@ltx/gr_gr_cog/miscellanea:misc.Mode2
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C22.ode	@ltx/gr_gr_cog/miscellanea:misc.Ode1
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C22.text	Ἄναρχε ῥείθροις, συνταφέντα σοι Λόγε, * Νέον περαίνεις, τὸν φθαρέντα τῇ πλάνῃ, * Ταύτην ἀφράστως, πατρόθεν δεδεγμένος, * Ὄπα κρατίστην· Οὗτος ἠγαπημένος, * Ἴσος τέ μοι Παῖς, χρηματίζει τὴν φύσιν.
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C221.text	Ἄναρχε ῥείθροις, συνταφέντα σοι Λόγε,
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C221a.text	Ἄ
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C221b.text	ναρχε ῥείθροις, συνταφέντα σοι Λόγε,
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C222.text	Νέον περαίνεις, τὸν φθαρέντα τῇ πλάνῃ,
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C222a.text	Ν
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C222b.text	έον περαίνεις, τὸν φθαρέντα τῇ πλάνῃ,
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C223.text	Ταύτην ἀφράστως, πατρόθεν δεδεγμένος,
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C223a.text	Τ
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C223b.text	αύτην ἀφράστως, πατρόθεν δεδεγμένος,
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C224.text	Ὄπα κρατίστην· Οὗτος ἠγαπημένος,
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C224a.text	Ὄ
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C224b.text	πα κρατίστην· Οὗτος ἠγαπημένος,
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C225.text	Ἴσος τέ μοι Παῖς, χρηματίζει τὴν φύσιν.
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C225a.text	Ἴ
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C225b.text	σος τέ μοι Παῖς, χρηματίζει τὴν φύσιν.
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C2H.greekmelody	@ltx/gr_gr_cog/he.h.m2:heHE.SteiveiThalassis.greekname
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C2H.incipit	@ltx/gr_gr_cog/he.h.m2:heHE.SteiveiThalassis.incipit
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C2H.melody	@ltx/gr_gr_cog/he.h.m2:heHE.SteiveiThalassis.melody
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C2H.mode	@ltx/gr_gr_cog/miscellanea:misc.Mode2
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C2H.ode	@ltx/gr_gr_cog/miscellanea:misc.Ode1
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C2H.text	@ltx/gr_gr_cog/he.h.m2:heHE.SteiveiThalassis.text
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C2H1.text	@ltx/gr_gr_cog/he.h.m2:heHE.SteiveiThalassis1.text
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C2H1a.text	@ltx/gr_gr_cog/he.h.m2:heHE.SteiveiThalassis1a.text
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C2H1b.text	@ltx/gr_gr_cog/he.h.m2:heHE.SteiveiThalassis1b.text
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C2H2.text	@ltx/gr_gr_cog/he.h.m2:heHE.SteiveiThalassis2.text
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C2H2a.text	@ltx/gr_gr_cog/he.h.m2:heHE.SteiveiThalassis2a.text
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C2H2b.text	@ltx/gr_gr_cog/he.h.m2:heHE.SteiveiThalassis2b.text
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C2H3.text	@ltx/gr_gr_cog/he.h.m2:heHE.SteiveiThalassis3.text
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C2H3a.text	@ltx/gr_gr_cog/he.h.m2:heHE.SteiveiThalassis3a.text
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C2H3b.text	@ltx/gr_gr_cog/he.h.m2:heHE.SteiveiThalassis3b.text
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C2H4.text	@ltx/gr_gr_cog/he.h.m2:heHE.SteiveiThalassis4.text
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C2H4a.text	@ltx/gr_gr_cog/he.h.m2:heHE.SteiveiThalassis4a.text
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C2H4b.text	@ltx/gr_gr_cog/he.h.m2:heHE.SteiveiThalassis4b.text
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C2H5.text	@ltx/gr_gr_cog/he.h.m2:heHE.SteiveiThalassis5.text
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C2H5a.text	@ltx/gr_gr_cog/he.h.m2:heHE.SteiveiThalassis5a.text
ltx/gr_gr_cog/me.m01.d06:meMA.Ode1C2H5b.text	@ltx/gr_gr_cog/he.h.m2:heHE.SteiveiThalassis5b.text
ltx/gr_gr_cog/me.m01.d06:meMA.Ode3C11.greekmelody	@ltx/gr_gr_cog/he.h.m2:heHE.IschynODidous.greekname
ltx/gr_gr_cog/me.m01.d06:meMA.Ode3C11.incipit	Στειρεύουσα πρίν…
ltx/gr_gr_cog/me.m01.d06:meMA.Ode3C11.melody	@ltx/gr_gr_cog/he.h.m2:heHE.IschynODidous.name
ltx/gr_gr_cog/me.m01.d06:meMA.Ode3C11.mode	@ltx/gr_gr_cog/miscellanea:misc.Mode2
ltx/gr_gr_cog/me.m01.d06:meMA.Ode3C11.ode	@ltx/gr_gr_cog/miscellanea:misc.Ode3
ltx/gr_gr_cog/me.m01.d06:meMA.Ode3C11.text	Στειρεύουσα πρίν, ἠτεκνωμένη δεινῶς σήμερον, εὐφραίνου Χριστοῦ ἡ Ἐκκλησία· διʼ ὕδατος καὶ Πνεύματος, υἱοὶ γὰρ σοι γεγέννηνται, ἐν πίστει ἀνακράζοντες· Οὐκ ἔστιν ἅγιος, ὡς ὁ Θεὸς ἡμῶν, καὶ οὐκ ἔστι δίκαιος, πλήν σου Κύριε.
ltx/gr_gr_cog/le.ep.me.m01.d06:lemeGW.Epistle.book_abbr	@ltx/gr_gr_cog/bible:Corinthians1.abbr
ltx/gr_gr_cog/le.ep.me.m01.d06:lemeGW.Epistle.book_string	@ltx/gr_gr_cog/bible:Corinthians1.text
ltx/gr_gr_cog/le.ep.me.m01.d06:lemeGW.Epistle.chapverse	10:1 – 4
ltx/gr_gr_cog/le.ep.me.m01.d06:lemeGW.Epistle.incipit	Οὐ θέλω ὑμᾶς ἀγνοεῖν, ὅτι οἱ πατέρες ἡμῶν πάντες…
ltx/gr_gr_cog/le.ep.me.m01.d06:lemeGW.Epistle.text	Ἀδελφοί, οὐ θέλω ὑμᾶς ἀγνοεῖν, ὅτι οἱ πατέρες ἡμῶν πάντες ὑπὸ τὴν νεφέλην ἦσαν, καὶ πάντες διὰ τῆς θαλάσσης διῆλθον, καὶ πάντες εἰς τὸν Μωϋσῆν ἐβαπτίσαντο ἐν τῇ νεφέλῃ καὶ ἐν τῇ θαλάσσῃ, καὶ πάντες τὸ αὐτὸ βρῶμα πνευματικὸν ἔφαγον, καὶ πάντες τὸ αὐτὸ πόμα πνευματικὸν ἔπιον· ἔπινον γὰρ ἐκ πνευματικῆς ἀκολουθούσης πέτρας, ἡ δὲ πέτρα ἦν ὁ Χριστός.
ltx/gr_gr_cog/le.ep.me.m01.d06:lemeGW.Epistle.title	Εἰς τὸν Ἁγιασμόν.
ltx/gr_gr_cog/le.ep.me.m01.d06:lemeGW.Epistle.title_abbr	Εἰς τὸν Ἁγιασμόν
ltx/gr_gr_cog/le.ep.me.m01.d06:lemeGW.Epistle.version	@ltx/gr_gr_cog/properties:version.designation
ltx/gr_gr_cog/le.ep.me.m01.d06:lemeLI.Epistle.book_abbr	@ltx/gr_gr_cog/le.ep.me.m01.d05:lemeH9.Epistle.book_abbr
ltx/gr_gr_cog/le.ep.me.m01.d06:lemeLI.Epistle.book_string	@ltx/gr_gr_cog/le.ep.me.m01.d05:lemeH9.Epistle.book_string
ltx/gr_gr_cog/le.ep.me.m01.d06:lemeLI.Epistle.chapverse	@ltx/gr_gr_cog/le.ep.me.m01.d05:lemeH9.Epistle.chapverse
ltx/gr_gr_cog/le.ep.me.m01.d06:lemeLI.Epistle.incipit	@ltx/gr_gr_cog/le.ep.me.m01.d05:lemeH9.Epistle.incipit
ltx/gr_gr_cog/le.ep.me.m01.d06:lemeLI.Epistle.text	@ltx/gr_gr_cog/le.ep.me.m01.d05:lemeH9.Epistle.text
ltx/gr_gr_cog/le.ep.me.m01.d06:lemeLI.Epistle.title	Τῆς Ἑορτῆς
ltx/gr_gr_cog/le.ep.me.m01.d06:lemeLI.Epistle.title_abbr	Τῆς Ἑορτῆς
ltx/gr_gr_cog/le.ep.me.m01.d06:lemeLI.Epistle.version	@ltx/gr_gr_cog/le.ep.me.m01.d05:lemeH9.Epistle.version
ltx/gr_gr_cog/le.ep.me.m01.d06:lemeVE.Epistle.book_abbr	@ltx/gr_gr_cog/bible:Corinthians1.abbr
ltx/gr_gr_cog/le.ep.me.m01.d06:lemeVE.Epistle.book_string	@ltx/gr_gr_cog/bible:Corinthians1.text
ltx/gr_gr_cog/le.ep.me.m01.d06:lemeVE.Epistle.chapverse	9:19 – 27
ltx/gr_gr_cog/le.ep.me.m01.d06:lemeVE.Epistle.incipit	Ἐλεύθερος ὢν ἐκ πάντων πᾶσιν ἐμαυτὸν ἐδούλωσα…
ltx/gr_gr_cog/le.ep.me.m01.d06:lemeVE.Epistle.text	Ἀδελφοί, ἐλεύθερος ὢν ἐκ πάντων πᾶσιν ἐμαυτὸν ἐδούλωσα, ἵνα τοὺς πλείονας κερδήσω· καὶ ἐγενόμην τοῖς Ἰουδαίοις ὡς Ἰουδαῖος, ἵνα Ἰουδαίους κερδήσω· τοῖς ὑπὸ νόμον ὡς ὑπὸ νόμον, ἵνα τοὺς ὑπὸ νόμον κερδήσω· τοῖς ἀνόμοις ὡς ἄνομος, μὴ ὢν ἄνομος Θεῷ, ἀλλ᾿ ἔννομος Χριστῷ, ἵνα κερδήσω ἀνόμους· ἐγενόμην τοῖς ἀσθενέσιν ὡς ἀσθενής, ἵνα τοὺς ἀσθενεῖς κερδήσω· τοῖς πᾶσι γέγονα τὰ πάντα, ἵνα πάντως τινὰς σώσω. Τοῦτο δὲ ποιῶ διὰ τὸ εὐαγγέλιον, ἵνα συγκοινωνὸς αὐτοῦ γένωμαι. Οὐκ οἴδατε ὅτι οἱ ἐν σταδίῳ τρέχοντες πάντες μὲν τρέχουσιν, εἷς δὲ λαμβάνει τὸ βραβεῖον; οὕτω τρέχετε, ἵνα καταλάβητε. πᾶς δὲ ὁ ἀγωνιζόμενος πάντα ἐγκρατεύεται, ἐκεῖνοι μὲν οὖν ἵνα φθαρτὸν στέφανον λάβωσιν, ἡμεῖς δὲ ἄφθαρτον. ἐγὼ τοίνυν οὕτω τρέχω, ὡς οὐκ ἀδήλως, οὕτω πυκτεύω, ὡς οὐκ ἀέρα δέρων, ἀλλ᾿ ὑποπιάζω μου τὸ σῶμα καὶ δουλαγωγῶ, μήπως ἄλλοις κηρύξας αὐτὸς ἀδόκιμος γένωμαι.
ltx/gr_gr_cog/le.ep.me.m01.d06:lemeVE.Epistle.title	Τῆς Ἑορτῆς.
ltx/gr_gr_cog/le.ep.me.m01.d06:lemeVE.Epistle.title_abbr	Τῆς Ἑορτῆς
ltx/gr_gr_cog/le.ep.me.m01.d06:lemeVE.Epistle.version	@ltx/gr_gr_cog/properties:version.designation
`
	expectedRecCnt := 114 // update if you +/- from tsv above...
	// Create temporary directory that will be automatically cleaned up
	tmpDir := t.TempDir()

	// Create the data file path
	dataFile := filepath.Join(tmpDir, "data", "data.tsv")

	// Create data subdirectory
	dataDir := filepath.Join(tmpDir, "data")
	if err := os.Mkdir(dataDir, 0755); err != nil {
		t.Fatalf("failed to create data directory: %v", err)
	}

	// Create and write to the file
	f, err := os.Create(dataFile)
	if err != nil {
		t.Fatalf("failed to create test file: %v", err)
	}
	defer f.Close() // Write test data
	testData := []byte(tsv)
	if _, err = f.Write(testData); err != nil {
		t.Fatalf("failed to write test data: %v", err)
	}

	// Close the file
	if err = f.Close(); err != nil {
		t.Fatalf("failed to close test file: %v", err)
	}
	// Create the data file path
	dbPath := filepath.Join(tmpDir, "liturgical.db")
	importPath := filepath.Join(tmpDir, "data", "data.tsv")
	ds, err := NewBoltKVS(dbPath)
	if err != nil {
		t.Error(err)
		return
	}
	if ds == nil {
		t.Error("ds is nil")
		return
	}
	defer ds.Close()
	// Import a text file that has each line as a record.
	// In each line, the dirs, key, and value are separated by the delimiter.
	// If dirsPrefix is not empty, it is used to exclude lines whose dirs do not have that prefix.
	// If the validator is not null, it is used to check each line for validity.
	// If removeQuotes is true, the import file has quotes around every value that must be removed.
	// If allOrNone is true, records will only be written if all lines are error free.
	// If skipObsoleteConfigs is true, obsolete configuration records will be ignored and skipped.  Otherwise, an error will be returned.
	// Import(dirsPrefix, filepath string, lineParser LineParser, removeQuotes, allOrNone, skipObsoleteConfigs bool) (int, []error)
	recCount := 0
	var errs []error
	recCount, errs = ds.Import("", importPath, nil, false, false, true)
	if len(errs) != 0 {
		t.Errorf("Import() returned %d errors, expected 0", len(errs))
		t.Errorf("%v", errs[0])
		return
	}
	if recCount != expectedRecCnt {
		t.Errorf("Import() returned %d records, expected %d", recCount, expectedRecCnt)
		return
	}
	// now the database is ready for use.
	type testCase struct {
		pattern     string
		expectedCnt int
	}
	testCases := []testCase{
		{pattern: "me.*", expectedCnt: 90},
		{pattern: "me.m01.*", expectedCnt: 90},
		{pattern: "me.m01.d06.*", expectedCnt: 90},
		{pattern: "me.m[0-9]+.*", expectedCnt: 90},
		{pattern: "me.m[0-9]+.d[0-9]+.*", expectedCnt: 90},
		{pattern: "me.m[0-9]+.d[0-9]+.chapverse.*", expectedCnt: 90},
	}
	path := "ltx/gr_gr_cog"
	for i, tc := range testCases {
		// make a local copy of values
		pattern := tc.pattern
		expected := tc.expectedCnt
		// create a new matcher
		matcher := NewMatcher()
		// set bool values for matcher
		matcher.IdSearch = true
		matcher.Recursive = true
		matcher.NNP = false
		matcher.NFD = false
		// set the path
		matcher.KP.ParsePath(path)
		// set the value pattern
		matcher.ValuePattern = pattern
		// convert to a regEx matcher
		err = matcher.UseAsRegEx()
		if err != nil {
			t.Errorf("tc %d: %v", i, err)
		}
		var dbRecs []*DbR
		matchedRecCnt := 0
		dbRecs, err = ds.GetMatchingIDs(*matcher)
		matchedRecCnt = len(dbRecs)
		if matchedRecCnt != expected {
			t.Errorf("GetMatchingIDs() for %d pattern %s returned %d records, expected %d", i, pattern, matchedRecCnt, expected)
		}
	}
}
