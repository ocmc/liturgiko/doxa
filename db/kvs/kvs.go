// Package kvs provides an interface with methods for implementors of a key-value (data)store.
//
//	Implementations are also in this package, e.g. boltdb.go
package kvs

import (
	"encoding/json"
	"fmt"
	"github.com/emirpasic/gods/trees/btree"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"github.com/liturgiko/doxa/pkg/utils/stack"
	"github.com/spf13/cast"
	"golang.org/x/text/unicode/norm"
	"path"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

/*
  The following characters are reserved in Regular Expressions,
  so don't use them as the value for the IdDelimiter or SegmentDelimiter:
  \ ^ $ . | ? * + ( _ { } [ ]

  Also do not use as a delimiter anything in the const Redirect.
*/

const IdDelimiter = ":"      // delimits ID, which is the Dirs + the KeyParts
const SegmentDelimiter = "/" // delimits the internal segments of the Dirs and KeyParts

// RedirectsTo marks a value as a redirect, i.e. it is the ID of another record.
// Current the RedirectsTo values is the @ symbol.
// A value that starts with @ should be understood to mean that
// the values is at such-and-such an ID.
const RedirectsTo = "@"

// WordBoundary works with both ascii and unicode.
// \b works for ascii, but not unicode. If it does not match,
// the 'or-ed' patterns are tried:
// ^ beginning of string
// [^\p{L}^\p{M}] any character that is not a unicode letter or mark (diacritic)
// $ end of string
// Problem: consider the following sentences:
//
//	S1. "That cafe over there, not this cafe."
//	S2. "That café over there, not this café."
//
// And these two patterns:
//
//	P1. \bcafe\b
//	P2. \bcafé\b
//	P3. (?:\b|^|[^\p{L}^\p{M}]|$)cafe(?:\b|^|[^\p{L}^\p{M}]|$)
//	P4. (?:\b|^|[^\p{L}^\p{M}]|$)café(?:\b|^|[^\p{L}^\p{M}]|$)
//
// Matches:
//
//	P1 will match two words in S1
//	P1 will not match any word in S2
//	P2 will not match any word in either S2 or S2
//	P3 will match two words in S1, but also the surrounding elements: " cafe " and " cafe."
//	P4 will match two words in S2, but same problem: " café " and " café."
//
// Including the word boundary characters in the match
// is not a problem if the user is just searching.
// If they use this for replace as well, it could be an issue.
// They will need to consider the resulting groups.
// The WordBoundary is a capturing group.
// Making it a non-capturing group, e.g (?:\b|^|[^\p{L}^\p{M}]|$)
// does not solve the search and replace issue.
const WordBoundary = `(\b|^|[^\p{L}^\p{M}]|$)`

//const WordBoundary = `[ |\.|;|?|!|"|'|«|»|:|˙|$|^|\(|\)]`

// LineDelimiter is the character used delimit the fields in a line of record for export or import.
const LineDelimiter = "\t"

// FileExtension is the extension given to an export or import file.
// The extension should reflect the type of separator (delimiter) for the fields (value).
const FileExtension = "tsv" // tab separated value

var Disallowed = []string{"\b", "\f", "\n", "\r", "\v"}

type ExportDelimiter string

const (
	TabDelimiter   ExportDelimiter = "\t"
	EqualDelimiter ExportDelimiter = "="
)

type ExportType int

const (
	ExportAsLine ExportType = 0
	ExportAsJson ExportType = 1
)

type Store struct {
	Name        string
	Description string
	Path        string
	Prompt      string
	Kvs         *KVS
}
type Stores struct {
	Map map[string]*Store
}

func NewStores() *Stores {
	s := new(Stores)
	s.Map = make(map[string]*Store)
	return s
}
func (s *Stores) Add(store *Store) {
	s.Map[store.Name] = store
}
func (s *Stores) Get(name string) *Store {
	return s.Map[name]
}
func (s *Stores) Remove(name string) {
	delete(s.Map, name)
}
func (s *Stores) RemoveAll() {
	s.Map = make(map[string]*Store)
}

// Slice returns an unordered slice.
// After getting it, you can sort it as follows:
//
//	sort.Slice(slice, func(i, j int) bool {
//	 	return slice[i].Name < slice[i].Name
//	})
//
// Or any other Store property you want
func (s *Stores) Slice() (slice []*Store) {
	slice = make([]*Store, 0, len(s.Map))
	for _, v := range s.Map {
		slice = append(slice, v)
	}
	return slice
}
func (s *Stores) SliceSortedByName() (slice []*Store) {
	slice = s.Slice()
	sort.Sort(ByName(slice))
	return slice
}
func (s *Stores) SliceSortedByPrompt() (slice []*Store) {
	slice = s.Slice()
	sort.Sort(ByPrompt(slice))
	return slice
}

// ByName is a slice of *Store that implements sort.Interface
type ByName []*Store

func (s ByName) Len() int {
	return len(s)
}

func (s ByName) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s ByName) Less(i, j int) bool {
	return strings.ToLower(s[i].Name) < strings.ToLower(s[j].Name)
}

// ByPrompt is a slice of *Store that implements sort.Interface
type ByPrompt []*Store

func (s ByPrompt) Len() int {
	return len(s)
}

func (s ByPrompt) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s ByPrompt) Less(i, j int) bool {
	return strings.ToLower(s[i].Prompt) < strings.ToLower(s[j].Prompt)
}

// Record is used for serialization to json
type Record struct {
	ID    string
	Value string
}

func (r *Record) ToJson() ([]byte, error) {
	return json.Marshal(r)
}

// KVS holds a Key-Value (Data) Db with methods implementing interface KVI
type KVS struct {
	Db KVI
}

// NewKVS returns a new Key-Value (Data)Store
func NewKVS(db KVI) (*KVS, error) {
	if db == nil {
		return nil, fmt.Errorf("error: database is nil")
	}
	m := new(KVS)
	m.Db = db
	return m, nil
}

// KVI defines the functions required for implementers of a key-value data store.
// Be sure to read the documentation for models.DbR, including the KVPSorter.
// Implementations must convert all text to Unicode Normal Form C.
// Specifically in the implementation of Batch and Put.
type KVI interface {
	// Backup the data store to the specified file system path.  String returns with path to file (includes filename)
	Backup(path string) (string, error)
	// Batch put supplied records
	Batch(values []*DbR) error
	// Close the datastore
	Close() error
	// Copy from source to destination.
	// Depending on how KeyPath is configured, multiple records could be copied.
	Copy(from, to KeyPath) error
	// Delete per the KeyPath.
	// Depending on how KeyPath is configured, multiple records could be deleted.
	Delete(kp KeyPath) error
	// DirNames gets the names of the child directories of the parent specified in the KeyPath
	DirNames(kp KeyPath) ([]string, error)
	// DirPaths gets the full path to the child directories of the parent specified in the KeyPath
	DirPaths(kp KeyPath) ([]*KeyPath, error)
	// Exists checks to see if the record is in the database
	Exists(kp *KeyPath) bool
	// ExistsMatching uses a Matcher to determine if any bucket or path matches.
	ExistsMatching(matcher Matcher) (bool, error)
	// Export datastore to a text file as tab delimited key-value lines, starting at the indicated path. If path is unpopulated, starts at root. returns err and count of recs exported. Export type options are kvs.ExportAsJson, kvs.ExportAsLine
	Export(kp *KeyPath, pathOut string, exportType ExportType) error
	// Get a specific record by key
	Get(kp *KeyPath) (*DbR, error)
	// GetMatching records and a count of all occurrences, which
	// can be more than the number of records matching, since a
	// record value can have more than one occurrence of a value pattern.
	GetMatching(matcher Matcher) ([]*DbR, int, error)
	// GetMatchingIDs returns records with a matching ID
	GetMatchingIDs(matcher Matcher) ([]*DbR, error)
	// GetResolved gets a specific record by key, and if the value is a redirect, resolve it.
	// The returned string slice will provide all the redirects encountered
	GetResolved(kp *KeyPath) (*DbR, []string, error)
	// GetRedirectsMapper provides a two-way map between records redirected to and redirected from
	GetRedirectsMapper(kp *KeyPath) (*RedirectMapper, error)
	// GetRedirectsToDir returns the records that redirect to records in this directory
	GetRedirectsToDir(kp *KeyPath) ([]*DbR, error)
	// GetRedirectsToRecord returns the records that use this record's ID as a redirect
	GetRedirectsToRecord(kp *KeyPath) ([]*DbR, error)
	// IsRedirect returns true if the value of the record is a redirect ID
	IsRedirect(kp *KeyPath) (bool, *DbR, error)
	// Keys returns the KeyPaths for directories and records (non-recursively)
	// for the dir specified in KeyPath.
	// The first int is the count of keys that are for buckets.
	// The second int is the count of keys that are for records.
	// Note: this is not recursive.  Use RecursiveKeys to recursively read down the directories
	Keys(kp *KeyPath) ([]*KeyPath, int, int, error)
	// Import a text file that has each line as a record.
	// In each line, the dirs, key, and value are separated by the delimiter.
	// If dirsPrefix is not empty, it is used to exclude lines whose dirs do not have that prefix.
	// If the validator is not null, it is used to check each line for validity.
	// If removeQuotes is true, the import file has quotes around every value that must be removed.
	// If allOrNone is true, records will only be written if all lines are error free.
	// If skipObsoleteConfigs is true, obsolete configuration records will be ignored and skipped.  Otherwise, an error will be returned.
	Import(dirsPrefix, filepath string, lineParser LineParser, removeQuotes, allOrNone, skipObsoleteConfigs bool) (int, []error)
	// InverseKeyMap provides a means to get the path of records matching a particular key
	InverseKeyMap(kp *KeyPath) (*map[string][]*KeyPath, error)
	// MakeDir creates the specified directories (one or more).
	// If the implementing database does not support the notion of buckets,
	// returns an error.
	MakeDir(kp *KeyPath) error
	// MakeRedirects reads all records matching the "from" KeyPath
	// and writes the record paths as redirect records in the "to" KeyPath.
	// If either the "from" or "to" do not exist in the DB, an error is returned.
	// If any other func is called that returns a non-nil error, it is returned.
	MakeRedirects(fromKp, toKp *KeyPath) error
	// Move one or more records, depending on the values in KeyPath
	Move(from, to KeyPath) error
	// Open a datastore
	Open(filename string) error
	Put(kvp *DbR) error
	// Range returns records ranging from KeyPath to KeyPath
	Range(from, to *KeyPath) ([]*DbR, error)
	// Tree returns a tree representation of the database directories and records
	// dirOnly when true ignores records
	// excludeRedirect when true excludes records whose value is a redirect
	// excludeEmpty when true excludes records whose value is empty
	// excludeText when true excludes records whose value is not empty and not a redirect
	// depth controls how deep the tree will be, -1 all, 0 current dir, 1..n 1 level...n levels
	// width controls how many characters to return from value: -1 all, 0 none, 1...n 1 to n chars
	// Note that btree.Tree node key and value are type interface{}.
	// The key should be set to Dbr.KP.Path() and the value to DbR.
	Tree(kp *KeyPath, dirOnly, includeSystem, excludeRedirect, excludeEmpty, excludeText bool, depth, width int) (*btree.Tree, error)
}

// DbR (Database Record) holds the key and value for a record
// stored in a key-value database.
// See the documentation below for type KeyPath.
// Be sure to get an instance using NewDbR().
// This will also create an instance of KeyPath and assign it to
// the DbR.KP.  If you create a DbR using new(DbR), DbR.KP will be nil
// unless you create an instance an assign it to DbR.KP.
//
// If a resolving method was called to GetKeyPath the record, e.g. GetResolved(),
// and the Value is a redirect key, it will be converted into a KeyPath and stored
// in the Redirect field.  The Redirect record's value will be used to
// populate the Value field.
//
// If you are saving a record whose value should be a redirect, do this:
//
//	rDbr := NewDbR()
//
// push whatever values are needed for the redirect, e.g.
//
//	rDbr.KP.Dirs.Push("ltx")
//	rDbr.KP.Dirs.Push("en_us_dedes")
//	rDbr.KP.Dirs.Push("actors")
//	rDbr.KP.KeyParts.Push("Priest")
//
//	dbr := NewDbR()
//	// set value as a redirect
//	dbr.Value = fmt.Sprintf("%s%s", RedirectsTo, rDbr.KP.Path())
//
// If you want GetKeyPath a record whose value holds the resolved value:
//
//	kp, err := rec.GetRedirect() // converts the redirect value to a *KeyPath
//	dbr, err := GetResolved(kp) // returns a *DbR whose value holds the resolved value
//	                            // and Redirect holds the KeyPath to the redirect
type DbR struct {
	KP *KeyPath
	// Value normally holds a text value or a redirect path
	// but will hold the resolved value when retrieving a resolved redirect
	Value string
	// Redirect is only populated when retrieving a resolved redirect
	Redirect   *KeyPath
	Expression string  // if the record was obtained by a regEx, holds the regex expression
	Indexes    [][]int // holds the indexes for all occurrences that matched the expression
}

func NewDbR() *DbR {
	kvp := new(DbR)
	kvp.KP = NewKeyPath()
	return kvp
}

// GetRedirect parses the value into a KeyPath, if it is a redirect.
func (d *DbR) GetRedirect() (*KeyPath, error) {
	if !d.IsRedirect() {
		return nil, fmt.Errorf("not a redirect: %s", d.Value)
	}
	kp := NewKeyPath()
	err := kp.ParsePath(d.Value[len(RedirectsTo):])
	if err != nil {
		return nil, fmt.Errorf("invalid syntax for redirect path %s: %v", d.Value, err)
	}
	return kp, nil
}
func (d *DbR) IsRedirect() bool {
	return d.Redirect != nil || strings.HasPrefix(d.Value, RedirectsTo)
}

// SetRedirectFromValue sets the Redirects field from the Value field of the DbR  if it is an ID starting with an @ sign. e.g. @ltx/en_us_dedes/actors:Priest
func (d *DbR) SetRedirectFromValue() error {
	kp, err := d.GetRedirect()
	if err != nil {
		return err
	}
	d.Redirect = kp
	return nil
}

// KeyPath represents the Directory path and KeyParts for a record.
// Important: do not create an instance by new(KeyPath).  Do this:
//
//	kvs.NewKeyPath()
//
// When using a key-value datastore, how do we represent tables, table columns, and foreign keys?
// See https://medium.com/@wishmithasmendis/from-rdbms-to-key-value-store-data-modeling-techniques-a2874906bc46
// Analogous to the above article, for Doxa, the Dirs are the prefix and the KeyParts are the Identifier.
// Suffixes were not implemented, because the KeyParts satisfies our needs for Doxa.
// If, like BoltDB, the database supports the notion of buckets, the Dirs elements
// are used as the bucket name(s).  The KeyParts are the unique identifier of the record.
// It must be unique with the final bucket of the Dirs.
// If the implementing key-value database does not support the concept of buckets,
// then the string from Path() can be used as the entire key.
//
// Normally the len(KeyParts) will be 1.  But, it can be appended in order to capture attributes
// or relationships between records, e.g. KeyParts == I3244:C133:amount could mean
//
//	the amount owed by customer 133 for invoice 3244.
//
// The Dirs and KeyParts are of type stack.StringStack, and have methods that implement a stack.
// For example, to set the root directory (aka bucket) to ltx:
//
//	kp.Dirs.Push("ltx")
//
// To set a sub-directory within ltx to gr_gr_cog:
//
//	kp.Dirs.Push("gr_gr_cog")
//
// To set a sub-directory within gr_gr_cog to actors:
//
//	kp.Dirs.Push("actors")
//
// The resulting Dirs path is: ltx/gr_gr_cog/actors
// To remove the last Directory entered:
//
//	kp.Dirs.Pop()
type KeyPath struct {
	Dirs     *stack.StringStack
	KeyParts *stack.StringStack
}

func NewKeyPath() *KeyPath {
	kp := new(KeyPath)
	kp.Dirs = new(stack.StringStack)
	kp.KeyParts = new(stack.StringStack)
	return kp
}

// ApiID returns a url that can be used to directly access a text by its ID.
func (k *KeyPath) ApiID(domain, port string) string {
	return fmt.Sprintf("http://%s:%s/api/id/%s/%s", domain, port, k.Dirs.Join("/"), k.KeyParts.Join("/"))
}
func (k *KeyPath) ApiTopic(domain, port string) string {
	return fmt.Sprintf("http://%s:%s/api/cmp/%s/%s/%s", domain, port, k.Dirs.First(), k.Dirs.Last(), k.KeyParts.First())
}

// Clear sets the KeyPath slices to nil, so it can be used for another record.
func (k *KeyPath) Clear() {
	k.Dirs = new(stack.StringStack)
	k.KeyParts = new(stack.StringStack)
}

// Copy makes a deep copy of the KeyPath
func (k *KeyPath) Copy() *KeyPath {
	kp := NewKeyPath()
	kp.Dirs = k.Dirs.Copy()
	kp.KeyParts = k.KeyParts.Copy()
	return kp
}

// IsRecord returns true if the KeyPath is the path to a record.
func (k *KeyPath) IsRecord() bool {
	return k.KeyParts != nil && !k.KeyParts.Empty()
}

// IsMapEntry returns true if the KeyPath is an entry in a map.
func (k *KeyPath) IsMapEntry() bool {
	return k.Dirs.Last() == "map"
}

// IsSliceElement returns true if the KeyPath is an element for a slice property.
func (k *KeyPath) IsSliceElement() bool {
	if k.Dirs.Last() == "values" {
		if k.Dirs.Get(k.Dirs.Size()-2) == "layouts" {
			return true
		}
		if _, err := strconv.Atoi(k.KeyParts.First()); err == nil {
			return true
		}
	}
	return false
}

// Key returns the KeyParts.
// If the length of KeyParts > 1, the parts are separated by the SegmentDelimiter
func (k *KeyPath) Key() string {
	key := k.SegmentString(k.KeyParts)
	return key
}

// Last returns the last element in the KeyPath.
// If the last element is a directory, the 2nd return value == true
func (k *KeyPath) Last() (string, bool) {
	if k.KeyParts.Size() == 0 {
		return k.Dirs.Last(), true
	} else {
		return k.KeyParts.Last(), false
	}
}

// Path returns the Dirs and KeyParts as a path, separated by the IdDelimiter
// If the length of Dirs or KeyParts is > 1, the segments are separated by the SegmentDelimiter
func (k *KeyPath) Path() string {
	sb := strings.Builder{}
	if k.Dirs != nil {
		sb.WriteString(k.SegmentString(k.Dirs))
	}
	if !k.KeyParts.Empty() {
		sb.WriteString(IdDelimiter)
		sb.WriteString(k.SegmentString(k.KeyParts))
	}
	return sb.String()
}

// ToRedirectId converts the KeyPath to a string be used in the Value field of a redirect record.
// example: @ltx/en_us_dedes/actors:Priest
func (k *KeyPath) ToRedirectId() string {
	return fmt.Sprintf("%s%s", RedirectsTo, k.Path())
}

// AddNnpSuffix add -nnp to first dir
func (k *KeyPath) AddNnpSuffix() {
	if k.Dirs.Size() > 0 {
		k.Dirs.Set(0, k.Dirs.First()+"-nnp")
	}
}

// RemoveNnpSuffix removes -nnp from the first dir
func (k *KeyPath) RemoveNnpSuffix() {
	if k.Dirs.Size() > 0 {
		k.Dirs.Set(0, strings.ReplaceAll(k.Dirs.First(), "-nnp", ""))
	}
}

// DirHasNnpSuffix returns true if the first dir has -nnp as a suffix
func (k *KeyPath) DirHasNnpSuffix() bool {
	return k.Dirs.Size() > 0 && strings.HasSuffix(k.Dirs.First(), "-nnp")
}

// KeyRingRec returns the Topic and Key as a database record
func (k *KeyPath) KeyRingRec() *DbR {
	r := NewDbR()
	r.KP.Dirs.Push("keyring")
	if k.Dirs != nil && !k.Dirs.Empty() {
		r.KP.Dirs.Push(k.Dirs.Last())
	}
	if k.KeyParts != nil && !k.KeyParts.Empty() {
		r.KP.KeyParts.Push(k.KeyParts.First())
	}
	r.Value = k.Dirs.Get(1)
	return r
}

// LmlTopicKey returns the last segment  (aka, the topic) of the Dirs and the key, separated by IdDelimiter for use as an LML sid or rid ID.
func (k *KeyPath) LmlTopicKey() string {
	sb := strings.Builder{}
	if k.Dirs != nil && !k.Dirs.Empty() {
		sb.WriteString(k.Dirs.Last())
		sb.WriteString(IdDelimiter)
	}
	if k.KeyParts != nil && !k.KeyParts.Empty() {
		sb.WriteString(k.SegmentString(k.KeyParts))
	}
	return sb.String()
}

// LmlSid returns the ID as a sid "topic/key"
func (k *KeyPath) LmlSid() string {
	sb := strings.Builder{}
	if k.Dirs != nil && !k.Dirs.Empty() {
		sb.WriteString("sid \"")
		sb.WriteString(k.Dirs.Last())
		sb.WriteString(IdDelimiter)
	}
	if k.KeyParts != nil && !k.KeyParts.Empty() {
		sb.WriteString(k.SegmentString(k.KeyParts))
	} else {
		return ""
	}
	sb.WriteString("\"")
	return sb.String()
}

// LmlRid returns the ID as a rid "topic/key" if it is eligible to be one
func (k *KeyPath) LmlRid() string {
	sb := strings.Builder{}
	if k.Dirs != nil && !k.Dirs.Empty() {
		wc := ltstring.GetWildcard(k.Dirs.Last())
		if len(wc) == 0 {
			return "" // not eligible to be a rid
		}
		sb.WriteString("rid \"")
		sb.WriteString(wc)
		sb.WriteString(IdDelimiter)
	} else {
		return ""
	}
	if k.KeyParts != nil && !k.KeyParts.Empty() {
		sb.WriteString(k.SegmentString(k.KeyParts))
	} else {
		return ""
	}
	sb.WriteString("\"")
	return sb.String()
}

// DirPath returns the Dirs as a path, separated by the IdDelimiter
// If the length of Dirs is > 1, the segments are separated by the SegmentDelimiter
func (k *KeyPath) DirPath() string {
	sb := strings.Builder{}
	if k.Dirs != nil {
		sb.WriteString(k.SegmentString(k.Dirs))
	}
	return sb.String()
}

// DelimitedPath returns the Dirs and KeyParts as a path, separated by the supplied delimiter.
// The IdDelimiter and SegmentDelimiter are replaced with the supplied delimiter.
func (k *KeyPath) DelimitedPath(delimiter string) string {
	sb := strings.Builder{}
	if k.Dirs != nil {
		sb.WriteString(k.Dirs.Join(delimiter))
	}
	sb.WriteString(delimiter)
	if k.KeyParts != nil {
		sb.WriteString(k.KeyParts.Join(delimiter))
	}
	return sb.String()
}

// ParsePath sets the DbR Dirs and KeyParts after parsing the parts from the supplied path.
// A properly formatted path uses the IdDelimiter to delimit Dirs, KeyParts, Suffix.
// Even if a Dirs or Suffix is not used, there should still be a IdDelimiter for each.
// The Dirs and Suffix can optionally have segments delimited by the SegmentDelimiter.
// An example path is: ltx/gr_gr_cog/actors:Priest:value
func (k *KeyPath) ParsePath(p string) error {
	parts := k.ParseParts(p)
	switch len(parts) {
	case 1: // this is a partial path, with just the dirs
		k.Dirs.Clear()
		k.KeyParts.Clear()
		for _, p := range k.ParseSegments(parts[0]) {
			k.Dirs.Push(strings.TrimSpace(p))
		}
	case 2:
		k.Dirs.Clear()
		for _, p := range k.ParseSegments(parts[0]) {
			if len(p) > 0 {
				k.Dirs.Push(strings.TrimSpace(p))
			}
		}
		k.KeyParts.Clear()
		for _, p := range k.ParseSegments(parts[1]) {
			k.KeyParts.Push(strings.TrimSpace(p))
		}
	case 3:
		k.Dirs.Clear()
		for _, p := range k.ParseSegments(parts[0]) {
			if len(p) > 0 {
				k.Dirs.Push(strings.TrimSpace(p))
			}
		}
		k.KeyParts.Clear()
		for _, p := range k.ParseSegments(parts[1]) {
			k.KeyParts.Push(strings.TrimSpace(p))
		}
	default:
		msg := fmt.Sprintf("error: %s not properly formatted. Parts must be delimited by %s and Dirs and KeyParts optionally by %s", p, IdDelimiter, SegmentDelimiter)
		doxlog.Error(msg)
		return fmt.Errorf(msg)
	}
	return nil
}

// ParseParts splits s into its parts using the IdDelimiter as the delimiter.
func (k *KeyPath) ParseParts(s string) []string {
	return strings.Split(s, IdDelimiter)
}

// ParseSegments splits s into its segments using the SegmentDelimiter as the delimiter.
func (k *KeyPath) ParseSegments(s string) []string {
	return strings.Split(s, SegmentDelimiter)
}

// SegmentString joins the parameter slice into a segment using the SegmentDelimiter as the delimiter.
func (k *KeyPath) SegmentString(s *stack.StringStack) string {
	return s.Join(SegmentDelimiter)
}
func (k *KeyPath) DirsSegmentString() string {
	return k.Dirs.Join(SegmentDelimiter)
}
func (k *KeyPath) KeyPartsSegmentString() string {
	return k.KeyParts.Join(SegmentDelimiter)
}

// ToDirPath returns the directory path
func (k *KeyPath) ToDirPath() string {
	return path.Join(k.Dirs.Slice()...)
}

// ToFileSystemPath converts the keypath to a file system path so the content can be written as a file
func (k *KeyPath) ToFileSystemPath() string {
	sb := strings.Builder{}
	sb.WriteString(path.Join(k.Dirs.Slice()...))
	sb.WriteString("/")
	sb.WriteString(path.Join(k.KeyParts.Slice()...))
	return sb.String()
}

// ToFileSystemFile converts the keypath to a path and filename
// so the content can be written as a file.
// The Dir part of the KP becomes the path,
// the Key part becomes the file with extension as supplied.
// The extension will have a dot prefixed if it is not already prefixed.
func (k *KeyPath) ToFileSystemFile(extension string) string {
	sb := strings.Builder{}
	sb.WriteString(path.Join(k.Dirs.Slice()...))
	sb.WriteString("/")
	sb.WriteString(path.Join(k.KeyParts.Slice()...))
	if !strings.HasPrefix(extension, ".") {
		extension = fmt.Sprintf(".%s", extension)
	}
	sb.WriteString(extension)
	return sb.String()
}

// AppendPath adds the dirs and keyparts from a childpath onto a parent path
// Note that if the child path has dirs parent keyparts will be discarded
func (k *KeyPath) AppendPath(path *KeyPath) {
	subpath := path.Copy()
	if subpath.Dirs.Empty() {
		for _, part := range subpath.KeyParts.Slice() {
			k.KeyParts.Push(part)
		}
		return
	}
	subpath.Dirs.First()
	for _, dir := range subpath.Dirs.Slice() {
		k.Dirs.Push(dir)
	}
	k.KeyParts = subpath.KeyParts
}

// Matcher holds information used to match records during a find operation
type Matcher struct {
	KP           *KeyPath
	ValuePattern string
	IncludeEmpty bool // include records with len(value) == 0
	NNP          bool // use the nnp version of the bucket (normalized without punctuation)
	Recursive    bool
	RegEx        bool
	IdSearch     bool
	OriginalKP   *KeyPath
	// The following are slices because GetMatching
	// and GetMatchingIds walk the entire db tree
	// and compare all records against a working KeyPath
	// (which is an indexed stack) by comparing each
	// path segment in the indexed regEx to its
	// corresponding matcher.KP path segment.
	// For example:
	// `
	DirsRegEx            []*regexp.Regexp // used if a key is for a record
	IdRegEx              []*regexp.Regexp // used if a key is for a directory
	ValueRegEx           *regexp.Regexp
	WholeWord            bool
	ForConcordance       bool
	NFD                  bool // convert pattern and text to NFD for matching
	CaseInsensitive      bool
	DiacriticInsensitive bool
}

func NewMatcher() *Matcher {
	m := new(Matcher)
	kp := NewKeyPath()
	m.KP = kp
	return m
}

// Copy returns a pointer to a deep copy of the current matcher
func (m *Matcher) Copy() *Matcher {
	n := NewMatcher()
	if m.UsingRegEx() {
		n.KP = m.OriginalKP.Copy()
		_ = n.UseAsRegEx()
	} else {
		_ = n.KP.ParsePath(m.KP.Path())
	}
	n.ValuePattern = m.ValuePattern
	n.NNP = m.NNP
	n.Recursive = m.Recursive
	n.IncludeEmpty = m.IncludeEmpty
	return n
}

// UseAsRegEx initializes the regular expression matchers.
// If a syntactically incorrect expression is encountered, an error is returned.
// If Matcher.regEx == false, returns an error.
// Note:  A copy of m.KP is made.  It is the "original" KP.
//
//		This is because the m.KP is used as a working KP
//		during the kvs GetMatching or GetMatchingIDs methods.
//		Because each segment of a path, e.g. ltx/gr_gr_cog/en_us_dedes
//		is evaluated and its corresponding segment in a retrieved
//		record, the regEx version for each segment will
//	    create a pattern for each segment because
//		corresponding segment index of the IdRegEx and
//		the retrieved record's ID segment are compared,
//		a segment at a time.
func (m *Matcher) UseAsRegEx() error {
	// copy the original KP
	m.OriginalKP = NewKeyPath()
	for _, d := range *m.KP.Dirs {
		m.OriginalKP.Dirs.Push(d)
	}
	for _, d := range *m.KP.KeyParts {
		m.OriginalKP.KeyParts.Push(d)
	}
	if !m.IdSearch {
		// ensure the value is in Unicode normal form c
		if m.NFD {
			m.ValuePattern = norm.NFD.String(m.ValuePattern)
		} else {
			m.ValuePattern = norm.NFC.String(m.ValuePattern)
		}
		if m.CaseInsensitive {
			m.ValuePattern = "(?i)" + m.ValuePattern
		}
	}
	// set the regular expression for each segment
	// of the ID Dirs.  This is because during a
	// call to GetMatching or GetMatchingIDs, the
	// corresponding segment index of the IdRegEx and
	// the retrieved record's ID segment are compared.
	for _, p := range *m.KP.Dirs {
		switch p {
		case "": // set to wildcard
			p = ".*"
		case ".*": // already set
		default: // wrap with text start and end
			if !strings.HasPrefix(p, "^") {
				p = "^" + p
			}
			if !strings.HasSuffix(p, "$") {
				p = p + "$"
			}
		}
		r, err := regexp.Compile(p)
		if err != nil {
			msg := fmt.Sprintf("regexp.Compile(%s) error: %v", p, err)
			doxlog.Error(msg)
			return fmt.Errorf(msg)
		}
		m.DirsRegEx = append(m.DirsRegEx, r)
	}
	// nil the Dirs slice so that it can be reused with non regex values
	m.KP.Dirs.Clear()

	// set the ID KeyParts regular expressions
	var r *regexp.Regexp
	var err error
	pattern := m.KP.KeyPartsSegmentString()
	r, err = regexp.Compile(pattern)
	if err != nil {
		return err
	}
	m.IdRegEx = append(m.IdRegEx, r)
	m.KP.KeyParts.Clear()
	// since * is a key symbol in a regular expression,
	// and since the user might be searching for the literal
	// we will escape it for them.
	// But, they might have a .* we need to preserve.
	//m.ValuePattern = strings.ReplaceAll(m.ValuePattern, ".*", "~~")
	//m.ValuePattern = strings.ReplaceAll(m.ValuePattern, "*", "\\*")
	//m.ValuePattern = strings.ReplaceAll(m.ValuePattern, "~~", ".*")
	if m.WholeWord {
		m.ValuePattern = fmt.Sprintf("%s(%s)%s", WordBoundary, m.ValuePattern, WordBoundary)
	}
	m.ValueRegEx, err = regexp.Compile(m.ValuePattern)
	if err != nil {
		return err
	}
	m.ValuePattern = ""
	m.RegEx = true
	return nil
}
func (m *Matcher) ClearValueRegEx() {
	m.ValuePattern = ""
	m.ValueRegEx, _ = regexp.Compile(m.ValuePattern)
}

func (m *Matcher) UsingRegEx() bool {
	return m.RegEx
}

// KeyPath computes a new KeyPath based on
// the current patch context.
// Note that when a db search is underway,
// matcher.KP acts as a contextual cursor
// to the point in the db tree that it is
// searching from.  Calling KeyPath,
// returns a KeyPath that is created
// by appending to m.KP the KP in parameter ckp.
// m.KP is not modified by this method.  If it
// is modified, it is the caller who does it.
func (m *Matcher) KeyPath(ckp *KeyPath) (newKp *KeyPath) {
	newKp = NewKeyPath()
	// create a slice of dir path segments
	// populated with the ones in m.KP.Dirs.
	var dirVals []string
	for _, p := range *m.KP.Dirs {
		dirVals = append(dirVals, p)
	}
	// iterate dirVals and push them onto newKp
	j := len(dirVals)
	k := ckp.Dirs.Size()
	for i := 0; i < j; i++ {
		if i >= k {
			if len(dirVals) > i && dirVals[i] != "" {
				newKp.Dirs.Push(dirVals[i])
			} else {
				break
			}
		}
	}
	return newKp
}

// TopicKeyMatcher uses the last Dir and the ID of the current matcher
// to create a new one that will have wildcards, i.e. .*, for the 1..len(Dir)-1
// elements.  The new matcher will use regular expressions.
func (m *Matcher) TopicKeyMatcher() *Matcher {
	n := NewMatcher()
	var j int
	if m.KP.Dirs.First() == "btx" {
		j = m.KP.Dirs.Size() - 3
	} else {
		if m.KP.Dirs.Size() == 2 {
			j = 1
		} else {
			j = m.KP.Dirs.Size() - 2
		}
	}
	n.KP.Dirs.Push("^" + m.KP.Dirs.First() + "$") // preserve the table type
	for i := 0; i < j; i++ {
		n.KP.Dirs.Push(".*")
	}
	if m.KP.Dirs.First() == "btx" {
		n.KP.Dirs.Push(m.KP.Dirs.Get(m.KP.Dirs.Size() - 2))
		n.KP.Dirs.Push(m.KP.Dirs.Last())
	} else {
		n.KP.Dirs.Push(m.KP.Dirs.Last())
	}
	for _, p := range *m.KP.KeyParts {
		n.KP.KeyParts.Push(p)
	}
	id := "^" + n.KP.KeyPartsSegmentString() + "$"
	n.KP.KeyParts.Clear()
	n.KP.KeyParts.Push(id)
	n.Recursive = true
	n.IncludeEmpty = true
	_ = n.UseAsRegEx()
	return n
}

// KVPSorter implements methods to use the golang sort.
// e.g. var kvps []*DbR
//
//	.. append some kvp instances to kvps, then...
//	sort.Sort(KVPSorter(kvps))
type KVPSorter []*DbR

func (k KVPSorter) Len() int           { return len(k) }
func (k KVPSorter) Swap(i, j int)      { k[i], k[j] = k[j], k[i] }
func (k KVPSorter) Less(i, j int) bool { return k[i].KP.Path() < k[j].KP.Path() }

// HasKeyRing returns true if the parameter d is the name of a directory
// for which exists a corresponding keyring record.
func HasKeyRing(d string) bool {
	switch d {
	case "ltx":
		return true
	default:
		return false
	}
}

// IsSystem returns true if the directory reserved for use by the system.
// The parameter supplied should be obtained using KeyParts.Dirs.First().
func IsSystem(d string) bool {
	switch d {
	case "btx-nnp", "ltx-nnp", "system":
		return true
	default:
		return false
	}
}

// DirDescription returns a short description of a directory.
// These are for directories Doxa relies on, not end-user ones.
func DirDescription(d string) string {
	switch d {
	case "btx":
		return "Biblical texts"
	case "btx-nnp":
		return "System: Biblical texts"
	case "ltx":
		return "Liturgical texts"
	case "ltx-nnp":
		return "System: Liturgical texts"
	case "system":
		return "System: Doxa system properties"
	default:
		return ""
	}
}

// TreeValue is the value in a node in the tree returned by the KVS.Tree func.
type TreeValue struct {
	KP    *KeyPath
	Leaf  string
	Value string
}

// PropertyManager provides methods for getting and setting properties using a KVS.
// The KVS Dirs should be set before calling a method.
// For methods with a dir parm, the dir will be pushed on the KP.Dirs, the db call made, and KP.Dirs popped.
// For methods with a key parm, the key will be pushed on the KP.KeyParts, the db call made, and KP.KeyParts popped.
// Methods that put a slice store the record using the slice index+1 as the key, e.g. slice[0] is stored with 1 as the key
// Errors returned will either be a cast error or ID not found error.
type PropertyManager struct {
	KVS *KVS
	KP  *KeyPath
}

func NewPropertyManager(kvs *KVS) *PropertyManager {
	p := new(PropertyManager)
	p.KP = NewKeyPath()
	p.KP.Dirs.Push("config")
	p.KVS = kvs
	return p
}

// GetAnnotation returns a description of the directory if it exists.
func (p *PropertyManager) GetAnnotation(dir string) (string, error) {
	p.KP.Dirs.Push(dir)
	p.KP.KeyParts.Push("desc")
	val, err := p.KVS.Db.Get(p.KP)
	p.KP.Dirs.Pop()
	p.KP.KeyParts.Pop()
	if err != nil {
		return "", err
	}
	return val.Value, nil
}

func (p *PropertyManager) GetBool(key string) (bool, error) {
	p.KP.KeyParts.Push(key)
	val, err := p.KVS.Db.Get(p.KP)
	p.KP.KeyParts.Pop()
	if err != nil {
		return false, err
	}
	return cast.ToBoolE(val.Value)
}
func (p *PropertyManager) GetBoolSlice(dir string) ([]bool, error) {
	p.KP.Dirs.Push(dir)
	m := NewMatcher()
	m.KP = p.KP.Copy()
	recs, _, err := p.KVS.Db.GetMatching(*m)
	if err != nil {
		return nil, err
	}
	var result []bool
	for _, dbr := range recs {
		v, err := cast.ToBoolE(dbr.Value)
		if err != nil {
			return nil, err
		}
		result = append(result, v)
	}
	p.KP.Dirs.Pop()
	return result, nil
}

func (p *PropertyManager) GetFloat64(key string) (float64, error) {
	p.KP.KeyParts.Push(key)
	val, err := p.KVS.Db.Get(p.KP)
	p.KP.KeyParts.Pop()
	if err != nil {
		return 0.0, err
	}
	return cast.ToFloat64E(val.Value)
}
func (p *PropertyManager) GetFloat64Slice(dir string) ([]float64, error) {
	p.KP.Dirs.Push(dir)
	m := NewMatcher()
	m.KP = p.KP.Copy()
	recs, _, err := p.KVS.Db.GetMatching(*m)
	if err != nil {
		return nil, err
	}
	var result []float64
	for _, dbr := range recs {
		v, err := cast.ToFloat64E(dbr.Value)
		if err != nil {
			return nil, err
		}
		result = append(result, v)
	}
	p.KP.Dirs.Pop()
	return result, nil
}

// GetJson populates the struct v using the json retrieved from the database.
// If the record does not exist or if there is a json.Unmarshal error, an error is returned.
func (p *PropertyManager) GetJson(key string, v interface{}) error {
	p.KP.KeyParts.Push(key)
	dbr, err := p.KVS.Db.Get(p.KP)
	p.KP.KeyParts.Pop()
	if err != nil {
		return err
	}
	return json.Unmarshal([]byte(dbr.Value), v)
}
func (p *PropertyManager) GetInt(key string) (int, error) {
	p.KP.KeyParts.Push(key)
	val, err := p.KVS.Db.Get(p.KP)
	p.KP.KeyParts.Pop()
	if err != nil {
		return 0, err
	}
	return strconv.Atoi(val.Value)
	//	return cast.ToIntE(val)
}
func (p *PropertyManager) GetIntSlice(dir string) ([]int, error) {
	p.KP.Dirs.Push(dir)
	m := NewMatcher()
	m.KP = p.KP.Copy()
	recs, _, err := p.KVS.Db.GetMatching(*m)
	if err != nil {
		return nil, err
	}
	var result []int
	for _, dbr := range recs {
		v, err := cast.ToIntE(dbr.Value)
		if err != nil {
			return nil, err
		}
		result = append(result, v)
	}
	p.KP.Dirs.Pop()
	return result, nil
}
func (p *PropertyManager) GetString(key string) (string, error) {
	p.KP.KeyParts.Push(key)
	val, err := p.KVS.Db.Get(p.KP)
	p.KP.KeyParts.Pop()
	if err != nil {
		return "", err
	}
	return val.Value, nil
}
func (p *PropertyManager) GetAnnotatedBoolSlice(key string) (*AnnotatedBoolSlice, error) {
	a := new(AnnotatedBoolSlice)
	p.KP.Dirs.Push(key)
	// get value
	val, err := p.GetBoolSlice("values")
	if err != nil {
		return nil, err
	}
	for _, v := range val {
		a.Push(v)
	}

	// get description
	desc, err := p.GetString("desc")
	if err != nil {
		return nil, err
	}
	a.Desc = desc

	return a, nil
}
func (p *PropertyManager) GetAnnotatedFloat64Slice(key string) (*AnnotatedFloat64Slice, error) {
	a := new(AnnotatedFloat64Slice)
	p.KP.Dirs.Push(key)
	// get value
	val, err := p.GetFloat64Slice("values")
	if err != nil {
		return nil, err
	}
	for _, v := range val {
		a.Push(v)
	}

	// get description
	desc, err := p.GetString("desc")
	if err != nil {
		return nil, err
	}
	a.Desc = desc

	return a, nil
}
func (p *PropertyManager) GetAnnotatedIntSlice(key string) (*AnnotatedIntSlice, error) {
	a := new(AnnotatedIntSlice)
	p.KP.Dirs.Push(key)
	// get value
	val, err := p.GetIntSlice("values")
	if err != nil {
		return nil, err
	}
	for _, v := range val {
		a.Push(v)
	}

	// get description
	desc, err := p.GetString("desc")
	if err != nil {
		return nil, err
	}
	a.Desc = desc

	return a, nil
}

func (p *PropertyManager) GetAnnotatedString(key string) (*AnnotatedString, error) {
	a := new(AnnotatedString)
	p.KP.Dirs.Push(key)
	// get value
	val, err := p.GetString("value")
	if err != nil {
		return nil, err
	}
	a.Value = val

	// get description
	val, err = p.GetString("desc")
	if err != nil {
		return nil, err
	}
	a.Desc = val

	return a, nil
}
func (p *PropertyManager) GetAnnotatedStringSlice(key string) (*AnnotatedStringSlice, error) {
	a := new(AnnotatedStringSlice)
	p.KP.Dirs.Push(key)
	// get value
	val, err := p.GetStringSlice("values")
	if err != nil {
		return nil, err
	}
	for _, v := range val {
		a.Push(v)
	}

	// get description
	desc, err := p.GetString("desc")
	if err != nil {
		return nil, err
	}
	a.Desc = desc

	return a, nil
}
func (p *PropertyManager) GetStringSlice(dir string) ([]string, error) {
	p.KP.Dirs.Push(dir)
	m := NewMatcher()
	m.KP = p.KP.Copy()
	recs, _, err := p.KVS.Db.GetMatching(*m)
	if err != nil {
		p.KP.Dirs.Pop()
		return nil, err
	}
	var result []string
	for _, dbr := range recs {
		result = append(result, dbr.Value)
	}
	p.KP.Dirs.Pop()
	return result, nil
}

// SetAnnotation sets a description for the current directory
func (p *PropertyManager) SetAnnotation(val string) error {
	p.KP.KeyParts.Push("desc")
	dbr := NewDbR()
	dbr.KP = p.KP.Copy()
	dbr.Value = val
	p.KP.KeyParts.Pop()
	return p.KVS.Db.Put(dbr)
}

func (p *PropertyManager) SetBool(key string, val bool) error {
	p.KP.KeyParts.Push(key)
	dbr := NewDbR()
	dbr.KP = p.KP.Copy()
	dbr.Value = strconv.FormatBool(val)
	p.KP.KeyParts.Pop()
	return p.KVS.Db.Put(dbr)
}
func (p *PropertyManager) SetBoolSlice(dir string, val []bool) error {
	p.KP.Dirs.Push(dir)
	for i, v := range val {
		p.KP.KeyParts.Push(fmt.Sprintf("%03d", i+1))
		dbr := NewDbR()
		dbr.KP = p.KP.Copy()
		dbr.Value = fmt.Sprintf("%v", v)
		p.KP.KeyParts.Pop()
		err := p.KVS.Db.Put(dbr)
		if err != nil {
			return err
		}
	}
	p.KP.Dirs.Pop()
	return nil
}

func (p *PropertyManager) SetFloat64(key string, val float64) error {
	p.KP.KeyParts.Push(key)
	dbr := NewDbR()
	dbr.KP = p.KP.Copy()
	dbr.Value = fmt.Sprintf("%v", val)
	p.KP.KeyParts.Pop()
	return p.KVS.Db.Put(dbr)
}
func (p *PropertyManager) SetFloat64Slice(dir string, val []float64) error {
	p.KP.Dirs.Push(dir)
	for i, v := range val {
		p.KP.KeyParts.Push(fmt.Sprintf("%03d", i+1))
		dbr := NewDbR()
		dbr.KP = p.KP.Copy()
		dbr.Value = fmt.Sprintf("%v", v)
		p.KP.KeyParts.Pop()
		err := p.KVS.Db.Put(dbr)
		if err != nil {
			return err
		}
	}
	p.KP.Dirs.Pop()
	return nil
}

// SetJson marshals the val into a Json byte[] and saves it as a string in the database
func (p *PropertyManager) SetJson(key string, val interface{}) error {
	p.KP.KeyParts.Push(key)
	dbr := NewDbR()
	dbr.KP = p.KP.Copy()
	b, err := json.Marshal(val)
	if err != nil {
		return err
	}
	dbr.Value = string(b)
	p.KP.KeyParts.Pop()
	return p.KVS.Db.Put(dbr)
}

func (p *PropertyManager) SetInt(key string, val int) error {
	p.KP.KeyParts.Push(key)
	dbr := NewDbR()
	dbr.KP = p.KP.Copy()
	dbr.Value = fmt.Sprintf("%v", val)
	p.KP.KeyParts.Pop()
	return p.KVS.Db.Put(dbr)
}
func (p *PropertyManager) SetIntSlice(key string, val []int) error {
	p.KP.KeyParts.Push(key)
	for i, v := range val {
		p.KP.KeyParts.Push(fmt.Sprintf("%03d", i+1))
		dbr := NewDbR()
		dbr.KP = p.KP.Copy()
		dbr.Value = fmt.Sprintf("%v", v)
		p.KP.KeyParts.Pop()
		err := p.KVS.Db.Put(dbr)
		if err != nil {
			return err
		}
	}
	p.KP.KeyParts.Pop()
	return nil
}
func (p *PropertyManager) MakeStringRecord(key string, val string) *DbR {
	p.KP.KeyParts.Push(key)
	dbr := NewDbR()
	dbr.KP = p.KP.Copy()
	dbr.Value = val
	p.KP.KeyParts.Pop()
	return dbr
}

func (p *PropertyManager) SetString(key string, val string) error {
	p.KP.KeyParts.Push(key)
	dbr := NewDbR()
	dbr.KP = p.KP.Copy()
	dbr.Value = val
	p.KP.KeyParts.Pop()
	return p.KVS.Db.Put(dbr)
}

// SetAnnotatedBool treats the key as a directory, and stores value = val, desc = desc
func (p *PropertyManager) SetAnnotatedBool(key string, val bool, desc string) error {
	p.KP.Dirs.Push(key)

	// set value
	err := p.SetBool("value", val)
	if err != nil {
		return err
	}
	// set description
	err = p.SetString("desc", desc)
	if err != nil {
		return err
	}

	p.KP.Dirs.Pop()

	return nil
}

// SetAnnotatedBoolSlice treats the key as a directory, and stores value = val, desc = desc
func (p *PropertyManager) SetAnnotatedBoolSlice(key string, val []bool, desc string) error {
	p.KP.Dirs.Push(key)

	// set value
	err := p.SetBoolSlice("values", val)
	if err != nil {
		return err
	}
	// set description
	err = p.SetString("desc", desc)
	if err != nil {
		return err
	}

	p.KP.Dirs.Pop()

	return nil
}

// SetAnnotatedFloat64 treats the key as a directory, and stores value = val, desc = desc
func (p *PropertyManager) SetAnnotatedFloat64(key string, val float64, desc string) error {
	p.KP.Dirs.Push(key)

	// set value
	err := p.SetFloat64("value", val)
	if err != nil {
		return err
	}
	// set description
	err = p.SetString("desc", desc)
	if err != nil {
		return err
	}

	p.KP.Dirs.Pop()

	return nil
}

// SetAnnotatedFloat64Slice treats the key as a directory, and stores value = val, desc = desc
func (p *PropertyManager) SetAnnotatedFloat64Slice(key string, val []float64, desc string) error {
	p.KP.Dirs.Push(key)

	// set value
	err := p.SetFloat64Slice("values", val)
	if err != nil {
		return err
	}
	// set description
	err = p.SetString("desc", desc)
	if err != nil {
		return err
	}

	p.KP.Dirs.Pop()

	return nil
}

// SetAnnotatedInt treats the key as a directory, and stores value = val, desc = desc
func (p *PropertyManager) SetAnnotatedInt(key string, val int, desc string) error {
	p.KP.Dirs.Push(key)

	// set value
	err := p.SetInt("value", val)
	if err != nil {
		return err
	}
	// set description
	err = p.SetString("desc", desc)
	if err != nil {
		return err
	}

	p.KP.Dirs.Pop()

	return nil
}

// SetAnnotatedIntSlice treats the key as a directory, and stores value = val, desc = desc
func (p *PropertyManager) SetAnnotatedIntSlice(key string, val []int, desc string) error {
	p.KP.Dirs.Push(key)

	// set value
	err := p.SetIntSlice("values", val)
	if err != nil {
		return err
	}
	// set description
	err = p.SetString("desc", desc)
	if err != nil {
		return err
	}

	p.KP.Dirs.Pop()

	return nil
}

// SetAnnotatedString treats the key as a directory, and stores value = val, desc = desc
func (p *PropertyManager) SetAnnotatedString(key string, val string, desc string) error {
	p.KP.Dirs.Push(key)

	// set value
	err := p.SetString("value", val)
	if err != nil {
		return err
	}
	// set description
	err = p.SetString("desc", desc)
	if err != nil {
		return err
	}

	p.KP.Dirs.Pop()

	return nil
}

// SetAnnotatedStringSlice treats the key as a directory, and stores value = val, desc = desc
func (p *PropertyManager) SetAnnotatedStringSlice(key string, val []string, desc string) error {
	p.KP.Dirs.Push(key)
	var err error
	// set value
	err = p.SetStringSlice("values", val)
	if err != nil {
		return err
	}
	// set description
	err = p.SetString("desc", desc)
	if err != nil {
		return err
	}

	p.KP.Dirs.Pop()

	return nil
}
func (p *PropertyManager) MakeStringSliceDatabaseRecords(dir string, val []string) []*DbR {
	var recs []*DbR
	p.KP.Dirs.Push(dir)
	for i, v := range val {
		p.KP.KeyParts.Push(fmt.Sprintf("%03d", i+1))
		dbr := NewDbR()
		dbr.KP = p.KP.Copy()
		dbr.Value = strings.TrimSpace(v)
		recs = append(recs, dbr)
		p.KP.KeyParts.Pop()
	}
	p.KP.Dirs.Pop()
	return recs
}

func (p *PropertyManager) SetStringSlice(dir string, val []string) error {
	return p.KVS.Db.Batch(p.MakeStringSliceDatabaseRecords(dir, val))
}

// AnnotatedString is used to store a property along with a description.
type AnnotatedString struct {
	Value string
	Desc  string
}

// GetAnnotatedStringFromJson reads the database for an AnnotatedString stored as Json.
func (p *PropertyManager) GetAnnotatedStringFromJson(key string) (*AnnotatedString, error) {
	s := new(AnnotatedString)
	err := p.GetJson(key, s)
	return s, err
}

// SetAnnotatedStringAsJson stores an AnnotatedString as Json in the database
func (p *PropertyManager) SetAnnotatedStringAsJson(key string, s *AnnotatedString) error {
	return p.SetJson(key, s)
}

// AnnotatedBool is used to store a property along with a description.
type AnnotatedBool struct {
	Value bool
	Desc  string
}

// GetAnnotatedBoolFromJson reads the database for an AnnotatedBool stored as Json.
func (p *PropertyManager) GetAnnotatedBoolFromJson(key string) (*AnnotatedBool, error) {
	s := new(AnnotatedBool)
	err := p.GetJson(key, s)
	return s, err
}

// SetAnnotatedBoolAsJson stores an AnnotatedBool as Json in the database
func (p *PropertyManager) SetAnnotatedBoolAsJson(key string, s *AnnotatedBool) error {
	return p.SetJson(key, s)
}

// AnnotatedInt is used to store a property along with a description.
type AnnotatedInt struct {
	Value int
	Desc  string
}

// GetAnnotatedIntFromJson reads the database for an AnnotatedInt stored as Json.
func (p *PropertyManager) GetAnnotatedIntFromJson(key string) (*AnnotatedInt, error) {
	s := new(AnnotatedInt)
	err := p.GetJson(key, s)
	return s, err
}

// SetAnnotatedIntAsJson stores an AnnotatedInt as Json in the database
func (p *PropertyManager) SetAnnotatedIntAsJson(key string, s *AnnotatedInt) error {
	return p.SetJson(key, s)
}

type AnnotatedBoolSlice struct {
	Value []bool
	Desc  string
}

func (a *AnnotatedBoolSlice) Push(v bool) {
	a.Value = append(a.Value, v)
}

type AnnotatedFloat64Slice struct {
	Value []float64
	Desc  string
}

func (a *AnnotatedFloat64Slice) Push(v float64) {
	a.Value = append(a.Value, v)
}

type AnnotatedIntSlice struct {
	Value []int
	Desc  string
}

func (a *AnnotatedIntSlice) Push(v int) {
	a.Value = append(a.Value, v)
}

// AnnotatedStringSlice is used to store a property along with a description.
type AnnotatedStringSlice struct {
	Value []string
	Desc  string
}

func (a *AnnotatedStringSlice) Push(v string) {
	a.Value = append(a.Value, v)
}

// GetAnnotatedStringSliceFromJson reads the database for an AnnotatedStringSlice stored as Json.
func (p *PropertyManager) GetAnnotatedStringSliceFromJson(key string) (*AnnotatedStringSlice, error) {
	s := new(AnnotatedStringSlice)
	err := p.GetJson(key, s)
	return s, err
}

// SetAnnotatedStringSliceAsJson stores an AnnotatedStringSlice as Json in the database
func (p *PropertyManager) SetAnnotatedStringSliceAsJson(key string, s *AnnotatedStringSlice) error {
	return p.SetJson(key, s)
}

// Card is used to store a card property in the database.
// It also exists in generators/html, but can't be imported because it would create an import cycle.
type Card struct {
	Title      string
	Text       string
	Icon       string
	ButtonText string
	ButtonUrl  string
	ImgSrc     string
	ImgAlt     string
	InsertImg  bool
}
type AnnotatedCard struct {
	Card *Card
	Info string
}

func NewAnnotatedCard() *AnnotatedCard {
	a := new(AnnotatedCard)
	a.Card = new(Card)
	return a
}

type AnnotatedCardSlice struct {
	Value []*Card
	Desc  string
}

// GetCard TODO
func (p *PropertyManager) GetCard(key string) (*Card, error) {
	return nil, nil
}

// GetAnnotatedCard TODO
func (p *PropertyManager) GetAnnotatedCard(key string) (*AnnotatedCard, error) {
	return nil, nil
}

// GetAnnotatedCardSlice TODO
func (p *PropertyManager) GetAnnotatedCardSlice(key string) (*AnnotatedCardSlice, error) {
	return nil, nil
}
func (p *PropertyManager) SetCard(dir string, c *Card) error {
	p.KP.Dirs.Push(dir)

	err := p.SetAnnotatedString("buttonText", c.ButtonText, "If set, your card will have a button, and this is the text inside the button.")
	if err != nil {
		return err
	}
	err = p.SetAnnotatedString("buttonUrl", c.ButtonUrl, "If set, when the card button is clicked, the page will open to this url.")
	if err != nil {
		return err
	}
	err = p.SetAnnotatedString("icon", c.Icon, "If set, your card will use this Bootstrap icon.")
	if err != nil {
		return err
	}
	err = p.SetAnnotatedString("imageAlt", c.ImgAlt, "If set, readers for visually impaired users will use this to describe the image.")
	if err != nil {
		return err
	}
	err = p.SetAnnotatedString("imageSource", c.ImgSrc, "The image file to use from the img directory.")
	if err != nil {
		return err
	}
	err = p.SetAnnotatedString("title", c.Title, "If set, this will be the title of your card")
	if err != nil {
		return err
	}

	p.KP.Dirs.Pop()

	return nil
}
func (p *PropertyManager) SetAnnotatedCard(dir string, c *AnnotatedCard) error {
	p.KP.Dirs.Push(dir)

	// set value
	err := p.SetCard("value", c.Card)
	if err != nil {
		return err
	}
	// set description
	err = p.SetString("desc", c.Info)
	if err != nil {
		return err
	}

	p.KP.Dirs.Pop()

	return nil
}
func (p *PropertyManager) SetAnnotatedCardSlice(key string, val []*Card, desc string) error {
	return nil
}

type RedirectMapper struct {
	pointedTo   map[string][]string
	pointedFrom map[string]string
}

// PointsTo returns the ID of the record that the specified record redirects to.
// If the id is not found, returns an error
func (m *RedirectMapper) PointsTo(id string) (string, error) {
	if v, found := m.pointedFrom[id]; found {
		return v, nil
	} else {
		return "", fmt.Errorf("%s not used as a redirect", id)
	}
}

// PointedFrom returns the IDs of records that redirect to the one passed in as a parameter
// If the id is not found, returns an error
func (m *RedirectMapper) PointedFrom(id string) ([]string, error) {
	if v, found := m.pointedTo[id]; found {
		return v, nil
	} else {
		return nil, fmt.Errorf("%s not used as a redirect", id)
	}
}

// LineParser parses the line using the delimiter and from it
// creates and validates a key path and returns the key path
// the record value (as a byte slice), and any error.
// If removeQuotes == true, enclosing quotes will be removed
type LineParser func(line, delimiter string, removeQuotes bool, replace *DirReplace) (*KeyPath, []byte, error)

type DirReplace struct {
	index int
	value string
}
