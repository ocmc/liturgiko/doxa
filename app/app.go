// Package app is a static singleton that provides the context information
// and handles to resources used by many of the other packages.
package app

import (
	"context"
	"crypto/sha256"
	"embed"
	"encoding/base64"
	"errors"
	"fmt"
	"github.com/asaskevich/EventBus"
	"github.com/c-bata/go-prompt"
	"github.com/liturgiko/doxa/cli"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/catalog"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/dvcs"
	"github.com/liturgiko/doxa/pkg/enums/commands"
	"github.com/liturgiko/doxa/pkg/enums/goos"
	"github.com/liturgiko/doxa/pkg/enums/properties"
	"github.com/liturgiko/doxa/pkg/enums/versionStatuses"
	"github.com/liturgiko/doxa/pkg/keyring"
	"github.com/liturgiko/doxa/pkg/kvsw"
	"github.com/liturgiko/doxa/pkg/syncsrvc"
	"github.com/liturgiko/doxa/pkg/sysManager"
	"github.com/liturgiko/doxa/pkg/threadsafe"
	"github.com/liturgiko/doxa/pkg/updater"
	"github.com/liturgiko/doxa/pkg/users"
	"github.com/liturgiko/doxa/pkg/utils/clinput"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/vault"
	"github.com/spf13/viper"
	"github.com/xanzy/go-gitlab"
	"net"
	"net/http"
	"os"
	"path"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"
)

var Ctx = new(CtxMeta)

const DoxaSys = "doxaSys"
const DefaultGitlabHost = "gitlab.com"
const DefaultGitlabApiPathSuffix = "/api/v4"

type CtxMeta struct {
	BackupFrequency             int // minutes
	BuildSyncMutex              sync.Mutex
	Cloud                       bool
	Command                     commands.Command
	ConfigName                  string
	Bus                         EventBus.Bus //See note at bottom of file for event types
	DbM                         *cli.DbM
	Debug                       bool
	Dev                         bool // if true, we are running as a Doxa golang developer
	Docker                      bool
	DoxaHome                    string
	DoxaUpdater                 *updater.DoxaUpdater
	DoxaUpdateAvailable         bool
	DataStores                  *kvs.Stores
	ds                          *kvs.BoltKVS
	dsSystem                    kvs.KVI
	EmbeddedScriptsPatch        *embed.FS
	GitEnabled                  bool
	GitlabHost                  string
	GitlabApi                   string
	GitScriptManager            *dvcs.GitScriptManager
	GoOS                        goos.GoOs
	InternetAvailable           bool
	OnlineInterval              time.Duration
	OfflineInterval             time.Duration
	Keyring                     *keyring.KeyRing
	Kvs                         *kvs.KVS
	LocalHostName               string
	LocalUserName               string
	LocalUserCanUpdateGroup     bool // user's authority for the group is developer or higher
	LocalUidHost                string
	LocalGitWorkingBranch       string
	LocalPromptForGitlab        bool
	mu                          sync.Mutex
	Paths                       *config.Paths
	PM                          *kvs.PropertyManager
	PortApi                     string
	PortApp                     string
	PortMedia                   string
	PortMessage                 string
	PortSitePublic              string
	PortSiteTest                string
	Project                     string
	ProjectPushData             *dvcs.RepoList
	Realm                       string
	Site                        string
	SubCommand                  commands.Command
	SubscribeToFrS              bool
	SubscriptionUpdateAvailable bool
	SubscriptionVersion         string
	Suggestions                 []prompt.Suggest
	SyncManager                 *syncsrvc.Manager
	SyncSimulation              bool // does not actually pull or push. Set via config.yaml, syncSim: true/false, or shell
	SysManager                  *sysManager.SysManager
	CatalogManager              *catalog.Manager
	OfflineSimulation           bool
	RoAutoSyncRequestedInConfig bool
	RwAutoSyncRequestedInConfig bool
	TTY                         bool
	UserHome                    string
	UserManager                 *users.Manager
	Wrapper                     *kvsw.Wrapper // wraps the kvs to provide application specific methods
}

func Init(cliRequestSyncSubsSim, cliRequestSyncMaintainedSim bool, embeddedScriptsPatch *embed.FS) {
	// set the port numbers
	Ctx.PortApp = "8080"        // overwritten by config setting in database
	Ctx.PortSiteTest = "8085"   // overwritten by config setting in database
	Ctx.PortSitePublic = "8086" // overwritten by config setting in database
	Ctx.PortMedia = "8095"      // overwritten by config setting in database
	Ctx.PortMessage = "9000"    // overwritten by config setting in database
	inDockerStr := os.Getenv("IN_DOCKER")
	if inDockerStr == "true" {
		Ctx.Docker = true
		doxlog.Info("running in docker")
	}
	// shutdown any open instance of doxa.
	// we can only have a single process accessing the database.
	shutdown()
	Ctx.Bus = EventBus.New()
	Ctx.EmbeddedScriptsPatch = embeddedScriptsPatch
	authorizedUsers, doxaToken, err := readConfigFile()
	if err != nil {
		doxlog.Panic(err.Error())
	}
	if Ctx.Cloud {
		if len(doxaToken) == 0 {
			// see if it is set in the environment
			doxaToken = os.Getenv("TOKEN")
			if len(doxaToken) == 0 {
				doxlog.Panic("in cloud, but doxaSys token not set")
			}
		}
	}
	initDoxaPaths()
	//checkSeed()
	Ctx.Bus.Subscribe("gitlab:tokenSet", func(token string) {
		if Ctx.UserManager != nil {
			prevToken, _ := Ctx.UserManager.GetToken(users.LocalUser)
			if prevToken != token {
				if Ctx.UserManager.SetToken(users.LocalUser, token) == nil {
				}
			}
			Ctx.UserManager.IsDecrypted = true
		}
	})
	Ctx.Bus.Subscribe("gitlab:tokenNeeded", func() {
		if !Ctx.UserManager.IsDecrypted {
			//auth when needed
			authWithCancel()
		}
	})
	//TODO: persistent DB
	Ctx.OnlineInterval = 5 * time.Minute
	Ctx.OfflineInterval = 1 * time.Minute
	StartInternetChecker()
	// create the vault manager
	vaultManager, err := vault.NewManager(Ctx.Paths.VaultPath)
	if err != nil {
		doxlog.Error(fmt.Sprintf("error opening vault: %v", err))
	}
	// create the map of data stores
	Ctx.DataStores = kvs.NewStores()

	// create the sys manager
	err = initSysDb()
	if err != nil {
		doxlog.Error(fmt.Sprintf("error creating system database: %v", err))
	}
	// create the user manager
	Ctx.UserManager, err = users.NewManager(Ctx.Paths.VaultPath, vaultManager)
	if err != nil {
		doxlog.Error(fmt.Sprintf("error creating user manager: %v", err))
	}
	if authorizedUsers != nil {
		for _, u := range authorizedUsers {
			err = Ctx.UserManager.SetToken(u.UID, u.PWD)
			if err != nil {
				doxlog.Errorf("error setting token for %s: %v", u.UID, err)
			}
			if !Ctx.UserManager.IsValidToken(u.UID, u.PWD) {
				doxlog.Errorf("encrypt / write of PWD for %s does not match read / decrypt", u.UID)
			}
		}
		if len(doxaToken) > 0 {
			err = Ctx.UserManager.SetToken(DoxaSys, doxaToken)
			if err != nil {
				doxlog.Errorf("error setting token for %s: %v", DoxaSys, err)
			}
		}
	}
	err = InitDatabase()
	if err != nil {
		doxlog.Panic(err.Error())
	}
	//deleteObsoleteProperties()
	err = initializeHelpers()
	if err != nil {
		doxlog.Panic(err.Error())
	}

	myDvcs, err := dvcs.NewDvcsClientLocalOnly("assets", Ctx.Paths.ProjectDirPath)
	if err != nil {
		doxlog.Errorf("Could not create local git client: %v", err)
		doxlog.Info("NewDvcsClientLocalOnly failed. attempting Manual creation. Sync will be disabled")
		myDvcs = new(dvcs.DVCS)
		myDvcs.Git = new(dvcs.Git)
		myDvcs.Gitlab = new(dvcs.Gitlab) //this one needs more to prevent abend.
	}
	Ctx.Bus.SubscribeAsync("gitlab:tokenSet", func(token string) {
		if !Ctx.InternetAvailable {
			var netwatcher func()
			netwatcher = func() {
				err := initGitlab(token, myDvcs)
				if err != nil {

				}
				Ctx.Bus.Unsubscribe("net:available", netwatcher)
			}
			Ctx.Bus.SubscribeAsync("net:available", netwatcher, false)
			return //TODO: listen for internet to become available
		}
		err := initGitlab(token, myDvcs)
		if err != nil {

		}

	}, false)

	ds := Ctx.DataStores.Get(config.SystemDbName)
	Ctx.SyncManager = syncsrvc.NewManager(&syncsrvc.ManagerParms{
		Ds:           Ctx.Wrapper,
		Dvcs:         myDvcs,
		Keyring:      Ctx.Keyring,
		GitlabApi:    Ctx.GitlabApi,
		Paths:        Ctx.Paths,
		Pm:           Ctx.PM,
		Token:        doxaToken,
		RoAutoSyncOn: Ctx.RoAutoSyncRequestedInConfig,
		RwAutoSyncOn: Ctx.RwAutoSyncRequestedInConfig,
		SysDs:        ds.Kvs,
		LtxDs:        Ctx.Kvs,
		Bus:          Ctx.Bus,
		BuilderMutex: &Ctx.BuildSyncMutex,
		SysMan:       Ctx.SysManager,
	})
	//initialize catalogManager
	Ctx.CatalogManager, err = catalog.NewManager(Ctx.DataStores.Get(config.SystemDbName).Kvs, Ctx.Bus, myDvcs, Ctx.GitlabHost)
	if err != nil {
		doxlog.Errorf("Could not create catalog manager: %v", err)
	} else {
		//Ctx.CatCmd = cli.NewCatCmd(Ctx.CatalogManager)
		/*cata := Ctx.CatalogManager.GetPrimarySubscriptionCatalog()
		if cata != nil {
			if cata.Name != "" {
				config.SM.SetValueByType(properties.SiteBuildTemplatesFallbackDir, cata.Name)
				config.SM.SetValueByType(properties.SiteBuildTemplatesFallbackEnabled, "true")
				Ctx.Paths.FallbackTemplatesPath = path.Join(Ctx.Paths.SubscriptionsPath, config.TemplatesDir)
			}
		}*/
	}
	if state, _ := Ctx.SysManager.LoadCurrentProjectSettingByKey("ro", "state"); state == "dirty" {
		doxlog.Warnf("Last sync ended unexpectedly, restoring to pre-sync state...")
		syncsrvc.RestoreDataFromSavepoint(Ctx.SysManager, Ctx.CatalogManager, Ctx.Wrapper, path.Dir(Ctx.Paths.FallbackTemplatesPath))
	}
	go Ctx.CatalogManager.RebuildInventory(Ctx.Kvs)
}

func initGitlab(token string, myDvcs *dvcs.DVCS) error {
	tmpClient, err := gitlab.NewClient(token, gitlab.WithBaseURL(Ctx.GitlabApi))
	if err != nil {
		doxlog.Errorf("Could not create gitlab client: %v", err)
		return err
	}
	groups, _, err := tmpClient.Groups.SearchGroup(Ctx.Project)
	if err != nil {
		return err
	}
	for _, group := range groups {
		if group.Path == Ctx.Project {

			myDvcs.Gitlab, err = dvcs.NewGitlabClient(token, group.ID, Ctx.Paths.ProjectDirPath, Ctx.GitlabApi, true)
			if err != nil {
				doxlog.Errorf("Could not create gitlab client: %v", err)
				return err
			}
			doxlog.Infof("Gitlab client created succesfully")
			return err
		}
	}
	msg := fmt.Sprintf("Could not find group %s in your gitlab groups. Make sure group has been created and token is correct", Ctx.Project)
	doxlog.Error(msg)
	myDvcs.Gitlab = new(dvcs.Gitlab) //we may need more
	return fmt.Errorf(msg)
}

func authWithCancel() {
	//TODO: expand to allow input to be cancellable
	_, cancel := context.WithCancel(context.Background())
	defer cancel()

	if !Ctx.UserManager.HasToken(users.LocalUser) {
		if ResetTokenAndPassword("") {
			return
		}
		return
	}
	fmt.Println("Enter Password (will not be visible) or type `q` to quit")
	password, err := clinput.GetPassword(false)

	if password == "q" {
		return
	}

	//valid, err := Ctx.UserManager.Vault.SetGitlabCredentials(users.LocalUser, password)
	Ctx.UserManager.Vault.AddKey(users.LocalUser, sha256.Sum256([]byte(password)))
	token, err := Ctx.UserManager.GetToken(users.LocalUser)
	if err != nil {
		fmt.Printf("Invalid password or token not set (%v)\n", err)
		return
	}
	if !strings.HasPrefix(token, "glpat") {
		fmt.Printf("invalid password")
		return
	}
	fmt.Println("You are now logged in")
	go Ctx.Bus.Publish("gitlab:tokenSet", token) //must be goroutine to avoid deadlock
}

// ResetTokenAndPassword currently issues a cli request for a new token and/or password.
// if token is "" a user is prompted for a new token. otherwise the supplied token is evaluated
func ResetTokenAndPassword(token string) bool {
	if token == "" {
		fmt.Println("Enter your gitlab token (will not be visible) or type `q` to quit")
		token, _ = clinput.GetPassword(false)
		if token == "q" {
			return true
		}
	}
	if !strings.HasPrefix(token, "glpat") {
		fmt.Println("Invalid token")
		return true
	}
	fmt.Println("Now enter a new password (type `q` to quit)")
	fmt.Printf("Password: ")
	pwd, err := clinput.GetPassword(true)
	if err != nil {
		fmt.Printf("could not get password: %v\n", err)
	}
	Ctx.UserManager.Vault.AddKey(users.LocalUser, sha256.Sum256([]byte(pwd)))
	Ctx.UserManager.IsDecrypted = true
	err = Ctx.UserManager.SetToken(users.LocalUser, token)
	if err != nil {
		fmt.Printf("could not save token:%v\n", err)
		return true
	}
	fmt.Println("token saved successfully")
	go Ctx.Bus.Publish("gitlab:tokenSet", token) //must be goroutine to avoid deadlock
	return false
}

func checkSeed() {
	var err error
	/*
		Scenarios:
		  1. Running in the cloud.  If this is the case, then
		     the seed must be passed through an environmental setting.
	*/
	// we are running in the cloud.
	// See if the seed file exists.  If not, create it.
	var key []byte
	var seed string
	if ltfile.FileExists(Ctx.Paths.SeedPath) {
		seed, err = ltfile.GetFileContent(Ctx.Paths.SeedPath)
		if err != nil {
			doxlog.Errorf("error reading seed %v", err)
			return
		}
		var decryptedSeed []byte
		decryptedSeed, err = vault.DecryptWithKey(huwari, []byte(seed))
		if err != nil {
			doxlog.Errorf("error decrypting seed %v", err)
			return
		}
		var decodedKey []byte
		decodedKey, err = base64.StdEncoding.DecodeString(string(decryptedSeed))
		if err != nil {
			doxlog.Errorf("error decoding DOXA_SEED: %v", err)
		}
		seed = string(decodedKey)
	} else {
		key = vault.NewEncryptionKey()
		encodedKey := base64.StdEncoding.EncodeToString(key)
		if len(encodedKey) > 32 {
			encodedKey = encodedKey[:32]
		}
		// set a temporary key
		err = os.Setenv("DOXA_SEED", string(huwari))
		if err != nil {
			return
		}
		var decryptedKey []byte
		decryptedKey, err = vault.EncryptWithKey(huwari, []byte(encodedKey))
		if err != nil {
			doxlog.Errorf("error decrypting seed %v", err)
			return
		}
		err = ltfile.WriteFile(Ctx.Paths.SeedPath, string(decryptedKey))
		if err != nil {
			doxlog.Errorf("error writing seed %v", err)
			return
		}
		seed = encodedKey
	}
	err = os.Setenv("DOXA_SEED", seed)
	if err != nil {
		doxlog.Errorf("error setting environmental variable DOXA_SEED: %v", err)
		return
	}
}
func deleteObsoleteProperties() {
	var dbBackedUp bool
	var err error
	for _, o := range properties.ObsoleteProperties {
		matcher := kvs.NewMatcher()
		matcher.KP.Dirs.Push("configs")
		matcher.ValuePattern = path.Join("configs", ".*", o)
		matcher.Recursive = true
		matcher.IncludeEmpty = true
		err = matcher.UseAsRegEx()
		if err != nil {
			doxlog.Errorf("error parsing regex pattern: %v", err)
			return
		}
		var dbRecs []*kvs.DbR

		dbRecs, err = Ctx.Kvs.Db.GetMatchingIDs(*matcher)
		if err != nil {
			doxlog.Errorf("error searching for obsolete property %s: %v", o, err)
			return
		}
		if len(dbRecs) > 0 && !dbBackedUp {
			var backupDir string
			backupDir, err = BackupDb()
			if err != nil {
				doxlog.Errorf("error backing up database prior to deleting obsolete properties: %v", err)
				return
			}
			doxlog.Infof("backed up database to %s", backupDir)
			dbBackedUp = true
		}
		for _, dbr := range dbRecs {
			if strings.Contains(dbr.KP.Path(), o) {
				doxlog.Infof("deleting obsolete property records for property %s", o)
				err = Ctx.Kvs.Db.Delete(*dbr.KP)
				if err != nil {
					doxlog.Errorf("error deleting obsolete property %s: %v", o, err)
					continue
				}
			}
		}
	}
}
func BackupDb() (backupPath string, err error) {
	doxlog.Info("backing up database file")
	backupDbDir := path.Join(Ctx.Paths.BackupPath, "db")
	backupPath, err = Ctx.Wrapper.Backup(backupDbDir, 1)
	if err != nil {
		msg := fmt.Sprintf("error backing up db %s: %v", backupDbDir, err)
		doxlog.Error(msg)
		return backupPath, fmt.Errorf(msg)
	}
	doxlog.Infof("current db backed up to %s", backupPath)
	// export all records to backups/exports
	backupExportsDir := path.Join(Ctx.Paths.ProjectDirPath, "resources")
	doxlog.Infof("exporting all records to %s", backupExportsDir)
	err = Ctx.Wrapper.ExportAll(backupExportsDir)
	if err != nil {
		return backupPath, err
	}
	return backupPath, nil
}
func setUserHome() {
	Ctx.GoOS = goos.CodeForString(runtime.GOOS)
	if Ctx.GoOS == goos.Linux {
		Ctx.UserHome = "/var/local"
		doxlog.Infof("linux os detected: doxa home in %s", Ctx.UserHome)
	} else {
		var err error
		Ctx.UserHome, err = os.UserHomeDir()
		Ctx.UserHome = ltfile.ToUnixPath(Ctx.UserHome)
		if err != nil {
			doxlog.Panicf("unable to determine user home directory: %v\n", err)
		}
	}
}
func ReInitialize() {
	initDoxaPaths()
	ReOpenDatabase()
}
func getDotDoxa() string {
	if len(Ctx.UserHome) == 0 {
		setUserHome()
	}
	return path.Join(Ctx.UserHome, ".doxa")
}
func initDoxaPaths() {
	err := config.InitDoxaPaths(Ctx.GitlabHost, Ctx.UserHome, Ctx.DoxaHome, Ctx.Project)
	if err != nil {
		doxlog.Panic(fmt.Sprintf("error setting doxa paths: %v", err))
	}
	Ctx.Paths = config.DoxaPaths
}
func CloseDatabase() {
	err := Ctx.ds.Db.Close()
	if err != nil {
		doxlog.Error(err.Error())
	}
}
func initSysDb() error {
	dbPath := path.Join(Ctx.Paths.DbPathSystem, config.SystemDbName)
	// open the database
	sysDb, err := kvs.NewBoltKVS(dbPath)
	Ctx.dsSystem = threadsafe.NewThreadsafeKVS(sysDb, time.Duration(15*time.Minute), time.Duration(30*time.Minute))
	if err != nil { // should not happen, but just in case
		if strings.Contains(err.Error(), "timeout") {
			return fmt.Errorf("system database is already open")
		}
	}
	// create the database mapper
	var sysKvs *kvs.KVS
	sysKvs, err = kvs.NewKVS(Ctx.dsSystem)
	if err != nil {
		doxlog.Panicf("error creating mapper for system database: %v\n", err)
	}
	if sysKvs == nil {
		doxlog.Panicf("system kvs is nil")
	}
	// add current project to database
	curProjRec := kvs.NewDbR()
	curProjRec.KP.Dirs.Push(sysManager.Selected)
	sysKvs.Db.MakeDir(curProjRec.KP)
	curProjRec.KP.KeyParts.Push(sysManager.SelectedProject)
	curProjRec.Value = Ctx.Project
	sysKvs.Db.Put(curProjRec)

	Ctx.DataStores.Add(&kvs.Store{
		Name:        config.SystemDbName,
		Description: "Doxa system data for specific user and machine",
		Path:        dbPath,
		Prompt:      "sysDb",
		Kvs:         sysKvs,
	})
	// create the context System database Manager
	paths := new(sysManager.Paths)
	paths.KvsPath = dbPath
	paths.DoxaHome = Ctx.DoxaHome
	paths.DoxaProjectGroupsPath = path.Join(Ctx.DoxaHome, config.ProjectsDir)
	parms := sysManager.Parms{
		Kvs:     sysKvs,
		Paths:   paths,
		InCloud: Ctx.Cloud,
	}
	Ctx.SysManager, err = sysManager.NewSysManager(&parms)
	if err != nil {
		msg := fmt.Sprintf("error creating sys manager: %v", err)
		doxlog.Error(msg)
		return errors.New(msg)
	}
	return nil
}
func initializeHelpers() error {
	var err error
	Ctx.PM = kvs.NewPropertyManager(Ctx.Kvs)

	// Get the name of the configuration to use.
	// A user can have more than one named configuration,
	// but may only use one at a time.
	// Note: all database configuration properties
	// are defined in pkg/enums/properties.
	// It provides a constant for each property as well
	// as the description, default value, and value type.
	// If you add a property, do not forget to run go generate.
	Ctx.PM.KP.Clear()
	Ctx.PM.KP.Dirs.Push("configs")
	Ctx.PM.KP.Dirs.Push("use")
	var configName string

	// if there is no configuration name found, use the default
	if configName, err = Ctx.PM.GetString("value"); err != nil {
		configName = "default"
		err = Ctx.PM.SetString("desc", "You may have multiple configurations, but only one can be used at a time.  Indicate here the configuration to use.")
		if err != nil {
			return err
		}
		err = Ctx.PM.SetString("value", configName)
		if err != nil {
			return err
		}
	}
	Ctx.ConfigName = configName
	config.InitSettingsManager(configName, Ctx.Paths.ExportPath, Ctx.Paths.ImportPath, Ctx.Kvs, Ctx.PM, Ctx.Dev) // why both PM and SM? Because the settings manager uses the property manager.
	Ctx.PM.KP.Dirs.Pop()

	err = config.SM.SyncDbWithPropertiesMap()
	if err != nil {
		doxlog.Errorf(err.Error())
	}

	// Read in the configuration settings (properties).
	// Missing settings are automatically created using defaults
	// defined in pkg/enums/properties.
	// First, let the user know whether we are initializing
	// or simply reading the settings.
	Ctx.PM.KP.Dirs.Push(configName)
	if !Ctx.Kvs.Db.Exists(Ctx.PM.KP) {
		doxlog.Infof("Config `%s` does not exist.", configName)
		doxlog.Info("Initializing configuration using default values.")
	} else {
		doxlog.Infof("Reading %s configuration settings from the database.", configName)
	}
	if err = config.SM.ReadConfiguration(); err != nil {
		doxlog.Panicf("Could not read configurations.  Did developer forget to run `go-generate` for pkg/enums/properties? %s", err)
	}
	Ctx.TTY = config.SM.BoolProps[properties.SysShellTty]
	Ctx.Wrapper.SetExportFilter(Ctx.ProjectPushData.ToStrSlice())
	return nil
}

func ReOpenDatabase() {
	err := Ctx.ds.Db.Close()
	if err != nil {
		doxlog.Error(err.Error())
	}
	err = InitDatabase()
	if err != nil {
		doxlog.Panicf("error reopening database: %v", err)
	}
	err = initializeHelpers()
	if err != nil {
		doxlog.Error(err.Error())
		return
	}
}
func InitDatabase() error {
	// open the Doxa database
	var err error
	Ctx.ds, err = kvs.NewBoltKVS(Ctx.Paths.DbPath)
	if err != nil { // should not happen, but just in case
		if strings.Contains(err.Error(), "timeout") {
			return fmt.Errorf("doxa is already running in another terminal window. You cannot have multiple instances of doxa running at the same time")
		}
	}
	// create the database mapper
	Ctx.Kvs, err = kvs.NewKVS(Ctx.ds)
	if err != nil {
		doxlog.Panicf("error creating mapper: %v\n", err)
	}
	if Ctx.Kvs == nil {
		doxlog.Panicf("Ctx.Kvs is nil")
	}
	if Ctx.Kvs.Db == nil {
		doxlog.Panicf("Ctx.Kvs is nil")
	}
	Ctx.DataStores.Add(&kvs.Store{
		Name:        config.DbName,
		Description: "doxa.tools data",
		Path:        Ctx.Paths.DbPath,
		Prompt:      "db",
		Kvs:         Ctx.Kvs,
	})
	Ctx.Wrapper, err = kvsw.NewWrapper(Ctx.Kvs)
	if err != nil {
		doxlog.Panicf("error wrapping kvs: %v\n", err)
	}
	return nil
}

type UserAuth struct {
	UID string
	PWD string
}

// readConfigFile populates var Config by
// reading information from $HOME/.doxa/Ctx.yaml
// If .doxa does not exist or .doxa/Ctx.yaml does not exist,
// the user will be prompted for information to create it.
func readConfigFile() (users []*UserAuth, doxasys string, err error) {
	doxlog.Info("running app.readConfigFile")
	// Ctx.yaml property keys
	keyHome := "doxahome"
	keyProject := "project"
	keyDev := "dev"
	keyCloud := "cloud"
	keyDebug := "debug"
	keySubscribe := "subscribe"
	keyRoAutoSyncOn := "roAutoSyncOn"
	keyRwAutoSyncOn := "rwAutoSyncOn"
	keyGitlab := "gitlab"
	keyGitlabApiSuffix := "gitlabApiSuffix"
	ConfigFileName := "config"
	var ConfigPath string
	Ctx.GoOS = goos.CodeForString(runtime.GOOS)
	if Ctx.GoOS == goos.Linux {
		Ctx.UserHome = "/var/local"
		ConfigPath = "/usr/local/etc/doxa"
		doxlog.Infof("linux os detected: doxa home in %s", Ctx.UserHome)
	} else {
		Ctx.UserHome, err = os.UserHomeDir()
		if err != nil {
			doxlog.Panicf("unable to determine user home directory: %v\n", err)
		}
		ConfigPath = path.Join(Ctx.UserHome, ".doxa")
	}
	doxlog.Infof("checking to see if %s exists", ConfigPath)
	if !ltfile.DirExists(ConfigPath) {
		fmt.Printf("config directory %s not found, creating it\n", ConfigPath)
		err = ltfile.CreateDirs(ConfigPath)
		if err != nil {
			doxlog.Panicf("unable to create doxa config directory: %v\n", err)
		}
	} else {
		doxlog.Infof("ok, %s exists", ConfigPath)
	}

	cf := path.Join(ConfigPath, fmt.Sprintf("%s.yaml", ConfigFileName))
	doxlog.Infof("checking to see if %s exists", cf)
	if !ltfile.FileExists(cf) {
		doxlog.Infof("%s does not exist", cf)
		var rootDir string
		rootDir, err = getInstallDir()
		if err != nil {
			doxlog.Panic(err.Error())
		}

		Ctx.DoxaHome = ltfile.ToUnixPath(path.Join(rootDir, "doxa"))
		// ask use for the initial project name:
		fmt.Print("Enter a short name or abbreviation for your first project: ")
		_, err = fmt.Scanln(&Ctx.Project)
		if err != nil {
			doxlog.Panic(err.Error())
		}
		Ctx.Project = strings.ToLower(Ctx.Project)
		fmt.Printf("Prject name is %s\n", Ctx.Project)

		configSettings := []string{fmt.Sprintf("%s: %s", keyHome, Ctx.DoxaHome),
			fmt.Sprintf("%s: %v", keyCloud, Ctx.GoOS == goos.Linux),
			fmt.Sprintf("%s: false", keyDev),
			fmt.Sprintf("%s: false", keyDebug),
			fmt.Sprintf("%s: %s", keyProject, Ctx.Project)}
		err = ltfile.WriteLinesToFile(cf, configSettings)
		if err != nil {
			doxlog.Panicf("error creating config file %s: %v\n", Ctx.DoxaHome, err)
		}
	} else {
		doxlog.Infof("ok, %s exists", cf)
	}
	viper.AddConfigPath(ConfigPath)
	viper.SetConfigName(ConfigFileName)
	viper.SetConfigType("yaml")

	if err = viper.ReadInConfig(); err != nil {
		return nil, "", fmt.Errorf("error reading Ctx.yaml from %s: %v", ConfigPath, err)
	}

	Ctx.DoxaHome = ltfile.ToUnixPath(strings.TrimSpace(viper.GetString(keyHome)))
	if len(Ctx.DoxaHome) == 0 {
		doxlog.Panicf("./doxa/Ctx.yaml is missing path to doxahome.")
	}
	Ctx.RwAutoSyncRequestedInConfig = viper.GetBool(keyRwAutoSyncOn)
	Ctx.RoAutoSyncRequestedInConfig = viper.GetBool(keyRoAutoSyncOn)
	Ctx.Cloud = viper.GetBool(keyCloud)
	Ctx.Debug = viper.GetBool(keyDebug)
	Ctx.Dev = viper.GetBool(keyDev)
	Ctx.GitEnabled = viper.GetBool("git")
	Ctx.GitlabHost = viper.GetString(keyGitlab)
	if Ctx.GitlabHost == "" {
		Ctx.GitlabHost = DefaultGitlabHost
	}
	if !strings.HasPrefix(Ctx.GitlabHost, "https://") {
		if strings.HasPrefix(Ctx.GitlabHost, "http://") {
			Ctx.GitlabHost = strings.TrimPrefix(Ctx.GitlabHost, "http://")
		}
		Ctx.GitlabHost = "https://" + Ctx.GitlabHost
	}
	gitlabApi := viper.GetString(keyGitlabApiSuffix)
	if gitlabApi == "" {
		gitlabApi = DefaultGitlabApiPathSuffix
	}
	Ctx.GitlabApi = fmt.Sprintf("%s%s", Ctx.GitlabHost, gitlabApi)
	Ctx.SubscribeToFrS = viper.GetBool(keySubscribe)
	Ctx.Project = strings.ToLower(strings.TrimSpace(viper.GetString(keyProject)))
	if len(Ctx.Project) == 0 {
		doxlog.Panicf("config project undefined\n")
	}
	// make sure doxa home exists
	if !ltfile.DirExists(Ctx.DoxaHome) {
		fmt.Printf("creating doxa home dir %s\n", Ctx.DoxaHome)
		err = ltfile.CreateDirs(Ctx.DoxaHome)
		if err != nil {
			doxlog.Panicf("fatal error creating directories %s: %v\n", Ctx.DoxaHome, err)
			os.Exit(1)
		}
	}
	//var users []*UserAuth
	if viper.IsSet("users") {
		var it []string
		it = viper.GetStringSlice("users")
		for _, u := range it {
			parts := strings.Split(u, "~")
			if len(parts) == 2 {
				ua := new(UserAuth)
				ua.UID = parts[0]
				ua.PWD = parts[1]
				users = append(users, ua)
			}
		}
	}
	if viper.IsSet("doxasys") {
		doxasys = viper.GetString("doxasys")
	}
	if Ctx.Docker {
		doxlog.Info("setting backup frequency using environmental variable BACKUP")
		setBackupFrequency(os.Getenv("BACKUP"))
	} else if viper.IsSet("backup") {
		doxlog.Info("setting backup frequency using doxa config")
		setBackupFrequency(viper.GetString("backup"))
	}
	return users, doxasys, nil
}
func setBackupFrequency(intervalStr string) {
	var err error
	const defaultVal = 240
	if len(intervalStr) == 0 {
		Ctx.BackupFrequency = defaultVal
		doxlog.Infof("backup frequency set to default of %d minutes", Ctx.BackupFrequency)
		return
	}
	Ctx.BackupFrequency, err = strconv.Atoi(intervalStr)
	if err != nil {
		doxlog.Errorf("error converting %s to int: %v", intervalStr, err)
		Ctx.BackupFrequency = 4 * 60 // 4 hours
		doxlog.Infof("backup frequency set to default of %d minutes", Ctx.BackupFrequency)
	} else {
		doxlog.Infof("backup frequency set to %d minutes", Ctx.BackupFrequency)
	}
}

// getDirOptions lists the user home directory and any mounted volumes
// and prompts the user to select one for the setup of the doxa home directory.
func getDirOptions(dirPath string) (options []string, err error) {
	dirs, err := ltfile.DirsInDir(dirPath, true)
	if err != nil {
		return nil, err
	}
	var userHomeDir string
	userHomeDir, err = os.UserHomeDir()
	userHomeDir = ltfile.ToUnixPath(userHomeDir)
	if err != nil {
		return nil, err
	}
	options = append(options, fmt.Sprintf("%d", 1))
	options = append(options, userHomeDir)
	for i, d := range dirs {
		optionPath := path.Join(dirPath, d)
		options = append(options, fmt.Sprintf("%d", i+2))
		options = append(options, fmt.Sprintf("%s", optionPath))
	}
	options = append(options, "q")
	options = append(options, "quit")
	return options, nil
}
func getInstallDir() (installDir string, err error) {
	GoOS := goos.CodeForString(runtime.GOOS)
	if GoOS == goos.Linux {
		doxlog.Infof("detected linux os")
		return "/var/local", nil
	}
	doxlog.Infof("not running on linux os")

	q := "Select a directory for your doxa workspace:"
	root := ltfile.OsVolumesDir()
	o, err := getDirOptions(root)
	if err != nil {
		return "", err
	}
	selected := string(clinput.GetInput(q, o))
	if selected == "q" {
		doxlog.Info("Exiting doxa...")
		os.Exit(0)
	}
	var selectedInt int
	selectedInt, err = strconv.Atoi(selected)
	if err != nil {
		return "", err
	}
	selectedInt = (selectedInt * 2) - 1
	if selectedInt > len(o) {
		return "", fmt.Errorf("error selection > len(options)")
	}
	return o[selectedInt], nil
}
func checkDoxaVersion() {
	if Ctx.DoxaUpdater != nil {
		Ctx.DoxaUpdateAvailable = Ctx.DoxaUpdater.VersionStatus() == versionStatuses.BehindLatest
	}
}
func checkSubscriptionVersion() {
	if Ctx.CatalogManager != nil {
		//Ctx.SubscriptionUpdateAvailable = Ctx.CatalogManager.SubscriptionUpdateAvailable()
		Ctx.CatalogManager.RefreshCatalogs()
		Ctx.SubscriptionUpdateAvailable = len(Ctx.CatalogManager.GetUpdates()) > 0
	}
}
func checkInternet(address string) bool {
	// Set a timeout for the connection attempt
	timeout := 10 * time.Second
	startTime := time.Now()
	conn, err := net.DialTimeout("udp", address, timeout)
	if err != nil {
		return false // Unable to connect
	}
	defer conn.Close()
	if time.Since(startTime) > 2*time.Second {
		Ctx.Bus.Publish("net:slow")
	}
	return true // Successfully connected
}

func StartInternetChecker() {
	go func() {
		for {
			avail := checkInternet("8.8.8.8:53") //google DNS
			if avail != Ctx.InternetAvailable {
				Ctx.Bus.Publish("net:available", avail)
			}
			Ctx.InternetAvailable = avail
			if avail {
				checkDoxaVersion()
				checkSubscriptionVersion()
				time.Sleep(Ctx.OnlineInterval)
			} else {
				time.Sleep(Ctx.OfflineInterval)
			}
		}
	}()
}

// shutdown is called so that any instance of doxa already running will be shutdown
func shutdown() {
	doxlog.Info("Shutting down any previous local instance...")
	_, err := http.Get(fmt.Sprintf("http://127.0.0.1:%s/quit", Ctx.PortApp))
	if err != nil {
		doxlog.Info("No local instance running.")
	} else {
		doxlog.Info("Local instance found and stopped.")
	}
	time.Sleep(2 * time.Second)
}

// OverrideSysDb should only be used with much fear and trembling
func OverrideSysDb(ds *kvs.BoltKVS) {
	Ctx.dsSystem = ds
}

// used to encrypt / decrypt local files.  This only protects them
// if someone sees the file and opens it.
var huwari = []byte("sb51b56sb6d15vh!d6^20b0y5fbh1t6s")

/*
Event types used with EventBus
TYPE			ARGS	COMMENT
net:available		bool	indicates that the internet is available
net:slow			none	indicates that it took more than 2 seconds to connect to the google DNS server
subscriptions:sync	string	"started"|"stopped"|"paused" started indicates a sync is underway, stopped indicates it has finished, and paused means it is waiting mid-sync
subscriptions:set	string	catalogName or optionally catalog:library indicates that user modified subscription
gitlab:tokenSet		string	<token> indicates that the token has been set/decrypted.
gitlab:tokenRevoked	none	the user has logged out. this token should no longer be accessed unless tokenSet occurs again
gitlab:tokenNeeded	none	indicates that a particular routine needs gitlab access
catalog:pushed	none	indicates that the catalog was successfully pushed to gitlab
*/
