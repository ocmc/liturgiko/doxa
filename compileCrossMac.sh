GOOS=darwin GOARCH=amd64 go build -o dist/amd64_doxa
file dist/amd64_doxa
GOOS=darwin GOARCH=arm64 go build -o dist/arm64_doxa
file dist/arm64_doxa
lipo -create -output dist/doxa dist/amd64_doxa dist/arm64_doxa
file dist/doxa
