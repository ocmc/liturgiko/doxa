package dbPaths

import (
	"github.com/liturgiko/doxa/db/kvs"
	"sync"
)

func (s *sysDb) SetProject(proj string) {
	s.currentProjectPath.Set(s.ProjectPath(proj))
	s.settingsPath.Clear()
	s.subscriptionPath.Clear()
	s.inventoriesPath.Clear()
	s.fallbackPriorityPath.Clear()
}

func (s *sysDb) CatalogsPath() *kvs.KeyPath {
	//for something of this scale, it may not seem necessary
	//but this will save memory and time on longer KPs
	if s.catalogsPath.Nil() {
		s.catalogsPath.Push("catalogs")
	}
	return s.catalogsPath.Value()
}

func (s *sysDb) ProjectPath(proj string) *kvs.KeyPath {
	kp := kvs.NewKeyPath()
	kp.Dirs.Push("projects")
	kp.Dirs.Push(proj)
	return kp
}

func (s *sysDb) CurrentProjectPath() *kvs.KeyPath {
	if s.currentProjectPath.Nil() {
		s.currentProjectPath.Push("INVALID")
	}
	return s.currentProjectPath.Value()
}

func (s *sysDb) CatalogPath(catalog string) *kvs.KeyPath {
	catPat := s.CatalogsPath()
	catPat.Dirs.Push(catalog)
	return catPat
}

func (s *sysDb) VersionPath(catalog, version string) *kvs.KeyPath {
	verPat := s.CatalogPath(catalog)
	if version == "" {
		version = "latest"
	}
	verPat.Dirs.Push(version)
	return verPat
}

func (s *sysDb) EntriesPath(catalog, version string) *kvs.KeyPath {
	entPat := s.VersionPath(catalog, version)
	entPat.Dirs.Push("entries")
	return entPat
}

func (s *sysDb) EntryPath(catalog, version, entry string) *kvs.KeyPath {
	entPat := s.EntriesPath(catalog, version)
	entPat.Dirs.Push(entry)
	return entPat
}

func (s *sysDb) SubscriptionPath() *kvs.KeyPath {
	if s.subscriptionPath.Nil() {
		s.subscriptionPath.Set(s.CurrentProjectPath())
		s.subscriptionPath.Push("subscriptions")
	}
	return s.subscriptionPath.Value()
}

func (s *sysDb) FallbackPriorityPath() *kvs.KeyPath {
	if s.fallbackPriorityPath.Nil() {
		s.fallbackPriorityPath.Set(s.SettingsPath())
		s.fallbackPriorityPath.Push("fallbackPriority")
	}
	return s.fallbackPriorityPath.Value()
}

func (s *sysDb) SettingsPath() *kvs.KeyPath {
	if s.settingsPath.Nil() {
		s.settingsPath.Set(s.CurrentProjectPath())
		s.settingsPath.Push("settings")
	}
	return s.settingsPath.Value()
}

func (s *sysDb) SettingsCategoryPath(category string) *kvs.KeyPath {
	tmp := s.settingsPath.Value()
	tmp.Dirs.Push(category)
	return tmp
}

func (s *sysDb) LocalCatalogPath() *kvs.KeyPath {
	if s.inventoryPath.Nil() {
		s.inventoryPath.Set(s.CurrentProjectPath())
		s.inventoryPath.Push("catalog")
	}
	return s.inventoryPath.Value()
}

func (s *sysDb) LocalCatalogLastPublished() *kvs.KeyPath {
	tmp := s.LocalCatalogPath()
	tmp.KeyParts.Push("currentPublished")
	return tmp
}

func (s *sysDb) InventoryPath() *kvs.KeyPath {
	tmp := s.LocalCatalogPath()
	tmp.Dirs.Push("latest")
	return tmp
}

func (s *sysDb) InventoriesPath() *kvs.KeyPath {
	if s.inventoriesPath.Nil() {
		s.inventoriesPath.Push("inventories")
	}
	return s.inventoriesPath.Value()
}

type sysDb struct {
	catalogsPath         ThreadSafeKP
	inventoriesPath      ThreadSafeKP
	currentProjectPath   ThreadSafeKP
	subscriptionPath     ThreadSafeKP
	settingsPath         ThreadSafeKP
	inventoryPath        ThreadSafeKP
	fallbackPriorityPath ThreadSafeKP
}

var SYS sysDb

type ThreadSafeKP struct {
	kp *kvs.KeyPath
	mu sync.Mutex
}

func (t *ThreadSafeKP) Push(dir string) *ThreadSafeKP {
	t.mu.Lock()
	defer t.mu.Unlock()
	if t.kp == nil {
		t.kp = kvs.NewKeyPath()
	}
	t.kp.Dirs.Push(dir)
	return t
}

func (t *ThreadSafeKP) Value() *kvs.KeyPath {
	t.mu.Lock()
	defer t.mu.Unlock()
	return t.kp.Copy()
}

func (t *ThreadSafeKP) Nil() bool {
	t.mu.Lock()
	defer t.mu.Unlock()
	return t.kp == nil
}

func (t *ThreadSafeKP) Clear() {
	t.mu.Lock()
	defer t.mu.Unlock()
	t.kp = nil
}

func (t *ThreadSafeKP) Set(kp *kvs.KeyPath) {
	t.mu.Lock()
	defer t.mu.Unlock()
	t.kp = kp
}

const keyPathCacheMax = 8192 * 2 //It is very important that this is an exponent of two

type keyPathCache struct {
	real   map[string]*kvs.KeyPath
	mu     sync.RWMutex
	cap    int
	growth int
}

func (k *keyPathCache) retrieve(key string) (*kvs.KeyPath, bool) {
	k.mu.RLock()
	defer k.mu.RUnlock()
	kp, exists := k.real[key]
	return kp, exists
}
func (k *keyPathCache) store(key string, val *kvs.KeyPath) {
	k.mu.Lock()
	defer k.mu.Unlock()
	k.real[key] = val.Copy()
}

func (k *keyPathCache) check() {
	if k == nil {
		k = new(keyPathCache)
		k.cap = keyPathCacheMax >> 1
		k.growth = k.cap
	}
	k.mu.Lock()
	defer k.mu.Unlock()
	if k.real == nil || len(k.real) > k.cap {
		k.real = make(map[string]*kvs.KeyPath)
		k.growth = k.growth >> 1
		k.cap = k.cap | k.growth
	}
	if k.cap >= keyPathCacheMax || k.growth < 2 {
		k.cap = keyPathCacheMax >> 1
		k.growth = k.cap
	}
	CachedKPs = k
}

// CachedKPs is the singleton used for caching. It lazy-initializes when needed
var CachedKPs *keyPathCache

// ExtractCatalogKP takes a keypath, and checks if it includes a catalog.
// if it does, the path to the catalog (inclusive) is returned.
// if the path does not seem to contain a catalog, nil is returned
func (k *keyPathCache) ExtractCatalogKP(inputKp *kvs.KeyPath, db *kvs.KVS) *kvs.KeyPath {
	k.check()
	if k == nil {
		k = CachedKPs
	}
	if solved, verily := k.retrieve(inputKp.Path()); verily {
		return solved
	}
	//catalog will always have a sub-directory "latest"
	revKp := inputKp.Dirs.Slice()
	//slices.Reverse(revKp)

	braveNewKp := kvs.NewKeyPath()
	for _, dirName := range revKp {
		braveNewKp.Dirs.Push(dirName)
		//braveNewKp.Dirs.Push("latest")
		if db.Db.Exists(kpChild(braveNewKp, "latest")) {
			//	braveNewKp.Dirs.Pop() //remove "latest"
			k.store(braveNewKp.Path(), braveNewKp)
			return braveNewKp
		}
		//braveNewKp.Dirs.Pop() //remove latest
	}
	return nil
}

func kpChild(parentKp *kvs.KeyPath, child string) *kvs.KeyPath {
	kp := parentKp.Copy()
	kp.Dirs.Push(child)
	return kp
}
