// Package search uses lunr to provide the means to create an abstract of each generated webpage
// in a format usable by the search page of a generated liturgical website.
// Users can statically search the liturgical website.
// The file search.json is created by the Index function in this package,
// and contains the information to be used by the website search page.
// MAC: disabled for now because takes up too much disk space.
package search

import (
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/models"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"os"
	"path"
	"sort"
	"strings"
)

type Indexer struct {
	bc             *models.BuildConfig
	DcsPath        string
	Docs           map[string]map[string]*Doc
	HtmlPath       string
	kvs            *kvs.KVS
	LiturgicalPath string
	ltxDir         string
	Pages          []*Page
	refDir         string
	refDirPath     string
	TopicKeyHrefs  map[string]string
	versions       map[string]string
}

// NewIndexer initializes an indexer for the specified DCS path.
func NewIndexer(bc *models.BuildConfig, refDir, ltxDir string, kvs *kvs.KVS) (*Indexer, error) {
	//b.Config.titles, b.Config.Versions, b.Config.SitePath
	i := new(Indexer)
	i.bc = bc
	i.DcsPath = bc.SitePath
	i.ltxDir = ltxDir
	i.refDir = refDir
	i.refDirPath = path.Join(i.DcsPath, refDir)
	i.LiturgicalPath = path.Join(i.refDirPath, ltxDir)
	//i.titles =
	i.versions = bc.Versions
	i.kvs = kvs
	if !ltfile.DirExists(i.DcsPath) {
		return i, fmt.Errorf("%s does not exist", i.DcsPath)
	}
	i.HtmlPath = path.Join(i.DcsPath, "h")
	if !ltfile.DirExists(i.HtmlPath) {
		return i, fmt.Errorf("%s does not exist", i.HtmlPath)
	}
	i.Docs = make(map[string]map[string]*Doc)
	i.TopicKeyHrefs = make(map[string]string)
	return i, nil
}
func (i *Indexer) RemoveRefDir() error {
	return ltfile.DeleteDirRecursively(i.refDirPath)
}
func (i *Indexer) ToJson(pretty bool) (string, error) {
	var j []byte
	var err error
	if pretty {
		j, err = json.MarshalIndent(i.Pages, "", " ")
	} else {
		j, err = json.Marshal(i.Pages)
	}
	if err != nil {
		return "", fmt.Errorf("error marshaling json: %v", err)
	}
	return string(j), nil
}

// Index creates an index of the occurrence of all text in the generated website.
func (i *Indexer) Index() error {
	files, err := ltfile.FileMatcher(i.HtmlPath, "html", nil)
	if err != nil {
		return err
	}
	if len(files) == 0 {
		return fmt.Errorf("no html files found in %s", i.HtmlPath)
	}
	var p *Page
	for _, f := range files {
		p, err = NewPage(f, i.bc)
		if err != nil {
			return err
		}
		_, err = p.Parse()
		if err != nil {
			return err
		}
		for _, v := range p.Rows {
			for _, c := range v {
				if len(strings.TrimSpace(c.Body)) == 0 {
					continue
				}
				var m map[string]*Doc
				var doc *Doc
				var ok bool
				if m, ok = i.Docs[c.Language]; !ok {
					m = make(map[string]*Doc)
				}
				if doc, ok = m[c.ID]; !ok {
					doc = NewDoc()
					doc.Entry.ID = c.ID
					doc.Entry.Language = c.Language
					doc.Entry.Library = c.Library
					doc.Entry.Topic = c.Topic
					doc.Entry.Desc = c.TopicDesc
					doc.Entry.Key = c.Key
					doc.Entry.SetTopicKeyHref(i.refDir, i.ltxDir)
					doc.Entry.Version = i.versions[c.Library]
					if len(doc.Entry.Version) == 0 {
						doc.Entry.Version = c.Library
					}
					doc.Entry.Text = c.Body
					i.AddTopicKeyHref(c.Topic, c.Key)
				}
				l := new(Link)
				l.Href = c.Href
				l.Row = c.Row
				l.Desc = fmt.Sprintf("%s (%s)", c.Title, p.Languages)
				doc.Links = append(doc.Links, l)
				m[c.ID] = doc
				i.Docs[c.Language] = m
			}
		}
		i.Pages = append(i.Pages, p)
	}
	go func() {
		err = i.writeIndexes()
		if err != nil {
			doxlog.Errorf("error writing search indexes: %v", err)
		}
	}()
	//go i.WriteTopicKeyPages()
	return nil
}

// AddTopicKeyHref converts the topic key into an HREF and adds it to the map if it does not already exist
// These are used downstream to write topic-key pages to the generated website with the
// path ltx/topic/key.  These are linked to in the website search results.
func (i *Indexer) AddTopicKeyHref(topic, key string) {
	tk := path.Join(topic, key)
	if _, ok := i.TopicKeyHrefs[tk]; !ok {
		i.TopicKeyHrefs[tk] = path.Join(i.ltxDir, topic, key)
	}
}

// GetTopicKeyHrefs converts the values of the Index TopicKeyRefs into a sorted string slice
func (i *Indexer) GetTopicKeyHrefs() []string {
	var tkRefs []string
	for _, r := range i.TopicKeyHrefs {
		tkRefs = append(tkRefs, r)
	}
	sort.Strings(tkRefs)
	return tkRefs
}

type Doc struct {
	Entry *Entry
	Links []*Link
}

func NewDoc() *Doc {
	d := new(Doc)
	d.Entry = new(Entry)
	return d
}

type Entry struct {
	ID       string `json:"id"`
	TkHref   string `json:"tkHref""` // href to retrieve topic-key page in generated site
	Language string `json:"language"`
	Library  string `json:"library"`
	Topic    string `json:"topic"`
	Desc     string `json:"desc"`
	Key      string `json:"key"`
	Text     string `json:"text"`
	Version  string `json:"version"`
}

func (e *Entry) SetTopicKeyHref(refDir, ltxDir string) {
	e.TkHref = path.Join(refDir, ltxDir, e.Topic, e.Key)
}

type Link struct {
	Desc string `json:"desc"`
	Href string `json:"href"`
	Row  string `json:"row"`
}

// RowCell provides a structured means for users to search the generated site.
type RowCell struct {
	Href      string `json:"id"`
	Row       string `json:"row"`
	Title     string `json:"title"`
	Body      string `json:"body"`
	ID        string `json:"-"`
	Language  string `json:"language"`
	Library   string `json:"library"`
	Topic     string `json:"topic"`
	TopicDesc string `json:"topicDesc"`
	Key       string `json:"key"`
}

// Page provides a structured means for users to search the website
type Page struct {
	bc        *models.BuildConfig
	Href      string `json:"id"`
	Languages string `json:"languages"`
	Path      string `json:"-"`
	Rows      map[string][]*RowCell
	Title     string `json:"title"`
}

func NewPage(path string, bc *models.BuildConfig) (*Page, error) {
	p := new(Page)
	p.bc = bc
	p.Path = path
	if !ltfile.FileExists(p.Path) {
		return nil, fmt.Errorf("%s does not exist", p.Path)
	}
	p.Rows = make(map[string][]*RowCell)
	return p, p.setRelativePath()
}
func (p *Page) setRelativePath() error {
	sep := "/"
	sep = fmt.Sprintf("%sh%s", sep, sep)
	i := strings.Index(p.Path, sep)
	if i == -1 {
		msg := fmt.Sprintf("could not find start of html path in %s", p.Path)
		doxlog.Error(msg)
		return fmt.Errorf(msg)
	}
	p.Href = p.Path[i+1:]
	return nil
}
func (p *Page) Parse() (map[string][]*RowCell, error) {
	r, err := os.Open(p.Path)
	if err != nil {
		msg := fmt.Sprintf("error getting reader for %s: %v", p.Path, err)
		doxlog.Error(msg)
		return nil, fmt.Errorf(msg)
	}
	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(r)
	if err != nil {
		msg := fmt.Sprintf("error getting goquery doc for %s: %v", p.Path, err)
		doxlog.Error(msg)
		return nil, fmt.Errorf(msg)
	}
	/*
		  use data-title to get the title in the site language
			<head>
			    <title data-timestamp="Wed, 16 Nov 2022 10:49:34 -0800" data-language="es" data-title="Breve Santificación del Agua"
	*/
	var ok bool
	title := doc.Find("title").First()
	if title == nil {
		p.Title = "unknown"
	} else {
		if p.Title, ok = title.Attr("data-title"); !ok {
			p.Title = "unknown"
		}
		if p.Languages, ok = title.Attr("data-language"); !ok {
			p.Languages = "unknown"
		}
		parts := strings.Split(p.Languages, "-")
		sb := strings.Builder{}
		var langName string
		for _, part := range parts {
			if sb.Len() > 0 {
				sb.WriteString("-")
			}
			if langName, _ = p.bc.GetTitle(part); ok {
				sb.WriteString(langName)
			} else {
				sb.WriteString(part)
			}
		}
		p.Languages = sb.String()
	}
	//// Find the review items
	doc.Find("tr").Each(func(i int, row *goquery.Selection) {
		var rowId string
		var ok bool
		if rowId, ok = row.Attr("id"); !ok {
			rowId = ""
		}
		row.Find(".kvp").Each(func(i int, kvp *goquery.Selection) {
			var idUsed string
			if idUsed, ok = kvp.Attr("data-id-used"); ok {
				rc := new(RowCell)
				rc.Href = p.Href
				rc.Row = rowId
				rc.Title = p.Title
				rc.ID = idUsed
				rc.Library, rc.Topic, rc.Key, err = ltstring.ToIDParts(idUsed)
				rc.TopicDesc = p.TopicDesc(rc.Topic, rc.Key)
				if err == nil {
					rc.Language, err = ltstring.LangFromLibrary(rc.Library)
					if err == nil {
						rc.Body = kvp.Text()
						var rowCells []*RowCell
						if rowCells, ok = p.Rows[rc.Language]; !ok {
						}
						rowCells = append(rowCells, rc)
						p.Rows[rc.Language] = rowCells
					}
				}
			}
		})
	})
	return nil, nil
}
func (p *Page) TopicDesc(topic, key string) string {
	parts := strings.Split(topic, ".")
	sb := strings.Builder{}
	for _, part := range parts {
		var title string
		var ok bool
		if sb.Len() > 0 {
			sb.WriteString(" ")
		}
		if title, ok = p.bc.GetTitle(part); ok {
			sb.WriteString(title)
			if part == "ps" {
				if strings.HasPrefix(key, "psa") {
					psalmParts := strings.Split(key, ".") //"psa142.text"
					if len(psalmParts) > 1 {
						sb.WriteString(" - ")
						if ps, ok := p.bc.GetTitle("psalm"); ok {
							sb.WriteString(ps)
						} else {
							sb.WriteString("Psalm ")
						}
						sb.WriteString(" ")
						sb.WriteString(psalmParts[0][3:])
					}
				}
			}
		} else {
			sb.WriteString(part)
		}
	}
	return sb.String()
}

type Lang struct {
	Name  string
	Index int
}
type IndexFile struct {
	Langs []*Lang
	Docs  [][]*Doc
}

func (i *Indexer) WriteTopicKeyPages() {
	for _, tk := range i.GetTopicKeyHrefs() {
		parts := strings.Split(tk, "/")
		if len(parts) != 3 {
			fmt.Printf("error parsing %s\n", tk)
			continue
		}
		matcher := kvs.NewMatcher()
		matcher.KP.Dirs.Push(parts[0])
		matcher.KP.Dirs.Push(parts[1])
		matcher.KP.KeyParts.Push(parts[2])
		matcher = matcher.TopicKeyMatcher()
		recs, _, err := i.kvs.Db.GetMatching(*matcher)
		if err != nil {
			fmt.Printf("error comparing %s: %v", tk, err)
		}
		for _, r := range recs {
			fmt.Printf("%s: %s\n", r.KP.Path(), r.Value)
		}

	}
}

func (i *Indexer) writeIndexes() error {
	indexFile := new(IndexFile)
	index := 0
	for l, m := range i.Docs {
		var langName string
		var ok bool
		lang := new(Lang)
		lang.Index = index
		index++
		if langName, ok = i.bc.GetTitle(l); ok {
			lang.Name = langName
			indexFile.Langs = append(indexFile.Langs, lang)
		} else {
			lang.Name = l
			indexFile.Langs = append(indexFile.Langs, lang)
		}
		var docs []*Doc
		for _, d := range m {
			docs = append(docs, d)
		}
		indexFile.Docs = append(indexFile.Docs, docs)
	}
	sort.Slice(indexFile.Langs, indexFile.compareLangs)
	j, err := json.Marshal(indexFile)
	content := fmt.Sprintf("export const data = %s;", string(j))
	if err != nil {
		return err
	}
	err = ltfile.WriteFile(path.Join(i.DcsPath, "js", "wc", "doxa-search-data.js"), content)
	if err != nil {
		return err
	}
	return nil
}

// WriteEmptyIndexFile is called when the output is only PDF, which is not searchable.
func (i *Indexer) WriteEmptyIndexFile() error {
	indexFile := new(IndexFile)
	l := new(Lang)
	indexFile.Langs = append(indexFile.Langs, l)
	d := new(Doc)
	var a []*Doc
	a = append(a, d)
	indexFile.Docs = append(indexFile.Docs, a)
	j, err := json.Marshal(indexFile)
	content := fmt.Sprintf("export const data = %s;", string(j))
	if err != nil {
		return err
	}
	err = ltfile.WriteFile(path.Join(i.DcsPath, "js", "wc", "doxa-search-data.js"), content)
	if err != nil {
		return err
	}
	return nil
}
func (f *IndexFile) compareLangs(i, j int) bool {
	return f.Langs[j].Name > f.Langs[i].Name
}
