// Package queries provides predefined data requests
package queries

import (
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"time"
)

type QueryManager struct {
	mapper *kvs.KVS
}

func NewQueryManager(mapper *kvs.KVS) *QueryManager {
	qm := new(QueryManager)
	qm.mapper = mapper
	return qm
}

type MCDInfo struct {
	MCD            string
	Date           string
	Commemorations []string
}

// MCDCommemoration gets the commemoration title for each of the Movable Cycle Days.
// The first slice is for the Triodion, the second for the Pentecostarion.
func (q *QueryManager) MCDCommemoration(triodionStart time.Time, pascha time.Time, libraries []string) ([]*MCDInfo, []*MCDInfo, error) {
	kp := kvs.NewKeyPath()
	kp.Dirs.Push("ltx")
	// ltx/gr_gr_cog/tr.d001:trDA.commemoration.short
	// ...
	// ltx/gr_gr_cog/tr.d071:trDA.commemoration.short Τῷ Ἁγίῳ καὶ Μεγάλῳ Σαββάτῳ
	// ltx/gr_gr_cog/pe.d071:peDA.commemoration.short Κυριακῇ τοῦ Πάσχα
	// ...
	// ltx/gr_gr_cog/pe.d127:peDA.commemoration.short
	var t []*MCDInfo // triodion
	// initialize the triodion mcd.  There are 127 movable cycle days
	kp.KeyParts.Push("trDA.commemoration.short")
	for i := 0; i < 71; i++ {
		m := new(MCDInfo)
		m.MCD = fmt.Sprintf("%d", i+1)
		m.Date = triodionStart.AddDate(0, 0, i).Format("2006-01-02")
		for _, library := range libraries {
			kp.Dirs.Push(library)
			kp.Dirs.Push(fmt.Sprintf("tr.d%03d", i+1))
			dbr, err := q.mapper.Db.Get(kp)
			if err != nil {
				m.Commemorations = append(m.Commemorations, "NA")
			} else {
				m.Commemorations = append(m.Commemorations, dbr.Value)
			}
			kp.Dirs.Pop()
			kp.Dirs.Pop()
		}
		t = append(t, m)
	}
	var p []*MCDInfo // pentecostarion
	// initialize the pentecostarion mcd.  There are 127 movable cycle days
	kp.KeyParts.Pop()
	kp.KeyParts.Push("peDA.commemoration.short")
	for i := 0; i < 57; i++ {
		m := new(MCDInfo)
		m.MCD = fmt.Sprintf("%d", i+71)
		m.Date = pascha.AddDate(0, 0, i).Format("2006-01-02")
		for _, library := range libraries {
			kp.Dirs.Push(library)
			kp.Dirs.Push(fmt.Sprintf("pe.d%03d", i+71))
			dbr, err := q.mapper.Db.Get(kp)
			if err != nil {
				m.Commemorations = append(m.Commemorations, "NA")
			} else {
				m.Commemorations = append(m.Commemorations, dbr.Value)
			}
			kp.Dirs.Pop()
			kp.Dirs.Pop()
		}
		p = append(p, m)
	}
	return t, p, nil
}
