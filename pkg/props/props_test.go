package props

import (
	"testing"
)

func TestNewProperties(t *testing.T) {
	b := NewProperties()
	expect := "config"
	got := b.KP.Path()
	if got != expect {
		t.Errorf("Expected: %s Got: %s\n", expect, got)
	}
}
func TestNewSite(t *testing.T) {
	b := NewSite("doxa-liml")
	expect := "config/sites/doxa-liml"
	got := b.KP.Path()
	if got != expect {
		t.Errorf("Expected: %s Got: %s\n", expect, got)
	}
}
func TestNewSys(t *testing.T) {
	b := NewSys()
	expect := "config/sys"
	got := b.KP.Path()
	if got != expect {
		t.Errorf("Expected: %s Got: %s\n", expect, got)
	}
}
func TestNewBuild(t *testing.T) {
	b := NewBuild()
	expect := "config/sites/Build"
	got := b.KP.Path()
	if got != expect {
		t.Errorf("Expected: %s Got: %s\n", expect, got)
	}
}
