// Package props provides a means to get and set properties (configuration settings) for doxa
package props

// TODO: when props duplicates config, delete pkg/config and rename props to config

import "github.com/liturgiko/doxa/db/kvs"

const (
	DirRoot  = "config"
	DirSites = "sites"
	DirSys   = "sys"
)

type Properties struct {
	KP   *kvs.KeyPath
	Site *Site
	Sys  *Sys
}

func NewProperties() *Properties {
	r := new(Properties)
	r.KP = kvs.NewKeyPath()
	r.KP.Dirs.Push(DirRoot)
	r.Sys = NewSys()
	return r
}

type Site struct {
	KP *kvs.KeyPath
}

func NewSite(site string) *Site {
	r := new(Site)
	r.KP = kvs.NewKeyPath()
	r.KP.Dirs.Push(DirRoot)
	r.KP.Dirs.Push(DirSites)
	r.KP.Dirs.Push(site)
	return r
}

type Sys struct {
	KP     *kvs.KeyPath
	Build  *Build
	Glory  *Glory
	Server *Server
	Shell  *Shell
	Synch  *Synch
	Desc   string
}

func NewSys() *Sys {
	r := new(Sys)
	r.KP = kvs.NewKeyPath()
	r.KP.Dirs.Push(DirRoot)
	r.KP.Dirs.Push(DirSys)
	r.Build = NewBuild()
	return r
}

type Build struct {
	KP     *kvs.KeyPath
	Config *StringValue
	Site   *StringValue
	Desc   string
}

func NewBuild() *Build {
	r := new(Build)
	r.KP = kvs.NewKeyPath()
	r.KP.Dirs.Push(DirRoot)
	r.KP.Dirs.Push(DirSites)
	r.KP.Dirs.Push("Build")
	r.Config = new(StringValue)
	r.Site = new(StringValue)
	return r
}

type Glory struct {
	KP *kvs.KeyPath
}
type Server struct {
	KP *kvs.KeyPath
}
type Shell struct {
	KP *kvs.KeyPath
}
type Synch struct {
	KP   *kvs.KeyPath
	Btx  *SynchRepos
	Ltx  *SynchRepos
	Desc string
}
type SynchRepos struct {
	KP    *kvs.KeyPath
	Push  *BoolValue
	Desc  string
	Repos []*SynchUrl
}
type SynchUrl struct {
	Push  *BoolValue
	Desc  string
	Value string
}
type BoolValue struct {
	Desc  string
	Value bool
}
type StringValue struct {
	Desc  string
	Value string
}
type IntValue struct {
	Desc  string
	Value int
}
