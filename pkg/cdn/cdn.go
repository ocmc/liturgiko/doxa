package cdn

import (
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"log"
	"os"
	"strings"
)

const (
	LimlAcl       = "public-read"
	LimlCdnBucket = "liml-cdn"
)

type S3Data struct {
	Key  string
	Size int64
}
type Client struct {
	S3     *s3.S3
	Bucket string
	Config *aws.Config
}

func NewClient() (*Client, error) {
	client := new(Client)
	key := os.Getenv("SPACES_KEY")
	secret := os.Getenv("SPACES_SECRET")

	if len(key) == 0 {
		msg := fmt.Sprintf("missing  enivronmental variable SPACES_KEY")
		//		doxlog.Error(msg)
		return nil, fmt.Errorf(msg)
	}
	if len(secret) == 0 {
		msg := fmt.Sprintf("missing enivronmental variable SPACES_SECRET")
		//	doxlog.Error(msg)
		return nil, fmt.Errorf(msg)
	}
	client.Bucket = LimlCdnBucket
	client.Config = &aws.Config{
		Credentials:      credentials.NewStaticCredentials(key, secret, ""),
		Endpoint:         aws.String("https://ams3.digitaloceanspaces.com"),
		Region:           aws.String("us-east-1"),
		S3ForcePathStyle: aws.Bool(false), // // Configures to use subdomain/virtual calling format. Depending on your version, alternatively use o.UsePathStyle = false
	}

	newSession, err := session.NewSession(client.Config)
	if err != nil {
		msg := fmt.Sprintf("error creating s3 client: %v", err)
		doxlog.Error(msg)
		return nil, fmt.Errorf(msg)
	}
	client.S3 = s3.New(newSession)
	return client, nil
}
func (c *Client) List() {
	spaces, err := c.S3.ListBuckets(nil)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	for _, b := range spaces.Buckets {
		fmt.Println(aws.StringValue(b.Name))
	}
}
func (c *Client) CreateBucket(bucketName string) error {
	params := &s3.CreateBucketInput{
		Bucket: aws.String(bucketName),
	}

	_, err := c.S3.CreateBucket(params)
	if err != nil {
		msg := fmt.Sprintf("error creating bucket %s: %v", bucketName, err)
		//		doxlog.Error(msg)
		return fmt.Errorf(msg)
	}
	return nil
}
func (c *Client) UploadFile(key string, filename string) error {
	file, err := os.Open(ltfile.ToSysPath(filename))
	if err != nil {
		return fmt.Errorf("os.Open - filename: %s, err: %v", filename, err)
	}
	defer file.Close()

	_, err = c.S3.PutObject(&s3.PutObjectInput{
		Bucket:             aws.String(c.Bucket),
		Key:                aws.String(key),
		ACL:                aws.String(LimlAcl),
		Body:               file, // bytes.NewReader(buffer),
		ContentDisposition: aws.String("attachment"),
	})
	if err != nil {
		return fmt.Errorf("error putting %s: %v", key, err)
	}
	return err
}
func (c *Client) DeleteFile(key string) error {
	// Prepare the delete object input
	input := &s3.DeleteObjectInput{
		Bucket: aws.String(LimlCdnBucket), // s3 bucket name
		Key:    aws.String(key),           // file name
	}

	// Delete the object
	_, err := c.S3.DeleteObject(input)
	if err != nil {
		msg := fmt.Sprintf("error deleting %s: %v", key, err)
		return fmt.Errorf(msg)
	}
	return nil
}
func (c *Client) ListObjects(dir string) error {
	s3Keys := make([]string, 0)
	if !strings.HasSuffix(dir, "/") {
		dir = dir + "/"
	}
	if err := c.S3.ListObjectsPagesWithContext(context.TODO(), &s3.ListObjectsInput{
		Bucket: aws.String(LimlCdnBucket),
		Prefix: aws.String(dir), // list files in the directory.
	}, func(o *s3.ListObjectsOutput, b bool) bool { // callback func to enable paging.
		for _, o := range o.Contents {
			s3Keys = append(s3Keys, *o.Key)
		}
		return true
	}); err != nil {
		msg := fmt.Sprintf("error listing items in s3 directory: %v", err)
		return fmt.Errorf(msg)
	}

	log.Printf("number of files under `blog` directory: %d", len(s3Keys))
	for _, k := range s3Keys {
		log.Printf("file: %s", k)
	}
	return nil
}
