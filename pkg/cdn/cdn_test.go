package cdn

import (
	"testing"
)

func TestNewClient(t *testing.T) {
	c, err := NewClient()
	if err != nil {
		t.Error(err)
	}
	err = c.UploadFile("a/samuel/swh/bwanaHurumia.mp3", "/Users/mac002/Downloads/kyrieeleison8.mp3")
	if err != nil {
		t.Error(err)
		return
	}
	c.List()
}
func TestClient_DeleteFile(t *testing.T) {
	c, err := NewClient()
	if err != nil {
		t.Error(err)
		return
	}
	err = c.DeleteFile("a/samuel/swh/bwanaHurumia.mp3")
	if err != nil {
		t.Error(err)
		return
	}
}
func TestClient_ListObjects(t *testing.T) {
	c, err := NewClient()
	if err != nil {
		t.Error(err)
		return
	}
	err = c.ListObjects("media")
	if err != nil {
		t.Error(err)
		return
	}
}
