package dvcs

import (
	"fmt"
	"github.com/xanzy/go-gitlab"
	"strings"
)

type Action int

const (
	PullList Action = iota
	PushList
)

type RepoData struct {
	DbPath              string // ltx/gr_gr_cog
	FsRepoPath          string // resources/ltx/gr_gr_cog
	NormalizedUrlForApi string // doxa-eac/resources/ltx/gr_gr_cog
	Url                 string // gitlab.com/doxa-eac/resources/ltx/gr_gr_cog.git
	Gitlab              *gitlab.Project
}

// RepoList provides three ways to find the same instance of RepoData.
// The key is the Gitlab name of the repository (aka Project in GitLab's terminology)
type RepoList struct {
	Action                Action
	ReposByDbPath         map[string]*RepoData
	ReposByFileSystemPath map[string]*RepoData
	ReposByUrl            map[string]*RepoData
}

func NewRepoList(action Action) *RepoList {
	return &RepoList{
		Action:                action,
		ReposByDbPath:         make(map[string]*RepoData),
		ReposByFileSystemPath: make(map[string]*RepoData),
		ReposByUrl:            make(map[string]*RepoData),
	}
}

func (r *RepoList) AddRepo(dbPath, fsRepoPath, url string, gitlab *gitlab.Project) {
	if !strings.HasSuffix(url, ".git") {
		url = fmt.Sprintf("%s.git", url)
	}
	newRepoData := &RepoData{DbPath: dbPath, FsRepoPath: fsRepoPath, NormalizedUrlForApi: NormalizeUrlForApi(url), Url: url, Gitlab: gitlab}
	if len(dbPath) > 0 {
		r.ReposByDbPath[dbPath] = newRepoData
	}
	r.ReposByFileSystemPath[fsRepoPath] = newRepoData
	r.ReposByUrl[url] = newRepoData
}

func (r *RepoList) AddRepos(repos []*RepoData) {
	for _, repo := range repos {
		r.ReposByDbPath[repo.DbPath] = repo
		r.ReposByFileSystemPath[repo.FsRepoPath] = repo
		r.ReposByUrl[repo.Url] = repo
	}
}
func (r *RepoList) IncludesDbPath(dbPath string) bool {
	_, found := r.ReposByDbPath[dbPath]
	return found
}
func (r *RepoList) IncludesFsPath(fsPath string) bool {
	_, found := r.ReposByFileSystemPath[fsPath]
	return found
}
func (r *RepoList) RemoveRepoByDbPath(dbPath string) {
	repo, found := r.ReposByDbPath[dbPath]
	if found {
		delete(r.ReposByDbPath, dbPath)
		delete(r.ReposByFileSystemPath, repo.FsRepoPath)
		delete(r.ReposByUrl, repo.Url)
	}
}

func (r *RepoList) RemoveRepoByPath(repoPath string) {
	repo, found := r.ReposByFileSystemPath[repoPath]
	if found {
		delete(r.ReposByDbPath, repo.DbPath)
		delete(r.ReposByFileSystemPath, repoPath)
		delete(r.ReposByUrl, repo.Url)
	}
}

func (r *RepoList) RemoveRepoByUrl(url string) {
	repo, found := r.ReposByUrl[url]
	if found {
		delete(r.ReposByDbPath, repo.DbPath)
		delete(r.ReposByUrl, url)
		delete(r.ReposByFileSystemPath, repo.FsRepoPath)
	}
}

func (r *RepoList) FindRepoByDbPath(dbPath string) (*RepoData, bool) {
	repo, found := r.ReposByFileSystemPath[dbPath]
	return repo, found
}

func (r *RepoList) FindRepoByRepoPath(repoPath string) (*RepoData, bool) {
	repo, found := r.ReposByFileSystemPath[repoPath]
	return repo, found
}

func (r *RepoList) FindRepoByUrl(url string) (*RepoData, bool) {
	repo, found := r.ReposByUrl[url]
	return repo, found
}

func (r *RepoList) AddGitlabProject(project *gitlab.Project) error {
	var repoData *RepoData
	var ok bool
	if repoData, ok = r.FindRepoByUrl(project.HTTPURLToRepo); !ok {
		return fmt.Errorf("%s not found", project.HTTPURLToRepo)
	}
	// because repoData is a pointer, it updates all the maps.
	repoData.Gitlab = project
	return nil
}

func (r *RepoList) Clear() {
	r.ReposByDbPath = make(map[string]*RepoData)
	r.ReposByFileSystemPath = make(map[string]*RepoData)
	r.ReposByUrl = make(map[string]*RepoData)
}

func (r *RepoList) GetAllRepos() (data []*RepoData) {
	for _, v := range r.ReposByFileSystemPath {
		data = append(data, v)
	}
	return data
}

type MergeData struct {
	Name      string
	LocalPath string
	Gitlab    string
	Actions   []string
}

func (r *RepoList) GetAllPushReposAsMergeData() (data []*MergeData) {
	for _, v := range r.ReposByFileSystemPath {
		if v.Gitlab != nil {
			mergeData := &MergeData{
				Name:      v.Gitlab.Name,
				LocalPath: v.FsRepoPath,
				Gitlab:    v.Gitlab.WebURL,
				Actions:   []string{"No Action", "Favor Main", "Favor Work", "Replace Work"},
			}
			data = append(data, mergeData)
		}
	}
	return data
}
