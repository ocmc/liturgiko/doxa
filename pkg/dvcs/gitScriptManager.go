package dvcs

import (
	"embed"
	"errors"
	"fmt"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/enums/gitStatuses"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"net/url"
	"path"
	"strings"
)

const (
	cProjects     = "projects"
	cScripts      = "scripts"
	cScriptsData  = "scriptData"
	cPullUrlsFile = "pullUrls.txt"
	cPushUrlsFile = "pushUrls.txt"
)

// GitScriptManager handles the running of external scripts that make calls
// to git that is installed on the user's computer.
type GitScriptManager struct {
	// {doxa home}projects/doxa-eac/scripts/pushUrls.txt
	DoxaHomeDir          string
	EmbeddedScriptsPatch *embed.FS
	ProjectGroupName     string
	ProjectDirPath       string
	ScriptsDataDirPath   string // path within a specific project to find files to create parameters for script call
	ScriptsDirPath       string // path to where scripts are located as a direct child of the doxa home dir
	PushList             *RepoList
	SubscriptionList     *RepoList
	SubscriptionsPath    string
}

func NewGitScriptManager(doxaHomeDir, projectGroupName string, embeddedScriptsPatch *embed.FS) (*GitScriptManager, error) {
	// in case projectGroupName is a path and not the dir name:
	_, projectGroupName = path.Split(projectGroupName)
	manager := &GitScriptManager{
		DoxaHomeDir:          doxaHomeDir,
		EmbeddedScriptsPatch: embeddedScriptsPatch,
		ScriptsDirPath:       path.Join(doxaHomeDir, cScripts),
		ProjectDirPath:       path.Join(doxaHomeDir, cProjects, projectGroupName),
		ScriptsDataDirPath:   path.Join(doxaHomeDir, cProjects, projectGroupName, cScriptsData),
		PushList:             NewRepoList(PushList),
		SubscriptionList:     NewRepoList(PullList),
		SubscriptionsPath:    path.Join(doxaHomeDir, cProjects, projectGroupName, "subscriptions"),
		ProjectGroupName:     projectGroupName,
	}
	//	err := manager.copyScripts()
	//	if err != nil {
	//		return nil, err
	//	}
	urlList := path.Join(manager.ScriptsDataDirPath, cPushUrlsFile)
	if !ltfile.FileExists(urlList) {
		msg := fmt.Sprintf("%s not found", urlList)
		doxlog.Error(msg)
		return nil, errors.New(msg)
	}
	err := manager.loadPushList()
	if err != nil {
		return nil, err
	}
	//err = manager.loadPullList()
	//if err != nil {
	//	return nil, err
	//}
	return manager, nil
}

func (r *RepoList) ToStrSlice() []string {
	var slice []string
	if r == nil {
		return slice //todo: shouldn't have to check for r
	}
	if r.ReposByFileSystemPath == nil {
		//TODO: attempt to construct
		return slice
	}
	for k, _ := range r.ReposByFileSystemPath {
		slice = append(slice, k)
	}
	return slice
}

//	func (manager *GitScriptManager) copyScripts() error {
//		// copy scripts
//		doxlog.Info("Copying scripts from patch...")
//		err := ltfile.CopyEmbedded(*manager.EmbeddedScriptsPatch, "patch/scripts/code", []string{manager.ScriptsDirPath})
//		if err != nil {
//			doxlog.Infof("error copying missing asset files: %v", err)
//		}
//		// make them executable
//		err = filepath.Walk(manager.ScriptsDirPath, func(path string, info os.FileInfo, err error) error {
//			if err != nil {
//				return err
//			}
//			if !info.IsDir() {
//				err = os.Chmod(path, 0755)
//				if err != nil {
//					return err
//				}
//			}
//			return nil
//		})
//		if err != nil {
//			doxlog.Infof("error making scripts executable: %v", err)
//		}
//		// copy pull script data
//		embedFsScriptDataDir := "patch/scripts/data/pull"
//		if !ltfile.FileExists(manager.ScriptsDataDirPath) {
//			err = ltfile.CopyEmbedded(*manager.EmbeddedScriptsPatch, embedFsScriptDataDir, []string{manager.ScriptsDataDirPath})
//			if err != nil {
//				doxlog.Infof("error copying scripts push data file: %v", err)
//			}
//			// copy push script data
//			embedFsScriptDataDir = path.Join("patch/scripts/data/push", manager.ProjectGroupName)
//			err = ltfile.CopyEmbedded(*manager.EmbeddedScriptsPatch, embedFsScriptDataDir, []string{manager.ScriptsDataDirPath})
//			if err != nil {
//				doxlog.Infof("error copying scripts pull data file: %v", err)
//			}
//		}
//		return nil
//	}
func (manager *GitScriptManager) loadPushList() error {
	// TODO: need for user maintained
	urls, err := ltfile.GetFileLines(path.Join(manager.ScriptsDataDirPath, cPushUrlsFile))
	if err != nil {
		msg := fmt.Sprintf("load push list from %s failed: %v", cPushUrlsFile, err)
		doxlog.Error(msg)
		return err
	}
	for _, repoUrl := range urls {
		var dbPath string
		var repoPath string
		dbPath, err = manager.urlToDbPath(repoUrl)
		repoPath, err = DirPath(manager.ProjectDirPath, repoUrl) //manager.urlToFileSystemPath(repoUrl)
		if err != nil {
			msg := fmt.Sprintf("convert url to file system path failed: %v", err)
			doxlog.Error(msg)
			return err
		}
		manager.PushList.AddRepo(dbPath, repoPath, repoUrl, nil)
	}
	return nil
}

//func (manager *GitScriptManager) loadPullList() error {
//	urls, err := ltfile.GetFileLines(filepath.Join(manager.ScriptsDataDirPath, cPullUrlsFile))
//	if err != nil {
//		msg := fmt.Sprintf("load pull list from %s failed: %v", cPullUrlsFile, err)
//		doxlog.Error(msg)
//		return err
//	}
//	for _, repoUrl := range urls {
//		var dbPath string
//		var repoPath string
//		dbPath, err = manager.urlToDbPath(repoUrl)
//		repoPath, err = manager.urlToFileSystemPath(repoUrl)
//		if err != nil {
//			msg := fmt.Sprintf("convert url to file system path failed: %v", err)
//			doxlog.Error(msg)
//			return err
//		}
//		manager.SubscriptionList.AddRepo(dbPath, repoPath, repoUrl, nil)
//	}
//	return nil
//}

func (manager *GitScriptManager) urlToDbPath(repoUrl string) (dbPath string, err error) {
	// TODO: need for user maintained
	u, err := url.Parse(repoUrl)
	if err != nil {
		return fmt.Sprintf("url %s to db path failed: %v", repoUrl, err), nil
	}
	parts := strings.Split(u.Path, "/")
	if len(parts) < 3 {
		return "", errors.New("invalid url: could not parse url")
	}
	// parts[0] == "", parts[1] == the project group name
	var index int
	switch parts[2] {
	case "resources":
		index = 3
	case "media":
		index = 2
	default:
		return "", nil
	}
	dbPath = path.Join(parts[index:]...)
	dbPath = strings.TrimSuffix(dbPath, ".git")
	return dbPath, err
}
func (manager *GitScriptManager) urlToFileSystemPath(repoUrl string) (fs string, err error) {
	// TODO: need for user maintained
	u, err := url.Parse(repoUrl)
	if err != nil {
		return fmt.Sprintf("url %s to db path failed: %v", repoUrl, err), nil
	}
	parts := strings.Split(u.Path, "/")
	if len(parts) < 3 {
		return "", errors.New("invalid url: could not parse url")
	}
	// parts[0] == "", parts[1] == project group name
	if strings.Contains(fs, "contributors") {
		fs = path.Join(parts[4:]...)
	} else {
		fs = path.Join(parts[2:]...)
	}
	fs = path.Join(manager.ProjectDirPath, strings.TrimSuffix(fs, ".git"))
	return fs, err
}

// RunGitScript urlListFileName uses relative paths.
// repoPath can be empty "" except for scripts ours.sh, theirs.sh, rebaser.sh
//func (manager *GitScriptManager) RunGitScript(userName, token, scriptFileName string, urlListFileName, repoPath string) error {
//	urlListPath := filepath.Join(manager.ScriptsDataDirPath, urlListFileName)
//	scriptPath := filepath.Join(manager.ScriptsDirPath, scriptFileName)
//	lastArg := urlListPath
//	switch scriptFileName {
//	case "ours.sh", "rebaser.sh", "theirs.sh":
//		if len(repoPath) == 0 {
//			return fmt.Errorf("repo path can't be empty for this script")
//		}
//		lastArg = repoPath
//	default:
//	}
//	if scriptFileName == "" {
//		lastArg = "" // TODO
//	}
//	doxlog.Infof("Running git script %s", scriptPath)
//	cmd := exec.Command(scriptPath, userName, token, lastArg)
//	cmd.Dir = manager.ProjectDirPath
//	err := cmd.Run()
//	if err != nil {
//		//check to see if the error is an exit status
//		exi := new(exec.ExitError)
//		if errors.As(err, &exi) {
//			if exi.ExitCode() == 128 {
//				msg := fmt.Sprintf("Merge conflict in repo %s", os.Getenv("pusherProblematicRepo"))
//				doxlog.Error(msg)
//				return errors.New(msg)
//			}
//		}
//		msg := fmt.Sprintf("error running script %s: %v", scriptFileName, err)
//		doxlog.Errorf(msg)
//		return fmt.Errorf(msg)
//	}
//	return nil
//}

//func splicePath(victim, firstPart, payload string) string {
//	if strings.HasPrefix(victim, firstPart) {
//		survivor, success := strings.CutPrefix(victim, firstPart)
//		if success {
//			return filepath.Join(firstPart, payload, survivor)
//		}
//	}
//	return victim
//}

//func (m *GitScriptManager) SyncSubscriptions() error {
//	for _, d := range m.SubscriptionList.ReposByFileSystemPath {
//		//TODO: FsRepoPath is the full path, right?
//		path := filepath.Join(m.ProjectDirPath, "subscriptions") //splicePath(d.FsRepoPath, m.ProjectDirPath, "subscriptions")
//		repoPath, err := DirPath(path, d.Url)
//		if err != nil {
//			doxlog.Errorf("Could not parse Url '%s' or Path '%s': %v", path, d.Url, err)
//			continue
//		}
//		//doxlog.Warnf("path is %s", repoPath)
//		g := NewGitClient(repoPath)
//		if !ltfile.DirExists(filepath.Join(repoPath, ".git")) {
//			//create
//			repoPath, err = g.Clone(path, d.Url, false)
//			if err != nil {
//				doxlog.Errorf("Could not clone repo '%s': %v", d.Url, err)
//				continue
//			}
//		}
//		info, err := g.LocalAndOriginDiffer()
//		if err != nil {
//			doxlog.Errorf("Could not get status of repo '%s': %v", d.Url, err)
//			if info != nil {
//				doxlog.Info(info.DiffMessage)
//			}
//		}
//		if info.Differ {
//			status, err := g.PullOrigin(repoPath, true)
//			if err != nil {
//				doxlog.Errorf("Could not pull '%s': %v", d.Url, err)
//				doxlog.Infof("Status of operation is: %s", status.String())
//			}
//		}
//	}
//	//first check if file exists
//	//if not clone repo
//	//git pull
//	//error? reset
//	return nil
//}

func (m *GitScriptManager) SyncMaintains(username, token, apiPath string, exportFunc func(r *RepoList) error, importFunc func(a, b, c string, d bool) []error) error {
	// TODO: need for user maintained
	var err error
	for _, d := range m.PushList.ReposByFileSystemPath {
		fpath := d.FsRepoPath
		//we create the gitlab client in case the remote repo has not yet been initialized
		_, err = NewGitlabClient(token, 0, m.ProjectDirPath, apiPath, true)
		if err != nil {
			doxlog.Errorf("Could not create gitlab client for repo '%s': %v", d.Url, err)
		}
		g := NewGitClient(fpath)
		if !ltfile.DirExists(path.Join(fpath, ".git")) {
			//TODO: proper paths
			fpath, err = g.CloneWithAuth(m.ProjectDirPath, d.Url, username, token, false)
			if err != nil {
				doxlog.Errorf("Could not clone repo '%s': %v", d.Url, err)
				continue
			}
		}
		err = g.Checkout(fpath, "main", nil)
		if err != nil {
			err = g.Checkout(fpath, "master", nil)
			if err != nil {
				doxlog.Errorf("Could not check out primary branch: %v", err)
			}
		}
		status, err := g.LocalAndOriginDiffer()
		if err != nil {
			doxlog.Errorf("Could not get status of '%s': %v", d.Url, err)
		}
		if status.Differ {
			// check to see if local is ahead or origin is ahead
			if status.Diffs == nil {
				doxlog.Errorf("repo '%s': Local and origin differ, but no diffs present, skipping", d.Url)
				continue
			}
			if status.Rev1AheadRev2 && status.Rev2AheadRev1 {
				doxlog.Errorf("repo '%s': Local and Origin have diverged. please manually correct", d.Url)
				continue
			}
			if status.Rev1AheadRev2 {
				result, err := g.PullWithAuth(fpath, "origin", username, token, false)
				if err != nil {
					if result != gitStatuses.AlreadyUpToDate {
						doxlog.Errorf("Could not pull repo '%s': %v (status: %s)", d.Url, err, result.String())
						continue
					}
				}
			}
			if status.Rev2AheadRev1 {
				err := g.Push(fpath, username, token)
				if err != nil {
					doxlog.Errorf("Could not commit to repo '%s', %v", d.Url, err)
					continue
				}
			}
		}
		resPath := path.Join(m.ProjectDirPath + "resources")
		l := NewRepoList(PushList)
		l.AddRepo(d.DbPath, d.FsRepoPath, d.Url, d.Gitlab)
		err = exportFunc(l)
		if err != nil {
			doxlog.Errorf("Export failed: %v", err)
			return err
		}
		err = g.CommitAll(fpath, "doxa automatic commit")
		if err != nil {
			doxlog.Errorf("Could not commit to repo '%s': %v", d.Url, err)
			continue
		}
		err = g.Push(fpath, username, token)
		if err != nil {
			doxlog.Errorf("Push to repo '%s' failed: %v", d.Url, err)
			continue
		}
		//import the records
		errs := importFunc(d.DbPath, d.FsRepoPath, resPath, false)
		if len(errs) > 0 {
			doxlog.Errorf("Errors, while importing repo %s", d.Url)
			for _, e := range errs {
				doxlog.Error(e.Error())
			}
			continue
		}
	}
	return nil
}
