package dvcs

import (
	"testing"
)

func TestSshUrlToHttps(t *testing.T) {
	sshUrl := "git@gitlab.com:doxa-liml/websites/public.git"
	expected := "https://gitlab.com/doxa-liml/websites/public.git"
	got, err := SshUrlToHttps(sshUrl)
	if err != nil {
		t.Error(err)
		return
	}
	if got != expected {
		t.Errorf("expected %s but got %s", expected, got)
	}
}
func TestNewRepoProxy(t *testing.T) {
	dirPath := "/volumes/macWdb/doxa/projects/doxa-liml/websites/public"
	p, err := NewRepoProxy(dirPath)
	if err != nil {
		t.Error(err)
		return
	}
	if p.Repo == nil {
		t.Error("nil repo")
	}
}

type TestUrl struct {
	Url      string
	Expected string
	Actual   string
	Inverse  bool //value should not be expected to be equal
}

func TestDirPath(t *testing.T) {
	var err error
	parent := "/tmp/test"
	urls := []*TestUrl{
		{"https://gitlab.com/doxa-example/resources/ltx/example.git", "/tmp/test/resources/ltx/example", "", false},
		{"https://gitlab.com/doxa-example/resources/ltx/example", "/tmp/test/resources/ltx/example", "", false},                    //without extension
		{"gitlab.com/tmp/test/resources/ltx/example", "/tmp/test/resources/ltx/example", "", false},                                //without http
		{"https://gitlab.com/doxa-example/contributors/steve/resources/ltx/example", "/tmp/test/resources/ltx/example", "", false}, //with extra directories
		{"https://gitlab.com/resources/ltx/example.git", "/tmp/test/resources/ltx/example", "", true},
	}
	for i, url := range urls {
		url.Actual, err = DirPath(parent, url.Url)
		if err != nil && !url.Inverse {
			t.Errorf("%v occured on url %d", err, i)
		}
		if url.Expected == url.Actual {
			if url.Inverse {
				t.Errorf("[%d/%d] invalid value '%s' not reported", i, (len(urls)), url.Url)
			}
		}
	}
}
