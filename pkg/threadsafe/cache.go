package threadsafe

import (
	"github.com/liturgiko/doxa/db/kvs"
	"regexp"
	"strings"
	"sync/atomic"
	"time"
)

// CacheStats holds metrics about cache performance
type CacheStats struct {
	Hits   uint64
	Misses uint64
	Size   uint64
}

// recordCacheHit increments the hit counter
func (ts *ThreadsafeKVS) recordCacheHit() {
	atomic.AddUint64(&ts.stats.Hits, 1)
}

// recordCacheMiss increments the miss counter
func (ts *ThreadsafeKVS) recordCacheMiss() {
	atomic.AddUint64(&ts.stats.Misses, 1)
}

// GetCacheStats returns current cache statistics
func (ts *ThreadsafeKVS) GetCacheStats() CacheStats {
	return CacheStats{
		Hits:   atomic.LoadUint64(&ts.stats.Hits),
		Misses: atomic.LoadUint64(&ts.stats.Misses),
		Size:   atomic.LoadUint64(&ts.stats.Size),
	}
}

// invalidateCachePattern removes entries matching the given pattern
func (ts *ThreadsafeKVS) invalidateCachePattern(pattern string) error {
	reg, err := regexp.Compile(pattern)
	if err != nil {
		return err
	}

	ts.mutex.Lock()
	defer ts.mutex.Unlock()

	for key := range ts.cache {
		if reg.MatchString(key) {
			delete(ts.cache, key)
			atomic.AddUint64(&ts.stats.Size, ^uint64(0)) // decrement
		}
	}
	return nil
}

// invalidateCachePrefix removes entries with the given prefix
func (ts *ThreadsafeKVS) invalidateCachePrefix(prefix string) {
    ts.invalidateCachePrefixLocked(prefix, true)
}

// invalidateCachePrefixLocked removes entries with the given prefix
// if acquireLock is false, assumes the lock is already held
func (ts *ThreadsafeKVS) invalidateCachePrefixLocked(prefix string, acquireLock bool) {
    if acquireLock {
        ts.mutex.Lock()
        defer ts.mutex.Unlock()
    }

    for key := range ts.cache {
        if strings.HasPrefix(key, prefix) {
            delete(ts.cache, key)
            atomic.AddUint64(&ts.stats.Size, ^uint64(0)) // decrement
        }
    }
}

// invalidateCacheRange removes entries within the given range
func (ts *ThreadsafeKVS) invalidateCacheRange(from, to string) {
	ts.mutex.Lock()
	defer ts.mutex.Unlock()

	for key := range ts.cache {
		if key >= from && key <= to {
			delete(ts.cache, key)
			atomic.AddUint64(&ts.stats.Size, ^uint64(0)) // decrement
		}
	}
}

// setInCache adds or updates a cache entry with atomic size tracking
func (ts *ThreadsafeKVS) setInCache(key string, value *kvs.DbR) {
    ts.setInCacheLocked(key, value, true)
}

// setInCacheLocked adds or updates a cache entry with atomic size tracking
// if acquireLock is false, assumes the lock is already held
func (ts *ThreadsafeKVS) setInCacheLocked(key string, value *kvs.DbR, acquireLock bool) {
    if acquireLock {
        ts.mutex.Lock()
        defer ts.mutex.Unlock()
    }

    if _, exists := ts.cache[key]; !exists {
        atomic.AddUint64(&ts.stats.Size, 1)
    }

    ts.cache[key] = &CacheEntry{
        value:     value,
        timestamp: time.Now(),
    }
}
