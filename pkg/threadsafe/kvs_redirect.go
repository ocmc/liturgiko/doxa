package threadsafe

import (
	"github.com/liturgiko/doxa/db/kvs"
)

// GetResolved gets a record and resolves any redirects
func (ts *ThreadsafeKVS) GetResolved(kp *kvs.KeyPath) (*kvs.DbR, []string, error) {

	// Try cache first
	cacheKey := ts.getCacheKey(kp)
	if value, found := ts.getFromCache(cacheKey); found {
		ts.recordCacheHit()
		// Still need to check redirects even if cached
		if !value.IsRedirect() {
			return value, []string{kp.Path()}, nil
		}
	}

	ts.recordCacheMiss()
	ts.mutex.RLock()
	defer ts.mutex.RUnlock()
	return ts.db.GetResolved(kp)
}

// GetRedirectsMapper returns a two-way map of redirects
func (ts *ThreadsafeKVS) GetRedirectsMapper(kp *kvs.KeyPath) (*kvs.RedirectMapper, error) {
	ts.mutex.RLock()
	defer ts.mutex.RUnlock()

	return ts.db.GetRedirectsMapper(kp)
}

// GetRedirectsToDir returns records redirecting to this directory
func (ts *ThreadsafeKVS) GetRedirectsToDir(kp *kvs.KeyPath) ([]*kvs.DbR, error) {
	ts.mutex.RLock()
	defer ts.mutex.RUnlock()

	return ts.db.GetRedirectsToDir(kp)
}

// GetRedirectsToRecord returns records redirecting to this record
func (ts *ThreadsafeKVS) GetRedirectsToRecord(kp *kvs.KeyPath) ([]*kvs.DbR, error) {
	ts.mutex.RLock()
	defer ts.mutex.RUnlock()

	return ts.db.GetRedirectsToRecord(kp)
}

// IsRedirect checks if a record is a redirect
func (ts *ThreadsafeKVS) IsRedirect(kp *kvs.KeyPath) (bool, *kvs.DbR, error) {
	// Try cache first
	cacheKey := ts.getCacheKey(kp)
	if value, found := ts.getFromCache(cacheKey); found {
		ts.recordCacheHit()
		return value.IsRedirect(), value, nil
	}

	ts.recordCacheMiss()

	ts.mutex.RLock()
	defer ts.mutex.RUnlock()

	return ts.db.IsRedirect(kp)
}

// MakeRedirects creates redirect records
func (ts *ThreadsafeKVS) MakeRedirects(fromKp, toKp *kvs.KeyPath) error {
	ts.mutex.Lock()
	defer ts.mutex.Unlock()

	err := ts.db.MakeRedirects(fromKp, toKp)
	if err != nil {
		return err
	}

	// Invalidate cache entries that might be affected
	ts.invalidateCachePrefixLocked(fromKp.Path(), false)
	ts.invalidateCachePrefixLocked(toKp.Path(), false)

	return nil
}
