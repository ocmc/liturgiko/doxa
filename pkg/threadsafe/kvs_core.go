package threadsafe

import (
	"github.com/liturgiko/doxa/db/kvs"
)

// Get retrieves a record by key, checking cache first
func (ts *ThreadsafeKVS) Get(kp *kvs.KeyPath) (*kvs.DbR, error) {
	cacheKey := ts.getCacheKey(kp)

	// Try cache first
	if value, found := ts.getFromCache(cacheKey); found {
		ts.recordCacheHit()
		return value, nil
	}

	ts.recordCacheMiss()

	// Get from underlying db
	ts.mutex.RLock()
	value, err := ts.db.Get(kp)
	ts.mutex.RUnlock()

	if err != nil {
		return nil, err
	}

	// Cache the result
	ts.setInCache(cacheKey, value)
	return value, nil
}

// Put stores a record, updating cache and db
func (ts *ThreadsafeKVS) Put(kvp *kvs.DbR) error {
	ts.mutex.Lock()
	defer ts.mutex.Unlock()

	// Write through to db first
	err := ts.db.Put(kvp)
	if err != nil {
		return err
	}

	// Update cache without acquiring lock again
	cacheKey := ts.getCacheKey(kvp.KP)
	ts.setInCacheLocked(cacheKey, kvp, false)

	return nil
}

// Delete removes a record from both cache and db
func (ts *ThreadsafeKVS) Delete(kp kvs.KeyPath) error {
	ts.mutex.Lock()
	defer ts.mutex.Unlock()

	// Delete from db first
	err := ts.db.Delete(kp)
	if err != nil {
		return err
	}

	// Invalidate cache entries without acquiring lock again
	ts.invalidateCachePrefixLocked(kp.Path(), false)

	return nil
}

// Close shuts down the ThreadsafeKVS
func (ts *ThreadsafeKVS) Close() error {
	// Stop the cleanup goroutine
	close(ts.stopCleanup)

	ts.mutex.Lock()
	defer ts.mutex.Unlock()

	// Clear cache
	ts.cache = make(map[string]*CacheEntry)

	// Close underlying db
	return ts.db.Close()
}

// Open opens the underlying database
func (ts *ThreadsafeKVS) Open(filename string) error {
	ts.mutex.Lock()
	defer ts.mutex.Unlock()

	return ts.db.Open(filename)
}

// Exists checks if a record exists in cache or db
func (ts *ThreadsafeKVS) Exists(kp *kvs.KeyPath) bool {
	cacheKey := ts.getCacheKey(kp)

	// Check cache first
	if _, found := ts.getFromCache(cacheKey); found {
		ts.recordCacheHit()
		return true
	}

	ts.recordCacheMiss()

	// Check underlying db
	ts.mutex.RLock()
	exists := ts.db.Exists(kp)
	ts.mutex.RUnlock()

	return exists
}

// MakeDir creates a directory in the db
func (ts *ThreadsafeKVS) MakeDir(kp *kvs.KeyPath) error {
	ts.mutex.Lock()
	defer ts.mutex.Unlock()

	err := ts.db.MakeDir(kp)
	if err != nil {
		return err
	}

	// Invalidate any cached entries that might be affected
	ts.invalidateCachePrefixLocked(kp.Path(), false)

	return nil
}

// DirNames gets directory names with proper locking
func (ts *ThreadsafeKVS) DirNames(kp kvs.KeyPath) ([]string, error) {
	ts.mutex.RLock()
	defer ts.mutex.RUnlock()

	return ts.db.DirNames(kp)
}

// DirPaths gets directory paths with proper locking
func (ts *ThreadsafeKVS) DirPaths(kp kvs.KeyPath) ([]*kvs.KeyPath, error) {
	ts.mutex.RLock()
	defer ts.mutex.RUnlock()

	return ts.db.DirPaths(kp)
}
