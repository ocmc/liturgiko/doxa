package threadsafe

import (
	"context"
	"github.com/emirpasic/gods/trees/btree"
	"github.com/liturgiko/doxa/db/kvs"
	"sync/atomic"
)

// GetMatching returns records matching the given criteria with proper locking
func (ts *ThreadsafeKVS) GetMatching(matcher kvs.Matcher) ([]*kvs.DbR, int, error) {
	ts.mutex.RLock()
	defer ts.mutex.RUnlock()

	return ts.db.GetMatching(matcher)
}

// GetMatchingIDs returns records with matching IDs
func (ts *ThreadsafeKVS) GetMatchingIDs(matcher kvs.Matcher) ([]*kvs.DbR, error) {
	ts.mutex.RLock()
	defer ts.mutex.RUnlock()

	return ts.db.GetMatchingIDs(matcher)
}

// ExistsMatching checks if any records match the criteria
func (ts *ThreadsafeKVS) ExistsMatching(matcher kvs.Matcher) (bool, error) {
	ts.mutex.RLock()
	defer ts.mutex.RUnlock()

	return ts.db.ExistsMatching(matcher)
}

// Tree returns a tree representation of the database
func (ts *ThreadsafeKVS) Tree(kp *kvs.KeyPath,
	dirOnly bool,
	includeSystem bool,
	excludeRedirect bool,
	excludeEmpty bool,
	excludeText bool,
	depth int,
	width int) (*btree.Tree, error) {

	ts.mutex.RLock()
	defer ts.mutex.RUnlock()

	return ts.db.Tree(kp, dirOnly, includeSystem, excludeRedirect, excludeEmpty, excludeText, depth, width)
}

// Keys returns directory and record keys
func (ts *ThreadsafeKVS) Keys(kp *kvs.KeyPath) ([]*kvs.KeyPath, int, int, error) {
	ts.mutex.RLock()
	defer ts.mutex.RUnlock()

	return ts.db.Keys(kp)
}

// Range returns records within the given key range
func (ts *ThreadsafeKVS) Range(from, to *kvs.KeyPath) ([]*kvs.DbR, error) {
	ts.mutex.RLock()
	defer ts.mutex.RUnlock()

	records, err := ts.db.Range(from, to)
	if err != nil {
		return nil, err
	}

	// Cache the results
	for _, record := range records {
		ts.setInCacheLocked(ts.getCacheKey(record.KP), record, false)
	}

	return records, nil
}

// InverseKeyMap returns paths of records matching keys
func (ts *ThreadsafeKVS) InverseKeyMap(kp *kvs.KeyPath) (*map[string][]*kvs.KeyPath, error) {
	ts.mutex.RLock()
	defer ts.mutex.RUnlock()

	return ts.db.InverseKeyMap(kp)
}

// Export exports the database contents with context support
func (ts *ThreadsafeKVS) ExportWithContext(ctx context.Context, kp *kvs.KeyPath, pathOut string, exportType kvs.ExportType) error {
	// Quick context check before acquiring lock
	if err := ctx.Err(); err != nil {
		return err
	}

	ts.mutex.RLock()
	defer ts.mutex.RUnlock()

	// Check context again after acquiring lock
	if err := ctx.Err(); err != nil {
		return err
	}

	return ts.db.Export(kp, pathOut, exportType)
}

// Export exports the database contents
func (ts *ThreadsafeKVS) Export(kp *kvs.KeyPath, pathOut string, exportType kvs.ExportType) error {
	return ts.ExportWithContext(context.Background(), kp, pathOut, exportType)
}

// Import imports data into the database
func (ts *ThreadsafeKVS) Import(dirsPrefix, filepath string,
	lineParser kvs.LineParser,
	removeQuotes bool,
	allOrNone bool,
	skipObsoleteConfigs bool) (int, []error) {

	return ts.ImportWithContext(context.Background(), dirsPrefix, filepath,
		lineParser, removeQuotes, allOrNone, skipObsoleteConfigs)
}

// Import imports data into the database with context support
func (ts *ThreadsafeKVS) ImportWithContext(ctx context.Context, dirsPrefix, filepath string,
	lineParser kvs.LineParser,
	removeQuotes bool,
	allOrNone bool,
	skipObsoleteConfigs bool) (int, []error) {

	// Quick context check before acquiring lock
	if err := ctx.Err(); err != nil {
		return 0, []error{err}
	}

	ts.mutex.Lock()
	defer ts.mutex.Unlock()

	// Check context again after acquiring lock
	if err := ctx.Err(); err != nil {
		return 0, []error{err}
	}

	count, errs := ts.db.Import(dirsPrefix, filepath, lineParser, removeQuotes, allOrNone, skipObsoleteConfigs)
	if len(errs) == 0 {
		// Clear cache on successful import
		ts.cache = make(map[string]*CacheEntry)
		atomic.StoreUint64(&ts.stats.Size, 0)
	}

	return count, errs
}
