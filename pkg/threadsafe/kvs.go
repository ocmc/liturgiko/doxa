package threadsafe

import (
	"github.com/liturgiko/doxa/db/kvs"
	"sync"
	"time"
)

// ThreadsafeKVS wraps a KVS implementation with thread-safe caching
const (
	DefaultBatchSize = 2000
	MaxBatchSize     = 10000
)

type ThreadsafeKVS struct {
	db              kvs.KVI
	mutex           sync.RWMutex
	cache           map[string]*CacheEntry
	stats           CacheStats
	ttl             time.Duration
	cleanupInterval time.Duration
	stopCleanup     chan struct{}
	batchSize       int
}

// CacheEntry represents a cached database record with timestamp
type CacheEntry struct {
	value     *kvs.DbR
	timestamp time.Time
}

// NewThreadsafeKVS creates a new thread-safe KVS wrapper with caching
func NewThreadsafeKVS(db kvs.KVI, ttl, cleanup time.Duration) *ThreadsafeKVS {
	if db == nil {
		panic("db cannot be nil")
	}
	ts := &ThreadsafeKVS{
		db:              db,
		cache:           make(map[string]*CacheEntry),
		ttl:             ttl,
		cleanupInterval: cleanup,
		stopCleanup:     make(chan struct{}),
		batchSize:       DefaultBatchSize,
	}
	ts.startCleanup()
	return ts
}

// SetBatchSize sets the maximum batch size for operations
func (ts *ThreadsafeKVS) SetBatchSize(size int) {
	if size <= 0 {
		size = DefaultBatchSize
	}
	if size > MaxBatchSize {
		size = MaxBatchSize
	}
	ts.batchSize = size
}

// startCleanup starts the background cache cleanup routine
func (ts *ThreadsafeKVS) startCleanup() {
	go func() {
		ticker := time.NewTicker(ts.cleanupInterval)
		defer ticker.Stop()
		for {
			select {
			case <-ticker.C:
				ts.cleanupCache()
			case <-ts.stopCleanup:
				return
			}
		}
	}()
}

// cleanupCache removes expired entries from the cache
func (ts *ThreadsafeKVS) cleanupCache() {
	ts.mutex.Lock()
	defer ts.mutex.Unlock()

	now := time.Now()
	for key, entry := range ts.cache {
		if now.Sub(entry.timestamp) > ts.ttl {
			delete(ts.cache, key)
		}
	}
}

// getCacheKey generates a cache key from a KeyPath
func (ts *ThreadsafeKVS) getCacheKey(kp *kvs.KeyPath) string {
	return kp.Path()
}

// getFromCache retrieves an entry from the cache if it exists and is not expired
func (ts *ThreadsafeKVS) getFromCache(key string) (*kvs.DbR, bool) {
	ts.mutex.RLock()
	defer ts.mutex.RUnlock()

	entry, exists := ts.cache[key]
	if !exists {
		return nil, false
	}

	if time.Since(entry.timestamp) > ts.ttl {
		return nil, false
	}

	return entry.value, true
}

// invalidateCache removes an entry from the cache
func (ts *ThreadsafeKVS) invalidateCache(key string) {
	ts.mutex.Lock()
	defer ts.mutex.Unlock()

	delete(ts.cache, key)
}
