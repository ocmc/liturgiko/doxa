package threadsafe

import (
	"context"
	"github.com/liturgiko/doxa/db/kvs"
)

// Batch performs a batch update of multiple records with context support
func (ts *ThreadsafeKVS) Batch(values []*kvs.DbR) error {
	if len(values) == 0 {
		return nil
	}

	// Split into smaller batches if needed
	if len(values) > ts.batchSize {
		for i := 0; i < len(values); i += ts.batchSize {
			end := i + ts.batchSize
			if end > len(values) {
				end = len(values)
			}

			if err := ts.batchWithContext(context.Background(), values[i:end]); err != nil {
				return err
			}
		}
		return nil
	}

	return ts.batchWithContext(context.Background(), values)
}

// BatchWithContext performs a batch update that can be cancelled
func (ts *ThreadsafeKVS) BatchWithContext(ctx context.Context, values []*kvs.DbR) error {
	if len(values) == 0 {
		return nil
	}

	// Split into smaller batches if needed
	if len(values) > ts.batchSize {
		for i := 0; i < len(values); i += ts.batchSize {
			// Check context cancellation between batches
			if err := ctx.Err(); err != nil {
				return err
			}

			end := i + ts.batchSize
			if end > len(values) {
				end = len(values)
			}

			if err := ts.batchWithContext(ctx, values[i:end]); err != nil {
				return err
			}
		}
		return nil
	}

	return ts.batchWithContext(ctx, values)
}

// batchWithContext is the internal implementation of batch operations
func (ts *ThreadsafeKVS) batchWithContext(ctx context.Context, values []*kvs.DbR) error {
	// Quick context check before acquiring lock
	if err := ctx.Err(); err != nil {
		return err
	}

	ts.mutex.Lock()
	defer ts.mutex.Unlock()

	// Check context again after acquiring lock
	if err := ctx.Err(); err != nil {
		return err
	}

	// Write through to db first
	err := ts.db.Batch(values)
	if err != nil {
		return err
	}

	// Update cache for each value
	for _, value := range values {
		// Check context periodically during cache updates
		if err := ctx.Err(); err != nil {
			return err
		}

		cacheKey := ts.getCacheKey(value.KP)
		ts.setInCacheLocked(cacheKey, value, false)
	}

	return nil
}

// Copy copies records from source to destination
func (ts *ThreadsafeKVS) Copy(from, to kvs.KeyPath) error {
	ts.mutex.Lock()
	defer ts.mutex.Unlock()

	err := ts.db.Copy(from, to)
	if err != nil {
		return err
	}

	// Invalidate cache entries that might be affected
	ts.invalidateCachePrefixLocked(from.Path(), false)
	ts.invalidateCachePrefixLocked(to.Path(), false)

	return nil
}

// Move moves records from source to destination
func (ts *ThreadsafeKVS) Move(from, to kvs.KeyPath) error {
	ts.mutex.Lock()
	defer ts.mutex.Unlock()

	err := ts.db.Move(from, to)
	if err != nil {
		return err
	}

	// Invalidate cache entries that might be affected
	ts.invalidateCachePrefixLocked(from.Path(), false)
	ts.invalidateCachePrefixLocked(to.Path(), false)

	return nil
}

// Backup creates a backup of the database
func (ts *ThreadsafeKVS) Backup(path string) (string, error) {
	ts.mutex.RLock()
	defer ts.mutex.RUnlock()

	return ts.db.Backup(path)
}
