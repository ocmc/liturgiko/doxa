// Package inOut provides constants and a line parser for importing files to be written out to the database
package inOut

import (
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/bible"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/enums/properties"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"strings"
)

// LineParser splits the line using the delimiter, validates it,
// and returns the key path and value to use to write a record to
// the database.
// Regarding the semantics of the split fields, two forms are supported:
//  1. if the length of the slice resulting from the split is exactly two,
//     the first part is assumed to be an ID path, the last the value.
//  2. if the length is greater than 2, the assumption is that
//     the last part is the value, the second to last is the key,
//     and the remaining previous parts are the directory parts.
//
// Depending on the first part of the Directory path, validations
// of the entire path are made and an error will be returned if it is invalid.
func LineParser(line, delimiter string, removeQuotes bool, replace *kvs.DirReplace) (*kvs.KeyPath, []byte, error) {
	/*if strings.HasSuffix(line, "actors:ac.IL.Priest") {
		fmt.Printf("TODO")
	}*/
	kp := kvs.NewKeyPath()
	var value []byte
	parts := strings.Split(line, delimiter)
	err := kp.ParsePath(RemoveSpace(parts[0]))
	if err != nil {
		if len(parts) == 1 {
			return nil, nil, fmt.Errorf("line must be tab delimited with exactly two parts: the ID and the value")
		} else {
			return nil, nil, fmt.Errorf("invalid ID: %v", err)
		}
	}
	if kp.KeyParts == nil || kp.KeyParts.Size() == 0 {
		return nil, nil, fmt.Errorf("missing colon between topic and key")
	}
	if len(parts) > 1 { // could be a record with an ID but empty value
		txt := strings.TrimSpace(parts[1])
		if len(txt) > 0 {
			if strings.HasPrefix(txt, "@") {
				redirectKp := kvs.NewKeyPath()
				err = redirectKp.ParsePath(RemoveSpace(txt))
				if err != nil || redirectKp.KeyParts.Empty() {
					return nil, nil, fmt.Errorf("invalid redirect ID: %v", err)
				}
			}
			// If the user created the import file by exporting Excel or Numbers,
			// 1) values with enclosing quotes will be prefixed and suffixed with two more quotes
			// 2) unquoted values with a comma in them will be enclosed with a single quote.
			if strings.HasPrefix(txt, "\"\"") && strings.HasSuffix(txt, "\"\"") {
				txt = txt[2 : len(txt)-2]
			} else if strings.HasPrefix(txt, "\"") && strings.HasSuffix(txt, "\"") {
				if removeQuotes {
					txt = txt[1 : len(txt)-1]
				}
			}
		}
		value = []byte(txt)
	}
	// validate dirs
	switch kp.Dirs.First() {
	case "btx": // btx/en_uk_kjv/act/c001:001
		// 2nd must be a library
		_, err := ltstring.ToLibraryParts(kp.Dirs.Get(1))
		if err != nil {
			return nil, nil, fmt.Errorf("invalid library")
		}
		// dir size must be 4
		if kp.Dirs.Size() != 4 {
			return nil, nil, fmt.Errorf("ID should be btx/library/book abbreviation/chapter:verseNbr")
		}
		// dir 3 must be valid Bible book abbreviation
		if !bible.IsValidAbbreviation(kp.Dirs.Get(2)) {
			// TODO tell them what abbreviations are valid
			return nil, nil, fmt.Errorf("invalid Bible book abbreviation %s", kp.Dirs.Get(2))
		}
		var normalizedChapter string
		// dir last must be an int or a string len 4 and prefix c and 1-3 must be a number
		normalizedChapter, err = bible.ToNormalizedChapterNumber(kp.Dirs.Get(3))
		if err != nil {
			return nil, nil, fmt.Errorf("invalid chapter number")
		}
		kp.Dirs.Set(3, normalizedChapter)
		// key must be an int
		normalizedVerse, err := bible.ToNormalizedVerseNumber(kp.KeyParts.First())
		if err != nil {
			return nil, nil, fmt.Errorf("invalid verse number")
		}
		kp.KeyParts.Pop()
		kp.KeyParts.Push(normalizedVerse)
	case "configs":
		if kp.Dirs.Size() > 2 {
			var path string
			if kp.IsMapEntry() || kp.IsSliceElement() {
				path = "/" + kp.Dirs.SubPath(2, kp.Dirs.Size()-1, "/")
			} else {
				path = "/" + kp.Dirs.SubPath(2, kp.Dirs.Size(), "/")
			}
			if strings.HasPrefix(path, "/site/build/pdf/fonts/") {
				// properties contains keys for a default font,
				// but not individual language fonts.
				// When a language is added to the layouts,
				// doxa automatically generates the properties
				// using the language code.  In order to
				// verify the path is valid, we need to
				// substitute 'default' for the language code.
				pathParts := strings.Split(path, "/")
				path = fmt.Sprintf("%s/default/%s", strings.Join(pathParts[:5], "/"), strings.Join(pathParts[6:], "/"))
			}
			index := properties.PropertyForPath(path)
			if index == -1 {
				if properties.Obsolete(path) {
					return nil, nil, fmt.Errorf("obsolete configuration property: delete it from your database")
				}
				path = path + "/" + kp.KeyParts.First()
				index = properties.PropertyForPath(path)
				if index == -1 {
					if properties.Obsolete(path) {
						msg := fmt.Sprintf("obsolete configuration property %s: delete it from your database", path)
						doxlog.Warnf(msg)
						return nil, nil, fmt.Errorf(msg)
					}
					msg := fmt.Sprintf("unknown configuration property: %s - delete it from your database", path)
					doxlog.Warnf(msg)
					return nil, nil, fmt.Errorf(msg)
				}
			}
			// Uncomment if we want to disallow
			// the import of the property description.
			// These are set by the programmer, not the user.
			//if index.Data().DescOnly {
			//	return nil, nil, nil
			//}
		}
	case "ltx": // ltx/ara_us_aocana/he.h.m1:ChristosGennatai.text
		// 2nd must be a library
		_, err := ltstring.ToLibraryParts(kp.Dirs.Get(1))
		if err != nil {
			return nil, nil, fmt.Errorf("invalid library format, should be languageCode_countryCode_realm")
		}
		// dir size must be 3
		if kp.Dirs.Size() != 3 {
			return nil, nil, fmt.Errorf("%s is an invalid ID, should be ltx/library/topic", kp.Dirs.Join("/"))
		}
	case "media": // media/en_us_public/client/cl.bishop1.fimi/c1/score/w/1:link.info
		// 2nd must be a library
		_, err := ltstring.ToLibraryParts(kp.Dirs.Get(1))
		if err != nil {
			return nil, nil, fmt.Errorf("invalid library format, should be languageCode_countryCode_realm")
		}
		// key must have suffix link.info or link.path
		if kp.KeyParts.First() != "link.info" &&
			kp.KeyParts.First() != "link.path" {
			return nil, nil, fmt.Errorf("invalid key in path %s, must be either link.info or link.path", kp.Path())
		}
	default:
		// ignore
	}
	return kp, value, nil
}

// RemoveSpace trims spaces before and after and in the middle of the string
func RemoveSpace(s string) string {
	return strings.ReplaceAll(strings.TrimSpace(s), " ", "")
}

// ConsolidateErrors returns a single occurrence for each type of error,
// for each type
func ConsolidateErrors(errors []error) []string {
	var newErrors []string
	typeMap := make(map[string][]string)
	for _, e := range errors {
		l, msg := ParseError(e)
		var ok bool
		var v []string
		if v, ok = typeMap[msg]; ok {
			typeMap[msg] = append(v, l)
		} else {
			if l == "?" {
				typeMap[msg] = []string{}
			} else {
				typeMap[msg] = append(v, l)
			}
		}
	}
	for k, v := range typeMap {
		newErrors = append(newErrors, fmt.Sprintf("%s in line(s): %s", k, strings.Join(v, ", ")))
	}
	return newErrors
}

// ParseError returns the line number and error message
func ParseError(err error) (string, string) {
	e := fmt.Sprintf("%v", err)
	parts := strings.Split(e, ":")
	if len(parts) == 1 {
		return "?", e
	}
	if !strings.HasPrefix(parts[0], "line ") {
		return "?", e
	}
	l := strings.TrimSpace(parts[0])
	if len(l) < 6 {
		return "?", e
	}
	l = parts[0][5:]
	return l, parts[1]
}
