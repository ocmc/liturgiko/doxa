package inOut

import (
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"os"
	"testing"
)

func TestKVP_Import(t *testing.T) {
	dbPath := "/Users/mac002/doxa/projects/doxa-alwb/db/liturgical_imported.db"
	importDirPath := "/Volumes/ssd2/doxa/exports/all"
	var err error
	if ltfile.FileExists(dbPath) {
		err = os.Remove(dbPath)
		if err != nil {
			t.Errorf("error deleting %s: %v", dbPath, err)
			return
		}
	}
	ds, err := kvs.NewBoltKVS(dbPath)
	if err != nil {
		t.Error(err)
		return
	}
	var removeQuotes = true
	var allOrNone = true
	var skipObsoleteConfigs = true
	var count int
	var filePaths []string
	filePaths, err = ltfile.FileMatcher(importDirPath, "tsv", nil)
	if err != nil {
		t.Error(err)
	}
	if len(filePaths) < 1 {
		t.Errorf("no matching import files")
	}
	for _, f := range filePaths {
		var localCount int
		var localErrors []error
		localCount, localErrors = ds.Import(f, LineParser, removeQuotes, allOrNone, skipObsoleteConfigs)
		if len(localErrors) > 0 {
			for _, err = range localErrors {
				fmt.Printf("\n%v\n", err)
			}
			fmt.Println("")
			t.Errorf("errors importing %s", f)
			return
		}
		count = count + localCount
	}
	if count == 0 {
		t.Errorf("no records imported")
		return
	}
	fmt.Printf("%d records imported.\n", count)
}
