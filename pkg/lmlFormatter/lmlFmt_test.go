package lmlFormatter

import (
	"fmt"
	"strings"
	"testing"
)

func TestFormatLines(t *testing.T) {
	c := `
id = "a-templates/Blocks/MA/_12_Exaposteilarion__/append__/me__/exap123t/bl.theotokion_"
type = "block"
status = "na"
/* I
am
a multiline
comment
*/
if exists rid "me.*:meMA.ExapTheotokion.text" {
switch dayOfWeek {
case mon, tue, thu, sat, sun: { // I am an inline comment
insert "a-templates/Blocks/MA/_12_Exaposteilarion__/append__/me__/exap123t/theotokion_/bl.exaptheotokion_"
}
case wed, fri: {
if exists rid "me.*:meMA.ExapStavrotheotokion.text" {
insert "a-templates/Blocks/MA/_12_Exaposteilarion__/append__/me__/exap123t/theotokion_/bl.exapstavrotheotokion"
} else {
if exists rid "me.*:meMA.ExapTheotokion.text" {
switch modeOfWeek {
case 1: {
p.actor nid "Priest"
}
default: {
p.actor nid "Deacon"
}
}
insert "a-templates/Blocks/MA/_12_Exaposteilarion__/append__/me__/exap123t/theotokion_/bl.exaptheotokion_"
}
}
}
}
} 
if exists rid "me.*:meMA.ExapTheotokion.text" {p.Actor nid "choir"}
`
	lines := strings.Split(c, "\n")
	formattedLines, err := FormatLines(lines)
	if err != nil {
		t.Error(err)
	}
	fmt.Printf(strings.Join(formattedLines, "\n"))
}

func TestFormatLines2(t *testing.T) {
	c := `
id = "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/bl.specific_day"
type = "block"
status = "na"
switch date {
case feb 2 thru 9: {
switch movableCycleDay {
case 1 thru 13: {
switch date {
case feb 2 thru 9: {
            insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.feb02_antiphon3"
          }
        }
      }
      case 14: {
        switch date {
          case feb 2 thru 5: {
            insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.feb02_antiphon3"
          }
        }
      }
      case 15: {
        switch date {
          case feb 2 thru 6: {
            insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.feb02_antiphon3"
          }
        }
      }
      case 16: {
        switch date {
          case feb 2 thru 7: {
            insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.feb02_antiphon3"
          }
        }
      }
      case 17: {
        switch date {
          case feb 2 thru 8: {
            insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.feb02_antiphon3"
          }
        }
      }
      case 18: {
        switch date {
          case feb 2 thru 5: {
            insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.feb02_antiphon3"
          }
        }
      }
      case 19: {
        switch date {
          case feb 2 thru 6: {
            insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.feb02_antiphon3"
          }
        }
      }
      case 20: {
        switch date {
          case feb 2 thru 5: {
            insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.feb02_antiphon3"
          }
        }
      }
      case 21: {
        switch date {
          case feb 2 thru 3: {
            insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.feb02_antiphon3"
          }
        }
      }
      case 22: {
        switch date {
          case feb 2 thru 7: {
            insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.feb02_antiphon3"
          }
        }
      }
      case 23: {
        switch date {
          case feb 2: {
            insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.feb02_antiphon3"
          }
        }
      }
      default: {
        insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.feb02_antiphon3"
      }
    }
  }
  case mar 25: {
    insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.mar25_antiphon3"
  }
  case mar 26: {
    insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.mar26_antiphon3"
  }
  default: {
    switch movableCycleDay {
      case 29: {
        insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.lent1_antiphon3"
      }
      case 43: {
        insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.lent3_antiphon3"
      }
      case 56: {
        insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.akathist_antiphon3"
      }
      case 63: {
        insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.lazarus_antiphon3"
      }
      case 64: {
        insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.palm_antiphon3"
      }
      case 71 thru 109: {
        insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.pascha_antiphon3"
      }
      case 110 thru 118: {
        insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.ascension_antiphon3"
      }
      case 120 thru 126: {
        insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.pentecost_antiphon3"
      }
      default: {
        switch date {
          case jan 1: {
            insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.jan01_antiphon3"
          }
          case jan 6 thru 14: {
            insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.jan06_antiphon3"
          }
          case aug 6 thru 12: {
            insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.aug06_antiphon3"
          }
          case aug 15 thru 23: {
            insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.aug15_antiphon3"
          }
          case sep 1: {
            switch dayOfWeek {
              case sun: {
                insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.sunday_antiphon3"
              }
              default: {
                insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.sep01_antiphon3"
              }
            }
          }
          case sep 8 thru 12: {
            insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.sep08_antiphon3"
          }
          case sep 13: {
            switch dayOfWeek {
              case sun: {
                insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.sunday_antiphon3"
              }
              default: {
                insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.sep13_antiphon3"
              }
            }
          }
          case sep 14 thru 21: {
            insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.sep14_antiphon3"
          }
          case nov 21 thru 25: {
            insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.nov21_antiphon3"
          }
          case dec 25 thru 31: {
            insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.dec25_antiphon3"
          }
          default: {
            switch dayOfWeek {
              case sun: {
                insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.sunday_antiphon3"
              }
              default: {
                insert "a-templates/Blocks/Selectors/Selector_Antiphons/antiphon3__/specific_day/bl.weekday_antiphons3"
              }
            }
          }
        }
      }
    }
  }
}
`
	lines := strings.Split(c, "\n")
	formattedLines, err := FormatLines(lines)
	if err != nil {
		t.Error(err)
	}
	fmt.Printf(strings.Join(formattedLines, "\n"))
}
