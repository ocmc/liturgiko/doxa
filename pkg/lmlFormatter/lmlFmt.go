package lmlFormatter

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/stack"
	"strings"
)

const Indent = "  "

// FormatFile reads the lines from the specified file,
// passes them to FormatLines, then writes the formatted lines back to the file.
func FormatFile(fileToFormat string) error {
	lines, err := ltfile.GetFileLines(fileToFormat)
	if err != nil {
		return fmt.Errorf("error opening %s: %v", fileToFormat, err)
	}
	var formattedLines []string
	formattedLines, err = FormatLines(lines)
	if err != nil {
		return fmt.Errorf("lmlFmt.FormatFile %s: %v", fileToFormat, err)
	}
	return ltfile.WriteLinesToFile(fileToFormat, formattedLines)
}

// FormatLines this is split out from func Format
// so that we can write test cases
// without having to serialize them back to the file
func FormatLines(lines []string) ([]string, error) {
	indentStack := stack.StringStack{}
	var linesOut []string
	for _, l := range lines {
		if len(l) == 0 {
			linesOut = append(linesOut, l)
			continue
		}
		l = strings.TrimSpace(l)
		l = strings.ReplaceAll(l, "  ", " ")
		if strings.HasPrefix(l, "}") {
			indentStack.Pop()
		}
		closeBraceIndex := strings.Index(l, "}")
		if closeBraceIndex > 1 {
			// ensure inline closing brace has space before it
			if !strings.Contains(l, " }") {
				l = strings.ReplaceAll(l, "}", " }")
			}
		}
		// ensure opening brace has space before it
		if strings.Contains(l, "{") {
			if !strings.Contains(l, " {") {
				l = strings.ReplaceAll(l, "{", " {")
			}
			if !strings.HasSuffix(l, "{") { // it has a statement after it
				if !strings.Contains(l, "{ ") {
					l = strings.ReplaceAll(l, "{", "{ ")
				}

			}
		}
		if strings.Contains(l, "pdf.covers:logo_line") {
			l = "hr.solid1"
		}

		linesOut = append(linesOut, fmt.Sprintf("%s%s", indentStack.Join(""), l))
		if strings.HasPrefix(l, "case") ||
			strings.HasPrefix(l, "default") ||
			strings.HasPrefix(l, "} else") ||
			strings.HasPrefix(l, "} else if") ||
			strings.HasPrefix(l, "if") ||
			strings.HasPrefix(l, "switch") ||
			strings.Contains(l, "{") {
			indentStack.Push(Indent)
		}
		if closeBraceIndex > 1 {
			if strings.HasSuffix(l, "}") {
				indentStack.Pop()
			}
		}
	}
	var err error
	if !indentStack.Empty() {
		if indentStack.Size() == 1 {
			err = fmt.Errorf("unbalanced brace")
		} else {
			err = fmt.Errorf("%d unbalanced braces", indentStack.Size())
		}
	}
	if strings.TrimSpace(linesOut[len(linesOut)-1]) != "" {
		linesOut = append(linesOut, " ")
	}
	return linesOut, err
}
