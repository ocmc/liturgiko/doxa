// Package ltm (LML Template Mapper) provides an interface for reading, writing, and searching templates written in LML.
// By implementing this as an interface, it is possible to store templates either in a database or the file system.
// It also handles primary and fallback templates.  That is, when a template request is
// received, it first checks the primary templates' folder, and if not found, the fallback, which
// usually is a subscription to templates.
package ltm

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/enums/statuses"
	"github.com/liturgiko/doxa/pkg/enums/templateTypes"
	"strconv"
	"strings"
	"time"
)

const (
	PRIMARY  = "primary"
	FALLBACK = "fallback"
)

type Data struct {
	Path      string
	Content   string
	Date      time.Time
	Day       int
	DayOfWeek int
	Month     int
	Year      int
	Status    statuses.Status
	Type      templateTypes.TemplateType
}

// SetMetaData searches the content to find:
// Status (by calling GetStatus)
// Month (by calling GetMonth)
// Day (by calling GetDay)
// Year (by calling GetYear)
// And sets DayOfMonth via a time.Time object created using Month, Day, Year.
// If Year is not set, the current year is used.
// Type, e.g. Book, Service, Block (by calling GetType)
// If not found, an error is returned.
func (d *Data) SetMetaData() error {
	var err error
	if d.Type, err = d.GetType(); err != nil {
		return err
	}
	if d.Status, err = d.GetStatus(); err != nil {
		return err
	}
	if d.Type == templateTypes.Service {
		if d.Month, err = d.GetMonth(); err != nil {
			return err
		}
		if d.Day, err = d.GetDay(); err != nil {
			return err
		}
		d.Year, err = d.GetYear()
		if err != nil {
			d.Year = time.Now().UTC().Year()
		}
		d.Date = time.Date(d.Year, time.Month(d.Month), d.Day, 0, 0, 0, 0, time.UTC)
		d.DayOfWeek = int(d.Date.Weekday())
	}
	return err
}

// GetStatus searches the content to find the status of the template.
// Returns the status found in the content.
// If not found, an error is returned an error is returned.
func (d *Data) GetStatus() (statuses.Status, error) {
	lenContent := len(d.Content)
	var statusCode statuses.Status
	var err error
	subString := `status = "`
	lenSubString := len(subString)
	if lenContent > 0 {
		index := strings.Index(d.Content, subString)
		if index > 0 && lenContent-index-lenSubString > 8 {
			start := index + lenSubString
			status := strings.TrimSpace(d.Content[start : start+8])
			index = strings.Index(status, "\"")
			if index > 0 {
				status = strings.TrimSpace(status[:index])
				statusCode, err = statuses.StatusCode(status)
				if err != nil {
					err = fmt.Errorf("expected %s but got: %v", status, err)
				}
			}
		}
	} else {
		statusCode = -1
		err = fmt.Errorf("empty template, no status found")
	}
	return statusCode, err
}

// GetType searches the content to find the type of the template.
// Returns the type found in the content.
// If not found, an error is returned.
func (d *Data) GetType() (templateTypes.TemplateType, error) {
	lenContent := len(d.Content)
	var templateType templateTypes.TemplateType
	var err error
	subString := `type = "`
	lenSubString := len(subString)
	if lenContent > 0 {
		index := strings.Index(d.Content, subString)
		if index > 0 && lenContent-index-lenSubString > 8 { // max expected value is service (7 char)
			start := index + lenSubString
			typeName := strings.TrimSpace(d.Content[start : start+8])
			index = strings.Index(typeName, "\"")
			if index > 0 {
				typeName = strings.TrimSpace(typeName[:index])
				templateType = templateTypes.TypeForName(typeName)
				if err != nil {
					err = fmt.Errorf("expected %s but got: %v", typeName, err)
				}
			}
		}
	} else {
		templateType = -1
		err = fmt.Errorf("template type not found")
	}
	return templateType, err
}

// GetMonth searches the content to find the service month of the template.
// Returns the month found in the content.
// If not found, an error is returned.
func (d *Data) GetMonth() (int, error) {
	lenContent := len(d.Content)
	var month int
	var err error
	subString := `month = `
	lenSubString := len(subString)
	if lenContent > 0 {
		index := strings.Index(d.Content, subString)
		if index > 0 && lenContent-index-lenSubString > 2 {
			start := index + lenSubString
			strMonth := strings.TrimSpace(d.Content[start : start+2])
			month, err = strconv.Atoi(strMonth)
		}
	} else {
		err = fmt.Errorf("no month found")
	}
	return month, err
}

// GetDay searches the content to find the service day of the template.
// Returns the day found in the content.
// If not found, an error is returned.
func (d *Data) GetDay() (int, error) {
	lenContent := len(d.Content)
	var day int
	var err error
	subString := `day = `
	lenSubString := len(subString)
	if lenContent > 0 {
		index := strings.Index(d.Content, subString)
		if index > 0 && lenContent-index-lenSubString > 2 {
			start := index + lenSubString
			strDay := strings.TrimSpace(d.Content[start : start+2])
			day, err = strconv.Atoi(strDay)
		}
	} else {
		err = fmt.Errorf("no day found")
	}
	return day, err
}

// GetYear searches the content to find the service year of the template.
// Note that year is optional in templates, in which case the current year is used
// to instantiate a time.Time object.
// Returns the year found in the content.
// If not found, an error is returned.
func (d *Data) GetYear() (int, error) {
	lenContent := len(d.Content)
	var year int
	var err error
	subString := `year = `
	lenSubString := len(subString)
	if lenContent > 0 {
		index := strings.Index(d.Content, subString)
		if index > 0 && lenContent-index-lenSubString > 5 {
			start := index + lenSubString
			strYear := strings.TrimSpace(d.Content[start : start+4])
			year, err = strconv.Atoi(strYear)
		}
	} else {
		err = fmt.Errorf("no found found")
	}
	return year, err
}

type LTM interface {
	GetContent(templatePath string) (string, error)
	GetMatchingIDs(idPatterns []string, dirExclusionPatterns []string) ([]string, error)
	GetMatchingTemplates(idPatterns []string, dirExclusionPatterns []string, statuses []statuses.Status, daysOfWeek []int) ([]*Data, error)
	GetPrimary() string
	GetFallback() string
	GetRoot() string
	IsValidPath(templatePath string) bool
	IsReadOnly(templatePath string) bool
	// Resolve first checks to see if the templateId occurs in the primary root dir.
	// If not, it checks the fallback root dir.
	// Wherever it is found first (primary vs fallback) the absolute path is returned.
	// If it is not found in either, an error is returned.
	Resolve(templateId string) (string, error)
	SaveContent(templatePath, content string) error
	ToCanonicalPath(templatePath string) string
}
