package ltm

import (
	"github.com/liturgiko/doxa/pkg/enums/statuses"
	"github.com/liturgiko/doxa/pkg/enums/templateTypes"
	"testing"
)

func TestData_GetStatus(t *testing.T) {
	data := new(Data)
	data.Content = `
id = "a-templates/Dated-CompareServices/m04/d02/se.m04.d02.co"
type = "service"
status = "review"
month = 4
day = 2
year = 2023
pageHeaderEven = center @lookup sid "template.titles:co.pdf.header" rid "tr.*:tr.trCO.header" rid "da.*:da.daVE.OnTheEveningBefore" ver 1
pageHeaderOdd = center @lookup sid "template.titles:co.pdf.header" rid "tr.*:tr.trCO.header" rid "da.*:da.daVE.OnTheEveningBefore" ver 2
pageFooterEven = center @pageNbr
pageFooterOdd = center @pageNbr
pageNbr = 1
switch movableCycleDay {
  case 1 thru 127: {
    insert "a-templates/Movable-Cycle/mcMaps/bl.mc.map.co"
  }
  default: {
  }
}`
	err := data.SetMetaData()
	if err != nil {
		t.Error(err)
	}
	if data.Type != templateTypes.Service {
		t.Errorf("expected type == service, but got %s", templateTypes.NameForType(data.Type))
	}
	if data.Status != statuses.Review {
		t.Errorf("expected status == final, but got %s", statuses.String(data.Status))
	}
	intExpect := 4
	if data.Month != intExpect {
		t.Errorf("expected month == %d, but got %d", intExpect, data.Month)
	}
	intExpect = 2
	if data.Day != intExpect {
		t.Errorf("expected day == %d, but got %d", intExpect, data.Day)
	}
	intExpect = 2023
	if data.Year != intExpect {
		t.Errorf("expected year == %d, but got %d", intExpect, data.Year)
	}
	intExpect = 0
	if data.DayOfWeek != intExpect {
		t.Errorf("expected day of week == %d, but got %d", intExpect, data.DayOfWeek)
	}
}
