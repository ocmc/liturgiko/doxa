package ltm

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/enums/statuses"
	"github.com/liturgiko/doxa/pkg/enums/templateTypes"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"path"
	"slices"
	"strings"
)

type LTFM struct {
	PrimaryDirPath   string // file system path to templates directory to search first
	FallbackDirPath  string // file system path to templates directory to search if not found in primary
	FallbackEnabled  bool   // if true, primary and backup will be used.  Otherwise, root will be used.
	Root             string // this is the file system path to the root directory for the templates. Normally this is the same as the primary.
	SubscriptionPath string // the path to doxa/prescriptions
}

// NewLTFM returns a populated LTFM
// primaryRootDir is the top templates directory to search first
// fallbackRootDir is the one to search if a template is not found in the primary.
// if fallbackRootDir is empty string, it won't be searched
// subscriptionsPath is the path to doxa/subscriptions
func NewLTFM(primaryRootDir, fallbackRootDir, root, subscriptionsPath string, fallbackEnabled bool) *LTFM {
	l := new(LTFM)
	l.PrimaryDirPath = primaryRootDir
	l.FallbackEnabled = fallbackEnabled
	l.FallbackDirPath = fallbackRootDir
	if l.FallbackEnabled {
		if len(l.PrimaryDirPath) == 0 || len(l.FallbackDirPath) == 0 {
			l.FallbackEnabled = false
		}
	}
	l.Root = root
	l.SubscriptionPath = subscriptionsPath
	return l
}
func (l *LTFM) GetContent(templatePath string) (string, error) {
	thePath := l.ToCanonicalPath(templatePath)
	if l.FallbackEnabled {
		if thePath == templatePath { // we are going to first check the primary templates folder
			if ltfile.FileExists(path.Join(l.PrimaryDirPath, thePath)) { // use primary
				thePath = path.Join(l.PrimaryDirPath, thePath)
			} else if ltfile.FileExists(path.Join(l.FallbackDirPath, thePath)) { // use fallback
				thePath = path.Join(l.FallbackDirPath, thePath)
			}
		}
	}
	return ltfile.GetFileContent(thePath)
}

func (l *LTFM) getPath(templatePath string) (string, error) {
	if strings.HasPrefix(templatePath, FALLBACK) {
		return path.Join(l.PrimaryDirPath, templatePath[len(FALLBACK):]), nil
	} else {
		return path.Join(l.PrimaryDirPath, templatePath[len(PRIMARY):]), nil
	}
}
func (l *LTFM) GetPrimary() string {
	return l.PrimaryDirPath
}
func (l *LTFM) GetFallback() string {
	return l.FallbackDirPath
}
func (l *LTFM) Resolve(templateId string) (string, error) {
	thePath := path.Join(l.PrimaryDirPath, templateId)
	if ltfile.FileExists(thePath) {
		return thePath, nil
	}
	if len(l.FallbackDirPath) == 0 {
		return "", fmt.Errorf("%s not found", templateId)
	}
	thePath = path.Join(l.FallbackDirPath, templateId)
	if ltfile.FileExists(thePath) {
		return thePath, nil
	} else {
		return "", fmt.Errorf("%s not found in either primary or backup templates", templateId)
	}
}
func (l *LTFM) GetMatchingIDs(idPatterns []string, dirExclusionPatterns []string) ([]string, error) {
	var response []string
	// if fallback is enabled, we need to first find matches in primary
	// then in fallback.  If both primary and fallback match the same ID,
	// we need to use the one from the primary, not the fallback.
	// if fallback is not enabled, we will use the project templates directory, l.Root
	if !l.FallbackEnabled {
		return l.getMatchingIDs(l.Root, idPatterns, dirExclusionPatterns)
	}
	// fallback is enabled.
	matchMap := make(map[string]string)
	// first search the primary.
	var err error
	response, err = l.getMatchingIDs(l.PrimaryDirPath, idPatterns, dirExclusionPatterns)
	if err != nil {
		return nil, err
	}
	// add the results to the map
	for _, match := range response {
		key := match[len(l.PrimaryDirPath):]
		matchMap[key] = match
	}
	// second, search the fallback.
	response, err = l.getMatchingIDs(l.FallbackDirPath, idPatterns, dirExclusionPatterns)
	if err != nil {
		return nil, err
	}
	// if the template ID is already in the map, ignore it.
	// otherwise add it to the map.
	for _, match := range response {
		key := match[len(l.FallbackDirPath):]
		var found bool
		if _, found = matchMap[key]; !found {
			matchMap[key] = match
		}
	}
	// populate the response with the values in the map.
	response = []string{}
	for _, v := range matchMap {
		response = append(response, v)
	}
	slices.Sort(response)
	return response, nil
}
func (l *LTFM) getMatchingIDs(templateDirPath string, idPatterns []string, dirExclusionPatterns []string) ([]string, error) {
	var response []string
	// if fallback enabled, we need to first find matches in primary
	// then in secondary.  If both primary and fallback match the same ID,
	// we need to remove the one from the fallback result.

	matches, err := ltfile.FileMatcher(templateDirPath, "lml", idPatterns)
	if err != nil {
		return nil, err
	}
outer:
	for _, match := range matches {
		if dirExclusionPatterns != nil {
			for _, p := range dirExclusionPatterns {
				if strings.Contains(match, p) {
					continue outer
				}
			}
		}
		response = append(response, match)
	}
	return response, nil
}
func (l *LTFM) GetMatchingTemplates(idPatterns []string, dirExclusionPatterns []string, statuses []statuses.Status, daysOfWeek []int) ([]*Data, error) {
	var result []*Data
	// if fallback is enabled, we need to first find matches in primary
	// then in fallback.  If both primary and fallback match the same ID,
	// we need to use the one from the primary, not the fallback.
	// if fallback is not enabled, we will use the project templates directory, l.Root
	if !l.FallbackEnabled {
		return l.getMatchingTemplates(l.Root, idPatterns, dirExclusionPatterns, statuses, daysOfWeek)
	}
	// fallback is enabled.
	matchMap := make(map[string]*Data)
	// first search the primary.
	var err error
	result, err = l.getMatchingTemplates(l.PrimaryDirPath, idPatterns, dirExclusionPatterns, statuses, daysOfWeek)
	if err != nil {
		return nil, err
	}
	// add the results to the map
	for _, match := range result {
		key := match.Path[len(l.PrimaryDirPath):]
		matchMap[key] = match
	}
	// second, search the fallback.
	result, err = l.getMatchingTemplates(l.FallbackDirPath, idPatterns, dirExclusionPatterns, statuses, daysOfWeek)
	if err != nil {
		return nil, err
	}
	// if the template ID is already in the map, ignore it.
	// otherwise add it to the map.
	for _, match := range result {
		key := match.Path[len(l.FallbackDirPath):]
		var found bool
		if _, found = matchMap[key]; !found {
			matchMap[key] = match
		}
	}
	// populate the response with the values in the map.
	result = []*Data{}
	for _, v := range matchMap {
		result = append(result, v)
	}
	return result, nil
}
func (l *LTFM) getMatchingTemplates(templateDirPath string, idPatterns []string, dirExclusionPatterns []string, theStatuses []statuses.Status, daysOfWeek []int) ([]*Data, error) {
	paths, err := ltfile.FileMatcher(templateDirPath, "lml", idPatterns)
	if err != nil {
		return nil, fmt.Errorf("ltfm GetMatching: %v", err)
	}
	var result []*Data
outer:
	for _, thePath := range paths {
		for _, dirExclusionPattern := range dirExclusionPatterns {
			if strings.Contains(thePath, dirExclusionPattern) {
				continue outer
			}
		}
		d := new(Data)
		d.Path = thePath
		d.Content, err = l.GetContent(thePath)
		if err != nil {
			return nil, fmt.Errorf("error reading %s: %v", thePath, err)
		}
		d.Content = strings.ReplaceAll(d.Content, "  ", " ")
		err = d.SetMetaData()
		if err != nil {
			return nil, err
		}
		var okToProcess bool
		// filter by status
		if len(theStatuses) > 0 && theStatuses[0] != statuses.NA {
			for _, s := range theStatuses {
				if s == d.Status {
					okToProcess = true
				}
			}
		} else {
			okToProcess = true
		}
		if !okToProcess {
			continue
		}
		// filter by day of week if this is a service
		if d.Type == templateTypes.Service {
			okToProcess = false
			if daysOfWeek == nil || len(daysOfWeek) == 0 {
				okToProcess = true
			} else {
				for _, day := range daysOfWeek {
					if d.DayOfWeek == day {
						okToProcess = true
						break
					}
				}
			}
		}
		if !okToProcess {
			continue
		}
		result = append(result, d)
	}
	return result, nil
}
func (l *LTFM) GetRoot() string {
	return l.Root
}
func (l *LTFM) SaveContent(templatePath, content string) error {
	return ltfile.WriteFile(l.ToCanonicalPath(templatePath), content)
}
func (l *LTFM) ToCanonicalPath(templatePath string) string {
	if !strings.HasSuffix(templatePath, ".lml") {
		templatePath = templatePath + ".lml"
	}
	if l.FallbackEnabled {
		// see if already has full path for primary dir
		if strings.HasPrefix(templatePath, l.PrimaryDirPath) {
			return templatePath
		}
		// see if already has full path for fallback dir
		if strings.HasPrefix(templatePath, l.FallbackDirPath) {
			return templatePath
		}
		// does not have full path, so add it.
		if strings.HasPrefix(templatePath, PRIMARY) {
			return path.Join(l.PrimaryDirPath, templatePath[len(PRIMARY):])
		} else if strings.HasPrefix(templatePath, FALLBACK) {
			return path.Join(l.FallbackDirPath, templatePath[len(FALLBACK):])
		} else {
			testPath := path.Join(l.PrimaryDirPath, templatePath)
			if ltfile.FileExists(testPath) {
				return testPath
			}
			testPath = path.Join(l.FallbackDirPath, templatePath)
			if ltfile.FileExists(testPath) {
				return testPath
			}
		}
	} else {
		// see if it already is prefixed with the root path
		if strings.HasPrefix(templatePath, l.Root) {
			return templatePath
		}
		return path.Join(l.Root, templatePath)
	}
	return templatePath
}
func (l *LTFM) IsValidPath(templatePath string) bool {
	if l.FallbackEnabled {
		if strings.HasPrefix(templatePath, l.PrimaryDirPath) ||
			strings.HasPrefix(templatePath, l.FallbackDirPath) {
			return true
		}
	} else if strings.HasPrefix(templatePath, l.Root) {
		return true
	}
	return false
}
func (l *LTFM) IsReadOnly(templatePath string) bool {
	if strings.HasPrefix(templatePath, l.SubscriptionPath) {
		return true
	}
	return false
}
