package ltm

import (
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/enums/statuses"
	"github.com/liturgiko/doxa/pkg/enums/templateTypes"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"path"
	"sort"
	"strings"
)

type LTDM struct {
	mapper          *kvs.KVS
	PrimaryRootDir  string
	FallbackRootDir string
	Root            string
	ExportDir       string
}

func NewLTDM(db *kvs.KVS, primaryRootDir, fallbackRootDir, dbRoot string, exportDir string) *LTDM {
	l := new(LTDM)
	l.mapper = db
	l.PrimaryRootDir = primaryRootDir
	l.FallbackRootDir = fallbackRootDir
	l.Root = dbRoot
	l.ExportDir = exportDir
	return l
}
func (l *LTDM) GetPrimary() string {
	return l.PrimaryRootDir
}
func (l *LTDM) GetFallback() string {
	return l.FallbackRootDir
}
func (l *LTDM) Resolve(templateId string) (string, error) {
	// TODO: not implemented
	return "", nil
}
func (l *LTDM) SaveContent(templatePath, content string) error {
	// TODO: not implemented
	return nil
}

// LoadContentFromFs loads into the database all *.lml files found in the file system in the directory path.
// Returns the number of files loaded.
// It is a helper method and does not implement an interface method
// Usage: example code to load templates into database from file system
// ltdm := ltm.NewLTDM(mapper, "templates", siteBuilder.Paths.ExportPath)
// loadErr := ltdm.LoadContentFromFs(siteBuilder.Paths.TemplatesPath)
func (l *LTDM) LoadContentFromFs(fpath string) (int, error) {
	if !strings.HasSuffix(fpath, "/templates") {
		return 0, fmt.Errorf("fpath %s not a templates directory", fpath)
	}
	files, err := ltfile.FileMatcher(fpath, "lml", nil)
	if err != nil {
		return 0, err
	}
	if len(files) == 0 {
		return 0, fmt.Errorf("no lml files found in fpath %s", fpath)
	}
	var recs []*kvs.DbR
	// batch up the records to be written to the database
	for _, file := range files {
		// templates
		rec := new(kvs.DbR)
		rec.KP = kvs.NewKeyPath()
		dbPath := file[len(fpath)-9:]
		parts := strings.Split(dbPath, "/")
		dirPath := strings.Join(parts[:len(parts)-1], kvs.SegmentDelimiter)
		err = rec.KP.ParsePath(dirPath)
		if err != nil {
			return 0, err
		}
		rec.KP.KeyParts.Push(parts[len(parts)-1])
		rec.Value, err = ltfile.GetFileContent(file)
		recs = append(recs, rec)
	}
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(l.Root)
	// if we are loading from doxa/sites/{site}/templates dir, then export it before
	// we delete the templates dir in the database and reload it.
	if kp.Dirs.Get(kp.Dirs.Size()-1) != "exports" && kp.Dirs.Last() != "templates" {
		// export the current template dir if it exists, then delete it
		if l.mapper.Db.Exists(kp) {
			exportPath := path.Join(l.ExportDir, "templates.txt")
			err = l.mapper.Db.Export(kp, exportPath, kvs.ExportAsLine)
			if err != nil {
				return 0, fmt.Errorf("error exporting current templates in database to %s: %v", err, exportPath)
			}
		}
	}
	// delete the current template dir in the database
	if l.mapper.Db.Exists(kp) {
		err = l.mapper.Db.Delete(*kp)
		if err != nil {
			return 0, fmt.Errorf("error deleting old records in %s.  Your old templates were exported to %s if they existed. You might need to import them.", l.Root, l.ExportDir)
		}
	}
	return len(files), l.mapper.Db.Batch(recs)
}
func (l *LTDM) GetContent(path string) (string, error) {
	if !strings.HasPrefix(path, l.Root) {
		return "", fmt.Errorf("path %s does not match root %s", path, l.Root)
	}
	kp := kvs.NewKeyPath()
	err := kp.ParsePath(path)
	if err != nil {
		return "", err
	}
	var rec *kvs.DbR
	rec, err = l.mapper.Db.Get(kp)
	if err != nil {
		return "", err
	}
	return rec.Value, nil
}
func (l *LTDM) GetMatchingIDs(idPatterns []string, dirExclusionPatterns []string) ([]string, error) {
	var result []string
	m := kvs.NewMatcher()
	m.KP.Dirs.Push("templates")
	m.KP.Dirs.Push(".*")

	var sb strings.Builder
	sb.WriteString("templates/.*:")
	sb.WriteString("(")
	for i, p := range idPatterns {
		if i > 0 {
			sb.WriteString("|")
		}
		sb.WriteString(p)
	}
	sb.WriteString(")")
	m.ValuePattern = sb.String()
	m.Recursive = true
	m.IncludeEmpty = false
	m.UseAsRegEx()
	recs, err := l.mapper.Db.GetMatchingIDs(*m)
	if err != nil {
		return nil, err
	}
	for _, rec := range recs {
		result = append(result, rec.KP.Path())
	}
	return result, nil
}
func (l *LTDM) GetMatchingTemplates(idPatterns []string, dirExclusionPatterns []string, statuses []statuses.Status, daysOfWeek []int) ([]*Data, error) {
	var result []*Data
	templateMap := make(map[string]*Data)
	m := kvs.NewMatcher()
	m.KP.Dirs.Push("templates")
	m.KP.Dirs.Push(".*")

	var sb strings.Builder
	sb.WriteString("templates/.*:")
	sb.WriteString("(")
	for i, p := range idPatterns {
		if i > 0 {
			sb.WriteString("|")
		}
		sb.WriteString(p)
	}
	sb.WriteString(")")
	m.ValuePattern = sb.String()
	m.Recursive = true
	m.IncludeEmpty = false
	m.UseAsRegEx()
	recs, err := l.mapper.Db.GetMatchingIDs(*m)
	if err != nil {
		return nil, err
	}
	for _, rec := range recs {
		d := new(Data)
		d.Path = rec.KP.Key()
		d.Content = strings.ReplaceAll(rec.Value, "  ", " ")
		d.SetMetaData()
		var okToProcess bool
		// filter by status
		for _, s := range statuses {
			if s == d.Status {
				okToProcess = true
			}
		}
		if !okToProcess {
			continue
		}
		// filter by day of week if this is a service
		if d.Type == templateTypes.Service {
			okToProcess = false // set up for the next checks
			if daysOfWeek == nil || len(daysOfWeek) == 0 {
				okToProcess = true
			} else {
				for _, day := range daysOfWeek {
					if d.DayOfWeek == day {
						okToProcess = true
						break
					}
				}
			}
		}
		if !okToProcess {
			continue
		}
		var inMap bool
		// regEx template patterns might result
		// in overlaps.  By using a map we can
		// create a unique list of templates to process.
		if _, inMap = templateMap[d.Path]; !inMap {
			result = append(result, d)
			templateMap[d.Path] = d
		}
	}
	// sort the templates
	sort.Slice(result, func(i, j int) bool {
		return result[i].Path < result[j].Path
	})

	return result, nil
}
func (l *LTDM) GetRoot() string {
	return l.Root
}
func (l *LTDM) ToCanonicalPath(fpath string) string {
	if strings.Contains(fpath, kvs.IdDelimiter) {
		if !strings.HasPrefix(fpath, l.Root) {
			return path.Join(l.Root, fpath)
		}
		return fpath
	}
	parts := strings.Split(fpath, kvs.SegmentDelimiter)
	j := len(parts) - 1
	kp := kvs.NewKeyPath()
	if !strings.HasPrefix(fpath, l.Root) {
		kp.Dirs.Push(l.Root)
	}
	for i := 0; i < j; i++ {
		kp.Dirs.Push(parts[i])
	}
	kp.KeyParts.Push(parts[j])
	return kp.Path()
}
func (l *LTDM) IsValidPath(templatePath string) bool {
	// TODO: not implemented
	return false
}
func (l *LTDM) IsReadOnly(templatePath string) bool {
	// TODO: not implemented
	return false
}
