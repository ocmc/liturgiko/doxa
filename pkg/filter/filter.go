// Package filter provides a means to compare a string to a slice of strings.
// If there is a match, it is to be excluded (indicated by a return value == true)
package filter

import "strings"

type Excluder struct {
	Exclusions []string
}

func NewExcluder(exclusions []string) *Excluder {
	e := new(Excluder)
	e.Exclusions = exclusions
	return e
}
func (e *Excluder) ContainedIn(s string) bool {
	for _, exclude := range e.Exclusions {
		if strings.Contains(s, exclude) {
			return true
		}
	}
	return false
}

func (e *Excluder) IsPrefixIn(s string) bool {
	for _, exclude := range e.Exclusions {
		if strings.HasPrefix(s, exclude) {
			return true
		}
	}
	return false
}
func (e *Excluder) IsSuffixIn(s string) bool {
	for _, exclude := range e.Exclusions {
		if strings.HasSuffix(s, exclude) {
			return true
		}
	}
	return false
}
