// Package tkCompare compares two maps and reports
// the keys found between the two maps,
// and the number of times the keys occur in map 1 and map 2.
// If zero, it does not occur.
package tkCompare

import "fmt"

const (
	Map1 = 0
	Map2 = 1
)

type Comparer struct {
	Maps []map[string]int // key = topic:key, value = the number of times that key occurs, e.g. in HTML file
}

func NewComparer() *Comparer {
	c := new(Comparer)
	map1 := make(map[string]int)
	map2 := make(map[string]int)
	c.Maps = append(c.Maps, map1)
	c.Maps = append(c.Maps, map2)
	return c
}
func (c *Comparer) Add(s string, toMap int) error {
	if toMap > len(c.Maps) {
		return fmt.Errorf("toMap value %d is greater than the number of maps, which is 2", toMap)
	}
	var ok bool
	var i int
	if i, ok = c.Maps[toMap][s]; ok {
		// do nothing, we just set the value for i
	}
	c.Maps[toMap][s] = i + 1
	return nil
}

// Compare key = keys from map 1 and map 2, value == M1:{#times occurs in map 1} M2:{#times occurs in map 2}
func (c *Comparer) Compare() map[string]Stats {
	cmpMap := make(map[string]Stats)
	var ok bool
	var i int
	// see if all keys in map 1 occur in map 2
	for k, v := range c.Maps[Map1] {
		if i, ok = c.Maps[Map2][k]; ok {
			cmpMap[k] = Stats{v, i}
		} else {
			cmpMap[k] = Stats{v, 0}
		}
	}
	// see if all keys in map 2 occur in map 1
	for k, v := range c.Maps[Map2] {
		if i, ok = c.Maps[Map1][k]; ok {
			// already recorded above
		} else {
			cmpMap[k] = Stats{0, v}
		}
	}
	return cmpMap
}

type Stats struct {
	M1 int
	M2 int
}

func (s Stats) Equal() bool {
	return s.M1 == s.M2
}

func (s Stats) String() string {
	return fmt.Sprintf("M1:%d M2:%d", s.M1, s.M2)
}
