package tkCompare

import (
	"fmt"
	"testing"
)

func TestComparer_Compare(t *testing.T) {
	c := NewComparer()
	c.Add("actors:Priest", Map1)
	c.Add("actors:Priest", Map2)
	c.Add("actors:Deacon", Map1)
	c.Add("actors:Deacon", Map2)
	c.Add("actors:Bishop", Map1)
	c.Add("actors:Choir", Map2)
	c.Add("actors:ChoirR", Map1)
	c.Add("actors:ChoirR", Map2)
	c.Add("actors:ChoirR", Map2)
	m := c.Compare()
	for k, v := range m {
		fmt.Printf("%s: %s\n", k, v)
	}
	expected := []Expect{Expect{"actors:Priest", "M1:1 M2:1"},
		Expect{"actors:Deacon", "M1:1 M2:1"},
		Expect{"actors:Bishop", "M1:1 M2:0"},
		Expect{"actors:Choir", "M1:0 M2:1"},
		Expect{"actors:ChoirR", "M1:1 M2:2"},
	}
	for _, expect := range expected {
		var ok bool
		var stats Stats
		if stats, ok = m[expect.Key]; !ok {
			t.Errorf("%s not in compare map", expect.Key)
			return
		}
		if expect.Value != stats.String() {
			t.Errorf("%s - expected %s, got %s", expect.Key, expect.Value, stats)
		}
	}
}

type Expect struct {
	Key   string
	Value string
}

