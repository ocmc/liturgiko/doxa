package paged

import (
	"github.com/liturgiko/doxa/pkg/css"
	"github.com/liturgiko/doxa/pkg/fonts"
	"math"
	"strings"
	"sync"
)

type ElementCtx int

const (
	ElementIsPrimary = ElementCtx(iota)
	ElementIsPageSpecific
	ElementIsInvisible
)

type Element struct {
	Parent      *Element
	Children    []*Element
	Type        string
	Class       string
	Id          string
	X           float64
	Y           float64
	Width       float64
	Height      float64
	Text        string
	Constraints Constraints
	Stylesheet  *css.StyleSheet
	Style       *Selector
	Properties  sync.Map
	PrepGetW    *func(e *Element)
	GetWidth    *func(s string) float64
	// Replacements indicate specific element is replaced in a certain context by a different element
	Replacements map[ElementCtx]*Element
	// Replaces means that a particular element replaces a different one, and absolute calculations should be
	// performed on original
	Replaces *Element
	// Context is what context the current element functions in
	Context ElementCtx
}

// internal function to prevent nil errors
func check(element *Element) *Element {

	if element == nil {
		element = new(Element)
	}
	if element.Children == nil {
		element.Children = []*Element{}
	}
	if element.Style == nil {
		element.Style = new(Selector)
	}
	if element.Style.attrs == nil {
		element.Style.attrs = Attributes{}
	}
	if element.Replacements == nil {
		element.Replacements = make(map[ElementCtx]*Element)
	}
	return element
}

func Init(e *Element) *Element {
	return check(e)
}

func (e *Element) GetAdam() *Element {
	if e == nil {
		return nil
	}
	if e.Parent == new(Element).Parent {
		return e
	}
	return e.Parent.GetAdam()
}

// Fullfill fullfill what is lacking in Element. ensure there are no nil
func (e *Element) Fullfill() {
	if e.Children == nil {
		e.Children = []*Element{}
	}
	if e.Style == nil {
		e.Style = new(Selector)
		e.Style.attrs = map[string]string{}
	}
	check(e)
}

//factors like font affect width, so it is best to outsource the gruntwork

// ComputeRealWidth computes the width of an element based on it's contents and returns the value as a float64
// prep is a function that can do some necessary tasks like set font size
// getWidth is a function that takes a string of text and returns a float64 representing
// how much space such text would take on the page

func (e *Element) ComputeWordWidths() {
	e = check(e)
	if e.PrepGetW == nil || e.GetWidth == nil {
		return
	}
	(*e.PrepGetW)(e)
	if len(e.Children) == 0 {
		e.Width = (*e.GetWidth)(e.Text)
		return
	}
	for _, kiddo := range e.Children {
		if kiddo.PrepGetW == nil {
			kiddo.PrepGetW = e.PrepGetW
		}
		if kiddo.GetWidth == nil {
			kiddo.GetWidth = e.GetWidth
		}
		kiddo.ComputeWordWidths() //TODO: avoid infinite loops
	}
}

func (e *Element) ComputeRealWidth(prep func(*Element), getWidth func(string) float64) float64 {
	if e.Context == ElementIsInvisible {
		return 0.0
	}
	e = check(e)
	prep(e)
	if len(e.Children) == 0 {
		return getWidth(e.Text)
	} else {
		if e.Width == 0 {
			e.Width = 0.0001 //prevent infinite loop in case of self-adoption
		}
		widest := 0.0
		for _, kiddo := range e.Children {
			if kiddo.Width == 0 {
				kiddo.Width = kiddo.ComputeRealWidth(prep, getWidth)
			}
			impact := (kiddo.X - e.X) + kiddo.Width
			if impact > widest {
				widest = impact
			}
		}

		factors := []string{
			"padding-left",
			"padding-right",
		}

		for _, f := range factors {
			tad, _ := e.GetLengthVal(f)
			widest += tad
		}
		return widest
	}
}

// prep and getWidth not necessary
func (e *Element) ComputeRealHeight() float64 {
	if e.Context == ElementIsInvisible {
		return 0
	}
	e = check(e)
	if len(e.Children) == 0 {
		lh, _ := e.GetFontSizeVal("line-height")
		fs, _ := e.GetFontSizeVal("font-size")
		checkHeight := math.Max(lh, fs)
		if checkHeight < 0.0001 {
			if e.Height <= checkHeight {
				e.Height = e.GetAncestralStyleHeight(12.0)
			}
		}
		e.Height = checkHeight
		return fonts.PointsToMillimeters(math.Max(lh, fs))
	} else {
		if e.Height == 0 {
			e.Height = 0.0001 // prevent infinite loop in case of self-adoption
		}

		// Find the lowest and highest points of children
		lowestPoint := math.Inf(1)   // Initialize to positive infinity
		highestPoint := math.Inf(-1) // Initialize to negative infinity

		for _, kiddo := range e.Children {
			if kiddo.Height == 0 {
				kiddo.Height = kiddo.ComputeRealHeight()
			}

			// Update lowest and highest points
			lowestPoint = math.Min(lowestPoint, kiddo.Y)
			highestPoint = math.Max(highestPoint, kiddo.Y+kiddo.Height)
		}

		// Calculate the total height needed to contain all children
		contentHeight := highestPoint - lowestPoint

		// Add padding
		factors := []string{
			"padding-top",
			"padding-bottom",
		}

		for _, f := range factors {
			tad, _ := e.GetLengthVal(f)
			contentHeight += tad
		}

		e.Height = contentHeight
		return contentHeight
	}
}

func (e *Element) GetAncestralStyleHeight(fallback float64) float64 {
	current := e.Parent
	for current != nil {
		lh, _ := current.GetFontSizeVal("line-height")
		fs, _ := current.GetFontSizeVal("font-size")
		if lh > 0 || fs > 0 {
			return math.Max(lh, fs)
		}
		current = current.Parent
	}
	return fallback
}

// Cram is called after Sprawl and before FauxJustify.
// Sprawl must be called first, or unexpected results may occur
// if an element exceeds a particular length, it is put on a new "line"
// meaning, it's Y value is increased by the height of the leaf nodes.
// the element's height will also be updated.
func (e *Element) Cram() {
	spacer := 0.5 //half a milimeter
	//but if possible calculate based on font size
	if e.GetWidth != nil {
		if e.PrepGetW != nil {
			(*e.PrepGetW)(e)
		}
		spacer = (*e.GetWidth)(" ") * 0.1
	}
	//this is a map of y+lineHeight, where the key is the starting x coord of a given leaf, and the value is end y
	//when wrapping occurs, lastline becomes thisline, and thisline is reinitialized
	lastline := make(map[float64]float64)
	thisline := make(map[float64]float64)
	//prevent self referencing from causing an infinite loop
	processed := make(map[*Element]struct{})
	var raw, stack []*Element
	raw = append(raw, e)
	for len(raw) > 0 {
		curElem := raw[0]
		raw = raw[1:]
		if _, hath := processed[curElem]; hath {
			continue
		}
		if len(curElem.Children) > 0 {
			raw = append(raw, curElem.Children...)
			continue
		}
		// we are on the leaf node level
		stack = append(stack, curElem)
		lastline[curElem.X] = curElem.Y + curElem.Height
	}
	//now we only have leaf nodes
	//lineX is how far on a given "line" we have built already
	lineX := e.X
	//sort stack by greater X value
	sorted := make([]*Element, len(stack))
	copy(sorted, stack)
	//sort.Slice(sorted, func(i, j int) bool {
	//	return sorted[i].X < sorted[j].X
	//})
	for _, this := range sorted {
		if lineX+this.Width > e.Width {
			lastline = thisline
			thisline = make(map[float64]float64)
			lineX = e.X
		}
		if this.X+this.Width > e.Width {
			nearestPrevX := e.X
			var inlineX []float64
			for x, h := range lastline {
				if x < nearestPrevX {
					continue
				}
				if x > this.X+this.Width {
					continue
				}
				if x <= this.X {
					nearestPrevX = x
					continue
				}
				inlineX = append(inlineX, h)
			}
			if _, hath := lastline[nearestPrevX]; !hath {
				lastline[nearestPrevX] = 0.0
			}
			inlineX = append(inlineX, lastline[nearestPrevX])
			yAddend := 0.01
			for _, h := range inlineX {
				yAddend = math.Max(yAddend, h)
			}
			this.Y += yAddend
			this.X = lineX
			thisline[lineX] = this.Y + this.Height
			lineX += this.Width
			lineX += spacer
		}
		e.Height = this.Y + this.Height
	}
}

// Cram adjusts the X and Y of all the children so they fit into the Width
// ComputeRealWidth() must be called first, to ensure that all the Children
// have accurate widths.
func (e *Element) LegacyCram() {
	e = check(e)
	x := e.X
	y := e.Y
	baseX := x
	targetWidth := e.Width
	//e.Sprawl()
	/*xC := []string{
		"padding-left",
		"text-indent",
	}
	for _, cName := range xC {
		lv, _ := e.GetLengthVal(cName)
		x += lv
		if cName != "text-indent" {
			baseX += lv
		}
	}

	yC := []string{
		"padding-top",
	}
	for _, cName := range yC {
		lv, _ := e.GetLengthVal(cName)
		y += lv
	}*/
	spacer := 1.0
	if e.GetWidth != nil {
		if e.PrepGetW != nil {
			(*e.PrepGetW)(e)
		}
		spacer = (*e.GetWidth)(" ") / 3
	}

	for _, kiddo := range e.Children {
		if kiddo.Width == 0 && len(kiddo.Children) > 0 {
			kiddo.Sprawl()
		}
		if (x-e.X)+kiddo.Width > targetWidth {
			x = kiddo.Split(e.X+targetWidth, baseX)
			y += kiddo.Height
		} else {
			kiddo.X = x
			kiddo.Y = y
			x += kiddo.Width
			x += spacer
		}
	}
}

// Split turns one line into two lines if line is longer than x
// Run Sprawl first as Split assumes width and x information is accurate
func (e *Element) Split(maxX, baseX float64) float64 {
	if baseX >= maxX {
		return maxX // Prevent invalid input that could cause issues
	}

	if len(e.Children) == 0 {
		// This is a leaf node
		if e.X > maxX {
			// Wrap this element
			e.X = baseX
			e.Y += e.Height // Move to next line
		}
		return e.X // Return the rightmost X position after potential wrapping
	}

	currentX := baseX
	currentY := e.Y

	for i := 0; i < len(e.Children); i++ {
		child := e.Children[i]

		if child.X > maxX {
			// Wrap this child and all subsequent children
			currentX = baseX
			currentY += child.Height // Move to next line

			// Adjust this child and all subsequent children
			for j := i; j < len(e.Children); j++ {
				e.Children[j].X = currentX
				e.Children[j].Y = currentY
				currentX += e.Children[j].Width // Assume Width exists

				// If we exceed maxX again, prepare for next line
				if currentX > maxX && j < len(e.Children)-1 {
					currentX = baseX
					currentY += e.Children[j].Height
				}
			}

			break // We've adjusted all remaining children, so exit the loop
		} else {
			currentX = child.X + child.Width // Assume Width exists
		}
	}

	// Update the parent element's dimensions
	e.Width = maxX - baseX
	e.Height = currentY - e.Y + e.Children[len(e.Children)-1].Height

	return currentX
}

func (e *Element) LegacySplit(x, base float64) float64 {
	if base >= x {
		return x //impossible demands, could lead to infinite loop
	}
	if len(e.Children) < 2 {
		e.X = base
		e.Y += e.Height
		return base
	}
	for i, k := range e.Children {
		if k.X > x {
			delta := k.X - base
			k.X = base
			k.Y += k.Height
			for _, after := range e.Children[i+1:] {
				after.X -= delta
				after.Y += after.Height
			}
			e.Split(x, base) //TODO: avoid infinite loop
			return base
		}
	}
	return x
}

// Sprawl assigns X values to children based on their widths
// it also sets the receiver's Width to the sum of its Children widths
// running ComputeRealWidth is a good idea
func (e *Element) Sprawl() {
	if len(e.Children) == 0 {
		return
	}
	e.Width = 0.001 //prevent infinite loop
	widthCounter := 0.0
	//spacer := 1.0
	for _, kiddo := range e.Children {
		kiddo.X = e.X + widthCounter //+ spacer
		kiddo.Y = e.Y
		if len(kiddo.Children) > 0 {
			kiddo.Sprawl()
		}
		widthCounter += kiddo.Width // + spacer
	}
	e.Width = widthCounter
}

// Explode converts an Element's text to Children assuming it has no children
// After this it is probably a good idea to call ComputeRealWidth() and then Cram()
func (e *Element) Explode() bool {
	e = check(e)
	if len(e.Children) > 0 {
		return false
	}
	/*if e.Type == "word" {
		return false //TODO: support letter split?
	}*/
	//TODO: chinese
	wordList := strings.Split(e.Text, " ")
	for _, w := range wordList {
		wE := check(nil)
		wE.Parent = e
		wE.Text = w
		if e.GetWidth != nil {
			wE.Width = (*e.GetWidth)(w)
		}
		wE.Type = "word"
		//h, _ := e.GetFontSizeVal("line-height")
		//wE.Height = fonts.PointsToMillimeters(h)
		wE.Height = wE.Parent.Height
		e.Children = append(e.Children, wE)
	}
	e.Sprawl()
	return true
}

// func (e *Element) StackChildren() {
// this function vertically stacks elements
// for each child:
// displace child.Y (applies to child's children as well) by current displace factor
// calculate height (e.ComputeRealHeight()),
// increase remainging children displacement by new amount, unless childtype is "word"
// }
func (e *Element) StackChildren() {
	e.stackChildrenRecursive(0)
}

func (e *Element) stackChildrenRecursive(baseY float64) float64 {
	currentY := baseY
	maxWordHeight := 0.0

	for _, child := range e.Children {
		if child.Type == "word" {
			// Stack word elements within the same line
			child.Y = currentY
			if child.Height > maxWordHeight {
				maxWordHeight = child.Height
			}
		} else {
			// Stack non-word elements vertically
			child.Y = currentY
			childHeight := child.stackChildrenRecursive(currentY)
			currentY += childHeight
		}
	}

	// Add the maximum height of word elements to the currentY
	if maxWordHeight > 0 {
		currentY += maxWordHeight
	}

	return currentY - baseY
}

func (e *Element) LegacyStackChildren() {
	if len(e.Children) < 2 {
		return
	}
	if e.Type == "word" {
		return
	}
	ytally := 0.0
	//we need to redo this
	//TODO: words should not stack, but should be moved when other things stack
	for _, kiddo := range e.Children {
		if kiddo.Type == "word" {
			kiddo.Y += e.Y
			e.Height = e.ComputeRealHeight()
			return
		}
		kiddo.Y += ytally
		kiddo.StackChildren()
		ytally += kiddo.Height
	}
	if ytally > 0 {
		e.Height += ytally
	}
}

// RecursiveExplode goes through all the descendants of Element,
// up to depth levels deep, and explodes all the text elements
func (e *Element) RecursiveExplode(depth int) {
	e = check(e)
	if depth == 0 { //avoid infinite loop
		return
	}
	if len(e.Children) > 0 {
		for _, kiddo := range e.Children {
			kiddo.RecursiveExplode(depth - 1)
		}
		return
	}
	e.Explode()
}

// Justifaux mess with element spacing so we get nice rectangles
func (e *Element) Justifaux(min, max float64) {
	//create a slice of lines
	var leaves [][]*Element
	//rows by y coord
	indices := make(map[float64]int)
	stack := []*Element{e}
	processed := make(map[*Element]struct{})
	for len(stack) > 0 {
		cur := stack[0]
		stack = stack[1:]
		if _, is := processed[cur]; is {
			continue
		}
		if len(cur.Children) > 0 {
			stack = append(stack, cur.Children...)
			continue
		}
		if _, hath := indices[cur.Y]; !hath {
			indices[cur.Y] = len(leaves)
			leaves = append(leaves, []*Element{})
		}
		leaves[indices[cur.Y]] = append(leaves[indices[cur.Y]], cur)
	}
	for _, line := range leaves {
		if len(line) < 2 {
			continue
		}
		widthTally := 0.0
		for _, leaf := range line {
			widthTally += leaf.Width
		}
		totalGap := e.Width - widthTally
		idealSpacer := totalGap / float64(len(line)-1)
		if idealSpacer < min {
			idealSpacer = min
		}
		if idealSpacer > max {
			idealSpacer = max
		}
		prevX := e.X
		for pos, leaf := range line {
			leaf.X = prevX
			prevX += leaf.Width
			if pos < len(line)-1 {
				prevX += idealSpacer
			}
		}
	}
}

// FauxJustify monkeys with the spacing in between Elements so that it gives a justified appearence
// if min, max and spaceW are the same, it behaves like a normal paragraph
// it returns false if it is called on the bedrock level, true if it can justify
// it also returns false ig the "depth" is exceeded. This is to prevent infinite loops
func (e *Element) LegacyFauxJustify(depth int, min, max float64) bool { //should it be Justifaux() instead?
	if depth == 0 {
		return false
	}
	e = check(e)
	if len(e.Children) < 1 {
		return false
	}
	xCounter := 0.0
	//yTracker := 0.0
	iStart := 0
	var lines [][2]int
	var lineWidths []float64
	for i, kiddo := range e.Children {
		/*if yTracker == 0.0 && i == 0 {
			yTracker = kiddo.Y
		}*/
		kiddo.LegacyFauxJustify(depth-1, min, max)
		xCounter = kiddo.X
		//if kiddo.Y != yTracker || i == len(e.Children)-1 { //new line
		if kiddo.X == e.X && i != 0 || i == len(e.Children)-1 { //TODO: handle padding
			lines = append(lines, [2]int{iStart, i}) //TODO: was i-1 instead of i
			if len(e.Children) == 1 {
				lineWidths = append(lineWidths, kiddo.Width)
			} else {
				lineWidths = append(lineWidths, (xCounter+e.Children[i-1].Width)-e.Children[iStart].X)
			}
			iStart = i
			//yTracker = kiddo.Y
		}
	}
	for line, w := range lineWidths {
		numWords := lines[line][1] - lines[line][0]
		if numWords <= 1 {
			continue
		}
		gapSz := e.Width - w
		if gapSz < 0 {
			gapSz = 0
		}
		spaceSize := gapSz / float64(numWords)
		if spaceSize < min {
			spaceSize = min
		}
		if spaceSize > max {
			spaceSize = max
		}
		for i := lines[line][0]; i < lines[line][1]; i++ {
			e.Children[i].X += spaceSize * float64(i-lines[line][0])
		}
	}
	return true
}

func (e *Element) PassWidthToChildren() {
	if e.Children == nil {
		return
	}
	if e.Width == 0.0 {
		return
	}
	for _, kiddo := range e.Children {
		if kiddo.Width == 0.0 {
			kiddo.Width = e.Width
		}
		kiddo.PassWidthToChildren()
	}
}

// CopyElement copies all fields of element into new object
// Note that Parent information remains the same
// and that the children are the same, not duplicated
// Replacements and Replaces is not copied either
func CopyElement(source *Element) *Element {
	e := new(Element)
	e.Type = source.Type
	e.Parent = source.Parent
	e.Children = source.Children
	e.Type = source.Type
	e.Class = source.Class
	e.Id = source.Id
	e.X = source.X
	e.Y = source.Y
	e.Width = source.Width
	e.Height = source.Height
	e.Text = source.Text
	e.Constraints = source.Constraints
	e.Stylesheet = source.Stylesheet
	e.Style = source.Style
	source.Properties.Range(func(key, value any) bool {
		e.Properties.Store(key, value)
		return true
	})
	e.PrepGetW = source.PrepGetW
	e.GetWidth = source.GetWidth
	e.Context = source.Context
	return e
}

/*** CONCERNING THE JUSTIFICATION OF TEXT ***
while justifying words is within the realm of acheivability with our current configuration,
if a word is very long, it will need to be split. and this is where we are given a false choice.
The two options seems to be either automatically split or manually split.

The nice thing about automatic split is that the user isn't troubled with such things
The problem is that some words can't be split arbitrarily, especially in foreign languages.
Consider the word "enthusiastic": it clearly is comprised of five syllables: en-thu-si-as-tic
however our codebase doesn't know this, and can't reliably know this for foreign languages.
it might split it ent-husiastic which makes reading difficult.

of course, the other solution is manual splitting, where the user tells us where we may split a word if needed.
There are two main problems with this. One is the issue of if we are given no direction on how we may split a word
The other is the fatigue on the user

A THIRD SOLUTION attempts to combine the best of both worlds by following three main rules

1. Try to avoid splitting large words when possible
2. look up the word in a large word list and split accordingly.
if it isn't present, split arbitrarily
3. if the user changes how the word is split, note that and automatically add to the large word list

This might even be a place a little machine learning could be applied.
*/
