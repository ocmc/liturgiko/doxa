package paged

import (
	"github.com/liturgiko/doxa/pkg/css"
	"github.com/liturgiko/doxa/pkg/fonts"
)

type Constraints struct {
	Top        float64
	Bottom     float64
	Left       float64
	Right      float64
	Influences []*Selector
}

func (e *Element) ComputeParentConstraints() {
	e = check(e)
	if e.Parent != nil {
		e.Parent.ComputeParentConstraints()
	}
	e.ComputeConstraints()
}

func (e *Element) ComputeChildrenConstraints() {
	e = check(e)
	if e.Children != nil {
		if len(e.Children) == 0 {
			e.ComputeConstraints()
			return
		}
		for _, c := range e.Children {
			//TODO: what if two elements have each other as children
			c.ComputeChildrenConstraints()
		}
	}
}

func (e *Element) ComputeConstraints() {
	e = check(e)
	if e.Stylesheet != nil {
		sel := convertStylesheet(e.Stylesheet)
		e.Style = sel[e.Class]
	}
	e.ComputeTopConstraints()
	e.ComputeBottomConstraints()
	e.ComputeLeftConstraints()
	e.ComputeRightConstraints()
}

func (e *Element) ConstraintFromAttrib(side, name string) (float64, error) {
	e = check(e)
	switch name {
	case "margin", "padding":
		return 0, nil //depreciated, css.go handles this
		_, exists := e.Style.attrs[name+"-"+side]
		if exists {
			return 0.0, nil
		}
		return e.GetLengthVal(name)
	case "font-size":
		// while font-size is not a constraint, it can affect line-height, which is
		_, exists := e.Style.attrs["line-height"]
		if !exists {
			if e.Parent != nil {
				if e.Parent.Style != nil {
					lh, lhexists := e.Parent.Style.attrs["line-height"]
					if lhexists {
						//if strings.HasSuffix(lh, "%") {
						e.Style.attrs["line-height"] = lh
						res, err := e.ConstraintFromAttrib(side, "line-height")
						delete(e.Style.attrs, "line-height")
						return res, err
						//}
					}
				}
			}
		}
		return 0, nil
	case "line-height":
		//TODO: calculate this based on text-box-edge property
		//TODO: investigate multiple lines
		if side == "left" || side == "right" {
			return 0.0, nil
		}
		lh, err := e.GetFontSizeVal(name)
		if err != nil {
			return 0, err
		}
		fs, err := e.GetFontSizeVal("font-size")
		if err != nil {
			return 0.0, err
		}
		return fonts.PointsToMillimeters(lh-fs) / 2, nil
	case "margin-" + side, "padding-" + side:
		return e.GetLengthVal(name)
	default:
		return 0, nil
	}
}

func convertStylesheet(sheet *css.StyleSheet) (retval map[string]*Selector) {
	for s, r := range sheet.RuleMap {
		var at Attributes
		for _, d := range r.Declarations {
			at[d.Identifier] = d.Value
		}
		retval[s] = &Selector{
			attrs: at,
		}
	}
	return retval
}

func (e *Element) ComputeTopConstraints() {
	e = check(e)
	e.Constraints.Top = 0
	if e.Parent != nil {
		e.Constraints.Top = e.Parent.Constraints.Top
	}
	e.Fullfill()
	for label, _ := range e.Style.attrs {
		fac, _ := e.ConstraintFromAttrib("top", label)
		e.Constraints.Top += fac
	}
}

func (e *Element) ComputeBottomConstraints() {
	e = check(e)
	e.Constraints.Bottom = 0
	if e.Parent != nil {
		e.Constraints.Bottom = e.Parent.Constraints.Bottom
	}
	for label, _ := range e.Style.attrs {
		fac, _ := e.ConstraintFromAttrib("bottom", label)
		e.Constraints.Bottom += fac
	}
}

func (e *Element) ComputeLeftConstraints() {
	e = check(e)
	e.Constraints.Left = 0
	if e.Parent != nil {
		e.Constraints.Left = e.Parent.Constraints.Left
	}
	for label, _ := range e.Style.attrs {
		fac, _ := e.ConstraintFromAttrib("left", label)
		e.Constraints.Left += fac
	}
}

func (e *Element) ComputeRightConstraints() {
	e = check(e)
	e.Constraints.Right = 0
	if e.Parent != nil {
		e.Constraints.Right = e.Parent.Constraints.Right
	}

	for label, _ := range e.Style.attrs {
		fac, _ := e.ConstraintFromAttrib("right", label)
		e.Constraints.Right += fac
	}
}
