package paged

import (
	"github.com/liturgiko/doxa/pkg/css"
	gofpdf "github.com/liturgiko/doxa/pkg/fpdf"
)

type PrintPress struct {
	PrepPara  func(e *Element)
	PrintPara func(e *Element)
	PrepSpan  func(e *Element)
	PrintSpan func(e *Element)
}

// this one is meant to hook up to our current PDF generator
type Writer interface {
	WritePDF()
	GetPdf() *gofpdf.Fpdf
	GetColumnWidth() float64
}

func LegacyPdfPrintPress(w Writer) *PrintPress {
	var pp *PrintPress
	p := w.GetPdf()
	pp = &PrintPress{
		PrepPara: func(e *Element) {
			e = check(e)
			e.X = p.GetX()
			e.Y = p.GetY()
			e.RecursiveExplode(32)
			e.ComputeRealHeight()
			e.ComputeRealWidth(func(el *Element) {
				if ff, exists := el.Style.attrs["font-family"]; exists {
					p.SetFont(ff, "", 12)
				}
				fs, err := el.GetFontSizeVal("font-size")
				if err == nil {
					p.SetFontSize(fs)
				}
			}, func(s string) float64 {
				return p.GetStringWidth(s)
			})
			e.Sprawl()
			prep := func(el *Element) {
				if ff, exists := el.Style.attrs["font-family"]; exists {
					p.SetFont(ff, "", 12)
				}
				fs, err := el.GetFontSizeVal("font-size")
				if err == nil {
					p.SetFontSize(fs)
				}
			}
			e.PrepGetW = &prep
			getW := func(s string) float64 {
				return p.GetStringWidth(s)
			}
			e.GetWidth = &getW
			/*e.ComputeRealWidth(func(el *Element) {
				if ff, exists := el.Style.attrs["font-family"]; exists {
					p.SetFont(ff, "", 12)
				}
				fs, err := el.GetFontSizeVal("font-size")
				if err == nil {
					p.SetFontSize(fs)
				}
			}, func(s string) float64 {
				return p.GetStringWidth(s)
			})*/
			e.Width = w.GetColumnWidth()
			e.Cram()
		},
		PrintPara: func(e *Element) {
			e = check(e)
			p.SetX(e.X)
			p.SetY(e.Y)
			if ff, exists := e.Style.attrs["font-family"]; exists {
				p.SetFont(ff, "", 12)
			}
			if fs, err := e.GetFontSizeVal("font-size"); err == nil {
				p.SetFontSize(fs)
			}
			for _, k := range e.Children {
				pp.PrepSpan(k)
				pp.PrintSpan(k)
			}
		},
		PrepSpan: func(e *Element) {
			e = check(e)
			//p.SetX(e.X)
			//p.SetY(e.Y)
			if ff, exists := e.Style.attrs["font-family"]; exists {
				p.SetFont(ff, "", 12)
			}
			if fs, err := e.GetFontSizeVal("font-size"); err == nil {
				p.SetFontSize(fs)
			}
		},
		PrintSpan: func(e *Element) {
			e = check(e)
			for i, k := range e.Children {
				if k.Type == "word" {
					//p.SetX(k.X)
					//p.SetY(k.Y)
					x := p.GetX()
					if x+k.Width > w.GetColumnWidth() {
						p.SetX(e.Parent.X)
						y := p.GetY()
						p.SetY(y + k.Height)
					}
					p.Write(k.Height, k.Text)
					if i < len(e.Children) {
						p.Write(k.Height, " ")
					}
				} else {
					pp.PrintSpan(k)
				}
			}
		},
	}

	return pp
}

type Command byte

const (
	PlotText = Command(iota)
	PlotImage
	ChangeStyle
	NewPage
)

type Position struct {
	X float64
	Y float64
	W float64
	H float64
}

// this is the new code. the old code should probably be removed.
type PrintStack struct {
	Cmd           []Command
	PositionStack []*Position
	StyleStack    []*css.Rule
	PayloadStack  [][]byte
}

type PrintScaffolding struct {
	CurrentClass string
	//reverse map for tracking if a given style has been added
	Stylesheet *css.StyleSheet
}

func NewPrintStack() *PrintStack {
	s := new(PrintStack)
	s.PositionStack = make([]*Position, 0)
	s.PayloadStack = make([][]byte, 0)
	s.StyleStack = make([]*css.Rule, 0)
	s.Cmd = make([]Command, 0)
	return s
}

// should only be run once
func (e *Element) MkScaffolding() *PrintScaffolding {
	platform := new(PrintScaffolding)
	platform.Stylesheet = e.Stylesheet
	return platform
}

func (ps *PrintStack) PushElement(framework *PrintScaffolding, e *Element) {
	//check if element style diverges from current style.
	selec := e.Type
	if e.Class != "" {
		selec += "." + e.Class
	}
	if framework.CurrentClass == selec {
		return
	}
	if e.Type == "word" {
		ps.PayloadStack = append(ps.PayloadStack, []byte(e.Text))
		ps.PositionStack = append(ps.PositionStack, &Position{X: e.X, Y: e.Y, W: e.Width, H: e.Height})
		ps.Cmd = append(ps.Cmd, PlotText)
		return
	}
	if rule, hath := e.Stylesheet.GetRule(selec); hath {
		ps.StyleStack = append(ps.StyleStack, rule)
		ps.Cmd = append(ps.Cmd, ChangeStyle)
	}
	if len(e.Children) > 0 {
		for _, kiddo := range e.Children {
			ps.PushElement(framework, kiddo)
		}
	}
}

func (ps *PrintStack) PushElementHierarchy(h *ElementHierarchy) {
	var PrevStyles []*css.Rule
	var childCounts []int
	//check if element style diverges from current style.
	h.EnterNode(h.Root)
	framework := h.Root.MkScaffolding()
	for page, _ := range h.PageOffsetsY {
		pageMap := make(map[*Element]struct{})
		stackEH := []*Element{h.Root}
		for len(stackEH) > 0 {
			e := stackEH[len(stackEH)-1]
			stackEH = stackEH[:len(stackEH)-1]
			if len(childCounts) > 0 {
				if childCounts[len(childCounts)-1] == 0 {
					if len(PrevStyles) > 0 {
						rule := PrevStyles[len(PrevStyles)-1]
						PrevStyles = PrevStyles[:len(PrevStyles)-1]
						if rule != nil {
							ps.StyleStack = append(ps.StyleStack, rule)
							ps.Cmd = append(ps.Cmd, ChangeStyle)
						}
					}
					childCounts = childCounts[:len(childCounts)-1]
				} else {
					childCounts[len(childCounts)-1]--
				}
			}
			h.EnterNode(e)
			pagekids := h.ChildrenInPage(page, pageMap)
			/*if page > 0 {
			}
			if _, hath := pageMap[e]; hath {
				continue
			}
			pageMap[e] = struct{}{}
			pagekids := e.Children*/
			childCounts = append(childCounts, len(pagekids))
			stackEH = append(stackEH, pagekids...)
			h.ExitNode()
			selec := e.Type
			if e.Class != "" {
				selec += "." + e.Class
			}
			if framework.CurrentClass != selec {
				framework.CurrentClass = selec
				if rule, hath := h.Root.Stylesheet.GetRule(selec); hath {
					if len(ps.StyleStack) > 0 {
						PrevStyles = append(PrevStyles, ps.StyleStack[len(ps.StyleStack)-1])
					}
					ps.StyleStack = append(ps.StyleStack, rule)
					ps.Cmd = append(ps.Cmd, ChangeStyle)
				}
			}
			if len(e.Children) == 0 && len(e.Text) > 0 {
				ps.PayloadStack = append(ps.PayloadStack, []byte(e.Text))
				ps.PositionStack = append(ps.PositionStack, &Position{X: e.X, Y: e.Y - h.PageOffsetsY[page], W: e.Width, H: e.Height})
				ps.Cmd = append(ps.Cmd, PlotText)
			}
		}
		ps.Cmd = append(ps.Cmd, NewPage) //NewPage doesn't need anything from stack
	}
}

func (ps *PrintStack) LegacyPushElementHierarchy(h *ElementHierarchy) {
	var PrevStyles []*css.Rule
	var childCounts []int
	//check if element style diverges from current style.
	h.EnterNode(h.Root)
	framework := h.Root.MkScaffolding()
	for page, _ := range h.PageOffsetsY {
		pageMap := make(map[*Element]struct{})
		stackEH := []*Element{h.Root}
		for len(stackEH) > 0 {
			e := stackEH[len(stackEH)-1]
			stackEH = stackEH[:len(stackEH)-1]
			if len(childCounts) > 0 {
				if childCounts[len(childCounts)-1] == 0 {
					if len(PrevStyles) > 0 {
						rule := PrevStyles[len(PrevStyles)-1]
						PrevStyles = PrevStyles[:len(PrevStyles)-1]
						if rule != nil {
							ps.StyleStack = append(ps.StyleStack, rule)
							ps.Cmd = append(ps.Cmd, ChangeStyle)
						}
					}
				} else {
					childCounts[len(childCounts)-1]--
				}
			}
			h.EnterNode(e)
			pagekids := h.ChildrenInPage(page, pageMap)
			childCounts = append(childCounts, len(pagekids))
			stackEH = append(stackEH, pagekids...)
			h.ExitNode()
			selec := e.Type
			if e.Class != "" {
				selec += "." + e.Class
			}
			if framework.CurrentClass != selec {
				framework.CurrentClass = selec
				if rule, hath := h.Root.Stylesheet.GetRule(selec); hath {
					if len(ps.StyleStack) > 0 {
						PrevStyles = append(PrevStyles, ps.StyleStack[len(ps.StyleStack)-1])
					}
					ps.StyleStack = append(ps.StyleStack, rule)
					ps.Cmd = append(ps.Cmd, ChangeStyle)
				}
			}
			if len(e.Children) == 0 && len(e.Text) > 0 {
				ps.PayloadStack = append(ps.PayloadStack, []byte(e.Text))
				ps.PositionStack = append(ps.PositionStack, &Position{X: e.X, Y: e.Y, W: e.Width, H: e.Height})
				ps.Cmd = append(ps.Cmd, PlotText)
			}
		}
		ps.Cmd = append(ps.Cmd, NewPage) //NewPage doesn't need anything from stack
	}
}
