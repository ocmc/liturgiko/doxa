package paged

import (
	"errors"
	"fmt"
	"github.com/liturgiko/doxa/pkg/css"
	"github.com/liturgiko/doxa/pkg/fonts"
	"strconv"
	"strings"
)

type Attributes map[string]string

const RED = 0xFF0000
const GREEN = 0x00FF00
const BLUE = 0x0000FF

type Selector struct {
	attrs Attributes
}

func (e *Element) GetLengthVal(attrname string) (float64, error) {
	e = check(e)
	selector := e.Style
	if e.Style == nil {
		if e.Parent == nil {
			return 0, nil
		}
		selector = e.Parent.Style
	}
	if selector == nil {
		selector = e.Parent.GetAdam().Style
	}
	if selector == nil {
		return 0.0, errors.New("selector is nil")
	}
	myattr, exists := selector.attrs[attrname]
	if !exists {
		if strings.Contains(attrname, "margin-") {
			_, exists = selector.attrs["margin"]
			if exists {
				return e.GetLengthVal("margin")
			}
		}

		if strings.Contains(attrname, "padding-") {
			_, exists = selector.attrs["padding"]
			if exists {
				return e.GetLengthVal("padding")
			}
		}

		if e.Parent == new(Element).Parent {
			return 0.0, errors.New(fmt.Sprintf("%s does not exist.", attrname))
		}
		val, err := e.Parent.GetLengthVal(attrname)
		if err != nil {
			return val, err
		}
		return val, nil
	}
	if strings.HasSuffix(myattr, "mm") {
		size, err := strconv.ParseFloat(myattr[:len(myattr)-2], 64)
		if err != nil {
			return 0.0, err
		}
		return size, nil
	} else if strings.HasSuffix(myattr, "cm") {
		size, err := strconv.ParseFloat(myattr[:len(myattr)-2], 64)
		if err != nil {
			return 0.0, err
		}
		return size * 10, nil
	} else if strings.HasSuffix(myattr, "pt") {
		size, err := strconv.ParseFloat(myattr[0:len(myattr)-2], 64)
		if err != nil {
			return 0.0, err
		}
		return fonts.PointsToMillimeters(size), nil
	} else if strings.HasSuffix(myattr, "rem") {
		fs, fserr := e.GetAdam().GetFontSizeVal("font-size")
		if fserr != nil {
			return 0.0, nil
		}
		size, err := strconv.ParseFloat(myattr[0:len(myattr)-3], 64)
		if err != nil {
			return 0.0, err
		}
		return fonts.PointsToMillimeters(fs * size), err
	} else if strings.HasSuffix(myattr, "%") {
		size, err := strconv.ParseFloat(myattr[0:len(myattr)-1], 64)
		if err != nil {
			return 0.0, err
		}
		sz2, err2 := e.Parent.GetLengthVal(attrname)
		if err2 != nil {
			return 0, err2
		}
		return sz2 * (size / 100), nil
	} else if strings.HasSuffix(myattr, "in") {
		size, err := strconv.ParseFloat(myattr[0:len(myattr)-2], 64)
		if err != nil {
			return 0.0, err
		}
		return fonts.PointsToMillimeters(size * 72), nil
	} else {
		return 0.0, errors.New("invalid or unsupported unit in css")
	}
}
func (e *Element) GetFontSizeVal(attrname string) (float64, error) {
	e = check(e)
	selector := e.Style
	if e.Style == nil {
		if e.Parent == nil {
			return 0, nil
		}
		selector = e.Parent.Style
	}
	if selector == nil {
		selector = e.Parent.GetAdam().Style
	}
	if selector == nil {
		return 0.0, errors.New("selector is nil")
	}
	myattr, exists := selector.attrs[attrname]
	if !exists {
		if e.Parent == new(Element).Parent {
			return 0.0, errors.New(fmt.Sprintf("%s does not exist.", attrname))
		}
		val, err := e.Parent.GetFontSizeVal(attrname)
		if err != nil {
			return val, err
		}
		return val, nil
	}
	if strings.HasSuffix(myattr, "%") {
		size, err := strconv.ParseFloat(myattr[0:len(myattr)-1], 64)
		if err != nil {
			return 0.0, err
		}
		if attrname == "line-height" {
			lhb, err2 := e.GetFontSizeVal("font-size")
			if err2 != nil {
				return 0.0, err2
			}
			return lhb * (size / 100), nil
		}
	} else if strings.HasSuffix(myattr, "rem") {
		size, err := strconv.ParseFloat(myattr[0:len(myattr)-3], 64)
		if err != nil {
			return 0.0, err
		}
		fsb, err2 := e.GetAdam().GetFontSizeVal("font-size")
		if err2 != nil {
			return 0.0, err2
		}
		return fsb * size, nil
	} else if strings.HasSuffix(myattr, "pt") {
		size, err := strconv.ParseFloat(myattr[0:len(myattr)-2], 64)
		if err != nil {
			return 0.0, err
		}
		return size, nil
	} else {
		switch strings.ToLower(myattr) {
		case "x-small":
			return 7.5, nil
		case "small":
			return 10.0, nil
		case "medium":
			return 12, nil
		case "large":
			return 13.5, nil
		case "x-large":
			return 18, nil
		case "xx-large":
			return 24, nil
		default:
			return 0.0, errors.New("Unknown font-size format")
		}
	}
	return 0.0, errors.New("Unknown font-size format")
}

func (e *Element) ImportCss(sheet *css.StyleSheet) {
	e = check(e)
	for k, v := range sheet.RuleMap {
		switch k {
		case e.Type:
			for _, d := range v.Declarations {
				e.Style.attrs[d.Identifier] = d.Value
			}
		case e.Class:
			for _, d := range v.Declarations {
				e.Style.attrs[d.Identifier] = d.Value
			}
		case e.Id:
			for _, d := range v.Declarations {
				e.Style.attrs[d.Identifier] = d.Value
			}
		case e.Type + "." + e.Class:
			for _, d := range v.Declarations {
				e.Style.attrs[d.Identifier] = d.Value
			}
		}
	}
}

func (e *Element) GetRule(rule string) (string, bool) {
	e = check(e)
	val, exists := e.Style.attrs[rule]
	return val, exists
}

func (e *Element) SetRules() []string {
	var s []string
	for k, _ := range e.Style.attrs {
		s = append(s, k)
	}
	return s
}

func (e *Element) SetAttr(name, val string) {
	e = check(e)
	e.Style.attrs[name] = val
}
