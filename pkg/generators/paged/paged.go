// Package paged a generator for any format that uses pages as opposed to continuous text (e.g. PDF, EPUB)
package paged
