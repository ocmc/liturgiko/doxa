package paged

import (
	"github.com/liturgiko/doxa/pkg/atempl"
	"github.com/liturgiko/doxa/pkg/css"
	"math"
)

type ElementHierarchy struct {
	Root          *Element
	currentParent *Element //used by the stack
	Registry      []*Element
	Stylesheet    map[string]*Selector
	PrepGetW      *func(e *Element)
	GetWidth      *func(s string) float64
	PageOffsetsY  []float64
	PageHeights   []float64
	LineHeights   map[string]float64
}

func (h *ElementHierarchy) Initialize(adam *Element, sheet *css.StyleSheet) {
	h.Registry = append(h.Registry, adam)
	h.Stylesheet = map[string]*Selector{}
	adam.ImportCss(sheet)

	//TODO: repeat code
	for k, v := range sheet.RuleMap {
		h.Stylesheet[k] = new(Selector)
		h.Stylesheet[k].attrs = map[string]string{}
		for _, d := range v.Declarations {
			h.Stylesheet[k].attrs[d.Identifier] = d.Value
		}
	}
	h.Root = adam
	if adam.Children == nil {
		adam.Children = []*Element{}
	}
	h.currentParent = adam
	h.LineHeights = make(map[string]float64)
}

func (h *ElementHierarchy) SetupPaging(pageHeight float64) {
	h.PageOffsetsY = []float64{0.0}
	h.PageHeights = []float64{pageHeight}
}

func (h *ElementHierarchy) AddChild(e *Element) {
	h.Registry = append(h.Registry, e)
	e.Parent = h.currentParent
	h.currentParent.Children = append(h.currentParent.Children, e)
	if e.Children == nil {
		e.Children = []*Element{}
	}
	if _, exists := h.Stylesheet[e.Type+"."+e.Class]; exists {
		e.Style = h.Stylesheet[e.Type+"."+e.Class]
	} else if e.Type == "body" {
		if _, bexists := h.Stylesheet["body"]; bexists {
			e.Style = h.Stylesheet["body"]
		}
	}
	if e.GetWidth == nil {
		e.GetWidth = h.GetWidth
	}
	if e.PrepGetW == nil {
		e.PrepGetW = h.PrepGetW
	}
}

// TODO: decide if Elements not in the registry are acceptable

// EnterNode indicates that further calls to AddChild should choose Element e as their parent
func (h *ElementHierarchy) EnterNode(e *Element) {
	h.currentParent = e
}

// ExitNode sets the parent to the parent's parent. if that doesn't exist, it jumps to root
func (h *ElementHierarchy) ExitNode() {
	if h.currentParent.Parent == nil {
		h.currentParent = h.Root
	} else {
		h.currentParent = h.currentParent.Parent
	}
}

func (h *ElementHierarchy) AddAtemRow(row *atempl.MetaRow) {
	e := new(Element)
	e.Type = "row"
	h.AddChild(e)
	h.EnterNode(e)
	for _, child := range row.Cells {
		h.AddAtemCell(child)
	}
	h.ExitNode()
	e.Fullfill()
	e.ComputeConstraints()
	//e.ComputeRealHeight()
}

func (h *ElementHierarchy) AddAtemCell(cell *atempl.Cell) {
	if cell.ParaCell != nil {
		e := new(Element)
		e.Type = "p"
		e.Class = cell.ParaCell.PClass
		e.Properties.Store("language", cell.ParaCell.Language)
		styleInfo, present := h.Stylesheet[e.Class]
		if !present {
			styleInfo, present = h.Stylesheet[e.Type]
			if !present {
				styleInfo = h.Stylesheet[e.Type+"."+e.Class]
			}
		}
		if styleInfo != nil {
			for attr, val := range styleInfo.attrs {
				e.SetAttr(attr, val)
			}
		}
		h.AddChild(e)
		h.EnterNode(e)
		for _, child := range cell.ParaCell.Spans {
			h.AddAtemSpan(child)
		}
		h.ExitNode()
		e.Fullfill()
		e.ComputeConstraints()

	}
}

func (h *ElementHierarchy) AddAtemSpan(span *atempl.Span) {
	e := new(Element)
	e.Type = "span"
	if span.Anchor != nil {
		e.Type = "a"
	}
	e.Class = span.Class
	e.Id = span.ID
	h.AddChild(e)
	h.EnterNode(e)
	for _, child := range span.ChildSpans {
		h.AddAtemSpan(child)
	}
	if len(span.ChildSpans) == 0 {
		if span.Value != nil {
			e.Text = *span.Value
			h.ComputeSingleLineHeight()
		}
	}
	h.ExitNode()
	e.Fullfill()
	e.ComputeConstraints()
}

func (h *ElementHierarchy) StackWithAtem(pageHeight float64, callback func()) {
	switch h.currentParent.Type {
	case "body":
		yCounter := h.currentParent.Y
		for _, row := range h.currentParent.Children {
			if row.Type != "row" {
				continue
			}
			//row.ComputeRealHeight()
			//if yCounter+row.Height > pageHeight {
			//	yCounter = 0.0
			//callback()
			//}
			row.StackRowWithAtem(h.currentParent.X, yCounter)
			yCounter += row.Height
		}
	case "row":
	case "p": //p means cell
	}
}
func (e *Element) StackRowWithAtem(x, y float64) {
	e.Y = y
	e.X = x
	if len(e.Children) == 0 {
		return
	}
	newWidth := e.Width / float64(len(e.Children))
	//for i, cell := range e.Children {
	maxHeight := 0.0
	for _, cell := range e.Children {
		if cell.Type != "p" {
			continue
		}
		cell.Width = newWidth
		//cell.StackCellWithAtem(x+(float64(i)*newWidth), y)
		cell.Sprawl()
		cell.ComputeRealWidth(*cell.PrepGetW, *cell.GetWidth)
		cell.Cram()
		spaceW := (*e.GetWidth)(" ")
		cell.Justifaux(spaceW/3, spaceW*1.3)
		maxHeight = math.Max(maxHeight, cell.Height)
	}
	//since cells are parallel, we can improve performance by calculating on cell height
	e.Height = maxHeight
}
func (e *Element) StackCellWithAtem(x, y float64) {
	//everything in here should get spread out
	//e.X = x
	e.Y = y
	e.Sprawl()
	//this one is kinda archaic, it was there before we added these to the structure
	e.ComputeRealWidth(*e.PrepGetW, *e.GetWidth)
	//e.ComputeRealHeight()
	e.Width = e.Parent.Width //TODO: multicolumn
	e.Cram()
	spaceW := (*e.GetWidth)(" ")
	//e.FauxJustify(256, spaceW/3, spaceW+spaceW)
	e.Justifaux(spaceW/3, spaceW+spaceW)
	/* e.ComputeRealHeight()
	e.Explode()
	e.Sprawl()
	e.Cram()
	e.FauxJustify(32, 1, 1) //TODO: getwidth */
}

func (h *ElementHierarchy) ComputeSingleLineHeight() {
	if height, hath := h.LineHeights[h.currentParent.Type+"."+h.currentParent.Class]; hath {
		h.currentParent.Height = height
		return
	}
	lh, _ := h.currentParent.GetFontSizeVal("line-height")
	fs, _ := h.currentParent.GetFontSizeVal("font-size")
	if lh == 0 {
		lh = fs
	}
	h.LineHeights[h.currentParent.Type+"."+h.currentParent.Class] = lh
	h.currentParent.Height = lh
}

func (h *ElementHierarchy) AddPage(height float64) {
	// Get latest offset
	if len(h.PageHeights) == 0 || len(h.PageOffsetsY) == 0 {
		h.PageHeights = append(h.PageHeights, height)
		h.PageOffsetsY = append(h.PageOffsetsY, 0)
	} else {
		prevPageHeight := h.PageHeights[len(h.PageHeights)-1]
		prevPageStart := h.PageOffsetsY[len(h.PageOffsetsY)-1]
		h.PageHeights = append(h.PageHeights, height)
		h.PageOffsetsY = append(h.PageOffsetsY, prevPageHeight+prevPageStart)
	}

	// TODO: Recompute constraints of affected elements
}

func (h *ElementHierarchy) LegacyAddPage(height float64) {
	//get latest offset
	prevPageHeight := h.PageHeights[len(h.PageHeights)-1]
	prevPageStart := h.PageOffsetsY[len(h.PageOffsetsY)-1]
	h.PageHeights = append(h.PageHeights, height)
	h.PageOffsetsY = append(h.PageOffsetsY, prevPageHeight+prevPageStart)
	pb := new(Element)
	pb.Fullfill()
	pb.Context = ElementIsInvisible
	pb.Y = h.MaxYOfLastPage()
	pb.Type = "page-break"
	h.AddChild(pb)
	//TODO: recompute contstraints of effected elements
}

func (h *ElementHierarchy) MaxYOfLastPage() float64 {
	if len(h.PageOffsetsY) == 0 || len(h.PageHeights) == 0 {
		return 0
	}
	val := h.PageOffsetsY[len(h.PageOffsetsY)-1] + h.PageHeights[len(h.PageHeights)-1]
	return val
}

func (h *ElementHierarchy) PageSplit(pageHeight float64) {
	h.PageSplitRecursive(pageHeight)
}
func (h *ElementHierarchy) PageSplitRecursive(pageHeight float64) {
	//TODO: handle breakBefore and breakAfter
	//if page height is zero infinite loop can occur
	if pageHeight == 0 {
		return
	}

	//if none of our element is even on the page, create blank pages till it is
	if h.currentParent.Y >= h.MaxYOfLastPage() {
		h.AddPage(pageHeight)
		h.PageSplit(pageHeight)
		return
	}
	//if the element fits within the last page, we need not split
	if h.currentParent.Y+h.currentParent.Height < h.MaxYOfLastPage() {
		return
	}
	//the element ahs some stuff on this page, and it has stuff that overflows this page
	//therefore we need to split
	//first we create a split version of the element
	if h.currentParent.Context != ElementIsPageSpecific {
		splitVer := CopyElement(h.currentParent)
		original := h.currentParent
		splitVer.Context = ElementIsPageSpecific
		h.currentParent.Replacements[ElementIsPageSpecific] = splitVer
		splitVer.Replaces = h.currentParent
		h.ExitNode()
		defer h.EnterNode(original)
		h.AddChild(splitVer)
		h.EnterNode(splitVer)
		// if it doesn't have any children, we have to explode it and perform the operation on it
		// if it is of word type, we cannot explode it, and must adjust offset according to style
		// by default increase X until on next page
		if len(splitVer.Children) == 0 {
			//TODO: look at style information to split in more nuanced fashion
			splitVer.Explode()
			//if it is at the finest level it will not explode
			//adjust the y to be on next page
			if len(splitVer.Children) == 0 {
				splitVer.Y = h.MaxYOfLastPage()
				h.PageSplit(pageHeight)
				return
			}
			h.PageSplit(pageHeight)
			return
		}
	}
	//do none of the element's children overflow?
	//if so, add a page break element to it
	for _, kiddo := range h.currentParent.Children {
		if kiddo.Y < h.MaxYOfLastPage() && kiddo.Y+kiddo.Height > h.MaxYOfLastPage() {
			h.EnterNode(kiddo)
			h.PageSplit(pageHeight)
			h.ExitNode()
			return
		}
	}
	h.AddPage(pageHeight)
	return
}

func (h *ElementHierarchy) PageSplitIterative(pageHeight float64) {
	if pageHeight == 0 {
		return
	}

	stack := []*Element{h.currentParent}

	for len(stack) > 0 {
		current := stack[len(stack)-1]
		stack = stack[:len(stack)-1]

		if current.Y > h.MaxYOfLastPage() {
			h.AddPage(pageHeight)
			stack = append(stack, current)
			continue
		}

		if current.Y+current.Height < h.MaxYOfLastPage() {
			continue
		}

		splitVer := CopyElement(current)
		splitVer.Context = ElementIsPageSpecific
		current.Replacements[ElementIsPageSpecific] = splitVer
		splitVer.Replaces = current

		h.AddChild(splitVer)

		if len(splitVer.Children) == 0 {
			splitVer.Explode()
			if len(splitVer.Children) == 0 {
				splitVer.Y = h.MaxYOfLastPage()
				continue
			}
		}

		for i := len(splitVer.Children) - 1; i >= 0; i-- {
			kiddo := splitVer.Children[i]
			if kiddo.Y < h.MaxYOfLastPage() && kiddo.Y+kiddo.Height > h.MaxYOfLastPage() {
				stack = append(stack, kiddo)
			}
		}
	}
}

// ChildrenInPage return the currentParent's children that are in current page
// note that the function may return children that voerflow that page.
// make sure to use this any place you would use element.Children to prevent this
func (h *ElementHierarchy) ChildrenInPage(page int, processed map[*Element]struct{}) (inPage []*Element) {
	//make sure page even exists
	if len(h.PageOffsetsY) < page || len(h.PageHeights) < page {
		return nil
	}
	if replacement, hasReplacement := h.currentParent.Replacements[ElementIsPageSpecific]; hasReplacement {
		h.EnterNode(replacement)
	}
	if _, is := processed[h.currentParent]; is {
		return
	}
	processed[h.currentParent] = struct{}{}
	for _, elem := range h.currentParent.Children {
		if replacement, hasReplacement := elem.Replacements[ElementIsPageSpecific]; hasReplacement {
			elem = replacement
		}
		if elem.Y+elem.Height < h.PageOffsetsY[page] {
			continue
		}
		if elem.Y-h.PageOffsetsY[page] > h.PageHeights[page] {
			continue
		}
		inPage = append(inPage, elem)
	}
	return inPage
}

// ToPageY converts an absolute Y value to a page specific Y value
// note that if the y value exceeds page height, it will still be returned
// you can manually check that it does not exceed or use ChildrenInPage in conjunction
// if pageNum is not a valid page number, -1 is returned
func (h *ElementHierarchy) ToPageY(y float64, pageNum int) float64 {
	if pageNum >= len(h.PageOffsetsY) || pageNum < 0 {
		return -1
	}
	return y - h.PageOffsetsY[pageNum]
}

//TODO: layout relationships?
