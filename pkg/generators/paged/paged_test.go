package paged

import (
	"github.com/liturgiko/doxa/pkg/fonts"
	"testing"
)

func TestElement_GetAdam(t *testing.T) {
	e1 := new(Element)
	e2 := new(Element)
	e3 := new(Element)
	var eNil *Element
	e1.Children = append(e1.Children, e2)
	e2.Parent = e1
	e3.Parent = e2
	if e1.GetAdam() != e1 {
		t.Errorf("Expected e1 to be it's own adam")
	}
	if e2.GetAdam() != e1 {
		t.Errorf("Expected e1 to be e2's parent")
	}
	if e3.GetAdam() != e1 {
		t.Errorf("Expected e1 to be e3's grandparent")
	}
	if e1.GetAdam() == e1.Parent {
		t.Errorf("e1 should not be it's own parent")
	}
	if eNil.GetAdam() != nil {
		t.Errorf("expected eNil to be it's own adam")
	}
}

func TestElement_GetLengthVal(t *testing.T) {
	e1 := new(Element)
	e2 := new(Element)
	e3 := new(Element)
	var eNil *Element
	e1.Class = "p.actor1"
	e1Style := new(Selector)
	e1Style.attrs = map[string]string{
		"width": "30mm",
	}
	e1.Style = e1Style
	e2.Class = "span.red"
	e2.Parent = e1

	lv1, _ := e1.GetLengthVal("width")
	lv2, _ := e2.GetLengthVal("width")
	lv3, _ := e3.GetLengthVal("width")
	iv1, _ := e1.GetLengthVal("height")
	iv2, _ := e2.GetLengthVal("height")
	iv3, _ := e3.GetLengthVal("height")
	ev1 := 30.0
	ev2 := 30.0
	ev3 := 0.0

	if lv1 != ev1 {
		t.Errorf("expected %f but got %f", lv1, ev1)
	}

	if lv2 != ev2 {
		t.Errorf("expected %f but got %f", lv2, ev2)
	}

	if lv1 != ev1 {
		t.Errorf("expected %f but got %f", lv3, ev3)
	}

	if iv3 != ev3 || iv2 != ev3 || iv1 != ev3 {
		t.Errorf("expected %f %f %f but got %f %f %f", ev3, ev3, ev3, iv1, iv2, iv3)
	}
	_, err := eNil.GetLengthVal("width")
	if err == nil {
		t.Errorf("expected error, but got none")
	}
}

func TestElement_GetFontSizeVal(t *testing.T) {
	e1 := new(Element)
	e2 := new(Element)
	e3 := new(Element)
	var eNil *Element
	e1.Class = "p.actor1"
	e1Style := new(Selector)
	e1Style.attrs = map[string]string{
		"font-size": "32pt",
	}
	e1.Style = e1Style
	e2.Class = "span.red"
	e2.Parent = e1

	lv1, _ := e1.GetFontSizeVal("font-size")
	lv2, _ := e2.GetFontSizeVal("font-size")
	lv3, _ := e3.GetFontSizeVal("font-size")
	iv1, _ := e1.GetFontSizeVal("height")
	iv2, _ := e2.GetFontSizeVal("height")
	iv3, _ := e3.GetFontSizeVal("height")
	ev1 := 32.0
	ev2 := 32.0
	ev3 := 0.0

	if lv1 != ev1 {
		t.Errorf("expected %f but got %f", lv1, ev1)
	}

	if lv2 != ev2 {
		t.Errorf("expected %f but got %f", lv2, ev2)
	}

	if lv1 != ev1 {
		t.Errorf("expected %f but got %f", lv3, ev3)
	}

	if iv3 != ev3 || iv2 != ev3 || iv1 != ev3 {
		t.Errorf("expected %f %f %f but got %f %f %f", ev3, ev3, ev3, iv1, iv2, iv3)
	}

	_, err := eNil.GetFontSizeVal("line-height")
	if err == nil {
		t.Errorf("Expected error")
	}
}

func TestElement_ConstraintFromAttrib(t *testing.T) {
	e := new(Element)
	e.Style = new(Selector)
	e.Style.attrs = map[string]string{
		"color":         "#ffffff",
		"line-height":   "120%",
		"font-size":     "12pt",
		"padding-left":  "5mm",
		"margin":        "3mm",
		"margin-bottom": "20mm",
		"margin-right":  "7.3rem",
	}

	ev1 := fonts.PointsToMillimeters(1.2)
	ev2 := 3.0
	ev3 := 0.0
	ev4 := fonts.PointsToMillimeters(7.3 * 12)

	av1, _ := e.ConstraintFromAttrib("top", "line-height")
	av2, _ := e.ConstraintFromAttrib("left", "margin")
	av3, _ := e.ConstraintFromAttrib("bottom", "margin")
	av4, _ := e.ConstraintFromAttrib("right", "margin-right")

	//TODO: is this gutenough?
	if int64(ev1*10000) != int64(av1*10000) {
		t.Errorf("Expected %f but value is %f", ev1, av1)
	}

	if ev2 != av2 {
		t.Errorf("Expected %f but value is %f", ev2, av2)
	}

	if ev3 != av3 {
		t.Errorf("Expected %f but value is %f", ev3, av3)
	}

	if ev4 != av4 {
		t.Errorf("Expected %f but value is %f", ev4, av4)
	}
}

func initConstraintSandbox() (sandbox []*Element) {
	adam := new(Element)
	//I know, I know, adam's sons were Cain, Abel, Seth, but we don't want cain in the codebase
	//besides, shem ham and japeth are easier to remember
	shem := new(Element)
	ham := new(Element)
	japeth := new(Element)
	a := new(Element)

	adam.Style = new(Selector)
	shem.Style = new(Selector)
	ham.Style = new(Selector)
	japeth.Style = new(Selector)
	a.Style = new(Selector)

	adam.Style.attrs = map[string]string{
		"font-size":   "12pt",
		"margin":      "0.5in",
		"padding":     "0.75mm",
		"line-height": "1.2rem",
	}

	shem.Style.attrs = map[string]string{
		"font-size":  "16pt",
		"margin-top": "1mm",
	}

	ham.Style.attrs = map[string]string{
		"padding":      "3mm",
		"padding-left": "5mm",
		"margin-right": "2in",
	}

	japeth.Style.attrs = map[string]string{
		"line-height": "100%",
	}

	a.Style.attrs = map[string]string{
		"font-size": "1.2rem",
	}

	adam.Children = append(adam.Children, shem, ham, japeth)
	japeth.Children = append(japeth.Children, a)

	a.Parent = japeth
	shem.Parent = adam
	ham.Parent = adam
	japeth.Parent = adam

	adam.Class = "adam"
	shem.Class = "shem"
	ham.Class = "ham"
	japeth.Class = "japeth"
	a.Class = "classa"
	sandbox = append(sandbox, adam)
	sandbox = append(sandbox, shem)
	sandbox = append(sandbox, ham)
	sandbox = append(sandbox, japeth)
	sandbox = append(sandbox, a)
	return sandbox
}

func initExpectedConstraints() (results []*Constraints) {
	adam := new(Constraints)
	shem := new(Constraints)
	ham := new(Constraints)
	japeth := new(Constraints)
	a := new(Constraints)

	adam.Top = fonts.PointsToMillimeters((72*0.5)+(12*1.2-12)*0.5) + 0.75
	adam.Bottom = fonts.PointsToMillimeters((72*0.5)+(12*1.2-12)*0.5) + 0.75
	adam.Left = 0.75 + fonts.PointsToMillimeters(72)/2
	adam.Right = 0.75 + fonts.PointsToMillimeters(72)/2

	shem.Top = adam.Top + 1 + (fonts.PointsToMillimeters(12*1.2)-fonts.PointsToMillimeters(16))/2
	shem.Bottom = adam.Bottom + (fonts.PointsToMillimeters(12*1.2)-fonts.PointsToMillimeters(16))/2
	shem.Left = adam.Left
	shem.Right = adam.Right

	ham.Top = adam.Top + 3
	ham.Bottom = adam.Bottom + 3
	ham.Left = adam.Left + 5
	ham.Right = adam.Right + 3 + fonts.PointsToMillimeters(72*2)

	japeth.Top = adam.Top + fonts.PointsToMillimeters(12-12)/2
	japeth.Bottom = adam.Bottom + fonts.PointsToMillimeters(12-12)/2
	japeth.Left = adam.Left
	japeth.Right = adam.Right

	a.Top = japeth.Top + fonts.PointsToMillimeters((12*1.2)-(12*1.2))/2
	a.Bottom = japeth.Bottom + fonts.PointsToMillimeters((12*1.2)-(12*1.2))/2
	a.Left = japeth.Left
	a.Right = japeth.Right

	results = append(results, adam, shem, ham, japeth, a)
	return results
}

// cannot do direct float comparison, so convert to int of arbitrary precision.
// hopefully rounding to the nearest nanometer will be close enoug
func lifejacket(f float64) int64 {
	return int64(f * 1000000)
}

func TestElement_ComputeTopConstraints(t *testing.T) {
	world := initConstraintSandbox()
	expected := initExpectedConstraints()
	for i, e := range world {
		e.ComputeParentConstraints()
		e.ComputeTopConstraints()
		if lifejacket(e.Constraints.Top) != lifejacket(expected[i].Top) {
			t.Logf("Element index: %d Element Classname: %s Side: top", i, e.Class)
			t.Errorf("expected %f but got %f instead", expected[i].Top, e.Constraints.Top)
		}
		e.ComputeBottomConstraints()
		if lifejacket(e.Constraints.Bottom) != lifejacket(expected[i].Bottom) {
			t.Logf("Element index: %d Element Classname: %s Side: Bottom", i, e.Class)
			t.Errorf("expected %f but got %f instead", expected[i].Bottom, e.Constraints.Bottom)
		}
		e.ComputeLeftConstraints()
		if lifejacket(e.Constraints.Left) != lifejacket(expected[i].Left) {
			t.Logf("Element index: %d Element Classname: %s Side: Left", i, e.Class)
			t.Errorf("expected %f but got %f instead", expected[i].Left, e.Constraints.Left)
		}
		e.ComputeRightConstraints()
		if lifejacket(e.Constraints.Right) != lifejacket(expected[i].Right) {
			t.Logf("Element index: %d Element Classname: %s Side: Right", i, e.Class)
			t.Errorf("expected %f but got %f instead", expected[i].Right, e.Constraints.Right)
		}
	}
}

func TestElement_ComputeConstraints(t *testing.T) {
	world := initConstraintSandbox()
	expected := initExpectedConstraints()
	for i, e := range world {
		e.ComputeParentConstraints()
		e.ComputeConstraints()
		if lifejacket(e.Constraints.Top) != lifejacket(expected[i].Top) {
			t.Logf("Element index: %d Element Classname: %s Side: Top", i, e.Class)
			t.Errorf("expected %f but got %f instead", expected[i].Top, e.Constraints.Top)
		}

		if lifejacket(e.Constraints.Bottom) != lifejacket(expected[i].Bottom) {
			t.Logf("Element index: %d Element Classname: %s Side: Bottom", i, e.Class)
			t.Errorf("expected %f but got %f instead", expected[i].Bottom, e.Constraints.Bottom)
		}

		if lifejacket(e.Constraints.Left) != lifejacket(expected[i].Left) {
			t.Logf("Element index: %d Element Classname: %s Side: Left", i, e.Class)
			t.Errorf("expected %f but got %f instead", expected[i].Left, e.Constraints.Left)
		}

		if lifejacket(e.Constraints.Right) != lifejacket(expected[i].Right) {
			t.Logf("Element index: %d Element Classname: %s Side: Right", i, e.Class)
			t.Errorf("expected %f but got %f instead", expected[i].Right, e.Constraints.Right)
		}
	}
}

func TestElement_ComputeRealWidth(t *testing.T) {
	e := check(nil)
	a := check(nil)
	b := check(nil)
	c := check(nil)
	alpha := check(nil)
	beta := check(nil)

	a.Width = 32.0
	b.Children = append(a.Children, alpha, beta)
	alpha.Text = "joseph"
	beta.Width = 4.2
	prepCalled := false

	e.Children = append(e.Children, a, b, c)
	res := e.ComputeRealWidth(func(element *Element) {
		prepCalled = true
		return
	}, func(s string) float64 {
		return float64(len(s))
	})
	if !prepCalled {
		t.Errorf("the prep function was never called!")
	}
	expected := 32.0

	if nm(expected) != nm(res) {
		t.Errorf("expected %f but got %f", expected, res)
	}
}

func TestElement_Explode(t *testing.T) {
	e := check(nil)
	if len(e.Children) > 0 {
		t.Errorf("expected element to have 0 children, but instead element has %d children", len(e.Children))
	}
	e.Text = "hello, world!"
	e.Explode()
	expectedWords := []string{"hello,", "world!"}
	expectedCount := len(expectedWords)
	if len(e.Children) != expectedCount {
		t.Errorf("expected exploded element to have %d children, but instead it has %d", expectedCount, len(e.Children))
	}
	for i, w := range expectedWords {
		if e.Children[i].Text != w {
			t.Errorf("expected string '%s' but got '%s' instead", w, e.Children[i].Text)
		}
	}
}

func nm(mm float64) int {
	return int(mm * 1000000)
}

func TestElement_Cram(t *testing.T) {
	e := check(nil)
	a := check(nil)
	b := check(nil)
	c := check(nil)
	alpha := check(nil)
	beta := check(nil)
	a.Children = append(a.Children, alpha)
	alpha.Parent = a
	a.Children = append(a.Children, beta)
	e.Children = append(e.Children, a, b, c)
	a.Parent = e
	b.Parent = e
	c.Parent = e
	e.Width = 26.0    //mm
	alpha.Width = 4.0 //mm
	beta.Width = 8.0  //mm
	c.Height = 10.0   //mm
	b.Width = 12.0    //mm
	c.Width = 3.0     //mm
	e.X = 13.0
	e.Y = 6.2

	e.Cram()
	eaX := 13.0
	eaY := 6.2
	ebX := 13.0 + 1 + 13.0
	ebY := 6.2
	ecX := 13.0
	ecY := 16.2
	ealphaX := 13.0
	ealphaY := 6.2
	ebetaX := 13.0 + 4.0
	ebetaY := 6.2

	/*	e.ComputeRealWidth(func(e *Element) {}, func(s string) float64 {
		return 0.0
	})*/
	if nm(eaX) != nm(a.X) {
		t.Errorf("expected %f but got %f", eaX, a.X)
	}
	if nm(eaY) != nm(a.Y) {
		t.Errorf("expected %f but got %f", eaY, a.Y)
	}
	if nm(ebX) != nm(b.X) {
		t.Errorf("expected %f but got %f", ebX, b.X)
	}
	if nm(ebY) != nm(b.Y) {
		t.Errorf("expected %f but got %f", ebY, b.Y)
	}
	if nm(ecX) != nm(c.X) {
		t.Errorf("expected %f but got %f", ecX, c.X)
	}
	if nm(ecY) != nm(c.Y) {
		t.Errorf("expected %f but got %f", ecY, c.Y)
	}
	if nm(ealphaX) != nm(alpha.X) {
		t.Errorf("expected %f but got %f", ealphaX, alpha.X)
	}
	if nm(ealphaY) != nm(alpha.Y) {
		t.Errorf("expected %f but got %f", ealphaY, alpha.Y)
	}
	if nm(ebetaX) != nm(beta.X) {
		t.Errorf("expected %f but got %f", ebetaX, beta.X)
	}
	if nm(ebetaY) != nm(beta.Y) {
		t.Errorf("expected %f but got %f", ebetaY, beta.Y)
	}
}
