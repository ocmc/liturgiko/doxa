// Package html generates an html file from an ATEM. It contains html templates and structs used to populate the templates.
// Be sure to call InitTemplates before using this package.
package html

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/liturgiko/doxa/pkg/atempl"
	rowTypes "github.com/liturgiko/doxa/pkg/enums/RowTypes"
	"github.com/liturgiko/doxa/pkg/media"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"html/template"
	"os"
	"sort"
	"strings"
	"time"
)

/*
MetaHTML contains information to pass to the doc.gohtml template.
BaseHref is used to set the relative path to a web file resource specified in Links (CSS files) and Scripts (Javascript files).
For example, "../../../../../../../" is used to prepend the resource path, so it will first go up 7 folders.
Title is used in the <Head><title> tag.
Timestamp is used for the Title data-timestamp and should be formatted like "2020.07.01.04.57.25".
Language is used for the Title data-language and should provide the ISO language codes for the columns, e.g. "GR-EN"
Links are the css files to be loaded by the browser.
Scripts are the Javascript files to be loaded by the browser.
Rows contain the information to create rows for the HTML table.
*/
type MetaHTML struct {
	BaseHref        string // browser will prepend to link and script paths, e.g. ../../
	DoxaVersion     string
	Editable        bool // if true results in an ondblclick being set on kvs span
	Footer          Footer
	FramesEnabled   bool
	ID              string   // last segment of template ID
	IndexTitleCodes string   // comma delimited list of codes for indexer to use
	KeyWords        string   // comma delimited list of keywords for search engines to use
	Language        string   // combination of one or more ISO language codes for this html file
	Links           []Link   // path to css files for browser to load
	LmlTemplates    []string // lml templates from which the page was created
	Navbar          Navbar
	PathOut         string // path and filename of index.html file
	Rows            []*atempl.MetaRow
	RowTypes        struct {
		Section       rowTypes.RowType
		Subsection    rowTypes.RowType
		Subsubsection rowTypes.RowType
		EndSection    rowTypes.RowType
	}
	Scripts   []Script           // path to js files for browser to load
	Templates *template.Template // go html templates
	Timestamp string             // saved in html header so, we know when html page was created
	Title     string             // used as the html header title.
	Date      string             // used as attribute in header title
	Versions  map[string]string
}

func (mp *MetaHTML) GetMusicMediaLinks() (links []*media.Link) {
	mediaMap := make(map[string]*media.Link)
	for _, row := range mp.Rows {
		if row.IsMediaRow {
			for _, cell := range row.Cells {
				if cell.MediaCell != nil {
					if cell.MediaCell.Media.Audio != nil {
						for _, m := range cell.MediaCell.Media.Audio {
							mediaMap[m.DbId] = m
						}
					}
					if cell.MediaCell.Media.BScore != nil {
						for _, m := range cell.MediaCell.Media.BScore {
							mediaMap[m.DbId] = m
						}
					}
					if cell.MediaCell.Media.EScore != nil {
						for _, m := range cell.MediaCell.Media.EScore {
							mediaMap[m.DbId] = m
						}
					}
					if cell.MediaCell.Media.WScore != nil {
						for _, m := range cell.MediaCell.Media.WScore {
							mediaMap[m.DbId] = m
						}
					}
				}
			}
		}
	}
	for _, v := range mediaMap {
		links = append(links, v)
	}
	// sort links by DbId
	sort.Slice(links, func(i, j int) bool {
		return links[i].DbId < links[j].DbId
	})
	return links
}
func (mp *MetaHTML) Serialize() error {
	pathOut := fmt.Sprintf("%sjson", mp.PathOut[:len(mp.PathOut)-4])
	// MAC 2024-08-16. Removed serializing to ref dir.  Takes up too much space in generated website.
	//pathOut = strings.Replace(pathOut, "/h/", "/ref/h/", 1)
	jsonMp, err := json.MarshalIndent(mp, "", " ")
	if err != nil {
		return fmt.Errorf("error serializing MetaHTML to %s", pathOut)
	}
	err = ltfile.WriteFile(pathOut, string(jsonMp))
	if err != nil {
		return fmt.Errorf("error serializing MetaHTML to %s", pathOut)
	}
	return nil
}
func (mp *MetaHTML) CreateHTML(tmplName string) error {
	f, err := os.Create(mp.PathOut)
	if err != nil {
		return fmt.Errorf("create file %s: %v", mp.PathOut, err)
	}
	defer f.Close()
	err = mp.Templates.ExecuteTemplate(f, tmplName, mp)
	if err != nil {
		return err
	}
	return nil
	//	return mp.Serialize()
}
func (mp *MetaHTML) ToJson(prettyPrint bool) (string, error) {
	var j []byte
	var err error
	if prettyPrint {
		j, err = json.MarshalIndent(mp, "", " ")
	} else {
		j, err = json.Marshal(mp)
	}
	if err != nil {
		return "", fmt.Errorf("error marshaling ATEM for template %s: %v", mp.ID, err)
	}
	return string(j), nil
}
func NewMetaHTMLFromJson(j string) (*MetaHTML, error) {
	var a *MetaHTML
	err := json.Unmarshal([]byte(j), &a)
	if err != nil {
		return nil, err
	}
	return a, nil
}

// Link contains the HREF for a css file to be loaded by the browser
type Link struct {
	Href string
}

// Script contains the Source for a Javascript file to be loaded by the browser
// MediaType of the script should normally be set to "text/javascript".
// Defer, when true, specifies that the script is to be executed when the page has finished parsing.
type Script struct {
	MediaType string
	Source    string
	Defer     bool
}

type ServicesMasterIndex struct {
	Title         string
	SubTitle      string
	DoxaVersion   string
	BaseHref      string `json:"-"`
	LangCodes     []string
	LangNames     []string
	Links         []Link   `json:"-"` // path to css files for browser to load
	Scripts       []Script `json:"-"` // path to js files for browser to load
	MonthYears    []*ServiceMonthYearIndex
	Navbar        Navbar `json:"-"`
	Footer        Footer `json:"-"`
	FramesEnabled bool
}
type BooksMasterAccordionIndex struct {
	BaseHref      string
	Desc          []string
	DoxaVersion   string
	Footer        Footer
	FramesEnabled bool
	Links         []Link // path to css files for browser to load
	Navbar        Navbar
	Root          *TreeNode
	Scripts       []Script // path to js files for browser to load
	Search        string
	SubTitle      string
	Title         string
}
type TreeNode struct {
	ID            string
	Class         string
	Title         string
	LanguageCodes string
	LanguageNames string
	Href          string
	Pdf           bool
	PdfHref       string
	Children      []*TreeNode
}
type BooksMasterSimpleIndex struct {
	Title     string
	SubTitle  string
	BaseHref  string
	Links     []Link   // path to css files for browser to load
	Scripts   []Script // path to js files for browser to load
	Navbar    Navbar
	Footer    Footer
	Desc      []string
	BookTypes []*BookType
}
type BookType struct {
	Title string
	Books []*Book
}
type Book struct {
	Title     string
	Languages []*BookLanguage
}
type BookLanguage struct {
	Language   string
	MediaTitle string
	Href       string
}

type Navbar struct {
	HasBooksIcon         bool
	HasServicesIcon      bool
	HasSearchIcon        bool
	Simple               bool
	NavbarSrc            string
	NavbarHome           string
	NavbarTitle          string
	NavbarLogoSrc        string
	NavbarLogoHeight     string
	NavbarLogoWidth      string
	NumberOfVersions     int
	ShowColumnToggle     bool
	ShowNightDayToggle   bool
	ShowSecondIconColumn bool // Show second icon column
	ShowTime             bool
	SideBarTextMenuItems TextMenuItems
	TextDecrease         string
	TextIncrease         string
	ToggleItems          []*ToggleItem
	UseLogoForMenu       bool
	UseTextInSideBar     bool
}

type Footer struct {
	Enabled            bool
	Source             string
	InsertFacebook     bool
	FacebookUrl        string
	InsertOwnerWebsite bool
	OwnerWebsiteUrl    string
}
type ServiceMonthYearIndex struct {
	DoxaVersion string
	MonthYear   string
	MonthNbr    int
	YearNbr     int
	Days        []*ServiceDayIndex
}
type ServiceDayIndex struct {
	BaseHref      string
	Date          time.Time `json:"-"`
	DayNbrDow     string
	DayNbr        int
	Description   []string
	DoxaVersion   string
	Href          string
	SubTitle      string
	Title         string
	Links         []Link   `json:"-"` // path to css files for browser to load
	Scripts       []Script `json:"-"` // path to js files for browser to load
	Services      []DocIndex
	Navbar        Navbar `json:"-"`
	Footer        Footer `json:"-"`
	FramesEnabled bool
}
type DocIndex struct {
	Title     string
	Languages []DocLanguage
}
type DocLanguage struct {
	Title    string
	Language string
	Media    string
	Href     string
	PDF      bool
	PdfHref  string
	Target   string
}
type DCS struct {
	BaseHref          string
	Links             []Link // path to css files for browser to load
	Name              string
	Navbar            Navbar
	PathOut           string
	Scripts           []Script // path to js files for browser to load
	Title             string
	MetaDescription   string
	MetaOgTitle       string
	MetaOgDescription string
	MetaOgUrl         string
	Templates         *template.Template // go html templates
	FrameTextSrc      string
}

func (p *DCS) WriteHtml() error {
	f, err := os.Create(p.PathOut)
	if err != nil {
		return fmt.Errorf("create file %s: %v", p.PathOut, err)
	}
	defer f.Close()
	err = p.Templates.ExecuteTemplate(f, p.Name, p)
	if err != nil {
		return err
	}
	return nil
}

// LtxTopicKeysIndex is used to generate both the topics index and keys index.
type LtxTopicKeysIndex struct {
	PathOut    string
	Name       string
	Title      string
	SubTitle   string
	BaseHref   string
	Links      []Link   // path to css files for browser to load
	Scripts    []Script // path to js files for browser to load
	Navbar     Navbar
	Footer     Footer
	Desc       []string
	IndexLinks []*LtxTopicKey
	Templates  *template.Template // go html templates
}
type LtxTopicKey struct {
	Desc string
	Href string
}

func (l *LtxTopicKeysIndex) CompareLinks(i, j int) bool {
	return l.IndexLinks[j].Desc > l.IndexLinks[i].Desc
}

func (p *LtxTopicKeysIndex) WriteHtml() error {
	f, err := os.Create(p.PathOut)
	if err != nil {
		return fmt.Errorf("create file %s: %v", p.PathOut, err)
	}
	defer f.Close()
	err = p.Templates.ExecuteTemplate(f, p.Name, p)
	if err != nil {
		return err
	}
	return nil
}

type Device struct {
	Name      string
	PathOut   string
	Templates *template.Template // go html templates
}

func (p *Device) WriteHtml() error {
	f, err := os.Create(p.PathOut)
	if err != nil {
		return fmt.Errorf("create file %s: %v", p.PathOut, err)
	}
	defer f.Close()
	err = p.Templates.ExecuteTemplate(f, p.Name, p)
	if err != nil {
		return err
	}
	return nil
}

type Home struct {
	BaseHref          string
	BookCard          Card
	CardTmplSrc       string
	Footer            Footer
	FramesEnabled     bool
	HasBooks          bool
	HasServices       bool
	HelpTextPrefix    string
	HelpTextSuffix    string
	ImgAlt            string
	ImgSrc            string
	InsertImg         bool
	IntroCard         Card
	Links             []Link // path to css files for browser to load
	MetaDescription   string
	MetaOgDescription string
	MetaOgTitle       string
	MetaOgUrl         string
	Name              string
	Navbar            Navbar
	Paragraphs        []string
	PathOut           string
	Scripts           []Script // path to js files for browser to load
	ServiceCard       Card
	Source            string
	Templates         *template.Template // go html templates
	Title             string
}

func (p *Home) WriteHtml() error {
	f, err := os.Create(p.PathOut)
	if err != nil {
		return fmt.Errorf("create file %s: %v", p.PathOut, err)
	}
	defer f.Close()
	err = p.Templates.ExecuteTemplate(f, p.Name, p)
	if err != nil {
		return err
	}
	return nil
}

type Blank struct {
	PathOut       string
	Name          string
	Templates     *template.Template // go html templates
	Title         string
	Links         []Link   // path to css files for browser to load
	Scripts       []Script // path to js files for browser to load
	Heading       []string
	Donate        *Card
	Alerts        []string
	Paragraphs    []string
	TwitterHref   string
	TwitterHandle string
}

func (p *Blank) WriteHtml() error {
	f, err := os.Create(p.PathOut)
	if err != nil {
		return fmt.Errorf("create file %s: %v", p.PathOut, err)
	}
	defer f.Close()
	err = p.Templates.ExecuteTemplate(f, p.Name, p)
	if err != nil {
		return err
	}
	return nil
}

type Help struct {
	BaseHref           string
	DoxaVersion        string
	Footer             Footer
	GenTime            string
	IconAudio          string
	IconBooks          string
	IconBScore         string
	IconCalendar       string
	IconColumns        string
	IconEScore         string
	IconHelp           string
	IconHelpIntro      string
	IconHome           string
	IconInfo           string
	IconLeft           string
	IconMinus          string
	IconMoon           string
	IconPlus           string
	IconRight          string
	IconSearch         string
	IconSun            string
	IconTitle          string
	IconWScore         string
	Links              []Link // path to css files for browser to load
	Name               string
	Navbar             Navbar
	Paragraphs         []string
	PathOut            string
	ProviderDesc       string
	ProviderName       string
	ProviderSiteInvite string
	ProviderURL        string
	Scripts            []Script // path to js files for browser to load
	Source             string
	Templates          *template.Template // go html templates
	Title              string
}

func (p *Help) WriteHtml() error {
	f, err := os.Create(p.PathOut)
	if err != nil {
		return fmt.Errorf("create file %s: %v", p.PathOut, err)
	}
	defer f.Close()
	err = p.Templates.ExecuteTemplate(f, p.Name, p)
	if err != nil {
		return err
	}
	return nil
}

type Search struct {
	BaseHref      string
	DoxaVersion   string
	Footer        Footer
	FramesEnabled bool
	Links         []Link // path to css files for browser to load
	Name          string
	Navbar        Navbar
	PathOut       string
	Scripts       []Script // path to js files for browser to load
	Source        string
	Templates     *template.Template // go html templates
	Title         string
}

func (p *Search) WriteHtml() error {
	f, err := os.Create(p.PathOut)
	if err != nil {
		return fmt.Errorf("create file %s: %v", p.PathOut, err)
	}
	defer f.Close()
	err = p.Templates.ExecuteTemplate(f, p.Name, p)
	if err != nil {
		return err
	}
	return nil
}

type TopicKeyCompare struct {
	BaseHref     string
	Defer        bool
	DoxaVersion  string
	Footer       Footer
	Links        []Link // path to css files for browser to load
	Name         string
	Navbar       Navbar
	PathOut      string
	Scripts      []Script           // path to js files for browser to load
	Templates    *template.Template // go html templates
	Title        string
	TopicKey     string
	Greek        []*Translation
	Translations []*Translation
}

func (t *TopicKeyCompare) CompareTranslationLibraries(i, j int) bool {
	return t.Translations[j].Library > t.Translations[i].Library
}

type Translation struct {
	Library string
	Value   string
	Ver     string
}

func (p *TopicKeyCompare) WriteHtml() error {
	f, err := os.Create(p.PathOut)
	if err != nil {
		return fmt.Errorf("create file %s: %v", p.PathOut, err)
	}
	defer f.Close()
	err = p.Templates.ExecuteTemplate(f, p.Name, p)
	if err != nil {
		return err
	}
	return nil
}

// RowSpanning holds information to create a td (table cell) that spans the entire table.
// Its primary purpose is to create either a blank row or a rule that spans the table.
// TmplSrc is the path to the template to load
// Number is the number of columns to be spanned
// Class is the css class for the content of the td.
type RowSpanning struct {
	TmplSrc string
	Number  int
	Class   string
}

/*
RowImage holds the information to create an HTML image that spans the columns of a table.
TmplSrc is the source for the html template file
Number is the number of td (table columns) to be spanned
ImageSrc is the path to the image file
Alt is a text title of the file to be used by html readers
Class is the css class of the image
Width is the width of the image
Height is the height of the image
*/
type RowImage struct {
	TmplSrc  string
	Number   int
	ImageSrc string
	Alt      string
	Class    string
	Width    string
	Height   string
}

func (r *RowImage) ToHTML() (string, error) {
	t, err := template.ParseFiles(r.TmplSrc)
	if err != nil {
		return "", err
	}
	var result bytes.Buffer
	err = t.Execute(&result, r)
	return result.String(), err
}

type Card struct {
	Title      string
	Text       string
	ButtonIcon string
	ButtonText string
	ButtonUrl  string
	ImgSrc     string
	ImgAlt     string
	InsertImg  bool
}

type IndexAccordion struct {
	Id    string
	Class string
	Cards []*IndexCard
}

func (i *IndexAccordion) AddCard(c *IndexCard) {
	i.Cards = append(i.Cards, c)
}

type IndexCard struct {
	Class    string
	Header   string            // card-header
	Desc     []string          // card-body
	Anchors  []*Anchor         // card-body
	Children []*IndexAccordion // card-body
}

func (i *IndexCard) AddChildAccordion(a *IndexAccordion) {
	i.Children = append(i.Children, a)
}
func (i *IndexCard) AddAnchor(a *Anchor) {
	i.Anchors = append(i.Anchors, a)
}

// Anchor contains the information to create an anchor that opens an html or pdf file
type Anchor struct {
	Class   string
	Title   string
	Href    string
	Target  string
	Pdf     bool
	PdfHref string
}
type IndexAccordionStack []string

func (s *IndexAccordionStack) Clear() {
	*s = nil
}
func (s *IndexAccordionStack) Slice() []string {
	var r []string
	for _, s := range *s {
		r = append(r, s)
	}
	return r
}

// Copy values into a new IndexAccordionStack
func (s *IndexAccordionStack) Copy() *IndexAccordionStack {
	new := new(IndexAccordionStack)
	for _, s := range *s {
		new.Push(s)
	}
	return new
}
func (s *IndexAccordionStack) Empty() bool {
	return len(*s) == 0
}

// First returns the initial item pushed on the stack, without removing it
func (s *IndexAccordionStack) First() string {
	if len(*s) > 0 {
		return (*s)[0] // Index into the slice and obtain the element.
	}
	return ""
}

// Get the nth element in the stack, without removing it.
// The stack is zero based, i.e. first element is at index == 0.
// If the value of index >= len(stack), returns ""
func (s *IndexAccordionStack) Get(index int) string {
	if index >= len(*s) {
		return ""
	}
	return (*s)[index]
}

// Set the indexed element in the stack.
// The stack is zero based, i.e. first element is at index == 0.
// If the value of index >= len(stack), returns an error.
func (s *IndexAccordionStack) Set(index int, value string) error {
	if index >= len(*s) {
		return fmt.Errorf("%d exceeds length of stack", index)
	}
	(*s)[index] = value
	return nil
}

// Join returns the elements in the stack as a delimited string.
func (s *IndexAccordionStack) Join(delimiter string) string {
	return strings.Join(*s, delimiter)
}
func (s *IndexAccordionStack) SubPath(from, upTo int, delimiter string) string {
	sb := strings.Builder{}
	for i := from; i < upTo; i++ {
		if sb.Len() > 0 {
			sb.WriteString(delimiter)
		}
		sb.WriteString(s.Get(i))
	}
	return sb.String()
}

// Last returns most recent item pushed on the stack, without popping the stack
func (s *IndexAccordionStack) Last() string {
	if len(*s) > 0 {
		index := len(*s) - 1 // GetKeyPath the index of the last element.
		return (*s)[index]   // Index into the slice and obtain the element.
	}
	return ""
}
func (s *IndexAccordionStack) Pop() string {
	if len(*s) > 0 {
		index := len(*s) - 1   // GetKeyPath the index of the top most element.
		element := (*s)[index] // Index into the slice and obtain the element.
		*s = (*s)[:index]
		return element
	}
	return ""
}
func (s *IndexAccordionStack) Push(value string) {
	*s = append(*s, value)
}
func (s *IndexAccordionStack) Size() int {
	return len(*s)
}

// Split calls Clear(), then splits the value using the delimiter, and pushes each part of the split onto the stack.
func (s *IndexAccordionStack) Split(value, delimiter string) {
	s.Clear()
	parts := strings.Split(value, delimiter)
	for _, p := range parts {
		s.Push(p)
	}
}

type TextMenuItems []*TextMenuItem

func (tm TextMenuItems) Len() int {
	return len(tm)
}

func (tm TextMenuItems) Less(i, j int) bool {
	return tm[i].SortOrder < tm[j].SortOrder
}

func (tm TextMenuItems) Swap(i, j int) {
	tm[i], tm[j] = tm[j], tm[i]
}

type TextMenuItem struct {
	Disabled  bool
	CssClass  string
	Icon      string
	Text      string
	AltText   string // used by readers for visually impaired
	Href      string
	SortOrder string
}

func NewTextMenuItem(cssClass, icon, text, altText, href string, sortOrder int) *TextMenuItem {
	t := new(TextMenuItem)
	t.CssClass = cssClass
	t.Icon = icon
	t.Text = text
	if len(altText) == 0 {
		t.AltText = t.Text
	} else {
		t.AltText = altText
	}
	t.Href = href
	t.SortOrder = fmt.Sprintf("%02d", sortOrder)
	return t
}

type ToggleItem struct {
	Action   string // one of, "col1, col2, col3, layout, frames"
	Disabled bool
	Text     string // label for the toggle
	AltText  string // used by readers for visually impaired
}
