package pdf

import "testing"

func TestGetCssColor(t *testing.T) {
	var testInputs = []string{"rgb(255,0,255)", "rgb(255 0 255)", "rgb(255, 0, 255)", "#ff00ff", "magenta"}
	for _, s := range testInputs {
		rgb := GetCssColor(s, "#000000")
		expectRed := 255
		expectGreen := 0
		expectBlue := 255
		if rgb.Red != expectRed {
			t.Errorf("Expected Red=%d but got %d", expectRed, rgb.Red)
		}
		if rgb.Green != expectGreen {
			t.Errorf("Expected Green=%d but got %d", expectGreen, rgb.Green)
		}
		if rgb.Blue != expectBlue {
			t.Errorf("Expected Blue=%d but got %d", expectBlue, rgb.Blue)
		}
	}
}

func TestGetCssColorFail(t *testing.T) {
	var testInputs = []string{"rgb(257,0,255)", "rgb(-24 0 255)", "#fg00ff", "muddypurpleorange"}
	for _, s := range testInputs {
		rgb := GetCssColor(s, "#000000")
		expectRed := 0
		expectGreen := 0
		expectBlue := 0
		if rgb.Red != expectRed {
			t.Errorf("Expected Red=%d but got %d", expectRed, rgb.Red)
		}
		if rgb.Green != expectGreen {
			t.Errorf("Expected Green=%d but got %d", expectGreen, rgb.Green)
		}
		if rgb.Blue != expectBlue {
			t.Errorf("Expected Blue=%d but got %d", expectBlue, rgb.Blue)
		}
	}
}
