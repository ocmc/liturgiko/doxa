// Package pdf generates PDF files from a MetaPDF object.  It uses gofpdf.  For a pdf manipulation library see https://github.com/pdfcpu/pdfcpu
package pdf

import (
	"errors"
	"fmt"
	"github.com/01walid/goarabic"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/atempl"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/css"
	rowTypes "github.com/liturgiko/doxa/pkg/enums/RowTypes"
	"github.com/liturgiko/doxa/pkg/enums/directiveTypes"
	"github.com/liturgiko/doxa/pkg/enums/properties"
	"github.com/liturgiko/doxa/pkg/enums/versions"
	"github.com/liturgiko/doxa/pkg/fonts"
	gofpdf "github.com/liturgiko/doxa/pkg/fpdf"
	"github.com/liturgiko/doxa/pkg/generators/paged"
	"github.com/liturgiko/doxa/pkg/models"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"math"
	"path"
	"regexp"
	"strconv"
	"strings"
	goTmpl "text/template"
	"unicode"
	"unicode/utf8"
)

const indexTemplate = `
<!DOCTYPE html>
<html>
<head>
<title data-language='{{.Lang}}' data-type='pdf' data-filename='{{.Filename}}'>{{.Title}}</title>
<meta name="indexcodes" content="{{.IndexTitleCodes}}">
<meta name="keywords" content="{{.KeyWords}}">
<script>window.location="{{.Filename}}"</script>
</head>
<body></body>
</html>`

const indentProxy = "" //"~"
const linesBeforeTriggeredPageBreak = 5
const legacyIndent = true //this should eventually be removed
const legacyPDF = true

// how much can justify modify the spacing of glpyhs and words?
// note that these are based on the SpacerWidth
const justifyMaxFactorGlyph = 0.9
const justifyMaxFactorWord = 2.5

// how much should be given to
const justifyWordGlyphRatio = 0.75

type MetaPDF struct {
	ATEM                 *atempl.ATEM
	Config               *models.BuildConfig
	IndexTitleCodes      string
	KeyWords             string
	LayoutIndex          int
	PathOut              string
	Acronym              string
	GenLibs              []*atempl.GenLib
	Rows                 []*atempl.MetaRow
	MsgChan              chan string
	MsgMap               *map[string]int
	RelayMuted           bool // if true, relay is silenced
	RelayUsesStandardOut bool
}

type IndexFileData struct {
	Filename        string
	Title           string
	Lang            string
	IndexTitleCodes string
	KeyWords        string
}

func (m *MetaPDF) CreatePDF(msgChan chan string,
	relayMuted bool, // if true, relay is silenced
	RelayUsesStandardOut bool, msgMap *map[string]int) error {
	m.MsgChan = msgChan
	m.MsgMap = msgMap
	m.RelayMuted = relayMuted
	m.RelayUsesStandardOut = RelayUsesStandardOut
	if len(m.ATEM.RowLayouts) == 0 {
		return nil
	} else if len(m.ATEM.RowLayouts[0].Rows) == 0 {
		return nil
	}
	// fpdf allows various units, but we are only using millimeters (mm)
	orientation := gofpdf.OrientationPortrait
	pageSize := m.Config.SM.StringProps[properties.SiteBuildPdfPaperSize]
	if psel, exists := m.ATEM.PDF.CSS.RuleMap["@page"]; exists {
		for _, attrib := range psel.Declarations {
			switch attrib.Identifier {
			case "size":
				pageSize = strings.ToLower(attrib.Value)
				parts := strings.Split(pageSize, " ")
				if len(parts) > 1 {
					for _, o := range parts {
						switch o {
						case "landscape":
							orientation = gofpdf.OrientationLandscape
						case "portrait":
							orientation = gofpdf.OrientationPortrait
						default:
							pageSize = o
						}
					}
				}
			}
		}
	}

	pdf := gofpdf.New(orientation, "mm", pageSize, "")
	pw, err := NewPdfWriter(pdf, m.Config, m.ATEM.PDF, m.Acronym, m.LayoutIndex, m.Rows, m.MsgChan, m.RelayMuted, m.RelayUsesStandardOut)
	margLeft, margTop, margRight, _ := pdf.GetMargins()
	if psel, exists := m.ATEM.PDF.CSS.RuleMap["@page"]; exists {
		for _, attrib := range psel.Declarations {
			switch attrib.Identifier {
			case "margin-top":
				margTop, _ = GetCssLength(pw, attrib.Value, margTop)
			case "margin-left":
				margLeft, _ = GetCssLength(pw, attrib.Value, margLeft)
			case "margin-right":
				margRight, _ = GetCssLength(pw, attrib.Value, margRight)
			}
		}
	}
	pdf.SetMargins(margLeft, margTop, margRight)
	if err != nil {
		return err
	}

	pw.WritePDF()
	for k, v := range pw.Pdf.PdfMessages {
		(*m.MsgMap)[k] += v
	}
	err = pdf.OutputFileAndClose(m.PathOut)
	if err != nil {
		return err
	}
	// write out the index.html that will load the PDF into a web browser
	var indexData IndexFileData
	indexData.IndexTitleCodes = m.IndexTitleCodes
	indexData.KeyWords = m.KeyWords
	dir, base := path.Split(m.PathOut)
	indexData.Filename = base
	if len(base) > 4 { // should be, but play it safe
		indexData.Title = base[:len(base)-4]
	}
	dirParts := strings.Split(dir, "/")
	indexData.Lang = dirParts[len(dirParts)-2]
	indexFilePath := path.Join(dir, "index.html")
	f, err := ltfile.Create(indexFilePath)
	if err != nil {
		return err
	}
	t := goTmpl.Must(goTmpl.New("config").Parse(indexTemplate))
	err = t.Execute(f, indexData)
	if err != nil {
		return err
	}
	err = f.Close()
	if err != nil {
		return err
	}
	return nil
}

// WriteRecords creates a PDF file of each database record provided and displays the database ID under it.
func WriteRecords(pathOut string, recordMap map[string]*kvs.DbR, fontMap map[string]*fonts.FontSet) error {
	pdf := gofpdf.New(gofpdf.OrientationPortrait, "mm", "letter", "")

	// We use the font path from "default" to tell fpdf where to find the fonts.
	pdf.SetFontLocation(fontMap["default"].FontPath)
	// LoadRemote fonts from path.
	// The keys will be "default" and iso language codes, e.g. "en", "gr", "spa", etc.
	for key, value := range fontMap {
		pdf.AddUTF8Font(key, "", value.Regular)
		if pdf.Err() {
			return pdf.Error()
		}
		pdf.AddUTF8Font(key, "B", value.Bold)
		if pdf.Err() {
			return pdf.Error()
		}
		pdf.AddUTF8Font(key, "I", value.Italic)
		if pdf.Err() {
			return pdf.Error()
		}
		pdf.AddUTF8Font(key, "BI", value.BoldItalic)
		if pdf.Err() {
			return pdf.Error()
		}
	}
	// Set the initial font to "default".
	// When each cell of a table row is processed,
	// the font will be set for that language.
	pdf.SetFont("default", "", 12)

	defaultAlignment := "L"
	intPageNbr := 0
	lineHeight := 10.0
	title := "Texts Printed from OCMC's Doxa Database"
	pageWidth, pageHeight := pdf.GetPageSize()
	leftMargin, rightMargin, topMargin, bottomMargin := 20.0, 20.0, 20.0, 20.0
	middleWidth := pageWidth - leftMargin - rightMargin

	pdf.SetMargins(leftMargin, topMargin, rightMargin)
	header := func() {
		// Calculate width of title and position
		sw := pdf.GetStringWidth(title)
		// Colors of frame, background and text
		pdf.SetDrawColor(255, 255, 255) // white
		pdf.SetFillColor(255, 255, 255) // white
		pdf.SetTextColor(255, 0, 0)     // red
		// Thickness of frame (1 mm)
		pdf.SetLineWidth(1)
		pdf.SetX((pageWidth - pdf.GetStringWidth(title)) / 2)
		pdf.LTR()
		// Print Title
		pdf.CellFormat(sw, lineHeight, title, "1", 1, "C", true, 0, "")
		// Line break
		pdf.Ln(lineHeight)
		pdf.SetTextColor(0, 0, 0) // black
	}
	pdf.SetHeaderFunc(func() {
		header()
	})
	pdf.SetFooterFunc(func() {
		pdf.SetY(pageHeight - bottomMargin)
		// print page number
		intPageNbr++
		strPageNo := fmt.Sprintf("Body %d", intPageNbr)
		pdf.SetX((pageWidth - pdf.GetStringWidth(strPageNo)) / 2)
		wd := pdf.GetStringWidth(strPageNo) / 2
		pdf.LTR()
		pdf.SetFontSize(10)
		pdf.SetTextColor(255, 0, 0)
		pdf.CellFormat(wd, lineHeight, strPageNo,
			"", 0, defaultAlignment, false, 0, "")
		pdf.SetTextColor(0, 0, 0)
	})
	// initial call to header
	header()

	pdf.AddPage()

	for _, rec := range recordMap {
		if len(rec.Value) == 0 {
			continue
		}
		if rec.KP.Dirs.Size() > 1 {
			alignment := defaultAlignment
			lang, err := ltstring.LangFromLibrary(rec.KP.Dirs.Get(1))
			if err != nil {
				return fmt.Errorf("error getting library for record: %v", err)
			}
			pdf.SetFont(lang, "", 12)
			// set language direction and font
			switch lang {
			case "ara": // right to left languages
				rec.Value = goarabic.ToGlyph(rec.Value)
				pdf.RTL()
				if defaultAlignment == "L" {
					alignment = "R"
				}
			default: // left to right languages
				pdf.LTR()
			}
			if lang == "kor" {
				rec.Value = strings.ReplaceAll(rec.Value, " ", "  ")
			}
			// write record to PDF
			pdf.MultiCell(middleWidth, lineHeight, rec.Value, "", alignment, false)
			// print database ID
			pdf.LTR()
			pdf.SetFont("default", "I", 9)
			pdf.MultiCell(middleWidth, lineHeight, rec.KP.Path(),
				"", alignment, false)
			pdf.Ln(-1)
		}
	}
	err := pdf.OutputFileAndClose(pathOut)
	return err
}
func reverseString(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}

type Span struct {
	Text   string
	Format Format
}
type RGB struct {
	Red   int
	Green int
	Blue  int
}

func (r RGB) Color() (int, int, int) {
	return r.Red, r.Green, r.Blue
}

type Row struct {
	Cells  []Cell
	Format Format
}

func (r *Row) AddSpan(colNbr int, text string, format Format) {
	r.Cells[colNbr].AddSpan(text, format)
}

type Cell struct {
	Spans    []*Span
	Language string
}

func (c *Cell) AddSpan(text string, format Format) {
	c.Spans = append(c.Spans, NewSpan(text, format))
}

type SliceRange struct {
	StrFrom      int
	StrTo        int
	RuneFrom     int
	RuneTo       int
	SubString    string
	SubRunes     []rune
	Format       *Format
	OriginalSpan *atempl.Span
}
type FormattedSpans struct {
	Text         string
	FormatBreaks []int
	Formats      []*Format
	LineBreaks   []int
	Runes        []rune
	Spans        []*SliceRange
	CenterIndent float64
}

// CalculateLineWidth calculates the width of a given slice of formatted spans based on font size
func (f *FormattedSpans) CalculateLineWidth(pdf *gofpdf.Fpdf, sliceStart int, sliceEnd int) float64 {
	if sliceStart > len(f.Runes) {
		return 0.0
	}
	if sliceStart < 0 {
		sliceStart = 0
	}
	if sliceEnd <= sliceStart {
		return 0.0
	}
	if sliceEnd > len(f.Runes) {
		sliceEnd = len(f.Runes)
	}
	//push fontsize
	/*originalFontSize, _ := pdf.GetFontSize()
	fontSize := originalFontSize
	if f.Formats[0].FontSize > 0 { //TODO: audit
		fontSize = f.Formats[0].FontSize
	}
	// now set the current font size for the calculation
	//TODO: this function assumes that the paragraph font will not differ greatly in character width from the current.
	// this cannot be guaranteed
	pdf.SetFontSize(fontSize)
	//TODO: outsource push and pop to function*/
	return pdf.GetStringWidth(string(f.Runes[sliceStart:sliceEnd]))
	//pdf.SetFontSize(originalFontSize)
	//return fonts.PointsToMillimeters(fontSize * float64(symW) / 1000)
	//StringSymbolWidth*FontSize/1000
	//stringsymbolwidth is determined
}

// MaxLineWidth returns the length of the longest line break
func (f *FormattedSpans) MaxLineWidth(pdf *gofpdf.Fpdf) float64 {
	var maxW float64
	var priorIndex int
	for _, breakIndex := range f.LineBreaks {
		text := strings.TrimSpace(string(f.Runes[priorIndex:breakIndex]))
		l := pdf.GetStringWidth(text)
		if l > maxW {
			maxW = l
		}
		priorIndex = breakIndex
	}
	return maxW + 2
}

type LineConfig struct {
	X1     float64
	Y1     float64
	X2     float64
	Y2     float64
	Width  float64
	ColorR int
	ColorG int
	ColorB int
}

func (lc *LineConfig) GetCoordinates() (float64, float64, float64, float64) {
	return lc.X1, lc.Y1, lc.X2, lc.Y2
}
func (lc *LineConfig) GetRGB() (int, int, int) {
	return lc.ColorR, lc.ColorG, lc.ColorB
}

// a little structure to clean up the border drawing logic
type Border struct {
	startX       float64
	startY       float64
	endX         float64
	endY         float64
	BorderTop    BorderSide
	BorderBottom BorderSide
	BorderLeft   BorderSide
	BorderRight  BorderSide
}

func (b *Border) start(fpdf *gofpdf.Fpdf) {
	b.startX, b.startY = fpdf.GetXY()
}

func (b *Border) expand(fpdf *gofpdf.Fpdf) {
	x, y := fpdf.GetXY()
	_, fs := fpdf.GetFontSize()
	y += fs
	if b.endX < x {
		b.endX = x
	}
	if b.endY < y {
		b.endY = y
	}
}

// style is internally used. This function is called before every Pdf.Line in draw.
// it returns a string of the line style. e.g. "solid", "dashed", or "double"
func (b *Border) style(w *Writer, side string) string {

	//r, g, bl := w.currentParagraphFormat.BorderColor.Color()
	r, g, bl := RGB{}.Color()
	rs := "solid" //w.currentParagraphFormat.RuleStyle
	width := 0.0  //w.currentParagraphFormat.RuleWidth
	var solid = []float64{}
	var dashed = []float64{width, 2 * width}
	var sd BorderSide

	switch side {
	case "top":
		sd = b.BorderTop
		if sd.Style == "" {
			sd = w.currentParagraphFormat.BorderTop
		}
	case "bottom":
		sd = b.BorderBottom
		if sd.Style == "" {
			sd = w.currentParagraphFormat.BorderBottom
		}
	case "left":
		sd = b.BorderLeft
		if sd.Style == "" {
			sd = w.currentParagraphFormat.BorderLeft
		}
	case "right":
		sd = b.BorderRight
		if sd.Style == "" {
			sd = w.currentParagraphFormat.BorderRight
		}
	}
	if sd.Style != "" {
		rs = sd.Style
	}

	var blank RGB
	if sd.Color != blank {
		r, g, bl = sd.Color.Color()
	}

	w.Pdf.SetDrawColor(r, g, bl)
	w.Pdf.SetLineWidth(width)

	switch rs {
	case "solid", "double":
		w.Pdf.SetDashPattern(solid, 0.0)
	case "dashed":
		w.Pdf.SetDashPattern(dashed, 1)
	default:
		w.Pdf.SetDashPattern(solid, 0.0)
	}
	return rs
}

// draw is called to draw the border.
// I apologize for the messiness of the code.
// You may be tempted to refactor the code so that every b.style(w) == "double"
// is merged into one if block with for Line() calls
// DON'T! we have to update style for every side, and so this is more efficient
func (b *Border) draw(w *Writer) {

	// do some nice padding or whatever
	// TODO: set this to padding CSS property
	b.startX += 1
	b.startY += 1
	/*
		b.endX += w.BodyStyle.PaddingRight + w.BodyStyle.PaddingLeft
		b.endY += w.BodyStyle.PaddingBottom + w.BodyStyle.PaddingTop
		b.endX -= w.BodyStyle.MarginRight
		b.endY -= w.BodyStyle.MarginBottom
	*/
	fudge := 0.0
	fudgeFac := 1.0
	if b.style(w, "top") == "double" {
		fudge = b.BorderTop.Width * fudgeFac
		w.Pdf.Line(b.startX-fudge, b.startY-fudge, b.endX+fudge, b.startY-fudge)
	}
	w.Pdf.Line(b.startX+fudge, b.startY+fudge, b.endX-fudge, b.startY+fudge)

	if b.style(w, "right") == "double" {
		fudge = b.BorderRight.Width * fudgeFac
		w.Pdf.Line(b.endX+fudge, b.startY-fudge, b.endX+fudge, b.endY+fudge)
	} else {
		fudge = 0.0
	}
	w.Pdf.Line(b.endX-fudge, b.startY+fudge, b.endX-fudge, b.endY-fudge)

	if b.style(w, "bottom") == "double" {
		fudge = b.BorderBottom.Width * fudgeFac
		w.Pdf.Line(b.startX-fudge, b.endY+fudge, b.endX+fudge, b.endY+fudge)
	} else {
		fudge = 0.0
	}
	w.Pdf.Line(b.startX+fudge, b.endY-fudge, b.endX-fudge, b.endY-fudge)

	if b.style(w, "left") == "double" {
		fudge = b.BorderLeft.Width * fudgeFac
		w.Pdf.Line(b.startX-fudge, b.startY-fudge, b.startX-fudge, b.endY+fudge)
	} else {
		fudge = 0.0
	}
	w.Pdf.Line(b.startX+fudge, b.startY+fudge, b.startX+fudge, b.endY-fudge)

	//reset it
	w.Pdf.SetDashPattern([]float64{}, 0.0)
}

type Writer struct {
	Acronym                    string
	BottomMargin               float64
	BottomMarginLineConfig     *LineConfig
	CellMargin                 float64
	IndentBy                   float64
	CenterX                    float64
	ColumnLineConfig           []*LineConfig
	Columns                    bool
	ColumnsFromPreferences     bool
	ColumnWidth                float64
	ColumnWidthAdjustment      float64
	ColumnX                    []float64
	CurrentColumn              int
	currentFont                *fonts.FontSet
	CurrentFormats             []*Format
	currentParagraphFormat     *Format
	currentSpanFormat          *Format
	HasHeaderFirst             bool
	HasFooterFirst             bool
	FontMap                    map[string]*fonts.FontSet
	FontSize                   float64 //constant, set via CSS body font-size if present, else defaults to 12
	FooterFormat               *Format
	FooterLine                 bool
	FooterLineConfig           *LineConfig
	FooterSections             *ParitySections
	FooterY                    float64
	GutterLine                 bool
	GutterWidth                float64
	HeaderFormat               *Format
	HeaderLine                 bool
	HeaderLineConfig           *LineConfig
	HeaderSections             *ParitySections
	LayoutIndex                int
	AlreadyIndented            bool
	HeaderCalled               bool
	InitialPageNumber          int
	LangCodes                  []string
	LeftMargin                 float64
	LeftMarginLineConfig       *LineConfig
	LineHeight                 float64
	LinesRemaining             int
	MarginLine                 bool
	MetaPdfData                *atempl.PDF
	MiddleColumnAdjuster       float64
	MultiLanguage              bool
	OriginalMultiLanguage      bool
	NumberOfGutters            int
	NumberOfMonolingualColumns int
	NumberOfVersions           int
	PageHeight                 float64
	PageWidth                  float64
	ParagraphSpacing           float64
	Pdf                        *gofpdf.Fpdf
	RightMargin                float64
	RightMarginLineConfig      *LineConfig
	Rows                       []*atempl.MetaRow
	SpacerWidthMillimeters     float64
	VersionSpecific            bool // used when the template set a specific version to use temporarily
	Title                      string
	TopMargin                  float64
	TopMarginLineConfig        *LineConfig
	BodyStyle                  CssSelector
	MsgChan                    chan string
	RelayMuted                 bool // if true, relay is silenced
	RelayUsesStandardOut       bool
}

func SideToString(side BorderSide) string {
	r, g, b := side.Color.Color()
	rgb := (r << 16) + (g << 8) + b
	rgbStr := fmt.Sprintf("%fmm %s #%06x", side.Width, side.Style, rgb)
	return rgbStr
}

var recalibrateCentering, requiresAdditionalLineBreak bool //used to fix centering
var backupCenterX, backupLeftX float64

func (pw *Writer) WriteCell(column int, columnX, offsetleft, offsetright float64, cell *atempl.Cell, currentCC *paged.Element) {

	var formattedSpans *FormattedSpans
	var cellLineCount int
	pw.AlreadyIndented = false
	if cell.RuleCell != nil {
		pw.DrawSectionLine(cell.RuleCell.HrClass, columnX+pw.ColumnWidth/2, pw.Pdf.GetY())
		return
	}
	if cell.BlankCell != nil {
		// already handled above
		return
	}
	if cell.ParaCell != nil {
		pw.AlreadyIndented = false
		if paraRule, exists := pw.MetaPdfData.CSS.GetRule("p." + cell.ParaCell.PClass); exists {
			ff := paraRule.GetDeclaration("font-family")
			fst := paraRule.GetDeclaration("font-style")
			fsz := paraRule.GetDeclaration("font-size")
			style := ""
			size := pw.currentParagraphFormat.FontSize
			if fst != nil {
				style = strings.ToUpper(fst.Value[0:1])
			}
			if fsz != nil {
				size = fsz.Float
			}
			if ff != nil {
				pw.Pdf.SetFont(ff.Value, style, size)
			} else {
				pw.Pdf.SetFontSize(size)
			}
		} else {
			pw.Pdf.SetFont(cell.ParaCell.Language, "", pw.FontSize)
			pw.currentFont = pw.FontMap[cell.ParaCell.Language]
			pw.currentParagraphFormat.FontName = cell.ParaCell.Language
		}
		oldPWAC := pw.ColumnWidthAdjustment
		pw.ColumnWidthAdjustment += offsetleft
		pw.ColumnWidthAdjustment += offsetright
		formattedSpans = pw.GetFormattedSpans(cell.ParaCell.Spans, pw.currentParagraphFormat)
		pw.ColumnWidthAdjustment = oldPWAC

		if pw.currentParagraphFormat.BreakBefore {
			_, curY := pw.Pdf.GetXY()
			_, maxY := pw.Pdf.GetPageSize()
			acceptableThreshold := pw.BottomMargin + (linesBeforeTriggeredPageBreak * pw.LineHeight)
			if maxY-curY <= acceptableThreshold {
				pw.Pdf.SetAutoPageBreak(true, acceptableThreshold)
			}
		}

		l := len(formattedSpans.LineBreaks)
		if cellLineCount < l {
			cellLineCount = l
		}
	}
	var linesWritten int
	// initial column runeIndexes
	var runeIndex int
	var formatsWritten int
	// Normally, in the two for-loops below,
	// we want to iterate over all the versions,
	// thereby creating a column for each.
	// But, if in the LML template, the user
	// invoked setVersion = 1, for example,
	// there will only be one row cell.  So, we need to
	// default the nbrColumns to NumberOfVersions,
	// but if VersionSpecific (because of setVersion)
	// set nbrColumns to just 1.
	var nbrColumns = pw.NumberOfVersions
	if pw.VersionSpecific {
		nbrColumns = 1
	}

	indentCount := 0
	if !pw.AlreadyIndented {
		indentCount = nbrColumns
	}
	for linesWritten < cellLineCount {
		pw.Pdf.SetX(columnX + offsetleft)
		if indentCount > 0 {
			x := pw.Pdf.GetX()
			x += pw.currentParagraphFormat.TextIndent
			pw.Pdf.SetX(x)
			indentCount--
		}
		// track lines remaining to be written so if
		// page break function is called, we know whether
		// everything had been written for that column.
		pw.LinesRemaining = len(formattedSpans.LineBreaks)

		// set the font family, and font color to the default value.
		// They can be overridden below if there is a span-level
		// formatting change.
		font := pw.CurrentFormats[column]
		pclass := cell.ParaCell.PClass
		if paraRule, exists := pw.MetaPdfData.CSS.GetRule("p." + pclass); exists {
			fontFamDec := paraRule.GetDeclaration("font-family")
			fontSizeDec := paraRule.GetDeclaration("font-size")
			fontStyleDec := paraRule.GetDeclaration("font-style")
			fontWeightDec := paraRule.GetDeclaration("font-weight")
			fontSize := font.FontSize
			fontStyle := ""
			fontFam := cell.ParaCell.Language
			if fontSizeDec != nil {
				fontSizeDec.NormalizeFloatAndUnit(pw.FontSize)
				fontSize = fontSizeDec.Float
				pw.FontSize = fontSize
			}
			if fontStyleDec != nil {
				if strings.ToLower(fontStyleDec.Value) == "italic" {
					fontStyle += "I"
				}
			}

			if fontWeightDec != nil {
				if strings.ToLower(fontWeightDec.Value) == "bold" {
					fontStyle += "B"
				}
			}

			if fontFamDec != nil {
				fontFam = fontFamDec.Value
			}
			pw.Pdf.SetFont(fontFam, fontStyle, fontSize)
		}
		pw.Pdf.SetTextColor(font.GetColor())
		if currentCC != nil {
			if currentCC.Type == "row" {
				if len(currentCC.Children) > 0 {
					child := currentCC.Children[0]
					if child.Type == "p" {
						child.ComputeConstraints()
					}
				}
			} else if len(currentCC.Children) > 0 {
				//TODO: do we want this to be column specific
				currentCC.ComputeConstraints()
			}
		}
		// center title text by indenting with blank spaces
		if pw.currentParagraphFormat.Alignment == "C" {
			if len(formattedSpans.Formats) > 0 {
				pw.Pdf.SetFont(formattedSpans.Formats[0].FontName, formattedSpans.Formats[0].Style, formattedSpans.Formats[0].FontSize)
			}
			start := 0
			if linesWritten > 0 && linesWritten < len(formattedSpans.LineBreaks)+1 {
				start = formattedSpans.LineBreaks[linesWritten-1]
			}
			ending := len(formattedSpans.Runes)
			if linesWritten < len(formattedSpans.LineBreaks) {
				ending = formattedSpans.LineBreaks[linesWritten]
			}
			curLineW := pw.Pdf.GetStringWidth(string(formattedSpans.Runes[start:ending]))
			leftSide := pw.PageWidth - pw.RightMargin
			if pw.CurrentColumn < len(pw.ColumnX)-1 {
				if pw.ColumnX[pw.CurrentColumn] < pw.ColumnX[pw.CurrentColumn+1] {
					leftSide = pw.ColumnX[pw.CurrentColumn+1]
				}
			}
			columnWidth := leftSide - pw.ColumnX[pw.CurrentColumn]
			indent := (columnWidth - curLineW) / 2 //TODO: support paragraph's margin
			//this calculates a fudge factor, therefore it is a fudge factory
			//curLineW *= 1.0344  //legacy fudge factor, calculated in the stone age
			pageW, _ := pw.Pdf.GetPageSize()
			mL, _, _, _ := pw.Pdf.GetMargins()
			manuscript := string(formattedSpans.Runes[start:ending])
			lifejacket := 3200 //avoid infinite loop
			for pw.Pdf.GetStringWidth(manuscript)+indent+columnX > pageW-(mL+indent*0.9)-pw.Pdf.GetStringWidth("_") {
				if indent > 0.1 {
					indent *= 0.9
				}
				if indent < 0.1 {
					indent = 0
				}
				if indent == 0 && lifejacket > 0 {
					fs, _ := pw.Pdf.GetFontSize()
					pw.Pdf.SetFontSize(fs * 0.9)
					lifejacket--
				}
			}

			pw.Pdf.SetX(columnX + indent)
		}
		glyphFudgeFac := 0.0
		wordFudgeFac := 0.0
		if pw.currentParagraphFormat.Alignment == "J" && linesWritten < len(formattedSpans.LineBreaks)-1 {
			if len(formattedSpans.Formats) > 0 {
				pw.Pdf.SetFont(formattedSpans.Formats[0].FontName, formattedSpans.Formats[0].Style, formattedSpans.Formats[0].FontSize)
			}
			start := 0
			if linesWritten > 0 && linesWritten < len(formattedSpans.LineBreaks)+1 {
				start = formattedSpans.LineBreaks[linesWritten-1]
			}
			ending := len(formattedSpans.Runes)
			if linesWritten < len(formattedSpans.LineBreaks) {
				ending = formattedSpans.LineBreaks[linesWritten]
			}
			thisLine := formattedSpans.Runes[start:ending]
			curLineW := pw.Pdf.GetStringWidth(string(thisLine))
			leftSide := pw.PageWidth - pw.RightMargin
			if pw.CurrentColumn < len(pw.ColumnX)-1 {
				if pw.ColumnX[pw.CurrentColumn] < pw.ColumnX[pw.CurrentColumn+1] {
					leftSide = pw.ColumnX[pw.CurrentColumn+1]
				}
			}
			columnWidth := leftSide - pw.ColumnX[pw.CurrentColumn]
			if start == 0 {
				// avoid indent overlap
				columnWidth -= pw.currentParagraphFormat.TextIndent
			}
			curLineW += currentCC.Constraints.Left
			curLineW += currentCC.Constraints.Right
			gap := columnWidth - curLineW
			numChars := ending - start
			//get the number of spaces
			numSpaces := 0
			for _, r := range thisLine {
				//TODO: do chinese characters get treated special?
				if r == ' ' {
					numSpaces++
					numChars--
				}
			}
			wordFudgeFac = gap * justifyWordGlyphRatio / float64(numSpaces)
			if wordFudgeFac > pw.SpacerWidthMillimeters*justifyMaxFactorWord {
				wordFudgeFac = justifyMaxFactorWord
			}
			solved := wordFudgeFac * float64(numSpaces)
			//glyphFudgeFac = gap*(1-justifyWordGlyphRatio) - solved/float64(numChars)
			leftOver := gap - solved
			glyphFudgeFac = leftOver / (float64(numChars))
			if glyphFudgeFac > pw.SpacerWidthMillimeters*justifyMaxFactorGlyph {
				glyphFudgeFac = justifyMaxFactorGlyph
			}
		}
		var vetusX float64
		var floatright bool
		formattedSpans = CleanSpaces(formattedSpans)
		if linesWritten < len(formattedSpans.LineBreaks) {
			for runeIndex < formattedSpans.LineBreaks[linesWritten] {
				formatBreaks := formattedSpans.FormatBreaks
				if formatsWritten < len(formattedSpans.FormatBreaks) {
					currentFormatBreak := formatBreaks[formatsWritten]
					if runeIndex == currentFormatBreak {
						format := formattedSpans.Formats[formatsWritten]
						//if len(formattedSpans) > colIndex && len(formatsWritten) > colIndex {
						//if len(fsf) > fsi {
						pw.Pdf.SetFont(format.FontName, format.Style, format.FontSize)
						if format.Float == "right" {
							vetusX = pw.Pdf.GetX()
							spans := formattedSpans.Spans
							var str string
							for _, span := range spans {
								if span.Format.Float == "right" {
									str = span.SubString
									break
								}
							}
							elephant := pw.Pdf.GetStringWidth(str)
							fudge := 4.0 //* pw.Pdf.GetStringWidth("_")
							rightFac, err := currentCC.GetLengthVal("right")
							elephant *= 1.2
							if err == nil {
								fudge += rightFac
							}
							novusX := columnX + (pw.ColumnWidth - elephant) - fudge
							if novusX > vetusX {
								floatright = true
								pw.Pdf.SetX(novusX)
							}
						}
						if format.Float != "right" && vetusX != 0 {
							pw.Pdf.SetX(vetusX)
							vetusX = 0
						}
						//}
						//}
						pw.Pdf.SetTextColor(format.GetColor())
						pw.CurrentFormats[column] = format.Copy()
						formatsWritten++
					}
				}
				char := string(formattedSpans.Runes[runeIndex])
				// if we are starting a new line and the first character is a space,
				// make it an empty string.
				if pw.Pdf.GetX() == columnX+offsetleft {
					if char == " " {
						char = ""
					}
				}

				if pw.currentParagraphFormat.Alignment == "C" {
					pw.Pdf.SetCellMargin(0)
				}
				/*if !pw.AlreadyIndented {
					pw.AlreadyIndented = true
					x := pw.Pdf.GetX()
					x += pw.currentParagraphFormat.TextIndent
					pw.Pdf.SetX(x)
				}*/
				pw.Pdf.Write(pw.LineHeight, char)
				runeIndex++
				if pw.currentParagraphFormat.Alignment == "J" {
					x := pw.Pdf.GetX()
					if char == " " {
						x += wordFudgeFac
					} else {
						x += glyphFudgeFac
					}
					pw.Pdf.SetX(x)
				}

			}
			if floatright {
				pw.Pdf.SetX(vetusX)
				vetusX = 0
			}
		}
		pw.AlreadyIndented = false
		pw.Pdf.SetX(columnX + offsetleft)
	}
	pw.LinesRemaining--
	pw.Pdf.Ln(-1)
	linesWritten++
	if pw.currentParagraphFormat.BreakAfter {
		_, curY := pw.Pdf.GetXY()
		_, maxY := pw.Pdf.GetPageSize()
		acceptableThreshold := pw.BottomMargin + (linesBeforeTriggeredPageBreak * pw.LineHeight)
		if maxY-curY <= acceptableThreshold {
			pw.Pdf.SetAutoPageBreak(true, acceptableThreshold) //TODO: reset this
		}
	}
}

var recalculateSpan bool

// used in the case of page/column break for monolingual text.
var ignoreIndent bool

func (pw *Writer) StackWriter() {
	//concepts: cellstack expand to wordstack+formatstack
	pw.currentParagraphFormat = pw.GetFormat("body", "", new(Format))
	// row index to first page feauring them
	pageMap := make(map[int]int)
	index := 0
	pw.Pdf.SetAcceptPageBreakFunc(func() bool {
		if pw.MultiLanguage {
			pageMap[index] = pw.Pdf.PageCount()
			return true
		}
		//see if we are ready for second column
		return true
	})
	for ; index < len(pw.Rows); index++ {
		row := pw.Rows[index]
		if len(row.Cells) == 0 {
			continue
		}
		for _, cell := range row.Cells {
			if cell.ParaCell == nil {
				//TODO: handle different rowtypes
				continue
			}
			//TODO: body fallback
			paraFormat := pw.GetFormat("p", cell.ParaCell.PClass, new(Format))
			wordStack, formatStack, controlStack := pw.prepareCell(cell)
			//for now we just print them
			//in the future we might add getHeight functions and pageBreak sensing functions
			remainingSpace := pw.ColumnWidth - pw.ColumnWidthAdjustment
			space := " "
			spaceWidth := pw.Pdf.GetStringWidth(space)
			prospectiveLine := "" //for centering or justification
			x := pw.Pdf.GetX()
			x += pw.currentParagraphFormat.TextIndent
			pw.Pdf.SetX(x)
			remainingSpace -= pw.currentParagraphFormat.TextIndent
			for _, word := range wordStack {
				cmd := controlStack[0]
				controlStack = controlStack[1:]
				switch cmd {
				case nop:
				case exitFmt:
					pw.SetFormat(paraFormat)
				default:
					if len(formatStack) == 0 {
						break
					}
					format := formatStack[0]
					formatStack = formatStack[1:]
					pw.SetFormat(format)
					spaceWidth = pw.Pdf.GetStringWidth(space)
				}
				switch pw.currentParagraphFormat.Alignment {
				case "C":
					x := pw.Pdf.GetX()
					x += pw.currentParagraphFormat.TextIndent
					pw.Pdf.SetX(x)
					remainingSpace -= pw.currentParagraphFormat.TextIndent
					// see if this word fits on the line
					wordWidth := pw.Pdf.GetStringWidth(word)
					if wordWidth > remainingSpace {
						//well... is it wider than the columnWidth?
						if wordWidth > pw.ColumnWidth-pw.ColumnWidthAdjustment {
							exploded := []rune(word)
							for _, r := range exploded {
								runeWidth := pw.Pdf.GetStringWidth(string(r))
								if remainingSpace+runeWidth >= pw.ColumnWidth-pw.ColumnWidthAdjustment {
									pw.Pdf.Write(pw.LineHeight, prospectiveLine)
									pw.Pdf.Ln(pw.LineHeight)
									remainingSpace = pw.ColumnWidth - pw.ColumnWidthAdjustment
									prospectiveLine = ""
								}
								prospectiveLine += string(r)
							}
							continue //next word
						}
						x = pw.Pdf.GetX()
						x += remainingSpace / 2
						pw.Pdf.SetX(x)
						pw.Pdf.Write(pw.LineHeight, prospectiveLine)
						pw.Pdf.Ln(pw.LineHeight)
						remainingSpace = pw.ColumnWidth - pw.ColumnWidthAdjustment
					}
					if prospectiveLine != "" {
						prospectiveLine += space
						remainingSpace -= spaceWidth
					}
					prospectiveLine += word
					remainingSpace -= wordWidth

				default:
					// see if this word fits on the line
					wordWidth := pw.Pdf.GetStringWidth(word)
					if wordWidth > remainingSpace {
						//well... is it wider than the columnWidth?
						if wordWidth > pw.ColumnWidth-pw.ColumnWidthAdjustment {
							exploded := []rune(word)
							for _, r := range exploded {
								runeWidth := pw.Pdf.GetStringWidth(string(r))
								if remainingSpace+runeWidth >= pw.ColumnWidth-pw.ColumnWidthAdjustment {
									pw.Pdf.Ln(pw.LineHeight)
									remainingSpace = pw.ColumnWidth - pw.ColumnWidthAdjustment
								}
								pw.Pdf.Write(pw.LineHeight, string(r))
							}
							continue //next word
						}
						pw.Pdf.Ln(pw.LineHeight)
						remainingSpace = pw.ColumnWidth - pw.ColumnWidthAdjustment
					}
					pw.Pdf.Write(pw.LineHeight, word+space)
					remainingSpace -= wordWidth + spaceWidth
				}
			}
			//paragraph break
			pw.Pdf.Ln(pw.LineHeight)
		}
	}
}

type FmtCmd byte

const (
	nop = FmtCmd(iota)
	enterFmt
	exitFmt = 255
)

func (pw *Writer) prepareCell(cell *atempl.Cell) ([]string, []*Format, []FmtCmd) {
	//assume 16 words per span
	words := make([]string, 0, len(cell.ParaCell.Spans)*16)
	spanStack := make([]*atempl.Span, 0, len(cell.ParaCell.Spans)*2)
	fmts := make([]*Format, 0, len(cell.ParaCell.Spans))
	cmds := make([]FmtCmd, 1, len(cell.ParaCell.Spans)*32) //twice initial style of words
	cmdI := 0
	if cell.ParaCell == nil {
		return nil, nil, nil
	}
	spanStack = append(spanStack, cell.ParaCell.Spans...)
	for len(spanStack) > 0 {
		span := spanStack[len(spanStack)-1]
		spanStack = spanStack[:len(spanStack)-1]
		if pw.MetaPdfData.CSS.ContainsRule("span." + span.Class) {
			//TODO anchor support?
			if cmds[cmdI] == enterFmt {
				fmts[len(fmts)-1] = pw.GetFormat("span", span.Class, fmts[len(fmts)-1])
			} else {
				fmts = append(fmts, pw.GetFormat("span", span.Class, pw.GetFormat("p", cell.ParaCell.PClass, new(Format))))
			}
			cmds[cmdI] = enterFmt
		}
		if span.ChildSpans != nil && len(span.ChildSpans) > 0 {
			// we have to put them on backwards
			remaining := len(span.ChildSpans) - 1
			for remaining >= 0 {
				spanStack = append(spanStack, span.ChildSpans[remaining])
				remaining--
			}
			continue
		}
		if span.Value != nil {
			//words over 16 chars tend to be rare
			runes := make([]rune, 0, 16)
			for _, r := range []rune(*span.Value) {
				//Chinese, Japanese, Korean, and any Pictographs don;'t care about line-breaks
				if unicode.Is(unicode.Han, r) ||
					unicode.Is(unicode.Hiragana, r) ||
					unicode.Is(unicode.Katakana, r) ||
					unicode.Is(unicode.Hangul, r) ||
					(r > 0x1f000 && r < 0x1FFFF) {
					//finish up
					words = append(words, string(runes))
					cmds = append(cmds, nop)
					words = append(words, string(r))
					cmds = append(cmds, nop)
					runes = make([]rune, 0, 16)
					continue
				}
				if unicode.IsSpace(r) {
					words = append(words, string(runes))
					cmds = append(cmds, nop)
					runes = make([]rune, 0, 16)
					continue
				}
				runes = append(runes, r)
			}
			words = append(words, string(runes))
			cmds = append(cmds, exitFmt)
		}
	}
	return words, fmts, cmds
}

// WritePDF generates PDF files using pkg/fpdf
func (pw *Writer) WritePDF() {
	var elemCC paged.ElementHierarchy //Used in Constraint Calculation
	var offsetleft, offsetright float64
	var offsettop float64
	var glyphFudgeFac, wordFudgeFac float64
	bodyCC := new(paged.Element)
	bodyCC.Type = "body"
	elemCC.Initialize(bodyCC, pw.MetaPdfData.CSS)
	bodyCC.ComputeConstraints()
	headerCC := new(paged.Element)
	headerCC.Type = "p"
	headerCC.Class = "header"
	sourceCC := new(paged.Element)
	sourceCC.Type = "span"
	sourceCC.Class = "versiondesignation"
	elemCC.AddChild(sourceCC)
	//press := paged.LegacyPdfPrintPress(pw)
	debugIndent := false // if true, the indent space proxy character will be printed.
	debugSpaces := false // if true, spaces will be replaced with underscores
	debugMargins := false
	offsettop = 0
	rgbBlack := RGB{0, 0, 0} //TODO: this is black
	//y0 := pw.TopMargin - pw.CellMargin
	pw.SetHeaderSections()
	pw.SetFooterSections()

	setCol := func(col int) {
		//startX := pw.ColumnX[pw.CurrentColumn]
		pw.CurrentColumn = col
		x := pw.Pdf.GetX()
		xfac := pw.ColumnX[pw.CurrentColumn] - x //startX
		pw.Pdf.SetLeftMargin(pw.ColumnX[pw.CurrentColumn])
		pw.Pdf.SetX(x + xfac + offsetleft)
		/*if !pw.AlreadyIndented {
			x := pw.Pdf.GetX()
			x += pw.currentParagraphFormat.TextIndent
			pw.Pdf.SetX(x)
			//pw.AlreadyIndented = true
		}*/
		pw.AlreadyIndented = true
	}

	// Note: if monolingual, we output two columns with same language,
	//          the text at the bottom of the left column continuing
	//          at the top of the right column.
	//          The text at the bottom of the right column continues on
	//          the top of the right column of the next page.
	//       if multi-language, we have a column for each language,
	//          and the text at the bottom of each column continues
	//          at the top of its column on the next page.
	//       Returning true results in a new page, false stays on same page.
	pw.Pdf.SetAcceptPageBreakFunc(acceptBreak(pw, setCol, &offsetleft, &offsettop))
	header := func() {
		if pw.Pdf.PageCount() == 1 && pw.HasHeaderFirst { // no header on first page
			//TODO: implement
			pw.HeaderLine = false
			return
		} else {
			pw.HeaderLine = config.SM.BoolProps[properties.SiteBuildPdfPageHeaderLineOn]
			if pw.Pdf.PageCount() == 2 {
				elemCC.AddChild(headerCC)
				headerCC.ComputeConstraints()
				lh, err := headerCC.GetLengthVal("line-height")
				if err != nil {
					lh = pw.LineHeight
				}
				pw.HeaderLineConfig.Y1 = pw.TopMargin + lh
				pw.HeaderLineConfig.Y2 = pw.TopMargin + lh
				lenV, err := headerCC.GetLengthVal("margin-bottom")
				if err == nil {
					pw.HeaderLineConfig.Y1 += headerCC.Constraints.Bottom - lenV
					pw.HeaderLineConfig.Y2 += headerCC.Constraints.Bottom - lenV
				}

				//update the column lines so they don't intersect
				for i, _ := range pw.ColumnLineConfig {
					pw.ColumnLineConfig[i].Y1 = pw.HeaderLineConfig.Y1 + fonts.PointsToMillimeters(6) //TODO: user config
				}
			}

			pw.WriteHeader()
			pw.Pdf.SetY(pw.HeaderLineConfig.Y1)
			// Line break
			offset, err := headerCC.GetLengthVal("margin-bottom")
			if err == nil {
				pw.Pdf.Ln(offset)
			} else {
				//TODO: log error
				pw.Pdf.Ln(pw.LineHeight / 2)
			}
			// Save ordinate
			//y0 = pw.TopMargin + pw.CellMargin
			//y0 = pw.Pdf.GetY()
			pw.Pdf.SetTextColor(0, 0, 0) // black
			//pw.Pdf.Ln(pw.LineHeight / 2)
			if pw.currentParagraphFormat == nil {
				pw.currentParagraphFormat = pw.GetFormat("body", "", nil)
			}
			b := new(Border)
			b.startX = pw.LeftMargin
			b.startY = pw.TopMargin
			b.endX = pw.PageWidth - pw.RightMargin
			b.endY = pw.PageHeight - pw.BottomMargin
			b.BorderTop.Style = pw.BodyStyle.BorderTop.Style
			b.BorderTop.Color = pw.BodyStyle.BorderTop.Color
			b.BorderTop.Width = pw.BodyStyle.BorderTop.Width
			b.BorderBottom = pw.BodyStyle.BorderBottom
			b.BorderLeft = pw.BodyStyle.BorderLeft
			b.BorderRight = pw.BodyStyle.BorderRight
			// TODO: investigate
			if b.BorderTop.Style+b.BorderBottom.Style+b.BorderLeft.Style+b.BorderLeft.Style != "" {
				b.draw(pw)
			}
		}
	}

	pw.Pdf.SetHeaderFunc(header)
	pw.Pdf.SetFooterFunc(func() {
		if debugMargins {
			pw.Pdf.Line(pw.LeftMargin, 0, pw.LeftMargin, pw.PageHeight)
			pw.Pdf.Line(pw.PageWidth-pw.RightMargin, 0, pw.PageWidth-pw.RightMargin, pw.PageHeight)
			pw.Pdf.Line(0, pw.TopMargin, pw.PageWidth, pw.TopMargin)
			pw.Pdf.Line(0, pw.PageHeight-pw.BottomMargin, pw.PageWidth, pw.PageHeight-pw.BottomMargin)
		}
		if pw.Pdf.PageNo() == 1 && pw.HasFooterFirst {
			//TODO: implement
		} else {
			pw.Pdf.SetY(pw.FooterY)
			// Position X at center
			pw.Pdf.SetX(pw.CenterX)

			if pw.FooterLine {
				pw.DrawFooterLine()
			}
			if pw.GutterLine {
				pw.DrawColumnLines()
			}
			if pw.MarginLine {
				pw.DrawMarginLines()
			}
			pw.WriteFooter()
		}
	})
	// initial call to header
	header()

	// for each row
	for rowIndex, row := range pw.Rows {
		pw.Pdf.SetX(0) //TODO: is thsi needed
		//offsetleft = 0 //ALSO DEBUG
		elemCC.AddAtemRow(row)
		if row.Type == rowTypes.PageBreak {
			pw.Pdf.AddPage()
		}
		// always recompute the column widths in case the previous iteration changed them
		pw.Columns = pw.ColumnsFromPreferences
		pw.MultiLanguage = pw.OriginalMultiLanguage
		pw.ComputeColumnWidths()
		pw.Pdf.SetAutoPageBreak(true, pw.BottomMargin) //TODO: audit this
		// if version is specific, we will do single column and only used the indicated version
		pw.VersionSpecific = row.Version != versions.All
		//always recompute style
		pw.currentParagraphFormat = pw.GetFormat("body", "", nil)
		pw.LineHeight = pw.BodyStyle.LineHeight
		pw.IndentBy = 0
		pw.FontSize = pw.BodyStyle.FontSize
		if row.Cells == nil {
			continue
		}
		if row.HtmlOnly {
			continue
		}
		// we don't use media cells for pdfs, so skip if row is for media
		if row.Cells[0].MediaCell != nil {
			continue
		}
		// peek ahead to see if we are printing a blank line.
		// if so, write it here outside the row.cells loop so that it happens only once.
		if len(row.Cells) > 1 && row.Cells[0].BlankCell != nil {
			pw.Pdf.Ln(pw.LineHeight)
			continue
		}
		if pw.VersionSpecific {
			pw.Columns = false
			pw.MultiLanguage = false
			pw.ComputeColumnWidths()
		} else if pw.MultiLanguage {
			pw.Columns = true
		}
		if debugMargins {
			if pw.Columns {
				for _, x := range pw.ColumnX {
					pw.Pdf.Line(x, 0, x, pw.PageHeight)
				}
			}
		}

		pw.currentParagraphFormat = pw.GetFormat("p", row.Class, pw.GetFormat("body", "", nil))

		//margin-top
		//pw.Pdf.Ln(pw.currentParagraphFormat.BorderTop.Margin)
		/*if len(elemCC.Root.Children) > 0 {
			currentCC = elemCC.Root.Children[row.ID]
			if currentCC == nil {
				currentCC = paged.Init(currentCC)
			}
			if len(currentCC.Children) > 0 {
				pw.Pdf.Ln(currentCC.Children[0].Constraints.Top)
				currentCC = currentCC.Children[0]
			}
		}*/
		currentEH := new(paged.ElementHierarchy)
		currentEH.Initialize(new(paged.Element), pw.MetaPdfData.CSS) //newpagedElement will be eventually replaced
		currentEH.AddAtemRow(row)

		currentCC := currentEH.Root.Children[0]
		if currentCC.Type == "row" {
			if len(currentCC.Children) > 0 {
				currentCC = currentCC.Children[0]
			}
		}
		columnCC := new(paged.Element)
		columnCC.Fullfill()
		columnCC.Class = fmt.Sprintf("col%dof%d", pw.CurrentColumn, pw.NumberOfVersions)
		columnCC.ComputeConstraints()
		if columnCC.Constraints.Left == 0.0 {
			columnCC.Constraints.Left = 1
		}
		offsetleft = currentCC.Constraints.Left + columnCC.Constraints.Left
		offsetright = currentCC.Constraints.Right + columnCC.Constraints.Right
		//pw.Pdf.SetX(pw.Pdf.GetX() + offsetleft)
		paragraphRed, paragraphGreen, paragraphBlue := pw.currentParagraphFormat.BackgroundColor.Color()
		// set background color in case we need to put a box around the text
		pw.Pdf.SetDrawColor(paragraphRed, paragraphGreen, paragraphBlue)
		pw.Pdf.SetFillColor(paragraphRed, paragraphGreen, paragraphBlue)

		// for break-before and break-after as well as keep-with
		var yMeter float64
		_, pageHeight := pw.Pdf.GetPageSize()
		// Note: if monolingual, we output two columns with same language,
		//          the text at the bottom of the left column continuing
		//          at the top of the right column.
		//          The text at the bottom of the right column continues on
		//          the top of the right column of the next page.
		//       if multi-language, we have a column for each language,
		//          and the text at the bottom of each column continues
		//          at the top of its column on the next page.
		//       The positioning of the x and y coordinates is controlled by
		//         pw.Pdf.SetAcceptPageBreakFunc.
		pw.Pdf.SetY(pw.Pdf.GetY() + currentCC.Constraints.Top)
		if pw.MultiLanguage { // output multiple columns.
			var formattedSpans []*FormattedSpans
			var cellLineCount int
			for i, cell := range row.Cells {
				pw.AlreadyIndented = false
				pw.CurrentColumn = i
				if cell.RuleCell != nil {
					pw.DrawSectionLine(cell.RuleCell.HrClass, pw.ColumnX[i]+pw.ColumnWidth/2, pw.Pdf.GetY())
					continue
				}
				if cell.BlankCell != nil {
					// already handled above
					continue
				}
				if cell.ParaCell != nil {
					pw.AlreadyIndented = false
					if cell.ParaCell.PClass == "alttext" {
						fmt.Sprintf("TODO DELETE ME")
					}
					//pw.AlreadyIndented = false
					if paraRule, exists := pw.MetaPdfData.CSS.GetRule("p." + cell.ParaCell.PClass); exists {
						ff := paraRule.GetDeclaration("font-family")
						fst := paraRule.GetDeclaration("font-style")
						fsz := paraRule.GetDeclaration("font-size")
						style := ""
						size := pw.currentParagraphFormat.FontSize
						if fst != nil {
							style = strings.ToUpper(fst.Value[0:1])
						}
						if fsz != nil {
							size = fsz.Float
						}
						if ff != nil {
							pw.Pdf.SetFont(ff.Value, style, size)
						} else {
							pw.Pdf.SetFontSize(size)
						}
					} else {
						pw.Pdf.SetFont(cell.ParaCell.Language, "", pw.FontSize)
						pw.currentFont = pw.FontMap[cell.ParaCell.Language]
						pw.currentParagraphFormat.FontName = cell.ParaCell.Language
					}
					oldPWAC := pw.ColumnWidthAdjustment
					pw.ColumnWidthAdjustment += offsetleft
					pw.ColumnWidthAdjustment += offsetright
					formattedSpans = append(formattedSpans, pw.GetFormattedSpans(cell.ParaCell.Spans, pw.currentParagraphFormat))
					pw.ColumnWidthAdjustment = oldPWAC

					if pw.currentParagraphFormat.BreakBefore {
						_, curY := pw.Pdf.GetXY()
						_, maxY := pw.Pdf.GetPageSize()
						acceptableThreshold := pw.BottomMargin + (linesBeforeTriggeredPageBreak * pw.LineHeight)
						if maxY-curY <= acceptableThreshold {
							pw.Pdf.SetAutoPageBreak(true, acceptableThreshold)
						}
					}

					l := len(formattedSpans[i].LineBreaks)
					if cellLineCount < l {
						cellLineCount = l
					}
				}
			}
			var linesWritten int
			// initial column runeIndexes
			var runeIndex []int
			var formatsWritten []int
			// Normally, in the two for-loops below,
			// we want to iterate over all the versions,
			// thereby creating a column for each.
			// But, if in the LML template, the user
			// invoked setVersion = 1, for example,
			// there will only be one row cell.  So, we need to
			// default the nbrColumns to NumberOfVersions,
			// but if VersionSpecific (because of setVersion)
			// set nbrColumns to just 1.
			var nbrColumns = pw.NumberOfVersions
			if pw.VersionSpecific {
				nbrColumns = 1
			}

			for i := 0; i < nbrColumns; i++ {
				runeIndex = append(runeIndex, 0)
				formatsWritten = append(formatsWritten, 0)
			}
			indentCount := 0
			if !pw.AlreadyIndented {
				indentCount = nbrColumns
			}
			for linesWritten < cellLineCount {
				for colIndex := 0; colIndex < nbrColumns; colIndex++ {

					columnCC.Type = ""
					columnCC.Class = fmt.Sprintf("col%dof%d", colIndex+1, nbrColumns)
					elemCC.AddChild(columnCC)
					columnCC.ComputeConstraints()
					//remove columnCC for memory conservation
					elemCC.Registry = elemCC.Registry[:len(elemCC.Registry)-1]
					if columnCC.Constraints.Left == 0.0 {
						if colIndex > 0 {
							columnCC.Constraints.Left = 1
						}
					}
					offsetleft = currentCC.Constraints.Left + columnCC.Constraints.Left
					offsetright = currentCC.Constraints.Right + columnCC.Constraints.Right
					pw.Pdf.SetX(pw.ColumnX[colIndex] + offsetleft)
					if indentCount > 0 {
						x := pw.Pdf.GetX()
						x += pw.currentParagraphFormat.TextIndent
						pw.Pdf.SetX(x)
						indentCount--
					}
					// track lines remaining to be written so if
					// page break function is called, we know whether
					// everything had been written for that column.
					pw.LinesRemaining = len(formattedSpans[colIndex].LineBreaks)

					// set the font family, and font color to the default value.
					// They can be overridden below if there is a span-level
					// formatting change.
					font := pw.CurrentFormats[colIndex]
					pw.Pdf.SetFont(row.Cells[colIndex].ParaCell.Language, font.Style, font.FontSize)
					pclass := row.Cells[colIndex].ParaCell.PClass
					if paraRule, exists := pw.MetaPdfData.CSS.GetRule("p." + pclass); exists {
						fontFamDec := paraRule.GetDeclaration("font-family")
						fontSizeDec := paraRule.GetDeclaration("font-size")
						fontStyleDec := paraRule.GetDeclaration("font-style")
						fontWeightDec := paraRule.GetDeclaration("font-weight")
						fontSize := font.FontSize
						fontStyle := ""
						fontFam := row.Cells[colIndex].ParaCell.Language
						if fontSizeDec != nil {
							fontSizeDec.NormalizeFloatAndUnit(pw.FontSize)
							fontSize = fontSizeDec.Float
							pw.FontSize = fontSize
						}
						if fontStyleDec != nil {
							if strings.ToLower(fontStyleDec.Value) == "italic" {
								fontStyle += "I"
							}
						}

						if fontWeightDec != nil {
							if strings.ToLower(fontWeightDec.Value) == "bold" {
								fontStyle += "B"
							}
						}

						if fontFamDec != nil {
							fontFam = fontFamDec.Value
						}
						pw.Pdf.SetFont(fontFam, fontStyle, fontSize)
					}
					pw.Pdf.SetTextColor(font.GetColor())
					if paragraphRed != rgbBlack.Red {
						// We will draw a box around each line.
						// First, though, make sure this isn't
						// a string of spaces or indentProxy
						text := strings.TrimSpace(formattedSpans[colIndex].Text)
						text = strings.ReplaceAll(text, indentProxy, "")
						if len(text) > 0 {
							// we will draw a box around each line
							curX, curY := pw.Pdf.GetXY()
							pw.Pdf.SetFillColor(paragraphRed, paragraphGreen, paragraphBlue)
							pw.Pdf.SetDrawColor(paragraphRed, paragraphGreen, paragraphBlue)

							boxWidth := pw.ColumnWidth - 2*pw.CellMargin
							//pw.Pdf.SetX(pw.ColumnX[colIndex] + pw.CellMargin + pw.currentParagraphFormat.BorderLeft.Margin)
							pw.Pdf.CellFormat(boxWidth, pw.LineHeight, "",
								"*", 0, "C", true, 0, "")
							pw.Pdf.SetFillColor(0, 0, 0)
							pw.Pdf.SetXY(curX, curY)
						}
					}
					if len(elemCC.Root.Children) > 0 {
						//currentCC = elemCC.Root.Children[rowIndex]
						if currentCC != nil {
							if currentCC.Type == "row" {
								//for _, child := range currentCC.Children {
								if len(currentCC.Children) > 0 {
									child := currentCC.Children[0]
									if child.Type == "p" {
										child.ComputeConstraints()
									}
								}
							} else if len(currentCC.Children) > 0 {
								//TODO: do we want this to be column specific
								currentCC.ComputeConstraints()
								//pw.Pdf.SetX(pw.ColumnX[colIndex])
							}
							//pw.Pdf.SetX(pw.ColumnX[colIndex] + offsetleft)
						}
					}
					// center title text by indenting with blank spaces
					if pw.currentParagraphFormat.Alignment == "C" {
						if len(formattedSpans[colIndex].Formats) > 0 {
							pw.Pdf.SetFont(formattedSpans[colIndex].Formats[0].FontName, formattedSpans[colIndex].Formats[0].Style, formattedSpans[colIndex].Formats[0].FontSize)
						}
						start := 0
						if linesWritten > 0 && linesWritten < len(formattedSpans[colIndex].LineBreaks)+1 {
							start = formattedSpans[colIndex].LineBreaks[linesWritten-1]
						}
						ending := len(formattedSpans[colIndex].Runes)
						if linesWritten < len(formattedSpans[colIndex].LineBreaks) {
							ending = formattedSpans[colIndex].LineBreaks[linesWritten]
						}
						curLineW := pw.Pdf.GetStringWidth(string(formattedSpans[colIndex].Runes[start:ending]))
						leftSide := pw.PageWidth - pw.RightMargin
						if pw.CurrentColumn < len(pw.ColumnX)-1 {
							if pw.ColumnX[pw.CurrentColumn] < pw.ColumnX[pw.CurrentColumn+1] {
								leftSide = pw.ColumnX[pw.CurrentColumn+1]
							}
						}
						columnWidth := leftSide - pw.ColumnX[pw.CurrentColumn]
						indent := (columnWidth - curLineW) / 2 //TODO: support paragraph's margin
						//this calculates a fudge factor, therefore it is a fudge factory
						//curLineW *= 1.0344  //legacy fudge factor, calculated in the stone age
						pageW, _ := pw.Pdf.GetPageSize()
						mL, _, _, _ := pw.Pdf.GetMargins()
						manuscript := string(formattedSpans[colIndex].Runes[start:ending])
						lifejacket := 3200 //avoid infinite loop
						for pw.Pdf.GetStringWidth(manuscript)+indent+pw.ColumnX[colIndex] > pageW-(mL+indent*0.9)-pw.Pdf.GetStringWidth("_") {
							if indent > 0.1 {
								indent *= 0.9
							}
							if indent < 0.1 {
								indent = 0
							}
							if indent == 0 && lifejacket > 0 {
								fs, _ := pw.Pdf.GetFontSize()
								pw.Pdf.SetFontSize(fs * 0.9)
								lifejacket--
							}
						}

						if debugMargins {
							y := pw.Pdf.GetY()
							pw.Pdf.Line(pw.ColumnX[colIndex], y, pw.ColumnX[colIndex]+indent, y)
						}
						pw.Pdf.SetX(pw.ColumnX[colIndex] + indent)
					}
					glyphFudgeFac = 0.0
					wordFudgeFac = 0.0
					if pw.currentParagraphFormat.Alignment == "J" && linesWritten < len(formattedSpans[colIndex].LineBreaks)-1 {
						if len(formattedSpans[colIndex].Formats) > 0 {
							pw.Pdf.SetFont(formattedSpans[colIndex].Formats[0].FontName, formattedSpans[colIndex].Formats[0].Style, formattedSpans[colIndex].Formats[0].FontSize)
						}
						start := 0
						if linesWritten > 0 && linesWritten < len(formattedSpans[colIndex].LineBreaks)+1 {
							start = formattedSpans[colIndex].LineBreaks[linesWritten-1]
						}
						ending := len(formattedSpans[colIndex].Runes)
						if linesWritten < len(formattedSpans[colIndex].LineBreaks) {
							ending = formattedSpans[colIndex].LineBreaks[linesWritten]
						}
						thisLine := formattedSpans[colIndex].Runes[start:ending]
						curLineW := pw.Pdf.GetStringWidth(string(thisLine))
						leftSide := pw.PageWidth - pw.RightMargin
						if pw.CurrentColumn < len(pw.ColumnX)-1 {
							if pw.ColumnX[pw.CurrentColumn] < pw.ColumnX[pw.CurrentColumn+1] {
								leftSide = pw.ColumnX[pw.CurrentColumn+1]
							}
						}
						columnWidth := leftSide - pw.ColumnX[pw.CurrentColumn]
						if start == 0 {
							// avoid indent overlap
							columnWidth -= pw.currentParagraphFormat.TextIndent
						}
						curLineW += currentCC.Constraints.Left
						curLineW += currentCC.Constraints.Right
						gap := columnWidth - curLineW
						numChars := ending - start
						//get the number of spaces
						numSpaces := 0
						for _, r := range thisLine {
							//TODO: do chinese characters get treated special?
							if r == ' ' {
								numSpaces++
								numChars--
							}
						}
						wordFudgeFac = gap * justifyWordGlyphRatio / float64(numSpaces)
						if wordFudgeFac > pw.SpacerWidthMillimeters*justifyMaxFactorWord {
							wordFudgeFac = justifyMaxFactorWord
						}
						solved := wordFudgeFac * float64(numSpaces)
						//glyphFudgeFac = gap*(1-justifyWordGlyphRatio) - solved/float64(numChars)
						leftOver := gap - solved
						glyphFudgeFac = leftOver / (float64(numChars))
						if glyphFudgeFac > pw.SpacerWidthMillimeters*justifyMaxFactorGlyph {
							glyphFudgeFac = justifyMaxFactorGlyph
						}
					}
					var vetusX float64
					var floatright bool
					formattedSpans[colIndex] = CleanSpaces(formattedSpans[colIndex])
					if linesWritten < len(formattedSpans[colIndex].LineBreaks) {
						for runeIndex[colIndex] < formattedSpans[colIndex].LineBreaks[linesWritten] {
							formatBreaks := formattedSpans[colIndex].FormatBreaks
							if formatsWritten[colIndex] < len(formattedSpans[colIndex].FormatBreaks) {
								currentFormatBreak := formatBreaks[formatsWritten[colIndex]]
								if runeIndex[colIndex] == currentFormatBreak {
									format := formattedSpans[colIndex].Formats[formatsWritten[colIndex]]
									//if len(formattedSpans) > colIndex && len(formatsWritten) > colIndex {
									//if len(fsf) > fsi {
									pw.Pdf.SetFont(format.FontName, format.Style, format.FontSize)
									if format.Float == "right" {
										vetusX = pw.Pdf.GetX()
										spans := formattedSpans[colIndex].Spans
										var str string
										for _, span := range spans {
											if span.Format.Float == "right" {
												str = span.SubString
												break
											}
										}
										elephant := pw.Pdf.GetStringWidth(str)
										fudge := 4.0 //* pw.Pdf.GetStringWidth("_")
										rightFac, err := sourceCC.GetLengthVal("right")
										elephant *= 1.2
										if err == nil {
											fudge += rightFac
										}
										novusX := pw.ColumnX[colIndex] + (pw.ColumnWidth - elephant) - fudge
										if novusX > vetusX {
											floatright = true
											pw.Pdf.SetX(novusX)
										}
									}
									if format.Float != "right" && vetusX != 0 {
										pw.Pdf.SetX(vetusX)
										vetusX = 0
									}
									//}
									//}
									pw.Pdf.SetTextColor(format.GetColor())
									pw.CurrentFormats[colIndex] = format.Copy()
									formatsWritten[colIndex]++
								}
							}
							char := string(formattedSpans[colIndex].Runes[runeIndex[colIndex]])
							// if we are starting a new line and the first character is a space,
							// make it an empty string.
							if pw.Pdf.GetX() == pw.ColumnX[colIndex]+offsetleft {
								if char == " " {
									char = ""
								}
							}

							if legacyIndent {
								if char == indentProxy && !debugIndent {
									char = " "
								}
								if char == " " && debugSpaces {
									char = "_" //DEBUG
								}
								if pw.currentParagraphFormat.Alignment == "C" {
									if debugMargins {
										x, y := pw.Pdf.GetXY()
										pw.Pdf.Line(x, y, x+1, y-1)
										pw.Pdf.Line(x, y, x+1, y+1)
									}
									pw.Pdf.SetCellMargin(0)
								}
								/*if !pw.AlreadyIndented {
									pw.AlreadyIndented = true
									x := pw.Pdf.GetX()
									x += pw.currentParagraphFormat.TextIndent
									pw.Pdf.SetX(x)
								}*/
								pw.Pdf.Write(pw.LineHeight, char)
							} else {
								x := pw.Pdf.GetX()
								x += formattedSpans[colIndex].Spans[0].Format.TextIndent //TODO: get this from css
								pw.Pdf.SetX(x)
							}
							runeIndex[colIndex]++
							if pw.currentParagraphFormat.Alignment == "J" {
								x := pw.Pdf.GetX()
								if char == " " {
									x += wordFudgeFac
								} else {
									x += glyphFudgeFac
								}
								pw.Pdf.SetX(x)
							}

						}
						if floatright {
							pw.Pdf.SetX(vetusX)
							vetusX = 0
						}
					}
					pw.AlreadyIndented = false
					pw.Pdf.SetX(pw.ColumnX[colIndex] + offsetleft)
				}
				//check if we are at end of page
				y := pw.Pdf.GetY()
				if y+pw.LineHeight >= pw.PageHeight {
					//trigger the break now
					pw.Pdf.Ln(pw.LineHeight + 1)
				}
				pw.LinesRemaining--
				pw.Pdf.Ln(-1)
				linesWritten++
				if pw.currentParagraphFormat.BreakAfter {
					_, curY := pw.Pdf.GetXY()
					_, maxY := pw.Pdf.GetPageSize()
					acceptableThreshold := pw.BottomMargin + (linesBeforeTriggeredPageBreak * pw.LineHeight)
					if maxY-curY <= acceptableThreshold {
						pw.Pdf.SetAutoPageBreak(true, acceptableThreshold) //TODO: reset this
					}
				}
			}
		} else { // do double column for monolingual text
			leadingSpaceMode := true
			pw.Pdf.SetCellMargin(0)
			for i, cell := range row.Cells {
				if rowIndex > 0 {
					pw.Pdf.Ln(-1)
				}
				if cell.RuleCell != nil {
					pw.DrawSectionLine(cell.RuleCell.HrClass, pw.ColumnX[i]+pw.ColumnWidth/2, pw.Pdf.GetY())
					continue
				}
				if cell.BlankCell != nil {
					// already handled above for loop
					continue
				}
				if cell.MediaCell != nil { // ignore
					continue
				}

				if cell.ParaCell != nil {
					pw.AlreadyIndented = false
					fontSize := pw.FontSize
					if pw.currentParagraphFormat.FontSize > 0 {
						fontSize = pw.currentParagraphFormat.FontSize
					}
					pw.Pdf.SetFont(cell.ParaCell.Language, "", fontSize)

					pw.currentFont = pw.FontMap[cell.ParaCell.Language]
					pw.FontSize = pw.currentParagraphFormat.FontSize

					if paraRule, exists := pw.MetaPdfData.CSS.GetRule("p." + cell.ParaCell.PClass); exists {
						ff := paraRule.GetDeclaration("font-family")
						fst := paraRule.GetDeclaration("font-style")
						fsz := paraRule.GetDeclaration("font-size")
						style := ""
						size := pw.currentParagraphFormat.FontSize
						if fst != nil {
							style = strings.ToUpper(fst.Value[0:1])
						}
						if fsz != nil {
							size = fsz.Float
						}
						if ff != nil {
							pw.Pdf.SetFont(ff.Value, style, size)
						} else {
							pw.Pdf.SetFontSize(size)
						}
					}

					oldPWCA := pw.ColumnWidthAdjustment
					pw.ColumnWidthAdjustment += offsetleft  //currentCC.Constraints.Left
					pw.ColumnWidthAdjustment += offsetright //currentCC.Constraints.Right
					formattedSpans := /*CleanSpaces*/ (pw.GetFormattedSpans(cell.ParaCell.Spans, pw.currentParagraphFormat))
					//formattedSpans := pw.GetFormattedSpans(cell.ParaCell.Spans, pw.currentParagraphFormat)
					pw.ColumnWidthAdjustment = oldPWCA
					if pw.currentParagraphFormat.BreakBefore {
						_, curY := pw.Pdf.GetXY()
						_, maxY := pw.Pdf.GetPageSize()
						acceptableThreshold := pw.BottomMargin + (linesBeforeTriggeredPageBreak * pw.LineHeight)
						if maxY-curY <= acceptableThreshold {
							pw.Pdf.SetAutoPageBreak(true, acceptableThreshold)
						}
					}
					linesWritten := 0
					pBorder := new(Border)
					pBorder.start(pw.Pdf)
					if paragraphRed != rgbBlack.Red {
						text := strings.TrimSpace(formattedSpans.Text)
						text = strings.ReplaceAll(text, indentProxy, "")
						if len(text) > 0 {
							// we will draw a box around each line
							curX, curY := pw.Pdf.GetXY()
							pw.Pdf.SetFillColor(paragraphRed, paragraphGreen, paragraphBlue)
							pw.Pdf.SetDrawColor(paragraphRed, paragraphGreen, paragraphBlue)
							boxWidth := pw.ColumnWidth - pw.CellMargin
							l := len(formattedSpans.LineBreaks)
							for j := 0; j < l; j++ {
								pw.Pdf.CellFormat(boxWidth, pw.LineHeight, "",
									"*", 0, "C", true, 0, "")
								pw.Pdf.Ln(-1)
							}
							pw.Pdf.SetXY(curX, curY)
						}
					}

					pw.Pdf.SetX(pw.ColumnX[pw.CurrentColumn] + offsetleft)
					// if cell is centered, position x for writing 1st character
					//from := 0
					/*if pw.currentParagraphFormat.Alignment == "C" &&
						formattedSpans.LineBreaks != nil {
						to := formattedSpans.LineBreaks[0]
						if to == 0 || to > len(formattedSpans.Runes) {
							to = len(formattedSpans.Runes) - 1
						}
						lenText := formattedSpans.CalculateLineWidth(pw.Pdf, from, to) //pw.Pdf.GetStringWidth(textToCenter)
						totalGap := pw.ColumnWidth - lenText
						totalGap -= pw.CellMargin * 2
						indentBy := totalGap / 2
						leftSide := pw.PageWidth - pw.LeftMargin
						if pw.NumberOfMonolingualColumns == 2 {
							if pw.CurrentColumn == 0 {
								leftSide = pw.ColumnX[1]
							}
						}
						remainingGap := leftSide - indentBy - (pw.ColumnX[pw.CurrentColumn] + indentBy + lenText)
						if remainingGap != 0 {
							indentBy += remainingGap / 2
						}

						//if len(formattedSpans.LineBreaks) == 1 {
						pw.Pdf.SetX(pw.ColumnX[pw.CurrentColumn] + indentBy)
						//}
						pw.IndentBy = indentBy
					}*/

					if pw.currentParagraphFormat.Alignment == "J" {
						pw.currentParagraphFormat.Alignment = "L"
					}
					if pw.currentParagraphFormat.Alignment == "R" &&
						formattedSpans.LineBreaks != nil {
						to := formattedSpans.LineBreaks[0]
						if to == 0 || to > len(formattedSpans.Runes) {
							to = len(formattedSpans.Runes) - 1
						}
						lenText := formattedSpans.CalculateLineWidth(pw.Pdf, 0, to) //pw.Pdf.GetStringWidth(textToCenter)
						totalGap := pw.ColumnWidth - lenText
						totalGap -= pw.CellMargin
						indentBy := totalGap
						pw.Pdf.SetX(pw.Pdf.GetX() + indentBy)
						pw.IndentBy = indentBy
					}
					//overwrites previous
					if pw.currentParagraphFormat.Alignment == "C" {
						pw.Pdf.SetFont(formattedSpans.Formats[0].FontName, formattedSpans.Formats[0].Style, fontSize)
						//pw.Pdf.Ln(0)
						endIndex := len(formattedSpans.Runes)
						if len(formattedSpans.LineBreaks) > 1 {
							endIndex = formattedSpans.LineBreaks[0]
						}
						firstLineW := pw.Pdf.GetStringWidth(string(formattedSpans.Runes[0:endIndex]))
						widest := firstLineW
						curLineW := firstLineW
						if len(formattedSpans.LineBreaks) > 1 {
							endIndex = formattedSpans.LineBreaks[0]
							prevP := 0
							for _, pos := range formattedSpans.LineBreaks {
								curLineW = pw.Pdf.GetStringWidth(string(formattedSpans.Runes[prevP:pos]))
								if curLineW > widest {
									widest = curLineW
								}
								prevP = pos
							}
						}
						indent := (widest - firstLineW) / 2
						baseIndent := (pw.ColumnWidth - widest) / 2
						backupCenterX = pw.Pdf.GetX() + baseIndent + indent
						pw.Pdf.SetX(backupCenterX)
					} else {
						backupLeftX = pw.Pdf.GetX() + currentCC.Constraints.Left + columnCC.Constraints.Left
						if !pw.AlreadyIndented {
							backupLeftX += pw.currentParagraphFormat.TextIndent
						}
					}
					if pw.currentParagraphFormat.Alignment == "J" && linesWritten < len(formattedSpans.LineBreaks)-1 {
						pw.Pdf.SetFont(formattedSpans.Formats[0].FontName, formattedSpans.Formats[0].Style, fontSize)
						//pw.Pdf.Ln(0)
						endIndex := len(formattedSpans.Runes)
						if len(formattedSpans.LineBreaks) > 1 {
							endIndex = formattedSpans.LineBreaks[0]
						}
						firstLineW := pw.Pdf.GetStringWidth(strings.TrimSpace(string(formattedSpans.Runes[0:endIndex])))
						widest := firstLineW
						curLineW := firstLineW
						if len(formattedSpans.LineBreaks) > 1 {
							endIndex = formattedSpans.LineBreaks[0]
							prevP := 0
							for _, pos := range formattedSpans.LineBreaks {
								curLineW = pw.Pdf.GetStringWidth(strings.TrimSpace(string(formattedSpans.Runes[prevP:pos])))
								if curLineW > widest {
									widest = curLineW
								}
								prevP = pos
							}
						}
						curLineW += currentCC.Constraints.Left
						curLineW += currentCC.Constraints.Right
						gap := pw.ColumnWidth - curLineW
						numChars := formattedSpans.LineBreaks[0]
						//get the number of spaces
						numSpaces := 0
						for _, r := range formattedSpans.Runes[0:formattedSpans.LineBreaks[0]] {
							//TODO: do chinese characters get treated special?
							if r == ' ' {
								numSpaces++
								numChars--
							}
						}
						wordFudgeFac = gap * justifyWordGlyphRatio / float64(numSpaces)
						if wordFudgeFac > pw.SpacerWidthMillimeters*justifyMaxFactorWord {
							wordFudgeFac = justifyMaxFactorWord
						}
						solved := wordFudgeFac * float64(numSpaces)
						//glyphFudgeFac = gap*(1-justifyWordGlyphRatio) - solved/float64(numChars)
						leftOver := gap - solved
						glyphFudgeFac = leftOver / (float64(numChars))
						if glyphFudgeFac > pw.SpacerWidthMillimeters*justifyMaxFactorGlyph {
							glyphFudgeFac = justifyMaxFactorGlyph
						}
					}
					// write each span
					if legacyIndent {
						//pw.Pdf.SetX(pw.Pdf.GetX() + offsetleft)
						//pw.Pdf.SetX(pw.ColumnX[pw.CurrentColumn] + offsetleft)
						if currentCC.Class == "actor" {
							//pw.Pdf.SetX(0) //TODO: DEBUG
						}
						for _, span := range formattedSpans.Spans {
							if len(span.Format.FontName) == 0 {
								//	pw.Pdf.SetFont(cell.ParaCell.Language, span.Format.Style, span.Format.FontSize)
							} else {
								pw.Pdf.SetFont(span.Format.FontName, span.Format.Style, span.Format.FontSize)
							}
							pw.Pdf.SetTextColor(span.Format.GetColor())
							if span.OriginalSpan.Anchor != nil {
								//TODO: handle runes
								// HREF URL should start with https:// else it will be relative
								// HOWEVER: some url keys in db are sometimes stored without https:// therefore we need to
								// supply it automatically if it matches a TLD
								// like .-\.com/* but instead of canonical TLDs, do .-\....?/*
								// the final string appears to be
								// ^[^\/]*\.[^\/]*
								urlFilter := `^[^\/]*\.[^\/]*`
								match, _ := regexp.MatchString(urlFilter, span.OriginalSpan.Anchor.HREF.Url)
								if match {
									pw.Pdf.WriteLinkString(pw.LineHeight, *span.OriginalSpan.Anchor.Label.Value, fmt.Sprintf("https://%s", span.OriginalSpan.Anchor.HREF.Url))
									continue
								}
								pw.Pdf.WriteLinkString(pw.LineHeight, *span.OriginalSpan.Anchor.Label.Value, span.OriginalSpan.Anchor.HREF.Url)
								continue
							}
							/* calculate the widest line in case of centering */
							widest := 0.0
							curLineW := 0.0
							if len(formattedSpans.LineBreaks) > 1 {
								prevP := 0
								for _, pos := range formattedSpans.LineBreaks {
									curLineW = pw.Pdf.GetStringWidth(string(formattedSpans.Runes[prevP:pos]))
									if curLineW > widest {
										widest = curLineW
									}
									prevP = pos
								}
							}
							var vetusX float64
							if span.Format.Float == "right" {
								vetusX = pw.Pdf.GetX()
								elephant := pw.Pdf.GetStringWidth(*span.OriginalSpan.Value)
								fudge := 4.0
								rightFac, err := sourceCC.GetLengthVal("right")
								if err == nil {
									fudge += rightFac
								}
								novusX := (pw.ColumnX[pw.CurrentColumn] + (pw.ColumnWidth - elephant) - fudge)
								if novusX <= vetusX {
									pw.Pdf.SetY(pw.Pdf.GetY() + pw.LineHeight)
								}
								pw.Pdf.SetX(novusX)
							}
							//lineStart := span.RuneFrom
							for k := span.RuneFrom; k < span.RuneTo; k++ {
								if pw.Pdf.GetY()+pw.LineHeight > pw.PageHeight-pw.BottomMargin {
									// Body break needed
									recalibrateCentering = true
									//lineStart = k // Reset line start to current position
								}
								if k >= formattedSpans.LineBreaks[linesWritten] {
									if span.Format.Float != "right" {
										// if the span floats, the line break is already handled
										pw.Pdf.Ln(pw.LineHeight)
										//update backup X for page/column break
										if pw.currentParagraphFormat.Alignment == "L" {
											backupLeftX = pw.Pdf.GetX() + currentCC.Constraints.Left + columnCC.Constraints.Left
										}
										leadingSpaceMode = true

									}
									if linesWritten < len(formattedSpans.LineBreaks)-1 {
										linesWritten++
									}
									if pw.currentParagraphFormat.Alignment == "C" {
										if linesWritten > 0 && len(formattedSpans.LineBreaks) > linesWritten {
											curLineW = pw.Pdf.GetStringWidth(string(formattedSpans.Runes[formattedSpans.LineBreaks[linesWritten-1]:formattedSpans.LineBreaks[linesWritten]]))
										}
										indent := (widest - curLineW) / 2
										baseIndent := (pw.ColumnWidth - widest) / 2
										pw.Pdf.SetX(pw.ColumnX[pw.CurrentColumn] + baseIndent + indent)
									}

									glyphFudgeFac = 0.0
									wordFudgeFac = 0.0
									if pw.currentParagraphFormat.Alignment == "J" && linesWritten < len(formattedSpans.LineBreaks)-1 {
										if len(formattedSpans.Formats) > 0 {
											pw.Pdf.SetFont(formattedSpans.Formats[0].FontName, formattedSpans.Formats[0].Style, formattedSpans.Formats[0].FontSize)
										}
										start := 0
										if linesWritten > 0 && linesWritten < len(formattedSpans.LineBreaks)+1 {
											start = formattedSpans.LineBreaks[linesWritten-1]
										}
										ending := len(formattedSpans.Runes)
										if linesWritten < len(formattedSpans.LineBreaks) {
											ending = formattedSpans.LineBreaks[linesWritten]
										}
										thisLine := formattedSpans.Runes[start:ending]
										curLineW := pw.Pdf.GetStringWidth(string(thisLine))
										leftSide := pw.PageWidth - pw.RightMargin
										if pw.CurrentColumn < len(pw.ColumnX)-1 {
											if pw.ColumnX[pw.CurrentColumn] < pw.ColumnX[pw.CurrentColumn+1] {
												leftSide = pw.ColumnX[pw.CurrentColumn+1]
											}
										}
										columnWidth := leftSide - pw.ColumnX[pw.CurrentColumn]
										if start == 0 {
											// avoid indent overlap
											columnWidth -= pw.currentParagraphFormat.TextIndent
										}
										curLineW += currentCC.Constraints.Left
										curLineW += currentCC.Constraints.Right
										gap := columnWidth - curLineW
										numChars := ending - start
										//get the number of spaces
										numSpaces := 0
										for _, r := range thisLine {
											//TODO: do chinese characters get treated special?
											if r == ' ' {
												numSpaces++
												numChars--
											}
										}
										wordFudgeFac = gap * justifyWordGlyphRatio / float64(numSpaces)
										if wordFudgeFac > pw.SpacerWidthMillimeters*justifyMaxFactorWord {
											wordFudgeFac = justifyMaxFactorWord
										}
										solved := wordFudgeFac * float64(numSpaces)
										//glyphFudgeFac = gap*(1-justifyWordGlyphRatio) - solved/float64(numChars)
										leftOver := gap - solved
										glyphFudgeFac = leftOver / (float64(numChars))
										if glyphFudgeFac > pw.SpacerWidthMillimeters*justifyMaxFactorGlyph {
											glyphFudgeFac = justifyMaxFactorGlyph
										}
									}
									glyphFudgeFac = 0.0
									wordFudgeFac = 0.0
									if pw.currentParagraphFormat.Alignment == "J" && linesWritten < len(formattedSpans.LineBreaks)-1 {
										if len(formattedSpans.Formats) > 0 {
											pw.Pdf.SetFont(formattedSpans.Formats[0].FontName, formattedSpans.Formats[0].Style, formattedSpans.Formats[0].FontSize)
										}
										start := 0
										if linesWritten > 0 && linesWritten < len(formattedSpans.LineBreaks)+1 {
											start = formattedSpans.LineBreaks[linesWritten-1]
										}
										ending := len(formattedSpans.Runes)
										if linesWritten < len(formattedSpans.LineBreaks) {
											ending = formattedSpans.LineBreaks[linesWritten]
										}
										thisLine := formattedSpans.Runes[start:ending]
										curLineW := pw.Pdf.GetStringWidth(string(thisLine))
										leftSide := pw.PageWidth - pw.RightMargin
										if pw.CurrentColumn < len(pw.ColumnX)-1 {
											if pw.ColumnX[pw.CurrentColumn] < pw.ColumnX[pw.CurrentColumn+1] {
												leftSide = pw.ColumnX[pw.CurrentColumn+1]
											}
										}
										columnWidth := leftSide - pw.ColumnX[pw.CurrentColumn]
										if start == 0 {
											// avoid indent overlap
											columnWidth -= pw.currentParagraphFormat.TextIndent
										}
										curLineW += currentCC.Constraints.Left
										curLineW += currentCC.Constraints.Right
										gap := columnWidth - curLineW
										numChars := ending - start
										//get the number of spaces
										numSpaces := 0
										for _, r := range thisLine {
											//TODO: do chinese characters get treated special?
											if r == ' ' {
												numSpaces++
												numChars--
											}
										}
										wordFudgeFac = gap * justifyWordGlyphRatio / float64(numSpaces)
										if wordFudgeFac > pw.SpacerWidthMillimeters*justifyMaxFactorWord {
											wordFudgeFac = justifyMaxFactorWord
										}
										solved := wordFudgeFac * float64(numSpaces)
										//glyphFudgeFac = gap*(1-justifyWordGlyphRatio) - solved/float64(numChars)
										leftOver := gap - solved
										glyphFudgeFac = leftOver / (float64(numChars))
										if glyphFudgeFac > pw.SpacerWidthMillimeters*justifyMaxFactorGlyph {
											glyphFudgeFac = justifyMaxFactorGlyph
										}
									}
									pw.Pdf.SetCellMargin(0)
									//pw.Pdf.SetX(pw.Pdf.GetX() + offsetleft)
								}
								char := string(formattedSpans.Runes[k : k+1])
								if char == indentProxy {
									char = " "
								}
								//pw.Pdf.WriteAligned(pw.ColumnWidth, pw.LineHeight, char, pw.currentParagraphFormat.Alignment)
								if int(pw.Pdf.GetX()*100000) == int(pw.ColumnX[pw.CurrentColumn]*100000) {
									pw.Pdf.SetX(pw.ColumnX[pw.CurrentColumn] + offsetleft)
								}
								if !pw.AlreadyIndented {
									x := pw.Pdf.GetX()
									x += pw.currentParagraphFormat.TextIndent
									pw.Pdf.SetX(x)
									pw.AlreadyIndented = true
								}
								if recalibrateCentering {
									if pw.currentParagraphFormat.Alignment == "C" {
										// Calculate the width of the remaining text on this line
										if k <= formattedSpans.LineBreaks[linesWritten] {
											remainingText := string(formattedSpans.Runes[k:formattedSpans.LineBreaks[linesWritten]])
											remainingWidth := pw.Pdf.GetStringWidth(remainingText)

											// Calculate new indentation
											newIndent := (pw.ColumnWidth - remainingWidth) / 2

											// Set new X position
											pw.Pdf.SetX(pw.ColumnX[pw.CurrentColumn] + newIndent)

											// Reset the flag
										}
										recalibrateCentering = false
									}
								}
								if leadingSpaceMode {
									if unicode.IsSpace(formattedSpans.Runes[k]) {
										continue
									}
									leadingSpaceMode = false
								}
								pw.Pdf.Write(pw.LineHeight, char)
								pBorder.expand(pw.Pdf)
								if pw.currentParagraphFormat.Alignment == "J" {
									x := pw.Pdf.GetX()
									if char == " " {
										x += wordFudgeFac
									} else {
										x += glyphFudgeFac
									}
									pw.Pdf.SetX(x)
								}
							}
							if requiresAdditionalLineBreak {
								//pw.Pdf.Ln(pw.LineHeight)
								pw.Pdf.SetY(pw.Pdf.GetY() + pw.LineHeight)
								requiresAdditionalLineBreak = false
							}
							if span.Format.Float == "right" {
								pw.Pdf.SetX(vetusX)
							}
							pw.Pdf.SetFontSize(pw.FontSize)
							pw.Pdf.SetTextColor(0, 0, 0)
							pw.Pdf.SetFontStyle("")
						}
					} else {
						//press.PrepPara(elemCC.Root.Children[rowIndex])
						//press.PrintPara(elemCC.Root.Children[rowIndex])
						e := elemCC.Root.Children[rowIndex]
						prep := func(el *paged.Element) {
							if ff, exists := el.GetRule("font-family"); exists {
								pw.Pdf.SetFont(ff, "", 12)
							}
							fs, err := el.GetFontSizeVal("font-size")
							if err == nil {
								pw.Pdf.SetFontSize(fs)
							}
						}
						e.PrepGetW = &prep
						getW := func(s string) float64 {
							return pw.Pdf.GetStringWidth(s)
						}
						e.GetWidth = &getW
						e.RecursiveExplode(32)
						var printElem func(e *paged.Element)
						printElem = func(e *paged.Element) {
							if len(e.Children) == 0 {
								if e.Text != "" {
									x := pw.Pdf.GetX()
									if x+e.Width > pw.ColumnWidth {
										pw.Pdf.SetX(pw.ColumnX[pw.CurrentColumn])
										y := pw.Pdf.GetY()
										pw.Pdf.SetY(y + pw.LineHeight)
									}
									pw.Pdf.Write(pw.LineHeight, e.Text)
								}
								return
							}
							for i, el := range e.Children {
								printElem(el)
								if i < len(el.Children) {
									pw.Pdf.Write(pw.LineHeight, " ")
								}
							}
						}
						printElem(e)
					}
					if pw.currentParagraphFormat.RuleStyle != "" {
						pBorder.draw(pw)
					}

					pw.Pdf.SetY(pw.Pdf.GetY() + currentCC.Constraints.Bottom)
					if pw.currentParagraphFormat.BreakAfter {
						_, yMeter = pw.Pdf.GetXY()
						if pageHeight-yMeter-fonts.PointsToMillimeters(linesBeforeTriggeredPageBreak*pw.FontSize)-pw.BottomMargin > 0 {
							if acceptBreak(pw, setCol, &offsetleft, &offsettop)() {
								pw.Pdf.AddPage()
							}
						}
					}
				}
			}

			//offsetleft = 0
		}
		if len(elemCC.Root.Children) > 0 {
			currentCC = elemCC.Root.Children[rowIndex]
			if len(currentCC.Children) > 0 {
				pw.Pdf.Ln(elemCC.Root.Children[rowIndex].Children[0].Constraints.Bottom)
			}
		}
		//pw.Pdf.Ln(pw.ParagraphSpacing + pw.currentParagraphFormat.BorderBottom.Margin)
		_, yMeter = pw.Pdf.GetXY()
	}
	return //DEBUG
}

func acceptBreak(pw *Writer, setCol func(col int), xoff, yoff *float64) func() bool {
	return func() bool {
		if pw.MultiLanguage {
			recalibrateCentering = true
			if pw.LinesRemaining > 0 { // lines remaining for the current cell
				return true
			}
			setCol(0)
			// reposition x for centered text at top of column 1
			pw.Pdf.SetX(pw.LeftMargin + pw.IndentBy + *xoff)

			/*if pw.currentParagraphFormat.TextIndent > 0 { // reposition x for indented text at top of column 1
				for i := 0; i < pw.currentParagraphFormat.TextIndent; i++ {
					pw.Pdf.Write(pw.LineHeight, " ")
				}
			}*/
			recalibrateCentering = true
			return true // insert a page break
		}
		// monolingual text
		if pw.CurrentColumn < pw.NumberOfMonolingualColumns-1 {
			setCol(pw.CurrentColumn + 1)
			//pw.Pdf.SetY(pw.HeaderLineConfig.Y2 + pw.CellMargin)
			pw.Pdf.SetY(pw.HeaderLineConfig.Y2 + *yoff)
			// reposition x for centered text at top of next column
			//currX := pw.Pdf.GetX()
			//pw.Pdf.SetX(pw.ColumnX[pw.CurrentColumn] + pw.IndentBy + *xoff)
			//pw.Pdf.SetX(pw.ColumnX[pw.CurrentColumn] + currX)
			// Start new column, not new page
			//recalibrateCentering = true
			if pw.currentParagraphFormat.Alignment == "C" {
				//pw.Pdf.SetX(pw.ColumnX[pw.CurrentColumn] + backupCenterX)
				pw.Pdf.SetX(backupCenterX + (pw.ColumnX[pw.CurrentColumn] - pw.LeftMargin))
				requiresAdditionalLineBreak = true
				//	} else {
				//		pw.Pdf.SetX(backupCenterX + (pw.ColumnX[pw.CurrentColumn] - pw.LeftMargin))
			} else {
				pw.Pdf.SetX(backupLeftX + (pw.ColumnX[pw.CurrentColumn] - pw.LeftMargin))
			}
			return false
		}
		setCol(0)
		// reposition x for centered text at top of column 1
		//pw.Pdf.SetX(pw.LeftMargin + pw.IndentBy + *xoff)

		/*if pw.currentParagraphFormat.TextIndent > 0 { // reposition x for indented text at top of column 1
			for i := 0; i < pw.currentParagraphFormat.TextIndent; i++ {
				pw.Pdf.Write(pw.LineHeight, " ")
			}
		}*/
		//recalibrateCentering = true
		if pw.currentParagraphFormat.Alignment == "C" {
			if len(pw.ColumnX) > pw.CurrentColumn {
				if pw.CurrentColumn == 0 {
					pw.Pdf.SetX(backupCenterX)
					return true
				}
				pw.Pdf.SetX(backupCenterX - (pw.ColumnX[pw.CurrentColumn+1] - pw.ColumnX[pw.CurrentColumn]))
				requiresAdditionalLineBreak = true
			}
		} else {
			pw.Pdf.SetX(backupLeftX - (pw.ColumnX[pw.CurrentColumn+1] - pw.ColumnX[pw.CurrentColumn]))
		}
		return true // insert a page break
	}
}

func (pw *Writer) WriteHeader() {
	// [ ] Set format using the PDF css rules.
	// Colors of frame, background and text
	pw.Pdf.SetDrawColor(255, 255, 255) // white
	pw.Pdf.SetFillColor(255, 255, 255) // white

	pw.Pdf.SetY(pw.TopMargin)
	pw.SetFormat(pw.HeaderFormat)
	pw.WriteHeaderLeft()
	pw.WriteHeaderCenter()
	pw.WriteHeaderRight()

	// Thickness of frame (1 mm)
	if pw.Columns {
		pw.Pdf.SetLineWidth(pw.ColumnLineConfig[0].Width)
	} else {
		pw.Pdf.SetLineWidth(0)
	}

	if pw.HeaderLine {
		pw.DrawHeaderLine()
	}
}
func IsEven(i int) bool {
	if i%2 == 0 {
		return true
	}
	return false
}
func (pw *Writer) BuildSectionText(decorators []atempl.PdfDirective) (string, string) {
	sb := strings.Builder{}
	var langCode string
	for _, decorator := range decorators {
		if sb.Len() > 0 {
			sb.WriteString(" ")
		}
		switch decorator.Type {
		case directiveTypes.InsertDate:
			// TODO: in configs for each lang code fonts, add a setting for date format.
			// Note that week names and month names are hardwired in go.
			// There is no locale format available.  One solution is in settings to
			// have lang code specific names for each weekday and month name,
			// then convert the go ones into the locale specific using strings.NewReplacer().
			// See https://stackoverflow.com/questions/41897265/how-do-i-convert-date-to-the-string-in-go-using-locale
			libCode := pw.DownshiftLangCode(decorator.LibraryNbr)
			if len(pw.LangCodes) > libCode {
				langCode = pw.LangCodes[libCode]
			} else {
				langCode = pw.Acronym
			}
			sb.WriteString(fmt.Sprintf(decorator.Date.Format(pw.DateFormatForLangIndex(libCode))))
		case directiveTypes.InsertLookup: // Δόξα σοι, Κύριε, δόξα σοι! Λύνεται.
			lookup := decorator.Lookup
			lookupIndex := pw.DownshiftLangCode(lookup.Library)
			if len(pw.LangCodes) > lookupIndex {
				langCode = pw.LangCodes[lookupIndex]
			} else {
				langCode = pw.Acronym
			}
			// find the liturgical libs for the matching acronym, e.g. gr-en
		L:
			for _, v1 := range lookup.Values {
				for _, v2 := range v1.Values {
					if v2.Acronym == pw.Acronym {
						if len(v2.LiturgicalLibs) > lookupIndex {
							sb.WriteString(v2.LiturgicalLibs[lookupIndex].Value)
						}
						break L
					}
				}
			}
		case directiveTypes.InsertLiteral:
			sb.WriteString(decorator.Literal)
		case directiveTypes.InsertPageNbr:
			intPageNo := pw.Pdf.PageNo()
			if pw.InitialPageNumber > 1 { // user can set page number in template.
				intPageNo = intPageNo + pw.InitialPageNumber - 1
			}
			strPageNo := fmt.Sprintf("%d", intPageNo)
			sb.WriteString(fmt.Sprintf("%s", strPageNo))
		case directiveTypes.InsertVersion:
			// this case intentionally left blank

		}
	}
	txt := strings.TrimSpace(sb.String())
	if txt == " " { // for some reason, TrimSpace can result in " "
		return "", langCode
	} else {
		return txt, langCode
	}
}

// CleanSpaces is designed to iterate over formatted spans, remove the spaces at the start and end of lines,
// and update the LineBreaks and FormatBreaks indexes
// CleanSpaces is designed to iterate over formatted spans, remove the spaces at the start and end of lines,
// and update the LineBreaks and FormatBreaks indexes
func CleanSpaces(spans *FormattedSpans) *FormattedSpans {
	spans = cleanLeadingSpaces(spans)
	//spans = cleanTrailingSpaces(spans)
	return spans
}

func cleanLeadingSpaces(spans *FormattedSpans) *FormattedSpans {
	if spans == nil || len(spans.Runes) == 0 {
		return spans
	}

	newRunes := make([]rune, 0, len(spans.Runes))
	newLineBreaks := make([]int, 0, len(spans.LineBreaks))
	newFormatBreaks := make([]int, 0, len(spans.FormatBreaks))

	lineCount := 0
	formatCount := 0
	lineActuallyStarted := false

	for i, r := range spans.Runes {
		// Handle line breaks
		if lineCount < len(spans.LineBreaks) && i == spans.LineBreaks[lineCount] {
			if lineActuallyStarted || len(newRunes) > 0 {
				newLineBreaks = append(newLineBreaks, len(newRunes))
			}
			lineCount++
			lineActuallyStarted = false
		}

		// Handle format breaks
		if formatCount < len(spans.FormatBreaks) && i == spans.FormatBreaks[formatCount] {
			newFormatBreaks = append(newFormatBreaks, len(newRunes))
			formatCount++
		}

		// Skip leading spaces
		if !lineActuallyStarted && unicode.IsSpace(r) {
			continue
		}

		// Add non-space runes to newRunes
		newRunes = append(newRunes, r)
		lineActuallyStarted = true
	}

	// Handle the case where the last line break is at the end of the input
	if lineCount < len(spans.LineBreaks) && spans.LineBreaks[lineCount] == len(spans.Runes) {
		newLineBreaks = append(newLineBreaks, len(newRunes))
	}

	// Handle the case where the last format break is at the end of the input
	if formatCount < len(spans.FormatBreaks) && spans.FormatBreaks[formatCount] == len(spans.Runes) {
		newFormatBreaks = append(newFormatBreaks, len(newRunes))
	}

	// Update spans
	spans.Runes = newRunes
	spans.LineBreaks = newLineBreaks
	spans.FormatBreaks = newFormatBreaks

	return spans
}

func cleanTrailingSpaces(spans *FormattedSpans) *FormattedSpans {
	if spans == nil || len(spans.Runes) == 0 {
		return spans
	}

	// Create new slices to store updated data
	reverseRunes := make([]rune, 0, len(spans.Runes))

	lineCount := len(spans.LineBreaks) - 1
	formatCount := len(spans.FormatBreaks) - 1
	deficit := 0
	lineActuallyStarted := false
	for i := len(spans.Runes) - 1; i >= 0; i-- {
		if i == spans.LineBreaks[lineCount] {
			spans.LineBreaks[lineCount] -= deficit
			lineCount--
			lineActuallyStarted = false
		}
		if i == spans.FormatBreaks[formatCount] {
			spans.FormatBreaks[formatCount] -= deficit
			formatCount--
		}
		if lineActuallyStarted {
			reverseRunes = append(reverseRunes, spans.Runes[i])
			continue
		}
		if unicode.IsSpace(spans.Runes[i]) {
			deficit++
			continue
		}
		reverseRunes = append(reverseRunes, spans.Runes[i])
		lineActuallyStarted = true
	}

	// Update spans
	newRunes := make([]rune, len(reverseRunes))
	// the runes are in reverse order rn, we need to reverse the reverse
	for i, r := range reverseRunes {
		newRunes[len(reverseRunes)-i-1] = r
	}
	spans.Runes = newRunes
	return spans
}

// DownshiftLangCode checks to see if the LML language value is greater
// than the number of columns being generated.  If so, it returns
// a reduced value that is in keeping with the number of columns.
func (pw *Writer) DownshiftLangCode(langCode int) int {
	var downShiftedValue int
	switch pw.NumberOfVersions {
	case 1:
		return 0
	case 2:
		if langCode == 2 {
			return 1
		} else {
			return langCode
		}
	case 3:
		return langCode
	}
	return downShiftedValue
}
func (pw *Writer) DateFormatForLangIndex(index int) string {
	var langCode string
	if len(pw.LangCodes) > index {
		langCode = pw.LangCodes[index]
	} else {
		langCode = "default"
	}
	return pw.DateFormat(langCode)
}
func (pw *Writer) DateFormat(langCode string) string {
	if fontMap, ok := pw.FontMap[langCode]; ok {
		return fontMap.DateFormat
	} else {
		return "Monday, January 02, 2006"
	}
}
func (pw *Writer) WriteHeaderLeft() {
	directives := pw.GetHeaderForParity(pw.MetaPdfData.Headers, atempl.Left)
	if directives == nil {
		return
	}
	txt, langCode := pw.BuildSectionText(directives)
	if len(txt) == 0 {
		return
	}
	var sw float64
	y := pw.Pdf.GetY()
	pw.Pdf.SetX(pw.LeftMargin)
	if len(langCode) > 0 {
		pw.Pdf.SetFont(langCode, pw.HeaderFormat.Style, pw.HeaderFormat.FontSize)
		if langCode == "kor" {
			txt = strings.ReplaceAll(txt, " ", "  ")
		}
	}
	sw = pw.Pdf.GetStringWidth(txt)
	pw.Pdf.CellFormat(sw, pw.LineHeight, txt, "1", 1, "L", true, 0, "")
	pw.Pdf.SetY(y)
}
func (pw *Writer) WriteHeaderCenter() {
	directives := pw.GetHeaderForParity(pw.MetaPdfData.Headers, atempl.Center)
	if directives == nil {
		return
	}
	txt, langCode := pw.BuildSectionText(directives)
	var sw float64
	if len(txt) == 0 {
		return
	}
	y := pw.Pdf.GetY()
	if len(langCode) > 0 {
		pw.Pdf.SetFont(langCode, pw.HeaderFormat.Style, pw.HeaderFormat.FontSize)
	}
	sw = pw.Pdf.GetStringWidth(txt)
	pw.Pdf.SetX(pw.CenterX - sw/2)
	pw.Pdf.CellFormat(sw, pw.HeaderLineConfig.Y2-pw.TopMargin, txt, "1", 1, "C", true, 0, "")
	pw.Pdf.SetY(y)
}

// GetHeaderForParity returns the center header whose parity matches the page parity,
// that is even vs odd vs either
func (pw *Writer) GetHeaderForParity(headers []atempl.Header, position atempl.Position) []atempl.PdfDirective {
	pageNbrEven := IsEven(pw.Pdf.PageNo())
	for _, header := range headers {
		if pageNbrEven && (header.Parity == atempl.Even) {
			return pw.GetHeaderDecoratorsForPosition(header, position)
		}
		if !pageNbrEven && header.Parity == atempl.Odd {
			return pw.GetHeaderDecoratorsForPosition(header, position)
		}
	}
	return nil
}

// GetFooterForParity returns the center header whose parity matches the page parity,
// that is even vs odd vs either
func (pw *Writer) GetFooterForParity(footers []atempl.Footer, position atempl.Position) []atempl.PdfDirective {
	pageNbrEven := IsEven(pw.Pdf.PageNo())
	for _, footer := range footers {
		if pageNbrEven && (footer.Parity == atempl.Even) {
			return pw.GetFooterDecoratorsForPosition(footer, position)
		}
		if !pageNbrEven && footer.Parity == atempl.Odd {
			return pw.GetFooterDecoratorsForPosition(footer, position)
		}
	}
	return nil
}
func (pw *Writer) GetHeaderDecoratorsForPosition(header atempl.Header, position atempl.Position) []atempl.PdfDirective {
	switch position {
	case atempl.Left:
		return header.Left.Directives
	case atempl.Right:
		return header.Right.Directives
	default: // center
		return header.Center.Directives
	}
}
func (pw *Writer) GetFooterDecoratorsForPosition(footer atempl.Footer, position atempl.Position) []atempl.PdfDirective {
	switch position {
	case atempl.Left:
		return footer.Left.Directives
	case atempl.Right:
		return footer.Right.Directives
	default: // center
		return footer.Center.Directives
	}
}
func (pw *Writer) WriteHeaderRight() {
	directives := pw.GetHeaderForParity(pw.MetaPdfData.Headers, atempl.Right)
	if directives == nil {
		return
	}
	txt, langCode := pw.BuildSectionText(directives)
	if len(txt) == 0 {
		return
	}
	var sw float64
	if len(langCode) > 0 {
		pw.Pdf.SetFont(langCode, pw.HeaderFormat.Style, pw.HeaderFormat.FontSize)
		if langCode == "kor" {
			txt = strings.ReplaceAll(txt, " ", "  ")
		}
	}
	sw = pw.Pdf.GetStringWidth(txt)
	pw.Pdf.SetX(pw.PageWidth - pw.RightMargin - sw)
	pw.Pdf.CellFormat(sw, pw.LineHeight, txt, "1", 1, "R", true, 0, "")
}
func (pw *Writer) WriteFooter() {

	pw.Pdf.SetDrawColor(255, 255, 255) // white
	pw.Pdf.SetFillColor(255, 255, 255) // white
	pw.SetFormat(pw.FooterFormat)

	pw.WriteFooterLeft()
	pw.WriteFooterCenter()
	pw.WriteFooterRight()

	pw.Pdf.SetTextColor(0, 0, 0)
}

func (pw *Writer) WriteFooterLeft() {
	directives := pw.GetFooterForParity(pw.MetaPdfData.Footers, atempl.Left)
	if directives == nil {
		return
	}
	txt, langCode := pw.BuildSectionText(directives)
	if len(txt) == 0 {
		return
	}
	var sw float64
	y := pw.Pdf.GetY()
	pw.Pdf.SetX(pw.LeftMargin)
	if len(langCode) > 0 {
		pw.Pdf.SetFont(langCode, pw.FooterFormat.Style, pw.FooterFormat.FontSize)
		if langCode == "kor" {
			txt = strings.ReplaceAll(txt, " ", "  ")
		}
	}
	sw = pw.Pdf.GetStringWidth(txt)
	pw.Pdf.CellFormat(sw, pw.LineHeight, txt, "1", 1, "L", true, 0, "")
	pw.Pdf.SetY(y)
}
func (pw *Writer) WriteFooterCenter() {
	directives := pw.GetFooterForParity(pw.MetaPdfData.Footers, atempl.Center)
	if directives == nil {
		return
	}
	txt, langCode := pw.BuildSectionText(directives)
	if len(txt) == 0 {
		return
	}
	var sw float64
	y := pw.Pdf.GetY()
	if len(langCode) > 0 {
		pw.Pdf.SetFont(langCode, pw.FooterFormat.Style, pw.FooterFormat.FontSize)
		if langCode == "kor" {
			txt = strings.ReplaceAll(txt, " ", "  ")
		}
	}
	sw = pw.Pdf.GetStringWidth(txt)
	pw.Pdf.SetX(pw.CenterX - sw/2)
	pw.Pdf.CellFormat(sw, pw.LineHeight, txt, "1", 1, "C", true, 0, "")
	pw.Pdf.SetY(y)
}
func (pw *Writer) WriteFooterRight() {
	directives := pw.GetFooterForParity(pw.MetaPdfData.Footers, atempl.Right)
	if directives == nil {
		return
	}
	txt, langCode := pw.BuildSectionText(directives)
	if len(txt) == 0 {
		return
	}
	var sw float64
	if len(langCode) > 0 {
		pw.Pdf.SetFont(langCode, pw.FooterFormat.Style, pw.FooterFormat.FontSize)
		if langCode == "kor" {
			txt = strings.ReplaceAll(txt, " ", "  ")
		}
	}
	sw = pw.Pdf.GetStringWidth(txt)
	pw.Pdf.SetX(pw.PageWidth - pw.RightMargin - sw)
	pw.Pdf.CellFormat(sw, pw.LineHeight, txt, "1", 1, "R", true, 0, "")
}

func (pw *Writer) SetHeaderSections() {
	pw.HeaderSections = NewParitySections()
	pw.HeaderSections.Even.Left.Type = directiveTypes.InsertLookup
	pw.HeaderSections.Even.Left.Value = "HeaderEvenLeft"
	pw.HeaderSections.Even.Center.Type = directiveTypes.InsertLookup
	pw.HeaderSections.Even.Center.Value = "HeaderEvenCenter"
	pw.HeaderSections.Even.Right.Type = directiveTypes.InsertLookup
	pw.HeaderSections.Even.Right.Value = "HeaderEvenRight"
	pw.HeaderSections.Odd.Left.Type = directiveTypes.InsertLookup
	pw.HeaderSections.Odd.Left.Value = "HeaderOddLeft"
	pw.HeaderSections.Odd.Center.Type = directiveTypes.InsertLookup
	pw.HeaderSections.Odd.Center.Value = "HeaderOddCenter"
	pw.HeaderSections.Odd.Right.Type = directiveTypes.InsertLookup
	pw.HeaderSections.Odd.Right.Value = "HeaderOddRight"
}
func (pw *Writer) SetFooterSections() {
	pw.FooterSections = NewParitySections()
	pw.FooterSections.Even.Left.Type = directiveTypes.InsertLookup
	pw.FooterSections.Even.Left.Value = "FooterEvenLeft"
	pw.FooterSections.Even.Center.Type = directiveTypes.InsertLookup
	pw.FooterSections.Even.Center.Value = "FooterEvenCenter"
	pw.FooterSections.Even.Right.Type = directiveTypes.InsertDate
	pw.FooterSections.Even.Right.Value = "FooterEvenRight"
	pw.FooterSections.Odd.Left.Type = directiveTypes.InsertLookup
	pw.FooterSections.Odd.Left.Value = "FooterOddLeft"
	pw.FooterSections.Odd.Center.Type = directiveTypes.InsertDate
	pw.FooterSections.Odd.Center.Value = "FooterOddCenter"
	pw.FooterSections.Odd.Right.Type = directiveTypes.InsertLookup
	pw.FooterSections.Odd.Right.Value = "FooterOddRight"
}

type ParitySections struct {
	Even   *Sections
	Odd    *Sections
	Either *Sections
}

func NewParitySections() *ParitySections {
	p := new(ParitySections)
	p.Either = NewSections()
	p.Even = NewSections()
	p.Odd = NewSections()
	return p
}

type Sections struct {
	Left   *SectionType
	Center *SectionType
	Right  *SectionType
}

func NewSections() *Sections {
	s := new(Sections)
	s.Right = new(SectionType)
	s.Center = new(SectionType)
	s.Left = new(SectionType)
	return s
}

type SectionType struct {
	Type  directiveTypes.DirectiveType
	Value string
}

func (pw *Writer) SetFooterLineConfig(width float64, colorR, colorG, colorB int) {
	pw.FooterLineConfig = new(LineConfig)
	pw.FooterLineConfig.Width = width
	pw.FooterLineConfig.ColorR = colorR
	pw.FooterLineConfig.ColorB = colorB
	pw.FooterLineConfig.ColorG = colorG
	pw.FooterLineConfig.X1 = pw.LeftMargin
	pw.FooterLineConfig.Y1 = pw.PageHeight - pw.BottomMargin
	pw.FooterLineConfig.X2 = pw.PageWidth - pw.RightMargin
	pw.FooterLineConfig.Y2 = pw.PageHeight - pw.BottomMargin
}
func (pw *Writer) DrawFooterLine() {
	pw.Pdf.SetDrawColor(pw.FooterLineConfig.GetRGB())
	pw.Pdf.SetLineWidth(pw.FooterLineConfig.Width)
	pw.Pdf.Line(pw.FooterLineConfig.GetCoordinates())
}
func (pw *Writer) DrawHeaderLine() {
	if !pw.HeaderLine {
		return
	}
	pw.Pdf.SetDrawColor(pw.HeaderLineConfig.GetRGB())
	pw.Pdf.SetLineWidth(pw.HeaderLineConfig.Width)
	if pw.HeaderLineConfig.Width > 0 {
		pw.Pdf.Line(pw.HeaderLineConfig.GetCoordinates())
	}
}
func (pw *Writer) DrawMarginLines() {
	// Draw left margin
	pw.Pdf.SetDrawColor(pw.LeftMarginLineConfig.GetRGB())
	pw.Pdf.SetLineWidth(pw.LeftMarginLineConfig.Width)
	pw.Pdf.Line(pw.LeftMarginLineConfig.GetCoordinates())
	// Draw right margin
	pw.Pdf.SetDrawColor(pw.RightMarginLineConfig.GetRGB())
	pw.Pdf.SetLineWidth(pw.RightMarginLineConfig.Width)
	pw.Pdf.Line(pw.RightMarginLineConfig.GetCoordinates())
	// Draw Top margin
	pw.Pdf.SetDrawColor(pw.TopMarginLineConfig.GetRGB())
	pw.Pdf.SetLineWidth(pw.TopMarginLineConfig.Width)
	pw.Pdf.Line(pw.TopMarginLineConfig.GetCoordinates())
	// Draw Bottom margin
	pw.Pdf.SetDrawColor(pw.BottomMarginLineConfig.GetRGB())
	pw.Pdf.SetLineWidth(pw.BottomMarginLineConfig.Width)
	pw.Pdf.Line(pw.BottomMarginLineConfig.GetCoordinates())
}
func (pw *Writer) DrawColumnLines() {
	if pw.Columns {
		for i := 0; i < pw.NumberOfGutters; i++ {
			pw.Pdf.SetDrawColor(pw.ColumnLineConfig[i].GetRGB())
			pw.Pdf.SetLineWidth(pw.ColumnLineConfig[i].Width)
			pw.Pdf.Line(pw.ColumnLineConfig[i].GetCoordinates())
		}
	}
}
func (pw *Writer) DrawSectionLine(cssClass string, center, y float64) {
	format := pw.GetFormat("hr", cssClass, nil)
	pw.Pdf.SetDrawColor(format.Color.Red, format.Color.Green, format.Color.Blue)
	pw.Pdf.SetLineWidth(format.RuleWidth)
	pw.Pdf.SetLineCapStyle("round")
	length := format.RuleLength / 2
	yBoost := format.RuleWidth
	if rule, exists := pw.MetaPdfData.CSS.GetRule("hr." + cssClass); exists {
		constraints := []string{
			"line-height",
			"margin-top",
			"padding-top",
			"margin-bottom",
		}
		for _, c := range constraints {
			if rule.HasIdentifier(c) {
				lh := rule.GetDeclaration(c)
				lh.NormalizeFloatAndUnit(12)
				yBoost += lh.Float
			}
		}
	}
	oldY := pw.Pdf.GetY()
	pw.Pdf.SetY(oldY + (yBoost / 2))
	pw.Pdf.Line(center-length, y, center+length, y)
	pw.Pdf.SetY(oldY + (yBoost / 2))
}

func (pw *Writer) SetHeaderLineConfig(width float64, colorR, colorG, colorB int) {
	pw.HeaderLineConfig = new(LineConfig)
	pw.HeaderLineConfig.Width = width
	pw.HeaderLineConfig.ColorR = colorR
	pw.HeaderLineConfig.ColorB = colorB
	pw.HeaderLineConfig.ColorG = colorG
	pw.HeaderLineConfig.X1 = pw.LeftMargin
	pw.HeaderLineConfig.Y1 = pw.TopMargin + pw.LineHeight
	pw.HeaderLineConfig.X2 = pw.PageWidth - pw.RightMargin
	pw.HeaderLineConfig.Y2 = pw.TopMargin + pw.LineHeight
}
func (pw *Writer) SetLeftMarginLineConfig(width float64, colorR, colorG, colorB int) {
	pw.LeftMarginLineConfig = new(LineConfig)
	pw.LeftMarginLineConfig.Width = width
	pw.LeftMarginLineConfig.ColorR = colorR
	pw.LeftMarginLineConfig.ColorB = colorB
	pw.LeftMarginLineConfig.ColorG = colorG
	pw.LeftMarginLineConfig.X1 = pw.LeftMargin
	pw.LeftMarginLineConfig.Y1 = pw.TopMargin
	pw.LeftMarginLineConfig.X2 = pw.LeftMargin
	pw.LeftMarginLineConfig.Y2 = pw.PageHeight - pw.BottomMargin
}
func (pw *Writer) SetTopMarginLineConfig(width float64, colorR, colorG, colorB int) {
	pw.TopMarginLineConfig = new(LineConfig)
	pw.TopMarginLineConfig.Width = width
	pw.TopMarginLineConfig.ColorR = colorR
	pw.TopMarginLineConfig.ColorB = colorB
	pw.TopMarginLineConfig.ColorG = colorG
	pw.TopMarginLineConfig.X1 = pw.LeftMargin
	pw.TopMarginLineConfig.Y1 = pw.TopMargin
	pw.TopMarginLineConfig.X2 = pw.PageWidth - pw.RightMargin
	pw.TopMarginLineConfig.Y2 = pw.TopMargin
}
func (pw *Writer) SetBottomMarginLineConfig(width float64, colorR, colorG, colorB int) {
	pw.BottomMarginLineConfig = new(LineConfig)
	pw.BottomMarginLineConfig.Width = width
	pw.BottomMarginLineConfig.ColorR = colorR
	pw.BottomMarginLineConfig.ColorB = colorB
	pw.BottomMarginLineConfig.ColorG = colorG
	pw.BottomMarginLineConfig.X1 = pw.LeftMargin
	pw.BottomMarginLineConfig.Y1 = pw.PageHeight - pw.BottomMargin
	pw.BottomMarginLineConfig.X2 = pw.PageWidth - pw.RightMargin
	pw.BottomMarginLineConfig.Y2 = pw.PageHeight - pw.BottomMargin
}
func (pw *Writer) SetRightMarginLineConfig(width float64, colorR, colorG, colorB int) {
	pw.RightMarginLineConfig = new(LineConfig)
	pw.RightMarginLineConfig.Width = width
	pw.RightMarginLineConfig.ColorR = colorR
	pw.RightMarginLineConfig.ColorG = colorG
	pw.RightMarginLineConfig.ColorB = colorB
	pw.RightMarginLineConfig.X1 = pw.PageWidth - pw.RightMargin
	pw.RightMarginLineConfig.Y1 = pw.TopMargin
	pw.RightMarginLineConfig.X2 = pw.PageWidth - pw.RightMargin
	pw.RightMarginLineConfig.Y2 = pw.PageHeight - pw.BottomMargin
}
func (pw *Writer) SetColumnLineConfigs(width float64, colorR, colorG, colorB int) {
	for i := 0; i < pw.NumberOfGutters; i++ {
		var conf = new(LineConfig)
		conf.Width = width
		conf.ColorR = colorR
		conf.ColorB = colorB
		conf.ColorG = colorG
		conf.Y1 = pw.TopMargin + pw.LineHeight*1.5 // multiply by arbitrary amount
		conf.X2 = pw.CenterX
		conf.Y2 = pw.PageHeight - pw.LineHeight*3.25 // multiply by arbitrary amount
		pw.ColumnLineConfig = append(pw.ColumnLineConfig, conf)
	}
	if pw.NumberOfGutters == 1 {
		pw.ColumnLineConfig[0].X1 = pw.CenterX
		pw.ColumnLineConfig[0].X2 = pw.CenterX
	} else if pw.NumberOfGutters == 2 {
		pw.ColumnLineConfig[0].X1 = pw.ColumnX[1] - pw.GutterWidth/2
		pw.ColumnLineConfig[0].X2 = pw.ColumnX[1] - pw.GutterWidth/2
		pw.ColumnLineConfig[1].X1 = pw.ColumnX[2] - pw.GutterWidth/2
		pw.ColumnLineConfig[1].X2 = pw.ColumnX[2] - pw.GutterWidth/2
	}
}

// ComputeColumnWidths determines the width of each column based on the number of columns.
// If the number of columns is three, the CellMargin is set to 1 and GutterWidth to 0.
func (pw *Writer) ComputeColumnWidths() {
	var cols int
	if pw.MultiLanguage {
		cols = pw.NumberOfVersions
	} else {
		if pw.Columns {
			cols = 2 // print two columns for monolingual
			pw.NumberOfMonolingualColumns = 2
		} else {
			cols = 1 // single column for monolingual
			pw.NumberOfMonolingualColumns = 1
		}
	}
	pw.NumberOfGutters = cols - 1

	if cols == 3 {
		pw.CellMargin = 1
		pw.GutterWidth *= 0.5
	}
	numberOfColumns := float64(cols)
	allGutters := (numberOfColumns - 1) * pw.GutterWidth
	pw.ColumnWidth = (pw.PageWidth - pw.LeftMargin - pw.RightMargin - allGutters) / numberOfColumns

	// initialize slices
	for i := 0.0; i < float64(cols); i++ {
		if i == 0 {
			pw.ColumnX = append(pw.ColumnX, pw.LeftMargin)
		} else {
			pw.ColumnX = append(pw.ColumnX, pw.LeftMargin+i*pw.ColumnWidth+pw.GutterWidth)
		}
		pw.CurrentFormats = append(pw.CurrentFormats, new(Format))
	}

}
func (pw *Writer) GetFormattedSpans(spans []*atempl.Span, outerFormat *Format) *FormattedSpans {
	sb := new(strings.Builder)
	formattedSpans := new(FormattedSpans)
	var sliceRanges []*SliceRange
	formattedSpans, sliceRanges, sb = pw.ProcessSpans(spans, formattedSpans, sliceRanges, sb, outerFormat)
	formattedSpans.Text = sb.String()
	formattedSpans.Runes = []rune(formattedSpans.Text)
	formattedSpans.Spans = sliceRanges
	fontSize, _ := pw.Pdf.GetFontSize() // save current font size
	fontName := pw.Pdf.FontFamily
	if len(formattedSpans.Formats) > 0 { // set up format for line break calculations
		// TODO: there is an issue with grabbing the first format since it is possible
		// that multiple language codes occur in the various child spans due to use
		// of fallbacks.  No solution is currently known.
		pw.SetFormat(formattedSpans.Formats[0])
		pw.Pdf.SetFontSize(IndexOfMaxFontSize(formattedSpans.Formats))
	}
	pw.Pdf.SetFontStyle("BI") // to prevent overflow of line, causing it to wrap
	// handle centering
	trimmedText := strings.TrimSpace(formattedSpans.Text)
	var adjustedColumnWidth float64
	// 	w := f.w - f.rMargin - f.x
	//	wmax := (w - 2*f.cMargin) * 1000 / fontSize
	adjustedColumnWidth = pw.ColumnWidth - (2 * pw.CellMargin) - pw.ColumnWidthAdjustment
	if pw.NumberOfVersions == 3 && pw.CurrentColumn == 1 {
		adjustedColumnWidth = pw.ColumnWidth * pw.MiddleColumnAdjuster
	}
	if outerFormat.Alignment == "C" && len(trimmedText) > 0 {
		// calculate how far to indent in order to center text
		formattedSpans.CenterIndent = (adjustedColumnWidth - pw.Pdf.GetStringWidth(trimmedText)) / 2
	}
	if outerFormat.Alignment == "R" && len(trimmedText) > 0 {
		// calculate how far to indent in order to center text
		formattedSpans.CenterIndent = adjustedColumnWidth - pw.Pdf.GetStringWidth(trimmedText)
	}
	//TODO: right-align support
	if outerFormat != nil {
		formattedSpans.LineBreaks = pw.Pdf.RuneLineBreaksWithIndent(formattedSpans.Text, adjustedColumnWidth, outerFormat.TextIndent)
	} else {
		formattedSpans.LineBreaks = pw.Pdf.RuneLineBreaks(formattedSpans.Text, adjustedColumnWidth)
	} // reset format
	pw.Pdf.SetFont(fontName, "", fontSize)
	return formattedSpans
}

// IndexOfMaxFontSize returns the smallest font size in the slice
func IndexOfMaxFontSize(formats []*Format) float64 {
	l := len(formats)
	size := 0.0
	for i := 0; i < l; i++ {
		if formats[i].FontSize > size {
			size = formats[i].FontSize
		}
	}
	return size
}
func (pw *Writer) GetMaxWidth() float64 {
	_, unitSize := pw.Pdf.GetFontSize()
	return math.Ceil((pw.ColumnWidth - pw.currentFont.LineBreakAdjuster*pw.CellMargin) * 1000 / unitSize)

}
func (pw *Writer) SetFormat(format *Format) {
	pw.Pdf.SetFont(format.FontName, format.Style, format.FontSize)
	pw.Pdf.SetTextColor(format.Color.Color())
}

// GetCssLength if modified also must be updated in css.go.GetCssLength
func GetCssLength(w *Writer, ln string, fallback float64) (float64, error) {
	if strings.HasSuffix(ln, "mm") {
		size, err := strconv.ParseFloat(ln[:len(ln)-2], 64)
		if err != nil {
			return fallback, err
		}
		return size, nil
	} else if strings.HasSuffix(ln, "cm") {
		size, err := strconv.ParseFloat(ln[:len(ln)-2], 64)
		if err != nil {
			return fallback, err
		}
		return size * 10, nil
	} else if strings.HasSuffix(ln, "pt") {
		size, err := strconv.ParseFloat(ln[0:len(ln)-2], 64)
		if err != nil {
			return fallback, err
		}
		return fonts.PointsToMillimeters(size), nil
	} else if strings.HasSuffix(ln, "rem") {
		size, err := strconv.ParseFloat(ln[0:len(ln)-3], 64)
		if err != nil {
			return fallback, err
		}
		return fonts.PointsToMillimeters(w.FontSize * size), err
	} else if strings.HasSuffix(ln, "%") {
		size, err := strconv.ParseFloat(ln[0:len(ln)-1], 64)
		if err != nil {
			return fallback, err
		}
		return fonts.PointsToMillimeters(w.FontSize * (size / 100)), nil
	} else if strings.HasSuffix(ln, "in") {
		size, err := strconv.ParseFloat(ln[0:len(ln)-2], 64)
		if err != nil {
			return fallback, err
		}
		return fonts.PointsToMillimeters(size * 72), nil
	} else {
		return fallback, errors.New("invalid or unsupported unit in css")
	}
}

func GetCssLine(w *Writer, ln string, fallback BorderSide) (returnStyle BorderSide) {
	parts := strings.Split(ln, " ")
	if len(parts) == 0 {
		return fallback
	}
	// the easiest is to just force a convention on the user e.g. thickness style color
	// however with a little effort we can convert them in any order or with any number of arguments
	var finalRGB, c1, c2, c3 RGB
	var finalSize, l1, l2, l3 float64
	canonicalStyles := []string{"solid", "dashed", "double"}
	c1 = GetCssColor(parts[0], "#000000")
	l1, _ = GetCssLength(w, parts[0], 0)
	if len(parts) > 1 {
		c2 = GetCssColor(parts[1], "#000000")
		l2, _ = GetCssLength(w, parts[1], 0)
	}
	if len(parts) > 2 {
		c3 = GetCssColor(parts[2], "#000000")
		l3, _ = GetCssLength(w, parts[2], 0)
	}
	finalRGB = RGB{c1.Red + c2.Red + c3.Red, c1.Green + c2.Green + c3.Green, c1.Blue + c2.Blue + c3.Blue}
	finalSize = l1 + l2 + l3

	for _, style := range canonicalStyles {
		if strings.Contains(ln, style) {
			returnStyle.Style = style
			returnStyle.Width = finalSize
			returnStyle.Color = finalRGB
			break
		}
	}

	return returnStyle
}

func (pw *Writer) GetFormat(element, class string, outerFormat *Format) *Format {
	format := new(Format)
	if outerFormat != nil {
		// set format to outerFormat, but can be overridden below
		format = outerFormat.Copy()
	}
	format.Element = element
	format.Class = class
	format.Alignment = "L" // default
	if format.FontSize == 0 {
		format.FontSize = pw.FontSize
	}
	dotClass := "." + class
	elementClass := element + dotClass
	var rule *css.Rule
	var ok bool
	if rule, ok = pw.MetaPdfData.CSS.GetRule(elementClass); ok {
		rule = pw.MetaPdfData.CSS.RuleMap[elementClass]
	} else {
		return format
	}
	if rule.Declarations == nil {
		return format
	}
	//itterate over it twice to ensure that dependant rules are set in accordance with their selector
	for _, declaration := range rule.Declarations {
		switch strings.ToLower(declaration.Identifier) {
		case "font-family":
			//TODO: handle bold and italic
			format.FontName = strings.Trim(declaration.Value, "\"")
		case "font-size": // see https://weboverhauls.github.io/conversion-table/
			if strings.HasSuffix(declaration.Value, "%") {
				size, err := strconv.ParseFloat(declaration.Value[0:len(declaration.Value)-1], 64)
				if err == nil {
					format.FontSize = pw.BodyStyle.FontSize * (size / 100)
				} else {
					format.FontSize = pw.FontSize
				}
			} else if strings.HasSuffix(declaration.Value, "rem") {
				size, err := strconv.ParseFloat(declaration.Value[0:len(declaration.Value)-2], 64)
				if err == nil {
					format.FontSize = pw.FontSize * size
				} else {
					format.FontSize = pw.FontSize
				}
			} else if strings.HasSuffix(declaration.Value, "pt") {
				size, err := strconv.ParseFloat(declaration.Value[0:len(declaration.Value)-2], 64)
				if err == nil {
					format.FontSize = size
				} else {
					format.FontSize = pw.FontSize
				}
			} else {
				switch strings.ToLower(declaration.Value) {
				case "x-small":
					format.FontSize = 7.5
				case "small":
					format.FontSize = 10
				case "medium":
					format.FontSize = 12
				case "large":
					format.FontSize = 13.5
				case "x-large":
					format.FontSize = 18
				case "xx-large":
					format.FontSize = 24
				default:
					format.FontSize = pw.FontSize
				}
			}
		case "margin":
			format.BorderLeft.Margin, _ = GetCssLength(pw, declaration.Value, pw.BodyStyle.MarginLeft)
			format.BorderRight.Margin, _ = GetCssLength(pw, declaration.Value, pw.BodyStyle.MarginRight)
			format.BorderTop.Margin, _ = GetCssLength(pw, declaration.Value, pw.BodyStyle.MarginTop)
			format.BorderBottom.Margin, _ = GetCssLength(pw, declaration.Value, pw.BodyStyle.MarginBottom)
		case "padding":
			format.BorderLeft.Padding, _ = GetCssLength(pw, declaration.Value, pw.BodyStyle.PaddingLeft)
			format.BorderRight.Padding, _ = GetCssLength(pw, declaration.Value, pw.BodyStyle.PaddingRight)
			format.BorderTop.Padding, _ = GetCssLength(pw, declaration.Value, pw.BodyStyle.PaddingTop)
			format.BorderBottom.Padding, _ = GetCssLength(pw, declaration.Value, pw.BodyStyle.PaddingBottom)
		}
	}
	for _, declaration := range rule.Declarations {
		switch strings.ToLower(declaration.Identifier) {
		case "background-color":
			format.BackgroundColor = GetCssColor(declaration.Value, CssBodyLookup(pw.MetaPdfData, "background-color"))
		case "border", "border-top", "border-bottom", "border-left", "border-right": // e.g., 1pt solid black;
			parts := strings.Split(declaration.Value, " ")
			rw := 0.0
			if len(parts) == 3 {
				// set line width from parts[0]
				//TODO: width
				rw, _ = GetCssLength(pw, parts[0], 0)

				switch strings.ToLower(declaration.Identifier) {
				case "border":
					format.RuleWidth = rw
					format.RuleStyle = parts[1]
					format.BorderColor = GetCssColor(parts[2][:len(parts[2])], "")
				case "border-top":
					format.BorderTop.Width = rw
					format.BorderTop.Style = parts[1]
					format.BorderTop.Color = GetCssColor(parts[2][:len(parts[2])], "")
				case "border-right":
					format.BorderRight.Width = rw
					format.BorderRight.Style = parts[1]
					format.BorderRight.Color = GetCssColor(parts[2][:len(parts[2])], "")
				case "border-left":
					format.BorderLeft.Width = rw
					format.BorderLeft.Style = parts[1]
					format.BorderLeft.Color = GetCssColor(parts[2][:len(parts[2])], "")
				case "border-bottom":
					format.BorderBottom.Width = rw
					format.BorderBottom.Style = parts[1]
					format.BorderBottom.Color = GetCssColor(parts[2][:len(parts[2])], "")
				}
			}
		case "break-after":
			if declaration.Value == "page" {
				format.BreakAfter = true
			}
		case "break-before":
			if declaration.Value == "page" {
				format.BreakBefore = true
			}
		case "color":
			format.Color = GetCssColor(declaration.Value, CssBodyLookup(pw.MetaPdfData, "color"))
		case "text-align":
			switch declaration.Value {
			case "left":
				format.Alignment = "L"
			case "center":
				format.Alignment = "C"
			case "right":
				format.Alignment = "R"
			case "justify":
				format.Alignment = "J"
			}
		case "float":
			format.Float = declaration.Value
		case "font-style", "font-weight":
			switch strings.ToLower(declaration.Value) {
			case "bold", "bolder":
				if format.Style == "I" {
					format.Style = "BI"
				} else {
					format.Style = "B"
				}
			case "italic":
				if format.Style == "B" {
					format.Style = "BI"
				} else {
					format.Style = "I"
				}
			case "normal":
				format.Style = ""
			}
		case "line-height":
			fallback := pw.BodyStyle.LineHeight
			if fallback <= 0 {
				fallback = format.FontSize * 1.2
			}
			oldFontSize := pw.FontSize
			pw.FontSize = format.FontSize
			pw.LineHeight, _ = GetCssLength(pw, declaration.Value, fallback)
			pw.FontSize = oldFontSize
			//pw.LineHeight *= 2.83465 //TODO: more robust conversion
		case "text-indent":
			// Tell people to use mm.
			// But just in case, handle cm or pt
			ti, err := GetCssLength(pw, declaration.Value, 0.5)
			if err != nil {
			}
			format.TextIndent = ti //* 3.5 // not sure why 3.5
		case "width":
			if element == "hr" {
				if strings.HasSuffix(declaration.Value, "%") {
					size, err := strconv.ParseFloat(declaration.Value[0:len(declaration.Value)-1], 64)
					if err == nil {
						format.RuleLength = pw.ColumnWidth * (size / 100)
					}
					continue
				}
				ti, err := GetCssLength(pw, declaration.Value, pw.ColumnWidth/3)
				if err != nil {
					//TODO: error handling?
				}
				format.RuleLength = ti
			}
		case "margin-top":
			format.BorderTop.Margin, _ = GetCssLength(pw, declaration.Value, pw.BodyStyle.MarginTop)
		case "margin-bottom":
			format.BorderBottom.Margin, _ = GetCssLength(pw, declaration.Value, pw.BodyStyle.MarginBottom)
		case "margin-left":
			format.BorderLeft.Margin, _ = GetCssLength(pw, declaration.Value, pw.BodyStyle.MarginLeft)
		case "margin-right":
			format.BorderRight.Margin, _ = GetCssLength(pw, declaration.Value, pw.BodyStyle.MarginRight)
		case "padding-top":
			format.BorderTop.Padding, _ = GetCssLength(pw, declaration.Value, pw.BodyStyle.PaddingTop)
		case "padding-bottom":
			format.BorderBottom.Padding, _ = GetCssLength(pw, declaration.Value, pw.BodyStyle.PaddingBottom)
		case "padding-left":
			format.BorderLeft.Padding, _ = GetCssLength(pw, declaration.Value, pw.BodyStyle.PaddingLeft)
		case "padding-right":
			format.BorderRight.Padding, _ = GetCssLength(pw, declaration.Value, pw.BodyStyle.PaddingRight)
		}
	}
	return format
}

// GetCssColor returns an RGB value corresponding to the color chosen. If the input is invalid, it returns rgb(0,0,0)
func GetCssColor(value string, fallback string) RGB {
	if strings.HasPrefix(value, "#") {
		var hex = fonts.Hex(value[1:])
		rgb, err := fonts.Hex2RGB(hex)
		if err == nil {
			return RGB{int(rgb.Red), int(rgb.Green), int(rgb.Blue)}
		}
	} else if strings.HasPrefix(value, "rgb(") {
		rxp, err := regexp.Compile(`\d+`)
		if err != nil {
			return RGB{0, 0, 0}
		}
		rawamounts := rxp.FindAllString(value, -1)
		var amounts []int
		for _, v := range rawamounts {
			igr, _ := strconv.Atoi(v)
			if igr > 255 {
				return RGB{0, 0, 0}
			}
			amounts = append(amounts, igr)
		}
		return RGB{amounts[0], amounts[1], amounts[2]}
	} else {
		r, g, b := fonts.ColorName2RGB(value)
		return RGB{r, g, b}
	}
	if fallback == "" {
		return RGB{0, 0, 0}
	}
	return GetCssColor(fallback, "")
}
func String2RGB(color string) (fonts.RGB, error) {
	var rgb fonts.RGB
	var err error
	parts := strings.Split(color, ",")
	if len(parts) == 3 { // e.g., 255, 0, 0
		var r, g, b uint64
		r, err = strconv.ParseUint(strings.TrimSpace(parts[0]), 10, 8)
		if err != nil {
			r = 0
		}
		rgb.Red = uint8(r)
		g, err = strconv.ParseUint(strings.TrimSpace(parts[1]), 10, 8)
		if err != nil {
			g = 0
		}
		rgb.Green = uint8(g)
		b, err = strconv.ParseUint(strings.TrimSpace(parts[2]), 10, 8)
		if err != nil {
			b = 0
		}
		rgb.Blue = uint8(b)
	} else { // e.g. #ff0000
		if strings.HasPrefix(color, "#") {
			var hex = fonts.Hex(color[1:])
			rgb, err = fonts.Hex2RGB(hex)
			if err != nil {
				rgb.Red = 0
				rgb.Green = 0
				rgb.Blue = 0
			}
		} else { // e.g. red
			r, g, b := fonts.ColorName2RGB(color)
			rgb.Red = uint8(r)
			rgb.Green = uint8(g)
			rgb.Blue = uint8(b)
		}
	}
	var wrappedError error
	if err != nil {
		wrappedError = fmt.Errorf("invalid color %s: %v", color, err)
	}
	return rgb, wrappedError
}
func (pw *Writer) ProcessSpans(spans []*atempl.Span, formattedSpans *FormattedSpans, sliceRanges []*SliceRange, sb *strings.Builder, outerFormat *Format) (*FormattedSpans, []*SliceRange, *strings.Builder) {
	for i, span := range spans {
		sr := new(SliceRange)
		sr.Format = pw.GetFormat("span", span.Class, outerFormat)
		if sr.Format.FontName == "" {
			sr.Format.FontName = span.UsedLanguage
		}
		sr.StrFrom = sb.Len()
		sr.RuneFrom = utf8.RuneCountInString(sb.String())
		sr.OriginalSpan = span

		if span.Anchor != nil {
			if span.Value == nil {
				span.Value = span.Anchor.Label.Value
				sr.Format = pw.GetFormat("a", span.Anchor.Class, outerFormat)
			}
		}

		if span.Value != nil {
			formattedSpans.Formats = append(formattedSpans.Formats, sr.Format)
			formattedSpans.FormatBreaks = append(formattedSpans.FormatBreaks, sr.RuneFrom)

			// handle 1st line indent
			indentAddition := 0.0
			if !legacyIndent {
				if r, exists := pw.MetaPdfData.CSS.GetRule("p." + outerFormat.Class); exists {
					d := r.GetDeclaration("text-indent")
					if d != nil {
						d.NormalizeFloatAndUnit(pw.FontSize)
						indentAddition += d.Float
					}
				}
			}
			if i == 0 && !pw.AlreadyIndented && legacyIndent {
				if pw.currentParagraphFormat.TextIndent > 0 {
					for j := 0; j < int(pw.currentParagraphFormat.TextIndent); j++ {
						break
						sr.SubString = sr.SubString + indentProxy
					}
					//pw.AlreadyIndented = true
				}
			}
			sr.SubString = sr.SubString + *span.Value + " "
			sb.WriteString(sr.SubString)
			sr.StrTo = sb.Len()

			sr.SubRunes = []rune(sr.SubString)
			sr.RuneTo = utf8.RuneCountInString(sb.String())

			// append slice range
			sliceRanges = append(sliceRanges, sr)
		} else {
			formattedSpans, sliceRanges, sb = pw.ProcessSpans(span.ChildSpans, formattedSpans, sliceRanges, sb, sr.Format)
		}
	}
	return formattedSpans, sliceRanges, sb
}

type BorderSide struct {
	Color   RGB
	Style   string
	Width   float64
	Margin  float64
	Padding float64
}

// TODO: note that this overlaps with CssSelector
type Format struct {
	Alignment       string
	BackgroundColor RGB
	BorderColor     RGB
	BorderStyle     string
	BorderTop       BorderSide
	BorderLeft      BorderSide
	BorderRight     BorderSide
	BorderBottom    BorderSide
	BreakAfter      bool
	BreakBefore     bool
	Class           string
	Color           RGB
	Element         string
	Float           string
	FontName        string
	FontSize        float64
	RuleLength      float64 // for printing a section line
	RuleStyle       string  // for printing a section line
	RuleWidth       float64 // for printing a section line
	Style           string
	TextIndent      float64
}

// Copy copies current format to new instance and returns a pointer to it
func (f *Format) Copy() *Format {
	format := new(Format)
	format.Alignment = f.Alignment
	format.BackgroundColor = f.BackgroundColor
	format.Class = f.Class
	format.Color = f.Color
	format.Element = f.Element
	format.FontName = f.FontName
	format.FontSize = f.FontSize
	format.RuleLength = f.RuleLength
	format.RuleStyle = f.RuleStyle
	format.RuleWidth = f.RuleWidth
	format.Style = f.Style
	format.TextIndent = f.TextIndent
	format.Float = f.Float
	return format
}
func (f *Format) GetColor() (int, int, int) {
	return f.Color.Color()
}
func NewSpan(text string, format Format) *Span {
	span := new(Span)
	span.Format = format
	span.Text = text
	return span
}
func CssBodyLookup(pdf *atempl.PDF, attrib string) string {
	for _, dec := range pdf.CSS.RuleMap["body"].Declarations {
		if dec.Identifier == attrib {
			return dec.Value
		}
	}
	return "" //not found
}

// CssSelector stores all the pdf-applicable values for a css selector
// note that even though all of these are pdf applicable, not all of them are implmented.
// also note that most shorthands are ommited. it is the duty of the programmer to supply shorthand unpacking logic
// this list is based on https://developer.mozilla.org/en-US/docs/Web/CSS/Reference
// this selector was created for use with <body> and @page
type CssSelector struct {
	BackgroundColor RGB        //Supported [E]
	BorderTop       BorderSide //Supported [E]
	BorderBottom    BorderSide //Supported [E]
	BorderLeft      BorderSide //Supported [E]
	BorderRight     BorderSide //Supported [E]
	BreakAfter      string
	BreakBefore     string
	Color           RGB //Supported [E]
	// Though ColumnCount makes so much sense, I have decided not to add it
	// this is because columns behave differently in a monolingual layout
	ColumnCount          int
	ColumnGap            float64 //the gutter width
	ColumnRule           BorderSide
	FontFamily           string //TODO: do we support the full list
	FontKerning          string
	FontLanguageOverride string
	FontSize             float64 //TODO: typedef points?
	FontStretch          string
	FontStyle            string //Supported [E]
	//FontVariant not implemented because it complicates runes
	FontWeight string //Supported [E]
	//The grid properties are ommited, as our pdf generator does not support div based layouts
	Height              float64
	HyphenateCharacter  rune
	HyphenateLimitChars int
	Hyphens             string
	//TODO: image support?
	InitialLetter string  //note that this can be 'normal' or a float followed by an optional int. manually convert
	LineHeight    float64 //supported [E]
	LineBreak     string
	//TODO: list-style?
	MarginTop               float64
	MarginBottom            float64
	MarginLeft              float64
	MarginRight             float64
	Orphans                 int
	PaddingTop              float64
	PaddingBottom           float64
	PaddingRight            float64
	PaddingLeft             float64
	Page                    string
	PrintColorAdjust        string
	RowGap                  float64
	RubyPosition            string
	TextAlign               string
	TextAlignLast           string
	TextDecorationLine      string //TODO: support underline
	TextDecorationColor     RGB
	TextDecorationStyle     string
	TextDecorationThickness string
	TextIndent              float64 //partial support [E]
	TextJustify             string
	TextTransform           string
	TextUnderlineOffset     float64
	TextUnderlinePosition   float64
	TextWrap                string
	Widows                  int

	/* NON-STANDARD PROPERTIES */
	//page-break-threshold. Maximum lines before page break triggered in element with break-before

}

func (sel *CssSelector) AddAttrib(w *Writer, name string, value string) {
	switch name {
	case "background-color":
		sel.BackgroundColor = GetCssColor(value, "#ffffff")
	case "border":
		borderStyle := GetCssLine(w, value, BorderSide{})
		openBorder := BorderSide{}
		//determine if it is default
		if sel.BorderRight == openBorder {
			sel.BorderRight = borderStyle
		}
		if sel.BorderLeft == openBorder {
			sel.BorderLeft = borderStyle
		}
		if sel.BorderTop == openBorder {
			sel.BorderTop = borderStyle
		}
		if sel.BorderBottom == openBorder {
			sel.BorderBottom = borderStyle
		}
	case "border-right":
		sel.BorderRight = GetCssLine(w, value, BorderSide{})
	case "border-left":
		sel.BorderLeft = GetCssLine(w, value, BorderSide{})
	case "border-top":
		sel.BorderTop = GetCssLine(w, value, BorderSide{})
	case "border-bottom":
		sel.BorderBottom = GetCssLine(w, value, BorderSide{})
	case "break-after":
		sel.BreakAfter = value
	case "break-before":
		sel.BreakBefore = value
	case "color":
		sel.Color = GetCssColor(value, "#000000")
	case "column-gap":
		sel.ColumnGap, _ = GetCssLength(w, value, 1.2)
	case "column-rule":
		sel.ColumnRule = GetCssLine(w, value, BorderSide{})
	case "font-family":
		sel.FontFamily = value
	case "font-kerning":
		sel.FontKerning = value
	case "font-language-override":
		sel.FontLanguageOverride = value
	case "font-size":
		defaultSize := 12.0
		if strings.HasSuffix(value, "%") {
			size, err := strconv.ParseFloat(value[0:len(value)-1], 64)
			if err == nil {
				sel.FontSize = w.FontSize * (size / 100)
			} else {
				sel.FontSize = defaultSize
			}
		} else if strings.HasSuffix(value, "rem") {
			size, err := strconv.ParseFloat(value[0:len(value)-2], 64)
			if err == nil {
				sel.FontSize = w.FontSize * size
			} else {
				sel.FontSize = w.FontSize
			}
		} else if strings.HasSuffix(value, "pt") {
			size, err := strconv.ParseFloat(value[0:len(value)-2], 64)
			if err == nil {
				sel.FontSize = size
			} else {
				sel.FontSize = w.FontSize
			}
		} else {
			switch strings.ToLower(value) {
			case "x-small":
				sel.FontSize = 7.5
			case "small":
				sel.FontSize = 10
			case "medium":
				sel.FontSize = 12
			case "large":
				sel.FontSize = 13.5
			case "x-large":
				sel.FontSize = 18
			case "xx-large":
				sel.FontSize = 24
			default:
				sel.FontSize = defaultSize
			}
		}
	case "font-stretch":
		sel.FontStretch = value
	case "font-style":
		sel.FontStyle = value
	case "font-weight":
		sel.FontWeight = value
	case "height":
		sel.Height, _ = GetCssLength(w, value, 20)
	case "hyphenate-character":
		sel.HyphenateCharacter, _ = utf8.DecodeRuneInString(value)
	case "hyphenate-limitchars":
		val, err := strconv.ParseUint(value, 10, 64)
		if err != nil {
			sel.HyphenateLimitChars = 8
			break
		}
		sel.HyphenateLimitChars = int(val)
	case "hyphens":
		sel.Hyphens = value
	case "initial-letter":
		sel.InitialLetter = value
	case "line-height":
		fallback := w.BodyStyle.LineHeight
		if fallback <= 0 {
			fallback = 1.2 * w.FontSize
		}
		sel.LineHeight, _ = GetCssLength(w, value, fallback)
	case "line-break":
		sel.LineBreak = value
	case "margin":
		if sel.MarginLeft == 0.0 { //TODO: what if we want a zero margin
			sel.MarginLeft, _ = GetCssLength(w, value, 1)
		}
		if sel.MarginRight == 0.0 { //TODO: what if we want a zero margin
			sel.MarginRight, _ = GetCssLength(w, value, 1)
		}
		if sel.MarginTop == 0.0 { //TODO: what if we want a zero margin
			sel.MarginTop, _ = GetCssLength(w, value, 1)
		}
		if sel.MarginBottom == 0.0 { //TODO: what if we want a zero margin
			sel.MarginBottom, _ = GetCssLength(w, value, 1)
		}
	case "margin-right":
		sel.MarginRight, _ = GetCssLength(w, value, 1)
	case "margin-left":
		sel.MarginLeft, _ = GetCssLength(w, value, 1)
	case "margin-top":
		sel.MarginTop, _ = GetCssLength(w, value, 1)
	case "margin-bottom":
		sel.MarginBottom, _ = GetCssLength(w, value, 1)
	case "orphans":
		tmp, err := strconv.ParseUint(value, 10, 64)
		if err == nil {
			sel.Orphans = int(tmp)
		}
	case "padding":
		if sel.PaddingRight == 0 {
			sel.PaddingRight, _ = GetCssLength(w, value, 0)
		}
		if sel.PaddingLeft == 0 {
			sel.PaddingLeft, _ = GetCssLength(w, value, 0)
		}
		if sel.PaddingTop == 0 {
			sel.PaddingTop, _ = GetCssLength(w, value, 0)
		}
		if sel.PaddingBottom == 0 {
			sel.PaddingBottom, _ = GetCssLength(w, value, 0)
		}
	case "padding-right":
		sel.PaddingRight, _ = GetCssLength(w, value, 0)
	case "padding-left":
		sel.PaddingLeft, _ = GetCssLength(w, value, 0)
	case "padding-top":
		sel.PaddingTop, _ = GetCssLength(w, value, 0)
	case "padding-bottom":
		sel.PaddingBottom, _ = GetCssLength(w, value, 0)
	case "page":
		sel.Page = value
	case "print-color-adjust":
		sel.PrintColorAdjust = value
	case "row-gap":
		sel.RowGap, _ = GetCssLength(w, value, 1)
	case "ruby-position":
		sel.RubyPosition = value
	case "text-align":
		sel.TextAlign = value
	case "text-align-last":
		sel.TextAlignLast = value
	case "text-decoration-line":
		sel.TextDecorationLine = value
	case "text-generation-color":
		sel.TextDecorationColor = GetCssColor(value, "#000000")
	case "text-decoration-style":
		sel.TextDecorationStyle = value
	case "text-decoration-thickness":
		sel.TextDecorationThickness = value
	case "text-indent":
		sel.TextIndent, _ = GetCssLength(w, value, 5)
	case "text-justify":
		sel.TextJustify = value
	case "text-transform":
		sel.TextTransform = value
	case "text-underline-offset":
		sel.TextUnderlineOffset, _ = GetCssLength(w, value, 0)
	case "text-underline-position":
		sel.TextUnderlinePosition, _ = GetCssLength(w, value, 0)
	case "text-wrap":
		sel.TextWrap = value
	case "widows":
		tmp, err := strconv.ParseUint(value, 10, 64)
		if err == nil {
			sel.Widows = int(tmp)
		}
	}
}

func (selector *CssSelector) setupFallbacks() {
	emptySelector := CssSelector{}
	if selector.FontSize == emptySelector.FontSize {
		selector.FontSize = config.SM.FloatProps[properties.SiteBuildPdfFontSize]
	}
	if selector.MarginLeft == emptySelector.MarginLeft {
		selector.MarginLeft = config.SM.FloatProps[properties.SiteBuildPdfPageMarginLeftWidth]
	}
	if selector.MarginRight == emptySelector.MarginRight {
		selector.MarginRight = config.SM.FloatProps[properties.SiteBuildPdfPageMarginRightWidth]
	}
	if selector.MarginTop == emptySelector.MarginTop {
		selector.MarginTop = config.SM.FloatProps[properties.SiteBuildPdfPageMarginTopWidth]
	}
	if selector.MarginBottom == emptySelector.MarginBottom {
		selector.MarginBottom = config.SM.FloatProps[properties.SiteBuildPdfPageMarginTopWidth]
	}
	if selector.ColumnGap == emptySelector.ColumnGap {
		selector.ColumnGap = config.SM.FloatProps[properties.SiteBuildPdfPageGutterWidth]
	}
	if selector.LineHeight == emptySelector.LineHeight {
		selector.LineHeight = config.SM.FloatProps[properties.SiteBuildPdfPageLineHeight]
	}
}
func NewPdfWriter(pdf *gofpdf.Fpdf,
	config *models.BuildConfig,
	metaPdfData *atempl.PDF,
	acronym string,
	layoutIndex int,
	rows []*atempl.MetaRow,
	msgChan chan string,
	relayMuted bool, // if true, relay is silenced
	relayUsesStandardOut bool) (*Writer, error) {
	pw := new(Writer)
	pw.Pdf = pdf
	pw.RelayUsesStandardOut = relayUsesStandardOut
	pw.RelayMuted = relayMuted
	pw.MsgChan = msgChan
	pw.Acronym = acronym
	pw.LangCodes = strings.Split(acronym, "-")
	if len(metaPdfData.FirstHeaders) > 0 {
		pw.HasHeaderFirst = true
	}
	if len(metaPdfData.FirstFooters) > 0 {
		pw.HasFooterFirst = true
	}
	if bsel, exists := metaPdfData.CSS.RuleMap["body"]; exists {
		for _, attrib := range bsel.Declarations {
			pw.BodyStyle.AddAttrib(pw, attrib.Identifier, attrib.Value)
		}
	}
	pw.BodyStyle.setupFallbacks()

	pw.LayoutIndex = layoutIndex
	if len(rows) == 0 {
		return nil, fmt.Errorf("%s: len(rows) == 0", acronym)
	}
	//if len(rows[0].Cells) == 0 {
	//	return nil, fmt.Errorf("%s: pkg/pdf.NewPdfWriter - len(rows[0].Cells)", acronym)
	//}
	pw.NumberOfVersions = len(pw.LangCodes)
	pw.Title = metaPdfData.Title
	pw.FontSize = pw.BodyStyle.FontSize
	if pw.FontSize == 0 {
		pw.FontSize = 12.0
	}
	pw.PageWidth, pw.PageHeight = pw.Pdf.GetPageSize()
	/*
		pw.LeftMargin = pw.BodyStyle.MarginLeft
		pw.RightMargin = pw.BodyStyle.MarginRight
		pw.TopMargin = pw.BodyStyle.MarginTop
		pw.BottomMargin = pw.BodyStyle.MarginBottom
	*/

	if psel, exists := metaPdfData.CSS.RuleMap["@page"]; exists {
		for _, attrib := range psel.Declarations {
			if attrib.Identifier == "margin" {
				marglen, _ := GetCssLength(pw, attrib.Value, 22)
				pw.TopMargin = marglen
				pw.RightMargin = marglen
				pw.BottomMargin = marglen
				pw.LeftMargin = marglen
			}
		}
		for _, attrib := range psel.Declarations {
			switch attrib.Identifier {
			case "margin-top":
				pw.TopMargin, _ = GetCssLength(pw, attrib.Value, 22)
			case "margin-left":
				pw.LeftMargin, _ = GetCssLength(pw, attrib.Value, 22)
			case "margin-right":
				pw.RightMargin, _ = GetCssLength(pw, attrib.Value, 22)
			case "margin-bottom":
				pw.BottomMargin, _ = GetCssLength(pw, attrib.Value, 22)
			}
		}
	}
	pw.Pdf.SetMargins(pw.LeftMargin, pw.TopMargin, pw.RightMargin)
	cellMargin := config.SM.FloatProps[properties.SiteBuildPdfPageMarginCellWidth]
	if cellMargin == 0 {
		cellMargin = 1 // disallow a zero value because used as a multiplier for determining linebreaks
	}
	pw.FooterY = pw.PageHeight - pw.BottomMargin
	pw.CellMargin = cellMargin
	pw.Pdf.SetCellMargin(cellMargin)
	pw.OriginalMultiLanguage = pw.NumberOfVersions > 1
	pw.MultiLanguage = pw.OriginalMultiLanguage
	pw.GutterWidth = pw.BodyStyle.ColumnGap
	pw.LineHeight = pw.BodyStyle.LineHeight
	pw.ParagraphSpacing = config.SM.FloatProps[properties.SiteBuildPdfParagraphSpacing]
	pw.ColumnsFromPreferences = config.SM.BoolProps[properties.SiteBuildPdfPageColumnsOn]
	pw.Columns = pw.ColumnsFromPreferences
	pw.HeaderLine = config.SM.BoolProps[properties.SiteBuildPdfPageHeaderLineOn]
	pw.MarginLine = config.SM.BoolProps[properties.SiteBuildPdfPageMarginLineOn]
	pw.GutterLine = config.SM.BoolProps[properties.SiteBuildPdfPageGutterLineOn]
	pw.FooterLine = config.SM.BoolProps[properties.SiteBuildPdfPageFooterLineOn]

	pw.ComputeColumnWidths() // also gets recomputed if printing a cover page (single version, single column)

	pageWidth, _ := pw.Pdf.GetPageSize()
	pw.CenterX = pageWidth / 2
	// override if monolingual and no columns
	if !pw.MultiLanguage && !pw.Columns {
		pw.GutterLine = false
	}
	pw.InitialPageNumber = metaPdfData.PageNbr
	pw.Pdf.SetPage(pw.InitialPageNumber)
	pw.Rows = rows
	pw.MetaPdfData = metaPdfData

	//if pw.HeaderLine {
	rgb, err := String2RGB(config.SM.StringProps[properties.SiteBuildPdfPageHeaderLineColor])
	if err != nil {
		return pw, fmt.Errorf("setting %s: %v", properties.SiteBuildPdfPageHeaderLineColor.Data().Path, err)
	}
	pw.SetHeaderLineConfig(config.SM.FloatProps[properties.SiteBuildPdfPageHeaderLineWidth], int(rgb.Red), int(rgb.Green), int(rgb.Blue))
	//}
	if pw.FooterLine {
		rgb, err := String2RGB(config.SM.StringProps[properties.SiteBuildPdfPageFooterLineColor])
		if err != nil {
			return pw, fmt.Errorf("setting %s: %v", properties.SiteBuildPdfPageFooterLineColor.Data().Path, err)
		}
		pw.SetFooterLineConfig(config.SM.FloatProps[properties.SiteBuildPdfPageFooterLineWidth], int(rgb.Red), int(rgb.Green), int(rgb.Blue))
	}
	if pw.Columns || pw.MultiLanguage {
		rgb, err := String2RGB(config.SM.StringProps[properties.SiteBuildPdfPageGutterLineColor])
		if err != nil {
			return pw, fmt.Errorf("setting %s: %v", properties.SiteBuildPdfPageGutterLineColor.Data().Path, err)
		}
		pw.SetColumnLineConfigs(config.SM.FloatProps[properties.SiteBuildPdfPageGutterLineWidth], int(rgb.Red), int(rgb.Green), int(rgb.Blue))
	}
	if pw.MarginLine {
		rgb, err := String2RGB(config.SM.StringProps[properties.SiteBuildPdfPageMarginLineColor])
		if err != nil {
			return pw, fmt.Errorf("setting %s: %v", properties.SiteBuildPdfPageMarginLineColor.Data().Path, err)
		}
		pw.SetLeftMarginLineConfig(config.SM.FloatProps[properties.SiteBuildPdfPageMarginLineWidth], int(rgb.Red), int(rgb.Green), int(rgb.Blue))
		pw.SetRightMarginLineConfig(config.SM.FloatProps[properties.SiteBuildPdfPageMarginLineWidth], int(rgb.Red), int(rgb.Green), int(rgb.Blue))
		pw.SetTopMarginLineConfig(config.SM.FloatProps[properties.SiteBuildPdfPageMarginLineWidth], int(rgb.Red), int(rgb.Green), int(rgb.Blue))
		pw.SetBottomMarginLineConfig(config.SM.FloatProps[properties.SiteBuildPdfPageMarginLineWidth], int(rgb.Red), int(rgb.Green), int(rgb.Blue))
	}
	pw.FontMap = config.PdfFontMap
	// We use the font path from "default" to tell fpdf where to find the fonts.
	pdf.SetFontLocation(pw.FontMap["default"].FontPath)
	// LoadRemote fonts from path.d
	// The keys will be "default" and iso language codes, e.g. "en", "gr", "spa", etc.
	for key, value := range pw.FontMap {
		pdf.AddUTF8Font(key, "", value.Regular)
		if pdf.Err() {
			return pw, pdf.Error()
		}
		pdf.AddUTF8Font(key, "B", value.Bold)
		if pdf.Err() {
			return pw, pdf.Error()
		}
		pdf.AddUTF8Font(key, "I", value.Italic)
		if pdf.Err() {
			return pw, pdf.Error()
		}
		pdf.AddUTF8Font(key, "BI", value.BoldItalic)
		if pdf.Err() {
			return pw, pdf.Error()
		}
	}

	// TODO: itterate through CSS
	for _, guts := range metaPdfData.CSS.FontFaceMap {
		fmt.Sprintf("%v", guts)
		// EXTRACT font-family
		ff := guts.GetDeclaration("font-family")
		src := guts.GetDeclaration("src")
		fontlist := strings.Split(src.Value, ",")
		// ITTERATE over extracted and search for BOLD, ITALIC, NORMAL, and BOLD ITALIC
		for _, fn := range fontlist {
			shards := strings.Split(fn, "'")
			for _, shard := range shards {
				if strings.HasSuffix(shard, ".ttf") {
					stylestring := ""
					if strings.Contains(shard, "Bold") {
						stylestring += "B"
					}
					if strings.Contains(shard, "Italic") {
						stylestring += "I"
					}
					if strings.HasPrefix(shard, "fonts/") {
						shard = shard[6:]
					}
					pw.Pdf.AddUTF8Font(strings.Trim(ff.Value, "\"'"), stylestring, shard)
					break
				}
			}
		}

	}

	// Set the initial font to "default".
	// When each cell of a table row is processed,
	// the font will be set for that language.
	pdf.SetFont("default", "", pw.FontSize)
	pw.SpacerWidthMillimeters = pw.Pdf.GetStringWidth(" ")
	// set the format for headers and footers
	pw.HeaderFormat = pw.GetFormat("p", "header", nil)
	pw.FooterFormat = pw.GetFormat("p", "footer", nil)

	// load the middle column adjusters
	if len(pw.LangCodes) > 2 {
		if font, ok := pw.FontMap[pw.LangCodes[1]]; ok {
			pw.MiddleColumnAdjuster = font.MiddleColAdjuster
		} else {
			pw.MiddleColumnAdjuster = 0.95
		}
	}

	pdf.AddPage()
	return pw, nil
}

// the following functions are for the interface

func (w *Writer) GetPdf() *gofpdf.Fpdf {
	return w.Pdf
}

func (w *Writer) GetColumnWidth() float64 {
	return w.ColumnWidth
}

// Relay sends on the msgChannel using a standard prefix.
// If msgChannel is nil, it won't attempt to send the message.
func (w *Writer) Relay(msg string) {
	if w.RelayMuted {
		return
	}
	if w.RelayUsesStandardOut {
		fmt.Printf("GM: %s\n", msg)
		return
	}
	if w.MsgChan != nil {
		w.MsgChan <- fmt.Sprintf("GM: %s", msg)
	}
}
