// Package ssesvr provides a server to publish messages using sse (Server Send Events)
// MAC: This is not being used. It works OK
//      if it is go client, but
//      not if it is a javascript client.
//      Here is what the javascript should be:
// 			this.evtSource = new EventSource(":8080/sse");
// 			this.evtSource.onmessage = function(ev) {
//   			console.log(ev.data)
// 			};
// 			this.evtSource.onopen = e => {
//   			console.log(e);
// 			}
// 			this.evtSource.addEventListener('clear', e => {
//   			console.log(e);
// 			});
// 			this.evtSource.addEventListener('gm', e => {
//   			console.log(e);
// 			});
// 			this.evtSource.addEventListener('pm', e => {
//   			console.log(e);
// 			});
package ssesvr

import (
	"fmt"
	"github.com/tmaxmax/go-sse"
	"log"
	"net/http"
	"os"
	"time"
)

type Server struct {
	port   string
	server *sse.Server
}

func NewServer(port string) (*Server, error) {
	s := new(Server)
	s.port = port
	s.server = sse.NewServer()
	var err error
	go (func(port string) {
		err = http.ListenAndServe(fmt.Sprintf(":%s", port), s.server)
	})(port)
	return s, err
}
func (s *Server) Message(text string) error {
	m := sse.Message{}
	m.AppendText(text)
	err := s.server.Publish(&m)
	if err != nil {
		if err != nil {
			fmt.Printf("sse message error: %v", err)
			return fmt.Errorf("error sending text %s: %v", text, err)
		}
	}
	return nil
}
func (s *Server) Publish(topic, text string) error {
	m := sse.Message{}
	m.SetName(topic)
	m.AppendText(text)
	err := s.server.Publish(&m)
	if err != nil {
		if err != nil {
			fmt.Printf("sse publish error: %v", err)
			return fmt.Errorf("%s: error sending text %s: %v", topic, text, err)
		}
	}
	return nil
}
func (s *Server) Shutdown() error {
	return s.server.Shutdown()
}
func (s *Server) Timer() {
	out := log.New(os.Stdout, "", 0)
	go testClient(s.port, out)
	for {
		timer := time.NewTimer(2 * time.Second)
		<-timer.C
		m := sse.Message{}
		m.SetName("time")
		dt := time.Now()
		m.AppendText(dt.String())
		err := s.server.Publish(&m)
		if err != nil {
			out.Println(err)
		}
		out.Printf("<- %s", dt.String())
	}
}
func testClient(port string, out *log.Logger) {

	r, err := http.NewRequest(http.MethodGet, fmt.Sprintf("http://localhost:%s", port), nil)
	if err != nil {
		fmt.Println(err)
		return
	}
	conn := sse.NewConnection(r)

	conn.SubscribeToAll(func(event sse.Event) {
		switch event.Name {
		case "time":
			out.Printf("-> %s: %s\n", event.Name, event)
		default: // no event name
			out.Printf("-> %s", event)
		}
	})

	if err = conn.Connect(); err != nil {
		out.Println(err)
	}
}
