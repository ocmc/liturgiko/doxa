package ssesvr

import (
	"fmt"
	"testing"
)

func TestHello(t *testing.T) {
	fmt.Println("hi")
}
func TestNewServer(t *testing.T) {
	fmt.Println("creating server")
	s, err := NewServer("9000")
	if err != nil {
		t.Errorf("error creating sse server: %v", err)
		return
	}
	fmt.Println("server started")
	for i := 0; i < 10; i++ {
		err = s.Publish("greetings", "hi")
		if err != nil {
			t.Errorf("error publishing: %v", err)
			return
		}
		fmt.Println("message sent")
	}
	err = s.Shutdown()
	if err != nil {
		t.Errorf("error shutting down: %v", err)
		return
	}
	fmt.Println("server stopped")
}
func TestServer_IamAlive(t *testing.T) {
	fmt.Println("creating server")
	s, err := NewServer("9000")
	if err != nil {
		t.Errorf("error creating sse server: %v", err)
		return
	}
	fmt.Println("starting timer")
	s.Timer()
}
