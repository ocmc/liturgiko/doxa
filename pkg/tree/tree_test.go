package tree

import (
	"html/template"
	"os"
	"testing"
)

func TestBuildTree(t *testing.T) {
	table := [][]string{
		{"eu", "baptism", "html", "en", "0"},
		{"eu", "baptism", "html", "gr-en", "1"},
		{"eu", "baptism", "pdf", "en-gr", "2"},
		{"eu", "baptism", "pdf", "en", "3"},
		{"eu", "li", "chrys", "html", "en", "4"},
		{"eu", "li", "chrys", "html", "gr-en", "5"},
		{"eu", "li", "chrys", "pdf", "en-gr", "6"},
		{"eu", "li", "chrys", "pdf", "en", "7"},
		{"eu", "li", "basil", "html", "en", "8"},
		{"eu", "li", "basil", "html", "gr-en", "9"},
		{"eu", "li", "basil", "pdf", "en-gr", "10"},
		{"eu", "li", "basil", "pdf", "en", "11"},
	}
	tree := Build(table)
	//PrintTree(tree, 0)
	// Parse the templates
	tmpl, err := template.ParseFiles("tree.gohtml")
	if err != nil {
		panic(err)
	}

	// Execute the root template with the tree structure
	err = tmpl.ExecuteTemplate(os.Stdout, "root", tree)
	if err != nil {
		panic(err)
	}
}
