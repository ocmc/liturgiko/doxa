// Package tree converts a 2-d string slice [][]string into a tree, collapsing duplicate values into a single node.
package tree

import (
	"fmt"
	"strings"
)

type Node struct {
	Value    string
	Children []*Node
}

func addNode(root *Node, value string) *Node {
	// Check if the node already exists among the children
	for _, child := range root.Children {
		if child.Value == value {
			return child
		}
	}

	// If the node does not exist, create a new one and add it to the children
	newNode := &Node{Value: value}
	root.Children = append(root.Children, newNode)
	return newNode
}

func Build(table [][]string) *Node {
	root := &Node{Value: "root"} // Starting point of the tree

	for _, row := range table {
		currentNode := root
		for _, value := range row {
			currentNode = addNode(currentNode, value)
		}
	}

	return root
}

func PrintTree(node *Node, level int) {
	if node == nil {
		return
	}

	fmt.Printf("%s%s\n", " "+strings.Repeat("-", level*2), node.Value)
	for _, child := range node.Children {
		PrintTree(child, level+1)
	}
}
