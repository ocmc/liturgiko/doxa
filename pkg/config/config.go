// Package config provides a means to initialize a config file for the doxago cli
package config

import (
	"errors"
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/enums/goos"
	"github.com/liturgiko/doxa/pkg/enums/properties"
	"github.com/liturgiko/doxa/pkg/enums/propertyValueTypes"
	"github.com/liturgiko/doxa/pkg/inOut"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"github.com/liturgiko/doxa/pkg/utils/stack"
	"os"
	"path"
	"runtime"
	"strconv"
	"strings"
)

const (
	AlwbDir          = "alwb"
	AppCss           = "app.css"
	AssetsDir        = "assets"
	AssetsRootDir    = "rootFiles"
	AtemDir          = "atem"
	BackupDir        = "backups"
	BinDir           = "bin"
	BtxDir           = "btx" // a database bucket and exports to resources/btx repo
	CatalogDir       = "catalog"
	CatalogFile      = "catalog.json"
	InventoryFile    = "inventory.json"
	ClipboardDir     = "clipboard"
	ConfigsDir       = "configs"
	CssDir           = "css"
	DatabaseDir      = "db"
	ResourcesDir     = "resources"
	DbName           = "liturgical.db"
	DoxaYaml         = "config.yaml"
	ExportDir        = "exports"
	HomeDir          = "doxa"
	HyphenDir        = "hyphenPatterns"
	ImportDir        = "imports"
	LogDir           = "logs"
	LogFile          = "doxa.log"
	LmlDir           = "lml"
	LtxDir           = "ltx"   // a database bucket and exports to resources/ltx repo
	MediaDir         = "media" // a database bucket and exports to resources/media repo
	MetaTemplatesDir = "meta-templates"
	PdfCssDir        = "css"
	PdfDir           = "pdf"
	PdfCssFile       = "pdf.css"
	ProjectsDir      = "projects"
	RemoteGit        = "gitlab.com"
	WebsitesDir      = "websites"
	WebsitePublicDir = "public"
	WebsiteTestDir   = "test"
	SiteRoot         = "dcs"
	SubscriptionsDir = "subscriptions"
	SysDir           = "sys"
	SysHiddenDir     = ".sys"
	SystemDbName     = ".sys.db"
	UsersDbName      = ".users.db"
	TempDir          = "temp"
	TemplatesDir     = "templates"
	Vault            = ".vault"
	SiteBuildLayouts = "/site/build/layouts/values"
)

var DoxaPaths *Paths

type RemoteMeta struct {
	// call the Http method to get URL prefixed with https, etc.
	Url             string // gitlab url, e.g. gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_dedes
	UrlSubGroupPath string
	DbPath          string // doxa db path, e.g. ltx/en_us_dedes
	FsPath          string // file system path to dir with .git, e.g. ~/doxa/projects/doxa-eac/subscriptions/doxa-seraphimdedes/resources/en_us_dedes
	GroupName       string // same as doxa project name
	RepoName        string // aka Gitlab Project.
}

// Https prefixes the RemoteMeta Url with https:// and suffixes it with .git
func (rm *RemoteMeta) Https() string {
	return fmt.Sprintf("https://%s.git", rm.Url)
}
func (rm *RemoteMeta) GetRemoteMeta(repo string) (m *RemoteMeta) {
	//p.UrlPaths.ResourcesLtx = &RemoteMeta{
	//	Url:             path.Join(RemoteGit, p.Project, ResourcesDir, LtxDir),
	//	UrlSubGroupPath: path.Join(ResourcesDir, LtxDir),
	//	DbPath:          LtxDir,
	//	FsPath:          path.Join(p.ResourcesPath, LtxDir),
	//	GroupName:       p.Project,
	//	RepoName:        LtxDir,
	//}
	m = new(RemoteMeta)
	m.GroupName = rm.GroupName
	m.Url = path.Join(rm.Url, repo)
	m.UrlSubGroupPath = rm.UrlSubGroupPath
	m.DbPath = path.Join(rm.DbPath, repo)
	m.FsPath = path.Join(rm.FsPath, repo)
	m.RepoName = repo
	return
}

type UrlPaths struct {
	Assets           *RemoteMeta
	PrimaryTemplates *RemoteMeta
	ResourcesConfig  *RemoteMeta
	ResourcesBtx     *RemoteMeta
	ResourcesLtx     *RemoteMeta
	ResourcesMedia   *RemoteMeta
}

type Paths struct {
	AssetsPath            string
	AssetsRootPath        string
	AtemPath              string // AGES atem files
	BackupPath            string
	BinPath               string
	CatalogPath           string
	CatalogFile           string
	InventoryFile         string
	ClipboardPath         string
	ConfigsPath           string
	DbPath                string
	DbPathUsers           string
	DbPathSystem          string
	DoxaHomePath          string
	ResourcesPath         string
	DotDoxa               string
	DoxaConfigPath        string
	ExportPath            string
	FallbackTemplatesPath string
	HtmlCss               string
	HomePath              string
	HTTPPath              string
	HyphenPath            string
	ImportPath            string
	LogPath               string
	LogFile               string
	MediaDirPath          string
	MetaTemplatesPath     string
	PathSeparator         string
	PdfCss                string
	PreviewPath           string
	Project               string
	ProjectDirPath        string // gets set = to either SiteGenPath or SiteTestPath
	ProjectsDirPath       string
	PrimaryTemplatesPath  string
	ReferenceDir          string
	SeedPath              string
	SiteGenPath           string
	SiteGenPublicPath     string
	SiteGenTestPath       string
	SubscriptionsPath     string
	SysHiddenPath         string
	SysPath               string // AGES ares system files
	TempDirPath           string
	TempDirAlwbPath       string
	TempDirLmlPath        string
	TopicKeyDir           string
	UrlPaths              *UrlPaths
	UserHomePath          string
	VaultPath             string
}

func InitDoxaPaths(gitlabUrl, userHome, doxaHome, project string) error {
	DoxaPaths = new(Paths)

	err := DoxaPaths.SetPaths(gitlabUrl, userHome, doxaHome, project)
	if err != nil {
		msg := fmt.Sprintf("error setting doxa paths: %v", err)
		doxlog.Errorf(msg)
		return fmt.Errorf("%v", err)
	}
	return nil
}
func (p *Paths) GetTemplateSubscriptionGroupId() (string, error) {
	if len(p.FallbackTemplatesPath) == 0 {
		return "", fmt.Errorf("Paths.FallBackTemplatesPath not set")
	}
	subscriptionDirs := path.Dir(p.FallbackTemplatesPath)
	parts := strings.Split(subscriptionDirs, "/")
	var groupName string
	if len(parts) > 2 {
		groupName = parts[len(parts)-1]
		return groupName, nil
	} else {
		return "", fmt.Errorf("could not deterimine group ID in %s", subscriptionDirs)
	}
}
func (p *Paths) MoveSubscriptionsDirToProject() error {
	projectsDir := path.Join(p.HomePath, ProjectsDir)
	err := ltfile.CreateDirs(projectsDir)
	if err != nil {
		return err
	}
	oldLocation := path.Join(p.HomePath, SubscriptionsDir)
	// return nil if subscriptions already moved from doxa home/subscriptions
	if exists := ltfile.DirExists(oldLocation); !exists {
		return nil
	}
	var dirs []string
	// get a list of the project groups in the projects Directory
	dirs, err = ltfile.DirsInDir(projectsDir, true)
	if err != nil {
		msg := fmt.Sprintf("could not determine projects in project dir %s: %v", projectsDir, err)
		doxlog.Errorf(msg)
		return fmt.Errorf("%v", msg)
	}
	// copy from doxa home/subscriptions to projects/{project}/subscriptions
	for _, d := range dirs {
		//projectDir := filepath.Join(projectsDir, d)
		// but, skip if subscriptions already is there
		//if exists := ltfile.DirExists(filepath.Join(projectDir, SubscriptionsDir)); exists {
		//	continue
		//}
		to := path.Join(projectsDir, d, SubscriptionsDir)
		doxlog.Infof("moving %s to %s: %v", oldLocation, to, err)
		err = ltfile.CopyDir(oldLocation, to)
		if err != nil {
			msg := fmt.Sprintf("could not move subscriptions dir from %s to %s: %v", oldLocation, to, err)
			doxlog.Errorf(msg)
			return fmt.Errorf("%v", msg)
		}
	}
	return nil
}
func (p *Paths) SetSubscriptionsDirPath() {
	p.SubscriptionsPath = path.Join(p.ProjectDirPath, SubscriptionsDir)
	p.FallbackTemplatesPath = path.Join(p.SubscriptionsPath, TemplatesDir)
}
func (p *Paths) SetPaths(gitlabUrl, userHome, doxaHome, project string) error {
	p.HomePath = doxaHome
	p.Project = project
	p.UserHomePath = userHome
	// DotDoxa paths
	p.DotDoxa = path.Join(userHome, ".doxa")
	p.DbPathUsers = path.Join(p.DotDoxa, UsersDbName)
	p.DbPathSystem = path.Join(p.DotDoxa, SysDir, p.DbPathSystem)
	p.DoxaConfigPath = path.Join(p.DotDoxa, DoxaYaml)
	p.SeedPath = path.Join(p.DotDoxa, ".seed")
	// Doxa home paths
	p.DoxaHomePath = doxaHome

	p.TempDirPath = path.Join(p.HomePath, TempDir)
	p.TempDirAlwbPath = path.Join(p.TempDirPath, AlwbDir)
	p.TempDirLmlPath = path.Join(p.TempDirPath, LmlDir)

	p.ProjectsDirPath = path.Join(p.HomePath, ProjectsDir)
	p.ProjectDirPath = path.Join(p.ProjectsDirPath, p.Project)

	p.BackupPath = path.Join(p.ProjectDirPath, BackupDir)
	p.BinPath = path.Join(p.HomePath, BinDir)
	p.CatalogPath = path.Join(p.ProjectDirPath, CatalogDir)
	p.CatalogFile = path.Join(p.CatalogPath, CatalogFile)
	p.InventoryFile = path.Join(p.CatalogPath, InventoryFile)
	p.SetSubscriptionsDirPath()
	p.SysHiddenPath = path.Join(p.ProjectDirPath, SysHiddenDir)
	p.MetaTemplatesPath = path.Join(p.ProjectDirPath, SysHiddenDir, MetaTemplatesDir)
	p.SiteGenPath = path.Join(p.ProjectDirPath, WebsitesDir)
	p.SetWebSitePaths(SiteRoot) // can be changed at generation time
	p.AssetsPath = path.Join(p.ProjectDirPath, AssetsDir)
	p.AssetsRootPath = path.Join(p.AssetsPath, AssetsRootDir)
	p.HtmlCss = path.Join(p.AssetsPath, CssDir, AppCss)
	p.PdfCss = path.Join(p.AssetsPath, PdfDir, PdfCssDir, PdfCssFile)
	p.ClipboardPath = path.Join(p.ProjectDirPath, ClipboardDir)
	p.DbPath = path.Join(p.ProjectDirPath, DatabaseDir, DbName)
	runtimeOs := goos.CodeForString(runtime.GOOS)
	if runtimeOs == goos.Linux {
		p.DotDoxa = path.Join(userHome, "doxa")
	} else {
		p.DotDoxa = path.Join(userHome, ".doxa")
	}
	p.DoxaConfigPath = path.Join(p.DotDoxa, DoxaYaml)
	p.ExportPath = path.Join(p.ProjectDirPath, ExportDir)
	p.ImportPath = path.Join(p.ProjectDirPath, ImportDir)
	p.ConfigsPath = path.Join(p.ProjectDirPath, ConfigsDir)
	p.HyphenPath = path.Join(p.AssetsPath, HyphenDir)
	p.LogPath = path.Join(p.ProjectDirPath, LogDir)
	p.LogFile = path.Join(p.LogPath, LogFile)
	p.ResourcesPath = path.Join(p.ProjectDirPath, ResourcesDir)
	p.PrimaryTemplatesPath = path.Join(p.ProjectDirPath, TemplatesDir)
	p.AtemPath = path.Join(p.ResourcesPath, AtemDir)
	p.SeedPath = path.Join(p.DotDoxa, ".seed")
	p.SysPath = path.Join(p.ResourcesPath, SysDir)
	p.VaultPath = path.Join(p.DotDoxa, Vault)
	p.PathSeparator = "/"

	var paths []string // used to automatically create dirs that do not exist
	//paths = append(paths, p.BackupPath)
	//paths = append(paths, p.ClipboardPath)
	//paths = append(paths, p.ExportPath)
	//paths = append(paths, p.LogPath)
	//paths = append(paths, p.ResourcesPath)
	//paths = append(paths, p.SiteGenPublicPath)
	//paths = append(paths, p.SiteGenTestPath)
	paths = append(paths, p.SubscriptionsPath)

	for _, thePath := range paths {
		p.createDirIfNotExist(thePath)
	}
	p.SetUrlsPaths(gitlabUrl)
	return nil
}
func (p *Paths) SetUrlsPaths(gitlabUrl string) {
	if strings.HasPrefix(gitlabUrl, "https://") {
		gitlabUrl = strings.TrimPrefix(gitlabUrl, "https://")
	}
	p.UrlPaths = new(UrlPaths)
	// Assets doxa/projects/{project}/assets
	p.UrlPaths.Assets = &RemoteMeta{
		Url:             path.Join(gitlabUrl, p.Project, AssetsDir),
		UrlSubGroupPath: "",
		DbPath:          "",
		FsPath:          p.AssetsPath,
		GroupName:       p.Project,
		RepoName:        AssetsDir,
	}
	// Primary templates doxa/projects/{project}/templates
	p.UrlPaths.PrimaryTemplates = &RemoteMeta{
		Url:             path.Join(gitlabUrl, p.Project, TemplatesDir),
		UrlSubGroupPath: "",
		DbPath:          "",
		FsPath:          p.PrimaryTemplatesPath,
		GroupName:       p.Project,
		RepoName:        TemplatesDir,
	}
	// resources/config doxa/projects/{project}/resources/config
	p.UrlPaths.ResourcesConfig = &RemoteMeta{
		Url:             path.Join(gitlabUrl, p.Project, ResourcesDir, ConfigsDir),
		UrlSubGroupPath: path.Join(ResourcesDir, ConfigsDir),
		DbPath:          ConfigsDir,
		FsPath:          path.Join(p.ResourcesPath, ConfigsDir),
		GroupName:       p.Project,
		RepoName:        ConfigsDir,
	}
	// resources/btx doxa/projects/{project}/resources/btx
	p.UrlPaths.ResourcesBtx = &RemoteMeta{
		Url:             path.Join(gitlabUrl, p.Project, ResourcesDir, BtxDir),
		UrlSubGroupPath: path.Join(ResourcesDir, BtxDir),
		DbPath:          BtxDir,
		FsPath:          path.Join(p.ResourcesPath, BtxDir),
		GroupName:       p.Project,
		RepoName:        BtxDir,
	}
	// resources/ltx doxa/projects/{project}/resources/ltx
	p.UrlPaths.ResourcesLtx = &RemoteMeta{
		Url:             path.Join(gitlabUrl, p.Project, ResourcesDir, LtxDir),
		UrlSubGroupPath: path.Join(ResourcesDir, LtxDir),
		DbPath:          LtxDir,
		FsPath:          path.Join(p.ResourcesPath, LtxDir),
		GroupName:       p.Project,
		RepoName:        LtxDir,
	}
	// resources/media doxa/projects/{project}/resources/media
	p.UrlPaths.ResourcesMedia = &RemoteMeta{
		Url:             path.Join(gitlabUrl, p.Project, ResourcesDir, MediaDir),
		UrlSubGroupPath: path.Join(ResourcesDir, MediaDir),
		DbPath:          MediaDir,
		FsPath:          path.Join(p.ResourcesPath, MediaDir),
		GroupName:       p.Project,
		RepoName:        MediaDir,
	}
}
func (p *Paths) RemoteMeta() (r *RemoteMeta) {
	r = new(RemoteMeta)
	return
}
func (p *Paths) SetWebSitePaths(root string) {
	r := ltfile.ToSysPath(root)
	p.SiteGenPublicPath = path.Join(p.SiteGenPath, WebsitePublicDir, r)
	p.SiteGenTestPath = path.Join(p.SiteGenPath, WebsiteTestDir, r)
	p.PreviewPath = path.Join(p.SiteGenPath, WebsiteTestDir, r, "t")
}
func (p *Paths) createDirIfNotExist(path string) {
	err := ltfile.CreateDirs(path)
	if err != nil {
		fmt.Printf("Error creating %s: %v\n", path, err)
	}
}
func (p *Paths) CreateSitePath(site string) string {
	return path.Join(p.ProjectsDirPath, site)
}

// TODO: review if needed
//func (p *Paths) InitDoxaConfigYaml() error {
//	if !ltfile.DirExists(p.HomePath) {
//		err := ltfile.CreateDirs(p.HomePath)
//		if err != nil {
//			return err
//		}
//	}
//	configPath := path.Join(p.HomePath, DoxaYaml)
//	// generate the config file
//	fmt.Printf("Creating the %s configuration file...\n", DoxaYaml)
//	t := template.Must(template.New("config").Parse(doxagoTmpl))
//	f, err := ltfile.Create(ltfile.ToSysPath(configPath))
//	if err != nil {
//		log.Println("create file: ", err)
//		return err
//	}
//	configData := new(DoxaConfigData)
//	configData.SiteName = p.Project
//	configData.PathSeparator = string(os.PathSeparator)
//	if err = t.Execute(f, configData); err != nil {
//		return err
//	}
//	return nil
//}

// InitDoxa Initializes the config file and directories
// for the doxago cli. If these already exist,
// they will not be overwritten.
//func (p *Paths) InitDoxa() (string, error) {
//	if !ltfile.DirExists(p.HomePath) {
//		err := ltfile.CreateDirs(p.HomePath)
//		if err != nil {
//			return "", err
//		}
//	}
//	if !ltfile.DirExists(p.LogPath) {
//		err := ltfile.CreateDirs(p.LogPath)
//		if err != nil {
//			return "", err
//		}
//	}
//	projectsDir := path.Join(p.HomePath, ProjectsDir)
//	if !ltfile.DirExists(projectsDir) {
//		err := ltfile.CreateDirs(projectsDir)
//		if err != nil {
//			return "", err
//		}
//	}
//	err := p.InitDoxaConfigYaml()
//	if err != nil {
//		return "", err
//	}
//	if !ltfile.DirExists(p.ProjectDirPath) {
//		fmt.Println("Cloning demonstration website files")
//		fmt.Println("Depending on your internet connection speed, this could take 15 minutes or more...")
//		url := "https://gitlab.com/ocmc/liml/site.git"
//		showProgress := true
//		deleteFirst := true
//		err := repos.CloneTo(p.ProjectDirPath, url, showProgress, deleteFirst, true)
//		if err != nil {
//			return "", fmt.Errorf("error cloning demo project from %s into %s: %v", url, p.ProjectDirPath, err)
//		}
//	}
//	err = p.RemoveGit()
//	if err != nil {
//		doxlog.Infof("could not remove git: %v", err)
//	}
//	// rewrite config.yaml file
//	yaml := `--- # document start
//# Settings for doxa.  These are the only ones set here.  All others are set in the database.
//# Name of the site folder to use in doxa/sites folder. You can have multiple website folders in your doxa/sites folder. But, the doxa commands work against only one site, known as the active site. Set the name of the active site here. Use a short name (without spaces).
//site: liml
//# Are you running in cloud mode? Cloud mode is used when doxa is available on the Internet.  Set this to false if you are using it locally.  If running in the cloud, users have to sign in.
//cloud: false
//--- # document end`
//	yamlFile := path.Join(p.HomePath, DoxaYaml)
//	_ = os.Remove(yamlFile) // ignore error if occurs
//	err = ltfile.WriteFile(yamlFile, yaml)
//	if err != nil {
//		doxlog.Infof("could not write %s file to %s:%v", DoxaYaml, yamlFile, err)
//	}
//	return fmt.Sprintf("doxa home directory is: %s\nConfig file is: %s. The demo folder contains all the sources to generate a website.  You can run doxago serve to view the demo website. See the doxa manual for instructions about how to create your own website (https://doxa.liml.org).", p.HomePath, p.DoxaConfigPath), nil
//}

// RemoveGit removes git directory and git related files from a project
//func (p *Paths) RemoveGit() error {
//	err := os.RemoveAll(path.Join(p.ProjectDirPath, ".git"))
//	if err != nil {
//		return err
//	}
//	err = os.Remove(path.Join(p.ProjectDirPath, ".gitignore"))
//	if err != nil {
//		return err
//	}
//	_ = os.Remove(path.Join(p.ProjectDirPath, ".gitlab-ci.yml"))
//	return nil
//}

// UpdateConfig reads in the config.yaml file and sets the site and site_url values
//func (p *Paths) UpdateConfig(site, url string) error {
//	lines, err := ltfile.GetFileLines(p.DoxaConfigPath)
//	if err != nil {
//		return err
//	}
//	newLines := lines
//	for i, line := range lines {
//		if strings.HasPrefix(line, "site:") {
//			newLines[i] = fmt.Sprintf("site: %s", site)
//		}
//		if strings.HasPrefix(line, "site_url:") {
//			newLines[i] = fmt.Sprintf("site_url: %s", url)
//		}
//	}
//	err = ltfile.WriteLinesToFile(p.DoxaConfigPath, newLines)
//	return err
//}

//type DoxaConfigData struct {
//	SiteName      string
//	PathSeparator string
//}

//const doxagoTmpl = `
//
//--- # document start
//# Settings for doxa.  These are the only ones set here.  All others are set in the database.
//# Name of the site folder to use in doxa/sites folder. You can have multiple website folders in your doxa/sites folder. But, the doxa commands work against only one site, known as the active site. Set the name of the active site here. Use a short name (without spaces).
//site: liml
//# Are you running in cloud mode? Cloud mode is used when doxa is available on the Internet.  Set this to false if you are using it locally.  If running in the cloud, users have to sign in.
//cloud: false
//--- # document end
//
//`
//const README = `
//Doxa provides tools for the translation of Eastern Orthodox Liturgical texts and the generation of liturgical websites.
//
//The sub-folders in the doxa folder are:
//
//bin - contains the binary (executable) doxa file
//clipboard - contains PDF or txt files when you clip records in the database.
//exports - contains exported records from the database.
//logs - contains information written by doxa that is useful in diagnosing issues
//sites - contains the information required to generate liturgical web sites and your generated site.
//
//You can have multiple sites in the sites folder. The config.yaml file is where you indicate which site to use.  Only one site can be used at a time.
//`

const (
	ConfigsPathPrefix = "configs/"
)

type SettingsManager struct {
	BoolProps            map[properties.Property]bool
	ConfigPath           string
	Dev                  bool // true if the config.yaml file had dev: true
	ExportPath           string
	FloatProps           map[properties.Property]float64
	ImportPath           string
	IntProps             map[properties.Property]int
	Kvs                  *kvs.KVS
	PM                   *kvs.PropertyManager
	StringMapStringProps map[properties.Property]map[string]string
	StringProps          map[properties.Property]string
	StringSliceProps     map[properties.Property][]string
}

var SM *SettingsManager

func InitSettingsManager(name string,
	exportPath string,
	importPath string,
	kvs *kvs.KVS,
	pm *kvs.PropertyManager,
	dev bool) {
	SM = new(SettingsManager)
	SM.Dev = dev
	SM.Kvs = kvs
	SM.PM = pm
	SM.ConfigPath = ConfigsPathPrefix + name
	SM.ExportPath = exportPath
	SM.ImportPath = importPath
	SM.BoolProps = make(map[properties.Property]bool)
	SM.FloatProps = make(map[properties.Property]float64)
	SM.IntProps = make(map[properties.Property]int)
	SM.StringProps = make(map[properties.Property]string)
	SM.StringSliceProps = make(map[properties.Property][]string)
	SM.StringMapStringProps = make(map[properties.Property]map[string]string)
}

// AvailableConfigs returns the database paths to each available config
func (s *SettingsManager) AvailableConfigs() ([]string, error) {
	var result []string
	s.PM.KP.Clear()
	err := s.PM.KP.ParsePath("configs")
	if err != nil {
		return nil, err
	}
	var configs []*kvs.KeyPath
	if configs, err = s.PM.KVS.Db.DirPaths(*s.PM.KP); err != nil {
		return nil, err
	}
	for _, c := range configs {
		if c.Dirs.Last() == "use" {
			continue
		}
		result = append(result, c.Path())
	}
	return result, nil
}
func (s *SettingsManager) UpdateConfiguration(thePath string) error {
	if !strings.HasPrefix(thePath, "configs") {
		return fmt.Errorf("not in configs: %s", thePath)
	}

	sep := "/"
	target := sep + "site" + sep
	sysDir := sep + "sys" + sep
	if strings.Contains(thePath, target) {
		parts := strings.Split(thePath, target)
		if len(parts) == 2 {
			thePath = path.Join(target, parts[1])
		} else { //  path has more than once occurrence of /site/
			thePath = path.Join(target, path.Join(parts[1:]...))
		}
	} else if strings.Contains(thePath, sysDir) {
		parts := strings.Split(thePath, sysDir)
		if len(parts) == 2 {
			thePath = path.Join(sysDir, parts[1])
		} else { //  path has more than once occurrence of /sys/
			thePath = path.Join(sysDir, path.Join(parts[1:]...))
		}
	} else {
		return fmt.Errorf("updateConfiguration: unknown path %s", thePath)
	}
	p := properties.PropertyForPath(thePath)
	if p == -1 {
		return fmt.Errorf("settings manager unable to get property for path %s", thePath)
	}
	data := p.Data()
	switch data.ValueType {
	case propertyValueTypes.Boolean:
		var v bool
		var err error
		if v, err = s.GetElseSetBoolProp(p); err != nil {
			return err
		}
		s.BoolProps[p] = v
	case propertyValueTypes.Float:
		var v float64
		var err error
		if v, err = s.GetElseSetFloatProp(p); err != nil {
			return err
		}
		s.FloatProps[p] = v
	case propertyValueTypes.Int:
		var v int
		var err error
		if v, err = s.GetElseSetIntProp(p); err != nil {
			return err
		}
		s.IntProps[p] = v
	case propertyValueTypes.String:
		var v string
		var err error
		if v, err = s.GetElseSetStringProp(p); err != nil {
			return err
		}
		s.StringProps[p] = v
	case propertyValueTypes.StringMapString:
		var v map[string]string
		var err error
		if v, err = s.GetElseSetStringMapStringProp(p); err != nil {
			return err
		}
		s.StringMapStringProps[p] = v
	case propertyValueTypes.StringSlice:
		var v []string
		var err error
		if v, err = s.GetElseSetStringSliceProp(p); err != nil {
			return err
		}
		s.StringSliceProps[p] = v
	default:
		return fmt.Errorf("unknown property type %s", data.ValueType.String())

	}
	return nil
}

// ReadConfiguration iterates through the properties enum
// and uses the property information to create both its description
// and value records in the database.  Since a SettingsManager is
// instantiated for a specific configuration name, the properties
// will be created in the configs directory for that name.
func (s *SettingsManager) ReadConfiguration() error {
	var errMsg string
	for _, p := range properties.PropertyValues() {
		if p == properties.SiteBuildFramesScoreParagraphs {
			fmt.Sprintf("TODO DELETE ME")
		}
		errMsg = fmt.Sprintf("error reading property %s: ", p.String())
		data := p.Data()
		if data == nil {
			doxlog.Infof("pkg/enum/properties: data function not implemented for property number %d\n", p)
			os.Exit(1)
		}
		// check to see if the property is considered obsolete
		if properties.Obsolete(data.Path) {
			var msg string
			if s.Dev {
				msg = fmt.Sprintf("developer: you must have added a property that previously was obsolete: %s - if you want to use this property, remove it from the obsolete properties slice in enums/properties.go", data.Path)
			} else {
				msg = fmt.Sprintf("you have an obsolete property that you should delete: %s", data.Path)
			}
			doxlog.Errorf(msg)
			continue
		}
		switch data.ValueType {
		case propertyValueTypes.Boolean:
			var v bool
			var err error
			if v, err = s.GetElseSetBoolProp(p); err != nil {
				errMsg = fmt.Sprintf("%s%v", errMsg, err)
				doxlog.Error(errMsg)
				continue
				//				return fmt.Errorf(errMsg)
			}
			s.BoolProps[p] = v
		case propertyValueTypes.Float:
			var v float64
			var err error
			if v, err = s.GetElseSetFloatProp(p); err != nil {
				errMsg = fmt.Sprintf("%s%v", errMsg, err)
				doxlog.Error(errMsg)
				continue
				//				return fmt.Errorf(errMsg)
			}
			s.FloatProps[p] = v
		case propertyValueTypes.Int:
			var v int
			var err error
			if v, err = s.GetElseSetIntProp(p); err != nil {
				errMsg = fmt.Sprintf("%s%v", errMsg, err)
				doxlog.Error(errMsg)
				continue
				//				return fmt.Errorf(errMsg)
			}
			s.IntProps[p] = v
		case propertyValueTypes.String:
			var v string
			var err error
			if v, err = s.GetElseSetStringProp(p); err != nil {
				errMsg = fmt.Sprintf("%s%v", errMsg, err)
				doxlog.Error(errMsg)
				continue
				//				return fmt.Errorf(errMsg)
			}
			s.StringProps[p] = v
		case propertyValueTypes.StringMapString:
			var v map[string]string
			var err error
			if v, err = s.GetElseSetStringMapStringProp(p); err != nil {
				errMsg = fmt.Sprintf("%s%v", errMsg, err)
				doxlog.Error(errMsg)
				continue
				//				return fmt.Errorf(errMsg)
			}
			s.StringMapStringProps[p] = v
		case propertyValueTypes.StringSlice:
			switch p {
			case properties.Epistle,
				properties.Gospel,
				properties.Liturgical,
				properties.Prophet,
				properties.Psalter:
			default:
				var v []string
				var err error
				if v, err = s.GetElseSetStringSliceProp(p); err != nil {
					errMsg = fmt.Sprintf("%s%v", errMsg, err)
					doxlog.Error(errMsg)
					return fmt.Errorf(errMsg)
				}
				s.StringSliceProps[p] = v
			}
		default:
			errMsg = fmt.Sprintf("unknown property type %s", data.ValueType.String())
			doxlog.Error(errMsg)
		}
	}
	return nil
}

func (s *SettingsManager) GetSiteMediaMapFiles(site string) ([]string, error) {
	mediaDir := s.StringProps[properties.SysCmdAgesMediaPath]
	if mediaDir == "" {
		return nil, fmt.Errorf("no value set for %s", properties.SysCmdAgesMediaPath.Data().Path)
	}
	files, err := ltfile.FileMatcher(mediaDir, "ares", nil)
	if err != nil {
		return nil, fmt.Errorf("error reading media files from %s: %v", mediaDir, err)
	}
	if files == nil {
		return nil, fmt.Errorf("no media files found in %s", mediaDir)
	}
	return files, nil
}
func (s *SettingsManager) SetSiteMediaMapFiles(site string, vals stack.StringStack) error {
	s.PM.KP.Clear()
	s.PM.KP.Dirs.Push("config")
	s.PM.KP.Dirs.Push("sites")
	s.PM.KP.Dirs.Push(site)
	s.PM.KP.Dirs.Push("media")
	s.PM.KP.Dirs.Push("mapFiles")
	if err := s.PM.SetStringSlice("values", vals); err != nil {
		return err
	}
	s.PM.KP.Clear()
	return nil
}
func (s *SettingsManager) SetSiteMediaMapFilesDesc(site string, desc string) error {
	s.PM.KP.Clear()
	s.PM.KP.Dirs.Push("config")
	s.PM.KP.Dirs.Push("sites")
	s.PM.KP.Dirs.Push(site)
	s.PM.KP.Dirs.Push("media")
	s.PM.KP.Dirs.Push("mapFiles")
	if err := s.PM.SetAnnotation(desc); err != nil {
		return err
	}
	s.PM.KP.Clear()
	return nil
}

type DomainComboNotFoundError struct{}

func (d *DomainComboNotFoundError) Error() string {
	return "build combination not found"
}

type DomainColumnNotFoundError struct{}

func (d *DomainColumnNotFoundError) Error() string {
	return "build column not found"
}

//func (s *SettingsManager) AddSiteBuildLayoutsAcronym(acronym string) error {
//	acronyms, err := s.GetSiteBuildLayoutsAcronyms()
//	if err != nil {
//		return fmt.Errorf("settingsManager.AddSiteBuildLayoutsAcronym error: %v", err)
//	}
//	for _, a := range acronyms {
//		if a == acronym {
//			return nil // already exists, so ignore and return
//		}
//	}
//	// did not exist, so save it
//	acronyms = append(acronyms, acronym)
//	sort.Strings(acronyms)
//	kvp := kvs.NewDbR()
//	kvp.KP.ParsePath(s.ConfigPath + properties.SiteBuildLayoutsRows.Data().Path)
//	kvp.KP.KeyParts.Push("value")
//	kvp.Value = strings.Join(acronyms[:], ",")
//	return s.Kvs.Db.Put(kvp)
//}
//func (s *SettingsManager) GetSiteBuildLayoutsAcronyms() ([]string, error) {
//	var acronyms []string
//	layouts := s.StringProps[properties.SiteBuildLayoutsRows]
//	var err error
//	if len(layouts) == 0 { // get default value and save to database
//		layouts = properties.SiteBuildLayoutsRows.Data().DefaultValue
//		kvp := kvs.NewDbR()
//		kvp.KP.ParsePath(s.ConfigPath + properties.SiteBuildLayoutsRows.Data().Path)
//		kvp.KP.KeyParts.Push("value")
//		kvp.Value = layouts
//		err = s.Kvs.Db.Put(kvp)
//		if err != nil {
//			fmt.Printf("pkg/configs/config.go SettingsManager.GetSiteBuildLayoutsAcronyms error saving missing layout acronyms %s: %v", kvp.Value, err)
//		}
//	}
//	// continue processing the request for acronyms
//	parts := strings.Split(s.StringProps[properties.SiteBuildLayoutsRows], ",")
//	sort.Strings(parts)
//	for _, p := range parts {
//		acronyms = append(acronyms, strings.TrimSpace(p))
//	}
//	sort.Strings(acronyms)
//	return acronyms, nil
//}
//func (s *SettingsManager) GetSiteBuildColumnMap() (map[string]*atempl.TableLayout, error) {
//	columnMap := make(map[string]*atempl.TableLayout)
//	matcher := kvs.NewMatcher()
//	matcher.KP.ParsePath(s.ConfigPath + properties.SiteBuildLayoutsColumns.Data().Path + "/map")
//	recs, _, err := s.Kvs.Db.GetMatching(*matcher)
//	if err != nil {
//		return nil, err
//	}
//	if len(recs) == 0 { // we need to initialize the values
//		columnLayoutsMap, err := properties.SiteBuildLayoutsColumns.PropertyMap()
//		if err != nil {
//			return nil, fmt.Errorf(fmt.Sprintf("config.SettingsManager.GetSiteBuildColumnMap error: %v", err))
//		}
//		for _, v := range columnLayoutsMap {
//			l, err := atempl.NewTableLayoutFromJson(v)
//			if err != nil {
//				return nil, err
//			}
//			columnMap[l.Acronym] = l
//			err = s.SetSiteBuildLayout(l)
//			if err != nil {
//				return nil, err
//			}
//		}
//	} else {
//		for _, rec := range recs {
//			l, err := atempl.NewTableLayoutFromJson(rec.Value)
//			if err != nil {
//				return nil, err
//			}
//			columnMap[l.Acronym] = l
//		}
//	}
//	return columnMap, nil
//}
//func (s *SettingsManager) GetSiteBuildLayouts() ([]*atempl.TableLayout, error) {
//	var acronyms []string
//	var err error
//	var layouts []*atempl.TableLayout
//
//	acronyms, err = s.GetSiteBuildLayoutsAcronyms()
//	if err != nil {
//		return nil, err
//	}
//	for _, combo := range acronyms {
//		var tableLayout *atempl.TableLayout
//		tableLayout, err = s.GetSiteBuildLayout(combo)
//		if err != nil {
//			return nil, err
//		}
//		layouts = append(layouts, tableLayout)
//	}
//	return layouts, nil
//}
//
////func (s *SettingsManager) GetSiteBuildLayoutsOld() ([]*atempl.TableLayout, error) {
////	var err error
////	var layouts []*atempl.TableLayout
////	matcher := kvs.NewMatcher()
////	matcher.KP.ParsePath(s.ConfigPath + SiteBuildLayouts)
////	recs, _, err := s.Kvs.Db.GetMatching(*matcher)
////	if err != nil {
////		return nil, err
////	}
////	for _, rec := range recs {
////		l, err := atempl.NewTableLayoutFromJson(rec.Value)
////		if err != nil {
////			return nil, err
////		}
////		layouts = append(layouts, l)
////	}
////	return layouts, nil
////}
//
//// GetSiteBuildLayout provides the layout for a specific combination of language codes.
//// A layout specifies the number of columns for a table and the versions to use to populate cells
//// in each column of the table.
//func (s *SettingsManager) GetSiteBuildLayout(acronymCombo string) (*atempl.TableLayout, error) {
//	columnMap, err := s.GetSiteBuildColumnMap()
//	if err != nil {
//		return nil, err
//	}
//	tableLayout := new(atempl.TableLayout)
//	tableLayout.Acronym = acronymCombo
//	parts := strings.Split(acronymCombo, "-")
//	for _, p := range parts {
//		var langLayout *atempl.TableLayout
//		var found bool
//		if langLayout, found = columnMap[p]; !found {
//			return nil, fmt.Errorf("could not find layout for %s", p)
//		}
//		tableLayout.LiturgicalLibs = append(tableLayout.LiturgicalLibs, langLayout.LiturgicalLibs[0])
//		tableLayout.EpistleLibs = append(tableLayout.EpistleLibs, langLayout.EpistleLibs[0])
//		tableLayout.GospelLibs = append(tableLayout.GospelLibs, langLayout.GospelLibs[0])
//		tableLayout.ProphetLibs = append(tableLayout.ProphetLibs, langLayout.ProphetLibs[0])
//		tableLayout.PsalterLibs = append(tableLayout.PsalterLibs, langLayout.PsalterLibs[0])
//		tableLayout.DateFormat = langLayout.DateFormat
//		tableLayout.WeekDayFormat = langLayout.WeekDayFormat
//	}
//	return tableLayout, nil
//}
//func (s *SettingsManager) CreateSiteBuildTableLayout(layout *atempl.TableLayout) (string, error) {
//	return "", nil
//}
//func (s *SettingsManager) DeleteSiteBuildLayout(acronym string) error {
//	// remove from rows
//	// for any lang code not used in rows, delete it from columns
//	acronyms, err := s.GetSiteBuildLayoutsAcronyms()
//	if err != nil {
//		return fmt.Errorf("settingsMangager.DeleteSiteBuildLayout error: %v", err)
//	}
//	var newAcronyms []string
//	for _, a := range acronyms {
//		if a != acronym {
//			newAcronyms = append(newAcronyms, a)
//		}
//	}
//	sort.Strings(newAcronyms)
//	kvp := kvs.NewDbR()
//	kvp.KP.ParsePath(s.ConfigPath + properties.SiteBuildLayoutsRows.Data().Path)
//	kvp.KP.KeyParts.Push("value")
//	kvp.Value = strings.Join(newAcronyms[:], ",")
//	return s.Kvs.Db.Put(kvp)
//}
//func (s *SettingsManager) SetSiteBuildLayout(layout *atempl.TableLayout) error {
//	var err error
//	if layout.DateFormat == "" {
//		switch layout.Acronym {
//		case "en":
//			layout.DateFormat = "%A, %B %d, %Y"
//		case "el", "gr":
//			layout.DateFormat = "%A, %d %B %Y"
//		case "es":
//			layout.DateFormat = "%A, %d de %B de %Y"
//		default:
//			layout.DateFormat = "%d-%m-%Y"
//		}
//	}
//	if layout.WeekDayFormat == "" {
//		layout.WeekDayFormat = "%A"
//	}
//
//	dbr := kvs.NewDbR()
//	dbr.KP.ParsePath(s.ConfigPath + properties.SiteBuildLayoutsColumns.Data().Path + "/map")
//	dbr.KP.KeyParts.Push(layout.Acronym)
//	dbr.Value, err = layout.ToJson()
//	if err != nil {
//		return err
//	}
//	return s.Kvs.Db.Put(dbr)
//}

// GetLiturgicalTextLanguageCodes reads the ltx directory of the database
// and returns the language code from each library found.  The set of
// language codes will be unique with no duplicates
func (s *SettingsManager) GetLiturgicalTextLanguageCodes() ([]string, error) {
	var values []string
	var err error
	var domains []string
	languageMap := make(map[string]string)
	s.PM.KP.Clear()
	err = s.PM.KP.ParsePath("ltx")
	if err != nil {
		return nil, err
	}
	if domains, err = s.PM.KVS.Db.DirNames(*s.PM.KP); err != nil {
		return nil, err
	}
	// add the language codes to the map
	for _, library := range domains {
		language, _ := ltstring.LangFromLibrary(library)
		if language == "" || language == " " {
			// ignore
		} else {
			languageMap[language] = language
		}
	}
	for key, _ := range languageMap {
		values = append(values, key)
	}
	s.PM.KP.Clear()
	return values, nil
}

// GetSiteBuildDomains returns an array of the domains to use to generate a book or service.
// Users can define an arbitrary number of combinations of domains, each identified by a number.
// A generated book or service can have up to three columns (left, center, right).
// For each column, one or more domains must exist for epistle, gospel, liturgical, prophet, and psalter.
func (s *SettingsManager) GetSiteBuildDomains(combo, column, kind string) ([]string, error) {
	var values []string
	var err error
	s.PM.KP.Clear()
	err = s.PM.KP.ParsePath(s.ConfigPath + SiteBuildLayouts)
	if err != nil {
		s.PM.KP.Clear()
		return nil, err
	}
	s.PM.KP.Dirs.Push(combo)
	if !s.PM.KVS.Db.Exists(s.PM.KP) {
		s.PM.KP.Clear()
		return nil, new(DomainComboNotFoundError)
	}
	s.PM.KP.Dirs.Push(column)
	if s.PM.KP.Dirs.Size() != 7 {
		fmt.Sprintf("bummer")
	}
	if !s.PM.KVS.Db.Exists(s.PM.KP) {
		s.PM.KP.Clear()
		return nil, new(DomainColumnNotFoundError)
	}
	s.PM.KP.Dirs.Push(kind)
	values, err = s.PM.GetStringSlice("values")
	if err != nil {
		s.PM.KP.Clear()
		return nil, fmt.Errorf("GetSiteBuildDomains(%s, %s, %s) GetStringSlice error: %v", combo, column, kind, err)
	}
	s.PM.KP.Clear()
	return values, err
}

func (s *SettingsManager) GetElseSetBoolProp(prop properties.Property) (bool, error) {
	var val bool
	var err error
	propData := prop.Data()
	if propData.ValueType != propertyValueTypes.Boolean {
		return false, fmt.Errorf("%s not a boolean datatype", propData.Path)
	}
	s.PM.KP.Clear()
	dbPath := s.ConfigPath + propData.Path
	if err = s.PM.KP.ParsePath(dbPath); err != nil {
		return false, errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	if val, err = s.PM.GetBool("value"); err != nil {
		if val, err = prop.BoolValue(); err != nil {
			return false, err
		}
		return val, s.SetDefaultBoolProp(prop)
	}
	s.PM.KP.Clear()
	return val, err
}

// SetDefaultBoolProp sets the property using the default value and description.
func (s *SettingsManager) SetDefaultBoolProp(prop properties.Property) error {
	var err error
	propData := prop.Data()
	if propData.ValueType != propertyValueTypes.Boolean {
		return fmt.Errorf("%s not a boolean datatype", propData.Path)
	}
	s.PM.KP.Clear()
	dbPath := s.ConfigPath + propData.Path
	if err = s.PM.KP.ParsePath(dbPath); err != nil {
		return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	if !propData.DescOnly {
		// set default value
		var val bool
		if val, err = prop.BoolValue(); err != nil {
			return err
		}
		if err = s.PM.SetBool("value", val); err != nil {
			return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
		}
	}
	// and add the description
	if err = s.PM.SetString("desc", propData.Description); err != nil {
		return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	s.PM.KP.Clear()
	return nil
}

// GetElseSetFloatProp guarantees the return of either the value found in the database or a default value.
// If the record does not exist, it is created using the default value and description.
// When prefix and the prop path are concatenated, they should form a valid database path.
func (s *SettingsManager) GetElseSetFloatProp(prop properties.Property) (float64, error) {
	var val float64
	var err error
	propData := prop.Data()
	if propData.ValueType != propertyValueTypes.Float {
		return 1, fmt.Errorf("%s not float datatype", propData.Path)
	}
	s.PM.KP.Clear()
	dbPath := s.ConfigPath + propData.Path
	if err = s.PM.KP.ParsePath(dbPath); err != nil {
		return 1, errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	if val, err = s.PM.GetFloat64("value"); err != nil {
		if val, err = prop.FloatValue(); err != nil {
			return 1, err
		}
		return val, s.SetDefaultFloatProp(prop)
	}
	s.PM.KP.Clear()
	return val, err
}
func (s *SettingsManager) SetFloatProp(prop properties.Property) (float64, error) {
	var val float64
	var err error
	propData := prop.Data()
	if propData.ValueType != propertyValueTypes.Float {
		return 1, fmt.Errorf("%s not float datatype", propData.Path)
	}
	s.PM.KP.Clear()
	dbPath := s.ConfigPath + propData.Path
	if err = s.PM.KP.ParsePath(dbPath); err != nil {
		return 1, errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	if val, err = s.PM.GetFloat64("value"); err != nil {
		if val, err = prop.FloatValue(); err != nil {
			return 1, err
		}
		return val, s.SetDefaultFloatProp(prop)
	}
	s.PM.KP.Clear()
	return val, err
}

// SetDefaultFloatProp sets the property using the default value and description.
func (s *SettingsManager) SetDefaultFloatProp(prop properties.Property) error {
	var err error
	propData := prop.Data()
	if propData.ValueType != propertyValueTypes.Float {
		return fmt.Errorf("%s not an float datatype", propData.Path)
	}
	s.PM.KP.Clear()
	dbPath := s.ConfigPath + propData.Path
	if err = s.PM.KP.ParsePath(dbPath); err != nil {
		return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	if !propData.DescOnly {
		// set default value
		var val float64
		if val, err = prop.FloatValue(); err != nil {
			return err
		}
		if err = s.PM.SetFloat64("value", val); err != nil {
			return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
		}
	}
	// and add the description
	if err = s.PM.SetString("desc", propData.Description); err != nil {
		return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	s.PM.KP.Clear()
	return nil
}

// GetElseSetIntProp guarantees the return of either the value found in the database or a default value.
// If the record does not exist, it is created using the default value and description.
// When prefix and the prop path are concatenated, they should form a valid database path.
func (s *SettingsManager) GetElseSetIntProp(prop properties.Property) (int, error) {
	var val int
	var err error
	propData := prop.Data()
	if propData.ValueType != propertyValueTypes.Int {
		return 1, fmt.Errorf("%s not an int datatype", propData.Path)
	}
	s.PM.KP.Clear()
	dbPath := s.ConfigPath + propData.Path
	if err = s.PM.KP.ParsePath(dbPath); err != nil {
		return 1, errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	if val, err = s.PM.GetInt("value"); err != nil {
		if val, err = prop.IntValue(); err != nil {
			return 1, err
		}
		return val, s.SetDefaultIntProp(prop)
	}
	s.PM.KP.Clear()
	return val, err
}

// SetDefaultIntProp sets the property using the default value and description.
func (s *SettingsManager) SetDefaultIntProp(prop properties.Property) error {
	var err error
	propData := prop.Data()
	if propData.ValueType != propertyValueTypes.Int {
		return fmt.Errorf("%s not an int datatype", propData.Path)
	}
	s.PM.KP.Clear()
	dbPath := s.ConfigPath + propData.Path
	if err = s.PM.KP.ParsePath(dbPath); err != nil {
		return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	if !propData.DescOnly {
		// set default value
		var val int
		if val, err = prop.IntValue(); err != nil {
			return err
		}
		if err = s.PM.SetInt("value", val); err != nil {
			return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
		}
	}
	// and add the description
	if err = s.PM.SetString("desc", propData.Description); err != nil {
		return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	s.PM.KP.Clear()
	return nil
}

// GetElseSetStringProp guarantees the return of either the string value found in the database or a default value.
// If the record does not exist, it is created using the default value and description.
// When prefix and the prop path are concatenated, they should form a valid database path.
func (s *SettingsManager) GetElseSetStringProp(prop properties.Property) (string, error) {
	var val string
	var err error
	propData := prop.Data()
	if propData.ValueType != propertyValueTypes.String {
		return "", fmt.Errorf("%s not a string datatype", propData.Path)
	}
	s.PM.KP.Clear()
	dbPath := s.ConfigPath + propData.Path
	if err = s.PM.KP.ParsePath(dbPath); err != nil {
		return "", errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	if val, err = s.PM.GetString("value"); err != nil {
		val = propData.DefaultValue
		return val, s.SetDefaultStringProp(prop)
	}
	s.PM.KP.Clear()
	return val, err
}

// SetDefaultStringProp sets the property using the default value and description.
// When prefix and prop path are concatenated, they should form a valid database path.
func (s *SettingsManager) SetDefaultStringProp(prop properties.Property) error {
	var err error
	propData := prop.Data()
	if propData.ValueType != propertyValueTypes.String {
		return fmt.Errorf("%s not a string datatype", propData.Path)
	}
	s.PM.KP.Clear()
	dbPath := s.ConfigPath + propData.Path
	if err = s.PM.KP.ParsePath(dbPath); err != nil {
		return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	if !propData.DescOnly {
		// set default value
		if err = s.PM.SetString("value", propData.DefaultValue); err != nil {
			return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
		}
	}
	// and add the description
	if err = s.PM.SetString("desc", propData.Description); err != nil {
		return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	s.PM.KP.Clear()
	return nil
}
func (s *SettingsManager) GetAdHocElseSetBoolProp(propData properties.PropertyData) (bool, error) {
	var val bool
	var err error
	if propData.ValueType != propertyValueTypes.Boolean {
		return false, fmt.Errorf("%s not a boolean datatype", propData.Path)
	}
	s.PM.KP.Clear()
	dbPath := s.ConfigPath + propData.Path
	if err = s.PM.KP.ParsePath(dbPath); err != nil {
		return false, errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	if val, err = s.PM.GetBool("value"); err != nil {
		val, err = strconv.ParseBool(propData.DefaultValue)
		if err != nil {
			return false, err
		}
		return val, s.SetAdHocDefaultBoolProp(propData)
	}
	s.PM.KP.Clear()
	return val, err
}
func (s *SettingsManager) GetAdHocElseSetFloatProp(propData properties.PropertyData) (float64, error) {
	var val float64
	var err error
	if propData.ValueType != propertyValueTypes.Float {
		return 0.0, fmt.Errorf("%s not a float datatype", propData.Path)
	}
	s.PM.KP.Clear()
	dbPath := s.ConfigPath + propData.Path
	if err = s.PM.KP.ParsePath(dbPath); err != nil {
		return 0.0, errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	if val, err = s.PM.GetFloat64("value"); err != nil {
		val, err = strconv.ParseFloat(propData.DefaultValue, 64)
		if err != nil {
			return 0.0, err
		}
		return val, s.SetAdHocDefaultFloatProp(propData)
	}
	s.PM.KP.Clear()
	return val, err
}

// GetAdHocElseSetStringProp guarantees the return of either the string value found in the database or a default value.
// If the record does not exist, it is created using the default value and description.
// When prefix and the prop path are concatenated, they should form a valid database path.
func (s *SettingsManager) GetAdHocElseSetStringProp(propData properties.PropertyData) (string, error) {
	var val string
	var err error
	if propData.ValueType != propertyValueTypes.String {
		return "", fmt.Errorf("%s not a string datatype", propData.Path)
	}
	s.PM.KP.Clear()
	dbPath := s.ConfigPath + propData.Path
	if err = s.PM.KP.ParsePath(dbPath); err != nil {
		return "", errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	if val, err = s.PM.GetString("value"); err != nil {
		val = propData.DefaultValue
		return val, s.SetAdHocDefaultStringProp(propData)
	}
	s.PM.KP.Clear()
	return val, err
}

// SetAdHocDefaultStringProp sets the property using the default value and description.
// When prefix and prop path are concatenated, they should form a valid database path.
func (s *SettingsManager) SetAdHocDefaultStringProp(propData properties.PropertyData) error {
	var err error
	if propData.ValueType != propertyValueTypes.String {
		return fmt.Errorf("%s not a string datatype", propData.Path)
	}
	s.PM.KP.Clear()
	dbPath := s.ConfigPath + propData.Path
	if err = s.PM.KP.ParsePath(dbPath); err != nil {
		return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	if !propData.DescOnly {
		// set default value
		if err = s.PM.SetString("value", propData.DefaultValue); err != nil {
			return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
		}
	}
	// and add the description
	if err = s.PM.SetString("desc", propData.Description); err != nil {
		return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	s.PM.KP.Clear()
	return nil
}

// SetAdHocDefaultBoolProp sets the property using the default value and description.
// When prefix and prop path are concatenated, they should form a valid database path.
func (s *SettingsManager) SetAdHocDefaultBoolProp(propData properties.PropertyData) error {
	var err error
	if propData.ValueType != propertyValueTypes.Boolean {
		return fmt.Errorf("%s not a boolean datatype", propData.Path)
	}
	s.PM.KP.Clear()
	dbPath := s.ConfigPath + propData.Path
	if err = s.PM.KP.ParsePath(dbPath); err != nil {
		return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	if !propData.DescOnly {
		// set default value
		if err = s.PM.SetString("value", propData.DefaultValue); err != nil {
			return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
		}
	}
	// and add the description
	if err = s.PM.SetString("desc", propData.Description); err != nil {
		return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	s.PM.KP.Clear()
	return nil
}

// SetAdHocDefaultFloatProp sets the property using the default value and description.
// When prefix and prop path are concatenated, they should form a valid database path.
func (s *SettingsManager) SetAdHocDefaultFloatProp(propData properties.PropertyData) error {
	var err error
	if propData.ValueType != propertyValueTypes.Float {
		return fmt.Errorf("%s not a float datatype", propData.Path)
	}
	s.PM.KP.Clear()
	dbPath := s.ConfigPath + propData.Path
	if err = s.PM.KP.ParsePath(dbPath); err != nil {
		return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	if !propData.DescOnly {
		// set default value
		if err = s.PM.SetString("value", propData.DefaultValue); err != nil {
			return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
		}
	}
	// and add the description
	if err = s.PM.SetString("desc", propData.Description); err != nil {
		return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	s.PM.KP.Clear()
	return nil
}

// SetValueByPath gets the property by path,
// converts the value to a bool, float64, or int
// or keeps it as string depending on the data type of the
// property, and saves it to the database.
func (s *SettingsManager) SetValueByPath(path, value string) error {
	prop := properties.PropertyForPath(path)
	if prop == -1 {
		return fmt.Errorf("uknown property path %s", path)
	}
	s.PM.KP.Clear()
	propData := prop.Data()
	dbPath := s.ConfigPath + propData.Path
	if err := s.PM.KP.ParsePath(dbPath); err != nil {
		return fmt.Errorf("error parsing path %s: %v", dbPath, err)
	}
	switch propData.ValueType {
	case propertyValueTypes.Boolean:
		if value != "true" && value != "false" {
			return fmt.Errorf("expected value to be true or false, but got %s", value)
		}
		return s.PM.SetBool("value", value == "true")
	case propertyValueTypes.Float:
		i, err := strconv.ParseFloat(value, 64)
		if err != nil {
			return fmt.Errorf("expected value to be convertable to a float, but got %s", value)
		}
		return s.PM.SetFloat64("value", i)
	case propertyValueTypes.Int:
		i, err := strconv.Atoi(value)
		if err != nil {
			return fmt.Errorf("expected value to be convertable to an integer, but got %s", value)
		}
		return s.PM.SetInt("value", i)
	case propertyValueTypes.String:
		return s.PM.SetString("value", value)
	}
	return nil
}

// SetValueByType converts the value to a bool, float64, or int
// or keeps it as string depending on the data type of the
// property type, and saves it to the database.
func (s *SettingsManager) SetValueByType(prop properties.Property, value string) error {
	value = strings.TrimSpace(value)
	s.PM.KP.Clear()
	propData := prop.Data()
	dbPath := s.ConfigPath + propData.Path
	var err error
	if err = s.PM.KP.ParsePath(dbPath); err != nil {
		return fmt.Errorf("error parsing path %s: %v", dbPath, err)
	}
	switch propData.ValueType {
	case propertyValueTypes.Boolean:
		if value != "true" && value != "false" {
			return fmt.Errorf("expected value to be true or false, but got %s", value)
		}
		err = s.PM.SetBool("value", value == "true")
	case propertyValueTypes.Float:
		var i float64
		i, err = strconv.ParseFloat(value, 64)
		if err != nil {
			return fmt.Errorf("expected value to be convertable to a float, but got %s", value)
		}
		err = s.PM.SetFloat64("value", i)
	case propertyValueTypes.Int:
		var i int
		i, err = strconv.Atoi(value)
		if err != nil {
			return fmt.Errorf("expected value to be convertable to an integer, but got %s", value)
		}
		err = s.PM.SetInt("value", i)
	case propertyValueTypes.String:
		err = s.PM.SetString("value", value)
	}
	s.PM.KP.Clear()
	if err != nil {
		return fmt.Errorf("error setting %s == %s: %v", dbPath, value, err)
	}
	// update the in-memory configuration
	err = s.UpdateConfiguration(dbPath)
	if err != nil {
		return fmt.Errorf("error updating configuration for %s", dbPath)
	}
	return nil
}

// SetStringSliceByType saves the values to the database using the type's database path
func (s *SettingsManager) SetStringSliceByType(prop properties.Property, values []string) error {
	propData := prop.Data()
	if propData.ValueType != propertyValueTypes.StringSlice {
		return fmt.Errorf("property %s not a string slice property", propData.Path)
	}
	var err error
	dbPath := s.ConfigPath + propData.Path
	s.PM.KP.Clear()
	if err = s.PM.KP.ParsePath(dbPath); err != nil {
		s.PM.KP.Clear()
		return fmt.Errorf("property %s: %v", dbPath, err)
	}
	// delete the existing values
	kp := s.PM.KP.Copy()
	kp.Dirs.Push("values")
	err = s.Kvs.Db.Delete(*kp)
	if err != nil {
		return fmt.Errorf("error deleting existing values for %s: %v", dbPath, err)
	}
	// save the new ones
	if err = s.PM.SetStringSlice("values", values); err != nil {
		s.PM.KP.Clear()
		return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	s.PM.KP.Clear()
	// update the in-memory configuration
	err = s.UpdateConfiguration(dbPath)
	if err != nil {
		return fmt.Errorf("error updating configuration for %s", dbPath)
	}
	return nil
}

// SyncDbWithPropertiesMap checks to see if the /site/build/pages/titles/map
// contains a key for every key found in the properties map.
// This is done because there might be new values that have
// been added by the developer, i.e. you or me.
func (s *SettingsManager) SyncDbWithPropertiesMap() error {
	var err error
	prop := properties.SiteBuildPagesTitles
	var pMap map[string]string
	pMap, err = prop.PropertyMap()
	kp := kvs.NewKeyPath()
	dbPath := s.ConfigPath + prop.Data().Path
	if err = kp.ParsePath(dbPath); err != nil {
		return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	kp.Dirs.Push("map")
	for k, v := range pMap {
		kp.KeyParts.Push(k)
		if !s.PM.KVS.Db.Exists(kp) {
			dbr := kvs.NewDbR()
			err = dbr.KP.ParsePath(kp.Path())
			if err != nil {
				msg := fmt.Sprintf("error parsing titles map path for key %s: %v", k, err)
				doxlog.Errorf(msg)
				return fmt.Errorf(msg)
			}
			dbr.Value = v
			err = s.PM.KVS.Db.Put(dbr)
			if err != nil {
				msg := fmt.Sprintf("error creating title map item %s: %v", k, err)
				doxlog.Errorf(msg)
				return fmt.Errorf(msg)
			}
		}
		kp.KeyParts.Pop()
	}
	return nil
}

// GetElseSetStringMapStringProp guarantees the return of a key-value map found in the database or a default value.
// If the record does not exist, it is created using the default value and description.
func (s *SettingsManager) GetElseSetStringMapStringProp(prop properties.Property) (map[string]string, error) {
	val := make(map[string]string)
	var err error
	propData := prop.Data()
	if propData.ValueType != propertyValueTypes.StringMapString {
		return nil, fmt.Errorf("%s not a string map string datatype", propData.Path)
	}
	s.PM.KP.Clear()
	dbPath := s.ConfigPath + propData.Path
	if err = s.PM.KP.ParsePath(dbPath); err != nil {
		return nil, errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	s.PM.KP.Dirs.Push("map")
	if s.PM.KVS.Db.Exists(s.PM.KP) {
		matcher := kvs.NewMatcher()
		matcher.KP = s.PM.KP.Copy()
		matcher.Recursive = true
		recs, _, err := s.PM.KVS.Db.GetMatching(*matcher)
		if err != nil {
			return val, err
		}
		if len(recs) == 0 {
			if err = s.SetDefaultStringMapStringProp(prop); err != nil {
				return val, err
			}
			return prop.PropertyMap()
		}
		for _, r := range recs {
			val[r.KP.Key()] = r.Value
		}
	} else {
		if err = s.SetDefaultStringMapStringProp(prop); err != nil {
			return val, err
		}
		return prop.PropertyMap()
	}
	s.PM.KP.Clear()
	return val, err
}

// SetDefaultStringMapStringProp sets the property using a map.
func (s *SettingsManager) SetDefaultStringMapStringProp(prop properties.Property) error {
	var err error
	propData := prop.Data()
	if propData.ValueType != propertyValueTypes.StringMapString {
		return fmt.Errorf("%s not a string map datatype", propData.Path)
	}
	s.PM.KP.Clear()
	dbPath := s.ConfigPath + propData.Path
	if err = s.PM.KP.ParsePath(dbPath); err != nil {
		return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	// set the description
	if err = s.PM.SetString("desc", propData.Description); err != nil {
		return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	// set default values
	var propertyMap map[string]string
	if propertyMap, err = prop.PropertyMap(); err != nil {
		return err
	}
	s.PM.KP.Dirs.Push("map")

	for key, value := range propertyMap {
		if err = s.PM.SetString(key, value); err != nil {
			return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
		}
	}
	s.PM.KP.Clear()
	return nil
}

// GetElseSetStringSliceProp guarantees the return of either the string value found in the database or a default value.
// If the record does not exist, it is created using the default value and description.
func (s *SettingsManager) GetElseSetStringSliceProp(prop properties.Property) ([]string, error) {
	var val []string
	var err error
	propData := prop.Data()
	if propData.ValueType != propertyValueTypes.StringSlice {
		return nil, fmt.Errorf("%s not a string slice datatype", propData.Path)
	}
	s.PM.KP.Clear()
	dbPath := s.ConfigPath + propData.Path
	if err = s.PM.KP.ParsePath(dbPath); err != nil {
		return nil, errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	if val, err = s.PM.GetStringSlice("values"); err != nil {
		val, err = prop.StringSliceValue()
		return val, s.SetDefaultStringSliceProp(prop)
	}
	s.PM.KP.Clear()
	return val, err
}

// SetDefaultStringSliceProp sets the property using the default value and description.
// When prefix and prop path are concatenated, they should form a valid database path.
func (s *SettingsManager) SetDefaultStringSliceProp(prop properties.Property) error {
	var err error
	propData := prop.Data()
	if propData.ValueType != propertyValueTypes.StringSlice {
		return fmt.Errorf("%s not a string slice datatype", propData.Path)
	}
	s.PM.KP.Clear()
	dbPath := s.ConfigPath + propData.Path
	if err = s.PM.KP.ParsePath(dbPath); err != nil {
		return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	if !propData.DescOnly {
		// set default value
		var val []string
		val, err = prop.StringSliceValue()
		if err = s.PM.SetStringSlice("values", val); err != nil {
			return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
		}
	}
	// and add the description
	if err = s.PM.SetString("desc", propData.Description); err != nil {
		return errors.New(fmt.Sprintf("property %s: %v", dbPath, err))
	}
	s.PM.KP.Clear()
	return nil
}

// GetSysSynchBtxUrls returns a slice  of remote repository urls for biblical text
func (s *SettingsManager) GetSysSynchBtxUrls(ocmc bool) ([]string, error) {
	return s.getSysSynchUrls("btx", ocmc)
}

// GetSysSyncLtxUrls returns a slice  of remote repository urls for liturgical text
func (s *SettingsManager) GetSysSyncLtxUrls(ocmc bool) ([]string, error) {
	return s.getSysSynchUrls("ltx", ocmc)
}

func (s *SettingsManager) getSysSynchUrls(libType string, ocmc bool) ([]string, error) {
	var err error
	thePath := path.Join(s.ConfigPath, "sys", "synch", libType, "repos")
	var matcher *kvs.Matcher
	matcher = kvs.NewMatcher()
	err = matcher.KP.ParsePath(thePath)
	if err != nil {
		return nil, err
	}
	matcher.ValuePattern = thePath + "/.*:value"
	err = matcher.UseAsRegEx()
	if err != nil {
		return nil, err
	}
	matcher.Recursive = true
	// get matching records
	recs, err := s.PM.KVS.Db.GetMatchingIDs(*matcher)
	if err != nil {
		return nil, err
	}
	var urls []string
	for _, rec := range recs {
		if len(rec.Value) > 0 {
			if rec.KP.Dirs.Last() != "push" {
				if !ocmc {
					if strings.Contains(rec.Value, "ocmc") {
						continue
					}
				}
				urls = append(urls, rec.Value)
			}
		}
	}
	return urls, nil
}

// Export saves the records in the specified kp to a file in the doxa/exports directory.
// It returns the path to the exported file and any error that occurred.
func (s *SettingsManager) Export(kp *kvs.KeyPath, exportType kvs.ExportType) (string, error) {
	dirPath := kp.DirPath()
	if len(dirPath) == 0 {
		dirPath = "doxaDatabase"
	}
	filePath := path.Join(s.ExportPath, dirPath+".tsv")
	err := s.PM.KVS.Db.Export(kp, filePath, exportType)
	return filePath, err
}

// ImportFromFile loads the database from a file of tab value delimited lines.
// The file must be in the doxa/imports directory.
// It returns the path to the import file, the number of records written to the database, and any error that occurred.
func (s *SettingsManager) ImportFromFile(kp *kvs.KeyPath, removeQuotes, allOrNone, skipObsoleteConfigs bool) (string, int, []error) {
	dirPath := kp.DirPath()
	if len(dirPath) == 0 {
		dirPath = "doxaDatabase"
	}
	filePath := path.Join(s.ImportPath, dirPath+".tsv")
	count, theErrors := s.PM.KVS.Db.Import("", filePath, inOut.LineParser, removeQuotes, allOrNone, skipObsoleteConfigs)
	return filePath, count, theErrors
}
