package config

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"path/filepath"
	"testing"
)

func init() {
	doxlog.Init("", "", true)
	doxlog.Info("doxLog initialized")
}

func TestPaths_MoveSubscriptionsDirToProject(t *testing.T) {
	testDir := t.TempDir()
	p := new(Paths)
	p.HomePath = filepath.Join(testDir, HomeDir)
	p.ProjectsDirPath = filepath.Join(p.HomePath, ProjectsDir)
	legacySubscriptionsDirPath := filepath.Join(p.HomePath, SubscriptionsDir)

	subscribedDirs := []string{"a", "b", "c"}
	var err error
	projects := []string{"doxa-eac", "doxa-liml"}
	for _, proj := range projects {
		err = ltfile.CreateDirs(filepath.Join(p.ProjectsDirPath, proj))
		if err != nil {
			t.Fatal(err)
		}
	}
	for _, subDir := range subscribedDirs {
		for j := 1; j < 3; j++ {
			fullPath := filepath.Join(legacySubscriptionsDirPath, subDir, fmt.Sprintf("f%d.txt", j))
			err = ltfile.WriteFile(fullPath, "test")
			if err != nil {
				t.Fatalf("Failed to write file %s: %v", fullPath, err)
			}
		}
	}
	fileCheck := func(from string, printLn bool) (count int) {
		var files []string
		files, err = ltfile.FileMatcher(from, "txt", nil)
		if err != nil {
			t.Fatal(err)
		}
		count = len(files)
		if !printLn {
			return
		}
		testDirLength := len(testDir)
		for _, file := range files {
			fmt.Println(file[testDirLength:])
		}
		return
	}
	fileCountBefore := fileCheck(p.HomePath, false)
	err = p.MoveSubscriptionsDirToProject()
	if err != nil {
		t.Fatalf("Failed to move subscriptions directory: %v", err)
	}
	fileCountAfter := fileCheck(p.HomePath, false) / len(projects)
	if fileCountBefore != fileCountAfter {
		t.Errorf("Moved %d files, expected %d", fileCountBefore, fileCountAfter)
	}
}
