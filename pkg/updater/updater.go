package updater

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/enums/versionStatuses"
	"github.com/liturgiko/doxa/pkg/utils/clinput"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/walle/targz"
	"golang.org/x/mod/semver"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"runtime"
	"strings"
)

var releaseInfoUrl = "https://github.com/liturgiko/doxa/raw/main/release.json"

type Paths struct {
	CurrentDoxaFile  string
	DoxaBin          string
	DoxaHome         string
	PreviousDir      string
	PreviousDoxaFile string
	TempDownload     string
	UserHome         string
}

func newPaths() (Paths, error) {
	var err error
	var p Paths
	p.UserHome, err = os.UserHomeDir()
	if err != nil {
		return p, err
	}
	p.DoxaHome = path.Join(p.UserHome, "doxa")
	p.DoxaBin = path.Join(p.DoxaHome, "bin")
	p.CurrentDoxaFile = path.Join(p.DoxaBin, "doxa")
	p.PreviousDir = path.Join(p.DoxaBin, ".previous")
	p.PreviousDoxaFile = path.Join(p.PreviousDir, "doxa")
	if runtime.GOOS == "windows" {
		p.CurrentDoxaFile = fmt.Sprintf("%s.exe", p.CurrentDoxaFile)
		p.PreviousDoxaFile = fmt.Sprintf("%s.exe", p.PreviousDoxaFile)
	}
	p.TempDownload = path.Join(p.DoxaBin, ".tmp")
	return p, nil
}

type DoxaUpdater struct {
	LocalDoxaVersion string
	Paths            Paths
	ReleaseInfo      ReleaseInfo
	ReleaseInfoUrl   string
}

func NewDoxaUpdater(localDoxaVersion string) (*DoxaUpdater, error) {
	updater := new(DoxaUpdater)
	updater.LocalDoxaVersion = localDoxaVersion
	updater.ReleaseInfoUrl = releaseInfoUrl
	p, err := newPaths()
	if err != nil {
		return updater, err
	}
	updater.Paths = p
	// get the release information
	err = updater.getReleaseInfo()
	return updater, err
}

// getReleaseInfo reads the release.json file specified by the url,
// and populates releaseInfo from the json string.
func (d *DoxaUpdater) getReleaseInfo() error {
	resp, err := http.Get(d.ReleaseInfoUrl)
	if err != nil {
		return fmt.Errorf("GET error: %v", err)
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("status error: %v", resp.StatusCode)
	}

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("read body: %v", err)
	}

	err = json.Unmarshal(data, &d.ReleaseInfo)
	if err != nil {
		fmt.Println(err)
	}
	return nil
}

// VersionStatus compares the local version to the remote and
// indicates whether there is a newer version available using a simulated enum using an int
// It also returns the local version number.
func (d *DoxaUpdater) VersionStatus() versionStatuses.VersionStatus {
	status := semver.Compare(d.ReleaseInfo.ReleaseId, d.LocalDoxaVersion)
	switch status {
	case 0:
		return versionStatuses.Current
	case 1:
		return versionStatuses.BehindLatest
	default:
		return versionStatuses.AheadOfLatest
	}
}

// ReleaseBinary holds the unmarshalled json information for an available binary
type ReleaseBinary struct {
	ARCH        string
	ArchiveName string
	OS          string
	SHA256      string
	URL         string
}

func (r *ReleaseBinary) ToJson() (string, error) {
	indent, err := json.MarshalIndent(r, "", " ")
	if err != nil {
		return "", fmt.Errorf("error marshalling %s to json string: %v", r.URL, err)
	}
	return string(indent), nil
}

// ReleaseInfo holds the unmarshalled json release information
type ReleaseInfo struct {
	Description string
	ReleaseId   string
	ReleaseInfo string
	BinaryMap   map[string]ReleaseBinary
}

func NewReleaseInfo() *ReleaseInfo {
	r := new(ReleaseInfo)
	r.BinaryMap = make(map[string]ReleaseBinary)
	return r
}
func (r *ReleaseInfo) ToJson() (string, error) {
	indent, err := json.MarshalIndent(r, "", " ")
	if err != nil {
		return "", err
	}
	return string(indent), nil
}

// getBinary returns binary information for the specified OS and architecture
func (r *ReleaseInfo) getBinary(os, arch string) ReleaseBinary {
	key := fmt.Sprintf("%s/%s", os, arch)
	b := r.BinaryMap[key]
	return b
}

// Update prompts the user to determine if he/she wants to Update doxa or its newer version.
// If so, it will Update it.
func (d *DoxaUpdater) Update() error {
	var q string
	var err error

	if len(d.LocalDoxaVersion) > 0 {
		fmt.Printf("If you chose to upgrade doxa, it will install the new version and quit.  You will need to restart doxa.\n\n")
		q = fmt.Sprintf("Upgrade doxa from version %s to %s? Type y or n.", d.LocalDoxaVersion, d.ReleaseInfo.ReleaseId)
	} else {
		q = fmt.Sprintf("Install doxa version %s?", d.ReleaseInfo.ReleaseId)
	}
	var o = []string{
		"y", "yes",
		"n", "no",
	}
	switch clinput.GetInput(q, o) {
	case 'y':
		var b ReleaseBinary
		if b, err = d.getReleaseBinaryInfo(); err != nil {
			return err
		} else {
			err = d.downloadFile(b.URL)
			if err != nil {
				return err
			}
			d.AddDoxaBinPath()
			fmt.Printf("doxa has been updated to version %s\n", d.ReleaseInfo.ReleaseId)
			fmt.Println("Please restart doxa. Once it starts, you can use the restore command to install the previous version.")
			os.Exit(0)
		}
		return nil
	}
	return nil
}

// getReleaseBinaryInfo returns the information to download a binary for the user's OS and architecture
func (d *DoxaUpdater) getReleaseBinaryInfo() (ReleaseBinary, error) {
	var b ReleaseBinary
	if b = d.ReleaseInfo.getBinary(runtime.GOOS, runtime.GOARCH); len(b.URL) > 0 {
		return b, nil
	}
	return b, fmt.Errorf("doxa not available for %s/%s", runtime.GOOS, runtime.GOARCH)
}

// downloadFile downloads the compressed binary for the user's OS and architecture,
// extracts it, and copies it to the doxa/bin directory.
// so they can be restored via the doxa restore command.
func (d *DoxaUpdater) downloadFile(URL string) error {
	if runtime.GOOS == "windows" { // switch URL to zip
		URL = strings.ReplaceAll(URL, ".tar.gz", ".zip")
	}
	extractPath := path.Join(d.Paths.TempDownload, "extracted")
	// just in case it exists, remove the previous extraction dir
	if ltfile.DirExists(extractPath) {
		_ = os.RemoveAll(extractPath)
	}
	// Get release info from the remote repo
	response, err := http.Get(URL)
	if err != nil {
		return err
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(response.Body)

	if response.StatusCode != 200 {
		return errors.New("received non 200 response code")
	}
	// download the requested doxa tar gz file to the temp dir
	err = ltfile.CreateDirs(d.Paths.TempDownload)
	if err != nil {
		return err
	}
	parts := strings.Split(URL, "/")
	urlFile := parts[len(parts)-1]
	var extractedDir string
	if strings.HasSuffix(urlFile, "zip") {
		extractedDir = urlFile[:len(urlFile)-4]
	} else { // assume gz
		extractedDir = urlFile[:len(urlFile)-7]
	}
	filePath := path.Join(d.Paths.TempDownload, urlFile)
	//Create an empty file to copy contents into
	file, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(file)

	//Write the bytes to the file
	_, err = io.Copy(file, response.Body)
	if err != nil {
		return err
	}
	// extract the downloaded file
	if strings.HasSuffix(filePath, "zip") {
		// extract the downloaded zip
		_, err = ltfile.Unzip(filePath, extractPath)
	} else if strings.HasSuffix(filePath, "gz") {
		// extract the downloaded tar gz
		err = targz.Extract(filePath, extractPath)
	}

	// if exists, save old Doxa as bin/.previous/doxa
	if ltfile.FileExists(d.Paths.CurrentDoxaFile) {
		// create .previous dir if it does not exist
		if !ltfile.DirExists(d.Paths.PreviousDir) {
			err = ltfile.CreateDirs(d.Paths.PreviousDir)
			if err != nil {
				log.Fatal(err)
			}
		}
		// move the doxa file to .previous
		err = os.Rename(d.Paths.CurrentDoxaFile, d.Paths.PreviousDoxaFile)
		if err != nil {
			_ = os.RemoveAll(extractPath)
			log.Fatal(err)
		}
	}
	// move downloaded doxa to doxa/bin
	downloadedDoxa := path.Join(extractPath, extractedDir, "doxa")
	if runtime.GOOS == "windows" {
		downloadedDoxa = fmt.Sprintf("%s.exe", downloadedDoxa)
	}
	err = os.Chmod(downloadedDoxa, 0700)
	if err != nil {
		_ = os.RemoveAll(extractPath)
		return err
	}
	err = os.Rename(downloadedDoxa, d.Paths.CurrentDoxaFile)
	if err != nil {
		log.Fatal(err)
	}
	return nil
}

// AddDoxaBinPath updates the PATH variable on the user's computer to include doxa/bin
func (d *DoxaUpdater) AddDoxaBinPath() {
	var shellResourceFile string

	switch runtime.GOOS {
	case "darwin":
		shellResourceFile = path.Join(d.Paths.UserHome, ".zshrc")
		d.addPath(shellResourceFile, fmt.Sprintf("export PATH=$PATH:\"%s\"", d.Paths.DoxaBin))
	case "windows":
		// TBD
	default:
		return
	}
}

// addPath appends the resource file with doxa bin path information
func (d *DoxaUpdater) addPath(resourceFile, doxaBinPath string) {
	// get the resource file lines
	lines, err := ltfile.GetFileLines(resourceFile)
	if err != nil {
		fmt.Printf("Could not read %s: %v\n", resourceFile, err)
	}
	// process the lines
	var newLines []string
	var hasDoxaBin bool

	for _, line := range lines {
		if strings.Contains(line, "doxa/bin") {
			hasDoxaBin = true
			break
		}
		newLines = append(newLines, line)
	}
	if !hasDoxaBin {
		q := fmt.Sprintf("Add doxa/bin to path in %s?\n", resourceFile)
		var o = []string{
			"y", "yes",
			"n", "no",
		}
		switch clinput.GetInput(q, o) {
		case 'y':
			f, err := os.OpenFile(resourceFile,
				os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
			if err != nil {
				doxlog.Errorf("%v", err)
			}
			defer func(f *os.File) {
				err := f.Close()
				if err != nil {
					doxlog.Errorf("%v", err)
					return
				}
			}(f)
			fmt.Printf("Appending doxa/bin to PATH variable in %s\n", resourceFile)
			if _, err := f.WriteString(fmt.Sprintf("%s\n", doxaBinPath)); err != nil {
				doxlog.Errorf("%v", err)
				return
			}
			if runtime.GOOS == "darwin" {
				fmt.Println("Either exit the terminal and restart it, or type `source ~/.zshrc` then press the ENTER key.")
			}
		}
	}
}

// Restore moves doxa/bin/.previous/.doxa to doxa/bin/doxa
// It then removes doxa/bin/.previous
func (d *DoxaUpdater) Restore() error {
	if !ltfile.FileExists(d.Paths.PreviousDoxaFile) {
		return errors.New("the previous version of doxa is not on your computer")
	}
	if runtime.GOOS == "windows" {
		fmt.Printf("restore does not work on Windows due to permission issues.  You need exit doxa, then manually copy\n\t%s\nto\nt\t%s\nAfter that, restart doxa.\n", d.Paths.PreviousDoxaFile, d.Paths.CurrentDoxaFile)
		return nil
	}
	q := "Restore doxa to the previous version?"
	var o = []string{
		"y", "yes",
		"n", "no",
	}
	switch clinput.GetInput(q, o) {
	case 'y':
		err := os.Rename(d.Paths.PreviousDoxaFile, d.Paths.CurrentDoxaFile)
		if err != nil {
			return err
		}
		_ = os.RemoveAll(d.Paths.PreviousDir)
		fmt.Println("doxa has been restored to the previous version")
		fmt.Println("doxa is quitting.  Restart it to run the previous version...")
		os.Exit(0)
	}
	return nil
}
