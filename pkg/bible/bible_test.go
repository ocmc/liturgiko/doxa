package bible

import (
	"fmt"
	"testing"
)

func TestNewBibleMap(t *testing.T) {
	versions := []string{"testData"}
	var m map[string]string
	var err error
	m, err = NewBibleMap(versions)
	if err != nil {
		t.Error(err)
		return
	}
	expect := 31
	got := len(m)
	if expect != got {
		t.Errorf("expected number of keys to be %d, got %d", expect, got)
	}
	for k, v := range m {
		fmt.Printf("%s: %s\n", k, v)
	}
}
func TestNormalize(t *testing.T) {
	dataIn := []string{"c1", "c1b", "0001", "01a"}
	expect := []string{"c001", "c001b", "001", "001a"}
	for i, s := range dataIn {
		g := Normalize(s)
		if expect[i] != g {
			t.Errorf("expected %s, got %s", expect[i], g)
		}
	}
}

func TestToNormalizedChapterNumber(t *testing.T) {
	dataIn := []string{"c1", "c1b", "c001", "c01a"}
	expect := []string{"c001", "c001b", "c001", "c001a"}
	for i, s := range dataIn {
		g := Normalize(s)
		if expect[i] != g {
			t.Errorf("expected %s, got %s", expect[i], g)
		}
	}
}

func TestToNormalizedVerseNumber(t *testing.T) {
	dataIn := []string{"1", "1b", "001", "01a"}
	expect := []string{"001", "001b", "001", "001a"}
	for i, s := range dataIn {
		g := Normalize(s)
		if expect[i] != g {
			t.Errorf("expected %s, got %s", expect[i], g)
		}
	}
}
