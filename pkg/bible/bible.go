// Package bible provides methods to load the Bible from git repositories into a database
// Files to be loaded must have tilde delimited lines, with each verse on a separate line.
package bible

import (
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/repos"
	"os"
	"strconv"
	"strings"
	"unicode"
)

type Loader struct {
	Delimiter     string
	FileExtension string
	Mapper        *kvs.KVS
	DbPath        string
	Parts         int
}

// NewLoader parameters:
//
//	extension - file extension used in the repos for the Biblical texts, e.g. txt
//	delimiter - the character used to delimit each line, e.g. ~
//	mapper - the mapper to the database
func NewLoader(mapper *kvs.KVS, extension, delimiter string, parts int) (*Loader, error) {
	l := new(Loader)
	l.Mapper = mapper
	l.FileExtension = extension
	l.Delimiter = delimiter
	l.Parts = parts
	return l, nil
}
func (l *Loader) Load(urls []string, showDetails bool) error {
	home, err := os.UserHomeDir()
	home = ltfile.ToUnixPath(home)
	if err != nil {
		return fmt.Errorf("error getting user home directory: %v", err)
	}
	fmt.Println("Loading Bible versions...")
	urlCount := len(urls)

	for i, repo := range urls {
		dir, err := ltfile.MkdirTemp(home, ".bible")
		if err != nil {
			return err
		}
		doxlog.Infof("Processing %d/%d: %s\n", i+1, urlCount, repo)
		err = repos.CloneTo(dir, repo, showDetails, true, true)
		if err != nil {
			_ = ltfile.DeleteDirRecursively(dir)
			errMsg := fmt.Sprintf("%v", err)
			if strings.Contains(errMsg, "authentication") {
				return fmt.Errorf("error cloning repo %s: repo not found", repo)
			} else {
				return fmt.Errorf("error cloning repo %s: %v", repo, err)
			}
		}
		var recs []*kvs.DbR
		var match []string
		match = append(match, ".*")
		files, err := ltfile.FileMatcher(dir, l.FileExtension, match)
		if err != nil {
			_ = ltfile.DeleteDirRecursively(dir)
			return fmt.Errorf("error matching cloned files: %v", err)
		}
		for _, f := range files {
			lines, err := ltfile.GetFileLines(f)
			if err != nil {
				_ = ltfile.DeleteDirRecursively(dir)
				return fmt.Errorf("error getting lines from file: %v", err)
			}
			for _, line := range lines {
				parts := strings.Split(line, l.Delimiter)
				if len(parts) != l.Parts {
					return fmt.Errorf("not enough delimited parts in line: %s in file %s", line, f)
				}
				dbr := kvs.NewDbR()
				dbr.KP.Dirs.Push("btx")
				j := len(parts)
				k := j - 2
				for i := 0; i < k; i++ {
					d := Normalize(parts[i])
					dbr.KP.Dirs.Push(d)
				}
				dbr.KP.KeyParts.Push(Normalize(parts[j-2]))
				dbr.Value = parts[j-1]
				recs = append(recs, dbr)
			}
		}
		if len(recs) > 0 {
			err := l.Mapper.Db.Batch(recs)
			if err != nil {
				_ = ltfile.DeleteDirRecursively(dir)
				return fmt.Errorf("error writing records to database: %v", err)
			}
		}
		err = ltfile.DeleteDirRecursively(dir)
	}
	return err
}
func isNumber(s string) (int, bool) {
	var i int
	var err error
	if i, err = strconv.Atoi(s); err == nil {
		return i, true
	} else {
		return 0, false
	}
}

// Normalize converts the string to lower case
// then checks to see if it contains a number.
// If it does, the number is padded with leading zeros.
// That way, all chapter and verse numbers have the same width.
func Normalize(s string) string {
	s = strings.ToLower(s)
	r := s
	// Normalize chapter numbers
	var i int
	if strings.HasPrefix(s, "c") {
		i = 1
	}
	var subLetter string
	if unicode.IsLetter(rune(r[len(r)-1])) {
		subLetter = string(r[len(r)-1])
		r = r[:len(r)-1]
	}
	if n, ok := isNumber(r[i:]); ok {
		r = fmt.Sprintf("%03d", n)
		if i == 1 {
			r = "c" + r
		}
	}
	return fmt.Sprintf("%s%s", r, subLetter)
}
func ToNormalizedChapterNumber(s string) (string, error) {
	s = strings.ToLower(s)
	r := s
	// Normalize chapter numbers
	var i int
	if strings.HasPrefix(s, "c") {
		i = 1
	}
	var subLetter string
	if unicode.IsLetter(rune(r[len(r)-1])) {
		subLetter = string(r[len(r)-1])
		r = r[:len(r)-2]
	}

	if n, ok := isNumber(r[i:]); ok {
		r = fmt.Sprintf("c%03d", n)
	} else {
		return "", fmt.Errorf("%s not a number", s)
	}
	return fmt.Sprintf("%s%s", r, subLetter), nil
}
func ToNormalizedVerseNumber(s string) (string, error) {
	s = strings.ToLower(s)
	// TODO: in the future, we need to disallow subLetters on verse number.
	var subLetter string
	if unicode.IsLetter(rune(s[len(s)-1])) {
		subLetter = string(s[len(s)-1])
		s = s[:len(s)-1]
	}
	rangeChar := "-"
	if strings.Index(s, rangeChar) > -1 {
		// this is a verse range
		var p1, p2 string
		var err error
		parts := strings.Split(s, rangeChar)
		if len(parts) != 2 {
			return "", fmt.Errorf("error slitting %s using %s: %v", s, rangeChar, err)
		}
		p1 = parts[0]
		p2 = parts[1]
		p1, err = ToNormalizedVerseNumber(p1)
		if err != nil {
			return "", fmt.Errorf("error converting %s to padded number: %v", p1, err)
		}
		p2, err = ToNormalizedVerseNumber(p2)
		if err != nil {
			return "", fmt.Errorf("error converting %s to padded number: %v", p2, err)
		}
		return fmt.Sprintf("%s-%s%s", p1, p2, subLetter), nil
	} else {
		verseInt, err := strconv.Atoi(s)
		if err != nil {
			return "", fmt.Errorf("%s not a number: %v", s, err)
		}
		return fmt.Sprintf("%03d%s", verseInt, subLetter), nil
	}
}

// IsValidAbbreviation returns true if the Upper case of s is found in the map DoxaAbr2Num
func IsValidAbbreviation(s string) bool {
	if _, ok := DoxaAbr2Num[strings.ToUpper(s)]; ok {
		return true
	}
	return false
}

/*
ΓΕΝΕΣΙΣ
ΕΞΟΔΟΣ
ΛΕΥΙΤΙΚΟΝ
ΑΡΙΘΜΟΙ
ΔΕΥΤΕΡΟΝΟΜΙΟΝ
ΙΗΣΟΥΣ ΝΑΥΗ
ΚΡΙΤΑΙ
ΡΟΥΘ
ΒΑΣΙΛΕΙΩΝ Α'
ΒΑΣΙΛΕΙΩΝ Β'
ΒΑΣΙΛΕΙΩΝ Γ'
ΒΑΣΙΛΕΙΩΝ Δ'
ΠΑΡΑΛΕΙΠΟ- ΜΕΝΩΝ Α'
ΠΑΡΑΛΕΙΠΟ- ΜΕΝΩΝ Β'
ΕΣΔΡΑΣ Α'
ΕΣΔΡΑΣ Β'
ΝΕΕΜΙΑΣ
ΤΩΒΙΤ
ΙΟΥΔΙΘ
ΕΣΘΗΡ
ΜΑΚΚΑΒΑΙΩΝ Α'
ΜΑΚΚΑΒΑΙΩΝ Β'
ΜΑΚΚΑΒΑΙΩΝ Γ'
ΨΑΛΜΟΙ
ΙΩΒ
ΠΑΡΟΙΜΙΑΙ ΣΟΛΟΜΩΝΤΟΣ
ΕΚΚΛΗΣΙΑΣΤΗΣ
ΑΣΜΑ ΑΣΜΑΤΩΝ
ΣΟΦΙΑ ΣΟΛΟΜΩΝΤΟΣ
ΣΟΦΙΑ ΣΕΙΡΑΧ
ΩΣΗΕ
ΑΜΩΣ
ΜΙΧΑΙΑΣ
ΙΩΗΛ
ΟΒΔΙΟΥ
ΙΩΝΑΣ
ΝΑΟΥΜ
ΑΜΒΑΚΟΥΜ
ΣΟΦΟΝΙΑΣ
ΑΓΓΑΙΟΣ
ΖΑΧΑΡΙΑΣ
ΜΑΛΑΧΙΑΣ
ΗΣΑΪΑΣ
ΙΕΡΕΜΙΑΣ
ΒΑΡΟΥΧ
ΘΡΗΝΟΙ ΙΕΡΕΜΙΟΥ
ΕΠΙΣΤΟΛΗ ΙΕΡΕΜΙΟΥ
ΙΕΖΕΚΙΗΛ
ΔΑΝΙΗΛ
ΜΑΚΚΑΒΑΙΩΝ Δ'
*/
type BookMeta struct {
	AbrDoxa string
	NameGr  string
	NameEn  string
	SeqDoxa string
	SeqLxx  string
}

var AltAbr2Doxa = map[string]string{
	"1CH":  "CH1", // alt
	"1CHR": "CH1", // alt
	"1CO":  "CO1", // alt
	"1JO":  "JO1", // alt
	"1JN":  "JO1", // alt
	"1KG":  "1KI", // alt
	"1KI":  "KI1", // alt
	"1PE":  "PE1", // alt
	"1SA":  "SA1", // alt
	"1TH":  "TH1", // alt
	"1TI":  "TI1", // alt
	"2CH":  "CH2", // alt
	"2CO":  "CO2", // alt
	"2JO":  "JO2", // alt
	"2JN":  "JO2", // alt
	"2KG":  "KI2", // alt
	"2KI":  "KI2", // alt
	"2PE":  "PE2", // alt
	"2SA":  "SA2", // alt
	"2TH":  "TH2", // alt
	"2TI":  "TI2", // alt
	"3JO":  "JO3", // alt
	"3JN":  "JO3", // alt
	"ACT":  "ACT",
	"AMO":  "AMO",
	"BAR":  "BAR",
	"BEL":  "BEL",
	"CH1":  "CH1",
	"CH2":  "CH2",
	"CO1":  "CO1",
	"CO2":  "CO2",
	"COL":  "COL",
	"DAN":  "DAN",
	"DEU":  "DEU",
	"DEUT": "DEU", // alt
	"ECC":  "ECC",
	"EPH":  "EPH",
	"ES1":  "ES1",
	"ES4":  "ES4",
	"EST":  "EST",
	"EXO":  "EXO",
	"EZE":  "EZE",
	"EZK":  "EZE",
	"EZR":  "EZR",
	"GAL":  "GAL",
	"GEN":  "GEN",
	"HAB":  "HAB",
	"HAG":  "HAG",
	"HEB":  "HEB",
	"HOS":  "HOS",
	"ISA":  "ISA",
	"JAM":  "JAM",
	"JAS":  "JAM",
	"JDE":  "JDE",
	"JDG":  "JDG",
	"JDT":  "JDT",
	"JER":  "JER",
	"JO1":  "JO1",
	"JO2":  "JO2",
	"JO3":  "JO3",
	"JOB":  "JOB",
	"JOE":  "JOE",
	"JOL":  "JOE",
	"JHN":  "JOH",
	"JOH":  "JOH",
	"JON":  "JON",
	"JOS":  "JOS",
	"JUD":  "JDE", // alt
	"KI1":  "KI1",
	"KI2":  "KI2",
	"LAM":  "LAM",
	"LEV":  "LEV",
	"LJE":  "LJE",
	"LUK":  "LUK",
	"MA1":  "MA1",
	"MA2":  "MA2",
	"MA3":  "MA3",
	"MA4":  "MA4",
	"MAL":  "MAL",
	"MAR":  "MAR",
	"MAT":  "MAT",
	"MIC":  "MIC",
	"MRK":  "MAR",
	"NAH":  "NAH",
	"NAM":  "NAH", // alt
	"NEH":  "NEH",
	"NUM":  "NUM",
	"OBA":  "OBA",
	"ODE":  "ODE",
	"PE1":  "PE1",
	"PE2":  "PE2",
	"PHI":  "PHP", // alt
	"PHM":  "PHM",
	"PHP":  "PHP",
	"POS":  "POS",
	"PRA":  "PRA",
	"PRM":  "PRM",
	"PRO":  "PRO",
	"PROV": "PRO", // alt
	"PS":   "PSA", // alt
	"PSA":  "PSA",
	"REV":  "REV",
	"ROM":  "ROM",
	"RUT":  "RUT",
	"SA1":  "SA1",
	"SA2":  "SA2",
	"SIR":  "SIR",
	"SOL":  "SOS", // alt
	"SON":  "SOS", // alt
	"SOS":  "SOS",
	"SNG":  "SOS", // alt
	"SUS":  "SUS",
	"TH1":  "TH1",
	"TH2":  "TH2",
	"TI1":  "TI1",
	"TI2":  "TI2",
	"TIT":  "TIT",
	"TOB":  "TOB",
	"WIS":  "WIS",
	"ZEC":  "ZEC",
	"ZEP":  "ZEP",
}
var AbrDoxa2Meta = map[string]*BookMeta{}

func GetNumForAbr(a string) (string, bool) {
	var found bool
	var n string
	if n, found = DoxaAbr2Num[a]; !found {
		if n, found = AltAbr2Doxa[a]; !found {
			return "", false
		}
		n, found = DoxaAbr2Num[n]
	}
	return n, found
}

// TODO: not finished.  Reconcile with Greek list above which is the order found on the Apostoliki Diathiki.

var DoxaAbr2Num = map[string]string{
	"GEN": "101",
	"EXO": "102",
	"LEV": "103",
	"NUM": "104",
	"DEU": "105",
	"JOS": "106",
	"JDG": "107",
	"RUT": "108",
	"SA1": "109", // 1SA
	"SA2": "110",
	"KI1": "111",
	"KI2": "112",
	"CH1": "113",
	"CH2": "114",
	"ES1": "115",
	"ES2": "116", // this is Ezr.  gr_gr_cog bible should use different?
	"EZR": "116",
	"NEH": "116", // same as EZR? Because one book in LXX?
	"TOB": "117",
	"JDT": "118",
	"ESG": "119", // translation of greek version of Esther, used in WEB
	"EST": "119",
	"MA1": "120",
	"MA2": "121",
	"MA3": "122",
	"MA4": "123", // Apostoliki Diathaniki shows this as the last book and designates it as an appendum. Might need to redo the numbering.
	"PSA": "124",
	"ODE": "125",
	"PRO": "126",
	"ECC": "127",
	"SOS": "128",
	"JOB": "129",
	"WIS": "130",
	"POS": "131",
	"SIR": "132",
	"HOS": "133",
	"AMO": "134",
	"MIC": "135",
	"JOE": "136",
	"OBA": "137",
	"JON": "138",
	"NAH": "139",
	"HAB": "140",
	"ZEP": "141",
	"HAG": "142",
	"ZEC": "143",
	"MAL": "144",
	"ISA": "145",
	"JER": "146",
	"BAR": "147",
	"LAM": "148",
	"LJE": "149",
	"EZE": "150",
	"SUS": "151",
	"DAN": "152",
	"BEL": "153",
	"PRA": "154",
	"PRM": "155",
	"ES4": "156",
	"PSX": "157", // Psalm 151
	// New Testament
	"MAT": "201",
	"MAR": "202",
	"LUK": "203",
	"JOH": "204",
	"ACT": "205",
	"ROM": "206",
	"CO1": "207",
	"CO2": "208",
	"GAL": "209",
	"EPH": "210",
	"PHP": "211",
	"COL": "212",
	"TH1": "213",
	"TH2": "214",
	"TI1": "215",
	"TI2": "216",
	"TIT": "217",
	"PHM": "218",
	"HEB": "219",
	"JAM": "220",
	"PE1": "221",
	"PE2": "222",
	"JO1": "223",
	"JO2": "224",
	"JO3": "225",
	"JDE": "226",
	"REV": "227",
}

type Book struct {
	Abr                     string
	Chap                    int
	SQN                     int
	TitleEnglish            string
	TitleEnglishShort       string
	TitleEnglishSupperShort string
	TitleGreek              string
	TitleGreekShort         string
	TitleGreekSuperShort    string
}

func (b Book) ToSqnAbr() string {
	return fmt.Sprintf("%02d.%s", b.SQN, b.Abr)
}
func (b Book) ToChapterFilename(chapter int, suffix string) string {
	if chapter == 0 {
		return fmt.Sprintf("%s.%s", b.ToSqnAbr(), suffix)
	} else {
		return fmt.Sprintf("%s.%03d.%s", b.ToSqnAbr(), chapter, suffix)
	}
}

var NTBooks = []Book{
	{"MAT", 28, 40, "The Gospel According to Matthew", "According to Matthew", "Matthew", "ΤΟ ΚΑΤΑ ΜΑΤΘΑΙΟΝ ΕΥΑΓΓΕΛΙΟΝ", "ΚΑΤΑ ΜΑΤΘΑΙΟΝ", "ΜΑΤΘΑΙΟΝ"},
	{"MRK", 16, 41, "The Gospel According to Mark", "According to Mark", "Mark", "ΤΟ ΚΑΤΑ ΜΑΡΚΟΝ ΕΥΑΓΓΕΛΙΟΝ", "ΚΑΤΑ ΜΑΡΚΟΝ", "ΜΑΡΚΟΝ"},
	{"LUK", 24, 42, "The Gospel According to Luke", "According to Luke", "Luke", "ΤΟ ΚΑΤΑ ΛΟΥΚΑΝ ΕΥΑΓΓΕΛΙΟΝ", "ΚΑΤΑ ΛΟΥΚΑΝ", "ΛΟΥΚΑΝ"},
	{"JHN", 21, 43, "The Gospel According to John", "According to John", "John", "ΤΟ ΚΑΤΑ ΙΩΑΝΝΗΝ ΕΥΑΓΓΕΛΙΟΝ", "ΚΑΤΑ ΙΩΑΝΝΗΝ", "ΙΩΑΝΝΗΝ"},
	{"ACT", 28, 44, "The Acts of the Apostles", "Acts", "Acts", "ΠΡΑΞΕΙΣ ΤΩΝ ΑΠΟΣΤΟΛΩΝ", "ΠΡΑΞΕΙΣ", "ΠΡΑΞΕΙΣ"},
	{"ROM", 16, 45, "Letter to the Romans", "To the Romans", "Romans", "ΕΠΙΣΤΟΛΗ ΠΡΟΣ ΡΩΜΑΙΟΥΣ", "ΠΡΟΣ ΡΩΜΑΙΟΥΣ", "ΡΩΜΑΙΟΥΣ"},
	{"1CO", 16, 46, "First Letter to the Corinthians", "1st to the Corinthians", "1 Corinthians", "ΕΠΙΣΤΟΛΗ ΠΡΟΣ ΚΟΡΙΝΘΙΟΥΣ Α΄", "ΠΡΟΣ ΚΟΡΙΝΘΙΟΥΣ Α΄", "ΚΟΡΙΝΘΙΟΥΣ Α΄"},
	{"2CO", 13, 47, "Second Letter to the Corinthians", "2nd to the Corinthians", "2 Corinthians", "ΕΠΙΣΤΟΛΗ ΠΡΟΣ ΚΟΡΙΝΘΙΟΥΣ Β΄", "ΠΡΟΣ ΚΟΡΙΝΘΙΟΥΣ Β΄", "ΚΟΡΙΝΘΙΟΥΣ Β΄"},
	{"GAL", 6, 48, "Letter to the Galatians", "To the Galatians", "Galatians", "ΕΠΙΣΤΟΛΗ ΠΡΟΣ ΓΑΛΑΤΑΣ", "ΠΡΟΣ ΓΑΛΑΤΑΣ", "ΓΑΛΑΤΑΣ"},
	{"EPH", 6, 49, "Letter to the Ephesians", "To the Ephesians", "Ephesians", "ΕΠΙΣΤΟΛΗ ΠΡΟΣ ΕΦΕΣΙΟΥΣ", "ΠΡΟΣ ΕΦΕΣΙΟΥΣ", "ΕΦΕΣΙΟΥΣ"},
	{"PHP", 4, 50, "Letter to the Philippians", "To the Philippians", "Philippians", "ΕΠΙΣΤΟΛΗ ΠΡΟΣ ΦΙΛΙΠΠΗΣΙΟΥΣ", "ΠΡΟΣ ΦΙΛΙΠΠΗΣΙΟΥΣ", "ΦΙΛΙΠΠΗΣΙΟΥΣ"},
	{"COL", 4, 51, "Letter to the Colossians", "To the Colossians", "Colossians", "ΕΠΙΣΤΟΛΗ ΠΡΟΣ ΚΟΛΟΣΣΑΕΙΣ", "ΠΡΟΣ ΚΟΛΟΣΣΑΕΙΣ", "ΚΟΛΟΣΣΑΕΙΣ"},
	{"1TH", 5, 52, "First Letter to Thessalonians", "1st to the Thessalonians", "1 Thessalonians", "ΕΠΙΣΤΟΛΗ ΠΡΟΣ ΘΕΣΣΑΛΟΝΙΚΕΙΣ Α΄", "ΠΡΟΣ ΘΕΣΣΑΛΟΝΙΚΕΙΣ Α΄", "ΘΕΣΣ. Α΄"},
	{"2TH", 3, 53, "Second Letter to the Thessalonians", "2nd to the Thessalonians", "2 Thessalonians", "ΕΠΙΣΤΟΛΗ ΠΡΟΣ ΘΕΣΣΑΛΟΝΙΚΕΙΣ Β΄", "ΠΡΟΣ ΘΕΣΣΑΛΟΝΙΚΕΙΣ Β΄", "ΘΕΣΣ. Β΄"},
	{"1TI", 6, 54, "First Letter to Timothy", "1st to Timothy", "1 Timothy", "ΕΠΙΣΤΟΛΗ ΠΡΟΣ ΤΙΜΟΘΕΟΝ Α΄", "ΠΡΟΣ ΤΙΜΟΘΕΟΝ Α΄", "ΤΙΜΟΘΕΟΝ Α΄"},
	{"2TI", 4, 55, "Second Letter to Timothy", "2nd to Timothy", "2 Timothy", "ΕΠΙΣΤΟΛΗ ΠΡΟΣ ΤΙΜΟΘΕΟΝ Β΄", "ΠΡΟΣ ΤΙΜΟΘΕΟΝ Β΄", "ΤΙΜΟΘΕΟΝ Β΄"},
	{"TIT", 3, 56, "Letter to Titus", "To Titus", "Titus", "ΕΠΙΣΤΟΛΗ ΠΡΟΣ ΤΙΤΟΝ", "ΠΡΟΣ ΤΙΤΟΝ", "ΤΙΤΟΝ"},
	{"PHM", 0, 57, "Letter to Philemon", "To Philemon", "Philemon", "ΕΠΙΣΤΟΛΗ ΠΡΟΣ ΦΙΛΗΜΟΝΑ", "ΠΡΟΣ ΦΙΛΗΜΟΝΑ", "ΦΙΛΗΜΟΝΑ"},
	{"HEB", 13, 58, "Letter to the Hebrews", "To the Hebrews", "Hebrews", "ΕΠΙΣΤΟΛΗ ΠΡΟΣ ΕΒΡΑΙΟΥΣ", "ΠΡΟΣ ΕΒΡΑΙΟΥΣ", "ΕΒΡΑΙΟΥΣ"},
	{"JAS", 5, 59, "Letter of James", "By James", "James", "ΕΠΙΣΤΟΛΗ ΙΑΚΩΒΟΥ", "ΙΑΚΩΒΟΥ", "ΙΑΚΩΒΟΥ"},
	{"1PE", 5, 60, "First Letter of Peter", "1st by Peter", "1 Peter", "ΕΠΙΣΤΟΛΗ ΠΕΤΡΟΥ Α΄", "ΠΕΤΡΟΥ Α΄", "ΠΕΤΡΟΥ Α΄"},
	{"2PE", 3, 61, "Second Letter of Peter", "2nd by Peter", "2 Peter", "ΕΠΙΣΤΟΛΗ ΠΕΤΡΟΥ Β΄", "ΠΕΤΡΟΥ Β΄", "ΠΕΤΡΟΥ Β΄"},
	{"1JN", 5, 62, "First Letter of John", "1st by John", "1 John", "ΕΠΙΣΤΟΛΗ ΙΩΑΝΝΟΥ Α΄", "ΙΩΑΝΝΟΥ Α΄", "ΙΩΑΝΝΟΥ Α΄"},
	{"2JN", 0, 63, "Second Letter of John", "2nd by John", "2 John", "ΕΠΙΣΤΟΛΗ ΙΩΑΝΝΟΥ Β΄", "ΙΩΑΝΝΟΥ Α΄", "ΙΩΑΝΝΟΥ Α΄"},
	{"3JN", 0, 64, "Third Letter of John", "3rd by John", "3 John", "ΕΠΙΣΤΟΛΗ ΙΩΑΝΝΟΥ Γ΄", "ΙΩΑΝΝΟΥ Γ΄", "ΙΩΑΝΝΟΥ Γ΄"},
	{"JUD", 0, 65, "Letter of Jude", "By Jude", "Jude", "ΕΠΙΣΤΟΛΗ ΙΟΥΔΑ", "ΙΟΥΔΑ", "ΙΟΥΔΑ"},
	{"REV", 22, 66, "Revelation of John", "Revelation of John", "Revelation", "ΑΠΟΚΑΛΥΨΙΣ ΙΩΑΝΝΟΥ", "ΑΠΟΚΑΛΥΨΙΣ ΙΩΑΝΝΟΥ", "ΑΠΟΚΑΛΥΨΙΣ"},
}

/*
\itId{en}{uk}{lash}{titles}{KyriakiProsefchi.title.toc}{
\itRid{titles}{KyriakiProsefchi.title.doc}
}%
\itId{en}{uk}{lash}{baptism}{p0001.title.header}{
The Sacrament of Holy Baptism
}%
\itId{en}{uk}{lash}{baptism}{p0001.title.toc}{
Holy Baptism
}%
\itId{en}{uk}{lash}{baptism}{p0002.title.cover}{
\itRid{baptism}{p0002.title.doc}
}%
\itId{en}{uk}{lash}{baptism}{p0002.title.doc}{
Order That Takes Place Before Holy Baptism
}%

*/

func NtOslwTitles(lang, country, domain string, superShortHeader bool) []string {
	var titles []string
	var title string
	var chapterName string
	var chapterNameShort string
	if lang == "gr" {
		chapterName = "ΚΕΦΆΛΑΙΟ"
		chapterNameShort = "Κεφ."
	} else {
		chapterName = "Chapter"
		chapterNameShort = "Chap."
	}
	for i := 1; i < 29; i++ {
		title = fmt.Sprintf("\\itId{%s}{%s}{%s}{titles}{chapter%d.title.header}{\n%s %d\n}%%", lang, country, domain, i, chapterNameShort, i)
		titles = append(titles, title)
		title = fmt.Sprintf("\\itId{%s}{%s}{%s}{titles}{chapter%d.title.toc}{\n%s %d\n}%%", lang, country, domain, i, chapterNameShort, i)
		titles = append(titles, title)
		title = fmt.Sprintf("\\itId{%s}{%s}{%s}{titles}{chapter%d.title.cover}{\n%s %d\n}%%", lang, country, domain, i, chapterName, i)
		titles = append(titles, title)
		title = fmt.Sprintf("\\itId{%s}{%s}{%s}{titles}{chapter%d.title.doc}{\n%s %d\n}%%", lang, country, domain, i, chapterName, i)
		titles = append(titles, title)
	}
	for _, b := range NTBooks {
		if lang == "gr" {
			if superShortHeader {
				title = fmt.Sprintf("\\itId{%s}{%s}{%s}{titles}{%s.title.header}{\n%s\n}%%", lang, country, domain, strings.ToLower(b.Abr), b.TitleGreekSuperShort)
			} else {
				title = fmt.Sprintf("\\itId{%s}{%s}{%s}{titles}{%s.title.header}{\n%s\n}%%", lang, country, domain, strings.ToLower(b.Abr), b.TitleGreekShort)
			}
			titles = append(titles, title)
			title = fmt.Sprintf("\\itId{%s}{%s}{%s}{titles}{%s.title.toc}{\n%s\n}%%", lang, country, domain, strings.ToLower(b.Abr), b.TitleGreekShort)
			titles = append(titles, title)
			title = fmt.Sprintf("\\itId{%s}{%s}{%s}{titles}{%s.title.cover}{\n%s\n}%%", lang, country, domain, strings.ToLower(b.Abr), b.TitleGreek)
			titles = append(titles, title)
			title = fmt.Sprintf("\\itId{%s}{%s}{%s}{titles}{%s.title.doc}{\n%s\n}%%", lang, country, domain, strings.ToLower(b.Abr), b.TitleGreek)
			titles = append(titles, title)
		} else {
			if superShortHeader {
				title = fmt.Sprintf("\\itId{%s}{%s}{%s}{titles}{%s.title.header}{\n%s\n}%%", lang, country, domain, strings.ToLower(b.Abr), b.TitleEnglishSupperShort)
			} else {
				title = fmt.Sprintf("\\itId{%s}{%s}{%s}{titles}{%s.title.header}{\n%s\n}%%", lang, country, domain, strings.ToLower(b.Abr), b.TitleEnglishShort)
			}
			title = fmt.Sprintf("\\itId{%s}{%s}{%s}{titles}{%s.title.header}{\n%s\n}%%", lang, country, domain, strings.ToLower(b.Abr), b.TitleEnglishShort)
			titles = append(titles, title)
			title = fmt.Sprintf("\\itId{%s}{%s}{%s}{titles}{%s.title.toc}{\n%s\n}%%", lang, country, domain, strings.ToLower(b.Abr), b.TitleEnglishShort)
			titles = append(titles, title)
			title = fmt.Sprintf("\\itId{%s}{%s}{%s}{titles}{%s.title.cover}{\n%s\n}%%", lang, country, domain, strings.ToLower(b.Abr), b.TitleEnglish)
			titles = append(titles, title)
			title = fmt.Sprintf("\\itId{%s}{%s}{%s}{titles}{%s.title.doc}{\n%s\n}%%", lang, country, domain, strings.ToLower(b.Abr), b.TitleEnglish)
			titles = append(titles, title)
		}
	}
	return titles
}

// Note: get Greek titles from https://www.wordproject.org/bibles/gk_2/index.htm

var OTBooks = []Book{
	{"Gen", 50, 1, "", "", "", "", "", ""},
	{"Exod", 40, 2, "", "", "", "", "", ""},
	{"Lev", 27, 3, "", "", "", "", "", ""},
	{"Num", 36, 4, "", "", "", "", "", ""},
	{"Deut", 34, 5, "", "", "", "", "", ""},
	{"Josh", 24, 6, "", "", "", "", "", ""},
	{"Judg", 21, 7, "", "", "", "", "", ""},
	{"Ruth", 4, 8, "", "", "", "", "", ""},
	{"1Sam", 31, 9, "", "", "", "", "", ""},
	{"2Sam", 24, 10, "", "", "", "", "", ""},
	{"1Kgs", 22, 11, "", "", "", "", "", ""},
	{"2Kgs", 25, 12, "", "", "", "", "", ""},
	{"1Chr", 29, 13, "", "", "", "", "", ""},
	{"2Chr", 36, 14, "", "", "", "", "", ""},
	{"Ezra", 10, 15, "", "", "", "", "", ""},
	{"Neh", 13, 16, "", "", "", "", "", ""},
	{"Esth", 10, 17, "", "", "", "", "", ""},
	{"Job", 42, 18, "", "", "", "", "", ""},
	{"Ps", 150, 19, "", "", "", "", "", ""},
	{"Prov", 31, 20, "", "", "", "", "", ""},
	{"Eccl", 12, 21, "", "", "", "", "", ""},
	{"Song", 8, 22, "", "", "", "", "", ""},
	{"Isa", 66, 23, "", "", "", "", "", ""},
	{"Jer", 52, 24, "", "", "", "", "", ""},
	{"Lam", 5, 25, "", "", "", "", "", ""},
	{"Ezek", 48, 26, "", "", "", "", "", ""},
	{"Dan", 12, 27, "", "", "", "", "", ""},
	{"Hos", 14, 28, "", "", "", "", "", ""},
	{"Joel", 3, 29, "", "", "", "", "", ""},
	{"Amos", 9, 30, "", "", "", "", "", ""},
	{"Obad", 0, 31, "", "", "", "", "", ""},
	{"Jonah", 4, 32, "", "", "", "", "", ""},
	{"Mic", 7, 33, "", "", "", "", "", ""},
	{"Nah", 3, 34, "", "", "", "", "", ""},
	{"Hab", 3, 35, "", "", "", "", "", ""},
	{"Zeph", 3, 36, "", "", "", "", "", ""},
	{"Hag", 2, 37, "", "", "", "", "", ""},
	{"Zech", 14, 38, "", "", "", "", "", ""},
	{"Mal", 4, 39, "", "", "", "", "", ""},
}
var EBibleAbr = []string{
	"MAT",
	"MRK",
	"LUK",
	"JHN",
	"ACT",
	"ROM",
	"1CO",
	"2CO",
	"GAL",
	"EPH",
	"PHP",
	"COL",
	"1TH",
	"2TH",
	"1TI",
	"2TI",
	"TIT",
	"PHM",
	"HEB",
	"JAS",
	"1PE",
	"2PE",
	"1JN",
	"2JN",
	"3JN",
	"JUD",
	"REV"}

func NewBibleMap(versionDirs []string) (map[string]string, error) {
	var err error
	m := make(map[string]string)
	for _, versionDir := range versionDirs {
		var theFilePaths []string
		theFilePaths, err = ltfile.FileMatcher(versionDir, "txt", nil)
		if err != nil {
			return nil, fmt.Errorf("%s: %v", versionDir, err)
		}
		for _, f := range theFilePaths {
			var lines []string
			lines, err = ltfile.GetFileLines(f)
			if err != nil {
				return nil, fmt.Errorf("%s: %v", f, err)
			}
			for _, l := range lines {
				var key, value string
				key, value, err = KeyValue(l)
				if err != nil {
					return nil, fmt.Errorf("%s - %s: %v", f, l, err)
				}
				m[strings.ToLower(key)] = value
			}
		}
	}
	return m, err
}

// KeyValue splits a tilde delimited Biblical verse line into the key and value parts.
func KeyValue(l string) (string, string, error) {
	parts := strings.Split(l, "~")
	if len(parts) != 5 {
		return "", "", fmt.Errorf("invalid format %s", l)
	}
	return fmt.Sprintf("%s.%s.%s.%s", parts[0], parts[1], Normalize(parts[2]), Normalize(parts[3])), parts[4], nil
}

/**
TODO:
 1. Write a map that allows conversion from LXX numbering to Masoretic and back
    and from PGNT to Nestle-Aland and back.
    key: lxx.{bk}.{chap}.{verse} <- Masoretic (MT)
          mt.{bk}.{chap}.{verse} <- lxx
         pgnt.{bk}.{chap}.{verse} <- na
*/
