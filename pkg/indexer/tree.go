package indexer

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/generators/html"
	"sort"
	"strings"
)

type MetaData struct {
	Data []*IndexMeta
	Root *html.TreeNode
}

func NewMetaData(data []*IndexMeta) (md *MetaData, err error) {
	md = new(MetaData)
	md.Data = data
	md.Root, err = md.Build()
	// build the language maps
	langNames := make(map[string]string)
	langCodes := make(map[string]string)
	for _, meta := range md.Data {
		langCodes[meta.LanguageCodes] = meta.LanguageCodes
		langNames[meta.LanguageNames] = meta.LanguageNames
	}
	// create a string slice from the keys of each map
	langCodesSlice := make([]string, 0, len(langCodes))
	for k := range langCodes {
		langCodesSlice = append(langCodesSlice, k)
	}
	langNamesSlice := make([]string, 0, len(langNames))
	for k := range langNames {
		langNamesSlice = append(langNamesSlice, k)
	}
	// sort the two slices
	sort.Strings(langCodesSlice)
	sort.Strings(langNamesSlice)
	md.Root.LanguageCodes = strings.Join(langCodesSlice, ",")
	md.Root.LanguageNames = strings.Join(langNamesSlice, ",")
	return md, err
}
func (md *MetaData) AddNode(node *html.TreeNode, value string, i, j int, leaf bool) (*html.TreeNode, error) {
	// Check if the node already exists among the children.
	for _, child := range node.Children {
		if child.Title == value {
			// by returning this child, we are combining
			// like values into a single parent node, so we get this
			// kind of tree:
			// Euchologion
			//   |- Baptism
			//        |- Print
			//            |- English <-- becomes an anchor
			//            |- Greek-English <-- becomes an anchor
			//        |- View
			//            |- English <-- becomes an anchor
			//            |- Greek-English <-- becomes an anchor
			//   |- Liturgy
			// etc.
			return child, nil
		}
	}

	// If the node does not exist, create a new one and add it to the children
	newNode := &html.TreeNode{ID: fmt.Sprintf("n%d%d", i, j), Title: value}
	newNode.Class = fmt.Sprintf("indexL%d", j) // can be overwritten below
	if leaf {
		if i > len(md.Data) {
			msg := fmt.Sprintf("index %d exceeds length of md.Data", i)
			doxlog.Errorf(msg)
			return nil, fmt.Errorf(msg)
		}
		if len(md.Data[i].Href) == 0 {
			msg := fmt.Sprintf("index metadata %d missing value for Href", i)
			doxlog.Errorf(msg)
			return nil, fmt.Errorf(msg)
		}
		// a leaf node must be populated with info to create an HTML anchor.
		newNode.Class = "indexAnchor" // overrides
		newNode.Href = md.Data[i].Href
		newNode.Title = md.Data[i].LanguageNames
		newNode.Pdf = md.Data[i].Pdf
		newNode.PdfHref = md.Data[i].PdfHref
	}
	node.Children = append(node.Children, newNode)
	return newNode, nil
}

// Build creates a tree out of md.Data []*IndexMeta.
// A pointer to the root node of the tree is returned.
// Leaf nodes have all the information needed to create an anchor.
// The tree can therefore be used to create a hierarchical index
// for a generated website.
func (md *MetaData) Build() (*html.TreeNode, error) {
	var err error
	root := &html.TreeNode{Title: "root"} // Starting point of the tree
	for i, row := range md.Data {
		// Each row is the metadata for an index for a specific HTML or PDF file
		// in a generated DCS website.
		// Each row contains a Titles slice, which provides
		// the hierarchical structure.
		// The first element of each Titles slice results in
		// a node that is a child of the root.
		// The last element of each Titles slice results in
		// a terminal node that is populated with the information
		// needed to create an anchor.
		l := len(row.Titles) - 1 // index of the final element in the titles slice
		currentNode := root
		for j, value := range row.Titles {
			leaf := j == l // let AddNode know that this will be a leaf node.
			currentNode, err = md.AddNode(currentNode, value, i, j, leaf)
			if err != nil {
				return nil, err
			}
		}
	}
	return root, nil
}
func (md *MetaData) PrintTree(node *html.TreeNode, level int) {
	if node == nil {
		return
	}

	fmt.Printf("%s%s\n", " "+strings.Repeat("-", level*2), node.Title)
	for _, child := range node.Children {
		md.PrintTree(child, level+1)
	}
}

// MetaTitlesTable creates a table such that each row is the TitleCodes []string
// from each element of data.  It appends the data index to each row of the
// table so that the source element in data can subsequently be read.
// The main purpose of this function is to prepare the data so that
// a tree can be created that combines repeating node names.
func (md *MetaData) dataToTable() (table [][]string) {
	for i, im := range md.Data {
		table[i] = im.TitleCodes
		table[i] = append(table[i], fmt.Sprintf("%d", i))
	}
	return table
}
