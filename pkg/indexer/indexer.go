// Package indexer provides functions to read
// the books and services in a generated website
// and create the index pages for the user to
// navigate and select the desired book or service.
// There are separate functions to index books versus services.
package indexer

import (
	"encoding/json"
	"fmt"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/enums/books"
	"github.com/liturgiko/doxa/pkg/enums/mediaTypes"
	"github.com/liturgiko/doxa/pkg/enums/officeTypes"
	"github.com/liturgiko/doxa/pkg/enums/templateTypes"
	"github.com/liturgiko/doxa/pkg/generators/html"
	"github.com/liturgiko/doxa/pkg/media"
	"github.com/liturgiko/doxa/pkg/models"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"html/template"
	"os"
	"path"
	"sort"
	"strconv"
	"strings"
	"time"
)

// IndexBooks reads the generated site and builds an index of Books
func IndexBooks(bc *models.BuildConfig) ([]string, error) {
	funcId := "pkg/indexer/indexer.go IndexBooks() "
	var msg string
	var messages []string
	var booksIndexMetaData []*IndexMeta
	var err error

	if bc.OutputHtml {
		htmlDir := path.Join(bc.SitePath, "h", "b")
		if !ltfile.DirExists(htmlDir) {
			msg = fmt.Sprintf("%s no book html files found in %s", funcId, htmlDir)
			messages = append(messages, msg)
		} else { // exists
			var instances []*IndexMeta
			instances, err = LoadIndexMetaData(htmlDir)
			if err != nil {
				messages = append(messages, fmt.Sprintf("%v", err))
			} else {
				if instances == nil {
					messages = append(messages, fmt.Sprintf("could not find index.json files in %s", htmlDir))
				} else {
					booksIndexMetaData = append(booksIndexMetaData, instances...)
				}
			}
		}
	}
	if bc.OutputPdf {
		pdfDir := path.Join(bc.SitePath, "p", "b")
		if !ltfile.DirExists(pdfDir) {
			msg = fmt.Sprintf("%s pdf directory %s does not exist", funcId, pdfDir)
			messages = append(messages, msg)
		} else {
			var instances []*IndexMeta
			instances, err = LoadIndexMetaData(pdfDir)
			if err != nil {
				messages = append(messages, fmt.Sprintf("%v", err))
			} else {
				booksIndexMetaData = append(booksIndexMetaData, instances...)
			}
		}
	}
	booksIndexMetaData = SetSortOrder(booksIndexMetaData, bc)
	// serialize the flat version of the books index to json
	err = writeJsonFlatBooksIndex(bc.SitePath, "booksIndexFlat.json", booksIndexMetaData, true)
	if err != nil {
		doxlog.Errorf(err.Error())
	}

	// create the object that holds the information for the master books index
	bmi := new(html.BooksMasterAccordionIndex)
	bmi.Title = bc.IndexBooksTabTitle
	bmi.SubTitle = bc.IndexBooksTitle
	bmi.DoxaVersion = bc.DoxaVersion
	bmi.Links = bc.Links
	bmi.Scripts = bc.Scripts
	bmi.Navbar = bc.IndexNavbar
	bmi.Footer = bc.Footer
	bmi.FramesEnabled = bc.FramesEnabled
	bmi.Desc = bc.IndexBooksDescription
	bmi.Search = bc.IndexBooksSearchPrompt

	var metaData *MetaData
	metaData, err = NewMetaData(booksIndexMetaData)
	if err != nil {
		doxlog.Errorf(err.Error())
		return messages, err
	}
	bmi.Root = metaData.Root

	// serialize the hierarchical version of the books index to json
	err = writeJsonHierarchicalBooksIndex(bc.SitePath, "booksIndexHierarchical.json", metaData.Root, true)
	if err != nil {
		doxlog.Errorf(err.Error())
		return messages, err
	}
	// create books index
	pathOut := path.Join(bc.SitePath, "booksindex.html")
	err = createMasterBooksIndexFile(bc.Templates, pathOut, *bmi)
	if err != nil {
		msg = fmt.Sprintf("%s error creating master books index file %s: %v", funcId, pathOut, err)
		messages = append(messages, msg)
	}

	return messages, nil
}

// IndexServices reads the generated site and builds an index of services.
func IndexServices(bc *models.BuildConfig) []error {
	var servicesIndexData indexAttribCollection
	var files []string
	var pdfFiles []string
	// filter for only files that have name 'index'
	var expressions = []string{
		"index",
	}
	var errors []error
	var err error
	htmlDir := path.Join(bc.SitePath, "h", "s")
	if bc.OutputHtml {
		if !ltfile.DirExists(htmlDir) {
			errors = append(errors, fmt.Errorf("error in indexer.IndexServices: html directory %s does not exist", htmlDir))
		} else { // exists
			files, err = ltfile.FileMatcher(htmlDir, "html", expressions)
			if err != nil {
				errors = append(errors, fmt.Errorf("error in indexer.IndexServices - no service html files found in %s", htmlDir))
			}
		}
	}
	pdfDir := path.Join(bc.SitePath, "p", "s")
	if bc.OutputPdf {
		if !ltfile.DirExists(pdfDir) {
			errors = append(errors, fmt.Errorf("error in indexer.IndexServices: pdf directory %s does not exist", pdfDir))
		} else { // exists
			pdfFiles, err = ltfile.FileMatcher(pdfDir, "html", expressions)
			if err != nil {
				errors = append(errors, fmt.Errorf("error in indexer.IndexServices - no service html files found in %s", pdfDir))
			} else {
				files = append(files, pdfFiles...)
			}
		}
	}
	if len(files) == 0 {
		errors = append(errors, fmt.Errorf("error in indexer.IndexServices - no service html or pdf files found in %s", bc.SitePath))
	}
	for _, f := range files {
		attribs, err := ParsePath(f, bc)
		if err != nil {
			errors = append(errors, err)
			continue
		}
		switch attribs.TemplateType {
		case templateTypes.Service:
			servicesIndexData = append(servicesIndexData, attribs)
		}
	}
	sort.Sort(servicesIndexData)

	// create the subdirectory to hold day indexes
	err = ltfile.CreateDirs(path.Join(bc.SitePath, "indexes"))
	if err != nil {
		errors = append(errors, err)
		return errors
	}

	// create the object that holds the information for the master service index
	smi := new(html.ServicesMasterIndex)
	smi.DoxaVersion = bc.DoxaVersion
	smi.Title = bc.IndexYearMonthTitle
	smi.SubTitle = bc.IndexYearMonthSubTitle
	smi.Links = bc.Links
	smi.Scripts = bc.Scripts
	smi.Navbar = bc.IndexNavbar
	smi.Footer = bc.Footer
	smi.FramesEnabled = bc.FramesEnabled
	// create the 1st object instance that holds info for the services for a specific year and month
	smyi := new(html.ServiceMonthYearIndex)

	langCodeMap := make(map[string]string)
	langNameMap := make(map[string]string)

	// create the 1st object instance that provides a link to the services for a specific day
	//sdi := new(html.ServiceDaysIndex)

	// create the 1st object instance that holds info for services for a specific day
	serviceDay := new(html.ServiceDayIndex)
	service := new(html.DocIndex)

	// In order to build a hierarchical representation of the services,
	// we have to read the path to each index.html file in the generated
	// site, parse the path segments, and replace them with
	// a user-friendly label.

	// write each day index
	for _, s := range servicesIndexData {
		if !serviceDay.Date.Equal(s.Date) {
			if len(serviceDay.Title) > 0 {
				serviceDay.Services = append(serviceDay.Services, *service)
				pathOut := path.Join(bc.SitePath, "indexes", serviceDay.Date.Format("20060102"))
				pathOut = pathOut + ".html"
				err = createDayIndexFile(bc.Templates, pathOut, *serviceDay)
				if err != nil {
					errors = append(errors, err)
					continue
				}
				_, filename := path.Split(pathOut)
				serviceDay.Href = path.Join("indexes", filename)
				smyi.Days = append(smyi.Days, serviceDay)
				serviceDay = new(html.ServiceDayIndex)
				service = new(html.DocIndex)
			}
			serviceDay = new(html.ServiceDayIndex)
			serviceDay.Date = s.Date
			serviceDay.DoxaVersion = bc.DoxaVersion
			serviceDay.BaseHref = "../"
			serviceDay.Links = bc.Links
			serviceDay.Scripts = bc.Scripts
			serviceDay.Title = bc.IndexDayTitle
			serviceDay.Navbar = bc.IndexNavbar
			serviceDay.Footer = bc.Footer
			serviceDay.FramesEnabled = bc.FramesEnabled

			sb := strings.Builder{}
			sb.WriteString(bc.IndexDaySubTitleDatePrefix)
			sb.WriteString(bc.LocaleDate(s.Date.Year(), int(s.Date.Month()), s.Date.Day(), bc.IndexDaySubTitleDateFormat))
			sb.WriteString(bc.IndexDaySubTitleDateSuffix)
			serviceDay.SubTitle = sb.String()

			for _, desc := range bc.IndexDayDescription {
				serviceDay.Description = append(serviceDay.Description, desc)
			}
		}
		monthYear := fmt.Sprintf("%s %s", s.MonthTitle, s.Year)
		if smyi.MonthYear != monthYear {
			if len(smyi.MonthYear) != 0 {
				smi.MonthYears = append(smi.MonthYears, smyi)
				smyi = new(html.ServiceMonthYearIndex)
			}
			smyi.DoxaVersion = bc.DoxaVersion
			smyi.MonthYear = monthYear
			smyi.MonthNbr = int(s.Date.Month())
			smyi.YearNbr = s.Date.Year()
		}
		if len(serviceDay.DayNbrDow) == 0 {
			serviceDay.DayNbrDow = fmt.Sprintf("%s-%s", s.Day, s.DOWTitle)
		}
		serviceDay.DayNbr = s.Date.Day()
		if service.Title != s.OfficeTitle {
			if len(service.Title) > 0 {
				serviceDay.Services = append(serviceDay.Services, *service)
			}
			service = new(html.DocIndex)
			service.Title = s.OfficeTitle
		}
		serviceLanguage := new(html.DocLanguage)
		serviceLanguage.Href = s.HrefIndexHtml
		serviceLanguage.Language = s.Language
		langCodeMap[s.LanguageCode] = s.LanguageCode
		langNameMap[s.Language] = s.Language
		if len(s.HrefPdf) > 0 {
			serviceLanguage.PdfHref = s.HrefPdf
			serviceLanguage.PDF = true
		}
		serviceLanguage.Title = s.OfficeTitle
		serviceLanguage.Media = s.MediaTitle
		if strings.HasPrefix(serviceLanguage.Href, "p") {
			serviceLanguage.Target = "_blank"
		} else { // it is a service, not a book
			serviceLanguage.Target = "_self"
		}
		service.Languages = append(service.Languages, *serviceLanguage)
	}
	if len(serviceDay.Title) > 0 {
		serviceDay.Services = append(serviceDay.Services, *service)
		pathOut := path.Join(bc.SitePath, "indexes", serviceDay.Date.Format("20060102"))
		pathOut = pathOut + ".html"
		err = createDayIndexFile(bc.Templates, pathOut, *serviceDay)
		if err != nil {
			errors = append(errors, err)
		}
		_, filename := path.Split(pathOut)
		serviceDay.Href = path.Join("indexes", filename)
		smyi.Days = append(smyi.Days, serviceDay)
	}
	smi.MonthYears = append(smi.MonthYears, smyi)
	// Extract the keys from the maps
	for key := range langCodeMap {
		smi.LangCodes = append(smi.LangCodes, key)
	}
	for key := range langNameMap {
		smi.LangNames = append(smi.LangNames, key)
	}

	// Sort the keys
	sort.Strings(smi.LangCodes)
	sort.Strings(smi.LangNames)

	pathOut := path.Join(bc.SitePath, "servicesindex.html")
	err = createMasterServicesIndexFile(bc.Templates, pathOut, *smi)
	if err != nil {
		errors = append(errors, err)
	}

	err = writeJsonServicesIndex(bc.SitePath, "servicesindex.json", smi, true)
	return errors
}

func writeJsonServicesIndex(pathOut, filename string, smi *html.ServicesMasterIndex, prettyPrint bool) error {
	var contents []byte
	var err error
	// MAC.  2024-01-16.
	// The index pages point to an index.html for pdfs, which
	// when reached loads the pdf.
	// Bruce Geerdes, who is the developer of the mobile apps
	// that screen scrape a DCS website, wants the direct link.
	if prettyPrint {
		contents, err = json.MarshalIndent(smi, "", "  ")
	} else {
		contents, err = json.Marshal(smi)
	}
	if err != nil {
		msg := fmt.Sprintf("error marshalling services index to json: %v", err)
		doxlog.Error(msg)
		return err
	}
	err = ltfile.WriteFile(path.Join(pathOut, filename), string(contents))
	if err != nil {
		msg := fmt.Sprintf("error writing json services index to %s: %v", pathOut, err)
		doxlog.Error(msg)
		return err
	}
	return nil
}

func writeJsonFlatBooksIndex(pathOut, filename string, booksIndex []*IndexMeta, prettyPrint bool) error {
	var contents []byte
	var err error
	// MAC.  2024-01-22.
	// The index pages point to an index.html for pdfs, which
	// when reached loads the pdf.
	// Bruce Geerdes, who is the developer of the mobile apps
	// that screen scrapes a DCS website, wants the direct link.
	if prettyPrint {
		contents, err = json.MarshalIndent(booksIndex, "", "  ")
	} else {
		contents, err = json.Marshal(booksIndex)
	}
	if err != nil {
		msg := fmt.Sprintf("error marshalling books index to json: %v", err)
		doxlog.Error(msg)
		return err
	}
	err = ltfile.WriteFile(path.Join(pathOut, filename), string(contents))
	if err != nil {
		msg := fmt.Sprintf("error writing json books index to %s: %v", pathOut, err)
		doxlog.Error(msg)
		return err
	}
	return nil
}
func writeJsonHierarchicalBooksIndex(pathOut, filename string, booksIndex *html.TreeNode, prettyPrint bool) error {
	var contents []byte
	var err error
	// MAC.  2024-01-22.
	// The index pages point to an index.html for pdfs, which
	// when reached loads the pdf.
	// Bruce Geerdes, who is the developer of the mobile apps
	// that screen scrapes a DCS website, wants the direct link.
	if prettyPrint {
		contents, err = json.MarshalIndent(booksIndex, "", "  ")
	} else {
		contents, err = json.Marshal(booksIndex)
	}
	if err != nil {
		msg := fmt.Sprintf("error marshalling books index to json: %v", err)
		doxlog.Error(msg)
		return err
	}
	err = ltfile.WriteFile(path.Join(pathOut, filename), string(contents))
	if err != nil {
		msg := fmt.Sprintf("error writing json books index to %s: %v", pathOut, err)
		doxlog.Error(msg)
		return err
	}
	return nil
}

// createMasterServicesIndexFile writes servicesindex.html,
// that lists by month the days which have services available.
func createMasterServicesIndexFile(t *template.Template, pathOut string, smi html.ServicesMasterIndex) error {
	f, err := os.Create(pathOut)
	if err != nil {
		return fmt.Errorf("create file %s: %v", pathOut, err)
	}
	defer func(f *os.File) {
		err = f.Close()
		if err != nil {
			doxlog.Error(err.Error())
		}
	}(f)
	err = t.ExecuteTemplate(f, "servicesIndexTable", smi)
	if err != nil {
		return err
	}
	return nil
}

// createDayIndexFile writes an indexes/{year}{month}{day}.html file
// which lists the services available for a specific day.
// This is the page a user sees when they click on a specific day
// listed in the servicesindex.html file.
func createDayIndexFile(t *template.Template, pathOut string, serviceDay html.ServiceDayIndex) error {
	f, err := os.Create(pathOut)
	if err != nil {
		return fmt.Errorf("create file %s: %v", pathOut, err)
	}
	defer f.Close()
	err = t.ExecuteTemplate(f, "dayIndexTable", serviceDay)
	if err != nil {
		return err
	}
	return nil
}
func createSimpleMasterBooksIndexFile(t *template.Template, pathOut string, bmi *html.BooksMasterSimpleIndex) error {
	f, err := os.Create(pathOut)
	if err != nil {
		return fmt.Errorf("create file %s: %v", pathOut, err)
	}
	defer f.Close()
	err = t.ExecuteTemplate(f, "simpleBooksIndexTable", bmi)
	if err != nil {
		return err
	}
	return nil
}

func createMasterBooksIndexFile(t *template.Template, pathOut string, bmi html.BooksMasterAccordionIndex) error {
	f, err := os.Create(pathOut)
	if err != nil {
		return fmt.Errorf("create file %s: %v", pathOut, err)
	}
	defer f.Close()
	err = t.ExecuteTemplate(f, "booksIndexTable", bmi)
	if err != nil {
		return err
	}
	return nil
}

// indexAttribCollection exists in order to implement the sort interface.
type indexAttribCollection []*IndexAttributes

func (c indexAttribCollection) Len() int {
	return len(c)
}
func (c indexAttribCollection) Swap(i, j int) {
	c[i], c[j] = c[j], c[i]
}
func (c indexAttribCollection) Less(i, j int) bool {
	return c[i].SortKey < c[j].SortKey
}

// IndexAttributes holds the results of parsing the path to an index.html file for either HTML or PDF output.
// Some of these attributes are passed to the output templates for creating indexes, e.g. servicesIndexTable.gohtml
type IndexAttributes struct {
	BookAbr           string
	BookTitle         string
	BookType          books.Book
	BookTypeName      string
	Date              time.Time
	DOWAbr            string // used to look up the title for the day of week as defined in the user's build config
	DOWTitle          string // title as defined in user's build config
	HrefIndexHtml     string
	HrefPdf           string
	Language          string
	LanguageCode      string
	MediaAbr          string
	MediaTitle        string
	MediaType         mediaTypes.MediaType
	Mode              string
	MonthAbr          string // used to look up the title for the month as defined in the user's build config
	MonthTitle        string // title as defined in user's build config yaml file
	OfficeAbr         string // used to look up title as defined in user's build config
	OfficeTitle       string // title as defined in user's build config
	OfficeType        officeTypes.OfficeType
	OfficeTypeSortKey string
	SortKey           string // used to sort slices of this struct type.
	TemplateAbr       string
	TemplateType      templateTypes.TemplateType
	Title             string
	Year, Month, Day  string
}

// set the i.SortKey for downstream sorting
func (i *IndexAttributes) setSortKey() {
	sb := strings.Builder{}
	switch i.TemplateType {
	case templateTypes.Book:
		//accordion := false // place holder
		//if accordion {
		//	sb.WriteString(strconv.Itoa(int(i.BookType)))
		//	sb.WriteString(i.Title)
		//	sb.WriteString(i.MediaTitle)
		//	sb.WriteString(i.Language)
		//} else {
		//	sb.WriteString(strconv.Itoa(int(i.BookType)))
		//	sb.WriteString(i.Title)
		//	sb.WriteString(i.Language)
		//	sb.WriteString(i.MediaTitle)
		//}
		// TODO: fill in missing types
		sb.WriteString(strconv.Itoa(int(i.BookType)))
		sb.WriteString(i.BookTypeName)
		sb.WriteString(i.Title)
		switch i.BookType {
		case books.Apostolos:
		case books.Archieratokon:
		case books.Euchologion:
		case books.Evangelion:
		case books.Heirmologion:
		case books.Hieratikon:
		case books.Katavasias:
		case books.Lectionary:
		case books.LiturgicalGuide:
		case books.Menaia:
			sb.WriteString(i.Month)
			sb.WriteString(i.Day)
		case books.Octeochos:
			sb.WriteString(i.Mode)
			sb.WriteString(i.DOWAbr)
		case books.Other:
		case books.Pentecostarion:
			sb.WriteString("")
		case books.Prophetologion:
		case books.Psalter:
		case books.Synaxarion:
		case books.Triodion:
		default:
		}
		sb.WriteString(i.MediaTitle)
		sb.WriteString(fmt.Sprintf("%d ", strings.Count(i.Language, "-")))
		sb.WriteString(strings.ReplaceAll(i.Language, "-", ""))
		sb.WriteString(" ") // important: do not remove.  Otherwise, languages sort backwards
	case templateTypes.Service:
		sb.WriteString(i.Year)
		sb.WriteString(i.Month)
		sb.WriteString(i.Day)
		sb.WriteString(i.OfficeTypeSortKey)
		// service (aka office type) codes in the template name
		// must have at least two characters.  Variations of
		// them add a single digit number.
		// configuration site/build/page/titles/map
		// lists each service code as a key and for the value provides
		// the title that the user will see in the index.
		// For example:
		// ma  = Matins
		// ma2 = Matins - Customizable
		// ma4 = Matins - Alternative Commemoration
		// ma6 = Matins - Alternative Full (Psalms / Canon)
		// The users (e.g. Fr Seraphim) want the service
		// that is just two characters to come first,
		// followed by the ones with numbers, sorted
		// by service code:
		// Matins
		// Matins - Customizable
		// Matins - Alternative Commemoration
		// Matins - Alternative Full (Psalms / Canon)
		//
		// In order to do this, if the service (aka office)
		// code is two characters, we add a zero to make it three.
		// That way, in our sort, we have normalized the codes.
		// For the codes > 2 characters, we use "zzz".
		// That does two things:  first, it ensures the
		// code with the zero comes first, and it
		// neutralizes the sort of the others.
		if len(i.OfficeAbr) == 2 {
			sb.WriteString(fmt.Sprintf("%s0", i.OfficeAbr))
		} else {
			//			sb.WriteString("zzz") // this would sub-sort by title, not code
			sb.WriteString(i.OfficeAbr)
		}
		// Now we add the title.  The above plus this achieves the desired sort order.
		sb.WriteString(i.OfficeTitle)
		sb.WriteString(fmt.Sprintf("%d ", strings.Count(i.Language, "-")))
		sb.WriteString(strings.ReplaceAll(i.Language, "-", ""))
		sb.WriteString(" ") // important: do not remove.  Otherwise, languages sort backwards
		sb.WriteString(i.MediaTitle)
	default:
	}
	i.SortKey = sb.String()
}
func GetIndexJson(p string) (*IndexMeta, error) {
	var indexMeta IndexMeta
	pathToJson := fmt.Sprintf("%s.json", p[:len(p)-5])
	j, err := ltfile.GetFileContent(pathToJson)
	if err != nil {
		msg := fmt.Sprintf("error reading %s: %v", pathToJson, err)
		doxlog.Errorf(msg)
		return nil, fmt.Errorf(msg)
	}
	err = json.Unmarshal([]byte(j), &indexMeta)
	if err != nil {
		msg := fmt.Sprintf("error unmarshalling %s to IndexMeta struct: %v", pathToJson, err)
		doxlog.Errorf(msg)
		return nil, fmt.Errorf(msg)
	}
	return &indexMeta, nil
}

// ParsePath uses the information in the path to populate the index attributes to display on the index page
func ParsePath(path string, bc *models.BuildConfig) (*IndexAttributes, error) {
	var ok bool
	base := bc.SitePath
	titlesMapPath := "site/build/pages/titles/map"
	attribs := new(IndexAttributes)
	indexJson, err := GetIndexJson(path)
	if err == nil {
		attribs.Year = strconv.Itoa(indexJson.ServiceYear)
		attribs.Month = strconv.Itoa(indexJson.ServiceYear)
		attribs.Day = strconv.Itoa(indexJson.ServiceYear)
		if indexJson.MediaType == "pdf" {
			attribs.HrefPdf = indexJson.PdfHref
		}
	}
	if !strings.HasSuffix(base, "/") {
		base += "/"
	}
	rel := strings.TrimPrefix(path, base)
	if base+rel != path {
		return attribs, fmt.Errorf("could not split path %s using base %s", path, base)
	}
	attribs.HrefIndexHtml = strings.ReplaceAll(rel, "\\", "/")
	parts := strings.Split(rel, "/")
	attribs.MediaType = mediaTypes.TypeFor(parts[0])
	attribs.MediaAbr = parts[0]
	switch attribs.MediaType {
	case mediaTypes.HTML:
		attribs.MediaTitle, _ = bc.GetTitle("html")
	case mediaTypes.PDF:
		attribs.MediaTitle, _ = bc.GetTitle("pdf")
	default:
	}
	attribs.TemplateType = templateTypes.TypeForPathSegment(parts[1])
	attribs.TemplateAbr = parts[1]
	switch attribs.TemplateType {
	case templateTypes.Book:
		l := len(parts)
		if l < 8 {
			attribs.BookType = books.BookTypeForAbr(parts[2])
			attribs.BookAbr = parts[2]
			attribs.BookTitle, _ = bc.GetTitle(attribs.BookAbr)
			switch attribs.BookType {
			case books.Octeochos:
				{
					// see if this is a canonical octoechos path, e.g. oc.m1.d1, etc.
					if parts[3][0:1] == "m" && parts[4][0:1] == "d" {
						attribs.Mode = parts[3]
						var modeTitle string
						if modeTitle, ok = bc.GetTitle(parts[3]); !ok {
							modeTitle = parts[3]
						}
						attribs.DOWAbr = parts[4]
						var dowTitle string
						if dowTitle, ok = bc.GetTitle(parts[4]); !ok {
							dowTitle = parts[4]
						}
						attribs.Title = fmt.Sprintf("%s %s", modeTitle, dowTitle)
					} else {
						var title string
						if title, ok = bc.GetTitle(parts[3]); !ok {
							title = parts[3]
						}
						attribs.Title = title

					}
				}
			default:
				{
					attribs.Title, _ = bc.GetTitle(parts[3])
					if attribs.Title == "" {
						attribs.Title = parts[3]
						fmt.Printf("%s not found in %s in your configuration settings.\n", parts[3], titlesMapPath)
					}
				}
			}
			attribs.Language = strings.ToUpper(parts[l-2])
			attribs.LanguageCode = strings.ToUpper(parts[l-2])
		} else {
			return attribs, fmt.Errorf("ParsePath can't parse path %s as a book", path)
		}
		///Users/mac002/doxa/sites/liml/site/h/b/eu/smallwaterblessing/en/index.html
	case templateTypes.Service:
		if len(parts) != 8 {
			return attribs, fmt.Errorf("can't ParsePath path %s as a service", path)
		}
		attribs.Year = parts[2]
		attribs.Month = parts[3]
		attribs.Day = parts[4]
		attribs.OfficeType = officeTypes.OfficeTypeForAbr(parts[5])
		attribs.OfficeTypeSortKey = fmt.Sprintf("%03s", int(attribs.OfficeType))
		attribs.OfficeAbr = parts[5]
		attribs.Title, _ = bc.GetTitle(attribs.OfficeAbr)
		attribs.OfficeTitle, _ = bc.GetTitle(attribs.OfficeAbr) // TODO: redundant with attribs.Title
		if attribs.OfficeTitle == "" {
			attribs.OfficeTitle = parts[5]
			fmt.Printf("%s not found in %s in your configuration settings.\n", parts[5], titlesMapPath)
		}

		attribs.Language = strings.ToUpper(parts[6])
		attribs.LanguageCode = strings.ToUpper(parts[6])
		year, _ := strconv.Atoi(attribs.Year)
		var month, day int
		var err error
		if len(attribs.Month) == 2 {
			if strings.HasPrefix(attribs.Month, "0") {
				month, err = strconv.Atoi(attribs.Month[1:])
			} else {
				month, err = strconv.Atoi(attribs.Month)
			}
			if err != nil {
				return attribs, fmt.Errorf("indexer error: %s has invalid month: %e", path, err)
			}
		} else {
			return attribs, fmt.Errorf("indexer error: %s has invalid month: %s", path, attribs.Month)
		}
		if len(attribs.Day) == 2 {
			day, err = strconv.Atoi(attribs.Day)
			if err != nil {
				return attribs, fmt.Errorf("indexer error: %s has invalid day: %e", path, err)
			}
		} else {
			return attribs, fmt.Errorf("indexer error: %s has invalid day: %s", path, attribs.Day)
		}
		attribs.Date = time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC)
		attribs.MonthAbr = strings.ToLower(attribs.Date.Format("Jan"))
		if attribs.Month == "05" && attribs.MonthAbr != "may" {
			fmt.Sprintf("TODO Delete me")
		}
		attribs.DOWAbr = strings.ToLower(attribs.Date.Format("Mon"))
		attribs.DOWTitle, _ = bc.GetTitle(attribs.DOWAbr)
		attribs.MonthTitle, _ = bc.GetTitle(attribs.MonthAbr)
	default:
		return attribs, fmt.Errorf("unknown templateType for path %s", path)
	}
	// convert language iso codes to text set in map by the user
	parts = strings.Split(attribs.Language, "-")
	sb := strings.Builder{}
	for _, part := range parts {
		part = strings.ToLower(part)
		if sb.Len() > 0 {
			sb.WriteString("-")
		}
		if val, ok := bc.GetTitle(part); ok {
			sb.WriteString(val)
		} else {
			sb.WriteString(part)
		}
	}
	attribs.Language = sb.String()
	attribs.setSortKey()
	return attribs, nil
}
func LoadIndexMetaDataFromDirs(fromDirs []string) ([]*IndexMeta, error) {
	var ds []*IndexMeta
	var err error
	var indexMetaInstances []*IndexMeta
	for _, fromDir := range fromDirs {
		indexMetaInstances, err = LoadIndexMetaData(fromDir)
		if err != nil {
			return nil, err
		}
		ds = append(ds, indexMetaInstances...)
	}
	return ds, nil
}
func LoadIndexMetaData(fromDir string) ([]*IndexMeta, error) {
	var ds []*IndexMeta
	jsonFilePaths, err := ltfile.FileMatcher(fromDir, "json", []string{"index"})
	if err != nil {
		return nil, fmt.Errorf("error finding index.json files in %s: %v", fromDir, err)
	}
	if len(jsonFilePaths) == 0 {
		return nil, fmt.Errorf("could not find index.json files in %s", fromDir)
	}
	var jsonStr string
	for _, jsonFilePath := range jsonFilePaths {
		jsonStr, err = ltfile.GetFileContent(jsonFilePath)
		if err != nil {
			return nil, fmt.Errorf("error reading %s: %v", jsonStr, err)
		}
		var indexMeta *IndexMeta
		indexMeta, err = NewIndexMetaFromJson(jsonStr)
		if err != nil {
			return nil, fmt.Errorf("error converting json string to IndexMeta object from %s: %v", jsonStr, err)
		}
		if indexMeta != nil {
			ds = append(ds, indexMeta)
		}
	}
	return ds, nil
}
func SetSortOrder(indexMetaObjects []*IndexMeta, bc *models.BuildConfig) []*IndexMeta {
	for _, o := range indexMetaObjects {
		o.SetSortOrder(bc)
		o.SortOrder = strings.Join(o.Titles, "")
	}
	sort.Sort(BySortOrder(indexMetaObjects))
	return indexMetaObjects
}

// IndexMeta records information for a generated HTML or PDf
// and is written to an index.json file in the same directory
// as the index.html file.  It is used by the Indexer to build
// or rebuild the website indexes.
type IndexMeta struct {
	BookTitle         string
	GeneratedDate     string
	Href              string
	MetaHref          string // href to the meta-template, which can be used to dynamically render an html page
	Pdf               bool   // available for convenience. Can also be determined by len(PdfHref) > 0
	PdfHref           string
	LanguageCodes     string
	LanguageNames     string
	LastTitleOverride string `json:"-"`
	MediaTitle        string
	MediaType         string // HTML or PDF
	MusicMedia        []*media.Link
	SectionTitle      string
	ServiceDay        int // if it is a service template
	ServiceMonth      int // if it is a service template
	ServiceType       string
	ServiceYear       int // if it is a service template
	SortOrder         string
	TemplateId        string   // from template
	TitleCodes        []string // from template, comma delimited
	Titles            []string // comma delimited
	Type              string   // Book or Service.
}

func NewIndexMetaFromJson(j string) (*IndexMeta, error) {
	i := new(IndexMeta)
	err := json.Unmarshal([]byte(j), i)
	if err != nil {
		return nil, err
	}
	return i, nil
}
func (im *IndexMeta) SetHref(pathToIndex string, buildForPreview bool) error {
	var typeDelimiter string
	var theType string
	delimiter := "/"

	if buildForPreview {
		theType = "t"
		doxlog.Info("indexer setting href for preview")
	} else {
		if im.MediaType == strings.ToLower(mediaTypes.HTML.String()) {
			theType = "h"
			im.TitleCodes = append(im.TitleCodes, "html")
		} else if im.MediaType == strings.ToLower(mediaTypes.PDF.String()) {
			theType = "p"
			im.Pdf = true
			im.TitleCodes = append(im.TitleCodes, "pdf")
		} else {
			return fmt.Errorf("unknown media type %s", im.MediaType)
		}
		im.TitleCodes = append(im.TitleCodes, im.LanguageCodes)
	}
	typeDelimiter = fmt.Sprintf("%s%s%s", delimiter, theType, delimiter)
	splitIndex := strings.Index(pathToIndex, typeDelimiter)
	if splitIndex > 0 {
		splitIndex = splitIndex + 3
	} else {
		return fmt.Errorf("could not find index of %s in %s", typeDelimiter, pathToIndex)
	}
	if splitIndex >= len(pathToIndex) {
		return fmt.Errorf("could not split %s using %s", pathToIndex, typeDelimiter)
	} else {
		im.Href = path.Join(theType, pathToIndex[splitIndex:])
		if im.Pdf {
			idParts := strings.Split(im.TemplateId, "/")
			if len(idParts) > 1 {
				pdfFile := fmt.Sprintf("%s.%s.pdf", idParts[len(idParts)-1], im.LanguageCodes)
				im.PdfHref = path.Join(im.Href[:len(im.Href)-10], pdfFile)
			} else {
				return fmt.Errorf("could not split %s using /", pathToIndex)
			}
		}
	}
	return nil
}
func (im *IndexMeta) SetSortOrder(bc *models.BuildConfig) {
	var err error
	var title string
	var ok bool
	missingMsg := " not in titles in config"
	sortOrderSb := strings.Builder{}
	titlesSb := strings.Builder{}

	if im.Type == templateTypes.Service.String() {
		sortOrderSb.WriteString(fmt.Sprintf("%s%s%s", im.ServiceYear, im.ServiceMonth, im.ServiceDay))
	}
	for i, t := range im.TitleCodes {
		t = strings.TrimSpace(t)
		// next two lines set titles independent of sort order
		title, _ = bc.GetTitle(t)
		im.Titles = append(im.Titles, strings.TrimSpace(title))

		if strings.HasPrefix(t, "sort") {
			sortOrderSb.WriteString(t)
			continue
		}
		if strings.HasPrefix(t, "oc") {
			last := t[len(t)-2:]
			_, err = strconv.Atoi(last)
			if err == nil {
				sortOrderSb.WriteString(fmt.Sprintf("sort%s", t[2:]))
				continue
			}
		}
		if strings.HasPrefix(t, "m") || strings.HasPrefix(t, "d") || strings.HasPrefix(t, "oc") {
			// check to see if the last character is a number
			last := t[len(t)-1:]
			if len(last) > 0 {
				_, err = strconv.Atoi(last)
				if err == nil {
					first := t[:1]
					for i := 0; i < 1000; i++ {
						if t == fmt.Sprintf("%s%01d", first, i) ||
							t == fmt.Sprintf("%s%02d", first, i) ||
							t == fmt.Sprintf("%s%03d", first, i) {
							sortOrderSb.WriteString(fmt.Sprintf("sort%03d", i))
							if titlesSb.Len() > 0 {
								titlesSb.WriteString(",")
							}
							if title, ok = bc.GetTitle(t); !ok {
								titlesSb.WriteString(fmt.Sprintf("%s %s", t, missingMsg))
							} else {
								titlesSb.WriteString(title)
							}
						}
					}
					continue
				}
			}
		}
		if titlesSb.Len() > 0 {
			titlesSb.WriteString(",")
		}
		if title, ok = bc.GetTitle(t); ok {
			// no action yet
		} else {
			title = fmt.Sprintf("%s %s", t, missingMsg)
		}
		if im.Type == templateTypes.Book.String() && i == 0 {
			im.BookTitle = title
		} else {
			titlesSb.WriteString(title)
		}
		sortOrderSb.WriteString(t)
		continue
	}
	im.SectionTitle = strings.ReplaceAll(titlesSb.String(), ",", " ")
	// media title
	if im.MediaTitle, ok = bc.GetTitle(im.MediaType); !ok {
		im.MediaTitle = fmt.Sprintf("%s%s", im.MediaType, missingMsg)
	}
	sortOrderSb.WriteString(im.MediaTitle)
	titlesSb.WriteString(",")
	titlesSb.WriteString(im.MediaTitle)
	// convert language iso codes to text set in map by the user
	parts := strings.Split(im.LanguageCodes, "-")
	sbLang := strings.Builder{}
	for _, part := range parts {
		part = strings.ToLower(part)
		if sbLang.Len() > 0 {
			sbLang.WriteString("-")
		}
		if val, ok := bc.GetTitle(part); ok {
			sbLang.WriteString(val)
		} else {
			sbLang.WriteString(part)
		}
	}
	titlesSb.WriteString(",")
	im.LanguageNames = sbLang.String()
	titlesSb.WriteString(sbLang.String())
	sortOrderSb.WriteString(sbLang.String())
	im.SortOrder = sortOrderSb.String()
}
func (im *IndexMeta) ToJson() ([]byte, error) {
	return json.Marshal(im)
}
func (im *IndexMeta) ToAnchorCard() (a *html.Anchor) {
	a = new(html.Anchor)
	a.Title = im.LanguageNames
	a.Href = im.Href
	a.Pdf = im.Pdf
	a.PdfHref = im.PdfHref
	if a.Pdf {
		a.Target = "_blank"
	} else {
		a.Target = "_self"
	}
	return a
}

type BySortOrder []*IndexMeta

func (b BySortOrder) Len() int {
	return len(b)
}
func (b BySortOrder) Less(i, j int) bool {
	return b[i].SortOrder < b[j].SortOrder
}
func (b BySortOrder) Swap(i, j int) {
	b[i], b[j] = b[j], b[i]
}
