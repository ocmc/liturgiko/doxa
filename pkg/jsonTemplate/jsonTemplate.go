package jsonTemplate

import (
	"github.com/liturgiko/doxa/pkg/enums/templateTypes"
	"github.com/liturgiko/doxa/pkg/ldp"
	"time"
)

type JasonTemplate struct {
	Type  templateTypes.TemplateType
	Date  time.Time // when created
	LDP   ldp.LDP   // if it is a service
	TKMap map[string]string
}
