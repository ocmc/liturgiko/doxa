package models

// Struct for CSS record
type CSS struct {
	ID           string `json:"id"`
	Value        string `json:"value"`
	CreatedWhen  string `json:"createdWhen"`
	ModifiedWhen string `json:"modifiedWhen"`
}

// An array of CSS text records
type CssArray []CSS
