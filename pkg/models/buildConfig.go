package models

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/atempl"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/css"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/enums/calendarTypes"
	"github.com/liturgiko/doxa/pkg/enums/pdfLibs"
	"github.com/liturgiko/doxa/pkg/enums/properties"
	"github.com/liturgiko/doxa/pkg/enums/propertyValueTypes"
	"github.com/liturgiko/doxa/pkg/enums/templateSourceTypes"
	"github.com/liturgiko/doxa/pkg/fonts"
	"github.com/liturgiko/doxa/pkg/generators/html"
	"github.com/liturgiko/doxa/pkg/layouts"
	"html/template"
	"os"
	"path"
	"sort"
	"strconv"
	"strings"
	"time"
)

// Font is the struct for TrueType font names
type Font struct {
	Bold       string
	BoldItalic string
	Italic     string
	Regular    string
}

// BuildConfig is the struct for a Build Configuration. This is converted to json
// and stored in the database using the BuildConfigRecord struct below.
type BuildConfig struct {
	AssetsPath                       string
	AssetsRootPath                   string
	BuildLanguages                   []*atempl.TableLayout
	BuildPublic                      bool
	Calendar                         calendarTypes.CalendarType
	CardSrc                          string
	ConfigName                       string
	CopyAssets                       bool
	DayIndexSrc                      string
	DeleteHtmlDir                    bool
	DeletePdfDir                     bool
	DcsPage                          *html.DCS
	DevicePage                       *html.Device
	DocNavbar                        html.Navbar // navbar for books and services html pages
	DoxaVersion                      string      // the version of Doxa used to generate the dcs
	ExportsPath                      string
	FilePatterns                     []string
	FilePatternSelections            []string // these are integers stored as strings.  If its length is zero or length is one and value == *, it means process all
	FlagMissingRidTk                 bool     // set from db value for properties.SiteBuildTemplatesCheckingRidKey
	FlagMissingRidValue              bool     // set from db value for properties.SiteBuildTemplatesCheckingRidValue
	Footer                           html.Footer
	FramesEnabled                    bool
	HelpPage                         *html.Help
	HelpSrc                          string
	HiddenSysPath                    string
	HomePage                         *html.Home
	HomeSrc                          string
	HtmlCss                          *css.StyleSheet
	IncludeCovers                    bool
	IndexBooks                       bool
	IndexBooksDescription            []string
	IndexBooksSearchPrompt           string
	IndexBooksTabTitle               string
	IndexBooksTitle                  string
	IndexCustomDescription           []string
	IndexCustomTabTitle              string
	IndexCustomTitle                 string
	IndexDayDescription              []string
	IndexDaySubTitleDateFormat       string
	IndexDaySubTitleDatePrefix       string
	IndexDaySubTitleDateSuffix       string
	IndexDayTabTitle                 string
	IndexDayTitle                    string
	IndexForNav                      bool
	IndexForSearch                   bool
	IndexNavbar                      html.Navbar // navbar for index html pages
	IndexServices                    bool
	IndexYearMonthDescription        []string
	IndexYearMonthSubTitle           string
	IndexYearMonthTabTitle           string
	IndexYearMonthTitle              string
	Interlinear                      bool
	LayoutsPath                      string
	LayoutsSelected                  map[string]bool
	Links                            []html.Link
	NavbarHasBooksIcon               bool
	NavbarHasSearchIcon              bool
	NavbarHasServicesIcon            bool
	OutputHtml                       bool
	OutputPdf                        bool
	PdfCss                           *css.StyleSheet
	PdfFontMap                       map[string]*fonts.FontSet
	PdfLib                           pdfLibs.PDFLib
	PreviewPath                      string
	PreviewLayoutAcronymn            string
	Realm                            string
	ScoreFrame                       *html.Blank
	Scripts                          []html.Script
	SearchPage                       *html.Search
	SearchSrc                        string
	SiteName                         string
	SitePath                         string
	SiteURL                          string
	Source                           templateSourceTypes.TemplateSourceType
	SubRealm                         string
	Templates                        *template.Template
	TemplatesPrimaryPath             string
	TemplatesFallbackEnabled         bool
	TemplatesFallbackPath            string
	TemplatesFallbackGitLabGroupName string
	titles                           map[string]string
	TopicKeyComparePage              *html.TopicKeyCompare
	TopicKeyIndexPage                *html.LtxTopicKeysIndex
	SM                               *config.SettingsManager
	Versions                         map[string]string // set from outside via resolver.Versions()
	YearOverride                     int
}

// NewBuildConfig returns a BuildConfig for the specified site and config name.
// You must call the LoadRemote() method to populate it with values from the database.
// The value for siteName and configName come from the config.yaml file.
func NewBuildConfig(siteName string, sm *config.SettingsManager) *BuildConfig {
	bc := new(BuildConfig)
	bc.SiteName = siteName
	bc.PdfFontMap = make(map[string]*fonts.FontSet)
	bc.LayoutsSelected = make(map[string]bool)
	bc.SM = sm
	return bc
}

// GetTitle returns the value found for the key in the db path site/build/pages/titles/map
func (c *BuildConfig) GetTitle(key string) (title string, ok bool) {
	key = strings.ToLower(key)
	if title, ok = c.titles[key]; !ok {
		if strings.Contains(key, "-") {
			return c.GetDashTitles(key)
		}
		if key == "" {
			return "", false
		}
		title = fmt.Sprintf("%s not found in database configs site/build/pages/titles/map", key)
		doxlog.Warnf(title)
	}
	return title, ok
}

// GetDashTitles looks up dash delimited title codes and returns the title separated by a dash.
// For example, gr-en could return "Greek-English" depending on the results of the lookups.
func (c *BuildConfig) GetDashTitles(key string) (title string, ok bool) {
	parts := strings.Split(key, "-")
	var titles []string
	var t string
	for _, p := range parts {
		if t, ok = c.titles[p]; !ok {
			titles = append(titles, fmt.Sprintf("%s not found in database configs site/build/pages/titles/map", p))
			doxlog.Warnf(title)
		} else {
			titles = append(titles, t)
		}
	}
	title = strings.Join(titles, "-")
	return title, ok
}

// SetSelectedLayouts creates a map of selected layouts.  At generation time, a layout acronym must be in the map in order to be processed
func (c *BuildConfig) SetSelectedLayouts(layouts string) {
	c.LayoutsSelected = make(map[string]bool)
	if len(layouts) == 0 || layouts == "*" {
		return // an empty map means all layouts will be processed
	}
	acronyms := strings.Split(layouts, ",")
	for _, a := range acronyms {
		if a == "null" {
			continue
		}
		c.LayoutsSelected[strings.TrimSpace(a)] = true
	}
}

// Load the config from the database.
// If a property does not exist, it will be set to a specified default
// and a description will also be added.
func (c *BuildConfig) Load() error {
	var err error
	// delete html directory from site before generating?
	c.DeleteHtmlDir = c.SM.BoolProps[properties.SiteBuildPregenDeleteHtmlDir]
	// delete pdf directory from site before generating?
	c.DeletePdfDir = c.SM.BoolProps[properties.SiteBuildPregenDeletePdfDir]
	c.FlagMissingRidTk = c.SM.BoolProps[properties.SiteBuildTemplatesCheckingRidKey]
	c.FlagMissingRidValue = c.SM.BoolProps[properties.SiteBuildTemplatesCheckingRidValue]
	c.IncludeCovers = c.SM.BoolProps[properties.SiteBuildPdfCoversOn]
	//  After generating, create the navigation index for the site?
	c.IndexForNav = c.SM.BoolProps[properties.SiteBuildPostgenIndexForNav]
	// After generating, create the search index?
	c.IndexForSearch = c.SM.BoolProps[properties.SiteBuildPostgenIndexForSearch]

	// use interlinear?
	c.Interlinear = c.SM.BoolProps[properties.SiteBuildInterlinear]
	// get the realm
	c.Realm = c.SM.StringProps[properties.SiteBuildRealm]
	// get the url to which to publish the website
	c.SiteURL = c.SM.StringProps[properties.SiteBuildPublishPublicUrl]
	// set the output paths for the build
	config.DoxaPaths.SetWebSitePaths(c.SM.StringProps[properties.SiteBuildRoot])
	// Year Override
	c.YearOverride = c.SM.IntProps[properties.SiteBuildYear]
	if c.YearOverride > 0 && c.YearOverride < 1583 {
		fmt.Printf("invalid year %d.  Cannot be less than 1583.", c.YearOverride)
		c.YearOverride = 0
	}
	err = c.LoadBuildLanguages()
	if err != nil {
		doxlog.Error(err.Error())
		return err
	}

	c.SetSelectedLayouts(c.SM.StringProps[properties.SiteBuildLayoutsSelected])

	// frames enabled?
	c.FramesEnabled = c.SM.BoolProps[properties.SiteBuildFramesUseFrames]
	if c.FramesEnabled {
		c.DocNavbar.NavbarHome = "/home.html"
		c.IndexNavbar.NavbarHome = "/home.html"
	} else {
		c.DocNavbar.NavbarHome = "/"
		c.IndexNavbar.NavbarHome = "/"
	}

	// title for tab of day services index
	c.IndexDayTabTitle = c.SM.StringProps[properties.SiteBuildPagesIndexServicesDayTabTitle]
	// title for tab of month/year services index
	c.IndexYearMonthTabTitle = c.SM.StringProps[properties.SiteBuildPagesIndexServicesMonthTabTitle]
	// title for month/year services index
	c.IndexYearMonthTitle = c.SM.StringProps[properties.SiteBuildPagesIndexServicesMonthTitle]
	// subtitle for month/year services index
	c.IndexYearMonthSubTitle = c.SM.StringProps[properties.SiteBuildPagesIndexServicesMonthSubtitle]
	// description of month/year services index
	c.IndexYearMonthDescription = c.SM.StringSliceProps[properties.SiteBuildPagesIndexServicesMonthDescription]
	// title for day services index
	c.IndexDayTitle = c.SM.StringProps[properties.SiteBuildPagesIndexServicesDayTitle]
	// prefix for subtitle for day services index
	c.IndexDaySubTitleDatePrefix = c.SM.StringProps[properties.SiteBuildPagesIndexServicesDaySubtitlePrefix]
	// suffix for subtitle for day services index
	c.IndexDaySubTitleDateSuffix = c.SM.StringProps[properties.SiteBuildPagesIndexServicesDaySubtitleSuffix]
	// date format for subtitle for day services index
	c.IndexDaySubTitleDateFormat = c.SM.StringProps[properties.SiteBuildPagesIndexServicesDaySubtitleDateFormat]
	c.IndexDayDescription = c.SM.StringSliceProps[properties.SiteBuildPagesIndexServicesDayDescription]
	// 	index books title
	c.IndexBooksSearchPrompt = c.SM.StringProps[properties.SiteBuildPagesIndexBooksSearchPrompt]
	// 	index books tab title
	c.IndexBooksTabTitle = c.SM.StringProps[properties.SiteBuildPagesIndexBooksTabTitle]
	// 	index books title
	c.IndexBooksTitle = c.SM.StringProps[properties.SiteBuildPagesIndexBooksTitle]
	//	index books description
	c.IndexBooksDescription = c.SM.StringSliceProps[properties.SiteBuildPagesIndexBooksDescription]
	// 	index custom tab title
	c.IndexCustomTabTitle = c.SM.StringProps[properties.SiteBuildPagesIndexCustomTabTitle]
	// 	index custom title
	c.IndexCustomTitle = c.SM.StringProps[properties.SiteBuildPagesIndexCustomTitle]
	//	index custom description
	c.IndexCustomDescription = c.SM.StringSliceProps[properties.SiteBuildPagesIndexCustomDescription]
	// get the navbar icons
	c.NavbarHasBooksIcon = c.SM.BoolProps[properties.SiteBuildNavbarHasBooksIcon]
	c.NavbarHasSearchIcon = c.SM.BoolProps[properties.SiteBuildNavbarHasSearchIcon]
	c.NavbarHasServicesIcon = c.SM.BoolProps[properties.SiteBuildNavbarHasServicesIcon]
	// get the primary templates path
	c.TemplatesPrimaryPath = config.DoxaPaths.PrimaryTemplatesPath // c.SM.StringProps[properties.SiteBuildTemplatesFallbackPrimaryDir]
	// get the fallback templates path
	c.TemplatesFallbackGitLabGroupName = c.SM.StringProps[properties.SiteBuildTemplatesFallbackDir]
	if len(c.TemplatesFallbackGitLabGroupName) > 0 {
		c.TemplatesFallbackPath = path.Join(config.DoxaPaths.SubscriptionsPath /*c.TemplatesFallbackGitLabGroupName,*/, "templates")
	}
	// see if fallback is enabled
	c.TemplatesFallbackEnabled = c.SM.BoolProps[properties.SiteBuildTemplatesFallbackEnabled]

	// if fallback is enabled, make sure the primary and fallback directories have been set.
	// if not, set enabled to false.
	if c.TemplatesFallbackEnabled {
		if len(c.TemplatesFallbackPath) == 0 ||
			len(c.TemplatesPrimaryPath) == 0 {
			c.TemplatesFallbackEnabled = false
			err = c.SM.SetValueByType(properties.SiteBuildTemplatesFallbackEnabled, "false")
			if err != nil {
				doxlog.Error(err.Error())
				return err
			}
		} else {
			// make sure the full paths are set
			if !strings.HasPrefix(c.TemplatesPrimaryPath, config.DoxaPaths.HomePath) {
				c.TemplatesPrimaryPath = path.Join(config.DoxaPaths.HomePath, c.TemplatesPrimaryPath)
			}
			config.DoxaPaths.PrimaryTemplatesPath = c.TemplatesPrimaryPath
			if !strings.HasPrefix(c.TemplatesFallbackPath, config.DoxaPaths.HomePath) {
				c.TemplatesFallbackPath = path.Join(config.DoxaPaths.HomePath, c.TemplatesFallbackPath)
			}
			config.DoxaPaths.FallbackTemplatesPath = c.TemplatesFallbackPath
		}
	}

	// get the other HTML titles
	c.titles = c.SM.StringMapStringProps[properties.SiteBuildPagesTitles]
	// Get the source for templates (file vs database)
	//if c.SM.BoolProps[properties.SiteBuildUseExternalTemplates] {
	c.Source = templateSourceTypes.FILE
	// set navbar icons booleans
	if c.NavbarHasBooksIcon, err = c.SM.GetElseSetBoolProp(properties.SiteBuildNavbarHasBooksIcon); err != nil {
		fmt.Sprintf("TODO delete me after handling")
	}
	if c.NavbarHasSearchIcon, err = c.SM.GetElseSetBoolProp(properties.SiteBuildNavbarHasSearchIcon); err != nil {
		fmt.Sprintf("TODO delete me after handling")
	}
	if c.NavbarHasServicesIcon, err = c.SM.GetElseSetBoolProp(properties.SiteBuildNavbarHasServicesIcon); err != nil {
		fmt.Sprintf("TODO delete me after handling")
	}

	// Set the patterns to use to match templates and the indexes of which ones to use
	if c.FilePatterns, err = c.SM.GetElseSetStringSliceProp(properties.SiteBuildTemplatesPatterns); err != nil {
		fmt.Sprintf("TODO delete me after handling")
	}
	var hasChanges bool
	l := len(c.FilePatterns)
	for i := 0; i < l; i++ {
		if !strings.HasSuffix(c.FilePatterns[i], ".lml") {
			c.FilePatterns[i] = c.FilePatterns[i] + ".lml"
			hasChanges = true
		}
	}
	if hasChanges {
		// save patterns to the database
		if err = c.SM.SetStringSliceByType(properties.SiteBuildTemplatesPatterns, c.FilePatterns); err != nil {
			doxlog.Error(err.Error())
			return err
		}
	}
	var patternsProp string
	if patternsProp, err = c.SM.GetElseSetStringProp(properties.SiteBuildTemplatesPatternsSelected); err != nil {
		fmt.Sprintf("TODO delete me after handling")
	}
	parts := strings.Split(patternsProp, ",")
	for _, p := range parts {
		var pInt int
		pInt, err = strconv.Atoi(p)
		if err != nil {
			return err
		}
		if pInt > len(c.FilePatterns) {
			continue // this is an error.
		}
		c.FilePatternSelections = append(c.FilePatternSelections, p)
	}
	if c.FilePatternSelections == nil || len(c.FilePatternSelections) == 0 {
		msg := "build config load error: no template ID pattern selected for generation"
		doxlog.Errorf("%s\n", msg)
		fmt.Println(msg)
	}

	// Set the output types
	c.OutputHtml = c.SM.BoolProps[properties.SiteBuildOutputHtml]
	c.OutputPdf = c.SM.BoolProps[properties.SiteBuildOutputPdf]

	// Get library for PDF files for frame
	var pdfLib string
	pdfLib = c.SM.StringProps[properties.SiteBuildPdfLib]
	if pdfLib == "go" {
		c.PdfLib = pdfLibs.GO
	} else if pdfLib == "latex" {
		c.PdfLib = pdfLibs.LATEX
	} else {
		doxlog.Panicf("cannot do build...missing or unknown PDF lib type: %s", pdfLib)
	}
	// Copy assets?
	c.CopyAssets = c.SM.BoolProps[properties.SiteBuildAssetsCopyAssets]
	// Index books?
	c.IndexBooks = c.SM.BoolProps[properties.SiteBuildIndexBooks]
	// Index services?
	c.IndexServices = c.SM.BoolProps[properties.SiteBuildIndexServices]

	// get css assets
	var theCss []string
	theCss = c.SM.StringSliceProps[properties.SiteBuildAssetsCss]
	var links []html.Link
	for _, aCss := range theCss {
		link := new(html.Link)
		link.Href = aCss
		links = append(links, *link)
	}
	c.Links = links

	// get js assets
	var theJs []string
	theJs = c.SM.StringSliceProps[properties.SiteBuildAssetsJs]
	var scripts []html.Script
	for _, js := range theJs {
		script := new(html.Script)
		script.Source = js
		script.MediaType = "application/javascript"
		scripts = append(scripts, *script)
	}
	c.Scripts = scripts

	//	use simple navbar?
	c.IndexNavbar.Simple = true
	// TODO: simple vs AGES supported by properties
	//if c.IndexNavbar.Simple = c.sm.BoolProps[
	//	properties.SiteBuildNavbarSimple); err != nil {
	//	return err
	//}
	// get logo for simple navbar
	c.IndexNavbar.NavbarLogoSrc = c.SM.StringProps[properties.SiteBuildNavbarLogoImage]

	c.IndexNavbar.NavbarLogoHeight = c.SM.StringProps[properties.SiteBuildNavbarLogoHeight]
	c.IndexNavbar.NavbarLogoWidth = c.SM.StringProps[properties.SiteBuildNavbarLogoWidth]
	c.IndexNavbar.NavbarTitle = c.SM.StringProps[properties.SiteBuildNavbarTitle]
	c.IndexNavbar.UseLogoForMenu = c.SM.BoolProps[properties.SiteBuildNavbarUseLogoForMenu]
	c.IndexNavbar.UseTextInSideBar = c.SM.BoolProps[properties.SiteBuildNavbarUseTextInSideBarMenu]
	css := "sideBarMenuItem"
	c.IndexNavbar.SideBarTextMenuItems = make(html.TextMenuItems, 0, 4)
	// home
	c.IndexNavbar.SideBarTextMenuItems = append(c.IndexNavbar.SideBarTextMenuItems,
		html.NewTextMenuItem(css,
			"house",
			c.SM.StringProps[properties.SiteBuildNavbarSideBarMenuHome],
			"Home",
			c.IndexNavbar.NavbarHome,
			1))
	// books
	c.IndexNavbar.SideBarTextMenuItems = append(c.IndexNavbar.SideBarTextMenuItems,
		html.NewTextMenuItem(css,
			"book",
			c.SM.StringProps[properties.SiteBuildNavbarSideBarMenuBooks],
			"Service Books",
			"booksindex.html",
			2))
	// services
	c.IndexNavbar.SideBarTextMenuItems = append(c.IndexNavbar.SideBarTextMenuItems,
		html.NewTextMenuItem(css,
			"calendar-date",
			c.SM.StringProps[properties.SiteBuildNavbarSideBarMenuServices],
			"Daily Services",
			"servicesindex.html",
			3))
	// Help
	c.IndexNavbar.SideBarTextMenuItems = append(c.IndexNavbar.SideBarTextMenuItems,
		html.NewTextMenuItem(css,
			"question-circle",
			c.SM.StringProps[properties.SiteBuildNavbarSideBarMenuHelp],
			"Help",
			"help.html",
			4))

	// Search
	//c.IndexNavbar.SideBarTextMenuItems = append(c.IndexNavbar.SideBarTextMenuItems,
	//	html.NewTextMenuItem(css,
	//		"search",
	//		c.SM.StringProps[properties.SiteBuildNavbarSideBarMenuSearch],
	//		"",
	//		"search.html",
	//		5))
	sort.Sort(c.IndexNavbar.SideBarTextMenuItems)
	if c.FramesEnabled {
		text := c.SM.StringProps[properties.SiteBuildNavbarSideBarMenuToggleFrames]
		c.IndexNavbar.ToggleItems = append(c.IndexNavbar.ToggleItems,
			&html.ToggleItem{
				Action:   "frames",
				Disabled: true,
				Text:     text,
				AltText:  fmt.Sprintf("Hide or show %s", text),
			})
	}

	if c.IndexNavbar.Simple {
		c.IndexNavbar.NavbarSrc = "navbar"
		c.DocNavbar = c.IndexNavbar // same for simple
	} else { // TODO: handle AGES style. Not implemented at this time.
		c.IndexNavbar.NavbarSrc = "navbar"
		c.DocNavbar = c.IndexNavbar // same for simple
	}
	// get footer properties to use in footer template
	// TODO: the footer properties do not appear to be used anywhere
	c.Footer.Enabled = c.SM.BoolProps[properties.SiteBuildFooterEnabled]
	c.Footer.FacebookUrl = c.SM.StringProps[properties.SiteBuildFooterFacebookUrl]

	c.Footer.OwnerWebsiteUrl = c.SM.StringProps[properties.SiteBuildFooterOwnerUrl]

	c.Footer.InsertFacebook = len(c.Footer.FacebookUrl) > 0
	c.Footer.InsertOwnerWebsite = len(c.Footer.OwnerWebsiteUrl) > 0

	// get the properties for the home page (index.html)
	c.HomePage = new(html.Home)
	c.HomePage.Templates = c.Templates
	c.HomePage.Name = "home"
	c.HomePage.Source = c.HomeSrc
	if c.FramesEnabled {
		c.HomePage.PathOut = path.Join(c.SitePath, "home.html")
	} else {
		c.HomePage.PathOut = path.Join(c.SitePath, "index.html")
	}
	// next
	c.HomePage.Title = c.SM.StringProps[properties.SiteBuildPagesHomeTitle]
	//c.HomePage.Paragraphs = bc.GetStringSlice("home.paragraphs")
	//c.HomePage.HelpTextPrefix = bc.GetString("home.card.help.prefix")
	//c.HomePage.HelpTextSuffix = bc.GetString("home.card.help.suffix")
	c.HomePage.Navbar = c.IndexNavbar
	c.HomePage.Scripts = c.Scripts
	c.HomePage.Links = c.Links
	c.HomePage.Footer = c.Footer
	c.HomePage.CardTmplSrc = c.CardSrc
	c.HomePage.MetaDescription = c.SM.StringProps[properties.SiteBuildPagesHomeMetaDescription]
	c.HomePage.MetaOgTitle = c.SM.StringProps[properties.SiteBuildPagesHomeMetaOgTitle]
	c.HomePage.MetaOgDescription = c.SM.StringProps[properties.SiteBuildPagesHomeMetaOgDescription]
	c.HomePage.MetaOgUrl = c.SM.StringProps[properties.SiteBuildPagesHomeMetaOgDescription]
	c.HomePage.FramesEnabled = c.FramesEnabled
	err = c.LoadFontMap()
	if err != nil {
		doxlog.Error(err.Error())
		return err
	}
	return nil
}
func (c *BuildConfig) TopicDesc(topic, key string) string {
	parts := strings.Split(topic, ".")
	sb := strings.Builder{}
	for _, part := range parts {
		var title string
		var ok bool
		if sb.Len() > 0 {
			sb.WriteString(" ")
		}
		if title, ok = c.titles[part]; ok {
			sb.WriteString(title)
			if part == "ps" {
				if strings.HasPrefix(key, "psa") {
					psalmParts := strings.Split(key, ".") //"psa142.text"
					if len(psalmParts) > 1 {
						sb.WriteString(" - ")
						if ps, ok := c.titles["psalm"]; ok {
							sb.WriteString(ps)
						} else {
							sb.WriteString("Psalm ")
						}
						sb.WriteString(" ")
						sb.WriteString(psalmParts[0][3:])
					}
				}
			}
		} else {
			sb.WriteString(part)
		}
	}
	return sb.String()
}

// GetSelectedPatterns returns the patterns referenced by FilePatternSelections
func (c *BuildConfig) GetSelectedPatterns() ([]string, error) {
	p, err := c.SM.GetElseSetStringProp(properties.SiteBuildTemplatesPatternsSelected)
	if err != nil {
		return nil, fmt.Errorf("BuildConfig.GetSelectedPatterns error reading property: %s", err)
	}
	c.FilePatternSelections = strings.Split(p, ",")
	var patterns []string
	if c.FilePatternSelections == nil {
		return nil, fmt.Errorf("BuildConfig.GetSelectedPatterns(): no pattern selected")
	}

	if len(c.FilePatternSelections) == 1 && c.FilePatternSelections[0] == "*" {
		return c.FilePatterns, nil
	}
	filePatternSliceLength := len(c.FilePatterns)
	for _, p := range c.FilePatternSelections {
		i, err := strconv.Atoi(p)
		if err != nil {
			return nil, fmt.Errorf("error converting %s to integer: %v", p, err)
		}
		if i >= filePatternSliceLength {
			msg := fmt.Sprintf("no pattern selected (index out of bounds)")
			doxlog.Error(msg)
			return nil, fmt.Errorf(msg)
		}
		patterns = append(patterns, c.FilePatterns[i])
	}
	return patterns, nil
}
func (c *BuildConfig) AddBuildLanguage(buildLanguage *atempl.TableLayout) {
	c.BuildLanguages = append(c.BuildLanguages, buildLanguage)
}
func (c *BuildConfig) AddBuildLanguages(buildLanguages []*atempl.TableLayout) {
	for _, buildLanguage := range buildLanguages {
		c.BuildLanguages = append(c.BuildLanguages, buildLanguage)
	}
}
func (c *BuildConfig) LoadBuildLanguages() error {
	buildLanguages, err := layouts.Manager.GetSiteBuildLayouts() //c.SM.GetSiteBuildLayouts()
	if err != nil {
		msg := fmt.Sprintf("buildConfig.LoadBuildLanguages: %v", err)
		doxlog.Error(msg)
		return fmt.Errorf(msg)
	}
	c.AddBuildLanguages(buildLanguages)
	return nil
}
func (c *BuildConfig) LoadFontMap() error {
	var err error
	// create the default FontSet
	var defaultFontSet = new(fonts.FontSet)
	defaultFontSet.FontPath = path.Join(c.AssetsPath, "pdf", "fonts")
	if !strings.HasSuffix(defaultFontSet.FontPath, string(os.PathSeparator)) {
		defaultFontSet.FontPath = defaultFontSet.FontPath + string(os.PathSeparator)
	}
	defaultFontSet.Bold = c.SM.StringProps[properties.SiteBuildPdfFontsDefaultBold]
	defaultFontSet.BoldItalic = c.SM.StringProps[properties.SiteBuildPdfFontsDefaultBoldItalic]
	defaultFontSet.Italic = c.SM.StringProps[properties.SiteBuildPdfFontsDefaultItalic]
	defaultFontSet.Regular = c.SM.StringProps[properties.SiteBuildPdfFontsDefaultRegular]
	defaultFontSet.KerningOn = c.SM.BoolProps[properties.SiteBuildPdfFontsDefaultKerningOn]
	defaultFontSet.KerningDivisor = c.SM.FloatProps[properties.SiteBuildPdfFontsDefaultKerningDivisor]
	defaultFontSet.LineBreakAdjuster = float64(c.SM.IntProps[properties.SiteBuildPdfFontsDefaultLineBreakAdjuster])
	c.PdfFontMap["default"] = defaultFontSet
	// get a slice of all unique language codes in the ltx directory of the database
	languageCodes, err := c.SM.GetLiturgicalTextLanguageCodes()
	if err != nil {
		return err
	}
	// Populate the font map with a fontSet for each language code.
	// The fonts to use are read from the database.  If a record
	// does not exist, it will be set to the default or one relevant
	// to the language code for the case of middle eastern and asian languages.
	for _, languageCode := range languageCodes {
		var fontSet = new(fonts.FontSet)
		fontSet.FontPath = defaultFontSet.FontPath
		var boldValue string
		var boldPath string
		var boldItalicValue string
		var boldItalicPath string
		var italicValue string
		var italicPath string
		var regularValue string
		var regularPath string
		var kerningOnPath string
		var kerningOn string
		var kerningDivisorPath string
		var kerningDivisor string
		var middleColAdjustmentValue string
		var middleColAdjustmentPath string
		defaultMiddleColAdjustmentValue := "0.95"
		var lineBreakAdjusterPath string
		var lineBreakAdjusterValue string
		defaultLineBreakAdjustmentValue := "2"
		englishPalaLineBreakAdjustmentValue := "3" // to avoid line break wrapping into another column
		var dateFormat string
		defaultDateFormat := "Monday, January 02, 2006"
		DMYFormat := "02-01-2006"
		YMDFormat := "2006-01-02"
		var dateFormatPath string

		switch languageCode {
		case "ar", "ara": // arabic
			boldValue = "GeezaPro.ttf" // "ScheherazadeNew-Bold.ttf" // "amiri-bold.ttf"
			boldPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultBold.Data().Path, "default", languageCode)
			boldItalicValue = "GeezaPro.ttf" // "ScheherazadeNew-Bold.ttf" // "GeezaPro.ttf" //"amiri-boldslanted.ttf"
			boldItalicPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultBoldItalic.Data().Path, "default", languageCode)
			dateFormat = DMYFormat
			dateFormatPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultDateFormat.Data().Path, "default", languageCode)
			italicValue = "GeezaPro.ttf" // "ScheherazadeNew-Bold.ttf" // "GeezaPro.ttf" //"amiri-slanted.ttf"
			italicPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultItalic.Data().Path, "default", languageCode)
			regularValue = "GeezaPro.ttf" // "ScheherazadeNew-Regular.ttf" // "GeezaPro.ttf" //"amiri-regular.ttf"
			regularPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultRegular.Data().Path, "default", languageCode)
			kerningOnPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultKerningOn.Data().Path, "default", languageCode)
			kerningOn = "false"
			kerningDivisorPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultKerningDivisor.Data().Path, "default", languageCode)
			kerningDivisor = "6.5"
			lineBreakAdjusterPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultMiddleColAdjuster.Data().Path, "default", languageCode)
			lineBreakAdjusterValue = defaultLineBreakAdjustmentValue
			middleColAdjustmentPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultMiddleColAdjuster.Data().Path, "default", languageCode)
			middleColAdjustmentValue = defaultMiddleColAdjustmentValue
		case "chi", "zh", "zho": // chinese
			boldValue = "simfang.ttf"
			boldPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultBold.Data().Path, "default", languageCode)
			boldItalicValue = "simfang.ttf"
			boldItalicPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultBoldItalic.Data().Path, "default", languageCode)
			italicValue = "simfang.ttf"
			italicPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultItalic.Data().Path, "default", languageCode)
			regularValue = "simfang.ttf"
			regularPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultRegular.Data().Path, "default", languageCode)
			kerningOnPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultKerningOn.Data().Path, "default", languageCode)
			kerningOn = "false"
			kerningDivisorPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultKerningDivisor.Data().Path, "default", languageCode)
			kerningDivisor = "6.5"
			lineBreakAdjusterPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultLineBreakAdjuster.Data().Path, "default", languageCode)
			lineBreakAdjusterValue = defaultLineBreakAdjustmentValue
			middleColAdjustmentPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultMiddleColAdjuster.Data().Path, "default", languageCode)
			middleColAdjustmentValue = defaultMiddleColAdjustmentValue
			dateFormat = YMDFormat
			dateFormatPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultDateFormat.Data().Path, "default", languageCode)
		case "chu": // church slavonic
			boldValue = "PonomarUnicode.ttf"
			boldPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultBold.Data().Path, "default", languageCode)
			boldItalicValue = "PonomarUnicode.ttf"
			boldItalicPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultBoldItalic.Data().Path, "default", languageCode)
			italicValue = "PonomarUnicode.ttf"
			italicPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultItalic.Data().Path, "default", languageCode)
			regularValue = "PonomarUnicode.ttf"
			regularPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultRegular.Data().Path, "default", languageCode)
			kerningOnPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultKerningOn.Data().Path, "default", languageCode)
			kerningOn = "false"
			kerningDivisorPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultKerningDivisor.Data().Path, "default", languageCode)
			kerningDivisor = "6.5"
			middleColAdjustmentPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultMiddleColAdjuster.Data().Path, "default", languageCode)
			middleColAdjustmentValue = defaultMiddleColAdjustmentValue
			lineBreakAdjusterPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultLineBreakAdjuster.Data().Path, "default", languageCode)
			lineBreakAdjusterValue = defaultLineBreakAdjustmentValue
			dateFormat = defaultDateFormat
			dateFormatPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultDateFormat.Data().Path, "default", languageCode)
		case "en", "es", "swh": // english, spanish, swahili
			boldValue = "LiberationSerif-Bold.ttf"
			boldPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultBold.Data().Path, "default", languageCode)
			boldItalicValue = "LiberationSerif-BoldItalic.ttf"
			boldItalicPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultBoldItalic.Data().Path, "default", languageCode)
			italicValue = "LiberationSerif-Italic.ttf"
			italicPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultItalic.Data().Path, "default", languageCode)
			regularValue = "LiberationSerif-Regular.ttf"
			regularPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultRegular.Data().Path, "default", languageCode)
			kerningOnPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultKerningOn.Data().Path, "default", languageCode)
			kerningOn = "false"
			kerningDivisorPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultKerningDivisor.Data().Path, "default", languageCode)
			kerningDivisor = "6.5"
			lineBreakAdjusterPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultLineBreakAdjuster.Data().Path, "default", languageCode)
			lineBreakAdjusterValue = englishPalaLineBreakAdjustmentValue
			middleColAdjustmentPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultMiddleColAdjuster.Data().Path, "default", languageCode)
			middleColAdjustmentValue = defaultMiddleColAdjustmentValue
			dateFormat = defaultDateFormat
			dateFormatPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultDateFormat.Data().Path, "default", languageCode)
		case "gr": // greek
			boldValue = "LiberationSerif-Bold.ttf"
			boldPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultBold.Data().Path, "default", languageCode)
			boldItalicValue = "LiberationSerif-BoldItalic.ttf"
			boldItalicPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultBoldItalic.Data().Path, "default", languageCode)
			italicValue = "LiberationSerif-Italic.ttf"
			italicPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultItalic.Data().Path, "default", languageCode)
			regularValue = "LiberationSerif-Regular.ttf"
			regularPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultRegular.Data().Path, "default", languageCode)
			kerningOnPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultKerningOn.Data().Path, "default", languageCode)
			kerningOn = "false"
			kerningDivisorPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultKerningDivisor.Data().Path, "default", languageCode)
			kerningDivisor = "6.5"
			lineBreakAdjusterPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultLineBreakAdjuster.Data().Path, "default", languageCode)
			lineBreakAdjusterValue = "2"
			middleColAdjustmentPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultMiddleColAdjuster.Data().Path, "default", languageCode)
			middleColAdjustmentValue = defaultMiddleColAdjustmentValue
			dateFormat = DMYFormat
			dateFormatPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultDateFormat.Data().Path, "default", languageCode)
		case "he": // hebrew
			boldValue = "ShofarDemi-Bold.ttf"
			boldPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultBold.Data().Path, "default", languageCode)
			boldItalicValue = "ShofarDemi-BoldOblique.ttf"
			boldItalicPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultBoldItalic.Data().Path, "default", languageCode)
			italicValue = "ShofarRegularOblique.ttf"
			italicPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultItalic.Data().Path, "default", languageCode)
			regularValue = "ShofarRegular.ttf"
			regularPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultRegular.Data().Path, "default", languageCode)
			kerningOnPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultKerningOn.Data().Path, "default", languageCode)
			kerningOn = "false"
			kerningDivisorPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultKerningDivisor.Data().Path, "default", languageCode)
			kerningDivisor = "6.5"
			lineBreakAdjusterPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultLineBreakAdjuster.Data().Path, "default", languageCode)
			lineBreakAdjusterValue = defaultLineBreakAdjustmentValue
			middleColAdjustmentPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultMiddleColAdjuster.Data().Path, "default", languageCode)
			middleColAdjustmentValue = defaultMiddleColAdjustmentValue
			dateFormat = DMYFormat
			dateFormatPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultDateFormat.Data().Path, "default", languageCode)
		case "ko", "kor": // korean
			boldValue = "UnGungseo.ttf"
			boldPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultBold.Data().Path, "default", languageCode)
			boldItalicValue = "UnGungseo.ttf"
			boldItalicPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultBoldItalic.Data().Path, "default", languageCode)
			italicValue = "UnGungseo.ttf"
			italicPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultItalic.Data().Path, "default", languageCode)
			regularValue = "UnGungseo.ttf"
			regularPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultRegular.Data().Path, "default", languageCode)
			kerningOnPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultKerningOn.Data().Path, "default", languageCode)
			kerningOn = "true"
			kerningDivisorPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultKerningDivisor.Data().Path, "default", languageCode)
			kerningDivisor = "6.5"
			lineBreakAdjusterPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultLineBreakAdjuster.Data().Path, "default", languageCode)
			lineBreakAdjusterValue = defaultLineBreakAdjustmentValue
			middleColAdjustmentPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultMiddleColAdjuster.Data().Path, "default", languageCode)
			middleColAdjustmentValue = defaultMiddleColAdjustmentValue
			dateFormat = YMDFormat
			dateFormatPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultDateFormat.Data().Path, "default", languageCode)
		default:
			boldValue = defaultFontSet.Bold
			boldPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultBold.Data().Path, "default", languageCode)
			boldItalicValue = defaultFontSet.BoldItalic
			boldItalicPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultBoldItalic.Data().Path, "default", languageCode)
			italicValue = defaultFontSet.Italic
			italicPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultItalic.Data().Path, "default", languageCode)
			regularValue = defaultFontSet.Regular
			regularPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultRegular.Data().Path, "default", languageCode)
			kerningOnPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultKerningOn.Data().Path, "default", languageCode)
			kerningOn = "false"
			kerningDivisorPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultKerningDivisor.Data().Path, "default", languageCode)
			kerningDivisor = "6.5"
			lineBreakAdjusterPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultLineBreakAdjuster.Data().Path, "default", languageCode)
			lineBreakAdjusterValue = defaultLineBreakAdjustmentValue
			middleColAdjustmentPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultMiddleColAdjuster.Data().Path, "default", languageCode)
			middleColAdjustmentValue = defaultMiddleColAdjustmentValue
			dateFormat = defaultDateFormat
			dateFormatPath = strings.ReplaceAll(properties.SiteBuildPdfFontsDefaultDateFormat.Data().Path, "default", languageCode)
		}
		var propBold = properties.PropertyData{
			Description:  "bold font to use",
			DefaultValue: boldValue,
			Path:         boldPath,
			ValueType:    propertyValueTypes.String,
		}
		fontSet.Bold, err = c.SM.GetAdHocElseSetStringProp(propBold)
		if err != nil {
			return err
		}
		var propBoldItalic = properties.PropertyData{
			Description:  "bold italic font to use.",
			DefaultValue: boldItalicValue,
			Path:         boldItalicPath,
			ValueType:    propertyValueTypes.String,
		}
		fontSet.BoldItalic, err = c.SM.GetAdHocElseSetStringProp(propBoldItalic)
		if err != nil {
			return err
		}
		var propItalic = properties.PropertyData{
			Description:  "italic font to use",
			DefaultValue: italicValue,
			Path:         italicPath,
			ValueType:    propertyValueTypes.String,
		}
		fontSet.Italic, err = c.SM.GetAdHocElseSetStringProp(propItalic)
		if err != nil {
			return err
		}
		var propRegular = properties.PropertyData{
			Description:  "regular font to use",
			DefaultValue: regularValue,
			Path:         regularPath,
			ValueType:    propertyValueTypes.String,
		}
		fontSet.Regular, err = c.SM.GetAdHocElseSetStringProp(propRegular)
		if err != nil {
			return err
		}
		// kerning on
		var propKerningOn = properties.PropertyData{
			Description:  "if set to true, kerning will be used with this font",
			DefaultValue: kerningOn,
			Path:         kerningOnPath,
			ValueType:    propertyValueTypes.Boolean,
		}
		fontSet.KerningOn, err = c.SM.GetAdHocElseSetBoolProp(propKerningOn)
		if err != nil {
			return err
		}
		// kerning divisor
		var propKerningDivisor = properties.PropertyData{
			Description:  "value used to adjust width between letters for this font",
			DefaultValue: kerningDivisor,
			Path:         kerningDivisorPath,
			ValueType:    propertyValueTypes.Float,
		}
		fontSet.KerningDivisor, err = c.SM.GetAdHocElseSetFloatProp(propKerningDivisor)

		// Line break adjustment
		var propLineBreakAdjustment = properties.PropertyData{
			Description:  "value by which to adjust linebreaks, normally set to 2",
			DefaultValue: lineBreakAdjusterValue,
			Path:         lineBreakAdjusterPath,
			ValueType:    propertyValueTypes.String,
		}

		var strLineBreakAdjustment string
		strLineBreakAdjustment, err = c.SM.GetAdHocElseSetStringProp(propLineBreakAdjustment)
		if err != nil {
			return err
		}
		fontSet.LineBreakAdjuster, err = strconv.ParseFloat(strLineBreakAdjustment, 64)
		if err != nil {
			fontSet.LineBreakAdjuster = 2
		}
		// Middle Column adjustment
		var propMiddleColAdjustment = properties.PropertyData{
			Description:  "value by which to adjust the middle column when the page has three columns, normally set to 0.95",
			DefaultValue: middleColAdjustmentValue,
			Path:         middleColAdjustmentPath,
			ValueType:    propertyValueTypes.String,
		}

		var strMiddleColAdjustment string
		strMiddleColAdjustment, err = c.SM.GetAdHocElseSetStringProp(propMiddleColAdjustment)
		if err != nil {
			return err
		}
		fontSet.MiddleColAdjuster, err = strconv.ParseFloat(strMiddleColAdjustment, 64)
		if err != nil {
			fontSet.MiddleColAdjuster = 0
		}
		// Date Format
		var propDateFormat = properties.PropertyData{
			Description:  properties.SiteBuildPdfFontsDefaultDateFormat.Data().Description,
			DefaultValue: dateFormat,
			Path:         dateFormatPath,
			ValueType:    propertyValueTypes.String,
		}
		fontSet.DateFormat, err = c.SM.GetAdHocElseSetStringProp(propDateFormat)
		if err != nil {
			fontSet.DateFormat = "01-02-2006"
		}
		// having built the fontSet for this language code, add it to the font map
		c.PdfFontMap[languageCode] = fontSet
	}
	return nil
}

type BuildConfigArray []BuildConfig

// BuildConfigRecord Struct
// Value holds the json for the BuildConfig
type BuildConfigRecord struct {
	ID           string `json:"id"`
	Value        string `json:"value"`
	CreatedWhen  string `json:"createdWhen"`
	ModifiedWhen string `json:"modifiedWhen"`
}

// BuildConfigRecordArray is an array of BuildConfigRecords
type BuildConfigRecordArray []CSS

func (c *BuildConfig) LocaleDate(year, month, day int, layout string) string {
	t := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC)
	f := t.Format(layout)
	switch t.Month().String() {
	case "January":
		f = strings.Replace(f, "January", c.titles["jan"], 1)
	case "February":
		f = strings.Replace(f, "February", c.titles["feb"], 1)
	case "March":
		f = strings.Replace(f, "March", c.titles["mar"], 1)
	case "April":
		f = strings.Replace(f, "April", c.titles["apr"], 1)
	case "May":
		f = strings.Replace(f, "May", c.titles["may"], 1)
	case "June":
		f = strings.Replace(f, "June", c.titles["jun"], 1)
	case "July":
		f = strings.Replace(f, "July", c.titles["jul"], 1)
	case "August":
		f = strings.Replace(f, "August", c.titles["aug"], 1)
	case "September":
		f = strings.Replace(f, "September", c.titles["sep"], 1)
	case "October":
		f = strings.Replace(f, "October", c.titles["oct"], 1)
	case "November":
		f = strings.Replace(f, "November", c.titles["nov"], 1)
	case "December":
		f = strings.Replace(f, "December", c.titles["dec"], 1)
	}
	switch t.Weekday().String() {
	case "Sunday":
		f = strings.Replace(f, "Sunday", c.titles["sun"], 1)
	case "Monday":
		f = strings.Replace(f, "Monday", c.titles["mon"], 1)
	case "Tuesday":
		f = strings.Replace(f, "Tuesday", c.titles["tue"], 1)
	case "Wednesday":
		f = strings.Replace(f, "Wednesday", c.titles["wed"], 1)
	case "Thursday":
		f = strings.Replace(f, "Thursday", c.titles["thu"], 1)
	case "Friday":
		f = strings.Replace(f, "Friday", c.titles["fri"], 1)
	case "Saturday":
		f = strings.Replace(f, "Saturday", c.titles["sat"], 1)
	}
	return f
}
