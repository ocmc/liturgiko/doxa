// Package vault provides encryption / decryption. This package uses code written by George Tankersley, from https://github.com/gtank/cryptopasta/blob/master/encrypt.go and modified for Doxa purposes. Users should be instructed not to include the vault in a github or gitlab repository. However, the cypher key is derived from the user's mac address, so it is unlikely that it could be decrypted by someone not logged into the user's machine. This also means that the vault is not portable across machines.
package vault

import (
	"bufio"
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	bolt "go.etcd.io/bbolt"
	"io"
	"net"
	"path"
	"strings"
	"sync"
)

type Manager struct {
	mutex       sync.Mutex
	VaultPath   string
	keyRing     map[string][32]byte
	CurrentUser string
}

func (m *Manager) AddKey(username string, key [32]byte) {
	m.keyRing[username] = key
}

func (m *Manager) RevokeKey(username string) {
	delete(m.keyRing, username)
}

// NewManager returns a vault manager.  The vault will be created if it does not exist.
func NewManager(vaultPath string) (*Manager, error) {
	m := new(Manager)
	m.VaultPath = vaultPath
	m.keyRing = map[string][32]byte{}
	// open the vault db, which will create it if it does not exist
	ds, err := kvs.NewBoltKVS(m.VaultPath)
	if err != nil {
		return nil, err
	}
	// close the vault db
	err = ds.Close()
	if err != nil {
		return nil, err
	}
	//TODO: desktop v cloud?
	m.CurrentUser = "localUser"
	return m, nil
}

// SetGitlabCredentials prompts the user for their
// gitlab user ID and password and
// stores them encrypted in the local Doxa vault
func (m *Manager) SetGitlabCredentials(username, password string) (bool, error) {
	username = strings.TrimSpace(username)
	err := m.WriteVaultCredentials(username, password, username)
	if err != nil {
		return false, err
	}
	dUid, dPwd, err := m.ReadVaultCredentials("users", m.VaultPath)
	if err != nil {
		return false, err
	}
	if dUid != username {
		return false, nil
	}
	if dPwd != password {
		return false, nil
	}
	return true, nil
}

// WriteVaultCredentials encrypts the plaintext user and password, then writes them to the specified file.
// The prefix is the section in the vault in which to store the credentials.
// If it does not exist, it will be created.
func (m *Manager) WriteVaultCredentials(user, pwd, prefix string) error {
	// EncryptWithKey
	userEncrypted, err := m.Encrypt([]byte(user))
	if err != nil {
		return err
	}
	pwdEncrypted, err := m.Encrypt([]byte(pwd))

	var records []*kvs.DbR

	// create the record for the user id
	uidRec := kvs.NewDbR()
	uidRec.KP.Dirs.Push(prefix)
	uidRec.KP.KeyParts.Push("uid")
	uidRec.Value = string(userEncrypted)
	records = append(records, uidRec)

	// create the record for the password
	pwdRec := kvs.NewDbR()
	pwdRec.KP.Dirs.Push(prefix)
	pwdRec.KP.KeyParts.Push("pwd")
	pwdRec.Value = string(pwdEncrypted)
	records = append(records, pwdRec)

	// write the records
	return m.Write(records)
}

// ReadVaultCredentials returns the unencrypted user id and password stored under the prefix.
// The prefix is the section in the vault in which to store the credentials.
func (m *Manager) ReadVaultCredentials(prefix, vaultPath string) (string, string, error) {
	var keyPaths []*kvs.KeyPath

	// set up key path for user id
	uKp := kvs.NewKeyPath()
	uKp.Dirs.Push(prefix)
	uKp.KeyParts.Push("uid")
	keyPaths = append(keyPaths, uKp)

	// set up key path for password
	pKp := kvs.NewKeyPath()
	pKp.Dirs.Push(prefix)
	pKp.KeyParts.Push("pwd")
	keyPaths = append(keyPaths, pKp)

	records, err := m.Read(keyPaths)
	if err != nil {
		return "", "", err
	}
	if len(records) != 2 {
		return "", "", fmt.Errorf("error reading %s. Expected 2 records, got %d", vaultPath, len(records))
	}
	return records[0].Value, records[1].Value, err
}

// ReadProfile returns the unencrypted user profile
func (m *Manager) ReadProfile(username, vaultPath string) (string, string, error) {
	var keyPaths []*kvs.KeyPath

	// set up key path for user id
	uKp := kvs.NewKeyPath()
	uKp.Dirs.Push(username)
	uKp.KeyParts.Push("profile")
	keyPaths = append(keyPaths, uKp)

	records, err := m.Read(keyPaths)
	if err != nil {
		return "", "", err
	}
	if len(records) != 2 {
		return "", "", fmt.Errorf("error reading %s. Expected 2 records, got %d", vaultPath, len(records))
	}
	return records[0].Value, records[1].Value, err
}

// Read returns the records in the vault that match the supplied key paths.
func (m *Manager) Read(keyPaths []*kvs.KeyPath) ([]*kvs.DbR, error) {
	ds, err := kvs.NewBoltKVS(m.VaultPath)
	if err != nil {
		return nil, fmt.Errorf("error opening db %s: %v", m.VaultPath, err)
	}
	defer ds.Db.Close()
	mapper, err := kvs.NewKVS(ds)
	if err != nil {
		fmt.Errorf("error creating key-value-store: %v", err)
	}

	var records []*kvs.DbR

	for _, kp := range keyPaths {
		rec, err := mapper.Db.Get(kp)
		if err != nil {
			return nil, fmt.Errorf("error reading %s: %v", kp.Path(), err)
		}
		decoded, err := base64.StdEncoding.DecodeString(rec.Value)
		if err != nil {
			return nil, fmt.Errorf("error decoding value for %s: %v", kp.Path(), err)
		}
		val, err := m.Decrypt([]byte(decoded))
		if err != nil {
			return nil, fmt.Errorf("error decrypting value for %s: %v", kp.Path(), err)
		}
		rec.Value = string(val)
		records = append(records, rec)
	}
	err = mapper.Db.Close()
	return records, err
}

func (m *Manager) RecExists(path *kvs.KeyPath) bool {
	ds, err := kvs.NewBoltKVS(m.VaultPath)
	if err != nil {
		doxlog.Errorf("error opening db %s: %v", m.VaultPath, err)
		return false
	}
	defer ds.Db.Close()
	return ds.Exists(path)
}
func (m *Manager) Write(records []*kvs.DbR) error {
	m.mutex.Lock()
	defer m.mutex.Unlock()
	// open the vault db
	ds, err := kvs.NewBoltKVS(m.VaultPath)
	if err != nil {
		msg := fmt.Sprintf("%v", err)
		if strings.Contains(msg, "timeout") {
			msg = fmt.Sprintf("%s. A timeout can occur if another program has the database open for writing.  Find and shutdown the other program and try again.", msg)
		}
		return fmt.Errorf("error opening db %s: %s", m.VaultPath, msg)
	}
	defer func(Db *bolt.DB) {
		err := Db.Close()
		if err != nil {
			fmt.Printf("error closing vault: %v", err)
		}
	}(ds.Db)
	mapper, err := kvs.NewKVS(ds)
	if err != nil {
		return fmt.Errorf("error creating key-value-store: %v", err)
	}
	for _, rec := range records {
		rec.Value = base64.StdEncoding.EncodeToString([]byte(rec.Value))
		err = mapper.Db.Put(rec)
		if err != nil {
			return err
		}
	}
	return mapper.Db.Close()
}

func (m *Manager) Delete(kp *kvs.KeyPath) error {
	m.mutex.Lock()
	defer m.mutex.Unlock()
	// open the vault db
	ds, err := kvs.NewBoltKVS(m.VaultPath)
	if err != nil {
		msg := fmt.Sprintf("%v", err)
		if strings.Contains(msg, "timeout") {
			msg = fmt.Sprintf("%s. A timeout can occur if another program has the database open for writing.  Find and shutdown the other program and try again.", msg)
		}
		return fmt.Errorf("error opening db %s: %s", m.VaultPath, msg)
	}
	defer func(Db *bolt.DB) {
		err := Db.Close()
		if err != nil {
			fmt.Printf("error closing vault: %v", err)
		}
	}(ds.Db)
	mapper, err := kvs.NewKVS(ds)
	if err != nil {
		return fmt.Errorf("error creating key-value-store: %v", err)
	}
	err = mapper.Db.Delete(*kp)
	if err != nil {
		return err
	}
	return mapper.Db.Close()
}

func (m *Manager) IsInKeyRing(user string) bool {
	_, answer := m.keyRing[user]
	return answer
}

// Encrypt encrypts data using 256-bit AES-GCM.  This both hides the content of
// the data and provides a check that it hasn't been altered. Output takes the
// form nonce|ciphertext|tag where '|' indicates concatenation.
func (m *Manager) Encrypt(plaintext []byte) (ciphertext []byte, err error) {
	return EncryptWithKey(m.getKeyFromOsEnv(), plaintext)
}
func EncryptWithKey(key, plaintext []byte) (ciphertext []byte, err error) {
	c, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return nil, err
	}
	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return nil, err
	}
	return gcm.Seal(nonce, nonce, plaintext, nil), nil
}

// Decrypt decrypts data using 256-bit AES-GCM.  This both hides the content of
// the data and provides a check that it hasn't been altered. Expects input
// form nonce|ciphertext|tag where '|' indicates concatenation.
func (m *Manager) Decrypt(ciphertext []byte) (plaintext []byte, err error) {

	return DecryptWithKey(m.getKeyFromOsEnv(), ciphertext)
}
func DecryptWithKey(key, ciphertext []byte) (plaintext []byte, err error) {
	c, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return nil, err
	}
	nonceSize := gcm.NonceSize()
	nonce, ciphertext := ciphertext[:nonceSize], ciphertext[nonceSize:]
	return gcm.Open(nil, nonce, ciphertext, nil)
}
func (m *Manager) getKeyFromOsEnv() (seed []byte) {
	// somehow we need to find out which user is the current user
	// for desktop it is easy
	k := m.keyRing[m.CurrentUser]
	return k[:]
}

// NewEncryptionKey generates a random 256-bit key for Encrypt() and
// Decrypt(). It panics if the source of randomness fails.
func NewEncryptionKey() []byte {
	key := [32]byte{}
	_, err := io.ReadFull(rand.Reader, key[:])
	if err != nil {
		doxlog.Errorf("error creating encryption key: %v", err)
	}
	return key[:]
}
func CreateSeed(filename string) error {
	key := make([]byte, 32) // Adjust the size of the byte slice as needed
	_, err := rand.Read(key)
	if err != nil {
		return fmt.Errorf("error generating random key: %v", err)
	}
	base64Key := base64.StdEncoding.EncodeToString(key)
	if err := ltfile.CreateDirs(path.Dir(filename)); err != nil {
		return err
	}
	f, err := ltfile.Create(filename)
	defer f.Close()
	if err != nil {
		return err
	}
	w := bufio.NewWriter(f)
	_, err = w.WriteString(base64Key)
	w.Flush()
	return err
}

// keyFromMac creates a 32 byte key using the computer's mac address.
func keyFromMac() *[32]byte {
	key := fmt.Sprintf("%32.32X", macUint64())
	sb := strings.Builder{}
	runes := []rune(key)
	l := len(runes)
	for i := 0; i < l; i++ {
		// convert non-digits into digits
		sb.WriteString(toDigit(runes[i]))
	}
	newKey := sb.String()
	if len(newKey) > 32 { // shorten to 32
		newKey = newKey[:32]
	} else { // lengthen to 32 if necessary
		i := 0
		for len(newKey) < 32 {
			newKey = newKey + string(newKey[i])
			i++
		}
	}
	keyByte := []byte(newKey)
	var arr [32]byte
	copy(arr[:], keyByte)
	return &arr
}
func macUint64() uint64 {
	interfaces, err := net.Interfaces()
	if err != nil {
		return uint64(0)
	}

	for _, i := range interfaces {
		if i.Flags&net.FlagUp != 0 && bytes.Compare(i.HardwareAddr, nil) != 0 {

			// Skip locally administered addresses
			if i.HardwareAddr[0]&2 == 2 {
				continue
			}

			var mac uint64
			for j, b := range i.HardwareAddr {
				if j >= 8 {
					break
				}
				mac <<= 8
				mac += uint64(b)
			}

			return mac
		}
	}

	return uint64(0)
}
func toDigit(r rune) string {
	s := strings.ToLower(string(r))
	switch s {
	case "0":
		return ""
	case "1", "2", "3", "4", "5", "6", "7", "8", "9":
		return s
	case "a":
		return "1"
	case "b":
		return "2"
	case "c":
		return "3"
	case "d":
		return "4"
	case "e":
		return "5"
	case "f":
		return "6"
	case "g":
		return "7"
	case "h":
		return "8"
	case "i":
		return "9"
	case "j":
		return "1"
	case "k":
		return "2"
	case "l":
		return "3"
	case "m":
		return "4"
	case "n":
		return "5"
	case "o":
		return "6"
	case "p":
		return "7"
	case "q":
		return "8"
	case "r":
		return "9"
	case "s":
		return "1"
	case "t":
		return "2"
	case "u":
		return "3"
	case "v":
		return "4"
	case "w":
		return "5"
	case "x":
		return "6"
	case "y":
		return "7"
	case "z":
		return "8"
	default:
		return "9"
	}
}
