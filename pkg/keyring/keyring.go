// Package keyring provides a set of liturgical text (ltx) topic/keys
// Always be sure to use the constructor func NewKeyRing rather than new(KeyRing).
// Note that a database Put (e.g. in kvs/boltdb.go) for a ltx record
// will write the corresponding keyring record.  The database will not
// remove a keyring record if a ltx delete record occurs, since the
// keyring will get reloaded at startup, and we don't know if the deleted ltx
// was the only record with that topic:key.
// Note: this package is incomplete.  A future version will provide an
// index of all libraries using each topic:key and all templates using them.
package keyring

import (
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"golang.org/x/net/context"
	"golang.org/x/sync/errgroup"
	"strings"
	"sync"
)

const DirName = "keyring"

type KeyRing struct {
	MU                 sync.Mutex
	mapper             *kvs.KVS
	topicKeyMap        sync.Map
	topicMap           sync.Map
	titleKp            *kvs.KeyPath
	UiTitlesMap        sync.Map // e.g. UiTitlesMap["en"] returns titles for English
	MediaReverseLookup sync.Map // key is topic.key, value = topic:key
}

func NewKeyRing(mapper *kvs.KVS, titlesPath string) *KeyRing {
	kr := new(KeyRing)
	kr.titleKp = kvs.NewKeyPath()
	kr.titleKp.ParsePath(titlesPath)
	kr.titleKp.Dirs.Push("map")
	kr.mapper = mapper
	// kr.topicKeyMap = make(map[string]bool)
	// kr.topicMap = make(map[string]string)
	// kr.MediaReverseLookup = make(map[string]string)
	return kr
}

// Build combines the topic/keys from all ltx libraries and combines them into a set and writes them to the keyring dir in the database
func (kr *KeyRing) Build() error {
	// kr.MU.Lock()
	// defer kr.MU.Unlock()
	var err error
	err = kr.IndexLibraries()
	if err != nil {
		return err
	}
	// TODO: index templates
	return nil
}

// AddForMedia is used when converting an ALWB media map to Doxa.
func (kr *KeyRing) AddForMedia(id string) {
	if !strings.HasSuffix(id, ".text") {
		return
	}
	var key, value string
	if strings.HasPrefix(id, "keyring/") {
		value = id[8:]
		value = value[:len(value)-5]
		key = strings.ReplaceAll(value, ":", ".")
		kr.MediaReverseLookup.Store(key, value)
	}
}

func (kr *KeyRing) Add(kp *kvs.KeyPath) error {
	if kp == nil {
		return fmt.Errorf("nil kp not allowed")
	}
	// kr.MU.Lock()
	// defer kr.MU.Unlock()

	if kp.Dirs.First() != "ltx" {
		return nil
	}
	if kp.Dirs.Size() != 3 {
		return nil
	}
	topicKey := kp.LmlTopicKey()
	if _, ok := kr.topicKeyMap.Load(topicKey); !ok {
		kr.topicKeyMap.Store(topicKey, true)
	}
	if _, ok := kr.topicMap.Load(topicKey); !ok {
		kr.topicMap.Store(topicKey, kp.Dirs.Get(1))
	}
	return nil
}

func (kr *KeyRing) IndexLibraries() error {
	ProcessLibs := func(ctx context.Context) ([][]*kvs.DbR, error) {
		g, ctx := errgroup.WithContext(ctx)
		kp := kvs.NewKeyPath()
		kp.Dirs.Push("ltx")
		libs, err := kr.mapper.Db.DirNames(*kp)
		if err != nil {
			return nil, err
		}
		results := make([][]*kvs.DbR, len(libs))
		for i, lib := range libs {
			i, lib := i, lib
			g.Go(func() error {
				result, err := kr.IndexLibrary(ctx, lib)
				if err == nil {
					results[i] = result
				}
				return err
			})
		}
		if err := g.Wait(); err != nil {
			return nil, err
		}
		return results, nil
	}

	results, err := ProcessLibs(context.Background())
	if err != nil {
		return err
	}
	resultsMap := make(map[string]*kvs.DbR)
	for _, recs := range results {
		for _, rec := range recs {
			var r *kvs.DbR
			var ok bool
			// create a reverse lookup for converting the alwb media map
			kr.AddForMedia(rec.KP.Path())
			// add each topic.  We will later parse the topic and create configs/../site/build/pages/titles/map records
			kr.topicMap.Store(rec.KP.Dirs.Last(), "")
			// add topic key
			kr.topicKeyMap.Store(rec.KP.LmlTopicKey(), true)
			// continue processing to record which libraries use the topic
			if r, ok = resultsMap[rec.KP.Path()]; ok {
				if !strings.Contains(r.Value, rec.Value) {
					if len(r.Value) > 0 {
						r.Value = fmt.Sprintf("%s, %s", r.Value, rec.Value)
					} else {
						r.Value = rec.Value
					}
				}
				resultsMap[r.KP.Path()] = r
			} else {
				resultsMap[rec.KP.Path()] = rec
			}
		}
	}
	var topicKeys []*kvs.DbR
	for _, v := range resultsMap {
		topicKeys = append(topicKeys, v)
	}
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(DirName)
	if kr.mapper.Db.Exists(kp) {
		if err = kr.mapper.Db.Delete(*kp); err != nil {
			return fmt.Errorf("error deleting keyring from database: %v", err)
		}
	}
	err = kr.mapper.Db.Batch(topicKeys)
	if err != nil {
		return err
	}
	return kr.CreateSiteBuildPagesTitles()
}

// CreateSiteBuildPagesTitles ensures there is a database record for all segments of a topic
// so the user can specify the text to appear in the search results.  For example,
// the topic me.m01.d30 will have three entries:  me (Menaion), m01 (January), and d30 (Day 30).
// The method names comes from the database path: configs/{name}/site/build/pages/Titles.
// If the topic segment does not exist in the config for titles, it is created, and the value
// is set to an empty string.  It is up to the user to decide what text to show for the segment.
func (kr *KeyRing) CreateSiteBuildPagesTitles() error {
	var returnErr error
	kr.topicMap.Range(func(k, val any) bool {
		parts := strings.Split(k.(string), ".")
		for _, p := range parts {
			kp := kvs.NewKeyPath()
			err := kp.ParsePath(kr.titleKp.Path())
			if err != nil {
				returnErr = fmt.Errorf("error parsing path %s: %v\n", kr.titleKp.Path(), err)
				doxlog.Error(returnErr.Error())
				return false
			}
			kp.KeyParts.Push(p)
			if !kr.mapper.Db.Exists(kp) {
				dbr := kvs.NewDbR()
				dbr.KP = kp
				err := kr.mapper.Db.Put(dbr)
				if err != nil {
					returnErr = fmt.Errorf("error writing %s to database: %v\n", kp.Path(), err)
					doxlog.Error(returnErr.Error())
					return false
				}
			}
		}
		return true
	})
	return returnErr
}
func (kr *KeyRing) IndexLibrary(ctx context.Context, lib string) ([]*kvs.DbR, error) {
	libKp := kvs.NewKeyPath()
	libKp.Dirs.Push("ltx")
	libKp.Dirs.Push(lib)
	keyMap, err := kr.mapper.Db.InverseKeyMap(libKp)
	if err != nil {
		return nil, err
	}
	var records []*kvs.DbR
	for _, v := range *keyMap {
		for _, r := range v {
			records = append(records, r.KeyRingRec())
		}
	}
	return records, nil
}
func (kr *KeyRing) IndexTemplates() error {
	// for the future
	return nil
}

// Exists returns true if the topicKey is found in the keyring
func (kr *KeyRing) Exists(topicKey string) bool {
	_, exists := kr.topicKeyMap.Load(topicKey)
	return exists
}
