/*
Package tmplfunc provides a convenient way to manage and apply custom functions
to Go HTML templates, including predefined functions.

Predefined Functions:

This package comes with predefined functions that are automatically added to every
new FuncMap created with New(). Here are the predefined functions and their usage:

  - slice: {{slice start .Array}}
    Returns a slice from the given start index to the end of the array.

  - lower: {{lower .String}}
    Converts the entire string to lowercase.

  - upper: {{upper .String}}
    Converts the entire string to uppercase.

To get information about predefined functions programmatically, use the
GetPredefinedFunctionInfo() function.

Example usage in a template:

	<ul>
	{{range slice 2 .Items}}
	    <li>{{upper .}}</li>
	{{end}}
	</ul>

	<p>{{lower "HELLO WORLD"}}</p>

This will create a list of items starting from the third item (index 2) of .Items,
with each item converted to uppercase. It will also display "hello world" in lowercase.

Basic usage of the package:

	import "yourmodule/pkg/tmplfunc"

	// Create a new FuncMap with predefined functions
	tfm := tmplfunc.New()

	// Add a custom function
	tfm.Add("customFunc", yourCustomFunction)

	// Apply functions to an existing template
	template = tfm.ApplyTo(template)

	// Get information about predefined functions
	info := tmplfunc.GetPredefinedFunctionInfo()
	for _, funcInfo := range info {
	    fmt.Printf("Function: %s\nUsage: %s\n", funcInfo.Key, funcInfo.Usage)
	}

For more information on how to use this package, see the documentation for individual functions.
*/
package tmplfunc

import (
	"fmt"
	"html/template"
	"reflect"
	"strings"
)

// FuncMap is a wrapper around template.FuncMap that provides
// methods for adding and applying custom functions to templates.
// To get the underlying map, call Map()
type FuncMap struct {
	funcMap template.FuncMap
}

// PredefinedFunc represents a predefined function with its usage information.
type PredefinedFunc struct {
	Key   string
	Func  interface{}
	Usage string
}

// predefinedFunctions is a slice of PredefinedFunc, containing all predefined functions.
var predefinedFunctions = []PredefinedFunc{
	{
		Key: "slice",
		Func: func(start int, a interface{}) interface{} {
			v := reflect.ValueOf(a)
			if v.Kind() != reflect.Slice {
				return nil
			}
			if start < 0 || start >= v.Len() {
				return nil
			}
			return v.Slice(start, v.Len()).Interface()
		},
		Usage: "{{slice start .Array}} - Returns a slice from the given start index to the end of the array. You can use range over the returned slice to iterate over the , e.g. {{range slice 2 .Array}}.",
	},
	{
		Key:   "lower",
		Func:  strings.ToLower,
		Usage: "{{lower .String}} - Converts the entire string to lowercase.",
	},
	{
		Key:   "upper",
		Func:  strings.ToUpper,
		Usage: "{{upper .String}} - Converts the entire string to uppercase.",
	},
	{
		Key: "typeOf",
		Func: func(v interface{}) string {
			return fmt.Sprintf("%T", v)
		},
		Usage: "{{typeOf .Value}} - Returns the type of the given value as a string.",
	},
	{
		Key: "add",
		Func: func(a, b int) int {
			return a + b
		},
		Usage: "{{add a b}} - Adds two integers and returns the result.",
	},
	// Add more predefined functions here as needed
}

// New creates and returns a new FuncMap with predefined functions.
// It initializes a template.FuncMap internally and adds predefined functions.
//
// Usage:
//
//	tfm := tmplfunc.New()
func New() *FuncMap {
	fm := &FuncMap{
		funcMap: make(template.FuncMap),
	}

	// Add predefined functions
	for _, predef := range predefinedFunctions {
		fm.Add(predef.Key, predef.Func)
	}

	return fm
}

// Add adds a new function to the FuncMap.
// The function can later be used in templates under the specified name.
//
// Parameters:
//   - name: The name to use for the function in templates.
//   - fn: The function to add. It can be any valid template function.
//
// Usage:
//
//	tfm.Add("uppercase", strings.ToUpper)
//	tfm.Add("formatDate", func(t time.Time) string {
//	    return t.Format("2006-01-02")
//	})
func (fm *FuncMap) Add(name string, fn interface{}) {
	fm.funcMap[name] = fn
}

// Map returns the underlying template.FuncMap.
// This can be useful when you need to pass the function map directly
// to template parsing functions.
//
// Usage:
//
//	template.New("example").Funcs(tfm.Map()).Parse(templateString)
func (fm *FuncMap) Map() template.FuncMap {
	return fm.funcMap
}

// ApplyTo applies the function map to an existing template.
// This is useful when you have already parsed templates and want to
// add custom functions to them.
//
// Parameters:
//   - t: A pointer to the template to which the functions will be applied.
//
// Returns:
//   - A pointer to the modified template.
//
// Usage:
//
//	// Assuming 'templates' is your parsed template
//	templates = tfm.ApplyTo(templates)
//
// See the package documentation for examples of using predefined functions.
func (fm *FuncMap) ApplyTo(t *template.Template) *template.Template {
	return t.Funcs(fm.funcMap)
}

// GetPredefinedFunctionInfo returns a slice of PredefinedFunc,
// containing information about all predefined functions.
//
// Usage:
//
//	info := tmplfunc.GetPredefinedFunctionInfo()
//	for _, funcInfo := range info {
//	    fmt.Printf("Function: %s\nUsage: %s\n\n", funcInfo.Key, funcInfo.Usage)
//	}
func GetPredefinedFunctionInfo() []PredefinedFunc {
	return predefinedFunctions
}
