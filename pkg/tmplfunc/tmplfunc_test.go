package tmplfunc

import (
	"html/template"
	"reflect"
	"strings"
	"testing"
)

// TestNew tests the New function
func TestNew(t *testing.T) {
	tfm := New()
	if tfm == nil {
		t.Error("New() returned nil")
	}
	if len(tfm.Map()) == 0 {
		t.Error("New() returned an empty FuncMap")
	}
}

// TestFuncMapMethods tests various methods of the FuncMap struct
func TestFuncMapMethods(t *testing.T) {
	tfm := New()

	// Test Add method
	tfm.Add("exampleFunc", func() string {
		return "example"
	})
	if _, ok := tfm.Map()["exampleFunc"]; !ok {
		t.Errorf("Expected function 'exampleFunc' not found")
	}

	// Test Map method
	if len(tfm.Map()) == 0 {
		t.Errorf("Expected FuncMap to have functions")
	}
}

// TestApplyTo tests the ApplyTo method
func TestApplyTo(t *testing.T) {
	tfm := New()
	tmpl := template.New("test")
	tmpl = tfm.ApplyTo(tmpl)

	tmpl, err := tmpl.Parse("{{upper .}}")
	if err != nil {
		t.Fatalf("Failed to parse template: %v", err)
	}

	var buf strings.Builder
	err = tmpl.Execute(&buf, "hello")
	if err != nil {
		t.Fatalf("Failed to execute template: %v", err)
	}
	if buf.String() != "HELLO" {
		t.Errorf("Expected 'HELLO', but got %s", buf.String())
	}
}

// TestPredefinedFunctions tests the predefined functions in FuncMap
func TestPredefinedFunctions(t *testing.T) {
	tfm := New()

	// Test slice function
	data := []string{"item1", "item2", "item3", "item4"}
	slicedItems := tfm.Map()["slice"].(func(int, interface{}) interface{})
	result := slicedItems(2, data)
	expected := []string{"item3", "item4"}
	if !reflect.DeepEqual(result, expected) {
		t.Errorf("slice: Expected %v, got %v", expected, result)
	}

	// Test upper function
	upperFunc := tfm.Map()["upper"].(func(string) string)
	upperResult := upperFunc("hello")
	if upperResult != "HELLO" {
		t.Errorf("upper: Expected 'HELLO', but got %s", upperResult)
	}

	// Test lower function
	lowerFunc := tfm.Map()["lower"].(func(string) string)
	lowerResult := lowerFunc("HELLO")
	if lowerResult != "hello" {
		t.Errorf("lower: Expected 'hello', but got %s", lowerResult)
	}
}

// TestGetPredefinedFunctionInfo tests the GetPredefinedFunctionInfo function
func TestGetPredefinedFunctionInfo(t *testing.T) {
	info := GetPredefinedFunctionInfo()
	if len(info) == 0 {
		t.Error("GetPredefinedFunctionInfo returned empty slice")
	}

	expectedFuncs := map[string]bool{
		"slice": false,
		"upper": false,
		"lower": false,
	}

	for _, funcInfo := range info {
		if _, ok := expectedFuncs[funcInfo.Key]; ok {
			expectedFuncs[funcInfo.Key] = true
		}
	}

	for funcName, found := range expectedFuncs {
		if !found {
			t.Errorf("Expected predefined function %s not found", funcName)
		}
	}
}

func TestTemplateExecution(t *testing.T) {
	tfm := New()
	tmpl := template.New("test").Funcs(tfm.Map())

	tmpl, err := tmpl.Parse(`
        {{range slice 1 .Items}}
            {{upper .}}
        {{end}}
        {{lower .Text}}
    `)
	if err != nil {
		t.Fatalf("Failed to parse template: %v", err)
	}

	data := struct {
		Items []string
		Text  string
	}{
		Items: []string{"one", "two", "three", "four"},
		Text:  "HELLO WORLD",
	}

	var buf strings.Builder
	err = tmpl.Execute(&buf, data)
	if err != nil {
		t.Fatalf("Failed to execute template: %v", err)
	}

	expected := "\n            TWO\n        \n            THREE\n        \n            FOUR\n        \n        hello world\n    "
	if strings.TrimSpace(buf.String()) != strings.TrimSpace(expected) {
		t.Errorf("Template execution failed.\nExpected:\n%s\nGot:\n%s", expected, buf.String())
	}
}

// TestSliceEdgeCases tests edge cases for the slice function
func TestSliceEdgeCases(t *testing.T) {
	tfm := New()
	sliceFunc := tfm.Map()["slice"].(func(int, interface{}) interface{})

	// Test with non-slice input
	result := sliceFunc(0, 123)
	if result != nil {
		t.Errorf("Expected nil for non-slice input, got %v", result)
	}

	// Test with invalid start indices
	data := []int{1, 2, 3, 4}
	result = sliceFunc(10, data)
	if result != nil {
		t.Errorf("Expected nil for out of range index, got %v", result)
	}
	result = sliceFunc(-1, data)
	if result != nil {
		t.Errorf("Expected nil for negative index, got %v", result)
	}

	// Test boundary conditions
	data2 := []string{"a", "b", "c"}
	result = sliceFunc(3, data2)
	if result != nil {
		t.Errorf("Expected nil for start index equal to length, got %v", result)
	}
}

// TestOverwriteFunction tests overwriting an existing function
func TestOverwriteFunction(t *testing.T) {
	tfm := New()
	tfm.Add("exampleFunc", func(x int) int { return x * 2 })
	tfm.Add("exampleFunc", func(x int) int { return x * 3 })

	result := tfm.Map()["exampleFunc"].(func(int) int)(2)
	if result != 6 {
		t.Errorf("Expected 6 (last added function), got %d", result)
	}
}

// BenchmarkSliceFunction benchmarks the slice function
func BenchmarkSliceFunction(b *testing.B) {
	tfm := New()
	sliceFunc := tfm.Map()["slice"].(func(int, interface{}) interface{})
	data := make([]int, 1000)
	for i := 0; i < 1000; i++ {
		data[i] = i
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		sliceFunc(500, data)
	}
}

// BenchmarkUpperFunction benchmarks the upper function
func BenchmarkUpperFunction(b *testing.B) {
	tfm := New()
	upperFunc := tfm.Map()["upper"].(func(string) string)
	input := strings.Repeat("a", 1000)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		upperFunc(input)
	}
}

// TestComplexTemplateExecution tests a more complex template execution scenario
func TestComplexTemplateExecution(t *testing.T) {
	tfm := New()
	tmpl := template.New("test").Funcs(tfm.Map())

	tmpl, err := tmpl.Parse(`
        {{range $index, $element := slice 1 .Items}}
            {{if eq $index 0}}First: {{else}}Then: {{end}}{{upper $element}}
        {{end}}
        {{lower .Text}}
    `)
	if err != nil {
		t.Fatalf("Failed to parse template: %v", err)
	}

	data := struct {
		Items []string
		Text  string
	}{
		Items: []string{"one", "two", "three", "four"},
		Text:  "HELLO WORLD",
	}

	var buf strings.Builder
	err = tmpl.Execute(&buf, data)
	if err != nil {
		t.Fatalf("Failed to execute template: %v", err)
	}

	expected := "\n            First: TWO\n        \n            Then: THREE\n        \n            Then: FOUR\n        \n        hello world\n    "
	if strings.TrimSpace(buf.String()) != strings.TrimSpace(expected) {
		t.Errorf("Complex template execution failed.\nExpected:\n%s\nGot:\n%s", expected, buf.String())
	}
}
