package sysManager

import (
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"os"
	"path/filepath"
	"testing"
)

var tmpDir string
var localUser string
var sysManager *SysManager

func init() {
	doxlog.Init("", "", true)
	doxlog.Info("doxLog initialized")
}
func TestMain(m *testing.M) {
	// Setup operations
	setup()
	// Run the tests
	exitCode := m.Run()
	// Teardown operations
	teardown()
	// Exit with the test suite's exit code
	os.Exit(exitCode)
}
func setup() {
	var err error
	tmpDir, err = os.MkdirTemp("", "sysManager-")
	if err != nil {
		doxlog.Errorf(err.Error())
		return
	}
	dbPath := filepath.Join(tmpDir, ".doxa", ".sys.db")

	// open the database
	dsSystem, err := kvs.NewBoltKVS(dbPath)
	if err != nil {
		doxlog.Errorf(fmt.Sprintf("error creating database %s", dbPath), err)
		return
	}
	// create the database mapper
	var sysKvs *kvs.KVS
	sysKvs, err = kvs.NewKVS(dsSystem)
	if err != nil {
		doxlog.Errorf("error creating mapper for system database: %v\n", err)
		return
	}
	if sysKvs == nil {
		doxlog.Errorf("system kvs is nil")
		return
	}
	// create the context System database Manager
	sysPaths := new(Paths)
	sysPaths.KvsPath = dbPath
	sysPaths.DoxaHome = filepath.Join(tmpDir, "doxa")
	sysPaths.DoxaProjectGroupsPath = filepath.Join(sysPaths.DoxaHome, "projects")
	parms := new(Parms)
	parms.Kvs = sysKvs
	parms.Paths = sysPaths
	parms.InCloud = false
	sysManager, err = NewSysManager(parms)
	if err != nil {
		doxlog.Errorf("error creating sys manager: %v", err)
		return
	}
	if sysManager == nil {
		doxlog.Errorf("sysManager is nil")
	}

}
func createProjectGroups(groups []string) error {
	for _, group := range groups {
		err := os.MkdirAll(filepath.Join(sysManager.Paths.DoxaProjectGroupsPath, group), os.ModePerm)
		if err != nil {
			return fmt.Errorf("error creating project group %s directory: %v", group, err)
		}
	}
	return nil
}
func removeProjectGroups(groups []string) error {
	for _, group := range groups {
		err := os.Remove(filepath.Join(sysManager.Paths.DoxaProjectGroupsPath, group))
		if err != nil {
			return fmt.Errorf("error removing project group %s directory: %v", group, err)
		}
	}
	return nil
}
func teardown() {
	err := sysManager.kvs.Db.Close()
	if err != nil {
		doxlog.Error(err.Error())
	}

	// Remove the temporary directory
	err = os.RemoveAll(tmpDir)
	if err != nil {
		doxlog.Errorf(err.Error())
	}
}
func TestNewSysManager(t *testing.T) {
	if sysManager == nil {
		t.Error("sysManager is nil")
	}
}
func TestSysManager_ReconcileProjectGroups(t *testing.T) {
	projectGroups := []string{"doxa-liml", "doxa-mcolburn", "doxa-eac"}
	removeGroups := []string{"doxa-mcolburn"}
	err := createProjectGroups(projectGroups)
	if err != nil {
		doxlog.Errorf("error creating project groups: %v", err)
		t.Error(err)
		return
	}
	err = sysManager.ReconcileProjectGroups(localUser)
	if err != nil {
		doxlog.Errorf("error reconciling project groups: %v", err)
		t.Error(err)
	}
	var dbProjectGroups []*FsProjectGroup
	dbProjectGroups, err = sysManager.GetProjectGroups(localUser)
	if err != nil {
		t.Errorf("error getting project groups: %v", err)
	}
	if len(dbProjectGroups) != len(projectGroups) {
		t.Errorf("expected %d project groups, got %d", len(projectGroups), len(dbProjectGroups))
		return
	}
	for _, group := range projectGroups {
		found := false
		for _, dbGroup := range dbProjectGroups {
			if dbGroup.Name == group {
				found = true
				break
			}
		}
		if !found {
			t.Errorf("project group %s not found in database", group)
		}
	}
	// now remove a projectGroup
	err = removeProjectGroups(removeGroups)
	if err != nil {
		t.Errorf("error removing project groups: %v", err)
	}
	// reconcile again.
	err = sysManager.ReconcileProjectGroups(localUser)
	if err != nil {
		doxlog.Errorf("error reconciling project groups: %v", err)
		t.Error(err)
	}
	// expect that remove groups do not exist in db
	for _, group := range removeGroups {
		if sysManager.ExistsProjectGroup(localUser, group) {
			t.Errorf("project group %s still exists in database after removal", group)
		}
	}
	if !sysManager.ExistsRecordsFor(localUser) {
		t.Errorf("expected records for user %s, but none found", localUser)
	}
	err = sysManager.DeleteUserRecords(localUser)
	if err != nil {
		t.Errorf("error deleting user records: %v", err)
	}
	if sysManager.ExistsRecordsFor(localUser) {
		t.Errorf("expected no records for user %s", localUser)
	}
}
