package sysManager

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"path"
	"strconv"
	"sync"
)

const (
	usersDir                = "users"
	theProjectGroups        = "projectGroups"
	userCurrentProjectGroup = "currentProjectGroup"
	handlesGitKey           = "handlesGit"
	TempProjectGroup        = ".doxa-temp"
)

// sys db directory literals

const (
	Selected        = "selected"
	SelectedProject = "project"
	Projects        = "projects" // top level
	Groups          = "groups"
	Catalogs        = "catalogs"
	Settings        = "settings"
)

type SysManager struct {
	mutex   sync.Mutex
	kvs     *kvs.KVS
	Paths   *Paths
	InCloud bool
}

type Parms struct {
	Kvs     *kvs.KVS
	Paths   *Paths
	InCloud bool
}

func NewSysManager(parms *Parms) (*SysManager, error) {
	if parms.Kvs == nil {
		return nil, fmt.Errorf("kvs is nil")
	}
	if parms.Kvs.Db == nil {
		return nil, fmt.Errorf("kvs db is nil")
	}
	s := new(SysManager)
	s.kvs = parms.Kvs
	s.Paths = parms.Paths
	s.InCloud = parms.InCloud
	return s, nil
}

func (s *SysManager) SetDoxaHandlesGit(userName, doxaProjectName string, value bool) error {
	// create the record
	rec := kvs.NewDbR()
	rec.KP.Dirs.Push(usersDir)
	rec.KP.Dirs.Push(userName)
	rec.KP.Dirs.Push(doxaProjectName) // sometimes same as gitlab group name
	rec.KP.KeyParts.Push(handlesGitKey)
	rec.Value = fmt.Sprintf("%t", value)
	// write the record
	s.mutex.Lock()
	err := s.kvs.Db.Put(rec)
	s.mutex.Unlock()
	if err != nil {
		msg := fmt.Sprintf("failed to set doxa handles git repo at %s: %v", s.Paths.KvsPath, err)
		doxlog.Errorf(msg)
		return errors.New(msg)
	}
	return nil
}
func (s *SysManager) AddProjectGroup(userName string, project *FsProjectGroup) error {
	var err error
	if len(project.UserName) == 0 {
		project.UserName = userName
	}
	// create the record
	rec := kvs.NewDbR()
	rec.KP.Dirs.Push(usersDir)
	rec.KP.Dirs.Push(userName)
	rec.KP.Dirs.Push(theProjectGroups)
	rec.KP.KeyParts.Push(project.Name)
	rec.Value, err = project.ToJson()
	// write the record
	s.mutex.Lock()
	err = s.kvs.Db.Put(rec)
	s.mutex.Unlock()
	if err != nil {
		msg := fmt.Sprintf("failed to set doxa handles git repo at %s: %v", s.Paths.KvsPath, err)
		doxlog.Errorf(msg)
		return errors.New(msg)
	}
	doxlog.Infof("added project group at %s", s.Paths.KvsPath)
	return nil
}
func (s *SysManager) ExistsProjectGroup(userName string, projectName string) bool {
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(usersDir)
	kp.Dirs.Push(userName)
	kp.Dirs.Push(theProjectGroups)
	kp.KeyParts.Push(projectName)
	return s.kvs.Db.Exists(kp)
}

func (s *SysManager) GetProjectGroup(userName string, projectName string) (*FsProjectGroup, error) {
	var err error
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(usersDir)
	kp.Dirs.Push(userName)
	kp.Dirs.Push(theProjectGroups)
	kp.KeyParts.Push(projectName)
	// get the record
	var dbr *kvs.DbR
	dbr, err = s.kvs.Db.Get(kp)
	if err != nil {
		msg := fmt.Sprintf("failed to get project group %s]: %v", s.Paths.KvsPath, err)
		doxlog.Errorf(msg)
		return nil, errors.New(msg)
	}
	if dbr == nil {
		return nil, fmt.Errorf("not found %s", kp.Path())
	}
	project := FsProjectGroup{}
	err = json.Unmarshal([]byte(dbr.Value), &project)
	if err != nil {
		msg := fmt.Sprintf("failed to unmarshal project group at %s: %v", kp.Path(), err)
		doxlog.Errorf(msg)
		return nil, errors.New(msg)
	}
	return &project, nil
}
func (s *SysManager) GetProjectGroups(userName string) ([]*FsProjectGroup, error) {
	var err error
	matcher := kvs.NewMatcher()
	matcher.KP.Dirs.Push(usersDir)
	matcher.KP.Dirs.Push(userName)
	matcher.KP.Dirs.Push(theProjectGroups)
	matcher.Recursive = true
	recs, _, err := s.kvs.Db.GetMatching(*matcher)
	if err != nil {
		msg := fmt.Sprintf("error reading %s: %v", matcher.KP.Path(), err)
		doxlog.Errorf(msg)
		return nil, errors.New(msg)
	}
	var projects []*FsProjectGroup
	for _, rec := range recs {
		project := FsProjectGroup{}
		err = json.Unmarshal([]byte(rec.Value), &project)
		if err != nil {
			msg := fmt.Sprintf("failed to unmarshal project group at %s: %v", rec.KP.Path(), err)
			doxlog.Errorf(msg)
			return nil, errors.New(msg)
		}
		projects = append(projects, &project)
	}
	return projects, nil
}
func (s *SysManager) ExistsCurrentProjectGroup(userName string) bool {
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(usersDir)
	kp.Dirs.Push(userName)
	kp.KeyParts.Push(userCurrentProjectGroup)
	return s.kvs.Db.Exists(kp)
}
func (s *SysManager) ExistsRecordsFor(userName string) bool {
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(usersDir)
	kp.Dirs.Push(userName)
	return s.kvs.Db.Exists(kp)
}

func (s *SysManager) GetCurrentDoxaProjectGroup(userName string) (*FsProjectGroup, error) {
	var err error
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(usersDir)
	kp.Dirs.Push(userName)
	kp.KeyParts.Push(userCurrentProjectGroup)
	// get the record
	var dbr *kvs.DbR
	dbr, err = s.kvs.Db.Get(kp)
	var project *FsProjectGroup
	if err != nil || dbr == nil {
		project, err = s.addTempProjectGroup(userName)
		if err != nil {
			return nil, err
		}
	} else {
		err = json.Unmarshal([]byte(dbr.Value), &project)
		if err != nil {
			msg := fmt.Sprintf("failed to unmarshal project group at %s: %v", kp.Path(), err)
			doxlog.Errorf(msg)
			return nil, errors.New(msg)
		}
	}
	return project, nil
}

// SetCurrentDoxaProjectGroup sets the project as the current only being used by the user.
// If the project group does not exist, it is also added to the database under the user's groups
// directory.
func (s *SysManager) SetCurrentDoxaProjectGroup(userName string, project *FsProjectGroup) error {
	if len(project.UserName) == 0 {
		project.UserName = userName
	}
	if !s.ExistsProjectGroup(userName, project.Name) {
		err := s.AddProjectGroup(userName, project)
		if err != nil {
			msg := fmt.Sprintf("failed to add missing project group for user %s: %v", userName, err)
			doxlog.Errorf(msg)
			return errors.New(msg)
		}
	}
	// create the record
	rec := kvs.NewDbR()
	rec.KP.Dirs.Push(usersDir)
	rec.KP.Dirs.Push(userName)
	rec.KP.KeyParts.Push(userCurrentProjectGroup)
	theJson, err := project.ToJson()
	if err != nil {
		msg := fmt.Sprintf("failed to convert project to json for user %s: %v", userName, err)
		doxlog.Errorf(msg)
		return errors.New(msg)
	}
	rec.Value = theJson
	// write the record
	s.mutex.Lock()
	err = s.kvs.Db.Put(rec)
	s.mutex.Unlock()
	if err != nil {
		msg := fmt.Sprintf("sys database put failed for %s: %v", s.Paths.KvsPath, err)
		doxlog.Errorf(msg)
		return errors.New(msg)
	}
	doxlog.Infof("set current project group to %s", project.Path)
	return nil
}
func (s *SysManager) ReconcileProjectGroups(userName string) error {
	err := s.processFileSystemProjectGroups(userName)
	if err != nil {
		return err
	}
	err = s.processDbProjectGroups(userName)
	if err != nil {
		return err
	}
	// check to see if current user projectGroup is set.
	// If it is, make sure the projectGroup exists in the db.
	// If it does not, delete the current user project group.
	if s.ExistsCurrentProjectGroup(userName) {
		var currentProjectGroup *FsProjectGroup
		// get it
		currentProjectGroup, err = s.GetCurrentDoxaProjectGroup(userName)
		if err != nil {
			msg := fmt.Sprintf("failed to get current project group for user %s: %v", userName, err)
			doxlog.Errorf(msg)
			return errors.New(msg)
		}
		if currentProjectGroup != nil {
			// Now see if it is in the list of project groups for the user.
			// If not, delete the current project group because it is invalid.
			if !s.ExistsProjectGroup(userName, currentProjectGroup.Name) {
				err = s.DeleteCurrentProjectGroup(userName)
				if err != nil {
					msg := fmt.Sprintf("failed to delete current project group for user %s: %v", userName, err)
					doxlog.Errorf(msg)
					return errors.New(msg)
				}
				doxlog.Infof("deleted current project group for user %s because it does not exist.", userName)
				_, err = s.addTempProjectGroup(userName)
				if err != nil {
					return fmt.Errorf("failed to add project group for user %s: %v", userName, err)
				}
			}
		}
	} else {
		_, err = s.addTempProjectGroup(userName)
		if err != nil {
			return fmt.Errorf("failed to add project group for user %s: %v", userName, err)
		}
	}
	return nil
}
func (s *SysManager) addTempProjectGroup(userName string) (*FsProjectGroup, error) {
	pg := s.GetTempProjectGroup(userName)
	err := s.AddProjectGroup(userName, pg)
	if err != nil {
		msg := fmt.Sprintf("failed to add temp project group for user %s: %v", userName, err)
		doxlog.Errorf(msg)
		return nil, errors.New(msg)
	}
	doxlog.Infof("added temp current project group for user %s", userName)
	return pg, nil
}
func (s *SysManager) GetTempProjectGroup(userName string) *FsProjectGroup {
	// create a temp project group
	tempProjectGroup := &FsProjectGroup{
		Name:           userName,
		Path:           path.Join(s.Paths.DoxaProjectGroupsPath, TempProjectGroup),
		TempGroup:      true,
		GitEnabled:     false,
		DoxaManagesGit: false,
		GitlabGroup:    nil,
	}
	return tempProjectGroup
}

// processFileSystemProjectGroups reads the projects folder in doxa home,
// and checks to see if each project (aka projectGroup) exists in
// the sysDb.  If not, it adds it.
func (s *SysManager) processFileSystemProjectGroups(userName string) error {
	dirs, err := ltfile.DirsInDir(s.Paths.DoxaProjectGroupsPath, true)
	if err != nil {
		msg := fmt.Sprintf("failed to list doxa project groups at %s: %v", s.Paths.DoxaProjectGroupsPath, err)
		doxlog.Errorf(msg)
		return errors.New(msg)
	}
	for _, dir := range dirs {
		var group *FsProjectGroup
		if !s.ExistsProjectGroup(userName, dir) {
			group = &FsProjectGroup{
				Name:           dir,
				Path:           path.Join(s.Paths.DoxaProjectGroupsPath, dir),
				GitEnabled:     false,
				DoxaManagesGit: false,
				GitlabGroup:    nil,
			}
			err = s.AddProjectGroup(userName, group)
			if err != nil {
				msg := fmt.Sprintf("failed to add project group %s for user %s: %v", group.Name, userName, err)
				doxlog.Errorf(msg)
				return errors.New(msg)
			}
		}
	}
	return nil
}

// processDbProjectGroups reads from the sysDb, all projectGroups
// for the specified user.  It then checks to make sure each
// projectGroup has a corresponding filesystem projectGroup.
// If it does not, the projectGroup is deleted from sysDB.
func (s *SysManager) processDbProjectGroups(userName string) error {
	groups, err := s.GetProjectGroups(userName)
	if err != nil {
		return err
	}
	for _, group := range groups {
		if !s.existsFileSystemProjectGroup(group.Name) {
			err = s.DeleteProjectGroup(userName, group.Name)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (s *SysManager) existsFileSystemProjectGroup(groupName string) bool {
	return ltfile.DirExists(path.Join(s.Paths.DoxaProjectGroupsPath, groupName))
}
func (s *SysManager) DeleteUserRecords(userName string) error {
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(usersDir)
	kp.Dirs.Push(userName)
	err := s.kvs.Db.Delete(*kp)
	if err != nil {
		msg := fmt.Sprintf("failed to delete sysDb records for user %s: %v", kp.Path(), err)
		doxlog.Errorf(msg)
		return errors.New(msg)
	}
	return nil
}

func (s *SysManager) DeleteProjectGroup(userName, groupName string) error {
	if !s.ExistsProjectGroup(userName, groupName) {
		return fmt.Errorf("project group %s does not exist", groupName)
	}
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(usersDir)
	kp.Dirs.Push(userName)
	kp.Dirs.Push(theProjectGroups)
	kp.KeyParts.Push(groupName)
	err := s.kvs.Db.Delete(*kp)
	if err != nil {
		msg := fmt.Sprintf("failed to delete project group %s: %v", kp.Path(), err)
		doxlog.Errorf(msg)
		return errors.New(msg)
	}
	return nil
}
func (s *SysManager) DeleteCurrentProjectGroup(userName string) error {
	if !s.ExistsCurrentProjectGroup(userName) {
		return fmt.Errorf("current project group for user %s does not exist", userName)
	}
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(usersDir)
	kp.Dirs.Push(userName)
	kp.KeyParts.Push(userCurrentProjectGroup)
	err := s.kvs.Db.Delete(*kp)
	if err != nil {
		msg := fmt.Sprintf("failed to delete current project group %s: %v", kp.Path(), err)
		doxlog.Errorf(msg)
		return errors.New(msg)
	}
	return nil
}
func (s *SysManager) ExportAll(toPath string) error {
	// get all records
	matcher := kvs.NewMatcher()
	matcher.Recursive = true
	recs, _, err := s.kvs.Db.GetMatching(*matcher)
	if err != nil {
		msg := fmt.Sprintf("error reading %s: %v", matcher.KP.Path(), err)
		doxlog.Errorf(msg)
		return errors.New(msg)
	}
	// convert records to json
	var data []string
	for _, rec := range recs {
		recJson, err := json.Marshal(rec)
		if err != nil {
			msg := fmt.Sprintf("failed to marshal record at %s: %v", rec.KP.Path(), err)
			doxlog.Errorf(msg)
			return errors.New(msg)
		}
		recStr := string(recJson)
		data = append(data, recStr)
	}
	// write data to file
	err = ltfile.WriteLinesToFile(toPath, data)
	if err != nil {
		msg := fmt.Sprintf("failed to write data to file %s: %v", toPath, err)
		doxlog.Errorf(msg)
		return errors.New(msg)
	}
	return nil
}
func (s *SysManager) CreateTestProjectGroup(userName, groupName, groupPath string, gitlabGroup *GitlabGroup, setAsCurrent bool) error {
	pg := new(FsProjectGroup)
	pg.Name = groupName
	pg.Path = path.Join(s.Paths.DoxaProjectGroupsPath, groupPath)
	pg.UserName = userName
	pg.GitlabGroup = gitlabGroup
	err := s.AddProjectGroup(userName, pg)
	if err != nil {
		msg := fmt.Sprintf("failed to add test project group %s for user %s: %v", groupName, userName, err)
		doxlog.Errorf(msg)
		return errors.New(msg)
	}
	if setAsCurrent {
		err = s.SetCurrentDoxaProjectGroup(userName, pg)
		if err != nil {
			msg := fmt.Sprintf("failed to set current doxa project group for user %s: %v", userName, err)
			doxlog.Errorf(msg)
			return errors.New(msg)
		}
	}
	return nil
}

// CurrentProject looks at the appropriate field in the database, and returns current project as a string
// it differs from GetCurrentDoxaProjectGroup in that the former is general purpose, the latter is specialized for git
func (s *SysManager) CurrentProject() string {
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(Selected)
	kp.KeyParts.Push(SelectedProject)
	rec, err := s.kvs.Db.Get(kp)
	if err != nil {
		doxlog.Errorf("Could not get name of current project: %v", err)
		return ""
	}
	return rec.Value
}

func (s *SysManager) LoadCurrentProjectSettingByKey(category, key string) (string, bool) {
	hwy := kvs.NewKeyPath()
	hwy.Dirs.Push(Projects)
	hwy.Dirs.Push(s.CurrentProject())
	hwy.Dirs.Push(Settings)
	hwy.Dirs.Push(category)
	hwy.KeyParts.Push(key)
	if !s.kvs.Db.Exists(hwy) {
		return "", false
	}
	retVal, err := s.kvs.Db.Get(hwy)
	if err != nil {
		return err.Error(), false
	}
	return retVal.Value, true
}

func (s *SysManager) StoreCurrentProjectSettingByKey(category, key, value string) {
	hwy := kvs.NewKeyPath()
	hwy.Dirs.Push(Projects)
	hwy.Dirs.Push(s.CurrentProject())
	hwy.Dirs.Push(Settings)
	hwy.Dirs.Push(category)
	if !s.kvs.Db.Exists(hwy) {
		s.kvs.Db.MakeDir(hwy)
	}
	hwy.KeyParts.Push(key)
	storeMe := kvs.NewDbR()
	storeMe.KP = hwy.Copy()
	storeMe.Value = value
	s.kvs.Db.Put(storeMe)
}

func (s *SysManager) StoreSliceAtPath(kp *kvs.KeyPath, slice []string) {
	if s.kvs.Db.MakeDir(kp) != nil {
		return
	}
	for i, ent := range slice {
		kr := kvs.NewDbR()
		kr.KP = kp.Copy()
		kr.KP.KeyParts.Push(fmt.Sprintf("%d", i))
		kr.Value = ent
		s.kvs.Db.Put(kr)
	}
}

func (s *SysManager) LoadSliceFromPath(kp *kvs.KeyPath) []string {
	if indices, _, _, err := s.kvs.Db.Keys(kp); err == nil {
		retSlice := make([]string, len(indices))
		oopscount := 0
		for _, iKp := range indices {
			i, err := strconv.Atoi(iKp.Copy().KeyParts.Pop())
			if err != nil {
				oopscount++
				continue
			}
			kr, err := s.kvs.Db.Get(iKp)
			if err != nil {
				oopscount++
				continue
			}
			retSlice[i] = kr.Value
		}
		return retSlice[:len(retSlice)-oopscount]
	}
	return nil
}
