package sysManager

import "encoding/json"

type Paths struct {
	DoxaHome              string
	DoxaProjectGroupsPath string
	KvsPath               string
}

// FsProjectGroup aka file system project group.
// Each folder in doxa/projects is (theoretically) a Gitlab Group.
// However, if Doxa does not manage git for the user, then
// it is just a local group.
type FsProjectGroup struct {
	Name           string
	Path           string
	UserName       string
	TempGroup      bool // this means doxa started up without knowing what project group to use
	GitEnabled     bool
	DoxaManagesGit bool         // true if any Gitlab repo in the group is handled by Doxa
	GitlabGroup    *GitlabGroup // nil if DoxaManagesGit is false
}

func (l FsProjectGroup) ToJson() (string, error) {
	jsonStr, err := json.Marshal(l)
	if err != nil {
		return err.Error(), err
	}
	return string(jsonStr), nil
}

type GitlabGroup struct {
	Name          string
	ID            int
	CloneUrlHttps string
	CloneUrlSsh   string
}
