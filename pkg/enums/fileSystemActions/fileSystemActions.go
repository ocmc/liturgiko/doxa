// Package fileSystemActions
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate
//
//go:generate enumer -type=FileSystemAction -json -text -yaml -sql
package fileSystemActions

type FileSystemAction int

const (
	Copy FileSystemAction = iota
	Create
	Delete
	Read
	Rename
	Update
)
