//go:generate enumer -type=ExpressionType -json -text -yaml -sql
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate

// Package expressionTypes provides an enum of statuses for liturgical artifacts, e.g. translation, template
package expressionTypes

type ExpressionType int

const (
	NA ExpressionType = iota
	Date
	DayOfWeek
	Eothinon
	Exists
	LukanCycleDay
	ModeOfWeek
	MovableCycleDay
	Parenthetical
	Realm
	SundayAfterElevationOfCross
	SundaysBeforeTriodion
	Year
)
