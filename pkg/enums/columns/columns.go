//go:generate enumer -type=Column -json -text -yaml -sql
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate

// Package columns provides an enum of table columns (left, center, right)
package columns

import "fmt"

type Column int

const (
	Col1 Column = iota
	Col2
	Col3
)

func (c Column) Name() string {
	switch c {
	case Col1:
		return "col1"
	case Col2:
		return "col2"
	case Col3:
		return "col3"
	default:
		return "unknown"
	}
}
func TypeFor(i int) (Column, error) {
	switch i {
	case 1:
		return Col1, nil
	case 2:
		return Col2, nil
	case 3:
		return Col3, nil
	default:
		return -1, fmt.Errorf("%d invalid column number", i)
	}
}
