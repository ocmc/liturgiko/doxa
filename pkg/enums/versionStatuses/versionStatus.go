//go:generate enumer -type=VersionStatus -json -text -yaml -sql
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate

// Package versionStatuses provides an enum of possible version states for doxa
package versionStatuses

type VersionStatus int

const (
	Current VersionStatus = iota
	AheadOfLatest
	BehindLatest
)
func (v VersionStatus) Name() string {
	switch v {
	case AheadOfLatest:
		return "ahead of the latest public version"
	case BehindLatest:
		return "behind the latest public version"
	default:
		return "current"
	}
}
