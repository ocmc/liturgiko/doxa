// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate
//
//go:generate enumer -type=MediaType -json -text -yaml -sql
package mediaTypes

type MediaType int

const (
	NA MediaType = iota
	Audio
	HTML
	PDF
	ScoreByz
	ScoreByzE
	ScoreWestern
	Text
)

func TypeFor(t string) MediaType {
	switch t {
	case "na":
		return NA
	case "a":
		return Audio
	case "h":
		return HTML
	case "p":
		return PDF
	case "sb":
		return ScoreByz
	case "sbe":
		return ScoreByzE
	case "sw":
		return ScoreWestern
	case "txt":
		return Text
	default:
		return -1
	}
}
