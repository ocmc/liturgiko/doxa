//go:generate enumer -type=RowInclusionType -json -text -yaml
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate

// Package rowInclusionTypes provides an enum for whether content is to be included in a row
package rowInclusionTypes

type RowInclusionType int

const (
	Both RowInclusionType = iota
	HtmlOnly
	PdfOnly
)
