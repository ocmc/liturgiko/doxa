//go:generate enumer -type=CalendarType -json -text -yaml -sql
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate

// Package calendarTypes provides an enum of calendar types for liturgical services
package calendarTypes

import "strings"

type CalendarType int

const (
	Gregorian CalendarType = iota
	Julian
)

func ToType(cal string) CalendarType {
	switch strings.ToLower(cal) {
	case "julian":
		return Julian
	default:
		return Gregorian
	}
}
