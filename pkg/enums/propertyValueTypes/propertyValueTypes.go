//go:generate enumer -type=PropertyType -json -text -yaml -sql
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate

//Package propertyValueTypes provides an enum of Liturgical Books
package propertyValueTypes

// PropertyType is used to enumerate the Liturgical Books
type PropertyType int

const (
	Boolean PropertyType = iota
	Float
	Int
	String
	StringMapString
	StringSlice
)
