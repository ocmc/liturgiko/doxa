//go:generate enumer -type=TemplateSourceType -json -text -yaml -sql
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate
package templateSourceTypes

type TemplateSourceType int

const (
	DB TemplateSourceType = iota
	FILE
)
