// Code generated by "enumer -type=TemplateSourceType -json -text -yaml -sql"; DO NOT EDIT.

//
package templateSourceTypes

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
)

const _TemplateSourceTypeName = "DBFILE"

var _TemplateSourceTypeIndex = [...]uint8{0, 2, 6}

func (i TemplateSourceType) String() string {
	if i < 0 || i >= TemplateSourceType(len(_TemplateSourceTypeIndex)-1) {
		return fmt.Sprintf("TemplateSourceType(%d)", i)
	}
	return _TemplateSourceTypeName[_TemplateSourceTypeIndex[i]:_TemplateSourceTypeIndex[i+1]]
}

var _TemplateSourceTypeValues = []TemplateSourceType{0, 1}

var _TemplateSourceTypeNameToValueMap = map[string]TemplateSourceType{
	_TemplateSourceTypeName[0:2]: 0,
	_TemplateSourceTypeName[2:6]: 1,
}

// TemplateSourceTypeString retrieves an enum value from the enum constants string name.
// Throws an error if the param is not part of the enum.
func TemplateSourceTypeString(s string) (TemplateSourceType, error) {
	if val, ok := _TemplateSourceTypeNameToValueMap[s]; ok {
		return val, nil
	}
	return 0, fmt.Errorf("%s does not belong to TemplateSourceType values", s)
}

// TemplateSourceTypeValues returns all values of the enum
func TemplateSourceTypeValues() []TemplateSourceType {
	return _TemplateSourceTypeValues
}

// IsATemplateSourceType returns "true" if the value is listed in the enum definition. "false" otherwise
func (i TemplateSourceType) IsATemplateSourceType() bool {
	for _, v := range _TemplateSourceTypeValues {
		if i == v {
			return true
		}
	}
	return false
}

// MarshalJSON implements the json.Marshaler interface for TemplateSourceType
func (i TemplateSourceType) MarshalJSON() ([]byte, error) {
	return json.Marshal(i.String())
}

// UnmarshalJSON implements the json.Unmarshaler interface for TemplateSourceType
func (i *TemplateSourceType) UnmarshalJSON(data []byte) error {
	var s string
	if err := json.Unmarshal(data, &s); err != nil {
		return fmt.Errorf("TemplateSourceType should be a string, got %s", data)
	}

	var err error
	*i, err = TemplateSourceTypeString(s)
	return err
}

// MarshalText implements the encoding.TextMarshaler interface for TemplateSourceType
func (i TemplateSourceType) MarshalText() ([]byte, error) {
	return []byte(i.String()), nil
}

// UnmarshalText implements the encoding.TextUnmarshaler interface for TemplateSourceType
func (i *TemplateSourceType) UnmarshalText(text []byte) error {
	var err error
	*i, err = TemplateSourceTypeString(string(text))
	return err
}

// MarshalYAML implements a YAML Marshaler for TemplateSourceType
func (i TemplateSourceType) MarshalYAML() (interface{}, error) {
	return i.String(), nil
}

// UnmarshalYAML implements a YAML Unmarshaler for TemplateSourceType
func (i *TemplateSourceType) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var s string
	if err := unmarshal(&s); err != nil {
		return err
	}

	var err error
	*i, err = TemplateSourceTypeString(s)
	return err
}

func (i TemplateSourceType) Value() (driver.Value, error) {
	return i.String(), nil
}

func (i *TemplateSourceType) Scan(value interface{}) error {
	if value == nil {
		return nil
	}

	str, ok := value.(string)
	if !ok {
		bytes, ok := value.([]byte)
		if !ok {
			return fmt.Errorf("value is not a byte slice")
		}

		str = string(bytes[:])
	}

	val, err := TemplateSourceTypeString(str)
	if err != nil {
		return err
	}

	*i = val
	return nil
}
