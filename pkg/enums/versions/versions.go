//go:generate enumer -type=Version -json -text -yaml -sql
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate

// Package versions provides an enum of possible versions (columns) in generated book or service
package versions

type Version int

const (
	One Version = iota + 1
	Two
	Three
	All
)

func VersionFor(s string) Version {
	switch s {
	case "1":
		return One
	case "2":
		return Two
	case "3":
		return Three
	default:
		return All
	}
}
