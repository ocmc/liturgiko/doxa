//go:generate enumer -type=Command -json -text -yaml -sql
// If changes are made, open a termimal
// in the enum subfolder for this enum and
// issue the command: go generate

// Package commands provides an enum of Doxa commands
package commands

// Command is used to enumerate the doxa commands
type Command int

const (
	Home Command = iota
	AGES
	A2L
	All
	Build
	BuildIndex
	Change
	Database
	Day
	Eform
	Eval
	EvalLukanCycleDay
	EvalModeOfWeek
	EvalMovableCycleDay
	EvalDauOfDay
	EvalSundayAfterElevationOfCross
	EvalSundaysBeforeTriodion
	Exit
	Export
	Feasts
	Help
	Init
	Internet
	Interval
	Julian
	KbM // Knowledge-Base Manager
	Ldp
	Load
	Login
	Logout
	Model
	Month
	NetSim
	Pull
	Push
	Quit
	Restore
	Server
	SetPwd
	SetUid
	Start
	Status
	Stop
	RO
	ROGet
	ROInfo
	ROList
	ROConfig
	ROSubscribe
	ROUnsubscribe
	ROUpdates
	SyncSubscriptionsOn
	SyncSubscriptionsOff
	SyncSubscriptionsSim
	SyncSubscriptionsNow
	SyncUserMaintained
	SyncUserMaintainedOn
	SyncUserMaintainedOff
	SyncUserMaintainedSim
	SyncUserMaintainedNow
	SyncStatus
	Vault
	Version
	Year
)

func (i Command) Abr() string {
	switch i {
	case A2L:
		return "a2l"
	case AGES:
		return "ages"
	case All:
		return "all"
	case Build:
		return "build"
	case BuildIndex:
		return "build -index"
	case Change:
		return "change"
	case Database:
		return "db"
	case Day:
		return "day"
	case Eform:
		return "eform"
	case Eval:
		return "eval"
	case EvalLukanCycleDay:
		return "eval lukanCycleDay"
	case EvalModeOfWeek:
		return "eval modeOfWeek"
	case EvalMovableCycleDay:
		return "eval movableCycleDay"
	case EvalDauOfDay:
		return "eval dayOfWeek"
	case EvalSundayAfterElevationOfCross:
		return "eval sundayAfterElevationOfCross"
	case EvalSundaysBeforeTriodion:
		return "eval sundaysBeforeTriodion"
	case Exit:
		return "exit"
	case Export:
		return "export"
	case Feasts:
		return "feasts"
	case Help:
		return "help"
	case Home:
		return "doxa"
	case Init:
		return "init"
	case Internet:
		return "internet"
	case Interval:
		return "interval"
	case Julian:
		return "julian"
	case KbM:
		return "kbm"
	case Ldp:
		return "ldp"
	case Load:
		return "load"
	case Login:
		return "login"
	case Logout:
		return "logout"
	case Model:
		return "model"
	case Month:
		return "month"
	case NetSim:
		return "netSim"
	case Push:
		return "push"
	case Pull:
		return "pull"
	case Quit:
		return "quit"
	case SetPwd:
		return "pwd"
	case Server:
		return "server"
	case Start:
		return "start"
	case Status:
		return "status"
	case Stop:
		return "stop"
	case RO:
		return "ro"
	case ROGet:
		return "update"
	case ROConfig:
		return "config"
	case ROInfo:
		return "info"
	case ROList:
		return "list"
	case ROSubscribe:
		return "subscribe"
	case ROUnsubscribe:
		return "unsubscribe"
	case ROUpdates:
		return "updates"
	case SyncSubscriptionsOn:
		return "autoSync on"
	case SyncSubscriptionsOff:
		return "autoSync off"
	case SyncSubscriptionsSim:
		return "simulate"
	case SyncSubscriptionsNow:
		return "syncNow"
	case SyncUserMaintained:
		return "syncUserMaintained"
	case SyncUserMaintainedOn:
		return "on"
	case SyncUserMaintainedOff:
		return "off"
	case SyncUserMaintainedSim:
		return "simulate"
	case SyncUserMaintainedNow:
		return "syncNow"
	case SyncStatus:
		return "syncStatus"
	case SetUid:
		return "uid"
	case Restore:
		return "restore"
	case Vault:
		return "vault"
	case Version:
		return "version"
	case Year:
		return "year"
	default:
		return "unknown"
	}
}
func (i Command) DescShort() string {
	switch i {
	case A2L:
		return "convert atem files to lml files"
	case AGES:
		return "utilities to allow AGES files to be used by Doxa"
	case All:
		return "show all liturgical properties"
	case Build:
		return "build liturgical web site and index"
	case BuildIndex:
		return "build index only for liturgical web site"
	case Database:
		return "search database, create, read, update, delete records"
	case Day:
		return "day for which you want to view liturgical properties, e.g. day 30 month 1 year 2022"
	case Eform:
		return "create or use text entry form from a liturgical website"
	case Eval:
		return "evaluate an expression, e.g. modeOfWeek == 3 or month 12 day 25 eval ModeOfWeek == 3"
	case EvalLukanCycleDay:
		return "evaluate LukanCycleDay, e.g. eval lukanCycleDay == 30"
	case EvalModeOfWeek:
		return "evaluate ModeOfWeek, e.g. eval modeOfWeek == 3"
	case EvalMovableCycleDay:
		return "evaluate MovableCycleDay, e.g. eval movableCycleDay >= 20"
	case EvalDauOfDay:
		return "evaluate DayOfWeek, e.g. eval dayOfWeek == Sun"
	case EvalSundayAfterElevationOfCross:
		return "evaluate SundayAfterElevationOfCross, e.g. eval sundayAfterElevationOfCross == sep 20"
	case EvalSundaysBeforeTriodion:
		return "evaluate SundaysBeforeTriodion, e.g. eval sundaysBeforeTriodion == 5"
	case Exit:
		return "exit Doxa"
	case Export:
		return "export records within this directory to a file"
	case Feasts:
		return "view feast dates for current year. For another year do this: feasts year 2025"
	case Help:
		return "help"
	case Home:
		return "return to the Doxa home prompt"
	case Init:
		return "initialize liturgical website"
	case Internet:
		return "manage Internet settings"
	case Interval:
		return "interval"
	case Julian:
		return "use Julian calendar"
	case Ldp:
		return "display liturgical day properties"
	case Load:
		return "load records into the database"
	case Login:
		return "enter password to unlock your gitlab token for DOXA to use"
	case Logout:
		return "lock your gitlab token so DOXA can't use it"
	case Model:
		return "set the model library for evaluating rid expressions.  The default is gr_gr_cog."
	case Month:
		return "month for which you want to view liturgical properties"
	case NetSim:
		return "simulate offline/online behaviour"
	case Pull:
		return "pull site from gitlab"
	case Push:
		return "push site to gitlab"
	case Quit:
		return "exit Doxa"
	case Restore:
		return "restore Doxa to previous version"
	case SetPwd:
		return "store your gitlab password in an encrypted vault on your computer"
	case Server:
		return "view the web server port numbers"
	case Start:
		return "start the web server"
	case Status:
		return "view the status of "
	case Stop:
		return "stop the web server"
	case RO:
		return "manage subscription to read only materials"
	case ROGet:
		return "download files from the subscribed catalog"
	case ROInfo:
		return "view the name of the subscribed catalog and last update"
	case ROConfig:
		return "adjust ro settings"
	case ROList:
		return "list available catalogs"
	case ROSubscribe:
		return "subscribe to a catalog"
	case ROUnsubscribe:
		return "unsubscribe from the subscribed catalog"
	case ROUpdates:
		return "list what updates are available"
	case SyncSubscriptionsOn:
		return "turn subscription auto synchronizer on"
	case SyncSubscriptionsOff:
		return "turn subscription auto synchronizer off"
	case SyncSubscriptionsSim:
		return "toggle subscription synchronization simulator"
	case SyncSubscriptionsNow:
		return "run subscription synchronizer now"
	case SyncUserMaintained:
		return "manage user maintained synchronization"
	case SyncUserMaintainedOn:
		return "turn  user maintained synchronizer on"
	case SyncUserMaintainedOff:
		return "turn  user maintained synchronizer off"
	case SyncUserMaintainedSim:
		return "toggle  user maintained synchronization simulator"
	case SyncUserMaintainedNow:
		return "run  user maintained synchronizer now"
	case SyncStatus:
		return "view synchronizer status for both subscriptions and user maintained"
	case SetUid:
		return "store your gitlab user ID in an encrypted vault on your computer"
	case Vault:
		return "store gitlab token in local vault"
	case Version:
		return "view the Doxa version number"
	case Year:
		return "year for which you want to view liturgical properties"
	default:
		return "unknown"
	}
}
