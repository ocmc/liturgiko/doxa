//go:generate enumer -type=RowType -json -text -yaml
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate

// Package rowTypes provides an enum for types of rows
package rowTypes

type RowType int

const (
	Para RowType = iota
	BlankLine
	HorizontalRule
	SectionHeading1
	SectionHeading2
	SectionHeading3
	Image
	Media
	PageBreak
	EndDiv
)
