//go:generate enumer -type=PDFLib -json -text -yaml -sql
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate

// Package modes provides an enum of types of PDF libraries
package pdfLibs

type PDFLib int

const (
	GO PDFLib = iota
	LATEX
)
