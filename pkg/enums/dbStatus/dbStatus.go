//go:generate enumer -type=DbStatus -json -text -yaml -sql
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate

// Package dbStatus provides an enum of outcomes from attempting to read a database record
package dbStatus

type DbStatus int

const (
	NA DbStatus = iota
	NotFound
	NoValue
	OK
)
