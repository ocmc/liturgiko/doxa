//go:generate enumer -type=Property -json -text -yaml -sql
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate
// Whenever you add a new property, you have to add the path
// to both the PropertyForPath and Data methods.
// All default values are specified as strings.
// If the ValueType is StringSlice, the default value must use tilde
// to separate the string representation of the string slice.

// Package properties provides an enum of configuration properties.
// Note that there is also a map for title key-value pairs.
// The map does not use the enum, but by having it here,
// there is just one place to go for initializing properties.
// Note that some properties are designated as "description only".
// For such properties, they must not end with Desc.  If they do,
// when imported, boltdb will throw an error saying "incompatible value".
package properties

import (
	"errors"
	"fmt"
	"github.com/liturgiko/doxa/pkg/enums/calendarTypes"
	"github.com/liturgiko/doxa/pkg/enums/columns"
	"github.com/liturgiko/doxa/pkg/enums/propertyValueTypes"
	"strconv"
	"strings"
)

// Property is used to enumerate the Liturgical Books
type Property int

// property names are the path converted to camel case with slashes removed
// Don't forget to run go generate from command line if you update this!
const (
	Epistle    Property = iota // if you add to the layouts, update func LangComboProperties()
	Gospel                     // used in layouts
	Liturgical                 // used in layouts
	Prophet                    // used in layouts
	Psalter                    // used in layouts
	Site
	SiteBuildAssets
	SiteBuildAssetsCopyAssets
	SiteBuildAssetsCss
	SiteBuildAssetsJs
	SiteBuildCalendar
	SiteBuildFooter
	SiteBuildFooterEnabled
	SiteBuildFooterFacebookUrl
	SiteBuildFooterOwnerUrl
	SiteBuildFrames
	SiteBuildFramesScore
	SiteBuildFramesScoreAlerts
	SiteBuildFramesScoreAlertsDesc
	SiteBuildFramesScoreAlertsEnabled
	SiteBuildFramesScoreDonate
	SiteBuildFramesScoreDonateEnabled
	SiteBuildFramesScoreDonateText
	SiteBuildFramesScoreDonateTitle
	SiteBuildFramesScoreHeading
	SiteBuildFramesScoreParagraphs
	SiteBuildFramesScoreParagraphsEnabled
	SiteBuildFramesScoreParagraphsValues
	SiteBuildFramesScoreTwitter
	SiteBuildFramesScoreTwitterEnabled
	SiteBuildFramesScoreTwitterHandle
	SiteBuildFramesScoreTwitterUrl
	SiteBuildFramesTextSrc
	SiteBuildFramesUseFrames
	SiteBuildHome
	SiteBuildIndex
	SiteBuildIndexBooks
	SiteBuildIndexServices
	SiteBuildInterlinear
	SiteBuildLayouts
	SiteBuildLayoutsColumns
	SiteBuildLayoutsRows
	SiteBuildLayoutsSelected
	SiteBuildNavbar
	SiteBuildNavbarHasBooksIcon
	SiteBuildNavbarHasSearchIcon
	SiteBuildNavbarHasServicesIcon
	SiteBuildNavbarLogo
	SiteBuildNavbarLogoHeight
	SiteBuildNavbarLogoImage
	SiteBuildNavbarLogoWidth
	SiteBuildNavbarTitle
	SiteBuildNavbarShowColumnToggle
	SiteBuildNavbarShowNightDayToggle
	SiteBuildNavbarUseLogoForMenu
	SiteBuildNavbarUseTextInSideBarMenu
	SiteBuildNavbarSideBarMenuBooks
	SiteBuildNavbarSideBarMenuDecreaseFontSize
	SiteBuildNavbarSideBarMenuIncreaseFontSize
	SiteBuildNavbarSideBarMenuHelp
	SiteBuildNavbarSideBarMenuHome
	SiteBuildNavbarSideBarMenuSearch
	SiteBuildNavbarSideBarMenuServices
	SiteBuildNavbarSideBarMenuToggleFrames
	SiteBuildNavbarSideBarMenuToggleVersionOrientation
	SiteBuildNavbarSideBarMenuToggleVersions
	SiteBuildOutput
	SiteBuildOutputHtml
	SiteBuildOutputPdf
	SiteBuildPages
	SiteBuildPagesHelp
	SiteBuildPagesHelpIcons
	SiteBuildPagesHelpIconsAudio
	SiteBuildPagesHelpIconsBooks
	SiteBuildPagesHelpIconsBscore
	SiteBuildPagesHelpIconsCalendar
	SiteBuildPagesHelpIconsColumns
	SiteBuildPagesHelpIconsEscore
	SiteBuildPagesHelpIconsHelp
	SiteBuildPagesHelpIconsHome
	SiteBuildPagesHelpIconsInfo
	SiteBuildPagesHelpIconsIntro
	SiteBuildPagesHelpIconsLeft
	SiteBuildPagesHelpIconsMinus
	SiteBuildPagesHelpIconsMoon
	SiteBuildPagesHelpIconsPlus
	SiteBuildPagesHelpIconsRight
	SiteBuildPagesHelpIconsSearch
	SiteBuildPagesHelpIconsSun
	SiteBuildPagesHelpIconsTitle
	SiteBuildPagesHelpIconsWscore
	SiteBuildPagesHelpParagraphs
	SiteBuildPagesHelpTitle
	SiteBuildPagesHome
	SiteBuildPagesHomeCards
	SiteBuildPagesHomeCardsParentValueImageAlt
	SiteBuildPagesHomeCardsParentValueImageSource
	SiteBuildPagesHomeCardsBooks
	SiteBuildPagesHomeCardsBooksValueBody
	SiteBuildPagesHomeCardsBooksValueButtonText
	SiteBuildPagesHomeCardsBooksValueButtonUrl
	SiteBuildPagesHomeCardsBooksValueIcon
	SiteBuildPagesHomeCardsBooksValueImageAlt
	SiteBuildPagesHomeCardsBooksValueImageSource
	SiteBuildPagesHomeCardsBooksValueTitle
	SiteBuildPagesHomeCardsClosing
	SiteBuildPagesHomeCardsClosingValueBodyPrefix
	SiteBuildPagesHomeCardsClosingValueBodySuffix
	SiteBuildPagesHomeCardsClosingValueButtonText
	SiteBuildPagesHomeCardsClosingValueButtonUrl
	SiteBuildPagesHomeCardsClosingValueIcon
	SiteBuildPagesHomeCardsClosingValueImageAlt
	SiteBuildPagesHomeCardsClosingValueImageSource
	SiteBuildPagesHomeCardsClosingValueTitle
	SiteBuildPagesHomeCardsHelpPrefix
	SiteBuildPagesHomeCardsIntro
	SiteBuildPagesHomeCardsIntroValueBody
	SiteBuildPagesHomeCardsIntroValueButtonIcon
	SiteBuildPagesHomeCardsIntroValueButtonText
	SiteBuildPagesHomeCardsIntroValueButtonUrl
	SiteBuildPagesHomeCardsIntroValueImageAlt
	SiteBuildPagesHomeCardsIntroValueImageSource
	SiteBuildPagesHomeCardsIntroValueTitle
	SiteBuildPagesHomeCardsServices
	SiteBuildPagesHomeCardsServicesValueBody
	SiteBuildPagesHomeCardsServicesValueButtonText
	SiteBuildPagesHomeCardsServicesValueButtonUrl
	SiteBuildPagesHomeCardsServicesValueIcon
	SiteBuildPagesHomeCardsServicesValueImageAlt
	SiteBuildPagesHomeCardsServicesValueImageSource
	SiteBuildPagesHomeCardsServicesValueTitle
	SiteBuildPagesHomeIntro
	SiteBuildPagesHomeMetaDescription
	SiteBuildPagesHomeMetaOgDescription
	SiteBuildPagesHomeMetaOgTitle
	SiteBuildPagesHomeMetaOgUrl
	SiteBuildPagesHomeParagraphs
	SiteBuildPagesHomeTitle
	SiteBuildPagesIndex
	SiteBuildPagesIndexBooks
	SiteBuildPagesIndexBooksDescription
	SiteBuildPagesIndexBooksSearchPrompt
	SiteBuildPagesIndexBooksSubtitle
	SiteBuildPagesIndexBooksTabTitle
	SiteBuildPagesIndexBooksTitle
	SiteBuildPagesIndexCustom
	SiteBuildPagesIndexCustomDescription
	SiteBuildPagesIndexCustomSubtitle
	SiteBuildPagesIndexCustomTabTitle
	SiteBuildPagesIndexCustomTitle
	SiteBuildPagesIndexServices
	SiteBuildPagesIndexServicesDay
	SiteBuildPagesIndexServicesDayDescription
	SiteBuildPagesIndexServicesDaySubtitleDateFormat
	SiteBuildPagesIndexServicesDaySubtitlePrefix
	SiteBuildPagesIndexServicesDaySubtitleSuffix
	SiteBuildPagesIndexServicesDayTabTitle
	SiteBuildPagesIndexServicesDayTitle
	SiteBuildPagesIndexServicesMonth
	SiteBuildPagesIndexServicesMonthDescription
	SiteBuildPagesIndexServicesMonthSubtitle
	SiteBuildPagesIndexServicesMonthTabTitle
	SiteBuildPagesIndexServicesMonthTitle
	SiteBuildPagesTitles
	SiteBuildPdf
	SiteBuildPdfCoversOn
	SiteBuildPdfFonts
	SiteBuildPdfFontsDefaultBold
	SiteBuildPdfFontsDefaultBoldItalic
	SiteBuildPdfFontsDefaultDateFormat
	SiteBuildPdfFontsDefaultItalic
	SiteBuildPdfFontsDefaultKerningDivisor
	SiteBuildPdfFontsDefaultKerningOn
	SiteBuildPdfFontsDefaultLineBreakAdjuster
	SiteBuildPdfFontsDefaultMiddleColAdjuster
	SiteBuildPdfFontsDefaultRegular
	SiteBuildPdfFontSize
	SiteBuildPdfLib
	SiteBuildPdfPage
	SiteBuildPdfPageColumnsOn
	SiteBuildPdfPageFooterLineColor
	SiteBuildPdfPageFooterLineOn
	SiteBuildPdfPageFooterLineWidth
	SiteBuildPdfPageGutterLineColor
	SiteBuildPdfPageGutterLineOn
	SiteBuildPdfPageGutterLineWidth
	SiteBuildPdfPageGutterWidth
	SiteBuildPdfPageHeaderLineColor
	SiteBuildPdfPageHeaderLineOn
	SiteBuildPdfPageHeaderLineWidth
	SiteBuildPdfPageLineHeight
	SiteBuildPdfPageMarginBottomWidth
	SiteBuildPdfPageMarginCellWidth
	SiteBuildPdfPageMarginLeftWidth
	SiteBuildPdfPageMarginLineColor
	SiteBuildPdfPageMarginLineOn
	SiteBuildPdfPageMarginLineWidth
	SiteBuildPdfPageMarginRightWidth
	SiteBuildPdfPageMarginTopWidth
	SiteBuildPdfPaperSize
	SiteBuildPdfParagraphSpacing
	SiteBuildPregenDeleteHtmlDir
	SiteBuildPregenDeletePdfDir
	SiteBuildPostgenIndexForNav
	SiteBuildPostgenIndexForSearch
	SiteBuildPublic
	SiteBuildPublishPublicUrl
	SiteBuildPublishTestUrl
	SiteBuildRealm
	SiteBuildShowTime
	SiteBuildTemplatesDaysOfWeekSelected
	SiteBuildTemplatesChecking
	SiteBuildTemplatesCheckingRid
	SiteBuildTemplatesCheckingRidKey
	SiteBuildTemplatesCheckingRidValue
	SiteBuildTemplatesFallback
	SiteBuildTemplatesFallbackEnabled
	SiteBuildTemplatesFallbackDir
	SiteBuildTemplatesPatterns
	SiteBuildTemplatesPatternsSelected
	//	SiteBuildTemplatesFallbackPrimaryDir
	SiteBuildTemplatesStatusesSelected
	SiteBuildUseExternalTemplates
	SiteBuildRoot
	SiteBuildVersionPrefix
	SiteBuildVersionSuffix
	SiteBuildYear
	SysDbAutoBackupOn
	SiteMediaAudioUrl
	SiteMediaDir
	SiteMediaEnabled
	SiteMediaPdfUrl
	SiteMediaPdfViewer
	Sys
	SysBuildConfig
	SysBuildDesc
	SysBuildSite
	SysCmdAgesAresPath
	SysCmdAgesMediaPath
	SysGlory
	SysGloryGloryStrings
	SysGloryIndex
	SysGloryLanguages
	SysGloryUse
	SysRemoteMetaTemplate
	SysRemoteMetaTemplateCreate
	SysRemoteMetaTemplateUrls
	SysServer
	SysServerPortsApi
	SysServerPortsDoxa
	SysServerPortsMedia
	SysServerPortsMessages
	SysServerPortsPublicSite
	SysServerPortsTestSite
	SysShell
	SysShellDb
	SysShellDbConcordancePaddingP1
	SysShellDbConcordancePaddingP2
	SysShellDbConcordancePaddingP3
	SysShellDbConcordanceSort
	SysShellDbConcordanceWidth
	SysShellDbEditor
	SysShellDbFindEmpty
	SysShellDbFindExact
	SysShellDbFindWholeword
	SysShellDbHyphen
	SysShellDbHyphenKeepPunct
	SysShellDbHyphenKeepScansion
	SysShellDbHyphenPunctuationChars
	SysShellDbHyphenScansionChars
	SysShellDbShortPrompt
	SysShellDbShowHidden
	SysShellDbShowHints
	SysShellTty
	SysSynch
	SysSynchBtx
	SysSynchBtxPush
	SysSynchBtxReposEn_uk_kjv
	SysSynchBtxReposEn_uk_kjvPush
	SysSynchBtxReposEn_uk_webbe
	SysSynchBtxReposEn_uk_webbePush
	SysSynchBtxReposGr_gr_cog
	SysSynchBtxReposGr_gr_cogPush
	SysSynchLtx
	SysSynchLtxPush
	SysSynchLtxReposAlwbScripture
	SysSynchLtxReposEn_redirects_goarch
	SysSynchLtxReposEn_us_aana
	SysSynchLtxReposEn_uk_ware
	SysSynchLtxReposEn_uk_warePush
	SysSynchLtxReposEn_us_acook
	SysSynchLtxReposEn_us_acookPush
	SysSynchLtxReposEn_us_andronache
	SysSynchLtxReposEn_us_andronachePush
	SysSynchLtxReposEn_us_barrett
	SysSynchLtxReposEn_us_barrettPush
	SysSynchLtxReposEn_us_carroll
	SysSynchLtxReposEn_us_carrollPush
	SysSynchLtxReposEn_us_dedes
	SysSynchLtxReposEn_us_dedesPush
	SysSynchLtxReposEn_us_duvall
	SysSynchLtxReposEn_us_duvallPush
	SysSynchLtxReposEn_us_goa
	SysSynchLtxReposEn_us_goaDedes
	SysSynchLtxReposEn_us_goaDedesPush
	SysSynchLtxReposEn_us_goaPush
	SysSynchLtxReposEn_us_holycross
	SysSynchLtxReposEn_us_holycrossPush
	SysSynchLtxReposEn_us_houpos
	SysSynchLtxReposEn_us_houposPush
	SysSynchLtxReposEn_us_oca
	SysSynchLtxReposEn_us_ocaPush
	SysSynchLtxReposEn_us_public
	SysSynchLtxReposEn_us_publicPush
	SysSynchLtxReposEn_us_roumas
	SysSynchLtxReposEn_us_roumasPush
	SysSynchLtxReposEn_us_system
	SysSynchLtxReposEn_us_unknown
	SysSynchLtxReposEn_us_unknownPush
	SysSynchLtxReposGr_gr_cog
	SysSynchLtxReposGr_us_goa
	SysSynchLtxReposSwh_ke_oak
	SysSynchLtxReposFra_fr_oaf
	SysSynchLtxReposGr_redirects_goarch
	SysSynchLtxReposEn_us_mlgs
)

func PropertyForPath(path string) Property {
	if len(path) == 0 {
		return -1
	}
	switch path {
	case "/epistle":
		return Epistle
	case "/gospel":
		return Gospel
	case "/liturgical":
		return Liturgical
	case "/prophet":
		return Prophet
	case "/psalter":
		return Psalter
	case "/site":
		return Site
	case "/site/build/interlinear":
		return SiteBuildInterlinear
	case "/site/media/dir":
		return SiteMediaDir
	case "/site/media/enabled":
		return SiteMediaEnabled
	case "/site/media/audio/url":
		return SiteMediaAudioUrl
	case "/site/media/pdf/url":
		return SiteMediaPdfUrl
	case "/site/media/pdfViewer":
		return SiteMediaPdfViewer
	case "/sys/glory/index":
		return SysGloryIndex
	case "/sys/glory/languages":
		return SysGloryLanguages
	case "/site/build/assets":
		return SiteBuildAssets
	case "/site/build/assets/copy.assets":
		return SiteBuildAssetsCopyAssets
	case "/site/build/assets/css":
		return SiteBuildAssetsCss
	case "/site/build/assets/js":
		return SiteBuildAssetsJs
	case "/site/build/calendar":
		return SiteBuildCalendar
	case "/site/build/frames":
		return SiteBuildFrames
	case "/site/build/footer":
		return SiteBuildFooter
	case "/site/build/footer/enabled":
		return SiteBuildFooterEnabled
	case "/site/build/footer/facebook":
		return SiteBuildFooterFacebookUrl
	case "/site/build/footer/owner":
		return SiteBuildFooterOwnerUrl
	case "/site/build/home":
		return SiteBuildHome
	case "/site/build/navbar":
		return SiteBuildNavbar
	case "/site/build/navbar/hasBookIcon":
		return SiteBuildNavbarHasBooksIcon
	case "/site/build/navbar/hasSearchIcon":
		return SiteBuildNavbarHasSearchIcon
	case "/site/build/navbar/hasServicesIcon":
		return SiteBuildNavbarHasServicesIcon
	case "/site/build/navbar/logo":
		return SiteBuildNavbarLogo
	case "/site/build/navbar/logo/height":
		return SiteBuildNavbarLogoHeight
	case "/site/build/navbar/logo/image":
		return SiteBuildNavbarLogoImage
	case "/site/build/navbar/logo/width":
		return SiteBuildNavbarLogoWidth
	case "/site/build/navbar/title":
		return SiteBuildNavbarTitle
	case "/site/build/navbar/showColumnToggle":
		return SiteBuildNavbarShowColumnToggle
	case "/site/build/navbar/showNightDayToggle":
		return SiteBuildNavbarShowNightDayToggle
	case "/site/build/navbar/useLogoForMenu":
		return SiteBuildNavbarUseLogoForMenu
	case "/site/build/navbar/useTextInSideMenu":
		return SiteBuildNavbarUseTextInSideBarMenu
	case "/site/build/navbar/sideBarMenu/home":
		return SiteBuildNavbarSideBarMenuHome
	case "/site/build/navbar/sideBarMenu/books":
		return SiteBuildNavbarSideBarMenuBooks
	case "/site/build/navbar/sideBarMenu/text/size/decreased":
		return SiteBuildNavbarSideBarMenuDecreaseFontSize
	case "/site/build/navbar/sideBarMenu/text/size/increase":
		return SiteBuildNavbarSideBarMenuIncreaseFontSize
	case "/site/build/navbar/sideBarMenu/toggle/frames":
		return SiteBuildNavbarSideBarMenuToggleFrames
	case "/site/build/navbar/sideBarMenu/toggle/version/orientation":
		return SiteBuildNavbarSideBarMenuToggleVersionOrientation
	case "/site/build/navbar/sideBarMenu/services":
		return SiteBuildNavbarSideBarMenuServices
	case "/site/build/navbar/sideBarMenu/toggle/versions":
		return SiteBuildNavbarSideBarMenuToggleVersions
	case "/site/build/navbar/sideBarMenu/search":
		return SiteBuildNavbarSideBarMenuSearch
	case "/site/build/pdf":
		return SiteBuildPdf
	case "/site/build/pdf/covers/on":
		return SiteBuildPdfCoversOn
	case "/site/build/pdf/font/size":
		return SiteBuildPdfFontSize
	case "/site/build/pdf/fonts":
		return SiteBuildPdfFonts
	case "/site/build/pdf/fonts/default/date/format":
		return SiteBuildPdfFontsDefaultDateFormat
	case "/site/build/pdf/fonts/default/bold":
		return SiteBuildPdfFontsDefaultBold
	case "/site/build/pdf/fonts/default/boldItalic":
		return SiteBuildPdfFontsDefaultBoldItalic
	case "/site/build/pdf/fonts/default/italic":
		return SiteBuildPdfFontsDefaultItalic
	case "/site/build/pdf/fonts/default/regular":
		return SiteBuildPdfFontsDefaultRegular
	case "/site/build/pdf/fonts/default/lineBreakAdjuster":
		return SiteBuildPdfFontsDefaultLineBreakAdjuster
	case "/site/build/pdf/fonts/default/middleColAdjuster":
		return SiteBuildPdfFontsDefaultMiddleColAdjuster
	case "/site/build/pdf/fonts/default/kerning/on":
		return SiteBuildPdfFontsDefaultKerningOn
	case "/site/build/pdf/fonts/default/kerning/divisor":
		return SiteBuildPdfFontsDefaultKerningDivisor
	case "/site/build/pdf/lib":
		return SiteBuildPdfLib
	case "/site/build/pdf/page":
		return SiteBuildPdfPage
	case "/site/build/pdf/page/columns/on":
		return SiteBuildPdfPageColumnsOn
	case "/site/build/pdf/page/footer/line/color":
		return SiteBuildPdfPageFooterLineColor
	case "/site/build/pdf/page/footer/line/on":
		return SiteBuildPdfPageFooterLineOn
	case "/site/build/pdf/page/footer/line/width":
		return SiteBuildPdfPageFooterLineWidth
	case "/site/build/pdf/page/gutter/line/color":
		return SiteBuildPdfPageGutterLineColor
	case "/site/build/pdf/page/gutter/line/on":
		return SiteBuildPdfPageGutterLineOn
	case "/site/build/pdf/page/gutter/line/width":
		return SiteBuildPdfPageGutterLineWidth
	case "/site/build/pdf/page/gutter/width":
		return SiteBuildPdfPageGutterWidth
	case "/site/build/pdf/page/header/line/color":
		return SiteBuildPdfPageHeaderLineColor
	case "/site/build/pdf/page/header/line/on":
		return SiteBuildPdfPageHeaderLineOn
	case "/site/build/pdf/page/header/line/width":
		return SiteBuildPdfPageHeaderLineWidth
	case "/site/build/pdf/page/line/height":
		return SiteBuildPdfPageLineHeight
	case "/site/build/pdf/page/margins/bottom/width":
		return SiteBuildPdfPageMarginBottomWidth
	case "/site/build/pdf/page/margins/cell/width":
		return SiteBuildPdfPageMarginCellWidth
	case "/site/build/pdf/page/margins/left/width":
		return SiteBuildPdfPageMarginLeftWidth
	case "/site/build/pdf/page/margins/line/color":
		return SiteBuildPdfPageMarginLineColor
	case "/site/build/pdf/page/margins/line/on":
		return SiteBuildPdfPageMarginLineOn
	case "/site/build/pdf/page/margins/line/width":
		return SiteBuildPdfPageMarginLineWidth
	case "/site/build/pdf/page/margins/right/width":
		return SiteBuildPdfPageMarginRightWidth
	case "/site/build/pdf/page/margins/top/width":
		return SiteBuildPdfPageMarginTopWidth
	case "/site/build/pdf/paper/size":
		return SiteBuildPdfPaperSize
	case "/site/build/pdf/paragraph/spacing":
		return SiteBuildPdfParagraphSpacing
	case "/site/build/frames/text/src":
		return SiteBuildFramesTextSrc
	case "/site/build/frames/score":
		return SiteBuildFramesScore
	case "/site/build/frames/score/alerts":
		return SiteBuildFramesScoreAlerts
	case "/site/build/frames/score/alerts/desc":
		return SiteBuildFramesScoreAlertsDesc
	case "/site/build/frames/score/alerts/enabled":
		return SiteBuildFramesScoreAlertsEnabled
	case "/site/build/frames/score/alerts/values":
		return SiteBuildFramesScoreAlertsDesc
	case "/site/build/frames/score/donate":
		return SiteBuildFramesScoreDonate
	case "/site/build/frames/score/donate/enabled":
		return SiteBuildFramesScoreDonateEnabled
	case "/site/build/frames/score/donate/text":
		return SiteBuildFramesScoreDonateText
	case "/site/build/frames/score/donate/title":
		return SiteBuildFramesScoreDonateTitle
	case "/site/build/frames/score/heading":
		return SiteBuildFramesScoreHeading
	case "/site/build/frames/score/paragraphs":
		return SiteBuildFramesScoreParagraphs
	case "/site/build/frames/score/paragraphs/enabled":
		return SiteBuildFramesScoreParagraphsEnabled
	case "/site/build/frames/score/paragraphs/values":
		return SiteBuildFramesScoreParagraphsValues
	case "/site/build/frames/score/twitter":
		return SiteBuildFramesScoreTwitter
	case "/site/build/frames/score/twitter/enabled":
		return SiteBuildFramesScoreTwitterEnabled
	case "/site/build/frames/score/twitter/handle":
		return SiteBuildFramesScoreTwitterHandle
	case "/site/build/frames/score/twitter/url":
		return SiteBuildFramesScoreTwitterUrl
	case "/site/build/frames/use.frames":
		return SiteBuildFramesUseFrames
	case "/site/build/index":
		return SiteBuildIndex
	case "/site/build/index/books":
		return SiteBuildIndexBooks
	case "/site/build/index/services":
		return SiteBuildIndexServices
	case "/site/build/layouts":
		return SiteBuildLayouts
	case "/site/build/layouts/columns":
		return SiteBuildLayoutsColumns
	case "/site/build/layouts/rows":
		return SiteBuildLayoutsRows
	case "/site/build/layouts/selected":
		return SiteBuildLayoutsSelected
	case "/site/build/output":
		return SiteBuildOutput
	case "/site/build/output/html":
		return SiteBuildOutputHtml
	case "/site/build/output/pdf":
		return SiteBuildOutputPdf
	case "/site/build/pages":
		return SiteBuildPages
	case "/site/build/pages/help":
		return SiteBuildPagesHelp
	case "/site/build/pages/help/icons":
		return SiteBuildPagesHelpIcons
	case "/site/build/pages/help/icons/audio":
		return SiteBuildPagesHelpIconsAudio
	case "/site/build/pages/help/icons/books":
		return SiteBuildPagesHelpIconsBooks
	case "/site/build/pages/help/icons/bscore":
		return SiteBuildPagesHelpIconsBscore
	case "/site/build/pages/help/icons/calendar":
		return SiteBuildPagesHelpIconsCalendar
	case "/site/build/pages/help/icons/columns":
		return SiteBuildPagesHelpIconsColumns
	case "/site/build/pages/help/icons/escore":
		return SiteBuildPagesHelpIconsEscore
	case "/site/build/pages/help/icons/help":
		return SiteBuildPagesHelpIconsHelp
	case "/site/build/pages/help/icons/home":
		return SiteBuildPagesHelpIconsHome
	case "/site/build/pages/help/icons/info":
		return SiteBuildPagesHelpIconsInfo
	case "/site/build/pages/help/icons/intro":
		return SiteBuildPagesHelpIconsIntro
	case "/site/build/pages/help/icons/left":
		return SiteBuildPagesHelpIconsLeft
	case "/site/build/pages/help/icons/minus":
		return SiteBuildPagesHelpIconsMinus
	case "/site/build/pages/help/icons/moon":
		return SiteBuildPagesHelpIconsMoon
	case "/site/build/pages/help/icons/plus":
		return SiteBuildPagesHelpIconsPlus
	case "/site/build/pages/help/icons/right":
		return SiteBuildPagesHelpIconsRight
	case "/site/build/pages/help/icons/search":
		return SiteBuildPagesHelpIconsSearch
	case "/site/build/pages/help/icons/sun":
		return SiteBuildPagesHelpIconsSun
	case "/site/build/pages/help/icons/title":
		return SiteBuildPagesHelpIconsTitle
	case "/site/build/pages/help/icons/wscore":
		return SiteBuildPagesHelpIconsWscore
	case "/site/build/pages/help/paragraphs":
		return SiteBuildPagesHelpParagraphs
	case "/site/build/pages/help/title":
		return SiteBuildPagesHelpTitle
	case "/site/build/pages/home":
		return SiteBuildPagesHome
	case "/site/build/pages/home/cards":
		return SiteBuildPagesHomeCards
	case "/site/build/pages/home/cards/parent/value/imageAlt":
		return SiteBuildPagesHomeCardsParentValueImageAlt
	case "/site/build/pages/home/cards/parent/value/imageSource":
		return SiteBuildPagesHomeCardsParentValueImageSource
	case "/site/build/pages/home/cards/books":
		return SiteBuildPagesHomeCardsBooks
	case "/site/build/pages/home/cards/books/value/body":
		return SiteBuildPagesHomeCardsBooksValueBody
	case "/site/build/pages/home/cards/books/value/buttonText":
		return SiteBuildPagesHomeCardsBooksValueButtonText
	case "/site/build/pages/home/cards/books/value/buttonUrl":
		return SiteBuildPagesHomeCardsBooksValueButtonUrl
	case "/site/build/pages/home/cards/books/value/icon":
		return SiteBuildPagesHomeCardsBooksValueIcon
	case "/site/build/pages/home/cards/books/value/imageAlt":
		return SiteBuildPagesHomeCardsBooksValueImageAlt
	case "/site/build/pages/home/cards/books/value/imageSource":
		return SiteBuildPagesHomeCardsBooksValueImageSource
	case "/site/build/pages/home/cards/books/value/title":
		return SiteBuildPagesHomeCardsBooksValueTitle
	case "/site/build/pages/home/cards/closing":
		return SiteBuildPagesHomeCardsClosing
	case "/site/build/pages/home/cards/closing/value/body/prefix":
		return SiteBuildPagesHomeCardsClosingValueBodyPrefix
	case "/site/build/pages/home/cards/closing/value/buttonText":
		return SiteBuildPagesHomeCardsClosingValueButtonText
	case "/site/build/pages/home/cards/closing/value/body/suffix":
		return SiteBuildPagesHomeCardsClosingValueBodySuffix
	case "/site/build/pages/home/cards/closing/value/buttonUrl":
		return SiteBuildPagesHomeCardsClosingValueButtonUrl
	case "/site/build/pages/home/cards/closing/value/icon":
		return SiteBuildPagesHomeCardsClosingValueIcon
	case "/site/build/pages/home/cards/closing/value/imageAlt":
		return SiteBuildPagesHomeCardsClosingValueImageAlt
	case "/site/build/pages/home/cards/closing/value/imageSource":
		return SiteBuildPagesHomeCardsClosingValueImageSource
	case "/site/build/pages/home/cards/closing/value/title":
		return SiteBuildPagesHomeCardsClosingValueTitle
	case "/site/build/pages/home/cards/intro":
		return SiteBuildPagesHomeCardsIntro
	case "/site/build/pages/home/cards/intro/value/body":
		return SiteBuildPagesHomeCardsIntroValueBody
	case "/site/build/pages/home/cards/intro/value/buttonText":
		return SiteBuildPagesHomeCardsIntroValueButtonText
	case "/site/build/pages/home/cards/intro/value/buttonUrl":
		return SiteBuildPagesHomeCardsIntroValueButtonUrl
	case "/site/build/pages/home/cards/intro/value/buttonIcon":
		return SiteBuildPagesHomeCardsIntroValueButtonIcon
	case "/site/build/pages/home/cards/intro/value/imageAlt":
		return SiteBuildPagesHomeCardsIntroValueImageAlt
	case "/site/build/pages/home/cards/intro/value/imageSource":
		return SiteBuildPagesHomeCardsIntroValueImageSource
	case "/site/build/pages/home/cards/intro/value/title":
		return SiteBuildPagesHomeCardsIntroValueTitle
	case "/site/build/pages/home/cards/help/prefix":
		return SiteBuildPagesHomeCardsHelpPrefix
	case "/site/build/pages/home/cards/services":
		return SiteBuildPagesHomeCardsServices
	case "/site/build/pages/home/cards/services/value/body":
		return SiteBuildPagesHomeCardsServicesValueBody
	case "/site/build/pages/home/cards/services/value/buttonText":
		return SiteBuildPagesHomeCardsServicesValueButtonText
	case "/site/build/pages/home/cards/services/value/buttonUrl":
		return SiteBuildPagesHomeCardsServicesValueButtonUrl
	case "/site/build/pages/home/cards/services/value/icon":
		return SiteBuildPagesHomeCardsServicesValueIcon
	case "/site/build/pages/home/cards/services/value/imageAlt":
		return SiteBuildPagesHomeCardsServicesValueImageAlt
	case "/site/build/pages/home/cards/services/value/imageSource":
		return SiteBuildPagesHomeCardsServicesValueImageSource
	case "/site/build/pages/home/cards/services/value/title":
		return SiteBuildPagesHomeCardsServicesValueTitle
	case "/site/build/pages/home/intro":
		return SiteBuildPagesHomeIntro
	case "/site/build/pages/home/meta/description":
		return SiteBuildPagesHomeMetaDescription
	case "/site/build/pages/home/meta/og.description":
		return SiteBuildPagesHomeMetaOgDescription
	case "/site/build/pages/home/meta/og.title":
		return SiteBuildPagesHomeMetaOgTitle
	case "/site/build/pages/home/meta/og.url":
		return SiteBuildPagesHomeMetaOgUrl
	case "/site/build/pages/home/paragraphs":
		return SiteBuildPagesHomeParagraphs
	case "/site/build/pages/home/title":
		return SiteBuildPagesHomeTitle
	case "/site/build/pages/index":
		return SiteBuildPagesIndex
	case "/site/build/pages/index/books":
		return SiteBuildPagesIndexBooks
	case "/site/build/pages/index/books/description":
		return SiteBuildPagesIndexBooksDescription
	case "/site/build/pages/index/books/searchPrompt":
		return SiteBuildPagesIndexBooksSearchPrompt
	case "/site/build/pages/index/books/subtitle":
		return SiteBuildPagesIndexBooksSubtitle
	case "/site/build/pages/index/books/tabTitle":
		return SiteBuildPagesIndexBooksTabTitle
	case "/site/build/pages/index/books/title":
		return SiteBuildPagesIndexBooksTitle
	case "/site/build/pages/index/custom":
		return SiteBuildPagesIndexCustom
	case "/site/build/pages/index/custom/description":
		return SiteBuildPagesIndexCustomDescription
	case "/site/build/pages/index/custom/subtitle":
		return SiteBuildPagesIndexCustomSubtitle
	case "/site/build/pages/index/custom/tabTitle":
		return SiteBuildPagesIndexCustomTabTitle
	case "/site/build/pages/index/custom/title":
		return SiteBuildPagesIndexCustomTitle
	case "/site/build/pages/index/services":
		return SiteBuildPagesIndexServices
	case "/site/build/pages/index/services/day":
		return SiteBuildPagesIndexServicesDay
	case "/site/build/pages/index/services/day/description":
		return SiteBuildPagesIndexServicesDayDescription
	case "/site/build/pages/index/services/day/subtitle/date.format":
		return SiteBuildPagesIndexServicesDaySubtitleDateFormat
	case "/site/build/pages/index/services/day/subtitle/prefix":
		return SiteBuildPagesIndexServicesDaySubtitlePrefix
	case "/site/build/pages/index/services/day/subtitle/suffix":
		return SiteBuildPagesIndexServicesDaySubtitleSuffix
	case "/site/build/pages/index/services/day/tabTitle":
		return SiteBuildPagesIndexServicesDayTabTitle
	case "/site/build/pages/index/services/day/title":
		return SiteBuildPagesIndexServicesDayTitle
	case "/site/build/pages/index/services/month":
		return SiteBuildPagesIndexServicesMonth
	case "/site/build/pages/index/services/month/description":
		return SiteBuildPagesIndexServicesMonthDescription
	case "/site/build/pages/index/services/month/subtitle":
		return SiteBuildPagesIndexServicesMonthSubtitle
	case "/site/build/pages/index/services/month/tabTitle":
		return SiteBuildPagesIndexServicesMonthTabTitle
	case "/site/build/pages/index/services/month/title":
		return SiteBuildPagesIndexServicesMonthTitle
	case "/site/build/pregen/deleteHtmlDir":
		return SiteBuildPregenDeleteHtmlDir
	case "/site/build/pregen/deletePdfDir":
		return SiteBuildPregenDeletePdfDir
	case "/site/build/postgen/index/forNav":
		return SiteBuildPostgenIndexForNav
	case "/site/build/postgen/index/forSearch":
		return SiteBuildPostgenIndexForSearch
	case "/site/build/pages/titles":
		return SiteBuildPagesTitles
	case "/site/build/publish/public/url":
		return SiteBuildPublishPublicUrl
	case "/site/build/realm":
		return SiteBuildRealm
	case "/site/build/root":
		return SiteBuildRoot
	case "/site/build/public":
		return SiteBuildPublic
	case "/site/build/show.time":
		return SiteBuildShowTime
	case "/site/build/templates/daysOfWeek/selected":
		return SiteBuildTemplatesDaysOfWeekSelected
	case "/site/build/templates/checking":
		return SiteBuildTemplatesChecking
	case "/site/build/templates/checking/rid":
		return SiteBuildTemplatesCheckingRid
	case "/site/build/templates/checking/rid/key":
		return SiteBuildTemplatesCheckingRidKey
	case "/site/build/templates/checking/rid/value":
		return SiteBuildTemplatesCheckingRidValue
	case "/site/build/templates/fallback":
		return SiteBuildTemplatesFallback
	case "/site/build/templates/fallback/enabled":
		return SiteBuildTemplatesFallbackEnabled
	case "/site/build/templates/fallback/dir":
		return SiteBuildTemplatesFallbackDir
	case "/site/build/templates/patterns":
		return SiteBuildTemplatesPatterns
	case "/site/build/templates/patterns/selected":
		return SiteBuildTemplatesPatternsSelected
	//case "/site/build/templates/fallback/primaryDir":
	//	return SiteBuildTemplatesFallbackPrimaryDir
	case "/site/build/templates/external":
		return SiteBuildUseExternalTemplates
	case "/site/build/templates/statuses/selected":
		return SiteBuildTemplatesStatusesSelected
	case "/site/build/publish/test/url":
		return SiteBuildPublishTestUrl
	case "/site/build/version.prefix":
		return SiteBuildVersionPrefix
	case "/site/build/version.suffix":
		return SiteBuildVersionSuffix
	case "/site/build/year":
		return SiteBuildYear
	case "/sys":
		return Sys
	case "/sys/build/desc":
		return SysBuildDesc
	case "/sys/build/config":
		return SysBuildConfig
	case "/sys/build/site":
		return SysBuildSite
	case "/sys/db/autoBackupOn":
		return SysDbAutoBackupOn
	case "/sys/glory":
		return SysGlory
	case "/sys/glory/gloryStrings":
		return SysGloryGloryStrings
	case "/sys/glory/use":
		return SysGloryUse
	case "/sys/glory/values":
		return SysGlory
	case "/sys/remote/meta/template":
		return SysRemoteMetaTemplate
	case "/sys/remote/meta/template/create":
		return SysRemoteMetaTemplateCreate
	case "/sys/remote/meta/template/urls":
		return SysRemoteMetaTemplateUrls
	case "/sys/server":
		return SysServer
	case "/sys/server/ports/api":
		return SysServerPortsApi
	case "/sys/server/ports/doxa":
		return SysServerPortsDoxa
	case "/sys/server/ports/media":
		return SysServerPortsMedia
	case "/sys/server/ports/messages":
		return SysServerPortsMessages
	case "/sys/server/ports/site/public":
		return SysServerPortsPublicSite
	case "/sys/server/ports/site/test":
		return SysServerPortsTestSite
	case "/sys/shell":
		return SysShell
	case "/sys/cmd/ages/ares/path":
		return SysCmdAgesAresPath
	case "/sys/cmd/ages/media/path":
		return SysCmdAgesMediaPath
	case "/sys/shell/db":
		return SysShellDb
	case "/sys/shell/db/concordance/Padding/P1":
		return SysShellDbConcordancePaddingP1
	case "/sys/shell/db/concordance/Padding/P2":
		return SysShellDbConcordancePaddingP2
	case "/sys/shell/db/concordance/Padding/P3":
		return SysShellDbConcordancePaddingP3
	case "/sys/shell/db/concordance/sort":
		return SysShellDbConcordanceSort
	case "/sys/shell/db/concordance/width":
		return SysShellDbConcordanceWidth
	case "/sys/shell/db/editor":
		return SysShellDbEditor
	case "/sys/shell/db/find/empty":
		return SysShellDbFindEmpty
	case "/sys/shell/db/find/exact":
		return SysShellDbFindExact
	case "/sys/shell/db/find/wholeword":
		return SysShellDbFindWholeword
	case "/sys/shell/db/hyphen":
		return SysShellDbHyphen
	case "/sys/shell/db/hyphen/punctuationChars":
		return SysShellDbHyphenPunctuationChars
	case "/sys/shell/db/hyphen/scansionChars":
		return SysShellDbHyphenScansionChars
	case "/sys/shell/db/hyphen/keepPunctuation":
		return SysShellDbHyphenKeepPunct
	case "/sys/shell/db/hyphen/keepScansion":
		return SysShellDbHyphenKeepScansion
	case "/sys/shell/db/shortPrompt":
		return SysShellDbShortPrompt
	case "/sys/shell/db/showHidden":
		return SysShellDbShowHidden
	case "/sys/shell/db/showHints":
		return SysShellDbShowHints
	case "/sys/shell/tty":
		return SysShellTty
	case "/sys/synch":
		return SysSynch
	case "/sys/synch/btx":
		return SysSynchBtx
	case "/sys/synch/btx/push":
		return SysSynchBtxPush
	case "/sys/synch/btx/repos/en_uk_kjv":
		return SysSynchBtxReposEn_uk_kjv
	case "/sys/synch/btx/repos/en_uk_kjv/push":
		return SysSynchBtxReposEn_uk_kjvPush
	case "/sys/synch/btx/repos/en_uk_webbe":
		return SysSynchBtxReposEn_uk_webbe
	case "/sys/synch/btx/repos/en_uk_webbe/push":
		return SysSynchBtxReposEn_uk_webbePush
	case "/sys/synch/btx/repos/gr_gr_cog":
		return SysSynchBtxReposGr_gr_cog
	case "/sys/synch/btx/repos/gr_gr_cog/push":
		return SysSynchBtxReposGr_gr_cogPush
	case "/sys/synch/ltx":
		return SysSynchLtx
	case "/sys/synch/ltx/push":
		return SysSynchLtxPush
	case "/sys/synch/ltx/repos/en_redirects_goarch":
		return SysSynchLtxReposEn_redirects_goarch
	case "/sys/synch/ltx/repos/en_us_alwbScripture":
		return SysSynchLtxReposAlwbScripture
	case "/sys/synch/ltx/repos/en_us_aana":
		return SysSynchLtxReposEn_us_aana
	case "/sys/synch/ltx/repos/en_uk_ware":
		return SysSynchLtxReposEn_uk_ware
	case "/sys/synch/ltx/repos/en_uk_ware/push":
		return SysSynchLtxReposEn_uk_warePush
	case "/sys/synch/ltx/repos/en_us_acook":
		return SysSynchLtxReposEn_us_acook
	case "/sys/synch/ltx/repos/en_us_acook/push":
		return SysSynchLtxReposEn_us_acookPush
	case "/sys/synch/ltx/repos/en_us_andronache":
		return SysSynchLtxReposEn_us_andronache
	case "/sys/synch/ltx/repos/en_us_andronache/push":
		return SysSynchLtxReposEn_us_andronachePush
	case "/sys/synch/ltx/repos/en_us_barrett":
		return SysSynchLtxReposEn_us_barrett
	case "/sys/synch/ltx/repos/en_us_barrett/push":
		return SysSynchLtxReposEn_us_barrettPush
	case "/sys/synch/ltx/repos/en_us_carroll":
		return SysSynchLtxReposEn_us_carroll
	case "/sys/synch/ltx/repos/en_us_carroll/push":
		return SysSynchLtxReposEn_us_carrollPush
	case "/sys/synch/ltx/repos/en_us_dedes":
		return SysSynchLtxReposEn_us_dedes
	case "/sys/synch/ltx/repos/en_us_dedes/push":
		return SysSynchLtxReposEn_us_dedesPush
	case "/sys/synch/ltx/repos/en_us_goadedes":
		return SysSynchLtxReposEn_us_goaDedes
	case "/sys/synch/ltx/repos/en_us_goadedes/push":
		return SysSynchLtxReposEn_us_goaDedesPush
	case "/sys/synch/ltx/repos/en_us_duvall":
		return SysSynchLtxReposEn_us_duvall
	case "/sys/synch/ltx/repos/en_us_duvall/push":
		return SysSynchLtxReposEn_us_duvallPush
	case "/sys/synch/ltx/repos/en_us_goa":
		return SysSynchLtxReposEn_us_goa
	case "/sys/synch/ltx/repos/en_us_goa/push":
		return SysSynchLtxReposEn_us_goaPush
	case "/sys/synch/ltx/repos/en_us_holycross":
		return SysSynchLtxReposEn_us_holycross
	case "/sys/synch/ltx/repos/en_us_holycross/push":
		return SysSynchLtxReposEn_us_holycrossPush
	case "/sys/synch/ltx/repos/en_us_houpos":
		return SysSynchLtxReposEn_us_houpos
	case "/sys/synch/ltx/repos/en_us_houpos/push":
		return SysSynchLtxReposEn_us_houposPush
	case "/sys/synch/ltx/repos/en_us_oca":
		return SysSynchLtxReposEn_us_oca
	case "/sys/synch/ltx/repos/en_us_oca/push":
		return SysSynchLtxReposEn_us_ocaPush
	case "/sys/synch/ltx/repos/en_us_public":
		return SysSynchLtxReposEn_us_public
	case "/sys/synch/ltx/repos/en_us_public/push":
		return SysSynchLtxReposEn_us_publicPush
	case "/sys/synch/ltx/repos/en_us_roumas":
		return SysSynchLtxReposEn_us_roumas
	case "/sys/synch/ltx/repos/en_us_roumas/push":
		return SysSynchLtxReposEn_us_roumasPush
	case "/sys/synch/ltx/repos/en_us_system":
		return SysSynchLtxReposEn_us_system
	case "/sys/synch/ltx/repos/en_us_unknown":
		return SysSynchLtxReposEn_us_unknown
	case "/sys/synch/ltx/repos/en_us_unknown/push":
		return SysSynchLtxReposEn_us_unknownPush
	case "/sys/synch/ltx/repos/gr_gr_cog":
		return SysSynchLtxReposGr_gr_cog
	case "/sys/synch/ltx/repos/gr_us_goa":
		return SysSynchLtxReposGr_us_goa
	case "/sys/synch/ltx/repos/fra_fr_oaf":
		return SysSynchLtxReposFra_fr_oaf
	case "/sys/synch/ltx/repos/swh_ke_oak":
		return SysSynchLtxReposSwh_ke_oak
	case "/sys/synch/ltx/repos/gr_redirects_goarch":
		return SysSynchLtxReposGr_redirects_goarch
	case "/sys/synch/ltx/repos/en_us_mlgs":
		return SysSynchLtxReposEn_us_mlgs
	default:
		return -1
	}
}

// Data returns the data for the property.
// All default values are strings and can be cast to the proper type
// using the BoolValue, StringValue, or StringSliceValue methods.
// If the ValueType is a string slice, the parts are tilde delimited.
// If the property is Epistle, Gospel, Liturgical, Prophet, or Psalter,
// use LangComboData() instead, because the path needs to be prefixed.
func (i Property) Data() *PropertyData {
	switch i {
	case Epistle:
		return &PropertyData{
			Description:  "Epistle libraries for build. First is primary, optional others are fallbacks.",
			DefaultValue: "en_us_rsv~en_us_nkjv",
			Path:         "/epistle",
			ValueType:    propertyValueTypes.StringSlice,
		}
	case Gospel:
		return &PropertyData{
			Description:  "Gospel libraries for build. First is primary, optional others are fallbacks.",
			DefaultValue: "en_us_rsv~en_us_nkjv",
			Path:         "/gospel",
			ValueType:    propertyValueTypes.StringSlice,
		}
	case Liturgical:
		return &PropertyData{
			Description:  "Liturgical libraries for build. First is primary, optional others are fallbacks.",
			DefaultValue: "en_us_public",
			Path:         "/liturgical",
			ValueType:    propertyValueTypes.StringSlice,
		}
	case Prophet:
		return &PropertyData{
			Description:  "Prophet libraries for build. First is primary, optional others are fallbacks.",
			DefaultValue: "en_us_saas~en_uk_lash",
			Path:         "/prophet",
			ValueType:    propertyValueTypes.StringSlice,
		}
	case Psalter:
		return &PropertyData{
			Description:  "Psalter libraries for build. First is primary, optional others are fallbacks.",
			DefaultValue: "en_us_saas~en_us_rsv",
			Path:         "/psalter",
			ValueType:    propertyValueTypes.StringSlice,
		}
	case SiteBuildInterlinear:
		return &PropertyData{
			"If true, different versions will appear on lines one after the other (i.e. as an interlinear). If false, different versions will appear in columns, side-by-side.",
			"false",
			"/site/build/interlinear",
			propertyValueTypes.Boolean,
			false,
		}
	case SiteMediaDir:
		return &PropertyData{
			Description:  "If media url is set to http://localhost, you must provide the local directory path to audio recordings and musical scores. The top-most sub-directories must be a (for audio) and m (for musical scores). There must also be a js/viewer directory.",
			DefaultValue: "https://dcs.goarch.org",
			Path:         "/site/media/dir",
			ValueType:    propertyValueTypes.String,
		}
	case SiteMediaEnabled:
		return &PropertyData{
			Description:  "If media.enabled is set to true, then Doxa will generate media dropdowns in html pages. Valid values for this setting are: true, false.",
			DefaultValue: "false",
			Path:         "/site/media/enabled",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteMediaAudioUrl:
		return &PropertyData{
			Description:  "When running Doxa, it can serve audio media from either the local host, e.g. http://localhost, or from a remote server, e.g. https://dcs.goarch.org. Doxa will serve media only if media.enabled: true. There must be directory named 'a' (for audio) and an 'm' directory (for musical scores) that is accessible via the remote url. Do not put a trailing slash on the url. Important note: if you change this setting, you must regenerate the site.",
			DefaultValue: "https://dcs.goarch.org",
			Path:         "/site/media/audio/url",
			ValueType:    propertyValueTypes.String,
		}
	case SiteMediaPdfUrl:
		return &PropertyData{
			Description:  "When running Doxa, it can serve pdf media from either the local host, e.g. http://localhost, or from a remote server, e.g. https://dcs.goarch.org. Doxa will serve media only if media.enabled: true. There must be directory named 'a' (for audio) and an 'm' directory (for musical scores) that is accessible via the remote url. Do not put a trailing slash on the url. Important note: if you change the setting below, you must regenerate the site.",
			DefaultValue: "https://dcs.goarch.org",
			Path:         "/site/media/pdf/url",
			ValueType:    propertyValueTypes.String,
		}
	case SiteMediaPdfViewer:
		return &PropertyData{
			Description:  "If you are using a special pdf viewer, provide the url path prefix to it. If you set it to be empty, PDFs will be rendered by the default pdf viewer that your browser uses. Important note: if you change the setting below, you must regenerate the site.",
			DefaultValue: "https://dcs.goarch.org/goa/dcs/js/viewer/web/viewer.html?file=",
			Path:         "/site/media/pdfViewer",
			ValueType:    propertyValueTypes.String,
		}
	case SysGloryIndex:
		return &PropertyData{
			Description:  "The number key of the Language to use when Doxa gives glory to God, e.g. 1. Leave off the leading zeroes.",
			DefaultValue: "1",
			Path:         "/sys/glory/use",
			ValueType:    propertyValueTypes.Int,
		}
	case SysGloryLanguages:
		return &PropertyData{
			Description:  "Whenever you exit (quit) Doxa, it will give glory to God.  These properties control which language is used.",
			DefaultValue: "Δόξα Πατρὶ καὶ Υἱῷ καὶ Ἁγίῳ Πνεύματι, νῦν καὶ ἀεὶ καὶ εἰς τοὺς αἰῶνας τῶν αἰώνων!~Gloria al Padre, y al Hijo y al Espíritu Santo, perpetuamente, ahora y siempre y por los siglos de los siglos!~Utukufu kwa Baba na Mwana na Roho Mtakatifu, Daima, sasa na siku zote, hata milele na milele!~Glory to the Father and the Son and the Holy Spirit, both now and forever and unto the ages of ages!",
			Path:         "/sys/glory/languages",
			ValueType:    propertyValueTypes.StringSlice,
		}
	case SiteBuildAssets:
		return &PropertyData{
			Description:  "assets properties provide information used for building your liturgical web site.",
			DefaultValue: "",
			Path:         "/site/build/assets",
			DescOnly:     true,
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildAssetsCopyAssets:
		return &PropertyData{
			Description:  "The first time your site is built, Doxa will copy the assets folder contents into your site.  If you set copy.assets to false, it won't copy assets if they already exist in your site.  However, assets/css/app.css and assets/css/doxa.css are always copied.",
			DefaultValue: "false",
			Path:         "/site/build/assets/copy.assets",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildAssetsCss:
		return &PropertyData{
			Description:  "list the css files to be loaded in each html file.  Make sure app.css is 2nd to last and doxa.css is last.",
			DefaultValue: "css/bootstrap.min.css~css/bootstrap-table.min.css~img/bootstrap-icons.css~css/app.css~css/doxa.css",
			Path:         "/site/build/assets/css",
			ValueType:    propertyValueTypes.StringSlice,
		}
	case SiteBuildAssetsJs:
		return &PropertyData{
			Description:  "list the javascript files to be loaded in each html file.  Make sure app.js loads last.",
			DefaultValue: "js/jquery-2.0.3.min.js~js/jquery.dropdown.js~js/bootstrap.bundle.min.js", //~js/app.js",  // MAC 7/27/24. template doc.gohtml loads it just before the </body> tag.
			Path:         "/site/build/assets/js",
			ValueType:    propertyValueTypes.StringSlice,
		}
	case SiteBuildCalendar:
		return &PropertyData{
			Description:  "the calendar to use for computing liturgical day properties, i.e. gregorian vs julian",
			DefaultValue: calendarTypes.Gregorian.String(),
			Path:         "/site/build/calendar",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildRoot:
		return &PropertyData{
			Description:  "this is the name of the root folder for the website",
			DefaultValue: "dcs",
			Path:         "/site/build/root",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildYear:
		return &PropertyData{
			Description:  "if a service template does not have a specified year, Doxa will default to the current year.  There are times, however, when you might like to specify a year.  If set to 0, the current year will be used.",
			DefaultValue: "0",
			Path:         "/site/build/year",
			ValueType:    propertyValueTypes.Int,
		}
	case SiteBuildFrames:
		return &PropertyData{
			Description:  "If use.frames:value is set to true, your site will use frames: one for text, one for scores, and one for audio.",
			DefaultValue: "",
			Path:         "/site/build/frames",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildFooter:
		return &PropertyData{
			Description:  "Properties for footers on your site's web pages. The footer provides up to two links, one for the owner's website and one for the owner's facebook page. The owner's website could be an archdiocese, diocese, metropolis, or a parish.",
			DefaultValue: "",
			Path:         "/site/build/footer",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildFooterEnabled:
		return &PropertyData{
			Description:  "If set to true, your pages will have footers.",
			DefaultValue: "false",
			Path:         "/site/build/footer/enabled",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildFooterFacebookUrl:
		return &PropertyData{
			Description:  "url for owner's facebook page.",
			DefaultValue: "SiteBuildFooterFacebookUrl",
			Path:         "/site/build/footer/facebook",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildFooterOwnerUrl:
		return &PropertyData{
			Description:  "url of the website's owner.",
			DefaultValue: "SiteBuildFooterOwnerUrl",
			Path:         "/site/build/footer/owner",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildHome:
		return &PropertyData{
			Description:  "typically this should be set to servicesindex.html.  It indicates which html page to load in the text frame when it is refreshed.",
			DefaultValue: "home.html",
			Path:         "/site/build/home",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildNavbar:
		return &PropertyData{
			Description:  "These properties are used by assets/layouts/simplenavbar.gohtml to create the navbar on your site's web pages.",
			DefaultValue: "",
			Path:         "/site/build/navbar",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildNavbarHasBooksIcon:
		return &PropertyData{
			Description:  "If set to true, the navbar on generated pages will have an icon to access the books index page.",
			DefaultValue: "true",
			Path:         "/site/build/navbar/hasBookIcon",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildNavbarHasSearchIcon:
		return &PropertyData{
			Description:  "If set to true, the navbar on generated pages will have an icon to access the website search page.",
			DefaultValue: "true",
			Path:         "/site/build/navbar/hasSearchIcon",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildNavbarHasServicesIcon:
		return &PropertyData{
			Description:  "If set to true, the navbar on generated pages will have an icon to access the services index page.",
			DefaultValue: "true",
			Path:         "/site/build/navbar/hasServicesIcon",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildNavbarLogo:
		return &PropertyData{
			Description:  "These properties are for the logo part of the navbar.",
			DefaultValue: "",
			Path:         "/site/build/navbar/logo",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildNavbarUseTextInSideBarMenu:
		return &PropertyData{
			Description:  "If set to true, in the main menu on the navbar on generated pages, the user will see text in addition to icons for the menu items",
			DefaultValue: "false",
			Path:         "/site/build/navbar/useTextInSideMenu",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildNavbarSideBarMenuHome:
		return &PropertyData{
			Description:  "When the menu is opened, if the user clicks on this text, it will go to to the generated website's home page. This text is used in addition to an icon.",
			DefaultValue: "Home",
			Path:         "/site/build/navbar/sideBarMenu/home",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildNavbarSideBarMenuBooks:
		return &PropertyData{
			Description:  "When the menu is opened, if the user clicks on this text, it will go to to the generated website's index for books. This text is used in addition to an icon.",
			DefaultValue: "Books",
			Path:         "/site/build/navbar/sideBarMenu/books",
			ValueType:    propertyValueTypes.String,
		}

	case SiteBuildNavbarSideBarMenuDecreaseFontSize:
		return &PropertyData{
			Description:  "Label for icon to decrease text size.",
			DefaultValue: "Smaller text",
			Path:         "/site/build/navbar/sideBarMenu/text/size/decrease",
			ValueType:    propertyValueTypes.String,
		}

	case SiteBuildNavbarSideBarMenuIncreaseFontSize:
		return &PropertyData{
			Description:  "Label for icon to increase text size.",
			DefaultValue: "Larger text",
			Path:         "/site/build/navbar/sideBarMenu/text/size/increase",
			ValueType:    propertyValueTypes.String,
		}

	case SiteBuildNavbarSideBarMenuToggleFrames:
		return &PropertyData{
			Description:  "Text displayed with check box controlling visibility of media frames.",
			DefaultValue: "Hide music window",
			Path:         "/site/build/navbar/sideBarMenu/toggle/frames",
			ValueType:    propertyValueTypes.String,
		}

	case SiteBuildNavbarSideBarMenuToggleVersionOrientation:
		return &PropertyData{
			Description:  "Label for the version view check box. If unchecked versions are viewed as rows.  If checked, as columns.",
			DefaultValue: "Show versions as columns",
			Path:         "/site/build/navbar/sideBarMenu/toggle/version/orientation",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildNavbarSideBarMenuToggleVersions:
		return &PropertyData{
			Description:  "Text displayed with check box controlling visibility specific versions.",
			DefaultValue: "Show",
			Path:         "/site/build/navbar/sideBarMenu/toggle/versions",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildNavbarSideBarMenuServices:
		return &PropertyData{
			Description:  "When the menu is opened, if the user clicks on this text, it will go to to the generated website's index for services. This text is used in addition to an icon.",
			DefaultValue: "Services",
			Path:         "/site/build/navbar/sideBarMenu/services",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildNavbarSideBarMenuHelp:
		return &PropertyData{
			Description:  "When the menu is opened, if the user clicks on this text, it will go to to the generated website's help page. This text is used in addition to an icon.",
			DefaultValue: "Help",
			Path:         "/site/build/navbar/sideBarMenu/help",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildNavbarSideBarMenuSearch:
		return &PropertyData{
			Description:  "When the menu is opened, if the user clicks on this text, it will go to to the generated website's help page. This text is used in addition to an icon.",
			DefaultValue: "Search",
			Path:         "/site/build/navbar/sideBarMenu/search",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildNavbarUseLogoForMenu:
		return &PropertyData{
			Description:  "If set to true, the main menu on the navbar on generated pages will use the logo you specify.  When user's click on the logo, the site menu will show.  If you set it to false, the default bootstrap list icon (aka hamburger) will be used instead.",
			DefaultValue: "false",
			Path:         "/site/build/navbar/useLogoForMenu",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildNavbarLogoHeight:
		return &PropertyData{
			Description:  "in pixels",
			DefaultValue: "30",
			Path:         "/site/build/navbar/logo/height",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildNavbarLogoImage:
		return &PropertyData{
			Description:  "The file must be in your assets/img folder.",
			DefaultValue: "favicon.ico",
			Path:         "/site/build/navbar/logo/image",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildNavbarLogoWidth:
		return &PropertyData{
			Description:  "in pixels",
			DefaultValue: "30",
			Path:         "/site/build/navbar/logo/width",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildNavbarShowColumnToggle:
		return &PropertyData{
			Description:  "If set to true, the menu on generated pages will show icons to allow the user to hide or show columns.",
			DefaultValue: "true",
			Path:         "/site/build/navbar/showColumnToggle",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildNavbarShowNightDayToggle:
		return &PropertyData{
			Description:  "If set to true, the menu on generated pages will show icons to allow the user to switch between night and day mode.",
			DefaultValue: "true",
			Path:         "/site/build/navbar/showNightDayToggle",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildNavbarTitle:
		return &PropertyData{
			Description:  "The title that will appear on the navbar.",
			DefaultValue: "The Liturgy in My Language (Demo)",
			Path:         "/site/build/navbar/title",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPdf:
		return &PropertyData{
			Description:  "Settings for the generation of PDFs",
			DefaultValue: "",
			Path:         "/site/build/pdf",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildPdfFonts:
		return &PropertyData{
			Description:  "Settings for the fonts to use with PDFs",
			DefaultValue: "",
			Path:         "/site/build/pdf/fonts",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildPdfFontsDefaultDateFormat:
		return &PropertyData{
			Description:  "date format for PDFs.  See https://www.golangprograms.com/get-current-date-and-time-in-various-format-in-golang.html",
			DefaultValue: "Monday, January 02, 2006",
			Path:         "/site/build/pdf/fonts/default/date/format",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPdfFontsDefaultBold:
		return &PropertyData{
			Description:  "bold font to use",
			DefaultValue: "LiberationSerif-Bold.ttf",
			Path:         "/site/build/pdf/fonts/default/bold",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPdfFontsDefaultBoldItalic:
		return &PropertyData{
			Description:  "bold italic font to use.",
			DefaultValue: "LiberationSerif-BoldItalic.ttf",
			Path:         "/site/build/pdf/fonts/default/boldItalic",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPdfFontsDefaultItalic:
		return &PropertyData{
			Description:  "italic font to use",
			DefaultValue: "LiberationSerif-Italic.ttf",
			Path:         "/site/build/pdf/fonts/default/italic",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPdfFontsDefaultRegular:
		return &PropertyData{
			Description:  "regular font to use",
			DefaultValue: "LiberationSerif-Regular.ttf",
			Path:         "/site/build/pdf/fonts/default/regular",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPdfFontsDefaultLineBreakAdjuster:
		return &PropertyData{
			Description:  "value used to adjust line breaks, normally 2",
			DefaultValue: "2",
			Path:         "/site/build/pdf/fonts/default/lineBreakAdjuster",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPdfFontsDefaultMiddleColAdjuster:
		return &PropertyData{
			Description:  "value used to adjust the width of the middle column of a three column page, normally 0.95",
			DefaultValue: "0.95",
			Path:         "/site/build/pdf/fonts/default/middleColAdjuster",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPdfFontsDefaultKerningOn:
		return &PropertyData{
			Description:  "if set to true, kerning will be used with this font",
			DefaultValue: "false",
			Path:         "/site/build/pdf/fonts/default/kerning/on",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildPdfFontsDefaultKerningDivisor:
		return &PropertyData{
			Description:  "value used to adjust width between letters for this font",
			DefaultValue: "6.5",
			Path:         "/site/build/pdf/fonts/default/kerning/divisor",
			ValueType:    propertyValueTypes.Float,
		}
	case SiteBuildPdfFontSize:
		return &PropertyData{
			Description:  "default size of fonts in points. 14.0 is recommended for pdf fonts",
			DefaultValue: "13",
			Path:         "/site/build/pdf/font/size",
			ValueType:    propertyValueTypes.Float,
		}
	case SiteBuildPdfLib:
		return &PropertyData{
			Description:  "Indicates which software to use to generate PDFs.  Values are `go` or `latex`.  If you set it to `latex`, you must have latex installed on our computer.",
			DefaultValue: "go",
			Path:         "/site/build/pdf/lib",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPdfPage:
		return &PropertyData{
			Description:  "settings affecting the layout of a pdf page",
			DefaultValue: "",
			Path:         "/site/build/pdf/page",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildPdfPageColumnsOn:
		return &PropertyData{
			Description:  "if true, monolingual texts will have two columns on each page",
			DefaultValue: "true",
			Path:         "/site/build/pdf/page/columns/on",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildPdfPageFooterLineOn:
		return &PropertyData{
			Description:  "if true, a line will display above the footer",
			DefaultValue: "false",
			Path:         "/site/build/pdf/page/footer/line/on",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildPdfPageGutterLineOn:
		return &PropertyData{
			Description:  "if true, a line will display between columns",
			DefaultValue: "true",
			Path:         "/site/build/pdf/page/gutter/line/on",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildPdfPageHeaderLineOn:
		return &PropertyData{
			Description:  "if true, a line will display below the header",
			DefaultValue: "true",
			Path:         "/site/build/pdf/page/header/line/on",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildPdfCoversOn:
		return &PropertyData{
			Description:  "if true, if a template has a cover section, it will be processed.",
			DefaultValue: "true",
			Path:         "/site/build/pdf/covers/on",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildPdfPageMarginLineOn:
		return &PropertyData{
			Description:  "if true, the margins will have a line, making a rectangle around the text",
			DefaultValue: "false",
			Path:         "/site/build/pdf/page/margins/line/on",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildPdfPageFooterLineColor:
		return &PropertyData{
			Description:  "the color of the footer line by name (e.g. gray) or rgb values (e.g. 128, 128, 128) or hex (e.g. #808080)",
			DefaultValue: "gray",
			Path:         "/site/build/pdf/page/footer/line/color",
			ValueType:    propertyValueTypes.String,
			DescOnly:     false,
		}
	case SiteBuildPdfPageGutterLineColor:
		return &PropertyData{
			Description:  "the color of the gutter line (between columns) by name (e.g. gray) or rgb values (e.g. 128, 128, 128) or hex (e.g. #808080)",
			DefaultValue: "128, 128, 128",
			Path:         "/site/build/pdf/page/gutter/line/color",
			ValueType:    propertyValueTypes.String,
			DescOnly:     false,
		}
	case SiteBuildPdfPageHeaderLineColor:
		return &PropertyData{
			Description:  "the color of the header line by name (e.g. gray) or rgb values (e.g. 128, 128, 128) or hex (e.g. #808080)",
			DefaultValue: "gray",
			Path:         "/site/build/pdf/page/header/line/color",
			ValueType:    propertyValueTypes.String,
			DescOnly:     false,
		}
	case SiteBuildPdfPageMarginLineColor:
		return &PropertyData{
			Description:  "the color of the margin lines by name (e.g. gray) or rgb values (e.g. 128, 128, 128) or hex (e.g. #808080)",
			DefaultValue: "gray",
			Path:         "/site/build/pdf/page/margins/line/color",
			ValueType:    propertyValueTypes.String,
			DescOnly:     false,
		}
	case SiteBuildPdfPageFooterLineWidth:
		return &PropertyData{
			Description:  "the width of the footer line in millimeters (mm), e.g. 0.01",
			DefaultValue: "0.01",
			Path:         "/site/build/pdf/page/footer/line/width",
			ValueType:    propertyValueTypes.Float,
			DescOnly:     false,
		}
	case SiteBuildPdfPageGutterLineWidth:
		return &PropertyData{
			Description:  "the width of the gutter (columns) line in millimeters (mm), e.g. 0.01",
			DefaultValue: "0.01",
			Path:         "/site/build/pdf/page/gutter/line/width",
			ValueType:    propertyValueTypes.Float,
			DescOnly:     false,
		}
	case SiteBuildPdfPageGutterWidth:
		return &PropertyData{
			Description:  "the width of the gutter (space between columns) in millimeters (mm), e.g. 10 or 10.5",
			DefaultValue: "1",
			Path:         "/site/build/pdf/page/gutter/width",
			ValueType:    propertyValueTypes.Float,
			DescOnly:     false,
		}
	case SiteBuildPdfPageHeaderLineWidth:
		return &PropertyData{
			Description:  "the width of the header line in millimeters (mm), e.g. 0.01",
			DefaultValue: "0.01",
			Path:         "/site/build/pdf/page/header/line/width",
			ValueType:    propertyValueTypes.Float,
			DescOnly:     false,
		}
	case SiteBuildPdfPageMarginBottomWidth:
		return &PropertyData{
			Description:  "the width of the bottom margin in millimeters (mm), e.g. 10 or 10.5",
			DefaultValue: "10.0",
			Path:         "/site/build/pdf/page/margins/bottom/width",
			ValueType:    propertyValueTypes.Float,
			DescOnly:     false,
		}
	case SiteBuildPdfPageMarginCellWidth:
		return &PropertyData{
			Description:  "the width of the margins within each cell in millimeters (mm), e.g. 5 or 5.5",
			DefaultValue: "2",
			Path:         "/site/build/pdf/page/margins/cell/width",
			ValueType:    propertyValueTypes.Float,
			DescOnly:     false,
		}
	case SiteBuildPdfPageMarginLeftWidth:
		return &PropertyData{
			Description:  "the width of the left margin in millimeters (mm), e.g. 10 or 10.5",
			DefaultValue: "10.0",
			Path:         "/site/build/pdf/page/margins/left/width",
			ValueType:    propertyValueTypes.Float,
			DescOnly:     false,
		}
	case SiteBuildPdfPageMarginLineWidth:
		return &PropertyData{
			Description:  "the width of each margin line in millimeters (mm), e.g. 0.01",
			DefaultValue: "0.01",
			Path:         "/site/build/pdf/page/margins/line/width",
			ValueType:    propertyValueTypes.Float,
			DescOnly:     false,
		}
	case SiteBuildPdfPageMarginRightWidth:
		return &PropertyData{
			Description:  "the width of the right margin in millimeters (mm), e.g. 10 or 10.5",
			DefaultValue: "10.0",
			Path:         "/site/build/pdf/page/margins/right/width",
			ValueType:    propertyValueTypes.Float,
			DescOnly:     false,
		}
	case SiteBuildPdfPageMarginTopWidth:
		return &PropertyData{
			Description:  "the width of the top margin in millimeters (mm), e.g. 10 or 10.5",
			DefaultValue: "10.0",
			Path:         "/site/build/pdf/page/margins/top/width",
			ValueType:    propertyValueTypes.Float,
			DescOnly:     false,
		}
	case SiteBuildPdfPageLineHeight:
		return &PropertyData{
			Description:  "the height of each text line in millimeters (mm), e.g. 10 or 10.5",
			DefaultValue: "6",
			Path:         "/site/build/pdf/page/line/height",
			ValueType:    propertyValueTypes.Float,
			DescOnly:     false,
		}
	case SiteBuildPdfPaperSize:
		return &PropertyData{
			Description:  "the size of the paper, which must be either A3, A4, A5, Letter, Legal, or Tabloid",
			DefaultValue: "Letter",
			Path:         "/site/build/pdf/paper/size",
			ValueType:    propertyValueTypes.String,
			DescOnly:     false,
		}
	case SiteBuildPdfParagraphSpacing:
		return &PropertyData{
			Description:  "the spacing between paragraphs in millimeters (mm), e.g. 1 or 1.0",
			DefaultValue: "1.518",
			Path:         "/site/build/pdf/paragraph/spacing",
			ValueType:    propertyValueTypes.Float,
			DescOnly:     false,
		}
	case SiteBuildFramesTextSrc:
		return &PropertyData{
			Description:  "HTML file to initially load as source (src) into the dcs.html FrameText frame, e.g. servicesindex.html or a custom home.html page.",
			DefaultValue: "home.html",
			Path:         "/site/build/frames/text/src",
			ValueType:    propertyValueTypes.String,
			DescOnly:     false,
		}
	case SiteBuildFramesScore:
		return &PropertyData{
			Description:  "Properties for the score frame",
			DefaultValue: "",
			Path:         "/site/build/frames/score",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildFramesScoreAlertsDesc:
		return &PropertyData{
			Description:  "These properties are for alerts that can appear in the score frame.",
			DefaultValue: "",
			Path:         "/site/build/frames/score/alerts",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildFramesScoreAlertsEnabled:
		return &PropertyData{
			Description:  "If set to true, alerts will appear in the score frame.",
			DefaultValue: "false",
			Path:         "/site/build/frames/score/alerts/enabled",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildFramesScoreAlerts:
		return &PropertyData{
			Description:  "",
			DefaultValue: "When updates or corrections are made to the DCS, you can read about them below:",
			Path:         "/site/build/frames/score/alerts/values",
			ValueType:    propertyValueTypes.StringSlice,
		}
	case SiteBuildFramesScoreDonate:
		return &PropertyData{
			Description:  "Optional. If you have a donate.html page, you can use frame.score.donate to provide brief text about making a donation.  The full information should be in your donate.html page. Your donate.html page should be in your site assets folder. A donate icon will automatically be displayed.",
			DefaultValue: "",
			Path:         "/site/build/frames/score/donate",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildFramesScoreDonateEnabled:
		return &PropertyData{
			Description:  "If set to true, donation information will appear in the score frame when first displayed.",
			DefaultValue: "false",
			Path:         "/site/build/frames/score/donate/enabled",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildFramesScoreDonateText:
		return &PropertyData{
			Description:  "Information about your organization, e.g. if donations are tax deductible.",
			DefaultValue: "",
			Path:         "/site/build/frames/score/donate/text",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildFramesScoreDonateTitle:
		return &PropertyData{
			Description:  "The title for your Donate section of the score frame.",
			DefaultValue: "Donate",
			Path:         "/site/build/frames/score/donate/title",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildFramesScoreHeading:
		return &PropertyData{
			Description:  "displays as the heading lines in the score frame.  You can have more than one line.",
			DefaultValue: "Musical Scores Will Appear Here",
			Path:         "/site/build/frames/score/heading",
			ValueType:    propertyValueTypes.StringSlice,
		}
	case SiteBuildFramesScoreParagraphs:
		return &PropertyData{
			Description:  "These properties are for paragraphs that can appear in the score frame.",
			DefaultValue: "",
			Path:         "/site/build/frames/score/paragraphs",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildFramesScoreParagraphsEnabled:
		return &PropertyData{
			Description:  "If set to true, paragraphs will appear in the score frame.",
			DefaultValue: "false",
			Path:         "/site/build/frames/score/paragraphs/enabled",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildFramesScoreParagraphsValues:
		return &PropertyData{
			Description:  "",
			DefaultValue: "",
			Path:         "/site/build/frames/score/paragraphs/values",
			ValueType:    propertyValueTypes.StringSlice,
		}
	case SiteBuildFramesScoreTwitter:
		return &PropertyData{
			Description:  "These properties are for inserting a twitter feed in the score frame. If your organization is on Twitter, to embed a twitter timeline, go to https://publish.twitter.com and enter your twitter url. Click on 'timeline'. Then copy the url from the code shown to you by Twitter and use the information to set the properties.",
			DefaultValue: "",
			Path:         "/site/build/frames/score/twitter",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildFramesScoreTwitterEnabled:
		return &PropertyData{
			Description:  "If set to true, tweets will appear in the score frame.",
			DefaultValue: "false",
			Path:         "/site/build/frames/score/twitter/enabled",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildFramesScoreTwitterHandle:
		return &PropertyData{
			Description:  "the handle for your twitter feed",
			DefaultValue: "",
			Path:         "/site/build/frames/score/twitter/handle",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildFramesScoreTwitterUrl:
		return &PropertyData{
			Description:  "the url for your twitter feed",
			DefaultValue: "",
			Path:         "/site/build/frames/score/twitter/url",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildFramesUseFrames:
		return &PropertyData{
			Description:  "if true, your site will use frames.",
			DefaultValue: "true",
			Path:         "/site/build/frames/use.frames",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildIndex:
		return &PropertyData{
			Description:  "If you set the index properties to false, you need to create your own servicesindex.html and/or booksindex.html and put them in your site assets folder. They will be copied into the site public folder.",
			DefaultValue: "",
			Path:         "/site/build/index",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildIndexBooks:
		return &PropertyData{
			Description:  "if true, indexes will be created for books",
			DefaultValue: "true",
			Path:         "/site/build/index/books",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildIndexServices:
		return &PropertyData{
			Description:  "if true, indexes will be created for services",
			DefaultValue: "true",
			Path:         "/site/build/index/services",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildLayouts:
		return &PropertyData{
			Description: "combinations of libraries to use as layouts by the generator",
			DescOnly:    true,
			Path:        "/site/build/layouts",
			ValueType:   propertyValueTypes.String,
		}
	case SiteBuildLayoutsColumns:
		return &PropertyData{
			Description: "for a specific language code, the libraries to be used in a column by the generator for liturgical and biblical texts",
			DescOnly:    false,
			Path:        "/site/build/layouts/columns",
			ValueType:   propertyValueTypes.StringMapString,
		}
	case SiteBuildLayoutsRows:
		return &PropertyData{
			Description:  "a comma delimited list of language codes for a row, to be used by the generator, e.g., en, gr-en, gr.",
			DefaultValue: "en, gr-en",
			Path:         "/site/build/layouts/rows",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildLayoutsSelected:
		return &PropertyData{
			Description:  "acronyms of layouts to be processed by the generator. If empty or set to *, all will be processed.  Example: en, gr-en",
			DefaultValue: "*",
			DescOnly:     false,
			Path:         "/site/build/layouts/selected",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildTemplatesStatusesSelected:
		return &PropertyData{
			Description:  "template statuses to be processed by the generator. If empty or set to *, all will be processed.  Example: draft, final.  These are normally set via the Doxa web browser Generate web page.",
			DefaultValue: "final",
			DescOnly:     false,
			Path:         "/site/build/templates/statuses/selected",
			ValueType:    propertyValueTypes.String,
		}

	case SiteBuildTemplatesDaysOfWeekSelected:
		return &PropertyData{
			Description:  "service template days of week (based on month,day, year), comma separated, to be processed by the generator. If empty or set to *, all will be processed.  Example: 0, 7.  Sunday starts with zero. These are normally set via the Doxa web browser Generate web page.",
			DefaultValue: "*",
			DescOnly:     false,
			Path:         "/site/build/templates/daysOfWeek/selected",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildOutput:
		return &PropertyData{
			Description:  "the properties in this directory indicate which types of files you want to generate for your site when the build command is run. If you have already generated your html and pdf files and only want to generate the indexes, set the html and pdf to false.",
			DefaultValue: "",
			Path:         "/site/build/output",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildOutputHtml:
		return &PropertyData{
			Description:  "if true, build will generate html",
			DefaultValue: "true",
			Path:         "/site/build/output/html",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildOutputPdf:
		return &PropertyData{
			Description:  "if true, build will generate pdf",
			DefaultValue: "true",
			Path:         "/site/build/output/pdf",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysDbAutoBackupOn:
		return &PropertyData{
			Description:  "if true, a backup of the Doxa database will be made each time Doxa starts",
			DefaultValue: "false",
			Path:         "/sys/db/autoBackupOn",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildPages:
		return &PropertyData{
			Description:  "Properties for home, help, and index pages.",
			DefaultValue: "",
			Path:         "/site/build/pages",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildPagesHelp:
		return &PropertyData{
			Description:  "These properties are used in assets/layouts/help.gohtml template to create the help page for your site. The help page combines the information of an About page and a Help page.",
			DefaultValue: "",
			Path:         "/site/build/pages/help",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildPagesHelpIcons:
		return &PropertyData{
			Description:  "These properties are used to explain the meaning of the menu icons.",
			DefaultValue: "",
			Path:         "/site/build/pages/help/icons",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildPagesHelpIconsAudio:
		return &PropertyData{
			Description:  "Explains what happens when this icon is clicked.",
			DefaultValue: "This icon appears above a hymn if an audio recording is available for you to listen to. Click it to view a list of available recordings.",
			Path:         "/site/build/pages/help/icons/audio",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHelpIconsBooks:
		return &PropertyData{
			Description:  "Explains what happens when this icon is clicked.",
			DefaultValue: "Click this icon to see liturgical books.",
			Path:         "/site/build/pages/help/icons/books",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHelpIconsBscore:
		return &PropertyData{
			Description:  "Explains what happens when this icon is clicked.",
			DefaultValue: "This icon appears above a hymn if there are Byzantine musical scores available for it.  Click it to view a list of available scores.",
			Path:         "/site/build/pages/help/icons/bscore",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHelpIconsCalendar:
		return &PropertyData{
			Description:  "Explains what happens when this icon is clicked.",
			DefaultValue: "Click this icon to see a calendar of services.",
			Path:         "/site/build/pages/help/icons/calendar",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHelpIconsColumns:
		return &PropertyData{
			Description:  "Explains what happens when this icon is clicked.",
			DefaultValue: "Shows all columns.",
			Path:         "/site/build/pages/help/icons/columns",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHelpIconsEscore:
		return &PropertyData{
			Description:  "Explains what happens when this icon is clicked.",
			DefaultValue: "This icon appears above a hymn if there are enhanced Byzantine musical scores available for it.  Click it to view a list of available scores.",
			Path:         "/site/build/pages/help/icons/escore",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHelpIconsHelp:
		return &PropertyData{
			Description:  "Explains what happens when this icon is clicked.",
			DefaultValue: "Click this icon to see this page.",
			Path:         "/site/build/pages/help/icons/help",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHelpIconsHome:
		return &PropertyData{
			Description:  "Explains what happens when this icon is clicked.",
			DefaultValue: "Click this icon to see the home page.",
			Path:         "/site/build/pages/help/icons/home",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHelpIconsInfo:
		return &PropertyData{
			Description:  "Explains what happens when this icon is clicked.",
			DefaultValue: "This icon appears when viewing a book or service. When you click on it, it will toggle the display of additional information.",
			Path:         "/site/build/pages/help/icons/info",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHelpIconsIntro:
		return &PropertyData{
			Description:  "Explains what happens when this icon is clicked.",
			DefaultValue: "When you click on this icon you will see a menu with icons.  Below is an explanation of what happens when you click on each menu icon.",
			Path:         "/site/build/pages/help/icons/intro",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHelpIconsLeft:
		return &PropertyData{
			Description:  "Explains what happens when this icon is clicked.",
			DefaultValue: "This icon appears when viewing a book or service. When you click on it, it will show just the left column.",
			Path:         "/site/build/pages/help/icons/left",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHelpIconsMinus:
		return &PropertyData{
			Description:  "Explains what happens when this icon is clicked.",
			DefaultValue: "This icon appears when viewing a book or service. When you click on it, it will decrease the text size.",
			Path:         "/site/build/pages/help/icons/minus",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHelpIconsMoon:
		return &PropertyData{
			Description:  "Explains what happens when this icon is clicked.",
			DefaultValue: "This icon appears when viewing a book or service. When you click on it, it will switch to night mode.",
			Path:         "/site/build/pages/help/icons/moon",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHelpIconsPlus:
		return &PropertyData{
			Description:  "Explains what happens when this icon is clicked.",
			DefaultValue: "This icon appears when viewing a book or service. When you click on it, it will increase the text size.",
			Path:         "/site/build/pages/help/icons/plus",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHelpIconsRight:
		return &PropertyData{
			Description:  "Explains what happens when this icon is clicked.",
			DefaultValue: "This icon appears when viewing a book or service. When you click on it, it will show just the right column.",
			Path:         "/site/build/pages/help/icons/right",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHelpIconsSearch:
		return &PropertyData{
			Description:  "Explains what happens when this icon is clicked.",
			DefaultValue: "Click this icon to search the website.",
			Path:         "/site/build/pages/help/icons/search",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHelpIconsSun:
		return &PropertyData{
			Description:  "Explains what happens when this icon is clicked.",
			DefaultValue: "Switches to daytime mode.",
			Path:         "/site/build/pages/help/icons/sun",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHelpIconsTitle:
		return &PropertyData{
			Description:  "Explains what happens when this icon is clicked.",
			DefaultValue: "What the Menu Icons Mean and How to Use Them",
			Path:         "/site/build/pages/help/icons/title",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHelpIconsWscore:
		return &PropertyData{
			Description:  "Explains what happens when this icon is clicked.",
			DefaultValue: "This icon appears above a hymn if there are Western notation musical scores available for it.  Click it to view a list of available scores.",
			Path:         "/site/build/pages/help/icons/wscore",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHelpParagraphs:
		return &PropertyData{
			Description:  "These become paragraphs on your help page.  You can add as many as you like.",
			DefaultValue: "This website was created by the Liturgy in My Language.",
			Path:         "/site/build/pages/help/paragraphs",
			ValueType:    propertyValueTypes.StringSlice,
		}
	case SiteBuildPagesHelpTitle:
		return &PropertyData{
			Description:  "Title for your help page",
			DefaultValue: "About This web Site",
			Path:         "/site/build/pages/help/title",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHome:
		return &PropertyData{
			Description:  "These properties are used in assets/layouts/home.gohtml to create your site's home page.",
			DefaultValue: "",
			Path:         "/site/build/pages/home",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildPagesHomeCards:
		return &PropertyData{
			Description:  "Cards are a type of Boostrap component and are used by Doxa. By setting properties here, you can provide the information for Doxa to create cards. The only property required for a card is it's text. The other properties are: title, icon (the name of a Bootstrap icon to display, ButtonText, ButtonUrl, Image (a file in the img/ directory), and ImageAlt (alternate text to be read by reader software for visually challenged individuals).",
			DefaultValue: "",
			Path:         "/site/build/pages/home/cards",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildPagesHomeCardsParentValueImageAlt:
		return &PropertyData{
			Description:  "If set, readers for visually impaired users will use this to describe the image.",
			DefaultValue: "liturgical books and services",
			Path:         "/site/build/pages/home/cards/parent/value/imageAlt",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsParentValueImageSource:
		return &PropertyData{
			Description:  "The image file to use from the img directory for the home page.",
			DefaultValue: "stphotios.png",
			Path:         "/site/build/pages/home/cards/parent/value/imageSource",
			ValueType:    propertyValueTypes.String,
		}

	case SiteBuildPagesHomeCardsBooks:
		return &PropertyData{
			Description:  "This card displays on your home page if your website contains books.",
			DefaultValue: "",
			Path:         "/site/build/pages/home/cards/books",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildPagesHomeCardsBooksValueBody:
		return &PropertyData{
			Description:  "If set, these paragraphs will appear in the card body",
			DefaultValue: "Liturgical books (e.g. the Menaion) are available in a variety of languages.",
			Path:         "/site/build/pages/home/cards/books/value/body",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsBooksValueButtonText:
		return &PropertyData{
			Description:  "If set, your card will have a button, and this is the text inside the button.",
			DefaultValue: "View Books",
			Path:         "/site/build/pages/home/cards/books/value/buttonText",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsBooksValueButtonUrl:
		return &PropertyData{
			Description:  "If set, when the card button is clicked, the page will open to this url.",
			DefaultValue: "booksindex.html",
			Path:         "/site/build/pages/home/cards/books/value/buttonUrl",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsBooksValueIcon:
		return &PropertyData{
			Description:  "If set, your card will use this Bootstrap icon.",
			DefaultValue: "book",
			Path:         "/site/build/pages/home/cards/books/value/icon",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsBooksValueImageAlt:
		return &PropertyData{
			Description:  "If set, readers for visually impaired users will use this to describe the image.",
			DefaultValue: "liturgical books",
			Path:         "/site/build/pages/home/cards/books/value/imageAlt",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsBooksValueImageSource:
		return &PropertyData{
			Description:  "The image file to use from the img directory.",
			DefaultValue: "img/books.png",
			Path:         "/site/build/pages/home/cards/books/value/imageSource",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsBooksValueTitle:
		return &PropertyData{
			Description:  "If set, this will be the title of your card",
			DefaultValue: "Liturgical Books",
			Path:         "/site/build/pages/home/cards/books/value/title",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsClosing:
		return &PropertyData{
			Description:  "This card appears as the bottom of your home page before the footer.",
			DefaultValue: "",
			Path:         "/site/build/pages/home/cards/closing",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildPagesHomeCardsClosingValueButtonText:
		return &PropertyData{
			Description:  "If set, your card will have a button, and this is the text inside the button.",
			DefaultValue: "",
			Path:         "/site/build/pages/home/cards/closing/value/buttonText",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsClosingValueButtonUrl:
		return &PropertyData{
			Description:  "If set, when the card button is clicked, the page will open to this url.",
			DefaultValue: "",
			Path:         "/site/build/pages/home/cards/closing/value/buttonUrl",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsClosingValueIcon:
		return &PropertyData{
			Description:  "If set, your card will use this Bootstrap icon.",
			DefaultValue: "",
			Path:         "/site/build/pages/home/cards/closing/value/icon",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsClosingValueImageAlt:
		return &PropertyData{
			Description:  "If set, readers for visually impaired users will use this to describe the image.",
			DefaultValue: "",
			Path:         "/site/build/pages/home/cards/closing/value/imageAlt",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsClosingValueImageSource:
		return &PropertyData{
			Description:  "The image file to use from the img directory.",
			DefaultValue: "",
			Path:         "/site/build/pages/home/cards/closing/value/imageSource",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsClosingValueBodyPrefix:
		return &PropertyData{
			Description:  "If set, these paragraphs will appear in the card body",
			DefaultValue: "On all pages, you can view the menu by clicking on the logo",
			Path:         "/site/build/pages/home/cards/closing/value/body/prefix",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsClosingValueBodySuffix:
		return &PropertyData{
			Description:  "If set, these paragraphs will appear in the card body",
			DefaultValue: "at the top of the page. To view the Help page, open the menu and click on this icon:",
			Path:         "/site/build/pages/home/cards/closing/value/body/suffix",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsClosingValueTitle:
		return &PropertyData{
			Description:  "If set, this will be the title of your card",
			DefaultValue: "Demonstration Website",
			Path:         "/site/build/pages/home/cards/closing/value/title",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsIntro:
		return &PropertyData{
			Description:  "This card appears as the introduction on your home page.",
			DefaultValue: "",
			Path:         "/site/build/pages/home/cards/intro",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildPagesHomeCardsIntroValueButtonText:
		return &PropertyData{
			Description:  "If set, your card will have a button, and this is the text inside the button.",
			DefaultValue: "",
			Path:         "/site/build/pages/home/cards/intro/value/buttonText",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsIntroValueButtonUrl:
		return &PropertyData{
			Description:  "If set, when the card button is clicked, the page will open to this url.",
			DefaultValue: "",
			Path:         "/site/build/pages/home/cards/intro/value/buttonUrl",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsIntroValueButtonIcon:
		return &PropertyData{
			Description:  "If set, your card will use this Bootstrap icon.",
			DefaultValue: "",
			Path:         "/site/build/pages/home/cards/intro/value/buttonIcon",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsIntroValueImageAlt:
		return &PropertyData{
			Description:  "If set, readers for visually impaired users will use this to describe the image.",
			DefaultValue: "",
			Path:         "/site/build/pages/home/cards/intro/value/imageAlt",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsIntroValueImageSource:
		return &PropertyData{
			Description:  "The image file to use from the img directory.",
			DefaultValue: "",
			Path:         "/site/build/pages/home/cards/intro/value/imageSource",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsIntroValueBody:
		return &PropertyData{
			Description:  "If set, these paragraphs will appear in the card body",
			DefaultValue: "This DOXA.today liturgical website was generated as a demonstration of DOXA.tools for Digital Orthodox Akolouthia. DOXA.tools can be used to generate liturgical books and dated services (with variable content) in one, two, or three languages side-by-side. The demo includes only a limited number of services and books.",
			Path:         "/site/build/pages/home/cards/intro/value/body",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsIntroValueTitle:
		return &PropertyData{
			Description:  "If set, this will be the title of your card",
			DefaultValue: "Demonstration Digital Chant Stand",
			Path:         "/site/build/pages/home/cards/intro/value/title",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsHelpPrefix:
		return &PropertyData{
			Description:  "This is the suffix.",
			DefaultValue: "at the top of the page. To view the Help page, open the menu and click on this icon:",
			Path:         "/site/build/pages/home/cards/help/prefix",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsServices:
		return &PropertyData{
			Description:  "This card displays on your home page if your website contains dated liturgical services.",
			DefaultValue: "",
			Path:         "/site/build/pages/home/cards/services",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildPagesHomeCardsServicesValueBody:
		return &PropertyData{
			Description:  "If set, these paragraphs will appear in the card body",
			DefaultValue: "Eastern Orthodox Christian liturgical services are available for specific dates.",
			Path:         "/site/build/pages/home/cards/services/value/body",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsServicesValueButtonText:
		return &PropertyData{
			Description:  "If set, your card will have a button, and this is the text inside the button.",
			DefaultValue: "View Calendar of Services",
			Path:         "/site/build/pages/home/cards/services/value/buttonText",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsServicesValueButtonUrl:
		return &PropertyData{
			Description:  "If set, when the card button is clicked, the page will open to this url.",
			DefaultValue: "servicesindex.html",
			Path:         "/site/build/pages/home/cards/services/value/buttonUrl",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsServicesValueIcon:
		return &PropertyData{
			Description:  "If set, your card will use this Bootstrap icon.",
			DefaultValue: "calendar-date",
			Path:         "/site/build/pages/home/cards/services/value/icon",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsServicesValueImageAlt:
		return &PropertyData{
			Description:  "If set, readers for visually impaired users will use this to describe the image.",
			DefaultValue: "liturgical services",
			Path:         "/site/build/pages/home/cards/services/value/imageAlt",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsServicesValueImageSource:
		return &PropertyData{
			Description:  "The image file to use from the img directory.",
			DefaultValue: "stphotios.png",
			Path:         "/site/build/pages/home/cards/services/value/imageSource",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeCardsServicesValueTitle:
		return &PropertyData{
			Description:  "If set, this will be the title of your card",
			DefaultValue: "Liturgical Services",
			Path:         "/site/build/pages/home/cards/services/value/title",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeIntro:
		return &PropertyData{
			Description:  "If set to true, an introductory card will be included on your home page.",
			DefaultValue: "true",
			Path:         "/site/build/pages/home/intro",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildPagesHomeMetaDescription:
		return &PropertyData{
			Description:  "Meta tag for HTML header. Description of your website.",
			DefaultValue: "Demo of an Eastern Orthodox Christian liturgical website generated using DOXA.tools for Digital Orthodox Akolouthia.",
			Path:         "/site/build/pages/home/meta/description",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeMetaOgDescription:
		return &PropertyData{
			Description:  "OG Meta tag for HTML header. Description of your website.",
			DefaultValue: "Demo of an Eastern Orthodox Christian liturgical website generated using DOXA.tools for Digital Orthodox Akolouthia.",
			Path:         "/site/build/pages/home/meta/og.description",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeMetaOgTitle:
		return &PropertyData{
			Description:  "OG Meta tag for HTML header. Set this to the title for your website.",
			DefaultValue: "The Liturgy in My Language (Demo)",
			Path:         "/site/build/pages/home/meta/og.title",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeMetaOgUrl:
		return &PropertyData{
			Description:  "OG Meta tag for HTML header. Set this to the url for your website.",
			DefaultValue: "SiteBuildPagesHomeMetaOgUrl",
			Path:         "/site/build/pages/home/meta/og.url",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesHomeParagraphs:
		return &PropertyData{
			Description:  "Enter zero to many paragraphs, separated by a tilde ~.  These appear as cards on the generated home page, following the help card.",
			DefaultValue: "",
			Path:         "/site/build/pages/home/paragraphs",
			ValueType:    propertyValueTypes.StringSlice,
		}
	case SiteBuildPagesHomeTitle:
		return &PropertyData{
			Description:  "This is the title for your web site. It appears in the menu bar.",
			DefaultValue: "The Liturgy in My Language (Demo)",
			Path:         "/site/build/pages/home/title",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesIndex:
		return &PropertyData{
			Description:  "Properties for indexing your website.",
			DefaultValue: "",
			Path:         "/site/build/pages/index",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildPagesIndexBooks:
		return &PropertyData{
			Description:  "Properties for creating an index of books.",
			DefaultValue: "",
			Path:         "/site/build/pages/index/books",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildPagesIndexBooksDescription:
		return &PropertyData{
			Description:  "Description of the page.  You can have as many lines as you want.",
			DefaultValue: "Click headings to expand or contract the contents.",
			Path:         "/site/build/pages/index/books/description",
			ValueType:    propertyValueTypes.StringSlice,
		}
	case SiteBuildPagesIndexBooksSearchPrompt:
		return &PropertyData{
			Description:  "Words to prompt a user to enter text to search for.",
			DefaultValue: "Search...",
			Path:         "/site/build/pages/index/books/searchPrompt",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesIndexBooksSubtitle:
		return &PropertyData{
			Description:  "Appears as the subtitle for the page.",
			DefaultValue: "",
			Path:         "/site/build/pages/index/books/subtitle",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesIndexBooksTabTitle:
		return &PropertyData{
			Description:  "Appears in the HTML web page tab.",
			DefaultValue: "Books",
			Path:         "/site/build/pages/index/books/tabTitle",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesIndexBooksTitle:
		return &PropertyData{
			Description:  "Appears as the title on the page.",
			DefaultValue: "Sacraments and Services",
			Path:         "/site/build/pages/index/books/title",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesIndexCustom:
		return &PropertyData{
			Description:  "Properties for creating an index of custom content.",
			DefaultValue: "",
			Path:         "/site/build/pages/index/custom",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildPagesIndexCustomDescription:
		return &PropertyData{
			Description:  "Description of the page.  You can have as many lines as you want.",
			DefaultValue: "",
			Path:         "/site/build/pages/index/custom/description",
			ValueType:    propertyValueTypes.StringSlice,
		}
	case SiteBuildPagesIndexCustomSubtitle:
		return &PropertyData{
			Description:  "Appears as the subtitle for the page.",
			DefaultValue: "",
			Path:         "/site/build/pages/index/custom/subtitle",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesIndexCustomTabTitle:
		return &PropertyData{
			Description:  "Appears in the HTML web page tab.",
			DefaultValue: "Custom",
			Path:         "/site/build/pages/index/custom/tabTitle",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesIndexCustomTitle:
		return &PropertyData{
			Description:  "Appears as the title on the page.",
			DefaultValue: "Index of Additional Resources",
			Path:         "/site/build/pages/index/custom/title",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesIndexServices:
		return &PropertyData{
			Description:  "Properties for creating an index of services.",
			DefaultValue: "",
			Path:         "/site/build/pages/index/services",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildPagesIndexServicesDay:
		return &PropertyData{
			Description:  "Properties for creating an index of services for a specific day.",
			DefaultValue: "",
			Path:         "/site/build/pages/index/services/day",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildPagesIndexServicesDayDescription:
		return &PropertyData{
			Description:  "Description of the day index page.  You can have as many lines as you want.",
			DefaultValue: "Links with the label 'Print' open the pdf files of the texts.~Links with the label 'View' open the HTML text of a service, which has the links to scores and audio files. To open the score of a particular hymn or listen to the audio recording if available, scroll to the desired place in the service, click the Byzantine neum or the Western note for the respective score, or the audio icon for the recording. To print a score that you have opened, use your device's file print options.",
			Path:         "/site/build/pages/index/services/day/description",
			ValueType:    propertyValueTypes.StringSlice,
		}
	case SiteBuildPagesIndexServicesDaySubtitleDateFormat:
		return &PropertyData{
			Description:  "Always use Monday January 2 2006.  The correct date will be inserted. See https://golang.org/pkg/time/#Time.Format",
			DefaultValue: "Monday, January 2, 2006",
			Path:         "/site/build/pages/index/services/day/subtitle/date.format",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesIndexServicesDaySubtitlePrefix:
		return &PropertyData{
			Description:  "Text preceding the date.",
			DefaultValue: "Services for ",
			Path:         "/site/build/pages/index/services/day/subtitle/prefix",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesIndexServicesDaySubtitleSuffix:
		return &PropertyData{
			Description:  "Text following the date.",
			DefaultValue: "",
			Path:         "/site/build/pages/index/services/day/subtitle/suffix",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesIndexServicesDayTabTitle:
		return &PropertyData{
			Description:  "Appears in the HTML web page tab.",
			DefaultValue: "Services",
			Path:         "/site/build/pages/index/services/day/tabTitle",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesIndexServicesDayTitle:
		return &PropertyData{
			Description:  "Appears as the title on the page.",
			DefaultValue: "Digital Chant Stand",
			Path:         "/site/build/pages/index/services/day/title",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesIndexServicesMonth:
		return &PropertyData{
			Description:  "Properties for creating an months index of services.",
			DefaultValue: "",
			Path:         "/site/build/pages/index/services/month",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildPagesIndexServicesMonthDescription:
		return &PropertyData{
			Description:  "Description of the page.  You can have as many lines as you want.",
			DefaultValue: "",
			Path:         "/site/build/pages/index/services/month/description",
			ValueType:    propertyValueTypes.StringSlice,
		}
	case SiteBuildPagesIndexServicesMonthSubtitle:
		return &PropertyData{
			Description:  "Appears as the subtitle for the page.",
			DefaultValue: "Calendar Index",
			Path:         "/site/build/pages/index/services/month/subtitle",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesIndexServicesMonthTabTitle:
		return &PropertyData{
			Description:  "Appears in the HTML web page tab.",
			DefaultValue: "Services",
			Path:         "/site/build/pages/index/services/month/tabTitle",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPagesIndexServicesMonthTitle:
		return &PropertyData{
			Description:  "Appears as the title on the page.",
			DefaultValue: "Liturgical Services",
			Path:         "/site/build/pages/index/services/month/title",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPregenDeleteHtmlDir:
		return &PropertyData{
			Description:  "if set to true, the website builder will delete the existing HTML directory before re-building the site",
			DefaultValue: "true",
			Path:         "/site/build/pregen/deleteHtmlDir",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildPregenDeletePdfDir:
		return &PropertyData{
			Description:  "if set to true, the website builder will delete the existing HTML directory before re-building the site",
			DefaultValue: "true",
			Path:         "/site/build/pregen/deletePdfDir",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildPostgenIndexForNav:
		return &PropertyData{
			Description:  "if set to true, the website builder will index the HTML and PDF files to provide navigation pages to show the user what is available.",
			DefaultValue: "true",
			Path:         "/site/build/postgen/index/forNav",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildPostgenIndexForSearch:
		return &PropertyData{
			Description:  "if set to true, the website builder will index the text in the HTML files to provide a search capability.",
			DefaultValue: "true",
			Path:         "/site/build/postgen/index/forSearch",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildPagesTitles:
		return &PropertyData{
			Description:  "The text to display on the website based on the book or service type code in the template ID. For example, se.m08.d05.li has li as the service type, and based on the title property for `li` would display as `Divine Liturgy`.",
			DefaultValue: "",
			Path:         "/site/build/pages/titles",
			ValueType:    propertyValueTypes.StringMapString,
		}
	case SiteBuildPublishPublicUrl:
		return &PropertyData{
			Description:  "The web address for your published liturgical web site.  This is used as a link in the Doxa app for View > Published Public Site.",
			DefaultValue: "",
			Path:         "/site/build/publish/public/url",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPublishTestUrl:
		return &PropertyData{
			Description:  "The web address for your published test liturgical web site.  This is used as a link in the Doxa app for View > Published Test Site.",
			DefaultValue: "",
			Path:         "/site/build/publish/test/url",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildRealm:
		return &PropertyData{
			Description:  "# A realm is used to identify an individual, an organization, a parish, a metropolis, a diocese, an archdiocese, etc. It must be a unique identifier. A realm can be used in an if statement or for a switch, in a template in order to customize a book or service. The realm is set below, but can be overridden using the lml directive: SetRealm and restored using: RestoreRealm which will revert the realm to what you set below. See the lml directives: SetRealm and RestoreRealm.",
			DefaultValue: "doxa-liml",
			Path:         "/site/build/realm",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildPublic:
		return &PropertyData{
			Description:  "If set to true, the public site will be generated. If false, the test site.",
			DefaultValue: "false",
			Path:         "/site/build/public",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildShowTime:
		return &PropertyData{
			Description:  "If set to true, the time will be displayed in the navbar when on book or service pages.",
			DefaultValue: "true",
			Path:         "/site/build/show.time",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildTemplatesChecking:
		return &PropertyData{
			Description:  "Settings to control some aspects of error checking when a template is rendered.",
			DefaultValue: "",
			Path:         "/site/build/templates/checking",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildTemplatesCheckingRid:
		return &PropertyData{
			Description:  "Settings to control error checking of a rid when a template is rendered.",
			DefaultValue: "",
			Path:         "/site/build/templates/checking/rid",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SiteBuildTemplatesCheckingRidKey:
		return &PropertyData{
			Description:  "show error if rid topic:key does not exist",
			DefaultValue: "false",
			Path:         "/site/build/templates/checking/rid/key",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildTemplatesCheckingRidValue:
		return &PropertyData{
			Description:  "show error if rid topic:key exists, ends in .text, but does not have a value",
			DefaultValue: "false",
			Path:         "/site/build/templates/checking/rid/value",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildTemplatesFallback:
		return &PropertyData{
			Description:  "If you subscribe to templates made by other people, and want to make a change, copy the template you want to modify, and place it in your project templates folder.  Use the fallback properties to indicate which GitLab Group to use as the fallback. You also use the fallback properties to enable or disable fallback.",
			DefaultValue: "",
			DescOnly:     true,
			Path:         "/site/build/templates/fallback",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildTemplatesFallbackEnabled:
		return &PropertyData{
			Description:  "If set to true, when Doxa looks for a template, it will first look in your project templates directory.  If it is not found there, Doxa will look in the templates folder for the GitLab Group you selected as the fallback.  It will be in doxa/subscriptions/{GitLab group name}/templates.",
			DefaultValue: "true",
			Path:         "/site/build/templates/fallback/enabled",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildTemplatesFallbackDir:
		return &PropertyData{
			Description:  "The name of the GitLab Group whose templates you want to use if a requested template is not found in your primary templates directory. This group name must be a directory in the doxa/subscriptions directory and have a subdirectory named templates. NOTE: the subscriptions command will automatically set this value when you subscribe to a catalog.",
			DefaultValue: "",
			Path:         "/site/build/templates/fallback/dir",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildTemplatesPatterns:
		return &PropertyData{
			Description:  "Regular expressions for selecting which templates to use.",
			DefaultValue: `bk.eu.baptism.lml~bk\..*.lml~se\.m([0-9][0-9])\.d([0-9][0-9])\..*.lml~bk|se\.m([0-9][0-9])\.d([0-9][0-9])\.(.*).lml~se\.m10\.d([0-9][0-9])\.li\.lml~se\.m12\.d25\.li\.lml~bk.he.(a|h).m([1-8]).lml~bk.he.ga.lml ~bk.me.m([0-9][0-9]).d([0-9][0-9]).lml ~bk.oc.m([1-8]).d([1-7]).lml~bk\.eu.*\.lml~se\.m(10|11|12)\.d([0-9][0-9])\..*.lml`,
			Path:         "/site/build/templates/patterns",
			ValueType:    propertyValueTypes.StringSlice,
		}
	case SiteBuildTemplatesPatternsSelected:
		return &PropertyData{
			Description:  "indexes of selected template patterns separated by commas, e.g. 2,4.  Indexes start with zero, not 1.",
			DefaultValue: "0,11",
			Path:         "/site/build/templates/patterns/selected",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildUseExternalTemplates:
		return &PropertyData{
			Description:  "If true, the site will be built from external templates, that is in doxa/sites/{your site}/templates directory.  If false, the templates will be read from the database templates directory.",
			DefaultValue: "true",
			Path:         "/site/build/templates/external",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SiteBuildVersionPrefix:
		return &PropertyData{
			Description:  "When sources are cited inline, what, if any, characters should be printed before",
			DefaultValue: "[",
			Path:         "/site/build/version.prefix",
			ValueType:    propertyValueTypes.String,
		}
	case SiteBuildVersionSuffix:
		return &PropertyData{
			Description:  "When sources are cited inline, what, if any, characters should be printed after",
			DefaultValue: "]",
			Path:         "/site/build/version.suffix",
			ValueType:    propertyValueTypes.String,
		}
	case Site:
		return &PropertyData{
			Description:  "Website wide properties.",
			DefaultValue: "",
			Path:         "/site",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case Sys:
		return &PropertyData{
			Description:  "System wide properties.",
			DefaultValue: "",
			Path:         "/sys",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SysBuildDesc:
		return &PropertyData{
			Description:  "Set these properties to indicate which site to use when the build command is run.  The site must exist in configs/sites.",
			DefaultValue: "",
			Path:         "/sys/build",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SysBuildConfig:
		return &PropertyData{
			Description:  "Configuration to use to build your site.",
			DefaultValue: "prod",
			Path:         "/sys/build/config",
			ValueType:    propertyValueTypes.String,
		}
	case SysBuildSite:
		return &PropertyData{
			Description:  "Short name of your site (no spaces).",
			DefaultValue: "doxa-liml",
			Path:         "/sys/build/site",
			ValueType:    propertyValueTypes.String,
		}
	case SysGlory:
		return &PropertyData{
			Description:  "Whenever you exit (quit) Doxa, it will give glory to God.  These properties control which language is used.",
			DefaultValue: "",
			Path:         "/sys/glory",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SysGloryGloryStrings:
		return &PropertyData{
			Description:  "These are the languages Doxa uses to give glory to God.",
			DefaultValue: "Δόξα Πατρὶ καὶ Υἱῷ καὶ Ἁγίῳ Πνεύματι, νῦν καὶ ἀεὶ καὶ εἰς τοὺς αἰῶνας τῶν αἰώνων!~Gloria al Padre, y al Hijo y al Espíritu Santo, perpetuamente, ahora y siempre y por los siglos de los siglos!~Utukufu kwa Baba na Mwana na Roho Mtakatifu, Daima, sasa na siku zote, hata milele na milele!~Glory to the Father and the Son and the Holy Spirit, both now and forever and unto the ages of ages!",
			Path:         "/sys/glory/gloryStrings",
			ValueType:    propertyValueTypes.StringSlice,
		}
	case SysGloryUse:
		return &PropertyData{
			Description:  "The number key of the Language to use when Doxa gives glory to God, e.g. 1. Leave off the leading zeroes.",
			DefaultValue: "1",
			Path:         "/sys/glory/use",
			ValueType:    propertyValueTypes.String,
		}
	case SysServer:
		return &PropertyData{
			Description:  "These are properties for running a web server on this machine.",
			DefaultValue: "",
			Path:         "/sys/server",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SysServerPortsApi:
		return &PropertyData{
			Description:  "http port for the doxa web api",
			DefaultValue: "8090",
			Path:         "/sys/server/ports/api",
			ValueType:    propertyValueTypes.String,
		}
	case SysServerPortsDoxa:
		return &PropertyData{
			Description:  "http port for the doxa web app",
			DefaultValue: "8080",
			Path:         "/sys/server/ports/doxa",
			ValueType:    propertyValueTypes.String,
		}
	case SysServerPortsMedia:
		return &PropertyData{
			Description:  "http port for the media server (if you run one)",
			DefaultValue: "8095",
			Path:         "/sys/server/ports/media",
			ValueType:    propertyValueTypes.String,
		}
	case SysServerPortsMessages:
		return &PropertyData{
			Description:  "http port for the messages server (used for Doxa to send messages to your web browser)",
			DefaultValue: "9000",
			Path:         "/sys/server/ports/messages",
			ValueType:    propertyValueTypes.String,
		}
	case SysServerPortsPublicSite:
		return &PropertyData{
			Description:  "http port for your generated public liturgical web site",
			DefaultValue: "8086",
			Path:         "/sys/server/ports/site/public",
			ValueType:    propertyValueTypes.String,
		}
	case SysServerPortsTestSite:
		return &PropertyData{
			Description:  "http port for your generated test liturgical web site",
			DefaultValue: "8085",
			Path:         "/sys/server/ports/site/test",
			ValueType:    propertyValueTypes.String,
		}
	case SysShell:
		return &PropertyData{
			Description:  "Properties used to configure how the Doxa shell works.",
			DefaultValue: "",
			Path:         "/sys/shell",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SysCmdAgesAresPath:
		return &PropertyData{
			Description:  "folder containing ares files",
			DefaultValue: "",
			Path:         "/sys/cmd/ages/ares/path",
			ValueType:    propertyValueTypes.String,
		}
	case SysCmdAgesMediaPath:
		return &PropertyData{
			Description:  "folder containing alwb media files",
			DefaultValue: "/Users/mac002/git/ages/atem/ages-alwb-templates/net.ages.liturgical.workbench.templates/media-maps",
			Path:         "/sys/cmd/ages/media/path",
			ValueType:    propertyValueTypes.String,
		}
	case SysRemoteMetaTemplate:
		return &PropertyData{
			Description:  "Settings to control whether Doxa will create meta-templates from specified DCS websites. Meta-templates can be used in the translation editor and to generate a different DCS website.",
			DefaultValue: "",
			DescOnly:     true,
			Path:         "/sys/remote/meta/template",
			ValueType:    propertyValueTypes.String,
		}
	case SysRemoteMetaTemplateCreate:
		return &PropertyData{
			Description:  "If set to true, each time Doxa is started, but only one time a day, it will create meta-templates from specified DCS websites. Meta-templates can be used in the translation editor and to generate a different DCS website.",
			DefaultValue: "true",
			Path:         "/sys/remote/meta/template/create",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysRemoteMetaTemplateUrls:
		return &PropertyData{
			Description:  "URLs of Digital Chant Stand (DCS) websites from which to create meta-templates. Meta-templates can be used in the translation editor and to generate a different DCS website. The meta-templates will be created each time Doxa is started, but only once each day.",
			DefaultValue: "https://dcs.goarch.org/goa/dcs/",
			Path:         "/sys/remote/meta/template/urls",
			ValueType:    propertyValueTypes.StringSlice,
		}
	case SysShellDb:
		return &PropertyData{
			Description:  "Properties for using the db command in the Doxa shell.",
			DefaultValue: "",
			Path:         "/sys/shell/db",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SysShellDbConcordancePaddingP1:
		return &PropertyData{
			Description:  "default: 4",
			DefaultValue: "4",
			Path:         "/sys/shell/db/concordance/Padding/P1",
			ValueType:    propertyValueTypes.String,
		}
	case SysShellDbConcordancePaddingP2:
		return &PropertyData{
			Description:  "default: 50",
			DefaultValue: "50",
			Path:         "/sys/shell/db/concordance/Padding/P2",
			ValueType:    propertyValueTypes.String,
		}
	case SysShellDbConcordancePaddingP3:
		return &PropertyData{
			Description:  "default: 0",
			DefaultValue: "0",
			Path:         "/sys/shell/db/concordance/Padding/P3",
			ValueType:    propertyValueTypes.String,
		}
	case SysShellDbConcordanceSort:
		return &PropertyData{
			Description:  "Indicates which part of concordance line to use for sorting. Values are: id, left, right.",
			DefaultValue: "right",
			Path:         "/sys/shell/db/concordance/sort",
			ValueType:    propertyValueTypes.String,
		}
	case SysShellDbConcordanceWidth:
		return &PropertyData{
			Description:  "width of a concordance line. default: 30",
			DefaultValue: "30",
			Path:         "/sys/shell/db/concordance/width",
			ValueType:    propertyValueTypes.String,
		}
	case SysShellDbEditor:
		return &PropertyData{
			Description:  "Indicate the editor to use within the shell to edit a database record value. On Windows, for example, it can be notepad.  On a Mac: nano, vi, emacs, vim, whatever is installed.",
			DefaultValue: "vim",
			Path:         "/sys/shell/db/editor",
			ValueType:    propertyValueTypes.String,
		}
	case SysShellDbFindEmpty:
		return &PropertyData{
			Description:  "Include records whose value is empty when using find command?",
			DefaultValue: "true",
			Path:         "/sys/shell/db/find/empty",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysShellDbFindExact:
		return &PropertyData{
			Description:  "Match punctuation, case, and accents when using find command?",
			DefaultValue: "false",
			Path:         "/sys/shell/db/find/exact",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysShellDbFindWholeword:
		return &PropertyData{
			Description:  "Match whole word when using find command?",
			DefaultValue: "false",
			Path:         "/sys/shell/db/find/wholeword",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysShellDbHyphen:
		return &PropertyData{
			Description:  "Settings that control hyphenation of words.",
			DefaultValue: "",
			Path:         "/sys/shell/db/hyphen",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SysShellDbHyphenPunctuationChars:
		return &PropertyData{
			Description:  "Characters used for punctuation.",
			DefaultValue: ".,;?!·:'\"`<>(){}",
			Path:         "/sys/shell/db/hyphen/punctuationChars",
			ValueType:    propertyValueTypes.String,
		}
	case SysShellDbHyphenScansionChars:
		return &PropertyData{
			Description:  "Characters used for scansions (meter separators).",
			DefaultValue: "*/",
			Path:         "/sys/shell/db/hyphen/scansionChars",
			ValueType:    propertyValueTypes.String,
		}
	case SysShellDbHyphenKeepPunct:
		return &PropertyData{
			Description:  "When hyphenating, keep punctuation?",
			DefaultValue: "true",
			Path:         "/sys/shell/db/hyphen/keepPunctuation",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysShellDbHyphenKeepScansion:
		return &PropertyData{
			Description:  "When hyphenating, keep scansions? A scansion is a character marking boundaries between metered text.",
			DefaultValue: "false",
			Path:         "/sys/shell/db/hyphen/keepScansion",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysShellDbShortPrompt:
		return &PropertyData{
			Description:  "Use a shortened version of the command line prompt?",
			DefaultValue: "true",
			Path:         "/sys/shell/db/shortPrompt",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysShellDbShowHidden:
		return &PropertyData{
			Description:  "Show hidden (system) directories when using the ls command?",
			DefaultValue: "false",
			Path:         "/sys/shell/db/showHidden",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysShellDbShowHints:
		return &PropertyData{
			Description:  "Show hints?",
			DefaultValue: "true",
			Path:         "/sys/shell/db/showHints",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysShellTty:
		return &PropertyData{
			Description:  "In some situations on Windows, the Doxa shell suggestion prompts do not work correctly. If you set tty to true, it will use plain tty output.",
			DefaultValue: "false",
			Path:         "/sys/shell/tty",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysSynch:
		return &PropertyData{
			Description:  "The directory `synch` contains the github or gitlab clone urls to synchronize cloud repositories with the Doxa database",
			DefaultValue: "",
			Path:         "/sys/synch",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SysSynchBtx:
		return &PropertyData{
			Description:  "The directory `btx` contains properties for online biblical text repositories used to populate the Doxa database.  These are used for research and are not included in liturgical texts. If you set push to true for a repository, when you run the synch command, Doxa will push the values from the database to that repository if you have authorization to update it.",
			DefaultValue: "",
			Path:         "/sys/synch/btx",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SysSynchLtxReposAlwbScripture:
		return &PropertyData{
			Description:  "a variety of lectionary libraries from ALWB, which will be removed after ALWB is replaced by doxa.",
			DefaultValue: "https://github.com/AGES-Initiatives/ages-alwb-scripture.git",
			Path:         "/sys/synch/ltx/repos/en_us_alwbScripture",
			ValueType:    propertyValueTypes.String,
		}

	case SysSynchBtxPush:
		return &PropertyData{
			Description:  "If set to true, the push properties for each repository will be checked to see if that repository should be updated from the database.  Use the push property here to turn off push for all repositories.  You can only push if you have authorization for that repository on github or gitlab.",
			DefaultValue: "false",
			Path:         "/sys/synch/btx/push",
			ValueType:    propertyValueTypes.Boolean,
		}

	case SysSynchBtxReposEn_uk_kjv:
		return &PropertyData{
			Description:  "King James Version",
			DefaultValue: "https://gitlab.com/ocmc/bible/en_uk_kjv.git",
			Path:         "/sys/synch/btx/repos/en_uk_kjv",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchBtxReposEn_uk_kjvPush:
		return &PropertyData{
			Description:  "include when pushing to repo?",
			DefaultValue: "false",
			Path:         "/sys/synch/btx/repos/en_uk_kjv/push",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysSynchBtxReposEn_uk_webbe:
		return &PropertyData{
			Description:  "World English Bible, British Edition",
			DefaultValue: "https://gitlab.com/ocmc/bible/en_uk_webbe.git",
			Path:         "/sys/synch/btx/repos/en_uk_webbe",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchBtxReposEn_uk_webbePush:
		return &PropertyData{
			Description:  "include when pushing to repo?",
			DefaultValue: "false",
			Path:         "/sys/synch/btx/repos/en_uk_webbe/push",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysSynchBtxReposGr_gr_cog:
		return &PropertyData{
			Description:  "Greek Old Testament (LXX) and Patriarchal New Testament",
			DefaultValue: "https://gitlab.com/ocmc/bible/gr_gr_cog.git",
			Path:         "/sys/synch/btx/repos/gr_gr_cog",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchBtxReposGr_gr_cogPush:
		return &PropertyData{
			Description:  "include when pushing to repo?",
			DefaultValue: "false",
			Path:         "/sys/synch/btx/repos/gr_gr_cog/push",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysSynchLtx:
		return &PropertyData{
			Description:  "The directory `ltx` contains properties for online liturgical text repositories used to populate the Doxa database. If you set push to true for a repository, when you run the synch command, Doxa will push the values from the database to that repository if you have authorization to update it.",
			DefaultValue: "",
			Path:         "/sys/synch/ltx",
			ValueType:    propertyValueTypes.String,
			DescOnly:     true,
		}
	case SysSynchLtxPush:
		return &PropertyData{
			Description:  "If set to true, the push properties for each repository will be checked to see if that repository should be updated from the database.  Use the push property here to turn off push for all repositories.  You can only push if you have authorization for that repository on github or gitlab.",
			DefaultValue: "false",
			Path:         "/sys/synch/ltx/push",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysSynchLtxReposEn_us_aana:
		return &PropertyData{
			Description:  "Translations of liturgical texts in English by the Antiochian Orthodox Christian Archdiocese of North America",
			DefaultValue: "https://github.com/AGES-Initiatives/ages-alwb-library-en-us-aana.git",
			Path:         "/sys/synch/ltx/repos/en_us_aana",
			ValueType:    propertyValueTypes.String,
		}

	case SysSynchLtxReposEn_uk_ware:
		return &PropertyData{
			Description:  "Translations of liturgical texts in English by Metropolitan Kallistos (Ware)",
			DefaultValue: "https://github.com/AGES-Initiatives/ages-alwb-library-en-uk-ware.git",
			Path:         "/sys/synch/ltx/repos/en_uk_ware",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchLtxReposEn_uk_warePush:
		return &PropertyData{
			Description:  "include when pushing to repo?",
			DefaultValue: "false",
			Path:         "/sys/synch/ltx/repos/en_uk_ware/push",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysSynchLtxReposEn_us_acook:
		return &PropertyData{
			Description:  "Translations of liturgical texts in English by Fr. Anthony Cook",
			DefaultValue: "https://github.com/AGES-Initiatives/ages-alwb-library-en-us-acook.git",
			Path:         "/sys/synch/ltx/repos/en_us_acook",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchLtxReposEn_us_acookPush:
		return &PropertyData{
			Description:  "include when pushing to repo?",
			DefaultValue: "false",
			Path:         "/sys/synch/ltx/repos/en_us_acook/push",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysSynchLtxReposEn_us_andronache:
		return &PropertyData{
			Description:  "Translations of liturgical texts in English by Virgil Peter Andronache",
			DefaultValue: "https://github.com/AGES-Initiatives/ages-alwb-library-en-us-andronache.git",
			Path:         "/sys/synch/ltx/repos/en_us_andronache",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchLtxReposEn_us_andronachePush:
		return &PropertyData{
			Description:  "include when pushing to repo?",
			DefaultValue: "false",
			Path:         "/sys/synch/ltx/repos/en_us_andronache/push",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysSynchLtxReposEn_us_barrett:
		return &PropertyData{
			Description:  "Translations of liturgical texts in English by Richard Barrett",
			DefaultValue: "https://github.com/AGES-Initiatives/ages-alwb-library-en-us-barrett.git",
			Path:         "/sys/synch/ltx/repos/en_us_barrett",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchLtxReposEn_us_barrettPush:
		return &PropertyData{
			Description:  "include when pushing to repo?",
			DefaultValue: "false",
			Path:         "/sys/synch/ltx/repos/en_us_barrett/push",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysSynchLtxReposEn_us_carroll:
		return &PropertyData{
			Description:  "Translations of liturgical texts in English by Thomas Carroll",
			DefaultValue: "https://github.com/AGES-Initiatives/ages-alwb-library-en-us-carroll.git",
			Path:         "/sys/synch/ltx/repos/en_us_carroll",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchLtxReposEn_us_carrollPush:
		return &PropertyData{
			Description:  "include when pushing to repo?",
			DefaultValue: "false",
			Path:         "/sys/synch/ltx/repos/en_us_carroll/push",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysSynchLtxReposEn_us_dedes:
		return &PropertyData{
			Description:  "Translations of liturgical texts in English by Fr. Seraphim Dedes",
			DefaultValue: "https://github.com/AGES-Initiatives/ages-alwb-library-en-us-dedes.git",
			Path:         "/sys/synch/ltx/repos/en_us_dedes",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchLtxReposEn_us_dedesPush:
		return &PropertyData{
			Description:  "include when pushing to repo?",
			DefaultValue: "false",
			Path:         "/sys/synch/ltx/repos/en_us_dedes/push",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysSynchLtxReposEn_us_goaDedes:
		return &PropertyData{
			Description:  "Translations of liturgical texts in English by Fr. Seraphim Dedes for the Greek Archdiocese of America",
			DefaultValue: "https://github.com/AGES-Initiatives/alwb-library-en-us-goadedes.git",
			Path:         "/sys/synch/ltx/repos/en_us_goadedes",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchLtxReposEn_us_goaDedesPush:
		return &PropertyData{
			Description:  "include when pushing to repo?",
			DefaultValue: "false",
			Path:         "/sys/synch/ltx/repos/en_us_goadedes/push",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysSynchLtxReposEn_us_duvall:
		return &PropertyData{
			Description:  "Translations of liturgical texts in English by George K. Duvall",
			DefaultValue: "https://github.com/AGES-Initiatives/ages-alwb-library-en-us-duvall.git",
			Path:         "/sys/synch/ltx/repos/en_us_duvall",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchLtxReposEn_us_duvallPush:
		return &PropertyData{
			Description:  "include when pushing to repo?",
			DefaultValue: "false",
			Path:         "/sys/synch/ltx/repos/en_us_duvall/push",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysSynchLtxReposEn_us_goa:
		return &PropertyData{
			Description:  "Translations of liturgical texts in English by the Greek Orthodox Archdiocese of America",
			DefaultValue: "https://github.com/AGES-Initiatives/ages-alwb-library-en-us-goa.git",
			Path:         "/sys/synch/ltx/repos/en_us_goa",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchLtxReposEn_us_goaPush:
		return &PropertyData{
			Description:  "include when pushing to repo?",
			DefaultValue: "false",
			Path:         "/sys/synch/ltx/repos/en_us_goa/push",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysSynchLtxReposEn_us_holycross:
		return &PropertyData{
			Description:  "Translations of liturgical texts in English by Holy Cross Greek Orthodox School of Theology",
			DefaultValue: "https://github.com/AGES-Initiatives/ages-alwb-library-en-us-holycross.git",
			Path:         "/sys/synch/ltx/repos/en_us_holycross",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchLtxReposEn_us_holycrossPush:
		return &PropertyData{
			Description:  "include when pushing to repo?",
			DefaultValue: "false",
			Path:         "/sys/synch/ltx/repos/en_us_holycross/push",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysSynchLtxReposEn_us_houpos:
		return &PropertyData{
			Description:  "Translations of liturgical texts in English by Fr Andreas Houpos",
			DefaultValue: "https://github.com/AGES-Initiatives/ages-alwb-library-en-us-houpos.git",
			Path:         "/sys/synch/ltx/repos/en_us_houpos",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchLtxReposEn_us_houposPush:
		return &PropertyData{
			Description:  "include when pushing to repo?",
			DefaultValue: "false",
			Path:         "/sys/synch/ltx/repos/en_us_houpos/push",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysSynchLtxReposEn_us_oca:
		return &PropertyData{
			Description:  "Translations of liturgical texts in English by the Orthodox Church in America",
			DefaultValue: "https://github.com/AGES-Initiatives/ages-alwb-library-en-us-oca.git",
			Path:         "/sys/synch/ltx/repos/en_us_oca",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchLtxReposEn_us_ocaPush:
		return &PropertyData{
			Description:  "include when pushing to repo?",
			DefaultValue: "false",
			Path:         "/sys/synch/ltx/repos/en_us_oca/push",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysSynchLtxReposEn_us_public:
		return &PropertyData{
			Description:  "This library contains every ID found in gr_gr_cog, but redirects to other libraries.",
			DefaultValue: "https://github.com/AGES-Initiatives/ages-alwb-library-client-enpublic.git",
			Path:         "/sys/synch/ltx/repos/en_us_public",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchLtxReposEn_redirects_goarch:
		return &PropertyData{
			Description:  "This library contains English language redirects for the Greek Orthodox Archdiocese of America (GOARCH). It replaces en_us_public.",
			DefaultValue: "https://github.com/AGES-Initiatives/en-redirects-goarch.git",
			Path:         "/sys/synch/ltx/repos/en_redirects_goarch",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchLtxReposEn_us_publicPush:
		return &PropertyData{
			Description:  "include when pushing to repo?",
			DefaultValue: "false",
			Path:         "/sys/synch/ltx/repos/en_us_public/push",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysSynchLtxReposEn_us_roumas:
		return &PropertyData{
			Description:  "Translations of liturgical texts in English by Nicholas Roumas",
			DefaultValue: "https://github.com/AGES-Initiatives/alwb-library-en-us-roumas.git",
			Path:         "/sys/synch/ltx/repos/en_us_roumas",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchLtxReposEn_us_roumasPush:
		return &PropertyData{
			Description:  "include when pushing to repo?",
			DefaultValue: "false",
			Path:         "/sys/synch/ltx/repos/en_us_roumas/push",
			ValueType:    propertyValueTypes.Boolean,
		}
	case SysSynchLtxReposEn_us_unknown:
		return &PropertyData{
			Description:  "Translations of liturgical texts in English by unknown individuals",
			DefaultValue: "https://github.com/AGES-Initiatives/ages-alwb-library-en-us-unknown.git",
			Path:         "/sys/synch/ltx/repos/en_us_unknown",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchLtxReposEn_us_unknownPush:
		return &PropertyData{
			Description:  "include when pushing to repo?",
			DefaultValue: "false",
			Path:         "/sys/synch/ltx/repos/en_us_unknown/push",
			ValueType:    propertyValueTypes.Boolean,
		}

	case SysSynchLtxReposGr_us_goa:
		return &PropertyData{
			Description:  "GOA Greek liturgical texts",
			DefaultValue: "https://github.com/AGES-Initiatives/alwb-library-gr-us-goa.git",
			Path:         "/sys/synch/ltx/repos/gr_us_goa",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchLtxReposGr_gr_cog:
		return &PropertyData{
			Description:  "Commonly used Greek liturgical texts",
			DefaultValue: "https://github.com/AGES-Initiatives/ages-alwb-library-gr-gr-cog.git",
			Path:         "/sys/synch/ltx/repos/gr_gr_cog",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchLtxReposEn_us_system:
		return &PropertyData{
			Description:  "alwb system ares properties, to be removed after conversion to doxa",
			DefaultValue: "https://github.com/AGES-Initiatives/ages-alwb-system.git",
			Path:         "/sys/synch/ltx/repos/en_us_system",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchLtxReposSwh_ke_oak:
		return &PropertyData{
			Description:  "Kiswahili for Kenya",
			DefaultValue: "https://gitlab.com/ocmc/ares/olw/swh_ke_oak.git",
			Path:         "/sys/synch/ltx/repos/swh_ke_oak",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchLtxReposFra_fr_oaf:
		return &PropertyData{
			Description:  "French for East Africa",
			DefaultValue: "https://gitlab.com/ocmc/ares/olw/fra_fr_oaf.git",
			Path:         "/sys/synch/ltx/repos/fra_fr_oaf",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchLtxReposGr_redirects_goarch:
		return &PropertyData{
			Description:  "Greek redirects for GOA",
			DefaultValue: "https://github.com/AGES-Initiatives/gr-redirects-goarch.git",
			Path:         "/sys/synch/ltx/repos/gr_redirects_goarch",
			ValueType:    propertyValueTypes.String,
		}
	case SysSynchLtxReposEn_us_mlgs:
		return &PropertyData{
			Description:  "Greek redirects for MLGS",
			DefaultValue: "https://github.com/AGES-Initiatives/library-en-us-mlgs.git",
			Path:         "/sys/synch/ltx/repos/en_us_mlgs",
			ValueType:    propertyValueTypes.String,
		}

	default:
		return nil
	}
}
func (i Property) BoolValue() (bool, error) {
	return strconv.ParseBool(i.Data().DefaultValue)
}
func (i Property) FloatValue() (float64, error) {
	return strconv.ParseFloat(i.Data().DefaultValue, 64)
}
func (i Property) IntValue() (int, error) {
	return strconv.Atoi(i.Data().DefaultValue)
}
func (i Property) StringValue() (string, error) {
	data := i.Data()
	switch data.ValueType {
	case propertyValueTypes.String:
		return data.DefaultValue, nil
	}
	return "", errors.New("not a string value")
}
func (i Property) StringSliceValue() ([]string, error) {
	data := i.Data()
	switch data.ValueType {
	case propertyValueTypes.StringSlice:
		var s []string
		parts := strings.Split(data.DefaultValue, "~")
		for _, part := range parts {
			s = append(s, part)
		}
		return s, nil
	}
	return nil, errors.New("not a string slice value")
}

type PropertyData struct {
	Description  string
	DefaultValue string
	Path         string
	ValueType    propertyValueTypes.PropertyType
	DescOnly     bool
}

// LangComboData completes the path for an Epistle, Gospel, Liturgical, Prophet, or Psalter.
// Do not prefix or suffix number or column with a slash.
// Valid position values are col1, col2, col3, i.e. column names.
func (i Property) LangComboData(number string, position columns.Column) (*PropertyData, error) {
	switch i {
	case Epistle, Gospel, Liturgical, Prophet, Psalter:
		data := i.Data()
		data.Path = "/site/build/lang.combos/" + number + "/" + position.Name() + data.Path
		return data, nil
	default:
		return nil, fmt.Errorf("not a language combo property")
	}
}

// LangComboProperties gives the subset of properties
// that are used as LangCombos. The returned slice
// can be used as with a range statement.
func LangComboProperties() []Property {
	values := []Property{
		Epistle,
		Gospel,
		Liturgical,
		Prophet,
		Psalter,
	}
	return values
}

// PropertyMap provides a key-value map if the Property is for a map.
// If it is not for a map, an error is returned.
func (i Property) PropertyMap() (map[string]string, error) {
	propData := i.Data()
	switch propData.ValueType {
	case propertyValueTypes.StringMapString:
		switch i {
		case SiteBuildPagesTitles:
			return getTitlesMap(), nil
		case SiteBuildLayoutsColumns:
			return getLayoutColumnsMap(), nil
		default:
			return nil, fmt.Errorf("%s missing map function", propData.Path)
		}
	default:
		return nil, fmt.Errorf("%s not a map property", propData.Path)
	}
}

// getLayoutColumnsMap provides key-value pairs, where key == ISO language code, and value == Json string for libraries to use in column
func getLayoutColumnsMap() map[string]string {
	return map[string]string{"en": `{"Acronym":"en","LiturgicalLibs":[{"Primary":"en_redirects_goarch","FallBacks":null}],"EpistleLibs":[{"Primary":"en_us_rsv","FallBacks":["en_us_nkjv"]}],"GospelLibs":[{"Primary":"en_us_rsv","FallBacks":["en_us_nkjv"]}],"ProphetLibs":[{"Primary":"en_us_saas","FallBacks":["en_uk_lash"]}],"PsalterLibs":[{"Primary":"en_us_saas","FallBacks":["en_uk_lash"]}],"MediaLibs":null,"TitleLibPrimary":0,"TitleLibSecondary":0,"DateFormat":"%A, %B %d, %Y","WeekDayFormat":"%A"}`,
		"gr": `{"Acronym":"gr","LiturgicalLibs":[{"Primary":"gr_redirects_goarch","FallBacks":null}],"EpistleLibs":[{"Primary":"gr_gr_cog","FallBacks":null}],"GospelLibs":[{"Primary":"gr_gr_cog","FallBacks":null}],"ProphetLibs":[{"Primary":"gr_gr_cog","FallBacks":null}],"PsalterLibs":[{"Primary":"gr_gr_cog","FallBacks":null}],"MediaLibs":null,"TitleLibPrimary":0,"TitleLibSecondary":0,"DateFormat":"%A, %d %B %Y","WeekDayFormat":"%A"}`,
	}
}

// getTitlesMap provides key-value pairs to convert filename abbreviations to titles
// abbreviations must be lowercase
func getTitlesMap() map[string]string {
	return map[string]string{
		"a":                                 "automela",
		"actors":                            "Actors",
		"adv":                               "Advent",
		"aff":                               "Post-Feast",
		"anoixantaria":                      "Anoixantaria",
		"ap":                                "ap",
		"apostle":                           "Apostle",
		"apr":                               "April",
		"archbishop":                        "Archbishop",
		"aug":                               "August",
		"automobile":                        "Automobile",
		"baptism":                           "Baptism ",
		"beforeliturgy":                     "Before Liturgy",
		"basil":                             "St. Basil the Great",
		"bible":                             "Bible",
		"bk":                                "Book Index",
		"blessingofloaves":                  "Blessing of Loaves",
		"bodes":                             "Bodes",
		"brightweek":                        "Bright Week",
		"buildingfoundation":                "Building Foundation",
		"calendar":                          "Calendar",
		"can":                               "Can",
		"childbirth":                        "Childbirth",
		"churchdooropening":                 "Church Opening",
		"client":                            "Client",
		"co":                                "Evening Compline ",
		"co1":                               "Pre-Resurrection Service ",
		"co2":                               "Small Compline and Salutations to the Theotokos",
		"co3":                               "Small Compline and Akathist Hymn",
		"co4":                               "Small Compline and Great Canon",
		"co5":                               "Pre-Resurrection - Abridged",
		"co6":                               "Paschal Compline",
		"co7":                               "Vigil: Great Compline, Lity, End of Vespers",
		"consecration":                      "Consecration",
		"consecration_liturgy":              "Consecration Liturgy ",
		"consecration_relics":               "Reception of Relics ",
		"consecration_vespers":              "Consecration Vespers ",
		"chrysbasil":                        "St. John Chrysostom and St. Basil the Great",
		"chrysostom":                        "St. John Chrysostom",
		"cu":                                "Custom Index",
		"d0":                                "Day 0",
		"d001":                              "Day 001",
		"d002":                              "Day 002",
		"d003":                              "Day 003",
		"d004":                              "Day 004",
		"d005":                              "Day 005",
		"d006":                              "Day 006",
		"d007":                              "Day 007",
		"d008":                              "Day 008",
		"d009":                              "Day 009",
		"d00P":                              "Day 00P",
		"d01":                               "Day 01",
		"d010":                              "Day 010",
		"d011":                              "Day 011",
		"d012":                              "Day 012",
		"d013":                              "Day 013",
		"d014":                              "Day 014",
		"d015":                              "Day 015",
		"d016":                              "Day 016",
		"d017":                              "Day 017",
		"d018":                              "Day 018",
		"d019":                              "Day 019",
		"d02":                               "Day 02",
		"d020":                              "Day 020",
		"d021":                              "Day 021",
		"d022":                              "Day 022",
		"d023":                              "Day 023",
		"d024":                              "Day 024",
		"d025":                              "Day 025",
		"d026":                              "Day 026",
		"d027":                              "Day 027",
		"d028":                              "Day 028",
		"d029":                              "Day 029",
		"d03":                               "Day 03",
		"d030":                              "Day 030",
		"d031":                              "Day 031",
		"d032":                              "Day 032",
		"d033":                              "Day 033",
		"d034":                              "Day 034",
		"d035":                              "Day 035",
		"d036":                              "Day 036",
		"d037":                              "Day 037",
		"d038":                              "Day 038",
		"d039":                              "Day 039",
		"d04":                               "Day 04",
		"d040":                              "Day 040",
		"d041":                              "Day 041",
		"d042":                              "Day 042",
		"d043":                              "Day 043",
		"d044":                              "Day 044",
		"d045":                              "Day 045",
		"d046":                              "Day 046",
		"d047":                              "Day 047",
		"d048":                              "Day 048",
		"d049":                              "Day 049",
		"d05":                               "Day 05",
		"d050":                              "Day 0510",
		"d051":                              "Day 051",
		"d052":                              "Day 052",
		"d053":                              "Day 053",
		"d054":                              "Day 054",
		"d055":                              "Day 055",
		"d056":                              "Day 056",
		"d057":                              "Day 057",
		"d058":                              "Day 058",
		"d059":                              "Day 059",
		"d06":                               "Day 06",
		"d060":                              "Day 060",
		"d061":                              "Day 061",
		"d062":                              "Day 062",
		"d063":                              "Day 063",
		"d064":                              "Day 064",
		"d065":                              "Day 065",
		"d066":                              "Day 066",
		"d067":                              "Day 067",
		"d068":                              "Day 068",
		"d069":                              "Day 069",
		"d07":                               "Day 07",
		"d070":                              "Day 070",
		"d071":                              "Day 071",
		"d072":                              "Day 072",
		"d073":                              "Day 073",
		"d074":                              "Day 074",
		"d075":                              "Day 075",
		"d076":                              "Day 076",
		"d077":                              "Day 077",
		"d078":                              "Day 078",
		"d079":                              "Day 079",
		"d08":                               "Day 08",
		"d080":                              "Day 080",
		"d081":                              "Day 081",
		"d082":                              "Day 082",
		"d083":                              "Day 083",
		"d084":                              "Day 084",
		"d085":                              "Day 085",
		"d086":                              "Day 086",
		"d087":                              "Day 087",
		"d088":                              "Day 088",
		"d089":                              "Day 089",
		"d09":                               "Day 09",
		"d090":                              "Day 090",
		"d091":                              "Day 091",
		"d092":                              "Day 092",
		"d093":                              "Day 093",
		"d094":                              "Day 094",
		"d095":                              "Day 095",
		"d096":                              "Day 096",
		"d097":                              "Day 097",
		"d098":                              "Day 098",
		"d099":                              "Day 099",
		"d1":                                "Day 1",
		"d10":                               "Day 10",
		"d100":                              "Day 100",
		"d101":                              "Day 101",
		"d102":                              "Day 102",
		"d103":                              "Day 103",
		"d104":                              "Day 104",
		"d105":                              "Day 105",
		"d106":                              "Day 106",
		"d107":                              "Day 107",
		"d108":                              "Day 108",
		"d109":                              "Day 109",
		"d11":                               "Day 11",
		"d110":                              "Day 110",
		"d111":                              "Day 111",
		"d112":                              "Day 112",
		"d113":                              "Day 113",
		"d114":                              "Day 114",
		"d115":                              "Day 115",
		"d116":                              "Day 116",
		"d117":                              "Day 117",
		"d118":                              "Day 118",
		"d119":                              "Day 119",
		"d12":                               "Day 012",
		"d120":                              "Day 0120",
		"d121":                              "Day 0121",
		"d122":                              "Day 0122",
		"d123":                              "Day 0123",
		"d124":                              "Day 0124",
		"d125":                              "Day 0125",
		"d126":                              "Day 0126",
		"d127":                              "Day 0127",
		"d128":                              "Day 0128",
		"d129":                              "Day 0129",
		"d13":                               "Day 13",
		"d130":                              "Day 130",
		"d131":                              "Day 131",
		"d132":                              "Day 132",
		"d133":                              "Day 133",
		"d134":                              "Day 134",
		"d135":                              "Day 135",
		"d136":                              "Day 136",
		"d137":                              "Day 137",
		"d138":                              "Day 138",
		"d139":                              "Day 139",
		"d14":                               "Day 14",
		"d140":                              "Day 140",
		"d141":                              "Day 141",
		"d142":                              "Day 142",
		"d143":                              "Day 143",
		"d144":                              "Day 144",
		"d145":                              "Day 145",
		"d146":                              "Day 146",
		"d147":                              "Day 147",
		"d148":                              "Day 148",
		"d149":                              "Day 149",
		"d15":                               "Day 15",
		"d150":                              "Day 150",
		"d151":                              "Day 151",
		"d152":                              "Day 152",
		"d153":                              "Day 153",
		"d154":                              "Day 154",
		"d155":                              "Day 155",
		"d156":                              "Day 156",
		"d157":                              "Day 157",
		"d158":                              "Day 158",
		"d159":                              "Day 159",
		"d16":                               "Day 16",
		"d160":                              "Day 160",
		"d161":                              "Day 161",
		"d162":                              "Day 162",
		"d163":                              "Day 163",
		"d164":                              "Day 164",
		"d165":                              "Day 165",
		"d166":                              "Day 166",
		"d167":                              "Day 167",
		"d168":                              "Day 168",
		"d169":                              "Day 169",
		"d17":                               "Day 17",
		"d170":                              "Day 170",
		"d171":                              "Day 171",
		"d172":                              "Day 172",
		"d173":                              "Day 173",
		"d174":                              "Day 174",
		"d175":                              "Day 175",
		"d176":                              "Day 176",
		"d177":                              "Day 177",
		"d178":                              "Day 178",
		"d179":                              "Day 179",
		"d18":                               "Day 18",
		"d180":                              "Day 180",
		"d181":                              "Day 181",
		"d182":                              "Day 182",
		"d183":                              "Day 183",
		"d184":                              "Day 184",
		"d185":                              "Day 185",
		"d186":                              "Day 186",
		"d187":                              "Day 187",
		"d188":                              "Day 188",
		"d189":                              "Day 189",
		"d19":                               "Day 19",
		"d190":                              "Day 190",
		"d191":                              "Day 191",
		"d192":                              "Day 192",
		"d193":                              "Day 193",
		"d194":                              "Day 194",
		"d195":                              "Day 195",
		"d196":                              "Day 196",
		"d197":                              "Day 197",
		"d198":                              "Day 198",
		"d199":                              "Day 199",
		"d2":                                "Day 2",
		"d20":                               "Day 20",
		"d200":                              "Day 200",
		"d201":                              "Day 201",
		"d202":                              "Day 202",
		"d203":                              "Day 203",
		"d204":                              "Day 204",
		"d205":                              "Day 205",
		"d206":                              "Day 206",
		"d207":                              "Day 207",
		"d208":                              "Day 208",
		"d209":                              "Day 209",
		"d20b":                              "Day 20b",
		"d21":                               "Day 21",
		"d210":                              "Day 210",
		"d211":                              "Day 211",
		"d212":                              "Day 212",
		"d213":                              "Day 213",
		"d214":                              "Day 214",
		"d215":                              "Day 215",
		"d216":                              "Day 216",
		"d217":                              "Day 217",
		"d218":                              "Day 218",
		"d219":                              "Day 219",
		"d22":                               "Day 22",
		"d220":                              "Day 220",
		"d221":                              "Day 221",
		"d222":                              "Day 222",
		"d223":                              "Day 223",
		"d224":                              "Day 224",
		"d225":                              "Day 225",
		"d226":                              "Day 226",
		"d227":                              "Day 227",
		"d228":                              "Day 228",
		"d229":                              "Day 229",
		"d23":                               "Day 23",
		"d230":                              "Day 230",
		"d231":                              "Day 231",
		"d232":                              "Day 232",
		"d233":                              "Day 233",
		"d234":                              "Day 234",
		"d235":                              "Day 235",
		"d236":                              "Day 236",
		"d237":                              "Day 237",
		"d238":                              "Day 238",
		"d239":                              "Day 239",
		"d24":                               "Day 24",
		"d240":                              "Day 240",
		"d241":                              "Day 241",
		"d242":                              "Day 242",
		"d243":                              "Day 243",
		"d244":                              "Day 244",
		"d245":                              "Day 245",
		"d246":                              "Day 246",
		"d247":                              "Day 247",
		"d248":                              "Day 248",
		"d249":                              "Day 249",
		"d25":                               "Day 25",
		"d250":                              "Day 250",
		"d251":                              "Day 251",
		"d252":                              "Day 252",
		"d253":                              "Day 253",
		"d254":                              "Day 254",
		"d255":                              "Day 255",
		"d256":                              "Day 256",
		"d257":                              "Day 257",
		"d258":                              "Day 258",
		"d259":                              "Day 259",
		"d26":                               "Day 26",
		"d260":                              "Day 260",
		"d261":                              "Day 261",
		"d262":                              "Day 262",
		"d263":                              "Day 263",
		"d264":                              "Day 264",
		"d265":                              "Day 265",
		"d266":                              "Day 266",
		"d267":                              "Day 267",
		"d268":                              "Day 268",
		"d269":                              "Day 269",
		"d27":                               "Day 27",
		"d270":                              "Day 270",
		"d271":                              "Day 271",
		"d272":                              "Day 272",
		"d273":                              "Day 273",
		"d274":                              "Day 274",
		"d275":                              "Day 275",
		"d276":                              "Day 276",
		"d277":                              "Day 277",
		"d278":                              "Day 278",
		"d279":                              "Day 279",
		"d28":                               "Day 28",
		"d280":                              "Day 280",
		"d281":                              "Day 281",
		"d282":                              "Day 282",
		"d283":                              "Day 283",
		"d284":                              "Day 284",
		"d285":                              "Day 285",
		"d286":                              "Day 286",
		"d287":                              "Day 287",
		"d288":                              "Day 288",
		"d289":                              "Day 289",
		"d28a":                              "Day 28a",
		"d29":                               "Day 29",
		"d290":                              "Day 290",
		"d291":                              "Day 291",
		"d292":                              "Day 292",
		"d293":                              "Day 293",
		"d294":                              "Day 294",
		"d295":                              "Day 295",
		"d296":                              "Day 296",
		"d297":                              "Day 297",
		"d298":                              "Day 298",
		"d299":                              "Day 299",
		"d3":                                "Day 3",
		"d30":                               "Day 30",
		"d300":                              "Day 300",
		"d301":                              "Day 301",
		"d302":                              "Day 302",
		"d303":                              "Day 303",
		"d304":                              "Day 304",
		"d305":                              "Day 305",
		"d306":                              "Day 306",
		"d307":                              "Day 307",
		"d308":                              "Day 308",
		"d309":                              "Day 309",
		"d31":                               "Day 31",
		"d310":                              "Day 310",
		"d311":                              "Day 311",
		"d312":                              "Day 312",
		"d313":                              "Day 313",
		"d314":                              "Day 314",
		"d315":                              "Day 315",
		"d316":                              "Day 316",
		"d317":                              "Day 317",
		"d318":                              "Day 318",
		"d319":                              "Day 319",
		"d320":                              "Day 320",
		"d321":                              "Day 321",
		"d322":                              "Day 322",
		"d323":                              "Day 323",
		"d324":                              "Day 324",
		"d325":                              "Day 325",
		"d326":                              "Day 326",
		"d327":                              "Day 327",
		"d328":                              "Day 328",
		"d329":                              "Day 329",
		"d330":                              "Day 33",
		"d331":                              "Day 331",
		"d332":                              "Day 332",
		"d333":                              "Day 333",
		"d334":                              "Day 334",
		"d335":                              "Day 335",
		"d336":                              "Day 336",
		"d337":                              "Day 337",
		"d338":                              "Day 338",
		"d339":                              "Day 339",
		"d340":                              "Day 34",
		"d341":                              "Day 341",
		"d342":                              "Day 342",
		"d343":                              "Day 343",
		"d344":                              "Day 344",
		"d345":                              "Day 345",
		"d346":                              "Day 346",
		"d347":                              "Day 347",
		"d348":                              "Day 348",
		"d349":                              "Day 349",
		"d350":                              "Day 350",
		"d351":                              "Day 351",
		"d352":                              "Day 352",
		"d353":                              "Day 353",
		"d354":                              "Day 354",
		"d355":                              "Day 355",
		"d356":                              "Day 356",
		"d357":                              "Day 357",
		"d358":                              "Day 358",
		"d359":                              "Day 359",
		"d360":                              "Day 360",
		"d361":                              "Day 361",
		"d362":                              "Day 362",
		"d363":                              "Day 363",
		"d364":                              "Day 364",
		"d4":                                "Day 4",
		"d5":                                "Day 5",
		"d6":                                "Day 6",
		"d7":                                "Day 7",
		"d__":                               "Day",
		"d___":                              "Day",
		"da":                                "da",
		"dAC":                               "dAC",
		"dBC":                               "dBC",
		"dec":                               "December",
		"deacon":                            "Deacon",
		"deceased":                          "Deceased",
		"deceasedinfants":                   "Deceased Infants",
		"descriptors":                       "Descriptors",
		"dFF":                               "dFF",
		"dHF":                               "dHF",
		"dHS":                               "dHS",
		"dismissals":                        "Dismissals",
		"dooropening":                       "Door Opening",
		"dormitionencomia":                  "Dormition Encomia",
		"dSatAC":                            "dSatAC",
		"dSatAE":                            "dSatAE",
		"dSatBC":                            "dSatBC",
		"dSatBE":                            "dSatBE",
		"dSunAC":                            "dSunAC",
		"dSunAE":                            "dSunAE",
		"dSunBC":                            "dSunBC",
		"dSunBE":                            "dSunBE",
		"e01":                               "Eothinon 1",
		"e02":                               "Eothinon 2",
		"e03":                               "Eothinon 3",
		"e04":                               "Eothinon 4",
		"e05":                               "Eothinon 5",
		"e06":                               "Eothinon 6",
		"e07":                               "Eothinon 7",
		"e08":                               "Eothinon 8",
		"e09":                               "Eothinon 9",
		"e10":                               "Eothinon 10",
		"e11":                               "Eothinon 11",
		"ecloge":                            "ecloge",
		"em":                                "Matins (in the evening)",
		"em3":                               "Lamentations - Complete Text",
		"en":                                "English",
		"eo":                                "EOTHINA",
		"ep":                                "EPISTLES",
		"eu":                                "EUCHOLOGION",
		"exaposteilaria":                    "Exaposteilaria",
		"fatherconfessor":                   "Father Confessor",
		"feb":                               "February",
		"forabishop":                        "For a Bishop",
		"forahieromartyr":                   "For a Hierarch",
		"foramartyr":                        "For a Martyr",
		"foranapostle":                      "For an Apostle",
		"foranascetic":                      "For an Ascetic",
		"foraprophet":                       "For a Prophet",
		"forawomanascetic":                  "For a Woman Ascetic",
		"forawomanmartyr":                   "For a Woman Martyr",
		"fortwoormoreapostles":              "For Two or More Apostles",
		"fortwoormoremartyrs":               "For Two or More Martyrs",
		"fortwoormorewomenmartyrs":          "For Two or More Women Martyrs",
		"fortydays":                         "Forty Days",
		"fri":                               "Friday",
		"fra":                               "French",
		"funeral":                           "Funeral ",
		"ga":                                "General Apolytikia",
		"gc":                                "Great Compline",
		"ge":                                "ge",
		"gh":                                "Great Hours",
		"gm":                                "gm",
		"go":                                "GOSPELS",
		"goa":                               "GOA",
		"gr":                                "Greek",
		"greatwaterblessing":                "Great Water Blessing",
		"h":                                 "heirmoi",
		"h1":                                "First Hour",
		"h12":                               "Variable parts for Hours",
		"h13":                               "Paschal Hours",
		"h3":                                "Third Hour",
		"h36":                               "Trithekti (Third and Sixth Hours)",
		"h6":                                "Sixth Hour",
		"h61":                               "Typika Service (no priest)",
		"h9":                                "Ninth Hour",
		"h91":                               "Readings for the Day",
		"h92":                               "Veneration of the Cross (moved from end of Matins to end of Liturgy)",
		"he":                                "HEIRMOLOGION",
		"hi":                                "PRIESTS HANDBOOK",
		"hi00":                              "Introduction",
		"hi01":                              "Vespers",
		"hi02":                              "Great Compline",
		"hi03":                              "Midnight Office",
		"hi04":                              "Matins",
		"hi05":                              "Vigil",
		"hi06":                              "Liturgy-Preparation",
		"hi07":                              "Liturgy-Chrysostom",
		"hi08":                              "Liturgy Hymns",
		"hi09":                              "Liturgy-Basil",
		"hi10":                              "Liturgy-Presanctified",
		"hi11":                              "Celebration",
		"hi12":                              "Communion Prayers",
		"hi13":                              "Cross Ceremonies",
		"hi14":                              "Great Blessing of Waters",
		"hi15":                              "Pentecost Kneeling Vespers",
		"hi16":                              "Special Services",
		"hi17":                              "Various Prayers",
		"hierarch":                          "Hierarch",
		"hierarchical":                      "Hierarchical",
		"ho":                                "HOROLOGION",
		"ho01":                              "Preliminary Prayers",
		"ho02":                              "Midnight Office",
		"ho03":                              "Matins",
		"ho04":                              "First Hour",
		"ho05":                              "Third Hour",
		"ho06":                              "Sixth Hour",
		"ho07":                              "Typika Service",
		"ho08":                              "Lunch Prayers",
		"ho09":                              "Ninth Hour",
		"ho10":                              "Vespers",
		"ho11":                              "Dinner Prayers",
		"ho12":                              "Great Compline",
		"ho13":                              "Small Compline",
		"ho14":                              "Paschal Service",
		"ho15":                              "Troparia by Month",
		"ho16":                              "Troparia of Triodion",
		"ho17":                              "Troparia of Pentecostarion",
		"ho18":                              "Resurrectional Hymns",
		"ho19":                              "Weekday Apolytikia",
		"ho20":                              "Theotokia by Mode and Day",
		"ho21":                              "Communion Prayers",
		"ho22":                              "Salutation / Akathist to Theotokos",
		"ho23":                              "Small Paraklesis",
		"ho24":                              "Great Paraklesis",
		"ho25":                              "Supplication Canon to Lord Jesus",
		"ho26":                              "Supplication Canon to Guardian Angel",
		"ho27":                              "Supplication Canon to Angels and Saints",
		"ho28":                              "Akathist to the Cross",
		"ho29":                              "Canon, Oikoi, and Hymns to the Holy Trinity",
		"html":                              "View",
		"hwk":                               "Holy Week",
		"hymnbystnektarios":                 "Hymn by St. Nektarios",
		"incipit":                           "Incipit",
		"info":                              "Information",
		"james":                             "St. James",
		"jan":                               "January",
		"jul":                               "July",
		"jun":                               "Jun",
		"k":                                 "Katavasias",
		"ka":                                "KATAVASIAS",
		"key":                               "Key",
		"kik":                               "Kikuyu",
		"ko":                                "ko",
		"kor":                               "Korean",
		"le":                                "LECTIONARY",
		"lectionaries":                      "Lectionaries",
		"li":                                "Divine Liturgy",
		"li1":                               "Liturgy - with Typika and Beatitudes",
		"li2":                               "Liturgy - Variable Parts",
		"li3":                               "Liturgy - Additional Commemoration",
		"li4":                               "Liturgy - Additional with Typika and Beatitudes",
		"li5":                               "Liturgy - Additional Variable Parts",
		"li6":                               "Liturgy - Alternative Commemoration",
		"li7":                               "Liturgy - Alternative Variable Parts",
		"li8":                               "Liturgy - Alt. Version 8",
		"li9":                               "Liturgy - Hierarchical",
		"lichrysbasil":                      "Liturgy of Chrysostom/Basil ",
		"lijames":                           "Liturgy of James",
		"lipresanctified":                   "Presanctified Liturgy",
		"liturgical":                        "Liturgical",
		"liturgy":                           "Liturgy",
		"lnt":                               "Lent",
		"lu":                                "lu",
		"lvt":                               "Leave-taking",
		"m01":                               "January",
		"m02":                               "February",
		"m03":                               "March",
		"m04":                               "April",
		"m05":                               "May",
		"m06":                               "June",
		"m07":                               "July",
		"m08":                               "August",
		"m09":                               "September",
		"m1":                                "Mode 1",
		"m10":                               "October",
		"m11":                               "November",
		"m12":                               "December",
		"m2":                                "Mode 2",
		"m3":                                "Mode 3",
		"m4":                                "Mode 4",
		"m5":                                "Mode 5",
		"m6":                                "Mode 6",
		"m7":                                "Mode 7",
		"m8":                                "Mode 8",
		"m_":                                "Mode",
		"m__":                               "Month",
		"ma":                                "Matins",
		"ma1":                               "Matins - Pascha Odes 1, 3, 9",
		"ma2":                               "Matins - Customizable",
		"ma3":                               "Matins - Full (Psalms / Canon)",
		"ma4":                               "Matins - Additional Commemoration",
		"ma5":                               "Matins - Alternative Commemoration",
		"ma6":                               "Matins - Alternative Full (Psalms / Canon)",
		"ma7":                               "Matins - (of the Vigil)",
		"ma8":                               "Matins - Alt. Version 8",
		"ma9":                               "Matins - Hierarchical",
		"mar":                               "March",
		"matins":                            "Matins",
		"martyr":                            "Martyr",
		"may":                               "May",
		"mc":                                "Movable Cycle",
		"me":                                "MENAION",
		"memorial":                          "Memorial ",
		"menaion":                           "Menaion",
		"meVE":                              "Menaion Vespers",
		"minororders":                       "Minor Orders",
		"misc":                              "Miscellaneous",
		"mo":                                "Midnight Office",
		"mo5":                               "Midnight Office - Alternative Commemoration",
		"mol":                               "Moleben",
		"mon":                               "Monday",
		"monastic":                          "Monastic",
		"movable":                           "Movable",
		"museum":                            "Museum",
		"nov":                               "November",
		"o":                                 "Other",
		"oc":                                "OCTOECHOS",
		"occasionalprayers":                 "Occasional Prayers",
		"oct":                               "October",
		"octoechos":                         "Octoechos",
		"ocwd":                              "ocwd",
		"Oliturgical":                       "Liturgical",
		"Omovable":                          "Movable",
		"ordination":                        "Ordination",
		"ordinations":                       "Ordinations",
		"orthros":                           "Orthros",
		"os":                                "os",
		"other":                             "Other",
		"page.search.db.id":                 "Doxa Database ID",
		"page.search.desc":                  "Books and services are available in the languages shown in the dropdown below. Select the language to use for the search. Enter a word, phrase, or regular expression to search for, then press the Enter or Return key. Matching texts are displayed a page at a time. You can change the number of matches shown per page. Note that regular expression word boundaries do not work with languages such as Greek. So, use the 'Whole Word' option instead. When you click on a link, the book or service will be displayed, and the page will be scrolled so that the matched text is at the top. If the matched text occurs at or near the bottom of the page, it won't be at the top, but somewhere below that. At this time, it is not possible to highlight the matched text in a book or service opening from a link on the search page. At the bottom of each matched text, there is information about the liturgical source of the text and the Doxa database ID. If you click the database ID, you can view the Greek source text, translations, grammatical information, and notes.",
		"page.search.exact":                 "Exact",
		"page.search.found.plural":          "found",
		"page.search.found.sing":            "found",
		"page.search.not.available":         "Search not available",
		"page.search.of":                    "of",
		"page.search.page":                  "Body",
		"page.search.page.size":             "Body size",
		"page.search.place.holder":          "Enter a word, phrase, or regular expression, and press the Enter or Return key",
		"page.search.showing.matches":       "Showing matches",
		"page.search.source":                "Liturgical Source",
		"page.search.status":                "Status",
		"page.search.title.page":            "Search Books and Services",
		"page.search.title.tab":             "Search",
		"page.search.to":                    "to",
		"page.search.whole.word":            "Whole Word",
		"pandemicparaklesis":                "Pandemic Paraklesis",
		"parts":                             "Parts",
		"pas":                               "Pascha",
		"pb":                                "Prayer Book",
		"pdf":                               "Print",
		"pe":                                "PENTECOSTARION",
		"pl":                                "Presanctified Liturgy",
		"pl2":                               "Presanctified Liturgy - Variable Parts",
		"poets":                             "Poets",
		"pr":                                "PROPHECIES",
		"prayerbook":                        "Prayer Book",
		"prayers":                           "Prayers",
		"presanctified":                     "Presanctified Liturgy ",
		"prf":                               "Pre-Feast",
		"priest":                            "Priest",
		"privateendeavor":                   "Private Endeavor",
		"properties":                        "Properties",
		"ps":                                "PSALTER",
		"ps44":                              "Psalm 44",
		"pub":                               "pub",
		"publicoffice":                      "Public Office",
		"r01":                               "rubrical 1",
		"r02":                               "rubrical 2",
		"r03":                               "rubrical 3",
		"r04":                               "rubrical 4",
		"r06":                               "rubrical 6",
		"r08":                               "rubrical 8",
		"r10":                               "rubrical 10",
		"r100":                              "rubrical 100",
		"r101":                              "rubrical 101",
		"r105":                              "rubrical 105",
		"r106":                              "rubrical 106",
		"r108":                              "rubrical 8",
		"r12":                               "rubrical 12 ",
		"r13":                               "rubrical 13",
		"r14":                               "rubrical 14",
		"r15":                               "rubrical 15",
		"r16":                               "rubrical 16",
		"r17":                               "rubrical 17",
		"r18":                               "rubrical 18",
		"r19":                               "rubrical 19",
		"r20":                               "rubrical 20",
		"r21":                               "rubrical 21",
		"r22":                               "rubrical 22",
		"r23":                               "rubrical 23",
		"r25":                               "rubrical 25",
		"r26":                               "rubrical 26",
		"r27":                               "rubrical 27",
		"r28":                               "rubrical 28",
		"r29":                               "rubrical 29",
		"r30":                               "rubrical 30",
		"r31":                               "rubrical 31",
		"r32":                               "rubrical 32",
		"r33":                               "rubrical 33",
		"r34":                               "rubrical 34",
		"r35":                               "rubrical 35",
		"r36":                               "rubrical 36",
		"r37":                               "rubrical 37",
		"r38":                               "rubrical 38",
		"r39":                               "rubrical 39",
		"r40":                               "rubrical 4",
		"r41":                               "rubrical 41",
		"r41a":                              "rubrical 42",
		"r48":                               "rubrical 43",
		"r49":                               "rubrical 44",
		"r50":                               "rubrical 50",
		"r51":                               "rubrical 51",
		"r55":                               "rubrical 55",
		"r56":                               "rubrical 56",
		"r57":                               "rubrical 57",
		"r61":                               "rubrical 61",
		"r62":                               "rubrical 62",
		"r63":                               "rubrical 63",
		"r64":                               "rubrical 64",
		"r65":                               "rubrical 65",
		"r66":                               "rubrical 66",
		"r67":                               "rubrical 67",
		"r68":                               "rubrical 68",
		"r69":                               "rubrical 69",
		"r71":                               "rubrical 71",
		"r72":                               "",
		"r73":                               "",
		"r74":                               "",
		"r75":                               "",
		"r76":                               "",
		"r77":                               "",
		"r78":                               "",
		"r81":                               "",
		"r82":                               "",
		"r83a":                              "",
		"r83b":                              "",
		"r84":                               "",
		"r85":                               "",
		"r87":                               "",
		"r89":                               "",
		"r91":                               "",
		"r93":                               "",
		"r94":                               "",
		"r95":                               "",
		"r96":                               "",
		"r97":                               "",
		"r98":                               "",
		"r99":                               "",
		"rds":                               "Reader Service",
		"relics":                            "Relics",
		"rubrical":                          "Rubrical",
		"rubrics":                           "Rubrics",
		"s01":                               "",
		"s02":                               "",
		"s03":                               "",
		"s04":                               "",
		"s05":                               "",
		"s06":                               "",
		"s07":                               "",
		"s08":                               "",
		"s09":                               "",
		"s10":                               "",
		"s11":                               "",
		"s12":                               "",
		"s13":                               "",
		"s14":                               "",
		"s15":                               "",
		"s16":                               "",
		"s17":                               "",
		"s18":                               "",
		"s19":                               "",
		"s20":                               "",
		"s21":                               "",
		"s22":                               "",
		"s23":                               "",
		"s24":                               "",
		"s25":                               "",
		"s26":                               "",
		"s27":                               "",
		"s28":                               "",
		"s29":                               "",
		"saints":                            "Saints",
		"sal":                               "Salutations",
		"sat":                               "Saturday",
		"schoolyear":                        "School Year",
		"se":                                "Service",
		"sep":                               "September",
		"servicebook":                       "Service Book",
		"ship":                              "Ship",
		"sla":                               "Slava",
		"smallwaterblessing":                "Small Blessing of Waters",
		"spa":                               "Spanish",
		"stbarbaraparaklesis":               "St. Barbara Paraklesis",
		"stciaranparaklesis":                "St. Ciaran Paraklesis",
		"stdemetriosparaklesis":             "St. Demetrios Paraklesis",
		"Sticheron01":                       "Sticheron 01",
		"stnektariosparaklesis1":            "St. Nektarios Paraklesis 1",
		"stnicholasparaklesis1":             "St. Nicholas Paraklesis 1",
		"stpaisiosparaklesis1":              "St. Paisios Paraklesis 1",
		"stpaisiosparaklesis2":              "St. Paisios Paraklesis 2",
		"stpanteleimonparaklesis1":          "St. Panteleimon Paraklesis 1",
		"stsraphaelnicholasireneparaklesis": "Sts. Raphael, Nicholas Irene Paraklesis",
		"sun":                               "Sunday",
		"sv":                                "Small Vespers",
		"sv3":                               "Small Vespers with Pre-Sanctified",
		"swh":                               "Swahili",
		"sy":                                "SYNAXARION",
		"template":                          "Template",
		"theotokos":                         "Theotokos",
		"thu":                               "Thursday",
		"titles":                            "Titles",
		"tr":                                "TRIODION",
		"trn":                               "Triodion",
		"tue":                               "Tuesday",
		"ty":                                "Typika",
		"typica":                            "Typica",
		"typikon":                           "Typikon",
		"unction":                           "Unction ",
		"unmercenary":                       "Unmercenary",
		"va":                                "va",
		"variables":                         "Variables",
		"ve":                                "Vespers",
		"ve1":                               "Vespers - Alt. Version 1",
		"ve2":                               "Vespers - Full (Lity and Loaves)",
		"ve3":                               "Great Vespers and Salutations",
		"ve4":                               "Vespers - Additional Commemoration",
		"ve5":                               "Vespers - Alternative Commemoration",
		"ve6":                               "Vespers - Alternative Full (Lity and Loaves)",
		"ve7":                               "Vespers - (of the Vigil)",
		"ve8":                               "Vespers - Alt. Version 8",
		"ve9":                               "Vespers - Hierarchical",
		"verses":                            "Verses",
		"vespers":                           "Vespers",
		"vimatarissaparaklesis":             "Vimatarissa Paraklesis",
		"vl":                                "Vesperal Liturgy",
		"vl2":                               "Vesperal Liturgy (Full)",
		"w01":                               "",
		"w02":                               "",
		"w03":                               "",
		"w04":                               "",
		"w05":                               "",
		"w06":                               "",
		"w07":                               "",
		"w08":                               "",
		"w09":                               "",
		"w10":                               "",
		"w11":                               "",
		"water":                             "",
		"wed":                               "Wednesday",
		"wedding":                           "Wedding ",
		"wkd":                               "Weekday",
	}
}

// getHomeIntroCardMap provides key-value pairs for settings to create a Bootstrap Card
func getHomeIntroCardMap() map[string]string {
	return map[string]string{"body": "This liturgical website was generated as a demonstration of DOXA tools for Digital Orthodox Akolouthia. Doxa can be used to generate liturgical books and dated services (with variable content) in one, two, or three languages side-by-side. The demo includes only a limited number of services and books and might not be complete. Currently, the focus is on generating html (web pages). PDF generation will be worked on soon.",
		"buttonIcon":  "",
		"buttonText":  "",
		"buttonUrl":   "",
		"imageAlt":    "",
		"imageSource": "",
		"title":       "Demonstration DCS",
	}
}

// getHomeServicesCardMap provides key-value pairs for settings to create a Bootstrap Card
func getHomeServicesCardMap() map[string]string {
	return map[string]string{"body": "Eastern Orthodox Christian liturgical services are available for specific dates.",
		"buttonIcon":  "calendar-date",
		"buttonText":  "View Calendar of Services",
		"buttonUrl":   "servicesindex.html",
		"imageAlt":    "liturgical services",
		"imageSource": "stphotios.png",
		"title":       "Liturgical Services",
	}
} // getHomeBooksCardMap provides key-value pairs for settings to create a Bootstrap Card
func getHomeBooksCardMap() map[string]string {
	return map[string]string{"body": "Liturgical books (e.g. the Menaion) are available in a variety of languages.",
		"buttonIcon":  "book",
		"buttonText":  "View Books",
		"buttonUrl":   "booksindex.html",
		"imageAlt":    "If set, readers for visually impaired users will use this to describe the image.",
		"imageSource": "img/books.png",
		"title":       "Liturgical Books",
	}
}

// getHomeClosingCardMap provides key-value pairs for settings to create a Bootstrap Card
func getHomeClosingCardMap() map[string]string {
	return map[string]string{"body": "On all pages, you can view the menu by clicking on the logo. In the menu, to get help, click the question icon.",
		"buttonIcon":  "",
		"buttonText":  "",
		"buttonUrl":   "",
		"imageAlt":    "",
		"imageSource": "",
		"title":       "",
	}
}

// Obsolete indicates whether a property path is no longer used by Doxa
func Obsolete(p string) bool {
	for _, o := range ObsoleteProperties {
		if strings.HasPrefix(p, o) {
			return true
		}
	}
	return false
}

var ObsoleteProperties = []string{
	"/site/build/lang.combos",
	"/site/build/navbar/hasBooksIcon",
	"/site/build/frames/score/paragraphs/desc",
	//"/site/build/pdf/fonts/ara",
	//"/site/build/pdf/fonts/cac",
	//"/site/build/pdf/fonts/chi",
	//"/site/build/pdf/fonts/chr",
	//"/site/build/pdf/fonts/chu",
	//"/site/build/pdf/fonts/cnm",
	//"/site/build/pdf/fonts/en",
	//"/site/build/pdf/fonts/es",
	//"/site/build/pdf/fonts/gr",
	//"/site/build/pdf/fonts/kor",
	//"/site/build/pdf/fonts/spa",
	//"/site/build/pdf/fonts/swh",
	"/site/db/",
	"/sys/build/002",
	"/sys/cmd/ages/templates/",
	"/sys/cmd/ages/sys/",
	"/sys/synch/ltx/repos/en_uk_lash",
	"/sys/synch/ltx/repos/en_uk_lash/push",
}
