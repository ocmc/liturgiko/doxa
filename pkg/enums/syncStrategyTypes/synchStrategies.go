//go:generate enumer -type=SyncStrategy -json -text -yaml -sql
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate

// Package syncStrategyTypes provides an enum of strategies for synchronization. For each type, it also defines the syncActions to be performed.
package syncStrategyTypes

import synchActionTypes "github.com/liturgiko/doxa/pkg/enums/syncActionTypes"

type SyncStrategy int

const (
	Unknown       SyncStrategy = iota
	Combine                    // makes remote, local, and database content identical, preserving database changes
	Export                     // simply export the database records
	ResetToDB                  // makes the remote and local identical to the database contents
	ResetToRemote              // makes the local and database identical to the remote content
	Status                     // just get the status of repo: see pkg/enums/repositoryStatuses
)

func ActionsForType(t SyncStrategy) []synchActionTypes.SyncActionType {
	var actions []synchActionTypes.SyncActionType
	switch t {
	case Combine:
		// make the local repo content identical to remote
		actions = append(actions, synchActionTypes.Fetch)
		actions = append(actions, synchActionTypes.ResetHard)
		actions = append(actions, synchActionTypes.Clean)
		// export the database records
		actions = append(actions, synchActionTypes.Export)
		// push the merged records to remote
		actions = append(actions, synchActionTypes.Commit)
		actions = append(actions, synchActionTypes.Push)
		// make the database dir content identical to the repo
		actions = append(actions, synchActionTypes.RemoveDbDir)
		actions = append(actions, synchActionTypes.Import)
		return actions
	case Export:
		// export the database records
		actions = append(actions, synchActionTypes.Export)
		return actions
	case ResetToDB:
		// reset local to remote
		actions = append(actions, synchActionTypes.Fetch)
		actions = append(actions, synchActionTypes.ResetHard)
		// delete all repo content, except .git* files
		actions = append(actions, synchActionTypes.RemoveRepoFiles)
		// push the empty repo
		actions = append(actions, synchActionTypes.Commit)
		actions = append(actions, synchActionTypes.Push)
		// write the database records to the repo
		actions = append(actions, synchActionTypes.Export)
		// push them to the remote repo
		actions = append(actions, synchActionTypes.Commit)
		actions = append(actions, synchActionTypes.Push)
		return actions
	case ResetToRemote:
		// make the local contents identical to remote
		actions = append(actions, synchActionTypes.Fetch)
		actions = append(actions, synchActionTypes.ResetHard)
		actions = append(actions, synchActionTypes.Clean)
		// delete the database dir this repo is for
		actions = append(actions, synchActionTypes.RemoveDbDir)
		// load the local content into the database
		actions = append(actions, synchActionTypes.Import)
		return actions
	case Status:
		actions = append(actions, synchActionTypes.Status)
		return actions
	default:
		return actions
	}
}
func CodeForType(t SyncStrategy) string {
	switch t {
	case Combine:
		return "combine"
	case Export:
		return "export"
	case ResetToDB:
		return "resetToDb"
	case ResetToRemote:
		return "resetToRemote"
	case Status:
		return "status"
	default:
		return "unknown"
	}
}

func TypeForString(s string) SyncStrategy {
	// if changes are made here to the string literals,
	// they must also be made in static/wc/synch-manager.js
	switch s {
	case "combine":
		return Combine
	case "export":
		return Export
	case "resetToDb":
		return ResetToDB
	case "resetToRemote":
		return ResetToRemote
	case "status":
		return Status
	default:
		return Unknown
	}
}
