//go:generate enumer -type=RepoStatusType -json -text -yaml -sql
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate

// Package repoStatuses provides an enum of the status of a repository
package repoStatuses

type RepoStatusType int

const (
	Unknown RepoStatusType = iota
	LocalAheadOfRemote
	RemoteAheadOfLocal
	UpToDate
)

func DescriptionForType(t RepoStatusType) string {
	switch t {
	case LocalAheadOfRemote:
		return "local repo has changes remote does not"
	case RemoteAheadOfLocal:
		return "remote repo has changes local does not"
	case UpToDate:
		return "remote and local content is the same"
	default:
		return "not known"
	}
}
