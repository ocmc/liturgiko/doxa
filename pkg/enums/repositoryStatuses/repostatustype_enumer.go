// Code generated by "enumer -type=RepoStatusType -json -text -yaml -sql"; DO NOT EDIT.

//
package repoStatuses

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
)

const _RepoStatusTypeName = "UnknownLocalAheadOfRemoteRemoteAheadOfLocalUpToDate"

var _RepoStatusTypeIndex = [...]uint8{0, 7, 25, 43, 51}

func (i RepoStatusType) String() string {
	if i < 0 || i >= RepoStatusType(len(_RepoStatusTypeIndex)-1) {
		return fmt.Sprintf("RepoStatusType(%d)", i)
	}
	return _RepoStatusTypeName[_RepoStatusTypeIndex[i]:_RepoStatusTypeIndex[i+1]]
}

var _RepoStatusTypeValues = []RepoStatusType{0, 1, 2, 3}

var _RepoStatusTypeNameToValueMap = map[string]RepoStatusType{
	_RepoStatusTypeName[0:7]:   0,
	_RepoStatusTypeName[7:25]:  1,
	_RepoStatusTypeName[25:43]: 2,
	_RepoStatusTypeName[43:51]: 3,
}

// RepoStatusTypeString retrieves an enum value from the enum constants string name.
// Throws an error if the param is not part of the enum.
func RepoStatusTypeString(s string) (RepoStatusType, error) {
	if val, ok := _RepoStatusTypeNameToValueMap[s]; ok {
		return val, nil
	}
	return 0, fmt.Errorf("%s does not belong to RepoStatusType values", s)
}

// RepoStatusTypeValues returns all values of the enum
func RepoStatusTypeValues() []RepoStatusType {
	return _RepoStatusTypeValues
}

// IsARepoStatusType returns "true" if the value is listed in the enum definition. "false" otherwise
func (i RepoStatusType) IsARepoStatusType() bool {
	for _, v := range _RepoStatusTypeValues {
		if i == v {
			return true
		}
	}
	return false
}

// MarshalJSON implements the json.Marshaler interface for RepoStatusType
func (i RepoStatusType) MarshalJSON() ([]byte, error) {
	return json.Marshal(i.String())
}

// UnmarshalJSON implements the json.Unmarshaler interface for RepoStatusType
func (i *RepoStatusType) UnmarshalJSON(data []byte) error {
	var s string
	if err := json.Unmarshal(data, &s); err != nil {
		return fmt.Errorf("RepoStatusType should be a string, got %s", data)
	}

	var err error
	*i, err = RepoStatusTypeString(s)
	return err
}

// MarshalText implements the encoding.TextMarshaler interface for RepoStatusType
func (i RepoStatusType) MarshalText() ([]byte, error) {
	return []byte(i.String()), nil
}

// UnmarshalText implements the encoding.TextUnmarshaler interface for RepoStatusType
func (i *RepoStatusType) UnmarshalText(text []byte) error {
	var err error
	*i, err = RepoStatusTypeString(string(text))
	return err
}

// MarshalYAML implements a YAML Marshaler for RepoStatusType
func (i RepoStatusType) MarshalYAML() (interface{}, error) {
	return i.String(), nil
}

// UnmarshalYAML implements a YAML Unmarshaler for RepoStatusType
func (i *RepoStatusType) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var s string
	if err := unmarshal(&s); err != nil {
		return err
	}

	var err error
	*i, err = RepoStatusTypeString(s)
	return err
}

func (i RepoStatusType) Value() (driver.Value, error) {
	return i.String(), nil
}

func (i *RepoStatusType) Scan(value interface{}) error {
	if value == nil {
		return nil
	}

	str, ok := value.(string)
	if !ok {
		bytes, ok := value.([]byte)
		if !ok {
			return fmt.Errorf("value is not a byte slice")
		}

		str = string(bytes[:])
	}

	val, err := RepoStatusTypeString(str)
	if err != nil {
		return err
	}

	*i = val
	return nil
}
