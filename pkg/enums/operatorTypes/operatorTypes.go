//go:generate enumer -type=OperatorType -json -text -yaml -sql
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate

// Package modes provides an enum of statuses for liturgical artifacts, e.g. translation, template
package operatorTypes

type OperatorType int

const (
	NA OperatorType = iota
	AND
	OR
	EQ
	NEQ
	GT
	LT
	GTEQ
	LTEQ
)
