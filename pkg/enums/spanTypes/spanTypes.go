//go:generate enumer -type=SpanType -json -text -yaml -sql
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go

// Package spanTypes provides an enum for types of spans
package spanTypes

type SpanType int

const (
	Wrapper SpanType = iota
	Anchor
	Image
	Lid
	Media
	Nid
	Note
	Sid
	Rid
)
