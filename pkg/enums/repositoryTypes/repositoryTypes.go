//go:generate enumer -type=RepoTypes -json -text -yaml -sql
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate

// Package repositoryTypes provides an enum of types of git repositories of Doxa artifacts
package repositoryTypes

type RepoTypes int

const (
	Unknown RepoTypes = iota
	Asset
	Btx
	Catalog
	Config
	Ltx
	Media
	Template
	WebSite
)

func CodeForString(s string) RepoTypes {
	switch s {
	case "assets":
		return Asset
	case "btx":
		return Btx
	case "catalog":
		return Catalog
	case "configs":
		return Config
	case "ltx":
		return Ltx
	case "media":
		return Media
	case "templates":
		return Template
	case "websites":
		return WebSite
	default:
		return Unknown
	}
}
