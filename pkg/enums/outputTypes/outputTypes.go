// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate
//
//go:generate enumer -type=OutputType -json -text -yaml -sql
package outputTypes

import (
	"fmt"
	"strings"
)

type OutputType int

const (
	Both OutputType = iota
	HTML
	PDF
)

func TypeFor(t string) (OutputType, error) {
	n := strings.ToLower(t)
	n = strings.ReplaceAll(n, " ", "")
	switch n {
	case "html,pdf":
		return Both, nil
	case "html":
		return HTML, nil
	case "pdf":
		return PDF, nil
	case "pdf,html":
		return Both, nil
	default:
		return -1, fmt.Errorf("invalid output type %s: expected html, or pdf, or html,pdf]", t)
	}
}
