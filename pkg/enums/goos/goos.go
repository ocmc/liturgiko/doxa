//go:generate enumer -type=GoOs -json -text -yaml -sql
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate

// Package goos provides an enum of types of operating systems
package goos

type GoOs int

const (
	Darwin GoOs = iota
	Linux
	Windows
	Unknown
)

func CodeForString(s string) GoOs {
	switch s {
	case "darwin":
		return Darwin
	case "linux":
		return Linux
	case "windows":
		return Windows
	default:
		return Unknown
	}
}
