//go:generate enumer -type=OfficeType -json -text -yaml -sql
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate

// Package officeTypes provides an enum for types of liturgical offices
package officeTypes

type OfficeType int

const (
	NA OfficeType = iota
	MidnightOffice
	Matins
	GreatHours
	FirstHour
	ThirdHour
	SixthHour
	Typika
	Liturgy
	ReaderService
	Moleben
	Slava
	NinthHour
	Vespers
	Presanctified
	SmallVespers
	VesperalLiturgy
	GreatCompline
	Compline
	Salutations
	MatinsInEvening
)

func (i OfficeType) Abr() string {
	switch i {
	case Vespers:
		return "ve"
	case SmallVespers:
		return "sv"
	case VesperalLiturgy:
		return "vl"
	case Compline:
		return "co"
	case GreatCompline:
		return "gc"
	case Salutations:
		return "sa"
	case Matins:
		return "ma"
	case FirstHour:
		return "h1"
	case GreatHours:
		return "gh"
	case Liturgy:
		return "li"
	case MatinsInEvening:
		return "em"
	case MidnightOffice:
		return "mo"
	case NinthHour:
		return "h9"
	case Presanctified:
		return "pl"
	case SixthHour:
		return "h6"
	case ThirdHour:
		return "h3"
	case Typika:
		return "ty"
	case Moleben:
		return "ml"
	case Slava:
		return "sl"
	case ReaderService:
		return "rs"
	default:
		return "na"
	}
}
func OfficeTypeForAbr(abr string) OfficeType {
	if len(abr) < 2 {
		return -1
	}
	switch abr[:2] {
	case "co":
		return Compline
	case "em":
		return MatinsInEvening
	case "gc":
		return GreatCompline
	case "gh":
		return GreatHours
	case "h1":
		return FirstHour
	case "h3":
		return ThirdHour
	case "h6":
		return SixthHour
	case "h9":
		return NinthHour
	case "li":
		return Liturgy
	case "ma":
		return Matins
	case "ml":
		return Moleben
	case "mo":
		return MidnightOffice
	case "na":
		return NA
	case "pl":
		return Presanctified
	case "rs":
		return ReaderService
	case "sa":
		return Salutations
	case "sl":
		return Slava
	case "sv":
		return SmallVespers
	case "ty":
		return Typika
	case "ve":
		return Vespers
	case "vl":
		return VesperalLiturgy
	default:
		return -1
	}
}

// CelebratedPreviousEvening true for services done
// the evening of the template date.
func (i OfficeType) CelebratedPreviousEvening() bool {
	switch i {
	case Vespers, Compline,
		SmallVespers, Presanctified,
		VesperalLiturgy, GreatCompline,
		Salutations, MatinsInEvening:
		return true
	default:
		return false
	}
}
func (i OfficeType) IndexTitle() string {
	switch i {
	case Vespers:
		return "Vespers"
	case SmallVespers:
		return "Small Vespers"
	case VesperalLiturgy:
		return "Vesperal Liturgy"
	case Matins:
		return "Matins"
	case Compline:
		return "Evening Compline "
	case GreatCompline:
		return "Great Compline"
	case FirstHour:
		return "First Hour"
	case GreatHours:
		return "Great Hours"
	case Liturgy:
		return "Divine Liturgy"
	case MidnightOffice:
		return "Midnight Office"
	case MatinsInEvening:
		return "Matins (in the evening)"
	case NinthHour:
		return "Ninth Hour"
	case Presanctified:
		return "Presanctified Liturgy"
	case SixthHour:
		return "Sixth Hour"
	case ThirdHour:
		return "Third Hour"
	case Typika:
		return "Typika Service (no priest)"
	case Moleben:
		return "Moleben"
	case Slava:
		return "Slava"
	case ReaderService:
		return "Reader Service"
	default:
		return ""
	}
}
