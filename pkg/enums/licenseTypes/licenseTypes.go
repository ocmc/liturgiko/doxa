//go:generate enumer -type=LicenseType -json -text -yaml -sql
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate

// Package licenseTypes provides an enum of types of licenses a person can use to provide their work to others
package licenseTypes

type LicenseType int

const (
	NotSet LicenseType = iota
	BY
	ByNc
	ByNcNd
	ByNcSa
	ByNd
	BySa
	CCO
	PdSj
	PdWw
)

func StringForType(t LicenseType) string {
	switch t {
	case BY:
		return "BY"
	case ByNc:
		return "BY-NC"
	case ByNcNd:
		return "BY-NC-ND"
	case ByNcSa:
		return "BY-NC-SA"
	case ByNd:
		return "BY-ND"
	case BySa:
		return "BY-SA"
	case CCO:
		return "CCO"
	case PdSj:
		return "PD-SJ"
	case PdWw:
		return "PD-WW"
	default:
		return "Not Set"
	}
}

func TitleForType(t LicenseType) string {
	switch t {
	case BY:
		return "Creative Commons Attribution 4.0 International (BY 4.0)"
	case ByNc:
		return "Creative Commons Attribution-NonCommercial  4.0 International (BY-NC 4.0)"
	case ByNcNd:
		return "Creative Commons Attribution-NonCommercial-NoDerivatives  4.0 International (BY-NC-ND 4.0)"
	case ByNcSa:
		return "Creative Commons Attribution-NonCommercial-ShareAlike  4.0 International (BY-NC-SA 4.0)"
	case ByNd:
		return "Creative Commons Attribution-NoDerivatives  4.0 International (BY-ND 4.0)"
	case BySa:
		return "Creative Commons Attribution-ShareAlike  4.0 International (BY-SA 4.0)"
	case CCO:
		return "Creative Commons No Rights Reserved (CCO 1.0)"
	case PdSj:
		return "Public Domain in Specified Jurisdictions (PD-SJ)"
	case PdWw:
		return "Public Domain World-Wide (PD-WW)"
	default:
		return "License not specified"
	}
}
func TypeForString(s string) LicenseType {
	switch s {
	case "BY":
		return BY
	case "BY-NC":
		return ByNc
	case "BY-NC-ND":
		return ByNcNd
	case "BY-NC-SA":
		return ByNcSa
	case "BY-ND":
		return ByNd
	case "BY-SA":
		return BySa
	case "CCO":
		return CCO
	case "PD-SJ":
		return PdSj
	case "PD-WW":
		return PdWw
	default:
		return NotSet
	}
}
