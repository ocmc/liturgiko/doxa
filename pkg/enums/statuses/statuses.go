//go:generate enumer -type=Status -json -text -yaml -sql
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate

// Package statuses provides an enum of statuses for liturgical artifacts, e.g. translation, template
package statuses

import (
	"fmt"
	"strings"
)

type Status int

const (
	NA Status = iota
	Draft
	Review
	Final
)

// String returns the string name of the status
func String(s Status) string {
	switch s {
	case NA:
		return "na"
	case Draft:
		return "draft"
	case Review:
		return "review"
	case Final:
		return "final"
	default:
		return "unknown"
	}
}
func StatusCode(s string) (Status, error) {
	switch s {
	case "na":
		return NA, nil
	case "draft":
		return Draft, nil
	case "review":
		return Review, nil
	case "final":
		return Final, nil
	default:
		return -1, fmt.Errorf("unknown status %s", s)
	}
}

func ToCodes(statusNames []string) ([]Status, error) {
	var err error
	var c Status
	sb := strings.Builder{}
	var codes []Status
	for _, s := range statusNames {
		c, err = StatusCode(strings.TrimSpace(s))
		if err != nil {
			if sb.Len() > 0 {
				sb.WriteString(", ")
			}
			sb.WriteString(fmt.Sprintf("%v", err))
			continue
		}
		codes = append(codes, c)
	}
	if sb.Len() > 0 {
		err = fmt.Errorf(sb.String())
	}
	return codes, err
}
func StatusTypeNames() []string {
	return []string{String(NA), String(Draft), String(Review), String(Final)}
}
