//go:generate enumer -type=SyncActionType -json -text -yaml -sql
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate

// Package synchActionTypes provides an enum of synchronization action types
package synchActionTypes

type SyncActionType int

const (
	Unknown SyncActionType = iota
	Clean
	Commit
	Export
	Fetch
	ResetHard
	Status
	Import
	Pull
	Push
	RemoveRepoFiles
	RemoveDbDir
)

func descriptionForType(t SyncActionType) string {
	switch t {
	case Clean:
		return "deletes untracked files in the local repo"
	case Commit:
		return "adds all files and commits them"
	case Export:
		return "export the database records to the local repo"
	case Fetch:
		return "gets information about changes to the remote repo content"
	case ResetHard:
		return "discards local changes, makes local contents like remote"
	case Import:
		return "import the Doxa key-value file entries into the database"
	case Pull:
		return "fetches remote content and merges it into the local content"
	case Push:
		return "makes the remote repo content identical to the local repo content"
	case RemoveDbDir:
		return "deletes the specified database directory this repo is for"
	case RemoveRepoFiles:
		return "calls git rm on all files and directories recursively, leaving on .git* entries"
	case Status:
		return "gets the status of the repo:  up-to-date; remote ahead of local; local ahead of remote"
	default:
		return "not known"
	}
}
