package books

import (
	"fmt"
	"testing"
)

func TestDesc(t *testing.T) {
	for _, e := range Desc() {
		fmt.Printf("%s - %s\n", e.Abr, e.Name)
	}
}

func TestMap(t *testing.T) {
	for k, v := range Map() {
		fmt.Printf("%s - %s\n", k, v)
	}
}
