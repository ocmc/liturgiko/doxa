//go:generate enumer -type=Book -json -text -yaml -sql
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate

// Package books provides an enum of Liturgical Books
package books

import "sort"

// Book is used to enumerate the Liturgical Books
type Book int

const (
	Apostolos Book = iota
	Archieratokon
	Eothina
	Euchologion
	Evangelion
	Hieratikon
	Heirmologion
	Horologion
	Katavasias
	Lectionary
	LiturgicalGuide
	Menaia
	Octeochos
	Other
	Pentecostarion
	PrayerBook
	Prophetologion
	Psalter
	ServiceBook
	Synaxarion
	Triodion
)

var Books = []Book{
	Apostolos,
	Archieratokon,
	Eothina,
	Euchologion,
	Evangelion,
	Hieratikon,
	Heirmologion,
	Horologion,
	Katavasias,
	Lectionary,
	LiturgicalGuide,
	Menaia,
	Octeochos,
	Other,
	Pentecostarion,
	PrayerBook,
	Prophetologion,
	Psalter,
	ServiceBook,
	Synaxarion,
	Triodion,
}

func (i Book) Abr() string {
	switch i {
	case Apostolos:
		return "ep"
	case Archieratokon:
		return "ar"
	case Eothina:
		return "eo"
	case Euchologion:
		return "eu"
	case Evangelion:
		return "go"
	case Hieratikon:
		return "hr"
	case Heirmologion:
		return "he"
	case Horologion:
		return "ho"
	case Katavasias:
		return "ka"
	case Lectionary:
		return "le"
	case LiturgicalGuide:
		return "lg"
	case Menaia:
		return "me"
	case Octeochos:
		return "oc"
	case Pentecostarion:
		return "pe"
	case PrayerBook:
		return "pb"
	case Prophetologion:
		return "pr"
	case Psalter:
		return "ps"
	case ServiceBook:
		return "sb"
	case Synaxarion:
		return "sy"
	case Triodion:
		return "tr"
	default:
		return "ot" // other
	}
}
func BookTypeForAbr(abr string) Book {
	if len(abr) == 0 {
		return -1
	}
	switch abr[:2] {
	case "ar":
		return Archieratokon
	case "eo":
		return Eothina
	case "ep":
		return Apostolos
	case "eu":
		return Euchologion
	case "go":
		return Evangelion
	case "he":
		return Heirmologion
	case "ho":
		return Horologion
	case "hr":
		return Hieratikon
	case "ka":
		return Katavasias
	case "le":
		return Lectionary
	case "lg":
		return LiturgicalGuide
	case "me":
		return Menaia
	case "oc":
		return Octeochos
	case "ot":
		return Other
	case "pb":
		return PrayerBook
	case "pe":
		return Pentecostarion
	case "pr":
		return Prophetologion
	case "ps":
		return Psalter
	case "sb":
		return ServiceBook
	case "sy":
		return Synaxarion
	case "tr":
		return Triodion
	default:
		return Other
	}
}
func (i Book) Name() string {
	switch i {
	case Apostolos:
		return "Apostolos"
	case Archieratokon:
		return "Archieratokon"
	case Eothina:
		return "Eothina"
	case Euchologion:
		return "Euchologion"
	case Evangelion:
		return "Evangelion"
	case Hieratikon:
		return "Hieratikon"
	case Heirmologion:
		return "Heirmologion"
	case Horologion:
		return "Horologion"
	case Katavasias:
		return "Katavasias"
	case Lectionary:
		return "Lectionary"
	case LiturgicalGuide:
		return "Liturgical Guide"
	case Menaia:
		return "Menaia"
	case Octeochos:
		return "Octochechos"
	case Pentecostarion:
		return "Pentecostarion"
	case PrayerBook:
		return "PrayerBook"
	case Prophetologion:
		return "Prophetologion"
	case Psalter:
		return "Psalter"
	case ServiceBook:
		return "ServiceBook"
	case Synaxarion:
		return "Synaxarion"
	case Triodion:
		return "Triodion"
	default:
		return "Other"
	}
}

type AbrName struct {
	Abr  string
	Name string
}

func Desc() []*AbrName {
	var d []*AbrName
	for _, b := range Books {
		a := new(AbrName)
		a.Abr = b.Abr()
		a.Name = b.Name()
		d = append(d, a)
	}
	sort.Slice(d, func(i, j int) bool {
		return d[i].Abr < d[j].Abr
	})
	return d
}

// Map returns k=Abr v=Name.  It is not sorted.
func Map() map[string]string {
	m := make(map[string]string)
	for _, b := range Books {
		m[b.Abr()] = b.Name()
	}
	return m
}
