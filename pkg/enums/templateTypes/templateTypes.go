// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate
//
//go:generate enumer -type=TemplateType -json -text -yaml -sql
package templateTypes

type TemplateType int

const (
	Block TemplateType = iota
	Book
	Service
)

func NameForType(t TemplateType) string {
	switch t {
	case Block:
		return "block"
	case Book:
		return "book"
	case Service:
		return "service"
	default:
		return "unknown"
	}
}

// TypeForPathSegment returns the type for a given site directory, e.g. h for HTML, s for Service,
// as found in the os path to an html file or pdf file in a generated DCS website.
// The default return is Block, but that should be treated as an error since
// it cannot occur as a path segment.
func TypeForPathSegment(s string) TemplateType {
	switch s {
	case "b":
		return Book
	case "s":
		return Service
	default:
		return Block
	}
}

// TypeForName returns the type for a given name, i.e. block, book, service.
// Returns -1 if not found
func TypeForName(name string) TemplateType {
	switch name {
	case "block":
		return Block
	case "book":
		return Book
	case "service":
		return Service
	default:
		return -1
	}
}

// TypeForAcronym returns the type for a given acronym, i.e. bl, bk, se.
// Returns -1 if not found
func TypeForAcronym(name string) TemplateType {
	switch name {
	case "bl":
		return Block
	case "bk":
		return Book
	case "se":
		return Service
	default:
		return -1
	}
}
