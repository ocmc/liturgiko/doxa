//go:generate enumer -type=GitStatus -json -text -yaml -sql
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate

// Package gitStatuses provides an enum of types of operating systems
package gitStatuses

import (
	"fmt"
)

type GitStatus int

const (
	Unknown GitStatus = iota
	AlreadyUpToDate
	Clean
	NonFastForward
	RemoteEmpty
	Success // used when git doesn't throw an error
	UnstagedChanges
)

func CodeForString(s string) GitStatus {
	switch s {
	case "already up-to-date":
		return AlreadyUpToDate
	case "nothing to commit, working tree clean":
		return Clean
	case "non-fast-forward update":
		return NonFastForward
	case "success":
		return Success
	case "remote repository is empty":
		return RemoteEmpty
	case "worktree contains unstaged changes":
		return UnstagedChanges
	default:
		return Unknown
	}
}
func TypeForError(err error) GitStatus {
	s := fmt.Sprintf("%s", err)
	return CodeForString(s)
}
