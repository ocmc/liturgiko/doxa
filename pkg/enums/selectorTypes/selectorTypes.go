//go:generate enumer -type=SelectorType -json -text -yaml -sql
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate

// Package modes provides an enum of directive types
package selectorTypes

type SelectorType int

const (
	Unknown SelectorType = iota
	Attribute
	Class
	FontFace
	ID
	Type
	Universal
)
