//go:generate enumer -type=Ranks -json -text -yaml -sql
// 1) go get github.com/alvaroloes/enumer
// 2) in the enum subfolder for this enum: go generate

// Package ranks provides an enum of ranks of feasts and commemorations
package ranks

type Ranks int

const (
	ClassOne Ranks = iota // feasts of the Lord
	ClassTwo              // feasts of the Theotokos
	ClassThree
	ClassFour
	ClassFive
	OrdinaryDay
)

func CodeForString(s string) Ranks {
	switch s {
	case "1", "I", "i":
		return ClassOne
	case "2", "II", "ii":
		return ClassTwo
	case "3", "III", "iii":
		return ClassThree
	case "4", "IV", "iv":
		return ClassFour
	case "5", "V", "v":
		return ClassFive
	default:
		return OrdinaryDay
	}
}

// Characteristics of feasts based on Arch. Job Getcha, The Typikon Decoded, p.99
// See also:
// https://churchmotherofgod.org/orthodox-terminology/glossary-c/2317-classes-of-feasts.html
type Characteristics struct {
	IfFastFishAllowed                          bool
	IfFastOilAllowed                           bool
	IfFastWineAllowed                          bool
	NonFestal                                  bool
	OnlyOfficeOfFeastSungEvenIfSunday          bool
	OfficeOfFeastAndResurrectionalSungIfSunday bool
	WithGreatDoxologyAtMatins                  bool
	WithMatinsGospel                           bool
	WithPolyeleos                              bool
	WithSixStichera                            bool
	WithVespersReading                         bool
	WithVigil                                  bool
}

// Properties based on rank.  Not completely filled out yet.
//
//goland:noinspection ALL
func (r Ranks) Properties() Characteristics {
	switch r {
	case ClassOne:
		return Characteristics{
			IfFastFishAllowed:                          true,
			IfFastOilAllowed:                           true,
			IfFastWineAllowed:                          true,
			NonFestal:                                  false,
			OnlyOfficeOfFeastSungEvenIfSunday:          false,
			OfficeOfFeastAndResurrectionalSungIfSunday: false,
			WithGreatDoxologyAtMatins:                  false,
			WithMatinsGospel:                           false,
			WithPolyeleos:                              false,
			WithSixStichera:                            false,
			WithVespersReading:                         false,
			WithVigil:                                  true,
		}
	case ClassTwo:
		return Characteristics{
			IfFastFishAllowed:                          true,
			IfFastOilAllowed:                           true,
			IfFastWineAllowed:                          true,
			NonFestal:                                  false,
			OnlyOfficeOfFeastSungEvenIfSunday:          false,
			OfficeOfFeastAndResurrectionalSungIfSunday: false,
			WithGreatDoxologyAtMatins:                  false,
			WithMatinsGospel:                           false,
			WithPolyeleos:                              false,
			WithSixStichera:                            false,
			WithVespersReading:                         false,
			WithVigil:                                  true,
		}
	case ClassThree:
		return Characteristics{
			IfFastFishAllowed:                          false,
			IfFastOilAllowed:                           false,
			IfFastWineAllowed:                          false,
			NonFestal:                                  false,
			OnlyOfficeOfFeastSungEvenIfSunday:          false,
			OfficeOfFeastAndResurrectionalSungIfSunday: false,
			WithGreatDoxologyAtMatins:                  false,
			WithMatinsGospel:                           false,
			WithPolyeleos:                              true,
			WithSixStichera:                            false,
			WithVespersReading:                         false,
			WithVigil:                                  false,
		}
	case ClassFour:
		return Characteristics{
			IfFastFishAllowed:                          false,
			IfFastOilAllowed:                           false,
			IfFastWineAllowed:                          false,
			NonFestal:                                  false,
			OnlyOfficeOfFeastSungEvenIfSunday:          false,
			OfficeOfFeastAndResurrectionalSungIfSunday: false,
			WithGreatDoxologyAtMatins:                  true,
			WithMatinsGospel:                           false,
			WithPolyeleos:                              false,
			WithSixStichera:                            false,
			WithVespersReading:                         false,
			WithVigil:                                  false,
		}
	case ClassFive:
		return Characteristics{
			IfFastFishAllowed:                          false,
			IfFastOilAllowed:                           false,
			IfFastWineAllowed:                          false,
			NonFestal:                                  false,
			OnlyOfficeOfFeastSungEvenIfSunday:          false,
			OfficeOfFeastAndResurrectionalSungIfSunday: false,
			WithGreatDoxologyAtMatins:                  false,
			WithMatinsGospel:                           false,
			WithPolyeleos:                              false,
			WithSixStichera:                            true,
			WithVespersReading:                         false,
			WithVigil:                                  false,
		}
	default:
		return Characteristics{
			IfFastFishAllowed:                          false,
			IfFastOilAllowed:                           false,
			IfFastWineAllowed:                          false,
			NonFestal:                                  true,
			OnlyOfficeOfFeastSungEvenIfSunday:          false,
			OfficeOfFeastAndResurrectionalSungIfSunday: false,
			WithGreatDoxologyAtMatins:                  false,
			WithMatinsGospel:                           false,
			WithPolyeleos:                              false,
			WithSixStichera:                            false,
			WithVespersReading:                         false,
			WithVigil:                                  false,
		}
	}
}
func (r Ranks) Symbol() {
	switch r {
	case ClassOne:
	case ClassTwo:
	case ClassThree:
	case ClassFour:
	case ClassFive:
	default:
	}
}
