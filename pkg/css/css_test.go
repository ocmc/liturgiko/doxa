package css

import (
	"encoding/json"
	"fmt"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"strings"
	"testing"
)

const printJson = false

func init() {
	doxlog.Init("", "", true)
	doxlog.Info("doxLog initialized")
}
func TestParseCss01(t *testing.T) {
	const input = `
p.heirmos {
    text-align:left;
    text-indent: 0px;
    line-height:150%;
    margin-bottom: 10px;
}
.actor {
  color: red;
}
`
	stylesheet, err := NewStyleSheet("", "", false)
	if err != nil {
		t.Error(err)
		return
	}
	errors := stylesheet.Parse(input)
	if len(errors) > 0 {
		for _, e := range errors {
			t.Error(e.Error())
			return
		}
	}

	expectedMapLength := 2 // because of .nc rule
	expectedRuleSelector := "p.heirmos"
	if len(stylesheet.RuleMap) == expectedMapLength {
		if stylesheet.ContainsRule(expectedRuleSelector) {
			expectedDeclarationCount := 4
			actualDeclarationCount := len(stylesheet.RuleMap[expectedRuleSelector].Declarations)
			if actualDeclarationCount != expectedDeclarationCount {
				t.Errorf("expected len(Declarations) to be %d, got %d", expectedDeclarationCount, actualDeclarationCount)
			}
		} else {
			t.Errorf("expected %s to be a key in Map, but not found", expectedRuleSelector)
		}
	} else {
		t.Errorf("expected len(Map) to be %d, got %d", expectedMapLength, len(stylesheet.RuleMap))
	}
	if printJson {
		atemJson, _ := json.MarshalIndent(stylesheet, "", " ")
		fmt.Println(string(atemJson))
	}
}
func TestParseCss02(t *testing.T) {
	const input = `
p.heirmos:first-letter {
    color:red;
    font-size:180%;
    font-weight:normal;
}
`
	stylesheet, err := NewStyleSheet("", "", false)
	if err != nil {
		t.Error(err)
	}

	errors := stylesheet.Parse(input)
	if len(errors) > 0 {
		for _, e := range errors {
			t.Error(e.Error())
			return
		}
	}
	expectedMapLength := 2 // because of .nc rule
	expectedRuleSelector := "p.heirmos"
	if len(stylesheet.RuleMap) == expectedMapLength {
		if stylesheet.ContainsRule(expectedRuleSelector) {
			expectedDeclarationCount := 3
			actualDeclarationCount := len(stylesheet.RuleMap[expectedRuleSelector].Declarations)
			if actualDeclarationCount != expectedDeclarationCount {
				t.Errorf("expected len(Declarations) to be %d, got %d", expectedDeclarationCount, actualDeclarationCount)
			}
		} else {
			t.Errorf("expected %s to be a key in Map, but not found", expectedRuleSelector)
		}
		expectedPseudo := "first-letter"
		if stylesheet.RuleMap[expectedRuleSelector].Pseudo != expectedPseudo {
			t.Errorf("expected pseudo == %s, got %s", expectedPseudo, stylesheet.RuleMap[expectedRuleSelector].Pseudo)
		}
	} else {
		t.Errorf("expected len(Map) to be %d, got %d", expectedMapLength, len(stylesheet.RuleMap))
	}
	if printJson {
		atemJson, _ := json.MarshalIndent(stylesheet, "", " ")
		fmt.Println(string(atemJson))
	}
}
func TestParseCss03(t *testing.T) {
	const input = `
@font-face {
	font-family: "Arimo";
	src:
		url('fonts/Arimo-Bold.ttf') format('truetype'),
		url('fonts/Arimo-BoldItalic.ttf') format('truetype'),
		url('fonts/Arimo-Italic.ttf') format('truetype'),
		url('fonts/Arimo-Regular.ttf') format('truetype');
}`
	stylesheet, err := NewStyleSheet("", "", false)
	if err != nil {
		t.Error(err)
	}
	errors := stylesheet.Parse(input)
	if len(errors) > 0 {
		for _, e := range errors {
			t.Error(e.Error())
			return
		}
	}
	expectedMapLength := 1 // does NOT include .nc rule
	expectedRuleSelector := "@font-face:Arimo"
	if len(stylesheet.FontFaceMap) == expectedMapLength {
		if stylesheet.ContainsFontFace(expectedRuleSelector) {
			expectedDeclarationCount := 2
			actualDeclarationCount := len(stylesheet.FontFaceMap[expectedRuleSelector].Declarations)
			if actualDeclarationCount != expectedDeclarationCount {
				t.Errorf("expected len(Declarations) to be %d, got %d", expectedDeclarationCount, actualDeclarationCount)
			}
		} else {
			t.Errorf("expected %s to be a key in FontFaceMap, but not found", expectedRuleSelector)
		}
	} else {
		t.Errorf("expected len(FontFaceMap) to be %d, got %d", expectedMapLength, len(stylesheet.FontFaceMap))
	}
	if printJson {
		atemJson, _ := json.MarshalIndent(stylesheet, "", " ")
		fmt.Println(string(atemJson))
	}
}
func TestParseCss04(t *testing.T) {
	const input = `
title, p.designation, p.mixed, p.mode, p.melody {
    text-align:center;
}`
	stylesheet, err := NewStyleSheet("", "", false)
	if err != nil {
		t.Error(err)
		return
	}
	errors := stylesheet.Parse(input)
	if len(errors) > 0 {
		for _, e := range errors {
			t.Error(e.Error())
			return
		}
	}
	expectedMapLength := 6 // because of .nc rule
	expectedRuleSelector := "title"
	if len(stylesheet.RuleMap) == expectedMapLength {
		if stylesheet.ContainsRule(expectedRuleSelector) {
			expectedDeclarationCount := 1
			actualDeclarationCount := len(stylesheet.RuleMap[expectedRuleSelector].Declarations)
			if actualDeclarationCount != expectedDeclarationCount {
				t.Errorf("expected len(Declarations) to be %d, got %d", expectedDeclarationCount, actualDeclarationCount)
			}
		} else {
			t.Errorf("expected %s to be a key in Map, but not found", expectedRuleSelector)
		}
	} else {
		t.Errorf("expected len(Map) to be %d, got %d", expectedMapLength, len(stylesheet.RuleMap))
	}
	if printJson {
		atemJson, _ := json.MarshalIndent(stylesheet, "", " ")
		fmt.Println(string(atemJson))
	}
}
func TestParseCss05(t *testing.T) {
	const input = `
.content, .index-content > a >.fa {
    color: #000000;
}
`
	stylesheet, err := NewStyleSheet("", "", false)
	if err != nil {
		t.Error(err)
		return
	}
	errors := stylesheet.Parse(input)
	if len(errors) > 0 {
		for _, e := range errors {
			t.Error(e.Error())
			return
		}
	}
	expectedMapLength := 3 // because of .nc rule
	expectedRuleSelector := ".content"
	if len(stylesheet.RuleMap) == expectedMapLength {
		if stylesheet.ContainsRule(expectedRuleSelector) {
			expectedDeclarationCount := 1
			actualDeclarationCount := len(stylesheet.RuleMap[expectedRuleSelector].Declarations)
			if actualDeclarationCount != expectedDeclarationCount {
				t.Errorf("expected len(Declarations) to be %d, got %d", expectedDeclarationCount, actualDeclarationCount)
			}
		} else {
			t.Errorf("expected %s to be a key in Map, but not found", expectedRuleSelector)
		}
	} else {
		t.Errorf("expected len(Map) to be %d, got %d", expectedMapLength, len(stylesheet.RuleMap))
	}
	if printJson {
		atemJson, _ := json.MarshalIndent(stylesheet, "", " ")
		fmt.Println(string(atemJson))
	}
}
func TestParseCss06(t *testing.T) {
	const input = `
p.heirmos {
    text-align:left;
    text-indent: 0px;
    line-height:150%;
    margin-bottom: 10px;
}
/* This is a single-line comment */
.actor {
  color: red;  /* Set text color to red */
}
/* This is
a multi-line
comment */ 
/*  .actor {
  color: red;  /* Set text color to red */
} */ 
`
	stylesheet, err := NewStyleSheet("", "", false)
	if err != nil {
		t.Error(err)
		return
	}
	errors := stylesheet.Parse(input)
	if len(errors) > 0 {
		for _, e := range errors {
			t.Error(e.Error())
			return
		}
	}
	expectedMapLength := 3 // because of .nc rule
	expectedRuleSelector := "p.heirmos"
	if len(stylesheet.RuleMap) == expectedMapLength {
		if stylesheet.ContainsRule(expectedRuleSelector) {
			expectedDeclarationCount := 4
			actualDeclarationCount := len(stylesheet.RuleMap[expectedRuleSelector].Declarations)
			if actualDeclarationCount != expectedDeclarationCount {
				t.Errorf("expected len(Declarations) to be %d, got %d", expectedDeclarationCount, actualDeclarationCount)
			}
		} else {
			t.Errorf("expected %s to be a key in Map, but not found", expectedRuleSelector)
		}
	} else {
		t.Errorf("expected len(Map) to be %d, got %d", expectedMapLength, len(stylesheet.RuleMap))
	}
	if printJson {
		atemJson, _ := json.MarshalIndent(stylesheet, "", " ")
		fmt.Println(string(atemJson))
	}
}
func TestSort(t *testing.T) {
	//fileIn := "/Users/mac002/temp/frSeraphim.css"
	//fileOut := "/Users/mac002/temp/frSeraphimSorted.css"
	fileIn := "/Users/mac002/temp/app.css"
	fileOut := "/Users/mac002/temp/appSorted.css"
	err := SortCss(fileIn, fileOut)
	if err != nil {
		t.Error(err)
	}
}
func TestParseCss07(t *testing.T) {
	const input = `
body {
  font-size: 12pt;
}
p.heirmos {
    line-height:150%;
    margin: 0 3pt;
    margin-top: 2pt;
}
p.versiondesignation  {
  color: red;
  font-size: 6pt;
  font-style: normal;
  font-weight: normal;
  vertical-align: middle;
}
span.versiondesignation  {
    color: red;
    float: right;
    font-size:50%;
    font-style:normal;
    font-weight:lighter;
    vertical-align:middle;
}
`
	stylesheet, err := NewStyleSheet("", "", true)
	if err != nil {
		t.Error(err)
		return
	}
	errors := stylesheet.Parse(input)
	if len(errors) > 0 {
		for _, e := range errors {
			t.Error(e.Error())
			return
		}
	}

	expectedMapLength := 2 // because of .nc rule
	expectedRuleSelector := "p.heirmos"
	if len(stylesheet.RuleMap) == expectedMapLength {
		if stylesheet.ContainsRule(expectedRuleSelector) {
			expectedDeclarationCount := 6
			actualDeclarationCount := len(stylesheet.RuleMap[expectedRuleSelector].Declarations)
			if actualDeclarationCount != expectedDeclarationCount {
				t.Errorf("expected len(Declarations) to be %d, got %d", expectedDeclarationCount, actualDeclarationCount)
			}
		} else {
			t.Errorf("expected %s to be a key in Map, but not found", expectedRuleSelector)
		}
	} else {
		t.Errorf("expected len(Map) to be %d, got %d", expectedMapLength, len(stylesheet.RuleMap))
	}
	if printJson {
		atemJson, _ := json.MarshalIndent(stylesheet, "", " ")
		fmt.Println(string(atemJson))
	}
}
func TestParseCss08(t *testing.T) {
	const input = `
span.versiondesignation  {
    color: red;
    float: right;
    font-size:smaller;
    font-style:normal;
    font-weight:lighter;
    vertical-align:middle;
}
`
	stylesheet, err := NewStyleSheet("", "", true)
	if err != nil {
		t.Error(err)
		return
	}
	errors := stylesheet.Parse(input)
	if len(errors) > 0 {
		for _, e := range errors {
			t.Error(e.Error())
			return
		}
	}
	type expectedRules struct {
		Selector         string
		DeclarationCount int
	}
	expectedMapLength := 2
	var expectedRuleSelectors []*expectedRules
	expectedRuleSelectors = append(expectedRuleSelectors, &expectedRules{
		Selector:         "span.versiondesignation",
		DeclarationCount: 6,
	})
	expectedRuleSelectors = append(expectedRuleSelectors, &expectedRules{
		Selector:         "body",
		DeclarationCount: 2,
	})
	if len(stylesheet.RuleMap) == expectedMapLength {
		for _, expectedRule := range expectedRuleSelectors {
			if !stylesheet.ContainsRule(expectedRule.Selector) {
				t.Error(fmt.Sprintf("expected %s but missing", expectedRule.Selector))
			} else {
				rule := stylesheet.RuleMap[expectedRule.Selector]
				if len(rule.Declarations) != expectedRule.DeclarationCount {
					t.Errorf("expected %d declarations for selector %s, got %d", expectedRule.DeclarationCount, expectedRule.Selector, len(rule.Declarations))
				}
				if rule.Selector == "span.versiondesignation" {
					if rule.Selector == "span.versiondesignation" {
						for _, declaration := range rule.Declarations {
							if declaration.Identifier == "font-size" {
								if strings.HasPrefix(declaration.Value, "6") {
									if !strings.HasSuffix(declaration.Value, "pt") {
										t.Error(fmt.Sprintf("expected %s %s.Value to end in pt", rule.Selector, declaration.Value))
									}
								}
							}
						}
					}
				}
			}
		}
		if printJson {
			atemJson, _ := json.MarshalIndent(stylesheet, "", " ")
			fmt.Println(string(atemJson))
		}
	} else {
		t.Errorf("expect map length %d got %d", expectedMapLength, len(stylesheet.RuleMap))
	}
}
func TestParseCss09(t *testing.T) {
	const input = `
p.versiondesignation  {
    font-size:smaller;
}
`
	stylesheet, err := NewStyleSheet("", "", true)
	if err != nil {
		t.Error(err)
		return
	}
	errors := stylesheet.Parse(input)
	if len(errors) > 0 {
		for _, e := range errors {
			t.Error(e.Error())
			return
		}
	}
	type expectedRules struct {
		Selector         string
		DeclarationCount int
	}
	expectedMapLength := 2
	var expectedRuleSelectors []*expectedRules
	expectedRuleSelectors = append(expectedRuleSelectors, &expectedRules{
		Selector:         "p.versiondesignation",
		DeclarationCount: 1,
	})
	expectedRuleSelectors = append(expectedRuleSelectors, &expectedRules{
		Selector:         "body",
		DeclarationCount: 2,
	})
	if len(stylesheet.RuleMap) == expectedMapLength {
		for _, expectedRule := range expectedRuleSelectors {
			if !stylesheet.ContainsRule(expectedRule.Selector) {
				t.Error(fmt.Sprintf("expected %s but missing", expectedRule.Selector))
			} else {
				rule := stylesheet.RuleMap[expectedRule.Selector]
				if len(rule.Declarations) != expectedRule.DeclarationCount {
					t.Errorf("expected %d declarations for selector %s, got %d", expectedRule.DeclarationCount, expectedRule.Selector, len(rule.Declarations))
				}
				if rule.Selector == "p.versiondesignation" {
					if rule.Selector == "p.versiondesignation" {
						for _, declaration := range rule.Declarations {
							if declaration.Identifier == "font-size" {
								if strings.HasPrefix(declaration.Value, "6") {
									if !strings.HasSuffix(declaration.Value, "pt") {
										t.Error(fmt.Sprintf("expected %s %s.Value to end in pt", rule.Selector, declaration.Value))
									}
								}
							}
						}
					}
				}
			}
		}
		if printJson {
			atemJson, _ := json.MarshalIndent(stylesheet, "", " ")
			fmt.Println(string(atemJson))
		}
	} else {
		t.Errorf("expect map length %d got %d", expectedMapLength, len(stylesheet.RuleMap))
	}
}
func TestExpandToFourSides(t *testing.T) {
	inputs := []string{
		"1em",
		"1em 2pt",
		"1em 2em 3em",
		"1em 2em 3em 4em",
	}
	id := "margin"
	expectedIdentifiers := []string{"top", "bottom", "left", "right"}
	for _, input := range inputs {
		d := new(Declaration)
		d.Identifier = id
		d.Value = input
		sides, err := expandToFourSides(d)
		if err != nil {
			t.Errorf("error for input %s: %v", input, err)
		}
		rule := new(Rule)
		rule.Selector = "p.hymn"
		rule.AddDeclarations(sides)
		for _, identifier := range expectedIdentifiers {
			if !rule.HasIdentifier(fmt.Sprintf("%s-%s", id, identifier)) {
				t.Errorf("missing identifier %s in rule %s", identifier, rule.Selector)
			}
		}
	}
}
