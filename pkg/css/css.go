// Package css provides formatting for liturgical texts
package css

import (
	"bytes"
	"fmt"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/enums/selectorTypes"
	"github.com/liturgiko/doxa/pkg/fonts"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/tdewolff/parse/v2/css"
	"sort"
	"strconv"
	"strings"
)

const AlwbStyleSheetPath = "https://raw.githubusercontent.com/AGES-Initiatives/ages-alwb-assets/master/net.ages.liturgical.workbench.website.assets.ages/css/alwb.css"
const AlwbPdfStyleSheetPath = "https://raw.githubusercontent.com/AGES-Initiatives/ages-alwb-templates/master/net.ages.liturgical.workbench.templates/c-generator-settings/pdf-settings/pdf.stylesheet_goarch.ares"

// StyleSheet holds the results of parsing a css stylesheet.
// @font-face rules are not unique keys, so they are held in a separate map (FontFaceMap) and the key is @font-face:{font-family name}, e.g. @font-face:Arimo
// All other css rules are in the RuleMap.
type StyleSheet struct {
	ID          string
	CssPath     string // path to the local css file, e.g. assets/css/app.css
	RuleMap     map[string]*Rule
	FontFaceMap map[string]*Rule
	RulesToAdd  []*Rule // missing rules to be appended to local file
	ForPdf      bool
}

// NewStyleSheet creates an instance and if the cssPath
// is not empty, loads the css rules from the css file.
func NewStyleSheet(id, cssPath string, pdfTest bool) (*StyleSheet, error) {
	var s StyleSheet
	var err error
	s.ID = id
	s.CssPath = cssPath
	s.ForPdf = strings.HasSuffix(s.CssPath, "pdf.css") || pdfTest
	s.RuleMap = make(map[string]*Rule)
	s.FontFaceMap = make(map[string]*Rule)
	if len(s.CssPath) > 0 {
		err = s.ParseLocal()
	}
	return &s, err
}
func (s *StyleSheet) PrepareForPdfGeneration() error {
	// We will do some pre-processing.
	// First, we need to get the font-size for the body.
	// If the body rule does not exist, we will create one.
	// And, add font-size and line-height declarations.
	// Then, we will iterate the rules, setting or adding line-height
	// using the rule's font-size if it has it,
	// otherwise, we use the body font-size.
	// When finished, every rule will have an appropriate
	// line-height, normalized to mm.
	fs := "font-size"
	lh := "line-height"
	// body-rule
	bodyFontFloat, err := s.InitializeForPdfGeneration()
	if err != nil {
		return err
	}
	if bodyFontFloat <= 0 {
		msg := "body font size must be greater than zero"
		doxlog.Errorf(msg)
		return fmt.Errorf(msg)
	}
	bodyFontValue := fmt.Sprintf("%fpt", bodyFontFloat)

	for k, rule := range s.RuleMap {
		if rule.HasIdentifier("font-family") {
			dec := rule.GetDeclaration("font-family")
			dec.NormalizeFontFamily()
		}
		if k == "body" || strings.HasPrefix(k, "@") {
			continue
		}
		isSpan := strings.HasPrefix(rule.Selector, "span.")

		if rule.HasIdentifier(fs) {
			d := rule.GetDeclaration(fs)
			if d.Units == "" {
				err = d.SetFloatAndUnit()
				if err != nil {
					doxlog.Errorf(err.Error())
					return err
				}
				if !d.Normalized {
					err = d.NormalizeFloatAndUnit(bodyFontFloat)
					if err != nil {
						doxlog.Errorf(err.Error())
						return err
					}
				}
			}
		} else {
			// all p elements will have a font-size added if absent.
			if strings.HasPrefix(rule.Selector, "p.") {
				if !rule.HasIdentifier("font-size") {
					_, err = rule.AddFontSizeDeclaration(bodyFontValue)
					if err != nil {
						return err
					}
				}
			}
		}
		if rule.HasIdentifier(lh) {
			d := rule.GetDeclaration(lh)
			err = d.SetFloatAndUnit()
			if err != nil {
				return fmt.Errorf("%s - %s: %v", rule.Selector, d.Identifier, err)
			}
			err = d.NormalizeFloatAndUnit(bodyFontFloat)
			if err != nil {
				return fmt.Errorf("%s - %s: %v", rule.Selector, d.Identifier, err)
			}
		} else {
			if isSpan {
				continue
			}
			err = rule.AddLineHeightDeclaration(bodyFontFloat)
			if err != nil {
				return fmt.Errorf("%s: %v", rule.Selector, err)
			}
		}
		// now iterate the other declarations
		// and process any defined in the case statements
		var toRemove []string    // identifiers for removal of declarations
		var toAdd []*Declaration // to add to the rule
		for _, d := range rule.Declarations {
			switch d.Identifier {
			case "border", "margin", "padding":
				var sides []*Declaration
				sides, err = expandToFourSides(d)
				if err != nil {
					// TODO
					continue
				}
				// add check for existing sides
				if len(sides) == 4 {
					for _, side := range sides {
						if !rule.HasIdentifier(side.Identifier) {
							toAdd = append(toAdd, side)
						}
					}
					toRemove = append(toRemove, d.Identifier)
				}
			default:
				// do nothing
			}
		}
		if len(toRemove) > 0 {
			rule.RemoveDeclarations(toRemove)
		}
		if len(toAdd) > 0 {
			rule.AddDeclarations(toAdd)
		}
	}

	return nil
}

// InitializeForPdfGeneration ensures the existence of and returns the body font-size points as a float64.
// Because there must be a body-rule, if one doesn't exist, it is created.
// Also, as a result of calling this function, it is guaranteed that the
// body-rule contains a font-size declaration in points and line-height in mm.
func (s *StyleSheet) InitializeForPdfGeneration() (float64, error) {
	// scenarios
	// 1. No body-rule found
	// 2. Found, but missing font-size.
	// 3. Found, but missing line-height
	fs := "font-size"
	lh := "line-height"
	var bodyRule *Rule
	var bodyFontSizeDeclaration *Declaration
	var bodyLineHeightDeclaration *Declaration
	var bodyFontFloat float64
	var err error
	var found bool
	if bodyRule, found = s.RuleMap["body"]; !found {
		bodyRule, err = s.CreateBodyRule()
		if err != nil {
			return 0, err
		}
	}
	// confirm we have font-size and get its float value
	bodyFontSizeDeclaration = bodyRule.GetDeclaration(fs)
	if bodyFontSizeDeclaration == nil {
		bodyFontFloat, err = bodyRule.AddDefaultBodyFontSizeDeclaration()
		if err != nil {
			return 0, err
		}
	} else {
		if !bodyFontSizeDeclaration.Normalized {
			err = bodyFontSizeDeclaration.SetFloatAndUnit()
			if err != nil {
				return 0, err
			}
		}
		bodyFontFloat = bodyFontSizeDeclaration.Float
	}
	// confirm we have line-height
	bodyLineHeightDeclaration = bodyRule.GetDeclaration(lh)
	if bodyLineHeightDeclaration == nil {
		err = bodyRule.AddLineHeightDeclaration(bodyFontFloat)
		if err != nil {
			return 0, err
		}
	} else {
		err = bodyLineHeightDeclaration.NormalizeFloatAndUnit(bodyFontFloat)
		if err != nil {
			return 0, err
		}
	}

	return bodyFontFloat, nil
}

// CreateBodyRule returns a body-rule with font-size and line-height declarations
func (s *StyleSheet) CreateBodyRule() (*Rule, error) {
	var bodyRule *Rule
	var bodyFontFloat float64
	var err error
	var found bool
	if bodyRule, found = s.RuleMap["body"]; found {
		return bodyRule, fmt.Errorf("body-rule already exists")
	}
	bodyRule = new(Rule)
	bodyRule.Selector = "body"
	bodyRule.PDF = s.ForPdf
	bodyFontFloat, err = bodyRule.AddDefaultBodyFontSizeDeclaration()
	if err != nil {
		return bodyRule, err
	}
	err = bodyRule.AddLineHeightDeclaration(bodyFontFloat)
	if err != nil {
		return bodyRule, err
	}
	s.Add("body", bodyRule)
	return bodyRule, nil
}
func (s *StyleSheet) String() string {
	sb := strings.Builder{}
	var keys []string
	for k, _ := range s.RuleMap {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	for _, k := range keys {
		v, _ := s.RuleMap[k]
		sb.WriteString(v.String())
	}
	return sb.String()
}

// AddMissingRulesForHtml loads a remote css and
// adds any css classes not in the Stylesheet.
// If you want them to be added to the physical files as well,
// call SerializeMissingRules.
// To load a remote ALWB pdf css, call AddMissingRulesForPdf.
func (s *StyleSheet) AddMissingRulesForHtml(cssUrl string) error {
	contents, err := ltfile.ContentsFromUrl(cssUrl)
	if err != nil {
		return fmt.Errorf("error in pkg/css AddMissingRulesForHtml() reading css from %s: %v", cssUrl, err)
	}
	remoteCss, err := NewStyleSheet("remoteCss", "", false)
	errors := remoteCss.Parse(contents)
	if len(errors) > 0 {
		for _, e := range errors {
			doxlog.Error(e.Error())
		}
	}

	for k, v := range remoteCss.RuleMap {
		if !s.ContainsRule(k) {
			if !(strings.HasPrefix(k, "p.") || strings.HasPrefix(k, "span.") || strings.HasPrefix(k, "hr.") || strings.HasPrefix(k, "a.")) {
				continue
			}
			s.Add(k, v)
			s.RulesToAdd = append(s.RulesToAdd, v)
		}
	}
	return nil
}

// AddMissingRulesForPdf converts an alwb pdf stylesheet to standard css format
// and adds missing rules to the local css
func (s *StyleSheet) AddMissingRulesForPdf(cssUrl string) error {
	// load remote pdf css
	contents, err := ltfile.ContentsFromUrl(cssUrl)
	if err != nil {
		return fmt.Errorf("error in pkg/css AddMissingRulesForHtml() reading css from %s: %v", cssUrl, err)
	}
	lines := strings.Split(contents, "\n")

	rule := new(Rule)
	rule.PDF = true
	rule.Type = selectorTypes.Class

	for _, l := range lines {
		l = strings.TrimSpace(l)
		if len(l) == 0 {
			continue
		}
		if strings.HasPrefix(l, "A_Resource") {
			continue
		}
		if strings.Contains(l, "=") {
			parts := strings.Split(l, "=")
			selector := strings.TrimSpace(parts[0])
			if strings.HasPrefix(selector, "p.") ||
				strings.HasPrefix(selector, "a.") ||
				strings.HasPrefix(selector, "span.") ||
				strings.HasPrefix(selector, "hr.") ||
				strings.HasPrefix(selector, "@font-face") {
				rule.Selector = selector
			} else {
				continue
				//rule.Selector = fmt.Sprintf("p.%s", selector)
			}
			continue
		}
		if strings.Contains(l, ";") {
			parts := strings.Split(l[:strings.Index(l, ";")], ":")
			declaration := new(Declaration)
			declaration.Identifier = strings.TrimSpace(parts[0])
			if declaration.Identifier == "font-family" {
				continue
			}
			if declaration.Identifier == "padding-after" {
				declaration.Identifier = "padding-bottom" // or padding-right
			}
			if declaration.Identifier == "padding-before" {
				declaration.Identifier = "padding-top" // or padding-left
			}
			declaration.Value = strings.TrimSpace(parts[1])
			rule.AddDeclaration(declaration)
		}
		if strings.HasSuffix(l, `"`) {
			// add both a p and span rule
			var rules []*Rule
			rules = append(rules, rule)
			var spanRule *Rule
			spanRule, err = rule.CreateSpanRule()
			if err == nil {
				rules = append(rules, spanRule)
			}
			for _, r := range rules {
				if !s.ContainsRule(r.Selector) {
					// create a canonical css original (ALWB pdf css rule is not canonical css)
					sb := strings.Builder{}
					sb.WriteString(fmt.Sprintf("%s {\n", r.Selector))
					indent := "  "
					for _, d := range r.Declarations {
						important := ";"
						if d.Important {
							important = " !important;"
						}
						sb.WriteString(fmt.Sprintf("%s%s: %s%s\n", indent, d.Identifier, d.Value, important))
					}
					sb.WriteString("}\n")
					r.OriginalRule = sb.String()
					r.PDF = true
					s.RulesToAdd = append(s.RulesToAdd, r)
					if !s.ContainsRule(r.Selector) {
						s.Add(rule.Selector, r)
					}
				}
			}
			rule = new(Rule)
			rule.PDF = true
		}
	}
	return nil

}

// SerializeMissingRules during the atem2lml template conversion process
// if a css class name in an atem is not found in the local css file,
// it is added in memory.  This function serializes the missing css rules
// by appending them to the StyleSheet.CssPath.
// After the missing rules have been serialized,
// stylesheet.RulesToAdd will be zero length.
func (s *StyleSheet) SerializeMissingRules() error {
	if !s.RulesNeedToBeAdded() {
		return nil
	}
	sort.Slice(s.RulesToAdd, func(i, j int) bool {
		return s.RulesToAdd[i].Selector < s.RulesToAdd[j].Selector
	})
	lines, err := ltfile.GetFileLines(s.CssPath)
	if err != nil {
		return fmt.Errorf("in pkg/css css.go SerializeMissingRules error getting lines from %s: %v", s.CssPath, err)
	}
	for _, rule := range s.RulesToAdd {
		ruleString := fmt.Sprintf("%v", rule.OriginalRule)
		lines = append(lines, ruleString)
	}
	err = ltfile.WriteLinesToFile(s.CssPath, lines)
	if err != nil {
		return fmt.Errorf("in pkg/css css.go SerializeMissingRules error updating %s: %v", s.CssPath, err)
	}
	s.RulesToAdd = []*Rule{}
	return nil
}

// RulesNeedToBeAdded returns true if len(stylesheet.RulesToAdd) > 0
func (s *StyleSheet) RulesNeedToBeAdded() bool {
	return len(s.RulesToAdd) > 0
}

// ContainsRule returns true if the stylesheet RuleMap has a key == selector
func (s *StyleSheet) ContainsRule(selector string) bool {
	// these are javascript hooks used by alwb.
	if strings.HasPrefix(selector, "p.bmc_") ||
		strings.HasPrefix(selector, "p.emc_") {
		return true
	}
	if s.RuleMap == nil {
		doxlog.Errorf("css rulemap is nil")
		return false
	}
	if _, ok := s.RuleMap[selector]; ok {
		return true
	} else {
		// see if it exists without an element selector
		dot := strings.Index(selector, ".")
		if dot > 0 {
			if _, ok = s.RuleMap[selector]; ok {
				return true
			} else {
				return false
			}
		} else {
			return false
		}
	}
}

// GetRule returns true if the stylesheet RuleMap has a key == selector
func (s *StyleSheet) GetRule(selector string) (*Rule, bool) {
	if v, ok := s.RuleMap[selector]; ok {
		return v, true
	} else {
		// see if it exists without an element selector
		dot := strings.Index(selector, ".")
		if dot > 0 {
			if v, ok = s.RuleMap[selector]; ok {
				return v, true
			} else {
				return v, false
			}
		} else {
			return v, false
		}
	}
}

// ContainsFontFace returns true if the stylesheet FontFaceMap has a key == selector
func (s *StyleSheet) ContainsFontFace(selector string) bool {
	if _, ok := s.FontFaceMap[selector]; ok {
		return true
	} else {
		return false
	}
}

// Classnames returns all line break, horizontal rule, paragraph and span classnames found in the css file.
func (s *StyleSheet) Classnames() ([]string, []error) {
	var classnames []string
	var errors []error
	nameMap := make(map[string]string)
	for key, _ := range s.RuleMap {
		if strings.HasPrefix(key, "br.") ||
			strings.HasPrefix(key, "hr.") ||
			strings.HasPrefix(key, "p.") ||
			strings.HasPrefix(key, "span.") {
			nameMap[key] = ""
		}
	}
	for key, _ := range nameMap {
		classnames = append(classnames, key)
	}
	sort.Strings(classnames)
	if len(errors) == 0 {
		return classnames, nil
	} else {
		return classnames, errors
	}
}

// ParseLocal loads the content of StyleSheet.CssPath into
// StyleSheet.RuleMap and .FontMap
func (s *StyleSheet) ParseLocal() error {
	content, err := ltfile.GetFileContent(s.CssPath)
	if err != nil {
		return fmt.Errorf("could not get content from %s: %v", s.CssPath, err)
	}
	errors := s.Parse(content)
	if len(errors) > 0 {
		for _, e := range errors {
			doxlog.Error(fmt.Sprintf("error loading the css: %v", e))
		}
	}
	return nil
}

// Parse parses the supplied content (a css stylesheet)
// and puts the results into the instance of StyleSheet.
func (s *StyleSheet) Parse(input string) (errors []error) {
	if len(input) == 0 {
		return
	}
	rules := s.GetRuleLines(input)
	rule := new(Rule)
	rule.PDF = s.ForPdf
	declaration := new(Declaration)
	selectors := make([]string, 0)
	for _, ruleLines := range rules {
		theOriginal := strings.Join(ruleLines, "")
		p := css.NewParser(bytes.NewBufferString(theOriginal), false)
		out := ""
		for {
			gt, _, data := p.Next()
			if gt == css.ErrorGrammar {
				break
			} else if gt == css.QualifiedRuleGrammar {
				out += string(data)
				for _, val := range p.Values() {
					out += string(val.Data)
				}
				selectors = append(selectors, out)
				out = ""
			} else if gt == css.AtRuleGrammar || gt == css.BeginAtRuleGrammar || gt == css.BeginRulesetGrammar || gt == css.DeclarationGrammar {
				out += string(data)
				if gt == css.DeclarationGrammar {
					declaration.Identifier = out
					out = ""
				}
				for _, val := range p.Values() {
					out += string(val.Data)
				}
				if gt == css.BeginAtRuleGrammar || gt == css.BeginRulesetGrammar {
					if len(selectors) == 0 {
						rule.AddSelector(out)
					} else {
						rule.AddSelector(out)
						selectors = append(selectors, out)
					}
					out = ""
				} else if gt == css.AtRuleGrammar || gt == css.DeclarationGrammar {
					declaration.AddValue(out)
					//err := declaration.SetFloatAndUnit()
					//if err != nil {
					//	errors = append(errors, err)
					//}
					rule.AddDeclaration(declaration)
					declaration = new(Declaration)
					out = ""
				}
			} else {
				if len(rule.Selector) > 0 {
					if len(selectors) > 0 {
						for _, selector := range selectors {
							theRule := new(Rule)
							rule.PDF = s.ForPdf
							theRule.AddSelector(selector)
							theRule.Attribute = rule.Attribute
							theRule.Type = rule.Type
							theRule.Pseudo = rule.Pseudo
							theRule.Combinator = rule.Combinator
							theRule.Declarations = rule.Declarations
							theRule.OriginalRule = rule.OriginalRule
							braceStart := strings.Index(theRule.OriginalRule, "{")
							if braceStart > 0 {
								theRule.OriginalRule = fmt.Sprintf("%s %s", theRule.Selector, theRule.OriginalRule[braceStart:])
							}
							s.Add(theRule.Selector, theRule)
						}
						selectors = make([]string, 0)
					} else {
						rule.OriginalRule = theOriginal
						s.Add(rule.Selector, rule)
						rule = new(Rule)
						rule.PDF = s.ForPdf
					}
				}
				out = ""
			}
		}
	}

	//-------------------------------
	if s.ForPdf {
		err := s.PrepareForPdfGeneration()
		if err != nil {
			errors = append(errors, err)
		}
		//s.DumpLineHeights(true)
	}
	return errors
}
func (s *StyleSheet) DumpLineHeights(zeroOnly bool) {
	fmt.Println()
	for _, rule := range s.RuleMap {
		if rule.HasIdentifier("line-height") {
			d := rule.GetDeclaration("line-height")
			if zeroOnly && d.Float > 0 {
				continue
			}
			fmt.Printf("%s line-height: %f%s\n", rule.Selector, d.Float, d.Units)
		}
	}
}
func (s *StyleSheet) Add(key string, rule *Rule) {
	if rule.Type == selectorTypes.FontFace {
		for _, d := range rule.Declarations {
			if d.Identifier == "font-family" {
				value, err := strconv.Unquote(d.Value)
				if err == nil {
					key = "@font-face:" + value
				}
			}
		}
		if _, ok := s.FontFaceMap[key]; !ok {
			s.FontFaceMap[key] = rule
		}
	} else {
		if s.ContainsRule(key) {
			//			fmt.Printf("duplicate css rule %s\n", key)
		} else {
			s.RuleMap[key] = rule
		}
	}
}

type Rule struct {
	Selector         string
	Type             selectorTypes.SelectorType
	Combinator       string
	Attribute        string
	Pseudo           string
	Declarations     []*Declaration
	OriginalRule     string // the rule as found in the file from which it was loaded
	OriginalSelector string // the selector part of the original rule
	PDF              bool   // is this rule for pdf instead of html?
}

func (r *Rule) CreateSpanRule() (*Rule, error) {
	if strings.HasPrefix(r.Selector, "p.") {
		spanRule := r.Copy()
		spanRule.Selector = fmt.Sprintf("span.%s", r.Selector[2:])
		return spanRule, nil
	}
	return nil, fmt.Errorf("original rule %s must start with p", r.Selector)
}
func (r *Rule) Copy() *Rule {
	return &Rule{
		Selector:         r.Selector,
		Type:             r.Type,
		Combinator:       r.Combinator,
		Attribute:        r.Attribute,
		Pseudo:           r.Pseudo,
		Declarations:     append([]*Declaration{}, r.Declarations...),
		OriginalRule:     r.OriginalRule,
		OriginalSelector: r.OriginalSelector,
		PDF:              r.PDF,
	}
}

// HasIdentifier searches the rule's declarations to see if a declaration identifier matches the pattern
func (r *Rule) HasIdentifier(pattern string) bool {
	for _, d := range r.Declarations {
		if d.Identifier == pattern {
			return true
		}
	}
	return false
}

// GetDeclaration searches the rule's declarations identifier to find one that matches the pattern.  If found, returns the declaration
func (r *Rule) GetDeclaration(pattern string) *Declaration {
	for _, d := range r.Declarations {
		if d.Identifier == pattern {
			return d
		}
	}
	return nil
}
func (r *Rule) String() string {
	sb := strings.Builder{}
	sb.WriteString(r.OriginalSelector)
	sb.WriteString(" {\n")
	if len(r.Declarations) == 0 {
		sb.WriteString("\tcontent: \"\";\n") // so we do not have an empty rule
	} else {
		var lines []string
		for _, d := range r.Declarations {
			lines = append(lines, d.String())
		}
		sort.Strings(lines)
		seen := make(map[string]bool)
		var exists bool
		for _, l := range lines {
			if _, exists = seen[l]; !exists {
				sb.WriteString(fmt.Sprintf("\t%s\n", l))
				seen[l] = true
			}
		}
	}
	sb.WriteString("}\n")
	return sb.String()
}
func (r *Rule) AddSelector(selector string) {
	r.OriginalSelector = selector
	s := selector
	switch selector[0] {
	case '.':
		r.Type = selectorTypes.Class
	case '#':
		r.Type = selectorTypes.ID
	case '*':
		r.Type = selectorTypes.Universal
	}
	if r.Type == selectorTypes.Unknown {
		if strings.Contains(selector, "[") {
			r.Type = selectorTypes.Attribute
		} else if strings.HasPrefix(selector, "@font-face") {
			r.Type = selectorTypes.FontFace
		} else {
			r.Type = selectorTypes.Type
		}
	}
	if strings.Contains(selector, "::") {
		parts := strings.Split(selector, "::")
		s = parts[0]
		sb := strings.Builder{}
		j := len(parts)
		for i := 1; i < j; i++ {
			sb.WriteString(parts[i])
		}
		r.Pseudo = sb.String()
	} else if strings.Contains(selector, ":") {
		parts := strings.Split(selector, ":")
		s = parts[0]
		sb := strings.Builder{}
		j := len(parts)
		for i := 1; i < j; i++ {
			sb.WriteString(parts[i])
		}
		r.Pseudo = sb.String()
	} else if strings.Contains(selector, ">") {
		parts := strings.Split(selector, ">")
		if len(parts) > 1 {
			s = parts[len(parts)-1]
			r.Combinator = selector
		}
	} else {
		parts := strings.Split(selector, " ")
		if len(parts) > 1 {
			s = parts[len(parts)-1]
			r.Combinator = selector
		}
	}
	r.Selector = s
}
func (r *Rule) AddDeclaration(declaration *Declaration) {
	r.Declarations = append(r.Declarations, declaration)
}
func (r *Rule) AddDeclarations(declarations []*Declaration) {
	r.Declarations = append(r.Declarations, declarations...)
}
func (r *Rule) RemoveDeclarations(identifiers []string) {
	for _, id := range identifiers {
		if r.HasIdentifier(id) {
			r.RemoveDeclaration(id)
		}
	}
}
func (r *Rule) RemoveDeclaration(identifier string) {
	if !r.HasIdentifier(identifier) {
		return
	}
	var newDeclarations []*Declaration
	for _, d := range r.Declarations {
		if d.Identifier == identifier {
			continue
		}
		newDeclarations = append(newDeclarations, d)
	}
	if len(newDeclarations) > 0 {
		r.Declarations = newDeclarations
	}
}

// AddDefaultBodyFontSizeDeclaration adds a default font-size and returns it as a float
// If such a declaration already exists, an error is returned.
// Only use for body-rule.
func (r *Rule) AddDefaultBodyFontSizeDeclaration() (float64, error) {
	if r.HasIdentifier("font-size") {
		return 0, fmt.Errorf("font-size declaration already exists")
	}
	return r.AddFontSizeDeclaration("12pt")
}
func (r *Rule) AddFontSizeDeclaration(value string) (float64, error) {
	if r.HasIdentifier("font-size") {
		return 0, fmt.Errorf("font-size declaration already exists")
	}
	d := new(Declaration)
	d.Identifier = "font-size"
	d.Value = value
	err := d.SetFloatAndUnit()
	if err != nil {
		return 0, fmt.Errorf("error creating default font-size declaration: %v", err)
	} else {
		r.AddDeclaration(d)
	}
	return d.Float, nil
}

// AddLineHeightDeclaration adds the declaration using the rule's font-size
// if exists, otherwise, uses bodyFontSize.
// The height is normalized to mm.
// If such a declaration already exists, an error is returned.
func (r *Rule) AddLineHeightDeclaration(bodyFontSize float64) error {
	if bodyFontSize <= 0 {
		msg := fmt.Sprintf("rule %s fontSize zero not allowed", r.Selector)
		doxlog.Errorf(msg)
		return fmt.Errorf(msg)
	}
	lh := "line-height"
	fs := "font-size"
	if r.HasIdentifier(lh) {
		return fmt.Errorf("%s declaration already exists", lh)
	}
	useFontSize := bodyFontSize // if this rule has font-size, will override it below
	if r.HasIdentifier(fs) {
		fontSizeDeclaration := r.GetDeclaration(fs)
		if fontSizeDeclaration == nil {
			return fmt.Errorf("%s exists yet not found", fs)
		}
		if fontSizeDeclaration.Units != "pt" {
			switch fontSizeDeclaration.Units {
			case "xx-small":
			case "x-small", "smaller":
			case "small":
			case "medium":
			case "large":
			case "x-large", "larger":
			case "xx-large":
			default:
				msg := fmt.Sprintf("rule %s fontSize units invalid: %s, must be pt", r.Selector, fontSizeDeclaration.Units)
				doxlog.Errorf(msg)
				return fmt.Errorf(msg)
			}
		}
		useFontSize = fontSizeDeclaration.Float
	}
	d := new(Declaration)
	d.Identifier = lh
	d.Value = fmt.Sprintf("%fpt", useFontSize*1.2)
	err := d.NormalizeFloatAndUnit(useFontSize)
	if err != nil {
		return fmt.Errorf("error creating default line-height declaration: %v", err)
	}
	r.AddDeclaration(d)
	return nil
}

type Declaration struct {
	Identifier string
	Value      string
	Float      float64
	Units      string
	Important  bool
	Normalized bool
}

func (d *Declaration) NormalizeFloatAndUnit(baseFontSize float64) error {
	exclusions := []string{"text-indent"}
	for _, e := range exclusions {
		if d.Identifier == e {
			return nil
		}
	}
	if d.Units == "" {
		err := d.SetFloatAndUnit()
		if err != nil {
			msg := fmt.Sprintf("%s units not set, so called SetFloatAndUnit, but resulted in error: %v", d.Identifier, err)
			doxlog.Errorf(msg)
			return fmt.Errorf(msg)
		}
	}
	unit := "mm"
	switch d.Units {
	case "%":
		if d.Identifier == "font-size" {
			d.Float = baseFontSize * (d.Float / 100)
			d.Units = "pt"
		} else {
			d.Float = fonts.PointsToMillimeters(baseFontSize * (d.Float / 100))
			d.Units = unit
		}
	case "cm":
		d.Float = d.Float * 10
		d.Units = unit
	case "in":
		d.Float = fonts.PointsToMillimeters(d.Float * 72)
		d.Units = unit
	case "mm": // no op
	case "pt", "px":
		if d.Identifier != "font-size" {
			d.Float = fonts.PointsToMillimeters(d.Float)
			d.Units = unit
		}
	case "rem":
		if d.Identifier == "font-size" {
			d.Float = baseFontSize * d.Float
			d.Units = "pt"
		} else {
			d.Float = fonts.PointsToMillimeters(baseFontSize * d.Float)
			d.Units = unit
		}
	default:
		size, err := fonts.RelativePoints(baseFontSize, strings.ToLower(d.Value))
		if err != nil {
			return err
		} else {
			if d.Identifier == "line-height" {
				d.Float = fonts.PointsToMillimeters(size * 1.2)
				d.Units = unit
			} else {
				if d.Value == "inherit" {
					d.Float = size
					d.Units = "pt"
				}
				doxlog.Warnf("could not normalize %s %s", d.Identifier, d.Value)
				return nil
			}
		}
	}
	d.Value = fmt.Sprintf("%f%s", d.Float, d.Units)
	d.Normalized = true
	return nil
}

// SetFloatAndUnit parses the Value string into its float64 value and units
func (d *Declaration) SetFloatAndUnit() error {
	if strings.HasSuffix(d.Value, "em") {
		if !strings.HasSuffix(d.Value, "rem") {
			d.Value = fmt.Sprintf("%srem", strings.TrimSuffix(d.Value, "em"))
		}
	}
	if strings.HasSuffix(d.Value, "mm") {
		size, err := strconv.ParseFloat(d.Value[:len(d.Value)-2], 64)
		if err != nil {
			return err
		}
		d.Float = size
		d.Units = "mm"
	} else if strings.HasSuffix(d.Value, "cm") {
		size, err := strconv.ParseFloat(d.Value[:len(d.Value)-2], 64)
		if err != nil {
			return err
		}
		d.Float = size
		d.Units = "cm"
	} else if strings.HasSuffix(d.Value, "pt") {
		size, err := strconv.ParseFloat(d.Value[0:len(d.Value)-2], 64)
		if err != nil {
			return err
		}
		d.Float = size
		d.Units = "pt"
	} else if strings.HasSuffix(d.Value, "rem") {
		size, err := strconv.ParseFloat(d.Value[0:len(d.Value)-3], 64)
		if err != nil {
			return err
		}
		d.Float = size
		d.Units = "rem"
	} else if strings.HasSuffix(d.Value, "%") {
		size, err := strconv.ParseFloat(d.Value[0:len(d.Value)-1], 64)
		if err != nil {
			return err
		}
		d.Float = size
		d.Units = "%"
	} else if strings.HasSuffix(d.Value, "in") {
		size, err := strconv.ParseFloat(d.Value[0:len(d.Value)-2], 64)
		if err != nil {
			return err
		}
		d.Float = size
		d.Units = "in"
	} else if strings.HasSuffix(d.Value, "px") {
		size, err := strconv.ParseFloat(d.Value[0:len(d.Value)-2], 64)
		if err != nil {
			return err
		}
		d.Float = size
		d.Units = "pt"
	}
	return nil
}

func (d *Declaration) NormalizeFontFamily() {
	fnts := strings.Split(d.Value, ",")
	if len(fnts) > 0 {
		d.Value = strings.Trim(fnts[0], "\"")
		d.Normalized = true
	}
}

func (d *Declaration) Copy() *Declaration {
	n := new(Declaration)
	n.Identifier = d.Identifier
	n.Value = d.Value
	n.Important = d.Important
	return n
}
func (d *Declaration) String() string {
	sb := strings.Builder{}
	sb.WriteString(fmt.Sprintf("%s: %s", d.Identifier, d.Value))
	if d.Important {
		sb.WriteString(" !important")
	}
	sb.WriteString(";")
	return sb.String()
}
func (d *Declaration) AddValue(value string) {
	parts := strings.Split(value, "!important")
	switch len(parts) {
	case 2:
		{
			d.Value = parts[0]
			d.Important = true
		}
	default:
		d.Value = value
	}
}

func decomment(line string, inComment bool) (string, bool) {
	var result strings.Builder
	for len(line) > 0 {
		if inComment {
			endIndex := strings.Index(line, "*/")
			if endIndex == -1 {
				return result.String(), true // Still in comment
			}
			line = line[endIndex+2:] // Skip the comment end
			inComment = false
		} else {
			startIndex := strings.Index(line, "/*")
			if startIndex == -1 {
				result.WriteString(line)
				break
			}
			result.WriteString(line[:startIndex])
			line = line[startIndex+2:] // Skip the comment start
			inComment = true
		}
	}
	return result.String(), inComment
}

func (s *StyleSheet) GetRuleLines(input string) [][]string {
	var lines []string
	tmpLines := strings.Split(input, "\n")
	for _, l := range tmpLines {
		if strings.Contains(l, "{") &&
			strings.Contains(l, "}") {
			l = strings.ReplaceAll(l, "{", "{\n")
			l = strings.ReplaceAll(l, ";", ";\n")
			l = strings.ReplaceAll(l, "}", "\n}\n")
			newLines := strings.Split(l, "\n")
			for _, nl := range newLines {
				lines = append(lines, nl)
			}
		} else {
			lines = append(lines, l)
		}
	}
	var inCommentBlock bool
	var buildingRule bool
	var rules [][]string
	var rule []string
	for _, line := range lines {
		line = strings.TrimSpace(line)
		line, inCommentBlock = decomment(line, inCommentBlock)
		if strings.HasPrefix(line, "/*") && strings.HasSuffix(line, "*/") {
			rule = append(rule, line)
			continue
		}
		if len(line) == 0 {
			continue
		}
		if inCommentBlock {
			continue
		}
		if strings.Contains(line, "{") {
			buildingRule = true
		}
		if line == "}" {
			buildingRule = false
			rule = append(rule, fmt.Sprintf("%s\n", line))
			rules = append(rules, rule)
			rule = []string{}
			continue
		}
		if buildingRule {
			rule = append(rule, fmt.Sprintf("%s\n", line))
		}
	}
	return rules
}

// removeComment if it is inline, i.e. /* this is a comment */
func removeComment(l string) string {
	if len(l) == 0 {
		return l
	}
	start := strings.Index(l, "/*")
	if start > -1 {
		end := strings.Index(l, "*/")
		if end > start+2 {
			return strings.TrimSpace(l[:start])
		}
	}
	return l
}

func SortCss(pathIn, pathOut string) error {
	fontFaceMap := make(map[string]*RuleMeta)
	ruleMap := make(map[string][]string)
	lines, err := ltfile.GetFileLines(pathIn)
	if err != nil {
		return err
	}
	var inFontFace, inRule bool
	var rule string
	var attributes []string
	var fontRule *RuleMeta
	sb := strings.Builder{}
	for _, l := range lines {
		t := strings.TrimSpace(l)
		if strings.HasPrefix(t, "/") {
			continue
		}
		if strings.HasSuffix(t, "{") { // start of rule
			if strings.HasPrefix(t, "@font-face") {
				inFontFace = true
				inRule = false
				fontRule = new(RuleMeta)
			} else {
				inFontFace = false
				inRule = true
			}
			rule = t
			attributes = []string{}
		} else if t == "}" { // end of rule
			if inFontFace {
				fontFaceMap[fontRule.FontFamily] = fontRule
				inFontFace = false
			} else {
				inRule = false
				ruleMap[rule] = attributes
			}
			sort.Strings(attributes)
		} else if inFontFace {
			/*
				@font-face {
				    font-family: "Arimo";
				    src:
				            url('fonts/Arimo-Bold.ttf') format('truetype'),
				            url('fonts/Arimo-BoldItalic.ttf') format('truetype'),
				            url('fonts/Arimo-Italic.ttf') format('truetype'),
				            url('fonts/Arimo-Regular.ttf') format('truetype');
				}

				@font-face {
				    font-family: 'EZ Psaltica';
				    src: url('fonts/EZ-Psaltica.ttf') format('truetype');
				    font-weight: normal;
				    font-style: normal;
				}

			*/
			if strings.HasPrefix(t, "font-family") {
				fontRule.FontFamily = t
			} else if strings.HasPrefix(t, "src:") {
				if strings.Contains(t, "url") {
					attributes = append(attributes, t) // just treat it like an attribute
				}
			} else if strings.HasPrefix(t, "url") {
				fontRule.UrlSources = append(fontRule.UrlSources, t)
			} else {
				fontRule.Attributes = append(attributes, t)
			}
		} else if inRule {
			attributes = append(attributes, t)
		}
	}
	sb.WriteString(writeFonts(fontFaceMap))
	sb.WriteString(writeRules(ruleMap))
	return ltfile.WriteFile(pathOut, sb.String())
}
func writeFonts(m map[string]*RuleMeta) string {
	sb := strings.Builder{}
	var keys []string
	for k, _ := range m {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	for _, k := range keys {
		v, _ := m[k]
		sb.WriteString("@font-face {\n")
		sb.WriteString(fmt.Sprintf("\t%s\n", v.FontFamily))
		if len(v.UrlSources) > 0 {
			sb.WriteString("\tsrc:\n")
			for _, a := range v.UrlSources {
				sb.WriteString(fmt.Sprintf("\t\t%s\n", a))
			}
		}
		for _, a := range v.Attributes {
			sb.WriteString(fmt.Sprintf("\t%s\n", a))
		}
		sb.WriteString("}\n")
	}
	return sb.String()
}
func writeRules(m map[string][]string) string {
	sb := strings.Builder{}
	var keys []string
	for k, _ := range m {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	for _, k := range keys {
		v, _ := m[k]
		sb.WriteString(fmt.Sprintf("%s\n", k))
		if v == nil || len(v) == 0 {
			sb.WriteString("\tcontent: \"\";\n")
		}
		for _, a := range v {
			sb.WriteString(fmt.Sprintf("\t%s\n", a))
		}
		sb.WriteString("}\n")
	}
	return sb.String()
}

type RuleMeta struct {
	Attributes []string
	FontFamily string
	UrlSources []string
}

func expandToFourSides(d *Declaration) (theFour []*Declaration, err error) {
	switch d.Identifier {
	case "border", "margin", "padding":
		sideMap := parseFour(d.Value)
		if sideMap == nil {
			return nil, fmt.Errorf("could not parse %s into sides", d.Identifier)
		}
		sides := []string{"top", "bottom", "left", "right"}
		for _, side := range sides {
			sd := new(Declaration)
			sd.Identifier = fmt.Sprintf("%s-%s", d.Identifier, side)
			sd.Value = sideMap[side]
			err = sd.SetFloatAndUnit()
			if err != nil {
				return nil, fmt.Errorf("%s: %v", d.Identifier, err)
			}
			theFour = append(theFour, sd)
		}
	default:
		return nil, fmt.Errorf("%s not used to specify top, bottom, left, right", d.Identifier)
	}
	return theFour, nil
}

// parseFour takes a single string input representing a margin, padding, or border property value(s)
// and returns a map with keys for each side (top, right, bottom, left)
// and their corresponding values, e.g. marginProperties["top"]
func parseFour(margin string) map[string]string {
	values := strings.Fields(margin)
	sidesMap := map[string]string{
		"top":    "0",
		"right":  "0",
		"bottom": "0",
		"left":   "0",
	}

	switch len(values) {
	case 1:
		// All sides are the same
		sidesMap["top"], sidesMap["right"], sidesMap["bottom"], sidesMap["left"] = values[0], values[0], values[0], values[0]
	case 2:
		// First value for top and bottom, second value for left and right
		sidesMap["top"], sidesMap["bottom"] = values[0], values[0]
		sidesMap["left"], sidesMap["right"] = values[1], values[1]
	case 3:
		// First value for top, second for left and right, third for bottom
		sidesMap["top"] = values[0]
		sidesMap["left"], sidesMap["right"] = values[1], values[1]
		sidesMap["bottom"] = values[2]
	case 4:
		// Each value corresponds to a side, in the order: top, right, bottom, left
		sidesMap["top"], sidesMap["right"], sidesMap["bottom"], sidesMap["left"] = values[0], values[1], values[2], values[3]
	}

	return sidesMap
}
