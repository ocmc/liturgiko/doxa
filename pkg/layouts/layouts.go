package layouts

import (
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/atempl"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/enums/properties"
	"sort"
	"strings"
)

type LayoutManager struct {
	sm *config.SettingsManager
}

var Manager *LayoutManager

func Init() {
	Manager = new(LayoutManager)
	Manager.sm = config.SM
}
func (s *LayoutManager) AddSiteBuildLayoutsAcronym(acronym string) error {
	acronyms, err := s.GetSiteBuildLayoutsAcronyms()
	if err != nil {
		return fmt.Errorf("settingsManager.AddSiteBuildLayoutsAcronym error: %v", err)
	}
	for _, a := range acronyms {
		if a == acronym {
			return nil // already exists, so ignore and return
		}
	}
	// did not exist, so save it
	acronyms = append(acronyms, acronym)
	sort.Strings(acronyms)
	kvp := kvs.NewDbR()
	kvp.KP.ParsePath(s.sm.ConfigPath + properties.SiteBuildLayoutsRows.Data().Path)
	kvp.KP.KeyParts.Push("value")
	kvp.Value = strings.Join(acronyms[:], ",")
	return s.sm.Kvs.Db.Put(kvp)
}
func invalidAcronyms(a string) bool {
	if len(a) == 0 {
		return true
	}
	if !strings.Contains(a, ",") {
		return true
	}
	return false
}
func (s *LayoutManager) GetSiteBuildLayoutsAcronyms() ([]string, error) {
	var acronyms []string
	layouts := s.sm.StringProps[properties.SiteBuildLayoutsRows]
	var err error
	if invalidAcronyms(layouts) { // set a value and save to database
		selectedLayouts := s.sm.StringProps[properties.SiteBuildLayoutsSelected]
		if invalidAcronyms(selectedLayouts) {
			// we will use the default set the available rows
			layouts = properties.SiteBuildLayoutsRows.Data().DefaultValue
		} else {
			// we will use the selected rows to set the available rows
			layouts = selectedLayouts
		}
		kvp := kvs.NewDbR()
		kvp.KP.ParsePath(s.sm.ConfigPath + properties.SiteBuildLayoutsRows.Data().Path)
		kvp.KP.KeyParts.Push("value")
		kvp.Value = layouts
		err = s.sm.Kvs.Db.Put(kvp)
		if err != nil {
			fmt.Printf("pkg/configs/config.go SettingsManager.GetSiteBuildLayoutsAcronyms error saving missing layout acronyms %s: %v", kvp.Value, err)
		}
	}
	// continue processing the request for acronyms
	parts := strings.Split(s.sm.StringProps[properties.SiteBuildLayoutsRows], ",")
	sort.Strings(parts)
	for _, p := range parts {
		acronyms = append(acronyms, strings.TrimSpace(p))
	}
	sort.Strings(acronyms)
	return acronyms, nil
}
func (s *LayoutManager) GetSiteBuildColumnMap() (map[string]*atempl.TableLayout, error) {
	columnMap := make(map[string]*atempl.TableLayout)
	matcher := kvs.NewMatcher()
	err := matcher.KP.ParsePath(s.sm.ConfigPath + properties.SiteBuildLayoutsColumns.Data().Path + "/map")
	if err != nil {
		msg := fmt.Sprintf("error parsing path from default value for properties.SiteBuildLayoutsColumns: %v", err)
		doxlog.Errorf(msg)
		return nil, err
	}
	recs, _, err := s.sm.Kvs.Db.GetMatching(*matcher)
	if err != nil {
		msg := fmt.Sprintf("error getting matching records: %v", err)
		doxlog.Errorf(msg)
		return nil, fmt.Errorf(msg)
	}
	if len(recs) == 0 { // we need to initialize the values
		columnLayoutsMap, err := properties.SiteBuildLayoutsColumns.PropertyMap()
		if err != nil {
			msg := fmt.Sprintf(fmt.Sprintf("config.SettingsManager.GetSiteBuildColumnMap error: %v", err))
			doxlog.Errorf(msg)
			return nil, fmt.Errorf(msg)
		}
		for _, v := range columnLayoutsMap {
			l, err := atempl.NewTableLayoutFromJson(v)
			if err != nil {
				return nil, err
			}
			columnMap[l.Acronym] = l
			err = s.SetSiteBuildLayout(l)
			if err != nil {
				msg := fmt.Sprintf(fmt.Sprintf("error: %v", err))
				doxlog.Errorf(msg)
				return nil, fmt.Errorf(msg)
			}
		}
	} else {
		for _, rec := range recs {
			l, err := atempl.NewTableLayoutFromJson(rec.Value)
			if err != nil {
				msg := fmt.Sprintf(fmt.Sprintf("error: %v", err))
				doxlog.Errorf(msg)
				return nil, fmt.Errorf(msg)
			}
			columnMap[l.Acronym] = l
		}
	}
	return columnMap, nil
}
func (s *LayoutManager) GetSiteBuildLayouts() ([]*atempl.TableLayout, error) {
	var acronyms []string
	var err error
	var layouts []*atempl.TableLayout

	acronyms, err = s.GetSiteBuildLayoutsAcronyms()
	if err != nil {
		return nil, err
	}
	for _, combo := range acronyms {
		var tableLayout *atempl.TableLayout
		tableLayout, err = s.GetSiteBuildLayout(combo)
		if err != nil {
			return nil, err
		}
		layouts = append(layouts, tableLayout)
	}
	return layouts, nil
}

// GetSiteBuildLayout provides the layout for a specific combination of language codes.
// A layout specifies the number of columns for a table and the versions to use to populate cells
// in each column of the table.
func (s *LayoutManager) GetSiteBuildLayout(acronymCombo string) (*atempl.TableLayout, error) {
	columnMap, err := s.GetSiteBuildColumnMap()
	if err != nil {
		doxlog.Errorf(err.Error())
		return nil, err
	}
	tableLayout := new(atempl.TableLayout)
	tableLayout.Acronym = acronymCombo
	parts := strings.Split(acronymCombo, "-")
	for _, p := range parts {
		var langLayout *atempl.TableLayout
		var found bool
		if langLayout, found = columnMap[p]; !found {
			msg := fmt.Sprintf("could not find layout for %s", p)
			doxlog.Errorf(msg)
			return nil, fmt.Errorf(msg)
		}
		tableLayout.LiturgicalLibs = append(tableLayout.LiturgicalLibs, langLayout.LiturgicalLibs[0])
		tableLayout.EpistleLibs = append(tableLayout.EpistleLibs, langLayout.EpistleLibs[0])
		tableLayout.GospelLibs = append(tableLayout.GospelLibs, langLayout.GospelLibs[0])
		tableLayout.ProphetLibs = append(tableLayout.ProphetLibs, langLayout.ProphetLibs[0])
		tableLayout.PsalterLibs = append(tableLayout.PsalterLibs, langLayout.PsalterLibs[0])
		tableLayout.DateFormat = langLayout.DateFormat
		tableLayout.WeekDayFormat = langLayout.WeekDayFormat
	}
	return tableLayout, nil
}
func (s *LayoutManager) CreateSiteBuildTableLayout(layout *atempl.TableLayout) (string, error) {
	return "", nil
}
func (s *LayoutManager) DeleteSiteBuildLayout(acronym string) error {
	// remove from rows
	// for any lang code not used in rows, delete it from columns
	acronyms, err := s.GetSiteBuildLayoutsAcronyms()
	if err != nil {
		return fmt.Errorf("settingsMangager.DeleteSiteBuildLayout error: %v", err)
	}
	var newAcronyms []string
	for _, a := range acronyms {
		if a != acronym {
			newAcronyms = append(newAcronyms, a)
		}
	}
	sort.Strings(newAcronyms)
	kvp := kvs.NewDbR()
	kvp.KP.ParsePath(s.sm.ConfigPath + properties.SiteBuildLayoutsRows.Data().Path)
	kvp.KP.KeyParts.Push("value")
	kvp.Value = strings.Join(newAcronyms[:], ",")
	return s.sm.Kvs.Db.Put(kvp)
}
func (s *LayoutManager) SetSiteBuildLayout(layout *atempl.TableLayout) error {
	var err error
	if layout.DateFormat == "" {
		switch layout.Acronym {
		case "en":
			layout.DateFormat = "%A, %B %d, %Y"
		case "el", "gr":
			layout.DateFormat = "%A, %d %B %Y"
		case "es":
			layout.DateFormat = "%A, %d de %B de %Y"
		default:
			layout.DateFormat = "%d-%m-%Y"
		}
	}
	if layout.WeekDayFormat == "" {
		layout.WeekDayFormat = "%A"
	}

	dbr := kvs.NewDbR()
	dbr.KP.ParsePath(s.sm.ConfigPath + properties.SiteBuildLayoutsColumns.Data().Path + "/map")
	dbr.KP.KeyParts.Push(layout.Acronym)
	dbr.Value, err = layout.ToJson()
	if err != nil {
		return err
	}
	return s.sm.Kvs.Db.Put(dbr)
}
