package syncsrvc

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/catalog"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/css"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/dvcs"
	"github.com/liturgiko/doxa/pkg/enums/gitStatuses"
	"github.com/liturgiko/doxa/pkg/inOut"
	"github.com/liturgiko/doxa/pkg/kvsw"
	"github.com/liturgiko/doxa/pkg/sysManager"
	ltUrl "github.com/liturgiko/doxa/pkg/utils"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/stamp"
	"net/url"
	"os"
	"path"
	"runtime"
	"sort"
	"strings"
	"sync"
	"time"
)

const cancelDebug = true

func traceMsg(str string) {
	if cancelDebug {
		fmt.Println(str)
	}
}

type ReadOnlySyncer struct {
	cancelAutoSync       *context.CancelFunc
	ctx                  *context.Context // initiated for auto sync, used to cancel it
	helpers              *SyncerHelpers
	rmm                  *RemoteMetaManager
	LastSync             time.Time
	LastSyncDuration     time.Duration
	OffLineWaitDuration  time.Duration
	OnlineWaitDuration   time.Duration
	Name                 string
	AutoOn               bool
	syncAllRunning       bool // true if SyncAll called when AutoOn != true && syncRunning != true
	syncRunning          bool // true if Sync called when AutoOn != true && syncAllRunning != true
	autoMux              sync.Mutex
	Debug                bool
	catMan               *catalog.Manager
	NumSyncsSinceStartup int
	InSync               bool //should we shorten to nsync?
	WaitingLock          bool
	msgChan              chan string
	// fileDownloadMap uses RepoName as the key.  The values are files to download.
	// This is used when the repo is not to be cloned.
	// Instead, individual files are downloaded.
	fileDownloadMap  map[string][]*MetaDownloadFileInfo
	downloadFiles    []*DownloadFileInfo
	cloneToFsRepos   []*RemoteMeta
	cloneToDbRepos   []*RemoteMeta
	cloneToParentDir string
	Cancelled        bool
	cancelMux        sync.Mutex
}
type ProcessList struct {
	CloneToFs    []*RemoteMeta
	CloneToDb    []*RemoteMeta
	DownloadToFs []*RemoteMeta
}

const oldVersionPrefix = "-old"

func NewReadOnlySyncer(helpers *SyncerHelpers, autoOn bool) *ReadOnlySyncer {
	var err error
	s := new(ReadOnlySyncer)
	s.helpers = helpers
	s.rmm = new(RemoteMetaManager)
	s.OffLineWaitDuration = ReadOnlyOfflineWaitDuration
	s.OnlineWaitDuration = ReadOnlyOnlineWaitDuration
	s.Name = ReadyOnlyName
	gitlabApi, err := url.Parse(s.helpers.GitlabApi)
	if err != nil {
		doxlog.Errorf("error parsing gitlab: %v", err)
		return nil
	}
	s.catMan, err = catalog.NewManager(s.helpers.sysDs, s.helpers.Bus, s.helpers.dvcs, gitlabApi.Host)
	if err != nil {
		doxlog.Errorf("could not access catalog: %v", err)
		return nil
	}
	if autoOn {
		err = s.SyncAutoOn(time.Minute * 1)
		if err != nil {
			doxlog.Warnf("auto %s  err: %v", s.Name, err)
		}
	}

	if s.helpers.SysManager != nil {
		if val, exists := s.helpers.SysManager.LoadCurrentProjectSettingByKey("ro", "lastSync"); exists {
			s.LastSync, err = time.Parse(time.RFC3339, val)
		}
	}
	go s.LoadActionList() //preliminary for ui
	return s
}

// assetsDownloadFiles is a slice of MetaDownloadFileInfo objects,
// each specifying a file to download
// with its source URL suffix and target path suffix.
// This allows a standardized place to indicate which
// files are to be copied from a repository
// in the scenario that the entire repo is not needed.
// These files will be placed in the subscriptions' directory.
// They can be used to view the files in use by the producer of
// the catalog subscribed to.
var assetsDownloadFiles = []*MetaDownloadFileInfo{
	&MetaDownloadFileInfo{
		UrlSuffix:    "-/raw/main/css/app.css",
		FsPathSuffix: "css/app.css",
	},
	&MetaDownloadFileInfo{
		UrlSuffix:    "-/raw/main/css/doxa.css",
		FsPathSuffix: "css/doxa.css",
	},
	&MetaDownloadFileInfo{
		UrlSuffix:    "-/raw/main/js/app.js",
		FsPathSuffix: "js/app.js",
	},
	&MetaDownloadFileInfo{
		UrlSuffix:    "-/raw/main/js/doxa.js",
		FsPathSuffix: "js/doxa.js",
	},
}

// MetaDownloadFileInfo provides the suffixes for the source url and target filesystem path.
// To get the full URL, call the ToDownloadUrl() method
// To get the full file system path, call ToFsPath()
// Note if you call Download(), you do not have to also call ToDownloadUrl() or ToFsPath()
type MetaDownloadFileInfo struct {
	UrlSuffix    string
	FsPathSuffix string
}

func (fd MetaDownloadFileInfo) ToDownloadFileInfo(metaUrl, metaFsPathPrefix string) *DownloadFileInfo {
	d := new(DownloadFileInfo)
	d.Url = fd.ToDownloadUrl(metaUrl)
	d.FsPath = fd.ToFsPath(metaFsPathPrefix)
	return d
}

// ToDownloadUrl constructs the full download URL using the provided metaRepoName
// and the UrlSuffix.
func (fd MetaDownloadFileInfo) ToDownloadUrl(metaUrl string) string {
	return fmt.Sprintf("https://%s", path.Join(metaUrl, fd.UrlSuffix))
}

// ToFsPath constructs a full filesystem path by joining the given prefix
// with the MetaDownloadFileInfo's filesystem path suffix.
func (fd MetaDownloadFileInfo) ToFsPath(metaFsPathPrefix string) string {
	return path.Join(metaFsPathPrefix, fd.FsPathSuffix)
}

// DownloadFileInfo contains the full URL and FsPath for a download.
// It is created by binding subscription information to a MetaDownLoadFileInfo instance.
type DownloadFileInfo struct {
	Url    string
	FsPath string
}

// Download reads the content from the remote file and writes it the file system.
func (fd DownloadFileInfo) Download() error {
	return ltfile.WriteContentsFromUrlToFile(fd.Url, fd.FsPath)
}
func (s *ReadOnlySyncer) ResetActionLists() {
	s.fileDownloadMap = make(map[string][]*MetaDownloadFileInfo)
	s.fileDownloadMap["assets"] = assetsDownloadFiles
	s.downloadFiles = []*DownloadFileInfo{}
	s.cloneToDbRepos = []*RemoteMeta{}
	s.cloneToFsRepos = []*RemoteMeta{}
	s.cloneToParentDir = ""
}

// LoadActionList clears the action lists and populates them using information for a subscription.
// Repositories are categories as follows:
// 1) Ones from which only specified files are to be downloaded.
// 2) Ones that are to be cloned to the filesystem but not loaded into the database, e.g. templates.
// 3) Ones that are to be cloned, then loaded into the database.
// After loading the list, call ProcessActionList().
func (s *ReadOnlySyncer) LoadActionList() (err error) {
	s.ResetActionLists() // s.fileDownloadMap, s.cloneToDbRepos, s.cloneToFsRepos.
	//get the list from the DB
	subs := s.catMan.Subscriptions()
	if subs == nil {
		return fmt.Errorf("no subscriptions")
	}
	//populate the remote meta map
	for _, ent := range subs {
		if ent == nil || ent.Name == "" || ent.Url == "" {
			continue
		}
		meta := new(RemoteMeta)
		meta.Url = ent.Url
		if strings.HasSuffix(meta.Url, ".git") {
			meta.Url = strings.TrimSuffix(meta.Url, ".git")
		}
		myUrl, err := url.Parse(meta.Url)
		if err != nil {
			return fmt.Errorf("error parsing URL %s: %v", meta.Url, err)
		}
		meta.RepoName = path.Base(myUrl.Path)
		meta.UrlSubGroupPath = myUrl.Path
		if strings.HasPrefix(meta.Url, "https://") {
			meta.Url = strings.TrimPrefix(meta.Url, "https://")
		}
		pathParts, err := ltUrl.GetURLPathParts(myUrl.Path)
		if err != nil {
			return fmt.Errorf("error parsing URL %s: %v", meta.Url, err)
		}
		meta.GroupName = pathParts[0]
		if len(pathParts) == 2 {
			meta.UrlSubGroupPath = pathParts[1]
		} else {
			meta.UrlSubGroupPath = path.Join(pathParts[1:(len(pathParts))]...)
		}
		meta.FsClonePath = path.Join(s.helpers.paths.SubscriptionsPath, meta.GroupName)
		meta.FsPath = path.Join(meta.FsClonePath, meta.UrlSubGroupPath)
		if strings.HasPrefix(ent.Path, "resources/") {
			meta.DbPath = strings.TrimPrefix(ent.Path, "resources/")
		}
		switch meta.RepoName {
		case "assets":
			meta.ActionType = DownloadToFs
			if downloadFiles, exists := s.fileDownloadMap[meta.RepoName]; exists {
				for _, fileMetaInfo := range downloadFiles {
					fileInfo := fileMetaInfo.ToDownloadFileInfo(
						meta.Url,
						meta.FsPath)
					meta.Downloads = append(meta.Downloads, fileInfo)
					s.downloadFiles = append(s.downloadFiles, fileInfo)
				}
			}
		case "configs", "templates":
			meta.ActionType = CloneToFs
			s.cloneToFsRepos = append(s.cloneToFsRepos, meta)
		default:
			if strings.HasSuffix(meta.RepoName, ":templates") {
				meta.RepoName = config.TemplatesDir
				meta.ActionType = CloneToFs
				s.cloneToFsRepos = append(s.cloneToFsRepos, meta)
			}
			meta.ActionType = CloneToDb
			s.cloneToDbRepos = append(s.cloneToDbRepos, meta)
		}
		s.rmm.putToMap(meta)
	}
	if s.cloneToParentDir == "" {
		if len(s.cloneToFsRepos) > 0 || len(s.cloneToDbRepos) > 0 {
			if s.cloneToFsRepos == nil || len(s.cloneToFsRepos) == 0 || s.cloneToFsRepos[0] == nil {
				return
			}
			s.cloneToParentDir = s.cloneToFsRepos[0].FsClonePath
		}
	}
	return
}

func (s *ReadOnlySyncer) AutoSyncOn() bool {
	return s.AutoOn
}
func (s *ReadOnlySyncer) SetDebug(to bool) {
	s.Debug = to
}

func (s *ReadOnlySyncer) Status() string {
	justWarns, errs := s.rmm.numRemotesWithIssues()
	clean := true
	part1 := "No errors detected."
	if justWarns > 0 || errs > 0 {
		clean = false
		part1 = fmt.Sprintf("There are %d warnings", justWarns)
		if errs > 0 {
			part1 += fmt.Sprintf(" and %d errs", errs)
		}
	}
	freq := s.OffLineWaitDuration
	if s.helpers.InternetAvailable() {
		freq = s.OnlineWaitDuration
	} else {
		if s.InSync {
			if clean {
				part1 = ""
				clean = false
			}
			part1 += "offline. Update will be delayed until internet is available"
		}
	}
	inSyncStr := "Waiting to update"
	if s.InSync {
		inSyncStr = "update in progress"
		if s.WaitingLock {
			inSyncStr += ", waiting on lock"
		}
		inSyncStr += fmt.Sprintf(" (started at %s)", s.LastSync.Format("15:04MST"))
	}
	part2 := fmt.Sprintf("%s\nNumber of updates since startup: %d, Update frequency: %f minutes", inSyncStr, s.NumSyncsSinceStartup, freq.Minutes())
	if !s.InSync && s.LastSyncDuration > 0 {
		part2 += fmt.Sprintf("\nlast sync was at %s and took %s to finish", s.LastSync.Format("15:04MST"), s.LastSyncDuration.String())
	} else {
		part2 += "no downloads since startup"
	}
	if !s.AutoOn {
		part2 = "manual " + inSyncStr
		if !s.InSync {
			part2 = "You have not opted into automatic updates. manually update with `ro update`\n" +
				"you can opt-in to automatic updates by running `ro config auto true`\n"
			if s.LastSyncDuration > 0 {
				part2 += fmt.Sprintf("\nlast sync was at %s and took %s to finish", s.LastSync.Format("15:04MST"), s.LastSyncDuration.String())
			} else {
				part2 += "no downloads since startup"
			}
		}
	}
	return strings.Join([]string{part1, part2}, "\n")
}

func (s *ReadOnlySyncer) processFileDownloads() error {
	var wg sync.WaitGroup
	errCh := make(chan error, len(s.downloadFiles))

	for _, meta := range s.downloadFiles {
		wg.Add(1)
		if s.Cancelled {
			//traceMsg("processFileDownloads has been cancelled")
			return nil
		}
		select {
		case <-(*s.ctx).Done():
			//traceMsg("cancelling in processFileDownloads")
			s.attemptCancel()
			return nil
		default:
		}
		go func(meta *DownloadFileInfo) {
			defer wg.Done()
			err := ltfile.WriteContentsFromUrlToFile(meta.Url, meta.FsPath)
			if err != nil {
				errCh <- err
			}
		}(meta)
	}

	wg.Wait()
	close(errCh)

	if len(errCh) > 0 {
		doxlog.Warnf("one or more file downloads failed")
	}
	return nil
}
func (s *ReadOnlySyncer) processCloneToFsList() map[string]error {
	var cloneUrlList []string
	for _, meta := range s.cloneToFsRepos {
		cloneUrlList = append(cloneUrlList, meta.ToCloneUrl())
	}
	return s.helpers.dvcs.Git.CloneConcurrently(*s.ctx, s.cloneToParentDir, cloneUrlList, 1, false, true)
}
func (s *ReadOnlySyncer) processCloneToDbList() map[string]error {
	batchSize := 15000 // has been tested using 50k. Each increase decreases the write time. But requires more user memory.
	var cloneUrlList []string
	for _, meta := range s.cloneToDbRepos {
		cloneUrlList = append(cloneUrlList, meta.ToCloneUrl())
	}

	depth := 1
	fileExtension := "tsv"
	outputChan := make(chan string)
	errorChan := make(chan dvcs.ProcessError)
	messageChan := make(chan dvcs.ProcessMessage)

	// Create channels for BatchStream
	dbrChan := make(chan *kvs.DbR)
	batchErrChan := make(chan error)

	// Create a timeout context
	ctxWithTimeout, cancelTimeout := context.WithTimeout(*s.ctx, 30*time.Minute)
	defer cancelTimeout()

	doxlog.Infof("Starting concurrent cloning of repositories")

	// Start BatchStream in a separate goroutine
	go s.helpers.ds.BatchStream(*s.ctx, batchSize, dbrChan, batchErrChan) // Adjust batch size as needed

	done := make(chan struct{})
	go func() {
		s.helpers.dvcs.Git.CloneAndProcessRepos(
			ctxWithTimeout,
			cloneUrlList,
			depth,
			fileExtension,
			outputChan,
			errorChan,
			messageChan,
		)
		close(done)
	}()

	lineCount := 0
	errorMap := make(map[string]error)
	for {
		if s.Cancelled {
			//traceMsg("processCloneToDbList has been cancelled")
			return errorMap
		}
		select {
		case <-(*s.ctx).Done():
			//traceMsg("cancelling in processCloneToDbList")
			s.attemptCancel()
			return errorMap
		case msg, ok := <-messageChan:
			if !ok {
				messageChan = nil
			} else {
				doxlog.Infof("Message: %s - %s", msg.RepoURL, msg.Message)
			}
		case err, ok := <-errorChan:
			if !ok {
				errorChan = nil
			} else {
				doxlog.Errorf("Error in %s: %v", err.RepoURL, err.Error)
				// Find the corresponding DbPath for the repo URL
				for _, meta := range s.cloneToDbRepos {
					if meta.Url == err.RepoURL {
						errorMap[meta.DbPath] = err.Error
						break
					}
				}
			}
		case line, ok := <-outputChan:
			if !ok {
				outputChan = nil
			} else {
				lineCount++
				dbr, err := s.tsv2dbr(line)
				if err != nil {
					doxlog.Errorf("Error processing line: %v", err)
				} else {
					go func(kp *kvs.KeyPath) {
						if !s.helpers.keyring.Exists(kp.LmlTopicKey()) {
							err := s.helpers.keyring.Add(kp)
							if err != nil {
								doxlog.Errorf("error adding topic-key to keypath %s: %v", kp.Path(), err)
							}
						}
					}(dbr.KP)

					dbrChan <- dbr // send kvsw.BatchStream to write to db in batches
				}
			}
		case err := <-batchErrChan:
			if err != nil {
				doxlog.Errorf("Error in BatchStream: %v", err)
			}
		case <-done:
			close(dbrChan) // Signal BatchStream that no more objects will be sent
			doxlog.Infof("Processed %d lines", lineCount)
			doxlog.Infof("Finished concurrent cloning of repositories")
			return errorMap
		case <-ctxWithTimeout.Done():
			s.attemptCancel()
			if errors.Is(ctxWithTimeout.Err(), context.DeadlineExceeded) {
				doxlog.Errorf("Operation timed out after 30 minutes")
			} else {
				doxlog.Infof("Operation was canceled")
			}
			errorMap["global"] = ctxWithTimeout.Err()
			return errorMap
		}
		if messageChan == nil && errorChan == nil && outputChan == nil {
			break
		}
	}

	<-done
	close(dbrChan) // Ensure dbrChan is closed if we break out of the loop
	doxlog.Infof("Processed %d lines", lineCount)
	doxlog.Infof("Finished concurrent cloning of repositories")
	return errorMap
}

// Assuming you have a method to process individual lines
func (s *ReadOnlySyncer) tsv2dbr(line string) (*kvs.DbR, error) {
	id, value, err := inOut.LineParser(line, "\t", false, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to parse line: %v", err)
	}
	if id == nil {
		return nil, fmt.Errorf("failed to parse line: %s", line)
	}
	dbr := kvs.NewDbR()
	dbr.KP = id
	dbr.Value = string(value)
	return dbr, nil
}

// ConcurrentClone2Db initiates the concurrent cloning of repositories writes each one to the database it becomes available.
// All repos will be cloned into the parentDir.
// If deleteParentDirFirst is true, the parent directory will be deleted before the cloning begins.
// All existing repos in the parent directory will be deleted and replaced.
// Only files with a .tsv suffix will be loaded into the database.
// If depth is set to 1, no git history will be downloaded.
// This can significantly decrease clone time and space of the cloned repo.
// If removeGitDir is true, after cloning, the .git directory will be deleted from
// the repository.
func (s *ReadOnlySyncer) ConcurrentClone2Db(parentDir string, urls []string, deleteParentFirst, removeGitDir bool, depth int) error {
	done, err := s.helpers.dvcs.Git.CloneReposWithNotification(*s.ctx, parentDir, urls, deleteParentFirst, removeGitDir, depth)
	if err != nil {
		return fmt.Errorf("failed to clone repos: %w", err)
	}

	dbrChan := make(chan *kvs.DbR)
	errChan := make(chan error)
	batchSize := 5000

	// Create a WaitGroup to wait for all goroutines to finish
	var wg sync.WaitGroup
	wg.Add(2) // One for data processing, one for error handling

	// Start the BatchStream processing in a separate goroutine
	go s.helpers.ds.BatchStream(*s.ctx, batchSize, dbrChan, errChan)

	// Send data to the BatchStream
	go func() {
		defer wg.Done()
		defer close(dbrChan)

		for clonedPath := range done {
			if s.Cancelled {
				//traceMsg("ConcurrentClone2Db has been cancelled")
				return
			}
			select {
			case <-(*s.ctx).Done():
				//traceMsg("cancelling in ConcurrentClone2Db")
				s.attemptCancel()
				return
			default:
			}

			doxlog.Infof("Repository cloned at: %s", clonedPath)
			if !ltfile.DirExists(clonedPath) {
				doxlog.Warnf("Cloned directory does not exist: %s", clonedPath)
				continue
			}

			tsvFiles, err := ltfile.FileMatcher(clonedPath, kvs.FileExtension, nil)
			if err != nil {
				doxlog.Errorf("Failed to match files in %s: %v", clonedPath, err)
				continue
			}

			if len(tsvFiles) == 0 {
				doxlog.Infof("No TSV files found in %s", clonedPath)
				continue
			}
			_ = s.processFiles(tsvFiles)

			errs := s.helpers.ds.ImportDir("", clonedPath, s.helpers.paths.BackupPath, true)
			if len(errs) > 0 {
				doxlog.Errorf("Failed to import directory %s: %v", clonedPath, errs[0])
			}
		}
	}()

	// Handle errors from BatchStream
	go func() {
		defer wg.Done()
		for err := range errChan {
			if s.Cancelled {
				//traceMsg("errors have been cancelled")
				return
			}
			select {
			case <-(*s.ctx).Done():
				//traceMsg("cancelling in ConcurrentClone2Db")
				s.attemptCancel()
				return
			default:
			}
			if err != nil {
				doxlog.Errorf("Error in BatchStream: %v", err)
			}
		}
	}()

	// Wait for all goroutines to finish
	wg.Wait()

	// Close the error channel after all processing is done
	close(errChan)
	doxlog.Infof("ConcurrentClone2Db finished processing data")
	return nil
}

func (s *ReadOnlySyncer) attemptCancel() {
	//traceMsg("attempt cancel called")
	s.cancelMux.Lock()
	if !s.Cancelled {
		go s.RestorePreviousSync()
		s.Cancelled = true
	}
	s.cancelMux.Unlock()
}
func (s *ReadOnlySyncer) InitAll() error {
	return nil
}

type FileResult struct {
	FileName string
	Lines    []string
}

func (s *ReadOnlySyncer) processFiles(fileList []string) []FileResult {
	numWorkers := runtime.NumCPU() // Optimize worker count based on available CPUs
	tasks := make(chan string, len(fileList))
	results := make(chan FileResult, len(fileList))
	var wg sync.WaitGroup
	// Start worker pool
	for i := 0; i < numWorkers; i++ {
		if s.Cancelled {
			//traceMsg("processFiles has been cancelled")
			break
		}
		select {
		case <-(*s.ctx).Done():
			//traceMsg("cancelling in processFiles")
			s.attemptCancel()
			return nil
		default:
		}
		wg.Add(1)
		go worker(tasks, results, &wg)
	}

	// Enqueue tasks
	for _, file := range fileList {
		if s.Cancelled {
			//traceMsg("enqueTasks has been cancelled")
			break
		}
		select {
		case <-(*s.ctx).Done():
			//traceMsg("cancelling in processFiles")
			s.attemptCancel()
			return nil
		default:
		}

		tasks <- file
	}
	close(tasks)

	// Wait for all workers to finish
	go func() {
		wg.Wait()
		close(results)
	}()

	// Collect and sort results
	var allResults []FileResult
	for result := range results {
		allResults = append(allResults, result)
	}

	// Sort results by filename
	sort.Slice(allResults, func(i, j int) bool {
		return allResults[i].FileName < allResults[j].FileName
	})

	return allResults
}

func worker(tasks <-chan string, results chan<- FileResult, wg *sync.WaitGroup) {
	defer wg.Done()
	for filePath := range tasks {
		result := processFile(filePath)
		results <- result
	}
}

func processFile(filePath string) FileResult {
	file, err := os.Open(ltfile.ToSysPath(filePath))
	if err != nil {
		// Handle error
		return FileResult{FileName: "/"}
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var lines []string

	for scanner.Scan() {
		line := scanner.Text()
		lines = append(lines, line)
	}

	if err := scanner.Err(); err != nil {
		// Handle error
	}

	// Sort lines alphabetically
	sort.Strings(lines)

	return FileResult{
		FileName: path.Base(filePath),
		Lines:    lines,
	}
}
func prepareBBoltRecords(results []FileResult) []BBoltRecord {
	var bboltRecords []BBoltRecord
	for _, result := range results {
		for _, line := range result.Lines {
			bboltRecords = append(bboltRecords, BBoltRecord{
				FileName: result.FileName,
				Line:     line,
			})
		}
	}
	return bboltRecords
}

type BBoltRecord struct {
	FileName string
	Line     string
}

// getCloneList returns RemoteMeta for which the file system dir does not contain a .git dir.
func (s *ReadOnlySyncer) getCLoneList() (list []*RemoteMeta) {
	for _, meta := range s.GetAllFromMap() {
		if !ltfile.DirExists(path.Join(meta.FsPath, ".git")) {
			meta.Cloned = true
			list = append(list, meta)
		}
	}
	return
}
func (s *ReadOnlySyncer) projectFromUrl(urlStr string) string {
	urlStruct, err := url.Parse(urlStr)
	if err != nil {
		return s.helpers.paths.Project
	}
	chunks := strings.Split(strings.TrimLeft(urlStruct.Path, "/"), "/")
	if len(chunks) < 1 {
		return s.helpers.paths.Project
	}
	return chunks[0]
}

// Init ensures that the specified remote has a local and vice versa
func (s *ReadOnlySyncer) Init(key string) error {
	//since this is the ReadOnly syncher, if a remote does not exist, it will NOT be created
	if meta, exists := s.rmm.getFromMap(key); exists {
		cloneUrl := meta.Url
		if !strings.HasPrefix(cloneUrl, "https://") {
			cloneUrl = "https://" + cloneUrl
		}
		if !strings.HasSuffix(cloneUrl, ".git") {
			cloneUrl += ".git"
		}
		_, err := s.helpers.dvcs.Git.Clone(meta.FsClonePath, cloneUrl, true, false, 1)
		if err != nil {
			meta.AddIssue("InitErr", fmt.Sprintf("Could not init '%s': %v", key, err), Error)
			return err
		}
	}
	return nil
}

func (s *ReadOnlySyncer) OkToSync() bool {
	return s.helpers.internetAvailable
}

// SyncAutoOn sleeps the initialDelay, then starts a go routine with a loop to periodically call SyncAll in a loop.
// To cancel, call SyncAutoOff
func (s *ReadOnlySyncer) SyncAutoOn(initialDelay time.Duration) error {
	if s.helpers == nil {
		return errors.New(fmt.Sprintf("%s helpers not initialized", s.Name))
	}
	ctx, cancelAutoSync := context.WithCancel(context.Background())
	s.ctx = &ctx
	s.cancelAutoSync = &cancelAutoSync
	go func() {
		s.syncAuto(initialDelay)
	}()
	return nil
}
func (s *ReadOnlySyncer) syncAuto(initialDelay time.Duration) {
	defer (*s.cancelAutoSync)() // call cancel when done to release resources
	duration := 10 * time.Minute
	s.autoMux.Lock()
	s.AutoOn = true
	s.helpers.RelayMuted = true
	s.autoMux.Unlock()

	defer func() {
		s.autoMux.Lock()
		s.helpers.RelayMuted = false
		s.AutoOn = false
		s.autoMux.Unlock()
	}()

	time.Sleep(initialDelay)
	for {
		select {
		case <-(*s.ctx).Done():
			// The context was cancelled
			doxlog.Infof("auto %s cancelled", s.Name)
			return
		default:
			// Continue
		}

		s.autoMux.Lock()
		if !s.OkToSync() {
			s.autoMux.Unlock()
			if s.Debug {
				doxlog.Debugf("auto %s is sleeping for %v minutes", s.Name, duration*time.Minute)
			}
			time.Sleep(duration)
			continue
		}
		s.autoMux.Unlock()

		err := s.syncAll()
		if err != nil {
			doxlog.Error(fmt.Sprintf("auto %s err: %v", s.Name, err.Error()))
			return
		}
		s.NumSyncsSinceStartup++
		time.Sleep(duration)
	}
}
func (s *ReadOnlySyncer) SyncAutoOff() error {
	if s.cancelAutoSync != nil {
		(*s.cancelAutoSync)()
	}
	s.AutoOn = false
	return nil
}

// SyncAll does a one-time sync of all remotes / locals in the map
func (s *ReadOnlySyncer) SyncAll() error {
	if s.AutoOn || s.syncAllRunning || s.syncRunning {
		return errors.New("sync already in progress")
	}
	s.syncAllRunning = true
	defer func() {
		s.NumSyncsSinceStartup++
		s.syncAllRunning = false
	}()
	return s.syncAll()
}
func (s *ReadOnlySyncer) updateSyncStats() {
	s.LastSyncDuration = time.Since(s.LastSync)
	s.LastSync = time.Now()
	if s.helpers.SysManager != nil {
		s.helpers.SysManager.StoreCurrentProjectSettingByKey("ro", "lastSync", s.LastSync.Format(time.RFC3339))
	}
}
func (s *ReadOnlySyncer) syncAll() error {
	s.helpers.SysManager.StoreCurrentProjectSettingByKey("ro", "state", "dirty")
	s.Cancelled = false
	s.prepareForSync()
	defer s.updateSyncStats()
	s.LastSync = time.Now()
	s.InSync = true
	defer func() {
		s.InSync = false
	}()
	ctx, cancelFunc := context.WithCancel(context.Background())
	s.ctx = &ctx
	s.cancelAutoSync = &cancelFunc
	defer cancelFunc()
	syncAllStamp := stamp.NewStamp("ro sync")
	syncAllStamp.Start()
	s.Relay("starting ro sync")
	defer func() {
		s.Relay(fmt.Sprintf("%s", syncAllStamp.FinishMillis()))
	}()

	err := s.LoadActionList()
	if err != nil {
		return err
	}
	// TODO: make use of ability for user to cancel the update.
	//ctx, cancel := context.WithCancel(context.Background())
	//defer cancel() // Ensure that cancel is called to free up resources
	wg := sync.WaitGroup{}
	errorChan := make(chan error, 3) // Buffer size to hold errors from goroutines

	wg.Add(1)
	go func() {
		defer wg.Done()
		s.Relay("started process file downloads")
		//traceMsg("started process file downloads")
		if err := s.processFileDownloads(); err != nil {
			errorChan <- err
			//			cancel() // Cancel other goroutines on error
		}
		s.Relay("finished process file downloads")
		//traceMsg("finished process file downloads")
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		s.Relay("started process clone to filesystem")
		//traceMsg("started process clone to filesystem")
		if errs := s.processCloneToFsList(); len(errs) > 0 {
			for url, err := range errs {
				if ent, ok := s.rmm.getFromMap(url); ok && ent != nil {
					ent.AddIssue("Clone Error", err.Error(), Error)
					continue
				}
				doxlog.Errorf("url '%s' marked as containing errors, but could not find it in map", url)
			}
		}
		s.Relay("finished process clone to filesystem")
		//traceMsg("finished process clone to filesystem")
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		s.Relay("started process clone to database")
		//traceMsg("started process clone to database")
		if errs := s.processCloneToDbList(); errs != nil {
			for dbPath, err := range errs {
				if meta, found := s.rmm.getFromMap(dbPath); found {
					meta.AddIssue("CloneErr", err.Error(), Error)
					continue
				}
				doxlog.Errorf("%s marked as containing error, but not found in remote meta map", dbPath)
			}
		}
		s.Relay("finished process clone to database")
		//traceMsg("finished process clone to database")
	}()

	wg.Wait()
	close(errorChan) // Close the error channel

	// Check for errors
	var combinedErr error
	for err := range errorChan {
		combinedErr = fmt.Errorf("%w; %v", combinedErr, err)
	}
	// export db records
	if !s.Cancelled {
		go s.cleanupAfterSync()
		s.helpers.SysManager.StoreCurrentProjectSettingByKey("ro", "state", "clean")
	}
	if combinedErr != nil {
		// Handle the combined error as needed, e.g., logging
		return combinedErr
	}
	return nil
}

func (s *ReadOnlySyncer) prepareForSync() {
	//rename fallback templates directory
	//TODO: all valid directories
	if ltfile.DirExists(s.helpers.paths.FallbackTemplatesPath) {
		ltfile.RenameDir(s.helpers.paths.FallbackTemplatesPath, s.helpers.paths.FallbackTemplatesPath+oldVersionPrefix)
	}
}

func (s *ReadOnlySyncer) cleanupAfterSync() {
	var exportList []string
	for _, rm := range s.GetAllFromMap() {
		exportList = append(exportList, rm.FsPath)
	}
	s.helpers.ds.SetExportFilter(exportList)
	s.helpers.ds.ExportAll(path.Join(path.Dir(s.helpers.paths.FallbackTemplatesPath), config.ResourcesDir))
	ltfile.DeleteDirRecursively(path.Dir(s.helpers.paths.FallbackTemplatesPath) + oldVersionPrefix)
	s.catMan.MarkUpdated()
}

/* cancellation protocol
beforeSync:
	[ x ] rename old templates directory
afterSync export method (no cancel)
	[ x ] export records
	[ x ] remove old templates dir
restore method:
	[ x ] delete subs drs from db
	[ x ] import old subscriptions dir from fs
	[   ] delete new template dir
	[   ] rename old template dir back to original name
*/

func (s *ReadOnlySyncer) RestorePreviousSync() {
	RestoreDataFromSavepoint(s.helpers.SysManager, s.catMan, s.helpers.ds, path.Dir(s.helpers.paths.FallbackTemplatesPath))
}
func RestoreDataFromSavepoint(sys *sysManager.SysManager, catMan *catalog.Manager, wrapper *kvsw.Wrapper, path string) {
	//fetch savepoint
	if cat, exists := sys.LoadCurrentProjectSettingByKey("ro", "catalog"); exists {
		if ver, exists := sys.LoadCurrentProjectSettingByKey("ro", "version"); exists {
			newCat := catMan.GetCatalogFromDb(catMan.CatalogCollectionKP(), cat, ver)
			if newCat == nil {
				return
			}
			if newCat.Entries == nil {
				return
			}
			var fsList []string
			var dsList []string
			var internal map[string]*struct{}
			for _, ent := range newCat.Entries {
				if strings.HasPrefix(ent.Path, config.ResourcesDir) {
					if strings.HasSuffix(ent.Path, config.ConfigsDir) {
						continue
					}
					dsList = append(dsList, ent.Path)
					continue
				}
				fsList = append(fsList, ent.Path)
				catMan.MarkSubscribedAtVersion(ent.Name, cat, ver, false, internal)
			}
			RestoreDataFromFilesystem(fsList, dsList, path, wrapper)
			sys.StoreCurrentProjectSettingByKey("ro", "state", "clean")
		}
	}
}

func RestoreDataFromFilesystem(files, imports []string, prefix string, wrapper *kvsw.Wrapper) {
	for _, dir := range files {
		if target := path.Join(prefix, dir); ltfile.DirExists(target + oldVersionPrefix) {
			go func() {
				ltfile.DeleteDirRecursively(target)
				ltfile.RenameDir(target+oldVersionPrefix, target)
			}()
		}
	}

	for _, dbent := range imports {
		go wrapper.ImportDir(strings.TrimPrefix(dbent, config.ResourcesDir+"/"), path.Join(prefix, dbent), "", true)
	}
}

// Sync does a one-time sync of the specified remote/ local
func (s *ReadOnlySyncer) Sync(key string) error {
	if s.AutoOn || s.syncAllRunning {
		return errors.New("sync already in progress")
	}
	s.syncRunning = true
	defer func() {
		s.syncRunning = false
	}()
	remote, _ := s.GetFromMap(key)
	if err := s.sync(remote); err != nil {
		fmt.Printf("Error: Could not sync %s: %v\n", key, err)
		return err
	}
	dirPath := path.Join(s.helpers.paths.SubscriptionsPath, s.projectFromUrl(remote.Url), remote.FsPath)
	if !ltfile.DirExists(path.Join(s.helpers.paths.ProjectDirPath, remote.FsPath)) || strings.Contains(remote.FsPath, "configs") {
		errs := s.helpers.ds.ImportDir("", dirPath, s.helpers.paths.BackupPath, true)
		if len(errs) > 0 {
			return errs[0]
		}
	}
	return s.afterSync(key)
}
func (s *ReadOnlySyncer) sync(remoteMeta *RemoteMeta) error {
	if !s.OkToSync() {
		s.Relay(fmt.Sprintf("internet not available, so can't sync %s", remoteMeta.Url))
		doxlog.Error(fmt.Sprintf("internet not available, so can't sync %s", remoteMeta.Url))
		return fmt.Errorf("internet unavailable, cannot sync")
	}
	s.Relay(fmt.Sprintf("syncing %s", remoteMeta.Url))
	doxlog.Info(fmt.Sprintf("syncing %s", remoteMeta.Url))
	s.helpers.Bus.Publish("subscriptions:sync", "started")
	defer s.helpers.Bus.Publish("subscriptions:sync", "finished")

	// download repo
	if ltfile.DirExists(path.Join(remoteMeta.FsPath, ".git")) {
		status, err := s.helpers.dvcs.Git.PullOrigin(remoteMeta.FsPath, true)
		if err != nil {
			if status != gitStatuses.AlreadyUpToDate {
				issue := fmt.Sprintf("Could not update %s: %v (status %s)", remoteMeta.FsPath, err, status.String())
				remoteMeta.AddIssue("PullErr", issue, Error)
				doxlog.Errorf("%v", err)
				s.Relay(issue)
				return err
			}
		}
	} else { // clone
		err := s.Init(remoteMeta.Url)
		if err != nil {
			doxlog.Errorf("%v", err)
		}

	}
	remoteMeta.LastSynced = time.Now()
	remoteMeta.NumSyncsSinceStartup++
	s.Relay(fmt.Sprintf("finished syncing %s", remoteMeta.Url))
	doxlog.Info(fmt.Sprintf("finished syncing %s", remoteMeta.Url))
	return nil
}
func (s *ReadOnlySyncer) PutToMap(remote *RemoteMeta) {
	s.rmm.putToMap(remote)
}
func (s *ReadOnlySyncer) DeleteFromMap(key string) {
	s.rmm.deleteFromMap(key)
}
func (s *ReadOnlySyncer) GetAllFromMap() []*RemoteMeta {
	return s.rmm.syncMapToSlice()
}

func (s *ReadOnlySyncer) GetFromMap(key string) (remote *RemoteMeta, ok bool) {
	return s.rmm.getFromMap(key)
}

func (s *ReadOnlySyncer) afterSync(key string) error {
	basePath, exists := s.rmm.getFromMap(key)
	if !exists {
		return errors.New(fmt.Sprintf("key '%s' not in map", key))
	}
	switch basePath.FsPath {
	case "assets":
		htmlCssUrl, err := url.JoinPath(s.helpers.paths.SubscriptionsPath, s.projectFromUrl(key), basePath.FsPath, "css", "app.css")
		if err != nil {
			doxlog.Errorf("error creating htmlCss URL: %v", err)
			return err
		}
		pdfCssUrl, err := url.JoinPath(s.helpers.paths.SubscriptionsPath, s.projectFromUrl(key), basePath.FsPath, "pdf", "css", "pdf.css")
		if err != nil {
			doxlog.Errorf("error creating pdfCss URL: %v", err)
			return err
		}
		html, err := css.NewStyleSheet("html", s.helpers.paths.HtmlCss, false)
		if err != nil {
			doxlog.Errorf("could not open HTML stylesheet: %v", err)
			return err
		}
		pdf, err := css.NewStyleSheet("pdf", s.helpers.paths.PdfCss, false)
		if err != nil {
			doxlog.Errorf("could not open PDF stylesheet: %v", err)
			return err
		}
		fallbackHtml, err := css.NewStyleSheet("html", htmlCssUrl, false)
		if err != nil {
			doxlog.Errorf("could not open HTML stylesheet: %v", err)
			return err
		}
		fallbackPdf, err := css.NewStyleSheet("pdf", pdfCssUrl, false)
		if err != nil {
			doxlog.Errorf("could not open PDF stylesheet: %v", err)
			return err
		}

		for sel, rule := range fallbackHtml.RuleMap {
			if _, exists := html.RuleMap[sel]; exists {
				continue
			}
			if !strings.HasPrefix(sel, "p.") {
				if !strings.HasPrefix(sel, "span.") {
					if !strings.HasPrefix(sel, "a.") {
						if !strings.HasPrefix(sel, "hr.") {
							if !strings.HasPrefix(sel, "@font-face") {
								continue
							}
						}
					}
				}
			}
			html.RulesToAdd = append(html.RulesToAdd, rule)
		}
		err = html.SerializeMissingRules()
		if err != nil {
			doxlog.Errorf("could not save HTML stylesheet: %v", err)
			return err
		}
		for sel, rule := range fallbackPdf.RuleMap {
			if _, exists := pdf.RuleMap[sel]; exists {
				continue
			}
			if !strings.HasPrefix(sel, "p.") {
				if !strings.HasPrefix(sel, "span.") {
					if !strings.HasPrefix(sel, "a.") {
						if !strings.HasPrefix(sel, "hr.") {
							if !strings.HasPrefix(sel, "@font-face") {
								continue
							}
						}
					}
				}
			}
			pdf.RulesToAdd = append(pdf.RulesToAdd, rule)
		}

		err = pdf.SerializeMissingRules()
		if err != nil {
			doxlog.Errorf("could not save PDF stylesheet: %v", err)
			return err
		}

		return nil
	}
	return nil
}

const relayPrefixRO = "SRO"

func (s *ReadOnlySyncer) SetMsgChannel(ch chan string) {
	s.msgChan = ch
}
func (s *ReadOnlySyncer) GetMsgChannel() chan string {
	return s.msgChan
}

func (s *ReadOnlySyncer) Relay(msg string) {
	if s.helpers.RelayMuted {
		return
	}
	if s.helpers.RelayUsesStandardOut {
		fmt.Printf("%s: %s\n", relayPrefixRO, msg)
		return
	}
	if s.msgChan != nil {
		s.msgChan <- fmt.Sprintf("%s: %s", relayPrefixRO, msg)
	}
}

// BuildFallbackTemplates goes over recipieList in FIFO order
// each target can be either a directory or a file. if a directory then all it's children are added,
// if a file, then the file is added to the base directory (the hierachy is not preserved)
func (s *ReadOnlySyncer) BuildFallbackTemplates(recipieList []string) {
	if recipieList == nil {
		return
	}
	//ensure said directory exists
	if !ltfile.DirExists(s.helpers.paths.FallbackTemplatesPath) {
		ltfile.CreateDir(s.helpers.paths.FallbackTemplatesPath)
	}
	for _, part := range recipieList {
		if part == s.helpers.paths.FallbackTemplatesPath {
			continue //prevent self-copy
		}
		if strings.HasSuffix(part, ".lml") {
			ltfile.CopyFile(part, path.Join(s.helpers.paths.FallbackTemplatesPath, path.Base(part)))
			continue
		}
		ltfile.ConcurrentCopyDir(part, s.helpers.paths.FallbackTemplatesPath, 128)
	}
}
