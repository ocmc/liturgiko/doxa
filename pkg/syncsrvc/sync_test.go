package syncsrvc

import (
	"path/filepath"
	"sort"
	"testing"
)

func TestNewSyncer(t *testing.T) {
	parms := new(ManagerParms)
	m := NewManager(parms)
	ro1 := new(RemoteMeta)
	ro1.Url = "https://gitlab.com/doxa-eac/resources/ltx/en_us_dedes.git"
	ro1.DbPath = "ltx/en_us_dedes"
	ro1.FsPath = filepath.Join("doxa", "projects", "doxa-eac", "resources", "ltx", "en_us_dedes")
	ro2 := new(RemoteMeta)
	ro2.Url = "https://gitlab.com/doxa-eac/resources/ltx/gr_gr_cog.git"
	ro2.DbPath = "ltx/gr_gr_cog"
	ro2.FsPath = filepath.Join("doxa", "projects", "doxa-eac", "resources", "ltx", "gr_gr_cog")
	m.RoSyncer.PutToMap(ro1)
	m.RoSyncer.PutToMap(ro2)
	m.RwSyncer.PutToMap(ro1)
	m.RwSyncer.PutToMap(ro2)
	remote, ok := m.RoSyncer.GetFromMap("ltx/en_us_dedes")
	if !ok {
		t.Errorf("RemoteMeta not found in map")
	}
	if remote.Url != "https://gitlab.com/doxa-eac/resources/ltx/en_us_dedes.git" {
		t.Errorf("Incorrect URL for remote")
	}
	if remote.DbPath != "ltx/en_us_dedes" {
		t.Errorf("Incorrect DB path for remote")
	}
	if remote.FsPath != filepath.Join("doxa", "projects", "doxa-eac", "resources", "ltx", "en_us_dedes") {
		t.Errorf("Incorrect file system path for remote")
	}
	m.RoSyncer.DeleteFromMap(remote.FsPath)
	remote, ok = m.RoSyncer.GetFromMap("ltx/en_us_dedes")
	if ok {
		t.Errorf("RemoteMeta should have been deleted from map")
	}
}

type TestIssueVals []struct {
	code string
	desc string
	typ  IssueType
}

func (s TestIssueVals) Len() int           { return len(s) }
func (s TestIssueVals) Less(i, j int) bool { return s[i].code < s[j].code }
func (s TestIssueVals) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }

// Implement sort.Interface for Issue
type IssueSlice []*Issue

func (s IssueSlice) Len() int           { return len(s) }
func (s IssueSlice) Less(i, j int) bool { return s[i].Code < s[j].Code }
func (s IssueSlice) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }

func TestNewRemoteMeta(t *testing.T) {
	myUrl := "https://example.com/project/repository.git"
	myRemote := NewRemoteMeta(myUrl, "", "", ReadWrite)
	if myRemote.Url != myUrl {
		t.Errorf("Expected url to be '%s' but got '%s'", myUrl, myRemote.Url)
		return
	}
	myProblems := TestIssueVals{{code: "responseError", desc: "Got 404 not found for url /project/repository.git", typ: Error},
		{code: "emptyProject", desc: "Project has no TSV files", typ: Warning},
		{code: "permissionsError", desc: "You do not have permission to access your home directory", typ: Error}}

	var expectedWarnCount, expectedErrCount int
	for _, issue := range myProblems {
		myRemote.AddIssue(issue.code, issue.desc, issue.typ)
		if issue.typ == Warning {
			expectedWarnCount++
		} else if issue.typ == Error {
			expectedErrCount++
		}
	}

	if !myRemote.HasIssues() {
		t.Errorf("Expected true, got false")
		return
	}

	if len(myRemote.Issues) != len(myProblems) {
		t.Errorf("Expected %d issues got %d", len(myProblems), len(myRemote.Issues))
		return
	}

	actualWarnCount := myRemote.NbrIssuesOfType(Warning)
	actualErrCount := myRemote.NbrIssuesOfType(Error)
	if expectedWarnCount != actualWarnCount {
		t.Errorf("Expected %d warnings but got %d", expectedWarnCount, actualWarnCount)
		return
	}
	if expectedErrCount != actualErrCount {
		t.Errorf("Expected %d warnings but got %d", expectedErrCount, actualErrCount)
		return
	}
	var expectedWarns TestIssueVals
	var actualWarns IssueSlice
	for _, i := range myProblems {
		if i.typ == Warning {
			expectedWarns = append(expectedWarns, i)
		}
	}
	actualWarns = myRemote.IssuesOfType(Warning)
	sort.Sort(expectedWarns)
	sort.Sort(actualWarns)
	levels := []string{"Warning", "Error"}

	for i, _ := range expectedWarns {
		if expectedWarns[i].code != actualWarns[i].Code {
			t.Errorf("Expected code '%s' but got '%s'", expectedWarns[i].code, actualWarns[i].Code)
		}
		if expectedWarns[i].desc != actualWarns[i].Desc {
			t.Errorf("Expected code '%s' but got '%s'", expectedWarns[i].code, actualWarns[i].Code)
		}
		if expectedWarns[i].typ != actualWarns[i].Level {
			t.Errorf("Expected level %d ('%s') but got %d", expectedWarns[i].typ, levels[expectedWarns[i].typ], actualWarns[i].Level)
		}
	}

	var expectedErrs TestIssueVals
	var actualErrs IssueSlice
	for _, i := range myProblems {
		if i.typ == Error {
			expectedErrs = append(expectedErrs, i)
		}
	}
	actualErrs = myRemote.IssuesOfType(Error)
	sort.Sort(expectedErrs)
	sort.Sort(actualErrs)

	for i, _ := range expectedErrs {
		if expectedErrs[i].code != actualErrs[i].Code {
			t.Errorf("Expected code '%s' but got '%s'", expectedErrs[i].code, actualErrs[i].Code)
		}
		if expectedErrs[i].desc != actualErrs[i].Desc {
			t.Errorf("Expected code '%s' but got '%s'", expectedErrs[i].code, actualErrs[i].Code)
		}
		if expectedErrs[i].typ != actualErrs[i].Level {
			t.Errorf("Expected level %d ('%s') but got %d", expectedErrs[i].typ, levels[expectedErrs[i].typ], actualErrs[i].Level)
		}
	}
}
