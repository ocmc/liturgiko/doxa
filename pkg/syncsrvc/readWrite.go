package syncsrvc

import (
	"context"
	"errors"
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/catalog"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/dbPaths"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/dvcs"
	"github.com/liturgiko/doxa/pkg/enums/gitStatuses"
	"github.com/liturgiko/doxa/pkg/utils/stamp"
	"net/url"
	"path"
	"sync"
	"time"
)

type ReadWriteSyncer struct {
	cancelAutoSync      *context.CancelFunc
	ctx                 *context.Context // initiated for auto sync, used to cancel it
	helpers             *SyncerHelpers
	rmm                 *RemoteMetaManager
	OffLineWaitDuration time.Duration
	OnlineWaitDuration  time.Duration
	Name                string
	AutoOn              bool
	syncAllRunning      bool // true if SyncAll called when AutoOn != true && syncRunning != true
	syncRunning         bool // true if Sync called when AutoOn != true && syncAllRunning != true
	autoMux             sync.Mutex
	Debug               bool
	catMan              *catalog.Manager
	omitMap             map[string]struct{}
	msgChan             chan string
	LastSync            time.Time
}

func NewReadWriteSyncer(helpers *SyncerHelpers, autoSync bool) *ReadWriteSyncer {
	var err error
	s := new(ReadWriteSyncer)
	s.helpers = helpers
	s.rmm = new(RemoteMetaManager)
	s.OffLineWaitDuration = ReadWriteOfflineWaitDuration
	s.OnlineWaitDuration = ReadWriteOnlineWaitDuration
	gitlabPath, err := url.Parse(s.helpers.GitlabApi)
	if err != nil {
		doxlog.Errorf("Could not parse gitlab api: %v", err)
		return nil
	}

	s.catMan, err = catalog.NewManager(s.helpers.sysDs, s.helpers.Bus, s.helpers.dvcs, gitlabPath.Host)
	if err != nil {
		doxlog.Errorf("could not access catalog: %v", err)
		return nil
	}
	if autoSync {
		err := s.SyncAutoOn(time.Minute * 1)
		if err != nil {
			doxlog.Warnf("auto %s  err: %v", s.Name, err)
		}
	}

	if s.helpers.SysManager != nil {
		if val, exists := s.helpers.SysManager.LoadCurrentProjectSettingByKey("rw", "lastSync"); exists {
			s.LastSync, err = time.Parse(time.RFC3339, val)
		}
	}
	return s
}
func (s *ReadWriteSyncer) AutoSyncOn() bool {
	fmt.Printf("AutoSync now enabled")
	return s.AutoOn
}
func (s *ReadWriteSyncer) SetDebug(to bool) {
	s.Debug = to
}

// InitAll calls Init for each instance of RemoteMeta in the map.
func (s *ReadWriteSyncer) InitAll() error {
	fmt.Println("Everything initialized")
	return nil
}

// Init ensures that the specified remote has a local and vice versa
func (s *ReadWriteSyncer) Init(key string) error {
	fmt.Printf("%s initialized\n", key)
	return nil
}

func (s *ReadWriteSyncer) PutToMap(remote *RemoteMeta) {
	s.rmm.putToMap(remote)
}

func (s *ReadWriteSyncer) DeleteFromMap(key string) {
	s.rmm.deleteFromMap(key)
}
func (s *ReadWriteSyncer) GetAllFromMap() []*RemoteMeta {
	return s.rmm.syncMapToSlice()
}
func (s *ReadWriteSyncer) GetFromMap(key string) (remote *RemoteMeta, ok bool) {
	return s.rmm.getFromMap(key)
}
func (s *ReadWriteSyncer) OkToSync() bool {
	return s.helpers.internetAvailable
}

func (s *ReadWriteSyncer) Status() string {
	justWarns, errs := s.rmm.numRemotesWithIssues()
	return fmt.Sprintf("ReadWriteSyncer-- autoOn: %v internetAvailable: %v syncListItems with warnings: %d with errors: %d", s.AutoOn, s.helpers.internetAvailable, justWarns, errs)
}

// SyncAutoOn waits initialDelay, then starts a go routine loop to sync all remotes and locals.
func (s *ReadWriteSyncer) SyncAutoOn(initialDelay time.Duration) error {
	if s.helpers == nil {
		return errors.New(fmt.Sprintf("%s helpers not initialized", s.Name))
	}
	ctx, cancelAutoSync := context.WithCancel(context.Background())
	s.ctx = &ctx
	s.cancelAutoSync = &cancelAutoSync
	go s.syncAuto(initialDelay)
	return nil
}
func (s *ReadWriteSyncer) SyncAutoOff() error {
	(*s.cancelAutoSync)()
	return nil
}

// SyncAll does a one-time sync of all remotes / locals in the map
func (s *ReadWriteSyncer) SyncAll() error {
	if s.AutoOn || s.syncAllRunning || s.syncRunning {
		return errors.New("sync already in progress")
	}
	if s.helpers.dvcs.Gitlab == nil {
		if s.helpers.token == "" {
			return fmt.Errorf("ReadWrite received an empty token. make sure your token is properly unlocked and valid")
		}
		// we have the token: try to initialize
		// it is possible gitlab is already being initialized, wait
		timeout := 1 * time.Second
		wayBackThen := time.Now()
		for s.helpers.dvcs.Gitlab == nil && time.Since(wayBackThen) < timeout {
		}
		//nope, it still is nil :/
		if s.helpers.dvcs.Gitlab == nil {
			gID, err := s.helpers.dvcs.GetProjectGroup(s.helpers.token, path.Base(s.helpers.paths.Project), s.helpers.GitlabApi)
			if err != nil {
				return err
			}
			myDvcs, err := dvcs.NewDvcsClientWithGitlab(s.helpers.paths.Project,
				s.helpers.paths.AssetsPath,
				s.helpers.token, gID, s.helpers.GitlabApi, true)
			if err != nil {
				return err
			}
			if myDvcs == nil {
				return fmt.Errorf("no error was encountered, yet Gitlab is stil nil")
			}
			// just in case, make sure it is still nil
			if s.helpers.dvcs.Gitlab == nil {
				//we replace the ENTIRE dvcs module as it is from the Ctx struct
				s.helpers.dvcs = myDvcs
			}
		}
	}
	s.syncAllRunning = true
	defer func() {
		s.syncAllRunning = false
	}()
	return s.syncAll()
}
func (s *ReadWriteSyncer) syncAll() error {
	var err error
	s.Relay("")
	if s.helpers.token == "" {
		s.Relay("Error: you must provide your token")
		return fmt.Errorf("missing token")
	}

	err = s.helpers.dvcs.Gitlab.Init()
	if err != nil {
		s.Relay(fmt.Sprintf("Error initializing Gitlab client: %v", err))
	}
	syncStamp := stamp.NewStamp("Synchronizing your repositories")
	syncStamp.Start()
	s.Relay("Synchronizing your repositories")
	s.PopulateRwMap()
	defer func() {
		go func() {
			err := s.catMan.UpdateInventoryVersionHash()
			if err != nil {
				s.Relay(err.Error())
				doxlog.Errorf(err.Error())
			}
		}()
	}()
	var wg sync.WaitGroup
	//iterate over remotes, and sync
	for _, meta := range s.GetAllFromMap() {
		err = s.helpers.dvcs.ValidateRepo(meta.FsPath,
			meta.Url,
			meta.GroupName,
			meta.UrlSubGroupPath,
			meta.RepoName,
			s.helpers.token)
		if err != nil {
			errMsg := fmt.Sprintf("Could not validate: %v", err)
			meta.AddIssue("Invalid", errMsg, Error)
			s.Relay(fmt.Sprintf("%s: %s", meta.DbPath, errMsg))
			continue
		}
		wg.Add(1)
		go func(url string) {
			defer wg.Done()
			err = s.sync(url)
			if err != nil {
				s.Relay(err.Error())
			}
		}(meta.Url)
	}
	wg.Wait()
	syncStamp.Finish()
	doxlog.Infof(fmt.Sprintf("Finished syncing everything"))
	s.Relay(fmt.Sprintf("Finished synchronizing your repositories."))
	s.Relay(fmt.Sprintf("%s", syncStamp.FinishMillis()))
	s.LastSync = time.Now()
	if s.helpers.SysManager != nil {
		s.helpers.SysManager.StoreCurrentProjectSettingByKey("rw", "lastSync", s.LastSync.Format(time.RFC3339))
	}
	//update inventory
	go s.catMan.PublishCatalog(dbPaths.SYS.InventoryPath(), s.helpers.paths.InventoryFile, true)
	return nil
}

// Sync does a one-time sync of the specified remote/ local
func (s *ReadWriteSyncer) Sync(key string) error {
	if s.AutoOn || s.syncAllRunning {
		return errors.New("sync already in progress")
	}
	s.syncRunning = true
	defer func() {
		s.syncRunning = false
	}()
	err := s.sync(key)
	if err != nil {
		return err
	}
	go func() {
		err := s.catMan.UpdateInventoryVersionHash()
		if err != nil {
			s.Relay(err.Error())
			doxlog.Errorf(err.Error())
		}
		go s.catMan.PublishCatalog(dbPaths.SYS.InventoryPath(), s.helpers.paths.InventoryFile, true)
	}()
	return nil
}
func (s *ReadWriteSyncer) sync(key string) error {
	var err error
	if !s.OkToSync() {
		s.Relay(fmt.Sprintf("internet not available, so can't sync %s", key))
	}

	remote, avail := s.GetFromMap(key)
	if !avail {
		msg := fmt.Sprintf("%s not found", key)
		doxlog.Error(msg)
		return errors.New(msg)
	}
	s.Relay(fmt.Sprintf("syncing %s", key))

	started := time.Now()
	// is this a library?
	if remote.IsDbLibrary() {
		s.helpers.dvcs.Git.LocalRepoPath = remote.FsPath
		status, err := s.helpers.dvcs.Git.Pull(remote.FsPath, dvcs.DoxaOrigin, true)
		if err != nil {
			if status != gitStatuses.AlreadyUpToDate && status != gitStatuses.RemoteEmpty {
				errMsg := fmt.Sprintf("Pull failed: %v (status: %v)", err, status.String())
				remote.AddIssue("PullErr", errMsg, Error)
				//if pull fails we proceed no further
				return fmt.Errorf("%s: %s", remote.DbPath, errMsg)
			}
		}
		// export
		kp := kvs.NewKeyPath()
		err = kp.ParsePath(remote.DbPath)
		if err != nil {
			errMsg := fmt.Sprintf("Could not parse path: %v", err)
			remote.AddIssue("ParseErr", errMsg, Error)
			return fmt.Errorf("%s: %s", remote.DbPath, errMsg)
		}

		s.Relay(fmt.Sprintf("exporting %s", remote.DbPath))
		err = s.helpers.ds.ExportLibrary(*kp, remote.FsPath)
		if err != nil {
			errMsg := fmt.Sprintf("Could not export: %v", err)
			remote.AddIssue("ExportErr", errMsg, Error)
			return fmt.Errorf("%s: %s", remote.DbPath, errMsg)
		}
		s.Relay(fmt.Sprintf("Export %s took %s", key, time.Since(started).String()))
		last := time.Now()
		// commit
		s.Relay(fmt.Sprintf("commiting %s", key))
		remote.LatestHash, err = s.helpers.dvcs.Git.CommitAllWithHash(remote.FsPath, "doxa generated backup")
		if err != nil {
			errMsg := fmt.Sprintf("Could not commit: %v", err)
			remote.AddIssue("CommitErr", errMsg, Error)
			return fmt.Errorf("%s: %s", remote.DbPath, errMsg)
		}
		s.Relay(fmt.Sprintf("Commit %s took %s", remote.DbPath, time.Since(last).String()))
		last = time.Now()
		// push
		s.Relay(fmt.Sprintf("pushing %s", key))
		err = s.helpers.dvcs.Git.Push(remote.FsPath, dvcs.Username, s.helpers.token)
		if err != nil {
			errMsg := fmt.Sprintf("Could not push: %v", err)
			remote.AddIssue("PushErr", errMsg, Error)
			return fmt.Errorf("%s: %s", remote.DbPath, errMsg)
		}
		s.Relay(fmt.Sprintf("Push of %s took %s", key, time.Since(last).String()))
		s.Relay(fmt.Sprintf("finished %s took %s", key, time.Since(started).String()))
	} else {
		// commit
		s.Relay(fmt.Sprintf("commiting %s", remote.RepoName))
		remote.LatestHash, err = s.helpers.dvcs.Git.CommitAllWithHash(remote.FsPath, "doxa generated backup")
		if err != nil {
			errMsg := fmt.Sprintf("Could not commit: %v", err)
			remote.AddIssue("CommitErr", errMsg, Error)
			return fmt.Errorf("%s: %s", remote.RepoName, errMsg)
		}
		s.Relay(fmt.Sprintf("Commit %s, took %s", remote.RepoName, time.Since(started).String()))
		last := time.Now()
		// push
		s.Relay(fmt.Sprintf("pushing %s", remote.RepoName))
		err = s.helpers.dvcs.Git.Push(remote.FsPath, dvcs.Username, s.helpers.token)
		if err != nil {
			errMsg := fmt.Sprintf("Could not push: %v", err)
			remote.AddIssue("PushErr", errMsg, Error)
			s.Relay(fmt.Sprintf("Error: could not push %s: %v", remote.RepoName, errMsg))
			return fmt.Errorf("%s: %s", remote.RepoName, errMsg)
		}
		s.Relay(fmt.Sprintf("pushed %s, took %s", remote.RepoName, time.Since(last).String()))
		s.Relay(fmt.Sprintf("finished %s took %s", remote.RepoName, time.Since(started).String()))
	}
	//update the hash in the local inventory
	go func() {
		entName := remote.RepoName
		if remote.DbPath != "" {
			entName = fmt.Sprintf("%s%s%s", config.ResourcesDir, kvs.SegmentDelimiter, remote.DbPath)
		}
		catch := s.catMan.UpdateVersionHash(entName, remote.LatestHash)
		if catch != nil {
			remote.AddIssue("InventoryUpdateErr", err.Error(), Error)
			err := fmt.Sprintf("error while updating version hash in inventory: %v", err)
			s.Relay(err)
			doxlog.Errorf(err)
		}
	}()
	remote.LastSynced = time.Now()
	remote.NumSyncsSinceStartup++
	doxlog.Infof(fmt.Sprintf("Finished syncing %s", remote.FsPath))
	return nil
}

func (s *ReadWriteSyncer) syncAuto(initialWait time.Duration) {
	defer (*s.cancelAutoSync)() // call cancel when done to release resources
	duration := 10 * time.Minute
	s.autoMux.Lock()
	s.AutoOn = true
	s.helpers.RelayMuted = true
	s.autoMux.Unlock()

	defer func() {
		s.autoMux.Lock()
		s.helpers.RelayMuted = false
		s.AutoOn = false
		s.autoMux.Unlock()
	}()

	time.Sleep(initialWait)
	for {
		select {
		case <-(*s.ctx).Done():
			// The context was cancelled
			doxlog.Infof("auto %s cancelled", s.Name)
			return
		default:
			// Continue
		}

		s.autoMux.Lock()
		if !s.OkToSync() {
			s.autoMux.Unlock()
			if s.Debug {
				doxlog.Debugf("auto %s is sleeping for %v minutes", s.Name, duration*time.Minute)
			}
			time.Sleep(duration)
			continue
		}
		s.autoMux.Unlock()

		err := s.SyncAll()
		if err != nil {
			doxlog.Error(fmt.Sprintf("auto %s err: %v", s.Name, err.Error()))
			return
		}
		time.Sleep(duration)
	}
}

func (s *ReadWriteSyncer) computeUrl(name, prefix string) string {
	return "https://gitlab.com/" + path.Join(s.helpers.paths.Project, prefix, name)
}

// PopulateRwMap populates the remote meta map, excluding subscription repositories
func (s *ReadWriteSyncer) PopulateRwMap() {
	omit := s.catMan.Subscriptions()
	s.omitMap = make(map[string]struct{})
	for _, ent := range omit {
		if ent == nil || ent.Name == "" {
			doxlog.Errorf("nil or empty subscription name")
			continue
		}
		s.omitMap[ent.Name] = struct{}{}
	}
	cat := s.catMan.GetPrimarySubscriptionCatalog()
	if cat != nil {
		s.omitMap[cat.Name] = struct{}{}
	}
	// add non-database repos to map
	assetsMeta := NewRemoteMeta(config.DoxaPaths.UrlPaths.Assets, ReadWrite)
	templatesMeta := NewRemoteMeta(config.DoxaPaths.UrlPaths.PrimaryTemplates, ReadWrite)
	s.rmm.putToMap(assetsMeta)
	s.rmm.putToMap(templatesMeta)
	s.addRemoteToMap(config.DoxaPaths.UrlPaths.ResourcesLtx)
	s.addRemoteToMap(config.DoxaPaths.UrlPaths.ResourcesMedia)
	s.addRemoteToMap(config.DoxaPaths.UrlPaths.ResourcesConfig)
}

func (s *ReadWriteSyncer) addRemoteToMap(resourcesSubgroup *config.RemoteMeta) {
	libKp := kvs.NewKeyPath()
	err := libKp.ParsePath(resourcesSubgroup.DbPath)
	if err != nil {
		return
	}
	libOpts, err := s.helpers.liturgicalDb.Db.DirNames(*libKp)
	if err != nil {
		s.Relay(fmt.Sprintf("Error: could not add remotes in %s: %v", resourcesSubgroup.RepoName, err))
	}
	for _, lib := range libOpts {
		if lib == "" {
			continue
		}
		if _, pleaseOmit := s.omitMap[lib]; !pleaseOmit {
			meta := NewRemoteMeta(resourcesSubgroup.GetRemoteMeta(lib), ReadWrite)
			s.rmm.putToMap(meta)
		}
	}
}

const relayPrefixRW = "SRW"

func (s *ReadWriteSyncer) SetMsgChannel(ch chan string) {
	s.msgChan = ch
}
func (s *ReadWriteSyncer) Relay(msg string) {
	if s.helpers.RelayMuted {
		return
	}
	if s.helpers.RelayUsesStandardOut {
		fmt.Printf("%s: %s\n", relayPrefixRW, msg)
		return
	}
	if s.msgChan != nil {
		s.msgChan <- fmt.Sprintf("%s: %s", relayPrefixRW, msg)
	}
}
