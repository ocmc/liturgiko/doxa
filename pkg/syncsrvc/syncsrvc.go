// Package syncsrvc Synchronization Service
package syncsrvc

import (
	"fmt"
	"github.com/asaskevich/EventBus"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/dvcs"
	"github.com/liturgiko/doxa/pkg/keyring"
	"github.com/liturgiko/doxa/pkg/kvsw"
	"github.com/liturgiko/doxa/pkg/sysManager"
	"github.com/liturgiko/doxa/pkg/utils/netStatus"
	"net/url"
	"strings"
	"sync"
	"time"
)

const (
	ReadOnlyOfflineWaitDuration  = time.Minute * 5
	ReadOnlyOnlineWaitDuration   = time.Minute * 15
	ReadWriteOfflineWaitDuration = time.Minute * 5
	ReadWriteOnlineWaitDuration  = time.Minute * 15
)
const ReadyOnlyName = "roSync"
const ReadyWriteName = "rwSync"

type ActionType int

const (
	CloneToFs    ActionType = iota // clone to fs only.
	CloneToDb                      // clone into memory then stream into db loader.
	DownloadToFs                   // Download specific file to fs.
)

type RemoteType int

const (
	ReadOnly RemoteType = iota
	ReadWrite
)

type SyncerHelpers struct {
	ds                   *kvsw.Wrapper // data store
	dvcs                 *dvcs.DVCS    // distributed version control
	GitlabApi            string
	internetAvailable    bool
	keyring              *keyring.KeyRing // because must be rebuilt after imports into db
	msgChan              chan string      // used by method relay to send messages to end user
	offlineSimulation    bool
	paths                *config.Paths
	pm                   *kvs.PropertyManager
	RelayMuted           bool // if true, relay is silenced
	RelayUsesStandardOut bool
	sysDs                *kvs.KVS
	liturgicalDb         *kvs.KVS
	Bus                  EventBus.Bus
	token                string // gitlab token for api calls
	BuilderMutex         *sync.Mutex
	SysManager           *sysManager.SysManager
}
type Manager struct {
	helpers  *SyncerHelpers
	RoSyncer SyncerInterface // Read-Only syncer
	RwSyncer SyncerInterface // Read-Write syncer
}

func (m *Manager) SetOfflineSimulation(to bool) {
	m.helpers.offlineSimulation = to
}

func (m *Manager) SetKeyring(kr *keyring.KeyRing) {
	m.helpers.keyring = kr
}

// SyncerInterface key parameter can be Url, DbPath, or FsPath
type SyncerInterface interface {
	// AutoSyncOn indicates whether the auto sync loop is running.
	AutoSyncOn() bool
	// Init ensures that the specified remote has a local and vice versa
	Init(key string) error
	// InitAll calls Init for each instance of RemoteMeta in the map.
	InitAll() error
	// OkToSync indicates whether all conditions have been met for synchronizations to occur
	OkToSync() bool
	SetDebug(to bool)
	SetMsgChannel(ch chan string)
	// Sync does a one-time sync of the specified remote/ local
	Sync(key string) error
	// SyncAll calls Sync for each instance of RemoteMeta in the map, then terminates.
	SyncAll() error
	// SyncAutoOn  waits initialDelay, then starts a go routine loop to periodically call SyncAll in a loop.
	SyncAutoOn(initialDelay time.Duration) error
	SyncAutoOff() error
	Status() string
	// PutToMap is used both to add an instance of RemoteMeta and to update an existing instance.
	PutToMap(remote *RemoteMeta)
	DeleteFromMap(key string)
	GetAllFromMap() []*RemoteMeta
	GetFromMap(key string) (remote *RemoteMeta, ok bool)
}

type ManagerParms struct {
	Ds           *kvsw.Wrapper    // data store
	Dvcs         *dvcs.DVCS       // distributed version control
	Keyring      *keyring.KeyRing // because must be rebuilt after imports into db
	GitlabApi    string
	MsgChan      chan string // used to communicate back real time to user to report progress, issues, etc.
	Paths        *config.Paths
	Pm           *kvs.PropertyManager
	Token        string // gitlab token for api calls
	RoAutoSyncOn bool
	RwAutoSyncOn bool
	SysDs        *kvs.KVS
	LtxDs        *kvs.KVS
	Bus          EventBus.Bus
	BuilderMutex *sync.Mutex
	SysMan       *sysManager.SysManager
}

// NewManager initializes and returns a pointer to Sync Manager, and starts a go routine to check the status of the Internet
func NewManager(parms *ManagerParms) *Manager {
	m := new(Manager)
	m.helpers = new(SyncerHelpers)
	m.helpers.ds = parms.Ds
	m.helpers.dvcs = parms.Dvcs
	m.helpers.keyring = parms.Keyring
	m.helpers.msgChan = parms.MsgChan
	m.helpers.paths = parms.Paths
	m.helpers.pm = parms.Pm
	m.helpers.sysDs = parms.SysDs
	m.helpers.liturgicalDb = parms.LtxDs
	m.helpers.token = parms.Token
	m.helpers.Bus = parms.Bus
	m.helpers.SysManager = parms.SysMan
	m.RoSyncer = NewReadOnlySyncer(m.helpers, parms.RoAutoSyncOn)
	m.RwSyncer = NewReadWriteSyncer(m.helpers, parms.RwAutoSyncOn)
	m.helpers.internetAvailable = netStatus.CheckInternetStatus()
	m.helpers.Bus.Subscribe("net:available", func(verily bool) {
		m.helpers.internetAvailable = verily
	})
	m.helpers.BuilderMutex = parms.BuilderMutex
	m.helpers.Bus.Subscribe("gitlab:tokenSet", func(token string) {
		m.helpers.token = token
	})
	m.helpers.Bus.Subscribe("gitlab:tokenRevoked", func() {
		m.helpers.token = ""
		if m.helpers.dvcs != nil && m.helpers.dvcs.Gitlab != nil {
			m.helpers.dvcs.Gitlab.RemoteToken = ""
		}
	})
	m.helpers.GitlabApi = parms.GitlabApi
	return m
}

func (h *SyncerHelpers) InternetAvailable() bool {
	return h.internetAvailable
}

type IssueType int

const (
	Warning IssueType = iota
	Error
)

type Issue struct {
	Code            string
	Desc            string
	FirstOccurrence time.Time
	LastOccurrence  time.Time
	Level           IssueType
	Count           int
}

func (i *Issue) String() string {
	var lev string
	switch i.Level {
	case Warning:
		lev = "warning"
	case Error:
		lev = "error"
	default:
		lev = "unknown"
	}
	first := i.FirstOccurrence.Format(time.RFC3339)
	last := i.FirstOccurrence.Format(time.RFC3339)
	//Error: PermDenied - not allowed (occurred: 9 times between 2024-03-01T15:43Z07:00 and 2024-03-01T17:32Z07:00)
	return fmt.Sprintf("%s: %s - %s (occurred: %d times between %s and %s)", lev, i.Code, i.Desc, i.Count, first, last)
}

func NewIssue(code, description string, level IssueType) *Issue {
	i := new(Issue)
	i.FirstOccurrence = time.Now()
	i.LastOccurrence = i.FirstOccurrence
	i.Code = code
	i.Desc = description
	i.Level = level
	i.Count = 1
	return i
}

type RemoteMeta struct {
	LatestHash           string
	Url                  string // gitlab url, e.g. https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_dedes
	UrlSubGroupPath      string
	Cloned               bool   // set to true if was just cloned.  If so, do not pull.
	DbPath               string // doxa db path, e.g. ltx/en_us_dedes
	FsClonePath          string // file system path to clone into, e.g. ~/doxa/projects/doxa-eac/subscriptions/doxa-seraphimdedes/resources/ltx
	FsPath               string // file system path to dir with .git, e.g. ~/doxa/projects/doxa-eac/subscriptions/doxa-seraphimdedes/resources/ltx/en_us_dedes
	GroupName            string // same as doxa project name
	RepoName             string // aka Gitlab Project.
	Type                 RemoteType
	ActionType           ActionType // e.g. Clone to FS, Clone to DB
	Downloads            []*DownloadFileInfo
	LastSynced           time.Time
	NumSyncsSinceStartup int
	Issues               map[string]*Issue
}

//func NewRemoteMeta(theUrl, dbPath, fsPath string, remoteType RemoteType) *RemoteMeta {
//	r := new(RemoteMeta)
//	r.Issues = make(map[string]*Issue)
//	r.Type = remoteType
//	r.Url = theUrl
//	r.GroupName, r.UrlSubGroupPath, r.RepoName = r.ParseUrl()
//	r.DbPath = dbPath
//	if dbPath == "" && strings.HasPrefix(r.UrlSubGroupPath, "resources") {
//		r.DbPath = path.Join(r.UrlSubGroupPath, r.RepoName)[len("resources")+1:]
//	}
//	r.FsPath = fsPath
//	return r
//}

func NewRemoteMeta(configMeta *config.RemoteMeta, remoteType RemoteType) *RemoteMeta {
	r := new(RemoteMeta)
	r.Issues = make(map[string]*Issue)
	r.Type = remoteType
	r.GroupName = configMeta.GroupName
	r.Url = configMeta.Https()
	r.UrlSubGroupPath = configMeta.UrlSubGroupPath
	r.RepoName = configMeta.RepoName
	r.FsPath = configMeta.FsPath
	r.DbPath = configMeta.DbPath
	return r
}
func (r *RemoteMeta) ToCloneUrl() (cloneUr string) {
	cloneUr = r.Url
	if !strings.HasPrefix(cloneUr, "https://") {
		cloneUr = "https://" + cloneUr
	}
	if !strings.HasSuffix(cloneUr, ".git") {
		cloneUr = cloneUr + ".git"
	}
	return
}
func (r *RemoteMeta) ParseUrl() (groupName, subGroupPath, repoName string) {
	thePath, err := removeDomain(r.Url)
	if err != nil {
		doxlog.Errorf(err.Error())
		return
	}
	if strings.HasPrefix(thePath, kvs.SegmentDelimiter) {
		thePath = thePath[1:]
	}
	kp := kvs.NewKeyPath()
	err = kp.ParsePath(thePath)
	if err != nil {
		doxlog.Errorf(err.Error())
		return
	}
	if kp.Dirs.Size() < 2 {
		doxlog.Errorf("Invalid url: %s", r.Url)
	}
	groupName = kp.Dirs.First()
	if kp.Dirs.Size() == 3 {
		subGroupPath = kp.Dirs.Get(1)
	} else if kp.Dirs.Size() > 3 {
		subGroupPath = kp.Dirs.SubPath(1, kp.Dirs.Size()-1, "/")
	}
	repoName = kp.Dirs.Last()
	if strings.HasSuffix(repoName, ".git") {
		repoName = repoName[:len(repoName)-4]
	}
	return
}
func removeDomain(urlString string) (string, error) {
	parsedURL, err := url.Parse(urlString)
	if err != nil {
		return "", err
	}

	return parsedURL.Path + parsedURL.RawQuery + parsedURL.Fragment, nil
}
func (r *RemoteMeta) IsDbLibrary() bool {
	return r.DbPath != ""
}
func (r *RemoteMeta) HasIssues() bool {
	return len(r.Issues) > 0
}

func (r *RemoteMeta) HasIssuesOfType(t IssueType) bool {
	for _, v := range r.Issues {
		if v.Level == t {
			return true
		}
	}
	return false
}

func (r *RemoteMeta) IssuesOfType(t IssueType) (matching []*Issue) {
	for _, v := range r.Issues {
		if v.Level == t {
			matching = append(matching, v)
		}
	}
	return matching
}

func (r *RemoteMeta) NbrIssuesOfType(t IssueType) (count int) {
	for _, v := range r.Issues {
		if v.Level == t {
			count++
		}
	}
	return count
}

func (r *RemoteMeta) IssuesToString() string {
	var sb strings.Builder
	for _, v := range r.Issues {
		sb.WriteString(v.String())
		sb.WriteString("\n")
	}
	return sb.String()
}

func (r *RemoteMeta) AddIssue(code, description string, level IssueType) {
	if issue, exists := r.Issues[code]; exists {
		issue.LastOccurrence = time.Now()
		issue.Count++
		return
	}
	if r.Issues == nil {
		r.Issues = make(map[string]*Issue)
	}
	r.Issues[code] = NewIssue(code, description, level)
}

type RemoteMetaManager struct {
	mapByUrl    sync.Map
	mapByFsPath sync.Map
	mapByDbPath sync.Map
}

func (rm *RemoteMetaManager) syncMapToSlice() []*RemoteMeta {
	var remotes []*RemoteMeta
	rm.mapByUrl.Range(func(key, value interface{}) bool {
		remote := value.(*RemoteMeta)
		remotes = append(remotes, remote)
		return true
	})
	return remotes
}

func (rm *RemoteMetaManager) putToMap(remote *RemoteMeta) {
	rm.mapByUrl.Store(remote.Url, remote)
	if remote.IsDbLibrary() {
		rm.mapByDbPath.Store(remote.DbPath, remote)
	}
	rm.mapByFsPath.Store(remote.FsPath, remote)
}

func (rm *RemoteMetaManager) deleteFromMap(key string) {
	if r, ok := rm.getFromMap(key); ok {
		rm.mapByDbPath.Delete(r.DbPath)
		rm.mapByUrl.Delete(r.Url)
		rm.mapByFsPath.Delete(r.FsPath)
	}
}
func (rm *RemoteMetaManager) getFromMap(key string) (remote *RemoteMeta, ok bool) {
	var v interface{}
	if v, ok = rm.mapByUrl.Load(key); ok {
		return v.(*RemoteMeta), true
	}
	if v, ok = rm.mapByUrl.Load(strings.TrimPrefix(strings.TrimSuffix(key, ".git"), "https://")); ok {
		return v.(*RemoteMeta), true
	}
	if v, ok = rm.mapByDbPath.Load(key); ok {
		return v.(*RemoteMeta), true
	}
	if v, ok = rm.mapByFsPath.Load(key); ok {
		return v.(*RemoteMeta), true
	}
	return nil, false
}

func (rm *RemoteMetaManager) numRemotesWithIssues() (warningsOnly, errs int) {
	for _, remote := range rm.syncMapToSlice() {
		if remote.HasIssuesOfType(Error) {
			errs++
		} else if remote.HasIssuesOfType(Warning) {
			warningsOnly++
		}
	}
	return errs, warningsOnly
}

// dumpAllErrors is for debug purposes
func (rm *RemoteMetaManager) dumpAllErrors() {
	fmt.Println("--BEGIN RMM ERROR DUMP--")
	rm.mapByUrl.Range(func(key, value any) bool {
		corrected := value.(*RemoteMeta)
		if corrected == nil {
			fmt.Printf("FAILED TO PROCESS '%s': invalid interface type\n", key)
			return true
		}
		if corrected.Issues == nil {
			fmt.Printf("No issues in '%s'\n", key)
			return true
		}
		fmt.Printf("%d issues in '%s':\n", len(corrected.Issues), key)
		for _, err := range corrected.Issues {
			fmt.Println(err.String())
		}
		return true
	})
	fmt.Println("--END RMM ERROR DUMP--")
}
