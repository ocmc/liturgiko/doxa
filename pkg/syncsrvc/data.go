package syncsrvc

import (
	"strings"
)

func toFsPathFrom(thePath string, start, splitter string) string {
	parts := strings.Split(thePath, splitter)
	var found bool
	for i, part := range parts {
		if part == start {
			found = true
		}
		if !found {
			continue
		}
		return strings.Join(parts[i:], "/")
	}
	return thePath
}

// roUrls can be used for test purposes
var roUrls = []string{"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_redirects_goarch.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/en_uk_kjv.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/en_uk_ware.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_aana.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_acook.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_andronache.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_barrett.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_carroll.gitv",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_dedes.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_duvall.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_eob.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_goa.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_goadedes.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_goarch.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_holycross.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_houpos.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_mlgs.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_net.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_nkjv.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_oca.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_public.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_roumas.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_rsv.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_saas.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_unknown.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/gr_gr_cog.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/gr_redirects_goarch.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/gr_us_goa.git",
	"https: //gitlab.com/doxa-seraphimdedes/resources/ltx/gr_us_holycross.git",
	"https: //gitlab.com/doxa-seraphimdedes/templates.git",
}
