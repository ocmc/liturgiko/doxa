// Package mappers provides pipelines for to map things to another form
package mappers

import (
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/ages/ares"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/inOut"
	"github.com/liturgiko/doxa/pkg/models"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"time"
)

// Lp2Lt converts a stream of LineParts into Ltx structs
func Lp2Lt(in <-chan *ares.LineParts) <-chan *models.Ltx {
	out := make(chan *models.Ltx)
	go func() {
		for lineParts := range in {
			var ltx models.Ltx
			ltx.ID = ltstring.ToId(
				lineParts.Language,
				lineParts.Country,
				lineParts.Realm,
				lineParts.Topic,
				lineParts.Key,
			)
			ltx.Library = ltstring.ToDomain(lineParts.Language, lineParts.Country, lineParts.Realm)
			if ltx.Library == "__" {
				doxlog.Errorf("invalid library %s from %v", ltx.Library, lineParts)
				continue
			}
			ltx.Topic = lineParts.Topic
			ltx.Key = lineParts.Key

			if lineParts.HasValue {
				ltx.Value = lineParts.Value
			}
			if lineParts.HasComment {
				ltx.Comment = lineParts.Comment
			}
			if lineParts.IsRedirect {
				ltx.Redirect = lineParts.Redirect
			}
			timestamp := time.Now().UTC().String()
			ltx.CreatedWhen = timestamp
			ltx.ModifiedWhen = timestamp
			out <- &ltx
		}
		close(out)
	}()
	return out
}

func Tsv2Dbr(in <-chan string) <-chan *kvs.DbR {
	out := make(chan *kvs.DbR)
	delimiter := "\t"
	removeQuotes := false
	go func() {
		for line := range in {
			kp, value, err := inOut.LineParser(line, delimiter, removeQuotes, nil)
			if err != nil {
				return
			}
			dbr := new(kvs.DbR)
			dbr.KP = kp
			dbr.Value = string(value)
			out <- dbr
		}
	}()
	close(out)
	return out
}
