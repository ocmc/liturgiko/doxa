package scraper

import (
	"fmt"
	"testing"
)

func TestNewScrapper(t *testing.T) {
	url := "https://gitlab.com"
	s, err := NewScrapper(url)
	if err != nil {
		t.Error(err)
		return
	}
	if s == nil {
		t.Error(fmt.Errorf("doc nil"))
	}
}
func TestIsAvailable(t *testing.T) {
	url := "https://gitlab.com"
	available := IsAvailable("https://gitlab.com")
	if !available {
		t.Errorf("expected %s to be available, but not", url)
	}
}
