// Package scraper provides a means to scrape web pages
package scraper

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"net/http"
)

type Scraper struct {
	Doc *goquery.Document
	Url string
}

func NewScrapper(url string) (*Scraper, error) {
	s := new(Scraper)
	s.Url = url
	// Request the HTML page.
	res, err := http.Get(s.Url)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		return nil, fmt.Errorf("status code error: %d %s", res.StatusCode, res.Status)
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return nil, fmt.Errorf("doc from reader error: %v", err)
	}
	s.Doc = doc
	return s, nil
}

// IsAvailable returns true if no errors occurred in making an HTTP request to the specified url
func IsAvailable(url string) bool {
	_, err := NewScrapper(url)
	return err == nil
}
