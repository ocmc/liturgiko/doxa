// Package synch provides a means for the user to keep remote git repositories (on Gitlab), local repos, and the database synchronized.
package synch

import (
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/domain"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/dvcs"
	"github.com/liturgiko/doxa/pkg/enums/gitStatuses"
	"github.com/liturgiko/doxa/pkg/enums/licenseTypes"
	"github.com/liturgiko/doxa/pkg/enums/repositoryTypes"
	"github.com/liturgiko/doxa/pkg/enums/syncActionTypes"
	"github.com/liturgiko/doxa/pkg/enums/syncStrategyTypes"
	"github.com/liturgiko/doxa/pkg/library"
	"github.com/liturgiko/doxa/pkg/license"
	"github.com/liturgiko/doxa/pkg/readme"
	"github.com/liturgiko/doxa/pkg/scraper"
	"github.com/liturgiko/doxa/pkg/users"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/repos"
	"github.com/xanzy/go-gitlab"
	"path"
	"sort"
	"strings"
	"sync"
)

const kvsFileExtension = ".dkv"

type Syncher struct {
	dvcs                  *dvcs.DVCS
	externalTemplatesPath string // will be empty if using database templates
	libraries             []string
	mapper                *kvs.KVS
	settingsManager       *config.SettingsManager
	homePath              string
	repoPath              string
	userProfile           *users.Profile
	copyrightMap          map[string]string
	strategy              syncStrategyTypes.SyncStrategy
	actions               []synchActionTypes.SyncActionType
	Simulate              bool // used to simulate sync actions
	msgChan               chan string
}

// NewSyncher creates a syncher object.  If len(templatesPath) > 0, this tells the syncher to use filesystem LML templates instead of database templates.
// Parameters:
//
//	strategy must be a valid code for a synch strategy (see pkg/enums/syncStrategies)
//	   the enum package defines the synch actions for each strategy.
//	repoPath path to the root of the repositories
func NewSyncher(strategy string,
	homePath string,
	repoPath string,
	templatesPath string,
	libraries []string,
	mapper *kvs.KVS,
	dvcs *dvcs.DVCS,
	settingsManager *config.SettingsManager,
	userProfile *users.Profile,
	msgChan chan string) (*Syncher, error) {

	s := new(Syncher)
	s.msgChan = msgChan
	s.strategy = syncStrategyTypes.TypeForString(strategy)
	if s.strategy == syncStrategyTypes.Unknown {
		msg := fmt.Sprintf("error: unknown sync strategy: %s", strategy)
		return nil, fmt.Errorf("%s", msg)
	}
	s.actions = syncStrategyTypes.ActionsForType(s.strategy)
	if len(s.actions) == 0 {
		msg := fmt.Sprintf("error: no actions defined for sync strategy: %s", strategy)
		return nil, fmt.Errorf("%s", msg)
	}
	s.homePath = homePath
	s.libraries = libraries
	s.mapper = mapper
	s.dvcs = dvcs
	s.settingsManager = settingsManager
	s.userProfile = userProfile
	s.repoPath = repoPath
	s.externalTemplatesPath = templatesPath
	s.copyrightMap = make(map[string]string)
	s.relay("go loadCopyrightMap()")
	go func() {
		err := s.loadCopyrightMap()
		if err != nil {
			doxlog.Errorf("error loading copyright map: %v", err)
		}
	}()
	s.relay("burp")
	return s, nil
}

// relay sends on the msgChannel using a standard prefix.
// If msgChannel is nil, it won't attempt to send the message.
func (s *Syncher) relay(msg string) {
	if s.msgChan != nil {
		s.msgChan <- fmt.Sprintf("SM: %s", msg)
	}
}
func (s *Syncher) Synch() error {
	var err error
	if err = s.SynchAssets(); err != nil {
		return err
	}
	if err = s.SynchResources(); err != nil {
		return err
	}
	if err = s.SynchTemplates(); err != nil {
		return err
	}
	if err = s.SynchWebsites(); err != nil {
		return err
	}

	return nil
}
func (s *Syncher) SynchAssets() error {
	if s.Simulate {
		s.relay("simulating Synch Assets")
		return nil
	}
	return nil
}
func (s *Syncher) SynchResources() error {
	if s.Simulate {
		s.relay("simulating Synch Resources")
		return nil
	}
	kp := kvs.NewKeyPath()
	dirs, err := s.mapper.Db.DirNames(*kp)
	if err != nil {
		return err
	}
	for _, dir := range dirs {
		switch dir {
		case "btx", "configs", "ltx", "media":
			{
				groupPath := path.Join(s.dvcs.Gitlab.RemoteRootGroup.Name, dvcs.SubGroupResources, dir)
				err = s.exportLibraryTopics(groupPath, dir)
				if err != nil {
					return err
				}
			}
		case "btx-nnp", "ltx-nnp":
			{
				// ignore.  When a btx or ltx is imported, the -nnp will be created automatically
			}
		case "test":
			{
				// ignore
			}
		default:
			{
				err = s.initGit(s.dvcs.Gitlab.RemoteRootGroup.Name, dir, dir, "")
				if err != nil {
					return err
				}
				err = s.exportFrom(dir)
				if err != nil {
					return err
				}
			}
		}
	}
	return nil
}
func (s *Syncher) SynchTemplates() error {
	if s.Simulate {
		s.relay("simulating Synch Templates")
		return nil
	}
	return nil
}
func (s *Syncher) SynchWebsites() error {
	if s.Simulate {
		s.relay("simulating Synch Websites")
		return nil
	}

	return nil
}
func (s *Syncher) SynchWebsite() error {
	return nil
}

func (s *Syncher) Export() error {
	kp := kvs.NewKeyPath()
	dirs, err := s.mapper.Db.DirNames(*kp)
	if err != nil {
		return err
	}
	for _, dir := range dirs {
		switch dir {
		case "btx", "configs", "ltx", "media":
			{
				groupPath := path.Join(s.dvcs.Gitlab.RemoteRootGroup.Name, dvcs.SubGroupResources, dir)
				err = s.exportLibraryTopics(groupPath, dir)
				if err != nil {
					return err
				}
			}
		case "btx-nnp", "ltx-nnp":
			{
				// ignore.  When a btx or ltx is imported, the -nnp will be created automatically
			}
		case "test":
			{
				// ignore
			}
		default:
			{
				err = s.initGit(s.dvcs.Gitlab.RemoteRootGroup.Name, dir, dir, "")
				if err != nil {
					return err
				}
				err = s.exportFrom(dir)
				if err != nil {
					return err
				}
			}
		}
	}
	return nil
}

// initGit ensures we have both a remote and local git repo
func (s *Syncher) initGit(groupPath, subgroup, name, desc string) error {
	if s.userProfile == nil {
		return fmt.Errorf("user profile has not been set by user")
	}
	// return nil if the user wants to handle git him/herself
	if !s.userProfile.Gitlab.DoxaHandlesGitlab {
		return nil
	}
	// make sure we have gitlab preferences
	if s.userProfile.Gitlab == nil {
		return fmt.Errorf("git and gitlab section of user profile has not been set by user")
	}
	// make sure the user's gitlab group ID and token have been verified
	if !s.userProfile.Gitlab.Verified {
		return fmt.Errorf("gitlab Group ID and Personal Access Token have not been verified")
	}
	// create the remote project
	var project *gitlab.Project
	var err error
	var ok bool
	projectKey := path.Join(groupPath, name)
	if project, ok = s.dvcs.Gitlab.RemoteProjectsMap[projectKey]; !ok {
		project, err = s.dvcs.Gitlab.CreateProject(groupPath, name, desc)
		if err != nil {
			return err
		}
		s.dvcs.Gitlab.RemoteProjectsMap[projectKey] = project
	}
	// create the local repo if it does not exist
	localPath := path.Join(s.repoPath, subgroup, name)
	if s.dvcs.Git.LocalExists(localPath) {
		return nil
	}
	err = s.dvcs.Gitlab.InitLocalRepo(localPath, project.HTTPURLToRepo)
	if err != nil {
		return err
	}
	// initialize a README file
	r := new(readme.ReadMe)
	if !r.Exists(localPath) {
		r.Copyright = s.userProfile.Gitlab.Copyright
		r.Title = fmt.Sprintf("%s %s ", subgroup, name)
		r.RepositoryType = repositoryTypes.CodeForString(subgroup)
		err = r.Write(localPath)
		if err != nil {
			return err
		}
	}
	// initialize a LICENSE file
	l := new(license.License)
	if !l.Exists(localPath) {
		l.Contact = s.userProfile.Gitlab.Contact
		l.Copyright = s.userProfile.Gitlab.Copyright
		l.LicenseType = licenseTypes.TypeForString(s.userProfile.Gitlab.License)
		if l.LicenseType == licenseTypes.PdSj {
			l.Jurisdiction = s.userProfile.Gitlab.Jurisdiction
		}
		l.WorkName = fmt.Sprintf("%s %s ", subgroup, name)
		err = l.Write(localPath)
		if err != nil {
			return err
		}
	}
	// initialize a domain.json file
	// TODO: make use of the copyrightMap
	d := new(domain.Domain)
	if !d.Exists(localPath) {
		var cp string
		var lib *library.Library
		lib, err = library.NewLibrary(name)
		// TODO: userProfile should list the user's libraries
		if err == nil {
			if cp, ok = s.copyrightMap[lib.Realm]; ok {
				d.Copyright = cp
			} else {
				d.Copyright = s.userProfile.Gitlab.Copyright
			}
		} else {
			d.Copyright = s.userProfile.Gitlab.Copyright
		}
		d.License = licenseTypes.TitleForType(licenseTypes.TypeForString(s.userProfile.Gitlab.License))
		d.SetNameDerivableFields(subgroup, name)
		err = d.Write(localPath)
		if err != nil {
			return err
		}
	}
	return nil
}
func (s *Syncher) exportAll() error {
	kp := kvs.NewKeyPath()
	dirPaths, err := s.mapper.Db.DirPaths(*kp)
	if err != nil {
		return err
	}
	for _, dirPath := range dirPaths {
		s.mapper.Db.Export(dirPath, path.Join(s.repoPath, "all", fmt.Sprintf("%s%s", dirPath.ToFileSystemPath(), kvsFileExtension)), kvs.ExportAsLine)
	}
	return nil
}
func (s *Syncher) exportLibraryTopics(groupPath, rootDir string) error {
	var wg sync.WaitGroup
	kpAll := kvs.NewKeyPath()
	kpAll.Dirs.Push(rootDir)
	libraryPaths, err := s.mapper.Db.DirNames(*kpAll)
	if err != nil {
		return err
	}
	for _, libraryPath := range libraryPaths {
		wg.Add(1)
		go s.exportLibraryTopic(groupPath, rootDir, libraryPath)
	}
	go func() {
		wg.Wait()
	}()
	return nil
}

// cleanRepo removes all untracked files
func (s *Syncher) cleanRepo(repoPath string) {

}

// commitRepo commits the files in the repo
func (s *Syncher) commitRepo(repoPath, msg string) {

}

// deleteRepo deletes the local repo.
// parameters:
//
//	remoteAlso - if true, the remote will also be deleted
func (s *Syncher) deleteRepo(remoteAlso bool) {

}

// exportDbDir exports recursively the records in the specified database dir
func (s *Syncher) exportDbDir(repoPath, dbDirPath string) {

}

// fetchRemote fetches the remote doxa-origin for the dir
func (s *Syncher) fetchRemote(repoPath string) {

}

// importRepo loads the database using the *.dkv files in the repo
func (s *Syncher) importRepo(dbDir, repoPath string) {
	// TODO: verify that each ID matches the dbDir.

}

// initRepo initializes a new git repository
// parameters:
//
//	remoteAlso - if true, the remote will also be initialized
func (s *Syncher) initRepo(remoteAlso bool) {

}

// pullRepo pulls from doxa-origin
func (s *Syncher) pullRepo() {

}

// pushRepo pushes the repo to doxa-origin
func (s *Syncher) pushRepo() {

}

// removeDbDir deletes the database directory this repo is for
func (s *Syncher) removeDbDir(repoPath string) {

}

// removeRepoFiles performs a git rm on all dirs and files
func (s *Syncher) removeRepoFiles() {

}

// resetHardRepo performs a fetch, reset hard, and clean on the repo
func (s *Syncher) resetHardRepo() {

}

// exportLibraryTopic exports each record using the specified paths.
// If the user does not want Doxa to handle git and gitlab,
// or Internet connectivity is not available, the records
// are simply exported and nothing else is done.
// If the user wants Doxa to handle git and gitlab and the Internet
// is available, Doxa will pull from the remote, export the records,
// commit them, and push them.  It will then re-import them into
// the database so that it has any remote updates.
func (s *Syncher) exportLibraryTopic(groupPath, rootDir, libraryPath string) {
	s.relay(fmt.Sprintf("Synching %s: %s\n", groupPath, libraryPath))
	gitlabAccessible := scraper.IsAvailable(dvcs.BaseUrl)
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(rootDir)
	kp.Dirs.Push(libraryPath)
	var topicPaths []string
	var err error
	topicPaths, err = s.mapper.Db.DirNames(*kp)
	if err != nil {
		s.relay(fmt.Sprintf("error - %s: %v", kp.Path(), err))
		return
	}
	// initialize remote and local repo if the local does not exist
	repoPath := path.Join(s.repoPath, kp.ToFileSystemPath())
	var importData bool // set to true if we did a git pull before the export
	// if Doxa handles git
	if s.userProfile.Gitlab.DoxaHandlesGitlab && gitlabAccessible {
		if ltfile.FileExists(path.Join(repoPath, ".git")) {
			importData = true
		} else {
			// ...initialize the git repo
			err = s.initGit(groupPath, rootDir, libraryPath, "")
			if err != nil {
				s.relay(fmt.Sprintf("error - %s: %v", kp.Path(), err))
				return
			}
		}
		// pull from remote
		var gitStatus gitStatuses.GitStatus
		gitStatus, err = s.dvcs.Git.PullDoxa(repoPath, false)
		if gitStatus != gitStatuses.Success &&
			gitStatus != gitStatuses.AlreadyUpToDate &&
			gitStatus != gitStatuses.RemoteEmpty {
			if err != nil {
				s.relay(fmt.Sprintf("error - %s: %v", kp.Path(), err))
				return
			}
		}
		importData = true
	}
	fileSeparator := "/"
	for _, topicPath := range topicPaths {
		// export records
		kp.Dirs.Push(topicPath)
		exportFile := fmt.Sprintf("%s%s", topicPath, kvsFileExtension)
		kpAsFileSysPath := kp.ToFileSystemPath()
		kpAsFileSysPath = strings.ReplaceAll(kpAsFileSysPath, ".", fileSeparator)
		exportPath := path.Join(repoPath, kpAsFileSysPath, exportFile)
		err = s.mapper.Db.Export(kp, exportPath, kvs.ExportAsLine)
		if err != nil {
			s.relay(fmt.Sprintf("error - %s: %v", kp.Path(), err))
			return
		}
		kp.Dirs.Pop()
	}
	if s.userProfile.Gitlab.DoxaHandlesGitlab && gitlabAccessible {
		// commit the changes
		err = s.dvcs.Git.CommitAll(repoPath, "doxa synchronization")
		if err != nil {
			s.relay(fmt.Sprintf("error - %s: %v", kp.Path(), err))
			return
		}
		err = s.dvcs.Git.Push(repoPath, "doxaUser", s.dvcs.Gitlab.RemoteToken)
		if err != nil {
			s.relay(fmt.Sprintf("error - %s: %v", kp.Path(), err))
			return
		}
		if importData {
			// TODO: handle import
		}
	}
}
func (s *Syncher) loadCopyrightMap() error {
	kp := kvs.NewKeyPath()
	kp.Dirs.Push("ltx")
	kp.Dirs.Push(".*")
	kp.Dirs.Push("properties")
	kp.KeyParts.Push("version.copyright1")
	matcher := kvs.NewMatcher()
	matcher.KP = kp.Copy()
	matcher = matcher.TopicKeyMatcher()
	matcher.IncludeEmpty = true
	recs, _, err := s.mapper.Db.GetMatching(*matcher)
	if err != nil {
		return err
	}
	for _, rec := range recs {
		var lib *library.Library
		lib, err = library.NewLibrary(rec.KP.Dirs.Get(1))
		if err != nil {
			return err
		}
		switch lib.Realm {
		case "cog":
			rec.Value = "Common Orthodox Liturgical Greek"
		case "kjv":
			rec.Value = "King James Version, copyrighted in UK, public domain elsewhere"
		case "public":
			rec.Value = "various copyright holders, used by permission"
		case "unknown":
			rec.Value = "unknown"
		}
		s.copyrightMap[lib.Realm] = rec.Value
	}
	return nil
}
func (s *Syncher) exportFrom(rootDir string) error {
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(rootDir)
	var thePath string
	if rootDir == "templates" {
		// TODO: if using db templates, this works.  If using file templates, need to copy them.
		thePath = path.Join(s.repoPath, rootDir) // the database export function will create the remaining path segments
	} else {
		thePath = kp.ToFileSystemPath()
		thePath = path.Join(s.repoPath, rootDir, fmt.Sprintf("%s%s", thePath[:len(thePath)-1], kvsFileExtension))
	}
	s.mapper.Db.Export(kp, thePath, kvs.ExportAsLine)
	return nil
}
func (s *Syncher) exportTemplates() error {
	return nil
}
func (s *Syncher) LoadRepoMap() ([]*RepoMeta, error) {
	m := make(map[string]*RepoMeta)
	var err error
	// get filesystem doxa repo meta data
	rms, err := s.RepoDirs(s.repoPath)
	if err != nil {
		return nil, err
	}
	// load map with repo meta data
	for _, rm := range rms {
		m[rm.DbPath] = rm
	}
	// get the database paths for the top level directories
	dbDirs, err := s.DbDirs()
	if err != nil {
		return nil, err
	}
	// merge database paths into map
	for _, dir := range dbDirs {
		if _, ok := m[dir]; !ok {
			rm := new(RepoMeta)
			rm.DbPath = dir
			m[dir] = rm
		}
	}
	// verify each local repo with a remote url has been loaded in the database
	dbPathMap := map[string]string{} // convert dbDirs to map for quick lookup
	for _, v := range dbDirs {
		dbPathMap[v] = v
	}
	for k, v := range m { // iterate the repoMap and verify each path is in dbPaths map
		if len(v.RemoteUrl) > 0 {
			if _, ok := dbPathMap[k]; !ok {
				v.Comment = "repo not loaded into database"
				m[k] = v
			}
		}
	}
	// convert map to a slice as the return value
	var r []*RepoMeta
	for _, v := range m {
		r = append(r, v)
	}
	// sort the slice
	sort.Slice(r, func(i, j int) bool {
		return r[i].DbPath < r[j].DbPath
	})
	return r, nil
}

type RepoMeta struct {
	DbPath            string
	InUserGitlabGroup bool
	FilePath          string
	RemoteUrl         string
	Status            string
	Comment           string
}

func (s *Syncher) DbDirs() ([]string, error) {
	var dbDirs []string
	var dirs []string
	var childDirs []string
	var err error
	kp := kvs.NewKeyPath()
	dirs, err = s.mapper.Db.DirNames(*kp)
	if err != nil {
		return nil, err
	}
	for _, dir := range dirs {
		switch dir {
		case "btx", "ltx", "media":
			{
				kp.Dirs.Push(dir)
				childDirs, err = s.mapper.Db.DirNames(*kp)
				if err != nil {
					return nil, err
				}
				for _, cd := range childDirs {
					//if cd == "use" {
					//	continue
					//}
					kp.Dirs.Push(cd)
					dbDirs = append(dbDirs, kp.Path())
					kp.Dirs.Pop()
				}
				kp.Dirs.Pop()
			}
		case "btx-nnp", "ltx-nnp":
			{
				// ignore.  When a btx or ltx is imported, the -nnp will be created automatically
			}
		case "test":
			{
				// ignore
			}
		default:
			{
				dirs = append(dirs, kp.Path())
			}
		}
	}
	return dbDirs, nil
}
func (s *Syncher) RepoDirs(root string) ([]*RepoMeta, error) {
	var rms []*RepoMeta
	var childRms []*RepoMeta
	var err error
	entries, err := ltfile.EntriesInDir(root)
	if err != nil {
		return nil, err
	}
	for _, entry := range entries {
		if entry.IsDir() {
			dirName := path.Base(entry.Name())
			switch dirName {
			case "btx", "ltx", "media":
				{
					childRms, err = s.RepoDirs(path.Join(path.Join(root, entry.Name())))
					if err != nil {
						return nil, err
					}
					rms = append(rms, childRms...)
				}
			case "btx-nnp", "ltx-nnp":
				{
					// ignore.  When a btx or ltx is imported, the -nnp will be created automatically
				}
			case "test":
				{
					// ignore
				}
			default:
				{
					rm := new(RepoMeta)
					rm.FilePath = path.Join(root, entry.Name())
					rm.DbPath = strings.Replace(rm.FilePath, s.repoPath, "", 1)[1:]
					gitPath := path.Join(rm.FilePath, ".git")
					if ltfile.DirExists(gitPath) {
						var url string
						url, err = repos.RemoteUrl(rm.FilePath)
						if err != nil {
							switch err.(type) {
							case *repos.GitConfigMissingError:
								rm.Comment = "missing config in local git repo"
							case *repos.GitRemoteMissingError:
								rm.Comment = "missing remote origin in config"
							case *repos.GitRemoteUrlMissingError:
								rm.Comment = "missing URL for remote origin in config"
							case *repos.GitRepoMissingError:
								rm.Comment = "missing local git repo"
							default:
								rm.Comment = fmt.Sprintf("error - %v", err)
							}
							continue
						}
						rm.RemoteUrl = url
						if s.userProfile != nil {
							if s.userProfile.Gitlab != nil {
								if s.userProfile.Gitlab.Verified {
									var groupPrefixUrl string
									groupPrefixUrl, err = dvcs.GroupUrlToClonePathPrefix(s.userProfile.Gitlab.GitlabRootGroup.WebURL)
									if err == nil {
										rm.InUserGitlabGroup = strings.HasPrefix(rm.RemoteUrl, groupPrefixUrl)
									}
								}
							}
						}
					} else {
						rm.Comment = "missing local git repo"
					}
					// check to see if remote has more recent commits
					if len(rm.RemoteUrl) > 0 {
						git := dvcs.NewGitClient(rm.FilePath)
						var diffStatus *dvcs.DiffStatus
						diffStatus, err = git.LocalAndOriginDiffer()
						if err != nil {
							rm.Status = "unknown"
						} else {
							if diffStatus.Differ {
								if diffStatus.Rev1AheadRev2 {
									rm.Status = "remote ahead of local"
								} else {
									rm.Status = "local ahead of remote"
								}
							} else {
								rm.Status = "up-to-date"
							}
						}
					}
					rms = append(rms, rm)
				}
			}
		}
	}
	return rms, nil
}
