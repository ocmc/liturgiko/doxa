package gitlocal

import "github.com/liturgiko/doxa/pkg/dvcs"

type Manager struct {
	Git         *dvcs.Git
	RemoteToken string
}

func NewManager(localRepoPath, remoteToken string) *Manager {
	m := new(Manager)
	m.Git = new(dvcs.Git)
	m.Git.LocalRepoPath = localRepoPath
	m.RemoteToken = remoteToken
	return m
}
