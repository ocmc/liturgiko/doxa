package ed

import (
	"fmt"
	"testing"
)

func TestMatch(t *testing.T) {
	l1 := []string{"Candidate_Sponsor", "Chanter", "ChanterOrReader", "Choir", "ChoirL", "ChoirR", "Clergy", "ClergyAndPeople", "Deacon", "Hierarch", "HierarchOrPriest", "People", "Priest", "PriestOrDeacon", "Reader", "clChoirL", "clChoirR"}
	l2 := []string{"ac.Archbishop", "ac.Archdeacon", "ac.Bishop_Elect", "ac.Candidate_Sponsor", "ac.Chanter", "ac.ChanterOrReader", "ac.Chanters", "ac.Choir", "ac.ChoirL", "ac.ChoirR", "ac.Clergy", "ac.ClergyAndPeople", "ac.Deacon", "ac.Deacon1", "ac.Deacon2", "ac.Hierarch", "ac.HierarchOrPriest", "ac.Hierarchs", "ac.Metropolitan", "ac.Metropolitans", "ac.Patriarch", "ac.People", "ac.Priest", "ac.Priest1", "ac.Priest2", "ac.PriestOrDeacon", "ac.Priests", "ac.Reader", "ac.Respondent", "ac.Superior", "ac.SuperiorOrReader", "ac.clChoirL", "ac.clChoirR"}
	tuples, err := Match(l1, l2)
	if err != nil {
		t.Error(err)
		return
	}
	if tuples == nil {
		t.Error("tuples is nil")
	}
	for _, tuple := range tuples {
		fmt.Printf("%f %s -> %s\n", tuple.Similarity, tuple.S1, tuple.S2)
	}
}
