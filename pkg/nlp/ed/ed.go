// Package ed provides functions for computing the edit distance between two strings.
// This is useful for detecting redundancy or accidental variance.
package ed

import (
	"github.com/adrg/strutil"
	"github.com/adrg/strutil/metrics"
	"github.com/hbollon/go-edlib"
)

type Tuple struct {
	S1         string
	S2         string
	Similarity float64
}

func (t *Tuple) Changed() bool {
	return t.S1 != t.S2
}

// Match does a comparison of the slices and for each l1 string
// attempts to match it with an l2 string.
// The goal is to automatically detect renames of keys.
// The two slices should be from the same ltx topic.
func Match(l1, l2 []string) (tuples []*Tuple, err error) {
	for _, s1 := range l1 {
		t := new(Tuple)
		t.S1 = s1
		t.S2, err = edlib.FuzzySearch(s1, l2, edlib.Levenshtein)
		if err != nil {
			return nil, err
		}
		tuples = append(tuples, t)
	}
	return tuples, nil
}
func Similarity(s1, s2 string) (res float32, err error) {
	return edlib.StringsSimilarity("string1", "string2", edlib.JaroWinkler)
}
func Similarity2(s1, s2 string) float64 {
	return strutil.Similarity(s1, s2, metrics.NewJaroWinkler())
}
