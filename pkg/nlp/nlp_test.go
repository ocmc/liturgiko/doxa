package nlp

import (
	"testing"
	"unicode"
)

func TestFilterString(t *testing.T) {
	text := "ὁ Θεὸς ἡμῶν, * σῶσον τὸν λαόν σου."
	filter := FilterGreekString(text, unicode.Greek)
	expected := "ὁ Θεὸς ἡμῶν, σῶσον τὸν λαόν σου."
	if filter != expected {
		t.Errorf("FilterGreekString() = %s | want %s", filter, expected)
	}
}
func TestIsAncientGreekNumber(t *testing.T) {
	if !IsAncientGreekNumber('𐅀') {
		t.Errorf("IsAncientGreekNumber() = false; want true")
	}
	if IsAncientGreekNumber('A') {
		t.Errorf("IsAncientGreekNumber() = true; want false")
	}
}
