package ud

import (
	"encoding/json"
	"fmt"
	conllu "github.com/nuvi/go-conllu"
	"io"
	"log"
	"net/http"
	"net/url"
	"strings"
)

// see http://lindat.mff.cuni.cz/services/udpipe/api-reference.php#process

const baseURL = "lindat.mff.cuni.cz/services/udpipe/api/"

var Services = []string{"models", "process"}
var Models = []string{"ancient_greek-proiel-ud-2.12-230717", "ancient_greek-perseus-ud-2.12-230717"}

const maxBytes = 4 * 1024 * 1024 // 4 MB in bytes

type apiResponse struct {
	Model            string   `json:"model"`
	Acknowledgements []string `json:"acknowledgements"`
	Result           string   `json:"result"`
}

type Pipe struct {
}

func (p *Pipe) Models() []string {
	return nil
}
func (p *Pipe) Process(id, text string, model string) (*Analysis, error) {
	if len(text) >= maxBytes {
		return nil, fmt.Errorf("text size exceeds the api 4mb limits")
	}
	fmt.Printf("\t%s\n", id)
	analysis := new(Analysis)
	analysis.ID = id
	analysis.Model = model
	processUrl := fmt.Sprintf("https://%s%s", baseURL, Services[1])
	// Construct the URL with query parameters
	queryParams := url.Values{}
	queryParams.Add("model", model)
	queryParams.Add("tokenizer", "")
	queryParams.Add("tagger", "")
	queryParams.Add("parser", "")
	queryParams.Add("data", text)
	// Encode the parameters
	encodedParams := queryParams.Encode()
	finalUrl := strings.Join([]string{processUrl, encodedParams}, "?")

	// Make the GET request
	resp, err := http.Get(finalUrl)
	if err != nil {
		return nil, fmt.Errorf("error making the GET request: %v", err)
	}
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("GET request failed with status code: %d %s", resp.StatusCode, resp.Status)
	}
	defer func(Body io.ReadCloser) {
		err = Body.Close()
		if err != nil {
			// TODO
		}
	}(resp.Body)

	// Read and handle the response
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatalf("Error reading the response body: %v", err)
	}
	var result apiResponse
	// Unmarshal the JSON string into the Person struct
	err = json.Unmarshal([]byte(body), &result)
	if err != nil {
		fmt.Println("Error:", err)
		return nil, err
	}
	analysis.Acknowledgements = result.Acknowledgements
	var sentences []conllu.Sentence
	sentences, err = conllu.Parse(strings.NewReader(result.Result))
	if err != nil {
		return nil, err
	}

	analysis.Sentences = make([]*Sentence, len(sentences))
	for i, s := range sentences {
		analysis.Sentences[i] = &Sentence{
			Tokens: s.Tokens,
			Nodes:  make(Nodes, len(s.Tokens)),
		}
		for j, t := range s.Tokens {
			analysis.Sentences[i].Nodes[j] = TreeOrder{
				HeadIndex:   int(t.Head),
				TokenIndex:  int(t.ID),
				IndentLevel: 0,
				Token:       t.Form,
			}
		}
	}
	for _, s := range analysis.Sentences {
		s.Sort()
		s.CreateMetaData()
	}
	return analysis, err
}
