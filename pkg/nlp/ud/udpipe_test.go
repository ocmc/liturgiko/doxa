package ud

import (
	"fmt"
	"testing"
)

func TestPipe_Analyze(t *testing.T) {
	p := new(Pipe)
	text := "ὁ Θεὸς ἡμῶν, σῶσον τὸν λαόν σου."
	//text := "Θεὸς σῶσον λαόν"
	//text := "Κύριε ὁ Θεὸς ἡμῶν, οὗ τὸ κράτος ἀνείκαστον καὶ ἡ δόξα ἀκατάληπτος, οὗ τὸ ἔλεος ἀμέτρητον καὶ ἡ φιλανθρωπία ἄφατος. Αὐτὸς Δέσποτα, κατὰ τὴν εὐσπλαχνίαν σου, ἐπίβλεψον ἐφʼ ἡμᾶς καὶ ἐπὶ τὸν ἅγιον οἶκον τοῦτον καὶ ποίησον μεθʼ ἡμῶν, καί τῶν συνευχομένων ἡμῖν, πλούσια τὰ ἐλέη σου καί τοὺς οἰκτιρμούς σου."
	expectedSentenceCount := 1
	analysis, err := p.Process("ltx/gr_gr_cog/actors:test", text, Models[1])
	if err != nil {
		t.Errorf(err.Error())
		return
	}
	if len(analysis.Sentences) != expectedSentenceCount {
		t.Errorf("Expected %d sentence, got %d", expectedSentenceCount, len(analysis.Sentences))
		return
	}
	json, err := analysis.ToJson(true)
	if err != nil {
		t.Errorf(err.Error())
		return
	}
	if len(json) == 0 {
		t.Errorf("Expected non-empty JSON string, got empty string")
	}
	for _, s := range analysis.Sentences {
		tree := s.Tree()
		for _, line := range tree {
			fmt.Println(line)
		}
	}
}
