package ud

import (
	"encoding/json"
	"fmt"
	conllu "github.com/nuvi/go-conllu"
	"strings"
	"time"
)

// Lapi Linguistic API
type Lapi interface {
	Models() []string
	Process(id, text string) (*Analysis, error)
}
type Analysis struct {
	ID               string // doxa db id
	Date             time.Time
	API              string
	Model            string
	Acknowledgements []string
	Sentences        []*Sentence
}

func (a *Analysis) ToJson(formatted bool) (string, error) {
	var bytes []byte
	var err error

	if formatted {
		bytes, err = json.MarshalIndent(a, "", " ")
	} else {
		bytes, err = json.Marshal(a)
	}
	if err != nil {
		return "", fmt.Errorf("failed to serialize Pipe to JSON: %v", err)
	}

	return string(bytes), err
}

type Sentence struct {
	Tokens   []conllu.Token
	Nodes    Nodes
	MetaData *SentenceMetaData
}
type SentenceMetaData struct {
	Tokens           int
	Words            int
	InflectedWordMap map[string]int
	LemmaWordMap     map[string]int
}

func (s *Sentence) CreateMetaData() {
	s.MetaData = new(SentenceMetaData)
	s.MetaData.InflectedWordMap = make(map[string]int)
	s.MetaData.LemmaWordMap = make(map[string]int)
	for _, token := range s.Tokens {
		s.MetaData.Tokens++
		if token.UPOS != "PUNCT" {
			s.MetaData.Words++
			s.MetaData.InflectedWordMap[strings.ToLower(token.Form)]++
			s.MetaData.LemmaWordMap[strings.ToLower(token.Lemma)]++
		}
	}
}

func (s *Sentence) Sort() {
	s.SortByDependency()
}

// SortByDependency sorts the Tokens in a Sentence based on their dependency and calculates indentation.
func (s *Sentence) SortByDependency() {
	var sorted Nodes
	dependencyMap := make(map[int]Nodes)

	// Build a map where each key is a Head ID and the value is a slice of Tokens dependent on that Head.
	for _, node := range s.Nodes {
		dependencyMap[node.HeadIndex] = append(dependencyMap[node.HeadIndex], node)
	}

	// Recursive function to add tokens based on their dependency order and calculate indentation.
	var addByDependency func(headID int, indent int)
	addByDependency = func(headID int, indent int) {
		if dependents, exists := dependencyMap[headID]; exists {
			for _, dep := range dependents {
				dep.IndentLevel = indent
				sorted = append(sorted, dep)
				addByDependency(dep.TokenIndex, indent+1) // Increase indent for dependents.
			}
		}
	}

	// Start with the root token (Head == 0) and build the sorted slice with calculated indentation.
	addByDependency(0, 0)

	// Replace the original slice with the sorted one.
	s.Nodes = sorted
}
func (s *Sentence) TokenString(tokenIndex, indentLevel int) string {
	n := s.Tokens[tokenIndex-1]
	var padding string
	if n.Head != 0 {
		padding = fmt.Sprintf("%s|-- ", strings.Repeat(" ", indentLevel))
	}
	return fmt.Sprintf("%s%s (%s) I:%d H:%d", padding, n.Form, n.Lemma, int(n.ID), int(n.Head))
}

func (s *Sentence) Tree() []string {
	var tree []string
	for _, node := range s.Nodes {
		tree = append(tree, s.TokenString(node.TokenIndex, node.IndentLevel))
	}
	return tree
}

type TreeOrder struct {
	HeadIndex   int
	TokenIndex  int
	IndentLevel int
	Token       string
}
type Nodes []TreeOrder

func (n Nodes) Len() int {
	return len(n)
}

// The cat hit the dog
// 1   2   3   4   5
// 2   3   0   5   3

func (n Nodes) Less(i, j int) bool {
	ni := n[i]
	nj := n[j]
	//"ὁ Θεὸς ἡμῶν, σῶσον τὸν λαόν σου."
	//if nj.HeadIndex == ni.TokenIndex {
	//	return true
	//} // j depends on i

	var l = fmt.Sprintf("a%d%d", ni.HeadIndex, ni.TokenIndex)
	var r = fmt.Sprintf("a%d%d", nj.HeadIndex, nj.TokenIndex)
	fmt.Printf("%s %s < %s  %s ? %v\n", ni.Token, l, nj.Token, r, l < r)
	return l < r
}

func (n Nodes) Swap(i, j int) {
	n[i], n[j] = n[j], n[i]
}
