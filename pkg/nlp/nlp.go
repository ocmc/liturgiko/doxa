// Package nlp provides some basic natural language processing functions.
package nlp

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"github.com/speedata/hyphenation"
	"os"
	"path"
	"strconv"
	"strings"
	"text/scanner"
	"unicode"
)

type NLP struct {
	Frequency                  *Frequency
	HyphenDir                  string
	OrderByFrequencyAscending  bool
	OrderByFrequencyDescending bool
	KeepCase                   bool
	KeepPunctuation            bool
	KeepScansion               bool
	PunctuationChars           string
	ScansionChars              string
}

func NewNLP() *NLP {
	nlp := new(NLP)
	nlp.Frequency = NewFrequency()
	return nlp
}

type Token struct {
	Position string
	Text     string
}
type FrequencyData struct {
	Token          string
	Frequency      int
	FirstFivePaths []string
}
type Frequency struct {
	Map   map[string]*FrequencyData
	Keys  []string
	Total int
}

func NewFrequency() *Frequency {
	f := new(Frequency)
	f.Map = make(map[string]*FrequencyData)
	return f
}

func (f *Frequency) add(token, path string) {
	data := f.Map[token]
	if data == nil {
		data = new(FrequencyData)
		f.Keys = append(f.Keys, token)
	}
	data.Token = token
	data.Frequency += 1
	if len(data.FirstFivePaths) < 5 {
		data.FirstFivePaths = append(data.FirstFivePaths, path)
	}
	f.Map[token] = data
}
func (f *Frequency) FrequencyDataSlice() []*FrequencyData {
	var fd []*FrequencyData
	for _, value := range f.Map {
		fd = append(fd, value)
	}
	return fd
}

// Export writes the frequency data to the specified file
func Export(filepath string, frequencyData []*FrequencyData) error {
	sb := strings.Builder{}
	sb.WriteString(fmt.Sprintf("frequency, word\n"))
	for _, fd := range frequencyData {
		sb.WriteString(fmt.Sprintf("%d,%s\n", fd.Frequency, fd.Token))
	}
	return ltfile.WriteFile(filepath, sb.String())
}

// Count calculate the frequency count for each token in the text
func (n *NLP) Count(source, text string, lang string, includeNumbers, includePunctuation bool, table *unicode.RangeTable) {
	var textToTokenize string
	if includePunctuation {
		textToTokenize = text
	} else {
		textToTokenize = ltstring.RemovePunctuation(text)
	}

	for _, token := range n.Tokens(source, textToTokenize, lang) {
		if table != nil {
			r := []rune(token.Text)
			if !unicode.In(r[0], table) {
				continue
			}
		}
		n.Frequency.Total++
		if includeNumbers {
			n.Frequency.add(token.Text, source)
		} else {
			if _, err := strconv.Atoi(token.Text); err != nil {
				n.Frequency.add(token.Text, source)
			}
		}
	}
}

// FreqSorterDescending sorts frequency data by the frequency count
// descending with a sub-sort on the token.
type FreqSorterDescending []*FrequencyData

func (f FreqSorterDescending) Len() int      { return len(f) }
func (f FreqSorterDescending) Swap(i, j int) { f[i], f[j] = f[j], f[i] }
func (f FreqSorterDescending) Less(i, j int) bool {
	if f[i].Frequency == f[j].Frequency {
		return f[i].Token < f[j].Token
	} else {
		return f[i].Frequency > f[j].Frequency
	}
}

// FreqSorterAscending sorts frequency data by the frequency count
// ascending with a sub-sort on the token.
type FreqSorterAscending []*FrequencyData

func (f FreqSorterAscending) Len() int      { return len(f) }
func (f FreqSorterAscending) Swap(i, j int) { f[i], f[j] = f[j], f[i] }
func (f FreqSorterAscending) Less(i, j int) bool {
	if f[i].Frequency == f[j].Frequency {
		return f[i].Token < f[j].Token
	} else {
		return f[i].Frequency < f[j].Frequency
	}
}

// TokenSorter sorts frequency data by the token
type TokenSorter []*FrequencyData

func (f TokenSorter) Len() int           { return len(f) }
func (f TokenSorter) Swap(i, j int)      { f[i], f[j] = f[j], f[i] }
func (f TokenSorter) Less(i, j int) bool { return f[i].Token < f[j].Token }

// Tokens returns the tokens of the given text.
func (n *NLP) Tokens(source, text string, lang string) []Token {
	// TODO extract the language code from the source ID and remove the lang parameter
	var s scanner.Scanner
	s.Init(strings.NewReader(text))
	s.Filename = source
	var tokens []Token
	var apostrophe = rune('\'')
	s.Whitespace = 1<<'-' | 1<<'\t' | 1<<'\n' | 1<<' ' | 1<<'\'' // treat these as white space
	addApostrophe := false

	for tok := s.Scan(); tok != scanner.EOF; tok = s.Scan() {
		if !n.KeepPunctuation {
			i := strings.Index(n.PunctuationChars, s.TokenText())
			if i > -1 {
				continue
			}
		}
		if !n.KeepScansion {
			i := strings.Index(n.ScansionChars, s.TokenText())
			if i > -1 {
				continue
			}
		}
		var t Token
		t.Position = fmt.Sprintf("%s", s.Position)
		t.Text = s.TokenText()
		if addApostrophe {
			t.Text = "'" + t.Text
			addApostrophe = false
		}
		switch lang {
		case "en":
			if s.Peek() == apostrophe {
				addApostrophe = true
			}
		case "gr":
			if s.Peek() == apostrophe {
				t.Text = t.Text + "'"
			}
		}
		tokens = append(tokens, t)
	}
	return tokens
}

// Hyphenate tokenizes the text and hyphenates each token using patterns from
// https://github.com/hyphenation/tex-hyphen
func (n *NLP) Hyphenate(source, text string, lang string) (string, error) {
	// TODO extract the language code from the source ID and remove the lang parameter
	// TODO Greek hyphenation does not work
	var hyphenatedText []string
	var filename string
	switch lang {
	case "en":
		filename = path.Join(n.HyphenDir, "hyph-en-us.pat.txt")
	default:
		return text, fmt.Errorf("can only hyphentate English")
	}
	r, err := os.Open(ltfile.ToSysPath(filename))
	if err != nil {
		return "", err
	}
	l, err := hyphenation.New(r)
	if err != nil {
		return "", err
	}
	for _, token := range n.Tokens(source, text, lang) {
		indexes := l.Hyphenate(token.Text)
		hyphenatedWord := InsertCharacter(token.Text, "-", indexes)
		hyphenatedText = append(hyphenatedText, hyphenatedWord)
	}
	// convert slice to string, with no space before punctuation
	sb := strings.Builder{}
	for i, w := range hyphenatedText {
		if i == 0 {
			sb.WriteString(w)
		} else {
			if strings.Contains(n.PunctuationChars, w) {
				sb.WriteString(w)
			} else {
				sb.WriteString(" ")
				sb.WriteString(w)
			}
		}
	}
	return sb.String(), nil
}

// DebugHyphenate hyphenates a string using patterns from
// https://github.com/hyphenation/tex-hyphen
// and shows which patterns matched and in which order
func (n *NLP) DebugHyphenate(source, text string, lang string) (string, error) {
	var filename string
	switch lang {
	case "en":
		filename = path.Join(n.HyphenDir, "hyph-en-us.pat.txt")
	default:
		return text, fmt.Errorf("hyphenation only works with English")
	}
	r, err := os.Open(ltfile.ToSysPath(filename))
	if err != nil {
		return "", err
	}
	l, err := hyphenation.New(r)
	if err != nil {
		return "", err
	}
	return l.DebugHyphenate(text), nil
}
func InsertCharacter(source, character string, indexes []int) string {
	if len(indexes) == 0 {
		return source
	}
	sb := strings.Builder{}
	from := 0
	to := 0
	for _, to = range indexes {
		sb.WriteString(source[from:to])
		sb.WriteString(character)
		from = to
	}
	if to < len(source) {
		sb.WriteString(source[to:])
	}
	return sb.String()
}

// FilterGreekString removes any characters from the input string that are not found in the Greek Unicode table.
func FilterGreekString(input string) string {
	var result []rune
	allowedTable := unicode.Greek
	for _, r := range input {
		if unicode.Is(allowedTable, r) ||
			unicode.IsSpace(r) ||
			IsAncientGreekPunctuation(r) ||
			IsAncientGreekNumber(r) {
			result = append(result, r)
		}
	}
	return strings.ReplaceAll(strings.TrimSpace(string(result)), "  ", " ")
}

// Define a set of runes for the specified punctuation characters.
var greekPunctuation = map[rune]bool{
	':':  true, // Colon (U+003A)
	'·':  true, // Raised point (U+0387), equivalent to semi colon
	'…':  true, // Ellipsis (U+2026)
	'\'': true, // Apostrophe (U+0027)
	'«':  true, // Guillemets open (U+00AB)
	'»':  true, // Guillemets close (U+00BB)
	'-':  true, // Hyphen (U+002D)
	';':  true, // Question mark (U+003B) - Standard keyboard English semicolon character
	'!':  true, // Exclamation mark (U+0021)
	',':  true, // Comma (U+002C)
	'(':  true, // Parenthesis open (U+0028)
	')':  true, // Parenthesis close (U+0029)
	'—':  true, // Dash (U+2014)
	'―':  true, // Horizontal bar (U+2015)
	'.':  true, // Full stop (U+002E)
}

// IsAncientGreekPunctuation checks if a rune is a Greek punctuation character.
func IsAncientGreekPunctuation(r rune) bool {
	_, exists := greekPunctuation[r]
	return exists
}

// Define the Unicode range for Ancient Greek Numbers using Range32 to accommodate the higher code points.
var ancientGreekNumbers = &unicode.RangeTable{
	R32: []unicode.Range32{
		{Lo: 0x10140, Hi: 0x1018F, Stride: 1},
	},
}

// IsAncientGreekNumber checks if a rune is an Ancient Greek Number.
func IsAncientGreekNumber(r rune) bool {
	return unicode.Is(ancientGreekNumbers, r)
}
