// Package table provides a means to specify the data for a generic web component that renders the data into an HTML table
package table

import "fmt"

type Data struct {
	Title       string    // title of the table
	Desc        string    // description of the data
	Filter      int       // zero based index of Headings (aka columns) to use for the initial filter.
	FilterValue string    // initialize filter using the given string
	Headings    []string  // text for each heading (column)
	Rows        [][]*Cell // table rows
	Widths      []int     // width for each column as a percent
}

// Cell can be used for a simple value, or to show a hyperlink.
// Just leave Href and Target unset if you do not need them.
type Cell struct {
	Href   string // if the value will be a hyperlink, html anchor
	Target string // e.g. _blank
	Value  string // what shows in the table cell
}

func (d *Data) Validate() []error {
	var errors []error
	if len(d.Headings) != len(d.Widths) {
		errors = append(errors, fmt.Errorf("the number of column headings does not equal the number of column widths"))
	}
	for i, r := range d.Rows {
		if len(r) != len(d.Headings) {
			errors = append(errors, fmt.Errorf("row %d: number of cells does not equal the number of column headings", i))
		}
	}
	width := 0
	for _, w := range d.Widths {
		width += w
	}
	if width != 100 {
		errors = append(errors, fmt.Errorf("sum of column widths does not equal 100"))
	}
	return errors
}

// MakeColumnWidthsEqual sort of does so.
// The last column will be slightly wider if 100 / #headings is not even.
func (d *Data) MakeColumnWidthsEqual() error {
	l := len(d.Headings)
	if l == 0 {
		return fmt.Errorf("column headings must be set before calling func MakeColumnWidthsEqual")
	}
	w := 100 / l
	d.Widths = []int{}
	var t int
	for i := 0; i < l-1; i++ {
		t += w
		d.Widths = append(d.Widths, w)
	}
	d.Widths = append(d.Widths, 100-t)
	return nil
}
