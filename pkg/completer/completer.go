// Package completer provides an array of items for use in an Ace Editor for the Liturgical Markup Language (LML)
package completer

import (
	"encoding/json"
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/css"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/keyring"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"path"
	"sort"
	"strings"
)

type Item struct {
	Value string `json:"value"`
	Score int    `json:"score"`
	Meta  string `json:"meta"`
}

// Completer provides autocomplete values for LML keywords, css classes, topic/keys, and template paths.
type Completer struct {
	Values       []Item
	htmlCssPath  string
	pdfCssPath   string
	templatePath string
	mapper       *kvs.KVS
	useKeyRing   bool
}

func NewCompleter(htmlCssPath string, pdfCssPath string, templatePath string, mapper *kvs.KVS, useKeyRing bool) (*Completer, error) {
	c := new(Completer)
	c.htmlCssPath = htmlCssPath
	c.pdfCssPath = pdfCssPath
	c.templatePath = templatePath
	c.mapper = mapper
	c.useKeyRing = useKeyRing
	err := c.Load()
	if err != nil {
		return nil, err
	}
	return c, nil
}
func (c *Completer) Load() error {
	c.Values = KeyWords()
	err := c.addClassNames(c.htmlCssPath)
	if err != nil {
		return err
	}
	err = c.addClassNames(c.pdfCssPath)
	if err != nil {
		return err
	}
	err = c.addTemplatePaths(c.templatePath)
	if err != nil {
		return err
	}
	err = c.addTopicKeys(c.mapper, c.useKeyRing)
	if err != nil {
		return err
	}
	return nil
}

// addTopicKeys adds the topic-keys found in the keyring to the completer
func (c *Completer) addTopicKeys(mapper *kvs.KVS, useKeyRing bool) error {
	kp := kvs.NewKeyPath()
	if useKeyRing {
		kp.Dirs.Push(keyring.DirName)
	} else {
		kp.Dirs.Push("ltx")
		kp.Dirs.Push("gr_gr_cog")
	}
	keyMap, err := mapper.Db.InverseKeyMap(kp)
	if err != nil {
		return fmt.Errorf("could not get database keys from keyring : %v", err)
	}
	var topicKeys []string
	for _, v := range *keyMap {
		if v != nil && len(v) > 0 {
			// always add topicKey as a sid
			topicKeys = append(topicKeys, v[0].LmlSid())
			// optionally add topicKey as a rid if eligible.
			// if wc comes back as an empty string, it is not eligible to be a rid
			wc := v[0].LmlRid()
			if len(wc) > 0 {
				topicKeys = append(topicKeys, wc)
			}
		}
	}
	sort.Strings(topicKeys)
	for _, tk := range topicKeys {
		var i = Item{tk, 1, "topic:key"}
		c.Values = append(c.Values, i)
	}
	return nil
}

// addClassNames adds all paragraph and span classnames from the specified css file
func (c *Completer) addClassNames(path string) error {
	stylesheet, err := css.NewStyleSheet("completer", path, false)
	classnames, errors := stylesheet.Classnames()
	if err != nil {
		return errors[0]
	}
	if len(classnames) == 0 {
		return fmt.Errorf("%s no classnames", path)
	}
	for _, name := range classnames {
		var i = Item{name, 1, "css class"}
		c.Values = append(c.Values, i)
	}
	return nil
}

// ToJson returns the completer values as a Json byte slice
func (c *Completer) ToJson() string {
	jsonBytes, err := json.Marshal(c.Values)
	if err != nil {
		doxlog.Infof("error converting Completer.Values to json: %v\n", err)
		return ""
	}
	return string(jsonBytes)
}

// addTemplatePaths adds the paths to all LML files in specified directory to the completer
func (c *Completer) addTemplatePaths(dir string) error {
	var files []string
	var keys []string
	var err error
	if files, err = ltfile.FileMatcher(dir, "lml", nil); err != nil {
		doxlog.Errorf("%v", err)
		return err
	}
	theMap := make(map[string]Item)
	// load the map and the keys slice (for subsequent sorting of the map)
	for _, file := range files {
		parts := strings.Split(file, "/templates/")
		// only add templates that are blocks
		_, filename := path.Split(file)
		if strings.HasPrefix(filename, "bl.") {
			var i = Item{fmt.Sprintf("insert \"%s\"", parts[1]), 1, "template"}
			theMap[parts[1]] = i
			keys = append(keys, parts[1])
		}
	}
	sort.Strings(keys)
	// load the items
	for _, key := range keys {
		c.Values = append(c.Values, theMap[key])
	}
	return nil
}

// KeyWords return the keywords of the Liturgical Markup Language
// as a struct usable by the Ace Editor for autocompletion
func KeyWords() []Item {
	k := []Item{
		{"@allLiturgicalDayProperties", 1, "keyword"},
		{"@dayOfMonth", 1, "keyword"},
		{"@dayOfMovableCycle", 1, "keyword"},
		{"@dayOfWeekAsNumber", 1, "keyword"},
		{"@dayOfWeekAsText", 1, "keyword"},
		{"@eothinon", 1, "keyword"},
		{"@lukanCycleDay", 1, "keyword"},
		{"@lukanCycleElapsedDays", 1, "keyword"},
		{"@lukanCycleStartDate", 1, "keyword"},
		{"@lukanCycleWeek", 1, "keyword"},
		{"@modeOfWeek", 1, "keyword"},
		{"@nameOfPeriod", 1, "keyword"},
		{"@serviceDate", 1, "keyword"},
		{"@serviceYear", 1, "keyword"},
		{"@sundayAfterElevationCrossDate", 1, "keyword"},
		{"@sundaysBeforeTriodion", 1, "keyword"},
		{"apr", 1, "keyword"},
		{"aug", 1, "keyword"},
		{"calendar", 1, "keyword"},
		{"case", 1, "keyword"},
		{"center", 1, "keyword"},
		{"css", 1, "keyword"},
		{"date", 1, "keyword"},
		{"day", 1, "keyword"},
		{"dayOfWeek", 1, "keyword"},
		{"dec", 1, "keyword"},
		{"default", 1, "keyword"},
		{"else", 1, "keyword"},
		{"exists", 1, "keyword"},
		{"feb", 1, "keyword"},
		{"fri", 1, "keyword"},
		{"height", 1, "keyword"},
		{"href", 1, "keyword"},
		{"htmlOnly", 1, "keyword"},
		{"id", 1, "keyword"},
		{"if", 1, "keyword"}, //		{"insert", 1, "keyword"}, // this is covered by the template autocompletes
		{"indexLastTitleOverride = \"\"", 1, "keyword"},
		{"indexTitleCodes = \"\"", 1, "keyword"},
		{"jan", 1, "keyword"},
		{"jul", 1, "keyword"},
		{"jun", 1, "keyword"},
		{"ldp @allLiturgicalDayProperties", 1, "keyword"},
		{"ldp @dayOfMonth", 1, "keyword"},
		{"ldp @dayOfWeekAsNumber", 1, "keyword"},
		{"ldp @dayOfWeekAsText", 1, "keyword"},
		{"ldp @eothinon", 1, "keyword"},
		{"ldp @lukanCycleElapsedDays", 1, "keyword"},
		{"ldp @lukanCycleStartDate", 1, "keyword"},
		{"ldp @lukanCycleWeekDay", 1, "keyword"},
		{"ldp @modeOfWeek", 1, "keyword"},
		{"ldp @nameOfPeriod", 1, "keyword"},
		{"ldp @serviceDate", 1, "keyword"},
		{"ldp @serviceYear", 1, "keyword"},
		{"ldp @sundayAfterElevationCrossDate", 1, "keyword"},
		{"ldp @sundaysBeforeTriodion", 1, "keyword"},
		{"left", 1, "keyword"},
		{"lookup", 1, "keyword"},
		{"lpd @dayOfMovableCycle", 1, "keyword"},
		{"lukanCycleDay", 1, "keyword"},
		{"mar", 1, "keyword"},
		{"may", 1, "keyword"},
		{"media", 1, "keyword"},
		{"mode", 1, "keyword"},
		{"model", 1, "keyword"},
		{"modeOfWeek", 1, "keyword"},
		{"mon", 1, "keyword"},
		{"month", 1, "keyword"},
		{"movableCycleDay", 1, "keyword"},
		{"nid", 1, "keyword"},
		{"note", 1, "keyword"},
		{"nov", 1, "keyword"},
		{"oct", 1, "keyword"},
		{"office", 1, "keyword"},
		{"output = \"html\"", 1, "keyword"},
		{"output = \"html,pdf\"", 1, "keyword"},
		{"output = \"pdf\"", 1, "keyword"},
		{"pageBreak", 1, "keyword"},
		{"pageFooter", 1, "keyword"},
		{"pageFooterEven", 1, "keyword"},
		{"pageFooterOdd", 1, "keyword"},
		{"pageHeader", 1, "keyword"},
		{"pageHeaderEven", 1, "keyword"},
		{"pageHeaderOdd", 1, "keyword"},
		{"pageNbr", 1, "keyword"},
		{"pageNbr", 1, "keyword"},
		{"pdfOnly", 1, "keyword"},
		{"realm", 1, "keyword"},
		{"realm", 1, "keyword"},
		{"restoreDate", 1, "keyword"},
		{"restoreMovableCycleDay", 1, "keyword"},
		{"restoreRealm", 1, "keyword"},
		{"restoreVersion", 1, "keyword"}, //		{"rid", 1, "keyword"}, // this is covered by the topic-key autocompletes
		{"right", 1, "keyword"},
		{"sat", 1, "keyword"},
		{"sep", 1, "keyword"}, //		{"sid", 1, "keyword"}, // this is covered by the topic-key autocompletes
		{"src", 1, "keyword"},
		{"status = \"draft\"", 1, "keyword"},
		{"status = \"final\"", 1, "keyword"},
		{"status = \"review\"", 1, "keyword"},
		{"sun", 1, "keyword"},
		{"sundayAfterElevationOfCross", 1, "keyword"},
		{"sundaysBeforeTriodion", 1, "keyword"},
		{"switch", 1, "keyword"},
		{"target", 1, "keyword"},
		{"thru", 1, "keyword"},
		{"thu", 1, "keyword"},
		{"tue", 1, "keyword"},
		{"type = \"block\"", 1, "keyword"},
		{"type = \"book\"", 1, "keyword"},
		{"type = \"service\"", 1, "keyword"},
		{"ver", 1, "keyword"},
		{"ver", 1, "keyword"},
		{"version", 1, "keyword"},
		{"wed", 1, "keyword"},
		{"width", 1, "keyword"},
		{"year", 1, "keyword"},
	}
	return k
}
