package dbExport

import (
	"testing"
)

func TestManager_ExportAll(t *testing.T) {
	asSingleFile := false
	dbPath := "/Users/mac002/doxa/projects/doxa-alwb/db/liturgical.db"
	exportPath := "/Volumes/ssd2/doxa/exports"
	m, err := NewManager(dbPath, exportPath)
	if err != nil {
		t.Error(err)
		return
	}
	var count int
	count, err = m.ExportAll(asSingleFile)
	if err != nil {
		t.Error(err)
		return
	}
	if count == 0 {
		t.Errorf("zero records exorted")
	}
}
