package dbExport

import (
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"path"
	"strings"
)

type Manager struct {
	All     bool
	ds      *kvs.BoltKVS
	PathOut string
	KvPaths []*kvs.KeyPath
}

// NewManager is called when Doxa is NOT running
func NewManager(dbPath, pathOut string) (*Manager, error) {
	m := new(Manager)
	var err error
	m.ds, err = kvs.NewBoltKVS(dbPath)
	if err != nil {
		return nil, fmt.Errorf("error opening database %s: %v - only one process can open it at a time", dbPath, err)
	}
	m.PathOut = pathOut
	return m, nil
}

// NewManagerUsingOpenDb is called when Doxa is running
func NewManagerUsingOpenDb(ds *kvs.BoltKVS, pathOut string) (*Manager, error) {
	m := new(Manager)
	m.ds = ds
	m.PathOut = pathOut
	if !strings.HasSuffix(m.PathOut, "all") {
		m.PathOut = path.Join(m.PathOut, "all")
	}
	if ltfile.DirExists(m.PathOut) {
		ltfile.DeleteDirRecursively(m.PathOut)
	}
	return m, nil
}
func (m *Manager) ExportAll(singleFile bool) (count int, err error) {
	if !strings.HasSuffix(m.PathOut, "all") {
		m.PathOut = path.Join(m.PathOut, "all")
	}
	if ltfile.DirExists(m.PathOut) {
		ltfile.DeleteDirRecursively(m.PathOut)
	}
	if singleFile {
		return m.exportAsSingleFile()
	}
	return m.exportWithCanonicalParts()
}
func (m *Manager) exportAsSingleFile() (count int, err error) {
	var c int
	kvp := kvs.NewKeyPath()
	// if nothing set in kvp, will export all as
	// a single file
	err = m.ds.Export(kvp, m.PathOut, kvs.ExportAsLine)
	if err != nil {
		return -1, err
	}
	return c, nil
}
func (m *Manager) exportWithCanonicalParts() (count int, err error) {
	filepathSeparator := "/"
	var c int
	kvp := kvs.NewKeyPath()
	var dirNames []string
	dirNames, err = m.ds.DirNames(*kvp)
	if err != nil {
		return -1, err
	}
	for _, dirName := range dirNames {
		levels := -1
		switch dirName {
		case "btx":
			levels = 3
		case "btx-nnp":
			// ignore
		case "ltx":
			levels = 3
		case "ltx-nnp":
			// ignore
		case "configs":
			levels = 1
		case "keyring":
			// ignore
		case "media":
			levels = 3
		case "system":
			// for now ignore.  Later needs to be included, perhaps.
		default:
			// ignore
		}
		if levels > -1 { // get paths from this directory
			kvp.Dirs.Push(dirName)
			err = m.appendExportPaths(kvp, levels)
			if err != nil {
				return -1, err
			}
			kvp.Dirs.Pop()
		}
	}
	for _, k := range m.KvPaths {
		p := k.Dirs.SubPath(0, k.Dirs.Size(), filepathSeparator)
		p = fmt.Sprintf("%s.tsv", p)
		p = path.Join(m.PathOut, p)
		err = m.ds.Export(k, p, kvs.ExportAsLine)
		if err != nil {
			return c, fmt.Errorf("error exporting to %s: %v", p, err)
		}
		c++
	}
	return c, nil
}
func (m *Manager) appendExportPaths(kvp *kvs.KeyPath, levels int) (err error) {
	if kvp == nil {
		return fmt.Errorf("kvp is nil")
	}
	if kvp.Dirs.Size() > levels {
		return nil // depth already reached.
	}
	var dirNames []string
	dirNames, err = m.ds.DirNames(*kvp)
	if err != nil {
		return err
	}

	for _, dirName := range dirNames {
		if kvp.Dirs.Size() == 1 && levels == 1 {
			m.KvPaths = append(m.KvPaths, kvp.Copy())
			continue
		}
		kvp.Dirs.Push(dirName)
		if kvp.Dirs.Size() == levels {
			// add without recursion
			m.KvPaths = append(m.KvPaths, kvp.Copy())
		} else {
			// recursive call
			err = m.appendExportPaths(kvp, levels)
		}
		kvp.Dirs.Pop()
	}
	return err
}
