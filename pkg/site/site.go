package site

import (
	"context"
	"embed"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/asaskevich/EventBus"
	mapset "github.com/deckarep/golang-set/v2"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/ages/goarchdcs"
	"github.com/liturgiko/doxa/pkg/atempl"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/css"
	"github.com/liturgiko/doxa/pkg/doxlog"
	rowTypes "github.com/liturgiko/doxa/pkg/enums/RowTypes"
	"github.com/liturgiko/doxa/pkg/enums/calendarTypes"
	"github.com/liturgiko/doxa/pkg/enums/mediaTypes"
	"github.com/liturgiko/doxa/pkg/enums/outputTypes"
	"github.com/liturgiko/doxa/pkg/enums/properties"
	"github.com/liturgiko/doxa/pkg/enums/statuses"
	"github.com/liturgiko/doxa/pkg/enums/templateTypes"
	"github.com/liturgiko/doxa/pkg/enums/versions"
	"github.com/liturgiko/doxa/pkg/generators/html"
	"github.com/liturgiko/doxa/pkg/generators/pdf"
	"github.com/liturgiko/doxa/pkg/indexer"
	"github.com/liturgiko/doxa/pkg/keyring"
	"github.com/liturgiko/doxa/pkg/ltm"
	"github.com/liturgiko/doxa/pkg/models"
	"github.com/liturgiko/doxa/pkg/parser"
	"github.com/liturgiko/doxa/pkg/resolver"
	"github.com/liturgiko/doxa/pkg/sys"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"github.com/liturgiko/doxa/pkg/utils/queryw"
	"github.com/liturgiko/doxa/pkg/utils/stats"
	"github.com/liturgiko/doxa/pkg/valuemap"
	"html/template"
	"net/url"
	"os"
	"path"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

// const ReferenceDir = "ref"
const LiturgicalDir = "ltx"
const MaxTemplatesToReportWrite = 10

// Builder holds information for creating a website.
// Be sure to call NewBuilder() rather than using new(Builder)
type Builder struct {
	audioPathPrefix            string
	Config                     *models.BuildConfig
	DoxaVersion                string
	embeddedFiles              embed.FS
	Extension                  string
	Indexers                   []context.Context // to keep track of and stop indexer processes
	IndexMutex                 sync.Mutex
	IndexOnly                  bool
	Mapper                     *kvs.KVS
	MediaMapper                *valuemap.ValueMapper
	MsgBus                     EventBus.Bus
	MsgChan                    chan string
	MsgMap                     map[string]int
	SyncMutex                  *sync.Mutex
	NumberOfTemplatesProcessed int
	Patch                      embed.FS
	pdfPathPrefix              string
	pdfViewer                  string
	MediaReverseLookup         *sync.Map
	Realm                      string
	RelayMuted                 bool // if true, relay is silenced
	RelayUsesStandardOut       bool
	Resolver                   resolver.Resolver
	SiteName                   string
	sm                         *config.SettingsManager
	TemplateMapper             ltm.LTM
	ValueMapper                *valuemap.ValueMapper
}

func NewBuilder(doxaVersion string,
	indexOnly bool,
	mapper *kvs.KVS,
	realm string,
	audioPathPrefix string,
	pdfPathPrefix string,
	pdfViewer string,
	siteName string,
	sm *config.SettingsManager,
	embeddedPatch embed.FS,
	embeddedTemplates embed.FS,
	msgChan chan string,
	showUnresolvedTopicKey bool,
	keyRing *keyring.KeyRing,
	syncMutex *sync.Mutex,
	msgBus EventBus.Bus) (*Builder, error) {

	builder := new(Builder)
	builder.MsgMap = make(map[string]int)
	builder.DoxaVersion = doxaVersion
	builder.MediaMapper = valuemap.NewValueMapper()
	builder.ValueMapper = valuemap.NewValueMapper()
	builder.IndexOnly = indexOnly
	builder.Realm = realm
	builder.audioPathPrefix = audioPathPrefix
	builder.pdfViewer = pdfViewer
	builder.pdfPathPrefix = pdfPathPrefix
	builder.SiteName = siteName
	builder.sm = sm
	builder.embeddedFiles = embeddedTemplates
	builder.Patch = embeddedPatch
	builder.MsgChan = msgChan
	builder.MediaReverseLookup = &keyRing.MediaReverseLookup
	builder.SyncMutex = syncMutex
	builder.Extension = "lml"
	builder.MsgBus = msgBus
	err := builder.LoadConfig()
	if err != nil {
		return nil, err
	}

	// set up template mapper to use either templates in the database or the filesystem
	// depending on the property set by the user
	//if sm.BoolProps[properties.SiteBuildUseExternalTemplates] {
	builder.TemplateMapper = ltm.NewLTFM(builder.Config.TemplatesPrimaryPath,
		builder.Config.TemplatesFallbackPath,
		builder.Config.TemplatesPrimaryPath,
		config.DoxaPaths.SubscriptionsPath,
		builder.Config.TemplatesFallbackEnabled)
	//} else { // use the templates in the database templates directory
	//	builder.TemplateMapper = ltm.NewLTDM(builder.sm.Kvs, "templates", builder.Config.ExportsPath)
	//}

	builder.Mapper = mapper
	ltkResolver := resolver.NewDbResolver(mapper, keyRing, builder.TemplateMapper, showUnresolvedTopicKey)
	ltkResolver.PdfPathPrefix = pdfPathPrefix
	ltkResolver.AudioPathPrefix = audioPathPrefix
	ltkResolver.PdfViewer = pdfViewer
	builder.Resolver = ltkResolver

	builder.Config.HtmlCss, err = css.NewStyleSheet("html", config.DoxaPaths.HtmlCss, false)
	if err != nil {
		return nil, err
	}
	builder.Config.PdfCss, err = css.NewStyleSheet("pdf", config.DoxaPaths.PdfCss, false)
	if err != nil {
		return nil, err
	}

	// check to see if websites/test exists and has assets
	// if not, copy them asynchronously.  We do this so the template editor
	// can have assets when it generates a template in the editor.
	cssPath := path.Join(config.DoxaPaths.SiteGenTestPath, "css")
	if !ltfile.DirExists(cssPath) {
		go func() {
			builder.RelayMuted = true
			builder.CopyAssets(config.DoxaPaths.SiteGenTestPath)
			builder.RelayMuted = false
		}()
	}

	builder.MsgBus.SubscribeAsync("subscriptions:set", func(cata string) {
		if cata == "" {
			//builder.Config.TemplatesFallbackEnabled = false
			return
		}
		if strings.Contains(cata, ":") {
			if !strings.HasSuffix(cata, ":templates") {
				//something else was subbed too, we needn't update templates
				return
			}
			cata = strings.TrimSuffix(cata, ":templates")
		}
		builder.Config.TemplatesFallbackGitLabGroupName = cata
		builder.Config.TemplatesFallbackPath = path.Join(config.DoxaPaths.SubscriptionsPath /*cata,*/, "templates")
		//builder.Config.TemplatesFallbackEnabled = true
	}, false)
	return builder, nil
}
func (b *Builder) AddContext() context.Context {
	ctx := context.Background()
	b.Indexers = append(b.Indexers, ctx)
	if len(b.Indexers) == 1 {
		b.Relay(fmt.Sprintf("%d search website indexer running", len(b.Indexers)))
	} else {
		b.Relay(fmt.Sprintf("%d search website indexers running", len(b.Indexers)))
	}
	return ctx
}
func (b *Builder) CancelIndexers() {
	for _, ctx := range b.Indexers {
		ctx.Done()
		b.Relay("canceling website indexer")
	}
	b.Indexers = []context.Context{}
	b.Relay("all website indexers canceled")
}

// WriteCssFile writes the file to assets/css, websites/public/css, websites/test/css
func (b *Builder) WriteCssFile(filename string, content string) error {
	var thePaths []string
	base := path.Base(filename)
	if base == "app.css" { //
		filename = path.Join(config.DoxaPaths.AssetsPath, "css", "app.css")
		thePaths = append(thePaths, filename)
		filename = path.Join(config.DoxaPaths.SiteGenPublicPath, "css", "app.css")
		thePaths = append(thePaths, filename)
		filename = path.Join(config.DoxaPaths.SiteGenTestPath, "css", "app.css")
		thePaths = append(thePaths, filename)
	} else if base == "pdf.css" {
		filename = path.Join(config.DoxaPaths.AssetsPath, "pdf", "css", "pdf.css")
		thePaths = append(thePaths, filename)
		filename = path.Join(config.DoxaPaths.SiteGenPublicPath, "pdf", "css", "pdf.css")
		thePaths = append(thePaths, filename)
		filename = path.Join(config.DoxaPaths.SiteGenTestPath, "pdf", "css", "pdf.css")
		thePaths = append(thePaths, filename)
	} else {
		return fmt.Errorf("%s is not a css file", filename)
	}
	for _, p := range thePaths {
		err := ltfile.WriteFile(p, content)
		if err != nil {
			return err
		}
	}
	return nil
}

// WriteJsFile writes the file to assets/js, websites/public/s, websites/test/js
func (b *Builder) WriteJsFile(filename string, content string) error {
	var thePaths []string
	base := path.Base(filename)
	if base != "app.js" {
		return fmt.Errorf("%s is not the app javascript file", base)
	}
	filename = path.Join(config.DoxaPaths.AssetsPath, "js", base)
	thePaths = append(thePaths, filename)
	filename = path.Join(config.DoxaPaths.SiteGenPublicPath, "js", base)
	thePaths = append(thePaths, filename)
	filename = path.Join(config.DoxaPaths.SiteGenTestPath, "js", base)
	thePaths = append(thePaths, filename)
	for _, p := range thePaths {
		err := ltfile.WriteFile(p, content)
		if err != nil {
			return err
		}
	}
	return nil
}

// Relay sends on the msgChannel using a standard prefix.
// If msgChannel is nil, it won't attempt to send the message.
func (b *Builder) Relay(msg string) {
	if b.RelayMuted {
		return
	}
	if b.RelayUsesStandardOut {
		fmt.Printf("GM: %s\n", msg)
		return
	}
	if b.MsgChan != nil {
		b.MsgChan <- fmt.Sprintf("GM: %s", msg)
	}
}

// LoadConfig re-initializes the b.Config.  It is called by Build().
func (b *Builder) LoadConfig() error {
	var err error
	b.Config = models.NewBuildConfig(b.SiteName, b.sm)
	b.Config.AssetsPath = config.DoxaPaths.AssetsPath
	b.Config.AssetsRootPath = config.DoxaPaths.AssetsRootPath

	// TODO: Jesse delete
	doxlog.Info(fmt.Sprintf("root path == %s", b.Config.AssetsRootPath))
	if !ltfile.DirExists(b.Config.AssetsRootPath) {
		err = ltfile.CreateDirs(b.Config.AssetsRootPath)
		if err != nil {
			doxlog.Errorf("error creating directory %s: %v", b.Config.AssetsRootPath, err)
		}
	}
	b.Config.ExportsPath = config.DoxaPaths.ExportPath
	b.Config.BuildPublic = b.sm.BoolProps[properties.SiteBuildPublic]
	if b.Config.BuildPublic {
		b.Config.SitePath = config.DoxaPaths.SiteGenPublicPath
	} else {
		b.Config.SitePath = config.DoxaPaths.SiteGenTestPath
	}
	b.Config.PreviewPath = config.DoxaPaths.PreviewPath
	b.Config.DoxaVersion = b.DoxaVersion
	// parse the go html templates for the generated site
	b.Config.Templates, err = template.ParseFS(b.embeddedFiles, "templates/genSite/*.gohtml")
	if err != nil {
		fmt.Println(fmt.Sprintf("error initializing html templates in pkg/build/site.go: %v", err))
	}

	// load the config from the database
	if err = b.Config.Load(); err != nil {
		msg := fmt.Sprintf("error loading config from database: %v", err)
		doxlog.Error(msg)
		return fmt.Errorf("%s", err)
	}

	if !ltfile.DirExists(config.DoxaPaths.PrimaryTemplatesPath) {
		err = ltfile.CreateDirs(config.DoxaPaths.PrimaryTemplatesPath)
		if err != nil {
			msg := fmt.Sprintf("error creating dirs %s: %v", config.DoxaPaths.PrimaryTemplatesPath, err)
			doxlog.Error(msg)
			return fmt.Errorf("%s", err)
		}
	}
	b.Config.Calendar = calendarTypes.ToType(b.sm.StringProps[properties.SiteBuildCalendar])
	// set properties for dcs.html (Digital Chant Stand)
	b.Config.DcsPage = new(html.DCS)
	b.Config.DcsPage.Templates = b.Config.Templates
	b.Config.DcsPage.Name = "dcs" // this is a template name, not a directory name
	b.Config.DcsPage.PathOut = path.Join(b.Config.SitePath, "dcs.html")
	b.Config.DcsPage.Title = b.sm.StringProps[properties.SiteBuildPagesHomeTitle] // bc.GetString("home.title")
	b.Config.DcsPage.Navbar = b.Config.IndexNavbar
	b.Config.DcsPage.Scripts = b.Config.Scripts
	b.Config.DcsPage.Links = b.Config.Links
	b.Config.DcsPage.MetaDescription = b.sm.StringProps[properties.SiteBuildPagesHomeMetaDescription]
	b.Config.DcsPage.MetaOgTitle = b.sm.StringProps[properties.SiteBuildPagesHomeMetaOgTitle]
	b.Config.DcsPage.MetaDescription = b.sm.StringProps[properties.SiteBuildPagesHomeMetaOgDescription]
	b.Config.DcsPage.MetaOgUrl = b.sm.StringProps[properties.SiteBuildPagesHomeMetaOgUrl]
	b.Config.DcsPage.FrameTextSrc = b.sm.StringProps[properties.SiteBuildFramesTextSrc]
	if len(b.Config.DcsPage.FrameTextSrc) == 0 {
		b.Config.DcsPage.FrameTextSrc = "servicesindex.html"
	}
	// set properties for scoreFrame
	b.Config.ScoreFrame = new(html.Blank)
	b.Config.ScoreFrame.Name = "blank"
	b.Config.ScoreFrame.PathOut = path.Join(b.Config.SitePath, "blank.html")
	b.Config.ScoreFrame.Heading = b.sm.StringSliceProps[properties.SiteBuildFramesScoreHeading]
	donateText := b.sm.StringProps[properties.SiteBuildFramesScoreDonateText]
	if len(donateText) > 0 {
		b.Config.ScoreFrame.Donate = new(html.Card)
		b.Config.ScoreFrame.Donate.Title = b.sm.StringProps[properties.SiteBuildFramesScoreDonateTitle]
		b.Config.ScoreFrame.Donate.Text = donateText
	}
	b.Config.ScoreFrame.Alerts = b.sm.StringSliceProps[properties.SiteBuildFramesScoreAlerts]
	b.Config.ScoreFrame.Paragraphs = b.sm.StringSliceProps[properties.SiteBuildFramesScoreParagraphsValues]
	b.Config.ScoreFrame.TwitterHandle = b.sm.StringProps[properties.SiteBuildFramesScoreTwitterHandle]
	b.Config.ScoreFrame.TwitterHref = b.sm.StringProps[properties.SiteBuildFramesScoreTwitterUrl]
	b.Config.ScoreFrame.Templates = b.Config.Templates
	b.Config.ScoreFrame.Title = "Blank"
	b.Config.ScoreFrame.Links = b.Config.Links
	b.Config.ScoreFrame.Scripts = b.Config.Scripts

	// set properties for redirect index.html
	b.Config.DevicePage = new(html.Device)
	b.Config.DevicePage.Templates = b.Config.Templates
	b.Config.DevicePage.Name = "device"
	b.Config.DevicePage.PathOut = path.Join(b.Config.SitePath, "index.html")

	b.Config.HomePage.ImgSrc = b.sm.StringProps[properties.SiteBuildPagesHomeCardsParentValueImageSource]
	b.Config.HomePage.ImgAlt = b.sm.StringProps[properties.SiteBuildPagesHomeCardsParentValueImageAlt]
	b.Config.HomePage.InsertImg = len(b.Config.HomePage.ImgSrc) > 0

	b.Config.HomePage.IntroCard.Title = b.sm.StringProps[properties.SiteBuildPagesHomeCardsIntroValueTitle]
	b.Config.HomePage.IntroCard.Text = b.sm.StringProps[properties.SiteBuildPagesHomeCardsIntroValueBody]
	b.Config.HomePage.IntroCard.ButtonIcon = b.sm.StringProps[properties.SiteBuildPagesHomeCardsIntroValueButtonIcon]
	b.Config.HomePage.IntroCard.ButtonText = b.sm.StringProps[properties.SiteBuildPagesHomeCardsIntroValueButtonText]
	b.Config.HomePage.IntroCard.ButtonUrl = b.sm.StringProps[properties.SiteBuildPagesHomeCardsIntroValueButtonUrl]
	b.Config.HomePage.IntroCard.ImgSrc = b.sm.StringProps[properties.SiteBuildPagesHomeCardsIntroValueImageSource]
	b.Config.HomePage.IntroCard.ImgAlt = b.sm.StringProps[properties.SiteBuildPagesHomeCardsIntroValueImageAlt]
	b.Config.HomePage.IntroCard.InsertImg = len(b.Config.HomePage.IntroCard.ImgSrc) > 0

	b.Config.HomePage.ServiceCard.Title = b.sm.StringProps[properties.SiteBuildPagesHomeCardsServicesValueTitle]
	b.Config.HomePage.ServiceCard.Text = b.sm.StringProps[properties.SiteBuildPagesHomeCardsServicesValueBody]
	b.Config.HomePage.ServiceCard.ButtonText = b.sm.StringProps[properties.SiteBuildPagesHomeCardsServicesValueButtonText]
	b.Config.HomePage.ServiceCard.ButtonUrl = b.sm.StringProps[properties.SiteBuildPagesHomeCardsServicesValueButtonUrl]
	b.Config.HomePage.ServiceCard.ImgSrc = b.sm.StringProps[properties.SiteBuildPagesHomeCardsServicesValueImageSource]
	b.Config.HomePage.ServiceCard.ImgAlt = b.sm.StringProps[properties.SiteBuildPagesHomeCardsServicesValueImageAlt]
	b.Config.HomePage.ServiceCard.InsertImg = len(b.Config.HomePage.ServiceCard.ImgSrc) > 0

	b.Config.HomePage.BookCard.Title = b.sm.StringProps[properties.SiteBuildPagesHomeCardsBooksValueTitle]
	b.Config.HomePage.BookCard.Text = b.sm.StringProps[properties.SiteBuildPagesHomeCardsBooksValueBody]
	b.Config.HomePage.BookCard.ButtonText = b.sm.StringProps[properties.SiteBuildPagesHomeCardsBooksValueButtonText]
	b.Config.HomePage.BookCard.ButtonUrl = b.sm.StringProps[properties.SiteBuildPagesHomeCardsBooksValueButtonUrl]
	b.Config.HomePage.BookCard.ImgSrc = b.sm.StringProps[properties.SiteBuildPagesHomeCardsBooksValueImageSource]
	b.Config.HomePage.BookCard.ImgAlt = b.sm.StringProps[properties.SiteBuildPagesHomeCardsBooksValueImageAlt]
	b.Config.HomePage.BookCard.InsertImg = len(b.Config.HomePage.ServiceCard.ImgSrc) > 0

	b.Config.HomePage.HelpTextPrefix = b.sm.StringProps[properties.SiteBuildPagesHomeCardsClosingValueBodyPrefix]
	b.Config.HomePage.HelpTextSuffix = b.sm.StringProps[properties.SiteBuildPagesHomeCardsClosingValueBodySuffix]
	b.Config.HomePage.Paragraphs = b.sm.StringSliceProps[properties.SiteBuildPagesHomeParagraphs]

	// get the properties for the search page (search.html)
	b.Config.SearchPage = new(html.Search)
	b.Config.SearchPage.DoxaVersion = b.Config.DoxaVersion
	b.Config.SearchPage.Footer = b.Config.Footer
	b.Config.SearchPage.FramesEnabled = b.Config.FramesEnabled
	b.Config.SearchPage.Links = b.Config.Links
	b.Config.SearchPage.Name = "search"
	b.Config.SearchPage.Navbar = b.Config.IndexNavbar
	b.Config.SearchPage.PathOut = path.Join(b.Config.SitePath, "search.html")
	b.Config.SearchPage.Scripts = b.Config.Scripts
	b.Config.SearchPage.Source = b.Config.SearchSrc
	b.Config.SearchPage.Templates = b.Config.Templates
	b.Config.SearchPage.Title = "Search" // TODO: make config property

	// get the properties for a ltx/topic/key page
	b.Config.TopicKeyComparePage = new(html.TopicKeyCompare)
	b.Config.TopicKeyComparePage.DoxaVersion = b.Config.DoxaVersion
	b.Config.TopicKeyComparePage.Footer = b.Config.Footer
	b.Config.TopicKeyComparePage.Links = b.Config.Links
	b.Config.TopicKeyComparePage.Name = "tkcompare"
	b.Config.TopicKeyComparePage.Navbar = b.Config.IndexNavbar
	b.Config.TopicKeyComparePage.PathOut = path.Join(b.Config.SitePath, "search.html")
	b.Config.TopicKeyComparePage.Scripts = b.Config.Scripts
	b.Config.TopicKeyComparePage.Templates = b.Config.Templates
	b.Config.TopicKeyComparePage.Title = "Topic-Key Comparison" // TODO: make config property

	// get the properties for a ltx/topic/key index page
	b.Config.TopicKeyIndexPage = new(html.LtxTopicKeysIndex)
	b.Config.TopicKeyIndexPage.Footer = b.Config.Footer
	b.Config.TopicKeyIndexPage.Links = b.Config.Links
	b.Config.TopicKeyIndexPage.Name = "topicKeyIndexTable"
	b.Config.TopicKeyIndexPage.Navbar = b.Config.IndexNavbar
	b.Config.TopicKeyIndexPage.Scripts = b.Config.Scripts
	b.Config.TopicKeyIndexPage.Templates = b.Config.Templates

	// get the properties for the help page (help.html), aka About
	b.Config.HelpPage = new(html.Help)
	b.Config.HelpPage.Templates = b.Config.Templates
	b.Config.HelpPage.Name = "help"
	b.Config.HelpPage.Source = b.Config.HelpSrc
	b.Config.HelpPage.PathOut = path.Join(b.Config.SitePath, "help.html")

	b.Config.HelpPage.Title = b.sm.StringProps[properties.SiteBuildPagesHelpTitle]
	b.Config.HelpPage.Paragraphs = b.sm.StringSliceProps[properties.SiteBuildPagesHelpParagraphs]
	b.Config.HelpPage.Navbar = b.Config.IndexNavbar
	b.Config.HelpPage.Scripts = b.Config.Scripts
	b.Config.HelpPage.Links = b.Config.Links
	b.Config.HelpPage.Footer = b.Config.Footer
	b.Config.HelpPage.GenTime = fmt.Sprintf("%s UTC", ltstring.Timestamp2())
	b.Config.HelpPage.IconHelpIntro = b.sm.StringProps[properties.SiteBuildPagesHelpIconsIntro]
	b.Config.HelpPage.IconTitle = b.sm.StringProps[properties.SiteBuildPagesHelpIconsTitle]
	b.Config.HelpPage.IconBooks = b.sm.StringProps[properties.SiteBuildPagesHelpIconsBooks]
	b.Config.HelpPage.IconCalendar = b.sm.StringProps[properties.SiteBuildPagesHelpIconsCalendar]
	b.Config.HelpPage.IconColumns = b.sm.StringProps[properties.SiteBuildPagesHelpIconsColumns]
	b.Config.HelpPage.IconHelp = b.sm.StringProps[properties.SiteBuildPagesHelpIconsHelp]
	b.Config.HelpPage.IconHome = b.sm.StringProps[properties.SiteBuildPagesHelpIconsHome]
	b.Config.HelpPage.IconInfo = b.sm.StringProps[properties.SiteBuildPagesHelpIconsInfo]
	b.Config.HelpPage.IconLeft = b.sm.StringProps[properties.SiteBuildPagesHelpIconsLeft]
	b.Config.HelpPage.IconMinus = b.sm.StringProps[properties.SiteBuildPagesHelpIconsMinus]
	b.Config.HelpPage.IconMoon = b.sm.StringProps[properties.SiteBuildPagesHelpIconsMoon]
	b.Config.HelpPage.IconPlus = b.sm.StringProps[properties.SiteBuildPagesHelpIconsPlus]
	b.Config.HelpPage.IconRight = b.sm.StringProps[properties.SiteBuildPagesHelpIconsRight]
	b.Config.HelpPage.IconSearch = b.sm.StringProps[properties.SiteBuildPagesHelpIconsSearch]
	b.Config.HelpPage.IconSun = b.sm.StringProps[properties.SiteBuildPagesHelpIconsSun]
	b.Config.HelpPage.IconAudio = b.sm.StringProps[properties.SiteBuildPagesHelpIconsAudio]
	b.Config.HelpPage.IconBScore = b.sm.StringProps[properties.SiteBuildPagesHelpIconsBscore]
	b.Config.HelpPage.IconEScore = b.sm.StringProps[properties.SiteBuildPagesHelpIconsEscore]
	b.Config.HelpPage.IconWScore = b.sm.StringProps[properties.SiteBuildPagesHelpIconsWscore]
	// The properties of the DocNavbar are initially set in buildConfig for the IndexNavbar.
	// The DocNavbar is then set equal o the IndexNavbar.
	// Below, only set properties specific to a book or service page.
	b.Config.DocNavbar.ShowColumnToggle = b.sm.BoolProps[properties.SiteBuildNavbarShowColumnToggle]
	b.Config.DocNavbar.ShowNightDayToggle = b.sm.BoolProps[properties.SiteBuildNavbarShowNightDayToggle]
	b.Config.DocNavbar.ShowTime = b.sm.BoolProps[properties.SiteBuildShowTime]
	return nil
}
func (b *Builder) DbTemplateDirExists() bool {
	kp := kvs.NewKeyPath()
	kp.Dirs.Push("templates")
	return b.sm.PM.KVS.Db.Exists(kp)
}
func (b *Builder) FileTemplateDirExists() bool {
	return ltfile.DirExists(b.Config.TemplatesPrimaryPath)
}

// Build - For each domain, generates files of specified types whose names match one of the patterns
// The various directory paths should be appended by the caller to add a realm.
// LoadConfig is called each time Build is called so current database changes are used.
// preview == true means a template was opened, and we are rendering the preview pane.
// msgChan is used to stream feedback to the caller during the build process.
func (b *Builder) Build(buildPublic, preview bool) (error, []parser.ParseError) { // types of files to generate
	(*b.SyncMutex).Lock()
	defer (*b.SyncMutex).Unlock()
	// get selected statuses
	statusesStr := b.Config.SM.StringProps[properties.SiteBuildTemplatesStatusesSelected]
	statusesStr = strings.ReplaceAll(statusesStr, " ", "")
	var selectedTemplateStatuses []statuses.Status
	var err error
	selectedTemplateStatuses, err = statuses.ToCodes(strings.Split(statusesStr, ","))
	if err != nil {
		doxlog.Error(fmt.Sprintf("selected template statuses error: %v", err))
	}
	// get selected days of week
	var dowInt []int
	dowStr := b.Config.SM.StringProps[properties.SiteBuildTemplatesDaysOfWeekSelected]
	if len(dowStr) == 0 || dowStr == "*" {
		dowInt = []int{0, 1, 2, 3, 4, 5, 6}
	} else {
		if dowInt, err = ltstring.ToIntSlice(dowStr); err != nil {
			if err != nil {
				doxlog.Error(fmt.Sprintf("selected template days of week error: %v", err))
			}
		}
	}

	doxlog.Info("Starting site generation")
	var templates []*ltm.Data

	// reset Media and Value mappers
	b.MediaMapper = valuemap.NewValueMapper()
	b.ValueMapper = valuemap.NewValueMapper()

	// clear out any values held in resolver from previous build
	b.Resolver.Reset()

	// set up template mapper to use either templates in the database or the filesystem
	// depending on the property set by the user
	if b.sm.BoolProps[properties.SiteBuildUseExternalTemplates] {
		if !b.FileTemplateDirExists() {
			return fmt.Errorf("configuration property /site/build/templates/external is set to true, but the directory %s does not exist: set the property to false to use templates from the database", b.Config.TemplatesPrimaryPath), nil
		}
		b.TemplateMapper = ltm.NewLTFM(b.Config.TemplatesPrimaryPath,
			b.Config.TemplatesFallbackPath,
			b.Config.TemplatesPrimaryPath,
			config.DoxaPaths.SubscriptionsPath,
			b.Config.TemplatesFallbackEnabled)
	} else { // use the templates in the database templates directory
		if !b.DbTemplateDirExists() {
			return fmt.Errorf("configuration property /site/build/templates/external is set to false, but the database templates directory does not exist: set the property to true to use templates from the file system"), nil
		}
		// TODO: the LTDM primary and fallback have not been implemented for templates stored in the database.  And the parms here have wrong values.
		b.TemplateMapper = ltm.NewLTDM(b.sm.Kvs, "templates", "templates", "templates", b.Config.ExportsPath) // ltm.NewLTFM(Paths.TemplatesPath)
	}
	b.Resolver.SetTemplateMapper(b.TemplateMapper)
	err = b.LoadConfig()
	if err != nil {
		return err, nil
	}

	// load the version abbreviations
	b.Config.Versions = b.Resolver.Versions()

	var patterns []string
	patterns, err = b.Config.GetSelectedPatterns()
	if err != nil {
		return err, nil
	}
	if patterns == nil {
		return fmt.Errorf("builder.Build(): no patterns selected"), nil
	} else if len(patterns) == 0 {
		return fmt.Errorf("no patterns selected"), nil
	}
	// TODO: make this a parameter from the user
	osDelimiter := "/"
	dirExclusionPatterns := []string{fmt.Sprintf("%ss-temp%s", osDelimiter, osDelimiter),
		fmt.Sprintf("%ss-tests%s", osDelimiter, osDelimiter),
		fmt.Sprintf("%sa-templates%sMedia_Lists%s", osDelimiter, osDelimiter, osDelimiter)}
	templates, err = b.TemplateMapper.GetMatchingTemplates(patterns, dirExclusionPatterns, selectedTemplateStatuses, dowInt)
	if err != nil {
		return err, nil
	}
	templateCount := len(templates)
	b.Relay(fmt.Sprintf("%d templates matched", templateCount))

	if templates == nil || len(templates) == 0 {
		return fmt.Errorf("no templates matched"), nil
	}
	htmlCssFilePath := path.Join(b.Config.AssetsPath, "css", "app.css")
	b.Config.HtmlCss, err = css.NewStyleSheet("html", htmlCssFilePath, false)
	if err != nil {
		return err, nil
	}
	pdfCssFilePath := path.Join(b.Config.AssetsPath, "pdf", "css", "pdf.css")
	b.Config.PdfCss, err = css.NewStyleSheet("pdf", pdfCssFilePath, false)
	if err != nil {
		return err, nil
	}

	// set the build time for the help (about) page
	b.Config.HelpPage.GenTime = fmt.Sprintf("%s UTC", ltstring.Timestamp2())
	// and the doxa version
	b.Config.HelpPage.DoxaVersion = b.DoxaVersion
	// copy assets to site
	to := path.Join(b.Config.SitePath, "css")
	if b.Config.CopyAssets || !ltfile.DirExists(to) {
		if err = b.CopyAssets(b.Config.SitePath); err != nil {
			return err, nil
		}
	} else { // always copy doxa.css, app.css, app.js
		b.CopyAppDoxaCssJs()
	}
	//patch after copy so it can detect missing dir
	err = ltfile.CopyEmbedded(b.Patch, "patch/genSite/assets", []string{b.Config.AssetsPath, config.DoxaPaths.SiteGenTestPath})
	if err != nil {
		return err, nil
	}

	if b.Config.DeleteHtmlDir {
		delPath := path.Join(b.Config.SitePath, "h")
		if _, err := os.Stat(delPath); !os.IsNotExist(err) {
			b.Relay(fmt.Sprintf("removing HTML directory before building. If you do not want this in the future, set %s/value to false", properties.SiteBuildPregenDeleteHtmlDir.Data().Path))
			err := ltfile.DeleteDirRecursively(delPath)
			if err != nil {
				b.Relay(fmt.Sprintf("error removing HTML directory %s: %v", delPath, err))
			}
		}
	}
	if b.Config.DeletePdfDir {
		delPath := path.Join(b.Config.SitePath, "p")
		if ltfile.DirExists(delPath) {
			b.Relay(fmt.Sprintf("removing PDF directory before building. If you do not want this in the future, set %s/value to false\n", properties.SiteBuildPregenDeletePdfDir.Data().Path))
			err := ltfile.DeleteDirRecursively(delPath)
			if err != nil {
				b.Relay(fmt.Sprintf("error removing PDF directory %s: %v", delPath, err))
			}
		}
	}
	if b.Config.DeletePdfDir || b.Config.DeleteHtmlDir {
		delPath := path.Join(b.Config.SitePath, "indexes")
		if ltfile.DirExists(delPath) {
			err := ltfile.DeleteDirRecursively(delPath)
			if err != nil {
				doxlog.Infof("error removing %s: %v", delPath, err)
			}
		}
		delPath = path.Join(b.Config.SitePath, "booksindex.html")
		if ltfile.DirExists(delPath) {
			err := ltfile.DeleteDirRecursively(delPath)
			if err != nil {
				doxlog.Infof("error removing %s: %v", delPath, err)
			}
		}
		delPath = path.Join(b.Config.SitePath, "servicesindex.html")
		if ltfile.DirExists(delPath) {
			err := ltfile.DeleteDirRecursively(delPath)
			if err != nil {
				doxlog.Infof("error removing %s: %v", delPath, err)
			}
		}
	}

	errorChan := make(chan error, len(templates))
	parseErrorChan := make(chan []parser.ParseError, len(templates))

	var allParseErrors []parser.ParseError

	var wg sync.WaitGroup

	for i, t := range templates {
		_, fName := path.Split(t.Path)
		b.Relay(fmt.Sprintf("parsing template %s, status %s (%d of %d)", fName, statuses.String(t.Status), i+1, templateCount))
		b.NumberOfTemplatesProcessed++
		wg.Add(1)
		go b.processTemplate(
			t.Path,
			t.Content,
			b.Config.Interlinear,
			preview,
			parseErrorChan,
			errorChan,
			&wg)
		select {
		case err := <-errorChan:
			if err != nil {
				return err, nil
			}
		case parseErrors := <-parseErrorChan:
			for _, parseError := range parseErrors {
				allParseErrors = append(allParseErrors, parseError)
				doxlog.Error(fmt.Sprintf("%s", parseError.String()))
			}
		}
	}
	wg.Wait()
	for k, _ := range b.MsgMap {
		b.Relay(k)
	}
	//	b.Resolver.
	b.Relay("All templates processed")
	doxlog.Info("Finished site generation.")
	return nil, allParseErrors
}
func (b *Builder) CopyAppDoxaCssJs() {
	// copy doxa.css
	from := path.Join(b.Config.AssetsPath, "css", "doxa.css")
	to := path.Join(b.Config.SitePath, "css", "doxa.css")
	err := ltfile.CopyFile(from, to)
	if err != nil {
		doxlog.Infof("error copying %s: %v", to, err)
	}
	// copy doxa.js
	from = path.Join(b.Config.AssetsPath, "js", "doxa.js")
	to = path.Join(b.Config.SitePath, "js", "doxa.js")
	err = ltfile.CopyFile(from, to)
	if err != nil {
		doxlog.Infof("error copying %s: %v", to, err)
	}
	// copy app.css
	from = path.Join(b.Config.AssetsPath, "css", "app.css")
	to = path.Join(b.Config.SitePath, "css", "app.css")
	err = ltfile.CopyFile(from, to)
	if err != nil {
		doxlog.Infof("error copying %s: %v", to, err)
	}
	// copy app.js
	from = path.Join(b.Config.AssetsPath, "js", "app.js")
	to = path.Join(b.Config.SitePath, "js", "app.js")
	err = ltfile.CopyFile(from, to)
	if err != nil {
		doxlog.Infof("error copying %s: %v", to, err)
	}
}
func (b *Builder) GetFallbackSettings() (enabled bool, selected string, subscriptions []string, err error) {
	dirs, err := ltfile.DirsInDir(config.DoxaPaths.SubscriptionsPath, true)
	for _, d := range dirs {
		sPath := path.Join(config.DoxaPaths.SubscriptionsPath, d, "templates")
		if ltfile.DirExists(sPath) {
			subscriptions = append(subscriptions, d)
			if sPath == b.Config.TemplatesFallbackPath {
				selected = d
			}
		}
	}
	enabled = b.Config.TemplatesFallbackEnabled
	return enabled, selected, subscriptions, nil
}

func (b *Builder) SetFallbackSettings(enabled bool, selected string) error {
	fallbackPath := path.Join(config.DoxaPaths.SubscriptionsPath, selected)
	if enabled {
		if len(selected) == 0 {
			return fmt.Errorf("if fallbacks are enabled, you must select one of your template subscriptions")
		}
	}
	// verify that the selected directory exists
	if !ltfile.DirExists(fallbackPath) {
		return fmt.Errorf("the template subscription %s does not exist", selected)
	}
	// verify that the selected directory has a templates directory
	fallbackPath = path.Join(config.DoxaPaths.SubscriptionsPath, selected, "templates")
	if !ltfile.DirExists(fallbackPath) {
		return fmt.Errorf("the template subscription %s exists, but does not have a templates subdirectory", selected)
	}

	b.Config.TemplatesFallbackEnabled = enabled
	var strEnabled string
	if enabled {
		strEnabled = "true"
	} else {
		strEnabled = "false"
	}
	err := b.Config.SM.SetValueByType(properties.SiteBuildTemplatesFallbackEnabled, strEnabled)
	if err != nil {
		return fmt.Errorf("error saving property SiteBuildTemplatesFallbackEnabled: %v", err)
	}
	err = b.Config.SM.SetValueByType(properties.SiteBuildTemplatesFallbackDir, selected)
	if err != nil {
		return fmt.Errorf("error saving property SiteBuildTemplatesFallbackEnabled: %v", err)
	}
	if enabled {
		b.Config.TemplatesFallbackPath = path.Join(config.DoxaPaths.SubscriptionsPath, "templates") //fallbackPath
	} else {
		b.Config.TemplatesFallbackPath = ""
	}
	b.TemplateMapper = ltm.NewLTFM(b.Config.TemplatesPrimaryPath,
		b.Config.TemplatesFallbackPath,
		b.Config.TemplatesPrimaryPath,
		config.DoxaPaths.SubscriptionsPath,
		b.Config.TemplatesFallbackEnabled)
	return nil
}

func (b *Builder) RemoveAssetsFromSite(sitePath string) error {
	var err error
	dir := path.Join(sitePath, "css")
	err = ltfile.DeleteDirRecursively(dir)
	if err != nil {
		return fmt.Errorf("error removing %s: %v", dir, err)
	}
	dir = path.Join(sitePath, "img")
	err = ltfile.DeleteDirRecursively(dir)
	if err != nil {
		return fmt.Errorf("error removing %s: %v", dir, err)
	}
	dir = path.Join(sitePath, "js")
	err = ltfile.DeleteDirRecursively(dir)
	if err != nil {
		return fmt.Errorf("error removing %s: %v", dir, err)
	}
	dir = path.Join(sitePath, "static")
	err = ltfile.DeleteDirRecursively(dir)
	if err != nil {
		return fmt.Errorf("error removing %s: %v", dir, err)
	}
	return nil
}
func (b *Builder) CopyAssets(sitePath string) error {
	var err error
	if err = b.RemoveAssetsFromSite(sitePath); err != nil {
		return err
	}
	to := path.Join(sitePath, "css")

	b.Relay("copying css...")
	from := path.Join(b.Config.AssetsPath, "css")
	err = ltfile.CopyDir(from, to)
	if err != nil {
		return fmt.Errorf("error copying %s to %s: %v", from, to, err)
	}
	b.Relay("copying images...")
	to = path.Join(sitePath, "img")
	from = path.Join(b.Config.AssetsPath, "img")
	err = ltfile.CopyDir(from, to)
	if err != nil {
		return fmt.Errorf("error copying %s to %s: %v", from, to, err)
	}
	b.Relay("copying js...")
	to = path.Join(sitePath, "js")
	from = path.Join(b.Config.AssetsPath, "js")
	err = ltfile.CopyDir(from, to)
	if err != nil {
		return fmt.Errorf("error copying %s to %s: %v", from, to, err)
	}
	to = path.Join(sitePath, "static")
	from = path.Join(b.Config.AssetsPath, "static")
	if ltfile.DirExists(to) {
		b.Relay("copying static")
		err = ltfile.CopyDir(from, to)
		if err != nil {
			return fmt.Errorf("error copying %s to %s: %v", from, to, err)
		}
	}

	// copy all html files in the assets folder
	// If the home page (index.html) and help.html did not exist,
	// they will have been generated and put into the sites dir already.
	htmlFiles, err := ltfile.FileMatcher(b.Config.AssetsRootPath, "html", nil)
	if err != nil {
		msg := fmt.Sprintf("error searching for html files in %s: %v", b.Config.AssetsRootPath, err)
		doxlog.Error(msg)
	}
	msg := fmt.Sprintf("Copying html files from %s...", b.Config.AssetsRootPath)
	doxlog.Info(msg)
	b.Relay(msg)
	for _, file := range htmlFiles {
		_, filename := path.Split(file)
		to := path.Join(sitePath, filename)
		doxlog.Info(fmt.Sprintf("copying %s", file))
		doxlog.Info(fmt.Sprintf("to %s", to))
		err = ltfile.CopyFile(file, to)
		if err != nil {
			msg = fmt.Sprintf("error copying %s to %s: %v", file, to, err)
			doxlog.Error(msg)
			continue
		}
	}
	faviconFrom := path.Join(b.Config.AssetsRootPath, "favicon.ico")
	faviconTo := path.Join(b.Config.SitePath, "favicon.ico")
	if ltfile.FileExists(faviconFrom) {
		doxlog.Infof("copied %s to %s", faviconFrom, faviconTo)
		err = ltfile.CopyFile(faviconFrom, faviconTo)
		if err != nil {
			doxlog.Errorf("error copying %s to %s: %v", faviconFrom, faviconTo, err)
		}
	}
	msg = "Site assets copied"
	doxlog.Info(msg)
	b.Relay(msg)
	return nil
}
func (b *Builder) processTemplate(id string,
	content string,
	interlinear bool,
	preview bool,
	parseErrorChan chan<- []parser.ParseError,
	errorChan chan<- error,
	wg *sync.WaitGroup) {
	defer wg.Done()
	mu := sync.Mutex{}
	mu.Lock()
	defer mu.Unlock()
	var lml *parser.LML
	var atem *atempl.ATEM
	var parseErrors []parser.ParseError
	var err error
	start := time.Now()
	parms := parser.Parms{
		TemplatePath:        id,
		TemplateContent:     content,
		Realm:               b.Config.Realm,
		TheVersion:          versions.All,
		GenLangs:            b.Config.BuildLanguages,
		TheResolver:         b.Resolver,
		MediaReverseLookup:  b.MediaReverseLookup,
		TheMedia:            b.MediaMapper,
		Values:              b.ValueMapper,
		HtmlCss:             b.Config.HtmlCss,
		PdfCss:              b.Config.PdfCss,
		TheCalendarType:     b.Config.Calendar,
		TemplateDir:         b.Config.TemplatesPrimaryPath,
		YearOverride:        b.Config.YearOverride,
		FlagMissingRidTk:    b.Config.FlagMissingRidTk,
		FlagMissingRidValue: b.Config.FlagMissingRidValue,
		IncludeCovers:       b.Config.IncludeCovers,
	}
	lml, err = parser.NewLMLParser(&parms)

	if err != nil {
		errorChan <- err
	}
	atem, parseErrors = lml.WalkTemplate()
	elapsed := time.Since(start)
	_, filename := path.Split(id)
	if b.NumberOfTemplatesProcessed <= MaxTemplatesToReportWrite {
		b.Relay(fmt.Sprintf("Finished parsing template %s. Took %s", filename, elapsed.Truncate(time.Millisecond)))
	}
	if b.NumberOfTemplatesProcessed == MaxTemplatesToReportWrite {
		b.Relay("Finished parsing template ...will continue processing but not report finish times.")
	}

	// add to parse errors if there is no content in the template
	if len(atem.RowLayouts) == 0 {
		e := new(parser.ParseError)
		e.TemplateID = atem.ID
		e.Message = fmt.Sprintf("warning - template %s has no content lines", atem.ID)
		parseErrors = append(parseErrors)
	} else if len(atem.RowLayouts[0].Rows) == 0 {
		e := new(parser.ParseError)
		e.TemplateID = atem.ID
		e.Message = fmt.Sprintf("warning - template %s has no content lines", atem.ID)
		parseErrors = append(parseErrors)
	}
	nbrParseErrors := len(parseErrors)
	// Save the atem for the next time. We can reuse it if there were no parse errors.
	// 2023/10/15. MAC. Commented out for future analysis due to suspicion it causes the size of the database to increase too much.
	//prettyPrint := false
	//go b.SaveParseData(atem, nbrParseErrors == 0, prettyPrint)

	// if there are parse errors, stop processing this template
	if nbrParseErrors > 0 {
		var errorText = "errors"
		if nbrParseErrors == 1 {
			errorText = "error"
		}
		b.Relay(fmt.Sprintf("template %s has %d %s", atem.ID, len(parseErrors), errorText))
		parseErrorChan <- parseErrors
		return
	}
	var writerWg sync.WaitGroup
	year := strconv.Itoa(atem.Year)
	idPath, title := b.PathAndTitleFromTemplateID(atem.ID)
	atem.Title = title

	if atem.Type == templateTypes.Service {
		atem.FormattedDate = b.Config.LocaleDate(atem.Year, atem.Month, atem.Day, b.Config.IndexDaySubTitleDateFormat)
		atem.Title = fmt.Sprintf("%s %s", atem.Title, atem.FormattedDate)
		atem.AddKeyWord(atem.Title[:len(atem.Title)-len(atem.FormattedDate)-1])
		atem.AddKeyWord(fmt.Sprintf("%d", atem.Year))
		if atem.Office.CelebratedPreviousEvening() {
			year = strconv.Itoa(atem.VespersYear)
			idPath = b.shiftIdPathByOneDay(idPath, atem.VespersYear, atem.VespersMonth, atem.VespersDay)
		}
	}
	atem.SetIndexTitleCodes() // used by the indexer when producing the booksindex.html and servicesindex.html
	if b.Config.OutputHtml && atem.OutputType != outputTypes.PDF {
		writerWg.Add(1)
		htmlPathOut, baseHref := b.GetHtmlPathOut(atem.Type, year, idPath)
		go b.WriteHTML(&writerWg,
			atem,
			b.Config.DocNavbar,
			b.Config.Footer,
			htmlPathOut,
			baseHref,
			b.Config.Links,
			b.Config.Scripts,
			b.Config.Templates,
			interlinear,
			false,
			preview,
			b.Config.PreviewLayoutAcronymn,
			nil,
			errorChan)
	}
	if b.Config.OutputPdf && atem.OutputType != outputTypes.HTML {
		writerWg.Add(1)
		pdfPathOut := b.getPdfPathOut(atem.Type, year, idPath)
		go b.WritePDF(&writerWg, atem, pdfPathOut, b.Config, errorChan)
	}
	writerWg.Wait()
	errorChan <- nil
}
func (b *Builder) shiftIdPathByOneDay(p string, year, month, day int) (newPath string) {
	newPath = p
	parts := strings.Split(p, "/")
	if len(parts) == 3 {
		parts[0] = fmt.Sprintf("m%02d", month)
		parts[1] = fmt.Sprintf("d%02d", day)
		newPath = path.Join(parts...)
	}
	return newPath
}
func (b *Builder) TemplateIdFromPath(p string) string {
	templateId := p[len(config.DoxaPaths.PrimaryTemplatesPath)+1:]
	if strings.HasSuffix(templateId, ".lml") {
		templateId = templateId[:len(templateId)-4]
	}
	return templateId
}
func (b *Builder) WriteHTML(wg *sync.WaitGroup,
	atem *atempl.ATEM,
	navbar html.Navbar,
	footer html.Footer,
	pathOut string,
	baseHref string,
	links []html.Link,
	scripts []html.Script,
	templates *template.Template,
	interlinear bool,
	editable bool,
	preview bool,
	previewLayout string,
	urlCallbackChan chan string,
	errorChan chan<- error) {
	defer wg.Done()
	tmplName := "doc"
	if interlinear {
		tmplName = "docInterlinear"
	}
	var templateInfo []string
	for k, v := range atem.TemplatesByNbr {
		templateInfo = append(templateInfo, fmt.Sprintf("%03d: %s", k, v))
	}
	sort.Strings(templateInfo)
	// generate an html file for each tableLayout
	if atem.TableLayouts == nil {
		msg := "WriteHtml(): atem.TableLayouts is nil"
		doxlog.Error(msg)
		errorChan <- errors.New(msg)
		return
	}
	for layoutIndex, tableLayout := range atem.TableLayouts {
		if len(b.Config.LayoutsSelected) > 0 {
			var ok bool
			if ok = b.Config.LayoutsSelected[tableLayout.Acronym]; !ok {
				if previewLayout != tableLayout.Acronym {
					continue
				}
			}
		}
		metaHTML := new(html.MetaHTML)
		metaHTML.Timestamp = time.Now().Format(time.RFC1123Z)
		metaHTML.BaseHref = baseHref
		metaHTML.DoxaVersion = b.DoxaVersion
		metaHTML.Footer = footer
		metaHTML.FramesEnabled = b.Config.FramesEnabled
		metaHTML.ID = atem.IdBase
		metaHTML.IndexTitleCodes = atem.IndexTitleCodes
		metaHTML.KeyWords = atem.Keywords
		metaHTML.Language = tableLayout.Acronym
		metaHTML.Links = links
		metaHTML.LmlTemplates = templateInfo // string(templateJson)
		metaHTML.Navbar = navbar
		metaHTML.Navbar.ShowSecondIconColumn = true
		metaHTML.Navbar.HasBooksIcon = b.Config.NavbarHasBooksIcon
		metaHTML.Navbar.HasServicesIcon = b.Config.NavbarHasServicesIcon
		metaHTML.Navbar.HasSearchIcon = b.Config.NavbarHasSearchIcon
		metaHTML.Navbar.NumberOfVersions = len(tableLayout.LiturgicalLibs)
		if metaHTML.Navbar.NumberOfVersions > 0 {
			metaHTML.Navbar.TextDecrease = b.sm.StringProps[properties.SiteBuildNavbarSideBarMenuDecreaseFontSize]
			metaHTML.Navbar.TextIncrease = b.sm.StringProps[properties.SiteBuildNavbarSideBarMenuIncreaseFontSize]
		}
		//metaHTML.Navbar.NavbarTitle = fmt.Sprintf("%s - %s", metaHTML.Navbar.NavbarTitle, atem.Title)
		metaHTML.RowTypes = struct {
			Section       rowTypes.RowType
			Subsection    rowTypes.RowType
			Subsubsection rowTypes.RowType
			EndSection    rowTypes.RowType
		}{Section: rowTypes.SectionHeading1, Subsection: rowTypes.SectionHeading2, Subsubsection: rowTypes.SectionHeading3, EndSection: rowTypes.EndDiv}
		// Because the home and index pages need to be
		// able to toggle the music frame on/off, the
		// ToggleItems slice might have it already.
		// But, since this page is for services, we
		// need to remove it so that it is not first,
		//  then add it back to the end of ToggleItems.
		//The frames toggle might already be in ToggleItems.
		//
		if len(metaHTML.Navbar.ToggleItems) > 0 {
			metaHTML.Navbar.ToggleItems = []*html.ToggleItem{}
		}
		var theLanguageCodes []string
		var theLanguageNames []string
		theLanguageCodes = strings.Split(metaHTML.Language, "-")
		for _, version := range theLanguageCodes {
			var t string
			var ok bool
			if t, ok = b.Config.GetTitle(version); !ok {
				// language code not found, so use code instead of language name
				t = version
			}
			theLanguageNames = append(theLanguageNames, t)
		}
		if metaHTML.Navbar.NumberOfVersions > 1 {
			// Version CheckBox Info
			for i, version := range theLanguageNames {
				metaHTML.Navbar.ToggleItems = append(metaHTML.Navbar.ToggleItems,
					&html.ToggleItem{
						Action:   fmt.Sprintf("col%d", i),
						Disabled: false,
						Text:     version,
						AltText:  fmt.Sprintf("Hide or show %s", version),
					})
			}
			// View versions side-by-side as columns or one after the other as rows.
			text := b.Config.SM.StringProps[properties.SiteBuildNavbarSideBarMenuToggleVersionOrientation]
			metaHTML.Navbar.ToggleItems = append(metaHTML.Navbar.ToggleItems,
				&html.ToggleItem{
					Action:   "orientation",
					Disabled: false,
					Text:     text,
					AltText:  fmt.Sprintf("Hide or show %s", text),
				})
		}
		if b.Config.FramesEnabled {
			text := b.Config.SM.StringProps[properties.SiteBuildNavbarSideBarMenuToggleFrames]
			metaHTML.Navbar.ToggleItems = append(metaHTML.Navbar.ToggleItems,
				&html.ToggleItem{
					Action:   "frames",
					Disabled: true,
					Text:     text,
					AltText:  fmt.Sprintf("Hide or show %s", text),
				})
		}
		metaHTML.Rows = atem.RowLayouts[layoutIndex].Rows
		metaHTML.Scripts = scripts
		metaHTML.Templates = templates
		metaHTML.Title = atem.Title
		metaHTML.Editable = true // TODO: TBD parameter

		filename := path.Base(atem.ID)
		if strings.HasSuffix(filename, ".lml") {
			filename = filename[:len(filename)-3]
		}
		sb := strings.Builder{}
		sb.WriteString(pathOut)
		sb.WriteString("/")
		sb.WriteString(tableLayout.Acronym)
		err := ltfile.CreateDirs(sb.String())
		if err != nil {
			msg := fmt.Sprintf("error making directories for %s: %v", sb.String(), err)
			doxlog.Error(msg)
			errorChan <- fmt.Errorf(msg)
			return
		}
		sb.WriteString("/")
		sb.WriteString("index.html")
		metaHTML.PathOut = sb.String()

		err = metaHTML.CreateHTML(tmplName)
		if err != nil {
			msg := fmt.Sprintf("error writing %s: %v", tmplName, err)
			doxlog.Error(msg)
			errorChan <- fmt.Errorf(msg)
			return
		}
		indexMeta := new(indexer.IndexMeta)
		indexMeta.TemplateId = atem.ID
		indexMeta.GeneratedDate = metaHTML.Timestamp
		indexMeta.LanguageCodes = strings.Join(theLanguageCodes, "-")
		indexMeta.LanguageNames = strings.Join(theLanguageNames, "-")
		indexMeta.TitleCodes = strings.Split(atem.IndexTitleCodes, ",")
		indexMeta.LastTitleOverride = atem.IndexLastTitleCodeOverride
		indexMeta.MediaType = strings.ToLower(mediaTypes.HTML.String())
		indexMeta.MusicMedia = metaHTML.GetMusicMediaLinks()
		if atem.Type == templateTypes.Service {
			indexMeta.ServiceYear = atem.Year
			indexMeta.ServiceMonth = atem.Month
			indexMeta.ServiceDay = atem.Day
			indexMeta.ServiceType = atem.OfficeCode
		}
		indexMeta.Type = atem.Type.String()
		err = indexMeta.SetHref(metaHTML.PathOut, preview)
		if err != nil {
			msg := fmt.Sprintf("error setting href %s: %v", metaHTML.PathOut, err)
			doxlog.Error(msg)
			errorChan <- fmt.Errorf(msg)
			return
		}
		indexMetaJson, err := indexMeta.ToJson()
		if err != nil {
			msg := fmt.Sprintf("error converting indexMeta to json: %v", err)
			doxlog.Error(msg)
			errorChan <- fmt.Errorf(msg)
			return
		} else { // write the index.json file
			indexMetaPathOut := path.Dir(metaHTML.PathOut)
			indexMetaPathOut = path.Join(indexMetaPathOut, "index.json")
			err = ltfile.WriteFile(indexMetaPathOut, string(indexMetaJson))
			if err != nil {
				msg := fmt.Sprintf("error writing %s: %v", indexMetaPathOut, err)
				doxlog.Error(msg)
				errorChan <- fmt.Errorf(msg)
				return
			}
		}
		if urlCallbackChan != nil {
			urlCallbackChan <- fmt.Sprintf("wrote HTML to %s", metaHTML.PathOut)
		} else {
			if b.NumberOfTemplatesProcessed <= MaxTemplatesToReportWrite {
				b.Relay(fmt.Sprintf("wrote HTML to %s", metaHTML.PathOut))
			}
			if b.NumberOfTemplatesProcessed == MaxTemplatesToReportWrite {
				b.Relay("wrote HTML to ...will continue writing, but not reporting.")
			}
		}
	}
}
func (b *Builder) WritePDF(wg *sync.WaitGroup,
	atem *atempl.ATEM,
	pathOut string, config *models.BuildConfig,
	errorChan chan<- error) {
	defer wg.Done()
	if atem.OutputType == outputTypes.HTML {
		b.Relay(fmt.Sprintf("info - template %s output set to HTML only, so skipping PDF generation...", atem.ID))
		return
	} else if len(atem.RowLayouts) == 0 {
		b.Relay(fmt.Sprintf("warning - template %s does not have content lines", atem.ID))
		return
	} else if len(atem.RowLayouts[0].Rows) == 0 {
		b.Relay(fmt.Sprintf("warning - template %s does not have content lines", atem.ID))
		return
	}
	for layoutIndex, genLang := range atem.TableLayouts {
		if len(b.Config.LayoutsSelected) > 0 {
			var ok bool
			if ok = b.Config.LayoutsSelected[genLang.Acronym]; !ok {
				continue
			}
		}
		metaPDF := new(pdf.MetaPDF)
		metaPDF.ATEM = atem
		metaPDF.Config = config
		metaPDF.IndexTitleCodes = atem.IndexTitleCodes
		metaPDF.KeyWords = atem.Keywords
		metaPDF.GenLibs = genLang.LiturgicalLibs
		metaPDF.Rows = atem.RowLayouts[layoutIndex].Rows

		filename := path.Base(atem.ID)
		if strings.HasSuffix(filename, ".lml") {
			filename = filename[:len(filename)-3]
		}
		sb := strings.Builder{}
		sb.WriteString(pathOut)
		sb.WriteString("/")
		sb.WriteString(genLang.Acronym)
		err := ltfile.CreateDirs(sb.String())
		if err != nil {
			errorChan <- err
			return
		}
		sb.WriteString("/")
		basePath := sb.String()
		sb.WriteString(filename)
		sb.WriteString(fmt.Sprintf(".%s", genLang.Acronym))
		sb.WriteString(".pdf")
		metaPDF.LayoutIndex = layoutIndex
		metaPDF.PathOut = sb.String()
		metaPDF.Acronym = genLang.Acronym
		err = metaPDF.CreatePDF(b.MsgChan, b.RelayMuted, b.RelayUsesStandardOut, &b.MsgMap)
		if err != nil {
			errorChan <- err
			return
		}
		indexMeta := new(indexer.IndexMeta)
		indexMeta.TemplateId = atem.ID
		indexMeta.GeneratedDate = time.Now().Format(time.RFC1123Z) // atem.FormattedDate
		indexMeta.LanguageCodes = metaPDF.Acronym
		indexMeta.TitleCodes = strings.Split(atem.IndexTitleCodes, ",")
		indexMeta.LastTitleOverride = atem.IndexLastTitleCodeOverride
		indexMeta.MediaType = strings.ToLower(mediaTypes.PDF.String())
		if atem.Type == templateTypes.Service {
			indexMeta.ServiceYear = atem.Year
			indexMeta.ServiceMonth = atem.Month
			indexMeta.ServiceDay = atem.Day
			indexMeta.ServiceType = atem.OfficeCode
		}
		var theLanguageCodes []string
		var theLanguageNames []string
		theLanguageCodes = strings.Split(indexMeta.LanguageCodes, "-")
		for _, version := range theLanguageCodes {
			var t string
			var ok bool
			if t, ok = b.Config.GetTitle(version); !ok {
				// language code not found, so use code instead of language name
				t = version
			}
			theLanguageNames = append(theLanguageNames, t)
		}
		indexMeta.LanguageNames = strings.Join(theLanguageNames, "-")
		indexMeta.Type = atem.Type.String()
		err = indexMeta.SetHref(path.Join(basePath, "index.html"), false)
		if err != nil {
			errorChan <- err
		}

		indexMetaJson, err := indexMeta.ToJson()
		if err != nil {
			errorChan <- err
		} else { // write the index.json file
			indexMetaPathOut := path.Dir(metaPDF.PathOut)
			indexMetaPathOut = path.Join(indexMetaPathOut, "index.json")
			ltfile.WriteFile(indexMetaPathOut, string(indexMetaJson))
		}

		if b.NumberOfTemplatesProcessed <= MaxTemplatesToReportWrite {
			b.Relay(fmt.Sprintf("wrote PDF to %s", metaPDF.PathOut))
		}
		if b.NumberOfTemplatesProcessed == MaxTemplatesToReportWrite {
			b.Relay("wrote PDF to ...will continue writing, but not reporting.")
		}
	}
}
func (b *Builder) deleteBooksIndex() error {
	if b.HasBooksIndex() {
		err := ltfile.DeleteFile(path.Join(b.Config.SitePath, "booksindex.html"))
		if err != nil {
			return err
		}
	}
	return nil
}

// filter for only files that have name 'index'
var expressions = []string{
	"index",
}

func (b *Builder) HasBookFiles() bool {
	return b.has("h", "b", "html") || b.has("p", "b", "html")
}
func (b *Builder) has(fileType string, liturgicalType string, extension string) bool {
	files, _ := ltfile.FileMatcher(path.Join(b.Config.SitePath, fileType, liturgicalType), extension, expressions)
	return len(files) > 0
}
func (b *Builder) HasBooksIndex() bool {
	return ltfile.FileExists(path.Join(b.Config.SitePath, "booksindex.html"))
}
func (b *Builder) HasServiceFiles() bool {
	return b.has("h", "s", "html") || b.has("p", "s", "html")
}
func (b *Builder) HasServicesIndex() bool {
	return ltfile.FileExists(path.Join(b.Config.SitePath, "servicesindex.html"))
}
func (b *Builder) deleteServicesIndex() error {
	if b.HasServicesIndex() {
		err := ltfile.DeleteFile(path.Join(b.Config.SitePath, "servicesindex.html"))
		if err != nil {
			return err
		}
	}
	return nil
}

// WriteAuxPages creates various html pages needed by the site:
// the books and services indexes, and the site index.html.
// If frames are being used, it also creates blank.html for the scores iFrame,
// dcs.html (for the 3 frames: text, scores, audio), and home.html.
// If frames are being used, the index.html will autodetect mobile vs desktop.
func (b *Builder) WriteAuxPages() error {
	funcId := "pkg/site/site.go/builder.WriteAuxPages: "
	var err error
	var msg string
	var hasBookFiles = b.HasBookFiles()
	var hasServiceFiles = b.HasServiceFiles()
	b.CopyAppDoxaCssJs()
	// copy all html files in the assets folder
	// If the home page (index.html) and help.html did not exist, they will have
	// been generated and put into the sites dir already.
	htmlFiles, err := ltfile.FileMatcher(b.Config.AssetsRootPath, "html", nil)
	if err != nil {
		return err
	}
	msg = fmt.Sprintf("Copying static html files from %s", b.Config.AssetsRootPath)
	doxlog.Info(msg)
	b.Relay(msg)
	for _, file := range htmlFiles {
		_, filename := path.Split(file)
		to := path.Join(b.Config.SitePath, filename)
		doxlog.Infof("copying %s", file)
		doxlog.Infof("to %s", to)
		err = ltfile.CopyFile(file, to)
		if err != nil {
			msg = fmt.Sprintf("error copying %s to %s: %v", file, to, err)
			doxlog.Error(msg)
			b.Relay(msg)
			continue
		}
	}
	faviconFrom := path.Join(b.Config.AssetsRootPath, "favicon.ico")
	faviconTo := path.Join(b.Config.SitePath, "favicon.ico")
	if ltfile.FileExists(faviconFrom) {
		err = ltfile.CopyFile(faviconFrom, faviconTo)
		if err != nil {
			doxlog.Errorf("error copying %s to %s: %v", faviconFrom, faviconTo, err)
		} else {
			doxlog.Infof("copied %s to %s", faviconFrom, faviconTo)
		}
	}

	if !hasBookFiles && !hasServiceFiles {
		msg = fmt.Sprintf("%serror: no generated book or service files found", funcId)
		b.Relay(msg)
		return fmt.Errorf(msg)
	}
	// Prepare to see if user has custom books index
	// in the assets/rootFiles folder
	rootBooksIndexExists := ltfile.FileExists(path.Join(b.Config.AssetsRootPath, "booksindex.html"))

	if b.Config.IndexBooks && hasBookFiles && !rootBooksIndexExists {
		err = b.deleteBooksIndex()
		if err != nil {
			msg = fmt.Sprintf("%serror removing previous books index: %v", funcId, err)
			b.Relay(msg)
		}
		var messages []string
		messages, err = indexer.IndexBooks(b.Config)
		if err != nil {
			msg = fmt.Sprintf("%serror removing previous books index: %v", funcId, err)
			b.Relay(msg)
		}
		if len(messages) > 0 {
			for _, m := range messages {
				b.Relay(m)
			}
		}
	}
	if b.Config.IndexServices && hasServiceFiles {
		err = b.deleteServicesIndex()
		if err != nil {
			msg = fmt.Sprintf("%serror removing previous services index: %v", funcId, err)
			b.Relay(msg)
		}
		var indexErrors []error
		indexErrors = indexer.IndexServices(b.Config)
		if len(indexErrors) > 0 {
			sb := strings.Builder{}
			for _, e := range indexErrors {
				sb.WriteString(fmt.Sprintf("%v\n", e))
			}
			msg = fmt.Sprintf("%serror creating services index: %s", funcId, sb.String())
			b.Relay(msg)
			return fmt.Errorf(msg)
		}
	}
	b.Config.NavbarHasBooksIcon = b.HasBooksIndex()
	b.Config.NavbarHasServicesIcon = b.HasServicesIndex()
	b.Config.HomePage.HasBooks = b.Config.NavbarHasBooksIcon
	b.Config.HomePage.HasServices = b.Config.NavbarHasServicesIcon

	if !ltfile.FileExists(path.Join(b.Config.AssetsRootPath, "help.html")) {
		doxlog.Info("writing help.html")
		err = b.Config.HelpPage.WriteHtml()
		if err != nil {
			msg = fmt.Sprintf("%shelp.html does not exist", funcId)
			b.Relay(msg)
		}
	}

	err = b.Config.SearchPage.WriteHtml()
	if err != nil {
		msg = fmt.Sprintf("%serror writing search page: %v", funcId, err)
		b.Relay(msg)
	}

	if b.Config.FramesEnabled { // generate a home.html and blank.html page
		doxlog.Info("frames enabled")
		// home.html will be used for mobile
		b.Config.HomePage.FramesEnabled = false // so it will have a navbar
		if !ltfile.FileExists(path.Join(b.Config.AssetsRootPath, "home.html")) {
			err = b.Config.HomePage.WriteHtml()
			if err != nil {
				msg = fmt.Sprintf("%serror home.html: %v", funcId, err)
				b.Relay(msg)
			}
		}

		// blank.html will be used for iframe FrameScores
		customBlankExists := ltfile.FileExists(path.Join(b.Config.AssetsRootPath, "blank.html"))
		// create it if the user does not have a custom one
		if !customBlankExists {
			doxlog.Info("writing blank.html")
			err = b.Config.ScoreFrame.WriteHtml()
			if err != nil {
				msg = fmt.Sprintf("%serror blank.html: %v", funcId, err)
				b.Relay(msg)
			}
		}
		if !ltfile.FileExists(path.Join(b.Config.AssetsRootPath, "dcs.html")) {
			// write dcs.html (has iFrames)
			doxlog.Info("writing dcs.html")
			err = b.Config.DcsPage.WriteHtml()
			if err != nil {
				msg = fmt.Sprintf("%serror dcs.html: %v", funcId, err)
				b.Relay(msg)
			}
		}
		// write index.html, which detects mobile vs desktop and redirects to home or dcs.html.
		err = b.Config.DevicePage.WriteHtml()
		if err != nil {
			msg = fmt.Sprintf("%serror writing site index.html: %v", funcId, err)
			b.Relay(msg)
		}
	} else { // remove frames related html files
		doxlog.Info("frames not enabled")
		if ltfile.FileExists(b.Config.DcsPage.PathOut) {
			_ = os.Remove(b.Config.DcsPage.PathOut)
		}
		theHomePage := path.Join(b.Config.SitePath, "home.html")
		if ltfile.FileExists(theHomePage) {
			_ = os.Remove(theHomePage)
		}
		homePage := path.Join(b.Config.SitePath, "home.html")
		if ltfile.FileExists(homePage) {
			_ = os.Remove(homePage)
		}
		b.Config.HomePage.HasBooks = b.Config.NavbarHasBooksIcon
		b.Config.HomePage.HasServices = b.Config.NavbarHasServicesIcon
		indexHtml := path.Join(b.Config.AssetsRootPath, "index.html")
		if !ltfile.FileExists(indexHtml) {
			doxlog.Info("writing index.html")
			err = b.Config.HomePage.WriteHtml()
			if err != nil {
				msg = fmt.Sprintf("%serror writing home.html: %v", funcId, err)
				b.Relay(msg)
			}
		}
	}
	return nil
}

type SearchPageLabels struct {
	DbId           string // pages.db.id
	Desc           string // page.search.desc
	Exact          string // page.search.exact
	FoundPlural    string // page.search.found.plural
	FoundSingular  string // page.search.found.sing
	NotAvailable   string // page.search.not.available
	Of             string // page.search.of
	Page           string // page.search.page
	PageSize       string // page.search.page.size
	PlaceHolder    string // page.search.place.holder
	ShowingMatches string // page.search.showing.matches
	Source         string // page.search.source
	Status         string // page.search.status
	TitleTab       string // page.search.title.tab
	TitlePage      string // page.search.title.page
	To             string // page.search.to
	WholeWord      string // page.search.whole.word
}

// writeSearchLabelsJs - reads db config dir and outputs doxa-search-labels.js
func (b *Builder) writeSearchLabelJs() error {
	l := new(SearchPageLabels)
	var ok bool
	if l.DbId, ok = b.Config.GetTitle("page.search.db.id"); !ok {
		l.DbId = "Doxa Database ID"
	}
	if l.Desc, ok = b.Config.GetTitle("page.search.desc"); !ok {
		l.Desc = "Books and services are available in the languages shown in the dropdown below. Select the language to use for the search. Enter a word, phrase, or regular expression to search for, then press the Enter or Return key. Matching texts are displayed a page at a time. You can change the number of matches shown per page. Note that regular expression word boundaries do not work with languages such as Greek. So, use the 'Whole Word' option instead. When you click on a link, the book or service will be displayed, and the page will be scrolled so that the matched text is at the top. If the matched text occurs at or near the bottom of the page, it won't be at the top, but somewhere below that. At this time, it is not possible to highlight the matched text in a book or service opening from a link on the search page. At the bottom of each matched text, there is information about the liturgical source of the text and the Doxa database ID. If you click the database ID, you can view the Greek source text, translations, grammatical information, and notes."
	}
	if l.Exact, ok = b.Config.GetTitle("page.search.exact"); !ok {
		l.Exact = "Exact"
	}
	if l.FoundPlural, ok = b.Config.GetTitle("page.search.found.plural"); !ok {
		l.FoundPlural = "found"
	}
	if l.FoundSingular, ok = b.Config.GetTitle("page.search.found.sing"); !ok {
		l.FoundSingular = "found"
	}
	if l.NotAvailable, ok = b.Config.GetTitle("page.search.not.available"); !ok {
		l.NotAvailable = "Not Available"
	}
	if l.Of, ok = b.Config.GetTitle("page.search.of"); !ok {
		l.Of = "of"
	}
	if l.Page, ok = b.Config.GetTitle("page.search.page"); !ok {
		l.Page = "Body"
	}
	if l.PageSize, ok = b.Config.GetTitle("page.search.page.size"); !ok {
		l.PageSize = "Body size"
	}
	if l.PlaceHolder, ok = b.Config.GetTitle("page.search.place.holder"); !ok {
		l.PlaceHolder = "Enter a word, phrase, or regular expression, and press the Enter or Return key"
	}
	if l.ShowingMatches, ok = b.Config.GetTitle("page.search.showing.matches"); !ok {
		l.ShowingMatches = "Showing matches"
	}
	if l.Source, ok = b.Config.GetTitle("page.search.source"); !ok {
		l.Source = "Liturgical Source"
	}
	if l.Status, ok = b.Config.GetTitle("page.search.status"); !ok {
		l.Status = "Status"
	}
	if l.TitleTab, ok = b.Config.GetTitle("page.search.title.tab"); !ok {
		l.TitleTab = "Search"
	}
	if l.TitlePage, ok = b.Config.GetTitle("page.search.title.page"); !ok {
		l.TitlePage = "Search Books and Services"
	}
	if l.To, ok = b.Config.GetTitle("page.search.to"); !ok {
		l.To = "To1"
	}
	if l.WholeWord, ok = b.Config.GetTitle("page.search.whole.word"); !ok {
		l.WholeWord = "Whole Word"
	}
	j, err := json.Marshal(l)
	content := fmt.Sprintf("export const labels = %s;", string(j))
	if err != nil {
		return err
	}
	err = ltfile.WriteFile(path.Join(b.Config.SitePath, "js", "wc", "doxa-search-labels.js"), content)
	if err != nil {
		msg := fmt.Sprintf("error writing labels for search page: %v", err)
		doxlog.Error(msg)
		return fmt.Errorf("%s", msg)
	}
	return nil
}
func (b *Builder) WriteSearchPages(ctx context.Context) {
	// MAC 2024-0814 - takes up too much space in generated website
	//funcId := "pkg/site/site.go/builder.WriteSearchPages: "
	//var err error
	//var msg string
	//b.Relay("writing search pages")
	//
	//b.IndexMutex.Lock()
	//defer b.IndexMutex.Unlock()
	//// write the labels for the search page
	//err = b.writeSearchLabelJs()
	//if err != nil {
	//	msg = fmt.Sprintf("error writing language labels for search page: %v", err)
	//	doxlog.Errorf(msg)
	//	b.Relay(msg)
	//}
	//// write the search json files
	//i, err := search.NewIndexer(b.Config, ReferenceDir, LiturgicalDir, b.sm.Kvs)
	//if err != nil {
	//	if !b.Config.OutputHtml { // no html to index, so search function not available
	//		err = i.WriteEmptyIndexFile()
	//		if err != nil {
	//			msg = fmt.Sprintf("error writing empty index file: %v", err)
	//			b.Relay(msg)
	//		}
	//		msg = fmt.Sprintf("%sno html to index", funcId)
	//		b.Relay(msg)
	//		return
	//	}
	//	msg = fmt.Sprintf("%serror creating new search indexer: %v", funcId, err)
	//	b.Relay(msg)
	//}
	//if err = i.Index(); err != nil {
	//	msg = fmt.Sprintf("%serror creating search indexes: %v", funcId, err)
	//	b.Relay(msg)
	//}
	//if ltfile.DirExists(i.LiturgicalPath) {
	//	err = ltfile.DeleteDirRecursively(i.LiturgicalPath)
	//	if err != nil {
	//		if err != nil {
	//			msg = fmt.Sprintf("%serror deleting %s: %v", funcId, i.LiturgicalPath, err)
	//			b.Relay(msg)
	//		}
	//	}
	//}
	//// prepare page that will list all the topics in the index
	//tIdx := new(html.LtxTopicKeysIndex)
	//tIdx.BaseHref = "../.."
	//tIdx.Footer = b.Config.TopicKeyIndexPage.Footer
	//tIdx.Links = b.Config.TopicKeyIndexPage.Links
	//tIdx.Name = b.Config.TopicKeyIndexPage.Name
	//tIdx.Navbar = b.Config.TopicKeyIndexPage.Navbar
	//tIdx.PathOut = filepath.Join(b.Config.SitePath, ReferenceDir, LiturgicalDir)
	//err = ltfile.CreateDirs(tIdx.PathOut)
	//if err != nil {
	//	msg = fmt.Sprintf("%serror creating directories %s: %v", funcId, tIdx.PathOut, err)
	//	b.Relay(msg)
	//	return
	//}
	//tIdx.PathOut = filepath.Join(tIdx.PathOut, "index.html")
	//tIdx.Scripts = b.Config.TopicKeyIndexPage.Scripts
	//tIdx.Templates = b.Config.TopicKeyIndexPage.Templates
	//tIdx.Title = "Database Topics Index" // TODO: make a config property
	//tIdx.Desc = []string{"This site was generated using Doxa.  Doxa stores liturgical texts in database records.  The identifier of each liturgical record is a unique combination of library, topic, and key. Below is an index of the topics part of the record identifiers."}
	//var theTopic, theKey, theTopicDesc string
	//var tkIdx *html.LtxTopicKeysIndex
	//// write topic-key pages
	//for _, tk := range i.GetTopicKeyHrefs() {
	//	tk := tk // e.g., ltx/actors/Priest
	//	parts := strings.Split(tk, "/")
	//	if len(parts) != 3 {
	//		msg = fmt.Sprintf("%serror parsing %s: %v", funcId, tk, err)
	//		b.Relay(msg)
	//		return
	//	}
	//	// if the topic has changed, write out the topic index and create a new instance for the next topic
	//	if theTopic != parts[1] {
	//		// add this topic to the topic index
	//		ltk := new(html.LtxTopicKey)
	//		ltk.Desc = fmt.Sprintf("%s (%s)", theTopicDesc, theTopic)
	//		ltk.Href = filepath.Join(ReferenceDir, LiturgicalDir, theTopic, "index.html")
	//		tIdx.IndexLinks = append(tIdx.IndexLinks, ltk)
	//
	//		// write out previous topic if exists
	//		if tkIdx != nil {
	//			sort.Slice(tkIdx.IndexLinks, tkIdx.CompareLinks)
	//			err = tkIdx.WriteHtml()
	//			if err != nil {
	//				msg = fmt.Sprintf("%serror writing %s: %v", funcId, tkIdx.PathOut, err)
	//				b.Relay(msg)
	//			}
	//		}
	//		// create instance for new topic
	//		theTopic = parts[1]
	//		theTopicDesc = b.Config.TopicDesc(theTopic, theKey)
	//		tkIdx = new(html.LtxTopicKeysIndex)
	//		tkIdx.BaseHref = "../../.."
	//		tkIdx.Footer = b.Config.TopicKeyIndexPage.Footer
	//		tkIdx.Links = b.Config.TopicKeyIndexPage.Links
	//		tkIdx.Name = b.Config.TopicKeyIndexPage.Name
	//		tkIdx.Navbar = b.Config.TopicKeyIndexPage.Navbar
	//		tkIdx.PathOut = filepath.Join(b.Config.SitePath, ReferenceDir, LiturgicalDir, theTopic)
	//		err = ltfile.CreateDirs(tkIdx.PathOut)
	//		if err != nil {
	//			msg = fmt.Sprintf("%serror creating directories %s: %v", funcId, tkIdx.PathOut, err)
	//			b.Relay(msg)
	//			return
	//		}
	//		tkIdx.PathOut = filepath.Join(tkIdx.PathOut, "index.html")
	//		tkIdx.Scripts = b.Config.TopicKeyIndexPage.Scripts
	//		tkIdx.Templates = b.Config.TopicKeyIndexPage.Templates
	//		tkIdx.Title = "Database Topics Index" // TODO: make a config property
	//		tkIdx.Desc = []string{"This site was generated using Doxa.  Doxa stores liturgical texts in database records.  The identifier of each liturgical record is a unique combination of library, topic, and key. Below is an index of the topics part of the record identifiers."}
	//	}
	//	theKey = parts[2]
	//	tkcs := new(html.TopicKeyCompare)
	//	tkcs.BaseHref = "../../../../"
	//	if b.Config != nil && b.Config.TopicKeyComparePage != nil {
	//		tkcs.Footer = b.Config.TopicKeyComparePage.Footer
	//		tkcs.Links = b.Config.TopicKeyComparePage.Links
	//		tkcs.Name = b.Config.TopicKeyComparePage.Name
	//		tkcs.Navbar = b.Config.TopicKeyComparePage.Navbar
	//	} else {
	//		msg = fmt.Sprintf("%serror b.Config.TopicKeyComparePage is nil", funcId)
	//		b.Relay(msg)
	//		return
	//	}
	//	tkcs.PathOut = filepath.Join(b.Config.SitePath, ReferenceDir, tk)
	//	err := ltfile.CreateDirs(tkcs.PathOut)
	//	if err != nil {
	//		msg = fmt.Sprintf("%serror creating directories %s: %v", funcId, tkcs.PathOut, err)
	//		b.Relay(msg)
	//		return
	//	}
	//	tkcs.PathOut = filepath.Join(tkcs.PathOut, "index.html")
	//	tkcs.Scripts = b.Config.TopicKeyComparePage.Scripts
	//	tkcs.Templates = b.Config.TopicKeyComparePage.Templates
	//	tkcs.TopicKey = fmt.Sprintf("%s/%s", theTopic, theKey)
	//	tkcs.Title = theTopicDesc
	//	matcher := kvs.NewMatcher()
	//	matcher.KP.Dirs.Push(parts[0])
	//	matcher.KP.Dirs.Push(theTopic)
	//	matcher.KP.KeyParts.Push(theKey)
	//	matcher = matcher.TopicKeyMatcher()
	//	recs, _, err := b.sm.Kvs.Db.GetMatching(*matcher)
	//	if err != nil {
	//		msg = fmt.Sprintf("%serror comparing %s: %v", funcId, tk, err)
	//		b.Relay(msg)
	//		return
	//	}
	//	// forget about this topic-key if no records found
	//	if len(recs) == 0 {
	//		continue
	//	}
	//	// add this key to the topic index
	//	ltk := new(html.LtxTopicKey)
	//	ltk.Desc = fmt.Sprintf(theKey)
	//	ltk.Href = filepath.Join(ReferenceDir, LiturgicalDir, theTopic, theKey, "index.html")
	//	tkIdx.IndexLinks = append(tkIdx.IndexLinks, ltk)
	//
	//	// continue creating the data required to write the topic-key-comparison
	//
	//	tMap := make(map[string]*html.Translation)
	//	for _, r := range recs {
	//		// exclude empty value or a redirect
	//		if len(r.Value) == 0 || r.IsRedirect() {
	//			continue
	//		}
	//		var t *html.Translation
	//		var ok bool
	//		if t, ok = tMap[r.Value]; !ok {
	//			t = new(html.Translation)
	//			t.Value = r.Value
	//		}
	//		lib := r.KP.Dirs.Get(1)
	//		ver := b.Config.Versions[lib]
	//		if len(t.Library) == 0 {
	//			if len(ver) > 0 {
	//				t.Ver = ver
	//			}
	//			t.Library = lib
	//		} else {
	//			if len(ver) > 0 {
	//				t.Ver = t.Ver + ", " + ver
	//			}
	//			t.Library = t.Library + ", " + lib
	//		}
	//		tMap[t.Value] = t
	//	}
	//	for _, t := range tMap {
	//		// we want Greek to be the last item listed in the generated index.html, so treat it separately
	//		if strings.HasPrefix(t.Library, "gr_") {
	//			tkcs.Greek = append(tkcs.Greek, t)
	//		} else {
	//			tkcs.Translations = append(tkcs.Translations, t)
	//		}
	//	}
	//	sort.Slice(tkcs.Translations, tkcs.CompareTranslationLibraries)
	//	err = tkcs.WriteHtml()
	//	if err != nil {
	//		msg = fmt.Sprintf("%serror writing %s: %v", funcId, tkcs.PathOut, err)
	//		b.Relay(msg)
	//		return
	//	}
	//}
	//sort.Slice(tIdx.IndexLinks, tIdx.CompareLinks)
	//err = tIdx.WriteHtml()
	//if err != nil {
	//	msg = fmt.Sprintf("%serror writing %s: %v", funcId, tIdx.PathOut, err)
	//	b.Relay(msg)
	//	return
	//}
	//// MAC added 10-02-2023
	//err = i.RemoveRefDir()
	//if err != nil {
	//	msg = fmt.Sprintf("error removing refs dir: %s", err)
	//	b.Relay(msg)
	//}
	//b.Relay("finished writing search pages")
}

/*d
Website structure:
realm
|--css
|--h 	// html
   |--b // books
      |-- baptism
          |--en
             |--index.html
          |--gr-en
             |--index.html
		etc.
   |--c // custom
   |--s	// services
      |-- 2020
          |-- 06
              |-- 01
                  |-- h91
                  |-- li
                      |-- en
                          |-- index.html
                      |-- gr
                          |-- index.html
                      |-- gr-en            // etc. for each combination of libraries
                          |-- index.html
                  |-- li2
                  |-- ma
                  |-- ve
              |-- 02 // etc for each day of the month
          |-- 07
          |-- 08
|--indexes
|--img
|--js
|--p	// pdf
   |--b // books
      |-- 2020
          |-- 06
              |-- 01
                  |-- h91
                  |-- li
                      |-- en
                          |-- index.html
                          |-- se.m06.d01.li.pdf
                      |-- gr
                          |-- index.html
                          |-- se.m06.d01.li.pdf
                      |-- gr-en            // etc. for each combination of libraries
                          |-- index.html
                          |-- se.m06.d01.li.pdf
                  |-- li2
                  |-- ma
                  |-- ve
              |-- 02 // etc for each day of the month
          |-- 07
          |-- 08
   |--c // custom
   |--s	// services

The index.html file in the PDF directory exists so that
a URL path to the child dir in the PDF dir will automatically
get and display the PDF.
*/

// GetHtmlPathOut returns the path to the index.html file and the baseHref for relative Paths to css and js.
func (b *Builder) GetHtmlPathOut(templateType templateTypes.TemplateType, year string, idPath string) (string, string) {
	pathParts := strings.Split(idPath, "/")
	pathPartsLength := len(pathParts)
	sb := strings.Builder{}
	for i := 0; i < pathPartsLength+4; i++ {
		sb.WriteString("../")
	}
	baseHref := sb.String() // e.g. ../../../ to reach js and css
	switch templateType {
	case templateTypes.Book:
		idParts := strings.Split(idPath, ".")
		idPartsLength := len(idParts)
		pathOut := path.Join(b.Config.SitePath, "h", "b")
		for i := 0; i < idPartsLength; i++ {
			pathOut = path.Join(pathOut, idParts[i])
		}
		return pathOut, baseHref
	case templateTypes.Service:
		normalizedPath := idPath
		if strings.HasPrefix(normalizedPath, "m") {
			normalizedPath = normalizedPath[1:]
		}
		normalizedPath = fmt.Sprintf("%s%s", normalizedPath[0:3], normalizedPath[4:])
		return path.Join(b.Config.SitePath, "h", "s", year, normalizedPath), baseHref
	default: // custom
		return path.Join(b.Config.SitePath, "h", "c", year, idPath), baseHref
	}
}

// GetHtmlTempPathOut returns the path to the index.html file and the baseHref for relative Paths to css and js when used by the singleton parser
func (b *Builder) GetHtmlTempPathOut(templateType templateTypes.TemplateType, year string, idPath string) (string, string) {
	// when using the LML editor, special handling is required to get the href correct for blocks and services.
	// The generator does not write index.html that is for a block.  But the parser for the LML editor does.
	pathParts := strings.Split(idPath, "/")
	pathPartsLength := len(pathParts)
	sb := strings.Builder{}
	for i := 0; i < pathPartsLength+4; i++ {
		sb.WriteString("../")
	}
	baseHref := sb.String() // e.g. ../../../ to reach js and css
	switch templateType {
	case templateTypes.Block:
		idParts := strings.Split(idPath, ".")
		idPartsLength := len(idParts)
		pathOut := path.Join(b.Config.PreviewPath, "h", "k")
		for i := 0; i < idPartsLength; i++ {
			pathOut = path.Join(pathOut, idParts[i])
		}
		return pathOut, baseHref
	case templateTypes.Book:
		idParts := strings.Split(idPath, ".")
		idPartsLength := len(idParts)
		pathOut := path.Join(b.Config.PreviewPath, "h", "b")
		for i := 0; i < idPartsLength; i++ {
			pathOut = path.Join(pathOut, idParts[i])
		}
		return pathOut, baseHref
	case templateTypes.Service:
		idParts := strings.Split(idPath, ".")
		idPartsLength := len(idParts)
		pathOut := path.Join(b.Config.PreviewPath, "h", "s")
		for i := 0; i < idPartsLength; i++ {
			pathOut = path.Join(pathOut, idParts[i])
		}
		return pathOut, baseHref
	default: // custom
		return path.Join(b.Config.PreviewPath, "h", "c", year, idPath), baseHref
	}
}

func (b *Builder) getPdfPathOut(templateType templateTypes.TemplateType, year string, idPath string) string {
	switch templateType {
	case templateTypes.Book:
		idParts := strings.Split(idPath, ".")
		idPartsLength := len(idParts)
		pathOut := path.Join(b.Config.SitePath, "p", "b")
		for i := 0; i < idPartsLength; i++ {
			pathOut = path.Join(pathOut, idParts[i])
		}
		return pathOut
	case templateTypes.Service:
		normalizedPath := idPath
		if strings.HasPrefix(normalizedPath, "m") {
			normalizedPath = normalizedPath[1:]
		}
		normalizedPath = fmt.Sprintf("%s%s", normalizedPath[0:3], normalizedPath[4:])
		return path.Join(b.Config.SitePath, "p", "s", year, normalizedPath)
	default:
		normalizedPath := idPath
		if strings.HasPrefix(normalizedPath, "m") {
			normalizedPath = normalizedPath[1:]
		}
		normalizedPath = fmt.Sprintf("%s%s", normalizedPath[0:3], normalizedPath[4:])
		return path.Join(b.Config.SitePath, "p", "c", year, normalizedPath)
	}
}
func (b *Builder) PathAndTitleFromTemplateID(templateID string) (string, string) {
	_, file := path.Split(templateID)
	if strings.HasSuffix(file, ".lml") {
		file = file[:len(file)-3]
	}
	file = file[2:] // get rid of template type for path
	parts := strings.Split(file, ".")
	title, _ := b.Config.GetTitle(parts[len(parts)-1])
	// 2023.10.10 MAC change return path.Join to return filepath.Join
	return path.Join(parts...), title
}

// GetSerializedMetaAtem
// just provide the templateID, not the dir path
func (b *Builder) GetSerializedMetaAtem(id string) (metaAtem *sys.MetaData, err error) {
	kp := sys.MetaDataKeyPath(id)
	var dbr *kvs.DbR
	dbr, err = b.Mapper.Db.Get(kp)
	if err != nil {
		msg := fmt.Sprintf("error getting MetaData for %s: %v", id, err)
		doxlog.Error(msg)
		return nil, fmt.Errorf(msg)
	}
	var metaData *sys.MetaData
	metaData, err = sys.MetaAtemFromJson(dbr.Value)
	if err != nil {
		msg := fmt.Sprintf("error parsing json MetaData for %s: %v", id, err)
		doxlog.Error(msg)
		return nil, fmt.Errorf(msg)
	}
	return metaData, err
}
func (b *Builder) GetSerializedAtem(id string) (atem *atempl.ATEM, noErrors bool) {
	kp := sys.MetaDataKeyPath(id)
	var dbr *kvs.DbR
	dbr, err := b.Mapper.Db.Get(kp)
	if err != nil {
		doxlog.Error(fmt.Sprintf("error getting MetaData for %s: %v", id, err))
		return nil, false
	}
	var metaData *sys.MetaData
	metaData, err = sys.MetaAtemFromJson(dbr.Value)
	if err != nil {
		doxlog.Error(fmt.Sprintf("error parsing json MetaData for %s: %v", id, err))
		return nil, false
	}
	if !metaData.NoErrors {
		return nil, false
	}
	noErrors = true
	kp = sys.MetaAtemKeyPath(id)
	dbr, err = b.Mapper.Db.Get(kp)
	if err != nil {
		doxlog.Error(fmt.Sprintf("error getting json Atem for %s: %v", id, err))
		return nil, false
	}
	atem, err = atempl.NewAtemFromJson(dbr.Value)
	if err != nil {
		doxlog.Error(fmt.Sprintf("error parsing json Atem for %s: %v", id, err))
		return nil, false
	}
	return atem, noErrors
}
func (b *Builder) SaveParseData(atem *atempl.ATEM, noErrors bool, prettyPrint bool) {
	var err error
	var errorMsg string
	var ma sys.MetaData
	var maDbr *kvs.DbR
	if atem.Type != templateTypes.Block && noErrors {
		title := atem.Title
		if atem.Type == templateTypes.Service {
			if len(title) > len(atem.FormattedDate) {
				title = atem.Title[:len(atem.Title)-len(atem.FormattedDate)-2]
			}
		}
		ma = sys.MetaData{ID: atem.ID, LastParse: time.Now(), NoErrors: noErrors, Title: title, Date: atem.FormattedDate, Type: atem.Type, ErrorMsg: errorMsg}
		var maJson string
		maJson, err = ma.ToJson(prettyPrint)
		if err != nil {
			doxlog.Error(fmt.Sprintf("%v", err))
		} else {
			maDbr = kvs.NewDbR()
			maDbr.KP = sys.MetaDataKeyPath(atem.ID)
			maDbr.Value = maJson
			err = b.Mapper.Db.Put(maDbr)
			if err != nil {
				doxlog.Error(fmt.Sprintf("%v", err))
				return
			}
		}
		// serialize the ATEM
		var jsonAtem string
		jsonAtem, err = atem.Json(prettyPrint)
		if err != nil {
			errorMsg = fmt.Sprintf("could not marshal %s to json string: %v", atem.ID, err)
			noErrors = false
			doxlog.Error(fmt.Sprintf("%v", err))
		} else {
			dbr := kvs.NewDbR()
			dbr.KP = sys.MetaAtemKeyPath(atem.ID)
			dbr.Value = jsonAtem
			err = b.Mapper.Db.Put(dbr)
			if err != nil {
				errorMsg = fmt.Sprintf("could not save %s to database: %v", dbr.KP.Path(), err)
				noErrors = false
				doxlog.Error(fmt.Sprintf("%v", err))
				ma = sys.MetaData{ID: atem.ID, LastParse: time.Now(), NoErrors: noErrors, Title: title, Date: atem.FormattedDate, Type: atem.Type, ErrorMsg: errorMsg}
				err = b.Mapper.Db.Put(maDbr)
				if err != nil {
					doxlog.Error(fmt.Sprintf("%v", err))
					return
				}
			} else {
				// verify we can deserialize it
				var ok bool
				if _, ok = b.GetSerializedAtem(atem.ID); !ok {
					errorMsg = fmt.Sprintf("could not deserialize %s: %v", dbr.KP.Path(), err)
					noErrors = false
					doxlog.Error(errorMsg)
					ma = sys.MetaData{ID: atem.ID, LastParse: time.Now(), NoErrors: noErrors, Title: title, Date: atem.FormattedDate, Type: atem.Type, ErrorMsg: errorMsg}
					err = b.Mapper.Db.Put(maDbr)
					if err != nil {
						doxlog.Error(fmt.Sprintf("%v", err))
						return
					}
				}
			}
		}
	} else if !noErrors {
		kp := sys.MetaDataKeyPath(atem.ID)
		if b.Mapper.Db.Exists(kp) {
			err = b.Mapper.Db.Delete(*kp)
			if err != nil {
				doxlog.Error(fmt.Sprintf("error deleting meta data for %s: %v", atem.ID, err))
				return
			}
		}
	}
}

const servicesIndex = "servicesindex.html"
const metaAuthorDoxa = "meta name=\"author\" content=\"doxa\""

type Comparer struct {
	Sites         []*Site
	CombinedPaths mapset.Set[string]
	IdStats       []*DbIdStats
	Intersect     bool // if true, reports only htmlPaths
	// that are common to both sites.
	// If false, reports the union of the htmlPaths across the two sites.
	Matcher *regexp.Regexp // used to filter htmlPaths
}
type Site struct {
	Url              *url.URL
	BaseUrl          string
	ServicesIndexUrl string
	Matcher          *regexp.Regexp
	DoxaGenerated    bool
	ServiceUrls      []string
}

// DbIdStats is a denormalized report of the use of each Database ID by two sites.
// It is denormalized in the sense that the service info repeats across occurrences.
// The values are designed for use as rows in a table.
type DbIdStats struct {
	ServiceDate   string
	ServiceType   string
	ServiceLayout string
	DbId          string
	CountSite1    int  // number of times this DbId occurs in site 1
	CountSite2    int  // number of times this DbId occurs in site 2
	CountsMatch   bool // does CountSite1 == CountSite2?
}

func NewComparer(url1, url2, htmlPathRegEx string, intersect bool) (*Comparer, error) {
	c := new(Comparer)
	var err error
	c.Intersect = intersect
	c.Matcher, err = regexp.Compile(htmlPathRegEx)
	if err != nil {
		return nil, fmt.Errorf("error compiling regex %s: %v", htmlPathRegEx, err)
	}
	if err = c.AddSite(url1, c.Matcher); err != nil {
		return nil, err
	}
	if err = c.AddSite(url2, c.Matcher); err != nil {
		return nil, err
	}
	return c, nil
}
func (c *Comparer) AddSite(theUrl string, matcher *regexp.Regexp) error {
	s := new(Site)
	s.Matcher = matcher
	var err error
	var parsedUrl *url.URL
	if strings.HasSuffix(theUrl, "/") {
		theUrl = theUrl[:len(theUrl)-1]
	}
	if parsedUrl, err = url.Parse(theUrl); err != nil {
		return fmt.Errorf("bad url %s: %v", theUrl, err)
	}
	if strings.HasSuffix(parsedUrl.Path, "html") {
		parsedUrl.Path = path.Dir(s.Url.Path)
	}
	s.Url = parsedUrl
	s.BaseUrl = s.Url.String()
	parsedUrl.Path = path.Join(parsedUrl.Path, servicesIndex)
	if !queryw.ValidUrl(parsedUrl.String()) {
		return fmt.Errorf("site %s does not have a servicesindex.html file", s.BaseUrl)
	}
	s.ServicesIndexUrl = parsedUrl.String()
	c.Sites = append(c.Sites, s)
	return nil
}
func (c *Comparer) CompareServices() error {
	var err error
	for _, s := range c.Sites {
		err = s.GetServices()
		if err != nil {
			return err
		}
	}
	set1 := mapset.NewSet[string](c.Sites[0].ServiceUrls...)
	set2 := mapset.NewSet[string](c.Sites[1].ServiceUrls...)
	if c.Intersect {
		c.CombinedPaths = set1.Intersect(set2)
	} else {
		c.CombinedPaths = set1.Union(set2)
	}
	it := c.CombinedPaths.Iterator()
	for p := range it.C {
		var stats []*DbIdStats
		if stats, err = c.GetStats(p); err != nil {
			return err
		}
		if stats != nil {
			c.IdStats = append(c.IdStats, stats...)
		}
	}
	return nil
}
func (c *Comparer) GetStats(htmlPath string) (stats []*DbIdStats, err error) {
	var mapSite1 map[string]int
	var mapSite2 map[string]int
	s1Path := fmt.Sprintf("%s/%s", c.Sites[0].BaseUrl, htmlPath)
	fmt.Println(s1Path)
	s2Path := fmt.Sprintf("%s/%s", c.Sites[1].BaseUrl, htmlPath)
	fmt.Println(s2Path)
	if mapSite1, err = getDbIdMap(s1Path); err != nil {
		return nil, fmt.Errorf("error reading ")
	}
	if mapSite2, err = getDbIdMap(s2Path); err != nil {
		return nil, fmt.Errorf("error reading ")
	}
	// set up repeating data
	var serviceInfo *goarchdcs.ServiceInfo
	serviceInfo, err = goarchdcs.NewServiceInfo(c.Sites[0].BaseUrl, htmlPath)
	if err != nil {
		return nil, err
	}
	for k, v := range mapSite1 {
		var dbIdStats = new(DbIdStats)
		var ok bool
		var doxaId string
		if strings.HasPrefix(k, "ltx") {
			doxaId = k
		} else {
			doxaId, err = ltstring.AlwbToDoxaId(k)
			if err != nil {
				return nil, fmt.Errorf("%s: %v", k, err)
			}
		}
		dbIdStats.DbId = doxaId
		dbIdStats.ServiceType = serviceInfo.Type
		dbIdStats.ServiceDate = serviceInfo.Date
		dbIdStats.ServiceLayout = serviceInfo.LanguageAcronyms
		dbIdStats.CountSite1 = v
		if v, ok = mapSite2[doxaId]; ok {
			dbIdStats.CountSite2 = v
		}
		c.IdStats = append(c.IdStats, dbIdStats)
	}
	return stats, err
}
func getDbIdMap(theUrl string) (map[string]int, error) {
	author, err := queryw.MetaAuthor(theUrl)
	var generatedByDoxa bool
	if err == nil && strings.HasPrefix(strings.ToLower(author), "doxa") {
		generatedByDoxa = true
	}
	var theSelector string
	var theAttribute string
	if generatedByDoxa {
		theSelector = "[data-id-used]"
		theAttribute = "data-id-used"
	} else { // generated by ALWB
		theSelector = "[data-key]"
		theAttribute = "data-key"
	}
	values, err := queryw.Attribute(theUrl, theSelector, theAttribute)
	if err != nil {
		return nil, fmt.Errorf("%s: %v", theUrl, err)
	}
	if values == nil {
		return nil, fmt.Errorf("%s: no values returned", theUrl)
	}
	if len(values) == 0 {
		return nil, fmt.Errorf("%s: no values returned", theUrl)
	}
	if !generatedByDoxa {
		for i, v := range values {
			if v == "|" {
				continue
			}
			values[i], err = ltstring.AlwbToDoxaId(v)
			if err != nil {
				return nil, fmt.Errorf("%s: %v", theUrl, err)
			}
		}
	}
	m := stats.SliceToMap(values)
	return m, nil
}
func (s *Site) GetServices() error {
	theSelector := "a.index-day-link"
	theAttribute := "href"
	var err error
	var dayIndexUrls []string
	dayIndexUrls, err = queryw.Attribute(s.ServicesIndexUrl, theSelector, theAttribute)
	if err != nil {
		return err
	}
	if dayIndexUrls == nil {
		return fmt.Errorf("service day urls is nil")
	}
	if len(dayIndexUrls) == 0 {
		return fmt.Errorf("len(service day urls) == 0")
	}
	theSelector = "a.index-file-link"
	theAttribute = "href"
	for _, dayUrl := range dayIndexUrls {
		var serviceUrls []string
		theUrl := fmt.Sprintf("%s/%s", s.BaseUrl, dayUrl)
		serviceUrls, err = queryw.Attribute(theUrl, theSelector, theAttribute)
		if err != nil {
			return err
		}
		if serviceUrls == nil {
			return fmt.Errorf("%s: service urls is nil", theUrl)
		}
		if len(serviceUrls) == 0 {
			return fmt.Errorf("%s: len(service urls) == 0", theUrl)
		}
		for i, u := range serviceUrls {
			serviceUrls[i] = strings.ReplaceAll(u, "/m", "/")
			serviceUrls[i] = strings.ReplaceAll(serviceUrls[i], "/d", "/")
		}
		sort.Strings(serviceUrls)
		for _, su := range serviceUrls {
			if s.Matcher.Match([]byte(su)) {
				s.ServiceUrls = append(s.ServiceUrls, su)
			}
		}
	}
	return nil
}
func (c *Comparer) dayUrls(indexUrl string) ([]string, error) {
	var urls []string
	// for each class-"index-day-link" get href
	//    for each href
	//       for each class = index-file-link get href, eg.
	//      http://localhost:8085/indexes/h/s/2023/m07/d09/mo/en/index.html
	//      https://dcs.goarch.org/goa/dcs/indexes/h/s/2023/07/05/ma/en/index.html
	return urls, nil
}

func (c *Comparer) topicKeys(theUrl string) error {
	return nil
}
