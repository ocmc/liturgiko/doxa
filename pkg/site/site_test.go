package site

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/utils/stack"
	"path/filepath"
	"sort"
	"testing"
)

func TestFilePathMap(t *testing.T) {
	base := "/doxa/projects/doxa-alwb/websites/test/dcs/"
	fPaths := []string{
		base + "h/bk/me/m01/d02/en/index.html",
		base + "h/bk/me/m01/d02/gr-en/index.html",
		base + "h/bk/me/m01/d03/en/index.html",
		base + "h/bk/me/m01/d03/gr-en/index.html",
		base + "h/bk/me/m02/d02/en/index.html",
		base + "h/bk/me/m02/d02/gr-en/index.html",
		base + "h/bk/me/m02/d03/en/index.html",
		base + "h/bk/me/m02/d03/gr-en/index.html",
		base + "p/bk/me/m01/d02/en/index.html",
		base + "p/bk/me/m01/d02/gr-en/index.html",
		base + "p/bk/me/m01/d03/en/index.html",
		base + "p/bk/me/m01/d03/gr-en/index.html",
		base + "p/bk/me/m02/d02/en/index.html",
		base + "p/bk/me/m02/d02/gr-en/index.html",
		base + "p/bk/me/m02/d03/en/index.html",
		base + "p/bk/me/m02/d03/gr-en/index.html",
	}
	pMap := make(map[string]map[string]string)
	for _, f := range fPaths {
		var s stack.StringStack
		separator := string(filepath.Separator)
		s.Split(f, separator)
		var ok bool
		for i := 0; i < s.Size()-2; i++ {
			key := s.SubPath(0, i+1, separator)
			subKey := s.SubPath(i+1, i+2, separator)
			var subMap map[string]string
			if subMap, ok = pMap[key]; !ok {
				subMap = make(map[string]string)
			}
			subMap[subKey] = filepath.Join(key, subKey, separator)
			pMap[key] = subMap
		}
	}
	var keys []string
	for k, _ := range pMap {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	for _, k := range keys {
		fmt.Printf("%s \n", k)
		var ok bool
		var v map[string]string
		if v, ok = pMap[k]; ok {
			var subKeys []string
			for sk, _ := range v {
				subKeys = append(subKeys, sk)
			}
			sort.Strings(subKeys)
			for _, sk := range subKeys {
				if sv, ok := v[sk]; ok {
					fmt.Printf("\t-> %s: %s\n", sk, sv)
				}
			}
		}
	}
}

func TestSite_CompareServices(t *testing.T) {
	url1 := "http://localhost:8085/"
	url2 := "https://dcs.goarch.org/goa/dcs/"
	var err error
	var siteComparer *Comparer
	var htmlPathRegEx = "h/s/.*/li/gr-en/.*"
	intersect := true
	if siteComparer, err = NewComparer(url1, url2, htmlPathRegEx, intersect); err != nil {
		t.Error(err)
		return
	}
	if err = siteComparer.CompareServices(); err != nil {
		t.Error(err)
		return
	}
	if siteComparer.Sites == nil {
		t.Errorf("siteComparer.Sites is nil")
	}
}
