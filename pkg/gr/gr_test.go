package gr

import (
	"fmt"
	"strings"
	"testing"
)

func TestConsonants(t *testing.T) {
	letters := GetSet(Consonants)
	expect := 3
	if len(letters) != expect {
		t.Errorf("expected %d consonants, got %d", expect, len(letters))
	}
	rho := GetSet(Rho)
	if len(rho) != expect {
		t.Errorf("expected %d rho, got %d", expect, len(rho))
	}
}
func TestLetterList(t *testing.T) {
	for i := Alpha; i <= Ypogegrammeni; i++ {
		list := LetterList(i)
		fmt.Printf("const Set%s string = \"%s\"\n", Name(i), list)
	}
}
func TestNothing(t *testing.T) {
	var SetNames = []string{"Alpha",
		"AlphaDiacritics",
		"AlphaLower",
		"AlphaUpper",
		"Consonants",
		"Dasia",
		"DasiaOxia",
		"DasiaOxiaProsgegrammeni",
		"DasiaOxiaYpogegrammeni",
		"DasiaPerispomeni",
		"DasiaPerispomeniProsgegrammeni",
		"DasiaPerispomeniYpogegrammeni",
		"DasiaProsgegrammeni",
		"DasiaVaria",
		"DasiaVariaProsgegrammeni",
		"DasiaVariaYpogegrammeni",
		"DasiaYpogegrammeni",
		"Diacritics",
		"DialytikaOxia",
		"DialytikaPerispomeni",
		"DialytikaVaria",
		"Epsilon",
		"EpsilonDiacritics",
		"EpsilonLower",
		"EpsilonUpper",
		"Eta",
		"EtaDiacritics",
		"EtaLower",
		"EtaUpper",
		"Iota",
		"IotaDiacritics",
		"IotaLower",
		"IotaUpper",
		"Macron",
		"Omega",
		"OmegaDiacritics",
		"OmegaLower",
		"OmegaUpper",
		"Omicron",
		"OmicronDiacritics",
		"OmicronLower",
		"OmicronUpper",
		"Oxia",
		"OxiaYpogegrammeni",
		"Perispomeni",
		"PerispomeniYpogegrammeni",
		"Prosgegrammeni",
		"Psili",
		"PsiliOxia",
		"PsiliOxiaProsgegrammeni",
		"PsiliOxiaYpogegrammeni",
		"PsiliPerispomeni",
		"PsiliPerispomeniProsgegrammeni",
		"PsiliPerispomeniYpogegrammeni",
		"PsiliProsgegrammeni",
		"PsiliVaria",
		"PsiliVariaProsgegrammeni",
		"PsiliVariaYpogegrammeni",
		"PsiliYpogegrammeni",
		"Rho",
		"RhoDiacritics",
		"RhoLower",
		"RhoUpper",
		"Upsilon",
		"UpsilonDiacritics",
		"UpsilonLower",
		"UpsilonUpper",
		"Varia",
		"VariaYpogegrammeni",
		"Vowels",
		"Vrachy",
		"Ypogegrammeni",
	}
	for _, n := range SetNames {
		fmt.Printf("\ncase %s:\n\treturn \"%s\"", n, n)
	}
}
func TestExpandedRegEx(t *testing.T) {
	p := ` \ω(α)γ\ω(α)πη`
	ignoreCase := false
	ignoreDiacritics := false

	e, err := ExpandedRegEx(p, ignoreCase, ignoreDiacritics)
	if err != nil {
		t.Error(err)
	}
	fmt.Printf("\n%s\n%s\n", p, e)
}

func TestSplit(t *testing.T) {
	p := `\ω(`
	s := `\ω(α)γ\ω(α)πη`
	parts := strings.Split(s, p)
	if len(parts) == 1 {
		t.Errorf("Bummer")
	}
}
