// Package gr provides information from Unicode Extended Greek Block U+1F00
package gr

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/utils/ltUnicode"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"golang.org/x/text/unicode/norm"
	"strings"
	"text/scanner"
)

// BlockUrl provides the source for the information found in this package
const BlockUrl = "https://www.compart.com/en/unicode/block/U+1F00"

const WordBoundary = `[ |\.|;|?|!|"|'|«|»|:|˙|$|^|\(|\)]`

const DiacriticDasia = `῾` // U+0314 rough breathing mark

const DiacriticPsili = `᾽` // U+0313 smooth breathing mark

const DiacriticOxia = `´` // U+0301 acute accent

const DiacriticProsgegrammeni = `ι` // iota next to letter

const DiacriticYpogegrammeni = `ι` // U+0345 iota subscript

const DiacriticPerispomeni = `῀` // U+0342 circumflex

const DiacriticVaria = ``` // U+0300 grave accent

const DiacriticDialytika = `¨` // U+0308 diaeresis.  Note  ̈́ is deprecated. Use this.

const DiacriticKronis = `᾽` // U+0343 not the same as smooth breathing mark.  Indicates crasis, e.g. καὶ ἐγώ -> κἀγώ.

const DiacriticVrachy = ` ̆` // U+0306 A breve is the diacritic mark ˘, shaped like the bottom half of a circle. As used in Ancient Greek, it is also called brachy, βραχύ. (Wikipedia_

const DiacriticMacron = `¯` // U+0304 A macron is a diacritical mark: it is a straight bar ¯ placed above a letter, usually a vowel. Its name derives from Ancient Greek μακρόν (makrón) 'long' because it was originally used to mark long or heavy syllables in Greco-Roman metrics. It now more often marks a long vowel. (Wikipedia)

func ExpandedChar(c string, caseInsensitive, diacriticInsensitive bool) string {
	sb := strings.Builder{}
	switch c {
	// the cases for greek vowels and rho come first,
	// then the consonants
	case "α": // lower case
		if caseInsensitive {
			sb.WriteString(Bracket(SetAlpha))
		} else {
			sb.WriteString(Bracket(SetAlphaLower))
		}
	case "Α": // upper case
		if caseInsensitive {
			sb.WriteString(Bracket(SetAlpha))
		} else {
			sb.WriteString(Bracket(SetAlphaUpper))
		}
	case "αΑ", "Αα": // mixed case (i.e., case insensitive)
		sb.WriteString(Bracket(SetAlpha))
	case "ε":
		if caseInsensitive {
			sb.WriteString(Bracket(SetEpsilon))
		} else {
			sb.WriteString(Bracket(SetEpsilonLower))
		}
	case "Ε":
		if caseInsensitive {
			sb.WriteString(Bracket(SetEpsilon))
		} else {
			sb.WriteString(Bracket(SetEpsilonUpper))
		}
	case "εΕ", "Εε":
		sb.WriteString(Bracket(SetEpsilon))
	case "η":
		if caseInsensitive {
			sb.WriteString(Bracket(SetEta))
		} else {
			sb.WriteString(Bracket(SetEtaLower))
		}
	case "Η":
		if caseInsensitive {
			sb.WriteString(Bracket(SetEta))
		} else {
			sb.WriteString(Bracket(SetEtaUpper))
		}
	case "ηΗ", "Ηη":
		sb.WriteString(Bracket(SetEta))
	case "ι":
		if caseInsensitive {
			sb.WriteString(Bracket(SetIota))
		} else {
			sb.WriteString(Bracket(SetIotaLower))
		}
	case "Ι":
		if caseInsensitive {
			sb.WriteString(Bracket(SetIota))
		} else {
			sb.WriteString(Bracket(SetIotaUpper))
		}
	case "ιΙ", "Ιι":
		sb.WriteString(Bracket(SetIota))
	case "ο":
		if caseInsensitive {
			sb.WriteString(Bracket(SetOmicron))
		} else {
			sb.WriteString(Bracket(SetOmicronLower))
		}
	case "Ο":
		if caseInsensitive {
			sb.WriteString(Bracket(SetOmicron))
		} else {
			sb.WriteString(Bracket(SetOmicronUpper))
		}
	case "οΟ", "Οο":
		sb.WriteString(Bracket(SetOmicron))
	case "υ":
		if caseInsensitive {
			sb.WriteString(Bracket(SetUpsilon))
		} else {
			sb.WriteString(Bracket(SetUpsilonLower))
		}
	case "Υ":
		if caseInsensitive {
			sb.WriteString(Bracket(SetUpsilon))
		} else {
			sb.WriteString(Bracket(SetUpsilonUpper))
		}
	case "υΥ", "Υυ":
		sb.WriteString(Bracket(SetUpsilon))
	case "ω":
		if caseInsensitive {
			sb.WriteString(Bracket(SetOmega))
		} else {
			sb.WriteString(Bracket(SetOmegaLower))
		}
	case "Ω":
		if caseInsensitive {
			sb.WriteString(Bracket(SetOmega))
		} else {
			sb.WriteString(Bracket(SetOmegaUpper))
		}
	case "ωΩ", "Ωω":
		sb.WriteString(Bracket(SetOmega))
	case "ρ":
		if caseInsensitive {
			sb.WriteString(Bracket(SetRho))
		} else {
			sb.WriteString(Bracket(SetRhoLower))
		}
	case "Ρ":
		if caseInsensitive {
			sb.WriteString(Bracket(SetRho))
		} else {
			sb.WriteString(Bracket(SetRhoUpper))
		}
	case "ρΡ", "Ρρ":
		sb.WriteString(Bracket(SetRho))
		// add the consonants.
		// It is not necessary,
		// but some user might try
		// to use them with \δ
	case "β":
		if caseInsensitive {
			sb.WriteString("[βΒ]")
		} else {
			sb.WriteString("β")
		}
	case "Β":
		if caseInsensitive {
			sb.WriteString("[βΒ]")
		} else {
			sb.WriteString("Β")
		}
	case "βΒ", "Ββ":
		sb.WriteString("[βΒ]") // write a set
	case "γ":
		if caseInsensitive {
			sb.WriteString("[γΓ]")
		} else {
			sb.WriteString("γ")
		}
	case "Γ":
		if caseInsensitive {
			sb.WriteString("[γΓ]")
		} else {
			sb.WriteString("Γ")
		}
	case "γΓ", "Γγ":
		sb.WriteString("[γΓ]")
	case "δ":
		if caseInsensitive {
			sb.WriteString("[δΔ]")
		} else {
			sb.WriteString("δ")
		}
	case "Δ":
		if caseInsensitive {
			sb.WriteString("[δΔ]")
		} else {
			sb.WriteString("Δ")
		}
	case "δΔ", "Δδ":
		sb.WriteString("[δΔ]")
	case "ζ":
		if caseInsensitive {
			sb.WriteString("[ζΖ]")
		} else {
			sb.WriteString("ζ")
		}
	case "Ζ":
		if caseInsensitive {
			sb.WriteString("[ζΖ]")
		} else {
			sb.WriteString("Ζ")
		}
	case "ζΖ", "Ζζ":
		sb.WriteString("[ζΖ]")
	case "θ":
		if caseInsensitive {
			sb.WriteString("[θΘ]")
		} else {
			sb.WriteString("θ")
		}
	case "Θ":
		if caseInsensitive {
			sb.WriteString("[θΘ]")
		} else {
			sb.WriteString("Θ")
		}
	case "θΘ", "Θθ":
		sb.WriteString("[θΘ]")
	case "κ":
		if caseInsensitive {
			sb.WriteString("[κΚ]")
		} else {
			sb.WriteString("κ")
		}
	case "Κ":
		if caseInsensitive {
			sb.WriteString("[κΚ]")
		} else {
			sb.WriteString("Κ")
		}
	case "κΚ", "Κκ":
		sb.WriteString("[κΚ]")
	case "λ":
		if caseInsensitive {
			sb.WriteString("[λΛ]")
		} else {
			sb.WriteString("λ")
		}
	case "Λ":
		if caseInsensitive {
			sb.WriteString("[λΛ]")
		} else {
			sb.WriteString("Λ")
		}
	case "λΛ", "Λλ":
		sb.WriteString("[λΛ]")
	case "μ":
		if caseInsensitive {
			sb.WriteString("[μΜ]")
		} else {
			sb.WriteString("μ")
		}
	case "Μ":
		if caseInsensitive {
			sb.WriteString("[μΜ]")
		} else {
			sb.WriteString("Μ")
		}
	case "μΜ", "Μμ":
		sb.WriteString("[μΜ]")
	case "ν":
		if caseInsensitive {
			sb.WriteString("[νΝ]")
		} else {
			sb.WriteString("ν")
		}
	case "Ν":
		if caseInsensitive {
			sb.WriteString("[νΝ]")
		} else {
			sb.WriteString("Ν")
		}
	case "νΝ", "Νν":
		sb.WriteString("[νΝ]")
	case "ξ":
		if caseInsensitive {
			sb.WriteString("[ξΞ]")
		} else {
			sb.WriteString("ξ")
		}
	case "Ξ":
		if caseInsensitive {
			sb.WriteString("[ξΞ]")
		} else {
			sb.WriteString("Ξ")
		}
	case "ξΞ", "Ξξ":
		sb.WriteString("[ξΞ]")
	case "π":
		if caseInsensitive {
			sb.WriteString("[πΠ]")
		} else {
			sb.WriteString("π")
		}
	case "Π":
		if caseInsensitive {
			sb.WriteString("[πΠ]")
		} else {
			sb.WriteString("Π")
		}
	case "πΠ", "Ππ":
		sb.WriteString("[πΠ]")
	case "σ":
		if caseInsensitive {
			sb.WriteString("[σΣ]")
		} else {
			sb.WriteString("σ")
		}
	case "ς":
		sb.WriteString("ς")
	case "Σ":
		if caseInsensitive {
			sb.WriteString("[σΣ]")
		} else {
			sb.WriteString("Σ")
		}
	case "σΣ", "Σσ":
		sb.WriteString("[σΣ]")
	case "τ":
		if caseInsensitive {
			sb.WriteString("[τΤ]")
		} else {
			sb.WriteString("τ")
		}
	case "Τ":
		if caseInsensitive {
			sb.WriteString("[τΤ]")
		} else {
			sb.WriteString("Τ")
		}
	case "τΤ", "Ττ":
		sb.WriteString("[τΤ]")
	case "φ":
		if caseInsensitive {
			sb.WriteString("[φΦ]")
		} else {
			sb.WriteString("φ")
		}
	case "Φ":
		if caseInsensitive {
			sb.WriteString("[φΦ]")
		} else {
			sb.WriteString("Φ")
		}
	case "φΦ", "Φφ":
		sb.WriteString("[φΦ]")
	case "χ":
		if caseInsensitive {
			sb.WriteString("[χΧ]")
		} else {
			sb.WriteString("χ")
		}
	case "Χ":
		if caseInsensitive {
			sb.WriteString("[χΧ]")
		} else {
			sb.WriteString("Χ")
		}
	case "χΧ", "Χχ":
		sb.WriteString("[χΧ]")
	case "ψ":
		if caseInsensitive {
			sb.WriteString("[ψΨ]")
		} else {
			sb.WriteString("ψ")
		}
	case "Ψ":
		if caseInsensitive {
			sb.WriteString("[ψΨ]")
		} else {
			sb.WriteString("Ψ")
		}
	case "ψΨ", "Ψψ":
		sb.WriteString("[ψΨ]")
	default:
		sb.WriteString(c)
	}
	return sb.String()
}

func ExpandedRegEx2(pattern string, caseInsensitive, diacriticInsensitive bool) (string, error) {
	normalizedPattern := ltstring.ToUnicodeNFC(pattern)
	var s scanner.Scanner
	s.Init(strings.NewReader(normalizedPattern))
	s.Whitespace ^= 1<<'\t' | 1<<'\n' | 1<<'\r' | 1<<' ' // don't skip any white space
	sb := strings.Builder{}
	for tok := s.Scan(); tok != scanner.EOF; tok = s.Scan() {
		token := s.TokenText()
		switch token {
		case "\\":
			peek := s.Peek()
			switch peek {
			}
			if peek == 'b' {
				s.Scan() // go past it
				sb.WriteString(`(?:^|$|\P{L})`)
				continue
			} else if peek == 'B' {
				s.Scan() // go past it
				sb.WriteString(`(?:\p{L})`)
				continue
			}
			if peek == 'm' {
				s.Scan()                     // go past it
				sb.WriteString(`\p{M}{0,5}`) // zero to five combining marks
				continue
			}
			if peek == 'w' {
				s.Scan() // go past it
				sb.WriteString(Bracket(`0-9_` + SetAll()))
				continue
			} else if peek == 'W' {
				s.Scan() // go past it
				sb.WriteString(`(?:\p{L})`)
				continue
			}
			if peek == 'δ' {
				s.Scan() // go past it
				// read forward until reach ')'
				var openParenFound, closeParenFound bool
			omega:
				for i := 0; i < 4; i++ {
					s.Scan()
					token = s.TokenText()
					switch token {
					// if you add new cases, be sure wrap string in Bracket
					case "(":
						openParenFound = true
					case ")":
						closeParenFound = true
						break omega
						// the cases for greek vowels and rho come first,
						// then the consonants
					case "α": // lower case
						if caseInsensitive {
							sb.WriteString(Bracket(SetAlpha))
						} else {
							sb.WriteString(Bracket(SetAlphaLower))
						}
					case "Α": // upper case
						if caseInsensitive {
							sb.WriteString(Bracket(SetAlpha))
						} else {
							sb.WriteString(Bracket(SetAlphaUpper))
						}
					case "αΑ", "Αα": // mixed case (i.e., case insensitive)
						sb.WriteString(Bracket(SetAlpha))
					case "ε":
						if caseInsensitive {
							sb.WriteString(Bracket(SetEpsilon))
						} else {
							sb.WriteString(Bracket(SetEpsilonLower))
						}
					case "Ε":
						if caseInsensitive {
							sb.WriteString(Bracket(SetEpsilon))
						} else {
							sb.WriteString(Bracket(SetEpsilonUpper))
						}
					case "εΕ", "Εε":
						sb.WriteString(Bracket(SetEpsilon))
					case "η":
						if caseInsensitive {
							sb.WriteString(Bracket(SetEta))
						} else {
							sb.WriteString(Bracket(SetEtaLower))
						}
					case "Η":
						if caseInsensitive {
							sb.WriteString(Bracket(SetEta))
						} else {
							sb.WriteString(Bracket(SetEtaUpper))
						}
					case "ηΗ", "Ηη":
						sb.WriteString(Bracket(SetEta))
					case "ι":
						if caseInsensitive {
							sb.WriteString(Bracket(SetIota))
						} else {
							sb.WriteString(Bracket(SetIotaLower))
						}
					case "Ι":
						if caseInsensitive {
							sb.WriteString(Bracket(SetIota))
						} else {
							sb.WriteString(Bracket(SetIotaUpper))
						}
					case "ιΙ", "Ιι":
						sb.WriteString(Bracket(SetIota))
					case "ο":
						if caseInsensitive {
							sb.WriteString(Bracket(SetOmicron))
						} else {
							sb.WriteString(Bracket(SetOmicronLower))
						}
					case "Ο":
						if caseInsensitive {
							sb.WriteString(Bracket(SetOmicron))
						} else {
							sb.WriteString(Bracket(SetOmicronUpper))
						}
					case "οΟ", "Οο":
						sb.WriteString(Bracket(SetOmicron))
					case "υ":
						if caseInsensitive {
							sb.WriteString(Bracket(SetUpsilon))
						} else {
							sb.WriteString(Bracket(SetUpsilonLower))
						}
					case "Υ":
						if caseInsensitive {
							sb.WriteString(Bracket(SetUpsilon))
						} else {
							sb.WriteString(Bracket(SetUpsilonUpper))
						}
					case "υΥ", "Υυ":
						sb.WriteString(Bracket(SetUpsilon))
					case "ω":
						if caseInsensitive {
							sb.WriteString(Bracket(SetOmega))
						} else {
							sb.WriteString(Bracket(SetOmegaLower))
						}
					case "Ω":
						if caseInsensitive {
							sb.WriteString(Bracket(SetOmega))
						} else {
							sb.WriteString(Bracket(SetOmegaUpper))
						}
					case "ωΩ", "Ωω":
						sb.WriteString(Bracket(SetOmega))
					case "ρ":
						if caseInsensitive {
							sb.WriteString(Bracket(SetRho))
						} else {
							sb.WriteString(Bracket(SetRhoLower))
						}
					case "Ρ":
						if caseInsensitive {
							sb.WriteString(Bracket(SetRho))
						} else {
							sb.WriteString(Bracket(SetRhoUpper))
						}
					case "ρΡ", "Ρρ":
						sb.WriteString(Bracket(SetRho))
						// add the consonants.
						// It is not necessary,
						// but some user might try
						// to use them with \δ
					case "β":
						if caseInsensitive {
							sb.WriteString("[βΒ]")
						} else {
							sb.WriteString("β")
						}
					case "Β":
						if caseInsensitive {
							sb.WriteString("[βΒ]")
						} else {
							sb.WriteString("Β")
						}
					case "βΒ", "Ββ":
						sb.WriteString("[βΒ]") // write a set
					case "γ":
						if caseInsensitive {
							sb.WriteString("[γΓ]")
						} else {
							sb.WriteString("γ")
						}
					case "Γ":
						if caseInsensitive {
							sb.WriteString("[γΓ]")
						} else {
							sb.WriteString("Γ")
						}
					case "γΓ", "Γγ":
						sb.WriteString("[γΓ]")
					case "δ":
						if caseInsensitive {
							sb.WriteString("[δΔ]")
						} else {
							sb.WriteString("δ")
						}
					case "Δ":
						if caseInsensitive {
							sb.WriteString("[δΔ]")
						} else {
							sb.WriteString("Δ")
						}
					case "δΔ", "Δδ":
						sb.WriteString("[δΔ]")
					case "ζ":
						if caseInsensitive {
							sb.WriteString("[ζΖ]")
						} else {
							sb.WriteString("ζ")
						}
					case "Ζ":
						if caseInsensitive {
							sb.WriteString("[ζΖ]")
						} else {
							sb.WriteString("Ζ")
						}
					case "ζΖ", "Ζζ":
						sb.WriteString("[ζΖ]")
					case "θ":
						if caseInsensitive {
							sb.WriteString("[θΘ]")
						} else {
							sb.WriteString("θ")
						}
					case "Θ":
						if caseInsensitive {
							sb.WriteString("[θΘ]")
						} else {
							sb.WriteString("Θ")
						}
					case "θΘ", "Θθ":
						sb.WriteString("[θΘ]")
					case "κ":
						if caseInsensitive {
							sb.WriteString("[κΚ]")
						} else {
							sb.WriteString("κ")
						}
					case "Κ":
						if caseInsensitive {
							sb.WriteString("[κΚ]")
						} else {
							sb.WriteString("Κ")
						}
					case "κΚ", "Κκ":
						sb.WriteString("[κΚ]")
					case "λ":
						if caseInsensitive {
							sb.WriteString("[λΛ]")
						} else {
							sb.WriteString("λ")
						}
					case "Λ":
						if caseInsensitive {
							sb.WriteString("[λΛ]")
						} else {
							sb.WriteString("Λ")
						}
					case "λΛ", "Λλ":
						sb.WriteString("[λΛ]")
					case "μ":
						if caseInsensitive {
							sb.WriteString("[μΜ]")
						} else {
							sb.WriteString("μ")
						}
					case "Μ":
						if caseInsensitive {
							sb.WriteString("[μΜ]")
						} else {
							sb.WriteString("Μ")
						}
					case "μΜ", "Μμ":
						sb.WriteString("[μΜ]")
					case "ν":
						if caseInsensitive {
							sb.WriteString("[νΝ]")
						} else {
							sb.WriteString("ν")
						}
					case "Ν":
						if caseInsensitive {
							sb.WriteString("[νΝ]")
						} else {
							sb.WriteString("Ν")
						}
					case "νΝ", "Νν":
						sb.WriteString("[νΝ]")
					case "ξ":
						if caseInsensitive {
							sb.WriteString("[ξΞ]")
						} else {
							sb.WriteString("ξ")
						}
					case "Ξ":
						if caseInsensitive {
							sb.WriteString("[ξΞ]")
						} else {
							sb.WriteString("Ξ")
						}
					case "ξΞ", "Ξξ":
						sb.WriteString("[ξΞ]")
					case "π":
						if caseInsensitive {
							sb.WriteString("[πΠ]")
						} else {
							sb.WriteString("π")
						}
					case "Π":
						if caseInsensitive {
							sb.WriteString("[πΠ]")
						} else {
							sb.WriteString("Π")
						}
					case "πΠ", "Ππ":
						sb.WriteString("[πΠ]")
					case "σ":
						if caseInsensitive {
							sb.WriteString("[σΣ]")
						} else {
							sb.WriteString("σ")
						}
					case "ς":
						sb.WriteString("ς")
					case "Σ":
						if caseInsensitive {
							sb.WriteString("[σΣ]")
						} else {
							sb.WriteString("Σ")
						}
					case "σΣ", "Σσ":
						sb.WriteString("[σΣ]")
					case "τ":
						if caseInsensitive {
							sb.WriteString("[τΤ]")
						} else {
							sb.WriteString("τ")
						}
					case "Τ":
						if caseInsensitive {
							sb.WriteString("[τΤ]")
						} else {
							sb.WriteString("Τ")
						}
					case "τΤ", "Ττ":
						sb.WriteString("[τΤ]")
					case "φ":
						if caseInsensitive {
							sb.WriteString("[φΦ]")
						} else {
							sb.WriteString("φ")
						}
					case "Φ":
						if caseInsensitive {
							sb.WriteString("[φΦ]")
						} else {
							sb.WriteString("Φ")
						}
					case "φΦ", "Φφ":
						sb.WriteString("[φΦ]")
					case "χ":
						if caseInsensitive {
							sb.WriteString("[χΧ]")
						} else {
							sb.WriteString("χ")
						}
					case "Χ":
						if caseInsensitive {
							sb.WriteString("[χΧ]")
						} else {
							sb.WriteString("Χ")
						}
					case "χΧ", "Χχ":
						sb.WriteString("[χΧ]")
					case "ψ":
						if caseInsensitive {
							sb.WriteString("[ψΨ]")
						} else {
							sb.WriteString("ψ")
						}
					case "Ψ":
						if caseInsensitive {
							sb.WriteString("[ψΨ]")
						} else {
							sb.WriteString("Ψ")
						}
					case "ψΨ", "Ψψ":
						sb.WriteString("[ψΨ]")
					default:
						return "", fmt.Errorf("%s is not defined as an argument for \\δ()", token)
					}
				}
				if !openParenFound && !closeParenFound {
					return "", fmt.Errorf("expect both open and close parentheses for \\δ")
				}
			} else {
				sb.WriteString(token)
			}
		default:
			sb.WriteString(token)
		}
	}
	return sb.String(), nil
}

func ExpandedRegEx3(pattern string, caseInsensitive, diacriticInsensitive bool) (string, error) {
	var diacritics = `\p{M}{0,5}`
	var ia norm.Iter
	ia.InitString(norm.NFC, pattern)
	var lastWasBackslash bool
	sb := strings.Builder{}

	for !ia.Done() {
		token := ia.Next()
		strToken := string(token)
		if strToken == `\` {
			lastWasBackslash = true
			continue
		}
		if lastWasBackslash && (strToken == `m` || strToken == `μ`) {
			sb.WriteString(diacritics)
			lastWasBackslash = false
			continue
		}
		if lastWasBackslash {
			sb.WriteString(`\`)
			lastWasBackslash = false
		}
		sb.WriteString(strToken)
		// if doing a diacritic insensitive match,
		// and it is a letter, we add the
		// unicode properties for diacritics
		// after the word.
		if diacriticInsensitive {
			if ltUnicode.CanBeCombined(strToken) {
				sb.WriteString(diacritics)
			}
		}
	}
	return sb.String(), nil
}

func GetSet(s SetKey) []string {
	indexes := SetMap[s]
	var letters []string
	for _, i := range indexes {
		letters = append(letters, LetterMap[i].Grapheme)
	}
	return letters
}

// LetterList returns the set as a list to use in a regular expression
func LetterList(s SetKey) string {
	return fmt.Sprintf("[%s]", strings.Join(GetSet(s), ""))
}
func Bracket(s string) string {
	return "[" + s + "]"
}
func GetExpansionSet(s string, caseInsensitive bool) string {
	n := ltstring.ToNfcNoDiacritics(s)
	if caseInsensitive {
		n = strings.ToLower(n)
		switch n {
		case "α":
			return SetAlpha
		case "ε":
			return SetEpsilon
		case "η":
			return SetEta
		case "ι":
			return SetIota
		case "ο":
			return SetOmicron
		case "υ":
			return SetUpsilon
		case "ω":
			return SetOmega
		case "ρ":
			return SetRho
		default:
			return s
		}
	}
	switch n {
	case "α":
		return SetAlphaLower
	case "ε":
		return SetEpsilonLower
	case "η":
		return SetEtaLower
	case "ι":
		return SetIotaLower
	case "ο":
		return SetOmicronLower
	case "υ":
		return SetUpsilonLower
	case "ω":
		return SetOmegaLower
	case "ρ":
		return SetRhoLower
	case "Α":
		return SetAlphaLower
	case "Ε":
		return SetEpsilonLower
	case "Η":
		return SetEtaLower
	case "Ι":
		return SetIotaLower
	case "Ο":
		return SetOmicronLower
	case "Υ":
		return SetUpsilonLower
	case "Ω":
		return SetOmegaLower
	case "Ρ":
		return SetRhoLower
	default:
		return s
	}
}
func SetAll() string {
	return SetAlpha +
		SetEpsilon +
		SetEta +
		SetIota +
		SetOmicron +
		SetUpsilon +
		SetOmega +
		SetRho +
		SetConsonantsLessRho
}

const SetConsonantsLessRho string = "βΒγΓδΔζΖθΘκΚλΛμΜνΝξΞπΠσΣςτΤφΦχΧψΨ"
const SetConsonantsLessRhoLower string = "βγδζθκλμνξπσςτφχψ"
const SetConsonantsLessRhoUpper string = "ΒΓΔΖΘΚΛΜΝΞΠΣΤΦΧΨ"
const SetAlpha string = "ΑαᾼΆᾺᾹᾸᾷᾶᾴᾳᾲᾱᾰᾏᾎᾍᾌᾋᾊᾉᾈᾇᾆᾅᾄᾃᾂᾁᾀάὰἏἎἍἌἋἊἉἈἇἆἅἄἃἂἁἀ"
const SetAlphaDiacritics string = "ᾼΆᾺᾹᾸᾷᾶᾴᾳᾲᾱᾰᾏᾎᾍᾌᾋᾊᾉᾈᾇᾆᾅᾄᾃᾂᾁᾀάὰἏἎἍἌἋἊἉἈἇἆἅἄἃἂἁἀ"
const SetAlphaLower string = "αᾷᾶᾴᾳᾲᾱᾰᾇᾆᾅᾄᾃᾂᾁᾀάὰἇἆἅἄἃἂἁἀ"
const SetAlphaUpper string = "ΑᾼΆᾺᾹᾸᾏᾎᾍᾌᾋᾊᾉᾈἏἎἍἌἋἊἉἈ"
const SetConsonants string = "Ῥῥῤ"
const SetDasia string = "ῬῥὩὡὙὑὉὁἹἱἩἡἙἑἉἁ"
const SetDasiaOxia string = "ὭὥὝὕὍὅἽἵἭἥἝἕἍἅ"
const SetDasiaOxiaProsgegrammeni string = "ᾭᾝᾍ"
const SetDasiaOxiaYpogegrammeni string = "ᾥᾕᾅ"
const SetDasiaPerispomeni string = "ὯὧὟὗἿἷἯἧἏἇ"
const SetDasiaPerispomeniProsgegrammeni string = "ᾯᾟᾏ"
const SetDasiaPerispomeniYpogegrammeni string = "ᾧᾗᾇ"
const SetDasiaProsgegrammeni string = "ᾩᾙᾉ"
const SetDasiaVaria string = "ὫὣὛὓὋὃἻἳἫἣἛἓἋἃ"
const SetDasiaVariaProsgegrammeni string = "ᾫᾛᾋ"
const SetDasiaVariaYpogegrammeni string = "ᾣᾓᾃ"
const SetDasiaYpogegrammeni string = "ᾡᾑᾁ"
const SetDiacritics string = "ῼΏῺΌῸῷῶῴῳῲῬΎῪῩῨῧῦῥῤΰῢῡῠΊῚῙῘῗῖΐῒῑῐῌΉῊΈῈῇῆῄῃῂᾼΆᾺᾹᾸᾷᾶᾴᾳᾲᾱᾰᾯᾮᾭᾬᾫᾪᾩᾨᾧᾦᾥᾤᾣᾢᾡᾠᾟᾞᾝᾜᾛᾚᾙᾘᾗᾖᾕᾔᾓᾒᾑᾐᾏᾎᾍᾌᾋᾊᾉᾈᾇᾆᾅᾄᾃᾂᾁᾀώὼύὺόὸίὶήὴέὲάὰὯὮὭὬὫὪὩὨὧὦὥὤὣὢὡὠὟὝὛὙὗὖὕὔὓὒὑὐὍὌὋὊὉὈὅὄὃὂὁὀἿἾἽἼἻἺἹἸἷἶἵἴἳἲἱἰἯἮἭἬἫἪἩἨἧἦἥἤἣἢἡἠἝἜἛἚἙἘἕἔἓἒἑἐἏἎἍἌἋἊἉἈἇἆἅἄἃἂἁἀ"
const SetDialytikaOxia string = "ΰΐ"
const SetDialytikaPerispomeni string = "ῧῗ"
const SetDialytikaVaria string = "ῢῒ"
const SetEpsilon string = "ΕΈῈεέὲἝἜἛἚἙἘἕἔἓἒἑἐ"
const SetEpsilonDiacritics string = "ΈῈέὲἝἜἛἚἙἘἕἔἓἒἑἐ"
const SetEpsilonLower string = "εέὲἕἔἓἒἑἐ"
const SetEpsilonUpper string = "ΕΕΈῈἝἜἛἚἙἘ"
const SetEta string = "ΗηῌΉῊῇῆῄῃῂᾟᾞᾝᾜᾛᾚᾙᾘᾗᾖᾕᾔᾓᾒᾑᾐήὴἯἮἭἬἫἪἩἨἧἦἥἤἣἢἡἠ"
const SetEtaDiacritics string = "ῌΉῊῇῆῄῃῂᾟᾞᾝᾜᾛᾚᾙᾘᾗᾖᾕᾔᾓᾒᾑᾐήὴἯἮἭἬἫἪἩἨἧἦἥἤἣἢἡἠ"
const SetEtaLower string = "ηῇῆῄῃῂᾗᾖᾕᾔᾓᾒᾑᾐήὴἧἦἥἤἣἢἡἠ"
const SetEtaUpper string = "ΗῌΉῊᾟᾞᾝᾜᾛᾚᾙᾘἯἮἭἬἫἪἩἨ"
const SetIota string = "ΙιΊῚῙῘῗῖΐῒῑῐίὶἿἾἽἼἻἺἹἸἷἶἵἴἳἲἱἰ"
const SetIotaDiacritics string = "ΊῚῙῘῗῖΐῒῑῐίὶἿἾἽἼἻἺἹἸἷἶἵἴἳἲἱἰ"
const SetIotaLower string = "ιῗῖΐῒῑῐίὶἷἶἵἴἳἲἱἰ"
const SetIotaUpper string = "ΙΊῚῙῘἿἾἽἼἻἺἹἸ"
const SetMacron string = "ῩῡῙῑᾹᾱ"
const SetOmega string = "ΩωῼΏῺῷῶῴῳῲᾯᾮᾭᾬᾫᾪᾩᾨᾧᾦᾥᾤᾣᾢᾡᾠώὼὯὮὭὬὫὪὩὨὧὦὥὤὣὢὡὠ"
const SetOmegaDiacritics string = "ῼΏῺῷῶῴῳῲᾯᾮᾭᾬᾫᾪᾩᾨᾧᾦᾥᾤᾣᾢᾡᾠώὼὯὮὭὬὫὪὩὨὧὦὥὤὣὢὡὠ"
const SetOmegaLower string = "ωῷῶῴῳῲᾧᾦᾥᾤᾣᾢᾡᾠώὼὧὦὥὤὣὢὡὠ"
const SetOmegaUpper string = "ΩῼΏῺᾯᾮᾭᾬᾫᾪᾩᾨὯὮὭὬὫὪὩὨ"
const SetOmicron string = "ΟοΌῸόὸὍὌὋὊὉὈὅὄὃὂὁὀ"
const SetOmicronDiacritics string = "ΌῸόὸὍὌὋὊὉὈὅὄὃὂὁὀ"
const SetOmicronLower string = "οόὸὅὄὃὂὁὀ"
const SetOmicronUpper string = "ΟΌῸὍὌὋὊὉὈ"
const SetOxia string = "ΏΌΎΊΉΈΆώύόίήέά"
const SetOxiaYpogegrammeni string = "ῴῄᾴ"
const SetPerispomeni string = "ῶῦῖῆᾶ"
const SetPerispomeniYpogegrammeni string = "ῷῇᾷ"
const SetProsgegrammeni string = "ῼῌᾼ"
const SetPsili string = "ῤὨὠὐὈὀἸἰἨἠἘἐἈἀ"
const SetPsiliOxia string = "ὬὤὔὌὄἼἴἬἤἜἔἌἄ"
const SetPsiliOxiaProsgegrammeni string = "ᾬᾜᾌ"
const SetPsiliOxiaYpogegrammeni string = "ᾤᾔᾄ"
const SetPsiliPerispomeni string = "ᾮᾦᾞᾖᾎᾆὮὦὖἾἶἮἦἎἆ"
const SetPsiliPerispomeniProsgegrammeni string = ""
const SetPsiliPerispomeniYpogegrammeni string = ""
const SetPsiliProsgegrammeni string = "ᾨᾘᾈ"
const SetPsiliVaria string = "ὪὢὒὊὂἺἲἪἢἚἒἊἂ"
const SetPsiliVariaProsgegrammeni string = "ᾪᾚᾊ"
const SetPsiliVariaYpogegrammeni string = "ᾢᾒᾂ"
const SetPsiliYpogegrammeni string = "ᾠᾐᾀ"
const SetRho string = "ΡρῬῥῤ"
const SetRhoDiacritics string = "Ῥῥῤ"
const SetRhoLower string = "ρῥῤ"
const SetRhoUpper string = "ΡῬ"
const SetUpsilon string = "ΥυΎῪῩῨῧῦΰῢῡῠύὺὟὝὛὙὗὖὕὔὓὒὑὐ"
const SetUpsilonDiacritics string = "ΎῪῩῨῧῦΰῢῡῠύὺὟὝὛὙὗὖὕὔὓὒὑὐ"
const SetUpsilonLower string = "υῧῦΰῢῡῠύὺὗὖὕὔὓὒὑὐ"
const SetUpsilonUpper string = "ΥΎῪῩῨὟὝὛὙ"
const SetVaria string = "ῺῸῪῚῊῈᾺὼὺὸὶὴὲὰ"
const SetVariaYpogegrammeni string = "ῲῂᾲ"
const SetVowels string = "ΩῼΏῺΌῸωῷῶῴῳῲΥΎῪῩῨυῧῦΰῢῡῠΙΊῚῙῘιῗῖΐῒῑῐΗῌΉῊΕΈῈηῇῆῄῃῂΑᾼΆᾺᾹᾸαᾷᾶᾴᾳᾲᾱᾰᾯᾮᾭᾬᾫᾪᾩᾨᾧᾦᾥᾤᾣᾢᾡᾠᾟᾞᾝᾜᾛᾚᾙᾘᾗᾖᾕᾔᾓᾒᾑᾐᾏᾎᾍᾌᾋᾊᾉᾈᾇᾆᾅᾄᾃᾂᾁᾀώὼύὺόὸίὶήὴέὲάὰὯὮὭὬὫὪὩὨὧὦὥὤὣὢὡὠὟὝὛὙὗὖὕὔὓὒὑὐΟὍὌὋὊὉὈοὅὄὃὂὁὀἿἾἽἼἻἺἹἸἷἶἵἴἳἲἱἰἯἮἭἬἫἪἩἨἧἦἥἤἣἢἡἠἝἜἛἚἙἘἕἔἓἒἑἐἏἎἍἌἋἊἉἈἇἆἅἄἃἂἁἀ"
const SetVrachy string = "ῨῠῘῐᾸᾰ"
const SetYpogegrammeni string = "ῳῃᾳ"
const SetAlphaWithVaria string = "ἂᾲᾃᾂᾺᾋᾊἋἊ"
const SetEtaWithVaria string = "ᾓᾒὴἣἢῊῂᾛᾚἫἪ"
const SetEpsilonWithVaria string = "ὲἓἒἛἚ"
const SetIotaWithVaria string = "ὶῒἳἲἻἺῚ"
const SetOmicronWithVaria string = "ὸὃὂῸὋὊ"
const SetUpsilonWithVaria string = "ὓὒὺῢῪ"
const SetOmegaWithVaria string = "ὼὣὢῲᾣᾢῺᾫᾪὫὪ"

type GreekUnicode struct {
	UniCode           string
	Grapheme          string
	Base              string
	IsConsonant       bool
	IsPunctuation     bool
	IsVowel           bool
	IsLower           bool
	IsUpper           bool
	HasDiacritics     bool
	HasVaria          bool
	HasPerispomeni    bool
	HasYpogegrammeni  bool
	HasProsgegrammeni bool
	HasVrachy         bool
	HasMacron         bool
	HasDialytika      bool
	HasPsili          bool
	HasDasia          bool
	HasOxia           bool
	Description       string
}

// LetterMap Contains attributes for each code point from
// Unicode block U+1F00.  Extracted from https://www.compart.com/en/unicode/block/U+1F00
// Use this map in conjunction with
var LetterMap = map[int]GreekUnicode{
	0: {
		UniCode:           "U+1FFE",
		Grapheme:          "῾",
		Base:              "῾",
		IsConsonant:       true,
		IsPunctuation:     false,
		IsVowel:           false,
		IsLower:           false,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Dasia",
	},
	1: {
		UniCode:           "U+1FFD",
		Grapheme:          "´",
		Base:              "´",
		IsConsonant:       true,
		IsPunctuation:     false,
		IsVowel:           false,
		IsLower:           false,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Oxia",
	},
	2: {
		UniCode:           "U+1FFC",
		Grapheme:          "ῼ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Omega with Prosgegrammeni",
	},
	3: {
		UniCode:           "U+1FFB",
		Grapheme:          "Ώ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Capital Letter Omega with Oxia",
	},
	4: {
		UniCode:           "U+1FFA",
		Grapheme:          "Ὼ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Omega with Varia",
	},
	5: {
		UniCode:           "U+1FF9",
		Grapheme:          "Ό",
		Base:              "ο",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Capital Letter Omicron with Oxia",
	},
	6: {
		UniCode:           "U+1FF8",
		Grapheme:          "Ὸ",
		Base:              "ο",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Omicron with Varia",
	},
	7: {
		UniCode:           "U+1FF7",
		Grapheme:          "ῷ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Omega with Perispomeni and Ypogegrammeni",
	},
	8: {
		UniCode:           "U+1FF6",
		Grapheme:          "ῶ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Omega with Perispomeni",
	},
	9: {
		UniCode:           "U+1FF4",
		Grapheme:          "ῴ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Small Letter Omega with Oxia and Ypogegrammeni",
	},
	10: {
		UniCode:           "U+1FF3",
		Grapheme:          "ῳ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Omega with Ypogegrammeni",
	},
	11: {
		UniCode:           "U+1FF2",
		Grapheme:          "ῲ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Omega with Varia and Ypogegrammeni",
	},
	12: {
		UniCode:           "U+1FEF",
		Grapheme:          "`",
		Base:              "`",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           false,
		IsLower:           false,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Varia",
	},
	13: {
		UniCode:           "U+1FEE",
		Grapheme:          "΅",
		Base:              "¨",
		IsConsonant:       true,
		IsPunctuation:     false,
		IsVowel:           false,
		IsLower:           false,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      true,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Dialytika and Oxia",
	},
	14: {
		UniCode:           "U+1FED",
		Grapheme:          "῭",
		Base:              "¨",
		IsConsonant:       true,
		IsPunctuation:     false,
		IsVowel:           false,
		IsLower:           false,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      true,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Dialytika and Varia",
	},
	15: {
		UniCode:           "U+1FEC",
		Grapheme:          "Ῥ",
		Base:              "ρ",
		IsConsonant:       true,
		IsPunctuation:     false,
		IsVowel:           false,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Rho with Dasia",
	},
	16: {
		UniCode:           "U+1FEB",
		Grapheme:          "Ύ",
		Base:              "υ",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Capital Letter Upsilon with Oxia",
	},
	17: {
		UniCode:           "U+1FEA",
		Grapheme:          "Ὺ",
		Base:              "υ",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Upsilon with Varia",
	},
	18: {
		UniCode:           "U+1FE9",
		Grapheme:          "Ῡ",
		Base:              "υ",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         true,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Upsilon with Macron",
	},
	19: {
		UniCode:           "U+1FE8",
		Grapheme:          "Ῠ",
		Base:              "υ",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         true,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Upsilon with Vrachy",
	},
	20: {
		UniCode:           "U+1FE7",
		Grapheme:          "ῧ",
		Base:              "υ",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      true,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Upsilon with Dialytika and Perispomeni",
	},
	21: {
		UniCode:           "U+1FE6",
		Grapheme:          "ῦ",
		Base:              "υ",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Upsilon with Perispomeni",
	},
	22: {
		UniCode:           "U+1FE5",
		Grapheme:          "ῥ",
		Base:              "ρ",
		IsConsonant:       true,
		IsPunctuation:     false,
		IsVowel:           false,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Rho with Dasia",
	},
	23: {
		UniCode:           "U+1FE4",
		Grapheme:          "ῤ",
		Base:              "ρ",
		IsConsonant:       true,
		IsPunctuation:     false,
		IsVowel:           false,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Rho with Psili",
	},
	24: {
		UniCode:           "U+1FE3",
		Grapheme:          "ΰ",
		Base:              "υ",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      true,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Small Letter Upsilon with Dialytika and Oxia",
	},
	25: {
		UniCode:           "U+1FE2",
		Grapheme:          "ῢ",
		Base:              "υ",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      true,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Upsilon with Dialytika and Varia",
	},
	26: {
		UniCode:           "U+1FE1",
		Grapheme:          "ῡ",
		Base:              "υ",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         true,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Upsilon with Macron",
	},
	27: {
		UniCode:           "U+1FE0",
		Grapheme:          "ῠ",
		Base:              "υ",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         true,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Upsilon with Vrachy",
	},
	28: {
		UniCode:           "U+1FDF",
		Grapheme:          "῟",
		Base:              "῾",
		IsConsonant:       true,
		IsPunctuation:     false,
		IsVowel:           false,
		IsLower:           false,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Dasia and Perispomeni",
	},
	29: {
		UniCode:           "U+1FDE",
		Grapheme:          "῞",
		Base:              "῾",
		IsConsonant:       true,
		IsPunctuation:     false,
		IsVowel:           false,
		IsLower:           false,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           true,
		Description:       "Greek Dasia and Oxia",
	},
	30: {
		UniCode:           "U+1FDD",
		Grapheme:          "῝",
		Base:              "῾",
		IsConsonant:       true,
		IsPunctuation:     false,
		IsVowel:           false,
		IsLower:           false,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Dasia and Varia",
	},
	31: {
		UniCode:           "U+1FDB",
		Grapheme:          "Ί",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Capital Letter Iota with Oxia",
	},
	32: {
		UniCode:           "U+1FDA",
		Grapheme:          "Ὶ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Iota with Varia",
	},
	33: {
		UniCode:           "U+1FD9",
		Grapheme:          "Ῑ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         true,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Iota with Macron",
	},
	34: {
		UniCode:           "U+1FD8",
		Grapheme:          "Ῐ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         true,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Iota with Vrachy",
	},
	35: {
		UniCode:           "U+1FD7",
		Grapheme:          "ῗ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      true,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Iota with Dialytika and Perispomeni",
	},
	36: {
		UniCode:           "U+1FD6",
		Grapheme:          "ῖ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Iota with Perispomeni",
	},
	37: {
		UniCode:           "U+1FD3",
		Grapheme:          "ΐ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      true,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Small Letter Iota with Dialytika and Oxia",
	},
	38: {
		UniCode:           "U+1FD2",
		Grapheme:          "ῒ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      true,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Iota with Dialytika and Varia",
	},
	39: {
		UniCode:           "U+1FD1",
		Grapheme:          "ῑ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         true,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Iota with Macron",
	},
	40: {
		UniCode:           "U+1FD0",
		Grapheme:          "ῐ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         true,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Iota with Vrachy",
	},
	41: {
		UniCode:           "U+1FCF",
		Grapheme:          "῏",
		Base:              "᾿",
		IsConsonant:       true,
		IsPunctuation:     false,
		IsVowel:           false,
		IsLower:           false,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Psili and Perispomeni",
	},
	42: {
		UniCode:           "U+1FCE",
		Grapheme:          "῎",
		Base:              "᾿",
		IsConsonant:       true,
		IsPunctuation:     false,
		IsVowel:           false,
		IsLower:           false,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Psili and Oxia",
	},
	43: {
		UniCode:           "U+1FCD",
		Grapheme:          "῍",
		Base:              "᾿",
		IsConsonant:       true,
		IsPunctuation:     false,
		IsVowel:           false,
		IsLower:           false,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Psili and Varia",
	},
	44: {
		UniCode:           "U+1FCC",
		Grapheme:          "ῌ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Eta with Prosgegrammeni",
	},
	45: {
		UniCode:           "U+1FCB",
		Grapheme:          "Ή",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Capital Letter Eta with Oxia",
	},
	46: {
		UniCode:           "U+1FCA",
		Grapheme:          "Ὴ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Eta with Varia",
	},
	47: {
		UniCode:           "U+1FC9",
		Grapheme:          "Έ",
		Base:              "ε",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Capital Letter Epsilon with Oxia",
	},
	48: {
		UniCode:           "U+1FC8",
		Grapheme:          "Ὲ",
		Base:              "ε",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Epsilon with Varia",
	},
	49: {
		UniCode:           "U+1FC7",
		Grapheme:          "ῇ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Eta with Perispomeni and Ypogegrammeni",
	},
	50: {
		UniCode:           "U+1FC6",
		Grapheme:          "ῆ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Eta with Perispomeni",
	},
	51: {
		UniCode:           "U+1FC4",
		Grapheme:          "ῄ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Small Letter Eta with Oxia and Ypogegrammeni",
	},
	52: {
		UniCode:           "U+1FC3",
		Grapheme:          "ῃ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Eta with Ypogegrammeni",
	},
	53: {
		UniCode:           "U+1FC2",
		Grapheme:          "ῂ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Eta with Varia and Ypogegrammeni",
	},
	54: {
		UniCode:           "U+1FC1",
		Grapheme:          "῁",
		Base:              "¨",
		IsConsonant:       true,
		IsPunctuation:     false,
		IsVowel:           false,
		IsLower:           false,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      true,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Dialytika and Perispomeni",
	},
	55: {
		UniCode:           "U+1FC0",
		Grapheme:          "῀",
		Base:              "῀",
		IsConsonant:       true,
		IsPunctuation:     false,
		IsVowel:           false,
		IsLower:           false,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Perispomeni",
	},
	56: {
		UniCode:           "U+1FBF",
		Grapheme:          "᾿",
		Base:              "᾿",
		IsConsonant:       true,
		IsPunctuation:     false,
		IsVowel:           false,
		IsLower:           false,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Psili",
	},
	57: {
		UniCode:           "U+1FBE",
		Grapheme:          "ι",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Prosgegrammeni",
	},
	58: {
		UniCode:           "U+1FBD",
		Grapheme:          "᾽",
		Base:              "᾽",
		IsConsonant:       true,
		IsPunctuation:     false,
		IsVowel:           false,
		IsLower:           false,
		IsUpper:           false,
		HasDiacritics:     false,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Koronis",
	},
	59: {
		UniCode:           "U+1FBC",
		Grapheme:          "ᾼ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Alpha with Prosgegrammeni",
	},
	60: {
		UniCode:           "U+1FBB",
		Grapheme:          "Ά",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Capital Letter Alpha with Oxia",
	},
	61: {
		UniCode:           "U+1FBA",
		Grapheme:          "Ὰ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Alpha with Varia",
	},
	62: {
		UniCode:           "U+1FB9",
		Grapheme:          "Ᾱ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         true,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Alpha with Macron",
	},
	63: {
		UniCode:           "U+1FB8",
		Grapheme:          "Ᾰ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         true,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Alpha with Vrachy",
	},
	64: {
		UniCode:           "U+1FB7",
		Grapheme:          "ᾷ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Alpha with Perispomeni and Ypogegrammeni",
	},
	65: {
		UniCode:           "U+1FB6",
		Grapheme:          "ᾶ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Alpha with Perispomeni",
	},
	66: {
		UniCode:           "U+1FB4",
		Grapheme:          "ᾴ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Small Letter Alpha with Oxia and Ypogegrammeni",
	},
	67: {
		UniCode:           "U+1FB3",
		Grapheme:          "ᾳ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Alpha with Ypogegrammeni",
	},
	68: {
		UniCode:           "U+1FB2",
		Grapheme:          "ᾲ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Alpha with Varia and Ypogegrammeni",
	},
	69: {
		UniCode:           "U+1FB1",
		Grapheme:          "ᾱ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         true,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Alpha with Macron",
	},
	70: {
		UniCode:           "U+1FB0",
		Grapheme:          "ᾰ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         true,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Alpha with Vrachy",
	},
	71: {
		UniCode:           "U+1FAF",
		Grapheme:          "ᾯ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Omega with Dasia and Perispomeni and Prosgegrammeni",
	},
	72: {
		UniCode:           "U+1FAE",
		Grapheme:          "ᾮ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Omega with Psili and Perispomeni and Prosgegrammeni",
	},
	73: {
		UniCode:           "U+1FAD",
		Grapheme:          "ᾭ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           true,
		Description:       "Greek Capital Letter Omega with Dasia and Oxia and Prosgegrammeni",
	},
	74: {
		UniCode:           "U+1FAC",
		Grapheme:          "ᾬ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Capital Letter Omega with Psili and Oxia and Prosgegrammeni",
	},
	75: {
		UniCode:           "U+1FAB",
		Grapheme:          "ᾫ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Omega with Dasia and Varia and Prosgegrammeni",
	},
	76: {
		UniCode:           "U+1FAA",
		Grapheme:          "ᾪ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Omega with Psili and Varia and Prosgegrammeni",
	},
	77: {
		UniCode:           "U+1FA9",
		Grapheme:          "ᾩ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Omega with Dasia and Prosgegrammeni",
	},
	78: {
		UniCode:           "U+1FA8",
		Grapheme:          "ᾨ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Omega with Psili and Prosgegrammeni",
	},
	79: {
		UniCode:           "U+1FA7",
		Grapheme:          "ᾧ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Omega with Dasia and Perispomeni and Ypogegrammeni",
	},
	80: {
		UniCode:           "U+1FA6",
		Grapheme:          "ᾦ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Omega with Psili and Perispomeni and Ypogegrammeni",
	},
	81: {
		UniCode:           "U+1FA5",
		Grapheme:          "ᾥ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           true,
		Description:       "Greek Small Letter Omega with Dasia and Oxia and Ypogegrammeni",
	},
	82: {
		UniCode:           "U+1FA4",
		Grapheme:          "ᾤ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Small Letter Omega with Psili and Oxia and Ypogegrammeni",
	},
	83: {
		UniCode:           "U+1FA3",
		Grapheme:          "ᾣ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Omega with Dasia and Varia and Ypogegrammeni",
	},
	84: {
		UniCode:           "U+1FA2",
		Grapheme:          "ᾢ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Omega with Psili and Varia and Ypogegrammeni",
	},
	85: {
		UniCode:           "U+1FA1",
		Grapheme:          "ᾡ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Omega with Dasia and Ypogegrammeni",
	},
	86: {
		UniCode:           "U+1FA0",
		Grapheme:          "ᾠ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Omega with Psili and Ypogegrammeni",
	},
	87: {
		UniCode:           "U+1F9F",
		Grapheme:          "ᾟ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Eta with Dasia and Perispomeni and Prosgegrammeni",
	},
	88: {
		UniCode:           "U+1F9E",
		Grapheme:          "ᾞ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Eta with Psili and Perispomeni and Prosgegrammeni",
	},
	89: {
		UniCode:           "U+1F9D",
		Grapheme:          "ᾝ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           true,
		Description:       "Greek Capital Letter Eta with Dasia and Oxia and Prosgegrammeni",
	},
	90: {
		UniCode:           "U+1F9C",
		Grapheme:          "ᾜ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Capital Letter Eta with Psili and Oxia and Prosgegrammeni",
	},
	91: {
		UniCode:           "U+1F9B",
		Grapheme:          "ᾛ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Eta with Dasia and Varia and Prosgegrammeni",
	},
	92: {
		UniCode:           "U+1F9A",
		Grapheme:          "ᾚ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Eta with Psili and Varia and Prosgegrammeni",
	},
	93: {
		UniCode:           "U+1F99",
		Grapheme:          "ᾙ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Eta with Dasia and Prosgegrammeni",
	},
	94: {
		UniCode:           "U+1F98",
		Grapheme:          "ᾘ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Eta with Psili and Prosgegrammeni",
	},
	95: {
		UniCode:           "U+1F97",
		Grapheme:          "ᾗ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Eta with Dasia and Perispomeni and Ypogegrammeni",
	},
	96: {
		UniCode:           "U+1F96",
		Grapheme:          "ᾖ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Eta with Psili and Perispomeni and Ypogegrammeni",
	},
	97: {
		UniCode:           "U+1F95",
		Grapheme:          "ᾕ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           true,
		Description:       "Greek Small Letter Eta with Dasia and Oxia and Ypogegrammeni",
	},
	98: {
		UniCode:           "U+1F94",
		Grapheme:          "ᾔ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Small Letter Eta with Psili and Oxia and Ypogegrammeni",
	},
	99: {
		UniCode:           "U+1F93",
		Grapheme:          "ᾓ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Eta with Dasia and Varia and Ypogegrammeni",
	},
	100: {
		UniCode:           "U+1F92",
		Grapheme:          "ᾒ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Eta with Psili and Varia and Ypogegrammeni",
	},
	101: {
		UniCode:           "U+1F91",
		Grapheme:          "ᾑ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Eta with Dasia and Ypogegrammeni",
	},
	102: {
		UniCode:           "U+1F90",
		Grapheme:          "ᾐ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Eta with Psili and Ypogegrammeni",
	},
	103: {
		UniCode:           "U+1F8F",
		Grapheme:          "ᾏ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Alpha with Dasia and Perispomeni and Prosgegrammeni",
	},
	104: {
		UniCode:           "U+1F8E",
		Grapheme:          "ᾎ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Alpha with Psili and Perispomeni and Prosgegrammeni",
	},
	105: {
		UniCode:           "U+1F8D",
		Grapheme:          "ᾍ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           true,
		Description:       "Greek Capital Letter Alpha with Dasia and Oxia and Prosgegrammeni",
	},
	106: {
		UniCode:           "U+1F8C",
		Grapheme:          "ᾌ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Capital Letter Alpha with Psili and Oxia and Prosgegrammeni",
	},
	107: {
		UniCode:           "U+1F8B",
		Grapheme:          "ᾋ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Alpha with Dasia and Varia and Prosgegrammeni",
	},
	108: {
		UniCode:           "U+1F8A",
		Grapheme:          "ᾊ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Alpha with Psili and Varia and Prosgegrammeni",
	},
	109: {
		UniCode:           "U+1F89",
		Grapheme:          "ᾉ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Alpha with Dasia and Prosgegrammeni",
	},
	110: {
		UniCode:           "U+1F88",
		Grapheme:          "ᾈ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: true,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Alpha with Psili and Prosgegrammeni",
	},
	111: {
		UniCode:           "U+1F87",
		Grapheme:          "ᾇ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Alpha with Dasia and Perispomeni and Ypogegrammeni",
	},
	112: {
		UniCode:           "U+1F86",
		Grapheme:          "ᾆ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Alpha with Psili and Perispomeni and Ypogegrammeni",
	},
	113: {
		UniCode:           "U+1F85",
		Grapheme:          "ᾅ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           true,
		Description:       "Greek Small Letter Alpha with Dasia and Oxia and Ypogegrammeni",
	},
	114: {
		UniCode:           "U+1F84",
		Grapheme:          "ᾄ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Small Letter Alpha with Psili and Oxia and Ypogegrammeni",
	},
	115: {
		UniCode:           "U+1F83",
		Grapheme:          "ᾃ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Alpha with Dasia and Varia and Ypogegrammeni",
	},
	116: {
		UniCode:           "U+1F82",
		Grapheme:          "ᾂ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Alpha with Psili and Varia and Ypogegrammeni",
	},
	117: {
		UniCode:           "U+1F81",
		Grapheme:          "ᾁ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Alpha with Dasia and Ypogegrammeni",
	},
	118: {
		UniCode:           "U+1F80",
		Grapheme:          "ᾀ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  true,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Alpha with Psili and Ypogegrammeni",
	},
	119: {
		UniCode:           "U+1F7D",
		Grapheme:          "ώ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Small Letter Omega with Oxia",
	},
	120: {
		UniCode:           "U+1F7C",
		Grapheme:          "ὼ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Omega with Varia",
	},
	121: {
		UniCode:           "U+1F7B",
		Grapheme:          "ύ",
		Base:              "υ",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Small Letter Upsilon with Oxia",
	},
	122: {
		UniCode:           "U+1F7A",
		Grapheme:          "ὺ",
		Base:              "υ",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Upsilon with Varia",
	},
	123: {
		UniCode:           "U+1F79",
		Grapheme:          "ό",
		Base:              "ο",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Small Letter Omicron with Oxia",
	},
	124: {
		UniCode:           "U+1F78",
		Grapheme:          "ὸ",
		Base:              "ο",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Omicron with Varia",
	},
	125: {
		UniCode:           "U+1F77",
		Grapheme:          "ί",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Small Letter Iota with Oxia",
	},
	126: {
		UniCode:           "U+1F76",
		Grapheme:          "ὶ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Iota with Varia",
	},
	127: {
		UniCode:           "U+1F75",
		Grapheme:          "ή",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Small Letter Eta with Oxia",
	},
	128: {
		UniCode:           "U+1F74",
		Grapheme:          "ὴ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Eta with Varia",
	},
	129: {
		UniCode:           "U+1F73",
		Grapheme:          "έ",
		Base:              "ε",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Small Letter Epsilon with Oxia",
	},
	130: {
		UniCode:           "U+1F72",
		Grapheme:          "ὲ",
		Base:              "ε",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Epsilon with Varia",
	},
	131: {
		UniCode:           "U+1F71",
		Grapheme:          "ά",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Small Letter Alpha with Oxia",
	},
	132: {
		UniCode:           "U+1F70",
		Grapheme:          "ὰ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Alpha with Varia",
	},
	133: {
		UniCode:           "U+1F6F",
		Grapheme:          "Ὧ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Omega with Dasia and Perispomeni",
	},
	134: {
		UniCode:           "U+1F6E",
		Grapheme:          "Ὦ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Omega with Psili and Perispomeni",
	},
	135: {
		UniCode:           "U+1F6D",
		Grapheme:          "Ὥ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           true,
		Description:       "Greek Capital Letter Omega with Dasia and Oxia",
	},
	136: {
		UniCode:           "U+1F6C",
		Grapheme:          "Ὤ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Capital Letter Omega with Psili and Oxia",
	},
	137: {
		UniCode:           "U+1F6B",
		Grapheme:          "Ὣ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Omega with Dasia and Varia",
	},
	138: {
		UniCode:           "U+1F6A",
		Grapheme:          "Ὢ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Omega with Psili and Varia",
	},
	139: {
		UniCode:           "U+1F69",
		Grapheme:          "Ὡ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Omega with Dasia",
	},
	140: {
		UniCode:           "U+1F68",
		Grapheme:          "Ὠ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Omega with Psili",
	},
	141: {
		UniCode:           "U+1F67",
		Grapheme:          "ὧ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Omega with Dasia and Perispomeni",
	},
	142: {
		UniCode:           "U+1F66",
		Grapheme:          "ὦ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Omega with Psili and Perispomeni",
	},
	143: {
		UniCode:           "U+1F65",
		Grapheme:          "ὥ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           true,
		Description:       "Greek Small Letter Omega with Dasia and Oxia",
	},
	144: {
		UniCode:           "U+1F64",
		Grapheme:          "ὤ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Small Letter Omega with Psili and Oxia",
	},
	145: {
		UniCode:           "U+1F63",
		Grapheme:          "ὣ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Omega with Dasia and Varia",
	},
	146: {
		UniCode:           "U+1F62",
		Grapheme:          "ὢ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Omega with Psili and Varia",
	},
	147: {
		UniCode:           "U+1F61",
		Grapheme:          "ὡ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Omega with Dasia",
	},
	148: {
		UniCode:           "U+1F60",
		Grapheme:          "ὠ",
		Base:              "ω",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Omega with Psili",
	},
	149: {
		UniCode:           "U+1F5F",
		Grapheme:          "Ὗ",
		Base:              "υ",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Upsilon with Dasia and Perispomeni",
	},
	150: {
		UniCode:           "U+1F5D",
		Grapheme:          "Ὕ",
		Base:              "υ",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           true,
		Description:       "Greek Capital Letter Upsilon with Dasia and Oxia",
	},
	151: {
		UniCode:           "U+1F5B",
		Grapheme:          "Ὓ",
		Base:              "υ",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Upsilon with Dasia and Varia",
	},
	152: {
		UniCode:           "U+1F59",
		Grapheme:          "Ὑ",
		Base:              "υ",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Upsilon with Dasia",
	},
	153: {
		UniCode:           "U+1F57",
		Grapheme:          "ὗ",
		Base:              "υ",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Upsilon with Dasia and Perispomeni",
	},
	154: {
		UniCode:           "U+1F56",
		Grapheme:          "ὖ",
		Base:              "υ",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Upsilon with Psili and Perispomeni",
	},
	155: {
		UniCode:           "U+1F55",
		Grapheme:          "ὕ",
		Base:              "υ",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           true,
		Description:       "Greek Small Letter Upsilon with Dasia and Oxia",
	},
	156: {
		UniCode:           "U+1F54",
		Grapheme:          "ὔ",
		Base:              "υ",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Small Letter Upsilon with Psili and Oxia",
	},
	157: {
		UniCode:           "U+1F53",
		Grapheme:          "ὓ",
		Base:              "υ",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Upsilon with Dasia and Varia",
	},
	158: {
		UniCode:           "U+1F52",
		Grapheme:          "ὒ",
		Base:              "υ",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Upsilon with Psili and Varia",
	},
	159: {
		UniCode:           "U+1F51",
		Grapheme:          "ὑ",
		Base:              "υ",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Upsilon with Dasia",
	},
	160: {
		UniCode:           "U+1F50",
		Grapheme:          "ὐ",
		Base:              "υ",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Upsilon with Psili",
	},
	161: {
		UniCode:           "U+1F4D",
		Grapheme:          "Ὅ",
		Base:              "ο",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           true,
		Description:       "Greek Capital Letter Omicron with Dasia and Oxia",
	},
	162: {
		UniCode:           "U+1F4C",
		Grapheme:          "Ὄ",
		Base:              "ο",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Capital Letter Omicron with Psili and Oxia",
	},
	163: {
		UniCode:           "U+1F4B",
		Grapheme:          "Ὃ",
		Base:              "ο",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Omicron with Dasia and Varia",
	},
	164: {
		UniCode:           "U+1F4A",
		Grapheme:          "Ὂ",
		Base:              "ο",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Omicron with Psili and Varia",
	},
	165: {
		UniCode:           "U+1F49",
		Grapheme:          "Ὁ",
		Base:              "ο",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Omicron with Dasia",
	},
	166: {
		UniCode:           "U+1F48",
		Grapheme:          "Ὀ",
		Base:              "ο",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Omicron with Psili",
	},
	167: {
		UniCode:           "U+1F45",
		Grapheme:          "ὅ",
		Base:              "ο",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           true,
		Description:       "Greek Small Letter Omicron with Dasia and Oxia",
	},
	168: {
		UniCode:           "U+1F44",
		Grapheme:          "ὄ",
		Base:              "ο",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Small Letter Omicron with Psili and Oxia",
	},
	169: {
		UniCode:           "U+1F43",
		Grapheme:          "ὃ",
		Base:              "ο",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Omicron with Dasia and Varia",
	},
	170: {
		UniCode:           "U+1F42",
		Grapheme:          "ὂ",
		Base:              "ο",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Omicron with Psili and Varia",
	},
	171: {
		UniCode:           "U+1F41",
		Grapheme:          "ὁ",
		Base:              "ο",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Omicron with Dasia",
	},
	172: {
		UniCode:           "U+1F40",
		Grapheme:          "ὀ",
		Base:              "ο",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Omicron with Psili",
	},
	173: {
		UniCode:           "U+1F3F",
		Grapheme:          "Ἷ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Iota with Dasia and Perispomeni",
	},
	174: {
		UniCode:           "U+1F3E",
		Grapheme:          "Ἶ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Iota with Psili and Perispomeni",
	},
	175: {
		UniCode:           "U+1F3D",
		Grapheme:          "Ἵ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           true,
		Description:       "Greek Capital Letter Iota with Dasia and Oxia",
	},
	176: {
		UniCode:           "U+1F3C",
		Grapheme:          "Ἴ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Capital Letter Iota with Psili and Oxia",
	},
	177: {
		UniCode:           "U+1F3B",
		Grapheme:          "Ἳ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Iota with Dasia and Varia",
	},
	178: {
		UniCode:           "U+1F3A",
		Grapheme:          "Ἲ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Iota with Psili and Varia",
	},
	179: {
		UniCode:           "U+1F39",
		Grapheme:          "Ἱ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Iota with Dasia",
	},
	180: {
		UniCode:           "U+1F38",
		Grapheme:          "Ἰ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Iota with Psili",
	},
	181: {
		UniCode:           "U+1F37",
		Grapheme:          "ἷ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Iota with Dasia and Perispomeni",
	},
	182: {
		UniCode:           "U+1F36",
		Grapheme:          "ἶ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Iota with Psili and Perispomeni",
	},
	183: {
		UniCode:           "U+1F35",
		Grapheme:          "ἵ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           true,
		Description:       "Greek Small Letter Iota with Dasia and Oxia",
	},
	184: {
		UniCode:           "U+1F34",
		Grapheme:          "ἴ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Small Letter Iota with Psili and Oxia",
	},
	185: {
		UniCode:           "U+1F33",
		Grapheme:          "ἳ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Iota with Dasia and Varia",
	},
	186: {
		UniCode:           "U+1F32",
		Grapheme:          "ἲ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Iota with Psili and Varia",
	},
	187: {
		UniCode:           "U+1F31",
		Grapheme:          "ἱ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Iota with Dasia",
	},
	188: {
		UniCode:           "U+1F30",
		Grapheme:          "ἰ",
		Base:              "ι",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Iota with Psili",
	},
	189: {
		UniCode:           "U+1F2F",
		Grapheme:          "Ἧ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Eta with Dasia and Perispomeni",
	},
	190: {
		UniCode:           "U+1F2E",
		Grapheme:          "Ἦ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Eta with Psili and Perispomeni",
	},
	191: {
		UniCode:           "U+1F2D",
		Grapheme:          "Ἥ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           true,
		Description:       "Greek Capital Letter Eta with Dasia and Oxia",
	},
	192: {
		UniCode:           "U+1F2C",
		Grapheme:          "Ἤ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Capital Letter Eta with Psili and Oxia",
	},
	193: {
		UniCode:           "U+1F2B",
		Grapheme:          "Ἣ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Eta with Dasia and Varia",
	},
	194: {
		UniCode:           "U+1F2A",
		Grapheme:          "Ἢ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Eta with Psili and Varia",
	},
	195: {
		UniCode:           "U+1F29",
		Grapheme:          "Ἡ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Eta with Dasia",
	},
	196: {
		UniCode:           "U+1F28",
		Grapheme:          "Ἠ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Eta with Psili",
	},
	197: {
		UniCode:           "U+1F27",
		Grapheme:          "ἧ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Eta with Dasia and Perispomeni",
	},
	198: {
		UniCode:           "U+1F26",
		Grapheme:          "ἦ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Eta with Psili and Perispomeni",
	},
	199: {
		UniCode:           "U+1F25",
		Grapheme:          "ἥ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           true,
		Description:       "Greek Small Letter Eta with Dasia and Oxia",
	},
	200: {
		UniCode:           "U+1F24",
		Grapheme:          "ἤ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Small Letter Eta with Psili and Oxia",
	},
	201: {
		UniCode:           "U+1F23",
		Grapheme:          "ἣ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Eta with Dasia and Varia",
	},
	202: {
		UniCode:           "U+1F22",
		Grapheme:          "ἢ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Eta with Psili and Varia",
	},
	203: {
		UniCode:           "U+1F21",
		Grapheme:          "ἡ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Eta with Dasia",
	},
	204: {
		UniCode:           "U+1F20",
		Grapheme:          "ἠ",
		Base:              "η",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Eta with Psili",
	},
	205: {
		UniCode:           "U+1F1D",
		Grapheme:          "Ἕ",
		Base:              "ε",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           true,
		Description:       "Greek Capital Letter Epsilon with Dasia and Oxia",
	},
	206: {
		UniCode:           "U+1F1C",
		Grapheme:          "Ἔ",
		Base:              "ε",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Capital Letter Epsilon with Psili and Oxia",
	},
	207: {
		UniCode:           "U+1F1B",
		Grapheme:          "Ἓ",
		Base:              "ε",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Epsilon with Dasia and Varia",
	},
	208: {
		UniCode:           "U+1F1A",
		Grapheme:          "Ἒ",
		Base:              "ε",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Epsilon with Psili and Varia",
	},
	209: {
		UniCode:           "U+1F19",
		Grapheme:          "Ἑ",
		Base:              "ε",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Epsilon with Dasia",
	},
	210: {
		UniCode:           "U+1F18",
		Grapheme:          "Ἐ",
		Base:              "ε",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Epsilon with Psili",
	},
	211: {
		UniCode:           "U+1F15",
		Grapheme:          "ἕ",
		Base:              "ε",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           true,
		Description:       "Greek Small Letter Epsilon with Dasia and Oxia",
	},
	212: {
		UniCode:           "U+1F14",
		Grapheme:          "ἔ",
		Base:              "ε",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Small Letter Epsilon with Psili and Oxia",
	},
	213: {
		UniCode:           "U+1F13",
		Grapheme:          "ἓ",
		Base:              "ε",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Epsilon with Dasia and Varia",
	},
	214: {
		UniCode:           "U+1F12",
		Grapheme:          "ἒ",
		Base:              "ε",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Epsilon with Psili and Varia",
	},
	215: {
		UniCode:           "U+1F11",
		Grapheme:          "ἑ",
		Base:              "ε",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Epsilon with Dasia",
	},
	216: {
		UniCode:           "U+1F10",
		Grapheme:          "ἐ",
		Base:              "ε",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Epsilon with Psili",
	},
	217: {
		UniCode:           "U+1F0F",
		Grapheme:          "Ἇ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Alpha with Dasia and Perispomeni",
	},
	218: {
		UniCode:           "U+1F0E",
		Grapheme:          "Ἆ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Alpha with Psili and Perispomeni",
	},
	219: {
		UniCode:           "U+1F0D",
		Grapheme:          "Ἅ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           true,
		Description:       "Greek Capital Letter Alpha with Dasia and Oxia",
	},
	220: {
		UniCode:           "U+1F0C",
		Grapheme:          "Ἄ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Capital Letter Alpha with Psili and Oxia",
	},
	221: {
		UniCode:           "U+1F0B",
		Grapheme:          "Ἃ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Alpha with Dasia and Varia",
	},
	222: {
		UniCode:           "U+1F0A",
		Grapheme:          "Ἂ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Alpha with Psili and Varia",
	},
	223: {
		UniCode:           "U+1F09",
		Grapheme:          "Ἁ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Capital Letter Alpha with Dasia",
	},
	224: {
		UniCode:           "U+1F08",
		Grapheme:          "Ἀ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           false,
		IsUpper:           true,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Capital Letter Alpha with Psili",
	},
	225: {
		UniCode:           "U+1F07",
		Grapheme:          "ἇ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Alpha with Dasia and Perispomeni",
	},
	226: {
		UniCode:           "U+1F06",
		Grapheme:          "ἆ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    true,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Alpha with Psili and Perispomeni",
	},
	227: {
		UniCode:           "U+1F05",
		Grapheme:          "ἅ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           true,
		Description:       "Greek Small Letter Alpha with Dasia and Oxia",
	},
	228: {
		UniCode:           "U+1F04",
		Grapheme:          "ἄ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           true,
		Description:       "Greek Small Letter Alpha with Psili and Oxia",
	},
	229: {
		UniCode:           "U+1F03",
		Grapheme:          "ἃ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Alpha with Dasia and Varia",
	},
	230: {
		UniCode:           "U+1F02",
		Grapheme:          "ἂ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          true,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Alpha with Psili and Varia",
	},
	231: {
		UniCode:           "U+1F01",
		Grapheme:          "ἁ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          false,
		HasDasia:          true,
		HasOxia:           false,
		Description:       "Greek Small Letter Alpha with Dasia",
	},
	232: {
		UniCode:           "U+1F00",
		Grapheme:          "ἀ",
		Base:              "α",
		IsConsonant:       false,
		IsPunctuation:     false,
		IsVowel:           true,
		IsLower:           true,
		IsUpper:           false,
		HasDiacritics:     true,
		HasVaria:          false,
		HasPerispomeni:    false,
		HasYpogegrammeni:  false,
		HasProsgegrammeni: false,
		HasVrachy:         false,
		HasMacron:         false,
		HasDialytika:      false,
		HasPsili:          true,
		HasDasia:          false,
		HasOxia:           false,
		Description:       "Greek Small Letter Alpha with Psili",
	},
}

// SetMap provides keys to use in the LetterMap.
// The keys for SetMap are int constants of type SetKey.
// The values are sets of letters, e.g.
// the set of all consonants, or set of all vowels.
// For example, to find all entries in LetterMap that are
// consonants:
// indexes := LookupMap[Consonants]
//
//	for _, i := range indexes {
//	   c := LetterMap[i]
//	}
var SetMap = map[SetKey][]int{0: {59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 131, 132, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232},
	1:  {59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 131, 132, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232},
	2:  {64, 65, 66, 67, 68, 69, 70, 111, 112, 113, 114, 115, 116, 117, 118, 131, 132, 225, 226, 227, 228, 229, 230, 231, 232},
	3:  {59, 60, 61, 62, 63, 103, 104, 105, 106, 107, 108, 109, 110, 217, 218, 219, 220, 221, 222, 223, 224},
	4:  {15, 22, 23},
	5:  {15, 22, 139, 147, 152, 159, 165, 171, 179, 187, 195, 203, 209, 215, 223, 231},
	6:  {135, 143, 150, 155, 161, 167, 175, 183, 191, 199, 205, 211, 219, 227},
	7:  {73, 89, 105},
	8:  {81, 97, 113},
	9:  {133, 141, 149, 153, 173, 181, 189, 197, 217, 225},
	10: {71, 87, 103},
	11: {79, 95, 111},
	12: {77, 93, 109},
	13: {137, 145, 151, 157, 163, 169, 177, 185, 193, 201, 207, 213, 221, 229},
	14: {75, 91, 107},
	15: {83, 99, 115},
	16: {85, 101, 117},
	17: {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232},
	18: {24, 37},
	19: {20, 35},
	20: {25, 38},
	21: {47, 48, 129, 130, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216},
	22: {47, 48, 129, 130, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216},
	23: {129, 130, 211, 212, 213, 214, 215, 216},
	24: {47, 48, 205, 206, 207, 208, 209, 210},
	25: {44, 45, 46, 49, 50, 51, 52, 53, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 127, 128, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204},
	26: {44, 45, 46, 49, 50, 51, 52, 53, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 127, 128, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204},
	27: {49, 50, 51, 52, 53, 95, 96, 97, 98, 99, 100, 101, 102, 127, 128, 197, 198, 199, 200, 201, 202, 203, 204},
	28: {44, 45, 46, 87, 88, 89, 90, 91, 92, 93, 94, 189, 190, 191, 192, 193, 194, 195, 196},
	29: {31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 125, 126, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188},
	30: {31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 125, 126, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188},
	31: {35, 36, 37, 38, 39, 40, 125, 126, 181, 182, 183, 184, 185, 186, 187, 188},
	32: {31, 32, 33, 34, 173, 174, 175, 176, 177, 178, 179, 180},
	33: {18, 26, 33, 39, 62, 69},
	34: {2, 3, 4, 7, 8, 9, 10, 11, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 119, 120, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148},
	35: {2, 3, 4, 7, 8, 9, 10, 11, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 119, 120, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148},
	36: {7, 8, 9, 10, 11, 79, 80, 81, 82, 83, 84, 85, 86, 119, 120, 141, 142, 143, 144, 145, 146, 147, 148},
	37: {2, 3, 4, 71, 72, 73, 74, 75, 76, 77, 78, 133, 134, 135, 136, 137, 138, 139, 140},
	38: {5, 6, 123, 124, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172},
	39: {5, 6, 123, 124, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172},
	40: {123, 124, 167, 168, 169, 170, 171, 172},
	41: {5, 6, 161, 162, 163, 164, 165, 166},
	42: {3, 5, 16, 31, 45, 47, 60, 119, 121, 123, 125, 127, 129, 131},
	43: {9, 51, 66},
	44: {8, 21, 36, 50, 65},
	45: {7, 49, 64},
	46: {2, 44, 59},
	47: {23, 140, 148, 160, 166, 172, 180, 188, 196, 204, 210, 216, 224, 232},
	48: {136, 144, 156, 162, 168, 176, 184, 192, 200, 206, 212, 220, 228},
	49: {74, 90, 106},
	50: {82, 98, 114},
	51: {72, 80, 88, 96, 104, 112, 134, 142, 154, 174, 182, 190, 198, 218, 226},
	54: {78, 94, 110},
	55: {138, 146, 158, 164, 170, 178, 186, 194, 202, 208, 214, 222, 230},
	56: {76, 92, 108},
	57: {84, 100, 116},
	58: {86, 102, 118},
	59: {15, 22, 23},
	60: {15, 22, 23},
	61: {22, 23},
	62: {15},
	63: {16, 17, 18, 19, 20, 21, 24, 25, 26, 27, 121, 122, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160},
	64: {16, 17, 18, 19, 20, 21, 24, 25, 26, 27, 121, 122, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160},
	65: {20, 21, 24, 25, 26, 27, 121, 122, 153, 154, 155, 156, 157, 158, 159, 160},
	66: {16, 17, 18, 19, 149, 150, 151, 152},
	67: {4, 6, 17, 32, 46, 48, 61, 120, 122, 124, 126, 128, 130, 132},
	68: {11, 53, 68},
	69: {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 16, 17, 18, 19, 20, 21, 24, 25, 26, 27, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232},
	70: {19, 27, 34, 40, 63, 70},
	71: {10, 52, 67},
}

// GraphemeMap The keys for GraphemeMap are unicode graphemes,
// that is, the letter as it displays for end users.
// The keys in this map are in unicode normal form c.
// Normalize your key before using it with the map, e.g.
// k := "ά" // your key
// k = norm.NFC.String(k) // unicode normal form c
// i := GraphemeMap[k] // lookup the int key
// l := LetterMap[i] // use the int key to get the Letter info.
var GraphemeMap = map[string]int{"`": 220,
	"´": 231,
	"΅": 219,
	"Ά": 172,
	"Έ": 185,
	"Ή": 187,
	"Ί": 201,
	"Ό": 227,
	"Ύ": 216,
	"Ώ": 229,
	"ΐ": 195,
	"ά": 101,
	"έ": 103,
	"ή": 105,
	"ί": 107,
	"ΰ": 208,
	"ι": 175,
	"ό": 109,
	"ύ": 111,
	"ώ": 113,
	"ἀ": 0,
	"ἁ": 1,
	"ἂ": 2,
	"ἃ": 3,
	"ἄ": 4,
	"ἅ": 5,
	"ἆ": 6,
	"ἇ": 7,
	"Ἀ": 8,
	"Ἁ": 9,
	"Ἂ": 10,
	"Ἃ": 11,
	"Ἄ": 12,
	"Ἅ": 13,
	"Ἆ": 14,
	"Ἇ": 15,
	"ἐ": 16,
	"ἑ": 17,
	"ἒ": 18,
	"ἓ": 19,
	"ἔ": 20,
	"ἕ": 21,
	"Ἐ": 22,
	"Ἑ": 23,
	"Ἒ": 24,
	"Ἓ": 25,
	"Ἔ": 26,
	"Ἕ": 27,
	"ἠ": 28,
	"ἡ": 29,
	"ἢ": 30,
	"ἣ": 31,
	"ἤ": 32,
	"ἥ": 33,
	"ἦ": 34,
	"ἧ": 35,
	"Ἠ": 36,
	"Ἡ": 37,
	"Ἢ": 38,
	"Ἣ": 39,
	"Ἤ": 40,
	"Ἥ": 41,
	"Ἦ": 42,
	"Ἧ": 43,
	"ἰ": 44,
	"ἱ": 45,
	"ἲ": 46,
	"ἳ": 47,
	"ἴ": 48,
	"ἵ": 49,
	"ἶ": 50,
	"ἷ": 51,
	"Ἰ": 52,
	"Ἱ": 53,
	"Ἲ": 54,
	"Ἳ": 55,
	"Ἴ": 56,
	"Ἵ": 57,
	"Ἶ": 58,
	"Ἷ": 59,
	"ὀ": 60,
	"ὁ": 61,
	"ὂ": 62,
	"ὃ": 63,
	"ὄ": 64,
	"ὅ": 65,
	"Ὀ": 66,
	"Ὁ": 67,
	"Ὂ": 68,
	"Ὃ": 69,
	"Ὄ": 70,
	"Ὅ": 71,
	"ὐ": 72,
	"ὑ": 73,
	"ὒ": 74,
	"ὓ": 75,
	"ὔ": 76,
	"ὕ": 77,
	"ὖ": 78,
	"ὗ": 79,
	"Ὑ": 80,
	"Ὓ": 81,
	"Ὕ": 82,
	"Ὗ": 83,
	"ὠ": 84,
	"ὡ": 85,
	"ὢ": 86,
	"ὣ": 87,
	"ὤ": 88,
	"ὥ": 89,
	"ὦ": 90,
	"ὧ": 91,
	"Ὠ": 92,
	"Ὡ": 93,
	"Ὢ": 94,
	"Ὣ": 95,
	"Ὤ": 96,
	"Ὥ": 97,
	"Ὦ": 98,
	"Ὧ": 99,
	"ὰ": 100,
	"ὲ": 102,
	"ὴ": 104,
	"ὶ": 106,
	"ὸ": 108,
	"ὺ": 110,
	"ὼ": 112,
	"ᾀ": 114,
	"ᾁ": 115,
	"ᾂ": 116,
	"ᾃ": 117,
	"ᾄ": 118,
	"ᾅ": 119,
	"ᾆ": 120,
	"ᾇ": 121,
	"ᾈ": 122,
	"ᾉ": 123,
	"ᾊ": 124,
	"ᾋ": 125,
	"ᾌ": 126,
	"ᾍ": 127,
	"ᾎ": 128,
	"ᾏ": 129,
	"ᾐ": 130,
	"ᾑ": 131,
	"ᾒ": 132,
	"ᾓ": 133,
	"ᾔ": 134,
	"ᾕ": 135,
	"ᾖ": 136,
	"ᾗ": 137,
	"ᾘ": 138,
	"ᾙ": 139,
	"ᾚ": 140,
	"ᾛ": 141,
	"ᾜ": 142,
	"ᾝ": 143,
	"ᾞ": 144,
	"ᾟ": 145,
	"ᾠ": 146,
	"ᾡ": 147,
	"ᾢ": 148,
	"ᾣ": 149,
	"ᾤ": 150,
	"ᾥ": 151,
	"ᾦ": 152,
	"ᾧ": 153,
	"ᾨ": 154,
	"ᾩ": 155,
	"ᾪ": 156,
	"ᾫ": 157,
	"ᾬ": 158,
	"ᾭ": 159,
	"ᾮ": 160,
	"ᾯ": 161,
	"ᾰ": 162,
	"ᾱ": 163,
	"ᾲ": 164,
	"ᾳ": 165,
	"ᾴ": 166,
	"ᾶ": 167,
	"ᾷ": 168,
	"Ᾰ": 169,
	"Ᾱ": 170,
	"Ὰ": 171,
	"ᾼ": 173,
	"᾽": 174,
	"᾿": 176,
	"῀": 177,
	"῁": 178,
	"ῂ": 179,
	"ῃ": 180,
	"ῄ": 181,
	"ῆ": 182,
	"ῇ": 183,
	"Ὲ": 184,
	"Ὴ": 186,
	"ῌ": 188,
	"῍": 189,
	"῎": 190,
	"῏": 191,
	"ῐ": 192,
	"ῑ": 193,
	"ῒ": 194,
	"ῖ": 196,
	"ῗ": 197,
	"Ῐ": 198,
	"Ῑ": 199,
	"Ὶ": 200,
	"῝": 202,
	"῞": 203,
	"῟": 204,
	"ῠ": 205,
	"ῡ": 206,
	"ῢ": 207,
	"ῤ": 209,
	"ῥ": 210,
	"ῦ": 211,
	"ῧ": 212,
	"Ῠ": 213,
	"Ῡ": 214,
	"Ὺ": 215,
	"Ῥ": 217,
	"῭": 218,
	"ῲ": 221,
	"ῳ": 222,
	"ῴ": 223,
	"ῶ": 224,
	"ῷ": 225,
	"Ὸ": 226,
	"Ὼ": 228,
	"ῼ": 230,
	"῾": 232,
}

// SetKey int constants as keys for the SetMap
type SetKey int

const (
	Alpha SetKey = iota
	AlphaDiacritics
	AlphaLower
	AlphaUpper
	Consonants
	Dasia
	DasiaOxia
	DasiaOxiaProsgegrammeni
	DasiaOxiaYpogegrammeni
	DasiaPerispomeni
	DasiaPerispomeniProsgegrammeni
	DasiaPerispomeniYpogegrammeni
	DasiaProsgegrammeni
	DasiaVaria
	DasiaVariaProsgegrammeni
	DasiaVariaYpogegrammeni
	DasiaYpogegrammeni
	Diacritics
	DialytikaOxia
	DialytikaPerispomeni
	DialytikaVaria
	Epsilon
	EpsilonDiacritics
	EpsilonLower
	EpsilonUpper
	Eta
	EtaDiacritics
	EtaLower
	EtaUpper
	Iota
	IotaDiacritics
	IotaLower
	IotaUpper
	Macron
	Omega
	OmegaDiacritics
	OmegaLower
	OmegaUpper
	Omicron
	OmicronDiacritics
	OmicronLower
	OmicronUpper
	Oxia
	OxiaYpogegrammeni
	Perispomeni
	PerispomeniYpogegrammeni
	Prosgegrammeni
	Psili
	PsiliOxia
	PsiliOxiaProsgegrammeni
	PsiliOxiaYpogegrammeni
	PsiliPerispomeni
	PsiliPerispomeniProsgegrammeni
	PsiliPerispomeniYpogegrammeni
	PsiliProsgegrammeni
	PsiliVaria
	PsiliVariaProsgegrammeni
	PsiliVariaYpogegrammeni
	PsiliYpogegrammeni
	Rho
	RhoDiacritics
	RhoLower
	RhoUpper
	Upsilon
	UpsilonDiacritics
	UpsilonLower
	UpsilonUpper
	Varia
	VariaYpogegrammeni
	Vowels
	Vrachy
	Ypogegrammeni
)

func Name(s SetKey) string {
	switch s {
	case Alpha:
		return "Alpha"
	case AlphaDiacritics:
		return "AlphaDiacritics"
	case AlphaLower:
		return "AlphaLower"
	case AlphaUpper:
		return "AlphaUpper"
	case Consonants:
		return "Consonants"
	case Dasia:
		return "Dasia"
	case DasiaOxia:
		return "DasiaOxia"
	case DasiaOxiaProsgegrammeni:
		return "DasiaOxiaProsgegrammeni"
	case DasiaOxiaYpogegrammeni:
		return "DasiaOxiaYpogegrammeni"
	case DasiaPerispomeni:
		return "DasiaPerispomeni"
	case DasiaPerispomeniProsgegrammeni:
		return "DasiaPerispomeniProsgegrammeni"
	case DasiaPerispomeniYpogegrammeni:
		return "DasiaPerispomeniYpogegrammeni"
	case DasiaProsgegrammeni:
		return "DasiaProsgegrammeni"
	case DasiaVaria:
		return "DasiaVaria"
	case DasiaVariaProsgegrammeni:
		return "DasiaVariaProsgegrammeni"
	case DasiaVariaYpogegrammeni:
		return "DasiaVariaYpogegrammeni"
	case DasiaYpogegrammeni:
		return "DasiaYpogegrammeni"
	case Diacritics:
		return "Diacritics"
	case DialytikaOxia:
		return "DialytikaOxia"
	case DialytikaPerispomeni:
		return "DialytikaPerispomeni"
	case DialytikaVaria:
		return "DialytikaVaria"
	case Epsilon:
		return "Epsilon"
	case EpsilonDiacritics:
		return "EpsilonDiacritics"
	case EpsilonLower:
		return "EpsilonLower"
	case EpsilonUpper:
		return "EpsilonUpper"
	case Eta:
		return "Eta"
	case EtaDiacritics:
		return "EtaDiacritics"
	case EtaLower:
		return "EtaLower"
	case EtaUpper:
		return "EtaUpper"
	case Iota:
		return "Iota"
	case IotaDiacritics:
		return "IotaDiacritics"
	case IotaLower:
		return "IotaLower"
	case IotaUpper:
		return "IotaUpper"
	case Macron:
		return "Macron"
	case Omega:
		return "Omega"
	case OmegaDiacritics:
		return "OmegaDiacritics"
	case OmegaLower:
		return "OmegaLower"
	case OmegaUpper:
		return "OmegaUpper"
	case Omicron:
		return "Omicron"
	case OmicronDiacritics:
		return "OmicronDiacritics"
	case OmicronLower:
		return "OmicronLower"
	case OmicronUpper:
		return "OmicronUpper"
	case Oxia:
		return "Oxia"
	case OxiaYpogegrammeni:
		return "OxiaYpogegrammeni"
	case Perispomeni:
		return "Perispomeni"
	case PerispomeniYpogegrammeni:
		return "PerispomeniYpogegrammeni"
	case Prosgegrammeni:
		return "Prosgegrammeni"
	case Psili:
		return "Psili"
	case PsiliOxia:
		return "PsiliOxia"
	case PsiliOxiaProsgegrammeni:
		return "PsiliOxiaProsgegrammeni"
	case PsiliOxiaYpogegrammeni:
		return "PsiliOxiaYpogegrammeni"
	case PsiliPerispomeni:
		return "PsiliPerispomeni"
	case PsiliPerispomeniProsgegrammeni:
		return "PsiliPerispomeniProsgegrammeni"
	case PsiliPerispomeniYpogegrammeni:
		return "PsiliPerispomeniYpogegrammeni"
	case PsiliProsgegrammeni:
		return "PsiliProsgegrammeni"
	case PsiliVaria:
		return "PsiliVaria"
	case PsiliVariaProsgegrammeni:
		return "PsiliVariaProsgegrammeni"
	case PsiliVariaYpogegrammeni:
		return "PsiliVariaYpogegrammeni"
	case PsiliYpogegrammeni:
		return "PsiliYpogegrammeni"
	case Rho:
		return "Rho"
	case RhoDiacritics:
		return "RhoDiacritics"
	case RhoLower:
		return "RhoLower"
	case RhoUpper:
		return "RhoUpper"
	case Upsilon:
		return "Upsilon"
	case UpsilonDiacritics:
		return "UpsilonDiacritics"
	case UpsilonLower:
		return "UpsilonLower"
	case UpsilonUpper:
		return "UpsilonUpper"
	case Varia:
		return "Varia"
	case VariaYpogegrammeni:
		return "VariaYpogegrammeni"
	case Vowels:
		return "Vowels"
	case Vrachy:
		return "Vrachy"
	case Ypogegrammeni:
		return "Ypogegrammeni"
	default:
		return ""
	}
}
