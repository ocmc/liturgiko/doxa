// Package users provides a means to manage user information in an encrypted vault separate from the Doxa database.
// It supports both a single user and multi-user use of Doxa.
package users

import (
	json "encoding/json"
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/dvcs"
	"github.com/liturgiko/doxa/pkg/enums/licenseTypes"
	"github.com/liturgiko/doxa/pkg/license"
	"github.com/liturgiko/doxa/pkg/readme"
	"github.com/liturgiko/doxa/pkg/vault"
	"github.com/xanzy/go-gitlab"
	"strings"
)

const usersDir = "users"
const profileKey = "profile"
const tokenKey = "token" // personal access token for remote repositories, e.g. gitlab
const userPwdKey = "pwd" //
const LocalUser = "localUser"

// Manager is for a specific encrypted vault.
// It provides methods for creating,
// reading, and writing user profiles
// and personal access tokens
// in the specified vault.
type Manager struct {
	Vault       *vault.Manager
	VaultPath   string
	IsDecrypted bool
}

func NewManager(vaultPath string, vault *vault.Manager) (*Manager, error) {
	m := new(Manager)
	m.Vault = vault
	m.VaultPath = vaultPath
	return m, nil
}

type Profile struct {
	Gitlab   *Gitlab
	UserName string
	PWD      string
}

func NewProfile(userName string) *Profile {
	p := new(Profile)
	p.Gitlab = NewGitlab()
	p.UserName = userName
	return p
}
func ProfileFromJson(profile string) (*Profile, error) {
	p := new(Profile)
	err := json.Unmarshal([]byte(profile), p)
	return p, err
}
func (p *Profile) ToString() (string, error) {
	jsonStr, err := json.Marshal(p)
	if err != nil {
		return "", err
	}
	return string(jsonStr), nil
}

// License returns a license with all properties set except Workname
func (p *Profile) License() *license.License {
	l := new(license.License)
	l.Contact = p.Gitlab.Contact
	l.Copyright = p.Gitlab.Copyright
	l.LicenseType = licenseTypes.TypeForString(p.Gitlab.License)
	if l.LicenseType == licenseTypes.PdSj {
		l.Jurisdiction = p.Gitlab.Jurisdiction
	}
	return l
}

// Readme returns a readme with just the copyright set.
func (p *Profile) Readme() *readme.ReadMe {
	r := new(readme.ReadMe)
	r.Copyright = p.Gitlab.Copyright
	return r
}

// Valid compares the properties of Profile for internal consistency
func (p *Profile) Valid() (bool, []string) {
	var errors []string
	_, gitLabErrors := p.Gitlab.Valid()
	if gitLabErrors != nil {
		errors = append(errors, gitLabErrors...)
	}
	if len(p.UserName) == 0 {
		errors = append(errors, "missing user name")
	}
	return len(errors) == 0, errors
}

// GetProfile returns a Profile populated by reading the Vault.
// If doxa is not running in the cloud, and the user is localUser,
// an empty profile is created.
// Else, if the profile does not exist, an error is returned.
func (m *Manager) GetProfile(userName string) (*Profile, error) {
	p := NewProfile(userName)
	var keyPaths []*kvs.KeyPath

	// set up key path
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(usersDir)
	kp.Dirs.Push(userName)
	kp.KeyParts.Push(profileKey)
	keyPaths = append(keyPaths, kp)

	records, err := m.Vault.Read(keyPaths)
	if userName == LocalUser && (err != nil || len(records) == 0) {
		return p, nil
	}
	// // otherwise, return any errors
	if err != nil {
		return nil, err
	}
	if len(records) == 0 {
		return nil, fmt.Errorf(fmt.Sprintf("no profile found for %s", userName))
	}
	err = json.Unmarshal([]byte(records[0].Value), p)
	if err != nil {
		return nil, fmt.Errorf(fmt.Sprintf("could not unmarshal profile for %s: %v", userName, err))
	}
	// no errors, return the profile we read from the vault
	return p, nil
}

// SetProfile serializes the profile to the encrypted vault at vaultPath
func (m *Manager) SetProfile(profile *Profile) error {
	theJson, err := json.Marshal(profile)
	if err != nil {
		return fmt.Errorf(fmt.Sprintf("could not marshal profile to json: %v", err))
	}
	profileEncrypted, err := m.Vault.Encrypt(theJson)
	if err != nil {
		return fmt.Errorf("error encrypting profile: %v", err)
	}
	var records []*kvs.DbR

	// create the record for the profile
	rec := kvs.NewDbR()
	rec.KP.Dirs.Push(usersDir)
	rec.KP.Dirs.Push(profile.UserName)
	rec.KP.KeyParts.Push(profileKey)
	rec.Value = string(profileEncrypted)
	records = append(records, rec)
	// write the record
	return m.Vault.Write(records)
}

// GetGroupNameAndUrl will return the user's gitlab group name and gitlab url.  If not found, returns empty string
func (m *Manager) GetGroupNameAndUrl(userName string) (string, string) {
	profile, err := m.GetProfile(userName)
	if err == nil {
		if profile != nil {
			if profile.Gitlab != nil {
				if profile.Gitlab.GitlabRootGroup != nil {
					return profile.Gitlab.GitlabRootGroup.Name, profile.Gitlab.GitlabRootGroup.WebURL
				}
			}
		}
	}
	return "", ""
}

// GetToken gets the personal access token for the specified user
func (m *Manager) GetToken(userName string) (string, error) {
	var keyPaths []*kvs.KeyPath

	// set up key path
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(usersDir)
	kp.Dirs.Push(userName)
	kp.KeyParts.Push(tokenKey)
	keyPaths = append(keyPaths, kp)

	records, err := m.Vault.Read(keyPaths)
	if err != nil {
		return "", fmt.Errorf(fmt.Sprintf("could not get token for %s: %v", userName, err))
	}
	if len(records) == 0 {
		return "", fmt.Errorf(fmt.Sprintf("no token found for %s", userName))
	}
	return records[0].Value, nil
}
func (m *Manager) IsValidToken(username, token string) bool {
	storedToken, err := m.GetToken(username)
	if err != nil {
		return false
	}
	return strings.Compare(storedToken, token) == 0
}

// SetToken serializes the user's token to the encrypted vault at vaultPath
func (m *Manager) SetToken(userName, token string) error {
	tokenEncrypted, err := m.Vault.Encrypt([]byte(token))
	if err != nil {
		return fmt.Errorf("error encrypting token: %v", err)
	}
	var records []*kvs.DbR

	// create the record for the profile
	rec := kvs.NewDbR()
	rec.KP.Dirs.Push(usersDir)
	rec.KP.Dirs.Push(userName)
	rec.KP.KeyParts.Push(tokenKey)
	rec.Value = string(tokenEncrypted)
	//rec.Value = base64.StdEncoding.EncodeToString(tokenEncrypted)
	records = append(records, rec)
	// write the record
	return m.Vault.Write(records)
}

func (m *Manager) HasToken(username string) bool {
	rec := kvs.NewKeyPath()
	rec.Dirs.Push(usersDir)
	rec.Dirs.Push(username)
	rec.KeyParts.Push(tokenKey)
	return m.Vault.RecExists(rec)
}

type Gitlab struct {
	Contact           string      // option if GitlabPublic == false
	Copyright         string      // must have a value if GitlabPublic == false
	DoxaHandlesGitlab bool        // if true, the user is responsible for backups using git
	GitlabPublic      bool        // if true, repositories will be public rather than private
	GitlabRootGroup   *dvcs.Group // from gitlab itself
	Jurisdiction      string      // must have a value if license == Public Domain (within specified jurisdictions)
	License           string      // must have a value if GitlabPublic == true
	Verified          bool        // true if both personal access token and group ID verified
}

func NewGitlab() *Gitlab {
	g := new(Gitlab)
	g.GitlabRootGroup = new(dvcs.Group)
	g.License = licenseTypes.StringForType(licenseTypes.NotSet)
	return g
}

// Valid compares the properties of Gitlab for internal consistency
func (g *Gitlab) Valid() (bool, []string) {
	var errors []string
	if !g.DoxaHandlesGitlab { // make sure all properties are set to defaults
		g.GitlabPublic = false
		g.Copyright = ""
		g.Verified = false
		g.GitlabRootGroup = nil
		g.Jurisdiction = ""
		g.License = ""
		return true, nil
	}
	if !g.Verified {
		errors = append(errors, "gitlab group ID and token not verified")
	}
	if g.GitlabRootGroup == nil {
		errors = append(errors, "missing information for Gitlab group")
	}
	if !g.GitlabPublic { // set to default properties for public projects
		g.Contact = ""
		g.Copyright = ""
		g.Jurisdiction = ""
		g.License = ""
		return true, errors
	}
	if len(g.Copyright) == 0 {
		if g.License != licenseTypes.StringForType(licenseTypes.PdSj) && g.License != licenseTypes.StringForType(licenseTypes.PdWw) {
			errors = append(errors, "if your Gitlab projects are not Public Domain, there must be a copyright")
		}
	}
	if len(g.License) == 0 {
		errors = append(errors, "if your Gitlab projects are public, there must be a license")
	}
	if g.License == licenseTypes.StringForType(licenseTypes.PdSj) && len(g.Jurisdiction) == 0 {
		errors = append(errors, "if license type is Public Domain in certain jurisdictions, the jurisdictions must be specified")
	}
	return true, errors
}

type TokenValidity byte

const (
	ValidToken TokenValidity = iota
	InvalidToken
	ExpiredToken
	RevokedToken
	Unknown
)

func (m *Manager) ValidateToken(token, base string) (bool, TokenValidity, error) {
	git, err := gitlab.NewClient(token, gitlab.WithBaseURL(base))
	if err != nil {
		return false, Unknown, err
	}
	_, resp, err := git.Users.CurrentUser()
	if err != nil {
		if resp != nil && resp.StatusCode == 401 {
			// Token is invalid, expired, or revoked
			return false, InvalidToken, nil
		}
		return false, InvalidToken, err
	}
	/*tokens, resp, err := git.PersonalAccessTokens.ListPersonalAccessTokens(&gitlab.ListPersonalAccessTokensOptions{UserID: &usr.ID})
	if err != nil {
		return false, Unknown, err
	}
	for _, tok := range tokens {
		if tok.Token == token {
			if tok.Active {
				return true, ValidToken, nil
			}
			if tok.Revoked {
				return false, RevokedToken, nil
			}

			if time.Time(*tok.ExpiresAt).Before(time.Now()) {
				return false, ExpiredToken, nil
			}
		}
	}*/

	return true, ValidToken, nil
}

func (m *Manager) RemoveToken(username string) error {
	rec := kvs.NewKeyPath()
	rec.Dirs.Push(usersDir)
	rec.Dirs.Push(username)
	rec.KeyParts.Push(tokenKey)
	return m.Vault.Delete(rec)
}
