package users

import (
	"github.com/liturgiko/doxa/pkg/vault"
	"os"
	"testing"
)

func TestManager_SetProfile(t *testing.T) {
	VaultPath := os.Getenv("vaultPath")
	if len(VaultPath) == 0 {
		t.Errorf("missing vault")
		return
	}
	Uid := os.Getenv("uid")
	if len(Uid) == 0 {
		t.Errorf("missing uid")
		return
	}
	vaultManager, err := vault.NewManager(VaultPath)
	if err != nil {
		t.Error(err)
		return
	}
	m, err := NewManager(VaultPath, vaultManager)
	if err != nil {
		t.Error(err)
		return
	}
	profile := NewProfile(Uid)
	profile.Gitlab.License = "Creative Commons"
	m.SetProfile(profile)
	profileFromVault, err := m.GetProfile(Uid)
	if err != nil {
		t.Errorf("error reading user %s from vault: %v", Uid, err)
		return
	}
	if profileFromVault.UserName != Uid {
		t.Errorf("expected %s: got %s", Uid, profileFromVault.UserName)
	}
	expect := "Creative Commons"
	if profileFromVault.Gitlab.License != expect {
		t.Errorf("expected %s: got %s", expect, profileFromVault.Gitlab.License)
	}
}

func TestManager_SetToken(t *testing.T) {
	VaultPath := os.Getenv("vaultPath")
	if len(VaultPath) == 0 {
		t.Errorf("missing vault")
		return
	}
	Uid := os.Getenv("uid")
	if len(Uid) == 0 {
		t.Errorf("missing uid")
		return
	}
	Token := os.Getenv("token")
	if len(Uid) == 0 {
		t.Errorf("missing token")
		return
	}
	vaultManager, err := vault.NewManager(VaultPath)
	if err != nil {
		t.Error(err)
		return
	}
	m, err := NewManager(VaultPath, vaultManager)
	if err != nil {
		t.Error(err)
		return
	}
	err = m.SetToken(Uid, Token)
	if err != nil {
		t.Errorf("error getting token for %s: %v", Uid, err)
		return
	}
	var tokenFromVault string
	tokenFromVault, err = m.GetToken(Uid)
	if err != nil {
		t.Errorf("error reading token for user %s from vault: %v", Uid, err)
		return
	}
	if tokenFromVault != Token {
		t.Errorf("token from vault does not match original")
		return
	}
}
