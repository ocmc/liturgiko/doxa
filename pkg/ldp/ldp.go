// Package ldp calculates the liturgical properties for the specified date
// Ported from AGES Initiatives Java version.  The original
// liturgical day properties was written by John Holder of
// St. Catherine Greek Orthodox Church in Denver, Colorado.
// The golang version was ported by Michael Colburn, OCMC.
package ldp

import (
	"errors"
	"fmt"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/enums/calendarTypes"
	"log"
	"sort"
	"strconv"
	"strings"
	"time"
)

var logger log.Logger

// Triodion: 1st day: Sunday of Publican and Pharisee.  9 weeks before Pascha.
// 1st three Sundays precede Great Lent.
// Pascha: Day 1 of Pentecostarion.
// All-Saints: Last Day of Pentecostarion.
// Apostles' Fast: Monday after All Saints, up to and including Jun29, ApostlesPeter&Paul
//
// Thomas Sunday: eight-tone cycle begins w/ Tone 1, ends fri. 6th week Lent.
// Sunday of All-Saints: Eothinon cycle begins with Eothinon 1  (tones of week pl.4)
// Eothinon Cycle runs thru 5th Sunday of Lent (Sunday before Palm Sunday)
//

// LDP is the main struct for liturgical day properties
type LDP struct {
	TheDay                                    time.Time
	CalendarType                              calendarTypes.CalendarType
	Commemoration                             string
	CommemorationRank                         string
	AllSaintsDateLastYear                     time.Time
	AllSaintsDateThisYear                     time.Time
	DayOfWeek                                 string // for debugging output
	DayOfWeekOverride                         string // for debugging output
	DaysFromJan15ToStartOfTriodion            int    // Used to control lectionary and commemorations
	DaysSincePascha                           int
	DaysSinceStartOfTriodion                  int
	DaysSinceSundayAfterLastElevationOfCross  int
	DiffMillisJan15ToTriodionStart            int64
	ElevationOfCrossDateLast                  time.Time
	ElevationOfCrossDateThisYear              time.Time
	EothinonNumber                            int // 0..11. Valid values for 11 week cycle, only valid on Sundays aster Pentecost up to but not including Palm Sunday
	FirstSundayAfterElevationOfCrossThisYear  time.Time
	GreatLentStartDate                        time.Time
	IsDaysOfLuke                              bool
	IsFriday                                  bool
	IsMonday                                  bool
	IsPascha                                  bool
	IsPentecostarion                          bool
	IsSaturday                                bool
	IsSunday                                  bool
	IsThursday                                bool
	IsTriodion                                bool
	IsTuesday                                 bool
	IsWednesday                               bool
	LazarusSaturdayNextDate                   time.Time
	LukanCycleDay                             int // ALWAYS from the last date of Lukan Cycle.
	ModeOfWeek                                int // return 0..8
	ModeOfWeekOverride                        int
	MovableCycleDay                           int // return 1..127 (0 if no day set). Valid only when isPentecostarion or isTriodion.
	NbrDayOfMonth                             string
	NbrDayOfWeek                              string
	NbrDayOfWeekOverride                      string
	NbrModeOfWeek                             string
	NbrMonth                                  string
	NumberOfSundaysFromJan15ToStartOfTriodion int
	originalDay                               int
	OriginalDaysSinceStartOfTriodion          int
	OriginalMovableCycleDay                   int
	originalMonth                             int
	originalYear                              int
	PalmSundayDate                            time.Time
	PalmSundayNextDate                        time.Time
	PaschaDateLast                            time.Time
	PaschaDateLastYear                        time.Time
	PaschaDateNext                            time.Time
	PaschaDateThisYear                        time.Time
	PentecostDate                             time.Time
	StartDateOfLukanCycleLast                 time.Time
	StartDateOfLukanCycleThisYear             time.Time
	SundayAfterElevationOfCrossDateLast       time.Time
	SundayAfterElevationOfCrossDateThisYear   time.Time
	TheDayBefore                              time.Time
	ThomasSundayDate                          time.Time
	TriodionStartDateLast                     time.Time
	TriodionStartDateLastYear                 time.Time
	TriodionStartDateNextYear                 time.Time
	TriodionStartDateThisYear                 time.Time
}

func (ldp *LDP) StrYear() string {
	return fmt.Sprintf("%d", ldp.TheDay.Year())
}
func (ldp *LDP) StrMonth() string {
	return fmt.Sprintf("%d", ldp.TheDay.Month())
}
func (ldp *LDP) StrDay() string {
	return fmt.Sprintf("%d", ldp.TheDay.Day())
}
func (ldp *LDP) WeekDay() string {
	return fmt.Sprintf("%d", ldp.TheDay.Weekday())
}

// ShortForm is used as the format to create a date
const ShortForm = "2019-10-25"

func validateYMD(year, month, day int) error {
	if year < 1583 {
		return errors.New("year cannot be less than 1583")
	}
	if month < 1 || month > 12 {
		return errors.New("month must be between 1 and 12")
	}
	if day < 1 || day > 31 {
		return errors.New("day must be between 1 and 31")
	}
	return nil
}

// NewLDPYMD creates a new LDP initialized to the specified date
func NewLDPYMD(year, month, day int, calendarType calendarTypes.CalendarType) (LDP, error) {
	var ldp LDP
	if err := validateYMD(year, month, day); err != nil {
		doxlog.Errorf("year: %d month: %d day: %d error %v", year, month, day, err)
		return ldp, err
	}
	ldp.TheDay = NewDate(year, month, day)
	ldp.CalendarType = calendarType
	ldp.reinitializeOriginalDateTrackers()
	ldp.SetLiturgicalPropertiesByDate(ldp.TheDay.Year())
	ldp.SetYesterday()
	return ldp, nil
}

// NewLDPMD returns a new LDP initialized to the specified month and day.  If the month and day are before today's, the next year is used, otherwise it is the present year.
func NewLDPMD(month, day int, calendarType calendarTypes.CalendarType) (LDP, error) {
	var ldp LDP
	today := time.Now()
	year := today.Year()
	if err := validateYMD(year, month, day); err != nil {
		return ldp, err
	}

	t := NewDate(year, month, day)
	// if the date is before today, do it for next year by default.
	if t.Before(today) {
		t = NewDate(year+1, month, day)
	}
	ldp.TheDay = t
	ldp.CalendarType = calendarType
	ldp.reinitializeOriginalDateTrackers()
	ldp.SetLiturgicalPropertiesByDate(t.Year())
	ldp.SetYesterday()
	return ldp, nil
}

// NewLDPCalendar returns a new LDP initialized for today's date and specified calendarType
func NewLDPCalendar(calendarType calendarTypes.CalendarType) (LDP, error) {
	now := time.Now()
	ldp, err := NewLDPYMD(now.Year(), int(now.Month()), now.Day(), calendarType)
	return ldp, err
}

// NewLDP returns a new LDP initialized for today's date and calendarType Gregorian
func NewLDP() (LDP, error) {
	now := time.Now()
	return NewLDPYMD(now.Year(), int(now.Month()), now.Day(), calendarTypes.Gregorian)
}

func NewDate(year, month, day int) time.Time {
	return time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC)
}

var GreekMonths = []string{"Ἰανουαρίου",
	"Φεβρουαρίου",
	"Μαρτίου",
	"Ἀπριλίου",
	"Μαΐου",
	"Ἰουνίου",
	"Ἰουλίου",
	"Αὐγούστου",
	"Σεπτεμβρίου",
	"Ὀκτωβρίου",
	"Νοεμβρίου",
	"Δεκεμβρίου",
}
var GreekWeekDays = []string{
	"τοῦ Σαββάτου",
	"τῆς Κυριακῆς",
	"τῆς Δευτέρας",
	"τῆς Τρίτης",
	"τῆς Τετάρτης",
	"τῆς Πέμπτης",
	"τῆς Παρασκευῆς",
}
var GreekMonthDays = []string{
	"αʹ",
	"βʹ",
	"γʹ",
	"δʹ",
	"εʹ",
	"Ϛʹ",
	"ζʹ",
	"ηʹ",
	"θʹ",
	"ιʹ",
	"ιαʹ",
	"ιβʹ",
	"ιγʹ",
	"ιδʹ",
	"ιεʹ",
	"ιϚʹ",
	"ιζʹ",
	"ιηʹ",
	"ιθʹ",
	"κʹ",
	"καʹ",
	"κβʹ",
	"κγʹ",
	"κδʹ",
	"κεʹ",
	"κϚʹ",
	"κζʹ",
	"κηʹ",
	"κθʹ",
	"λʹ",
	"λαʹ",
}
var GreekMap = map[string]string{
	"Ιανουάριος":  "Ἰανουαρίου",
	"Φεβρουάριος": "Φεβρουαρίου",
	"Μάρτιος":     "Μαρτίου",
	"Απρίλιος":    "Ἀπριλίου",
	"Μάϊος":       "Μαΐου",
	"Ιούνιος":     "Ἰουνίου",
	"Ιούλιος":     "Ἰουλίου",
	"Αύγουστος":   "Αὐγούστου",
	"Σεπτέμβριος": "Σεπτεμβρίου",
	"Οκτώβριος":   "Ὀκτωβρίου",
	"Νοέμβριος":   "Νοεμβρίου",
	"Δεκέμβριος":  "Δεκεμβρίου",
	"Σάββατο":     "τοῦ Σαββάτου",
	"Κυριακή":     "τῆς Κυριακῆς",
	"Δευτέρα":     "τῆς Δευτέρας",
	"Τρίτη":       "τῆς Τρίτης",
	"Τετάρτη":     "τῆς Τετάρτης",
	"Πέμπτη":      "τῆς Πέμπτης",
	"Παρασκευή":   "τῆς Παρασκευῆς",
	"1":           "αʹ",
	"2":           "βʹ",
	"3":           "γʹ",
	"4":           "δʹ",
	"5":           "εʹ",
	"6":           "Ϛʹ",
	"7":           "ζʹv",
	"8":           "ηʹ",
	"9":           "θʹ",
	"10":          "ιʹ",
	"11":          "ιαʹ",
	"12":          "ιβʹ",
	"13":          "ιγʹ",
	"14":          "ιδʹ",
	"15":          "ιεʹ",
	"16":          "ιϚʹ",
	"17":          "ιζʹ",
	"18":          "ιηʹ",
	"19":          "ιθʹ",
	"20":          "κʹ",
	"21":          "καʹ",
	"22":          "κβʹ",
	"23":          "κγʹ",
	"24":          "κδʹ",
	"25":          "κεʹ",
	"26":          "κϚʹ",
	"27":          "κζʹ",
	"28":          "κηʹ",
	"29":          "κθʹ",
	"30":          "λʹ",
	"31":          "λαʹ",
}

func (ldp *LDP) reinitializeOriginalDateTrackers() {
	ldp.originalYear = -1
	ldp.originalMonth = -1
	ldp.originalDay = -1
	ldp.OriginalMovableCycleDay = -1
}
func (ldp *LDP) setDateTo(year, month, day int) {
	ldp.TheDay = NewDate(year, month, day)
	//  setLiturgicalPropertiesByDate(theYear)
	ldp.setYesterday()

	// if not already set, save the date values
	if ldp.originalYear == -1 {
		ldp.originalYear = year
	}
	if ldp.originalMonth == -1 {
		ldp.originalMonth = month
	}
	if ldp.originalDay == -1 {
		ldp.originalDay = day
	}
	if ldp.OriginalMovableCycleDay == -1 {
		ldp.OriginalMovableCycleDay = ldp.MovableCycleDay
	}
}
func (ldp *LDP) ResetDate() {
	if ldp.originalMonth == -1 && ldp.originalDay == -1 {
		ldp.originalYear = int(ldp.TheDay.Year())
		ldp.originalMonth = int(ldp.TheDay.Month())
		ldp.originalDay = int(ldp.TheDay.Day())
		ldp.OriginalMovableCycleDay = ldp.MovableCycleDay
	} else {
		ldp.TheDay = NewDate(ldp.originalYear, ldp.originalMonth, ldp.originalDay)
		ldp.SetLiturgicalPropertiesByDate(ldp.originalYear)
		ldp.SetYesterday()
	}
}
func (ldp *LDP) setYesterday() {
	ldp.TheDayBefore = ldp.TheDay.AddDate(0, 0, -1)
}
func (ldp *LDP) TimeDelta(dateFrom time.Time, days int) time.Time {
	return dateFrom.AddDate(0, 0, days)
}

func (ldp *LDP) SetLiturgicalPropertiesByDate(year int) {
	ldp.SetVariablesToDefaults()
	ldp.PaschaDateLastYear = ComputeDayOfPascha(year-1, ldp.CalendarType)
	ldp.PaschaDateThisYear = ComputeDayOfPascha(year, ldp.CalendarType)
	ldp.PaschaDateLast = ldp.lastPaschaDate()
	ldp.PaschaDateNext = ldp.nextPaschaDate()
	// 10 weeks before Pascha (inclusive), Starts with the Sunday of Publican and Pharisee
	ldp.TriodionStartDateThisYear = ldp.PaschaDateThisYear.AddDate(0, 0, -(10 * 7))
	ldp.TriodionStartDateLastYear = ldp.PaschaDateLastYear.AddDate(0, 0, -(10 * 7))
	ldp.TriodionStartDateNextYear = ldp.PaschaDateNext.AddDate(0, 0, -(10 * 7))
	ldp.setDateLastTriodionStart()

	ldp.PalmSundayDate = ldp.PaschaDateThisYear.AddDate(0, 0, -7)
	ldp.PentecostDate = ldp.PaschaDateThisYear.AddDate(0, 0, 49)
	ldp.AllSaintsDateThisYear = ldp.PaschaDateThisYear.AddDate(0, 0, 56)
	ldp.AllSaintsDateLastYear = ldp.PaschaDateLastYear.AddDate(0, 0, 56)
	// Pentecost starts  with Pascha and ends with All Saints, which is the day before the beginning
	// of the Apostle's Fast.
	if ldp.TheDay.Equal(ldp.PaschaDateThisYear) ||
		ldp.TheDay.Equal(ldp.AllSaintsDateThisYear) ||
		(ldp.TheDay.After(ldp.PaschaDateThisYear) && ldp.TheDay.Before(ldp.AllSaintsDateThisYear)) {
		ldp.IsPentecostarion = true
	} else {
		ldp.IsPentecostarion = false
	}

	if ldp.TheDay.Equal(ldp.TriodionStartDateThisYear) ||
		(ldp.TheDay.After(ldp.TriodionStartDateThisYear) && ldp.TheDay.Before(ldp.PaschaDateThisYear)) {
		ldp.IsTriodion = true
	}

	// Clean Monday, 7 weeks + a day before Pascha
	ldp.GreatLentStartDate = ldp.PaschaDateThisYear.AddDate(0, -(7*7)+1, 0)
	ldp.PalmSundayNextDate = ldp.PaschaDateNext.AddDate(0, 0, -7)
	ldp.ThomasSundayDate = ldp.PaschaDateLast.AddDate(0, 0, 7) // NewDate(ldp.PaschaDateLast.Year(), 0,7 )
	ldp.LazarusSaturdayNextDate = ldp.PaschaDateNext.AddDate(0, 0, -8)

	ldp.SetMovableCycleDay()
	ldp.setDaysSinceStartOfLastTriodion()
	ldp.SetDayOfWeek()
	ldp.setEothinonNumber()
	ldp.setModeOfWeek()
	ldp.setNbrDayOfMonth(ldp.TheDay.Day())
	ldp.setNbrMonth(int(ldp.TheDay.Month()))

	ldp.setDateFirstSundayAfterElevationOfCross()
	ldp.setDaysSinceSundayAfterLastElevationOfCross()
	ldp.ElevationOfCrossDateThisYear = NewDate(year, 9, 14)
	ldp.setDateFirstSundayAfterElevationOfCross()
	err := ldp.SetDateStartLukanCycle()
	if err != nil {
		logger.Println(err)
	}
	ldp.setDaysSinceStartLukanCycleLast()
	ldp.setDaysSincePascha()
	ldp.setElevationOfCross(NewDate(ldp.SundayAfterElevationOfCrossDateLast.Year(), 9, 14))
	ldp.SetNumberOfSundaysFromJan15ToStartOfTriodion()
}
func (ldp *LDP) setElevationOfCross(date time.Time) {
	ldp.ElevationOfCrossDateLast = date
}
func (ldp *LDP) GetJan15() time.Time {
	return NewDate(ldp.TriodionStartDateThisYear.Year(), 1, 15)
}

// SetNumberOfSundaysFromJan15ToStartOfTriodion
/*
 * The Typikon has conditions that depend on the number of Sundays from Jan 15 to the start of the Triodion.
 * This is specifically for the lectionary.
 */
func (ldp *LDP) SetNumberOfSundaysFromJan15ToStartOfTriodion() {
	/**
	 * 2007 Triodion Starts Jan 28, 1 Sundays between Jan 15 and Triodion start
	 * 2011 Triodion Starts Feb 13, 4 Sundays between Jan 15 and Triodion start
	 * 2012 Triodion Starts Feb 05, 3 Sundays between Jan 15 and Triodion start
	 * 2013 Triodion Starts Feb 24, 5 Sundays between Jan 15 and Triodion start
	 * 2014 Triodion Starts Feb 09, 3 Sundays between Jan 15 and Triodion start
	 */
	ldp.DiffMillisJan15ToTriodionStart = DiffMillis(ldp.TriodionStartDateThisYear, ldp.GetJan15())
	ldp.DaysFromJan15ToStartOfTriodion = int(ldp.DiffMillisJan15ToTriodionStart / (24 * 60 * 60 * 1000))
	ldp.NumberOfSundaysFromJan15ToStartOfTriodion = ldp.DaysFromJan15ToStartOfTriodion / 7
}

func (ldp *LDP) SetNumberOfSundaysBeforeStartOfTriodion() {
	diffMillis := DiffMillis(ldp.TriodionStartDateThisYear, ldp.TheDay)
	ldp.DaysFromJan15ToStartOfTriodion = int(diffMillis / (24 * 60 * 60 * 1000))
	if ldp.DaysFromJan15ToStartOfTriodion < 0 {
		ldp.DaysFromJan15ToStartOfTriodion = 0
		ldp.NumberOfSundaysFromJan15ToStartOfTriodion = 0
	} else {
		ldp.NumberOfSundaysFromJan15ToStartOfTriodion = ldp.DaysFromJan15ToStartOfTriodion / 7
	}
}

func (ldp *LDP) GetMonthOfSundayAfterElevationOfCross() int {
	return int(ldp.SundayAfterElevationOfCrossDateLast.Month())
}

func (ldp *LDP) getDayOfSundayAfterElevationOfCross() int {
	return int(ldp.SundayAfterElevationOfCrossDateLast.Day())
}

func (ldp *LDP) setDateLastTriodionStart() {
	if ldp.TheDay.Before(ldp.TriodionStartDateThisYear) {
		ldp.TriodionStartDateLast = ldp.TriodionStartDateLastYear
	} else {
		ldp.TriodionStartDateLast = ldp.TriodionStartDateThisYear
	}
}

func (ldp *LDP) setDateFirstSundayAfterElevationOfCross() error {
	firstSundayAfterElevationThisYear, err := ldp.computeSundayAfterElevationOfCross(NewDate(ldp.TheDay.Year(), 9, 14))
	firstSundayAfterElevationLastYear, err := ldp.computeSundayAfterElevationOfCross(NewDate(ldp.TheDay.Year()-1, 9, 14))
	if ldp.TheDay.Before(firstSundayAfterElevationThisYear) {
		ldp.SundayAfterElevationOfCrossDateLast = firstSundayAfterElevationLastYear
	} else {
		ldp.SundayAfterElevationOfCrossDateLast = firstSundayAfterElevationThisYear
	}
	ldp.SundayAfterElevationOfCrossDateThisYear = firstSundayAfterElevationThisYear
	return err
}

/*
{2006,9,14,time.Thursday},
{2007,9,14,time.Friday},
{2008,9,14,time.Sunday},
{2009,9,14,time.Monday},
{2010,9,14, time.Tuesday},
{2011,9,14, time.Wednesday},
{2012,9,14, time.Friday},
{2013,9,14, time.Saturday},
{2014,9,14, time.Sunday},
{2015,9,14, time.Monday},
{2016,9,14, time.Wednesday},
{2017,9,14, time.Thursday},
{2018,9,14, time.Friday},
{2019,9,14, time.Saturday},
{2020,9,14, time.Monday},
{2021,9,14, time.Tuesday},
{2022,9,14, time.Wednesday},
{2023,9,14, time.Thursday},
{2024,9,14, time.Saturday},
{2025,9,14, time.Sunday},
*/
func (ldp *LDP) computeSundayAfterElevationOfCross(date time.Time) (time.Time, error) {
	var dayOffset int
	switch date.Weekday() {
	case time.Sunday:
		dayOffset = 7
	case time.Monday:
		dayOffset = 6
	case time.Tuesday:
		dayOffset = 5
	case time.Wednesday:
		dayOffset = 4
	case time.Thursday:
		dayOffset = 3
	case time.Friday:
		dayOffset = 2
	case time.Saturday:
		dayOffset = 1
	}
	var sunday time.Time
	if ldp.CalendarType == calendarTypes.Gregorian {
		sunday = NewDate(date.Year(), 9, 14+dayOffset)
	} else {
		sunday = NewDate(date.Year(), 9, 27+dayOffset)
	}
	var err error
	if sunday.Weekday() != time.Sunday {
		err = errors.New(fmt.Sprintf("expect weekday for %d/%d/%d to be Sunday, got %s ", date.Year(), 9, 14+dayOffset, sunday.Weekday()))
	}
	return sunday, err
}

func (ldp *LDP) SetDateStartLukanCycle() error {
	var err error
	var firstSundayAfterElevationThisYear time.Time
	var firstSundayAfterElevationLastYear time.Time
	if ldp.CalendarType == calendarTypes.Gregorian {
		firstSundayAfterElevationThisYear, err = ldp.computeSundayAfterElevationOfCross(NewDate(ldp.TheDay.Year(), 9, 14))
		if err != nil {
			return err
		}
		firstSundayAfterElevationLastYear, err = ldp.computeSundayAfterElevationOfCross(NewDate(ldp.TheDay.Year()-1, 9, 14))
		if err != nil {
			return err
		}
	} else {
		firstSundayAfterElevationThisYear, err = ldp.computeSundayAfterElevationOfCross(NewDate(ldp.TheDay.Year(), 9, 27))
		if err != nil {
			return err
		}
		firstSundayAfterElevationLastYear, err = ldp.computeSundayAfterElevationOfCross(NewDate(ldp.TheDay.Year()-1, 9, 27))
		if err != nil {
			return err
		}
	}
	ldp.FirstSundayAfterElevationOfCrossThisYear = firstSundayAfterElevationThisYear
	startLukanCycleThisYear := firstSundayAfterElevationThisYear.AddDate(0, 0, 1)
	ldp.StartDateOfLukanCycleThisYear = startLukanCycleThisYear
	startLukanCycleLastYear := firstSundayAfterElevationLastYear.AddDate(0, 0, 1)
	if ldp.TheDay.Before(startLukanCycleThisYear) {
		ldp.StartDateOfLukanCycleLast = startLukanCycleLastYear
	} else {
		ldp.StartDateOfLukanCycleLast = startLukanCycleThisYear
	}
	return err
}

// ComputeDayOfPascha pass in the year and receive the month and day of Pascha.
func ComputeDayOfPascha(year int, calendarType calendarTypes.CalendarType) time.Time {
	var month, day, r19, r7, r4, n1, n2, n3, cent int
	r19 = year % 19
	r7 = year % 7
	r4 = year % 4
	// This is a formula by Gauss for the number of days after 21-Mar.
	n1 = (19*r19 + 16) % 30
	n2 = (2*r4 + 4*r7 + 6*n1) % 7
	n3 = n1 + n2
	if calendarType == calendarTypes.Gregorian {
		// Then adjust day onto the Gregorian Calendar (only valid from 1583 onwards)
		cent = year / 100
		n3 += cent - cent/4 - 2
	}
	if n3 > 40 {
		month = 5
		day = n3 - 40
	} else if n3 > 10 {
		month = 4
		day = n3 - 10
	} else {
		month = 3
		day = n3 + 21
	}
	// month is zero-indexed (0=Jan) up to this point to support this API.
	return NewDate(year, month, day)
}
func (ldp *LDP) GetModeOfWeek() int {
	if ldp.ModeOfWeekOverride > 0 {
		return ldp.ModeOfWeekOverride
	} else {
		return ldp.ModeOfWeek
	}
}

func (ldp *LDP) SetYesterday() {
	ldp.TheDayBefore = ldp.TheDay
	ldp.TheDayBefore = ldp.TheDayBefore.AddDate(0, 0, -1)
}

// Sometimes it is necessary to temporarily override the mode of the week
// It is important to clear the override after using it
func (ldp *LDP) setModeOfTheWeekOverride(mode string) {
	m, err := strconv.Atoi(mode)
	if err != nil {
		m = 0
	}
	ldp.ModeOfWeekOverride = m
}

func DiffMillis(d1, d2 time.Time) int64 {
	/*
		The Java (ALWB) has:
		int misMatchMillis = d1.get(Calendar.DST_OFFSET) - d2.get(Calendar.DST_OFFSET);
		return d1.getTimeInMillis()-d2.getTimeInMillis()+misMatchMillis;
	*/
	// TODO: figure out test dates to see if this works properly when one date is within daylight savings and the other is not
	d1Millis := d1.UnixNano() / (int64(time.Millisecond) / int64(time.Nanosecond))
	d2Millis := d2.UnixNano() / (int64(time.Millisecond) / int64(time.Nanosecond))
	return d1Millis - d2Millis
}

// DiffDays returns the number of days between the two dates
func (ldp *LDP) DiffDays(t1, t2 time.Time) int {
	var diff time.Duration
	if t1.After(t2) {
		diff = t1.Sub(t2)
	} else {
		diff = t2.Sub(t1)
	}
	return int(diff.Hours() / 24)
}
func (ldp *LDP) DiffWeeks(t1, t2 time.Time) int {
	diff := ldp.DiffDays(t1, t2)
	return int(diff / 7)
}
func (ldp *LDP) setModeOfWeek() {
	// Thomas Sunday: eight-tone cycle begins w/ Tone 1, ends Fri. 6th week Lent (day before Lazarus Sat.)
	diffMillis := DiffMillis(ldp.TheDay, ldp.ThomasSundayDate)
	// GetKeyPath difference in weeks, then mod 8 to get cycle number, and add 1 to use 1-based indexes.
	ldp.ModeOfWeek = (int)((diffMillis/(7*24*60*60*1000))%8 + 1)
	if ldp.IsPentecostarion {
		// override for Pascha through the Saturday before the Sunday of Thomas
		switch ldp.MovableCycleDay {
		case 1:
			{
				ldp.ModeOfWeek = 1
			}
		case 2:
			{
				ldp.ModeOfWeek = 2
			}
		case 3:
			{
				ldp.ModeOfWeek = 3
			}
		case 4:
			{
				ldp.ModeOfWeek = 4
			}
		case 5:
			{
				ldp.ModeOfWeek = 5
			}
		case 6:
			{
				ldp.ModeOfWeek = 6
			}
		case 7:
			{
				ldp.ModeOfWeek = 8
			} // note that it skips 7
		}
	}
	ldp.NbrModeOfWeek = fmt.Sprintf("%d", ldp.ModeOfWeek)
}
func (ldp *LDP) setEothinonNumber() {
	if ldp.IsSunday {
		var diffMillis int64
		if ldp.TheDay.Before(ldp.AllSaintsDateThisYear) {
			diffMillis = DiffMillis(ldp.TheDay, ldp.AllSaintsDateLastYear)
		} else {
			diffMillis = DiffMillis(ldp.TheDay, ldp.AllSaintsDateThisYear)
		}
		ldp.EothinonNumber = (int)(diffMillis/(7*24*60*60*1000))%11 + 1
	} else {
		ldp.EothinonNumber = 0
	}
}

func (ldp *LDP) SetMovableCycleDay() {
	if ldp.IsTriodion || ldp.IsPentecostarion {
		// GetKeyPath difference in milliseconds
		diffMillis := DiffMillis(ldp.TheDay, ldp.TriodionStartDateThisYear)
		// GetKeyPath difference in days, add 1 to be 1-index based instead of zero.
		ldp.MovableCycleDay = (int)(diffMillis/(24*60*60*1000)) + 1
	} else { // movable cycle starts with day 1 of Triodion and continues through the year
		ldp.MovableCycleDay = 0
	}
}

func (ldp *LDP) OverrideMovableCycleDay(d int) {
	if d > 0 {
		// save original mcd
		ldp.OriginalMovableCycleDay = ldp.MovableCycleDay
		ldp.OriginalDaysSinceStartOfTriodion = ldp.DaysSinceStartOfTriodion
		// override to the specified day
		ldp.MovableCycleDay = d
		ldp.DaysSinceStartOfTriodion = d
	}
}
func (ldp *LDP) SetMovableCycleDayTo(mcd, originalMcd int) {
	ldp.MovableCycleDay = mcd
	ldp.OriginalMovableCycleDay = originalMcd
}
func (ldp *LDP) RestoreMovableCycleDay() {
	if ldp.OriginalMovableCycleDay > -1 {
		ldp.MovableCycleDay = ldp.OriginalMovableCycleDay
		ldp.DaysSinceStartOfTriodion = ldp.OriginalDaysSinceStartOfTriodion
	}
}

func (ldp *LDP) setDaysSinceStartOfLastTriodion() {
	diffMillis := DiffMillis(ldp.TheDay, ldp.TriodionStartDateLast)
	// GetKeyPath difference in days, add 1 to be 1-index based instead of zero.
	ldp.DaysSinceStartOfTriodion = int(diffMillis/(24*60*60*1000)) + 1
}

func (ldp *LDP) setDaysSinceSundayAfterLastElevationOfCross() {
	diffMillis := DiffMillis(ldp.TheDay, ldp.SundayAfterElevationOfCrossDateLast)
	// GetKeyPath difference in days, add 1 to be 1-index based instead of zero.
	ldp.DaysSinceSundayAfterLastElevationOfCross = int(diffMillis / (24 * 60 * 60 * 1000))
}

func (ldp *LDP) setDaysSinceStartLukanCycleLast() {
	diffMillis := DiffMillis(ldp.TheDay, ldp.StartDateOfLukanCycleLast)
	// GetKeyPath difference in days, add 1 to be 1-index based instead of zero.
	ldp.LukanCycleDay = (int(diffMillis / (24 * 60 * 60 * 1000))) + 1
	ldp.IsDaysOfLuke = ldp.LukanCycleDay > 0
}

func (ldp *LDP) setDaysSincePascha() {
	if ldp.TheDay.Equal(ldp.PaschaDateThisYear) || ldp.TheDay.After(ldp.PaschaDateThisYear) {
		diffMillis := DiffMillis(ldp.TheDay, ldp.PaschaDateThisYear)
		// GetKeyPath difference in days, add 1 to be 1-index based instead of zero.
		ldp.DaysSincePascha = (int(diffMillis / (24 * 60 * 60 * 1000))) + 1
	} else {
		ldp.DaysSincePascha = -1
	}
}

// Returns the number of weeks elapsed in the Pentecostarion cycle.
func (ldp *LDP) getWeekOfPentecostarionCycle() int {
	if ldp.DaysSincePascha < 8 {
		return 1
	} else {
		return (ldp.DaysSincePascha / 7) + 1
	}
}

// GetWeekOfLukanCycle Returns the number of weeks elapsed in the Lukan cycle.
// The 1st week of the Lukan Cycle starts with the 1st Monday after the
// Elevation of the Cross.
// The 1st Sunday of Luke is the 1st Sunday after the start of the Lukan Cycle.
func (ldp *LDP) GetWeekOfLukanCycle() int {
	if ldp.LukanCycleDay < 8 {
		return 1
	} else {
		return (ldp.LukanCycleDay / 7) + 1
	}
}

// Returns the movable day.  It will be -1 if the
// days since the start of the triodion are not between
// 69 and 128 exclusive.
func (ldp *LDP) pentecostarionDayToMovableDay() int {
	if ldp.DaysSinceStartOfTriodion > 69 && ldp.DaysSinceStartOfTriodion < 128 {
		return ldp.DaysSinceStartOfTriodion - 70
	} else {
		return -1
	}
}

func (ldp *LDP) SetDayOfWeek() {
	dow := ldp.TheDay.Weekday()
	switch dow {
	case time.Sunday:
		ldp.IsSunday = true
		ldp.DayOfWeek = "Sun"
		ldp.NbrDayOfWeek = "1"
	case time.Monday:
		ldp.IsMonday = true
		ldp.DayOfWeek = "Mon"
		ldp.NbrDayOfWeek = "2"
	case time.Tuesday:
		ldp.IsTuesday = true
		ldp.DayOfWeek = "Tue"
		ldp.NbrDayOfWeek = "3"
	case time.Wednesday:
		ldp.IsWednesday = true
		ldp.DayOfWeek = "Wed"
		ldp.NbrDayOfWeek = "4"
	case time.Thursday:
		ldp.IsThursday = true
		ldp.DayOfWeek = "Thu"
		ldp.NbrDayOfWeek = "5"
	case time.Friday:
		ldp.IsFriday = true
		ldp.DayOfWeek = "Fri"
		ldp.NbrDayOfWeek = "6"
	case time.Saturday:
		ldp.IsSaturday = true
		ldp.DayOfWeek = "Sat"
		ldp.NbrDayOfWeek = "7"
	}
}

// Returns the month as an integer, such that 1 = January
func (ldp *LDP) getIntMonth() int { return int(ldp.TheDay.Month()) }

// Set the string form of the month, with a leading zero if > 10
func (ldp *LDP) setNbrMonth(month int) {
	ldp.NbrMonth = fmt.Sprintf("%02d", month)
}

// get the day of the month as an integer
func (ldp *LDP) getIntDayOfMonth() int {
	i, _ := strconv.Atoi(ldp.NbrDayOfMonth)
	return i
}

// Set the string form of the day of the month, with a leading zero if > 10
func (ldp *LDP) setNbrDayOfMonth(dayOfMonth int) {
	ldp.NbrDayOfMonth = fmt.Sprintf("%02d", dayOfMonth)
}

// If the Number day of the week has been overridden,
// returns the ldp.NbrDayOfWeekOverride
// otherwise, returns ldp.NbrDayOfWeek
func (ldp *LDP) getNbrDayOfWeek() string {
	if ldp.NbrDayOfWeekOverride == "" {
		return ldp.NbrDayOfWeek
	} else {
		return ldp.NbrDayOfWeekOverride
	}
}

func (ldp *LDP) getIntWeekOfLent() int {
	result := 0
	daysSinceStart := ldp.DaysSinceStartOfTriodion
	if daysSinceStart >= 23 && daysSinceStart <= 29 {
		result = 1
	} else if daysSinceStart >= 30 && daysSinceStart <= 36 {
		result = 2
	} else if daysSinceStart >= 37 && daysSinceStart <= 43 {
		result = 3
	} else if daysSinceStart >= 44 && daysSinceStart <= 50 {
		result = 4
	} else if daysSinceStart >= 51 && daysSinceStart <= 57 {
		result = 5
	} else if daysSinceStart >= 58 && daysSinceStart <= 64 {
		result = 6
	} else if daysSinceStart >= 65 && daysSinceStart <= 70 {
		result = 7
	}
	return result
}

func (ldp *LDP) getIntDayOfWeek() int {
	s, _ := strconv.Atoi(ldp.getNbrDayOfWeek())
	return s
}

func (ldp *LDP) setNbrDayOfWeek(intDayOfWeek int) {
	ldp.NbrDayOfWeek = fmt.Sprintf("%d", intDayOfWeek)
}

// the format if day is "D1", "D2", etc.
func (ldp *LDP) setNbrDayOfWeekOverride(day string) {
	if day == "" {
		ldp.NbrDayOfWeekOverride = ""
	} else {
		ldp.NbrDayOfWeekOverride = day[:1]
	}
}

// Because the date can be reset for an instance of this class,
// it is necessary to reset certain variables to their default value.
// Otherwise, there value can carry over erroneously to a new date.

func (ldp *LDP) SetVariablesToDefaults() {
	ldp.ModeOfWeek = 0
	ldp.ModeOfWeekOverride = 0
	ldp.EothinonNumber = 0
	ldp.MovableCycleDay = 0
	ldp.DaysSinceStartOfTriodion = 0
	ldp.DaysSinceSundayAfterLastElevationOfCross = 0
	ldp.LukanCycleDay = 0
	ldp.NumberOfSundaysFromJan15ToStartOfTriodion = 0
	ldp.IsPentecostarion = false
	ldp.IsTriodion = false
	ldp.IsPascha = false
	ldp.IsDaysOfLuke = false
	ldp.IsSunday = false
	ldp.IsMonday = false
	ldp.IsTuesday = false
	ldp.IsWednesday = false
	ldp.IsThursday = false
	ldp.IsFriday = false
	ldp.IsSaturday = false
}

// if pascha has not occurred this year, returns pascha for
// the current year.  Otherwise, returns pascha for next year

func (ldp *LDP) nextPaschaDate() time.Time {
	thisYear := ComputeDayOfPascha(ldp.TheDay.Year(), ldp.CalendarType)
	nextYear := ComputeDayOfPascha(ldp.TheDay.Year()+1, ldp.CalendarType)
	if thisYear.After(ldp.TheDay) {
		return thisYear
	} else {
		return nextYear
	}
}
func (ldp *LDP) lastPaschaDate() time.Time {
	lastYear := ComputeDayOfPascha(ldp.TheDay.Year()-1, ldp.CalendarType)
	thisYear := ComputeDayOfPascha(ldp.TheDay.Year(), ldp.CalendarType)
	if thisYear.Before(ldp.TheDay) || thisYear.Equal(ldp.TheDay) {
		return thisYear
	} else {
		return lastYear
	}
}

func (ldp *LDP) daysInMonth(month int) int {
	if month == 2 {
		return 28
	} else if month == 4 || month == 6 || month == 9 || month == 11 {
		return 30
	} else {
		return 31
	}
}

// RelativeTopic computes a new topic relative to liturgical day properties
// If modeOverride > 0, it will be used instead of the mode of the week for a topic starting with "oc" (Octoechos)
// If dayOverride > 0, it will be used instead of the day of the liturgical date for a topic starting with "oc" (Octoechos)
func (ldp *LDP) RelativeTopic(topic string, modeOverride, dayOverride int) string {
	// note: if you add, delete, or change rids here, also change lstring.GetWildCard().
	sb := strings.Builder{}
	eoNbr := ldp.EothinonNumber
	var bookAcronym string
	parts := strings.Split(topic, ".")
	if parts[0] == "le" {
		bookAcronym = parts[0] + "." + parts[1] + "." + parts[2]
	} else {
		bookAcronym = parts[0]
	}
	sb.WriteString(bookAcronym)
	sb.WriteString(".")

	switch bookAcronym {
	case "da":
		sb.WriteString("d")
		sb.WriteString(ldp.NbrDayOfWeek)
	case "eo": // Eothinon - hymns
		sb.WriteString("e")
		sb.WriteString(fmt.Sprintf("%02d", eoNbr))
	case "eu": // Euchologion
	case "he": // Heirmologion
	case "ho": // Horologion
	case "ka": // Katavasias
	case "le.go.eo": // Eothinon - lectionary
		sb.WriteString("w")
		sb.WriteString(fmt.Sprintf("%02d", eoNbr))
	case "le.go.lu": // Lectionary - Gospel - Luke
		sb.WriteString("d")
		sb.WriteString(fmt.Sprintf("%03d", ldp.LukanCycleDay))
	// Movable Day Cycle - Lectionary (Gospel and Epistle), Triodion, Pentecostarion
	case "le.go.mc", "le.ep.mc", "le.pr.tr", "pe", "tr":
		sb.WriteString("d")
		sb.WriteString(fmt.Sprintf("%03d", ldp.DaysSinceStartOfTriodion))
	// Service Month and Day - Lectionary (Gospel and Epistle), Menaion, Octoechos, Synxarion, Typikon
	case "le.go.me", "le.ep.me", "me", "sy", "ty":
		sb.WriteString("m")
		sb.WriteString(ldp.NbrMonth)
		sb.WriteString(".d")
		sb.WriteString(ldp.NbrDayOfMonth)
	case "oc":
		sb.WriteString("m")
		if modeOverride > 0 && modeOverride < 9 {
			sb.WriteString(strconv.Itoa(modeOverride))
		} else {
			sb.WriteString(ldp.NbrModeOfWeek)
		}
		sb.WriteString(".d")
		if dayOverride > 0 && dayOverride < 9 {
			sb.WriteString(strconv.Itoa(dayOverride))
		} else {
			sb.WriteString(ldp.NbrDayOfWeek)
		}
	}
	return sb.String()
}
func (ldp *LDP) getWeekAndDayOfLukanCycle() string {
	week := ldp.GetWeekOfLukanCycle()
	return fmt.Sprintf("%s of the %d%s week of Luke", ldp.DayOfWeek, week, getNumberDegree(week))
}
func (ldp *LDP) IsNativityOfChrist() bool {
	if ldp.CalendarType == calendarTypes.Gregorian {
		return ldp.TheDay.Month() == 12 && ldp.TheDay.Day() == 25
	} else {
		return ldp.TheDay.Month() == 1 && ldp.TheDay.Day() == 7
	}
}
func (ldp *LDP) DateNativityOfChrist() time.Time {
	if ldp.CalendarType == calendarTypes.Gregorian {
		return NewDate(ldp.TheDay.Year(), 12, 25)
	} else {
		return NewDate(ldp.TheDay.Year(), 1, 7)
	}
}

func getNumberDegree(i int) string {
	nbr := strconv.Itoa(i)
	if strings.HasSuffix(nbr, "1") {
		return "st"
	} else if strings.HasSuffix(nbr, "2") {
		return "nd"
	} else if strings.HasSuffix(nbr, "3") {
		return "rd"
	} else {
		return "th"
	}
}

// NewElevationData is used for testing
func (ldp *LDP) NewElevationData() ElevationData {
	var data ElevationData
	data.LiturgicalYear = ldp.TheDay.Year()
	data.LiturgicalMonth = int(ldp.TheDay.Month())
	data.LiturgicalDay = ldp.TheDay.Day()
	data.ElevationYear = ldp.ElevationOfCrossDateLast.Year()
	data.ElevationMonth = int(ldp.ElevationOfCrossDateLast.Month())
	data.ElevationDay = ldp.ElevationOfCrossDateLast.Day()
	data.SundayAfterYear = ldp.SundayAfterElevationOfCrossDateLast.Year()
	data.SundayAfterMonth = int(ldp.SundayAfterElevationOfCrossDateLast.Month())
	data.SundayAfterDay = ldp.SundayAfterElevationOfCrossDateLast.Day()
	data.LukanCycleStartYear = ldp.StartDateOfLukanCycleLast.Year()
	data.LukanCycleStartMonth = int(ldp.StartDateOfLukanCycleLast.Month())
	data.LukanCycleStartDay = ldp.StartDateOfLukanCycleLast.Day()
	data.ElapsedDays = ldp.DaysSinceSundayAfterLastElevationOfCross
	data.LukanCycleDayNbr = ldp.LukanCycleDay
	data.LukanCycleDayName = ldp.DayOfWeek
	data.LukanCycleWeekNbr = ldp.GetWeekOfLukanCycle()
	return data
}

type ElevationData struct {
	LiturgicalYear, LiturgicalMonth, LiturgicalDay                int
	ElevationYear, ElevationMonth, ElevationDay                   int
	SundayAfterYear, SundayAfterMonth, SundayAfterDay             int
	LukanCycleStartYear, LukanCycleStartMonth, LukanCycleStartDay int // from last date
	ElapsedDays                                                   int
	LukanCycleDayNbr                                              int    // from last date
	LukanCycleDayName                                             string // from last date
	LukanCycleWeekNbr                                             int    // from last date
}

func (e *ElevationData) String() string {
	sb := strings.Builder{}
	sb.WriteString("Liturgical date: ")
	sb.WriteString(fmt.Sprintf("%d/%d/%d", e.LiturgicalMonth, e.LiturgicalDay, e.LiturgicalYear))
	sb.WriteString(" Elevation: ")
	sb.WriteString(fmt.Sprintf("%d/%d/%d", e.ElevationMonth, e.ElevationDay, e.ElevationYear))
	sb.WriteString(" Sunday After: ")
	sb.WriteString(fmt.Sprintf("%d/%d/%d", e.SundayAfterMonth, e.SundayAfterDay, e.SundayAfterYear))
	sb.WriteString(" Elapsed days: ")
	sb.WriteString(fmt.Sprintf("%d", e.ElapsedDays))
	sb.WriteString(" Lukan Cycle start: ")
	sb.WriteString(fmt.Sprintf("%d/%d/%d", e.LukanCycleStartYear, e.LukanCycleStartMonth, e.LukanCycleStartDay))
	sb.WriteString(" Lukan Cycle day: ")
	sb.WriteString(fmt.Sprintf("%d", e.LukanCycleDayNbr))
	sb.WriteString(" ")
	sb.WriteString(fmt.Sprintf("%s of the %d%s week of Luke", e.LukanCycleDayName, e.LukanCycleWeekNbr, getNumberDegree(e.LukanCycleWeekNbr)))
	return sb.String()
}

// String returns a formatted statement of the liturgical day properties for the service date
func (ldp *LDP) String() string {
	sb := strings.Builder{}
	sb.WriteString(fmt.Sprintf("\nThe liturgical properties for %s are:", ldp.FormattedLiturgicalDate()))
	title := ldp.TitleOfDay()
	if len(title) > 0 {
		sb.WriteString(fmt.Sprintf("\n\tTitle: %s", title))
	}
	if ldp.ModeOfWeek > 0 {
		sb.WriteString(fmt.Sprintf("\n\tMode: %d", ldp.ModeOfWeek))
	}
	if ldp.EothinonNumber > 0 {
		sb.WriteString(fmt.Sprintf("\n\tEothinon: %d", ldp.EothinonNumber))
	}
	if ldp.MovableCycleDay > 0 {
		sb.WriteString(fmt.Sprintf("\n\tMovable Cycle Day: %d", ldp.MovableCycleDay))
	}
	return sb.String()
}

// Feasts returns a formatted statement of the dates of feasts for the year
func (ldp *LDP) Feasts() string {
	var feasts []string
	sb := strings.Builder{}
	sb.WriteString(fmt.Sprintf("\nThe feasts and liturgical events for the year %d are:\n", ldp.TheDay.Year()))
	if ldp.CalendarType == calendarTypes.Gregorian {
		feasts = append(feasts, fmt.Sprintf("\t%d/01/06 Holy Theophany", ldp.TheDay.Year()))
		feasts = append(feasts, fmt.Sprintf("\n\t%d/02/02 The Meeting of Our Lord", ldp.TheDay.Year()))
		feasts = append(feasts, fmt.Sprintf("\n\t%d/03/25 Annunciation of the Most Holy Theotokos", ldp.TheDay.Year()))
		feasts = append(feasts, fmt.Sprintf("\n\t%d/08/06 Commemoration of the Transfiguration of Our Lord", ldp.TheDay.Year()))
		feasts = append(feasts, fmt.Sprintf("\n\t%d/08/15 Dormition of Our Most Holy Lady Theotokos", ldp.TheDay.Year()))
		feasts = append(feasts, fmt.Sprintf("\n\t%d/09/08 The Nativity of Our Most Holy Lady Theotokos", ldp.TheDay.Year()))
		feasts = append(feasts, fmt.Sprintf("\n\t%d/09/14 The Elevation of the Cross", ldp.TheDay.Year()))
		feasts = append(feasts, fmt.Sprintf("\n\t%d/11/21 The Entry of the Most Holy Theotokos", ldp.TheDay.Year()))
		feasts = append(feasts, fmt.Sprintf("\n\t%d/12/25 The Nativity of Our Lord Jesus Christ", ldp.TheDay.Year()))
	} else {
		feasts = append(feasts, fmt.Sprintf("\t%d/01/07 The Nativity of Our Lord Jesus Christ", ldp.TheDay.Year()))
		feasts = append(feasts, fmt.Sprintf("\n\t%d/01/19 Holy Theophany", ldp.TheDay.Year()))
		feasts = append(feasts, fmt.Sprintf("\n\t%d/02/15 The Meeting of Our Lord", ldp.TheDay.Year()))
		feasts = append(feasts, fmt.Sprintf("\n\t%d/04/07 Annunciation of the Most Holy Theotokos", ldp.TheDay.Year()))
		feasts = append(feasts, fmt.Sprintf("\n\t%d/08/19 Commemoration of the Transfiguration of Our Lord", ldp.TheDay.Year()))
		feasts = append(feasts, fmt.Sprintf("\n\t%d/08/28 Dormition of Our Most Holy Lady Theotokos", ldp.TheDay.Year()))
		feasts = append(feasts, fmt.Sprintf("\n\t%d/09/21 The Nativity of Our Most Holy Lady Theotokos", ldp.TheDay.Year()))
		feasts = append(feasts, fmt.Sprintf("\n\t%d/09/27 The Elevation of the Cross", ldp.TheDay.Year()))
		feasts = append(feasts, fmt.Sprintf("\n\t%d/12/04 The Entry of the Most Holy Theotokos", ldp.TheDay.Year()))
	}
	feasts = append(feasts, fmt.Sprintf("\n\t%d/%02d/%02d Triodion start", ldp.TriodionStartDateThisYear.Year(), int(ldp.TriodionStartDateThisYear.Month()), ldp.TriodionStartDateThisYear.Day()))
	feasts = append(feasts, fmt.Sprintf("\n\t%d/%02d/%02d Palm Sunday", ldp.PalmSundayDate.Year(), int(ldp.PalmSundayDate.Month()), ldp.PalmSundayDate.Day()))
	feasts = append(feasts, fmt.Sprintf("\n\t%d/%02d/%02d Pascha", ldp.PaschaDateThisYear.Year(), int(ldp.PaschaDateThisYear.Month()), ldp.PaschaDateThisYear.Day()))
	feasts = append(feasts, fmt.Sprintf("\n\t%d/%02d/%02d Pentecost", ldp.PentecostDate.Year(), int(ldp.PentecostDate.Month()), ldp.PentecostDate.Day()))
	feasts = append(feasts, fmt.Sprintf("\n\t%d/%02d/%02d All Saints", ldp.AllSaintsDateThisYear.Year(), int(ldp.AllSaintsDateThisYear.Month()), ldp.AllSaintsDateThisYear.Day()))
	feasts = append(feasts, fmt.Sprintf("\n\t%d/%02d/%02d Lukan Cycle Start", ldp.FirstSundayAfterElevationOfCrossThisYear.Year(), int(ldp.FirstSundayAfterElevationOfCrossThisYear.Month()), ldp.FirstSundayAfterElevationOfCrossThisYear.Day()))
	sort.Strings(feasts)
	for _, feast := range feasts {
		sb.WriteString(feast)
	}
	sb.WriteString("\n")
	return sb.String()
}
func (ldp *LDP) LiturgicalPropertiesSummary() LiturgicalProperties {
	p := new(LiturgicalProperties)
	// set the non-date properties
	p.DayOfWeek = ldp.DayOfWeek
	p.Eothinon = ldp.EothinonNumber
	p.LukanCycleDay = ldp.LukanCycleDay
	p.LukanCycleYear = ldp.SundayAfterElevationOfCrossDateLast.Year()
	p.ModeOfWeek = ldp.ModeOfWeek
	p.MovableCycleDay = ldp.MovableCycleDay
	p.SundayAfterElevationOfCrossMonthDay = fmt.Sprintf("%s %02d", ldp.SundayAfterElevationOfCrossDateLast.UTC().Format("Jan"), ldp.SundayAfterElevationOfCrossDateLast.Day())
	p.SundaysBeforeTriodion = ldp.NumberOfSundaysFromJan15ToStartOfTriodion
	p.MonthDay = fmt.Sprintf("%s", ldp.TheDay.UTC().Format("Jan 02"))
	p.Year = ldp.TheDay.Year()
	// set the dates for events THIS year
	if ldp.CalendarType == calendarTypes.Gregorian {
		p.DateTheophany, p.DayTheophany = DateAndDay(p.Year, 1, 6)
		p.DateMeetingLord, p.DayMeetingLord = DateAndDay(p.Year, 2, 2)
		p.DateAnnunciation, p.DayAnnunciation = DateAndDay(p.Year, 3, 25)
		p.DateTransfiguration, p.DayTransfiguration = DateAndDay(p.Year, 8, 6)
		p.DateDormitionTheotokos, p.DayDormitionTheotokos = DateAndDay(p.Year, 8, 15)
		p.DateNativityTheotokos, p.DayNativityTheotokos = DateAndDay(p.Year, 9, 8)
		p.DateElevationCross, p.DayElevationCross = DateAndDay(p.Year, 9, 14)
		p.DateEntranceTheotokos, p.DayEntranceTheotokos = DateAndDay(p.Year, 11, 21)
		p.DateNativityChrist, p.DayNativityChrist = DateAndDay(p.Year, 12, 25)
	} else {
		p.DateNativityChrist, p.DayNativityChrist = DateAndDay(p.Year, 1, 7)
		p.DateTheophany, p.DayTheophany = DateAndDay(p.Year, 1, 19)
		p.DateMeetingLord, p.DayMeetingLord = DateAndDay(p.Year, 2, 15)
		p.DateAnnunciation, p.DayAnnunciation = DateAndDay(p.Year, 4, 7)
		p.DateTransfiguration, p.DayTransfiguration = DateAndDay(p.Year, 8, 19)
		p.DateDormitionTheotokos, p.DayDormitionTheotokos = DateAndDay(p.Year, 8, 28)
		p.DateNativityTheotokos, p.DayNativityTheotokos = DateAndDay(p.Year, 9, 21)
		p.DateElevationCross, p.DayElevationCross = DateAndDay(p.Year, 9, 27)
		p.DateEntranceTheotokos, p.DayEntranceTheotokos = DateAndDay(p.Year, 12, 04)
	}
	p.DateLukanCycleStart = fmt.Sprintf("%d-%02d-%02d", ldp.StartDateOfLukanCycleThisYear.Year(), int(ldp.StartDateOfLukanCycleThisYear.Month()), ldp.StartDateOfLukanCycleThisYear.Day())
	p.DayLukanCycleStart = ldp.StartDateOfLukanCycleThisYear.Format("Monday")
	p.DateTriodionStart = fmt.Sprintf("%d-%02d-%02d", ldp.TriodionStartDateThisYear.Year(), int(ldp.TriodionStartDateThisYear.Month()), ldp.TriodionStartDateThisYear.Day())
	p.DayTriodionStart = ldp.TriodionStartDateThisYear.Format("Monday")
	p.DatePalmSunday = fmt.Sprintf("%d-%02d-%02d", ldp.PalmSundayDate.Year(), int(ldp.PalmSundayDate.Month()), ldp.PalmSundayDate.Day())
	p.DayPalmSunday = "Sunday"
	p.DatePascha = fmt.Sprintf("%d-%02d-%02d", ldp.PaschaDateThisYear.Year(), int(ldp.PaschaDateThisYear.Month()), ldp.PaschaDateThisYear.Day())
	p.DayPascha = "Sunday"
	p.DatePentecost = fmt.Sprintf("%d-%02d-%02d", ldp.PentecostDate.Year(), int(ldp.PentecostDate.Month()), ldp.PentecostDate.Day())
	p.DayPentecost = ldp.PentecostDate.Format("Monday")
	p.DateAllSaints = fmt.Sprintf("%d-%02d-%02d", ldp.AllSaintsDateThisYear.Year(), int(ldp.AllSaintsDateThisYear.Month()), ldp.AllSaintsDateThisYear.Day())
	p.DayAllSaints = ldp.AllSaintsDateThisYear.Format("Monday")
	p.DateSundayAfterElevationOfCross = fmt.Sprintf("%d/%02d/%02d", ldp.FirstSundayAfterElevationOfCrossThisYear.Year(), int(ldp.FirstSundayAfterElevationOfCrossThisYear.Month()), ldp.FirstSundayAfterElevationOfCrossThisYear.Day())
	p.DaySundayAfterElevationOfCross = "Sunday"
	p.Date = ldp.FormattedLiturgicalDate()
	return *p
}

// DateAndDay returns formatted date ("2006-01-02") and day of week ("Monday")
func DateAndDay(year int, month time.Month, day int) (string, string) {
	date := time.Date(year, month, day, 0, 0, 0, 0, time.UTC)
	return date.Format("2006-01-02"), date.Format("Monday")
}

// LiturgicalProperties is designed as a response for a web api.
// The dates are all for events THIS year, e.g. Lukan Cycle start this year.
type LiturgicalProperties struct {
	Date                                string `json:"Date"`
	DateAllSaints                       string `json:"DateAllSaints"`
	DayAllSaints                        string `json:"DayAllSaints"`
	DateAnnunciation                    string `json:"DateAnnunciation"`
	DayAnnunciation                     string `json:"DayAnnunciation"`
	DateDormitionTheotokos              string `json:"DateDormitionTheotokos"`
	DayDormitionTheotokos               string `json:"DayDormitionTheotokos"`
	DateElevationCross                  string `json:"DateElevationCross"`
	DayElevationCross                   string `json:"DayElevationCross"`
	DateEntranceTheotokos               string `json:"DateEntranceTheotokos"`
	DayEntranceTheotokos                string `json:"DayEntranceTheotokos"`
	DateLukanCycleStart                 string `json:"DateLukanCycleStart"`
	DayLukanCycleStart                  string `json:"DayLukanCycleStart"`
	DateMeetingLord                     string `json:"DateMeetingLord"`
	DayMeetingLord                      string `json:"DayMeetingLord"`
	DateNativityChrist                  string `json:"DateNativityChrist"`
	DayNativityChrist                   string `json:"DayNativityChrist"`
	DateNativityTheotokos               string `json:"DateNativityTheotokos"`
	DayNativityTheotokos                string `json:"DayNativityTheotokos"`
	DatePalmSunday                      string `json:"DatePalmSunday"`
	DayPalmSunday                       string `json:"DayPalmSunday"`
	DatePascha                          string `json:"DatePascha"`
	DayPascha                           string `json:"DayPascha"`
	DatePentecost                       string `json:"DatePentecost"`
	DayPentecost                        string `json:"DayPentecost"`
	DateSundayAfterElevationOfCross     string `json:"DateSundayAfterElevationOfCross"`
	DaySundayAfterElevationOfCross      string `json:"DaySundayAfterElevationOfCross"`
	DateTheophany                       string `json:"DateTheophany"`
	DayTheophany                        string `json:"DayTheophany"`
	DateTransfiguration                 string `json:"DateTransfiguration"`
	DayTransfiguration                  string `json:"DayTransfiguration"`
	DateTriodionStart                   string `json:"DateTriodionStart"`
	DayTriodionStart                    string `json:"DayTriodionStart"`
	DayOfWeek                           string `json:"DayOfWeek"`
	Eothinon                            int    `json:"Eothinon"`
	LukanCycleDay                       int    `json:"LukanCycleDay"`
	LukanCycleYear                      int    `json:"LukanCycleYear"`
	MonthDay                            string `json:"MonthDay"`
	ModeOfWeek                          int    `json:"ModeOfWeek"`
	MovableCycleDay                     int    `json:"MovableCycleDay"`
	SundayAfterElevationOfCrossMonthDay string `json:"SundayAfterElevationOfCrossMonthDay"`
	SundaysBeforeTriodion               int    `json:"SundaysBeforeTriodion"`
	Year                                int    `json:"Year"`
}

func (ldp *LDP) TitleOfDay() string {
	sb := strings.Builder{}
	if ldp.IsDaysOfLuke {
		sb.WriteString(fmt.Sprintf("\t%s of the %d%s week of Luke", ldp.FormattedDate(ldp.TheDay, LayoutDayFull), ldp.GetWeekOfLukanCycle(), getNumberDegree(ldp.GetWeekOfLukanCycle())))
		sb.WriteString(fmt.Sprintf(", Lukan cycle day %d", ldp.LukanCycleDay))
		return sb.String()
	}
	if ldp.IsTriodion || ldp.IsPentecostarion {
		switch ldp.MovableCycleDay {
		case 1:
			sb.WriteString("The 33rd Sunday, of the Publican and Pharisee")
		case 8:
			sb.WriteString("The 34th Sunday (Sunday of the Prodigal)")
		case 9:
			sb.WriteString("MONDAY OF MEAT-FARE WEEK")
		case 10:
			sb.WriteString("TUESDAY OF MEAT-FARE WEEK")
		case 11:
			sb.WriteString("WEDNESDAY OF MEAT-FARE WEEK")
		case 12:
			sb.WriteString("THURSDAY OF MEAT-FARE WEEK")
		case 13:
			sb.WriteString("FRIDAY OF MEAT-FARE WEEK")
		case 14:
			sb.WriteString("MEAT-FARE SATURDAY")
		case 15:
			sb.WriteString("MEAT-FARE SUNDAY")
		case 16:
			sb.WriteString("MONDAY OF CHEESE-FARE WEEK")
		case 17:
			sb.WriteString("TUESDAY OF CHEESE-FARE WEEK")
		case 19:
			sb.WriteString("THURSDAY OF CHEESE-FARE WEEK")
		case 21:
			sb.WriteString("CHEESE-FARE SATURDAY")
		case 22:
			sb.WriteString("CHEESE-FARE SUNDAY")
		case 28:
			sb.WriteString("SATURDAY OF THE FIRST WEEK IN LENT")
		case 29:
			sb.WriteString("THE FIRST SUNDAY OF LENT")
		case 35:
			sb.WriteString("SATURDAY OF THE SECOND WEEK IN LENT")
		case 36:
			sb.WriteString("THE SECOND SUNDAY IN LENT")
		case 42:
			sb.WriteString("SATURDAY OF THE THIRD WEEK IN LENT")
		case 43:
			sb.WriteString("THE THIRD SUNDAY IN LENT")
		case 49:
			sb.WriteString("SATURDAY OF THE FOURTH WEEK IN LENT")
		case 50:
			sb.WriteString("THE FOURTH SUNDAY IN LENT")
		case 56:
			sb.WriteString("SATURDAY OF THE FIFTH WEEK IN LENT, SATURDAY OF THE AKATHISTOS HYMN")
		case 57:
			sb.WriteString("THE FIFTH SUNDAY IN LENT")
		case 63:
			sb.WriteString("SATURDAY OF SAINT LAZAROS THE JUST")
		case 64:
			sb.WriteString("PALM SUNDAY")
		case 68:
			sb.WriteString("GREAT AND HOLY THURSDAY")
		case 71:
			sb.WriteString("SUNDAY OF PASCHA")
		case 72:
			sb.WriteString("MONDAY OF RENEWAL WEEK")
		case 73:
			sb.WriteString("TUESDAY OF RENEWAL WEEK")
		case 74:
			sb.WriteString("WEDNESDAY OF RENEWAL WEEK")
		case 75:
			sb.WriteString("THURSDAY OF RENEWAL WEEK")
		case 76:
			sb.WriteString("FRIDAY OF RENEWAL WEEK")
		case 77:
			sb.WriteString("SATURDAY OF RENEWAL WEEK")
		case 78:
			sb.WriteString("SUNDAY AFTER EASTER, SUNDAY OF SAINT THOMAS")
		case 79:
			sb.WriteString("MONDAY OF THE SECOND WEEK")
		case 80:
			sb.WriteString("TUESDAY OF THE SECOND WEEK")
		case 81:
			sb.WriteString("WEDNESDAY OF THE SECOND WEEK")
		case 82:
			sb.WriteString("THURSDAY OF THE SECOND WEEK")
		case 83:
			sb.WriteString("FRIDAY OF THE SECOND WEEK")
		case 84:
			sb.WriteString("SATURDAY OF THE SECOND WEEK")
		case 85:
			sb.WriteString("THIRD SUNDAY, SUNDAY OF THE MYRRH-BEARERS")
		case 86:
			sb.WriteString("MONDAY OF THE THIRD WEEK")
		case 87:
			sb.WriteString("TUESDAY OF THE THIRD WEEK")
		case 88:
			sb.WriteString("WEDNESDAY OF THE THIRD WEEK")
		case 89:
			sb.WriteString("THURSDAY OF THE THIRD WEEK")
		case 90:
			sb.WriteString("FRIDAY OF THE THIRD WEEK")
		case 91:
			sb.WriteString("SATURDAY OF THE THIRD WEEK")
		case 92:
			sb.WriteString("FOURTH SUNDAY, SUNDAY OF THE PARALYTIC")
		case 93:
			sb.WriteString("MONDAY OF THE FOURTH WEEK")
		case 94:
			sb.WriteString("TUESDAY OF THE FOURTH WEEK")
		case 95:
			sb.WriteString("WEDNESDAY OF THE FOURTH WEEK, MID-PENTECOST")
		case 96:
			sb.WriteString("THURSDAY OF THE FOURTH WEEK")
		case 97:
			sb.WriteString("FRIDAY OF THE FOURTH WEEK")
		case 98:
			sb.WriteString("SATURDAY OF THE FOURTH WEEK")
		case 99:
			sb.WriteString("FIFTH SUNDAY, SUNDAY OF THE SAMARITAN WOMAN")
		case 100:
			sb.WriteString("MONDAY OF THE FIFTH WEEK")
		case 101:
			sb.WriteString("TUESDAY OF THE FIFTH WEEK")
		case 102:
			sb.WriteString("WEDNESDAY OF THE FIFTH WEEK")
		case 103:
			sb.WriteString("THURSDAY OF THE FIFTH WEEK")
		case 104:
			sb.WriteString("FRIDAY OF THE FIFTH WEEK")
		case 105:
			sb.WriteString("SATURDAY OF THE FIFTH WEEK")
		case 106:
			sb.WriteString("SIXTH SUNDAY, SUNDAY OF THE BLIND MAN")
		case 107:
			sb.WriteString("MONDAY OF THE SIXTH WEEK")
		case 108:
			sb.WriteString("TUESDAY OF THE SIXTH WEEK")
		case 109:
			sb.WriteString("WEDNESDAY OF THE SIXTH WEEK")
		case 110:
			sb.WriteString("THURSDAY OF THE ASCENSION")
		case 111:
			sb.WriteString("FRIDAY OF THE SIXTH WEEK")
		case 112:
			sb.WriteString("SATURDAY OF THE SIXTH WEEK")
		case 113:
			sb.WriteString("SEVENTH SUNDAY, SUNDAY OF THE HOLY FATHERS")
		case 114:
			sb.WriteString("MONDAY OF THE SEVENTH WEEK")
		case 115:
			sb.WriteString("TUESDAY OF THE SEVENTH WEEK")
		case 116:
			sb.WriteString("WEDNESDAY OF THE SEVENTH WEEK")
		case 117:
			sb.WriteString("THURSDAY OF THE SEVENTH WEEK")
		case 118:
			sb.WriteString("FRIDAY OF THE SEVENTH WEEK")
		case 119:
			sb.WriteString("SATURDAY OF THE SEVENTH WEEK")
		case 120:
			sb.WriteString("SUNDAY OF PENTECOST")
		case 121:
			sb.WriteString("MONDAY OF THE HOLY SPIRIT")
		case 122:
			sb.WriteString("TUESDAY OF THE FIRST WEEK")
		case 123:
			sb.WriteString("WEDNESDAY OF THE FIRST WEEK")
		case 124:
			sb.WriteString("THURSDAY OF THE FIRST WEEK")
		case 125:
			sb.WriteString("FRIDAY OF THE FIRST WEEK")
		case 126:
			sb.WriteString("SATURDAY OF THE FIRST WEEK")
		case 127:
			sb.WriteString("FIRST SUNDAY, SUNDAY OF ALL SAINTS")
		case 128:
			sb.WriteString("MONDAY OF THE SECOND WEEK")
		case 129:
			sb.WriteString("TUESDAY OF THE SECOND WEEK")
		case 130:
			sb.WriteString("WEDNESDAY OF THE SECOND WEEK")
		case 131:
			sb.WriteString("THURSDAY OF THE SECOND WEEK")
		case 132:
			sb.WriteString("FRIDAY OF THE SECOND WEEK")
		case 133:
			sb.WriteString("ΣΑΒΒΑΤΟΝ Βʹ ΕΒΔΟΜΑΔΟΣ")
		case 134:
			sb.WriteString("SECOND SUNDAY")
		case 135:
			sb.WriteString("MONDAY OF THE THIRD WEEK")
		case 136:
			sb.WriteString("TUESDAY OF THE THIRD WEEK")
		case 137:
			sb.WriteString("WEDNESDAY OF THE THIRD WEEK")
		case 138:
			sb.WriteString("THURSDAY OF THE THIRD WEEK")
		case 139:
			sb.WriteString("FRIDAY OF THE THIRD WEEK")
		case 140:
			sb.WriteString("SATURDAY OF THE THIRD WEEK")
		case 141:
			sb.WriteString("THIRD SUNDAY")
		case 142:
			sb.WriteString("MONDAY OF THE FOURTH WEEK")
		case 143:
			sb.WriteString("TUESDAY OF THE FOURTH WEEK")
		case 144:
			sb.WriteString("WEDNESDAY OF THE FOURTH WEEK")
		case 145:
			sb.WriteString("THURSDAY OF THE FOURTH WEEK")
		case 146:
			sb.WriteString("FRIDAY OF THE FOURTH WEEK")
		case 147:
			sb.WriteString("SATURDAY OF THE FOURTH WEEK")
		case 148:
			sb.WriteString("FOURTH SUNDAY")
		case 149:
			sb.WriteString("MONDAY OF THE FIFTH WEEK")
		case 150:
			sb.WriteString("TUESDAY OF THE FIFTH WEEK")
		case 151:
			sb.WriteString("WEDNESDAY OF THE FIFTH WEEK")
		case 152:
			sb.WriteString("THURSDAY OF THE FIFTH WEEK")
		case 153:
			sb.WriteString("FRIDAY OF THE FIFTH WEEK")
		case 154:
			sb.WriteString("SATURDAY OF THE FIFTH WEEK")
		case 155:
			sb.WriteString("FIFTH SUNDAY")
		case 156:
			sb.WriteString("MONDAY OF THE SIXTH WEEK")
		case 157:
			sb.WriteString("TUESDAY OF THE SIXTH WEEK")
		case 158:
			sb.WriteString("WEDNESDAY OF THE SIXTH WEEK")
		case 159:
			sb.WriteString("THURSDAY OF THE SIXTH WEEK")
		case 160:
			sb.WriteString("FRIDAY OF THE SIXTH WEEK")
		case 161:
			sb.WriteString("SATURDAY OF THE SIXTH WEEK")
		case 162:
			sb.WriteString("SIXTH SUNDAY")
		case 163:
			sb.WriteString("MONDAY OF THE SEVENTH WEEK")
		case 164:
			sb.WriteString("TUESDAY OF THE SEVENTH WEEK")
		case 165:
			sb.WriteString("WEDNESDAY OF THE SEVENTH WEEK")
		case 166:
			sb.WriteString("THURSDAY OF THE SEVENTH WEEK")
		case 167:
			sb.WriteString("FRIDAY OF THE SEVENTH WEEK")
		case 168:
			sb.WriteString("SATURDAY OF THE SEVENTH WEEK")
		case 169:
			sb.WriteString("SEVENTH SUNDAY")
		case 170:
			sb.WriteString("MONDAY OF THE EIGHTH WEEK")
		case 171:
			sb.WriteString("TUESDAY OF THE EIGHTH WEEK")
		case 172:
			sb.WriteString("WEDNESDAY OF THE EIGHTH WEEK")
		case 173:
			sb.WriteString("THURSDAY OF THE EIGHTH WEEK")
		case 174:
			sb.WriteString("FRIDAY OF THE EIGHTH WEEK")
		case 175:
			sb.WriteString("SATURDAY OF THE EIGHTH WEEK")
		case 176:
			sb.WriteString("EIGHTH SUNDAY")
		case 177:
			sb.WriteString("MONDAY OF THE NINTH WEEK")
		case 178:
			sb.WriteString("TUESDAY OF THE NINTH WEEK")
		case 179:
			sb.WriteString("WEDNESDAY OF THE NINTH WEEK")
		case 180:
			sb.WriteString("THURSDAY OF THE NINTH WEEK")
		case 181:
			sb.WriteString("FRIDAY OF THE NINTH WEEK")
		case 182:
			sb.WriteString("SATURDAY OF THE NINTH WEEK")
		case 183:
			sb.WriteString("NINTH SUNDAY")
		case 184:
			sb.WriteString("MONDAY OF THE TENTH WEEK")
		case 185:
			sb.WriteString("TUESDAY OF THE TENTH WEEK")
		case 186:
			sb.WriteString("WEDNESDAY OF THE TENTH WEEK")
		case 187:
			sb.WriteString("THURSDAY OF THE TENTH WEEK")
		case 188:
			sb.WriteString("FRIDAY OF THE TENTH WEEK")
		case 189:
			sb.WriteString("SATURDAY OF THE TENTH WEEK")
		case 190:
			sb.WriteString("TENTH SUNDAY")
		case 191:
			sb.WriteString("MONDAY OF THE ELEVENTH WEEK")
		case 192:
			sb.WriteString("TUESDAY OF THE ELEVENTH WEEK")
		case 193:
			sb.WriteString("WEDNESDAY OF THE ELEVENTH WEEK")
		case 194:
			sb.WriteString("THURSDAY OF THE ELEVENTH WEEK")
		case 195:
			sb.WriteString("FRIDAY OF THE ELEVENTH WEEK")
		case 196:
			sb.WriteString("SATURDAY OF THE ELEVENTH WEEK")
		case 197:
			sb.WriteString("ELEVENTH SUNDAY")
		case 198:
			sb.WriteString("MONDAY OF THE TWELFTH WEEK")
		case 199:
			sb.WriteString("TUESDAY OF THE TWELFTH WEEK")
		case 200:
			sb.WriteString("WEDNESDAY OF THE TWELFTH WEEK")
		case 201:
			sb.WriteString("THURSDAY OF THE TWELFTH WEEK")
		case 202:
			sb.WriteString("FRIDAY OF THE TWELFTH WEEK")
		case 203:
			sb.WriteString("SATURDAY OF THE TWELFTH WEEK")
		case 204:
			sb.WriteString("TWELFTH SUNDAY")
		case 205:
			sb.WriteString("MONDAY OF THE THIRTEENTH WEEK")
		case 206:
			sb.WriteString("TUESDAY OF THE THIRTEENTH WEEK")
		case 207:
			sb.WriteString("WEDNESDAY OF THE THIRTEENTH WEEK")
		case 208:
			sb.WriteString("THURSDAY OF THE THIRTEENTH WEEK")
		case 209:
			sb.WriteString("FRIDAY OF THE THIRTEENTH WEEK")
		case 210:
			sb.WriteString("SATURDAY OF THE THIRTEENTH WEEK")
		case 211:
			sb.WriteString("THIRTEENTH SUNDAY")
		case 212:
			sb.WriteString("MONDAY OF THE FOURTEENTH WEEK")
		case 213:
			sb.WriteString("TUESDAY OF THE FOURTEENTH WEEK")
		case 214:
			sb.WriteString("WEDNESDAY OF THE FOURTEENTH WEEK")
		case 215:
			sb.WriteString("THURSDAY OF THE FOURTEENTH WEEK")
		case 216:
			sb.WriteString("FRIDAY OF THE FOURTEENTH WEEK")
		case 217:
			sb.WriteString("SATURDAY OF THE FOURTEENTH WEEK")
		case 218:
			sb.WriteString("FOURTEENTH SUNDAY")
		case 219:
			sb.WriteString("MONDAY OF THE FIFTEENTH WEEK")
		case 220:
			sb.WriteString("TUESDAY OF THE FIFTEENTH WEEK")
		case 221:
			sb.WriteString("WEDNESDAY OF THE FIFTEENTH WEEK")
		case 222:
			sb.WriteString("THURSDAY OF THE FIFTEENTH WEEK")
		case 223:
			sb.WriteString("FRIDAY OF THE FIFTEENTH WEEK")
		case 224:
			sb.WriteString("SATURDAY OF THE FIFTEENTH WEEK")
		case 225:
			sb.WriteString("FIFTEENTH SUNDAY")
		case 226:
			sb.WriteString("MONDAY OF THE SIXTEENTH WEEK")
		case 227:
			sb.WriteString("TUESDAY OF THE SIXTEENTH WEEK")
		case 228:
			sb.WriteString("WEDNESDAY OF THE SIXTEENTH WEEK")
		case 229:
			sb.WriteString("THURSDAY OF THE SIXTEENTH WEEK")
		case 230:
			sb.WriteString("FRIDAY OF THE SIXTEENTH WEEK")
		case 231:
			sb.WriteString("SATURDAY OF THE SIXTEENTH WEEK")
		case 232:
			sb.WriteString("SIXTEENTH SUNDAY")
		case 233:
			sb.WriteString("MONDAY OF THE SEVENTEENTH WEEK")
		case 234:
			sb.WriteString("TUESDAY OF THE SEVENTEENTH WEEK")
		case 235:
			sb.WriteString("WEDNESDAY OF THE SEVENTEENTH WEEK")
		case 236:
			sb.WriteString("THURSDAY OF THE SEVENTEENTH WEEK")
		case 237:
			sb.WriteString("FRIDAY OF THE SEVENTEENTH WEEK")
		case 238:
			sb.WriteString("SATURDAY OF THE SEVENEETH WEEK")
		case 239:
			sb.WriteString("SEVENTEENTH SUNDAY")
		case 240:
			sb.WriteString("MONDAY OF THE EIGHTEENTH WEEK")
		case 241:
			sb.WriteString("TUESDAY OF THE EIGHTEENTH WEEK")
		case 242:
			sb.WriteString("WEDNESDAY OF THE EIGHTEENTH WEEK")
		case 243:
			sb.WriteString("THURSDAY OF THE EIGHTEENTH WEEK")
		case 244:
			sb.WriteString("FRIDAY OF THE EIGHTEENTH WEEK")
		case 245:
			sb.WriteString("SATURDAY OF THE EIGHTEENTH WEEK")
		case 246:
			sb.WriteString("EIGHTEENTH SUNDAY")
		case 247:
			sb.WriteString("MONDAY OF THE NINETEENTH WEEK")
		case 248:
			sb.WriteString("TUESDAY OF THE NINETEENTH WEEK")
		case 249:
			sb.WriteString("WEDNESDAY OF THE NINETEENTH WEEK")
		case 250:
			sb.WriteString("THURSDAY OF THE NINETEENTH WEEK")
		case 251:
			sb.WriteString("FRIDAY OF THE NINETEENTH WEEK")
		case 252:
			sb.WriteString("SATURDAY OF THE NINETEENTH WEEK")
		case 253:
			sb.WriteString("NINETEENTH SUNDAY")
		case 254:
			sb.WriteString("MONDAY OF THE TWENTIETH WEEK")
		case 255:
			sb.WriteString("TUESDAY OF THE TWENTIETH WEEK")
		case 256:
			sb.WriteString("WEDNESDAY OF THE TWENTIETH WEEK")
		case 257:
			sb.WriteString("THURSDAY OF THE TWENTIETH WEEK")
		case 258:
			sb.WriteString("FRIDAY OF THE TWENTIETH WEEK")
		case 259:
			sb.WriteString("SATURDAY OF THE TWENTIETH WEEK")
		case 260:
			sb.WriteString("TWENTIETH SUNDAY")
		case 261:
			sb.WriteString("MONDAY OF THE TWENTY-FIRST WEEK")
		case 262:
			sb.WriteString("TUESDAY OF THE TWENTY-FIRST WEEK")
		case 263:
			sb.WriteString("WEDNESDAY OF THE TWENTY-FIRST WEEK")
		case 264:
			sb.WriteString("THURSDAY OF THE TWENTY-FIRST WEEK")
		case 265:
			sb.WriteString("FRIDAY OF THE TWENTY-FIRST WEEK")
		case 266:
			sb.WriteString("SATURDAY OF THE TWENTY-FIRST WEEK")
		case 267:
			sb.WriteString("TWENTY-FIRST SUNDAY")
		case 268:
			sb.WriteString("MONDAY OF THE TWENTY-SECOND WEEK")
		case 269:
			sb.WriteString("TUESDAY OF THE TWENTY-SECOND WEEK")
		case 270:
			sb.WriteString("WEDNESDAY OF THE TWENTY-SECOND WEEK")
		case 271:
			sb.WriteString("THURSDAY OF THE TWENTY-SECOND WEEK")
		case 272:
			sb.WriteString("FRIDAY OF THE TWENTY-SECOND WEEK")
		case 273:
			sb.WriteString("SATURDAY OF THE TWENTY-SECOND WEEK")
		case 274:
			sb.WriteString("TWENTY-SECOND SUNDAY")
		case 275:
			sb.WriteString("MONDAY OF THE TWENTY-THIRD WEEK")
		case 276:
			sb.WriteString("TUESDAY OF THE TWENTY-THIRD WEEK")
		case 277:
			sb.WriteString("WEDNESDAY OF THE TWENTY-THIRD WEEK")
		case 278:
			sb.WriteString("THURSDAY OF THE TWENTY-THIRD WEEK")
		case 279:
			sb.WriteString("FRIDAY OF THE TWENTY-THIRD WEEK")
		case 280:
			sb.WriteString("SATURDAY OF THE TWENTY-THIRD WEEK")
		case 281:
			sb.WriteString("TWENTY-THIRD SUNDAY")
		case 282:
			sb.WriteString("MONDAY OF THE TWENTY-FOURTH WEEK")
		case 283:
			sb.WriteString("TUESDAY OF THE TWENTY-FOURTH WEEK")
		case 284:
			sb.WriteString("WEDNESDAY OF THE TWENTY-FOURTH WEEK")
		case 285:
			sb.WriteString("THURSDAY OF THE TWENTY-FOURTH WEEK")
		case 286:
			sb.WriteString("FRIDAY OF THE TWENTY-FOURTH WEEK")
		case 287:
			sb.WriteString("SATURDAY OF THE TWENTY-FOURTH WEEK")
		case 288:
			sb.WriteString("TWENTY-FOURTH SUNDAY")
		case 289:
			sb.WriteString("MONDAY OF THE TWENTY-FIFTH WEEK")
		case 290:
			sb.WriteString("TUESDAY OF THE TWENTY-FIFTH WEEK")
		case 291:
			sb.WriteString("WEDNESDAY OF THE TWENTY-FIFTH WEEK")
		case 292:
			sb.WriteString("THURSDAY OF THE TWENTY-FIFTH WEEK")
		case 293:
			sb.WriteString("FRIDAY OF THE TWENTY-FIFTH WEEK")
		case 294:
			sb.WriteString("SATURDAY OF THE TWENTY-FIFTH WEEK")
		case 295:
			sb.WriteString("TWENTY-FIFTH SUNDAY")
		case 296:
			sb.WriteString("MONDAY OF THE TWENTY-SIXTH WEEK")
		case 297:
			sb.WriteString("TUESDAY OF THE TWENTY-SIXTH WEEK")
		case 298:
			sb.WriteString("WEDNESDAY OF THE TWENTY-SIXTH WEEK")
		case 299:
			sb.WriteString("THURSDAY OF THE TWENTY-SIXTH WEEK")
		case 300:
			sb.WriteString("FRIDAY OF THE TWENTY-SIXTH WEEK")
		case 301:
			sb.WriteString("SATURDAY OF THE TWENTY-SIXTH WEEK")
		case 302:
			sb.WriteString("TWENTY-SIXTH SUNDAY")
		case 303:
			sb.WriteString("MONDAY OF THE TWENTY-SEVENTH WEEK")
		case 304:
			sb.WriteString("TUESDAY OF THE TWENTY-SEVENTH WEEK")
		case 305:
			sb.WriteString("WEDNESDAY OF THE TWENTY-SEVENTH WEEK")
		case 306:
			sb.WriteString("THURSDAY OF THE TWENTY-SEVENTH WEEK")
		case 307:
			sb.WriteString("FRIDAY OF THE TWENTY-SEVENTH WEEK")
		case 308:
			sb.WriteString("SATURDAY OF THE TWENTY-SEVENTH WEEK")
		case 309:
			sb.WriteString("TWENTY-SEVENTH SUNDAY")
		case 310:
			sb.WriteString("MONDAY OF THE TWENTY-EIGHTH WEEK")
		case 311:
			sb.WriteString("TUESDAY OF THE TWENTY-EIGHTH WEEK")
		case 312:
			sb.WriteString("WEDNESDAY OF THE TWENTY-EIGHTH WEEK")
		case 313:
			sb.WriteString("THURSDAY OF THE TWENTY-EIGHTH WEEK")
		case 314:
			sb.WriteString("FRIDAY OF THE TWENTY-EIGHTH WEEK")
		case 315:
			sb.WriteString("SATURDAY OF THE TWENTY-EIGHTH WEEK")
		case 316:
			sb.WriteString("TWENTY-EIGHTH SUNDAY")
		case 317:
			sb.WriteString("MONDAY OF THE TWENTY-NINTH WEEK")
		case 318:
			sb.WriteString("TUESDAY OF THE TWENTY-NINTH WEEK")
		case 319:
			sb.WriteString("WEDNESDAY OF THE TWENTY-NINTH WEEK")
		case 320:
			sb.WriteString("THURSDAY OF THE TWENTY-NINTH WEEK")
		case 321:
			sb.WriteString("FRIDAY OF THE TWENTY-NINTH WEEK")
		case 322:
			sb.WriteString("SATURDAY OF THE TWENTY-NINTH WEEK")
		case 323:
			sb.WriteString("TWENTY-NINTH SUNDAY")
		case 324:
			sb.WriteString("MONDAY OF THE THIRTIETH WEEK")
		case 325:
			sb.WriteString("TUESDAY OF THE THIRTIETH WEEK")
		case 326:
			sb.WriteString("WEDNESDAY OF THE THIRTIETH WEEK")
		case 327:
			sb.WriteString("THURSDAY OF THE THIRTIETH WEEK")
		case 328:
			sb.WriteString("FRIDAY OF THE THIRTIETH WEEK")
		case 329:
			sb.WriteString("SATURDAY OF THE THIRTIETH WEEK")
		case 330:
			sb.WriteString("THIRTIETH SUNDAY")
		case 331:
			sb.WriteString("MONDAY OF THE THIRTY-FIRST WEEK")
		case 332:
			sb.WriteString("TUESDAY OF THE THIRTY-FIRST WEEK")
		case 333:
			sb.WriteString("WEDNESDAY OF THE THIRTY-FIRST WEEK")
		case 334:
			sb.WriteString("THURSDAY OF THE THIRTY-FIRST WEEK")
		case 335:
			sb.WriteString("FRIDAY OF THE THIRTY-FIRST WEEK")
		case 336:
			sb.WriteString("SATURDAY OF THE THIRTY-FIRST WEEK")
		case 337:
			sb.WriteString("THIRTY-FIRST SUNDAY")
		case 338:
			sb.WriteString("MONDAY OF THE THIRTY-SECOND WEEK")
		case 339:
			sb.WriteString("TUESDAY OF THE THIRTY-SECOND WEEK")
		case 340:
			sb.WriteString("WEDNESDAY OF THE THIRTY-SECOND WEEK")
		case 341:
			sb.WriteString("THURSDAY OF THE THIRTY-SECOND WEEK")
		case 342:
			sb.WriteString("FRIDAY OF THE THIRTY-SECOND WEEK")
		case 343:
			sb.WriteString("SATURDAY OF THE THIRTY-SECOND WEEK")
		case 344:
			sb.WriteString("THIRTY-SECOND SUNDAY")
		case 345:
			sb.WriteString("MONDAY OF THE THIRTY-THIRD WEEK")
		case 346:
			sb.WriteString("TUESDAY OF THE THIRTY-SECOND WEEK")
		case 347:
			sb.WriteString("WEDNESDAY OF THE THIRTY-THIRD WEEK")
		case 348:
			sb.WriteString("THURSDAY OF THE THIRTY-THIRD WEEK")
		case 349:
			sb.WriteString("FRIDAY OF THE THIRTY-THIRD WEEK")
		case 350:
			sb.WriteString("SATURDAY OF THE THIRTY-THIRD WEEK")
		case 352:
			sb.WriteString("MONDAY OF THE THIRTY-FOURTH WEEK")
		case 353:
			sb.WriteString("TUESDAY OF THE THIRTY-FOURTH WEEK")
		case 354:
			sb.WriteString("WEDNESDAY OF THE THIRTY-FOURTH WEEK")
		case 355:
			sb.WriteString("THURSDAY OF THE THIRTY-FOURTH WEEK")
		case 356:
			sb.WriteString("FRIDAY OF THE THIRTY-FOURTH WEEK")
		case 357:
			sb.WriteString("SATURDAY OF THE THIRTY-FOURTH WEEK")
		case 359:
			sb.WriteString("MONDAY OF THE THIRTY-FIFTH WEEK")
		case 360:
			sb.WriteString("TUESDAY OF THE THIRTY-FIFTH WEEK")
		case 361:
			sb.WriteString("WEDNESDAY OF THE THIRTY-FIFTH WEEK")
		case 362:
			sb.WriteString("THURSDAY OF THE THIRTY-FIFTH WEEK")
		case 363:
			sb.WriteString("FRIDAY OF THE THIRTY-FIFTH WEEK")
		case 364:
			sb.WriteString("SATURDAY OF THE THIRTY-FIFTH WEEK")
		}
	}
	return sb.String()
}

// golang date/time layouts must use the reference time, which is:
// Mon Jan 2 15:04:05 -0700 MST 2006
const (
	LayoutMonth             = "Jan"
	LayoutDay               = "Mon"
	LayoutDayFull           = "Monday"
	LayoutMonthDay          = "Jan 2"
	LayoutJanuary1Comma2020 = "Monday, January 2, 2006"
)

func (ldp *LDP) FormattedDate(date time.Time, layout string) string {
	return date.Format(layout)
}

// FormattedLiturgicalDate returns theDay formatted using LayoutJanuary1Comma2020
func (ldp *LDP) FormattedLiturgicalDate() string {
	return ldp.FormattedDate(ldp.TheDay, LayoutJanuary1Comma2020)
}

// MonthDayEQ Is the LDP Month and Day == parm month and day?
func (ldp *LDP) MonthDayEQ(month, day int) bool {
	date := NewDate(ldp.TheDay.Year(), month, day)
	return ldp.TheDay.Equal(date)
}

// MonthDayNEQ Is the LDP Month and Day != parm month and day?
func (ldp *LDP) MonthDayNEQ(month, day int) bool {
	return !ldp.MonthDayEQ(month, day)
}

// MonthDayGT Is the LDP Month and Day > parm month and day?
func (ldp *LDP) MonthDayGT(month, day int) bool {
	date := NewDate(ldp.TheDay.Year(), month, day)
	return ldp.TheDay.After(date)
}

// MonthDayGTEQ Is the LDP Month and Day >= parm month and day?
func (ldp *LDP) MonthDayGTEQ(month, day int) bool {
	date := NewDate(ldp.TheDay.Year(), month, day)
	return ldp.TheDay.After(date) || ldp.MonthDayEQ(month, day)
}

// MonthDayLT Is the LDP Month and Day < parm month and day?
func (ldp *LDP) MonthDayLT(month, day int) bool {
	date := NewDate(ldp.TheDay.Year(), month, day)
	return ldp.TheDay.Before(date)
}

// MonthDayLTEQ Is the LDP Month and Day <= parm month and day?
func (ldp *LDP) MonthDayLTEQ(month, day int) bool {
	date := NewDate(ldp.TheDay.Year(), month, day)
	return ldp.TheDay.Before(date) || ldp.MonthDayEQ(month, day)
}

// DayOfWeekEQ Is the LDP DayOfWeek == parm day?
func (ldp *LDP) DayOfWeekEQ(day int) bool {
	return int(ldp.TheDay.Weekday()) == day
}

// DayOfWeekNEQ Is the LDP DayOfWeek != parm day?
func (ldp *LDP) DayOfWeekNEQ(day int) bool {
	return int(ldp.TheDay.Weekday()) != day
}

// DayOfWeekGT Is the LDP DayOfWeek > parm day?
func (ldp *LDP) DayOfWeekGT(day int) bool {
	return int(ldp.TheDay.Weekday()) > day
}

// DayOfWeekGTEQ Is the LDP DayOfWeek >= parm day?
func (ldp *LDP) DayOfWeekGTEQ(day int) bool {
	return int(ldp.TheDay.Weekday()) >= day
}

// DayOfWeekLT Is the LDP DayOfWeek < parm day?
func (ldp *LDP) DayOfWeekLT(day int) bool {
	return int(ldp.TheDay.Weekday()) < day
}

// DayOfWeekLTEQ Is the LDP DayOfWeek >= parm day?
func (ldp *LDP) DayOfWeekLTEQ(day int) bool {
	return int(ldp.TheDay.Weekday()) <= day
}

// SundayAfterElevationOfCrossEQ Is the Month and Day of LDP SundayAfterElevationOfCrossDateLast == parm month and day?
func (ldp *LDP) SundayAfterElevationOfCrossEQ(month, day int) bool {
	date := NewDate(ldp.SundayAfterElevationOfCrossDateLast.Year(), month, day)
	return ldp.SundayAfterElevationOfCrossDateLast.Equal(date)
}

// SundayAfterElevationOfCrossNEQ Is the Month and Day of LDP SundayAfterElevationOfCrossDateLast != parm month and day?
func (ldp *LDP) SundayAfterElevationOfCrossNEQ(month, day int) bool {
	return !ldp.SundayAfterElevationOfCrossEQ(month, day)
}

// SundayAfterElevationOfCrossGT Is the Month and Day of LDP SundayAfterElevationOfCrossDateLast > parm month and day?
func (ldp *LDP) SundayAfterElevationOfCrossGT(month, day int) bool {
	date := NewDate(ldp.SundayAfterElevationOfCrossDateLast.Year(), month, day)
	return ldp.SundayAfterElevationOfCrossDateLast.After(date)
}

// SundayAfterElevationOfCrossGTEQ Is the Month and Day of LDP SundayAfterElevationOfCrossDateLast >= parm month and day?
func (ldp *LDP) SundayAfterElevationOfCrossGTEQ(month, day int) bool {
	date := NewDate(ldp.SundayAfterElevationOfCrossDateLast.Year(), month, day)
	return ldp.SundayAfterElevationOfCrossDateLast.After(date) || ldp.SundayAfterElevationOfCrossEQ(month, day)
}

// SundayAfterElevationOfCrossLT Is the Month and Day of LDP SundayAfterElevationOfCrossDateLast < parm month and day?
func (ldp *LDP) SundayAfterElevationOfCrossLT(month, day int) bool {
	date := NewDate(ldp.SundayAfterElevationOfCrossDateLast.Year(), month, day)
	return ldp.SundayAfterElevationOfCrossDateLast.Before(date)
}

// SundayAfterElevationOfCrossLTEQ Is the Month and Day of LDP SundayAfterElevationOfCrossDateLast <= parm month and day?
func (ldp *LDP) SundayAfterElevationOfCrossLTEQ(month, day int) bool {
	date := NewDate(ldp.SundayAfterElevationOfCrossDateLast.Year(), month, day)
	return ldp.SundayAfterElevationOfCrossDateLast.Before(date) || ldp.SundayAfterElevationOfCrossEQ(month, day)
}

// EothinonEQ Is the LDP eothinon nbr == parm nbr?
func (ldp *LDP) EothinonEQ(nbr int) bool {
	return ldp.EothinonNumber == nbr
}

// EothinonNEQ Is the LDP eothinon nbr != parm nbr?
func (ldp *LDP) EothinonNEQ(nbr int) bool {
	return ldp.EothinonNumber != nbr
}

// EothinonGT Is the LDP eothinon nbr > parm nbr?
func (ldp *LDP) EothinonGT(nbr int) bool {
	return ldp.EothinonNumber > nbr
}

// EothinonGTEQ Is the LDP eothinon nbr >= parm nbr?
func (ldp *LDP) EothinonGTEQ(nbr int) bool {
	return ldp.EothinonNumber >= nbr
}

// EothinonLT Is the LDP eothinon nbr < parm nbr?
func (ldp *LDP) EothinonLT(nbr int) bool {
	return ldp.EothinonNumber < nbr
}

// EothinonLTEQ Is the LDP eothinon nbr <= parm nbr?
func (ldp *LDP) EothinonLTEQ(nbr int) bool {
	return ldp.EothinonNumber <= nbr
}

// ModeOfWeekEQ Is the LDP mode == parm mode?
func (ldp *LDP) ModeOfWeekEQ(mode int) bool {
	return ldp.ModeOfWeek == mode
}

// ModeOfWeekNEQ Is the LDP mode != parm mode?
func (ldp *LDP) ModeOfWeekNEQ(mode int) bool {
	return ldp.ModeOfWeek != mode
}

// ModeOfWeekGT Is the LDP mode > parm mode?
func (ldp *LDP) ModeOfWeekGT(mode int) bool {
	return ldp.ModeOfWeek > mode
}

// ModeOfWeekGTEQ Is the LDP mode >= parm mode?
func (ldp *LDP) ModeOfWeekGTEQ(mode int) bool {
	return ldp.ModeOfWeek >= mode
}

// ModeOfWeekLT Is the LDP mode < parm mode?
func (ldp *LDP) ModeOfWeekLT(mode int) bool {
	return ldp.ModeOfWeek < mode
}

// ModeOfWeekLTEQ Is the LDP mode <= parm mode?
func (ldp *LDP) ModeOfWeekLTEQ(mode int) bool {
	return ldp.ModeOfWeek <= mode
}

// NameOfDayEQ Is the LDP Week Day == parm day?
func (ldp *LDP) NameOfDayEQ(day int) bool {
	return int(ldp.TheDay.Weekday()) == day
}

// NameOfDayNEQ Is the LDP Week Day != parm day?
func (ldp *LDP) NameOfDayNEQ(day int) bool {
	return int(ldp.TheDay.Weekday()) != day
}

// NameOfDayGT Is the LDP Week Day > parm day?
func (ldp *LDP) NameOfDayGT(day int) bool {
	return int(ldp.TheDay.Weekday()) > day
}

// NameOfDayGTEQ Is the LDP Week Day >= parm day?
func (ldp *LDP) NameOfDayGTEQ(day int) bool {
	return int(ldp.TheDay.Weekday()) >= day

}

// NameOfDayLT Is the LDP Week Day < parm day?
func (ldp *LDP) NameOfDayLT(day int) bool {
	return int(ldp.TheDay.Weekday()) < day
}

// NameOfDayLTEQ Is the LDP Week Day <= parm day?
func (ldp *LDP) NameOfDayLTEQ(day int) bool {
	return int(ldp.TheDay.Weekday()) <= day
}

// MovableCycleDayEQ Is the LDP MovableCycleDay == parm day?
func (ldp *LDP) MovableCycleDayEQ(day int) bool {
	return ldp.MovableCycleDay == day
}

// MovableCycleDayNEQ Is the LDP MovableCycleDay != parm day?
func (ldp *LDP) MovableCycleDayNEQ(day int) bool {
	return ldp.MovableCycleDay != day
}

// MovableCycleDayGT Is the LDP MovableCycleDay > parm day?
func (ldp *LDP) MovableCycleDayGT(day int) bool {
	return ldp.MovableCycleDay > day
}

// MovableCycleDayGTEQ Is the LDP MovableCycleDay >= parm day?
func (ldp *LDP) MovableCycleDayGTEQ(day int) bool {
	return ldp.MovableCycleDay >= day
}

// MovableCycleDayLT Is the LDP MovableCycleDay < parm day?
func (ldp *LDP) MovableCycleDayLT(day int) bool {
	return ldp.MovableCycleDay < day
}

// MovableCycleDayLTEQ Is the LDP MovableCycleDay <= parm day?
func (ldp *LDP) MovableCycleDayLTEQ(day int) bool {
	return ldp.MovableCycleDay <= day
}

// LukanCycleDayEQ Is the LDP LukanCycleDay == parm day?
func (ldp *LDP) LukanCycleDayEQ(day int) bool {
	return ldp.LukanCycleDay == day
}

// LukanCycleDayNEQ Is the LDP LukanCycleDay != parm day?
func (ldp *LDP) LukanCycleDayNEQ(day int) bool {
	return ldp.LukanCycleDay != day
}

// LukanCycleDayGT Is the LDP LukanCycleDay > parm day?
func (ldp *LDP) LukanCycleDayGT(day int) bool {
	return ldp.LukanCycleDay > day
}

// LukanCycleDayGTEQ Is the LDP LukanCycleDay >= parm day?
func (ldp *LDP) LukanCycleDayGTEQ(day int) bool {
	return ldp.LukanCycleDay >= day
}

// LukanCycleDayLT Is the LDP LukanCycleDay < parm day?
func (ldp *LDP) LukanCycleDayLT(day int) bool {
	return ldp.LukanCycleDay < day
}

// LukanCycleDayLTEQ Is the LDP LukanCycleDay <= parm day?
func (ldp *LDP) LukanCycleDayLTEQ(day int) bool {
	return ldp.LukanCycleDay <= day
}

// SundaysBeforeTriodionEQ Is the LDP LukanCycleDay == parm nbr?
func (ldp *LDP) SundaysBeforeTriodionEQ(nbr int) bool {
	return ldp.NumberOfSundaysFromJan15ToStartOfTriodion == nbr
}

// SundaysBeforeTriodionNEQ Is the LDP LukanCycleDay != parm nbr?
func (ldp *LDP) SundaysBeforeTriodionNEQ(nbr int) bool {
	return ldp.NumberOfSundaysFromJan15ToStartOfTriodion != nbr
}

// SundaysBeforeTriodionGT Is the LDP LukanCycleDay > parm nbr?
func (ldp *LDP) SundaysBeforeTriodionGT(nbr int) bool {
	return ldp.NumberOfSundaysFromJan15ToStartOfTriodion > nbr
}

// SundaysBeforeTriodionGTEQ is the LDP LukanCycleDay >= parm nbr?
func (ldp *LDP) SundaysBeforeTriodionGTEQ(nbr int) bool {
	return ldp.NumberOfSundaysFromJan15ToStartOfTriodion >= nbr
}

// SundaysBeforeTriodionLT Is the LDP LukanCycleDay < parm nbr?
func (ldp *LDP) SundaysBeforeTriodionLT(nbr int) bool {
	return ldp.NumberOfSundaysFromJan15ToStartOfTriodion < nbr
}

// SundaysBeforeTriodionLTEQ Is the LDP LukanCycleDay <= parm day?
func (ldp *LDP) SundaysBeforeTriodionLTEQ(day int) bool {
	return ldp.NumberOfSundaysFromJan15ToStartOfTriodion <= day
}
