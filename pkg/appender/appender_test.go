package appender

import (
	"fmt"
	"testing"
)

func TestAppender(t *testing.T) {
	expect := `This is line 1. This is also on line 1.
if x == y {
  var y string
  y = 3
  if y == 3 {
// I am not indented
    alert('it is a three!')
  }
}
`
	a := NewAppender("  ") // use two spaces for indent
	a.Write("This is line 1. ")
	a.WriteLine("This is also on line 1.")
	a.WriteIndentedLine("if x == y {")
	a.PushIndent()
	a.WriteIndentedLine("var y string")
	a.WriteIndentedLine("y = 3")
	a.WriteIndentedLine("if y == 3 {")
	a.PushIndent()
	a.WriteLine("// I am not indented")
	a.WriteIndentedLine("alert('it is a three!')")
	a.PopIndent()
	a.WriteIndentedLine("}")
	a.PopIndent()
	a.WriteIndentedLine("}")
	s := a.String()
	if s != expect {
		t.Errorf(fmt.Sprintf("expected: \n%s\n\nbut got\n%s", expect, s))
	}
}
