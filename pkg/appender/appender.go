// Package appender provides a version of strings.Builder with supporting methods
// to output a string with both line breaks and indentation.  This is useful when
// writing a string to represent code written in a programming language or HTML
package appender

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/utils/stack"
	"strings"
)

// Appender holds three internal properties:
// sb is the underlying strings.Builder.
// indentSpaces is a string that provides each requested indentation, which can be spaces or tab.s
// indentStack stores the current level of indentation based on calls to PushIndent or PopIndent.
type Appender struct {
	sb           strings.Builder
	indentSpaces string
	indentStack  *stack.StringStack
}

// NewAppender returns an appender with the indent set to spaces, which can be either actual spaces or tabs.
func NewAppender(spaces string) *Appender {
	a := new(Appender)
	a.indentSpaces = spaces
	a.indentStack = new(stack.StringStack)
	return a
}

// ClearIndent resets the indent stack to zeros
func (a *Appender) ClearIndent() {
	a.indentStack = new(stack.StringStack)
}
func (a *Appender) Empty() bool {
	return a.sb.Len() == 0
}

// Indent returns a string with the current number of indents on the indent stack
func (a *Appender) Indent() string {
	return a.indentStack.Join("")
}

// Levels returns the number of indentation levels (i.e. the length of the indent stack)
func (a *Appender) Levels() int {
	return a.indentStack.Size()
}

// Len returns the size of the underlying strings.Builder
func (a *Appender) Len() int {
	return a.sb.Len()
}
func (a *Appender) PushIndent() {
	a.indentStack.Push(a.indentSpaces)
}
func (a *Appender) PopIndent() {
	a.indentStack.Pop()
}

// Reset assigns a new underlying strings.Builder and indent stack.
func (a *Appender) Reset() {
	a.sb = strings.Builder{}
	a.indentStack = new(stack.StringStack)
}
func (a *Appender) String() string {
	return a.sb.String()
}

// NewLine writes \n
func (a *Appender) NewLine() {
	a.sb.WriteString("\n")
}

// Write the string s without doing anything else.  It is up to you to put a space before or after if you want.
func (a *Appender) Write(s string) {
	a.sb.WriteString(s)
}

// WriteIndented writes 1) an indent and 2) the supplied string without a \n
func (a *Appender) WriteIndented(s string) {
	a.sb.WriteString(s)
}

// WriteLine writes 1) the supplied string and 2) \n
func (a *Appender) WriteLine(s string) {
	a.sb.WriteString(fmt.Sprintf("%s\n", s))
}

// WriteIndentedLine writes 1) an indent 2) the supplied string and 3) \n
func (a *Appender) WriteIndentedLine(s string) {
	a.sb.WriteString(fmt.Sprintf("%s%s\n", a.Indent(), s))
}
