package subscriptions

import (
	"fmt"
	"github.com/liturgiko/doxa/app"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/utils/clinput"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"path"
)

var subs *Subscriptions

func SubCmd(args []string) {
	if len(args) < 2 {
		fmt.Println("Commands: sourceAdd, install, autoSync, update, updateAll, installAll")
		return
	}
	if subs == nil {
		initSubs()
	}
	switch args[1] {
	case "sourceAdd":
	case "install":
		if len(args) < 3 {
			fmt.Printf("Expected a library name.\n(e.g. %s %s templates)\n", args[0], args[1])
			return
		}
		for listname, rl := range subs.Sources {
			for _, r := range rl.List {
				//TODO case insensitive/regexp?
				if r.Name == args[2] {
					switch clinput.GetInput(fmt.Sprintf("Found package '%s' (in list '%s') Do you want to:", r.Name, listname), []string{
						"a", "Install",
						"b", "Keep Looking",
						"c", "Quit",
					}) {
					case 'a':
						err := r.Install()
						if err != nil {
							fmt.Printf("Install failed: %v\n", err)
						}
						return
					case 'b':
						continue
					case 'c':
						return
					}
				}
			}
		}
		fmt.Printf("Package '%s' not found\n", args[2])
	case "auto":
	case "update":
		if len(args) < 3 {
			fmt.Printf("Expected a library name.\n(e.g. %s %s templates)\n", args[0], args[1])
			return
		}
		for _, rl := range subs.Sources {
			for _, r := range rl.List {
				if !r.Installed {
					continue
				}
				//TODO case insensitive/regexp?
				if r.Name == args[2] {
					err := r.Update()
					if err != nil {
						fmt.Printf("Update failed: %v\n", err)
					}
					return
				}
			}
		}
		fmt.Printf("Package '%s' not currently installed\n", args[2])
	case "updateAll":
	case "installAll":
	case "init":
	}
	if loc, sane := subs.Sources["local"]; sane {
		f, err := ltfile.Create(path.Join(app.Ctx.Paths.SubscriptionsPath, "localRepo.json"))
		if err != nil {
			doxlog.Warnf("unable to update local subscription information: %v", err)
			return
		}
		_, err = loc.WriteToFile(f)
		f.Close()
		if err != nil {
			doxlog.Warnf("unable to update local subscription information: %v", err)
		}
	}
}

func initSubs() {
	var err error
	fmt.Println("initializing subscription manager")
	subs = new(Subscriptions)
	subs.Sources = make(map[string]*RepoList)
	subs.Installed = make(map[string]*Repo)
	subs.Sources["local"], err = LoadRepoListFromFile(path.Join(app.Ctx.Paths.SubscriptionsPath, "localRepo.json"))
	if err != nil {
		doxlog.Warnf("load failed with error %v", err)
	}
	if subs.Sources["local"] == nil || subs.Sources["local"].List == nil {
		subs.Sources["local"] = new(RepoList)
		subs.Sources["local"].Local = true
		switch clinput.GetInput("No sources found... subscribe to Fr. Seraphim?",
			[]string{"y", "yes", "n", "no"}) {
		case 'y':
			frS := []string{
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_redirects_goarch.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_uk_kjv.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_uk_ware.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_aana.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_acook.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_andronache.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_barrett.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_carroll.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_dedes.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_duvall.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_eob.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_goa.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_goadedes.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_goarch.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_holycross.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_houpos.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_mlgs.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_net.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_nkjv.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_oca.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_public.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_roumas.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_rsv.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_saas.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/en_us_unknown.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/gr_gr_cog.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/gr_redirects_goarch.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/gr_us_goa.git",
				"https://gitlab.com/doxa-seraphimdedes/resources/ltx/gr_us_holycross.git",
				"https://gitlab.com/doxa-seraphimdedes/templates.git",
			}
			for _, r := range frS {
				err = subs.RepoInstall(r, subs.SuggestName(r, ""))
				if err != nil {
					doxlog.Errorf("Error installing repo: %v", err)
				}
			}
		case 'n':
			//attempt to salvage pullUrls.txt
		}
	}
	subs.Sources["local"].Local = true
}

func SetupLocal() {
	initSubs()
	if subs == nil {
		return
	}
	if loc, sane := subs.Sources["local"]; sane {
		f, err := ltfile.Create(path.Join(app.Ctx.Paths.SubscriptionsPath, "localRepo.json"))
		if err != nil {
			doxlog.Warnf("unable to update local subscription information: %v", err)
			return
		}
		_, err = loc.WriteToFile(f)
		f.Close()
		if err != nil {
			doxlog.Warnf("unable to update local subscription information: %v", err)
		}
	}
}
