package subscriptions

import (
	"github.com/liturgiko/doxa/app"
	"github.com/liturgiko/doxa/pkg/inOut"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"os"
	"path"
)

func LoadDB(repoPath string) error {
	tmpDir := os.TempDir()
	tmpComboFilePath := path.Join(tmpDir, "combo.tsv")
	errors := ltfile.Concatenate(repoPath, tmpComboFilePath, "tsv")
	if len(errors) > 0 {
		return errors[0]
	}
	_, errors = app.Ctx.Kvs.Db.Import("", tmpComboFilePath, inOut.LineParser, true, false, true)
	if len(errors) > 0 {
		return errors[0]
	}
	return nil
}
