package subscriptions

import (
	"encoding/json"
	"fmt"
	"github.com/liturgiko/doxa/app"
	"github.com/liturgiko/doxa/pkg/dvcs"
	"github.com/liturgiko/doxa/pkg/enums/gitStatuses"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"os"
	"strings"
	"time"
)

type RepoFormat int16

const (
	GitRepo RepoFormat = iota
	ZipFile
	Directory //offline only
)

// A Repo corresponds to an individual library, a templates collection, or a css collection
type Repo struct {
	Source        string     // where this be obtained from
	SourceFormat  RepoFormat // indicates whether the source is an url, file, etc.
	Version       string
	Name          string
	Installed     bool
	License       string
	Dependencies  *RepoList
	InstalledPath string
}

type RepoList struct {
	Url   string
	List  []*Repo
	Local bool //if a repoList is not local we cannot modify it.
}

func (l *RepoList) Refresh() error {
	return nil
}

func (l *RepoList) Contains(RepoName string) bool {
	return false
}

func (l *RepoList) AddRepo(r *Repo) error {
	if !l.Local {
		return fmt.Errorf("cannot add repo to remote repolist")
	}
	if l.List == nil {
		l.List = []*Repo{}
	}
	l.List = append(l.List, r)
	return nil
}

func (l *RepoList) RemoveRepo(r *Repo) error {
	if !l.Local {
		return fmt.Errorf("cannot remove repo from remote repolist")
	}
	return nil
}

// WriteToFile Write the list as JSON
func (l *RepoList) WriteToFile(file *os.File) (int, error) {
	cont, err := json.Marshal(l)
	if err != nil {
		return -1, err
	}
	return file.Write(cont)
}

func LoadRepoListFromFile(path string) (*RepoList, error) {
	var raw []byte
	raw, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}
	r := new(RepoList)
	err = json.Unmarshal(raw, r)
	if err != nil {
		return nil, err
	}
	return r, nil
}

func PullListToRepoList() {

}

type Subscriptions struct {
	Sources   map[string]*RepoList
	Installed map[string]*Repo
}

func (s *Subscriptions) SubscribeTo(r string) error {
	return nil
}

func (s *Subscriptions) UnsubscribeFrom(r string) error {
	return nil
}

func (s *Subscriptions) UpdateAll() error {
	return nil
}

func (s *Subscriptions) Update(r string) error {
	return nil
}

func (s *Subscriptions) UpdateFromList(l string) error {
	if list, exists := s.Sources[l]; exists {
		for _, r := range list.List {
			r.Update()
		}
	} else {
		return fmt.Errorf("%s does not exist", l)
	}
	return nil
}

func (s *Subscriptions) InstallDependencies(r string) error {
	return nil
}

func (s *Subscriptions) RepoInstall(url, name string) error {
	if local, exists := s.Sources["local"]; exists {
		for _, r := range local.List {
			if r.Name == name {
				return fmt.Errorf("%s already exists", name)
			}
		}

		r := new(Repo)
		r.Name = name
		r.SourceFormat = GitRepo //TODO: diversify
		r.Source = url
		if err := local.AddRepo(r); err != nil {
			return err
		}
		return r.Install()
	}
	return fmt.Errorf("failed to install repo. local not available")
}

func (s *Subscriptions) SuggestName(url, name string) string {
	var groupname, reponame string
	//extract the last bit of the url
	pieces := strings.Split(url, "/")
	domainIndex := 0
	for i, s := range pieces {
		if strings.Contains(s, ".") {
			domainIndex = i
			break
		}
	}
	if len(pieces) < 3 {
		return ""
	}
	groupname = pieces[domainIndex+1]
	reponame = pieces[len(pieces)-1]
	reponame, _ = strings.CutSuffix(reponame, ".git")
	if name == "" {
		name = reponame
	}
	// look in local, and if name is non-blank return it, if it is free otherwise return a new suggestion
	if local, exists := s.Sources["local"]; exists {
		for _, r := range local.List {
			if r.Name == name {
				if strings.HasPrefix(name, groupname+"-") {
					return s.SuggestName(url, name+time.Now().String())
				}
				return s.SuggestName(url, groupname+"-"+name)
			}
		}
	}
	return name
}

func (r *Repo) UpdateStatus() (bool, *dvcs.DiffStatus, error) {
	g := dvcs.NewGitClient(r.InstalledPath)
	// TODO: check to see if repo is up to date with remote
	diffStatus, err := g.LocalAndOriginDiffer()
	if err != nil {
		return false, diffStatus, err
	}
	if !diffStatus.Differ {
		return true, diffStatus, nil
	}
	return false, diffStatus, nil
}

func (r *Repo) Update() error {
	if r.InstalledPath == "" {
		r.GeneratePath()
	}

	if !ltfile.DirExists(r.InstalledPath) {
		return fmt.Errorf("could not update: '%s' not found", r.InstalledPath)
	}
	switch r.SourceFormat {
	case GitRepo:
		g := dvcs.NewGitClient(r.InstalledPath)
		upToDate, _, err := r.UpdateStatus()
		if upToDate {
			return nil
		}
		//Not forcing, because we can uninstall and reinstall
		status, err := g.PullOrigin(r.InstalledPath, true)
		if err != nil {
			return err
		}
		if status != gitStatuses.Success {
			return fmt.Errorf("could not update: %s", status.String())
		}
	case ZipFile:
	case Directory:
	default:
		return fmt.Errorf("could not update: unknown format")
	}
	return LoadDB(r.InstalledPath)
}

func (r *Repo) Install() error {
	var err error
	switch r.SourceFormat {
	case GitRepo:
		path := app.Ctx.Paths.SubscriptionsPath
		g := dvcs.NewGitClient(r.InstalledPath)
		r.InstalledPath, err = g.Clone(path, r.Source, false)
		if err != nil {
			return fmt.Errorf("could not clone repo '%s': %v", r.Source, err)
		}
		r.Installed = true
	default:
		//do nothing
	}
	return LoadDB(r.InstalledPath)
}

func (r *Repo) GeneratePath() {
	if r.InstalledPath != "" {
		return
	}
	if r.Source == "" {
		return
	}
	if strings.HasPrefix(r.Source, "https://") || strings.HasPrefix(r.Source, "http://") {
		//web url
		if r.SourceFormat == GitRepo {

		}
	}
}
