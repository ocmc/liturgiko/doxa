package subscriptions

import "testing"

func TestRepoList_Refresh(t *testing.T) {
}

func TestRepoList_Contains(t *testing.T) {
}

func TestRepoList_AddRepo(t *testing.T) {
	rl1 := new(RepoList)
	rl2 := new(RepoList)
	rl1.Local = true
	err := rl1.AddRepo(new(Repo))
	if err != nil {
		t.Errorf("local=true failed: %v", err)
	}
	err = rl2.AddRepo(new(Repo))
	if err == nil {
		t.Errorf("Expected error adding to non-local repolist")
	}
}

func TestRepoList_RemoveRepo(t *testing.T) {
	rl1 := new(RepoList)
	rl2 := new(RepoList)
	rl1.Local = true
	err := rl1.RemoveRepo(new(Repo))
	if err != nil {
		t.Errorf("local=true failed: %v", err)
	}
	err = rl2.RemoveRepo(new(Repo))
	if err == nil {
		t.Errorf("Expected error adding to non-local repolist")
	}
}

func TestRepoList_WriteToFile(t *testing.T) {
	rl := new(RepoList)
	rl.List = []*Repo{{Source: "https://www.example.com/group/repo.git", Version: "0", Name: "example-repo", SourceFormat: GitRepo, Installed: false}}
}

func TestLoadRepoListFromFile(t *testing.T) {

}

func TestSubscriptions_SubscribeTo(t *testing.T) {

}

func TestSubscriptions_UnsubscribeFrom(t *testing.T) {

}

func TestSubscriptions_UpdateAll(t *testing.T) {

}

func TestSubscriptions_Update(t *testing.T) {

}

func TestSubscriptions_UpdateFromList(t *testing.T) {

}

func TestSubscriptions_InstallDependencies(t *testing.T) {

}

func TestSubscriptions_RepoInstall(t *testing.T) {

}

func TestSubscriptions_SuggestName(t *testing.T) {

}

func TestRepo_Update(t *testing.T) {

}

func TestRepo_Install(t *testing.T) {

}
func TestRepo_GeneratePath(t *testing.T) {

}
