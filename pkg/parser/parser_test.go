package parser

import (
	"encoding/json"
	"fmt"
	"github.com/liturgiko/doxa/pkg/atempl"
	"github.com/liturgiko/doxa/pkg/css"
	"github.com/liturgiko/doxa/pkg/doxlog"
	rowTypes "github.com/liturgiko/doxa/pkg/enums/RowTypes"
	"github.com/liturgiko/doxa/pkg/enums/calendarTypes"
	"github.com/liturgiko/doxa/pkg/enums/directiveTypes"
	"github.com/liturgiko/doxa/pkg/enums/expressionTypes"
	"github.com/liturgiko/doxa/pkg/enums/operatorTypes"
	"github.com/liturgiko/doxa/pkg/enums/spanTypes"
	"github.com/liturgiko/doxa/pkg/enums/versions"
	ldp2 "github.com/liturgiko/doxa/pkg/ldp"
	"github.com/liturgiko/doxa/pkg/resolver"
	"github.com/liturgiko/doxa/pkg/valuemap"
	"strings"
	"testing"
)

var printJson = false
var realm = "Doxa"
var parms Parms

// genLangs holds a slice of GenLib.
var genLangs []*atempl.TableLayout

// genLibs holds the requested primary and backup libraries for generation
var genLibs []*atempl.GenLib

// resolver mocks a database resolver
var mockResolver resolver.MockResolver

var htmlCss *css.StyleSheet
var pdfCss *css.StyleSheet

func init() {
	doxlog.Init("", "", true)
	doxlog.Info("doxlog initialized")
	// set primary and backup libraries to use for generation
	var genLib = new(atempl.GenLib)
	genLib.Primary = "gr_gr_cog"
	genLibs = append(genLibs, genLib)
	genLib = new(atempl.GenLib)
	genLib.Primary = "en_us_goa"
	genLib.AddFallBack("en_us_dedes")
	genLibs = append(genLibs, genLib)
	genLang := new(atempl.TableLayout)
	genLang.AddLiturgicalLib(genLib)
	genLang.AddProphetLib(genLib)
	genLang.AddGospelLib(genLib)
	genLang.AddPsalterLib(genLib)
	genLang.AddEpistleLib(genLib)
	genLang.DateFormat = "%A, %B %d, %Y"
	genLang.WeekDayFormat = "%A"
	genLangs = append(genLangs, genLang)

	// initialize the two maps
	mockResolver.TKMap = make(map[string]bool)
	mockResolver.ResolvedValues = make(map[string]string)

	// load css
	const cssForHtml = `
a.link {
}
p.actor {
    text-align:left;
    font-size:80%;
    color:red;
    font-weight:bold;
    text-indent: 0px;
    margin-top: 5px;
    margin-bottom: 0px;
}
p.dialog {
    text-align:left;
    text-indent: 28px;
    font-size:95%;
    line-height:130%;
    margin-bottom: 5px;
}
p.hymn {
    text-align:left;
    text-indent: 0px;
    line-height:150%;
    margin-bottom: 10px;
}
p.title {
	color: red;
}
span.rubric{
	font-variant:normal;
	font-weight:normal;
	font-style:italic;
	font-size:80%;
	color:red;	
}
span.it {color: black; font-style: italic; font-weight: normal}
span.bd {color: black; font-style: normal; font-weight: bold}
span.bk {color: black; font-style: normal; font-weight: normal}
span.rd {color: red; font-style: normal; font-weight: normal}
span.bkit {color: black; font-style: italic; font-weight: normal}
span.bkbd {color: black; font-style: normal; font-weight: bold}
span.bkitbd {color: black; font-style: italic; font-weight: bold}
span.rdit {color: red; font-style: italic; font-weight: normal}
span.rdbd {color: red; font-style: normal; font-weight: bold}
span.rditbd {color: red; font-style: italic; font-weight: bold}`

	const cssForPdf = cssForHtml // just reuse the htmlCss for now
	var err error
	htmlCss, err = css.NewStyleSheet("html", "", true)
	if err != nil {
		fmt.Println(err)
		return
	}
	htmlCss.Parse(cssForHtml)

	pdfCss, err = css.NewStyleSheet("pdf", "", true)
	if err != nil {
		fmt.Println(err)
		return
	}
	pdfCss.Parse(cssForPdf)

	// load block template for testing insert.
	const insert = `
id = "ages/blocks/bl.test" // used as id in database
type = "block" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
p.actor sid "actors:Priest"
p.hymn sid "oc.m1.d1:ocMA.Lauds1.text" @ver
`
	mockResolver.TKMap["ages/blocks/bl.test"] = true
	mockResolver.ResolvedValues["ages/blocks/bl.test"] = insert

	// load values into the map used to check for the existence of a topicKey
	mockResolver.TKMap["actors:Bishop"] = true
	mockResolver.TKMap["actors:Choir"] = true
	mockResolver.TKMap["actors:Deacon"] = true
	mockResolver.TKMap["actors:Priest"] = true
	mockResolver.TKMap["actors:People"] = true
	mockResolver.TKMap["da.d2:daVE.OnTheEveningBefore"] = true
	mockResolver.TKMap["oc.m1.d1:ocVE.ApolTheotokionVM.text"] = true
	mockResolver.TKMap["misc:Mode1"] = true
	mockResolver.TKMap["misc:Mode2"] = true
	mockResolver.TKMap["misc:Mode3"] = true
	mockResolver.TKMap["misc:Mode4"] = true
	mockResolver.TKMap["misc:Mode5"] = true
	mockResolver.TKMap["misc:Mode6"] = true
	mockResolver.TKMap["misc:Mode7"] = true
	mockResolver.TKMap["misc:Mode8"] = true
	mockResolver.TKMap["prayers:enarxis02"] = true
	mockResolver.TKMap["prayers:res04p"] = true
	mockResolver.TKMap["rubrical:InALowVoice"] = true
	mockResolver.TKMap["rubrical:Thrice"] = true
	mockResolver.TKMap["template.titles:ve.pdf.header"] = true
	mockResolver.TKMap["me.m01.d06:meMA.Ode1C13.text"] = true
	mockResolver.TKMap["me.m01.d06:meMA.Ode9C1H.text"] = true
	mockResolver.TKMap["me.m01.d07:meMA.Ode1C13.text"] = true
	mockResolver.TKMap["me.m01.d29:meMA.Ode1C12.text"] = true
	mockResolver.TKMap["me.m01.d30:meMA.Ode1C11.text"] = true
	mockResolver.TKMap["me.m01.d30:meMA.Ode1C12.text"] = true
	mockResolver.TKMap["me.m01.d30:meMA.Ode1C13.text"] = true
	mockResolver.TKMap["me.m02.d09:meMA.Ode1C13.text"] = true
	mockResolver.TKMap["me.m05.d21:meMA.Ode1C13.text"] = true
	mockResolver.TKMap["me.m08.d01:meMA.Ode5C2T.text"] = true
	mockResolver.TKMap["me.m12.d15:meMA.Ode1C13.text"] = true
	mockResolver.TKMap["oc.m1.d1:ocMA.Lauds1.text"] = true
	mockResolver.TKMap["oc.m2.d1:ocMA.Lauds1.text"] = true
	mockResolver.TKMap["oc.m3.d1:ocMA.Lauds1.text"] = true
	mockResolver.TKMap["oc.m4.d1:ocMA.Lauds1.text"] = true
	mockResolver.TKMap["le.ep.me.m01.dSunAE:lemeLI.Epistle.title_abbr"] = true
	mockResolver.TKMap["le.go.mc.d148:lemcLI.Gospel.text"] = true
	mockResolver.TKMap["le.ep.mc.d344:lemcLI.Epistle.title_abbr"] = true
	mockResolver.TKMap["le.go.lu.d294:leluLI.Gospel.text"] = true
	mockResolver.TKMap["le.go.lu.d006:leluLI.Gospel.text"] = true
	mockResolver.TKMap["le.go.lu.d007:leluLI.Gospel.text"] = true
	mockResolver.TKMap["le.go.lu.d008:leluLI.Gospel.text"] = true
	mockResolver.TKMap["le.go.lu.d009:leluLI.Gospel.text"] = true
	mockResolver.TKMap["le.go.lu.d010:leluLI.Gospel.text"] = true
	mockResolver.TKMap["le.go.lu.d011:leluLI.Gospel.text"] = true
	mockResolver.TKMap["le.go.lu.d012:leluLI.Gospel.text"] = true
	mockResolver.TKMap["le.go.lu.d013:leluLI.Gospel.text"] = true

	// load values in the values map, in sets of n based on n libraries requested for generation

	// versions
	mockResolver.ResolvedValues["gr_gr_cog/properties:version.designation"] = "COG"
	mockResolver.ResolvedValues["en_us_goa/properties:version.designation"] = "DOA"
	mockResolver.ResolvedValues["en_us_dedes/properties:version.designation"] = "SD"

	// template.titles:ve.pdf.header
	mockResolver.ResolvedValues["gr_gr_cog/template.titles:ve.pdf.header"] = "Ἑσπερινὸς"
	mockResolver.ResolvedValues["en_us_goa/template.titles:ve.pdf.header"] = ""
	mockResolver.ResolvedValues["en_us_dedes/template.titles:ve.pdf.header"] = "Vespers"

	// da.d2/daVE.OnTheEveningBefore has no value in database but is used by Fr. Seraphim.
	mockResolver.ResolvedValues["gr_gr_cog/da.d2:daVE.OnTheEveningBefore"] = ""
	mockResolver.ResolvedValues["en_us_goa/da.d2:daVE.OnTheEveningBefore"] = ""
	mockResolver.ResolvedValues["en_us_dedes/da.d2:daVE.OnTheEveningBefore"] = ""

	// actors:People
	mockResolver.ResolvedValues["gr_gr_cog/actors:People"] = "ΛΑΟΣ"
	mockResolver.ResolvedValues["en_us_goa/actors:People"] = "PEOPLE"
	mockResolver.ResolvedValues["en_us_dedes/actors:People"] = "PEOPLE"

	// actors:Priest
	mockResolver.ResolvedValues["gr_gr_cog/actors:Priest"] = "ΙΕΡΕΥΣ"
	mockResolver.ResolvedValues["en_us_goa/actors:Priest"] = "PRIEST"
	mockResolver.ResolvedValues["en_us_dedes/actors:Priest"] = "PRIEST"

	// actors:Deacon
	mockResolver.ResolvedValues["gr_gr_cog/actors:Deacon"] = "ΔΙΑΚΟΝΟΣ"
	mockResolver.ResolvedValues["en_us_goa/actors:Deacon"] = "DEACON"
	mockResolver.ResolvedValues["en_us_dedes/actors:Deacon"] = "DEACON"

	// rubrical:InALowVoice
	mockResolver.ResolvedValues["gr_gr_cog/rubrical:InALowVoice"] = "χαμηλοφώνως"
	mockResolver.ResolvedValues["en_us_goa/rubrical:InALowVoice"] = "(in a low voice)"
	mockResolver.ResolvedValues["en_us_dedes/rubrical:InALowVoice"] = "(in a low voice)"

	// rubrical:Thrice
	mockResolver.ResolvedValues["gr_gr_cog/rubrical:Thrice"] = "(ἐκ γʹ)"
	mockResolver.ResolvedValues["en_us_goa/rubrical:Thrice"] = "(3)"
	mockResolver.ResolvedValues["en_us_dedes/rubrical:Thrice"] = "(3)"

	// oc.m1.d1:ocVE.ApolTheotokionVM.text
	mockResolver.ResolvedValues["gr_gr_cog/oc.m1.d1:ocVE.ApolTheotokionVM.text"] = "Τοῦ Γαβριὴλ φθεγξαμένου σοι Παρθένε τὸ Χαῖρε, σὺν τῇ φωνῇ ἐσαρκοῦτο ὁ τῶν ὅλων Δεσπότης, ἐν σοὶ τῇ ἁγίᾳ κιβωτῷ, ὡς ἔφη ὁ δίκαιος Δαυΐδ. Ἐδείχθης πλατυτέρα τῶν οὐρανῶν, βαστάσασα τὸν Κτίστην σου. Δόξα τῷ ἐνοικήσαντι ἐν σοί· δόξα τῷ προελθόντι ἐκ σοῦ· δόξα τῷ ἐλευθερώσαντι ἡμᾶς, διὰ τοῦ τόκου σου."
	mockResolver.ResolvedValues["en_us_goa/oc.m1.d1:ocVE.ApolTheotokionVM.text"] = ""
	mockResolver.ResolvedValues["en_us_dedes/oc.m1.d1:ocVE.ApolTheotokionVM.text"] = "When Gabriel had uttered rejoice to you, O Virgin, * then with the voice was the Lord of all becoming incarnate * in you whom the holy Ark of old * prefigured, as righteous David said. * You carried your Creator and proved to be more spacious than the heavens. * Glory to Him who dwelt inside of you; * glory to Him who came forth from You; * glory be to Him who through your childbirth has set us free."

	// me.m01.d06:meMA.Ode1C13.text
	mockResolver.ResolvedValues["gr_gr_cog/me.m01.d06:meMA.Ode1C13.text"] = "Τὸν ῥύπον ὁ σμήχων τῶν ἀνθρώπων, τούτοις καθαρθεὶς ἐν Ἰορδάνῃ, οἷς θελήσας ὡμοιώθη ὃ ἦν μείνας, τοὺς ἐν σκότει φωτίζει Κύριος, ὅτι δεδόξασται."
	mockResolver.ResolvedValues["en_us_goa/me.m01.d06:meMA.Ode1C13.text"] = ""
	mockResolver.ResolvedValues["en_us_dedes/me.m01.d06:meMA.Ode1C13.text"] = ""
	// me.m01.d07:meMA.Ode1C13.text
	mockResolver.ResolvedValues["gr_gr_cog/me.m01.d07:meMA.Ode1C13.text"] = "Πλήρης γέγονας, τοῦ Παναγίου Πνεύματος, ἔτι κοιλίᾳ σῆς Μητρός, ἒνδον φερόμενος, καὶ σκιρτήματι τερπνῷ, τὸν τῆς Παρθενίας καρπόν, χαίρων ἐμήνυσας, καὶ προσεκύνησας, Προφῆτα πανσεβάσμιε."
	mockResolver.ResolvedValues["en_us_goa/me.m01.d07:meMA.Ode1C13.text"] = ""
	mockResolver.ResolvedValues["en_us_dedes/me.m01.d07:meMA.Ode1C13.text"] = "O all-venerable Prophet, you were filled with the All-holy Spirit while you were being carried in your mother's womb; and you joyfully made known the Fruit of virginity when you leapt with delight and worshiped Him."
	// me.m01.d30:meMA.Ode1C12.text
	mockResolver.ResolvedValues["gr_gr_cog/me.m01.d30:meMA.Ode1C12.text"] = "Ὅλην ἐν σοί, τῶν χαρισμάτων τὴν ἄβυσσον, ἀποκειμένην ἔγνωμεν· διὸ προσφεύγοντες, Θεοτόκε προθύμως, τῇ σκέπῃ σου τῇ θεία, διασῳζόμεθα."
	mockResolver.ResolvedValues["en_us_goa/me.m01.d30:meMA.Ode1C12.text"] = ""
	mockResolver.ResolvedValues["en_us_dedes/me.m01.d30:meMA.Ode1C12.text"] = "We have come to know that in you is stored the entire ocean of the gifts of grace. Therefore, O Theotokos, we are rescued when we earnestly run to you for protection."
	// me.m01.d29:meMA.Ode1C12.text
	mockResolver.ResolvedValues["gr_gr_cog/me.m01.d29:meMA.Ode1C12.text"] = "Γῆ ὥσπερ γονιμωτάτη πέφηνας, ἑκατοστεύοντα, καρποφορῶν τὸν σπόρον τῷ Χριστῷ, Θεοφόρε Ἰγνάτιε, τῷ τὰς ψυχὰς ἀρδεύοντι, τῇ ἐπομβρίᾳ τῇ τοῦ Πνεύματος."
	mockResolver.ResolvedValues["en_us_goa/me.m01.d29:meMA.Ode1C12.text"] = ""
	mockResolver.ResolvedValues["en_us_dedes/me.m01.d29:meMA.Ode1C12.text"] = "All-pure Virgin, insistently implore your Son, who was incarnate of your immaculate and honorable blood, for us who extol you, to deliver us from offences and from bitter illness."
	// me.m01.d30:meMA.Ode1C13.text
	mockResolver.ResolvedValues["gr_gr_cog/me.m01.d30:meMA.Ode1C13.text"] = "Ὑπὲρ ἡμῶν, τῶν σὲ ὑμνούντων δυσώπησον, τὸν σαρκωθέντα Πάναγνε, ἐκ τῶν ἀχράντων σου, καὶ τιμίων αἱμάτων, ῥυσθῆναι ἐκ πταισμάτων, καὶ νοσημάτων πικρῶν."
	mockResolver.ResolvedValues["en_us_goa/me.m01.d30:meMA.Ode1C13.text"] = ""
	mockResolver.ResolvedValues["en_us_dedes/me.m01.d30:meMA.Ode1C13.text"] = ""
	// me.m02.d09:meMA.Ode1C13.text
	mockResolver.ResolvedValues["gr_gr_cog/me.m02.d09:meMA.Ode1C13.text"] = "Ἐνδεδυμένος τοῦ Σταυροῦ τὴν δύναμιν, ἐῤῥωμενέστερον, τῷ δυσμενεῖ Μάρτυς συμπλακεὶς κατέῤῥαξας, καὶ νικηφόρος γέγονας, προκληθεὶς φερωνύμως, ὦ Νικηφόρε πανεύφημε, Μάρτυς τοῦ Χριστοῦ παναοίδιμε."
	mockResolver.ResolvedValues["en_us_goa/me.m02.d09:meMA.Ode1C13.text"] = ""
	mockResolver.ResolvedValues["en_us_dedes/me.m02.d09:meMA.Ode1C13.text"] = ""
	// me.m12.d15:meMA.Ode1C13.text
	mockResolver.ResolvedValues["gr_gr_cog/me.m01.d30:meMA.Ode1C11.text"] = "Τὸν εὐσεβῶς, σὲ Θεοτόκον κυρίως ἁγνήν, ὁμολογοῦντα Ἄχραντε, ψυχῆς καὶ σώματος, χαλεπῶν ἐκ κινδύνων, καὶ νόσων καὶ πταισμάτων, σὺ με διάσωσον."
	mockResolver.ResolvedValues["en_us_goa/me.m01.d30:meMA.Ode1C11.text"] = ""
	mockResolver.ResolvedValues["en_us_dedes/me.m01.d30:meMA.Ode1C11.text"] = "Rescue me, O immaculate Virgin, from grave dangers and diseases and offences of both soul and body. I piously confess you to be literally pure Theotokos."
	// me.m01.d30:meMA.Ode1C11.text
	mockResolver.ResolvedValues["gr_gr_cog/me.m12.d15:meMA.Ode1C13.text"] = "Θεῷ προσκολλώμενος, καθαρωτάτῳ νοΐ, ἐκ νεότητος, σαρκικοῦ φρονήματος, ψυχὴν ἐμάκρυνας, καὶ τῶν θαυμάτων δαψιλῆ, χάριν ἐπλούτησας."
	mockResolver.ResolvedValues["en_us_goa/me.m12.d15:meMA.Ode1C13.text"] = ""
	mockResolver.ResolvedValues["en_us_dedes/me.m12.d15:meMA.Ode1C13.text"] = ""
	// oc.m1.d1:ocMA.Lauds1.text
	mockResolver.ResolvedValues["gr_gr_cog/oc.m1.d1:ocMA.Lauds1.text"] = "Ὑμνοῦμέν σου Χριστέ, τὸ σωτήριον πάθος, καὶ δοξάζομέν σου τὴν ἀνάστασιν."
	mockResolver.ResolvedValues["en_us_goa/oc.m1.d1:ocMA.Lauds1.text"] = ""
	mockResolver.ResolvedValues["en_us_dedes/oc.m1.d1:ocMA.Lauds1.text"] = "O Christ, we praise in song Your saving Passion, and Your resurrection do we glorify."
	// oc.m2.d1:ocMA.Lauds1.text
	mockResolver.ResolvedValues["gr_gr_cog/oc.m2.d1:ocMA.Lauds1.text"] = "Πᾶσα πνοή, καὶ πᾶσα κτίσις, σὲ δοξάζει Κύριε, ὅτι διὰ τοῦ Σταυροῦ τὸν θάνατον κατήργησας, ἵνα δείξῃς τοῖς λαοῖς, τὴν ἐκ νεκρῶν σου Ἀνάστασιν, ὡς μόνος φιλάνθρωπος."
	mockResolver.ResolvedValues["en_us_goa/oc.m2.d1:ocMA.Lauds1.text"] = ""
	mockResolver.ResolvedValues["en_us_dedes/oc.m2.d1:ocMA.Lauds1.text"] = "Every breath and all creation glorifies You, O Lord. For by means of the Cross You rendered death powerless, so that You might show the peoples Your Resurrection from the dead, as the only One who loves humanity."
	// oc.m3.d1:ocMA.Lauds1.text
	mockResolver.ResolvedValues["gr_gr_cog/oc.m3.d1:ocMA.Lauds1.text"] = "Δεῦτε πάντα τὰ ἔθνη, γνῶτε τοῦ φρικτοῦ μυστηρίου τὴν δύναμιν· Χριστὸς γὰρ ὁ Σωτὴρ ἡμῶν, ὁ ἐν ἀρχῇ Λόγος, ἐσταυρώθη δι' ἡμᾶς, καὶ ἑκὼν ἐτάφη, καὶ ἀνέστη ἐκ νεκρῶν, τοῦ σῶσαι τὰ σύμπαντα. Αὐτὸν προσκυνήσωμεν."
	mockResolver.ResolvedValues["en_us_goa/oc.m3.d1:ocMA.Lauds1.text"] = ""
	mockResolver.ResolvedValues["en_us_dedes/oc.m3.d1:ocMA.Lauds1.text"] = "O come, all you nations. Know the power of the awe-inspiring mystery. For Christ our Savior, the Logos who was in the beginning, voluntarily for us was crucified and buried, and He rose from the dead to save the universe. Let us worship Him."
	// oc.m4.d1:ocMA.Lauds1.text
	mockResolver.ResolvedValues["gr_gr_cog/oc.m4.d1:ocMA.Lauds1.text"] = "Ὁ σταυρὸν ὑπομείνας καὶ θάνατον, καὶ ἀναστὰς ἐκ τῶν νεκρῶν, παντοδύναμε Κύριε, δοξάζομέν σου τὴν Ἀνάστασιν."
	mockResolver.ResolvedValues["en_us_goa/oc.m4.d1:ocMA.Lauds1.text"] = ""
	mockResolver.ResolvedValues["en_us_dedes/oc.m4.d1:ocMA.Lauds1.text"] = "You endured crucifixion and death, O Lord, and resurrected from the dead. O Almighty One, we glorify Your resurrection."
	// le.ep.me.m01.dSunAE/lemeLI.Epistle.title_abbr
	mockResolver.ResolvedValues["gr_gr_cog/le.ep.me.m01.dSunAE:lemeLI.Epistle.title_abbr"] = "Κυρ. μετὰ τὰ Φώτα"
	mockResolver.ResolvedValues["en_us_goa/le.ep.me.m01.dSunAE:lemeLI.Epistle.title_abbr"] = ""
	mockResolver.ResolvedValues["en_us_dedes/le.ep.me.m01.dSunAE:lemeLI.Epistle.title_abbr"] = ""
	// le.go.mc.d148:lemcLI.Gospel.text
	mockResolver.ResolvedValues["gr_gr_cog/le.go.mc.d148:lemcLI.Gospel.text"] = "Τῷ καιρῷ ἐκείνῳ, ἐλθόντι τῷ Ἰησοῦ εἰς Καπερναοὺμ προσῆλθεν αὐτῷ ἑκατόνταρχος παρακαλῶν αὐτὸν καὶ λέγων· Κύριε, ὁ παῖς μου βέβληται ἐν τῇ οἰκίᾳ παραλυτικός, δεινῶς βασανιζόμενος. καὶ λέγει αὐτῷ ὁ Ἰησοῦς·"
	mockResolver.ResolvedValues["en_us_goa/le.go.mc.d148:lemcLI.Gospel.text"] = "At that time, as Jesus entered Capernaum, a centurion came forward to him, beseeching him and saying, \"Lord, my servant is lying paralyzed at home, in terrible distress.\" And he said to him, \"I will come and heal him.\""
	mockResolver.ResolvedValues["en_us_dedes/le.go.mc.d148:lemcLI.Gospel.text"] = "At that time, as Jesus entered Capernaum, a centurion came forward to him, beseeching him and saying, \"Lord, my servant is lying paralyzed at home, in terrible distress.\" And he said to him, \"I will come and heal him.\""
	// le.ep.mc.d344/lemcLI.Epistle.title_abbr
	mockResolver.ResolvedValues["gr_gr_cog/le.ep.mc.d344:lemcLI.Epistle.title_abbr"] = "Κυρ. ΛΒʹ ΕΒΔΟΜΑΔΟΣ"
	mockResolver.ResolvedValues["en_us_goa/le.ep.mc.d344:lemcLI.Epistle.title_abbr"] = ""
	mockResolver.ResolvedValues["en_us_dedes/le.ep.mc.d344:lemcLI.Epistle.title_abbr"] = ""

	parms = Parms{
		TemplatePath:        "a/se.b",
		TemplateContent:     "",
		Realm:               realm,
		TheVersion:          versions.All,
		GenLangs:            genLangs,
		TheResolver:         &mockResolver,
		MediaReverseLookup:  nil,
		CallStack:           nil,
		TemplatesById:       nil,
		TemplatesByNbr:      nil,
		TheMedia:            valuemap.NewValueMapper(),
		Values:              valuemap.NewValueMapper(),
		HtmlCss:             htmlCss,
		PdfCss:              pdfCss,
		TheCalendarType:     calendarTypes.Gregorian,
		TheLdp:              nil,
		TemplateDir:         "",
		YearOverride:        0,
		FlagMissingRidTk:    true,
		FlagMissingRidValue: false,
		IncludeCovers:       false,
	}
}

// Test to see if the parser reports the errors we expect by creating a template that is syntactically incorrect.
func TestExpectErrors(t *testing.T) {
	input := `id == "xy"
type = "services"
status = "drafty"
month = 66
day = 33
p.actor sid "actors:ThePriest""
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b",
	//input,
	//	realm,
	//	versions.All,
	//	genLangs,
	//	&mockResolver,
	//	nil,
	//	nil,
	//	nil,
	//	valuemap.NewValueMapper(),
	//	valuemap.NewValueMapper(),
	//	htmlCss,
	//	pdfCss,
	//	calendarTypes.Gregorian,
	//	nil,
	//	"",
	//	0,
	//	true,
	//	false)
	if err != nil {
		t.Errorf("%s", err)
	}
	expectedErrorCount := 7
	_, parseErrors := lml.WalkTemplate()
	if len(parseErrors) != expectedErrorCount {
		t.Errorf("expected %d parser errors, got %d", expectedErrorCount, len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
}

// Test to see if the parser reports the errors we expect by creating a template that is syntactically incorrect.
func TestExpectIdError1(t *testing.T) {
	input := `id = "x/bk.y"
type = "service" 
office =  "xx"
status = "draft"
month = 1
day = 6
p.actor sid "ltx/gr_gr_cog/actors:Priest"
`
	parms.TemplatePath = "x/bk.y"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("x/bk.y", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
		return
	}
	if lml == nil {
		t.Errorf("lml is nil")
		return
	}
	expectedErrorCount := 2 // should be 4
	_, parseErrors := lml.WalkTemplate()
	if len(parseErrors) != expectedErrorCount {
		t.Errorf("expected %d parser errors, got %d", expectedErrorCount, len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
}
func TestExpressionTrue(t *testing.T) {
	var exp = "modeOfWeek == 6"
	expTrue, parseErrors, err := ExpressionTrue(exp, 2022, 8, 1, calendarTypes.Gregorian, &mockResolver, "gr_gr_cog")
	if err != nil {
		t.Errorf("%s", err)
	}
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if !expTrue {
		t.Errorf("Expect %s == true, got false", exp)
	}
}
func TestExpressionTrueForExists(t *testing.T) {
	var exp = `exists rid "me.*:meMA.Ode1C13.text"`
	expTrue, parseErrors, err := ExpressionTrue(exp, 2020, 1, 30, calendarTypes.Gregorian, &mockResolver, "gr_gr_cog")
	if err != nil {
		t.Errorf("%s", err)
	}
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if !expTrue {
		t.Errorf("Expect %s == true, got false", exp)
	}
}

// Test to see if the parser reports the errors we expect by creating a template that is syntactically incorrect.
func TestExpectIdError2(t *testing.T) {
	input := `id = "x/se.y"
type = "service" 
office =  "zz"
status = "draft"
month = 1
day = 6
p.actor sid "actors:Priest"
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	_, parseErrors := lml.WalkTemplate()
	expect := 1
	if len(parseErrors) != expect {
		t.Errorf("expected %d parser errors, got %d", expect, len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
}
func TestLML_Tokens(t *testing.T) {
	input := `id = "x/y"`
	parms.TemplatePath = "x/y"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("x/y", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	if len(lml.ErrorListener.Errors) > 0 {
		t.Errorf("%v", lml.ErrorListener.Errors[0])
	}
}
func TestPDFHeaderEven(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.ve" // used as id in database
type = "service" 
office =  "ve" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1 // required if template type is a service
day = 6  // required if template type is a service
year = 2020 // optional for a service. If year is omitted or set to zero, the current year will be used.
pdfSettings {
	pageHeaderEven = 
		left   @pageNbr
		center @lookup sid "template.titles:ve.pdf.header" rid "da:daVE.OnTheEveningBefore" ver 1
		right  @date ver 1
}
p.actor sid "actors:Priest"
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if len(atem.PDF.Headers) == 1 {
		if atem.PDF.Headers[0].Parity != atempl.Even {
			t.Errorf("expected parity to be even, got %d", atem.PDF.Headers[0].Parity)
		}
		if len(atem.PDF.Headers[0].Left.Directives) == 1 {
			if atem.PDF.Headers[0].Left.Directives[0].Type != directiveTypes.InsertPageNbr {
				t.Errorf("expected left directive type to be InsertPageNbr, got %s", atem.PDF.Headers[0].Left.Directives[0].Type.String())
			}
		} else {
			t.Errorf("expected len(atem.PDF.Headers[0].Left.Directives)  == 1, got %d", len(atem.PDF.Headers[0].Left.Directives))
		}
		if len(atem.PDF.Headers[0].Center.Directives) == 1 {
			if atem.PDF.Headers[0].Center.Directives[0].Type != directiveTypes.InsertLookup {
				t.Errorf("expected center directive type to be InsertPageNbr, got %s", atem.PDF.Headers[0].Center.Directives[0].Type.String())
			}
		} else {
			t.Errorf("expected len(atem.PDF.Headers[0].Center.Directives)  == 1, got %d", len(atem.PDF.Headers[0].Center.Directives))
		}
		if len(atem.PDF.Headers[0].Right.Directives) == 1 {
			if atem.PDF.Headers[0].Right.Directives[0].Type != directiveTypes.InsertDate {
				t.Errorf("expected right directive type to be InsertPageNbr, got %s", atem.PDF.Headers[0].Right.Directives[0].Type.String())
			}
		} else {
			t.Errorf("expected len(atem.PDF.Headers[0].Right.Directives)  == 1, got %d", len(atem.PDF.Headers[0].Right.Directives))
		}
	} else {
		t.Errorf("expected len(atem.PDF.Headers) == 1, got %d", len(atem.PDF.Headers))
	}
}
func TestPDFHeaderOdd(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li" // used as id in database
type = "service" 
office =  "ve" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1 // required if template type is a service
day = 6  // required if template type is a service
year = 2020 // optional for a service. If year is omitted or set to zero, the current year will be used.
pdfSettings {
	pageHeaderOdd = 
		left   @pageNbr
		center @lookup sid "template.titles:ve.pdf.header" rid "da:daVE.OnTheEveningBefore" ver 1
		right  @date ver 1
}
p.actor sid "actors:Priest"
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if len(atem.PDF.Headers) == 1 {
		if atem.PDF.Headers[0].Parity != atempl.Odd {
			t.Errorf("expected parity to be even, got %d", atem.PDF.Headers[0].Parity)
		}
		if len(atem.PDF.Headers[0].Left.Directives) == 1 {
			if atem.PDF.Headers[0].Left.Directives[0].Type != directiveTypes.InsertPageNbr {
				t.Errorf("expected left directive type to be InsertPageNbr, got %s", atem.PDF.Headers[0].Left.Directives[0].Type.String())
			}
		} else {
			t.Errorf("expected len(atem.PDF.Headers[0].Left.Directives)  == 1, got %d", len(atem.PDF.Headers[0].Left.Directives))
		}
		if len(atem.PDF.Headers[0].Center.Directives) == 1 {
			if atem.PDF.Headers[0].Center.Directives[0].Type != directiveTypes.InsertLookup {
				t.Errorf("expected center directive type to be InsertPageNbr, got %s", atem.PDF.Headers[0].Center.Directives[0].Type.String())
			}
		} else {
			t.Errorf("expected len(atem.PDF.Headers[0].Center.Directives)  == 1, got %d", len(atem.PDF.Headers[0].Center.Directives))
		}
		if len(atem.PDF.Headers[0].Right.Directives) == 1 {
			if atem.PDF.Headers[0].Right.Directives[0].Type != directiveTypes.InsertDate {
				t.Errorf("expected right directive type to be InsertPageNbr, got %s", atem.PDF.Headers[0].Right.Directives[0].Type.String())
			}
		} else {
			t.Errorf("expected len(atem.PDF.Headers[0].Right.Directives)  == 1, got %d", len(atem.PDF.Headers[0].Right.Directives))
		}
	} else {
		t.Errorf("expected len(atem.PDF.Headers) == 1, got %d", len(atem.PDF.Headers))
	}
}
func TestPDFFooterEven(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.ve" // used as id in database
type = "service" 
office =  "ve" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1 // required if template type is a service
day = 6  // required if template type is a service
year = 2020 // optional for a service. If year is omitted or set to zero, the current year will be used.
pdfSettings {
	pageFooterEven = 
		left   @pageNbr
		center @lookup sid "template.titles:ve.pdf.header" rid "da:daVE.OnTheEveningBefore" ver 1
		right  @date ver 1
}
p.actor sid "actors:Priest"
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if len(atem.PDF.Footers) == 1 {
		if atem.PDF.Footers[0].Parity != atempl.Even {
			t.Errorf("expected parity to be even, got %d", atem.PDF.Footers[0].Parity)
		}
		if len(atem.PDF.Footers[0].Left.Directives) == 1 {
			if atem.PDF.Footers[0].Left.Directives[0].Type != directiveTypes.InsertPageNbr {
				t.Errorf("expected left directive type to be InsertPageNbr, got %s", atem.PDF.Footers[0].Left.Directives[0].Type.String())
			}
		} else {
			t.Errorf("expected len(atem.PDF.Footers[0].Left.Directives)  == 1, got %d", len(atem.PDF.Footers[0].Left.Directives))
		}
		if len(atem.PDF.Footers[0].Center.Directives) == 1 {
			if atem.PDF.Footers[0].Center.Directives[0].Type != directiveTypes.InsertLookup {
				t.Errorf("expected center directive type to be InsertPageNbr, got %s", atem.PDF.Footers[0].Center.Directives[0].Type.String())
			}
		} else {
			t.Errorf("expected len(atem.PDF.Footers[0].Center.Directives)  == 1, got %d", len(atem.PDF.Footers[0].Center.Directives))
		}
		if len(atem.PDF.Footers[0].Right.Directives) == 1 {
			if atem.PDF.Footers[0].Right.Directives[0].Type != directiveTypes.InsertDate {
				t.Errorf("expected right directive type to be InsertPageNbr, got %s", atem.PDF.Footers[0].Right.Directives[0].Type.String())
			}
		} else {
			t.Errorf("expected len(atem.PDF.Footers[0].Right.Directives)  == 1, got %d", len(atem.PDF.Footers[0].Right.Directives))
		}
	} else {
		t.Errorf("expected len(atem.PDF.Footers) == 1, got %d", len(atem.PDF.Footers))
	}
}
func TestPDFFooterOdd(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.ve" // used as id in database
type = "service" 
office =  "ve" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1 // required if template type is a service
day = 6  // required if template type is a service
year = 2020 // optional for a service. If year is omitted or set to zero, the current year will be used.
pdfSettings {
	pageFooterOdd = 
		left   @pageNbr
		center @lookup sid "template.titles:ve.pdf.header" rid "da:daVE.OnTheEveningBefore" ver 1
		right  @date ver 1
}
p.actor sid "actors:Priest"
`
	parms.TemplatePath = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("ages/Dated-CompareServices/m01/d06/se.m01.d06.li", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if len(atem.PDF.Footers) == 1 {
		if atem.PDF.Footers[0].Parity != atempl.Odd {
			t.Errorf("expected parity to be odd, got %d", atem.PDF.Footers[0].Parity)
		}
		if len(atem.PDF.Footers[0].Left.Directives) == 1 {
			if atem.PDF.Footers[0].Left.Directives[0].Type != directiveTypes.InsertPageNbr {
				t.Errorf("expected left directive type to be InsertPageNbr, got %s", atem.PDF.Footers[0].Left.Directives[0].Type.String())
			}
		} else {
			t.Errorf("expected len(atem.PDF.Footers[0].Left.Directives)  == 1, got %d", len(atem.PDF.Footers[0].Left.Directives))
		}
		if len(atem.PDF.Footers[0].Center.Directives) == 1 {
			if atem.PDF.Footers[0].Center.Directives[0].Type != directiveTypes.InsertLookup {
				t.Errorf("expected center directive type to be InsertPageNbr, got %s", atem.PDF.Footers[0].Center.Directives[0].Type.String())
			}
		} else {
			t.Errorf("expected len(atem.PDF.Footers[0].Center.Directives)  == 1, got %d", len(atem.PDF.Footers[0].Center.Directives))
		}
		if len(atem.PDF.Footers[0].Right.Directives) == 1 {
			if atem.PDF.Footers[0].Right.Directives[0].Type != directiveTypes.InsertDate {
				t.Errorf("expected right directive type to be InsertPageNbr, got %s", atem.PDF.Footers[0].Right.Directives[0].Type.String())
			}
		} else {
			t.Errorf("expected len(atem.PDF.Footers[0].Right.Directives)  == 1, got %d", len(atem.PDF.Footers[0].Right.Directives))
		}
	} else {
		t.Errorf("expected len(atem.PDF.Footers) == 1, got %d", len(atem.PDF.Footers))
	}
}

func TestLMLListener_IntegerExpressionTrue(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m12.d15.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 12
day = 15
year = 2019
p.actor sid "actors:Priest"
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	_, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	operatorType := operatorTypes.EQ
	expressionType := expressionTypes.ModeOfWeek
	value := 1
	if !lml.Listener.IntegerExpressionTrue(operatorType, expressionType, value) {
		msg := fmt.Sprintf("for %s expected mode == %d, got %d", lml.Listener.Ctx.LDP.TheDay.String(), value, lml.Listener.Ctx.LDP.ModeOfWeek)
		t.Errorf("%s", msg)
	}
	operatorType = operatorTypes.LTEQ
	if !lml.Listener.IntegerExpressionTrue(operatorType, expressionType, value) {
		msg := fmt.Sprintf("for %s expected mode == %d, got %d", lml.Listener.Ctx.LDP.TheDay.String(), value, lml.Listener.Ctx.LDP.ModeOfWeek)
		t.Errorf("%s", msg)
	}
	operatorType = operatorTypes.LT
	value = 5
	if !lml.Listener.IntegerExpressionTrue(operatorType, expressionType, value) {
		msg := fmt.Sprintf("for %s expected mode == %d, got %d", lml.Listener.Ctx.LDP.TheDay.String(), value, lml.Listener.Ctx.LDP.ModeOfWeek)
		t.Errorf("%s", msg)
	}
	operatorType = operatorTypes.EQ
	expressionType = expressionTypes.LukanCycleDay
	value = 91
	if !lml.Listener.IntegerExpressionTrue(operatorType, expressionType, value) {
		msg := fmt.Sprintf("for %s expected lukanCycleDay == %d, got %d", lml.Listener.Ctx.LDP.TheDay.String(), value, lml.Listener.Ctx.LDP.LukanCycleDay)
		t.Errorf("%s", msg)
	}
}
func TestLMLListener_dayOfWeekExpressionTrueEvaluation(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m12/d17/se.m12.d17.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 12
day = 17
year = 2019
p.actor sid "actors:Priest"
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	_, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expressionType := expressionTypes.DayOfWeek
	operatorType := operatorTypes.EQ
	value := 2
	if !lml.Listener.IntegerExpressionTrue(operatorType, expressionType, value) {
		msg := fmt.Sprintf("for %s expected dayOfWeek == %d, got false", lml.Listener.Ctx.LDP.FormattedLiturgicalDate(), value)
		t.Errorf("%s", msg)
	}
	operatorType = operatorTypes.LTEQ
	if !lml.Listener.IntegerExpressionTrue(operatorType, expressionType, value) {
		msg := fmt.Sprintf("for %s expected dayOfWeek <= %d, got false", lml.Listener.Ctx.LDP.FormattedLiturgicalDate(), value)
		t.Errorf("%s", msg)
	}
	operatorType = operatorTypes.LT
	value = 3
	if !lml.Listener.IntegerExpressionTrue(operatorType, expressionType, value) {
		msg := fmt.Sprintf("for %s expected dayOfWeek < %d, got %s", lml.Listener.Ctx.LDP.TheDay.String(), value, lml.Listener.Ctx.LDP.NbrDayOfWeek)
		t.Errorf("%s", msg)
	}
	operatorType = operatorTypes.NEQ
	if !lml.Listener.IntegerExpressionTrue(operatorType, expressionType, value) {
		msg := fmt.Sprintf("for %s expected dayOfWeek != %d, got %s", lml.Listener.Ctx.LDP.TheDay.String(), value, lml.Listener.Ctx.LDP.NbrDayOfWeek)
		t.Errorf("%s", msg)
	}
	operatorType = operatorTypes.GT
	value = 1
	if !lml.Listener.IntegerExpressionTrue(operatorType, expressionType, value) {
		msg := fmt.Sprintf("for %s expected dayOfWeek > %d, got false", lml.Listener.Ctx.LDP.FormattedLiturgicalDate(), value)
		t.Errorf("%s", msg)
	}
	operatorType = operatorTypes.GTEQ
	if !lml.Listener.IntegerExpressionTrue(operatorType, expressionType, value) {
		msg := fmt.Sprintf("for %s expected dayOfWeek >= %d, got %s", lml.Listener.Ctx.LDP.TheDay.String(), value, lml.Listener.Ctx.LDP.NbrDayOfWeek)
		t.Errorf("%s", msg)
	}
}

func TestInsert(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m07.d05.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 7
day = 5
year = 2020
// lukan cycle day is 294
if exists rid "le.go.lu.*:leluLI.Gospel.text" { // true
  p.hymn rid "le.go.lu.*:leluLI.Gospel.text" @ver
}
insert "ages/blocks/bl.test" // inserts two rows
`
	printLineNumbers(input)
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 3
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
	expectRealm := "Doxa"
	if lml.Listener.Ctx.Realm != expectRealm {
		t.Errorf("expected realm == %s, got %s", expectRealm, lml.Listener.Ctx.Realm)
	}
}
func TestIfStatement1(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30 // this is a thursday
year = 2020
if exists rid "me.*:meMA.Ode1C13.text"  { // true
  p.hymn rid "me.*:meMA.Ode1C13.text" @ver
}`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
	if err := expectedTKs([]string{"me.m01.d30:meMA.Ode1C13.text"}, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
	expectRealm := "Doxa"
	if lml.Listener.Ctx.Realm != expectRealm {
		t.Errorf("expected realm == %s, got %s", expectRealm, lml.Listener.Ctx.Realm)
	}
}
func expectedTKs(expectedTKs []string, rows []*atempl.MetaRow) error {
	if len(rows) == 0 {
		return fmt.Errorf("no row from which to extract TopicKey")
	}
	if len(rows[0].MetaSpans) == 0 {
		return fmt.Errorf("no span from which to extract TopicKey")
	}
	if len(rows) != len(expectedTKs) {
		var got []string
		for _, r := range rows {
			for _, s := range r.MetaSpans {
				if s.ChildSpans == nil {
					got = append(got, s.TopicKey)
				} else {
					for _, c := range s.ChildSpans {
						got = append(got, c.TopicKey)
					}
				}
			}
		}
		return fmt.Errorf("expectedTKs and rows length not same: %d, %d.\n\tExpected: %v.\n\tGot: %v", len(expectedTKs), len(rows), expectedTKs, got)
	}
	for i, tk := range expectedTKs {
		if rows[i].MetaSpans == nil {
			return fmt.Errorf("rows[%d].Metaspans == nil", i)
		}
		if rows[i].MetaSpans[0].TopicKey != tk {
			if len(rows[i].MetaSpans[0].ChildSpans) > 0 {
				if rows[i].MetaSpans[0].ChildSpans[0].TopicKey != tk {
					return fmt.Errorf("expected %s but got %s", tk, rows[i].MetaSpans[0].ChildSpans[0].TopicKey)
				} else {
					return nil
				}
			}
			return fmt.Errorf("expected %s but got %s", tk, rows[i].MetaSpans[0].TopicKey)
		}
	}
	return nil
}
func TestIfStatement2False(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
if date == jan 7 {
  p.hymn rid "me.*:meMA.Ode1C13.text" @ver
}`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 0
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
}
func TestIfStatement2True(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d07.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 7
if date == jan 7 { // true
  p.hymn rid "me.*:meMA.Ode1C13.text" @ver
}`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
	if err = expectedTKs([]string{"me.m01.d07:meMA.Ode1C13.text"}, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestIfStatement3False(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
if modeOfWeek == 1 { // false
  p.hymn rid "me.*:meMA.Ode1C13.text" @ver
}`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 0
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
}
func TestIfStatement3True(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m12.d15.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 12
day = 15
year = 2019
if modeOfWeek == 1 { // true
  p.hymn rid "me.*:meMA.Ode1C13.text" @ver
}`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
	if err = expectedTKs([]string{"me.m12.d15:meMA.Ode1C13.text"}, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestIfStatement4False(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d06.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 6
year = 2020
if  dayOfWeek == tue {
  p.hymn rid "me.*:meMA.Ode1C13.text" @ver
}`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 0
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
}
func TestIfStatement4True(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d06.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 6
year = 2020
if  dayOfWeek == mon { // true
  p.hymn rid "me.*:meMA.Ode1C13.text" @ver
}`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
	if err = expectedTKs([]string{"me.m01.d06:meMA.Ode1C13.text"}, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestIfStatement5False(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m02/d08/se.m02.d08.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 2
day = 8
year = 2020
if movableCycleDay == 1  {
  p.hymn rid "me.*:meMA.Ode1C13.text" @ver
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 0
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
}
func TestIfStatement5True(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m02/d09/se.m02.d09.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 2
day = 9
year = 2020
if movableCycleDay == 1  {
  p.hymn rid "me.*:meMA.Ode1C13.text" @ver
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
	if err = expectedTKs([]string{"me.m02.d09:meMA.Ode1C13.text"}, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestIfStatement6False(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m02/d08/se.m02.d08.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 2
day = 8
year = 2020
if lukanCycleDay == 119  {
  p.hymn rid "me.*:meMA.Ode1C13.text" @ver
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 0
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
}
func TestIfStatement7True(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m09.d17.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 9
day = 17
year = 2012
if sundayAfterElevationOfCross  == sep 16  {  
  p.dialog span.rd sid "actors:Priest" @ver 
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
	if err = expectedTKs([]string{"actors:Priest"}, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestIfStatement7False(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
if sundayAfterElevationOfCross  == sep 15  {  
  p.dialog span.rd sid "le.ep.me.m01.dSunAE:lemeLI.Epistle.title_abbr" @ver 
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 0
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
}
func TestIfStatement8True(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d02/se.m01.d02.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 2
year = 2016
if sundaysBeforeTriodion  == 5  {
  p.dialog span.rd sid "actors:Priest" @ver
}`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
	if err = expectedTKs([]string{"actors:Priest"}, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestIfStatement8False(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
if sundaysBeforeTriodion  == 1  {
  p.dialog span.rd sid "le.ep.mc.d344:lemcLI.Epistle.title_abbr" @ver
}`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 0
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
}
func TestIfStatement9(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
if exists rid "me.*:meMA.Ode1C13.text" {
  p.hymn rid "me.*:meMA.Ode1C13.text" @ver
} else {
  p.hymn rid "me.*:meMA.Ode1C12.text" @ver
}`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
	if err = expectedTKs([]string{"me.m01.d30:meMA.Ode1C13.text"}, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestIfRealmStatement(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
realm = "abc"
if realm == "abc" {
  p.hymn rid "me.*:meMA.Ode1C13.text" @ver
} else {
  p.hymn rid "me.*:meMA.Ode1C12.text" @ver
}
restoreRealm
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expectRealm := "Doxa"
	if lml.Listener.Ctx.Realm != expectRealm {
		t.Errorf("expected realm == %s, got %s", expectRealm, lml.Listener.Ctx.Realm)
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	} else {
		expectedSpanCount := 1
		paraIndex := 0
		spanIndex := 0
		if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
			t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
		} else {
			expectTK := "me.m01.d30:meMA.Ode1C13.text"
			got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
			if got != expectTK {
				t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
			}
		}
	}
}
func TestIfStatement11(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30	
year = 2020
if modeOfWeek == 1  || modeOfWeek == 7 {  // true
  p.hymn rid "me.*:meMA.Ode1C13.text" @ver // expect this
} else {  
  p.hymn rid "me.*:meMA.Ode1C12.text" @ver // not this
}`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	} else {
		expectedSpanCount := 1
		paraIndex := 0
		spanIndex := 0
		if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
			t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
		} else {
			expectTK := "me.m01.d30:meMA.Ode1C13.text"
			got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
			if got != expectTK {
				t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
			}
		}
	}
}
func TestIfStatement12(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
if modeOfWeek >= 1  || dayOfWeek > sun {  
  p.hymn rid "me.*:meMA.Ode1C13.text" @ver 
} else {  
  p.hymn rid "me.*:meMA.Ode1C12.text" @ver 
}`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err = expectedTKs([]string{"me.m01.d30:meMA.Ode1C13.text"}, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestIfStatement13False(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30	
year = 2020
// For this date, modeOfWeek == 7, dayOfWeek == Thu
if (modeOfWeek == 1  || modeOfWeek == 2) &&  dayOfWeek == thu {
  p.hymn rid "me.*:meMA.Ode1C13.text" @ver 
} else {
  p.hymn rid "me.*:meMA.Ode1C12.text" @ver 
}`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	} else {
		expectedSpanCount := 1
		paraIndex := 0
		spanIndex := 0
		if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
			t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
		} else {
			expectTK := "me.m01.d30:meMA.Ode1C12.text"
			got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
			if got != expectTK {
				t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
			}
		}
	}
}
func TestIfStatement13True(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30	
year = 2020
// For this date, modeOfWeek == 7, dayOfWeek == Thu
if (modeOfWeek == 1  || modeOfWeek == 7) &&  dayOfWeek == thu {
  p.hymn rid "me.*:meMA.Ode1C13.text" @ver 
} else {
  p.hymn rid "me.*:meMA.Ode1C12.text" @ver 
}`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	} else {
		expectedSpanCount := 1
		paraIndex := 0
		spanIndex := 0
		if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
			t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
		} else {
			expectTK := "me.m01.d30:meMA.Ode1C13.text"
			got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
			if got != expectTK {
				t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
			}
		}
	}
}
func TestIfStatement14(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// mode of week is 7
if modeOfWeek == 1 {
  p.hymn sid "oc.m1.d1:ocMA.Lauds1.text" @ver
} else if modeOfWeek == 2 {
  p.hymn sid "oc.m2.d1:ocMA.Lauds1.text" @ver
} else if modeOfWeek == 3 {
  p.hymn sid "oc.m3.d1:ocMA.Lauds1.text" @ver
} else {
  p.hymn sid "oc.m4.d1:ocMA.Lauds1.text" @ver
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
	if err = expectedTKs([]string{"oc.m4.d1:ocMA.Lauds1.text"}, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestIfStatement15a(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
if modeOfWeek == 7  && (dayOfWeek == sun || dayOfWeek == thu )  {
  p.hymn rid "me.*:meMA.Ode1C13.text" @ver  
} else if  modeOfWeek == 2  && dayOfWeek == sun {
  p.hymn rid "me.*:meMA.Ode1C12.text" @ver 
} else {
    p.hymn rid "me.*:meMA.Ode1C11.text" @ver 
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
	expectedSpanCount := 1
	paraIndex := 0
	spanIndex := 0
	if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
		t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
	} else {
		expectTK := "me.m01.d30:meMA.Ode1C13.text"
		got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
		if got != expectTK {
			t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
		}
	}
}
func TestIfStatement15b(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
if modeOfWeek == 1  && (dayOfWeek == sun || dayOfWeek == sat )  {
  p.hymn rid "me.*:meMA.Ode1C13.text" @ver  
} else if  modeOfWeek == 7  && dayOfWeek == thu {
  p.hymn rid "me.*:meMA.Ode1C12.text" @ver 
} else {
    p.hymn rid "me.*:meMA.Ode1C11.text" @ver 
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
	expectedSpanCount := 1
	paraIndex := 0
	spanIndex := 0
	if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
		t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
	} else {
		expectTK := "me.m01.d30:meMA.Ode1C12.text"
		got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
		if got != expectTK {
			t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
		}
	}
}
func TestIfStatement15c(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
if modeOfWeek == 1  && (dayOfWeek == sun || dayOfWeek == sat )  {
  p.hymn rid "me.*:meMA.Ode1C13.text" @ver  
} else if  modeOfWeek == 2  && dayOfWeek == sun {
  p.hymn rid "me.*:meMA.Ode1C12.text" @ver 
} else {
    p.hymn rid "me.*:meMA.Ode1C11.text" @ver 
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
	expectedSpanCount := 1
	paraIndex := 0
	spanIndex := 0
	if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
		t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
	} else {
		expectTK := "me.m01.d30:meMA.Ode1C11.text"
		got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
		if got != expectTK {
			t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
		}
	}
}
func TestIfStatement16a(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
if modeOfWeek == 7 { // true
  p.actor sid "actors:Priest"
} else if modeOfWeek == 2 {
  p.actor sid "actors:Deacon"
} else {
  p.actor sid "actors:People"
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
	expectedSpanCount := 1
	paraIndex := 0
	spanIndex := 0
	if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
		t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
	} else {
		expectTK := "actors:Priest"
		got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
		if got != expectTK {
			t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
		}
	}
}
func TestIfStatement16b(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
if modeOfWeek == 1 {
  p.actor sid "actors:Priest"
} else if modeOfWeek == 7 {
  p.actor sid "actors:Deacon"
} else {
  p.actor sid "actors:People"
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
	expectedSpanCount := 1
	paraIndex := 0
	spanIndex := 0
	if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
		t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
	} else {
		expectTK := "actors:Deacon"
		got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
		if got != expectTK {
			t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
		}
	}
}
func TestIfStatement16c(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
if modeOfWeek == 1 {
  p.actor sid "actors:Priest"
} else if modeOfWeek == 2 {
  p.actor sid "actors:Deacon"
} else {
  p.actor sid "actors:People"
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
	expectedSpanCount := 1
	paraIndex := 0
	spanIndex := 0
	if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
		t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
	} else {
		expectTK := "actors:People"
		got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
		if got != expectTK {
			t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
		}
	}
}
func TestIfStatement17a(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
if dayOfWeek == sun {
	 if modeOfWeek == 1 {
		  p.hymn sid "oc.m1.d1:ocMA.Lauds1.text" @ver
     } 
} else { 
	 p.hymn sid "oc.m2.d1:ocMA.Lauds1.text" @ver
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
	expectTK := "oc.m2.d1:ocMA.Lauds1.text"
	paraIndex := 0
	spanIndex := 0
	got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
	if got != expectTK {
		t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
	}
}
func TestIfStatement17b(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d26/se.m01.d26.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 26
year = 2020
if dayOfWeek == sun { // true
	if modeOfWeek == 1 {
	  p.hymn sid "oc.m1.d1:ocMA.Lauds1.text" @ver
	} 
} else { 
	p.hymn sid "oc.m2.d1:ocMA.Lauds1.text" @ver
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 0
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
}
func TestIfStatement17c(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d26/se.m01.d26.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 26
year = 2020
if dayOfWeek == sun {
	if modeOfWeek == 7 {
		p.hymn sid "oc.m1.d1:ocMA.Lauds1.text" @ver
	} 
} else { 
	 p.hymn sid "oc.m2.d1:ocMA.Lauds1.text" @ver
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
	expectTK := "oc.m1.d1:ocMA.Lauds1.text"
	paraIndex := 0
	spanIndex := 0
	got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
	if got != expectTK {
		t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
	}
}
func TestDate(t *testing.T) {
	i := 0
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li" // used as id in database
type = "service" 
office =  "li" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 6
year = 2022
p.actor sid "actors:Priest"
month = 2 day = 7 year = 2023
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	spans := atem.MetaRows[0].MetaSpans
	if len(spans) == 1 {
	} else {
		t.Errorf("Example %d expected len(spans) to be 1, got %d", i, len(spans))
	}
	expectedMonth := 2
	expectedDay := 7
	expectedYear := 2023

	gotMonth := int(lml.Listener.Ctx.LDP.TheDay.Month())
	gotDay := lml.Listener.Ctx.LDP.TheDay.Day()
	gotYear := lml.Listener.Ctx.LDP.TheDay.Year()

	if gotDay != expectedDay {
		t.Errorf("expected day == %d, got %d", expectedDay, gotDay)
	}
	if gotMonth != expectedMonth {
		t.Errorf("Example %d expected len(spans) to be 1, got %d", expectedMonth, gotMonth)
	}
	if gotYear != expectedYear {
		t.Errorf("expected year == %d, got %d", expectedYear, gotYear)
	}
}
func TestRestoreDate(t *testing.T) {
	i := 0
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li" // used as id in database
type = "service" 
office =  "li" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
day = 6
month = 1
year = 2022
p.actor sid "actors:Priest"
month = 2 day = 7 year = 2023
restoreDate
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	spans := atem.MetaRows[0].MetaSpans
	if len(spans) == 1 {
	} else {
		t.Errorf("Example %d expected len(spans) to be 1, got %d", i, len(spans))
	}
	expectedMonth := 1
	expectedDay := 6
	expectedYear := 2022
	gotMonth := int(lml.Listener.Ctx.LDP.TheDay.Month())
	gotDay := lml.Listener.Ctx.LDP.TheDay.Day()
	gotYear := lml.Listener.Ctx.LDP.TheDay.Year()

	if gotDay != expectedDay {
		t.Errorf("expected day == %d, got %d", expectedDay, gotDay)
	}
	if gotMonth != expectedMonth {
		t.Errorf("Example %d expected len(spans) to be 1, got %d", expectedMonth, gotMonth)
	}
	if gotYear != expectedYear {
		t.Errorf("expected year == %d, got %d", expectedYear, gotYear)
	}
}
func TestSetMcDay(t *testing.T) {
	i := 0
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li" // used as id in database
type = "service" 
office =  "li" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 6
p.actor sid "actors:Priest"
movableCycleDay = 20
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	spans := atem.MetaRows[0].MetaSpans
	if len(spans) == 1 {
	} else {
		t.Errorf("Example %d expected len(spans) to be 1, got %d", i, len(spans))
	}
	expectedMcDay := 20
	gotMcDay := lml.Listener.Ctx.LDP.MovableCycleDay
	if gotMcDay != expectedMcDay {
		t.Errorf("expected movable cycle day == %d, got %d", expectedMcDay, gotMcDay)
	}
}
func TestSetMcDayAndSwitch(t *testing.T) {
	i := 0
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li" // used as id in database
type = "service" 
office =  "li" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 5
day = 6
month = 4
day = 23
movableCycleDay = 72
switch movableCycleDay {
  case 71, 72, 73, 74, 75, 76, 77, 109: {
    p.title nid "case 1"
  }
  case 78 thru 108: {
    p.title nid "case 2"
  }
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	spans := atem.MetaRows[0].MetaSpans
	if len(spans) == 1 {
	} else {
		t.Errorf("Example %d expected len(spans) to be 1, got %d", i, len(spans))
	}
	expectedMcDay := 72
	gotMcDay := lml.Listener.Ctx.LDP.MovableCycleDay
	if gotMcDay != expectedMcDay {
		t.Errorf("expected movable cycle day == %d, got %d", expectedMcDay, gotMcDay)
	}
}
func TestRestoreSetMcDay(t *testing.T) {
	i := 0
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m03.d06.li" // used as id in database
type = "service" 
office =  "li" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 3
day = 6
year = 2022
p.actor sid "actors:Priest"
movableCycleDay = 20
restoreMovableCycleDay
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	ldp, err := ldp2.NewLDPYMD(2022, 3, 6, calendarTypes.Gregorian)
	if err != nil {
		t.Errorf("expected no error: got %v", err)
	}
	expectedMcDay := ldp.MovableCycleDay
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	spans := atem.MetaRows[0].MetaSpans
	if len(spans) == 1 {
	} else {
		t.Errorf("Example %d expected len(spans) to be 1, got %d", i, len(spans))
	}
	gotMcDay := lml.Listener.Ctx.LDP.MovableCycleDay
	if gotMcDay != expectedMcDay {
		t.Errorf("expected movable cycle day == %d, got %d", expectedMcDay, gotMcDay)
	}
}
func TestMedia(t *testing.T) {
	i := 0
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li" // used as id in database
type = "service" 
office =  "li" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 6
media sid "me.m01.d06:meMA.Ode1C13.text"
`
	parms.TemplatePath = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("ages/Dated-CompareServices/m01/d06/se.m01.d06.li", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if len(atem.MetaRows) == 0 {
		t.Errorf("expected MetaRows > 0, but got 0")
		return
	}
	spans := atem.MetaRows[0].MetaSpans
	if len(spans) == 2 {
		row := atem.MetaRows[0]
		span := atem.MetaRows[0].MetaSpans[0]
		expectedClass := "Media"
		expectedTK := "me.m01.d06:meMA.Ode1C13.text"
		if row.Class != expectedClass {
			t.Errorf("expected row.Class == Media, got %s", row.Class)
		}
		if span.TopicKey != expectedTK {
			t.Errorf("expected TK == %s, got %s", expectedTK, span.TopicKey)
		}
	} else {
		t.Errorf("Example %d expected len(spans) to be 2, got %d", i, len(spans))
	}
}
func TestImage(t *testing.T) {
	i := 0
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li" // used as id in database
type = "service" 
office =  "li" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 6
img.logo src "https://olw.ocmc.org/static/media/nikodemos-red-yellow-circle.png" title nid "St. Nikodemos" width "40px" height "40px"
`
	parms.TemplatePath = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("ages/Dated-CompareServices/m01/d06/se.m01.d06.li", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expectedLength := 1
	if len(atem.MetaRows) == expectedLength {
		row := atem.MetaRows[0]
		spans := row.MetaSpans
		if len(spans) == expectedLength {
			expectedClass := "img.logo"
			if spans[0].Class != expectedClass {
				t.Errorf("expected span.Class == %s, got %s", expectedClass, spans[0].Class)
			}
		} else {
			t.Errorf("Example %d expected len(spans) to be %d, got %d", i, expectedLength, len(spans))
		}
	} else {
		t.Errorf("Example %d expected len(Rows) to be %d, got %d", i, expectedLength, len(atem.MetaRows))
	}
}
func TestAnchorInPara(t *testing.T) {
	i := 0
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li" // used as id in database
type = "service" 
office =  "li" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 6
p.hymn rid "me.*:meMA.Ode1C13.text" a.link href nid "https://liml.org/logo.png" target _blank label nid "Logo"
`
	parms.TemplatePath = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("ages/Dated-CompareServices/m01/d06/se.m01.d06.li", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	spans := atem.MetaRows[0].MetaSpans
	expectSpanLength := 2
	if len(spans) == expectSpanLength {
		row := atem.MetaRows[0]
		expectedClass := "hymn"
		if row.Class != expectedClass {
			t.Errorf("expected row.Class == %s, got %s", expectedClass, row.Class)
		}
		if spans[1].Anchor == nil {
			t.Errorf("expected anchor to not be nil")
		} else {
			anchor := spans[1].Anchor
			expectedHREF := "https://liml.org/logo.png"
			if anchor.HREF.Literal != expectedHREF {
				t.Errorf("expected anchor.Href.Literal == %s, got %s", expectedHREF, anchor.HREF.Literal)
			}
			expectedTarget := "_blank"
			if anchor.Target != expectedTarget {
				t.Errorf("expected anchor.Target == %s, got %s", expectedTarget, anchor.Target)
			}
			expectedLabel := "Logo"
			if anchor.Label.Literal != expectedLabel {
				t.Errorf("expected anchor.Label.Literal == %s, got %s", expectedLabel, anchor.Label.Literal)
			}
		}
	} else {
		t.Errorf("Example %d expected len(spans) to be %d, got %d", i, expectSpanLength, len(spans))
	}
}
func TestAnchorInSpan(t *testing.T) {
	i := 0
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li" // used as id in database
type = "service" 
office =  "li" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 6
p.hymn rid "me.*:meMA.Ode1C13.text" span.rubric a.rd href "https://liml.org/logo.png" target "blank"
`
	parms.TemplatePath = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("ages/Dated-CompareServices/m01/d06/se.m01.d06.li", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	if lml == nil {
		t.Errorf("lml is nil")
		return
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	spans := atem.MetaRows[0].MetaSpans
	expectSpanLength := 2
	if len(spans) == expectSpanLength {
		row := atem.MetaRows[0]
		expectedClass := "hymn"
		if row.Class != expectedClass {
			t.Errorf("expected row.Class == %s, got %s", expectedClass, row.Class)
		}
		expectSpanLength = 1
		if len(spans[1].ChildSpans) == expectSpanLength {
			if spans[1].ChildSpans[0].Anchor == nil {
				t.Errorf("expected child anchor span to not be nil")
			} else {
				anchor := spans[1].ChildSpans[0].Anchor
				expectedHREF := "https://liml.org/logo.png"
				if anchor.HREF.Literal != expectedHREF {
					t.Errorf("expected anchor.HREF.Literal == %s, got %s", expectedHREF, anchor.HREF.Literal)
				}
				expectedTarget := "_blank"
				if anchor.Target != expectedTarget {
					t.Errorf("expected anchor.Target == %s, got %s", expectedTarget, anchor.Target)
				}
			}
		} else {
			t.Errorf("Example %d expected len(spans) to be %d, got %d", i, expectSpanLength, len(spans[1].ChildSpans))
		}
	} else {
		t.Errorf("Example %d expected len(spans) to be %d, got %d", i, expectSpanLength, len(spans))
	}
}
func TestBlankLine(t *testing.T) {
	i := 0
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li" // used as id in database
type = "service" 
office =  "li" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 6
p.hymn rid "me.*:meMA.Ode1C13.text" 
br.double
`
	parms.TemplatePath = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("ages/Dated-CompareServices/m01/d06/se.m01.d06.li", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expectedClass := "double"
	expectedType := rowTypes.BlankLine
	expectedRowCount := 2
	if len(atem.MetaRows) == 2 {
		row := atem.MetaRows[1]
		if row.Class != expectedClass {
			t.Errorf("expected row.Class == %s, got %s", expectedClass, row.Class)
		}
		if row.Type != expectedType {
			t.Errorf("expected row.Type == %s, got %s", expectedType, row.Type)
		}
	} else {
		t.Errorf("Example %d expected len(rows) to be %d, got %d", i, expectedRowCount, len(atem.MetaRows))
	}
}
func TestHorizontalRule(t *testing.T) {
	i := 0
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li" // used as id in database
type = "service" 
office =  "li" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 6
p.hymn rid "me.*:meMA.Ode1C13.text" 
hr.double
`
	parms.TemplatePath = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("ages/Dated-CompareServices/m01/d06/se.m01.d06.li", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expectedClass := "double"
	expectedType := rowTypes.HorizontalRule
	expectedRowCount := 2
	if len(atem.MetaRows) == 2 {
		row := atem.MetaRows[1]
		if row.Class != expectedClass {
			t.Errorf("expected row.Class == %s, got %s", expectedClass, row.Class)
		}
		if row.Type != expectedType {
			t.Errorf("expected row.Type == %s, got %s", expectedType, row.Type)
		}
	} else {
		t.Errorf("Example %d expected len(rows) to be %d, got %d", i, expectedRowCount, len(atem.MetaRows))
	}
}
func TestSpanEmbedding0(t *testing.T) {
	i := 0
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li" // used as id in database
type = "service" 
office =  "li" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
p.actor sid "actors:Priest"
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	spans := atem.MetaRows[0].MetaSpans
	if len(spans) == 1 {
	} else {
		t.Errorf("Example %d expected len(spans) to be 1, got %d", i, len(spans))
	}
	if printJson {
		atemJson, _ := json.MarshalIndent(atem.MetaRows, "", " ")
		fmt.Println(string(atemJson))
	}
}
func TestSpanEmbedding1(t *testing.T) {
	i := 1
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li" // used as id in database
type = "service" 
office =  "li" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
p.actor sid "actors:Priest" sid "rubrical:InALowVoice"
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	spans := atem.MetaRows[0].MetaSpans
	if len(spans) == 2 {
	} else {
		t.Errorf("Example %d expected len(spans) to be 2, got %d", i, len(spans))

	}
}
func TestSpanEmbedding2(t *testing.T) {
	i := 2
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li" // used as id in database
type = "service" 
office =  "li" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
p.dialog span.it sid "prayers:res04p" span.it nid "note" @note
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	spans := atem.MetaRows[0].MetaSpans
	expectSpanCount := 2
	if len(spans) == expectSpanCount {
		if spans[1].Type != spanTypes.Note {
			t.Errorf("example %d expected 2nd span type to be Note", i)
		}
	} else {
		t.Errorf("Example %d expected len(spans) to be %d, got %d", i, expectSpanCount, len(spans))
	}
}
func TestSpanEmbedding3(t *testing.T) {
	i := 3
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li" // used as id in database
type = "service" 
office =  "li" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
p.dialog span.it sid "prayers:res04p" span.rubric sid "rubrical:Thrice"
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	spans := atem.MetaRows[0].MetaSpans
	if len(spans) == 2 {
	} else {
		t.Errorf("Example %d expected len(spans) to be 2, got %d", i, len(spans))
	}
}
func TestSpanEmbedding4(t *testing.T) {
	i := 4
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li" // used as id in database
type = "service" 
office =  "li" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
p.actor span.rditbd sid "actors:Deacon" (span.rubric sid "rubrical:InALowVoice") span.bk nid "But, loud enough to be heard."
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	spans := atem.MetaRows[0].MetaSpans
	if len(spans) == 2 {
		if len(spans[0].ChildSpans) != 2 {
			t.Errorf("Example %d expected len(spans[0].ChildSpans) to be 2, got %d", i, len(spans[0].ChildSpans))
		}
		if len(spans[1].ChildSpans) != 1 {
			t.Errorf("Example %d expected len(spans[1].ChildSpans) to be 1, got %d", i, len(spans[1].ChildSpans))
		}
	} else {
		t.Errorf("Example %d expected len(spans) to be 2, got %d", i, len(spans))
	}
}
func TestSpanEmbedding5(t *testing.T) {
	i := 5
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li" // used as id in database
type = "service" 
office =  "li" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
p.actor sid "actors:Deacon" (span.rubric sid "rubrical:InALowVoice") span.bk nid "But, loud enough to be heard."
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	spans := atem.MetaRows[0].MetaSpans
	if len(spans) == 3 {
		j := 0
		expect := 0
		if len(spans[j].ChildSpans) != expect {
			t.Errorf("Example %d expected len(spans[%d].ChildSpans) to be %d, got %d", i, j, expect, len(spans[j].ChildSpans))
		}
		j = 1
		expect = 1
		if len(spans[j].ChildSpans) != expect {
			t.Errorf("Example %d expected len(spans[%d].ChildSpans) to be %d, got %d", i, j, expect, len(spans[j].ChildSpans))
		}
		j = 2
		expect = 1
		if len(spans[j].ChildSpans) != expect {
			t.Errorf("Example %d expected len(spans[%d].ChildSpans) to be %d, got %d", i, j, expect, len(spans[j].ChildSpans))
		}
	} else {
		t.Errorf("Example %d expected len(spans) to be 3, got %d", i, len(spans))
	}
}
func TestSpanEmbedding6(t *testing.T) {
	i := 6
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li" // used as id in database
type = "service" 
office =  "li" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
p.actor sid "actors:Deacon" (span.rubric sid "rubrical:InALowVoice") nid "But, loud enough to be heard."
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	spans := atem.MetaRows[0].MetaSpans
	if len(spans) == 3 {
		j := 0
		expect := 0
		if len(spans[j].ChildSpans) != expect {
			t.Errorf("Example %d expected len(spans[%d].ChildSpans) to be %d, got %d", i, j, expect, len(spans[j].ChildSpans))
		}
		j = 1
		expect = 1
		if len(spans[j].ChildSpans) != expect {
			t.Errorf("Example %d expected len(spans[%d].ChildSpans) to be %d, got %d", i, j, expect, len(spans[j].ChildSpans))
		}
		j = 2
		expect = 0
		if len(spans[j].ChildSpans) != expect {
			t.Errorf("Example %d expected len(spans[%d].ChildSpans) to be %d, got %d", i, j, expect, len(spans[j].ChildSpans))
		}
	} else {
		t.Errorf("Example %d expected len(spans) to be 3, got %d", i, len(spans))
	}
}
func TestSpanEmbedding7(t *testing.T) {
	i := 7
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li" // used as id in database
type = "service" 
office =  "li" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
p.actor sid "actors:Deacon" (span.rubric sid "rubrical:InALowVoice" span.rubric sid "rubrical:Thrice") nid "But, loud enough to be heard."
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	spans := atem.MetaRows[0].MetaSpans
	if len(spans) == 4 {
		j := 0
		expect := 0
		if len(spans[j].ChildSpans) != expect {
			t.Errorf("Example %d expected len(spans[%d].ChildSpans) to be %d, got %d", i, j, expect, len(spans[j].ChildSpans))
		}
		j = 1
		expect = 1
		if len(spans[j].ChildSpans) != expect {
			t.Errorf("Example %d expected len(spans[%d].ChildSpans) to be %d, got %d", i, j, expect, len(spans[j].ChildSpans))
		}
		j = 2
		expect = 1
		if len(spans[j].ChildSpans) != expect {
			t.Errorf("Example %d expected len(spans[%d].ChildSpans) to be %d, got %d", i, j, expect, len(spans[j].ChildSpans))
		}
		j = 3
		expect = 0
		if len(spans[j].ChildSpans) != expect {
			t.Errorf("Example %d expected len(spans[%d].ChildSpans) to be %d, got %d", i, j, expect, len(spans[j].ChildSpans))
		}
	} else {
		t.Errorf("Example %d expected len(spans) to be 4, got %d", i, len(spans))
	}
}
func TestSpanEmbedding8(t *testing.T) {
	i := 8
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li" // used as id in database
type = "service" 
office =  "li" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
p.actor span.it sid "actors:Deacon" (span.rubric sid "rubrical:InALowVoice" span.rubric sid "rubrical:Thrice") nid "But, loud enough to be heard."
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	spans := atem.MetaRows[0].MetaSpans
	if len(spans) == 1 {
		j := 0
		expect := 4
		if len(spans[j].ChildSpans) != expect {
			t.Errorf("Example %d expected len(spans[%d].ChildSpans) to be %d, got %d", i, j, expect, len(spans[j].ChildSpans))
		}
	} else {
		t.Errorf("Example %d expected len(spans) to be 1, got %d", i, len(spans))
	}
}
func TestSpanEmbedding9(t *testing.T) {
	i := 9
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li" // used as id in database
type = "service" 
office =  "li" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
p.actor span.it sid "actors:Deacon" (span.rubric sid "rubrical:InALowVoice"  sid "rubrical:Thrice") nid "But, loud enough to be heard."
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	spans := atem.MetaRows[0].MetaSpans
	if len(spans) == 1 {
		j := 0
		expect := 3
		if len(spans[j].ChildSpans) != expect {
			t.Errorf("Example %d expected len(spans[%d].ChildSpans) to be %d, got %d", i, j, expect, len(spans[j].ChildSpans))
		}
	} else {
		t.Errorf("Example %d expected len(spans) to be 1, got %d", i, len(spans))
	}
}
func TestSpanEmbedding10(t *testing.T) {
	i := 10
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li" // used as id in database
type = "service" 
office =  "li" // types are block, book, or service.
status = "draft" // status values are na, draft, review, or final.
p.actor span.it sid "actors:Deacon" (span.rubric sid "rubrical:InALowVoice" ( span.bk sid "rubrical:Thrice" ) ) nid "But, loud enough to be heard."
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	spans := atem.MetaRows[0].MetaSpans
	if len(spans) == 1 {
		j := 0
		expect := 3
		if len(spans[j].ChildSpans) != expect {
			t.Errorf("Example %d expected len(spans[%d].ChildSpans) to be %d, got %d", i, j, expect, len(spans[j].ChildSpans))
		}
	} else {
		t.Errorf("Example %d expected len(spans) to be 1, got %d", i, len(spans))
	}
	if printJson {
		atemJson, _ := json.MarshalIndent(&atem.MetaRows, "", " ")
		fmt.Println(string(atemJson))
	}
}

// There is no reason for a person to wrap the main span in parentheses, but just in case we will test it.
func TestSpanEmbedding11(t *testing.T) {
	i := 11
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li" // used as id in database
type = "service" 
office =  "li" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
p.actor (span.it sid "actors:Priest")
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	spans := atem.MetaRows[0].MetaSpans
	if len(spans) == 1 {
	} else {
		t.Errorf("Example %d expected len(spans) to be 2, got %d", i, len(spans))

	}
	if printJson {
		atemJson, _ := json.MarshalIndent(&atem.MetaRows, "", " ")
		fmt.Println(string(atemJson))
	}
}

// Again, an unlikely combination, but test anyway
func TestSpanEmbedding12(t *testing.T) {
	i := 12
	input := `
id = "ages/Dated-CompareServices/m01/d06/se.m01.d06.li" // used as id in database
type = "service" 
office =  "li" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
p.actor span.it sid "actors:Deacon" (span.rubric sid "rubrical:InALowVoice" nid "But, loud enough to be heard." ( span.bk sid "rubrical:Thrice" ) ) 
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	spans := atem.MetaRows[0].MetaSpans
	expected := 1
	if len(spans) == expected {
		j := 0
		expected = 2
		if len(spans[j].ChildSpans) == expected {
			k := 0
			expected = 0
			if len(spans[j].ChildSpans[k].ChildSpans) == expected {
			} else {
				t.Errorf("Example %d expected len(spans[%d].ChildSpans[%d].ChildSpans) to be %d, got %d", i, j, k, expected, len(spans[j].ChildSpans[k].ChildSpans))
			}
			k = 1
			expected = 3
			if len(spans[j].ChildSpans[k].ChildSpans) == expected {
				l := 0
				expected = 0
				if len(spans[j].ChildSpans[k].ChildSpans[l].ChildSpans) == expected {
				} else {
					t.Errorf("Example %d expected len(spans[%d].ChildSpans[%d].ChildSpans[%d].ChildSpans) to be %d, got %d", i, j, k, l, expected, len(spans[j].ChildSpans[k].ChildSpans))
				}
				l = 1
				expected = 0
				if len(spans[j].ChildSpans[k].ChildSpans[l].ChildSpans) == expected {
				} else {
					t.Errorf("Example %d expected len(spans[%d].ChildSpans[%d].ChildSpans[%d].ChildSpans) to be %d, got %d", i, j, k, l, expected, len(spans[j].ChildSpans[k].ChildSpans))
				}
				l = 2
				expected = 1
				if len(spans[j].ChildSpans[k].ChildSpans[l].ChildSpans) == expected {
				} else {
					t.Errorf("Example %d expected len(spans[%d].ChildSpans[%d].ChildSpans[%d].ChildSpans) to be %d, got %d", i, j, k, l, expected, len(spans[j].ChildSpans[k].ChildSpans))
				}
			} else {
				t.Errorf("Example %d expected len(spans[%d].ChildSpans[%d].ChildSpans) to be %d, got %d", i, j, k, expected, len(spans[j].ChildSpans[k].ChildSpans))
			}
		} else {
			t.Errorf("Example %d expected len(spans[%d].ChildSpans) to be %d, got %d", i, j, expected, len(spans[j].ChildSpans))
		}
	} else {
		t.Errorf("Example %d expected len(spans) to be 2, got %d", i, len(spans))
	}
	if printJson {
		atemJson, _ := json.MarshalIndent(&atem.MetaRows, "", " ")
		fmt.Println(string(atemJson))
	}
}
func TestLMLExpressiondayOfWeekFalse(t *testing.T) {
	input := `dayOfWeek == sun`
	lml, err := NewLMLExpressionParser(input, 2020, 1, 6, calendarTypes.Gregorian, &mockResolver)
	if err != nil {
		t.Errorf("%v", err)
	}
	_, parseErrors := lml.WalkExpression()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if lml.Listener.Ctx.ExpressionTrue {
		t.Errorf("expected expr %s to be false, but was true", input)
	}
}
func TestLMLExpressiondayOfWeekTrue(t *testing.T) {
	input := `dayOfWeek == mon`
	lml, err := NewLMLExpressionParser(input, 2020, 1, 6, calendarTypes.Gregorian, &mockResolver)
	if err != nil {
		t.Errorf("%v", err)
	}
	_, parseErrors := lml.WalkExpression()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if !lml.Listener.Ctx.ExpressionTrue {
		t.Errorf("expected expr %s to be true, but was false", input)
	}
}
func TestLMLExpressionModeOfWeekTrue(t *testing.T) {
	input := `modeOfWeek == 4`
	lml, err := NewLMLExpressionParser(input, 2020, 1, 6, calendarTypes.Gregorian, &mockResolver)
	if err != nil {
		t.Errorf("%v", err)
	}
	_, parseErrors := lml.WalkExpression()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if !lml.Listener.Ctx.ExpressionTrue {
		t.Errorf("expected expr %s to be true, but was false", input)
	}
}
func TestLMLExpressionComplexTrue(t *testing.T) {
	input := `(modeOfWeek >= 4 || modeOfWeek > 3) && (dayOfWeek < tue || dayOfWeek <= mon) && date == jan 6`
	lml, err := NewLMLExpressionParser(input, 2020, 1, 6, calendarTypes.Gregorian, &mockResolver)
	if err != nil {
		t.Errorf("%v", err)
	}
	_, parseErrors := lml.WalkExpression()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if !lml.Listener.Ctx.ExpressionTrue {
		t.Errorf("expected expr %s to be true, but was false", input)
	}
}
func TestLMLExpressionComplexFalse(t *testing.T) {
	input := `(dayOfWeek >= sun && dayOfWeek == tue) || modeOfWeek <= 3`
	lml, err := NewLMLExpressionParser(input, 2020, 1, 6, calendarTypes.Gregorian, &mockResolver)
	if err != nil {
		t.Errorf("%v", err)
	}
	_, parseErrors := lml.WalkExpression()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := false
	if lml.Listener.Ctx.ExpressionTrue {
		t.Errorf("expected expr %s to be %v, but was %v", input, expect, !expect)
	}
}
func TestSwitchStatement1dayOfWeekAsInt(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// this is a thursday
switch dayOfWeek {
  case 5: { // true
  		p.hymn rid "me.*:meMA.Ode1C13.text" @ver //  expect this
  }
  case 6: { // false
  	p.hymn rid "me.*:meMA.Ode1C12.text" @ver // should not get this
  } 
  default: {
    p.hymn rid "me.*:meMA.Ode1C11.text" @ver // should not get this
  }
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	} else {
		expectedSpanCount := 1
		paraIndex := 0
		spanIndex := 0
		if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
			t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
		} else {
			expectTK := "me.m01.d30:meMA.Ode1C13.text"
			got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
			if got != expectTK {
				t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
			}
		}
	}
}
func TestSwitchStatement1dayOfWeekAsEnum(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// this is a thursday
switch dayOfWeek {
  case mon: { p.hymn sid "actors:Bishop" @ver }
  case thu: { p.hymn sid "actors:Deacon" @ver } // true
  case fri: { p.hymn sid "actors:Priest" @ver }
  default: { p.hymn sid "actors:People" @ver }
}
`
	expectedTks := []string{"actors:Deacon"}
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err := expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestSwitchStatement2dayOfWeekAsIntRange(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// this is a thursday
switch dayOfWeek {
  case 1 thru 5: {
  		p.hymn rid "me.*:meMA.Ode1C13.text" @ver
  }
  case 6: {
  	p.hymn rid "me.*:meMA.Ode1C12.text" @ver
  } 
  default: {
    p.hymn rid "me.*:meMA.Ode1C11.text" @ver
  }
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	} else {
		expectedSpanCount := 1
		paraIndex := 0
		spanIndex := 0
		if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
			t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
		} else {
			expectTK := "me.m01.d30:meMA.Ode1C13.text"
			got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
			if got != expectTK {
				t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
			}
		}
	}
}
func TestSwitchStatement2dayOfWeekAsEnumRange(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// this is a thursday
switch dayOfWeek {
  case sun thru thu: {
  		p.hymn rid "me.*:meMA.Ode1C13.text" @ver
  }
  case fri: {
  	p.hymn rid "me.*:meMA.Ode1C12.text" @ver
  } 
  default: {
    p.hymn rid "me.*:meMA.Ode1C11.text" @ver
  }
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	} else {
		expectedSpanCount := 1
		paraIndex := 0
		spanIndex := 0
		if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
			t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
		} else {
			expectTK := "me.m01.d30:meMA.Ode1C13.text"
			got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
			if got != expectTK {
				t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
			}
		}
	}
}
func TestSwitchStatement3dayOfWeekAsIntList(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// this is a thursday
switch dayOfWeek {
  case 3, 4, 5: {
  		p.hymn rid "me.*:meMA.Ode1C13.text" @ver
  }
  case 6: {
  	p.hymn rid "me.*:meMA.Ode1C12.text" @ver
  } 
  default: {
    p.hymn rid "me.*:meMA.Ode1C11.text" @ver
  }
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	} else {
		expectedSpanCount := 1
		paraIndex := 0
		spanIndex := 0
		if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
			t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
		} else {
			expectTK := "me.m01.d30:meMA.Ode1C13.text"
			got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
			if got != expectTK {
				t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
			}
		}
	}
}
func TestSwitchStatement3dayOfWeekAsEnumList(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 29
year = 2020
//gr_gr_cog/me.m01.d29:meMA.Ode1C12.text
// this is a fri
switch dayOfWeek {
  case tue, wed, fri: {
  		p.hymn rid "me.*:meMA.Ode1C12.text" @ver
  }
  case thu: {
  	p.hymn rid "me.*:meMA.Ode1C13.text" @ver
  } 
  default: {
    p.hymn rid "me.*:meMA.Ode1C11.text" @ver
  }
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	} else {
		expectedSpanCount := 1
		paraIndex := 0
		spanIndex := 0
		if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
			t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
		} else {
			expectTK := "me.m01.d29:meMA.Ode1C12.text"
			got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
			if got != expectTK {
				t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
			}
		}
	}
}
func TestSwitchStatement4ModeOfWeekDiscrete(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// mode is 7
switch modeOfWeek {
  case 6: {
  		p.hymn rid "me.*:meMA.Ode1C12.text" @ver
  }
  case 7: {
  		p.hymn rid "me.*:meMA.Ode1C13.text" @ver
  }
  case 8: {
  	p.hymn rid "me.*:meMA.Ode1C12.text" @ver
  } 
  default: {
    p.hymn rid "me.*:meMA.Ode1C11.text" @ver
  }
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	} else {
		expectedSpanCount := 1
		paraIndex := 0
		spanIndex := 0
		if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
			t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
		} else {
			expectTK := "me.m01.d30:meMA.Ode1C13.text"
			got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
			if got != expectTK {
				t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
			}
		}
	}
}
func TestSwitchStatement4ModeOfWeekThru(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// this is a thursday
switch modeOfWeek {
  case 1 thru 7: {
  		p.hymn rid "me.*:meMA.Ode1C13.text" @ver
  }
  case 8: {
  	p.hymn rid "me.*:meMA.Ode1C12.text" @ver
  } 
  default: {
    p.hymn rid "me.*:meMA.Ode1C11.text" @ver
  }
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	} else {
		expectedSpanCount := 1
		paraIndex := 0
		spanIndex := 0
		if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
			t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
		} else {
			expectTK := "me.m01.d30:meMA.Ode1C13.text"
			got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
			if got != expectTK {
				t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
			}
		}
	}
}
func TestSwitchStatement4ModeOfWeekList(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// this is a thursday
switch modeOfWeek {
  case 4, 5, 6, 7: {
  		p.hymn rid "me.*:meMA.Ode1C13.text" @ver
  }
  case 8: {
  	p.hymn rid "me.*:meMA.Ode1C12.text" @ver
  } 
  default: {
    p.hymn rid "me.*:meMA.Ode1C11.text" @ver
  }
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	} else {
		expectedSpanCount := 1
		paraIndex := 0
		spanIndex := 0
		if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
			t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
		} else {
			expectTK := "me.m01.d30:meMA.Ode1C13.text"
			got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
			if got != expectTK {
				t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
			}
		}
	}
}
func TestSwitchStatement4EmbeddedSwitchStatements(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m07/d06/se.m07.d06.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 7
day = 6
year = 2020
// day is Mon, mode is 3
switch dayOfWeek {
    case sun: { // false
      switch modeOfWeek {
          case 1: { p.title sid "actors:Bishop" } // false
          case 3: { p.title sid "actors:Priest" } // true but exclude
      }
    }
    case mon: { p.title sid "actors:Deacon" } // true
    default: { p.title sid "actors:Choir" } // excluded becase case mon is true
}
if dayOfWeek == sun {
	if modeOfWeek == 1 { p.title sid "actors:Bishop"
    } else if modeOfWeek == 3 { p.title sid "actors:Priest"}
} else if dayOfWeek == mon { p.title sid "actors:Deacon" // true
} else { p.title sid "actors:Choir"}
`
	expectedTks := []string{"actors:Deacon", "actors:Deacon"}
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err := expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestSwitchStatement5Realm(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// this is a thursday
switch realm {
  case "Doxa": {
  		p.hymn rid "me.*:meMA.Ode1C13.text" @ver
  }
  case "AGES": {
  	p.hymn rid "me.*:meMA.Ode1C12.text" @ver
  } 
  default: {
    p.hymn rid "me.*:meMA.Ode1C11.text" @ver
  }
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	} else {
		expectedSpanCount := 1
		paraIndex := 0
		spanIndex := 0
		if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
			t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
		} else {
			expectTK := "me.m01.d30:meMA.Ode1C13.text"
			got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
			if got != expectTK {
				t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
			}
		}
	}
}
func TestSwitchStatement6DateDiscrete(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// this is a thursday
switch date {
  case jan 30: {
  		p.hymn rid "me.*:meMA.Ode1C13.text" @ver
  }
  default: {
    p.hymn rid "me.*:meMA.Ode1C11.text" @ver
  }
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	} else {
		expectedSpanCount := 1
		paraIndex := 0
		spanIndex := 0
		if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
			t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
		} else {
			expectTK := "me.m01.d30:meMA.Ode1C13.text"
			got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
			if got != expectTK {
				t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
			}
		}
	}
}
func TestSwitchStatement6DateThru(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// this is a thursday
switch date {
  case jan 25 thru 31: {
  		p.hymn rid "me.*:meMA.Ode1C13.text" @ver
  }
  default: {
    p.hymn rid "me.*:meMA.Ode1C11.text" @ver
  }
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	} else {
		expectedSpanCount := 1
		paraIndex := 0
		spanIndex := 0
		if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
			t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
		} else {
			expectTK := "me.m01.d30:meMA.Ode1C13.text"
			got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
			if got != expectTK {
				t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
			}
		}
	}
}
func TestSwitchStatement6DateList(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// this is a thursday
switch date {
  case jan 29, 30, 31: {
  		p.hymn rid "me.*:meMA.Ode1C13.text" @ver
  }
  default: {
    p.hymn rid "me.*:meMA.Ode1C11.text" @ver
  }
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	} else {
		expectedSpanCount := 1
		paraIndex := 0
		spanIndex := 0
		if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
			t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
		} else {
			expectTK := "me.m01.d30:meMA.Ode1C13.text"
			got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
			if got != expectTK {
				t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
			}
		}
	}
}
func TestSwitchStatement9EmbeddedIf(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// this is a thursday
switch date {
  case jan 29, 30, 31: {  // true
		if dayOfWeek == mon { // false
  			p.hymn rid "me.*:meMA.Ode1C13.text" @ver // erroneously get this
		} else { // true
	  		p.hymn rid "me.*:meMA.Ode1C12.text" @ver // this is the one we expect
		}
  }
  default: {
    p.hymn rid "me.*:meMA.Ode1C11.text" @ver 
  }
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
		for _, r := range atem.MetaRows {
			t.Errorf("%v\n", r.MetaSpans[0].TopicKey)
		}
	} else {
		expectedSpanCount := 1
		paraIndex := 0
		spanIndex := 0
		if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
			t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
		} else {
			expectTK := "me.m01.d30:meMA.Ode1C12.text"
			got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
			if got != expectTK {
				t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
			}
		}
	}
}
func TestSwitchStatement10EmbeddedIfWithEmbeddedSwitch(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2022
// this is a sunday, mode 7 (grave)
switch date {
  case jan 29, 30, 31: { // true
		if modeOfWeek == 7 { // true
			switch dayOfWeek { 
				case thu: { // false
					if lukanCycleDay == 137 { // false, it is 133
			  			p.hymn rid "me.*:meMA.Ode1C13.text" @ver
					}
                }
				default: {
			  		p.hymn rid "me.*:meMA.Ode1C12.text" @ver // should get this
                }
			}
		}
  }
  default: {
    p.hymn rid "me.*:meMA.Ode1C11.text" @ver
  }
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	} else {
		expectedSpanCount := 1
		paraIndex := 0
		spanIndex := 0
		if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
			t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
		} else {
			expectTK := "me.m01.d30:meMA.Ode1C12.text"
			got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
			if got != expectTK {
				t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
			}
		}
	}
}
func TestSwitchStatement10EmbeddedIfWithEmbeddedIf(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// this is a thursday
switch date {
  case jan 29, 30, 31: {
		if modeOfWeek == 7 {
			if lukanCycleDay == 137 {
				p.hymn rid "me.*:meMA.Ode1C13.text" @ver
            } else if lukanCycleDay == 138 {
				p.hymn rid "me.*:meMA.Ode1C12.text" @ver
			} else {
				p.hymn rid "me.*:meMA.Ode1C11.text" @ver
			}
		}
  }
  default: {
    p.hymn rid "me.*:meMA.Ode1C11.text" @ver
  }
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	} else {
		expectedSpanCount := 1
		paraIndex := 0
		spanIndex := 0
		if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
			t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
		} else {
			expectTK := "me.m01.d30:meMA.Ode1C13.text"
			got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
			if got != expectTK {
				t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
			}
		}
	}
}
func TestSwitchStatement11SwitchWithEmbeddedSwitch(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 3
day = 26
year = 2023
// this is a sunday, movable cycle day 50
switch movableCycleDay {
  case 63, 64: { // false
	p.title nid "1"
  }
  case 71 thru 127: { // false
    switch movableCycleDay {
      case 78, 85, 113: { // false because parent false
		p.title nid "2" 
      }
      case 121: {  // false because parent false
		p.title nid "3"
      }
      case 127: { // false because parent false
		p.title nid "4"
      }
      default: { // false because parent false
		p.title nid "5"
      }
    }
  }
  default: { // true
	p.title nid "6"
  }
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	} else {
		expectedSpanCount := 1
		paraIndex := 0
		spanIndex := 0
		if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
			t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
		} else {
			expectLiteral := "6"
			got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].Literal
			if got != expectLiteral {
				t.Errorf("expected literal %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectLiteral, paraIndex, spanIndex, got)
			}
		}
	}
}

func TestSwitchMovableCycleDayDefault(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m02/d09/se.m02.d09.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 2
day = 9
year = 2020
switch dayOfWeek {
  case mon: { p.hymn nid "!1 should not reach this" }
  default: {
	p.hymn nid "2 should get this"
	switch movableCycleDay  {
	 case 71: { p.hymn nid "!3 should not reach this" }
	 default: {
		p.hymn nid "4 should get this"
		p.hymn nid "5 and should get this"
     }
	}
	p.hymn nid "6 and should get this"
    }
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 4
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	}
}

func TestSwitchMovableCycleDayDefault2(t *testing.T) {
	input := `
id = "ages/Dated-Services/m02/d09/se.m02.d09.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 9
day = 27 // lcd d010
year = 2023
switch movableCycleDay {
  case 1 thru 127: { // false    
		p.hymn nid "1"
  }
  default: {
    switch exists { // true
      case rid "le.go.lu.*:leluLI.Gospel.text": {
		p.hymn nid "2"
      }
      default: { // false
		p.hymn nid "3"
      }
    }
  }
}
`
	printLineNumbers(input)
	expectRowCount := 1
	expectedSpanCount := 1
	expectLiteral := "2"
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if len(atem.MetaRows) != expectRowCount {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expectRowCount, len(atem.MetaRows))
	} else {
		paraIndex := 0
		spanIndex := 0
		if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
			t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
		} else {
			got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].Literal
			if got != expectLiteral {
				t.Errorf("expected literal %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectLiteral, paraIndex, spanIndex, got)
			}
		}
	}
}
func printLineNumbers(s string) {
	parts := strings.Split(s, "\n")
	for i, p := range parts {
		fmt.Printf("%03d %s\n", i+1, p)
	}
}
func TestSwitchDefaultFalse(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m02/d09/se.m02.d09.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 2
day = 9
year = 2020
// Sunday, Mode 1
switch dayOfWeek { 
    case sun: {// is true
        if exists rid "me.*:meDismissal" {  // is false
			p.hymn sid "actors:Priest"
        } else { // include next
			p.hymn sid "actors:Deacon"
        }	
	}
    default: { // should not reach
        if exists rid "me.*:meDismissal" {
			p.hymn sid "actors:Choir"
        } else {
			p.hymn sid "actors:People"
        }
	}
}
`
	expectedTks := []string{"actors:Deacon"}
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err := expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestSwitchDOWExistsRidDefaultTrue(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m02/d09/se.m02.d09.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 5
day = 21
year = 2023
// Movable Cycle Day 106, Sunday, Rid Not Exists
switch movableCycleDay {
  case 78, 85: {
	p.title nid "t1"
  }
  case 113: {
	p.title nid "t2"
  }
  default: {
    switch dayOfWeek {
      case sun: {
        switch exists {
          case rid "me.*:meDA.dismissal": { // false
            p.actor sid "actors:Priest"
			p.verse rid "me.*:meMA.Ode1C13.text"
          }
          default: {
            p.actor sid "actors:Deacon"
          }
        }
      }
      default: {
     	p.title nid "t3"
      }
    }
  }
}
`
	printLineNumbers(input)
	expectedTks := []string{"actors:Deacon"}
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err := expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestSwitchExistsRidDefaultTrue(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m02/d09/se.m02.d09.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 7
day = 2
year = 2023
// Movable Cycle Day 106, Sunday, Rid Not Exists
switch movableCycleDay {
  case 1 thru 127: {
  	     p.title nid "t1"
  }
  default: {
    switch exists {
      case rid "le.go.lu.*:leluLI.Gospel.text": {
  	     p.title nid "t2"
      }
      default: {
        p.title rid "le.go.mc.*:lemcLI.Gospel.text" @ver
      }
    }
  }
}
`
	expectedTks := []string{"le.go.mc.d148:lemcLI.Gospel.text"}
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err := expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func showTopicKeys(rows []*atempl.MetaRow) {
	for _, r := range rows {
		for _, s := range r.MetaSpans {
			if s.TopicKey == "" {
				for _, c := range s.ChildSpans {
					fmt.Println(c.TopicKey)
				}
			} else {
				fmt.Println(s.TopicKey)
			}
		}
	}
}
func TestIfEmbeddedIf(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// this is a thursday
if dayOfWeek != mon { // a silly case, but works for checking If within If
	if dayOfWeek == thu {
		p.hymn rid "me.*:meMA.Ode1C13.text" @ver
	} else {
		p.hymn rid "me.*:meMA.Ode1C12.text" @ver
	}
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 1
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	} else {
		expectedSpanCount := 1
		paraIndex := 0
		spanIndex := 0
		if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
			t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
		} else {
			expectTK := "me.m01.d30:meMA.Ode1C13.text"
			got := atem.MetaRows[paraIndex].MetaSpans[spanIndex].TopicKey
			if got != expectTK {
				t.Errorf("expected topic/key %s  in paragraphs[%d].spans[%d] in ATEM, got %s", expectTK, paraIndex, spanIndex, got)
			}
		}
	}
}
func TestIfEmbeddedIf2(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2022
// this is a sunday mode 7
if dayOfWeek == sun { // true
	if modeOfWeek == 7 { // true
		p.hymn rid "me.*:meMA.Ode1C11.text" @ver // <-- expect this
	} else if modeOfWeek == 8 { // false
		p.hymn rid "me.*:meMA.Ode1C12.text" @ver
	} else { // false
		p.hymn rid "me.*:meMA.Ode1C13.text" @ver
	}
}
`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expectTopicKey := "me.m01.d30:meMA.Ode1C11.text"
	gotTopicKey := ""
	expectLenMetaRows := 1
	if len(atem.MetaRows) == expectLenMetaRows {
		gotTopicKey = atem.MetaRows[0].MetaSpans[0].TopicKey
		if expectTopicKey != gotTopicKey {
			t.Errorf("expected %s got %s\n", expectTopicKey, gotTopicKey)
		}
	} else {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expectLenMetaRows, len(atem.MetaRows))
	}
}
func TestIfStatement100(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// Thursday, Mode 7
p.hymn sid "actors:Priest"
if dayOfWeek == thu { // true
  p.hymn sid "actors:Deacon"
}
p.hymn sid "actors:People"
`
	expectedTks := []string{"actors:Priest", "actors:Deacon", "actors:People"}

	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err := expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestIfElseStatement101(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// Thursday, Mode 7
p.hymn sid "actors:Priest"
if dayOfWeek == thu { // true
  p.hymn sid "actors:Deacon"
} else {
  p.hymn sid "actors:Choir"
}
p.hymn sid "actors:People"
`
	expectedTks := []string{"actors:Priest", "actors:Deacon", "actors:People"}

	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err := expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestIfElseStatement102(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// Thursday, Mode 7
p.hymn sid "actors:Priest"
if dayOfWeek == mon { // false
  p.hymn sid "actors:Deacon"
} else {
  p.hymn sid "actors:Choir"
}
p.hymn sid "actors:People"
`
	expectedTks := []string{"actors:Priest", "actors:Choir", "actors:People"}

	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err := expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestIfElseStatement103(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// Thursday, Mode 7
p.hymn sid "actors:Priest"
if dayOfWeek == mon { // false
  p.hymn sid "actors:Deacon"
} else if dayOfWeek == thu {
  p.hymn sid "actors:Bishop"
} else {
  p.hymn sid "actors:Choir"
}
p.hymn sid "actors:People"
`
	expectedTks := []string{"actors:Priest", "actors:Bishop", "actors:People"}

	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err := expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestIfElseStatement104(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// Thursday, Mode 7
p.hymn sid "actors:Priest"
if dayOfWeek == mon { // false
  p.hymn sid "actors:Deacon"
} else if dayOfWeek == tue {
  p.hymn sid "actors:Bishop"
} else {
  p.hymn sid "actors:Choir"
}
p.hymn sid "actors:People"
`
	expectedTks := []string{"actors:Priest", "actors:Choir", "actors:People"}

	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err := expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestIfElseStatement105(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// Thursday, Mode 7
p.hymn sid "actors:Priest"
if dayOfWeek == mon { // false
  p.hymn sid "actors:Deacon"
} else if dayOfWeek == tue {
  p.hymn sid "actors:Bishop"
} else {
  if modeOfWeek == 7 {
    p.hymn sid "actors:Choir"
  }
}
p.hymn sid "actors:People"
`
	expectedTks := []string{"actors:Priest", "actors:Choir", "actors:People"}

	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err := expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestIfElseStatement106(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// Thursday, Mode 7
p.hymn sid "actors:Priest"
if dayOfWeek == mon { // false
  p.hymn sid "actors:Deacon"
} else if dayOfWeek == tue {
  p.hymn sid "actors:Bishop"
} else {
  if modeOfWeek == 7 {
    p.hymn sid "actors:Choir"
  } else {
    p.hymn sid "actors:Bishop"
  }
}
p.hymn sid "actors:People"
`
	expectedTks := []string{"actors:Priest", "actors:Choir", "actors:People"}

	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err := expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestIfElseStatement107(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// Thursday, Mode 7
p.hymn sid "actors:Priest"
if dayOfWeek == mon { // false
  p.hymn sid "actors:Deacon"
} else if dayOfWeek == tue {
  p.hymn sid "actors:Bishop"
} else {
  if modeOfWeek == 1 {
    p.hymn sid "actors:Choir"
  } else {
    p.hymn sid "actors:Bishop"
  }
  p.hymn sid "actors:Priest"
}
p.hymn sid "actors:People"
`
	expectedTks := []string{"actors:Priest", "actors:Bishop", "actors:Priest", "actors:People"}

	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err := expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestIfElseStatement108(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// Thursday, Mode 7
p.hymn sid "actors:Priest"
if dayOfWeek == mon { // false
  p.hymn sid "actors:Deacon"
} else if dayOfWeek == thu { // true
  if modeOfWeek == 1 { // false
    p.hymn sid "actors:Choir"
  } else { // true
    p.hymn sid "actors:Priest"
  }
  p.hymn sid "actors:Bishop"
} else { // false
  if modeOfWeek == 1 {
    p.hymn sid "actors:Choir"
  } else {
    p.hymn sid "actors:Bishop"
  }
  p.hymn sid "actors:Priest"
}
p.hymn sid "actors:People"
`
	expectedTks := []string{"actors:Priest", "actors:Priest", "actors:Bishop", "actors:People"}

	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err := expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestIfElseStatement109(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// Thursday, Mode 7
p.hymn sid "actors:Priest"
if dayOfWeek == mon { // false
  p.hymn sid "actors:Deacon"
  if modeOfWeek == 1 { // false
    p.hymn sid "actors:Choir"
  } else { // true
    p.hymn sid "actors:Priest"
  }
} else if dayOfWeek == thu { // true
  if modeOfWeek == 1 { // false
    p.hymn sid "actors:Choir"
  } else { // true
    if modeOfWeek == 7 {
      p.hymn sid "actors:Priest"
    }
    p.hymn sid "actors:People"
  }
  p.hymn sid "actors:Bishop"
} else { // false
  if modeOfWeek == 1 {
    p.hymn sid "actors:Choir"
  } else {
    p.hymn sid "actors:Bishop"
  }
  p.hymn sid "actors:Priest"
}
p.hymn sid "actors:People"
`
	expectedTks := []string{"actors:Priest", "actors:Priest", "actors:People", "actors:Bishop", "actors:People"}

	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err := expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestIfElseStatement110(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
if modeOfWeek == 7 { // true
  p.hymn sid "actors:Priest"  
} else if  modeOfWeek == 2 {
  p.hymn sid "actors:Deacon"  
} else {
  p.hymn sid "actors:Choir"  
}
`
	expectedTks := []string{"actors:Priest"}
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err := expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestSwitchStatement100(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// Thursday, Mode 7
p.hymn sid "actors:Priest"
switch dayOfWeek {
  case thu : { p.hymn sid "actors:Deacon" } // true
}
p.hymn sid "actors:People"
`
	expectedTks := []string{"actors:Priest", "actors:Deacon", "actors:People"}

	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err := expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestSwitchStatement101(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// Thursday, Mode 7
p.hymn sid "actors:Priest"
switch dayOfWeek {
  case thu : { p.hymn sid "actors:Deacon" } // true
  default: { p.hymn sid "actors:Bishop" } // false
}
p.hymn sid "actors:People"
`
	expectedTks := []string{"actors:Priest", "actors:Deacon", "actors:People"}

	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err := expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestSwitchStatement102(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// Thursday, Mode 7
p.hymn sid "actors:Priest"
switch dayOfWeek {
  case mon : { p.hymn sid "actors:Deacon" } // false
  default: { p.hymn sid "actors:Bishop" } // true
}
p.hymn sid "actors:People"
`
	expectedTks := []string{"actors:Priest", "actors:Bishop", "actors:People"}

	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err := expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestSwitchStatement103(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// Thursday, Mode 7
p.hymn sid "actors:Priest"
switch dayOfWeek {
  case mon : { p.hymn sid "actors:Deacon" } // false
  default: {
    if modeOfWeek == 7 {
      p.hymn sid "actors:Choir" // true
    } else {
      p.hymn sid "actors:Bishop" // false
    }
  }
}
p.hymn sid "actors:People"
`
	expectedTks := []string{"actors:Priest", "actors:Choir", "actors:People"}

	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err := expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestSwitchStatement104(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// Thursday, Mode 7
p.hymn sid "actors:Priest"
switch dayOfWeek {
  case mon : { p.hymn sid "actors:Deacon" } // false
  default: {
    switch modeOfWeek {
      case 7: { p.hymn sid "actors:Choir" } // true
      default: { p.hymn sid "actors:Bishop" } // false
    }
  }
}
p.hymn sid "actors:People"
`
	expectedTks := []string{"actors:Priest", "actors:Choir", "actors:People"}

	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err := expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestSwitchStatement105(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// Thursday, Mode 7
p.hymn sid "actors:Priest"
switch dayOfWeek {
  case mon : { p.hymn sid "actors:Deacon" } // false
  default: {
    switch modeOfWeek {
      case 1: { p.hymn sid "actors:Choir" } // false
      default: { p.hymn sid "actors:Bishop" } // true
    }
  }
}
p.hymn sid "actors:People"
`
	expectedTks := []string{"actors:Priest", "actors:Bishop", "actors:People"}

	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err := expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestSwitchStatement106(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// Thursday, Mode 7
p.hymn sid "actors:Priest"
switch dayOfWeek {
  case mon : { p.hymn sid "actors:Deacon" } // false
  default: {
    switch modeOfWeek {
      case 1: { p.hymn sid "actors:Choir" } // false
      default: { p.hymn sid "actors:Bishop" } // true
    }
  }
}
p.hymn sid "actors:People"
`
	expectedTks := []string{"actors:Priest", "actors:Bishop", "actors:People"}

	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err := expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestSwitchStatement107(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// Thursday, Mode 7
p.hymn sid "actors:Priest"
switch dayOfWeek {
  case thu :  { // true 
    p.hymn sid "actors:Priest"
    if modeOfWeek == 7 {
      p.hymn sid "actors:Choir" // true
    } else {
      p.hymn sid "actors:Bishop" // false
    }
    p.hymn sid "actors:Priest"
  }
  default: {
    p.hymn sid "actors:Deacon" // false
  }
}
p.hymn sid "actors:People"
`
	expectedTks := []string{"actors:Priest", "actors:Priest", "actors:Choir", "actors:Priest", "actors:People"}

	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err = expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestBlocks(t *testing.T) {
	input := `
id = "ages/Dated-CompareServices/m01/d30/se.m01.d30.ma" // used as id in database
type = "service" 
office =  "ma" // types are block, book, or service. 
status = "draft" // status values are na, draft, review, or final. 
month = 1
day = 30
year = 2020
// Thursday, Mode 7
// Section 1
{
	p.hymn sid "actors:Priest"
	p.hymn sid "actors:Priest"
}
// section 2
{
	p.hymn sid "actors:Choir" // true
	p.hymn sid "actors:Bishop" // false
}
// section 3
{
	p.hymn sid "actors:Priest"
	p.hymn sid "actors:Deacon" // false
	p.hymn sid "actors:People"
}
`
	expectedTks := []string{"actors:Priest", "actors:Priest", "actors:Choir", "actors:Bishop", "actors:Priest", "actors:Deacon", "actors:People"}

	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	if err = expectedTKs(expectedTks, atem.MetaRows); err != nil {
		t.Errorf("%v", err)
	}
}
func TestRidiculouslyHugeSwitch(t *testing.T) {
	y := 2023
	mi := 9
	mj := 13
	di := 1
	dj := 30

	for m := mi; m < mj; m++ {
		for d := di; d < dj; d++ {
			input := hugeSwitch(y, m, d)
			parms.TemplatePath = "a/bl.b"
			parms.TemplateContent = input
			lml, err := NewLMLParser(&parms)
			// 			lml, err := NewLMLParser("a/bl.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
			if err != nil {
				t.Errorf("%s", err)
			}
			ldp, err := ldp2.NewLDPYMD(y, m, d, calendarTypes.Gregorian)
			if err != nil {
				t.Error(err)
				return
			}
			lml.Listener.Ctx.LDP = &ldp
			atem, parseErrors := lml.WalkTemplate()
			if len(parseErrors) > 0 {
				t.Errorf("expected no parser errors, got %d", len(parseErrors))
				for _, parseError := range parseErrors {
					t.Error(parseError.StringVerbose())
				}
			}
			expect := 1
			if len(atem.MetaRows) != expect {
				t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
			} else {
				ldp := lml.Listener.Ctx.LDP
				fmt.Printf("\nSAEC: %s Date: %s, %s-%s LCD: %d Line: %d TK: %s\n", ldp.ElevationOfCrossDateThisYear, ldp.DayOfWeek, ldp.NbrMonth, ldp.NbrDayOfMonth, ldp.LukanCycleDay, atem.MetaRows[0].TemplateLine-3, atem.MetaRows[0].MetaSpans[0].Literal)
			}
		}
	}
}
func TestMisc(t *testing.T) {
	input := `
id = "tests/bl.test.lml"
type = "block"
status = "draft"

switch date {
  case apr 1 thru 3: {
    p.title nid "dude"
  }
}`
	parms.TemplatePath = "tests/bl.test.lml"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("tests/bl.test.lml", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	_, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
}
func TestLMLListener_EnterLdpType(t *testing.T) {
	input := `
id = "tests/se.m04.d07.li"
type = "service"
status = "draft"
year = 2023
month = 4
day = 6 // a Thursday
p.title ldp @allLiturgicalDayProperties
p.title ldp @lukanCycleElapsedDays
p.title ldp @dayOfMonth
p.title ldp @dayOfWeekAsNumber  
p.title ldp @dayOfWeekAsText
p.title ldp @eothinon
p.title ldp @serviceDate 
p.title ldp @serviceYear 
p.title ldp @dayOfMovableCycle 
p.title ldp @modeOfWeek
p.title ldp @nameOfPeriod 
p.title ldp @sundayAfterElevationCrossDate
p.title ldp @sundaysBeforeTriodion
p.title ldp @lukanCycleStartDate
p.title ldp @lukanCycleWeekAndDay
p.title ldp @lukanCycleWeek

`
	parms.TemplatePath = "a/se.b"
	parms.TemplateContent = input
	lml, err := NewLMLParser(&parms)
	// 	lml, err := NewLMLParser("a/se.b", input, realm, versions.All, genLangs, &mockResolver, nil, nil, nil, valuemap.NewValueMapper(), valuemap.NewValueMapper(), htmlCss, pdfCss, calendarTypes.Gregorian, nil, "", 0, true, false)
	if err != nil {
		t.Errorf("%s", err)
	}
	atem, parseErrors := lml.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("expected no parser errors, got %d", len(parseErrors))
		for _, parseError := range parseErrors {
			t.Error(parseError.StringVerbose())
		}
	}
	expect := 16
	if len(atem.MetaRows) != expect {
		t.Errorf("expected %d paragraph(s) in ATEM, got %d", expect, len(atem.MetaRows))
	} else {
		expectedSpanCount := 1
		paraIndex := 0
		if len(atem.MetaRows[paraIndex].MetaSpans) != expectedSpanCount {
			t.Errorf("expected %d span(s) in ATEM, got %d", expectedSpanCount, len(atem.MetaRows[paraIndex].MetaSpans))
		} else {
			var expectCv []string
			var name string
			for i, row := range atem.MetaRows {
				switch i {
				case 0: // @allLiturgicalDayProperties
					name = "@allLiturgicalDayProperties"
					expectCv = []string{"The liturgical properties for Thursday, April 6, 2023 are: Title: Thursday of the 29th week of Luke, Lukan cycle day 200 Mode: 1 Movable Cycle Day: 61", ""}
				case 1: // @lukanCycleElapsedDays
					name = "@lukanCycleElapsedDays"
					expectCv = []string{"200", ""}
				case 2: // @dayOfMonth
					name = "@dayOfMonth"
					expectCv = []string{"06", ""}
				case 3: // @dayOfWeekAsNumber
					name = "@dayOfWeekAsNumber"
					expectCv = []string{"5", ""}
				case 4: // @dayOfWeekAsText
					name = "@dayOfWeekAsText"
					expectCv = []string{"Thursday", ""}
				case 5: // @eothinon
					name = "@eothinon"
					expectCv = []string{"0", ""}
				case 6: // @serviceDate
					name = "@serviceDate"
					expectCv = []string{"Thursday, April 06, 2023", ""}
				case 7: // @serviceYear
					name = "@serviceYear"
					expectCv = []string{"2023", ""}
				case 8: // @dayOfMovableCycle
					name = "@dayOfMovableCycle"
					expectCv = []string{"61", ""}
				case 9: // @modeOfWeek
					name = "@modeOfWeek"
					expectCv = []string{"1", ""}
				case 10: // @nameOfPeriod
					name = "@nameOfPeriod"
					expectCv = []string{"Triodion", ""}
				case 11: // @sundayAfterElevationCrossDate
					name = "@sundayAfterElevationCrossDate"
					expectCv = []string{"Sunday, September 18, 2022", ""}
				case 12: // @sundaysBeforeTriodion
					name = "@sundaysBeforeTriodion"
					expectCv = []string{"3", ""}
				case 13: // @lukanCycleStartDate
					name = "@lukanCycleStartDate"
					expectCv = []string{"Monday, September 19, 2022", ""}
				case 14: // @lukanCycleWeekDay
					name = "@lukanCycleWeekAndDay"
					expectCv = []string{"Thursday, week 29 of Luke", ""}
				case 15: // @lukanCycleWeek
					name = "@lukanCycleWeek"
					expectCv = []string{"29", ""}
				default:
					t.Errorf("did not expect atem.MetaRows index = %d", i)
					return
				}
				err = checkSpans(name, expectCv, i, row.MetaSpans)
				if err != nil {
					t.Error(err)
				}
			}
		}
	}
}
func checkSpans(name string, expect []string, i int, spans []*atempl.MetaSpan) error {
	for j, span := range spans {
		var gotCv string
		if len(span.Literal) > 0 {
			gotCv = span.Literal
		} else {
			if span.ValueIndexes != nil {
				if len(span.ValueIndexes.Values) > 0 {
					if len(span.ValueIndexes.Values[0].LiturgicalLibs) > 0 {
						gotCv = span.ValueIndexes.Values[0].LiturgicalLibs[0].Value
					}
				}
			}
		}
		r := strings.NewReplacer("\t", "", "\n", "")
		gotCv = r.Replace(gotCv)
		if gotCv != expect[j] {
			return fmt.Errorf("atem.MetaRows[%d].MetaSpans[%d] %s\n\texpected:\n\t\t%s\n\tgot\n\t\t%s", i, j, name, expect[j], gotCv)
		}
	}
	return nil
}
func hugeSwitch(y, m, d int) string {
	return fmt.Sprintf("%syear = %d\nmonth = %d\nday = %d\n%s", hugeSwitchFore, y, m, d, hugeSwitchAft)
}

const hugeSwitchFore = `id = "a/bl.b"
type = "block"
status = "draft"
`
const hugeSwitchAft = `switch lukanCycleDay {
  case 7: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        p.title nid "11 p.chapverse sid le.go.lu.d007:leluLI.Gospel.book_abbr sid le.go.lu.d007:leluLI.Gospel.chapverse"
      }
      case sep 16: {
        p.title nid "14 p.chapverse sid le.go.lu.d007:leluLI.Gospel.book_abbr sid le.go.lu.d007:leluLI.Gospel.chapverse"
      }
      case sep 17: {
        p.title nid "17 p.chapverse sid le.go.lu.d007:leluLI.Gospel.book_abbr sid le.go.lu.d007:leluLI.Gospel.chapverse"
      }
      case sep 18: {
        p.title nid "20 p.chapverse sid le.go.lu.d007:leluLI.Gospel.book_abbr sid le.go.lu.d007:leluLI.Gospel.chapverse"
      }
      case sep 19: {
        p.title nid "23 p.chapverse sid le.go.me.m09.d26:lemeLI.Gospel.book_abbr sid le.go.me.m09.d26:lemeLI.Gospel.chapverse"
      }
      case sep 20: {
        p.title nid "26 p.chapverse sid le.go.lu.d007:leluLI.Gospel.book_abbr sid le.go.lu.d007:leluLI.Gospel.chapverse"
      }
      case sep 21: {
        p.title nid "29 p.chapverse sid le.go.lu.d007:leluLI.Gospel.book_abbr sid le.go.lu.d007:leluLI.Gospel.chapverse"
      }
    }
  }
  case 14: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        p.title nid "36 p.chapverse sid le.go.lu.d014:leluLI.Gospel.book_abbr sid le.go.lu.d014:leluLI.Gospel.chapverse"
      }
      case sep 16: {
        p.title nid "39 p.chapverse sid le.go.lu.d014:leluLI.Gospel.book_abbr sid le.go.lu.d014:leluLI.Gospel.chapverse"
      }
      case sep 17: {
        p.title nid "42 p.chapverse sid le.go.lu.d014:leluLI.Gospel.book_abbr sid le.go.lu.d014:leluLI.Gospel.chapverse"
      }
      case sep 18: {
        p.title nid "45 p.chapverse sid le.go.lu.d014:leluLI.Gospel.book_abbr sid le.go.lu.d014:leluLI.Gospel.chapverse"
      }
      case sep 19: {
        p.title nid "48 p.chapverse sid le.go.lu.d014:leluLI.Gospel.book_abbr sid le.go.lu.d014:leluLI.Gospel.chapverse"
      }
      case sep 20: {
        p.title nid "51 p.chapverse sid le.go.lu.d014:leluLI.Gospel.book_abbr sid le.go.lu.d014:leluLI.Gospel.chapverse"
      }
      case sep 21: {
        p.title nid "54 p.chapverse sid le.go.lu.d014:leluLI.Gospel.book_abbr sid le.go.lu.d014:leluLI.Gospel.chapverse"
      }
    }
  }
  case 21: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        p.title nid "61 p.chapverse sid le.go.lu.d021:leluLI.Gospel.book_abbr sid le.go.lu.d021:leluLI.Gospel.chapverse"
      }
      case sep 16: {
        p.title nid "64 p.chapverse sid le.go.lu.d021:leluLI.Gospel.book_abbr sid le.go.lu.d021:leluLI.Gospel.chapverse"
      }
      case sep 17: {
        p.title nid "67 p.chapverse sid le.go.lu.d021:leluLI.Gospel.book_abbr sid le.go.lu.d021:leluLI.Gospel.chapverse"
      }
      case sep 18: {
        p.title nid "70 p.chapverse sid le.go.lu.d021:leluLI.Gospel.book_abbr sid le.go.lu.d021:leluLI.Gospel.chapverse"
      }
      case sep 19: {
        p.title nid "73 p.chapverse sid le.go.lu.d021:leluLI.Gospel.book_abbr sid le.go.lu.d021:leluLI.Gospel.chapverse"
      }
      case sep 20: {
        p.title nid "76 p.chapverse sid le.go.lu.d028:leluLI.Gospel.book_abbr sid le.go.lu.d028:leluLI.Gospel.chapverse"
      }
      case sep 21: {
        p.title nid "79 p.chapverse sid le.go.lu.d028:leluLI.Gospel.book_abbr sid le.go.lu.d028:leluLI.Gospel.chapverse"
      }
    }
  }
  case 28: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        p.title nid "86 p.chapverse sid le.go.lu.d028:leluLI.Gospel.book_abbr sid le.go.lu.d028:leluLI.Gospel.chapverse"
      }
      case sep 16: {
        p.title nid "89 p.chapverse sid le.go.lu.d028:leluLI.Gospel.book_abbr sid le.go.lu.d028:leluLI.Gospel.chapverse"
      }
      case sep 17: {
        p.title nid "92 p.chapverse sid le.go.lu.d028:leluLI.Gospel.book_abbr sid le.go.lu.d028:leluLI.Gospel.chapverse"
      }
      case sep 18: {
        p.title nid "95 p.chapverse sid le.go.lu.d028:leluLI.Gospel.book_abbr sid le.go.lu.d028:leluLI.Gospel.chapverse"
      }
      case sep 19: {
        p.title nid "98 p.chapverse sid le.go.lu.d028:leluLI.Gospel.book_abbr sid le.go.lu.d028:leluLI.Gospel.chapverse"
      }
      case sep 20: {
        p.title nid "101 p.chapverse sid le.go.me.m10.d18:lemeLI.Gospel.book_abbr sid le.go.me.m10.d18:lemeLI.Gospel.chapverse"
      }
      case sep 21: {
        p.title nid "104 p.chapverse sid le.go.lu.d021:leluLI.Gospel.book_abbr sid le.go.lu.d021:leluLI.Gospel.chapverse"
      }
    }
  }
  case 35: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        p.title nid "111 p.chapverse sid le.go.lu.d042:leluLI.Gospel.book_abbr sid le.go.lu.d042:leluLI.Gospel.chapverse"
      }
      case sep 16: {
        p.title nid "114 p.chapverse sid le.go.lu.d042:leluLI.Gospel.book_abbr sid le.go.lu.d042:leluLI.Gospel.chapverse"
      }
      case sep 17: {
        p.title nid "117 p.chapverse sid le.go.lu.d042:leluLI.Gospel.book_abbr sid le.go.lu.d042:leluLI.Gospel.chapverse"
      }
      case sep 18: {
        p.title nid "120 p.chapverse sid le.go.lu.d042:leluLI.Gospel.book_abbr sid le.go.lu.d042:leluLI.Gospel.chapverse"
      }
      case sep 19: {
        p.title nid "123 p.chapverse sid le.go.lu.d042:leluLI.Gospel.book_abbr sid le.go.lu.d042:leluLI.Gospel.chapverse"
      }
      case sep 20: {
        p.title nid "126 p.chapverse sid le.go.lu.d042:leluLI.Gospel.book_abbr sid le.go.lu.d042:leluLI.Gospel.chapverse"
      }
      case sep 21: {
        p.title nid "129 p.chapverse sid le.go.lu.d042:leluLI.Gospel.book_abbr sid le.go.lu.d042:leluLI.Gospel.chapverse"
      }
    }
  }
  case 42: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        p.title nid "136 p.chapverse sid le.go.lu.d049:leluLI.Gospel.book_abbr sid le.go.lu.d049:leluLI.Gospel.chapverse"
      }
      case sep 16: {
        p.title nid "139 p.chapverse sid le.go.lu.d049:leluLI.Gospel.book_abbr sid le.go.lu.d049:leluLI.Gospel.chapverse"
      }
      case sep 17: {
        p.title nid "142 p.chapverse sid le.go.lu.d049:leluLI.Gospel.book_abbr sid le.go.lu.d049:leluLI.Gospel.chapverse"
      }
      case sep 18: {
        p.title nid "145 p.chapverse sid le.go.lu.d035:leluLI.Gospel.book_abbr sid le.go.lu.d035:leluLI.Gospel.chapverse"
      }
      case sep 19: {
        p.title nid "148 p.chapverse sid le.go.lu.d035:leluLI.Gospel.book_abbr sid le.go.lu.d035:leluLI.Gospel.chapverse"
      }
      case sep 20: {
        p.title nid "151 p.chapverse sid le.go.lu.d035:leluLI.Gospel.book_abbr sid le.go.lu.d035:leluLI.Gospel.chapverse"
      }
      case sep 21: {
        p.title nid "154 p.chapverse sid le.go.lu.d035:leluLI.Gospel.book_abbr sid le.go.lu.d035:leluLI.Gospel.chapverse"
      }
    }
  }
  case 49: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        p.title nid "161 p.chapverse sid le.go.lu.d035:leluLI.Gospel.book_abbr sid le.go.lu.d035:leluLI.Gospel.chapverse"
      }
      case sep 16: {
        p.title nid "164 p.chapverse sid le.go.lu.d035:leluLI.Gospel.book_abbr sid le.go.lu.d035:leluLI.Gospel.chapverse"
      }
      case sep 17: {
        p.title nid "167 p.chapverse sid le.go.lu.d035:leluLI.Gospel.book_abbr sid le.go.lu.d035:leluLI.Gospel.chapverse"
      }
      case sep 18: {
        p.title nid "170 p.chapverse sid le.go.lu.d049:leluLI.Gospel.book_abbr sid le.go.lu.d049:leluLI.Gospel.chapverse"
      }
      case sep 19: {
        p.title nid "173 p.chapverse sid le.go.lu.d049:leluLI.Gospel.book_abbr sid le.go.lu.d049:leluLI.Gospel.chapverse"
      }
      case sep 20: {
        p.title nid "176 p.chapverse sid le.go.lu.d049:leluLI.Gospel.book_abbr sid le.go.lu.d049:leluLI.Gospel.chapverse"
      }
      case sep 21: {
        p.title nid "179 p.chapverse sid le.go.lu.d049:leluLI.Gospel.book_abbr sid le.go.lu.d049:leluLI.Gospel.chapverse"
      }
    }
  }
  case 56: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        p.title nid "186 p.chapverse sid le.go.lu.d056:leluLI.Gospel.book_abbr sid le.go.lu.d056:leluLI.Gospel.chapverse"
      }
      case sep 16: {
        p.title nid "189 p.chapverse sid le.go.lu.d056:leluLI.Gospel.book_abbr sid le.go.lu.d056:leluLI.Gospel.chapverse"
      }
      case sep 17: {
        p.title nid "192 p.chapverse sid le.go.lu.d056:leluLI.Gospel.book_abbr sid le.go.lu.d056:leluLI.Gospel.chapverse"
      }
      case sep 18: {
        p.title nid "195 p.chapverse sid le.go.lu.d056:leluLI.Gospel.book_abbr sid le.go.lu.d056:leluLI.Gospel.chapverse"
      }
      case sep 19: {
        p.title nid "198 p.chapverse sid le.go.lu.d056:leluLI.Gospel.book_abbr sid le.go.lu.d056:leluLI.Gospel.chapverse"
      }
      case sep 20: {
        p.title nid "201 p.chapverse sid le.go.lu.d056:leluLI.Gospel.book_abbr sid le.go.lu.d056:leluLI.Gospel.chapverse"
      }
      case sep 21: {
        p.title nid "204 p.chapverse sid le.go.me.m11.d16:lemeLI.Gospel.book_abbr sid le.go.me.m11.d16:lemeLI.Gospel.chapverse"
      }
    }
  }
  case 63: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        p.title nid "211 p.chapverse sid le.go.lu.d063:leluLI.Gospel.book_abbr sid le.go.lu.d063:leluLI.Gospel.chapverse"
      }
      case sep 16: {
        p.title nid "214 p.chapverse sid le.go.lu.d063:leluLI.Gospel.book_abbr sid le.go.lu.d063:leluLI.Gospel.chapverse"
      }
      case sep 17: {
        p.title nid "217 p.chapverse sid le.go.lu.d063:leluLI.Gospel.book_abbr sid le.go.lu.d063:leluLI.Gospel.chapverse"
      }
      case sep 18: {
        p.title nid "220 p.chapverse sid le.go.lu.d063:leluLI.Gospel.book_abbr sid le.go.lu.d063:leluLI.Gospel.chapverse"
      }
      case sep 19: {
        p.title nid "223 p.chapverse sid le.go.me.m11.d21:lemeLI.Gospel.book_abbr sid le.go.me.m11.d21:lemeLI.Gospel.chapverse"
      }
      case sep 20: {
        p.title nid "226 p.chapverse sid le.go.lu.d063:leluLI.Gospel.book_abbr sid le.go.lu.d063:leluLI.Gospel.chapverse"
      }
      case sep 21: {
        p.title nid "229 p.chapverse sid le.go.lu.d063:leluLI.Gospel.book_abbr sid le.go.lu.d063:leluLI.Gospel.chapverse"
      }
    }
  }
  case 70: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        p.title nid "236 p.chapverse sid le.go.lu.d091:leluLI.Gospel.book_abbr sid le.go.lu.d091:leluLI.Gospel.chapverse"
      }
      case sep 16: {
        p.title nid "239 p.chapverse sid le.go.lu.d091:leluLI.Gospel.book_abbr sid le.go.lu.d091:leluLI.Gospel.chapverse"
      }
      case sep 17: {
        p.title nid "242 p.chapverse sid le.go.lu.d091:leluLI.Gospel.book_abbr sid le.go.lu.d091:leluLI.Gospel.chapverse"
      }
      case sep 18: {
        p.title nid "245 p.chapverse sid le.go.lu.d091:leluLI.Gospel.book_abbr sid le.go.lu.d091:leluLI.Gospel.chapverse"
      }
      case sep 19: {
        p.title nid "248 p.chapverse sid le.go.lu.d091:leluLI.Gospel.book_abbr sid le.go.lu.d091:leluLI.Gospel.chapverse"
      }
      case sep 20: {
        p.title nid "251 p.chapverse sid le.go.lu.d091:leluLI.Gospel.book_abbr sid le.go.lu.d091:leluLI.Gospel.chapverse"
      }
      case sep 21: {
        p.title nid "254 p.chapverse sid le.go.me.m11.d30:lemeLI.Gospel.book_abbr sid le.go.me.m11.d30:lemeLI.Gospel.chapverse"
      }
    }
  }
  case 77: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        p.title nid "261 p.chapverse sid le.go.lu.d098:leluLI.Gospel.book_abbr sid le.go.lu.d098:leluLI.Gospel.chapverse"
      }
      case sep 16: {
        p.title nid "264 p.chapverse sid le.go.lu.d098:leluLI.Gospel.book_abbr sid le.go.lu.d098:leluLI.Gospel.chapverse"
      }
      case sep 17: {
        p.title nid "267 p.chapverse sid le.go.lu.d098:leluLI.Gospel.book_abbr sid le.go.lu.d098:leluLI.Gospel.chapverse"
      }
      case sep 18: {
        p.title nid "270 p.chapverse sid le.go.lu.d070:leluLI.Gospel.book_abbr sid le.go.lu.d070:leluLI.Gospel.chapverse"
      }
      case sep 19: {
        p.title nid "273 p.chapverse sid le.go.lu.d070:leluLI.Gospel.book_abbr sid le.go.lu.d070:leluLI.Gospel.chapverse"
      }
      case sep 20: {
        p.title nid "276 p.chapverse sid le.go.lu.d070:leluLI.Gospel.book_abbr sid le.go.lu.d070:leluLI.Gospel.chapverse"
      }
      case sep 21: {
        p.title nid "279 p.chapverse sid le.go.lu.d070:leluLI.Gospel.book_abbr sid le.go.lu.d070:leluLI.Gospel.chapverse"
      }
    }
  }
  case 84: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        p.title nid "286 p.chapverse sid le.go.lu.d070:leluLI.Gospel.book_abbr sid le.go.lu.d070:leluLI.Gospel.chapverse"
      }
      case sep 16: {
        p.title nid "289 p.chapverse sid le.go.lu.d070:leluLI.Gospel.book_abbr sid le.go.lu.d070:leluLI.Gospel.chapverse"
      }
      case sep 17: {
        p.title nid "292 p.chapverse sid le.go.lu.d070:leluLI.Gospel.book_abbr sid le.go.lu.d070:leluLI.Gospel.chapverse"
      }
      case sep 18: {
        p.title nid "295 p.chapverse sid le.go.lu.d077:leluLI.Gospel.book_abbr sid le.go.lu.d077:leluLI.Gospel.chapverse"
      }
      case sep 19: {
        p.title nid "298 p.chapverse sid le.go.lu.d077:leluLI.Gospel.book_abbr sid le.go.lu.d077:leluLI.Gospel.chapverse"
      }
      case sep 20: {
        p.title nid "301 p.chapverse sid le.go.lu.d077:leluLI.Gospel.book_abbr sid le.go.lu.d077:leluLI.Gospel.chapverse"
      }
      case sep 21: {
        p.title nid "304 p.chapverse sid le.go.lu.d077:leluLI.Gospel.book_abbr sid le.go.lu.d077:leluLI.Gospel.chapverse"
      }
    }
  }
  case 90: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        p.title nid "311 p.chapverse rid le.go.lu.*:leluLI.Gospel.book_abbr rid le.go.lu.*:leluLI.Gospel.chapverse"
      }
      case sep 16: {
        p.title nid "314 p.chapverse rid le.go.lu.*:leluLI.Gospel.book_abbr rid le.go.lu.*:leluLI.Gospel.chapverse"
      }
      case sep 17: {
        p.title nid "317 p.chapverse rid le.go.lu.*:leluLI.Gospel.book_abbr rid le.go.lu.*:leluLI.Gospel.chapverse"
      }
      case sep 18: {
        p.title nid "320 p.chapverse rid le.go.lu.*:leluLI.Gospel.book_abbr rid le.go.lu.*:leluLI.Gospel.chapverse"
      }
      case sep 19: {
        p.title nid "323 p.chapverse sid le.go.me.m12.dSatBC:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSatBC:lemeLI.Gospel.chapverse"
      }
      case sep 20: {
        p.title nid "326 p.chapverse sid le.go.me.m12.dSatBC:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSatBC:lemeLI.Gospel.chapverse"
      }
      case sep 21: {
        p.title nid "329 p.chapverse sid le.go.me.m12.dSatBC:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSatBC:lemeLI.Gospel.chapverse"
      }
    }
  }
  case 91: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        p.title nid "336 p.chapverse sid le.go.lu.d077:leluLI.Gospel.book_abbr sid le.go.lu.d077:leluLI.Gospel.chapverse"
      }
      case sep 16: {
        p.title nid "339 p.chapverse sid le.go.lu.d077:leluLI.Gospel.book_abbr sid le.go.lu.d077:leluLI.Gospel.chapverse"
      }
      case sep 17: {
        p.title nid "342 p.chapverse sid le.go.lu.d077:leluLI.Gospel.book_abbr sid le.go.lu.d077:leluLI.Gospel.chapverse"
      }
      case sep 18: {
        p.title nid "345 p.chapverse sid le.go.me.m12.dSunBC:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSunBC:lemeLI.Gospel.chapverse"
      }
      case sep 19: {
        p.title nid "348 p.chapverse sid le.go.me.m12.dSunBC:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSunBC:lemeLI.Gospel.chapverse"
      }
      case sep 20: {
        p.title nid "351 p.chapverse sid le.go.me.m12.dSunBC:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSunBC:lemeLI.Gospel.chapverse"
      }
      case sep 21: {
        p.title nid "354 p.chapverse sid le.go.me.m12.dSunBC:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSunBC:lemeLI.Gospel.chapverse"
      }
    }
  }
  case 97: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        p.title nid "361 p.chapverse sid le.go.me.m12.dSatBC:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSatBC:lemeLI.Gospel.chapverse"
      }
      case sep 16: {
        p.title nid "364 p.chapverse sid le.go.me.m12.dSatBC:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSatBC:lemeLI.Gospel.chapverse"
      }
      case sep 17: {
        p.title nid "367 p.chapverse sid le.go.me.m12.dSatBC:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSatBC:lemeLI.Gospel.chapverse"
      }
      case sep 18: {
        p.title nid "370 p.chapverse sid le.go.me.m12.dSatBC:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSatBC:lemeLI.Gospel.chapverse"
      }
      case sep 19: {
        p.title nid "373 p.chapverse sid le.go.me.m12.d25:lemeLI.Gospel.book_abbr sid le.go.me.m12.d25:lemeLI.Gospel.chapverse"
      }
      case sep 20: {
        p.title nid "376 p.chapverse sid le.go.me.m12.dSatAC:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSatAC:lemeLI.Gospel.chapverse"
      }
      case sep 21: {
        p.title nid "379 p.chapverse sid le.go.me.m12.dSatAC:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSatAC:lemeLI.Gospel.chapverse"
      }
    }
  }
  case 98: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        p.title nid "386 p.chapverse sid le.go.me.m12.dSunBC:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSunBC:lemeLI.Gospel.chapverse"
      }
      case sep 16: {
        p.title nid "389 p.chapverse sid le.go.me.m12.dSunBC:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSunBC:lemeLI.Gospel.chapverse"
      }
      case sep 17: {
        p.title nid "392 p.chapverse sid le.go.me.m12.dSunBC:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSunBC:lemeLI.Gospel.chapverse"
      }
      case sep 18: {
        p.title nid "395 p.chapverse sid le.go.me.m12.d25:lemeLI.Gospel.book_abbr sid le.go.me.m12.d25:lemeLI.Gospel.chapverse"
      }
      case sep 19: {
        p.title nid "398 p.chapverse sid le.go.me.m12.dSunAC:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSunAC:lemeLI.Gospel.chapverse"
      }
      case sep 20: {
        p.title nid "401 p.chapverse sid le.go.me.m12.dSunAC:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSunAC:lemeLI.Gospel.chapverse"
      }
      case sep 21: {
        p.title nid "404 p.chapverse sid le.go.me.m12.dSunAC:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSunAC:lemeLI.Gospel.chapverse"
      }
    }
  }
  case 104: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        p.title nid "411 p.chapverse sid le.go.me.m12.dSatAC:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSatAC:lemeLI.Gospel.chapverse"
      }
      case sep 16: {
        p.title nid "414 p.chapverse sid le.go.me.m12.dSatAC:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSatAC:lemeLI.Gospel.chapverse"
      }
      case sep 17: {
        p.title nid "417 p.chapverse sid le.go.me.m12.dSatAC:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSatAC:lemeLI.Gospel.chapverse"
      }
      case sep 18: {
        p.title nid "420 p.chapverse sid le.go.me.m12.dSatBE:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSatBE:lemeLI.Gospel.chapverse"
      }
      case sep 19: {
        p.title nid "423 p.chapverse sid le.go.me.m01.d01:lemeLI.Gospel.book_abbr sid le.go.me.m01.d01:lemeLI.Gospel.chapverse"
      }
      case sep 20: {
        p.title nid "426 p.chapverse sid le.go.me.m12.dSatBE:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSatBE:lemeLI.Gospel.chapverse"
      }
      case sep 21: {
        p.title nid "429 p.chapverse sid le.go.me.m12.dSatBE:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSatBE:lemeLI.Gospel.chapverse"
      }
    }
  }
  case 105: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        p.title nid "436 p.chapverse sid le.go.me.m12.dSunAC:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSunAC:lemeLI.Gospel.chapverse"
      }
      case sep 16: {
        p.title nid "439 p.chapverse sid le.go.me.m12.dSunAC:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSunAC:lemeLI.Gospel.chapverse"
      }
      case sep 17: {
        p.title nid "442 p.chapverse sid le.go.me.m12.dSunBE:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSunBE:lemeLI.Gospel.chapverse"
      }
      case sep 18: {
        p.title nid "445 p.chapverse sid le.go.me.m01.d01:lemeLI.Gospel.book_abbr sid le.go.me.m01.d01:lemeLI.Gospel.chapverse"
      }
      case sep 19: {
        p.title nid "448 p.chapverse sid le.go.me.m12.dSunBE:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSunBE:lemeLI.Gospel.chapverse"
      }
      case sep 20: {
        p.title nid "451 p.chapverse sid le.go.me.m12.dSunBE:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSunBE:lemeLI.Gospel.chapverse"
      }
      case sep 21: {
        p.title nid "454 p.chapverse sid le.go.me.m12.dSunBE:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSunBE:lemeLI.Gospel.chapverse"
      }
    }
  }
  case 111: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        p.title nid "461 p.chapverse sid le.go.me.m12.dSatBE:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSatBE:lemeLI.Gospel.chapverse"
      }
      case sep 16: {
        p.title nid "464 p.chapverse sid le.go.me.m12.dSatBE:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSatBE:lemeLI.Gospel.chapverse"
      }
      case sep 17: {
        p.title nid "467 p.chapverse sid le.go.me.m01.d06:lemeLI.Gospel.book_abbr sid le.go.me.m01.d06:lemeLI.Gospel.chapverse"
      }
      case sep 18: {
        p.title nid "470 p.chapverse sid le.go.me.m01.d07:lemeLI.Gospel.book_abbr sid le.go.me.m01.d07:lemeLI.Gospel.chapverse"
      }
      case sep 19: {
        p.title nid "473 p.chapverse sid le.go.me.m01.dSatAE:lemeLI.Gospel.book_abbr sid le.go.me.m01.dSatAE:lemeLI.Gospel.chapverse"
      }
      case sep 20: {
        p.title nid "476 p.chapverse sid le.go.me.m01.dSatAE:lemeLI.Gospel.book_abbr sid le.go.me.m01.dSatAE:lemeLI.Gospel.chapverse"
      }
      case sep 21: {
        p.title nid "479 p.chapverse sid le.go.me.m01.dSatAE:lemeLI.Gospel.book_abbr sid le.go.me.m01.dSatAE:lemeLI.Gospel.chapverse"
      }
    }
  }
  case 112: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        p.title nid "486 p.chapverse sid le.go.me.m12.dSunBE:lemeLI.Gospel.book_abbr sid le.go.me.m12.dSunBE:lemeLI.Gospel.chapverse"
      }
      case sep 16: {
        p.title nid "489 p.chapverse sid le.go.me.m01.d06:lemeLI.Gospel.book_abbr sid le.go.me.m01.d06:lemeLI.Gospel.chapverse"
      }
      case sep 17: {
        p.title nid "492 p.chapverse sid le.go.me.m01.d07:lemeLI.Gospel.book_abbr sid le.go.me.m01.d07:lemeLI.Gospel.chapverse"
      }
      case sep 18: {
        p.title nid "495 p.chapverse sid le.go.me.m01.dSunAE:lemeLI.Gospel.book_abbr sid le.go.me.m01.dSunAE:lemeLI.Gospel.chapverse"
      }
      case sep 19: {
        p.title nid "498 p.chapverse sid le.go.me.m01.dSunAE:lemeLI.Gospel.book_abbr sid le.go.me.m01.dSunAE:lemeLI.Gospel.chapverse"
      }
      case sep 20: {
        p.title nid "501 p.chapverse sid le.go.me.m01.dSunAE:lemeLI.Gospel.book_abbr sid le.go.me.m01.dSunAE:lemeLI.Gospel.chapverse"
      }
      case sep 21: {
        p.title nid "504 p.chapverse sid le.go.me.m01.dSunAE:lemeLI.Gospel.book_abbr sid le.go.me.m01.dSunAE:lemeLI.Gospel.chapverse"
      }
    }
  }
  case 118: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        p.title nid "511 p.chapverse sid le.go.me.m01.dSatAE:lemeLI.Gospel.book_abbr sid le.go.me.m01.dSatAE:lemeLI.Gospel.chapverse"
      }
      case sep 16: {
        p.title nid "514 p.chapverse sid le.go.me.m01.dSatAE:lemeLI.Gospel.book_abbr sid le.go.me.m01.dSatAE:lemeLI.Gospel.chapverse"
      }
      case sep 17: {
        p.title nid "517 p.chapverse sid le.go.me.m01.dSatAE:lemeLI.Gospel.book_abbr sid le.go.me.m01.dSatAE:lemeLI.Gospel.chapverse"
      }
      case sep 18: {
        p.title nid "520 p.chapverse sid le.go.me.m01.dSatAE:lemeLI.Gospel.book_abbr sid le.go.me.m01.dSatAE:lemeLI.Gospel.chapverse"
      }
      case sep 19: {
        p.title nid "523 p.chapverse rid le.go.lu.*:leluLI.Gospel.book_abbr rid le.go.lu.*:leluLI.Gospel.chapverse"
      }
      case sep 20: {
        p.title nid "526 p.chapverse rid le.go.lu.*:leluLI.Gospel.book_abbr rid le.go.lu.*:leluLI.Gospel.chapverse"
      }
      case sep 21: {
        p.title nid "529 p.chapverse rid le.go.lu.*:leluLI.Gospel.book_abbr rid le.go.lu.*:leluLI.Gospel.chapverse"
      }
    }
  }
  case 119: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        p.title nid "536 p.chapverse sid le.go.me.m01.dSunAE:lemeLI.Gospel.book_abbr sid le.go.me.m01.dSunAE:lemeLI.Gospel.chapverse"
      }
      case sep 16: {
        p.title nid "539 p.chapverse sid le.go.me.m01.dSunAE:lemeLI.Gospel.book_abbr sid le.go.me.m01.dSunAE:lemeLI.Gospel.chapverse"
      }
      case sep 17: {
        p.title nid "542 p.chapverse sid le.go.me.m01.dSunAE:lemeLI.Gospel.book_abbr sid le.go.me.m01.dSunAE:lemeLI.Gospel.chapverse"
      }
      case sep 18: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "547 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "550 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "553 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "556 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "559 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "562 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
        }
      }
      case sep 19: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "569 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "572 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "575 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "578 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "581 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "584 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
        }
      }
      case sep 20: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "591 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "594 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "597 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "600 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "603 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "606 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
        }
      }
      case sep 21: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "613 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "616 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "619 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "622 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "625 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "628 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
        }
      }
    }
  }
  case 126: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "639 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "642 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "645 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "648 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "651 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "654 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
        }
      }
      case sep 16: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "661 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "664 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "667 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "670 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "673 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "676 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
        }
      }
      case sep 17: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "683 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "686 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "689 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "692 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "695 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "698 p.chapverse sid le.go.lu.d084:leluLI.Gospel.book_abbr sid le.go.lu.d084:leluLI.Gospel.chapverse"
          }
        }
      }
      case sep 18: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "705 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "708 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "711 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "714 p.chapverse sid le.go.lu.d098:leluLI.Gospel.book_abbr sid le.go.lu.d098:leluLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "717 p.chapverse sid le.go.lu.d098:leluLI.Gospel.book_abbr sid le.go.lu.d098:leluLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "720 p.chapverse sid le.go.lu.d098:leluLI.Gospel.book_abbr sid le.go.lu.d098:leluLI.Gospel.chapverse"
          }
        }
      }
      case sep 19: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "727 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "730 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "733 p.chapverse sid le.go.lu.d098:leluLI.Gospel.book_abbr sid le.go.lu.d098:leluLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "736 p.chapverse sid le.go.lu.d098:leluLI.Gospel.book_abbr sid le.go.lu.d098:leluLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "739 p.chapverse sid le.go.lu.d098:leluLI.Gospel.book_abbr sid le.go.lu.d098:leluLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "742 p.chapverse sid le.go.lu.d098:leluLI.Gospel.book_abbr sid le.go.lu.d098:leluLI.Gospel.chapverse"
          }
        }
      }
      case sep 20: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "749 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "752 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "755 p.chapverse sid le.go.lu.d098:leluLI.Gospel.book_abbr sid le.go.lu.d098:leluLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "758 p.chapverse sid le.go.lu.d098:leluLI.Gospel.book_abbr sid le.go.lu.d098:leluLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "761 p.chapverse sid le.go.lu.d098:leluLI.Gospel.book_abbr sid le.go.lu.d098:leluLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "764 p.chapverse sid le.go.lu.d098:leluLI.Gospel.book_abbr sid le.go.lu.d098:leluLI.Gospel.chapverse"
          }
        }
      }
      case sep 21: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "771 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "774 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "777 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "780 p.chapverse sid le.go.lu.d098:leluLI.Gospel.book_abbr sid le.go.lu.d098:leluLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "783 p.chapverse sid le.go.lu.d098:leluLI.Gospel.book_abbr sid le.go.lu.d098:leluLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "786 p.chapverse sid le.go.lu.d098:leluLI.Gospel.book_abbr sid le.go.lu.d098:leluLI.Gospel.chapverse"
          }
        }
      }
    }
  }
  case 133: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "797 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "800 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "803 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "806 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "809 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "812 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
        }
      }
      case sep 16: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "819 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "822 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "825 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "828 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "831 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "834 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
        }
      }
      case sep 17: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "841 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "844 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "847 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "850 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "853 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "856 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
        }
      }
      case sep 18: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "863 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "866 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "869 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "872 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "875 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "878 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
        }
      }
      case sep 19: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "885 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "888 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "891 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "894 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "897 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "900 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
        }
      }
      case sep 20: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "907 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "910 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "913 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "916 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "919 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "922 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
        }
      }
      case sep 21: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "929 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "932 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "935 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "938 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "941 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "944 p.chapverse sid le.go.lu.d105:leluLI.Gospel.book_abbr sid le.go.lu.d105:leluLI.Gospel.chapverse"
          }
        }
      }
    }
  }
  case 140: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "955 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "958 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "961 p.chapverse sid le.go.me.m02.d02:lemeLI.Gospel.book_abbr sid le.go.me.m02.d02:lemeLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "964 p.chapverse sid le.go.me.m02.d02:lemeLI.Gospel.book_abbr sid le.go.me.m02.d02:lemeLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "967 p.chapverse sid le.go.me.m02.d02:lemeLI.Gospel.book_abbr sid le.go.me.m02.d02:lemeLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "970 p.chapverse sid le.go.me.m02.d02:lemeLI.Gospel.book_abbr sid le.go.me.m02.d02:lemeLI.Gospel.chapverse"
          }
        }
      }
      case sep 16: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "977 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "980 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "983 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "986 p.chapverse sid le.go.mc.d232:lemcLI.Gospel.book_abbr sid le.go.mc.d232:lemcLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "989 p.chapverse sid le.go.mc.d225:lemcLI.Gospel.book_abbr sid le.go.mc.d225:lemcLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "992 p.chapverse sid le.go.mc.d218:lemcLI.Gospel.book_abbr sid le.go.mc.d218:lemcLI.Gospel.chapverse"
          }
        }
      }
      case sep 17: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "999 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "1002 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "1005 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "1008 p.chapverse sid le.go.mc.d232:lemcLI.Gospel.book_abbr sid le.go.mc.d232:lemcLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "1011 p.chapverse sid le.go.mc.d225:lemcLI.Gospel.book_abbr sid le.go.mc.d225:lemcLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "1014 p.chapverse sid le.go.mc.d218:lemcLI.Gospel.book_abbr sid le.go.mc.d218:lemcLI.Gospel.chapverse"
          }
        }
      }
      case sep 18: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "1021 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "1024 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "1027 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "1030 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "1033 p.chapverse sid le.go.mc.d232:lemcLI.Gospel.book_abbr sid le.go.mc.d232:lemcLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "1036 p.chapverse sid le.go.mc.d225:lemcLI.Gospel.book_abbr sid le.go.mc.d225:lemcLI.Gospel.chapverse"
          }
        }
      }
      case sep 19: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "1043 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "1046 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "1049 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "1052 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "1055 p.chapverse sid le.go.mc.d232:lemcLI.Gospel.book_abbr sid le.go.mc.d232:lemcLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "1058 p.chapverse sid le.go.mc.d225:lemcLI.Gospel.book_abbr sid le.go.mc.d225:lemcLI.Gospel.chapverse"
          }
        }
      }
      case sep 20: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "1065 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "1068 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "1071 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "1074 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "1077 p.chapverse sid le.go.mc.d232:lemcLI.Gospel.book_abbr sid le.go.mc.d232:lemcLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "1080 p.chapverse sid le.go.mc.d225:lemcLI.Gospel.book_abbr sid le.go.mc.d225:lemcLI.Gospel.chapverse"
          }
        }
      }
      case sep 21: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "1087 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "1090 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "1093 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "1096 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "1099 p.chapverse sid le.go.mc.d232:lemcLI.Gospel.book_abbr sid le.go.mc.d232:lemcLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "1102 p.chapverse sid le.go.mc.d225:lemcLI.Gospel.book_abbr sid le.go.mc.d225:lemcLI.Gospel.chapverse"
          }
        }
      }
    }
  }
  case 147: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "1113 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "1116 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "1119 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "1122 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "1125 p.chapverse sid le.go.mc.d232:lemcLI.Gospel.book_abbr sid le.go.mc.d232:lemcLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "1128 p.chapverse sid le.go.mc.d225:lemcLI.Gospel.book_abbr sid le.go.mc.d225:lemcLI.Gospel.chapverse"
          }
        }
      }
      case sep 16: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "1135 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "1138 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "1141 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "1144 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "1147 p.chapverse sid le.go.mc.d232:lemcLI.Gospel.book_abbr sid le.go.mc.d232:lemcLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "1150 p.chapverse sid le.go.mc.d225:lemcLI.Gospel.book_abbr sid le.go.mc.d225:lemcLI.Gospel.chapverse"
          }
        }
      }
      case sep 17: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "1157 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "1160 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "1163 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "1166 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "1169 p.chapverse sid le.go.mc.d232:lemcLI.Gospel.book_abbr sid le.go.mc.d232:lemcLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "1172 p.chapverse sid le.go.mc.d225:lemcLI.Gospel.book_abbr sid le.go.mc.d225:lemcLI.Gospel.chapverse"
          }
        }
      }
      case sep 18: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "1179 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "1182 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "1185 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "1188 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "1191 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "1194 p.chapverse sid le.go.mc.d232:lemcLI.Gospel.book_abbr sid le.go.mc.d232:lemcLI.Gospel.chapverse"
          }
        }
      }
      case sep 19: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "1201 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "1204 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "1207 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "1210 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "1213 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "1216 p.chapverse sid le.go.mc.d232:lemcLI.Gospel.book_abbr sid le.go.mc.d232:lemcLI.Gospel.chapverse"
          }
        }
      }
      case sep 20: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "1223 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "1226 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "1229 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "1232 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "1235 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "1238 p.chapverse sid le.go.mc.d232:lemcLI.Gospel.book_abbr sid le.go.mc.d232:lemcLI.Gospel.chapverse"
          }
        }
      }
      case sep 21: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "1245 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "1248 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "1251 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "1254 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "1257 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "1260 p.chapverse sid le.go.mc.d232:lemcLI.Gospel.book_abbr sid le.go.mc.d232:lemcLI.Gospel.chapverse"
          }
        }
      }
    }
  }
  case 154: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "1271 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "1274 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "1277 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "1280 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "1283 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "1286 p.chapverse sid le.go.mc.d232:lemcLI.Gospel.book_abbr sid le.go.mc.d232:lemcLI.Gospel.chapverse"
          }
        }
      }
      case sep 16: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "1293 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "1296 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "1299 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "1302 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "1305 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "1308 p.chapverse sid le.go.mc.d232:lemcLI.Gospel.book_abbr sid le.go.mc.d232:lemcLI.Gospel.chapverse"
          }
        }
      }
      case sep 17: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "1315 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "1318 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "1321 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "1324 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "1327 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "1330 p.chapverse sid le.go.mc.d232:lemcLI.Gospel.book_abbr sid le.go.mc.d232:lemcLI.Gospel.chapverse"
          }
        }
      }
      case sep 18: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "1337 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "1340 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "1343 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "1346 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "1349 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "1352 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
        }
      }
      case sep 19: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "1359 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "1362 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "1365 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "1368 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "1371 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "1374 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
        }
      }
      case sep 20: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "1381 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "1384 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "1387 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "1390 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "1393 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "1396 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
        }
      }
      case sep 21: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "1403 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "1406 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "1409 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "1412 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "1415 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "1418 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
        }
      }
    }
  }
  case 161: {
    switch sundayAfterElevationOfCross {
      case sep 15: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "1429 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "1432 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "1435 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "1438 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "1441 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "1444 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
        }
      }
      case sep 16: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "1451 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "1454 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "1457 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "1460 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "1463 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "1466 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
        }
      }
      case sep 17: {
        switch sundaysBeforeTriodion {
          case 1: {
            p.title nid "1473 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 2: {
            p.title nid "1476 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 3: {
            p.title nid "1479 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 4: {
            p.title nid "1482 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 5: {
            p.title nid "1485 p.chapverse rid le.go.mc.*:lemcLI.Gospel.book_abbr rid le.go.mc.*:lemcLI.Gospel.chapverse"
          }
          case 6: {
            p.title nid "1488 p.chapverse sid le.go.mc.d239:lemcLI.Gospel.book_abbr sid le.go.mc.d239:lemcLI.Gospel.chapverse"
          }
        }
      }
      default: {
        p.title nid "1493 default 1494"
      }
    }
  }
  default: {
    p.title nid "1498 default 1498"
  }
}
`
