package parser

import (
	"errors"
	"fmt"
	"github.com/antlr4-go/antlr/v4"
	"github.com/liturgiko/doxa/pkg/atempl"
	"github.com/liturgiko/doxa/pkg/css"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/enums/calendarTypes"
	"github.com/liturgiko/doxa/pkg/enums/versions"
	"github.com/liturgiko/doxa/pkg/ldp"
	"github.com/liturgiko/doxa/pkg/resolver"
	"github.com/liturgiko/doxa/pkg/valuemap"
	lml "gitlab.com/ocmc/liturgiko/lml/golang/parser"
	"sync"
)

// LML provides access to a lexer and parser for the input stream.
// Use NewLMLParser to get an instance.
type LML struct {
	Lexer           *lml.LMLLexer
	Parser          *lml.LMLParser
	Listener        *LMLListener
	ErrorListener   *ErrorListener
	TemplatePath    string
	TemplateContent string
	Realm           string
}
type Parms struct {
	TemplatePath        string
	TemplateContent     string
	Realm               string
	TheVersion          versions.Version
	GenLangs            []*atempl.TableLayout
	TheResolver         resolver.Resolver
	MediaReverseLookup  *sync.Map
	CallStack           *CallStack
	TemplatesById       map[string]int
	TemplatesByNbr      map[int]string
	TheMedia            *valuemap.ValueMapper
	Values              *valuemap.ValueMapper
	HtmlCss             *css.StyleSheet
	PdfCss              *css.StyleSheet
	TheCalendarType     calendarTypes.CalendarType
	TheLdp              *ldp.LDP
	TemplateDir         string
	YearOverride        int
	FlagMissingRidTk    bool
	FlagMissingRidValue bool
	IncludeCovers       bool
}

// NewLMLParser provides an LML instance with a lexer and parser initialized for the input stream.
// Upon completion of the parsing, a slice of parse errors will be available, and a template.ATEM will be populated.
// If the ATEM will be used for generating HTML or PDF or something else, then genLibs and resolver may not be nil.
// If genLibs and resolver are nil, the purpose of calling the parser is presumed to be simply to validate the syntax of the input.
// If genLibs and resolver are not nil, they will be used to populate the Values and Version acronyms in the ATEM, so it can be used for generation.
// Returns an error if the database path is invalid.
// templatePath is the path to the template.
// genLibs provides the primary libraries and fallback libraries to use for generation.
// ldp is used when we want to parse an inserted template. The ldp passed in should be the ldp from the parent template.
// If an inserted template is not being parsed (i.e., it is the top template), then set the ldp parameter to nil.
func NewLMLParser(parms *Parms) (*LML, error) {
	l := new(LML)
	var msg string
	var err error
	if parms.TheResolver == nil {
		msg = "nil TheResolver error"
		doxlog.Error(msg)
		return nil, errors.New(msg)
	}
	if parms.HtmlCss == nil {
		msg = "nil HtmlCss error"
		doxlog.Error(msg)
		return nil, errors.New(msg)
	}
	if parms.PdfCss == nil {
		msg = "nil PdfCss error"
		doxlog.Error(msg)
		return nil, errors.New(msg)
	}
	l.TemplatePath = parms.TemplatePath
	l.TemplateContent = parms.TemplateContent
	l.Realm = parms.Realm
	l.Lexer = lml.NewLMLLexer(antlr.NewInputStream(parms.TemplateContent))
	stream := antlr.NewCommonTokenStream(l.Lexer, antlr.TokenDefaultChannel)
	l.Parser = lml.NewLMLParser(stream)
	if parms.TheMedia == nil {
		parms.TheMedia = valuemap.NewValueMapper()
	}
	if parms.Values == nil {
		parms.Values = valuemap.NewValueMapper()
	}
	l.Listener, err = NewLMLListener(
		parms.Realm,
		parms.TheVersion,
		parms.GenLangs,
		parms.TheResolver,
		parms.MediaReverseLookup,
		parms.CallStack,
		parms.TemplatesById,
		parms.TemplatesByNbr,
		parms.TheMedia,
		parms.Values,
		parms.HtmlCss,
		parms.PdfCss,
		parms.YearOverride,
		parms.FlagMissingRidTk,
		parms.FlagMissingRidValue,
		parms.IncludeCovers)
	l.Listener.TemplatesDir = parms.TemplateDir
	l.Listener.TemplatePath = parms.TemplatePath
	if err != nil {
		msg = fmt.Sprintf("new LML listener error: %v", err)
		return nil, fmt.Errorf(msg)
	}
	if parms.TheLdp == nil {
		l, _ := ldp.NewLDPCalendar(parms.TheCalendarType)
		parms.TheLdp = &l
	}
	if parms.TheLdp != nil {
		l.Listener.Ctx.month = int(parms.TheLdp.TheDay.Month())
		l.Listener.Ctx.day = parms.TheLdp.TheDay.Day()
		l.Listener.Ctx.year = parms.TheLdp.TheDay.Year()
		l.Listener.Ctx.calendar = parms.TheLdp.CalendarType
		err = l.Listener.SetLDP()
		if err != nil {
			doxlog.Errorf("%v", err)
			return nil, err
		}
	}
	if parms.TheLdp.OriginalMovableCycleDay != -1 {
		l.Listener.Ctx.LDP.MovableCycleDay = parms.TheLdp.MovableCycleDay
		l.Listener.Ctx.LDP.OriginalMovableCycleDay = parms.TheLdp.OriginalMovableCycleDay
		l.Listener.Ctx.LDP.DaysSinceStartOfTriodion = parms.TheLdp.DaysSinceStartOfTriodion
	}
	l.ErrorListener = NewLMLErrorListener(parms.TemplatePath)
	l.Parser.RemoveErrorListeners()
	l.Parser.AddErrorListener(l.ErrorListener)
	return l, nil
}

func NewLMLExpressionParser(input string, year, month, day int, calendar calendarTypes.CalendarType, resolver resolver.Resolver) (*LML, error) {
	l := new(LML)
	l.TemplateContent = input
	l.Lexer = lml.NewLMLLexer(antlr.NewInputStream(input))
	stream := antlr.NewCommonTokenStream(l.Lexer, antlr.TokenDefaultChannel)
	l.Parser = lml.NewLMLParser(stream)
	l.Listener = new(LMLListener)
	l.Listener.Resolver = resolver
	l.Listener.ATEM = atempl.NewATEM()
	l.Listener.ATEM.Calendar = calendar
	l.Listener.ATEM.Day = day
	l.Listener.ATEM.Month = month
	l.Listener.ATEM.Year = year
	l.Listener.Ctx = NewListenerContext()
	l.Listener.Ctx.calendar = calendar
	l.Listener.Ctx.day = day
	l.Listener.Ctx.month = month
	l.Listener.Ctx.year = year
	err := l.Listener.SetLDP()
	if err != nil {
		return nil, err
	}
	l.Listener.ResolvedTKs = valuemap.NewValueMapper()
	l.Listener.ResolvedMedia = valuemap.NewValueMapper()
	l.ErrorListener = NewLMLErrorListener("expression")
	l.Parser.RemoveErrorListeners()
	l.Parser.AddErrorListener(l.ErrorListener)
	return l, nil
}

// ExpressionTrue evaluates an expression and indicates whether it evaluates to true or false and if there are parse errors.
func ExpressionTrue(expression string,
	year, month, day int,
	calendar calendarTypes.CalendarType,
	resolver resolver.Resolver,
	model string) (bool, []ParseError, error) {
	theLml, err := NewLMLExpressionParser(expression, year, month, day, calendar, resolver)
	if err != nil {
		return false, nil, err
	}
	theLml.Listener.Ctx.Model = model

	_, parseErrors := theLml.WalkExpression()
	if len(parseErrors) > 0 {
		return false, parseErrors, nil
	}
	return theLml.Listener.Ctx.ExpressionTrue, nil, nil
}

// WalkTemplate performs a walk on the template parse tree starting at the root
// and going down recursively with depth-first search.
// On each node, EnterRule is called before recursively walking down into child nodes,
// then ExitRule is called after the recursive call to wind up.
// Returns the Errors found in the input stream by the parser.
// Note that antlr only returns an error of a specific type once.
// So, there may be more errors than are reported by this function.
func (l *LML) WalkTemplate() (*atempl.ATEM, []ParseError) {
	atem, parseErrors := l.walk(l.Parser.Template())
	return atem, parseErrors
}

func (l *LML) WalkExpression() (*atempl.ATEM, []ParseError) {
	return l.walk(l.Parser.Expression())
}

// WalkStatement starts the walking at a template's statement, by-passing any property settings.
// This ensures that the properties are only set by the top-most template.
// This method should only be called if the template type is 'block'.
func (l *LML) WalkStatement() (*atempl.ATEM, []ParseError) {
	return l.walk(l.Parser.Statement())
}

// Performs a walk on the given parse tree starting at the root
// and going down recursively with depth-first search.
// At each node, EnterRule is called before recursively
// walking down into child nodes,
// then ExitRule is called after the recursive call to wind up.
// Returns the Errors found in the input stream by the parser.
// Note that antlr only returns an error of a specific type once.
// So, there may be more errors than are reported by this function.
func (l *LML) walk(t antlr.Tree) (*atempl.ATEM, []ParseError) {
	antlr.ParseTreeWalkerDefault.Walk(l.Listener, t)
	return l.Listener.ATEM, l.ErrorListener.Errors
}

// Tokens provides back information about each identified token. It will not indicate any errors.
func (l *LML) Tokens() []antlr.Token {
	var tokens []antlr.Token
	for {
		t := l.Lexer.NextToken()
		if t.GetTokenType() == antlr.TokenEOF {
			break
		}
		tokens = append(tokens, t)
	}
	return tokens
}
