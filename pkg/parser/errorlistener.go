package parser

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/doxlog"

	//"github.com/antlr/antlr4/runtime/Go/antlr/v4"
	"github.com/antlr4-go/antlr/v4"

	"strings"
)

type ParseError struct {
	TemplateID string
	Line       int
	Column     int
	Message    string
}

// String returns an error formatted as line:column:message
func (e *ParseError) String() string {
	return fmt.Sprintf("\n%s %d:%d \n\t%s", e.TemplateID, e.Line, e.Column, e.Message)
}

// StringVerbose returns an error formatted as Template templateID Line number Column number: message
// e.g. Template a/b Line 1 Column 3: mismatched input '==' expecting '='
func (e *ParseError) StringVerbose() string {
	message := strings.ReplaceAll(e.Message, "{", "")
	message = strings.ReplaceAll(message, "}", "")
	message = strings.ReplaceAll(message, "parse error: ", "\n\t")
	return fmt.Sprintf("\nError in Template \n\t%s %d:%d \n\t%s", e.TemplateID, e.Line, e.Column, message)
}

type ErrorListener struct {
	antlr.ErrorListener
	TemplateID string
	Errors     []ParseError
	LastError  *ParseError // used to detect an endless loop
	LoopCount  int         // to confirm we are endlessly looping
}

func NewLMLErrorListener(templateID string) *ErrorListener {
	l := new(ErrorListener)
	l.TemplateID = templateID
	return l
}
func (l *ErrorListener) AddError(templateID string, line, column int, msg string) {
	theError := ParseError{templateID, line, column, msg}
	doxlog.Error(theError.String())
	l.Errors = append(l.Errors, theError)
	l.AddLastError(templateID, line, column, msg)
}
func (l *ErrorListener) AddLastError(templateID string, line, column int, msg string) {
	e := new(ParseError)
	e.Line = line
	e.Column = column
	e.Message = msg
	e.TemplateID = templateID
	l.LoopCount++
	l.LastError = e

}
func (l *ErrorListener) SyntaxError(recognizer antlr.Recognizer, offendingSymbol interface{}, line, column int, msg string, e antlr.RecognitionException) {
	// check to see if antlr4 is endlessly looping the same error
	if l.LastError != nil &&
		l.LastError.TemplateID == l.TemplateID &&
		l.LastError.Line == line &&
		l.LastError.Column == column &&
		l.LastError.Message == msg {
		if l.LoopCount > 2 {
			doxlog.Panicf("endless loop in parser caused by: %s", l.LastError.StringVerbose())
		}
		return
	} else {
		l.LoopCount = 0
	}
	if strings.HasPrefix(msg, "extraneous input '/'") {
		// ignore.  It should not report this as an error.  Comments are handled fine when checked using grun.
	} else {
		l.AddError(l.TemplateID, line, column, msg)
	}
}
func (l *ErrorListener) ReportAmbiguity(recognizer antlr.Parser, dfa *antlr.DFA, startIndex, stopIndex int, exact bool, ambigAlts *antlr.BitSet, configs *antlr.ATNConfigSet) {
	//	l.AddError(l.TemplateID, 0, 0, "Unhandled ReportAmbiguity exception")
}
func (l *ErrorListener) ReportAttemptingFullContext(recognizer antlr.Parser, dfa *antlr.DFA, startIndex, stopIndex int, conflictingAlts *antlr.BitSet, configs *antlr.ATNConfigSet) {
	//	l.AddError(l.TemplateID, 0, 0, "Unhandled ReportAttemptingFullContext exception")
}
func (l *ErrorListener) ReportContextSensitivity(recognizer antlr.Parser, dfa *antlr.DFA, startIndex, stopIndex, prediction int, configs *antlr.ATNConfigSet) {
	//	l.AddError(l.TemplateID, 0, 0, "Unhandled ReportContextSensitivity exception")
}
