package eform

import (
	"fmt"
	"testing"
)

func TestEFM_GetAGESHtmlServices(t *testing.T) {
	efm := NewEFM()
	efm.AGES = true
	entries, err := efm.GetHtmlServices()
	if err != nil {
		fmt.Println(err)
		return
	}
	for _, entry := range entries {
		fmt.Printf("%s: %s %s %s\n", entry.Title, entry.Date, entry.DOW, entry.URL)
	}
}
