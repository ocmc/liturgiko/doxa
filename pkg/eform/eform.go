// Package eform converts html into a form that can be used to enter translations
package eform

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"io"
	"net/http"
	"sort"
	"strings"
	"time"
)

type EFM struct {
	AGES bool
}

func NewEFM() *EFM {
	n := new(EFM)
	return n
}
func (m *EFM) GetHtmlServices() ([]*ServiceEntry, error) {
	if m.AGES {
		return m.GetAGESHtmlServices()
	}
	return nil, nil
}
func (m *EFM) GetAGESHtmlServices() ([]*ServiceEntry, error) {
	var result []*ServiceEntry
	base := "https://agesinitiatives.com/dcs/public/dcs/"
	href := "https://agesinitiatives.com/dcs/public/dcs/servicesindex.html"
	anchors, err := GetAnchors(base, href, "index-day-link")
	if err != nil {
		return nil, err
	}

	chError := make(chan error)
	chAnchors := make(chan []*Anchor)

	for _, a := range anchors {
		go a.GetAnchorsConcurrent(base, "index-file-link", chAnchors, chError)
	}
	var fetchErrors []error

	for i := 0; i < len(anchors); {
		select {
		case err := <-chError:
			fetchErrors = append(fetchErrors, err)
			i++
		case childAnchors := <-chAnchors:
			for _, a := range childAnchors {
				// filter for html services
				if !strings.HasPrefix(a.Href, "h/s/") {
					continue
				}
				// filter for English only (i.e. single column table)
				if !strings.HasSuffix(a.Href, "/en/index.html") {
					continue
				}
				parts := strings.Split(a.Href, "/")
				if len(parts) != 8 {
					continue
				}
				se := new(ServiceEntry)
				se.URL = base + a.Href
				date := fmt.Sprintf("%s-%s-%s", parts[2], parts[3], parts[4])
				t, _ := time.Parse("2006-01-02", date)
				se.DOW = t.Weekday().String()
				se.Date = date
				se.Title = parts[5]
				result = append(result, se)
			}
			i++
		}
	}
	if len(fetchErrors) > 0 {
		return nil, fetchErrors[0]
	}
	sort.Sort(ByDateTitle(result))
	return result, nil
}

type BookEntry struct {
	Title string
	URL   string
}
type ServiceEntry struct {
	Title string
	URL   string
	Date  string
	DOW   string
}
type ByDateTitle []*ServiceEntry

func (b ByDateTitle) Len() int {
	return len(b)
}
func (b ByDateTitle) Less(i, j int) bool {
	return b[i].Date+b[i].Title < b[j].Date+b[j].Title
}
func (b ByDateTitle) Swap(i, j int) {
	b[i], b[j] = b[j], b[i]
}

func getAgesServiceLinks(base, url string) ([]*ServiceEntry, error) {
	var result []*ServiceEntry
	if !strings.HasSuffix(base, "/") {
		base = base + "/"
	}
	resp, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("GET error: %v", err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, nil
	}
	content, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(string(content)))
	if err != nil {
		return nil, err
	}
	doc.Find("a.index-day-link").Each(func(i int, n *goquery.Selection) {
		if href, ok := n.Attr("href"); ok {
			fmt.Printf("%s: %s\n", n.Text(), href)
		}
	})

	return result, nil
}

// GetAnchors gets the specified web page and returns all anchors found with the specified class.
func GetAnchors(base, url string, anchorClass string) ([]*Anchor, error) {
	var result []*Anchor

	if !strings.HasSuffix(base, "/") {
		base = base + "/"
	}
	resp, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("GET error: %v", err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, nil
	}
	content, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(string(content)))
	if err != nil {
		return nil, err
	}
	doc.Find("a." + anchorClass).Each(func(i int, n *goquery.Selection) {
		if href, ok := n.Attr("href"); ok {
			a := new(Anchor)
			a.Href = href
			a.Text = n.Text()
			result = append(result, a)
		}
	})
	return result, nil
}

type Anchor struct {
	Href string
	Text string
}

func (a *Anchor) GetAnchors(base, anchorClass string) ([]*Anchor, error) {
	if !strings.HasSuffix(base, "/") {
		base = base + "/"
	}
	return GetAnchors(base, base+a.Href, anchorClass)
}
func (a *Anchor) GetAnchorsConcurrent(base, anchorClass string, chAnchors chan []*Anchor, chError chan error) {
	if !strings.HasSuffix(base, "/") {
		base = base + "/"
	}

	anchors, err := GetAnchors(base, base+a.Href, anchorClass)
	if err != nil {
		chError <- err
		return
	}
	chAnchors <- anchors
}
