// Package media provides support for HTML links to musical scores and recordings.
package media

import (
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/enums/mediaTypes"
	"github.com/liturgiko/doxa/pkg/enums/properties"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"path"
	"regexp"
	"strings"
	"sync"
)

type Manager struct {
	mapper          *kvs.KVS
	AudioPathPrefix string
	Enabled         bool // is media enabled for the site?
	PdfPathPrefix   string
	PdfViewer       string
	Counters        map[string]*Counters
	ReverseTkLookup *sync.Map // key = topic.key, value = topic:key
}

func NewMediaManager(mapper *kvs.KVS, reverseTk *sync.Map) *Manager {
	m := new(Manager)
	var err error
	m.mapper = mapper
	m.ReverseTkLookup = reverseTk
	m.Counters = make(map[string]*Counters)
	m.Enabled, err = config.SM.GetElseSetBoolProp(properties.SiteMediaEnabled)
	if err != nil {
		doxlog.Errorf("error getting property site/media/enabled: %v", err)
	}
	var mediaPort string
	mediaPort, err = config.SM.GetElseSetStringProp(properties.SysServerPortsMedia)
	if err != nil {
		doxlog.Errorf("error getting property sys/server/ports/media: %v", err)
	}
	m.AudioPathPrefix, err = config.SM.GetElseSetStringProp(properties.SiteMediaAudioUrl)
	if err != nil {
		doxlog.Errorf("error getting property site/media/enabled: %v", err)
	}
	if len(m.AudioPathPrefix) == 0 {
		m.AudioPathPrefix = fmt.Sprintf("http://localhost:%s", mediaPort)
	}
	m.PdfPathPrefix, err = config.SM.GetElseSetStringProp(properties.SiteMediaPdfUrl)
	if err != nil {
		doxlog.Errorf("error getting property site/media/pdf/url: %v", err)
	}
	if len(m.PdfPathPrefix) == 0 {
		m.PdfPathPrefix = fmt.Sprintf("http://localhost:%s", mediaPort)
	}
	m.PdfViewer, err = config.SM.GetElseSetStringProp(properties.SiteMediaPdfViewer)
	if err != nil {
		doxlog.Errorf("error getting property site/media/pdf/viewer: %v", err)
	}
	if len(m.PdfViewer) > 0 {
		if !strings.HasSuffix(m.PdfViewer, m.PdfPathPrefix) {
			m.PdfViewer = m.PdfViewer + m.PdfPathPrefix
		}
	}
	return m
}

type MetaData struct {
	LinkTarget *MetaEntry
	Tooltips   *MetaEntry
}

func NewMetaData() *MetaData {
	m := new(MetaData)
	m.LinkTarget = new(MetaEntry)
	m.Tooltips = new(MetaEntry)
	return m
}

type MetaEntry struct {
	Audio  string
	BScore string
	EScore string
	WScore string
}

func (m *Manager) MetaData(library string) (*MetaData, error) {
	meta := NewMetaData()
	matcher := kvs.NewMatcher()
	matcher.KP.Dirs.Push("media")
	matcher.KP.Dirs.Push(library)
	matcher.KP.Dirs.Push("html")
	matcher.KP.Dirs.Push("media")
	matcher.Recursive = true
	matcher.UseAsRegEx()
	recs, _, err := m.mapper.Db.GetMatching(*matcher)
	if err != nil {
		return nil, err
	}
	for _, rec := range recs {
		switch rec.KP.Dirs.Get(4) {
		case "target":
			switch rec.KP.KeyParts.Last() {
			case "audio":
				meta.LinkTarget.Audio = rec.Value
			case "byzantine":
				meta.LinkTarget.BScore = rec.Value
			case "enhanced":
				meta.LinkTarget.EScore = rec.Value
			case "western":
				meta.LinkTarget.WScore = rec.Value
			}
		case "tooltip":
			switch rec.KP.KeyParts.Last() {
			case "audio":
				meta.Tooltips.Audio = rec.Value
			case "byzantine":
				meta.Tooltips.BScore = rec.Value
			case "enhanced":
				meta.Tooltips.EScore = rec.Value
			case "western":
				meta.Tooltips.WScore = rec.Value
			}
		}
	}
	return meta, nil
}

// Media holds the collective media Links for a given library/topic/key
type Media struct {
	Audio  []*Link
	BScore []*Link
	EScore []*Link
	WScore []*Link
}
type Link struct {
	DbId       string // the DOXA db ID
	Desc       string // User friendly, from {library}/keys
	ID         string // the composition ID
	Info       string
	Path       string
	PathPrefix string
}

func (l *Link) Copy() *Link {
	newCopy := new(Link)
	newCopy.DbId = l.DbId
	newCopy.ID = l.ID
	newCopy.Info = l.Info
	newCopy.Path = l.Path
	newCopy.PathPrefix = l.PathPrefix
	return newCopy
}
func NewMedia() *Media {
	m := new(Media)
	m.Audio = make([]*Link, 0)
	m.BScore = make([]*Link, 0)
	m.EScore = make([]*Link, 0)
	m.WScore = make([]*Link, 0)
	return m
}
func (m *Media) HasMedia() bool {
	if len(m.Audio) == 0 && len(m.EScore) == 0 && len(m.BScore) == 0 && len(m.WScore) == 0 {
		return false
	}
	return true
}
func (m *Media) Copy() *Media {
	newCopy := NewMedia()
	for _, i := range m.Audio {
		newCopy.Audio = append(newCopy.Audio, i)
	}
	for _, i := range m.BScore {
		newCopy.BScore = append(newCopy.BScore, i)
	}
	for _, i := range m.EScore {
		newCopy.EScore = append(newCopy.EScore, i)
	}
	for _, i := range m.WScore {
		newCopy.WScore = append(newCopy.WScore, i)
	}
	return newCopy
}

// GetKeyDescription returns the description of supplied key as found in the library.
func (m *Manager) GetKeyDescription(library string, key string) string {
	kps := kvs.NewKeyPath()
	kps.Dirs.Push("ltx")
	kps.Dirs.Push(library)
	kps.Dirs.Push("key.descriptors")
	kps.KeyParts.Push(fmt.Sprintf("%s.desc", key))
	rec, _, err := m.mapper.Db.GetResolved(kps)
	if err != nil {
		return key
	}
	if rec != nil {
		return rec.Value
	}
	return key
}

// GetMedia reads the database and returns the media information for the specified library/topic/key.
// Note that media entries drop the .text at the end of key.
func (m *Manager) GetMedia(library, topic, key string) (bool, *Media, error) {
	media := NewMedia()
	matcher := kvs.NewMatcher()
	matcher.KP.Dirs.Push("media")
	matcher.KP.Dirs.Push(library)
	matcher.KP.Dirs.Push(topic)
	matcher.KP.Dirs.Push(key)
	matcher.Recursive = true
	recs, _, err := m.mapper.Db.GetMatching(*matcher)
	if err != nil {
		// always return an empty media if no media exists.  Otherwise, the html shifts the media to the left column.
		return false, media, nil
	}
	if len(recs) == 0 {
		return false, media, nil
	}
	/*
	  rec.KP.Dirs:
	     0 = media
	     1 = library, e.g. gr_gr_cog
	     2 = topic
	     3 = key
	     4 = composition ID
	     5 = media type, e.g. audio, score
	     6 = b | e | w if type is score, instance number if audio
	     7 = instance number, usually 1, if score. Otherwise, does not exist.
	  rec.KP.KeyParts[0] = link.info | link.path
	*/

	link := new(Link)

	for _, rec := range recs {
		var mediaType, scoreType string
		if rec.KP.Dirs.Size() < 6 {
			return true, nil, fmt.Errorf("%s dirs less than 6", rec.KP.Path())
		}
		link.DbId = rec.KP.DirPath()
		link.Desc = m.GetKeyDescription(rec.KP.Dirs.Get(1), rec.KP.Dirs.Get(3))
		link.ID = rec.KP.Dirs.Get(4)
		mediaType = rec.KP.Dirs.Get(5)
		if mediaType == "score" {
			if rec.KP.Dirs.Size() < 7 {
				return true, nil, fmt.Errorf("%s missing score type, e.g. b, e, or w", rec.KP.Path())
			}
			scoreType = rec.KP.Dirs.Get(6)
		}
		switch rec.KP.KeyParts.First() {
		case "link.info":
			link.Info = rec.Value
		case "link.path":
			link.Path = rec.Value
			switch mediaType {
			case "audio":
				link.PathPrefix = m.AudioPathPrefix
				media.Audio = append(media.Audio, link)
			case "score":
				switch scoreType {
				case "b":
					if strings.HasSuffix(link.Path, "pdf") {
						link.PathPrefix = m.PdfViewer
					} else {
						link.PathPrefix = m.PdfPathPrefix
					}
					media.BScore = append(media.BScore, link)
				case "e":
					if strings.HasSuffix(link.Path, "pdf") {
						link.PathPrefix = m.PdfViewer
					} else {
						link.PathPrefix = m.PdfPathPrefix
					}
					media.EScore = append(media.EScore, link)
				case "w":
					if strings.HasSuffix(link.Path, "pdf") {
						link.PathPrefix = m.PdfViewer
					} else {
						link.PathPrefix = m.PdfPathPrefix
					}
					media.WScore = append(media.WScore, link)
				default:
					return true, nil, fmt.Errorf("%s unknown score type. Must be b, e, or w", rec.KP.Path())
				}
			}
			link = new(Link)

		default:
			return true, nil, fmt.Errorf("%s unknown key type. Must be link.info or link.path", rec.KP.Path())
		}
	}
	return true, media, nil
}

// Counters for the occurrence number for each composition
type Counters struct {
	Audio  int // Audio file counter
	BScore int // Byzantine Score counter
	EScore int // Enhanced Byzantine Score counter
	WScore int // Western Score counter.
}

// Tuple holds two lines of an AGES media:
// the arranger | singer and the path.
// Each ages line has a topic + key + composition ID.
type Tuple struct {
	ID       string       // composition ID
	KP       *kvs.KeyPath // record path
	Index    int          // occurrence number for multiple audio or scores for this composition
	LinkInfo string       // becomes the visible text in an HTML link
	LinkPath string       // url path for the html link to media
	Type     mediaTypes.MediaType
	Complete bool // true if LinkPath is populated.
}

func NewTuple() *Tuple {
	t := new(Tuple)
	t.KP = kvs.NewKeyPath()
	t.KP.Dirs.Push("media")
	return t
}

// ToRecords returns a LinkInfo dbr and LinkPath dbr
func (t *Tuple) ToRecords() (*kvs.DbR, *kvs.DbR) {
	dbrInfo := kvs.NewDbR()
	dbrPath := kvs.NewDbR()

	switch t.Type {
	case mediaTypes.Audio:
		t.KP.Dirs.Push("audio")
		t.KP.Dirs.Push(fmt.Sprintf("%d", t.Index))
	case mediaTypes.ScoreByz:
		t.KP.Dirs.Push("score")
		t.KP.Dirs.Push("b")
		t.KP.Dirs.Push(fmt.Sprintf("%d", t.Index))
	case mediaTypes.ScoreByzE:
		t.KP.Dirs.Push("score")
		t.KP.Dirs.Push("e")
		t.KP.Dirs.Push(fmt.Sprintf("%d", t.Index))
	case mediaTypes.ScoreWestern:
		t.KP.Dirs.Push("score")
		t.KP.Dirs.Push("w")
		t.KP.Dirs.Push(fmt.Sprintf("%d", t.Index))
	}
	// populate record for link info
	dbrInfo.KP = t.KP.Copy()
	dbrInfo.KP.KeyParts.Push("link.info")
	dbrInfo.Value = t.LinkInfo

	// populate record for link path
	dbrPath.KP = t.KP.Copy()
	dbrPath.KP.KeyParts.Push("link.path")
	dbrPath.Value = t.LinkPath

	// text media use generic link info.
	// So, we return nil for that part.
	if t.Type == mediaTypes.Text {
		return nil, dbrPath
	}
	return dbrInfo, dbrPath
}

// LoadFromAresMediaFile reads the specified AGES ares file
// which must be a media-map, and loads the media
// records in the database.
func (m *Manager) LoadFromAresMediaFile(f string) error {
	if !ltfile.FileExists(f) {
		msg := fmt.Sprintf("%s not found", f)
		doxlog.Errorf(msg)
		return fmt.Errorf("%s", msg)
	}
	// get the name of the file, so we can extract the library part.
	// for example, media_en_US_public.ares.
	// We want the en_US_public part.
	fName := strings.ToLower(path.Base(f))
	if !strings.HasPrefix(fName, "media_") {
		msg := fmt.Sprintf("%s does not start with media_", f)
		doxlog.Errorf(msg)
		return fmt.Errorf("%s", msg)
	}
	if !strings.HasSuffix(fName, ".ares") {
		msg := fmt.Sprintf("%s does not end with .ares", f)
		doxlog.Errorf(msg)
		return fmt.Errorf("%s", msg)
	}
	fName = strings.ToLower(fName[6 : len(fName)-5])
	_, err := ltstring.ToLibraryParts(fName)
	if err != nil {
		msg := fmt.Sprintf("media library %s error: %v", f, err)
		doxlog.Errorf(msg)
		return fmt.Errorf("%s", msg)
	}
	var cName string
	cName, err = ltstring.CountryFromLibrary(fName)
	if err != nil {
		msg := fmt.Sprintf("media library %s country code error: %v", f, err)
		doxlog.Errorf(msg)
		return fmt.Errorf("%s", msg)
	}
	if cName != "redirects" {
		return nil // skip it
	}
	// recs holds the records to be batch written to the database
	var recs []*kvs.DbR

	lines, err := ltfile.GetFileLines(f)
	if err != nil {
		msg := fmt.Sprintf("error reading %s: %v", f, err)
		doxlog.Errorf(msg)
		return fmt.Errorf("%s", msg)
	}
	if len(lines) == 0 {
		return fmt.Errorf("media-map file %s is empty", f)
	}

	// tuple holds information for two related lines: the link.text and link.path
	tuple := NewTuple()
	// add the library name to the dirs
	tuple.KP.Dirs.Push(fName)
	// make a copy so that we can delete any existing db dir before writing records
	delKp := kvs.NewKeyPath()
	delKp.Dirs = tuple.KP.Dirs.Copy()

	// inCommentBlock allows us to ignore multi-line comment blocks
	var inCommentBlock bool

	var notInKingRing = make(map[string]string)

	// process each line in the media-map file.
	for i, line := range lines {
		line = strings.TrimSpace(line)
		// iterate through comment blocks
		if line == "/*" {
			inCommentBlock = true
		}
		if inCommentBlock {
			if line == "*/" || strings.HasSuffix(line, "*/") {
				inCommentBlock = false
			}
			continue
		}
		// check if this is a meta line.  If so, we handle differently.
		if strings.HasPrefix(line, "html.") {
			//dbr, err := m.ParseMeta(line, fName)
			//if err == nil {
			//	recs = append(recs, dbr)
			//} else {
			//	msg := fmt.Sprintf("error parsing %s:%d: %v", fName, i, err)
			//	doxlog.Errorf(msg)
			//	return fmt.Errorf("%s", msg)
			//}
			continue
		}
		var missing string
		missing, err = m.Parse(i, line, tuple)
		if err != nil {
			msg := fmt.Sprintf("error parsing %s:%d: %v", fName, i, err)
			doxlog.Errorf(msg)
			continue
		}
		if len(missing) > 0 {
			var val string
			var ok bool
			if val, ok = notInKingRing[missing]; ok {
				val = fmt.Sprintf("%s, %d", val, i)
			} else {
				val = fmt.Sprintf("%d", i)
			}
			notInKingRing[missing] = val
			continue
		}
		// normally a tuple is complete for every 2 lines parsed.
		if tuple.Complete {
			// Set occurrence index in Tuple.
			// There is a map that uses the tuple.KP.Dirs as its key
			// and a counters instance for its value.
			// We retrieve the counters for this composition and increment
			// the one corresponding to the media type.
			// We use that value to set the tuple.Index value.
			var counters *Counters
			counters = m.Counters[tuple.KP.Dirs.Join("/")]
			if counters == nil {
				counters = new(Counters)
			}
			switch tuple.Type {
			case mediaTypes.Audio:
				counters.Audio++
				tuple.Index = counters.Audio
			case mediaTypes.ScoreByz:
				counters.BScore++
				tuple.Index = counters.BScore
			case mediaTypes.ScoreByzE:
				counters.EScore++
				tuple.Index = counters.EScore
			case mediaTypes.ScoreWestern:
				counters.WScore++
				tuple.Index = counters.WScore
			}
			// save the updated counters for this composition
			m.Counters[tuple.KP.Dirs.Join("/")] = counters
			// get the two records (text and path)
			// and add them to the recs slice
			dbrText, dbrPath := tuple.ToRecords()
			if dbrText != nil {
				recs = append(recs, dbrText)
			}
			recs = append(recs, dbrPath)

			// initialize a new tuple
			tuple = NewTuple()
			tuple.KP.Dirs.Push(fName)
		}
	}
	for k, v := range notInKingRing {
		doxlog.Error(fmt.Sprintf("media file line(s) %s, topic:key %s.text does not exist in keyring", v, k))
	}
	if len(notInKingRing) > 0 {
		doxlog.Errorf("%d keys in media map but not in key ring", len(notInKingRing))
	}
	// delete existing db dir
	if m.mapper.Db.Exists(delKp) {
		err = m.mapper.Db.Delete(*delKp)
		if err != nil {
			msg := fmt.Sprintf("error deleting %s: %v", delKp.Path(), err)
			doxlog.Errorf(msg)
			return fmt.Errorf("%s", msg)
		}
	}
	// write the recs to the database, returning the result to the caller
	return m.mapper.Db.Batch(recs)
}

var compositionRegEx = regexp.MustCompile(`\.c\d{1,8}\.`)

// Parse analyzes each line and populates the tuple
// with extracted information.
func (m *Manager) Parse(lineNbr int, line string, tuple *Tuple) (notInKeyRing string, err error) {
	// ignore empty line
	if len(strings.TrimSpace(line)) == 0 {
		return "", nil
	}
	// ignore comment line
	if strings.HasPrefix(strings.TrimSpace(line), "//") {
		return "", nil
	}
	// ignore AGES ares ID line
	if strings.HasPrefix(line, "A_Resource_Whose_Name") {
		return "", nil
	}
	// ignore AGES model lines
	if strings.HasPrefix(line, "key.model") {
		return "", nil
	}
	// ignore AGES html keys
	if strings.HasPrefix(line, "html") {
		return "", nil
	}
	var left, right string
	// problem: sometimes the link path has an = sign.
	// so, we can't simply split on =.
	// We need to split the line using the first = sign.
	// Do so using an index of the 1st occurrence of = sign.
	eqIndex := strings.Index(line, "=")
	if eqIndex > -1 {
		left = strings.TrimSpace(line[:eqIndex])
		right = strings.TrimSpace(line[eqIndex:])
	} else {
		parts := strings.Split(line, "=")
		if len(parts) != 2 {
			return "", nil
		}
		left = strings.TrimSpace(parts[0])
		right = strings.TrimSpace(parts[1])
	}
	// Ares values are quoted. Remove surrounding quotes.
	if len(right) > 3 {
		right = right[3 : len(right)-1]
	} else {
		return "", fmt.Errorf("%s", line)
	}

	// debug
	if strings.HasPrefix(left, "ho.ho10.hoVE.Ps103.text.url.path") ||
		strings.HasPrefix(left, "eu.lichrysbasil.euLI.Key1300.c1.a2.singer") {
		_ = fmt.Sprintf("TODO DELETE ME")
	}

	// filter for suffix arranger, singer, and path.  Ignore all others.
	if strings.HasSuffix(left, "arranger") || strings.HasSuffix(left, "singer") {
		tuple.LinkInfo = right
		// Get rid of trailing forward slash if exists.x
		if strings.HasSuffix(tuple.LinkInfo, "/") {
			tuple.LinkInfo = tuple.LinkInfo[:len(tuple.LinkInfo)-1]
		}
		// Get rid of the word `enhanced` used by AGES.
		// Doxa uses a different ico for enhanced.
		l := strings.Index(tuple.LinkInfo, "enhanced")
		if l > -1 {
			tuple.LinkInfo = strings.TrimSpace(tuple.LinkInfo[:l])
			if strings.HasSuffix(tuple.LinkInfo, "-") {
				tuple.LinkInfo = strings.TrimSpace(tuple.LinkInfo[:len(tuple.LinkInfo)-1])
			}
		}
		if len(tuple.LinkInfo) == 0 {
			tuple.LinkInfo = "NA"
		}
	} else if strings.HasSuffix(left, "path") {
		tuple.LinkPath = right
		if strings.HasPrefix(tuple.LinkPath, "https") {
			i := strings.Index(tuple.LinkPath, "/media/")
			if i > -1 {
				tuple.LinkPath = tuple.LinkPath[i:]
			}
		}
		if strings.Contains(line, "text.url.path") {
			tuple.Type = mediaTypes.Text
			tuple.Complete = true
		}
	} else {
		return "", nil
	}
	// tuple is a pointer, and parse will update its object using data from the line
	if len(tuple.ID) == 0 { // we have not populated the KP dir
		var topicKey string
		var testTopicKey string
		var compositionId string
		indexes := compositionRegEx.FindStringIndex(line)
		if len(indexes) > 0 {
			i := indexes[0] - 1
			testTopicKey = line[:i+1]
			compositionId = compositionRegEx.FindString(line)
			compositionId = compositionId[1 : len(compositionId)-1]
		}
		if len(testTopicKey) == 0 {
			return "", fmt.Errorf("could not identify composition number in media map line %d %s", lineNbr, left)
		}
		if topicKeyGeneric, ok := m.ReverseTkLookup.Load(testTopicKey); ok {
			topicKey = topicKeyGeneric.(string)
			tuple.ID = topicKey
			parts := strings.Split(topicKey, ":")
			if len(parts) == 2 {
				if tuple.KP.Dirs.Last() != parts[1] {
					tuple.KP.Dirs.Push(parts[0])
					tuple.KP.Dirs.Push(parts[1])
					tuple.KP.Dirs.Push(compositionId)
				}
			}
		} else {
			return testTopicKey, nil
		}
	}
	parts := strings.Split(left, ".")
	for i, s := range parts {
		switch s {
		case "arranger":
			switch parts[i-1] {
			case "b":
				tuple.Type = mediaTypes.ScoreByz
			case "w":
				tuple.Type = mediaTypes.ScoreWestern
			default:
				return "", fmt.Errorf("%s: expected score type", left)
			}
		case "path":
			// check to see if this "enhanced" byzantine score
			if strings.Contains(tuple.LinkPath, "/b-e/") {
				tuple.Type = mediaTypes.ScoreByzE
			}
		case "singer":
			tuple.Type = mediaTypes.Audio
		}
	}
	// sometimes AGES has path key-values, without an arranger or singer.
	// To handle this, we treat a tuple as complete if the LinkPath is populated.
	tuple.Complete = len(tuple.LinkPath) > 0
	return "", nil
}

// ParseMeta data for html
func (m *Manager) ParseMeta(line string, library string) (*kvs.DbR, error) {
	line = strings.TrimSpace(line)
	if strings.HasPrefix(line, "/") {
		return nil, fmt.Errorf("%s is commented out", line)
	}
	dbr := kvs.NewDbR()
	dbr.KP.Dirs.Push("media")
	dbr.KP.Dirs.Push(library)

	var left, right string
	eqIndex := strings.Index(line, "=")
	if eqIndex > -1 {
		left = strings.TrimSpace(line[:eqIndex])
		right = strings.TrimSpace(line[eqIndex:])
	} else {
		parts := strings.Split(line, "=")
		if len(parts) != 2 {
			return nil, fmt.Errorf("%s missing equal sign", line)
		}
		left = strings.TrimSpace(parts[0])
		right = strings.TrimSpace(parts[1])
	}
	// Ares values are quoted. Remove surrounding quotes.
	right = right[3 : len(right)-1]
	parts := strings.Split(left, ".")
	j := len(parts) - 1
	for i := 0; i < j; i++ {
		dbr.KP.Dirs.Push(parts[i])
	}
	dbr.KP.KeyParts.Push(parts[j])
	dbr.Value = right
	return dbr, nil
}
