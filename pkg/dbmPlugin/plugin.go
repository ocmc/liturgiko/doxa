package dbmPlugin

import (
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/catalog"
	"github.com/liturgiko/doxa/pkg/config"
)

type WithEdit interface {
	// Get is used instead of kvs.Get
	Get(kp *kvs.KeyPath) (*kvs.DbR, error)
	// ValidPath indicates that the plugin applies in the given path
	ValidPath(kp *kvs.KeyPath) bool
	// AfterEdit makes any modifications to input string and returns resulting output string, and wether the record can be saved
	AfterEdit(str string) (string, bool)
	// Put is used instead of kvs.Put
	Put(rec *kvs.DbR) error
	// PreserveWhitespace tells the editor which to use
	PreserveWhitespace() bool
	// SetKVS can be used to enable the implementation to be called without initialization
	SetKVS(k *kvs.KVS)
	SetLiturgicalDb(k *kvs.KVS)
	SetCatalogManager(m *catalog.Manager)
}

type Command interface {
	// ValidPath determines if the current keypath is one that the command can run in
	ValidPath(kp *kvs.KeyPath) bool
	// Abr returns the abbreviation used to call the command.
	Abr() string
	// DescShort returns a short explanation of what the command does
	DescShort() string
	// Execute is what runs your command
	Execute(args []string)
	// OnSetup is called prior to execution, and provides the working directory keypath, and the kvs
	// if false is returned Execute will not be called
	// Note that OnSetup id called after ValidPath, Abr() and DescShort
	OnSetup(currentPath *kvs.KeyPath, sys, db *kvs.KVS, paths *config.Paths, manager *catalog.Manager) bool
}
