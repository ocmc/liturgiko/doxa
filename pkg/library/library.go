// Package library provides support for Doxa libraries with the format {iso-language-code}_{iso-country-code}_{realm}
package library

import (
	"fmt"
	"github.com/biter777/countries"
	"github.com/emvi/iso-639-1"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"strings"
)

type Library struct {
	Name         string
	LanguageCode string
	LanguageName string
	CountryCode  string
	CountryName  string
	Realm        string
	RealmDesc    string
}

func NewLibrary(library string) (*Library, error) {
	l := new(Library)
	l.Name = library
	err := l.parse()
	if err != nil {
		return nil, err
	}
	return l, nil
}
func (l *Library) parse() error {
	parts, err := ltstring.ToLibraryParts(l.Name)
	if err != nil {
		return fmt.Errorf("%s is not a valid library name: expect {iso-language-code}_{iso-country-code}_{realm}", l.Name)
	}
	l.LanguageCode = parts[0]
	if l.LanguageCode == "gr" {
		l.LanguageName = "Biblical or Liturgical Greek (Βιβλική ή Λειτουργική Ελληνική)"
	} else {
		lc := iso6391.FromCode(l.LanguageCode)
		if l.LanguageCode == "en" {
			l.LanguageName = fmt.Sprintf("%s", lc.Name)
		} else {
			l.LanguageName = fmt.Sprintf("%s (%s)", lc.Name, lc.NativeName)
		}
	}
	l.CountryCode = parts[1]
	cc := countries.ByName(l.CountryCode)
	l.CountryName = cc.String()
	l.Realm = parts[2]
	return nil
}
func (l *Library) Description(realmDesc string) string {
	l.RealmDesc = realmDesc
	sb := strings.Builder{}
	if len(l.LanguageName) > 0 && l.LanguageName != "unknown" {
		sb.WriteString(l.LanguageName)
	}
	if len(l.CountryName) > 0 && l.CountryName != "unknown" {
		if sb.Len() > 0 {
			sb.WriteString(", ")
		}
		sb.WriteString(l.CountryName)
	}
	if len(l.RealmDesc) > 0 && l.RealmDesc != "unknown" {
		if sb.Len() > 0 {
			sb.WriteString(", ")
		}
		sb.WriteString(l.RealmDesc)
	} else {
		sb.WriteString(l.Realm)
	}
	return sb.String()
}

// ConvertRedirectsInLibrary creates a tab-delimited (.tsv) file
// for import into Doxa, by replacing the named libraries in fileIn
// with the newLibrary.
// newLibrary must have the format {iso language code}_{iso country code}_{realm}
// fileIn must be a tab-delimited file (id\tvalue), typically created by exporting a library from Doxa
// fileOut the new tab-delimited file, should be written to the imports folder in a DCS project.
// ignoreLibs indicates which libraries are NOT to be replaced by newLibrary
//func ConvertRedirectsInLibrary(newLibrary, fileIn, fileOut string, ignoreLibs []string) error {
//	var lines []string
//	var err error
//	if !strings.HasSuffix(fileIn, ".tsv") {
//		return fmt.Errorf("%s must be a tab delimited file", fileIn)
//	}
//	if !strings.HasSuffix(fileOut, ".tsv") {
//		return fmt.Errorf("%s must be a tab delimited file", fileOut)
//	}
//	if lines, err = ltfile.GetFileLines(fileIn); err != nil {
//		return fmt.Errorf("error reading %s: %v", fileIn, err)
//	}
//	var linesOut []string
//	for _, l := range lines {
//
//	}
//	return nil
//}

// GetRedirectLibraries returns the unique set of libraries redirect to in fileIn
//func GetRedirectLibraries(fileIn string) ([]string, error) {
//	var redirectLibs []string
//	var mset mapset.Set[string]
//	var err error
//	if !strings.HasSuffix(fileIn, ".tsv") {
//		return nil, fmt.Errorf("%s must be a tab delimited file", fileIn)
//	}
//	var linesOut []string
//	for _, l := range lines {
//
//	}
//	set1 := mapset.NewSet[string](c.Sites[0].ServiceUrls...)
//
//	return redirectLibs, err
//}
