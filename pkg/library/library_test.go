package library

import (
	"fmt"
	"testing"
)

func TestNewLibrary(t *testing.T) {
	l, err := NewLibrary("gr_gr_cog")
	if err != nil {
		t.Error(err)
	}
	fmt.Println(l.Description("Common Orthodox Greek"))
}
