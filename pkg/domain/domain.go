// Package domain provides a struct for marshalling to domain.json, written to repositories that use the Doxa languageCode_countryCode_domain scheme.
package domain

import (
	"encoding/json"
	"fmt"
	"github.com/liturgiko/doxa/pkg/enums/licenseTypes"
	library2 "github.com/liturgiko/doxa/pkg/library"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"path"
	"strings"
)

const FILENAME = "domain.json"

type Domain struct {
	Language    string
	Country     string
	Realm       string
	Description string
	Copyright   string
	License     string
	Format      string
}

func (d *Domain) Exists(dirPath string) bool {
	return ltfile.FileExists(path.Join(dirPath, FILENAME))
}

// Read reads the domain.json file in the specified directory and returns a populated Domain object
// Parameters:
//
//	dirPath path to the directory to read from, without the file name.  It is automatically added.
func Read(dirPath string) (*Domain, error) {
	fileIn := dirPath
	_, filename := path.Split(fileIn)
	if filename != FILENAME {
		fileIn = path.Join(dirPath, FILENAME)
	}
	d := new(Domain)
	jsonStr, err := ltfile.GetFileContent(fileIn)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal([]byte(jsonStr), &d)
	if err != nil {
		return nil, err
	}
	return d, nil
}
func (d *Domain) TitleForLicenseType(licenseType string) string {
	return licenseTypes.TitleForType(licenseTypes.TypeForString(licenseType))
}

// Write serializes the Domain as domain.json in the specified directory.
// Parameters:
//
//	dirPath path to the directory to write to, without the file name.  It is automatically added.
func (d *Domain) Write(dirPath string) error {
	fileOut := dirPath
	_, filename := path.Split(fileOut)
	if filename != FILENAME {
		fileOut = path.Join(dirPath, FILENAME)
	}
	j, err := json.MarshalIndent(d, "", " ")
	if err != nil {
		return err
	}
	err = ltfile.WriteFile(fileOut, string(j))
	if err != nil {
		return err
	}
	return nil
}
func (d *Domain) SetNameDerivableFields(dirName, library string) error {
	var l *library2.Library
	var err error
	if len(library) > 0 {
		l, err = library2.NewLibrary(library)
		if err == nil {
			d.Language = l.LanguageCode
			d.Country = l.CountryCode
			d.Realm = l.Realm
		} else {
			parts, _ := ltstring.ToLibraryParts(library)
			if parts != nil {
				if len(parts) == 3 {
					d.Language = parts[0]
					d.Country = parts[1]
					d.Realm = parts[2]
				}
			}
		}
	}
	d.Description, err = d.DescriptionFor(dirName)
	if err != nil {
		return err
	}
	switch dirName {
	case "btx":
		d.Description = fmt.Sprintf("%s %s, %s, %s", l.LanguageName, d.Description, d.Realm, l.CountryName)
	case "ltx":
		d.Description = fmt.Sprintf("%s %s, %s, %s", l.LanguageName, d.Description, d.Copyright, l.CountryName)
	}
	d.Format = FormatFor(dirName)
	return nil
}
func (d *Domain) DescriptionFor(dirName string) (string, error) {
	sb := strings.Builder{}
	switch dirName {
	case "assets":
		sb.WriteString("liturgical website asset files")
		if len(d.Copyright) > 0 {
			sb.WriteString(fmt.Sprintf(" by %s", d.Copyright))
		}
	case "btx":
		sb.WriteString("translation of the Bible")
	case "configs":
		sb.WriteString("doxa configuration files")
		if len(d.Copyright) > 0 {
			sb.WriteString(fmt.Sprintf(" by %s", d.Copyright))
		}
	case "ltx":
		sb.WriteString("translation of the Orthodox Christian liturgical texts")
	case "media":
		sb.WriteString("doxa media map files")
		if len(d.Copyright) > 0 {
			sb.WriteString(fmt.Sprintf(" by %s", d.Copyright))
		}
	case "templates":
		sb.WriteString("doxa template files")
		if len(d.Copyright) > 0 {
			sb.WriteString(fmt.Sprintf(" by %s", d.Copyright))
		}
	case "websites":
		sb.WriteString("doxa generated website files")
		if len(d.Copyright) > 0 {
			sb.WriteString(fmt.Sprintf(" by %s", d.Copyright))
		}
	default:
		sb.WriteString("unknown")
	}
	return sb.String(), nil
}
func FormatFor(dirName string) string {
	switch dirName {
	case "assets":
		return "css, fonts, javascript, graphics"
	case "btx", "configs", "ltx", "media":
		return "doxa key = value text files with .dkv extension"
	case "templates":
		return "doxa templates written in Liturgical Markup Language with .lml extension"
	case "websites":
		return "html, css, fonts, javascript, graphics, pdf"
	default:
		return "unknown"
	}
}
