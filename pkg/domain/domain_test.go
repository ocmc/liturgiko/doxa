package domain

import (
	"fmt"
	"testing"
)

func TestDomain_Write(t *testing.T) {
	d := new(Domain)
	d.Copyright = "Fr Seraphim Dedes"
	d.License = d.TitleForLicenseType("BY-NC-SA")
	d.SetNameDerivableFields("btx", "en_uk_kjv")
	err := d.Write("")
	if err != nil {
		t.Error(err)
	}
	fmt.Println(d.Description)
	r, err := Read("")
	if err != nil {
		t.Error(err)
	}
	if r.Language != d.Language {
		t.Errorf("language not equal")
	}
}
