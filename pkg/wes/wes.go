// Package wes provide web session management
package wes

import (
	"encoding/base64"
	"fmt"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/vault"
	"sync"
	"time"
)

var SessionLength time.Duration = 15 * time.Minute

type Session struct {
	ID        string
	Username  string
	LastLogin time.Time
	ExpiresAt time.Time
}
type SessionManager struct {
	BySessionId     map[string]*Session // key = session id, value = *Session
	BySessionIdLock sync.Mutex
	ByUsername      map[string]string // key = username, value = session ID
	ByUserNameLock  sync.Mutex
}

func NewManager() *SessionManager {
	s := new(SessionManager)
	s.BySessionId = make(map[string]*Session)
	s.ByUsername = make(map[string]string)
	return s
}
func (m *SessionManager) Add(username string) string {

	var sessionId string
	var ok bool

	if sessionId, ok = m.ByUsername[username]; !ok {
		s := new(Session)
		s.Username = username
		s.ID = base64.StdEncoding.EncodeToString(vault.NewEncryptionKey())
		sessionId = s.ID
		s.LastLogin = time.Now()
		s.ExpiresAt = s.LastLogin.Add(SessionLength)
		m.BySessionIdLock.Lock()
		m.BySessionId[s.ID] = s
		m.BySessionIdLock.Unlock()
		m.ByUserNameLock.Lock()
		m.ByUsername[username] = s.ID
		m.ByUserNameLock.Unlock()
		doxlog.Infof("%s logged in, assigned session ID %s", s.Username, s.ID)
	}
	return sessionId
}
func (m *SessionManager) RemoveExpired() {
	var sidsToRemove []string
	var uidsToRemove []string
	for _, session := range m.BySessionId {
		if time.Now().After(session.ExpiresAt) {
			user := session.Username
			msg := fmt.Sprintf("session expired for %s", user)
			doxlog.Info(msg)
			sidsToRemove = append(sidsToRemove, session.ID)
			uidsToRemove = append(uidsToRemove, session.Username)
		}
	}
	m.BySessionIdLock.Lock()
	defer m.BySessionIdLock.Unlock()
	for _, i := range sidsToRemove {
		delete(m.BySessionId, i)
	}
	m.ByUserNameLock.Lock()
	defer m.ByUserNameLock.Unlock()
	for _, i := range uidsToRemove {
		delete(m.ByUsername, i)
		doxlog.Infof("removed session for %s", i)
	}
}
func (m *SessionManager) GetSessionByUserName(username string) (*Session, bool) {
	var sessionId string
	var ok bool
	if sessionId, ok = m.ByUsername[username]; ok {
		return m.GetSessionById(sessionId)
	}
	return nil, false
}

// Delete removes the session from BySessionId and the user from ByUserName
func (m *SessionManager) Delete(sessionId string) {
	if session, ok := m.GetSessionById(sessionId); ok {
		m.ByUserNameLock.Lock()
		defer m.ByUserNameLock.Unlock()
		delete(m.ByUsername, session.Username)
		m.BySessionIdLock.Lock()
		defer m.BySessionIdLock.Unlock()
		delete(m.BySessionId, session.ID)
		doxlog.Infof("user %s logged out, session ID %s", session.Username, session.ID)
	} else {
		doxlog.Errorf("delete could not find session ID %s", sessionId)
	}
}

// GetSessionById returns true if the session exists.
// If it does exist, the session expiration is extended another period of time
func (m *SessionManager) GetSessionById(sessionId string) (*Session, bool) {
	var ok bool
	var session *Session
	if session, ok = m.BySessionId[sessionId]; ok {
		session.ExpiresAt = time.Now().Add(SessionLength)
		m.BySessionIdLock.Lock()
		defer m.BySessionIdLock.Unlock()
		m.BySessionId[sessionId] = session
		return session, true
	}
	return nil, false
}
