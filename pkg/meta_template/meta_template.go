// Package meta_template creates meta-templates from existing remote websites that have been generated using Doxa.
package meta_template

import (
	"encoding/json"
	"fmt"
	"github.com/liturgiko/doxa/pkg/ages/goarchdcs"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"net/url"
	"path"
	"strings"
	"time"
)

func CreateMetaTemplates(remoteSites []string, metaSysPath string) {
	for _, site := range remoteSites {
		if !strings.HasPrefix(site, "https") {
			site = fmt.Sprintf("https://%s", site)
		}
		if !strings.HasSuffix(site, "/") {
			site = site + "/"
		}
		siteUrl, err := url.Parse(site)
		if err != nil {
			doxlog.Errorf("%v", err)
			return
		}
		hostName := strings.TrimPrefix(siteUrl.Hostname(), "www.")
		pathOut := path.Join(metaSysPath, hostName, "meta.json")
		// if we have already created the json file today, we won't do it again.
		if ltfile.FileExists(pathOut) {
			var modifiedToday bool
			modifiedToday, err = ltfile.ModifiedToday(pathOut)
			if modifiedToday {
				continue
			}
		}
		if hostName == "dcs.goarch.org" {
			createMetaTemplatesFromAlwbSite(site, hostName, pathOut)
		} else {
			// TODO: handle remote doxa dcs
		}
	}
}

func createMetaTemplatesFromAlwbSite(site, host string, pathOut string) {
	var err error
	doxlog.Infof("creating meta-templates from %s\n", site)
	// if we have already created the json file today, we won't do it again.
	if ltfile.FileExists(pathOut) {
		var modifiedToday bool
		modifiedToday, err = ltfile.ModifiedToday(pathOut)
		if modifiedToday {
			doxlog.Infof("json already created today for site %s\n", site)
			return
		}
	}
	if host == "dcs.goarch.org" {
		// handle ALWB generated dcs
		var s *goarchdcs.Scraper
		// TODO:
		s, err = goarchdcs.NewScraper(site, "servicesindex.json", nil, nil, nil, time.Now(), time.Now(), nil, nil)
		if err != nil {
			doxlog.Infof("error creating Scraper for %s: %v\n", site, err)
			return
		}
		err = s.GetMetaTemplates()
		if err != nil {
			doxlog.Infof("error getting meta-templates for %s: %v\n", site, err)
			return
		}
		var j []byte
		j, err = json.Marshal(s)
		if err != nil {
			doxlog.Infof("error creating json for %s: %v\n", site, err)
			return
		}
		err = ltfile.WriteFile(pathOut, string(j))
		if err != nil {
			doxlog.Infof("error writing json for %s to %s: %v\n", site, pathOut, err)
			return
		}
	} else {
		doxlog.Infof("%s is not an ALWB generated dcs\n", site)
		return
	}
	doxlog.Infof("finished creating meta-templates for site %s", site)
}
