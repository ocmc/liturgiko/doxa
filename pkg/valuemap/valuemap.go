package valuemap

import (
	"github.com/liturgiko/doxa/pkg/atempl"
	"sync"
)

// ValueMapper provides synchronization for writes to a map of template.TKVal.
// Be sure to use the NewValueMapper function to get an instance.
type ValueMapper struct {
	Values map[string]*atempl.TKVal
	Mutex  *sync.Mutex
}

func NewValueMapper() *ValueMapper {
	m := new(ValueMapper)
	m.Values = make(map[string]*atempl.TKVal)
	m.Mutex = &sync.Mutex{}
	return m
}
func (m *ValueMapper) Add(topicKey string, tkVal *atempl.TKVal) {
	m.Mutex.Lock()
	m.Values[topicKey] = tkVal
	m.Mutex.Unlock()
}
func (m *ValueMapper) Get(topicKey string) (*atempl.TKVal, bool) {
	m.Mutex.Lock()
	v := m.Values[topicKey]
	m.Mutex.Unlock()
	if v == nil {
		return nil, false
	}
	return v, true
}
