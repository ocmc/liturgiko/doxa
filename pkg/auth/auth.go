// Package auth manages user authentication and authorization when Doxa is running in the cloud.
package auth

import "time"

type User struct {
	Title       string
	NameFirst   string
	NameFamily  string
	Email       string
	LastLogin   time.Time
	TotalLogins int
}
type Library struct {
}
