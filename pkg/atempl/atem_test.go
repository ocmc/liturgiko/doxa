package atempl

import (
	"encoding/json"
	"fmt"
	"github.com/liturgiko/doxa/pkg/css"
	"github.com/liturgiko/doxa/pkg/enums/idTypes"
	"github.com/liturgiko/doxa/pkg/enums/statuses"
	"github.com/liturgiko/doxa/pkg/enums/templateTypes"
	"testing"
	"time"
)

func TestParagraph(t *testing.T) {
	var p MetaRow
	s1 := new(MetaSpan)
	_ = s1.AddChildSpan(NewNid("Literal Text"))
	_ = s1.AddChildSpan(NewSid("actors/Priest"))
	_ = s1.AddChildSpan(NewRid("oc.*/ocVE.ApolTheotokionVM.text"))
	p.AddMetaSpan(s1)
	j, err := json.MarshalIndent(p, "", " ")
	if err != nil {
		t.Error(err)
	}
	fmt.Println(string(j))

}
func TestNewTableLayoutFromMatrix(t *testing.T) {
	var matrix [][]string
	var row, col int
	nbrCols := 3
	libs := []string{"gr_gr_cog", "en_us_public", "spa_mex_gr"}
	for row = 0; row < 10; row++ {
		var libraries []string
		for col = 0; col < nbrCols; col++ {
			libraries = append(libraries, libs[col])
		}
		matrix = append(matrix, libraries)
	}
	layout, err := NewTableLayoutFromMatrix(matrix)
	if err != nil {
		t.Error(err)
	}
	var jsonData []byte
	jsonData, err = json.MarshalIndent(layout, "", " ")
	fmt.Printf("%s", string(jsonData))
}
func TestALT(t *testing.T) {
	alt := new(ATEM)
	alt.ID = "x/y"
	alt.Type = templateTypes.Service
	alt.Status = statuses.Draft
	hss := new(css.StyleSheet)
	alt.HtmlCss = hss //"ages.html.css"
	pdf := new(PDF)
	pdf.Title = "I am the title"
	pdf.CSS = hss // "ages.pdf.css"
	pdf.PageNbr = 1
	alt.PDF = pdf
	headerEven := NewHeaderEven()
	headerEven.AddLeftDirective(NewLiteralDirective(".it", "This is a test"))
	headerEven.AddCenterDirective(NewDateDirective(".it", time.Now(), 1))
	pdf.AddHeader(*headerEven)
	headerOdd := NewHeaderOdd()
	lookupDirective := NewLookupDirective(1)
	err := lookupDirective.AddLookupTK(idTypes.SID, "actor", "actors/priest", nil)
	headerOdd.AddRightDirective(lookupDirective)
	headerOdd.AddCenterDirective(NewPageNbrDirective(".it"))
	pdf.AddHeader(*headerOdd)
	footerEven := NewFooterEven()
	footerEven.AddCenterDirective(NewDateDirective(".it", time.Now(), 1))
	pdf.AddFooter(*footerEven)
	footerOdd := NewFooterOdd()
	footerOdd.AddCenterDirective(NewPageNbrDirective(".it"))
	pdf.AddFooter(*footerOdd)
	j, err := json.MarshalIndent(alt, "", " ")
	if err != nil {
		t.Error(err)
	}
	fmt.Println(string(j))
}
