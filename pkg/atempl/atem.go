// Package atempl provides an Abstract Template struct which contains all the information needed for a generator (e.g. an HTML generator or a PDF generator), when combined with the user requested libraries.
package atempl

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/css"
	"github.com/liturgiko/doxa/pkg/doxlog"
	rowTypes "github.com/liturgiko/doxa/pkg/enums/RowTypes"
	"github.com/liturgiko/doxa/pkg/enums/calendarTypes"
	"github.com/liturgiko/doxa/pkg/enums/dbStatus"
	"github.com/liturgiko/doxa/pkg/enums/directiveTypes"
	"github.com/liturgiko/doxa/pkg/enums/idTypes"
	"github.com/liturgiko/doxa/pkg/enums/officeTypes"
	"github.com/liturgiko/doxa/pkg/enums/outputTypes"
	"github.com/liturgiko/doxa/pkg/enums/properties"
	"github.com/liturgiko/doxa/pkg/enums/spanTypes"
	"github.com/liturgiko/doxa/pkg/enums/statuses"
	"github.com/liturgiko/doxa/pkg/enums/templateTypes"
	"github.com/liturgiko/doxa/pkg/enums/versions"
	"github.com/liturgiko/doxa/pkg/locale"
	"github.com/liturgiko/doxa/pkg/media"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"os"
	"path"
	"strings"
	"time"
)

const IDPathDelimiter = "/"

/*
ATEM is an Abstract Template.
Conceptually, an ATEM describes the content of a table with columns and rows.
Each set of primary and fallback libraries (GenLibs) defines a column.
Generated rows have as many cells as there are values in GenLibs.
The information for each cell is stored in the ATEM MetaRows property. Each row provides the meta data to use to generate the cells in the row.
Use the function NewATEM() to get a pointer to an ATEM.  Otherwise the maps will be nil.
ID is the identifier for the template and should match a corresponding path in the file system or an ID in a database.
TemplateByIDs and TemplatesByNbr are used to record the ids of templates used by this ATEM. The Template can by looked up by its string ID or its int.
Type is block, book, or service.
Status values are na, draft, review, or final.
Calendar indicates whether the Gregorian or Julian calendar is to be used with this template.
Month, Day, Year are used when the Type = service.
HtmlCss is the parsed content of the css file to be used for the creating HTML.
PDF contains information for creating PDF files, e.g. title, Header and Footer information, and the CSS to use.
LDP holds the liturgical day properties for the specified Year, Month, and Day.
GenLibs is a slice that holds the primary library and fallback generation libraries to be bound to each topic-key. The fallback libraries are used if the combination of the primary library and a topic/key does not exist. Each set of Primary and Fallback libraries define a column in the table.
TKValues is a map whose key is a TopicKey and value is a map.  The value map uses library as the key and the value is the text retrieved from the database.
Libraries is a map whose key is a library and value is the library's acronym. The acronym is used as the value for an @Ver directive to insert the version.
MetaRows holds a slice of MetaRow.
Important: if you add a map the ATEM struct, be sure to:
 1. AddPathToMap a make statement for it in the NewATEM method
 2. Copy its values to the master ATEM if it is an inserted ATEM.
    See the EnterInsert method in listener.go in pkg/parser.
*/
type ATEM struct {
	ID                                    string
	Calendar                              calendarTypes.CalendarType
	FilePath                              string // for file based templates
	FormattedDate                         string
	HtmlCss                               *css.StyleSheet
	IdBase                                string
	IndexTitleCodes                       string // these are written as metadata in the HTML file and are used by the site indexer
	IndexLastTitleCodeOverride            string // written as metadata
	Keywords                              string // these are written as metadata keywords in the HTML file for search engines to use
	MetaRows                              []*MetaRow
	Month, Day, Year                      int
	VespersMonth, VespersDay, VespersYear int                    // can be different from the Month, Day, Year if the service is one celebrated the evening before, e.g. Vespers.
	Office                                officeTypes.OfficeType // e.g. Vespers
	OfficeCode                            string                 // from template ID, if the type is a service
	OutputType                            outputTypes.OutputType
	PDF                                   *PDF
	RowLayouts                            []*RowLayout // one per TableLayout
	Status                                statuses.Status
	TableLayouts                          []*TableLayout
	TemplatesById                         map[string]int // map of Template IDs, including the main.
	TemplatesByNbr                        map[int]string // map of Template IDs, including the main.
	Title                                 string
	TitleAbrv                             string
	Type                                  templateTypes.TemplateType // one of block, book, service
	Libraries                             map[string]string
}

func NewATEM() *ATEM {
	atem := new(ATEM)
	tkValues := new(TKValues)
	tkValues.Map = make(map[string]map[string]string)
	atem.Libraries = make(map[string]string)
	atem.TemplatesById = make(map[string]int)
	atem.TemplatesByNbr = make(map[int]string)
	atem.Keywords = "dcs, digital chant stand, Orthodox Christian, Eastern Orthodox Christian, services, liturgical services"
	return atem
}
func (a *ATEM) AddKeyWord(w string) {
	parts := strings.Split(a.Keywords, ", ")
	parts = append(parts, w)
	a.Keywords = strings.Join(parts, ", ")
}
func (a *ATEM) SetOfficeCode() {
	if a.Type == templateTypes.Service {
		parts := strings.Split(a.ID, ".")
		a.OfficeCode = parts[len(parts)-1]
		if a.OfficeCode == "lml" {
			a.OfficeCode = parts[len(parts)-2]
		}
		a.Office = officeTypes.OfficeTypeForAbr(a.OfficeCode)
	}
}

// SetIndexTitleCodes checks to see if the IndexTitleCodes has been
// already set, and if not sets it using the segments of the ID base.
// This is used primarily to set them for a service, but is written
// to be safe to call for any template type.
func (a *ATEM) SetIndexTitleCodes() {
	if len(a.IndexTitleCodes) == 0 {
		base := strings.ToLower(path.Base(a.ID))
		if strings.HasPrefix(base, "se.") || strings.HasPrefix(base, "bl.") || strings.HasPrefix(base, "bk.") {
			base = base[3:]
		}
		if strings.HasSuffix(base, ".lml") {
			base = base[:len(base)-5]
		}
		a.IndexTitleCodes = strings.ReplaceAll(base, ".", ",")
		a.IndexTitleCodes = strings.ReplaceAll(a.IndexTitleCodes, " ", "")
	}
}
func (a *ATEM) Copy() *ATEM {
	c := NewATEM()
	c.ID = a.ID
	c.Calendar = a.Calendar
	c.FilePath = a.FilePath
	c.FormattedDate = a.FormattedDate
	c.HtmlCss = a.HtmlCss
	c.IdBase = a.IdBase
	c.IndexTitleCodes = a.IndexTitleCodes
	c.Keywords = a.Keywords
	c.MetaRows = a.MetaRows
	c.Month = a.Month
	c.Day = a.Day
	c.Year = a.Year
	c.VespersMonth = a.VespersMonth
	c.VespersDay = a.VespersDay
	c.VespersYear = a.VespersYear
	c.Office = a.Office
	c.PDF = a.PDF
	c.RowLayouts = a.RowLayouts
	c.Status = a.Status
	c.TableLayouts = a.TableLayouts
	c.TemplatesById = a.TemplatesById
	c.TemplatesByNbr = a.TemplatesByNbr
	c.Title = a.Title
	c.TitleAbrv = a.TitleAbrv
	c.Type = a.Type
	c.Libraries = a.Libraries
	return c
}
func (a *ATEM) Json(prettyPrint bool) (string, error) {
	var j []byte
	var err error
	if prettyPrint {
		j, err = json.MarshalIndent(a, "", " ")
	} else {
		j, err = json.Marshal(a)
	}
	if err != nil {
		msg := fmt.Sprintf("error marshaling ATEM for template %s: %v", a.ID, err)
		doxlog.Error(msg)
		return "", fmt.Errorf(msg)
	}
	return string(j), nil
}
func NewAtemFromJson(j string) (*ATEM, error) {
	var a *ATEM
	err := json.Unmarshal([]byte(j), &a)
	if err != nil {
		doxlog.Errorf("%v", err)
		return nil, err
	}
	return a, nil
}

/*
AddTemplateID Adds the id to both maps, so the id can be retrieved either by string or int
*/
func (a *ATEM) AddTemplateID(id string) {
	if _, ok := a.TemplatesById[id]; !ok {
		nbr := len(a.TemplatesById) + 1
		a.TemplatesById[id] = nbr
		a.TemplatesByNbr[nbr] = id
	}
}
func (a *ATEM) AddRowLayout(r *RowLayout) {
	a.RowLayouts = append(a.RowLayouts, r)
}
func (a *ATEM) AddRow(p MetaRow) {
	a.MetaRows = append(a.MetaRows, &p)
}
func (a *ATEM) AddTableLayout(tableLayout *TableLayout) {
	a.TableLayouts = append(a.TableLayouts, tableLayout)
}
func (a *ATEM) VisitRows(process func(i int, r *MetaRow)) {
	for i, row := range a.MetaRows {
		process(i, row)
	}
}

// GetIndexTitleCodeSlice splits a comma delimited string of title codes
func (a *ATEM) GetIndexTitleCodeSlice() (codes []string) {
	return strings.Split(a.IndexTitleCodes, ",")
}

// ExistsCssRule checks either the HTML or PDF css map or both, based on checkHtml and checkPdf
// If both are checked, the func returns true if the rule is found in either html or pdf css.
func (a *ATEM) ExistsCssRule(r string, checkHtml, checkPdf bool) bool {
	var exists, existsHtml, existsPdf bool
	if checkHtml {
		existsHtml = a.ExistsCssRuleForHtml(r)
	}
	if checkPdf {
		existsPdf = a.ExistsCssRuleForPdf(r)
	}
	if checkHtml && checkPdf {
		exists = existsHtml || existsPdf
		return exists
	}
	if checkHtml {
		return existsHtml
	}
	if checkPdf {
		return existsHtml
	}
	return exists
}
func (a *ATEM) ExistsCssRuleForHtml(r string) bool {
	var exists bool
	exists = a.HtmlCss.ContainsRule(r)
	nakedRule := r[4:]
	if !exists {
		exists = a.HtmlCss.ContainsRule(nakedRule)
	}
	return exists
}
func (a *ATEM) ExistsCssRuleForPdf(r string) bool {
	var exists bool
	exists = a.PDF.CSS.ContainsRule(r)
	nakedRule := r[4:]
	if !exists {
		exists = a.PDF.CSS.ContainsRule(nakedRule)
	}
	return exists
}
func (a *ATEM) CreateRowLayouts() error {
	for i, tableLayout := range a.TableLayouts {
		rowLayout := new(RowLayout)
		for _, metaRow := range a.MetaRows {
			row, err := metaRow.CopyForLayout(i, tableLayout, a.Libraries)
			if err != nil {
				doxlog.Errorf("%v", err)
				return err
			}
			if row.IsMediaRow && !row.HasMedia {
				continue // skip this row if it is empty media
			}
			rowLayout.AddRow(row)
		}
		a.AddRowLayout(rowLayout)
	}
	return nil
}

type TKValues struct {
	Map map[string]map[string]string
}

// Resolve uses the primary and fallback libraries to find the value for the requested topicKey,
// using the TKValues.Map
func (m *TKValues) Resolve(genLib GenLib, topicKey string) (*ResolvedValue, error) {
	// TODO: this does not seem to be used anywhere
	rv := new(ResolvedValue)
	rv.TopicKey = topicKey
	rv.RequestedLibrary = genLib.Primary
	if values, ok := m.Map[topicKey]; ok {
		for _, library := range genLib.All() {
			value, ok := values[library+string(os.PathSeparator)+topicKey]
			// return first value found
			if ok {
				rv.UsedLibrary = library
				rv.Value = value
				return rv, nil
			}
		}
		return rv, nil
	} else {
		return rv, fmt.Errorf("template.Resolve: missing topicKey %s in TKValues ", topicKey)
	}
}

type ResolvedValue struct {
	RequestedLibrary string
	UsedLibrary      string
	TopicKey         string
	Value            string
}

func (rv *ResolvedValue) ToID() string {
	return rv.RequestedLibrary + string(os.PathSeparator) + rv.TopicKey
}

// LTKVal holds a resolved UsedLibrary/Topic/Key and its value.
// Resolved means that it was retrieved from a database.
// Although an ID is library/topic/key, the library is split out
// to make it easy to get the Version acronym if needed.
// To get the complete ID, use the ID() method.
type LTKVal struct {
	RequestedLibrary string
	UsedLibrary      string
	TopicKey         string
	Value            string
}

// ID returns a concatenation of UsedLibrary and TopicKey as a valid lml ID
func (ltkv *LTKVal) ID() string {
	return ltkv.UsedLibrary + IDPathDelimiter + ltkv.TopicKey
}

// RequestedID returns a concatenation of RequestedLibrary and TopicKey as a valid lml ID
func (ltkv *LTKVal) RequestedID() string {
	return ltkv.RequestedLibrary + IDPathDelimiter + ltkv.TopicKey
}

// TKVal holds a slice of TableLayout for a specific topicKey, with the values from the database.
// The length of the slice corresponds to the number of columns
// to be generated, which are actually cells in a row.
type TKVal struct {
	Values           []*TableLayout
	HasNonEmptyValue bool   // true if the length of any record value > 0
	Libraries        string // comma delimited list of libraries that were searched
}

// TableLayout indicates how many columns are to be created for each table row and what the versions are for the content.
// Acronym is the ISO code for the Primary Library
// LiturgicalLibs are the primary and fallback libraries for each column of a table row.
// TitleLibPrimary is an index into LiturgicalLibs and indicates which library to use for image captions, notes, and titles.
// TitleLibSecondary is also an index into LiturgicalLibs and is used for alternative titles, e.g. on a facing page.
type TableLayout struct {
	Acronym           string
	LiturgicalLibs    []*GenLib // e.g. Menaion, Octoechos
	EpistleLibs       []*GenLib // for scripture readings from the Epistles (Apostolos)
	GospelLibs        []*GenLib // for scripture readings from the Gospels
	ProphetLibs       []*GenLib
	PsalterLibs       []*GenLib
	MediaLibs         []*media.Media
	TitleLibPrimary   int
	TitleLibSecondary int
	DateFormat        string // see pkg/locale
	WeekDayFormat     string // see pkg/locale
}

func NewTableLayoutFromJson(j string) (*TableLayout, error) {
	layout := new(TableLayout)
	err := json.Unmarshal([]byte(j), layout)
	if err != nil {
		doxlog.Errorf("%v", err)
		return nil, err
	}
	if len(layout.DateFormat) == 0 {
		layout.DateFormat = "c, %B %d, %Y"
	}
	if len(layout.WeekDayFormat) == 0 {
		layout.WeekDayFormat = "%A"
	}
	return layout, nil
}

// NewTableLayoutFromMatrix creates a TableLayout from the matrix.
// The length of the Matrix must be 10.  The odd rows are primary libraries
// and the even rows are fallbacks.  The length of each matrix[i] must be between 1 and 3,
// which represent columns on a generated html or pdf.
func NewTableLayoutFromMatrix(matrix [][]string) (*TableLayout, error) {
	if len(matrix) != 10 {
		return nil, fmt.Errorf("NewTableLayoutFromMatrix: expected len(matrix) == 10, got %d", len(matrix))
	}
	colLength := len(matrix[0])
	layout := new(TableLayout)
	var row, col int
	for row = 0; row < 10; row += 2 {
		if len(matrix[row]) != colLength {
			return nil, fmt.Errorf("NewTableLayoutFromMatrix: expected len(matrix[%d]) == %d, got %d", row, colLength, len(matrix[row]))
		}
		for col = 0; col < colLength; col++ {
			g := new(GenLib)
			if matrix[row][col] == "none" {
				continue
			}
			g.Primary = matrix[row][col]
			if matrix[row+1][col] != "none" {
				g.AddFallBack(matrix[row+1][col])
			}
			switch row {
			case 0:
				{ // liturgical
					layout.AddLiturgicalLib(g)
					break
				}
			case 2:
				{ // epistle
					layout.AddEpistleLib(g)
					break
				}
			case 4:
				{ // gospel
					layout.AddGospelLib(g)
					break
				}
			case 6:
				{ // prophet
					layout.AddProphetLib(g)
					break
				}
			case 8:
				{ // psalter
					layout.AddPsalterLib(g)
					break
				}
			default:
				{
					return nil, fmt.Errorf("NewTableLayoutFromMatix: invalid row number")
				}
			}

		}
	}
	err := layout.SetAcronym()
	if err != nil {
		doxlog.Errorf("%v", err)
		return nil, err
	}
	return layout, nil
}

type KVP struct {
	ID    string
	Value string
}

// GetKV searches the Liturgical, Gospel, Epistle, Prophet, and Psalter libs and returns the ID and Value of the one with len(value) > 0
func (c *TableLayout) GetKV() *KVP {
	kv := new(KVP)
	for _, l := range c.LiturgicalLibs {
		if len(l.Value) > 0 {
			kv.ID = l.IDUsed
			kv.Value = l.Value
		}
	}
	for _, l := range c.GospelLibs {
		if len(l.Value) > 0 {
			kv.ID = l.IDUsed
			kv.Value = l.Value
		}
	}
	for _, l := range c.EpistleLibs {
		if len(l.Value) > 0 {
			kv.ID = l.IDUsed
			kv.Value = l.Value
		}
	}
	for _, l := range c.ProphetLibs {
		if len(l.Value) > 0 {
			kv.ID = l.IDUsed
			kv.Value = l.Value
		}
	}
	for _, l := range c.PsalterLibs {
		if len(l.Value) > 0 {
			kv.ID = l.IDUsed
			kv.Value = l.Value
		}
	}
	return kv
}
func (c *TableLayout) SetAcronym() error {
	sb := strings.Builder{}
	if len(c.LiturgicalLibs) == 0 {
		return fmt.Errorf("atem - SetAcronym - missing liturgical libraries")
	}
	for _, lib := range c.LiturgicalLibs {
		parts := strings.Split(lib.Primary, "_")
		if len(parts) != 3 {
			return fmt.Errorf("atem - SetAcronym - invalid primary library %s", lib.Primary)
		}
		if sb.Len() > 0 {
			sb.WriteString("-")
		}
		sb.WriteString(parts[0])
	}
	c.Acronym = sb.String()
	return nil
}
func (c *TableLayout) AddLiturgicalLib(genLib *GenLib) {
	c.LiturgicalLibs = append(c.LiturgicalLibs, genLib)
}
func (c *TableLayout) AddEpistleLib(genLib *GenLib) {
	c.EpistleLibs = append(c.EpistleLibs, genLib)
}
func (c *TableLayout) AddGospelLib(genLib *GenLib) {
	c.GospelLibs = append(c.GospelLibs, genLib)
}
func (c *TableLayout) AddMediaLib(genLib *media.Media) {
	c.MediaLibs = append(c.MediaLibs, genLib)
}
func (c *TableLayout) AddProphetLib(genLib *GenLib) {
	c.ProphetLibs = append(c.ProphetLibs, genLib)
}
func (c *TableLayout) AddPsalterLib(genLib *GenLib) {
	c.PsalterLibs = append(c.PsalterLibs, genLib)
}
func (c *TableLayout) PrimaryTitleLibrary() string {
	return c.LiturgicalLibs[c.TitleLibPrimary].Primary
}
func (c *TableLayout) SetPrimaryTitleLibrary(i int) error {
	if len(c.LiturgicalLibs)-1 > i {
		return errors.New(fmt.Sprintf("primary title library index %d is greater than len(GenLibs), which is %d", i, len(c.LiturgicalLibs)))
	} else {
		c.TitleLibPrimary = i
		return nil
	}
}
func (c *TableLayout) SecondaryTitleLibrary() string {
	return c.LiturgicalLibs[c.TitleLibSecondary].Primary
}
func (c *TableLayout) SetSecondaryTitleLibrary(i int) error {
	if len(c.LiturgicalLibs)-1 > i {
		return errors.New(fmt.Sprintf("secondary title library index %d is greater than len(GenLibs), which is %d", i, len(c.LiturgicalLibs)))
	} else {
		c.TitleLibSecondary = i
		return nil
	}
}
func (c *TableLayout) Copy() *TableLayout {
	n := new(TableLayout)
	n.Acronym = c.Acronym
	n.TitleLibPrimary = c.TitleLibPrimary
	n.TitleLibSecondary = c.TitleLibSecondary
	for _, lib := range c.LiturgicalLibs {
		n.AddLiturgicalLib(lib.Copy())
	}
	for _, lib := range c.EpistleLibs {
		n.AddEpistleLib(lib.Copy())
	}
	for _, lib := range c.GospelLibs {
		n.AddGospelLib(lib.Copy())
	}
	for _, lib := range c.MediaLibs {
		n.AddMediaLib(lib.Copy())
	}
	for _, lib := range c.ProphetLibs {
		n.AddProphetLib(lib.Copy())
	}
	for _, lib := range c.PsalterLibs {
		n.AddPsalterLib(lib.Copy())
	}
	n.WeekDayFormat = c.WeekDayFormat
	n.DateFormat = c.DateFormat
	return n
}

func (c *TableLayout) ToJson() (string, error) {
	j, err := json.Marshal(c)
	if err != nil {
		doxlog.Errorf("%v", err)
		return "", err
	}
	return string(j), err
}

// GenLib holds the user's request to generate using a specific library
// and a slice of fallback libraries to use if the primary library + topic-key does not exist.
// IDRequested holds the primary library / topicKey.
// IDUsed holds the actual library / topicKey used to obtain the value.
// If the value was found using IDRequested, the value of it and IDUsed will be the same.
// Otherwise, IDUsed will be a fallback library / topicKey.
// LibUsed holds the library used, so we can use it later to get the version acronym.
// LangUsed holds the language code from the librqry, which is used in PDF generation to get the required font.
type GenLib struct {
	Primary           string
	FallBacks         []string
	TopicKeyRequested string
	IDRequested       string `json:"-"`
	IDUsed            string `json:"-"`
	LangUsed          string `json:"-"`
	LibUsed           string `json:"-"`
	Redirects         string `json:"-"`
	Status            dbStatus.DbStatus
	Value             string `json:"-"`
}

// PrimaryFallback returns the Primary and 1st Fallback as a slice
func (g *GenLib) PrimaryFallback() []string {
	var l []string
	if len(g.Primary) == 0 {
		return nil
	}
	l = append(l, g.Primary)
	if len(g.FallBacks) > 0 {
		l = append(l, g.FallBacks[0])
	}
	return l
}

// Copy returns a new instance of a GenLib with the properties copied from the source GenLib object.
func (g *GenLib) Copy() *GenLib {
	genLib := new(GenLib)
	genLib.Primary = g.Primary
	for _, fallBack := range g.FallBacks {
		genLib.AddFallBack(fallBack)
	}
	genLib.IDRequested = g.IDRequested
	genLib.IDUsed = g.IDUsed
	genLib.Value = g.Value
	return genLib
}

// AddFallBack appends a library to the slice of fallback libraries.
func (g *GenLib) AddFallBack(library string) {
	g.FallBacks = append(g.FallBacks, library)
}

// All flattens the GenLib by returning a string slice of the libraries.
// The primary library is at the zero index, the fallback libraries are the rest.
func (g *GenLib) All() []string {
	var all []string
	all = append(all, g.Primary)
	for _, library := range g.FallBacks {
		all = append(all, library)
	}
	return all
}

type PDF struct {
	CSS          *css.StyleSheet
	PageNbr      int
	Title        string
	Headers      []Header
	Footers      []Footer
	FirstHeaders []Header
	FirstFooters []Footer
}

// AddHeader appends a header to the PDF struct's slice of Headers
func (p *PDF) AddHeader(header Header) {
	p.Headers = append(p.Headers, header)
}

func (p *PDF) AddFirstHeader(header Header) {
	p.FirstHeaders = append(p.FirstHeaders, header)
}

// AddFooter appends a footer to the PDF struct's slice of Footers
func (p *PDF) AddFooter(footer Footer) {
	p.Footers = append(p.Footers, footer)
}

func (p *PDF) AddFirstFooter(footer Footer) {
	p.FirstFooters = append(p.FirstFooters, footer)
}

// Parity indicates whether a Header/Footer is even or odd or both
type Parity int

const (
	Both Parity = iota
	Even
	Odd
)

// Position designates slots in a header/footer. The slots are left, center, or right.
type Position int

const (
	Left Position = iota
	Center
	Right
)

// Header indicates parity (whether it is for even or odd pages or both)
// and the content of each of three slots: left, center, right.
// A slot can be empty.  To determine whether a slot has content, call the functions HasLeftSlot, HasCenterSlot, and HasRightSlot.
type Header struct {
	Parity Parity
	Left   Slot
	Center Slot
	Right  Slot
}

func (h *Header) AddLeftDirective(directive PdfDirective) {
	h.Left.Directives = append(h.Left.Directives, directive)
}
func (h *Header) AddCenterDirective(directive PdfDirective) {
	h.Center.Directives = append(h.Center.Directives, directive)
}
func (h *Header) AddRightDirective(directive PdfDirective) {
	h.Right.Directives = append(h.Right.Directives, directive)
}
func (h *Header) HasLeftSlot() bool {
	return len(h.Left.Directives) > 0
}
func (h *Header) HasCenterSlot() bool {
	return len(h.Center.Directives) > 0
}
func (h *Header) HasRightSlot() bool {
	return len(h.Right.Directives) > 0
}

// NewHeader returns a header with parity set to Both.
func NewHeader() *Header {
	var header = new(Header)
	header.Parity = Both
	return header
}

// NewHeaderEven returns a header with parity set to Even.
// That is, it is for even numbered pages.
func NewHeaderEven() *Header {
	var header = new(Header)
	header.Parity = Even
	return header
}

// NewHeaderOdd returns a header with parity set to Odd.
// That is, it is for odd numbered pages.
func NewHeaderOdd() *Header {
	var header = new(Header)
	header.Parity = Odd
	return header
}

// Slot contains information about a left, center, or right slot of a header or footer
type Slot struct {
	Directives []PdfDirective
}

func (s *Slot) AddDirective(directive PdfDirective) {
	s.Directives = append(s.Directives, directive)
}

// Footer indicates parity (whether it is for even or odd pages or both)
// and the content of each of three slots: left, center, right.
// A slot can be empty.  To determine whether a slot has content, call the functions HasLeftSlot, HasCenterSlot, and HasRightSlot.
type Footer struct {
	Parity Parity
	Left   Slot
	Center Slot
	Right  Slot
}

// NewFooter returns a footer with parity set to Both.
func NewFooter() *Footer {
	var footer = new(Footer)
	footer.Parity = Both
	return footer
}

// NewFooterEven returns a footer with parity set to Even.
// That is, it is for even numbered pages.
func NewFooterEven() *Footer {
	var footer = new(Footer)
	footer.Parity = Even
	return footer
}

// NewFooterOdd returns a footer with parity set to Odd.
// That is, it is for odd numbered pages.
func NewFooterOdd() *Footer {
	var footer = new(Footer)
	footer.Parity = Odd
	return footer
}
func (f *Footer) AddLeftDirective(directive PdfDirective) {
	f.Left.Directives = append(f.Left.Directives, directive)
}
func (f *Footer) AddCenterDirective(directive PdfDirective) {
	f.Center.Directives = append(f.Center.Directives, directive)
}
func (f *Footer) AddRightDirective(directive PdfDirective) {
	f.Right.Directives = append(f.Right.Directives, directive)
}
func (f *Footer) HasLeftSlot() bool {
	return len(f.Left.Directives) > 0
}
func (f *Footer) HasCenterSlot() bool {
	return len(f.Center.Directives) > 0
}
func (f *Footer) HasRightSlot() bool {
	return len(f.Right.Directives) > 0
}

//	type PDFDecorator interface {
//		DirType() directiveTypes.DirectiveType
//		DirClass() string
//	}
//
//	type PDFDateDecorator interface {
//		PDFDecorator
//		DateLibrary() int
//		Value() time.Time
//	}
//
//	type PDFLiteralDecorator interface {
//		PDFDecorator
//		Value() string
//	}
//
//	type PDFLookupDecorator interface {
//		PDFDecorator
//		LookupLibrary() int
//		Value() Lookup
//	}
//
// // PDFDirective indicates what is to be inserted into a header or footer.
// // Type indicates the type of the directive.
// // The Class string holds the name of the span CSS class to be applied.
//
//	type PDFDirective struct {
//		Type  directiveTypes.DirectiveType
//		Class string
//	}

type PdfDirective struct {
	Type  directiveTypes.DirectiveType
	Class string
	// Date Directive
	Date       time.Time
	LibraryNbr int
	// Literal Directive
	Literal string
	// Lookup Directive
	Lookup Lookup
	// Body Number Directive
}

func NewDateDirective(class string, time time.Time, libNbr int) PdfDirective {
	var dir PdfDirective
	dir.Type = directiveTypes.InsertDate
	dir.Class = class
	dir.Date = time
	dir.LibraryNbr = libNbr
	return dir
}
func NewPageNbrDirective(class string) PdfDirective {
	var dir PdfDirective
	dir.Type = directiveTypes.InsertPageNbr
	dir.Class = class
	return dir
}
func NewLiteralDirective(class, literal string) PdfDirective {
	var dir PdfDirective
	dir.Type = directiveTypes.InsertLiteral
	dir.Class = class
	dir.Literal = literal
	return dir
}
func NewLookupDirective(library int) PdfDirective {
	var dir PdfDirective
	dir.Type = directiveTypes.InsertLookup
	var lookup = new(Lookup)
	lookup.Library = library - 1 // convert to zero based
	dir.Lookup = *lookup
	return dir
}
func (p *PdfDirective) AddLookupTK(idType idTypes.IDType, class, topicKey string, values *TKVal) error {
	var err error
	var lookupID = new(LookupTopicKey)
	lookupID.Type = idType
	lookupID.Class = class
	lookupID.TopicKey = topicKey
	p.Lookup.Values = append(p.Lookup.Values, values)
	p.Lookup.TopicKeys = append(p.Lookup.TopicKeys, *lookupID)
	return err
}

// Lookup provides information to do a database lookup to insert the result in a header/footer.
type Lookup struct {
	TopicKeys []LookupTopicKey
	Library   int
	Values    []*TKVal
	Center    string
}

func (l *Lookup) Value(layout, column int) string {
	if layout > len(l.Values) {
		return ""
	}
	if l.Values[0].Values[layout].LiturgicalLibs == nil {
		return ""
	}
	if column > len(l.Values[0].Values[layout].LiturgicalLibs) {
		return ""
	}
	return l.Values[0].Values[layout].LiturgicalLibs[column].Value
}

// LookupTopicKey indicates the type of lookup (RID or SID) and the Topic-Key to use and the CSS style class to use.
type LookupTopicKey struct {
	Type         idTypes.IDType
	Class        string
	TopicKey     string
	OverrideDay  int
	OverrideMode int
}

// MetaSpan contains the information to create Table Row Spans (<td>.
// TemplateColumn is the column number for this span in the LML file parsed to create the ATEM.  ANTRL reports it as a zero based number, but here it is incremented by 1 to be user friendly.
// Class is the CSS Class name.
// TopicKeys during generation are prefixed with a library and used to obtain a value from the ltx table in a database.
// For example, actor/Priest -> gr_gr_cog/actor/Priest or en_us_dedes/actors/Priest, etc.
// ChildSpans are spans embedded within a span.
// TextSpans can be thought of as the terminal nodes of a span tree.
// TextSpans and ChildSpans are mutually exclusive. ChildSpans are nil in a TextSpan.
// DisplayVersion indicates whether to show the acronymn for the library from which the span text comes.
type MetaSpan struct {
	TemplateColumn int
	Type           spanTypes.SpanType
	Class          string
	ColSpan        string // the number of columns it will span.
	Version        string // must be string of int from enums/Version.go.  String here so it can be serialized.
	// if Type = nid has a literal value but no TopicKey value
	Literal string
	// if Type = sid or rid, the span has a TopicKey, but no literal value:
	TopicKey string
	// rather than being a nid, sid, or rid, a span can be an anchor, image, or superscript
	Anchor                    *MetaAnchor
	Image                     *Image
	Media                     *media.Media
	ChildSpans                []*MetaSpan
	ValueIndexes              *TKVal
	DisplayVersion            bool
	IsDate                    bool
	IsWeekDayName             bool
	IsLukanWeekDayNameAndWeek bool
	Primary                   string
	Fallback                  string
}

var ParseDateFormat = "2006-01-02" // year-month-day

func (s *MetaSpan) HasChildSpans() bool {
	return len(s.ChildSpans) > 0
}

// NewLid returns a MetaSpan with Type = LID, class "lid", and Literal set to the value parameter.
// AddLid returns an error if the span has ChildSpans.
// TextSpans and ChildSpans are mutually exclusive.
func NewLid(value string) *MetaSpan {
	s := new(MetaSpan)
	s.Type = spanTypes.Lid
	s.Class = "lid"
	s.Literal = value
	return s
}

// NewNid returns a MetaSpan with Type = NID, class "nid", and Literal set to the value parameter.
// AddNid returns an error if the span has ChildSpans.
// TextSpans and ChildSpans are mutually exclusive.
func NewNid(value string) *MetaSpan {
	s := new(MetaSpan)
	s.Type = spanTypes.Nid
	s.Class = "nid"
	s.Literal = value
	return s
}

// NewRid returns a MetaSpan with Type = RID, Class = "kvp", and TopicKey set to the topic/key parameter.
// topicKey is the resolved relative topicKey.  See note below.
// At generation time, the TopicKey will be prefixed with the library so that an inspection of the HTML (for example) shows the ID used to get the value.
// The value will also be added at generation time.
// Note that when this function is called, the topicKey is the one relative to the liturgical day properties, not the one specified in the template.
// For example, if the liturgical day properties are mode=1, day=1, oc.*/ocVE.ApolTheotokionVM.text would be resolved to oc.m1.d1/ocVE.ApolTheotokionVM.text.
// The latter is what is passed to the NewRid method.
func NewRid(topicKey string) *MetaSpan {
	s := new(MetaSpan)
	s.Type = spanTypes.Rid
	s.Class = "kvp"
	s.TopicKey = topicKey
	return s
}

func NewSid(topicKey string) *MetaSpan {
	s := new(MetaSpan)
	s.Type = spanTypes.Sid
	s.Class = "kvp"
	s.TopicKey = topicKey
	return s
}
func NewMetaAnchor() *MetaSpan {
	s := new(MetaSpan)
	s.Type = spanTypes.Anchor
	s.Class = ""
	s.Anchor = new(MetaAnchor)
	return s
}
func NewImage() *MetaSpan {
	s := new(MetaSpan)
	s.Type = spanTypes.Image
	s.Class = "image"
	s.Image = new(Image)
	return s
}
func NewMedia(topicKey string) *MetaSpan {
	s := new(MetaSpan)
	s.Type = spanTypes.Media
	s.Class = "media"
	s.Media = media.NewMedia()
	s.TopicKey = topicKey
	return s
}

// AddChildSpan adds a span as a child, and returns an error if the span has TopicKeys.
// TopicKeys and ChildSpans are mutually exclusive.
func (s *MetaSpan) AddChildSpan(span *MetaSpan) error {
	s.ChildSpans = append(s.ChildSpans, span)
	return nil
}

// MetaRow holds information that can be used to create a table row and its contents.
// Type indicates the type of row content, e.g. a Para(graph), Image (img tag), or Media.
// Class is the CSS class for the cell.
// TemplateID is an int key for the ATEM map of Template IDs.
// TemplateLine is the line number from the LML file that was parsed to create the ATEM. It is not zero based, so starts with 1.
// CallStack shows the sequence of template inserts used to get to this point.
// MetaSpans contain the information for creating spans within each cell of a row.
// Cells contains the table cells for the row, with pointers to layout specific values.
// Version contains information to create a span that will contain an acronym for the library used to retrieve the text values.
// Note that if the MetaRow Class == "Media", it must be handled differently.
// Media is used to produce a GUI popup list of available musical scores and audio recordings for the indicated topic-key in the span.
// This means that generators that are not producing a GUI, e.g. producing a PDF, should ignore a MetaRow whose class == Media.
type MetaRow struct {
	ID           int
	Type         rowTypes.RowType
	Class        string
	TemplateID   int
	TemplateLine int
	CallStack    string
	MetaSpans    []*MetaSpan
	Cells        []*Cell
	HtmlOnly     bool
	PdfOnly      bool
	Version      versions.Version
	IsMediaRow   bool
	HasMedia     bool
}

func (m *MetaRow) AddMetaSpan(span *MetaSpan) {
	if m.Version != versions.All {
		span.ColSpan = "1"
		span.Version = m.Version.String()
	}
	m.MetaSpans = append(m.MetaSpans, span)
}

// CopyForLayout creates a copy of the MetaRow with the cells populated using the specified TableLayout.
// tableLayoutIndex is the index into the slice of TableLayouts
// tableLayout is the instance obtained using the index (tableLayoutIndex).
func (m *MetaRow) CopyForLayout(tableLayoutIndex int, tableLayout *TableLayout, libraries map[string]string) (*MetaRow, error) {
	c := new(MetaRow)
	c.ID = m.ID
	c.Type = m.Type
	c.Class = m.Class
	c.HtmlOnly = m.HtmlOnly
	c.PdfOnly = m.PdfOnly
	c.Version = m.Version
	c.TemplateID = m.TemplateID
	c.TemplateLine = m.TemplateLine
	c.CallStack = m.CallStack
	c.MetaSpans = m.MetaSpans
	err := c.CreateCells(tableLayoutIndex, tableLayout, libraries)
	return c, err
}

// CreateCells uses the specified TableLayout to create cells for each requested library
// populated via pointers with the values for the library.
func (m *MetaRow) CreateCells(tableLayoutIndex int, tableLayout *TableLayout, libraries map[string]string) error {
	from := 0
	nbrColumns := len(tableLayout.LiturgicalLibs)

	if m.Version != versions.All {
		switch nbrColumns {
		case 1:
			// do nothing
		case 2:
			if m.Version == versions.One {
				from = 0
			} else if m.Version == versions.Two || m.Version == versions.Three {
				from = 1
			}
			nbrColumns = from + 1
		case 3:
			if m.Version == versions.One {
				from = 0
			} else if m.Version == versions.Two {
				from = 1
			} else if m.Version == versions.Three {
				from = 2
			}
			nbrColumns = from + 1
		}
	}
	langParts := strings.Split(tableLayout.Acronym, "-")

	for columnIndex := from; columnIndex < nbrColumns; columnIndex++ {
		var err error
		switch m.Type {
		case rowTypes.Para:
			cell := new(Cell)
			paraCell := new(ParaCell)
			paraCell.TdClassPrefix = columnIndex + 1
			paraCell.TdClassSuffix = nbrColumns
			paraCell.PClass = m.Class
			if m.Version != versions.All {
				paraCell.ColSpan = "1"
			}
			paraCell.Language = langParts[columnIndex]
			paraCell.Spans, err = populateSpans(tableLayoutIndex, columnIndex, m.MetaSpans, libraries)
			if err != nil {
				doxlog.Errorf("%v", err)
				return err
			}
			cell.ParaCell = paraCell
			m.Cells = append(m.Cells, cell)
		case rowTypes.SectionHeading1, rowTypes.SectionHeading2, rowTypes.SectionHeading3:
			cell := new(Cell)
			headingCell := new(HeadingCell)
			headingCell.TdClassPrefix = columnIndex + 1
			headingCell.TdClassSuffix = nbrColumns
			headingCell.HClass = m.Class
			switch m.Type {
			case rowTypes.SectionHeading1:
				headingCell.Level = 1
			case rowTypes.SectionHeading2:
				headingCell.Level = 2
			case rowTypes.SectionHeading3:
				headingCell.Level = 3

			}
			if m.Version != versions.All {
				headingCell.ColSpan = "1"
			}
			headingCell.Language = langParts[columnIndex]
			headingCell.Spans, err = populateSpans(tableLayoutIndex, columnIndex, m.MetaSpans, libraries)
			if err != nil {
				doxlog.Errorf("%v", err)
				return err
			}
			cell.HeadingCell = headingCell
			m.Cells = append(m.Cells, cell)
		case rowTypes.EndDiv:
			m.Cells = append(m.Cells, &Cell{EndDivCell: &EndDivCell{}})
		case rowTypes.BlankLine:
			cell := new(Cell)
			cell.AddBlankCell()
			cell.BlankCell.TdClassPrefix = columnIndex + 1
			cell.BlankCell.TdClassSuffix = nbrColumns
			cell.BlankCell.BrClass = m.Class
			if m.Version != versions.All {
				cell.BlankCell.ColSpan = "1"
			}
			m.Cells = append(m.Cells, cell)
		case rowTypes.HorizontalRule:
			cell := new(Cell)
			cell.AddRuleCell()
			cell.RuleCell.TdClassPrefix = columnIndex + 1
			cell.RuleCell.TdClassSuffix = nbrColumns
			cell.RuleCell.HrClass = m.Class
			if m.Version != versions.All {
				cell.RuleCell.ColSpan = "1"
			}
			m.Cells = append(m.Cells, cell)
		case rowTypes.Image:
		case rowTypes.Media:
			for _, span := range m.MetaSpans {
				if span.Type == spanTypes.Media {
					cell := new(Cell)
					mediaLib, err := getMedia(tableLayoutIndex, columnIndex, m.MetaSpans)
					if mediaLib != nil && err == nil {
						m.IsMediaRow = true
						if mediaLib.HasMedia() {
							m.HasMedia = true
						}
						cell.AddMediaCell()
						cell.MediaCell.TdClassPrefix = columnIndex + 1
						cell.MediaCell.TdClassSuffix = nbrColumns
						cell.MediaCell.Media = mediaLib
						if m.Version != versions.All {
							cell.MediaCell.ColSpan = "1"
						}
					} else {
						cell.AddBlankCell()
					}
					m.Cells = append(m.Cells, cell)
				}
			}
		}
	}
	return nil
}

// populateSpans creates spans for the specified libraries
func populateSpans(genLangIndex int, genLibIndex int, spans []*MetaSpan, versions map[string]string) ([]*Span, error) {
	var newSpans []*Span
	var err error
	var theDate time.Time
	for _, span := range spans {
		newSpan := new(Span)
		newSpan.Class = span.Class
		newSpan.TemplateCol = span.TemplateColumn
		if len(span.Literal) > 0 {
			newSpan.Value = &span.Literal
			if span.IsDate {
				if span.ValueIndexes != nil {
					if span.ValueIndexes.Values != nil {
						values := span.ValueIndexes.Values
						if len(values) > genLangIndex {
							theDate, err = time.Parse(ParseDateFormat, span.Literal)
							if err != nil {
								doxlog.Errorf("atem.populateSpans() error parsing date %s: %v\n", span.Literal, err)
							} else {
								genLibs := values[genLangIndex].LiturgicalLibs
								if len(genLibs) > genLibIndex {
									newSpan.RequestedLib = genLibs[genLibIndex].IDRequested
									newSpan.Redirects = genLibs[genLibIndex].Redirects
									newSpan.ID = genLibs[genLibIndex].IDUsed
									newSpan.Status = genLibs[genLibIndex].Status
									newSpan.Primary = genLibs[genLibIndex].Primary
									if len(genLibs[genLibIndex].FallBacks) > 0 {
										newSpan.Fallback = genLibs[genLibIndex].FallBacks[0]
									}
									newSpan.TopicKeyRequested = genLibs[genLibIndex].TopicKeyRequested
									parts := strings.Split(genLibs[genLibIndex].LibUsed, "_")
									if len(parts) == 3 {
										theLocale := fmt.Sprintf("%s_%s", parts[0], strings.ToUpper(parts[1]))
										if span.IsWeekDayName {
											var formattedDate string
											dateFormatter := locale.NewDateFormatter(theLocale, "%A")
											formattedDate, err = dateFormatter.FormattedDate(theDate)
											if err != nil {
												newSpan.Value = &formattedDate
											}
										} else {
											// TODO: get format from user
											dateFormatter := locale.NewDateFormatter(theLocale, "%A, %d %B %Y")
											formattedDate, err := dateFormatter.FormattedDate(theDate)
											if err != nil {
												newSpan.Value = &formattedDate
											}
										}
									}
								}
							}
						}
					}
				}
			} else {
				newSpan.Value = &span.Literal
			}
		} else if span.Anchor != nil {
			newSpan.Anchor = NewAnchor() // span.Anchor
			newSpan.Anchor.Class = span.Anchor.Class
			newSpan.Anchor.Target = span.Anchor.Target
			if len(span.Anchor.HREF.Literal) > 0 {
				newSpan.Anchor.HREF.Url = span.Anchor.HREF.Literal
			} else {
				href := span.Anchor.HREF
				if href.ValueIndexes.Values != nil {
					var genLib *GenLib
					genLib, err = selectGenLib(href.TopicKey, genLangIndex, genLibIndex, href.ValueIndexes.Values)
					if err != nil {
						doxlog.Errorf("%v", err)
						return nil, err
					}
					newSpan.Anchor.HREF.Url = genLib.Value
					newSpan.Anchor.HREF.TopicKeyRequested = genLib.TopicKeyRequested
					newSpan.Anchor.HREF.IdUsed = genLib.IDUsed
				}
			}

			// populate the Label span
			label := span.Anchor.Label
			newLabelSpan := new(Span)
			newLabelSpan.Class = span.Class
			newLabelSpan.TemplateCol = span.TemplateColumn
			if len(label.Literal) > 0 {
				newLabelSpan.Value = &span.Anchor.Label.Literal
			} else {
				if label.ValueIndexes != nil {
					if label.ValueIndexes.Values != nil {
						var genLib *GenLib
						genLib, err = selectGenLib(label.TopicKey, genLangIndex, genLibIndex, label.ValueIndexes.Values)
						if err != nil {
							doxlog.Errorf("%v", err)
							return nil, err
						}
						err = populateSpanDataAttributes(newLabelSpan, genLib)
						if err != nil {
							doxlog.Errorf("%v", err)
							return nil, err
						}
					}
				}

			}
			newSpan.Anchor.Label = newLabelSpan
		} else {
			if span.ValueIndexes != nil {
				if span.ValueIndexes.Values != nil {
					var genLib *GenLib
					genLib, err = selectGenLib(span.TopicKey, genLangIndex, genLibIndex, span.ValueIndexes.Values)
					if err != nil {
						doxlog.Errorf("%v", err)
						return nil, err
					}
					err = populateSpanDataAttributes(newSpan, genLib)
					if err != nil {
						doxlog.Errorf("%v", err)
						return nil, err
					}
				}
			}
		}
		if len(span.ChildSpans) > 0 {
			newSpan.ChildSpans, err = populateSpans(genLangIndex, genLibIndex, span.ChildSpans, versions)
			if err != nil {
				doxlog.Errorf("%v", err)
				return nil, err
			}
		}
		newSpans = append(newSpans, newSpan)
		if span.DisplayVersion {
			if newSpan.UsedLib != "gr_gr_cog" { // don't report version for the Greek library
				if version, ok := versions[newSpan.UsedLib]; ok {
					versionSpan := new(Span)
					versionSpan.Class = "versiondesignation"
					versionSpan.ID = fmt.Sprintf("ltx/%s/properties/version.designation", newSpan.UsedLib)
					versionSpan.Primary = newSpan.Primary
					versionSpan.Fallback = newSpan.Fallback
					versionSpan.Status = newSpan.Status
					versionSpan.Primary = newSpan.Primary
					versionSpan.Fallback = newSpan.Fallback
					versionSpan.TopicKeyRequested = newSpan.TopicKeyRequested
					versionSpan.UsedLanguage = newSpan.UsedLanguage
					versionSpan.UsedLib = newSpan.UsedLib
					versionSpan.RequestedLib = newSpan.RequestedLib
					versionSpan.Redirects = newSpan.Redirects
					versionSpan.TemplateCol = newSpan.TemplateCol
					value := fmt.Sprintf("%s%s%s", config.SM.StringProps[properties.SiteBuildVersionPrefix], version, config.SM.StringProps[properties.SiteBuildVersionSuffix])
					versionSpan.Value = &value
					newSpans = append(newSpans, WrapSpanAsKvp(versionSpan))
				}
			}
		}
	}
	return newSpans, err
}
func populateSpanDataAttributes(newSpan *Span, genLib *GenLib) (err error) {
	newSpan.Value = &genLib.Value
	if len(*newSpan.Value) == 0 {
		newSpan.Class = "kvp dummy"
	}
	newSpan.RequestedLib = genLib.IDRequested
	newSpan.Redirects = genLib.Redirects
	newSpan.Status = genLib.Status
	newSpan.ID = genLib.IDUsed
	newSpan.Primary = genLib.Primary
	if len(genLib.FallBacks) > 0 {
		newSpan.Fallback = genLib.FallBacks[0]
	}
	newSpan.TopicKeyRequested = genLib.TopicKeyRequested
	var libUsed string
	if len(newSpan.ID) == 0 {
		libUsed = genLib.Primary
	} else {
		libUsed, err = ltstring.LibraryFromDoxaId(newSpan.ID)
		if err != nil {
			doxlog.Errorf("%v", err)
			return err
		}
	}
	newSpan.UsedLib = libUsed
	newSpan.UsedLanguage, err = ltstring.LangFromLibrary(libUsed)
	if err != nil {
		doxlog.Errorf("%v", err)
		return err
	}
	if strings.TrimSpace(newSpan.UsedLanguage) == "" {
		return fmt.Errorf("newSpan.UsedLanguage empty for %s", newSpan.RequestedLib)
	}
	return nil
}
func selectGenLib(topicKey string, genLangIndex, genLibIndex int, values []*TableLayout) (genLib *GenLib, err error) {
	var genLibs []*GenLib
	if len(values) <= genLangIndex {
		return nil, fmt.Errorf("populating spans with values, genLangIndex is %d, exceeding len(span.ValueIndexes.Values) = %d ", genLangIndex, len(values))
	} else {
		if strings.HasPrefix(topicKey, "le.go.") {
			genLibs = values[genLangIndex].GospelLibs
		} else if strings.HasPrefix(topicKey, "le.ep.") {
			genLibs = values[genLangIndex].EpistleLibs
		} else if strings.HasPrefix(topicKey, "le.pr.") {
			genLibs = values[genLangIndex].ProphetLibs
		} else if strings.HasPrefix(topicKey, "ps.") {
			genLibs = values[genLangIndex].PsalterLibs
		} else {
			genLibs = values[genLangIndex].LiturgicalLibs
		}
	}
	if len(genLibs) <= genLibIndex {
		return nil, fmt.Errorf("populating spans with values, genLibsIndex is %d, exceeding len(genLibs) = %d ", genLibIndex, len(genLibs))
	}
	return genLibs[genLibIndex], nil
}
func genLibValues(newSpan *Span, genLangIndex, genLibIndex int, topicKey string, values []*TableLayout) error {
	var err error
	if len(values) > genLangIndex {
		var genLibs []*GenLib
		if strings.HasPrefix(topicKey, "le.go.") {
			genLibs = values[genLangIndex].GospelLibs
		} else if strings.HasPrefix(topicKey, "le.ep.") {
			genLibs = values[genLangIndex].EpistleLibs
		} else if strings.HasPrefix(topicKey, "le.pr.") {
			genLibs = values[genLangIndex].ProphetLibs
		} else if strings.HasPrefix(topicKey, "ps.") {
			genLibs = values[genLangIndex].PsalterLibs
		} else {
			genLibs = values[genLangIndex].LiturgicalLibs
		}
		if len(genLibs) > genLibIndex {
			newSpan.Value = &genLibs[genLibIndex].Value
			if len(*newSpan.Value) == 0 {
				newSpan.Class = "kvp dummy"
			}
			newSpan.RequestedLib = genLibs[genLibIndex].IDRequested
			newSpan.Redirects = genLibs[genLibIndex].Redirects
			newSpan.Status = genLibs[genLibIndex].Status
			newSpan.ID = genLibs[genLibIndex].IDUsed
			newSpan.Primary = genLibs[genLibIndex].Primary
			if len(genLibs[genLibIndex].FallBacks) > 0 {
				newSpan.Fallback = genLibs[genLibIndex].FallBacks[0]
			}
			newSpan.TopicKeyRequested = genLibs[genLibIndex].TopicKeyRequested
			var libUsed string
			if len(newSpan.ID) == 0 {
				libUsed = genLibs[genLibIndex].Primary
			} else {
				libUsed, err = ltstring.LibraryFromDoxaId(newSpan.ID)
				if err != nil {
					doxlog.Errorf("%v", err)
					return err
				}
			}
			newSpan.UsedLib = libUsed
			newSpan.UsedLanguage, err = ltstring.LangFromLibrary(libUsed)
			if err != nil {
				doxlog.Errorf("%v", err)
				return err
			}
			if strings.TrimSpace(newSpan.UsedLanguage) == "" {
				return fmt.Errorf("newSpan.UsedLanguage empty for %s", newSpan.RequestedLib)
			}
		} else {
			return fmt.Errorf("populating spans with values, genLibsIndex is %d, exceeding len(genLibs) = %d ", genLibIndex, len(genLibs))
		}
	} else {
		return fmt.Errorf("populating spans with values, genLangIndex is %d, exceeding len(span.ValueIndexes.Values) = %d ", genLangIndex, len(values))
	}
	return nil
}

// WrapSpanAsKvp adds the span as a child of another span,
// setting the class of the wrapper span to the class of the parameter s span
// and setting the s.Class to kvp.
func WrapSpanAsKvp(s *Span) *Span {
	wrapper := new(Span)
	wrapper.Class = s.Class
	if len(*s.Value) == 0 {
		s.Class = "kvp dummy"
	} else {
		s.Class = "kvp"
	}
	wrapper.TemplateCol = s.TemplateCol
	wrapper.UsedLib = s.UsedLib
	wrapper.UsedLanguage = s.UsedLanguage
	wrapper.ChildSpans = append(wrapper.ChildSpans, s)
	return wrapper
}
func getMedia(genLangIndex int, genLibIndex int, spans []*MetaSpan) (*media.Media, error) {

	// TODO set NoMedia
	var err error
	for _, span := range spans {
		if span.Type != spanTypes.Media {
			continue
		}
		if span.ValueIndexes != nil {
			if span.ValueIndexes.Values != nil {
				values := span.ValueIndexes.Values
				if len(values) > genLangIndex {
					layout := values[genLangIndex]
					if len(layout.MediaLibs) > genLibIndex {
						return layout.MediaLibs[genLibIndex], nil
					}
				}
			}
		}
	}
	return nil, err
}

type Image struct {
	Source string
	Width  string
	Height string
}
type Anchor struct {
	Class  string
	HREF   *HREF
	Label  *Span
	Target string
}
type HREF struct {
	Url               string
	TopicKeyRequested string
	IdUsed            string
}

func NewAnchor() *Anchor {
	a := new(Anchor)
	a.HREF = new(HREF)
	return a
}

type MetaAnchor struct {
	Class  string
	HREF   *MetaSpan
	Label  *MetaSpan
	Target string
}

type CellLayout struct {
	Cells []*Cell
}

func (c *CellLayout) AddCell(cell *Cell) {
	c.Cells = append(c.Cells, cell)
}

// Cell contains data for a table cell aka column.
// The various cell types are mutually exclusive
type Cell struct {
	BlankCell   *BlankCell
	ParaCell    *ParaCell
	MediaCell   *MediaCell
	RuleCell    *RuleCell
	HeadingCell *HeadingCell
	EndDivCell  *EndDivCell
}

func (c *Cell) AddRuleCell() {
	ruleCell := new(RuleCell)
	c.RuleCell = ruleCell
}
func (c *Cell) AddBlankCell() {
	blankCell := new(BlankCell)
	c.BlankCell = blankCell
}
func (c *Cell) AddMediaCell() {
	cell := NewMediaCell()
	c.MediaCell = cell
}

type BlankCell struct {
	ColSpan       string
	TdClassPrefix int
	TdClassSuffix int
	BrClass       string
}
type RuleCell struct {
	ColSpan       string
	TdClassPrefix int
	TdClassSuffix int
	HrClass       string
}
type ParaCell struct {
	ColSpan       string
	TdClassPrefix int
	TdClassSuffix int
	PClass        string
	Spans         []*Span
	Version       string
	Language      string
}
type HeadingCell struct {
	ColSpan       string
	TdClassPrefix int
	TdClassSuffix int
	HClass        string
	Level         int
	Version       string
	Language      string
	Spans         []*Span
}
type MediaCell struct {
	ColSpan       string
	TdClassPrefix int
	TdClassSuffix int
	Media         *media.Media
}

type EndDivCell struct {
}

func NewMediaCell() *MediaCell {
	c := new(MediaCell)
	c.Media = media.NewMedia()
	return c
}

/*
Span holds the information for an HTML span tag. It differs from template.MetaSpan in that it as a resolved value.
Class is the css class used to format the span.
ID holds the ID of the database record from which the value was read. It may have used a fallback library.
RequestedLib holds the primary library requested by the user. The ID and requested could be different if a fallback library was used.
TemplateCol is the column number of the line in an lml template from which the atem span was created.
Value is the value obtained by attempting to use the primary and backup libraries.
ChildSpans holds embedded spans.  Note that Value and ChildSpans are mutually exclusive.  A recursive processing of spans results in a final nil value for ChildSpans, and Value being non-nil.
*/
type Span struct {
	Class             string
	Fallback          string
	ID                string
	Primary           string
	Redirects         string
	RequestedLib      string
	Status            dbStatus.DbStatus
	TemplateCol       int
	TopicKeyRequested string
	UsedLanguage      string
	UsedLib           string
	Value             *string
	ChildSpans        []*Span
	Anchor            *Anchor
}

type RowLayout struct {
	Rows []*MetaRow
}

func (r *RowLayout) AddRow(row *MetaRow) {
	r.Rows = append(r.Rows, row)
}
