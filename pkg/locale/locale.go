// Package locale provides locale specific month and weekday names in date formats.
// Each library specified in a generation layout must have a topic locale with
// keys for month and weekday names.
package locale

import (
	"fmt"
	"github.com/klauspost/lctime"
	"strings"
	"time"
)

type DateFormatter struct {
	Locale string
	Format string
}

// NewDateFormatter initializes an instance to use the locale and format.
// locale is ISO langCode_countryCode, e.g. es_MX
// format uses the format parameters below, e.g. "%A, %d de %B de %Y"
// Note that the format can include literals as in the above example, i.e. de
// Format parameters that may be used are:
// %a  locale's abbreviated weekday name
// %A  locale's full weekday name
// %b  locale's abbreviated month name
// %B  locale's full month name
// %c  locale's appropriate date and time representation
// %C  year divided by 100 and truncated to an integer
// %d  day of the month as a decimal number [01,31]
// %D  %m/%d/%y
// %e  day of the month as a decimal number [1,31]
// %F  %Y/%m/%d
// %g  last 2 digits of the week-based year as a decimal number
// %G  week-based year as a decimal number (for example, 1977)
// %H  hour (24-hour clock) as a decimal number [00,23]
// %I  hour (12-hour clock) as a decimal number [01,12]
// %j  day of the year as a decimal number [001,366]
// %m  month as a decimal number [01,12]
// %M  minute as a decimal number [00,59]
// %n  returns a newline
// %p  locale's equivalent of either a.m. or p.m.
// %r  time in a.m. and p.m. notation.
// %R  time in 24-hour notation %H:%M
// %S  second as a decimal number [00,60]
// %t  returns a tab
// %T  %H:%M:%S
// %u  weekday as a decimal number [1,7]
// %U  week number of the year as a decimal number [00,53]
// %V  week number of the year
// %w  weekday as a decimal number [0,6]
// %W  week number of the year as a decimal number [00,53]
// %x  locale's appropriate date representation
// %X  locale's appropriate time representation
// %y  last two digits of the year as a decimal number [00,99]
// %Y  year as a decimal number (for example, 1997)
// %z  offset from UTC in the ISO 8601:2000 standard format
// %Z  timezone name
func NewDateFormatter(locale, format string) *DateFormatter {
	df := new(DateFormatter)
	df.Format = format
	df.Locale = locale
	if strings.HasPrefix(locale, "gr") {
		parts := strings.Split(locale, "_")
		if len(parts) == 2 {
			df.Locale = fmt.Sprintf("el_%s", parts[1])
		}
	}
	return df
}
func (df *DateFormatter) FormattedDate(t time.Time) (string, error) {
	txt, err := lctime.StrftimeLoc(df.Locale, df.Format, t)
	if err != nil {
		return "", err
	}
	if strings.HasPrefix(df.Locale, "el_") || strings.HasPrefix(df.Locale, "gr_") {
		txt = strings.ReplaceAll(txt, "ος", "ου")
	}
	return txt, nil
}
