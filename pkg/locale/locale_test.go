package locale

import (
	"testing"
	"time"
)

func TestFormattedDate(t *testing.T) {
	theTime := time.Date(2015, 12, 25, 3, 2, 1, 0, time.UTC)
	expect := "viernes, 25 de diciembre de 2015"
	df := NewDateFormatter("es_MX", "%A, %d de %B de %Y")
	got, err := df.FormattedDate(theTime)
	if err != nil {
		t.Error(err)
		return
	}
	if expect != got {
		t.Errorf("expected %s: got %s", expect, got)
	}
	expect = "Παρασκευή, 25 Δεκέμβριου 2015"
	df = NewDateFormatter("el_GR", "%A, %d %B %Y")
	got, err = df.FormattedDate(theTime)
	if err != nil {
		t.Error(err)
		return
	}
	if expect != got {
		t.Errorf("expected %s: got %s", expect, got)
	}
}
