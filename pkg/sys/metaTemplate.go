package sys

import (
	"encoding/json"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/enums/templateTypes"
	"time"
)

/*
  For these paths, do this:
  kp := kvs.NewKeyPath()
  kp.Parse(sys.DbDirPathAtem)
  kp.KeyParts.Push(theTemplateId)
*/

const DbDirPathMetaAtem = "system/templates/meta/atem"
const DbDirPathMetaData = "system/templates/meta/data"

type MetaData struct {
	// WARNING!!! If you add or remove properties,
	//you need provide a means to clean up the user's database
	ID        string // template ID
	LastParse time.Time
	NoErrors  bool
	ErrorMsg  string
	Title     string // template title to display to user
	Type      templateTypes.TemplateType
	Date      string // template date to display to user if type == service
}

func MetaAtemKeyPath(id string) *kvs.KeyPath {
	return keyPath(DbDirPathMetaAtem, id)
}
func MetaDataKeyPath(id string) *kvs.KeyPath {
	return keyPath(DbDirPathMetaData, id)
}
func keyPath(dir, id string) *kvs.KeyPath {
	kp := kvs.NewKeyPath()
	err := kp.ParsePath(dir)
	if err != nil {
		return nil
	}
	kp.KeyParts.Push(id)
	return kp
}
func (m *MetaData) ToJson(prettyPrint bool) (string, error) {
	var j []byte
	var err error
	if prettyPrint {
		j, err = json.MarshalIndent(m, "", "  ")
	} else {
		j, err = json.Marshal(m)
	}
	if err != nil {
		return "", err
	}
	return string(j), nil
}
func MetaAtemFromJson(j string) (*MetaData, error) {
	var m *MetaData
	err := json.Unmarshal([]byte(j), &m)
	if err != nil {
		return nil, err
	}
	return m, nil
}
