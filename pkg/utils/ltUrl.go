package ltUrl

import (
	"net/url"
	"strings"
)

// GetURLPathParts returns a slice of the path segments of the URL,
// i.e. https://mydomain.com/a/b/c returns []string{"a", "b", "c"}
func GetURLPathParts(urlString string) ([]string, error) {
	parsedURL, err := url.Parse(urlString)
	if err != nil {
		return nil, err
	}
	trimmedPath := strings.Trim(parsedURL.Path, "/")
	pathParts := strings.Split(trimmedPath, "/")
	return pathParts, nil
}
