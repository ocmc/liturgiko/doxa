package ltRegEx

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"golang.org/x/text/unicode/norm"
	"regexp"
	"strings"
	"testing"
	"unicode"
)

func TestFindAllIndexesGreek(t *testing.T) {
	content := "Ἡ χάρις τοῦ Κυρίου ἡμῶν Ἰησοῦ Χριστοῦ καὶ ἡ ἀγάπη τοῦ Θεοῦ καί Πατρὸς καὶ ἡ κοινωνία τοῦ Ἁγίου Πνεύματος εἴη μετὰ πάντων ὑμῶν."
	pattern := `ἀγάπη` // `[\p{L}\d_]`
	macA := `ά`
	cogA := `ά` //`ά`
	nrmA := norm.NFC.String(cogA)
	fmt.Printf("macA: %U\ncogA: %U\nnrmA: %U\n", []rune(macA), []rune(cogA), []rune(nrmA))
	// mac keyboard: ά  db: ἀγάπη
	matcher, err := regexp.Compile(pattern)
	if err != nil {
		fmt.Println(err)
	}
	indexes := matcher.FindAllStringIndex(norm.NFC.String(content), -1)
	expect := 1
	got := len(indexes)
	if expect != got {
		t.Errorf("expected %d, got %d\n", expect, got)
	}
}
func TestFindAllIndexes(t *testing.T) {
	content := []byte("London")
	pattern := "on"
	indexes, err := FindAllIndexes(content, pattern)
	if err != nil {
		t.Error(err)
	}
	expectedLength := 2
	if len(indexes) != expectedLength {
		t.Errorf("expected len(indexes) == %d got %d", expectedLength, len(indexes))
	}
	expected := "[1 3]"
	got := fmt.Sprintf("%v", indexes[0])
	if expected != got {
		t.Errorf("expected indexes[0] == %s got %s", expected, got)
	}
	expected = "[4 6]"
	got = fmt.Sprintf("%v", indexes[1])
	if expected != got {
		t.Errorf("expected indexes[1] == %s got %s", expected, got)
	}
}
func TestFindAllLineIndexes(t *testing.T) {
	content := []byte("London!\nI lived there when I was a teenager.\nThe years were 1966-1967")
	//	               01234567 8901234567890123456789012345678901234 567890123456789012345678
	//                            1         2         3         4          5         6
	indexes, err := FindAllLineIndexes(content)
	if err != nil {
		t.Error(err)
	}
	expect := []string{"[7 8]", "[44 45]"}
	if len(indexes) != len(expect) {
		t.Errorf("expected len(indexes) == len(expect) but got %d != %d", len(indexes), len(expect))
	}
	for i, e := range expect {
		if e != fmt.Sprintf("%v", indexes[i]) {
			t.Errorf("expected indexes[%d] == %s but got %d", i, e, indexes[i])
		}
	}
}
func TestFindAllLineStartIndexes(t *testing.T) {
	content := []byte("London!\nI lived there when I was a teenager.\nThe years were 1966-1967")
	indexes, err := FindAllLineStartIndexes(content)
	if err != nil {
		t.Error(err)
	}
	expectedLen := 3
	if len(indexes) != expectedLen {
		t.Errorf("execpted len(indexes) == %d but got %d", expectedLen, len(indexes))
	}
	expect := "[0 8 45]"
	got := fmt.Sprintf("%v", indexes)
	if got != expect {
		t.Errorf("expected indexes = %s got %s", expect, got)
	}
}
func TestFindAllLineSliceIndexes(t *testing.T) {
	content := []byte("|London!|\n|I lived there when I was a teenager.|\n|The years were 1966-1967.|\n|During that time, Walt Disney died, and the six-day war occurred.|")
	//	               0123456789 012345678901234567890123456789012345678 9012345678901234567890123456 78901234567890123456789012345678901234567890123456789012345678901234
	//                            1         2         3         4          5         6         7          8         9         10        11        12        13        14
	indexes, err := FindAllLineSliceIndexes(content)
	if err != nil {
		t.Error(err)
	}
	expectedLen := 4
	if len(indexes) != expectedLen {
		t.Errorf("expected len(indexes) == %d, got %d", expectedLen, len(indexes))
	}
	expect := "[[0 9] [10 48] [49 76] [77 144]]"
	got := fmt.Sprintf("%v", indexes)
	if got != expect {
		t.Errorf("expected indexes == %s but got %s", expect, got)
	}
}
func TestFindAllLineCol(t *testing.T) {
	content := []byte("|London!|\n|I lived there when I was a teenager.|\n|The years were then 1966-1967.|\n|During that time, Walt Disney died, and the six-day war occurred.|\n|We often took the tube.|")
	indexMap, err := FindAllLineCol(content, "\\bthe\\b")
	if err != nil {
		t.Error(err)
	}
	expect := "map[3:[[41 44]] 4:[[15 18]]]"
	got := fmt.Sprintf("%v", indexMap)
	if got != expect {
		t.Errorf("expected indexMap == %s but got %s", expect, got)
	}
}
func TestFindStringIndexes(t *testing.T) {
	//	text := " Ἀγαπητοί, ἀγαπῶμεν ἀλλήλους, ὅτι ἡ ἀγάπη ἐκ τοῦ Θεοῦ ἐστι, καὶ πᾶς ὁ ἀγαπῶν ἐκ τοῦ Θεοῦ γεγέννηται καὶ γινώσκει τὸν Θεόν. Ὁ μὴ ἀγαπῶν, οὐκ ἔγνω τὸν Θεόν. ὅτι ὁ Θεὸς ἀγάπη ἐστίν. Ἐν τούτῳ ἐφανερώθη ἡ ἀγάπη τοῦ Θεοῦ ἐν ἡμῖν, ὅτι τὸν υἱὸν αὐτοῦ τὸν μονογενῆ ἀπέσταλκεν ὁ Θεὸς εἰς τὸν κόσμον, ἵνα ζήσωμεν διʼ αὐτοῦ. Ἐν τούτῳ ἐστὶν ἡ ἀγάπη, οὐχ ὅτι ἡμεῖς ἠγαπήσαμεν τὸν Θεόν, ἀλλʼ ὅτι αὐτὸς ἠγάπησεν ἡμᾶς καὶ ἀπέστειλε τὸν Υἱὸν αὐτοῦ ἱλασμὸν περὶ τῶν ἁμαρτιῶν ἡμῶν. Ἀγαπητοί, εἰ οὕτως ὁ Θεὸς ἠγάπησεν ἡμᾶς, καὶ ἡμεῖς ὀφείλομεν ἀλλήλους ἀγαπᾷν." // "ά β ά β ά β"
	text := "ά ἀ"
	//	pattern := `(?:^|\W)α\P{M}(?:^|\W)`
	pattern := `\p{L}\p{M}*` //`\P{M}*α` // \P{LI}
	textNFD := ltstring.ToUnicodeNFD(text)
	fmt.Printf("len(textNFD) == %d\n", len(textNFD))
	fmt.Printf("%U\n", []rune(textNFD))
	for i := 0; i < len(textNFD); i++ {
		fmt.Printf("%x ", textNFD[i])
	}
	fmt.Println()
	matchResults, count, _ := FindStringIndexes(text, pattern, true)
	var names []string
	for name, table := range unicode.Categories {
		if unicode.Is(table, rune(`῀`[0])) {
			names = append(names, name)
		}
	}
	//if err != nil {
	//	t.Error(err)
	//}
	expectCount := 3
	if count != expectCount {
		t.Errorf("expected %d occurrences, got %d", expectCount, count)
	}
	sb := strings.Builder{}
	for _, m := range matchResults {
		if m.Match {
			sb.WriteString("|")
		}
		sb.WriteString(m.Value)
		if m.Match {
			sb.WriteString("|")
		}
	}
	fmt.Println(sb.String())
}
