package ltRegEx

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/gr"
	"github.com/liturgiko/doxa/pkg/utils/ltUnicode"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"golang.org/x/text/unicode/norm"
	"regexp"
	"strings"
)

type CodePointMeta struct {
	CodePoint    string
	Character    string
	Category     string
	CategoryName string
	Name         string
}

var lineBreakPattern = "(\\r\\n|\\n)" // handle both CRLF (windows) and LF (Unix like)

// FindAllIndexes returns the content[i,j] for each match of the pattern in the content.
func FindAllIndexes(content []byte, pattern string) ([][]int, error) {
	re, err := regexp.Compile(pattern)
	if err != nil {
		return nil, err
	}
	return re.FindAllIndex(content, -1), nil
}

// FindAllLineIndexes gives the content[i:j] for each line break found in the content
func FindAllLineIndexes(content []byte) ([][]int, error) {
	re, err := regexp.Compile(lineBreakPattern) // windows uses \r\n, all others \n
	if err != nil {
		return nil, err
	}
	return re.FindAllIndex(content, -1), nil
}

// FindAllLineStartIndexes gives the content[i] for the start of each line, starting with i==0
func FindAllLineStartIndexes(content []byte) ([]int, error) {
	var result []int
	result = append(result, 0) // first line always starts at zero
	re, err := regexp.Compile(lineBreakPattern)
	if err != nil {
		return nil, err
	}
	var indexes [][]int
	indexes, err = re.FindAllIndex(content, -1), nil
	if err != nil {
		return nil, err
	}
	for _, index := range indexes {
		result = append(result, index[1])
	}
	return result, nil
}

// FindAllLineSliceIndexes returns the slice indexes to get the text of each line in the content
func FindAllLineSliceIndexes(content []byte) ([][]int, error) {
	var result [][]int
	lineIndexes, err := FindAllLineStartIndexes(content)
	if err != nil {
		return nil, err
	}
	switch len(lineIndexes) {
	case 0:
		return nil, nil
	case 1:
		{
			var l []int
			l = append(l, 0)
			l = append(l, len(content))
			result = append(result, l)
		}
	case 2:
		{
			var l []int
			l = append(l, 0)
			l = append(l, lineIndexes[1]-1)
			result = append(result, l)
			l = nil
			l = append(l, lineIndexes[1])
			l = append(l, len(content))
			result = append(result, l)
		}
	default:
		{
			// expect [0 9] [10 48] [49 75]
			// set the first line slice
			var l []int
			l = append(l, 0)
			l = append(l, lineIndexes[1]-1)
			result = append(result, l)

			// set the middle line slices
			j := len(lineIndexes) - 1
			for i := 1; i < j; i++ {
				l = nil
				l = append(l, lineIndexes[i])
				l = append(l, lineIndexes[i+1]-1)
				result = append(result, l)
			}
			// set the last line slice
			l = nil
			l = append(l, lineIndexes[j])
			l = append(l, len(content))
			result = append(result, l)
		}
	}
	return result, nil
}

// FindAllLineCol returns a map where each key is a line number and the values are lineContent[i:j] ranges of each match.
// This assumes that before using the ranges you have converted the content to a slice of lines of text.
func FindAllLineCol(content []byte, pattern string) (map[int][][]int, error) {
	result := make(map[int][][]int)
	matchingIndexes, err := FindAllIndexes(content, pattern)
	if err != nil {
		return nil, err
	}
	var lineIndexes []int
	lineIndexes, err = FindAllLineStartIndexes(content)
	if err != nil {
		return nil, err
	}
	for _, matchIndex := range matchingIndexes {
		var lineNbr int
		// find out which line this match occurs in
		for i, lineIndex := range lineIndexes {
			if lineIndex > matchIndex[0] {
				break
			} else {
				lineNbr = i
			}
		}
		var colIndex []int
		colIndex = append(colIndex, matchIndex[0]-lineIndexes[lineNbr])
		colIndex = append(colIndex, matchIndex[1]-lineIndexes[lineNbr])

		var x [][]int
		var ok bool
		if x, ok = result[lineNbr]; ok {
			x = append(x, colIndex)
		} else {
			x = append(x, colIndex)
		}
		result[lineNbr] = x
	}
	return result, nil
}

type LineMatch struct {
	Line   int
	Slices []Slice
}
type Slice struct {
	From int
	To   int
}

func FindStringIndexes(text string, pattern string, nfd bool) (matchResults []MatchResult, count int, err error) {
	if nfd {
		pattern = ltstring.ToUnicodeNFD(pattern)
		text = ltstring.ToUnicodeNFD(text)
	} else {
		pattern = ltstring.ToUnicodeNFC(pattern)
		text = ltstring.ToUnicodeNFC(text)
	}
	re, err := regexp.Compile(pattern)
	if err != nil {
		return nil, 0, err
	}
	byteText := []byte(text)
	cursor := 0

	for _, match := range re.FindAllStringIndex(text, -1) {
		if match[0] > cursor {
			matchResult := MatchResult{fmt.Sprintf("%s", byteText[cursor:match[0]]), false}
			matchResults = append(matchResults, matchResult)
		}
		matchResult := MatchResult{fmt.Sprintf("%s", byteText[match[0]:match[1]]), true}
		matchResults = append(matchResults, matchResult)
		cursor = match[1]
		count++
	}
	if count > 0 && cursor < len(byteText) {
		matchResult := MatchResult{fmt.Sprintf("%s", byteText[cursor:]), false}
		matchResults = append(matchResults, matchResult)
	}
	return matchResults, count, nil
}

type MatchResult struct {
	Value string
	Match bool
}

// Diacritics provide a unicode based regular expression for diacritics
const Diacritics = `\p{M}{0,5}`

func RegExSplit(s string, pattern string) ([]string, error) {
	re, err := regexp.Compile(pattern)
	if err != nil {
		return nil, err
	}
	return re.Split(s, -1), nil
}

// ExpandedRegEx is necessary for matches that are diacritic-insensitive.
// A pattern can be diacritic-insensitive as a whole (analogous to a
// regular expression global case-insensitive), or for a specified character.
// Doxa allows a specific character to be treated diacritic-insensitive if
// it is followed by a \m or \μ.  The Greek mu is allowed so the user does
// not have to switch keyboards. For example, α\μ means match any form of
// alpha.  This function "expands" the pattern by replacing \m or \μ with
// the value of the constant Diacritics (see above).
// If the parameter diacriticInsensitive is true, then every character
// in the pattern that is allowed to have a diacritic will have the value
// of Diacritics suffixed to it.
// If a \m or \μ is found in the pattern, or diacriticInsensitive is true,
// the function returns requiresNFD as true.
func ExpandedRegEx(pattern string, caseInsensitive, diacriticInsensitive, greekNfc bool) (expandedPattern string, requiresNFD bool, err error) {
	requiresNFD = diacriticInsensitive // even if false here, might be set true below...
	var ia norm.Iter
	ia.InitString(norm.NFC, pattern)
	var lastWasBackslash bool
	var inDiacriticMode bool
	var inUnicodeClassMode bool
	var expectOpeningBrace bool // {
	var expectClosingBrace bool // }
	var expectOpeningParen bool // (
	var expectClosingParen bool // )

	sb := strings.Builder{}

	for !ia.Done() {
		token := ia.Next()
		strToken := string(token)

		if expectOpeningBrace {
			if strToken == "{" {
				sb.WriteString("{")
				expectOpeningBrace = false
				continue
			} else {
				return pattern, false, fmt.Errorf("opening brace missing - should be \\p{}, e.g. \\p{Z}")
			}
		}
		if expectClosingBrace {
			if strToken == "}" {
				expectClosingBrace = false
				lastWasBackslash = false
				inUnicodeClassMode = false
				sb.WriteString("}")
				continue
			} else {
				return pattern, false, fmt.Errorf("closing brace missing - should be \\p{}, e.g. \\p{Z}")
			}
		}
		if expectOpeningParen {
			if strToken == "(" {
				expectOpeningParen = false
				continue
			} else {
				return pattern, false, fmt.Errorf("opening parenthesis missing - should be \\m(), e.g. \\m(α)")
			}
		}
		if expectClosingParen {
			if strToken == ")" {
				expectClosingParen = false
				lastWasBackslash = false
				inDiacriticMode = false
				continue
			} else {
				return pattern, false, fmt.Errorf("closing parenthesis missing - should be \\m(), e.g. \\\\m(α)\"")
			}
		}

		if strToken == `\` {
			lastWasBackslash = true
			continue
		}
		if lastWasBackslash {
			if strToken == `m` || strToken == `μ` {
				inDiacriticMode = true
				expectOpeningParen = true
				if strToken == "m" {
					requiresNFD = true
				}
				lastWasBackslash = false
				continue
			} else if strToken == "p" || strToken == "P" {
				inUnicodeClassMode = true
				expectOpeningBrace = true
				lastWasBackslash = false
				sb.WriteString(`\`)
				sb.WriteString(strToken)
				continue
			}
		}
		if inDiacriticMode {
			if greekNfc {
				sb.WriteString(gr.ExpandedChar(strToken, caseInsensitive, diacriticInsensitive))
			} else {
				sb.WriteString(strToken)
				sb.WriteString(Diacritics)
			}
			expectClosingParen = true
			continue
		}
		if inUnicodeClassMode {
			sb.WriteString(strToken)
			expectClosingBrace = false
			continue
		}
		if lastWasBackslash {
			sb.WriteString(`\`)
			lastWasBackslash = false
			continue
		}
		if greekNfc {
			sb.WriteString(gr.ExpandedChar(strToken, caseInsensitive, diacriticInsensitive))
			continue
		}
		if diacriticInsensitive {
			// if doing a diacritic insensitive match,
			// and it is a letter, we add the
			// unicode properties for diacritics
			// after the word.
			sb.WriteString(strToken)
			if ltUnicode.CanBeCombined(strToken) {
				sb.WriteString(Diacritics)
			}
			continue
		}
		// if we get this far,
		// it is not a character
		// that requires special processing...
		sb.WriteString(strToken)
	}
	if requiresNFD {
		expandedPattern = norm.NFD.String(sb.String())
	} else {
		expandedPattern = sb.String() // already in NFC
	}
	return expandedPattern, requiresNFD, nil
}

const CaseInsensitive = "(?i)"

// SetCaseInsensitive prefixes the pattern with the
// case-insensitive regular expression used by go.
func SetCaseInsensitive(pattern string) string {
	return "(?i)" + pattern
}

// WordBoundary provides a unicode based regular expression for a word boundary.
// This is necessary because \b only works for ASCII characters.
const WordBoundary = `(^|$|^[^\p{L}]|[^\p{L}])`

// WholeWord wraps a pattern with a regular expression for word boundaries.
// This is necessary because \b only works with ASCII.
func WholeWord(pattern string) string {
	return WordBoundary + pattern + WordBoundary
}
