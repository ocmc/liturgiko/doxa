package stamp

import (
	"strings"
	"testing"
	"time"
)

func TestStamp_Start(t *testing.T) {
	st := NewStamp("Test Timer")
	msg := st.Start()
	if msg != "Start Test Timer" {
		t.Errorf("expected 'Start Test Timer', got '%s'", msg)
	}
}

func TestStamp_Finish(t *testing.T) {
	st := NewStamp("Test Timer")
	st.Start()
	time.Sleep(1 * time.Second)
	msg := st.Finish()
	if !strings.Contains(msg, "End Test Timer") {
		t.Errorf("expected message to contain 'End Test Timer', got '%s'", msg)
	}
	if !strings.Contains(msg, "1s") {
		t.Errorf("expected elapsed time to be about 1s, got '%s'", msg)
	}
}

func TestStamp_PauseAndResume(t *testing.T) {
	st := NewStamp("Test Timer")
	st.Start()
	time.Sleep(500 * time.Millisecond)
	pauseMsg := st.Pause()
	if pauseMsg != "Paused Test Timer" {
		t.Errorf("expected 'Paused Test Timer', got '%s'", pauseMsg)
	}
	time.Sleep(1 * time.Second) // This time should not be counted
	resumeMsg := st.Resume()
	if resumeMsg != "Resumed Test Timer" {
		t.Errorf("expected 'Resumed Test Timer', got '%s'", resumeMsg)
	}
	time.Sleep(500 * time.Millisecond)
	msg := st.FinishMillis()
	if !strings.Contains(msg, "End Test Timer") {
		t.Errorf("expected message to contain 'End Test Timer', got '%s'", msg)
	}
	elapsedStr := strings.Split(msg, "took ")[1]
	elapsed, err := time.ParseDuration(elapsedStr)
	if err != nil {
		t.Errorf("failed to parse duration: %v", err)
	}
	if elapsed < 900*time.Millisecond || elapsed > 1100*time.Millisecond {
		t.Errorf("expected elapsed time to be about 1s, got '%v'", elapsed)
	}
}

func TestStamp_MultiplePauses(t *testing.T) {
	st := NewStamp("Test Timer")
	st.Start()
	time.Sleep(300 * time.Millisecond)
	st.Pause()
	time.Sleep(500 * time.Millisecond) // Should not be counted
	st.Resume()
	time.Sleep(300 * time.Millisecond)
	st.Pause()
	time.Sleep(500 * time.Millisecond) // Should not be counted
	st.Resume()
	time.Sleep(400 * time.Millisecond)
	msg := st.FinishMillis()
	elapsedStr := strings.Split(msg, "took ")[1]
	elapsed, err := time.ParseDuration(elapsedStr)
	if err != nil {
		t.Errorf("failed to parse duration: %v", err)
	}
	if elapsed < 900*time.Millisecond || elapsed > 1100*time.Millisecond {
		t.Errorf("expected elapsed time to be about 1s, got '%v'", elapsed)
	}
}

func TestStamp_PauseWhilePaused(t *testing.T) {
	st := NewStamp("Test Timer")
	st.Start()
	st.Pause()
	msg := st.Pause()
	if msg != "Timer is already paused" {
		t.Errorf("expected 'Timer is already paused', got '%s'", msg)
	}
}

func TestStamp_ResumeWhileNotPaused(t *testing.T) {
	st := NewStamp("Test Timer")
	st.Start()
	msg := st.Resume()
	if msg != "Timer is not paused" {
		t.Errorf("expected 'Timer is not paused', got '%s'", msg)
	}
}

func TestStamp_FinishNanosecondsNoMessage(t *testing.T) {
	st := NewStamp("Test Timer")
	st.Start()
	time.Sleep(1 * time.Millisecond)
	msg := st.FinishNanosecondsNoMessage()
	duration, err := time.ParseDuration(msg)
	if err != nil {
		t.Errorf("failed to parse duration: %v", err)
	}
	if duration < 1*time.Millisecond {
		t.Errorf("expected duration to be at least 1ms, got %v", duration)
	}
}
