package stamp

import (
	"fmt"
	"time"
)

type Stamp struct {
	start       time.Time
	pauseStart  time.Time
	totalPaused time.Duration
	IsPaused    bool
	Message     string
}

func NewStamp(name string) *Stamp {
	s := new(Stamp)
	s.Message = name
	return s
}

func (st *Stamp) Start() string {
	st.start = time.Now()
	st.totalPaused = 0
	st.IsPaused = false
	return fmt.Sprintf("Start %s", st.Message)
}

func (st *Stamp) Pause() string {
	if !st.IsPaused {
		st.pauseStart = time.Now()
		st.IsPaused = true
		return fmt.Sprintf("Paused %s", st.Message)
	}
	return "Timer is already paused"
}

func (st *Stamp) Resume() string {
	if st.IsPaused {
		st.totalPaused += time.Since(st.pauseStart)
		st.IsPaused = false
		return fmt.Sprintf("Resumed %s", st.Message)
	}
	return "Timer is not paused"
}

func (st *Stamp) elapsedTime() time.Duration {
	if st.IsPaused {
		return st.pauseStart.Sub(st.start) - st.totalPaused
	}
	return time.Since(st.start) - st.totalPaused
}

func (st *Stamp) Finish() string {
	elapsed := st.elapsedTime().Round(time.Second)
	return fmt.Sprintf("End %s, took %v", st.Message, elapsed)
}

func (st *Stamp) FinishMillis() string {
	elapsed := st.elapsedTime().Round(time.Millisecond)
	return fmt.Sprintf("End %s, took %v", st.Message, elapsed)
}

func (st *Stamp) FinishNanosecondsNoMessage() string {
	return fmt.Sprintf("%v", st.elapsedTime().Round(time.Nanosecond))
}
