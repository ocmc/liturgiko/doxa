// Package clinput provides the user a way to select an option using a command line interface.
package clinput

import (
	"bufio"
	"errors"
	"fmt"
	"golang.org/x/term"
	"os"
	"strconv"
	"strings"
)

// GetInput prompts the user to enter a single letter to select one of the options.
// The len(options) must be an even number, with elements in pairs.
// Each odd numbered element is the letter to use to selection the even numbered element.
/*
Example:
	q := "load from what?"
	var o = []string{
		"a", "dir (directory on your computer)",
		"b", "github",
		"q", "quit the command",
	}
	switch clinput.GetInput(q, o) {
	case 'a':
		fromDir = true
	case 'b':
		fromGithub = true
	case 'q':
		os.Exit(0)
	default:
		fmt.Println("Invalid selection...")
	}
*/
func GetInput(question string, options []string) rune {
	reader := bufio.NewReader(os.Stdin)
	sb := strings.Builder{}
	j := len(options)
	for i := 0; i < j; i++ {
		if i < j {
			sb.WriteString(options[i][0:1])
			i++
		}
	}
	validChars := sb.String()
	var char rune
l:
	for {
		fmt.Println(question)
		for i := 0; i < j; i++ {
			if i < j {
				fmt.Printf("%s) %s\n", options[i], options[i+1])
				i++
			}
		}
		char, _, _ = reader.ReadRune()
		if strings.Contains(validChars, string(char)) {
			break l
		} else {
			fmt.Println("Invalid selection")
		}
	}
	return char
}

// GetEntry gets user input from the command line
func GetEntry(prompt string) (string, error) {
	fmt.Print(fmt.Sprintf("%s : ", prompt))
	reader := bufio.NewReader(os.Stdin)
	// ReadString will block until the delimiter is entered
	input, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("An error occurred while reading input. Please try again", err)
		return "", err
	}
	// remove the delimiter from the string
	return strings.TrimSuffix(input, "\n"), nil
}

// GetStringInput functions like GetInput but returns a string instead of a rune
func GetStringInput(question string, options []string) string {
	return ModGetStringInput(question, options, nil, nil)
}
func ModGetStringInput(question string, options []string, pre, post func(string) string) string {
	reader := bufio.NewReader(os.Stdin)
	sb := strings.Builder{}
	j := len(options)
	handle := false
	for _, opt := range options {
		handle = !handle
		if handle {
			sb.WriteString(opt + "|")
		}
	}
	validChars := sb.String()
	var input string
l:
	for {
		fmt.Println(question)
		for i := 0; i < j; i++ {
			if i < j {
				fmt.Printf("%s) %s\n", options[i], options[i+1])
				i++
			}
		}
		input, _ = reader.ReadString('\n')
		input = strings.TrimSuffix(input, "\n")
		if pre != nil {
			input = pre(input)
		}
		if strings.Contains(validChars, input+"|") {
			break l
		} else {
			fmt.Println("Invalid selection")
		}
	}
	if post != nil {
		input = post(input)
	}
	return input
}

// GetIndexSelection takes a 1 dimensional slice of strings, to which it automatically adds numbers,
// and prompts the user to pick. the corresponding index is returned. if entry[0] is "" it will be a 1 based list
// if an invalid input is received, the user will be prompted until a valid input is received.
// If for some reason an error occurs, -1 will be returned

func GetIndexSelection(question string, options []string) int {
	return ModGetIndexSelection(question, options, nil, nil)
}
func ModGetIndexSelection(question string, options []string, pre func(string) string, post func(string) string) int {
	var menu []string
	for i, opt := range options {
		if opt == "" && i == 0 {
			continue
		}
		menu = append(menu, fmt.Sprintf("%d", i), opt)
	}
	choice := ModGetStringInput(question, menu, pre, post)
	ans, err := strconv.ParseInt(choice, 10, 64)
	if err != nil {
		return -1
	}
	if int(ans) > len(options) {
		return -1
	}
	return int(ans)
}

func GetPassword(isNew bool) (string, error) {
	input, err := term.ReadPassword(int(os.Stdin.Fd()))
	if err != nil {
		return "", err
	}

	if isNew {
		if len(input) < 2 {
			if input[0] == 'q' {
				return "", errors.New("user chose to quit")
			}
			fmt.Println("Your password is too short! (type q to quit)")
			fmt.Printf("New Password: ")
			return GetPassword(true)
		}
		fmt.Print("\nConfirm Password: ")
		input2, err := term.ReadPassword(int(os.Stdin.Fd()))
		fmt.Printf("\n")
		if err != nil {
			return "", err
		}
		if string(input2) != string(input) {
			fmt.Println("Passwords do not match! (type q to quit)")
			fmt.Printf("New Password: ")
			return GetPassword(true)
		}
	}
	fmt.Printf("\n")
	return string(input), nil
}
