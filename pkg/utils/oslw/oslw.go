// Functions for the processing of resources and templates
// in the OslwResourceLine (OCMC ShareLatex Liturgical Workbench).
// OslwResourceLine uses LaTeX to generated PDF files for up to three languages.
package oslw

import (
	"bufio"
	"errors"
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/ages/ares"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"os"
	"strings"
)

/**
* resources
    en_uk_lash
      res.titles.tex
*/
/*
\itId{en}{uk}{lash}{actors}{Bishop}{
Bishop
}%
*/
func ParseOslwResource(id, value string) (ares.LineParts, error) {
	var result ares.LineParts
	var err error
	parts := strings.Split(id, "Id{")
	if len(parts) > 1 {
		parts = strings.Split(parts[1], "}{")
		if len(parts) == 6 {
			result.Language = parts[0]
			result.Country = parts[1]
			result.Realm = parts[2]
			result.Topic = parts[3]
			result.Key = parts[4]
			result.Value = value
		} else {
			err = errors.New("invalid OslwResourceLine ID format")
		}
	} else {
		err = errors.New("invalid OslwResourceLine ID format")
	}
	return result, err
}

// Res2Kvs Reads all OslwResourceLine resource files in specified directory
// and writes them to a database opened for the specified
// dbName
func Res2Kvs(dir string, dbName string) error {
	var err error
	//	var update = true // used to avoid updating record for gr_gr_cog if already exists

	// open the database
	ds, err := kvs.NewBoltKVS(dbName)
	if err != nil {
		fmt.Errorf("error opening db %s: %v", dbName, err)
	}
	defer ds.Db.Close()
	mapper, err := kvs.NewKVS(ds)
	if err != nil {
		os.Exit(1)
	}
	mapper.Db = ds

	// process the files
	ext := "tex"
	patterns := []string{
		"res.*",
	}
	filenames, err := ltfile.FileMatcher(dir, ext, patterns)
	if err != nil {
		return err
	}
	for _, filename := range filenames {
		fileIn, err := os.Open(filename)
		if err != nil {
			fileIn.Close()
			return err
		}
		scanner := bufio.NewScanner(fileIn)
		var lineParts ares.LineParts

		for scanner.Scan() {
			line := strings.TrimSpace(scanner.Text())
			if line == "}%" {
				// write to db
				dbr := kvs.NewDbR()
				dbr.KP.Dirs.Push("ltx")
				dbr.KP.Dirs.Push(fmt.Sprintf("%s_%s_%s", lineParts.Language, lineParts.Country, lineParts.Realm))
				dbr.KP.Dirs.Push(lineParts.Topic)
				dbr.KP.KeyParts.Push(lineParts.Key)
				dbr.Value = lineParts.Value
				err := mapper.Db.Put(dbr)
				if err != nil {
					doxlog.Errorf("%v", err)
				}
				// TODO: put nnp version
			} else if strings.HasPrefix(line, "\\itId") {
				lineParts, err = ParseOslwResource(line, "")
				if err != nil {
					doxlog.Errorf("%v", err)
				}
			} else {
				lineParts.Value = line
			}
		}
		fileIn.Close()
	}
	return err
}
