package lthash

import (
	"encoding/binary"
	"github.com/cespare/xxhash/v2"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"io"
	"os"
	"path/filepath"
	"sort"
)

// HashFile calculates the xxh64 hash of a file
func HashFile(path string) (uint64, error) {
	file, err := os.Open(ltfile.ToSysPath(path))
	if err != nil {
		return 0, err
	}
	defer file.Close()

	hash := xxhash.New()
	if _, err := io.Copy(hash, file); err != nil {
		return 0, err
	}

	return hash.Sum64(), nil
}

// HashDir calculates the xxh64 hash of a directory recursively
func HashDir(path string) (uint64, error) {
	var fileHashes []uint64
	err := filepath.Walk(ltfile.ToSysPath(path), func(filePath string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			fileHash, err := HashFile(filePath)
			if err != nil {
				return err
			}
			fileHashes = append(fileHashes, fileHash)
		}
		return nil
	})
	if err != nil {
		return 0, err
	}

	// Sort the hashes to ensure consistency
	sort.Slice(fileHashes, func(i, j int) bool {
		return fileHashes[i] < fileHashes[j]
	})

	// Combine hashes in a deterministic way
	finalHash := xxhash.New()
	for _, hash := range fileHashes {
		binary.Write(finalHash, binary.LittleEndian, hash)
	}
	return finalHash.Sum64(), nil
}
