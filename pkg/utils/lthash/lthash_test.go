package lthash

import (
	"github.com/cespare/xxhash/v2"
	"os"
	"path/filepath"
	"testing"
)

func TestHashFile(t *testing.T) {
	tempFile, err := os.CreateTemp("", "test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(tempFile.Name())

	content := []byte("Hello, World!")
	if _, err := tempFile.Write(content); err != nil {
		t.Fatal(err)
	}
	tempFile.Close()

	hash, err := HashFile(tempFile.Name())
	if err != nil {
		t.Fatal(err)
	}

	// Calculate the expected hash using xxhash directly
	expectedHash := xxhash.Sum64(content)
	if hash != expectedHash {
		t.Errorf("Expected hash %d, got %d", expectedHash, hash)
	}
}

func TestHashDir(t *testing.T) {
	tempDir, err := os.MkdirTemp("", "test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(tempDir)

	// Create some files in the temp directory
	files := []struct {
		name    string
		content string
	}{
		{"file1.txt", "Hello"},
		{"file2.txt", "World"},
		{"subdir/file3.txt", "!"},
	}

	for _, f := range files {
		path := filepath.Join(tempDir, f.name)
		if err := os.MkdirAll(filepath.Dir(path), 0755); err != nil {
			t.Fatal(err)
		}
		if err := os.WriteFile(path, []byte(f.content), 0644); err != nil {
			t.Fatal(err)
		}
	}

	hash1, err := HashDir(tempDir)
	if err != nil {
		t.Fatal(err)
	}

	if hash1 == 0 {
		t.Error("Expected non-zero hash")
	}

	// Test consistency
	hash2, err := HashDir(tempDir)
	if err != nil {
		t.Fatal(err)
	}

	if hash1 != hash2 {
		t.Errorf("Hashes are not consistent. Got %d and %d", hash1, hash2)
	}

	// Test that hash changes when content changes
	if err := os.WriteFile(filepath.Join(tempDir, "file1.txt"), []byte("Changed"), 0644); err != nil {
		t.Fatal(err)
	}

	hash3, err := HashDir(tempDir)
	if err != nil {
		t.Fatal(err)
	}

	if hash3 == hash1 {
		t.Error("Hash didn't change when file content changed")
	}
}
