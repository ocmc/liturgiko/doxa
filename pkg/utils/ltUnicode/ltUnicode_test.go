package ltUnicode

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"golang.org/x/text/unicode/norm"
	"strings"
	"testing"
)

func TestGetUnicodeBoundaries(t *testing.T) {
	grave := "\u0300"
	acute := "\u0301"
	macS := "ἀγάπη"
	frS := "ἀγάπη"
	data := []string{macS, frS}
	sources := []string{"mac", "Fr S"}
	for i, s := range data {
		fmt.Printf("\nSource: %s", sources[i])
		fmt.Printf("\n%s before normalization:", s)
		fmt.Printf("\n\t%s", ToEscapedUnicode(s))
		glyphs := EscapedUnicode(s)
		fmt.Printf("\nlen(glyphs) == %d\n", len(glyphs))
		for _, ru := range glyphs {
			fmt.Printf("\n\t%s %s", ru.Str, ru.Codes)
		}
		fmt.Printf("\nAfter NFD:")
		glyphs = GetUnicodeBoundaries(s, NFD)
		for _, g := range glyphs {
			fmt.Printf("\n\t%s : %s (%d)", g.Str, g.Codes, g.Length)
		}
		fmt.Println()
		fmt.Printf("\t\nAfter NFC:")
		glyphs = Glyphs(s, NFC)
		for _, g := range glyphs {
			fmt.Printf("\n\t%s : %s (%d)", g.Str, g.Codes, g.Length)
		}
		fmt.Println()
	}

	escUni := "\u03b1\u0313\u03b3\u03b1\u0301\u03c0\u03b7"
	fmt.Println("Escaped unicode to glyphs:")
	fmt.Printf("\n\t%s", ToEscapedUnicode(escUni))
	fec := FromEscapedUnicode(ToEscapedUnicode(escUni))
	fmt.Printf("\n\t%s\n", fec)

	fmt.Println("\nDemonstration of ability to find and replace a diacritic")
	data = []string{"ῲ", "ἂ"}
	for _, o := range data {
		nfd := norm.NFD.String(o)
		graveToAcute := strings.ReplaceAll(nfd, grave, acute)
		acuteToGrave := strings.ReplaceAll(nfd, acute, grave)
		fmt.Printf("\nnfd: %s -> %s", norm.NFC.String(o), norm.NFC.String(graveToAcute))
		fmt.Printf("\nnfd: %s -> %s", norm.NFC.String(graveToAcute), norm.NFC.String(acuteToGrave))
	}
	fmt.Println()
	fmt.Println(ToEscapedUnicode(ltstring.ToUnicodeNFC("ω")))
	fmt.Println(ToEscapedUnicode("ω"))
	fmt.Println()
	var ia norm.Iter
	ia.InitString(norm.NFKC, `α\μγα\μπη`)
	for !ia.Done() {
		n := ia.Next()
		fmt.Printf("|%s|", n)
	}

}

func TestX(t *testing.T) {
	data := []string{"a", "e", "i", "o", "u", "t", "α", "ε", "η", "ι", "ο", "ρ", "ω", "τ"}
	for _, d := range Diacritics {
		for _, l := range data {
			fmt.Printf("\n%s can be combined with %s: %v", l, d, canBeCombined(l, d))
		}
	}
}
func TestToEscapedUnicode(t *testing.T) {
	//	s := "ʼ᾽" // \u02bc apostrophe \u1fbd Greek Koronis
	s := "Ϛ"
	//r1 := []rune("᾽")
	//r2 := []rune("'")
	//fmt.Printf("%v %v %v %v\n", r1, r2, []byte("'"), []byte(""))
	fmt.Printf("%s\n", ToEscapedUnicode(s))
	//nfc := ToUnicodeNFD("λ᾽")
	//nfd := ToUnicodeNFD(nfc)
	//fmt.Printf("%s - nfc %d nfd %d\n", nfc, len(nfc), len(nfd))
}
func TestUnderline(t *testing.T) {
	fmt.Println(Underline("All has been enlightened by Your Resur_rec_tion, O Lord."))
}
