package ltUnicode

import (
	"fmt"
	"golang.org/x/text/runes"
	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
	"strconv"
	"strings"
	"unicode"
)

func ToUnicodeNFC(text string) string {
	return norm.NFC.String(text)
}
func ToUnicodeNFD(text string) string {
	return norm.NFD.String(text)
}

// ToNfcNoDiacritics converts text to Unicode NFC with no diacritic marks, aka combining characters.
func ToNfcNoDiacritics(text string) string {
	rmvAccents := transform.Chain(norm.NFD, runes.Remove(runes.In(unicode.Mn)), norm.NFC)
	result, _, _ := transform.String(rmvAccents, text)
	return result
}

// ToNfdNoDiacritics converts text to Unicode NFD with no diacritic marks, aka combining characters.
func ToNfdNoDiacritics(text string) string {
	rmvAccents := transform.Chain(norm.NFD, runes.Remove(runes.In(unicode.Mn)))
	result, _, _ := transform.String(rmvAccents, text)
	return result
}

type GlyphMeta struct {
	Str    string
	Length int
	Codes  string
}

func GetUnicodeBoundaries(t string, f Form) (glyphs []GlyphMeta) {
	var s string
	switch f {
	case NFC:
		s = norm.NFC.String(t)
	case NFD:
		s = norm.NFD.String(t)
	}
	for i := 0; i < len(s); {
		d := norm.NFC.NextBoundaryInString(s[i:], true)
		c := fmt.Sprintf("%s", s[i:i+d])
		l := len(c)
		glyphs = append(glyphs, GlyphMeta{c, l, fmt.Sprintf("%+q", s[i:i+d])})
		i += d
	}
	return glyphs
}

type Form int

const (
	NFC Form = iota
	NFD
)

// Glyphs returns a slice containing each glyph, its length in bytes, and its Unicode code point.
// This func differs from GlyphsRaw in that the text
// must be converted the text to either NFC or NFD.
func Glyphs(s string, f Form) (glyphs []GlyphMeta) {
	//matcher := regexp.MustCompile(`(\p{P}|\p{Z})`)
	var ia norm.Iter
	switch f {
	case NFC:
		ia.InitString(norm.NFKC, s)
	case NFD:
		ia.InitString(norm.NFKD, s)
	}
	for !ia.Done() {
		n := ia.Next()
		l := len(n)
		g := strings.ToUpper(fmt.Sprintf("U+%04X", n))
		if len(g) > 3 {
			k := g[2:]
			var d string
			var ok bool
			if d, ok = UnicodeNameMap[k]; ok {
				g = fmt.Sprintf("%s %s", g, d)
			}
		}
		//var g string
		//if matcher.FindIndex(n) != nil {
		//	g = fmt.Sprintf("%U", n)
		//} else {
		//	g = fmt.Sprintf("%+q", n)
		//	g = "[U+" + g[3:len(g)-1] + "]" // just so the result matches the %U above in format
		//}
		glyphs = append(glyphs, GlyphMeta{fmt.Sprintf("%s", n), l, g})
	}
	return glyphs
}

// GlyphsRaw returns a slice containing each glyph, its length in bytes, and its Unicode code point.
// This func differs from Glyphs in that the text
// is not converted. It stays in whatever form it is.
func GlyphsRaw(s string) (glyphs []GlyphMeta) {
	//matcher := regexp.MustCompile(`(\p{P}|\p{Z})`)
	for _, n := range s {
		c := string(n)
		l := len(c)
		g := fmt.Sprintf("U+%04X", n)
		if len(g) > 3 {
			k := g[2:]
			var d string
			var ok bool
			if d, ok = UnicodeNameMap[k]; ok {
				g = fmt.Sprintf("%s %s", g, d)
			}
		}

		//
		//var g string
		//if matcher.FindIndex([]byte(c)) != nil {
		//	g = fmt.Sprintf("%U", n)
		//} else {
		//	g = fmt.Sprintf("%+q", n)
		//	if len(g) > 3 {
		//		g = "[U+" + g[3:len(g)-1] + "]" // just so the result matches the %U above in format
		//	}
		//}

		glyphs = append(glyphs, GlyphMeta{fmt.Sprintf("%s", c), l, g})
	}
	return glyphs
}

// EscapedUnicode provides the \uxxxx code for the string
func EscapedUnicode(s string) (glyphs []GlyphMeta) {
	theRunes := []rune(s)
	for _, ru := range theRunes {
		quoted := strconv.QuoteRuneToASCII(ru)
		unquoted := quoted[1 : len(quoted)-1]
		strRu := string(ru)
		l := len(strRu)
		//		fmt.Printf("%s %s\n", string(ru), unquoted)
		glyphs = append(glyphs, GlyphMeta{fmt.Sprintf("%s", strRu), l, unquoted})
	}
	return glyphs
}

// FromEscapedUnicode coverts a string of escaped unicode,
// e.g. "\u03b1\u0313\u03b3\u03b1\u0301\u03c0\u03b7" and returns ἀγάπη
// Note that a string of escaped unicode is automatically converted to characters,
// so this function is unnecessary.  It is here in case you wonder
// how to do it.
func FromEscapedUnicode(s string) string {
	return fmt.Sprintf("%s", s)
}
func ToEscapedUnicode(s string) string {
	var r []string
	theRunes := []rune(s)
	for _, ru := range theRunes {
		quoted := strconv.QuoteRuneToASCII(ru)
		unquoted := quoted[1 : len(quoted)-1]
		r = append(r, unquoted)
	}
	return strings.Join(r, "")
}

// Diacritics is used with func CanBeCombined.
// Currently, only the Latin combining grave accent
// and the Greek rough breathing mark are used.
// Others could be added if when used by CanBeCombined
// an incorrect result is occurring for a particular language.
var Diacritics = []string{"\u0300", // Latin, combining grave accent
	"\u0314", // rough breathing mark.  This allows CanBeCombined("ρ") to be correctly categorized.
}

// CanBeCombined returns true if a unicode diacritic can be added to character s
// It does this as follows:
// For each diacritic in var Diacritics (above)...
// Combines s with the diacritic,
// Normalizes the combination to Unicode NFC
// Checks to see if the result is a single glyph.
// If it is, then it is assumed that s is a character with which
// unicode combining marks can be combined.
func CanBeCombined(s string) bool {
	for _, d := range Diacritics {
		if canBeCombined(s, d) {
			return true
		}
	}
	return false
}

// canBeCombined returns true if base (a unicode letter) can be combined with the diacritic.
// This is determined by normalizing base+diacritic to NFC.
// Then the NFC form is converted to glyphs.  If it is a single
// glyph, then that base and diacritic can be combined.
func canBeCombined(base string, diacritic string) bool {
	return len(EscapedUnicode(norm.NFC.String(base+diacritic))) == 1
}

// Underline adds the unicode underline diacritic \u0332 to letters
// between _, e.g. Resur_rec_tion -> Resur̲r̲e̲ction
func Underline(s string) string {
	var on bool
	var ia norm.Iter
	ia.InitString(norm.NFC, s)
	sb := strings.Builder{}

	for !ia.Done() {
		token := string(ia.Next())
		if token == "_" {
			on = !on
			continue
		}
		if on {
			sb.WriteString("\u0332")
		}
		sb.WriteString(token)
	}
	return sb.String()
}
