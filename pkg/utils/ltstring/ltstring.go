// Package ltstring provides string utilities for liturgical text processing
// The package is essentially a string package, but has
// lt as a prefix to avoid name collisions.
package ltstring

import (
	"encoding/json"
	"errors"
	"fmt"
	"golang.org/x/text/runes"
	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
	"html"
	"path"
	"runtime"
	"strconv"
	"strings"
	"text/scanner"
	"time"
	"unicode"
)

var idDelimiter = ":"
var segmentDelimiter = "/"

// RemovePunctuation removes punctuation from a string
func RemovePunctuation(text string) string {
	rmvPunct := transform.Chain(norm.NFD, runes.Remove(runes.In(unicode.Punct)), norm.NFC)
	var result string
	result, _, _ = transform.String(rmvPunct, text)
	return result
}

// ToUnderscored replaces all occurrences of _ with <u> and __ with </u>
func ToUnderscored(text string) string {
	u := strings.ReplaceAll(text, "__", "</u>")
	return strings.ReplaceAll(u, "_", "<u>")
}
func ToNfcNoDiacritics(s string) string {
	return norm.NFC.String(ToNfdNoDiacritics(s))
}
func ToNfdNoDiacritics(s string) string {
	rmvAccents := transform.Chain(norm.NFD, runes.Remove(runes.In(unicode.Mn)), norm.NFC)
	result, _, _ := transform.String(rmvAccents, s)
	return result
}

// ToNnp converts text to a Normalized form with no punctuation. It will be lowercase with no diacritics or punctuation.
func ToNnp(text string) string {
	rmvAccents := transform.Chain(norm.NFD, runes.Remove(runes.In(unicode.Mn)), norm.NFC)
	rmvPunct := transform.Chain(norm.NFD, runes.Remove(runes.In(unicode.Punct)), norm.NFC)
	result, _, _ := transform.String(rmvAccents, text)
	result, _, _ = transform.String(rmvPunct, result)
	result = strings.ToLower(result)
	return result
}

// ToNwp converts text to a Normalized form with punctuation. It will be lowercase with no diacritics. Punctuation is preserved.
func ToNwp(text string) string {
	rmvAccents := transform.Chain(norm.NFD, runes.Remove(runes.In(unicode.Mn)), norm.NFC)
	result, _, _ := transform.String(rmvAccents, text)
	result = strings.ToLower(result)
	return result
}
func ToUnicodeNFC(text string) string {
	return norm.NFC.String(text)
}
func ToUnicodeNFD(text string) string {
	return norm.NFD.String(text)
}

// ToNnd converts text to a Normalized form with no diacritic marks.
func ToNnd(text string) string {
	rmvAccents := transform.Chain(norm.NFD, runes.Remove(runes.In(unicode.Mn)), norm.NFC)
	result, _, _ := transform.String(rmvAccents, text)
	return result
}

// ToId merges canonical OLW ID from supplied parts
func ToId(lang string, country string, realm string, topic string, key string) string {
	return ToDomain(lang, country, realm) + segmentDelimiter + topic + idDelimiter + key
}
func ToIDParts(id string) (string, string, string, error) {
	if strings.HasPrefix(id, "ltx") {
		id = id[4:]
	}
	tmpId := strings.ReplaceAll(id, ":", "/")
	parts := strings.Split(tmpId, "/")
	if len(parts) == 2 {
		parts = strings.Split(id, ":")
		if len(parts) == 2 {
			return "", parts[0], parts[1], nil
		} else {
			return "", "", "", fmt.Errorf("%s is invalid id", id)
		}
	} else if len(parts) == 3 {
		return parts[0], parts[1], parts[2], nil
	} else {
		return "", "", "", fmt.Errorf("%s is invalid id. Only has %d parts", id, len(parts))
	}
}

// LibraryFromId returns the library portion of a library/topic/key ID.
// This form is used in AGES Liturgical Workbench and OLW.
// Doxa has a different form.
func LibraryFromId(id string) (string, error) {
	library, _, _, err := ToIDParts(id)
	if err != nil {
		return "", err
	}
	return library, nil
}
func LibraryFromDoxaId(id string) (string, error) {
	parts := strings.Split(id, "/")
	if len(parts) == 1 {
		return "", fmt.Errorf("invalid ID %s", id)
	}
	if strings.HasPrefix(id, "ltx/") {
		return parts[1], nil
	} else {
		return parts[0], nil
	}
}
func TopicFromId(id string) (string, error) {
	_, topic, _, err := ToIDParts(id)
	if err != nil {
		return "", err
	}
	return topic, nil
}
func KeyFromId(id string) (string, error) {
	_, _, key, err := ToIDParts(id)
	if err != nil {
		return "", err
	}
	return key, nil
}
func TopicKeyFromId(id string) (string, error) {
	_, topic, key, err := ToIDParts(id)
	if err != nil {
		return "", err
	}
	return topic + ":" + key, nil
}

// ToTK splits topicKey into its parts.
// Returns an error if it len(parts) != 2
func ToTK(topicKey string) (string, string, error) {
	parts := strings.Split(topicKey, idDelimiter)
	if len(parts) == 1 {
		// try legacy method
		parts = strings.Split(topicKey, segmentDelimiter)
	}
	if len(parts) == 2 {
		return parts[0], parts[1], nil
	} else {
		return "", "", errors.New(fmt.Sprintf("could not split %s into topic and key using %s", topicKey, idDelimiter))
	}
}

// ToDomain creates canonical OLW domain (aka library) from parts
func ToDomain(language string, country string, realm string) string {
	return strings.ToLower(language + "_" + country + "_" + realm)
}

// Timestamp returns a timestamp that is standardized for Liturgical Text Software (LTS).
// Always returns UTC.
// Example: 2019-08-14 12:50:29.275002 +0000 UTC
func Timestamp() string {
	return time.Now().UTC().String()
}
func Timestamp2() string {
	return time.Now().Format("Monday, 2006-01-02 15:04:05 -0700")
}
func ToLibraryParts(library string) ([]string, error) {
	parts := strings.Split(library, "_")
	if len(parts) == 3 {
		return parts, nil
	}
	return nil, errors.New(fmt.Sprintf("%s not a valid library, it is missing one or more of the three parts of a library name", library))
}
func LangFromLibrary(library string) (string, error) {
	parts, err := ToLibraryParts(library)
	if err != nil {
		return "", err
	}
	return parts[0], nil
}
func CountryFromLibrary(library string) (string, error) {
	parts, err := ToLibraryParts(library)
	if err != nil {
		return "", err
	}
	return parts[1], nil
}
func RealmFromLibrary(library string) (string, error) {
	parts, err := ToLibraryParts(library)
	if err != nil {
		return "", err
	}
	return parts[2], nil
}
func EscapeHtml(s string) string {
	return html.EscapeString(s)
}
func Error(err error) error {
	if err != nil {
		pc, fn, line, _ := runtime.Caller(1)
		return fmt.Errorf("[error] in %s[%s:%d] %v", runtime.FuncForPC(pc).Name(), fn, line, err)
	}
	return nil
}

// WrappedLines returns a string with newlines inserted
// every n characters, where n = width. Each line is padded
// by n spaces, where n = leftMargin.  Words are not split.
// The last line will not have a newline character.
func WrappedLines(value string, leftMargin, width, keyWidth int) string {
	var s scanner.Scanner
	s.Init(strings.NewReader(value))
	s.Filename = "value"
	padding := fmt.Sprintf("%*s", leftMargin, " ")
	result := strings.Builder{}
	line := strings.Builder{}
	line.WriteString(" ")
	tokens := strings.Split(value, " ")
	firstLine := true
	for _, token := range tokens {
		size := line.Len() + len(token) + 1
		if firstLine {
			size = size + leftMargin + keyWidth
		}
		if size > width {
			firstLine = false
			result.WriteString(line.String())
			result.WriteString("\n")
			line = strings.Builder{}
			line.WriteString(padding)
			line.WriteString(" ")
		}
		line.WriteString(token)
		line.WriteString(" ")
	}
	result.WriteString(line.String())
	return result.String()
}
func GetWildcard(id string) string {
	// note: this must match patters in package ldp function RelativeTopic().
	if strings.HasPrefix(id, "da.") {
		return "da.*"
	}
	if strings.HasPrefix(id, "eo.") {
		return "eo.*"
	}
	if strings.HasPrefix(id, "eu.") {
		return "eu.*"
	}
	if strings.HasPrefix(id, "he.") {
		return "he.*"
	}
	if strings.HasPrefix(id, "ho.") {
		return "ho.*"
	}
	if strings.HasPrefix(id, "ka.") {
		return "ka.*"
	}
	if strings.HasPrefix(id, "le.ep.mc.") {
		return "le.ep.mc.*"
	}
	if strings.HasPrefix(id, "le.ep.me.") {
		return "le.ep.me.*"
	}
	if strings.HasPrefix(id, "le.go.eo.") {
		return "le.go.eo.*"
	}
	if strings.HasPrefix(id, "le.go.lu.") {
		return "le.go.lu.*"
	}
	if strings.HasPrefix(id, "le.go.mc.") {
		return "le.go.mc.*"
	}
	if strings.HasPrefix(id, "le.go.me.") {
		return "le.go.me.*"
	}
	if strings.HasPrefix(id, "le.pr.tr.") {
		return "le.pr.tr.*"
	}
	if strings.HasPrefix(id, "me.") {
		return "me.*"
	}
	if strings.HasPrefix(id, "oc.") {
		return "oc.*"
	}
	if strings.HasPrefix(id, "pe.") {
		return "pe.*"
	}
	if strings.HasPrefix(id, "sy.") {
		return "sy.*"
	}
	if strings.HasPrefix(id, "tr.") {
		return "tr.*"
	}
	if strings.HasPrefix(id, "ty.") {
		return "ty.*"
	}
	return ""
}
func PrettyStruct(data interface{}) (string, error) {
	val, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		return "", err
	}
	return string(val), nil
}

// CountWords return the number of substrings surrounded by whitespace.
// This is not a linguistically accurate word count, and just an
// approximation.
func CountWords(s string) int {
	// strings.Fields() splits a string around one or more
	// consecutive whitespaces (https://golang.org/pkg/unicode/#IsSpace)
	// The number of fields is the actual number of words
	return len(strings.Fields(s))
}
func ToIntSlice(s string) (intSlice []int, err error) {
	parts := strings.Split(s, ",")
	var i int
	for _, p := range parts {
		if i, err = strconv.Atoi(strings.TrimSpace(p)); err != nil {
			return nil, fmt.Errorf("pkg/utils/ltstring.ToIntSlice() %s not a number: %v", s, err)
		}
		intSlice = append(intSlice, i)
	}
	return intSlice, nil
}
func MonthDayFromServiceTemplateId(id string) (int, int, error) {
	var err error
	var month int
	var day int
	parts := strings.Split(id, ".")
	for _, p := range parts {
		if strings.HasPrefix(p, "m") {
			month, err = strconv.Atoi(p[1:3])
			if err != nil {
				return month, day, fmt.Errorf("not a day number in %s", id)
			}
			if month > 0 && month < 13 {
				// ok
			} else {
				return month, day, fmt.Errorf("month must be > 0 and < 13 %s", id)
			}

		} else if strings.HasPrefix(p, "d") {
			day, err = strconv.Atoi(p[1:3])
			if err != nil {
				return month, day, fmt.Errorf("not a day number in %s", id)
			}
			if day > 0 && day < 32 {
				// ok
			} else {
				return month, day, fmt.Errorf("day must be > 0 and < 32 %s", id)
			}
		}
	}
	if month == 0 {
		return month, day, fmt.Errorf("could not find month in %s", id)
	} else if day == 0 {
		return month, day, fmt.Errorf("could not find day in %s", id)
	}
	return month, day, nil
}
func ToDirSegmentsAndBase(filePath string) ([]string, string) {
	delimiter := "/"
	dir, base := path.Split(filePath)
	dirs := strings.Split(dir, delimiter)
	return dirs, base
}
func IndexInSlice(s []string, target string) int {
	for i, a := range s {
		if a == target {
			return i
		}
	}
	return -1
}

// PadDate pads the month and day with a leading zero if needed.
func PadDate(dateToFormat string, delimiter string) (string, error) {
	var err error
	parts := strings.Split(dateToFormat, delimiter)
	if len(parts) == 3 {
		var year, month, day int
		year, err = strconv.Atoi(parts[0])
		if err != nil {
			return "", fmt.Errorf("%s year not a number", dateToFormat)
		}
		if !(year >= 1 && year <= 9999) {
			return "", fmt.Errorf("%s year must be >= 1 and <= 9999", dateToFormat)
		}
		month, err = strconv.Atoi(parts[1])
		if err != nil {
			return "", fmt.Errorf("%s month not a number", dateToFormat)
		}
		if !(month >= 1 && month <= 12) {
			return "", fmt.Errorf("%s month must be >= 1 and <= 12", dateToFormat)
		}
		day, err = strconv.Atoi(parts[2])
		if err != nil {
			return "", fmt.Errorf("%s day not a number", dateToFormat)
		}
		if !(day >= 1 && day <= 31) {
			return "", fmt.Errorf("%s day must be >= 1 and <= 31", dateToFormat)
		}
		return fmt.Sprintf("%d-%02d-%02d", year, month, day), nil
	} else {
		return "", fmt.Errorf("could not parse date into year, month, day using - as delimiter")
	}
}

// AlwbToDoxaId converts an ALWB id (from data-key in a span.kvp) to the Doxa ID format,
// e.g. ho.s21_en_US_goa|hoPrayer.Key151.text => ltx/en_us_goa/ho.s21/hoPrayer.Key151.text
func AlwbToDoxaId(alwb string) (doxa string, err error) {
	if len(alwb) == 0 {
		return "", nil
	}
	if alwb == "|" {
		return "", nil
	}
	parts := strings.Split(alwb, "|")
	if len(parts) != 2 {
		return "", fmt.Errorf("%s: not in ALWB format", alwb)
	}
	key := parts[1]
	parts = strings.Split(parts[0], "_")
	if len(parts) != 4 {
		return "", fmt.Errorf("%s: not in ALWB format", alwb)
	}
	topic := parts[0]
	library := strings.ToLower(strings.Join(parts[1:], "_"))
	doxa = fmt.Sprintf("ltx/%s/%s/%s", library, topic, key)
	return doxa, err
}
