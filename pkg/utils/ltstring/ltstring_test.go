package ltstring

import (
	"fmt"
	"strings"
	"testing"
)

// Test text to Nnp
func TestToNnp(t *testing.T) {
	s := "Εὐλογημένη ἡ Βασιλεία τοῦ Πατρὸς, καὶ τοῦ Υἱοῦ καὶ τοῦ Ἁγίου Πνεύματος."
	e := "ευλογημενη η βασιλεια του πατρος και του υιου και του αγιου πνευματος"
	r := ToNnp(s)
	if strings.Compare(r, e) != 0 {
		t.Error("expected ltstring to match", "\nOriginal: "+s+"\nExpected: "+e+"\nGot:      "+r)
	}
}

// Test text to Nnp
func TestToNwp(t *testing.T) {
	s := "Εὐλογημένη ἡ Βασιλεία τοῦ Πατρὸς, καὶ τοῦ Υἱοῦ καὶ τοῦ Ἁγίου Πνεύματος."
	e := "ευλογημενη η βασιλεια του πατρος, και του υιου και του αγιου πνευματος."
	r := ToNwp(s)
	if strings.Compare(r, e) != 0 {
		t.Error("expected ltstring to match", "\nOriginal: "+s+"\nExpected: "+e+"\nGot:      "+r)
	}
}
func TestRemovePunctuation(t *testing.T) {
	s := "_This__ is a test of an im_port__ant thing!"
	r := RemovePunctuation(s)
	if strings.Contains(r, "_") {
		t.Errorf("did not remove underscores")
	}
}
func TestToUnderscored(t *testing.T) {
	s := "_This__ is a test of an im_port__ant thing!"
	r := ToUnderscored(s)
	if strings.Contains(r, "_") {
		t.Errorf("did not remove underscores")
	}
}
func TestWrappedLines(t *testing.T) {
	v := "This is going to be a very long line if I can think of enough things to say after the race that will occur this coming fortnight at the race tracks in Chester where my granddad spent several nights when he joined up to fight in WWI."
	fmt.Printf(WrappedLines(v, 10, 40, 0))
}
