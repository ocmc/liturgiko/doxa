// Package ltfile provides file utilities for processing liturgical texts
package ltfile

import (
	"archive/zip"
	"bufio"
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"crypto/rand"
	"crypto/sha256"
	"embed"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/liturgiko/doxa/pkg/enums/goos"
	"golang.org/x/net/context"
	"golang.org/x/sync/errgroup"
	"golang.org/x/text/unicode/norm"
	"html/template"
	"io"
	"io/fs"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/user"
	"path"
	"path/filepath"
	"regexp"
	"runtime"
	"sort"
	"strings"
	"sync"
	"time"
)

type Form int

const (
	NFC Form = iota
	NFD
)

// FileExists returns true if the final segment of the path exists and is not a directory.
func FileExists(path string) bool {
	path = ToSysPath(ToUnixPath(path))
	info, err := os.Stat(path)
	if err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	if info == nil {
		return false
	}
	return !info.IsDir()
}

// DirExists returns true if the final segment of the path exists and is a directory
func DirExists(path string) bool {
	path = ToSysPath(ToUnixPath(path))
	info, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}
	return info.IsDir()
}
func DeleteDirRecursively(path string) error {
	path = ToUnixPath(path)
	safepath := ToSysPath(path)
	err := os.RemoveAll(safepath)
	if err != nil {
		//large files could return ErrDirNotEmpty, so try again
		if !DirExists(path) {
			//it's gone! must've been the wind
			return nil
		}
		err = os.RemoveAll(safepath)
		if err != nil {
			return fmt.Errorf("error deleting directory %s: %v", path, err)
		}
	}
	return nil
}

// CreateDir will create a directory if it does not exist.
// The error will be nil unless something is wrong with the path.
func CreateDir(path string) error {
	path = ToUnixPath(path)
	safepath := ToSysPath(path)
	var err error = nil
	if !DirExists(path) {
		err = os.MkdirAll(safepath, os.ModePerm)
	}
	return err
}

// CreateDirs creates the specified directory and parents in path if they do not exist.
// The error will be nil unless something is wrong with the path.
func CreateDirs(path string) error {
	path = ToUnixPath(path)
	var err error = nil
	if !DirExists(path) {
		err = os.MkdirAll(ToSysPath(path), os.ModePerm)
	}
	return err
}

func Create(theFilePath string) (*os.File, error) {
	return os.Create(ToSysPath(ToUnixPath(theFilePath)))
}

func CreateFileIfNotExists(theFilePath string) error {
	theFilePath = ToUnixPath(theFilePath)
	if !FileExists(theFilePath) {
		_, err := os.Create(ToSysPath(theFilePath))
		if err != nil {
			return fmt.Errorf("%s is missing and had error creating empty file: %v\n", theFilePath, err)
		}
	}
	return nil
}
func ModifiedToday(filename string) (bool, error) {
	lastModified, err := LastModified(filename)
	if err != nil {
		return false, err
	}
	now := time.Now()
	if now.Year() == lastModified.Year() && now.Month() == lastModified.Month() && now.Day() == lastModified.Day() {
		return true, nil
	}
	return false, nil
}
func LastModified(filename string) (time.Time, error) {
	filename = ToUnixPath(filename)
	f, err := os.Stat(ToSysPath(filename))
	if err != nil {
		return time.Time{}, fmt.Errorf("%s does not exist:%v", filename, err)
	}
	return f.ModTime(), nil
}

// WriteFile writes the supplied content using the filename.
func WriteFile(filename, content string) error {
	filename = ToUnixPath(filename)
	safepath := ToSysPath(filename)
	if err := os.MkdirAll(filepath.Dir(ToSysPath(safepath)), os.ModePerm); err != nil {
		return err
	}
	f, err := os.Create(safepath)
	if err != nil {
		return err
	}
	defer f.Close()
	w := bufio.NewWriter(f)
	_, err = w.WriteString(content)
	w.Flush()
	return err
}

// WriteLinesToFile writes the supplied lines to the specified filename.
func WriteLinesToFile(filename string, lines []string) error {
	filename = ToUnixPath(filename)
	err := CreateDirs(path.Dir(filename))
	if err != nil {
		return err
	}
	f, err := os.Create(ToSysPath(filename))
	defer f.Close()
	if err != nil {
		return err
	}
	for _, line := range lines {
		fmt.Fprintln(f, line)
	}
	return err
}

// WriteLinesToFileNormalized writes the supplied lines to the specified filename.
func WriteLinesToFileNormalized(filename string, lines []string, form Form) error {
	filename = ToUnixPath(filename)
	err := CreateDirs(path.Dir(filename))
	if err != nil {
		return err
	}
	f, err := os.Create(ToSysPath(filename))
	if err != nil {
		return err
	}
	defer f.Close()

	// wrap the file writer with a normalizer
	var fw io.WriteCloser

	switch form {
	case NFC:
		fw = norm.NFC.Writer(f)
	case NFD:
		fw = norm.NFD.Writer(f)
	}
	defer fw.Close()

	if err != nil {
		return err
	}
	for _, line := range lines {
		_, err = fmt.Fprintln(fw, line)
		if err != nil {
			return err
		}
	}
	return err
}

// HTMLTemplateToFile parses the html template and writes it to the filename provided.
// name: the name of the template
// html: the template
// filename: the name of the html file including its path
// data: the data to be filled into the html (can be nil)
func HTMLTemplateToFile(name, html, filename string, data interface{}) error {
	filename = ToUnixPath(filename)
	safefile := ToSysPath(filename)
	t := template.Must(template.New(name).Parse(html))
	f, err := os.Create(safefile)
	if err != nil {
		log.Println(fmt.Sprintf("create file %s: %s", safefile, err))
		return err
	}
	err = t.Execute(f, data)
	if err != nil {
		log.Println(fmt.Sprintf("create template: %s", err))
		return err
	}
	f.Close()
	return nil
}

// TextTemplateToFile parses the html template and writes it to the filename provided.
// name: the name of the template
// html: the template
// filename: the name of the html file including its path
// data: the data to be filled into the html (can be nil)
func TextTemplateToFile(name, tmpl, filename string, data interface{}) error {
	filename = ToUnixPath(filename)
	safepath := ToSysPath(filename)
	t := template.Must(template.New(name).Parse(tmpl))
	f, err := os.Create(safepath)
	if err != nil {
		log.Println(fmt.Sprintf("create file %s: %s", safepath, err))
		return err
	}
	err = t.Execute(f, data)
	if err != nil {
		log.Println(fmt.Sprintf("create template: %s", err))
		return err
	}
	f.Close()
	return nil
}

// FileMatcher returns all files recursively in the dir that have the specified file extension and match the supplied regular expressions.
// The file extension should not have a leading dot.
// The expressions are regular expressions that will match the name of a file.
// The extension will be automatically added to each expression if it is not already there.
// The expressions may be nil, in which case all file patterns will match.
func FileMatcher(dir, extension string, expressions []string) ([]string, error) {
	var theExpressions []string
	// precompile the expressions
	if expressions == nil {
		theExpressions = []string{".*"}
	} else {
		theExpressions = make([]string, len(expressions))
	}

	copy(theExpressions, expressions)
	if !DirExists(dir) {
		msg := fmt.Sprintf("directory %s does not exist", dir)
		return nil, fmt.Errorf(msg)
	}
	var result []string
	var extensionPattern string
	if strings.HasPrefix(extension, ".") {
		extension = extension[1:]
	}
	extensionPattern = "\\." + extension
	patterns := make([]*regexp.Regexp, len(theExpressions))
	for i, e := range theExpressions {
		compilePattern := e
		if !strings.HasSuffix(e, extension) {
			compilePattern = compilePattern + extensionPattern
		}
		p, err := regexp.Compile(compilePattern)
		if err != nil {
			return result, err
		}
		patterns[i] = p
	}
	// now walk the files and apply the regular expressions
	err := filepath.Walk(ResolvePath(dir),
		func(path string, info os.FileInfo, err error) error {
			path = ToUnixPath(path)
			for _, p := range patterns {
				if info.IsDir() {
					continue
				}
				if p.MatchString(info.Name()) {
					result = append(result, path)
				}
			}
			return nil
		})
	return result, err
}

// ContainsFiles returns true if files are found, false if an error occurs or no matches.
func ContainsFiles(dir, extension string, expressions []string) (hasMatches bool) {
	files, err := FileMatcher(dir, extension, expressions)
	if err != nil {
		return false
	}
	if files != nil && len(files) > 0 {
		return true
	}
	return false
}

//type MatchedLine struct {
//	Path         string
//	RelativePath string
//	FileName     string
//	Line         int
//	From         int
//	To           int
//	ContextLeft  string
//	Match        string
//	ContextRight string
//}

// FileContentMatcher returns all files recursively in the dirsToSearch that have the specified file extension and match the supplied regular expressions.
// dirsToSearch is a slice of the full path to each directory to search.
// sources is a slice containing the project ID (for primary or root) and GitLab Group Name (for a fallback)
// The file extension should not have a leading dot.
// The expression is regular expressions that will match the content of a file.
// width is the total number of characters within which to fit the left context, match, and right context
// if width == 0, the entire line will be returned.
func FileContentMatcher(dirsToSearch, sources []string, extension string, expression string, width int) ([]*MatchedLine, error) {
	var result []*MatchedLine
	for dIndex, root := range dirsToSearch {

		if !DirExists(root) {
			msg := fmt.Sprintf("directory %s does not exist", root)
			return nil, fmt.Errorf(msg)
		}
		rootLen := len(root)
		paths, err := FileMatcher(root, extension, nil)
		if err != nil {
			return nil, err
		}
		r, err := regexp.Compile(expression)
		if err != nil {
			return nil, err
		}
		for _, path := range paths {
			c, err := GetFileContent(path)
			if err != nil {
				return nil, err
			}
			if !r.MatchString(c) {
				continue
			}
			lines, err := GetFileLines(path)
			if err != nil {
				return nil, err
			}
			fileName := filepath.Base(path)
			relPath := path[rootLen+1:]
			for i, line := range lines {
				indexes := r.FindAllStringIndex(line, -1)
				if indexes == nil {
					continue
				}
				matches := r.FindAllString(line, -1)
				if matches == nil {
					continue
				}
				l := len(line)
				for j, tuple := range indexes {
					m := new(MatchedLine)
					m.Source = sources[dIndex]
					m.FileName = fileName
					m.Path = path
					m.RelativePath = relPath
					m.Line = i
					m.From = tuple[0]
					m.To = tuple[1]
					m.Match = matches[j]
					if width == 0 {
						m.ContextLeft = line[:tuple[0]]
						m.ContextRight = line[tuple[1]:]
					} else {
						targetWidth := width - len(m.Match)/2
						if tuple[0] > 0 { // zero means the match is at the beginning of the line
							// get the prefix context
							prefixStart := tuple[0] - targetWidth
							prefixEnd := tuple[0]
							if prefixStart < 0 {
								prefixStart = 0
							}
							if prefixEnd <= prefixStart {
								m.ContextLeft = line[prefixStart:]
							} else {
								m.ContextLeft = line[prefixStart:prefixEnd]
							}
						}
						// get the suffix context
						if tuple[1] < l { // if tuple[1] = l, the match is at the end of the line
							suffixStart := tuple[1]
							suffixEnd := tuple[1] + targetWidth
							var suffix string
							if suffixEnd >= l {
								suffix = line[suffixStart:]
							} else {
								suffix = line[suffixStart:suffixEnd]
							}
							m.ContextRight = suffix
						}
					}
					result = append(result, m)
				}
			}
		}
	}
	return result, nil
}

// EntriesInDir returns file info for each entry in the specified directory
// Note that this function returns a slice of os.DirEntry which unlike the rest of the ltfile return values have not been normalized.
func EntriesInDir(dir string) ([]os.DirEntry, error) {
	//TODO: The paths being returned have not been normalized because use cases appear to require displaying it to the user in native path format
	files, err := os.ReadDir(ToSysPath(ToUnixPath(dir)))
	return files, err
}

// DirsInDir returns the name of the immediate child dirs in the specified directory.
// parameters:
//
//	dir - full path to the directory to search
//	ignoreHidden - if true, ignores any directory starting with a period
func DirsInDir(dir string, ignoreHidden bool) ([]string, error) {
	dir = ToUnixPath(dir)
	var dirs []string
	files, err := os.ReadDir(ToSysPath(dir))
	if err != nil {
		return nil, err
	}
	for _, f := range files {
		if f.IsDir() {
			if strings.HasPrefix(f.Name(), ".") && ignoreHidden {
				continue
			}
			dirs = append(dirs, f.Name())
		}
	}
	return dirs, err
}
func FilesInDir(dir string, ignoreHidden bool) ([]string, error) {
	var files []string
	dir = ToUnixPath(dir)
	ents, err := os.ReadDir(ToSysPath(dir))
	if err != nil {
		return nil, err
	}
	for _, f := range ents {
		if !f.IsDir() {
			//TODO:support windows?
			if strings.HasPrefix(f.Name(), ".") && ignoreHidden {
				continue
			}
			files = append(files, f.Name())
		}
	}
	return files, err
}
func IsEmptyDir(dir string, ignoreHidden bool) (bool, error) {
	d, err := DirsInDir(dir, ignoreHidden)
	if err != nil {
		return false, err
	}
	f, err := FilesInDir(dir, ignoreHidden)
	if err != nil {
		return false, err
	}
	return len(f) == 0 && len(d) == 0, nil
}
func ReposInDir(dir string) ([]os.DirEntry, error) {
	files, err := os.ReadDir(ToSysPath(ToUnixPath(dir)))
	return files, err
}

// TemplatesInDir returns all files ending with .lml extension in specified directory.
// The dir part of the path will be removed. The files will be sorted.
func TemplatesInDir(dir string) ([]string, error) {
	files, err := FileMatcher(dir, "lml", nil)
	if err != nil {
		return nil, err
	}
	var lmlFiles []string
	for _, file := range files {
		parts := strings.Split(file, dir+"/")
		if len(parts) == 2 {
			lmlFiles = append(lmlFiles, parts[1])
		}
	}
	sort.Strings(lmlFiles)
	return lmlFiles, nil
}

// CopyEntriesInDir copies the contents of the source dir into the destination dir.
// The destination dir does not have to already exist.
// If verbose == true, fmt.Println will report each file and dir being copied
func CopyEntriesInDir(source, destination string, verbose bool) error {
	var err error
	if !DirExists(source) {
		return fmt.Errorf("directory %s does not exist", source)
	}
	if DirExists(destination) {
		files, err := EntriesInDir(source)
		if err != nil {
			return err
		}
		if verbose {
			fmt.Printf("\nCopying files and directories from %s to %s", source, destination)
		}
		for _, file := range files {
			if !strings.HasPrefix(file.Name(), ".") {
				from := path.Join(source, file.Name())
				to := path.Join(destination, file.Name())
				if verbose {
					fmt.Printf("\tCopying %s to %s\n", from, to)
				}
				if file.IsDir() {
					err := CopyDir(from, to)
					if err != nil {
						return err
					}
				} else {
					err := CopyFile(from, to)
					if err != nil {
						return err
					}
				}
			}
		}
		if verbose {
			fmt.Printf("Finished copying files and directories from %s to %s", source, destination)
		}
	} else {
		err = CopyDir(source, destination)
	}
	return err
}

// Returns the directory within which the caller is executing
// This is just an example.  It must exist as a function in
// the desired package.
func executionDir() string {
	_, filename, _, _ := runtime.Caller(0)
	dir, _ := path.Split(ToUnixPath(filename))
	return dir
}

// ResolvePath checks the path for a leading tilde and returns the expanded path.
// The tilde is replaced with the user's home dir path.
// If no tilde is present, the path is returned unmodified.
func ResolvePath(path string) string {
	usr, _ := user.Current()
	dir := ToUnixPath(usr.HomeDir)
	var newPath = ""
	if path == "~" {
		newPath = dir
	} else if strings.HasPrefix(path, "~/") {
		// Use strings.HasPrefix so we don't match paths like
		// "/something/~/something/"
		newPath = filepath.Join(dir, path[2:])
	} else {
		newPath = path
	}
	return newPath
}
func CopyEmbedded(efs embed.FS, splitter string, to []string) error {
	if !strings.HasSuffix(splitter, "/") {
		splitter += "/"
	}
	return fs.WalkDir(efs, ".", func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		f, err := efs.Open(path)
		if err != nil {
			return err
		}
		fileInfo, err := f.Stat()
		if err != nil {
			return err
		}
		if !fileInfo.IsDir() {
			b, err := io.ReadAll(f)
			if err != nil {
				return err
			}
			parts := strings.Split(path, splitter)
			if len(parts) == 2 {
				for _, t := range to {
					pathOut := filepath.Join(t, parts[1])
					dirPathOut, _ := filepath.Split(pathOut)
					err := os.MkdirAll(dirPathOut, os.ModePerm)
					if err != nil {
						return err
					}
					err = WriteFile(pathOut, string(b))
					if err != nil {
						return err
					}
				}
			} else {
				return fmt.Errorf("split failed: spliiting path %s with splitter %s yielded an unexpected split count of %d, should be 2", path, splitter, len(parts))
			}
		}
		return nil
	})
}
func ReadAll(path string) ([]byte, error) {
	f, err := os.Open(ToSysPath(ToUnixPath(path)))
	if err != nil {
		return nil, err
	}
	defer f.Close()
	data := make([]byte, 10000000)
	data = data[:cap(data)]
	spaces := 0
	for {
		n, err := f.Read(data)
		if err != nil {
			if err == io.EOF {
				break
			}
		}
		data = data[:n]
		for _, b := range data {
			if b == ' ' {
				spaces++
			}
		}
	}
	return data, nil
}

// GetFileContent opens the file at the specified path and returns the contents as a string
func GetFileContent(path string) (string, error) {
	path = ToSysPath(ToUnixPath(path))
	b, err := os.ReadFile(path) // just pass the file name
	if err != nil {
		return "", fmt.Errorf("%s: %v", path, err)
	}
	return string(b), nil // convert content to a 'string'
}

// GetFileContentWithoutComments opens the file at the specified path, removes comments, and returns the contents as a string
func GetFileContentWithoutComments(path string) (string, error) {
	lines, err := GetFileLines(path)
	if err != nil {
		return "", err
	}
	sb := strings.Builder{}
	var inCommentBlock bool
	for _, line := range lines {
		line = strings.TrimSpace(line)
		if strings.HasPrefix(line, "*/") { // end of comment block
			inCommentBlock = false
			continue
		} else if strings.HasPrefix(line, "/*") { // we are entering a comment block
			inCommentBlock = true
			continue
		}
		if strings.HasPrefix(line, "//") { // commented out line
			continue
		}
		if inCommentBlock {
			continue
		}
		sb.WriteString(fmt.Sprintf("%s\n", line))
	}
	return sb.String(), nil
}

// GetFileLines opens the file at the specified path and returns an array of its lines
func GetFileLines(path string) ([]string, error) {
	readFile, err := os.Open(ToSysPath(ToUnixPath(path)))
	if err != nil {
		return nil, err
	}
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)
	var fileTextLines []string

	for fileScanner.Scan() {
		fileTextLines = append(fileTextLines, fileScanner.Text())
	}
	return fileTextLines, readFile.Close()
}

// GetFileLinesNormalized opens the file at the specified path and returns an array of its lines
func GetFileLinesNormalized(path string, f Form) ([]string, error) {
	readFile, err := os.Open(ToSysPath(ToUnixPath(path)))
	var rf io.Reader
	switch f {
	case NFC:
		rf = norm.NFC.Reader(readFile)
	case NFD:
		rf = norm.NFD.Reader(readFile)
	}
	if err != nil {
		return nil, err
	}
	fileScanner := bufio.NewScanner(rf)
	fileScanner.Split(bufio.ScanLines)
	var fileTextLines []string

	for fileScanner.Scan() {
		fileTextLines = append(fileTextLines, fileScanner.Text())
	}
	return fileTextLines, readFile.Close()
}

// CopyFile and CopyDir are from https://gist.githubusercontent.com/r0l1/92462b38df26839a3ca324697c8cba04/raw/dd62b7c2d425aef9db94ee0f4b772c621d09a845/copy.go
/* MIT License
 *
 * Copyright (c) 2017 Roland Singer [roland.singer@desertbit.com]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
// CopyFile copies the contents of the file named src to the file named
// by dst. The file will be created if it does not already exist. If the
// destination file exists, all it's contents will be replaced by the contents
// of the source file. The file mode will be copied from the source and
// the copied data is synced/flushed to stable storage.
func CopyFile(src, dst string) (err error) {
	src = ToUnixPath(src)
	dst = ToUnixPath(dst)
	sysSrc := ToSysPath(src)
	sysDst := ToSysPath(dst)
	if src == dst {
		return fmt.Errorf("can't copy %s to itself (%s)", src, dst)
	}
	fileInfo, err := os.Stat(sysSrc)
	if err != nil {
		fmt.Println(err)
		return
	}
	srcSize := fileInfo.Size()
	if srcSize == 0 {
		return fmt.Errorf("%s is an empty file", src)
	}
	in, err := os.Open(sysSrc)
	if err != nil {
		msg := fmt.Sprintf("error opening %v: %v", in, err)
		return fmt.Errorf(msg)
	}
	defer func(in *os.File) {
		err = in.Close()
		if err != nil {
			fmt.Printf("error closing %s: %v", sysSrc, err)
		}
	}(in)

	err = CreateDirs(filepath.Dir(dst))
	if err != nil {
		return fmt.Errorf("error creating directories for %s: %v", sysDst, err)
	}
	out, err := os.Create(sysDst)
	if err != nil {
		return fmt.Errorf("error creating %s: %v", sysDst, err)
	}
	defer func(out *os.File) {
		err = out.Close()
		if err != nil {
			fmt.Printf("error closing %s: %v", sysDst, err)
		}
	}(out)
	var outSize int64
	outSize, err = io.Copy(out, in)
	if err != nil {
		return fmt.Errorf("error copying %s to %s: %v", sysSrc, sysDst, err)
	}
	if outSize != srcSize {
		return fmt.Errorf("copied file is not same size as source file %s", sysSrc)
	}
	err = out.Sync()
	if err != nil {
		return fmt.Errorf("%s sync error: %v", sysDst, err)
	}

	si, err := os.Stat(sysSrc)
	if err != nil {
		return fmt.Errorf("%s stat error: %v", sysDst, err)
	}
	err = os.Chmod(sysDst, si.Mode())
	if err != nil {
		return fmt.Errorf("%s chmod error: %v", sysDst, err)
	}
	return nil
}

// CopyDir recursively copies a directory tree, attempting to preserve permissions.
// Source directory must exist, destination directory must *not* exist.
// Symlinks are ignored and skipped.
func CopyDir(src string, dst string) (err error) {
	if src == dst {
		return fmt.Errorf("can't copy %s to itself (%s)", src, dst)
	}

	src = ToSysPath(ToUnixPath(src))
	dst = ToSysPath(ToUnixPath(dst))
	src = filepath.Clean(src)
	dst = filepath.Clean(dst)

	si, err := os.Stat(src)
	if err != nil {
		return err
	}
	if !si.IsDir() {
		return fmt.Errorf("source is not a directory")
	}
	err = os.MkdirAll(dst, si.Mode())
	if err != nil {
		return
	}

	entries, err := os.ReadDir(src)
	if err != nil {
		return
	}

	for _, entry := range entries {
		srcPath := filepath.Join(src, entry.Name())
		dstPath := filepath.Join(dst, entry.Name())

		if entry.IsDir() {
			err = CopyDir(srcPath, dstPath)
			if err != nil {
				return
			}
		} else {
			// Skip symlinks.
			if entry.Type()&os.ModeSymlink != 0 {
				continue
			}

			err = CopyFile(srcPath, dstPath)
			if err != nil {
				return
			}
		}
	}

	return
}

func ConcurrentCopyDir(src string, dst string, cap int) error {
	src = ToSysPath(ToUnixPath(src))
	dst = ToSysPath(ToUnixPath(dst))
	if cap == 0 {
		cap = 64 //TODO: adjust this to balance performance and memory
	}
	var wg sync.WaitGroup
	semaphore := make(chan struct{}, cap)
	var err error

	// Function to copy a single file
	copyFile := func(src, dst string) {
		defer wg.Done()
		defer func() { <-semaphore }()
		srcF, err := os.Open(src)
		if err != nil {
			return
		}
		defer srcF.Close()
		os.MkdirAll(filepath.Dir(dst), os.ModePerm)
		destF, err := os.Create(dst)
		if err != nil {
			return
		}
		defer destF.Close()
		io.Copy(destF, srcF)
	}

	// Walk through the source directory
	filepath.WalkDir(src, func(path string, ent os.DirEntry, err error) error {
		if err != nil {
			return err
		}

		if !ent.IsDir() {
			relPath, _ := filepath.Rel(src, path)
			dstPath := filepath.Join(dst, relPath)

			wg.Add(1)
			semaphore <- struct{}{} // This will block if we've reached maxGoroutines

			go copyFile(path, dstPath)
		}

		return nil
	})

	return err
}

// Unzip will decompress a zip archive, moving all files and folders
// within the zip file (parameter 1) to an output directory (parameter 2).
func Unzip(src string, dest string) ([]string, error) {
	src = ToSysPath(ToUnixPath(src))
	dest = ToSysPath(ToUnixPath(dest))
	var filenames []string

	r, err := zip.OpenReader(src)
	if err != nil {
		return filenames, err
	}
	defer r.Close()

	for _, f := range r.File {

		// Store filename/path for returning and using later on
		fpath := filepath.Join(dest, f.Name)

		// Check for ZipSlip. More Info: http://bit.ly/2MsjAWE
		if !strings.HasPrefix(fpath, filepath.Clean(dest)+string(os.PathSeparator)) {
			return filenames, fmt.Errorf("%s: illegal file path", fpath)
		}

		filenames = append(filenames, fpath)

		if f.FileInfo().IsDir() {
			// Make Folder
			os.MkdirAll(fpath, os.ModePerm)
			continue
		}

		// Make File
		if err = os.MkdirAll(filepath.Dir(fpath), os.ModePerm); err != nil {
			return filenames, err
		}

		outFile, err := os.OpenFile(fpath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
		if err != nil {
			return filenames, err
		}

		rc, err := f.Open()
		if err != nil {
			return filenames, err
		}

		_, err = io.Copy(outFile, rc)

		// Close the file without defer to close before next iteration of loop
		outFile.Close()
		rc.Close()

		if err != nil {
			return filenames, err
		}
	}
	return filenames, nil
}

// Concatenate finds all files with matching extension in the specified directory recursively, and combines them into a single combined file.
func Concatenate(rootDir, combinedFilePath string, extension string) (errors []error) {
	files, err := FileMatcher(rootDir, extension, nil)
	if err != nil {
		msg := fmt.Sprintf("cannot find tsv files: %v", err)
		errors = append(errors, fmt.Errorf(msg))
		return errors
	}
	var concatenatedLines []string
	for _, f := range files {
		if f == combinedFilePath {
			continue
		}
		var lines []string
		lines, err = GetFileLines(f)
		if err != nil {
			msg := fmt.Sprintf("error getting lines from %s: %v", f, err)
			errors = append(errors, fmt.Errorf(msg))
			continue
		}
		for _, line := range lines {
			concatenatedLines = append(concatenatedLines, line)
		}
	}
	err = WriteLinesToFile(combinedFilePath, concatenatedLines)
	if err != nil {
		errors = append(errors, err)
	}
	return errors
}
func RenameDir(oldPath, newPath string) error {
	oldPath = ToSysPath(ToUnixPath(oldPath))
	newPath = ToSysPath(ToUnixPath(newPath))
	var err error
	err = os.Rename(oldPath, newPath)
	if err != nil {
		return fmt.Errorf("error renaming %s to %s: %v", oldPath, newPath, err)
	}
	return nil
}
func RenameFile(oldPath, newPath string) error {
	uxNewPath := ToUnixPath(newPath)
	oldPath = ToSysPath(ToUnixPath(oldPath))
	newPath = ToSysPath(uxNewPath)
	var err error
	err = os.Chmod(oldPath, 0777)
	if err != nil {
		return fmt.Errorf("error renaming %s to %s: %v", oldPath, newPath, err)
	}
	// make directories in case they don't exist
	err = CreateDirs(path.Dir(uxNewPath))
	if err != nil {
		return err
	}
	err = os.Rename(oldPath, newPath)
	if err != nil {
		return fmt.Errorf("error renaming %s to %s: %v", oldPath, newPath, err)
	}
	return nil
}

type SearchReplacePattern struct {
	From []byte
	To   []byte
}
type SearchReplacePatterns []*SearchReplacePattern

func (s *SearchReplacePatterns) Add(from, to string) {
	p := new(SearchReplacePattern)
	p.From = []byte(from)
	p.To = []byte(to)
	*s = append(*s, p)
}

// Replace replaces matching patterns in files with the specified extension in the specified dir path.
// The length of the 'from' string slice and the 'to' string slice must be equal.
// If the file content matches from[i], it will be replaced with to[i].
// The patterns slices are converted to []byte and do not support regular expressions.
// The pattern slices are applied in sequence, so beware of changing something in a manner that causes it to match a downstream pattern.
//
// code from https://github.com/bketelsen/gogrep, with extension parm and replace code added
//
// Usage:
//
//	path := "path/to/some/dir"
//	extension := ".lml" // or whatever
//	create the slice of search-replace patterns
//	var s SearchReplacePatterns
//	s.Add("id = \"Lectionary", "id = \"%Readings")
//	s.Add("insert \"%Lectionary", "insert \"Readings")
//
//	duration := flag.Duration("timeout", 120*time.Second, "timeout in seconds")
//	ctx, cf := context.WithTimeout(context.Background(), *duration)
//
//	defer cf()
//	m, err := Replace(ctx, path, extension, s)
//	if err != nil {
//	  log.Fatal(err)
//	}
//	for _, name := range m {
//	  fmt.Println(name)
//	}
//	fmt.Println(len(m), "hits")
func Replace(ctx context.Context, root, extension string, s SearchReplacePatterns) ([]string, error) {
	root = ToSysPath(ToUnixPath(root))
	nbrPatterns := len(s)
	g, ctx := errgroup.WithContext(ctx)
	paths := make(chan string, 100)
	// get all the paths

	g.Go(func() error {
		defer close(paths)

		return filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if !info.Mode().IsRegular() {
				return nil
			}
			if !info.IsDir() && !strings.HasSuffix(info.Name(), extension) {
				return nil
			}

			select {
			case paths <- path:
			case <-ctx.Done():
				return ctx.Err()
			}
			return nil
		})

	})

	c := make(chan string, 100)
	for path := range paths {
		p := path
		g.Go(func() error {
			data, err := ioutil.ReadFile(p)
			if err != nil {
				return err
			}
			matched := false
			for i := 0; i < nbrPatterns; i++ {
				if bytes.Contains(data, s[i].From) {
					matched = true
					data = bytes.ReplaceAll(data, s[i].From, s[i].To)
				}
			}
			if !matched {
				return nil
			}
			err = ioutil.WriteFile(p, data, 0666)
			if err != nil {
				return err
			}
			select {
			case c <- p:
			case <-ctx.Done():
				return ctx.Err()
			}
			return nil
		})
	}
	go func() {
		g.Wait()
		close(c)
	}()
	var m []string
	for r := range c {
		m = append(m, r)
	}
	return m, g.Wait()
}

type MatchedLine struct {
	Source       string
	Path         string
	RelativePath string
	FileName     string
	Line         int
	From         int
	To           int
	ContextLeft  string
	Match        string
	ContextRight string
}

// Match returns matched content from each line in all files in the specified root,
// which is searched recursively.  The files must also match the specified extension.
// The width parameter specifies how many bytes before and after the match to return
// to provide a context.
func Match(root, extension, pattern string, width int) ([]*MatchedLine, error) {
	ctx := context.Background()
	m, err := match(ctx, root, extension, strings.TrimSpace(pattern), width)
	if err != nil {
		return nil, err
	}
	return m, nil
}

// Match returns the paths of all files in the specified root dir with the specified extension, and content matching the pattern.
// The pattern can be a regular expression.
// code based on https://pkg.go.dev/golang.org/x/sync/errgroup#pkg-variables
func match(ctx context.Context, root, extension, pattern string, width int) ([]*MatchedLine, error) {
	root = ToSysPath(ToUnixPath(root))
	r, err := regexp.Compile(pattern)
	if err != nil {
		return nil, err
	}
	rootLen := len(root) + 1 // add 1 to drop leading slash
	g, ctx := errgroup.WithContext(ctx)
	//	paths, err := FileMatcher(root, "lml", nil)
	paths := make(chan string, 10000)
	// get all the paths
	g.Go(func() error {
		defer close(paths)

		return filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if !info.Mode().IsRegular() {
				return nil
			}
			if !info.IsDir() && !strings.HasSuffix(info.Name(), extension) {
				return nil
			}

			select {
			case paths <- path:
			case <-ctx.Done():
				return ctx.Err()
			}
			return nil
		})

	})

	c := make(chan *MatchedLine, 100000)
	for path := range paths {
		fmt.Println(path)
		g.Go(func() error {
			d, err := GetFileContent(path)
			if err != nil {
				return err
			}
			if !r.MatchString(d) {
				//				fmt.Printf("\tno matches\n")
				return nil
			}
			//			fmt.Printf("\thas matches\n")
			fileName := filepath.Base(path)
			relPath := path[rootLen:]
			data := string(d)
			lines := strings.Split(data, "\n")
			for i, line := range lines {
				indexes := r.FindAllStringIndex(line, -1)
				if indexes == nil {
					continue
				}
				matches := r.FindAllString(line, -1)
				if matches == nil {
					continue
				}
				l := len(line)
				for j, tuple := range indexes {
					m := new(MatchedLine)
					m.FileName = fileName
					m.Path = path
					m.RelativePath = relPath
					m.Line = i
					m.From = tuple[0]
					m.To = tuple[1]
					m.Match = matches[j]
					// get the prefix context
					prefixStart := tuple[0] - width
					prefixEnd := tuple[0] - 1
					if prefixStart < 0 {
						prefixStart = 0
					}
					if prefixEnd <= prefixStart {
						m.ContextLeft = line[prefixStart:]
					} else {
						m.ContextLeft = line[prefixStart:prefixEnd]
					}
					// get the suffix context
					suffixStart := tuple[1] + 1
					suffixEnd := tuple[1] + width
					var suffix string
					if suffixEnd >= l {
						suffix = line[suffixStart:]
					} else {
						suffix = line[suffixStart:suffixEnd]
					}
					m.ContextRight = suffix
					select {
					case c <- m:
					case <-ctx.Done():
						return ctx.Err()
					}
				}
			}
			return nil
		})
	}
	go func() {
		g.Wait()
		close(c)
	}()

	var m []*MatchedLine
	for r := range c {
		m = append(m, r)
	}
	return m, g.Wait()
}

func CopyDirFast(ctx context.Context, from, to, extension string) ([]string, error) {
	g, ctx := errgroup.WithContext(ctx)
	paths := make(chan string, 100)
	// get all the paths
	g.Go(func() error {
		defer close(paths)

		return filepath.Walk(from, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if !info.Mode().IsRegular() {
				return nil
			}
			if !info.IsDir() && !strings.HasSuffix(info.Name(), extension) {
				return nil
			}

			select {
			case paths <- path:
			case <-ctx.Done():
				return ctx.Err()
			}
			return nil
		})

	})

	c := make(chan string, 100)
	for path := range paths {
		p := path
		g.Go(func() error {
			t := strings.Replace(p, from, to, 1)
			err := CopyFile(p, t)
			if err != nil {
				return err
			}
			select {
			case c <- p:
			case <-ctx.Done():
				return ctx.Err()
			}
			return nil
		})
	}
	go func() {
		g.Wait()
		close(c)
	}()

	var m []string
	for r := range c {
		m = append(m, r)
	}
	return m, g.Wait()
}

type md5Result struct {
	path string
	sum  [md5.Size]byte
}

// MD5All reads all the files in the file tree rooted at root and returns a map
// from file path to the MD5 sum of the file's contents. If the directory walk
// fails or any read operation fails, MD5All returns an error.
// Code copied from https://pkg.go.dev/golang.org/x/sync/errgroup#pkg-variables
func MD5All(ctx context.Context, root string) (map[string][md5.Size]byte, error) {
	// ctx is canceled when g.Wait() returns. When this version of MD5All returns
	// - even in case of error! - we know that all the goroutines have finished
	// and the memory they were using can be garbage-collected.
	g, ctx := errgroup.WithContext(ctx)
	paths := make(chan string)

	g.Go(func() error {
		defer close(paths)
		return filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if !info.Mode().IsRegular() {
				return nil
			}
			select {
			case paths <- path:
			case <-ctx.Done():
				return ctx.Err()
			}
			return nil
		})
	})

	// Start a fixed number of goroutines to read and digest files.
	c := make(chan md5Result)
	const numDigesters = 20
	for i := 0; i < numDigesters; i++ {
		g.Go(func() error {
			for path := range paths {
				data, err := ioutil.ReadFile(path)
				if err != nil {
					return err
				}
				select {
				case c <- md5Result{path, md5.Sum(data)}:
				case <-ctx.Done():
					return ctx.Err()
				}
			}
			return nil
		})
	}
	go func() {
		g.Wait()
		close(c)
	}()

	m := make(map[string][md5.Size]byte)
	for r := range c {
		m[r.path] = r.sum
	}
	// Check whether any of the goroutines failed. Since g is accumulating the
	// errors, we don't need to send them (or check for them) in the individual
	// results sent on the channel.
	if err := g.Wait(); err != nil {
		return nil, err
	}
	return m, nil
}
func Sha256(path string) (string, error) {
	if !FileExists(path) {
		return "", fmt.Errorf("%s does not exist", path)
	}
	f, err := os.Open(ToSysPath(ToUnixPath(path)))
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	h := sha256.New()
	if _, err = io.Copy(h, f); err != nil {
		return "", err
	}
	return fmt.Sprintf("%x", h.Sum(nil)), nil
}
func Checksum(path string) (string, error) {
	if !FileExists(path) {
		return "", fmt.Errorf("%s does not exist", path)
	}
	ck, err := Sha256(path)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s - %s", ck, filepath.Base(path)), nil
}

func ContentsFromUrl(theUrl string) (string, error) {
	if strings.HasPrefix(theUrl, "file://") {
		theFilePath, cut := strings.CutPrefix(theUrl, "file://")
		if !cut {
			return "", fmt.Errorf("failed to cut the file prefix from URL: %s", theUrl)
		}
		return GetFileContent(theFilePath)
	}

	response, err := http.Get(theUrl)
	if err != nil {
		return "", err
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		return "", fmt.Errorf("HTTP request to %s failed with status: %s", theUrl, response.Status)
	}

	n, err := io.ReadAll(response.Body)
	if err != nil {
		return "", err
	}

	return string(n), nil
}

// WriteContentsFromUrlToFile fetches content from a URL and writes it to a specified file.
func WriteContentsFromUrlToFile(theUrl, filePath string) error {
	if strings.HasPrefix(theUrl, "file://") {
		theFilePath, cut := strings.CutPrefix(theUrl, "file://")
		if !cut {
			return fmt.Errorf("failed to cut the file prefix from URL: %s", theUrl)
		}
		return copyFileContents(theFilePath, filePath)
	}
	thePath := ToUnixPath(filePath)
	thePath = ToSysPath(thePath)
	response, err := http.Get(theUrl)
	if err != nil {
		return err
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		return fmt.Errorf("HTTP request to %s failed with status: %s", theUrl, response.Status)
	}
	dirPath, _ := filepath.Split(thePath)
	err = CreateDirs(dirPath)
	if err != nil {
		return fmt.Errorf("failed to create directory for file: %s", filePath)
	}
	outFile, err := os.Create(thePath)
	if err != nil {
		return fmt.Errorf("failed to create file %s: %v", filePath, err)
	}
	defer outFile.Close()

	_, err = io.Copy(outFile, response.Body)
	if err != nil {
		return fmt.Errorf("failed to write content to file %s: %v", filePath, err)
	}

	return nil
}

// copyFileContents copies the contents of srcFile to destFile.
func copyFileContents(srcFile, destFile string) error {
	srcFile = ToSysPath(ToUnixPath(srcFile))
	destFile = ToSysPath(ToUnixPath(destFile))
	input, err := os.Open(srcFile)
	if err != nil {
		return fmt.Errorf("failed to open source file %s: %v", srcFile, err)
	}
	defer input.Close()

	output, err := os.Create(destFile)
	if err != nil {
		return fmt.Errorf("failed to create destination file %s: %v", destFile, err)
	}
	defer output.Close()

	_, err = io.Copy(output, input)
	if err != nil {
		return fmt.Errorf("failed to copy contents from %s to %s: %v", srcFile, destFile, err)
	}

	return nil
}
func DocFromUrl(url string) (*goquery.Document, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("error http Get %s: %v", url, err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("status code %d reading %s", resp.StatusCode, url)
	}
	content, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading %s: %v", url, err)
	}
	return goquery.NewDocumentFromReader(strings.NewReader(string(content)))
}

// OpenFileWriter returns a file writer and a function to close it.
// Usage:
//
//	fw, fwCloseFn, err := library.OpenFileFromArgs()
//	defer fwCloseFn()
func OpenFileWriter(inputFilePath string) (fw *os.File, fwCloseFn func(), err error) {
	fw, err = os.OpenFile(ToSysPath(ToUnixPath(inputFilePath)), os.O_CREATE|os.O_APPEND|os.O_RDWR, 0666)
	fwCloseFn = func() {
		err := fw.Close()
		if err != nil {
			panic("failed to close input file")
		}
	}

	return fw, fwCloseFn, err
}

func ifDir(path string) bool {
	file, err := os.Open(ToSysPath(ToUnixPath(path)))
	if err != nil {
		panic(err)
	}
	defer file.Close()
	info, err := file.Stat()
	if err != nil {
		panic(err)
	}
	if info.IsDir() {
		return true
	}
	return false
}

// DeleteAllDirsExcept deletes everything in the specified root directory
// except the top level ones listed in exclusions
func DeleteAllDirsExcept(rootDir string, exclusions []string) error {
	rootDir = ToSysPath(ToUnixPath(rootDir))
	contents, err := os.ReadDir(rootDir)
	if err != nil {
		return err
	}
	for _, c := range contents {
		if c.IsDir() {
			var exclude bool
			for _, e := range exclusions {
				if c.Name() == e {
					exclude = true
					break
				}
			}
			if exclude {
				// do nothing
			} else {
				err = os.RemoveAll(filepath.Join(rootDir, c.Name()))
				if err != nil {
					return err
				}
			}
		}
	}
	return nil
}

func GetLastLines(filepath string, n int) (lines []string, err error) {
	filepath = ToSysPath(ToUnixPath(filepath))
	file, err := os.Open(filepath)
	if err != nil {
		panic(err)
	}
	defer func(file *os.File) {
		err = file.Close()
		if err != nil {
			//
		}
	}(file)

	// get file size
	fileInfo, err := file.Stat()
	if err != nil {
		panic(err)
	}
	fileSize := fileInfo.Size()

	// seek almost to the end of the file
	offset := int64(1024) // start 1KB from the end
	if fileSize > offset {
		_, err = file.Seek(-offset, io.SeekEnd)
		if err != nil {
			return nil, err
		}
	} else {
		_, err = file.Seek(0, io.SeekCurrent)
		if err != nil {
			return nil, err
		}
	}

	// read forward and count lines
	scanner := bufio.NewScanner(file)
	lineCount := 0

	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "{") {
			lines = append(lines, scanner.Text())
		}
		lineCount++
		if lineCount > n {
			break
			//			lines = lines[1:]
		}
	}

	if err = scanner.Err(); err != nil {
		return nil, err
	}
	return lines, nil
}

// ToSysPath converts a path to use the delimiters
// of the OS on which go is executing.
func ToSysPath(p string) string {
	return filepath.FromSlash(p)
}
func ToUnixPath(p string) string {
	return filepath.ToSlash(p)
}

// OsVolumesDir returns the root directory
// from which to list mounted volumes.
func OsVolumesDir() string {
	switch goos.CodeForString(runtime.GOOS) {
	case goos.Darwin:
		return "/volumes"
	case goos.Linux:
		return "/"
	case goos.Windows:
		return os.Getenv("USERPROFILE")
	default:
		return "/"
	}
}
func DecryptFile(filename string, key []byte) error {
	filename = ToSysPath(ToUnixPath(filename))

	ciphertext, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer ciphertext.Close()

	plaintext, err := os.Create(filename + ".dec")
	if err != nil {
		return err
	}
	defer plaintext.Close()

	block, err := aes.NewCipher(key)
	if err != nil {
		return err
	}

	iv := make([]byte, aes.BlockSize)
	if _, err := io.ReadFull(ciphertext, iv); err != nil {
		return err
	}

	stream := cipher.NewCFBDecrypter(block, iv)
	reader := &cipher.StreamReader{S: stream, R: ciphertext}
	if _, err := io.Copy(plaintext, reader); err != nil {
		return err
	}

	return nil
}
func EncryptFile(filename string, key []byte) error {
	filename = ToSysPath(ToUnixPath(filename))
	plaintext, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer plaintext.Close()

	ciphertext, err := os.Create(filename + ".enc")
	if err != nil {
		return err
	}
	defer ciphertext.Close()

	block, err := aes.NewCipher(key)
	if err != nil {
		return err
	}

	iv := make([]byte, aes.BlockSize)
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return err
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	ciphertext.Write(iv)
	writer := &cipher.StreamWriter{S: stream, W: ciphertext}
	if _, err := io.Copy(writer, plaintext); err != nil {
		return err
	}

	return nil
}

func Max(dirPath, fileExtension string, max int) (deletedCount int, err error) {
	// Normalize dirPath to use UNIX-style delimiters
	dirPath = filepath.ToSlash(dirPath)

	// Convert dirPath to OS-specific format
	osSpecificPath := filepath.FromSlash(dirPath)

	// Check if the directory exists
	if _, err = os.Stat(osSpecificPath); os.IsNotExist(err) {
		return 0, fmt.Errorf("directory does not exist: %s", osSpecificPath)
	}
	// if max < 0 {
	if max < 0 {
		return 0, fmt.Errorf("max value cannot be negative:%d", max)
	}
	// Special case: if max is 0, delete all matching files
	if max == 0 {
		deletedCount, err = Remove(osSpecificPath, fileExtension, false)
		if err != nil {
			return 0, fmt.Errorf("error deleting extra files in %s: %v", osSpecificPath, err)
		}
	}
	// Ensure the file extension starts with a dot
	if !strings.HasPrefix(fileExtension, ".") {
		fileExtension = "." + fileExtension
	}

	// Read all files in the directory
	entries, err := os.ReadDir(osSpecificPath)
	if err != nil {
		return deletedCount, fmt.Errorf("error reading directory: %w", err)
	}

	// Filter and collect files with the specified extension
	var matchingFiles []fs.DirEntry
	for _, entry := range entries {
		if !entry.IsDir() && strings.HasSuffix(entry.Name(), fileExtension) {
			matchingFiles = append(matchingFiles, entry)
		}
	}

	// If the number of matching files is not greater than max, return
	if len(matchingFiles) <= max {
		return deletedCount, nil
	}

	// If the number of matching files is not greater than max, return
	if len(matchingFiles) <= max {
		return deletedCount, nil
	}

	// Sort files by modification time (oldest first)
	sort.Slice(matchingFiles, func(i, j int) bool {
		infoI, _ := matchingFiles[i].Info()
		infoJ, _ := matchingFiles[j].Info()
		return infoI.ModTime().Before(infoJ.ModTime())
	})

	// Delete extra files, keeping the most recent ones
	filesToDelete := len(matchingFiles) - max
	for i := 0; i < filesToDelete; i++ {
		filePath := filepath.Join(osSpecificPath, matchingFiles[i].Name())
		if err = os.Remove(filePath); err != nil {
			return deletedCount, fmt.Errorf("error deleting file %s: %w", filePath, err)
		}
	}
	return deletedCount, nil
}

// Remove deletes all files in dirPath with the specified file extension.
// If recursive == true, it will visit each child directory recursively,
// and delete matching files.
func Remove(dirPath, fileExtension string, recursive bool) (deletedCount int, err error) {
	// Normalize dirPath to use UNIX-style delimiters
	dirPath = filepath.ToSlash(dirPath)

	// Convert dirPath to OS-specific format
	osSpecificPath := filepath.FromSlash(dirPath)

	// Check if the directory exists
	if _, err := os.Stat(osSpecificPath); os.IsNotExist(err) {
		return 0, fmt.Errorf("directory does not exist: %s", osSpecificPath)
	}

	// Ensure the file extension starts with a dot
	if !strings.HasPrefix(fileExtension, ".") {
		fileExtension = "." + fileExtension
	}

	walkFn := func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() {
			if !recursive && path != osSpecificPath {
				return filepath.SkipDir
			}
			return nil
		}

		if filepath.Ext(path) == fileExtension {
			if err := os.Remove(path); err != nil {
				return fmt.Errorf("error deleting file %s: %w", path, err)
			}
			deletedCount++
		}

		return nil
	}

	err = filepath.Walk(osSpecificPath, walkFn)
	if err != nil {
		return deletedCount, fmt.Errorf("error walking through directory: %w", err)
	}

	return deletedCount, nil
}

func DeleteFile(fpath string) error {
	return os.Remove(fpath)
}

func MkdirTemp(dir, pattern string) (string, error) {
	return os.MkdirTemp(dir, pattern)
}
