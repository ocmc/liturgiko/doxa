package ltfile

import (
	"context"
	"os"
	"path/filepath"
	"testing"
)

func TestFileExists(t *testing.T) {
	// Create a temporary file
	tmpfile, err := os.CreateTemp("", "test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(tmpfile.Name())

	tests := []struct {
		name     string
		path     string
		expected bool
	}{
		{
			name:     "existing file",
			path:     tmpfile.Name(),
			expected: true,
		},
		{
			name:     "non-existing file",
			path:     "/path/to/nonexistent/file",
			expected: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FileExists(tt.path); got != tt.expected {
				t.Errorf("FileExists() = %v, want %v", got, tt.expected)
			}
		})
	}
}

func TestDirExists(t *testing.T) {
	// Create a temporary directory
	tmpdir, err := os.MkdirTemp("", "test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(tmpdir)

	tests := []struct {
		name     string
		path     string
		expected bool
	}{
		{
			name:     "existing directory",
			path:     tmpdir,
			expected: true,
		},
		{
			name:     "non-existing directory",
			path:     "/path/to/nonexistent/dir",
			expected: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DirExists(tt.path); got != tt.expected {
				t.Errorf("DirExists() = %v, want %v", got, tt.expected)
			}
		})
	}
}

func TestDeleteDirRecursively(t *testing.T) {
	// Create a temporary directory with some content
	tmpdir, err := os.MkdirTemp("", "test")
	if err != nil {
		t.Fatal(err)
	}

	// Create a file inside the temporary directory
	tmpfile := filepath.Join(tmpdir, "testfile")
	if err := os.WriteFile(tmpfile, []byte("test content"), 0666); err != nil {
		t.Fatal(err)
	}

	if err := DeleteDirRecursively(tmpdir); err != nil {
		t.Errorf("DeleteDirRecursively() error = %v", err)
	}

	if DirExists(tmpdir) {
		t.Errorf("Directory still exists after DeleteDirRecursively()")
	}
}

func TestCreateDir(t *testing.T) {
	tmpdir, err := os.MkdirTemp("", "test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(tmpdir)

	newDir := filepath.Join(tmpdir, "newdir")

	if err := CreateDir(newDir); err != nil {
		t.Errorf("CreateDir() error = %v", err)
	}

	if !DirExists(newDir) {
		t.Errorf("Directory was not created")
	}
}

func TestCreateFileIfNotExists(t *testing.T) {
	tmpdir, err := os.MkdirTemp("", "test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(tmpdir)

	newFile := filepath.Join(tmpdir, "newfile")

	if err := CreateFileIfNotExists(newFile); err != nil {
		t.Errorf("CreateFileIfNotExists() error = %v", err)
	}

	if !FileExists(newFile) {
		t.Errorf("File was not created")
	}
}

func TestModifiedToday(t *testing.T) {
	tmpfile, err := os.CreateTemp("", "test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(tmpfile.Name())

	modified, err := ModifiedToday(tmpfile.Name())
	if err != nil {
		t.Errorf("ModifiedToday() error = %v", err)
	}

	if !modified {
		t.Errorf("File should be modified today")
	}
}

func TestWriteFile(t *testing.T) {
	tmpdir, err := os.MkdirTemp("", "test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(tmpdir)

	testFile := filepath.Join(tmpdir, "test.txt")
	testContent := "test content"

	if err := WriteFile(testFile, testContent); err != nil {
		t.Errorf("WriteFile() error = %v", err)
	}

	content, err := os.ReadFile(testFile)
	if err != nil {
		t.Fatal(err)
	}

	if string(content) != testContent {
		t.Errorf("WriteFile() wrote %v, want %v", string(content), testContent)
	}
}

func TestWriteLinesToFile(t *testing.T) {
	tmpdir, err := os.MkdirTemp("", "test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(tmpdir)

	testFile := filepath.Join(tmpdir, "test.txt")
	testLines := []string{"line1", "line2", "line3"}

	if err := WriteLinesToFile(testFile, testLines); err != nil {
		t.Errorf("WriteLinesToFile() error = %v", err)
	}

	content, err := os.ReadFile(testFile)
	if err != nil {
		t.Fatal(err)
	}

	expected := "line1\nline2\nline3\n"
	if string(content) != expected {
		t.Errorf("WriteLinesToFile() wrote %v, want %v", string(content), expected)
	}
}

func TestConcurrentCopyDir(t *testing.T) {
	// Create source directory with some content
	srcDir, err := os.MkdirTemp("", "test-src")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(srcDir)

	// Create some test files
	testFiles := []string{"file1.txt", "file2.txt", "file3.txt"}
	for _, fname := range testFiles {
		if err := os.WriteFile(filepath.Join(srcDir, fname), []byte("test content"), 0666); err != nil {
			t.Fatal(err)
		}
	}

	// Create destination directory
	dstDir, err := os.MkdirTemp("", "test-dst")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dstDir)

	if err := ConcurrentCopyDir(srcDir, dstDir, 2); err != nil {
		t.Errorf("ConcurrentCopyDir() error = %v", err)
	}

	// Verify files were copied
	for _, fname := range testFiles {
		if !FileExists(filepath.Join(dstDir, fname)) {
			t.Errorf("File %s was not copied", fname)
		}
	}
}

func TestMD5All(t *testing.T) {
	// Create test directory with some content
	tmpdir, err := os.MkdirTemp("", "test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(tmpdir)

	// Create some test files
	testFiles := map[string]string{
		"file1.txt": "content1",
		"file2.txt": "content2",
	}

	for fname, content := range testFiles {
		if err := os.WriteFile(filepath.Join(tmpdir, fname), []byte(content), 0666); err != nil {
			t.Fatal(err)
		}
	}

	ctx := context.Background()
	hashes, err := MD5All(ctx, tmpdir)
	if err != nil {
		t.Errorf("MD5All() error = %v", err)
	}

	if len(hashes) != len(testFiles) {
		t.Errorf("MD5All() returned %d hashes, want %d", len(hashes), len(testFiles))
	}
}

// TODO: Add tests for functions requiring external dependencies or complex setup:
// - HTMLTemplateToFile (requires template setup)
// - DocFromUrl (requires network access)
// - Unzip (requires zip file)
// - EncryptFile/DecryptFile (requires crypto setup)
// - Functions involving GitLab integration
