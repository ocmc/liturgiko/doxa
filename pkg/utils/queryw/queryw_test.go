package queryw

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/utils/stats"
	"testing"
)

func TestAttributeDataKey(t *testing.T) {
	theUrl := "https://dcs.goarch.org/goa/dcs/h/s/2023/07/02/li/en/index.html"
	theSelector := "span.kvp,span.key" //"[data-key]" // "span:data-key"
	theAttribute := "data-key"
	values, err := Attribute(theUrl, theSelector, theAttribute)
	if err != nil {
		t.Error(err)
		return
	}
	if values == nil {
		t.Errorf("expected values != nil")
		return
	}
	if len(values) == 0 {
		t.Errorf("expected len(values) > 0")
		return
	}
	m := stats.SliceToMap(values)
	for k, v := range m {
		fmt.Printf("%s: %d\n", k, v)
	}

}

func TestAttributeDataKeyUsingFile(t *testing.T) {
	theUrl := "/Volumes/macWdb/doxa/projects/doxa-liml/websites/public/dcs/h/b/eu/baptism/en/index.html"
	theSelector := "span.kvp"
	theAttribute := "data-id-used"
	values, err := Attribute(theUrl, theSelector, theAttribute)
	if err != nil {
		t.Error(err)
		return
	}
	if values == nil {
		t.Errorf("expected values != nil")
		return
	}
	if len(values) == 0 {
		t.Errorf("expected len(values) > 0")
		return
	}
	m := stats.SliceToMap(values)
	for k, v := range m {
		fmt.Printf("%s: %d\n", k, v)
	}

}
func TestDoc(t *testing.T) {
	theUrl := "/Volumes/macWdb/doxa/projects/doxa-liml/websites/public/dcs/h/b/eu/baptism/en/index.html"
	p, err := NewDocProcessor(theUrl)
	if err != nil {
		t.Error(err)
		return
	}
	p.WordCount()
	p.CountRedundancies()
	if p.AllWords == 0 {
		t.Errorf("expected p.Words > 0")
		return
	}
}
func TestSiteStats_Process(t *testing.T) {
	//p := "/Volumes/macWdb/doxa/projects/doxa-liml/websites/public/dcs/h/b/oc"
	p := "/Volumes/macWdb/doxa/projects/doxa-liml/websites/public/dcs/h/b/tr"
	//p := "/Volumes/macWdb/doxa/projects/doxa-liml/websites/public/dcs/h/b/pe"
	//p := "/Volumes/macWdb/doxa/projects/doxa-liml/websites/public/dcs/h/b"
	s := NewSiteProcessor(p, "en")
	err := s.Process()
	if err != nil {
		t.Error(err)
	}
	fmt.Printf("All words: %d\nTranslationWords: %d\nRedundant words: %d\nReduction %f\n", s.AllWords, s.TranslationWords, s.RedundantWords, s.Reduction)
}
func TestAttributeHref(t *testing.T) {
	theUrl := "https://dcs.goarch.org/goa/dcs/servicesindex.html"
	theSelector := "a.index-day-link"
	theAttribute := "href"
	values, err := Attribute(theUrl, theSelector, theAttribute)
	if err != nil {
		t.Error(err)
		return
	}
	if values == nil {
		t.Errorf("expected values != nil")
		return
	}
	if len(values) == 0 {
		t.Errorf("expected len(values) > 0")
		return
	}
	m := stats.SliceToMap(values)
	for k, v := range m {
		fmt.Printf("%s: %d\n", k, v)
	}

}
