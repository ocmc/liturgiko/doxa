// Package queryw provides a wrapper for goquery
package queryw

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"io"
	"net/http"
	"os"
	"strings"
)

// Attribute returns all attribute values for
// elements that match the selector
// and have an attribute with the specified name
// contained in the doc for the specified url
// Examples:
//
//	Attribute("https://dcs.goarch.org/goa/dcs/h/s/2023/07/02/li/en/index.html",
//	     "[data-key]", // selector for any element having this attribute
//	      "data-key") // attribute name whose value we want
//	  gets all elements with the attribute data-key, and extracts the value
//	  of data-key.
//	Attribute("https://dcs.goarch.org/goa/dcs/h/s/2023/07/02/li/en/index.html",
//	    "span.kvp, span.key", // selector for all spans having class="kvp" or "key"
//	   "data-key")  // attribute name whose value we want
//	   This 2nd example gets the same result as the 1st, but less efficiently.
func Attribute(theUrl, theSelector, attrName string) (values []string, err error) {
	doc, err := Doc(theUrl)
	if err != nil {
		return nil, err
	}
	doc.Find(theSelector).Each(func(i int, s *goquery.Selection) {
		if attrVal, exists := s.Attr(attrName); exists {
			values = append(values, attrVal)
		}
	})
	return values, err
}
func Doc(theUrl string) (doc *goquery.Document, err error) {
	var content []byte
	if strings.HasPrefix(theUrl, "https://") {
		var resp *http.Response
		resp, err = http.Get(theUrl)
		if err != nil {
			return nil, fmt.Errorf("GET error: %v", err)
		}
		defer resp.Body.Close()
		if resp.StatusCode != http.StatusOK {
			return nil, fmt.Errorf("response status error: %s", resp.Status)
		}
		content, err = io.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}
	} else {
		content, err = os.ReadFile(theUrl)
	}
	doc, err = goquery.NewDocumentFromReader(strings.NewReader(string(content)))
	if err != nil {
		return nil, err
	}
	return doc, nil
}

type Record struct {
	ID          string
	Occurrences int
	Words       int
}
type DocProcessor struct {
	Doc            *goquery.Document
	AllWords       int
	RedundantWords int
	Reduction      float32
	Map            map[string]*Record
}

func NewDocProcessor(theUrl string) (*DocProcessor, error) {
	p := new(DocProcessor)
	doc, err := Doc(theUrl)
	if err != nil {
		return nil, err
	}
	p.Doc = doc
	p.Map = make(map[string]*Record)
	return p, nil
}
func (p *DocProcessor) WordCount() {
	p.Doc.Find("span.kvp").Each(func(i int, s *goquery.Selection) {
		if id, ok := s.Attr("data-id-used"); ok {
			t := s.Text()
			if len(t) == 0 {
				return
			}
			record := p.Map[id]
			if record == nil {
				record = &Record{
					ID:          id,
					Occurrences: 1,
					Words:       len(strings.Fields(ltstring.ToNnp(t))),
				}
			} else {
				record.Occurrences++
			}
			p.Map[id] = record
			p.AllWords += record.Words
		}
	})
}
func (p *DocProcessor) CountRedundancies() {
	for _, v := range p.Map {
		if v.Occurrences-1 >= 1 {
			p.RedundantWords += v.Words * (v.Occurrences - 1)
		}
	}
	if p.AllWords > 0 {
		p.Reduction = float32(p.RedundantWords) / float32(p.AllWords)
	}
}
func (p *DocProcessor) GetFullPageText() string {
	var sb strings.Builder
	p.Doc.Find("span.kvp").Contents().Each(func(i int, s *goquery.Selection) {
		if s.Is("script, style") {
			return
		}
		sb.WriteString(s.Text())
		sb.WriteString(" ")
	})
	return sb.String()
}

type SiteStats struct {
	FolderPath       string
	LangAcronym      string
	AllWords         int
	RedundantWords   int
	TranslationWords int
	Reduction        float32
}

func NewSiteProcessor(folderPath, langAcronym string) *SiteStats {
	s := new(SiteStats)
	s.FolderPath = folderPath
	s.LangAcronym = langAcronym
	s.AllWords = 0
	s.RedundantWords = 0
	s.Reduction = 0.0
	return s
}
func (s *SiteStats) Process() error {
	theFilePaths, err := ltfile.FileMatcher(s.FolderPath, ".html", nil)
	if err != nil {
		return err
	}
	sep := "/"
	pattern := fmt.Sprintf("%s%s%sindex.html", sep, s.LangAcronym, sep)
	for _, f := range theFilePaths {
		if strings.HasSuffix(f, pattern) {
			var p *DocProcessor
			p, err = NewDocProcessor(f)
			if err != nil {
				return err
			}
			p.WordCount()
			p.CountRedundancies()
			s.AllWords += p.AllWords
			s.RedundantWords += p.RedundantWords
		}
	}
	if s.AllWords > 0 {
		s.Reduction = float32(s.RedundantWords) / float32(s.AllWords)
		s.TranslationWords = int(float32(s.AllWords) * (1 - s.Reduction))
	}
	return nil
}
func ValidUrl(theUrl string) bool {
	resp, err := http.Get(theUrl)
	if err != nil {
		return false
	}
	if resp.StatusCode != http.StatusOK {
		return false
	}
	return true
}
func Meta(theUrl, name string) (values []string, err error) {
	theSelector := fmt.Sprintf("meta[name='%s']", name)
	theAttribute := "content"
	return Attribute(theUrl, theSelector, theAttribute)
}
func MetaAuthor(theUrl string) (string, error) {
	var values []string
	var err error
	if values, err = Meta(theUrl, "author"); err != nil {
		return "", err
	}
	if len(values) == 0 {
		return "", nil
	}
	return values[0], nil
}
func Children(theUrl, theSelector string) (*goquery.Selection, error) {
	resp, err := http.Get(theUrl)
	if err != nil {
		return nil, fmt.Errorf("GET error: %v", err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("response status error: %s", resp.Status)
	}
	content, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(string(content)))
	if err != nil {
		return nil, err
	}
	return doc.Find(theSelector), err
}
