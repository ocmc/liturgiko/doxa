package stats

import "sort"

func SliceToMap(s []string) map[string]int {
	m := make(map[string]int)
	sort.Strings(s)
	for _, i := range s {
		if len(i) == 0 {
			continue
		}
		v := 0
		var ok bool
		if v, ok = m[i]; ok {
		}
		v = v + 1
		m[i] = v
	}
	return m
}
