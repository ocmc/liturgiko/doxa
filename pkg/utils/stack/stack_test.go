package stack

import (
	"fmt"
	"testing"
)

func TestStringStack_Push(t *testing.T) {
	s := new(StringStack)
	s.Push("yo")
	s.Push("dude")
	expect := "yo/dude"
	got := s.Join("/")
	if expect != got {
		t.Errorf("expected %s, got %s", expect, got)
	}
	expect = "yo"
	got = s.Get(0)
	if expect != got {
		t.Errorf("expected %s, got %s", expect, got)
	}
	expect = "dude"
	got = s.Get(1)
	if expect != got {
		t.Errorf("expected %s, got %s", expect, got)
	}
	expect = ""
	got = s.Get(2)
	if expect != got {
		t.Errorf("expected %s, got %s", expect, got)
	}
	s.Clear()
	expect = "whats up?"
	s.Push(expect)
	got = s.Last()
	if expect != got {
		t.Errorf("expected %s, got %s", expect, got)
	}
}
func TestBoolStack_Push(t *testing.T) {
	s := new(BoolStack)
	s.Push(true)
	s.Push(true)
	expect := true
	got := s.Pop()
	if expect != got {
		t.Errorf("expected %t, got %t", expect, got)
	}
	got = s.True()
	if expect != got {
		t.Errorf("expected %t, got %t", expect, got)
	}
	s.Push(false)
	expect = false
	got = s.True()
	if expect != got {
		t.Errorf("expected %t, got %t", expect, got)
	}
	s.Flip()
	expect = true
	got = s.True()
	if expect != got {
		t.Errorf("expected %t, got %t", expect, got)
	}
	s.Print()
	s.FlipIfTrue()
	s.Print()
	expect = false
	got = s.True()
	if expect != got {
		t.Errorf("expected %t, got %t", expect, got)
	}
	s.Clear()
	s.Push(false)
	s.FlipIfFalse()
	expect = true
	got = s.True()
	if expect != got {
		t.Errorf("expected %t, got %t", expect, got)
	}
}
func TestComparator_CommonPath(t *testing.T) {
	s1 := new(StringStack)
	s1.Push("both__")
	s1.Push("endofmatins_theotokion__")
	s1.Push("modeofday")
	s2 := new(StringStack)
	s2.Push("both__")
	s2.Push("endofmatins_theotokion__")
	c := NewComparator(s1, s2)
	got := c.CommonPath()
	expect := "both__/endofmatins_theotokion__"
	if got != expect {
		t.Errorf("expected %s\ngot %s", expect, got)
	}
}
func TestGeneric(t *testing.T) {
	s1 := new(StringStack)
	s1.Push("Blocks")
	s1.Push("AP")
	s1.Push("both__")
	s1.Push("endofmatins_theotokion__")
	s1.Push("modeofday")

	s2 := new(StringStack)
	s2.Push("Blocks")
	s2.Push("AP")
	s2.Push("gloryboth__")
	s2.Push("endofmatins_theotokion__")
	s2.Push("modeofday")

	target := "mode1_/wednesday"

	pathMap := make(map[string]bool)
	pathMap["Blocks/AP/both__/endofmatins_theotokion__/mode1_/wednesday"] = true
	pathMap["Blocks/AP/both__/theotokion__/mode1_/wednesday"] = true
	pathMap["Blocks/AP/gloryboth__/endofmatins_theotokion__/mode1_/wednesday"] = true
	pathMap["Blocks/AP/gloryboth__/theotokion__/mode1_/wednesday"] = true
	pathMap["Blocks/AP/NM/no media/both__/endofmatins_theotokion__/mode1_/wednesday"] = true
	pathMap["Blocks/AP/NM/no media/both__/theotokion__/mode1_/wednesday"] = true
	pathMap["Blocks/AP/NM/no media/gloryboth__/endofmatins_theotokion__/mode1_/wednesday"] = true
	pathMap["Blocks/AP/NM/no media/gloryboth__/theotokion__/mode1_/wednesday"] = true

	expect := "Blocks/AP/both__/endofmatins_theotokion__/mode1_/wednesday"
	got := MatchingPath(target, s1, pathMap)
	if expect != got {
		t.Errorf("\nexpected %s\ngot %s", expect, got)
	}
	expect = "Blocks/AP/gloryboth__/endofmatins_theotokion__/mode1_/wednesday"
	got = MatchingPath(target, s2, pathMap)
	if expect != got {
		t.Errorf("\nexpected %s\ngot %s", expect, got)
	}
}
func MatchingPath(target string, contextStack *StringStack, pathMap map[string]bool) string {
	c := contextStack.Copy()
	j := c.Size()
	for i := 0; i < j; i++ {
		p := fmt.Sprintf("%s/%s", c.Join("/"), target)
		if _, ok := pathMap[p]; ok {
			return p
		}
		c.Pop()
	}
	return ""
}
