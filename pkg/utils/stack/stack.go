package stack

import (
	"encoding/json"
	"fmt"
	"strings"
)

type StringStack []string

// NewStringStack converts a json string that is an array into a StringStack
func NewStringStack(jsonArray string) (*StringStack, error) {
	s := new(StringStack)
	var theSlice []string
	err := json.Unmarshal([]byte(jsonArray), &theSlice)
	if err != nil {
		return nil, err
	}
	for _, i := range theSlice {
		s.Push(i)
	}
	return s, err
}
func (s *StringStack) Clear() {
	*s = nil
}
func (s *StringStack) Slice() []string {
	var r []string
	for _, s := range *s {
		r = append(r, s)
	}
	return r
}

// Copy values into a new StringStack
func (s *StringStack) Copy() *StringStack {
	newStack := new(StringStack)
	for _, item := range *s {
		newStack.Push(item)
	}
	return newStack
}

// Remove returns a new StringStack without the nth element
func (s *StringStack) Remove(r int) *StringStack {
	newStack := new(StringStack)
	for i, item := range *s {
		if i == r {
			continue
		}
		newStack.Push(item)
	}
	return newStack
}
func (s *StringStack) Empty() bool {
	return len(*s) == 0
}

// First returns the initial item pushed on the stack, without removing it
func (s *StringStack) First() string {
	if len(*s) > 0 {
		return (*s)[0] // Index into the slice and obtain the element.
	}
	return ""
}

// Get the nth element in the stack, without removing it.
// The stack is zero based, i.e. first element is at index == 0.
// If the value of index >= len(stack), returns ""
func (s *StringStack) Get(index int) string {
	if index >= len(*s) {
		return ""
	}
	return (*s)[index]
}

// Set the indexed element in the stack.
// The stack is zero based, i.e. first element is at index == 0.
// If the value of index >= len(stack), returns an error.
func (s *StringStack) Set(index int, value string) error {
	if index >= len(*s) {
		return fmt.Errorf("%d exceeds length of stack", index)
	}
	(*s)[index] = value
	return nil
}
func (s *StringStack) Json(pretty bool) string {
	var result []byte
	if pretty {
		result, _ = json.MarshalIndent(s, "", " ")
	} else {
		result, _ = json.Marshal(s)
	}
	return string(result)
}

// Join returns the elements in the stack as a delimited string.
func (s *StringStack) Join(delimiter string) string {
	return strings.Join(*s, delimiter)
}
func (s *StringStack) SubPath(from, upTo int, delimiter string) string {
	sb := strings.Builder{}
	for i := from; i < upTo; i++ {
		if sb.Len() > 0 {
			sb.WriteString(delimiter)
		}
		sb.WriteString(s.Get(i))
	}
	return sb.String()
}
func (s *StringStack) SubSlice(from, upTo int) []string {
	var subSlice []string
	for i := from; i < upTo; i++ {
		subSlice = append(subSlice, s.Get(i))
	}
	return subSlice
}

// Enqueue creates a new StringStack by first pushing item, then the rest of the original stack
func (s *StringStack) Enqueue(item string) *StringStack {
	n := new(StringStack)
	n.Push(item)
	l := s.Size()
	for i := 0; i < l; i++ {
		n.Push(s.Get(i))
	}
	return n
}

// Last returns most recent item pushed on the stack, without popping the stack
func (s *StringStack) Last() string {
	if len(*s) > 0 {
		index := len(*s) - 1 // GetKeyPath the index of the last element.
		return (*s)[index]   // Index into the slice and obtain the element.
	}
	return ""
}
func (s *StringStack) Pop() string {
	if len(*s) > 0 {
		index := len(*s) - 1   // GetKeyPath the index of the top most element.
		element := (*s)[index] // Index into the slice and obtain the element.
		*s = (*s)[:index]
		return element
	}
	return ""
}
func (s *StringStack) Push(value string) {
	*s = append(*s, value)
}
func (s *StringStack) Size() int {
	return len(*s)
}

// Split calls Clear(), then splits the value using the delimiter, and pushes each part of the split onto the stack.
func (s *StringStack) Split(value, delimiter string) {
	s.Clear()
	parts := strings.Split(value, delimiter)
	for _, p := range parts {
		s.Push(p)
	}
}

type BoolStack []bool

func (s *BoolStack) Clear() {
	*s = nil
}
func (s *BoolStack) Slice() []bool {
	var r []bool
	for _, s := range *s {
		r = append(r, s)
	}
	return r
}

// Copy values into a new BoolStack
func (s *BoolStack) Copy() *BoolStack {
	newStack := new(BoolStack)
	for _, s := range *s {
		newStack.Push(s)
	}
	return newStack
}

func (s *BoolStack) Empty() bool {
	return len(*s) == 0
}

// First returns the initial item pushed on the stack, without removing it
func (s *BoolStack) First() bool {
	if len(*s) > 0 {
		return (*s)[0] // Index into the slice and obtain the element.
	}
	return false
}

// Get the nth element in the stack, without removing it.
// The stack is zero based, i.e. first element is at index == 0.
// If the value of index >= len(stack), returns ""
func (s *BoolStack) Get(index int) bool {
	if index >= len(*s) {
		return false
	}
	return (*s)[index]
}

// True returns true if all values are true or stack is empty
func (s *BoolStack) True() bool {
	for _, s := range *s {
		if s == false {
			return false
		}
	}
	return true
}

// Print a single string.  Useful for debugging.
func (s *BoolStack) Print() string {
	return fmt.Sprintf("%v\n", *s)
}

// Set the indexed element in the stack.
// The stack is zero based, i.e. first element is at index == 0.
// If the value of index >= len(stack), returns an error.
func (s *BoolStack) Set(index int, value bool) error {
	if index >= len(*s) {
		return fmt.Errorf("%d exceeds length of stack", index)
	}
	(*s)[index] = value
	return nil
}

// Last returns most recent item pushed on the stack, without popping the stack
func (s *BoolStack) Last() bool {
	if len(*s) > 0 {
		index := len(*s) - 1 // GetKeyPath the index of the last element.
		return (*s)[index]   // Index into the slice and obtain the element.
	}
	return false
}

// Flip the value of the top of the stack, e.g. from true to false or vice versa
func (s *BoolStack) Flip() {
	if !s.Empty() {
		s.Push(!s.Pop())
	}
}

// FlipIfFalse changes top of stack to true if currently false
func (s *BoolStack) FlipIfFalse() {
	if !s.Empty() && !s.Last() {
		s.Push(!s.Pop())
	}
}

// FlipIfTrue changes top of stack to false if currently true
func (s *BoolStack) FlipIfTrue() {
	if !s.Empty() && s.Last() {
		s.Push(!s.Pop())
	}
}
func (s *BoolStack) Pop() bool {
	if len(*s) > 0 {
		index := len(*s) - 1   // GetKeyPath the index of the top most element.
		element := (*s)[index] // Index into the slice and obtain the element.
		*s = (*s)[:index]
		return element
	}
	return false
}
func (s *BoolStack) Push(value bool) {
	*s = append(*s, value)
}
func (s *BoolStack) Size() int {
	return len(*s)
}

type Comparator struct {
	Stack1 *StringStack
	Stack2 *StringStack
}

func NewComparator(s1, s2 *StringStack) *Comparator {
	c := new(Comparator)
	c.Stack1 = s1
	c.Stack2 = s2
	return c
}
func (c *Comparator) CommonPath() string {
	j := c.Stack1.Size()
	k := c.Stack2.Size()
	sb := strings.Builder{}
	for i := 0; i < j; i++ {
		v1 := c.Stack1.Get(i)
		var v2 string
		if i < k {
			v2 = c.Stack2.Get(i)
		}
		if v1 == v2 {
			if sb.Len() > 0 {
				sb.WriteString("/")
			}
			sb.WriteString(v1)
		} else {
			break
		}
	}
	return sb.String()
}

type IntStack []int

func (s *IntStack) Push(v int) {
	*s = append(*s, v)
}

func (s *IntStack) Pop() (int, bool) {
	if s.Size() == 0 {
		return 0, false
	}
	index := len(*s) - 1
	element := (*s)[index]
	*s = (*s)[:index]
	return element, true
}

func (s *IntStack) Size() int {
	return len(*s)
}

// First returns the initial item pushed on the stack, without removing it.
// If the stack is empty, returns -1, false
func (s *IntStack) First() (int, bool) {
	if len(*s) > 0 {
		return (*s)[0], true // Index into the slice and obtain the element.
	}
	return -1, false
}

func (s *IntStack) Dup() {
	val, hath := s.Pop()
	if !hath {
		return
	}
	s.Push(val)
}
