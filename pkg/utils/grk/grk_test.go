package grk

import (
	"testing"
)

func TestIntToUpperAll(t *testing.T) {
	var err error
	for i := 1; i < 10000; i++ {
		_, err = IntToNumeral(i, true)
		if err != nil {
			t.Error(err)
			return
		}
	}
}
func TestIntToLowerAll(t *testing.T) {
	var err error
	for i := 1; i < 10000; i++ {
		_, err = IntToNumeral(i, false)
		if err != nil {
			t.Error(err)
			return
		}
	}
}
func TestIntToNumeralOutOfRange(t *testing.T) {
	_, err := IntToNumeral(0, false)
	if err == nil {
		t.Errorf("for 0, expected an out of range error")
	}
	_, err = IntToNumeral(10000, false)
	if err == nil {
		t.Errorf("for 10000, expected an out of range error")
	}
}

func TestIntToNumeralBoundaries(t *testing.T) {
	type Case struct {
		number int
		upper  bool
		expect string
	}
	var cases = []Case{
		Case{1, false, "αʹ"},
		Case{1, true, "Αʹ"},
		Case{9, false, "θʹ"},
		Case{9, true, "Θʹ"},
		Case{10, false, "ιʹ"},
		Case{10, true, "Ιʹ"},
		Case{11, false, "ιαʹ"},
		Case{11, true, "ΙΑʹ"},
		Case{31, false, "λαʹ"},
		Case{31, true, "ΛΑʹ"},
		Case{99, false, "ϙθʹ"},
		Case{99, true, "ϘΘʹ"},
		Case{100, false, "ρʹ"},
		Case{100, true, "Ρʹ"},
		Case{101, false, "ραʹ"},
		Case{101, true, "ΡΑʹ"},
		Case{256, false, "σνστʹ"},
		Case{256, true, "ΣΝΣΤʹ"},
		Case{999, false, "ϡϙθʹ"},
		Case{999, true, "ϠϘΘʹ"},
		Case{1000, false, "͵α"},
		Case{1000, true, "͵Α"},
		Case{1001, false, "͵αα"},
		Case{1001, true, "͵ΑΑ"},
		Case{3000, false, "͵γ"},
		Case{3000, true, "͵Γ"},
		Case{3426, false, "͵γυκστ"},
		Case{3426, true, "͵ΓΥΚΣΤ"},
		Case{9999, false, "͵θϡϙθ"},
		Case{9999, true, "͵ΘϠϘΘ"},
	}
	for _, c := range cases {
		got, err := IntToNumeral(c.number, c.upper)
		if err != nil {
			t.Error(err)
			return
		}
		if got != c.expect {
			t.Errorf("for %d, expected %s, got %s", c.number, c.expect, got)
		}
		if c.number == 256 {
			if c.upper {
				expect := "ΣΝϚʹ"
				got = ToAncient(got)
				if got != expect {
					t.Errorf("for %d, expected %s, got %s", c.number, expect, got)
				}
			} else {
				expect := "σνϛʹ"
				got = ToAncient(got)
				if got != expect {
					t.Errorf("for %d, expected %s, got %s", c.number, expect, got)
				}
			}
		}
	}
}
