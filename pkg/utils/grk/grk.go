package grk

import (
	"fmt"
	"golang.org/x/text/unicode/norm"
	"strconv"
	"strings"
)

// UpperNumeralSign precedes numbers < 1000
var UpperNumeralSign = "ʹ" // U+0374
// LowerNumeralSign precedes numbers >= 1000
var LowerNumeralSign = "͵" // U+0375
var UpperCaseOnes = []string{
	"Α",  // 1
	"Β",  // 2
	"Γ",  // 3
	"Δ",  // 4
	"Ε",  // 5
	"ΣΤ", // 6, can also use Ϛ, by calling ToAncient()
	"ζ",  // 7, i == 6
	"Η",  // 8
	"Θ",  // 9
}
var LowerCaseOnes = []string{
	"α",  // 1
	"β",  // 2
	"γ",  // 3
	"δ",  // 4
	"ε",  // 5
	"στ", // or ϛ, by calling ToAncient()
	"ζ",  // 7
	"η",  // 8
	"θ",  // 9
}
var UpperCaseTens = []string{
	"Ι", // 10
	"Κ", // 20
	"Λ", // 30
	"Μ", // 40
	"Ν", // 50
	"Ξ", // 60
	"Ο", // 70
	"Π", // 80
	"Ϙ", // 90
}
var LowerCaseTens = []string{
	"ι", // 10
	"κ", // 20
	"λ", // 30
	"μ", // 40
	"ν", // 50
	"ξ", // 60
	"ο", // 70
	"π", // 80
	"ϙ", // 90
}
var UpperCaseHundreds = []string{
	"Ρ", // 100
	"Σ", // 200
	"Τ", // 300
	"Υ", // 400
	"Φ", // 500
	"Χ", // 600
	"Ψ", // 700
	"Ω", // 800
	"Ϡ", // 900
}
var LowerCaseHundreds = []string{
	"ρ", // 100
	"σ", // 200
	"τ", // 300
	"υ", // 400
	"φ", // 500
	"χ", // 600
	"ψ", // 700
	"ω", // 800
	"ϡ", // 900
}

var UpperCaseThousands = []string{
	"Α",  // 1000
	"Β",  // 2000
	"Γ",  // 3000
	"Δ",  // 4000
	"Ε",  // 5000
	"ΣΤ", // 6000
	"Ζ",  // 7000
	"Η",  // 8000
	"Θ",  // 9000
}
var LowerCaseThousands = []string{
	"α",  // 1000
	"β",  // 2000
	"γ",  // 3000
	"δ",  // 4000
	"ε",  // 5000
	"στ", // 6000
	"ζ",  // 7000
	"η",  // 8000
	"θ",  // 9000
}

// ToAncient converts all occurrences of στ to ϛ, and ΣΤ to Ϛ in a Greek numeral n. To get n, first call IntToNumeral
func ToAncient(n string) string {
	r := n
	r = strings.ReplaceAll(r, "στ", "ϛ")
	r = strings.ReplaceAll(r, "ΣΤ", "Ϛ")
	return r
}

// IntToNumeral returns the Greek numeral for n, an integer > 0 and < 10000. If upper == true, returns uppercase Greek numeral.
func IntToNumeral(n int, upper bool) (string, error) {
	if n == 0 || n > 9999 {
		return "", fmt.Errorf("number must be > 0 and < 10000")
	}
	var err error
	if n < 10 {
		var ones string
		if ones, err = GetOnesForCase(n, upper); err != nil {
			return "", err
		}
		return fmt.Sprintf("%s%s", ones, UpperNumeralSign), nil
	} else if n >= 10 && n < 100 {
		var str, strOnes, strTens string
		str = fmt.Sprintf("%d", n)
		intOnes, _ := strconv.Atoi(str[1:])
		if strOnes, err = GetOnesForCase(intOnes, upper); err != nil {
			return "", err
		}
		intTens, _ := strconv.Atoi(str[0:1])
		if strTens, err = GetTensForCase(intTens, upper); err != nil {
			return "", err
		}
		return fmt.Sprintf("%s%s%s", strTens, strOnes, UpperNumeralSign), nil
	} else if n >= 100 && n < 1000 {
		var str, strOnes, strTens, strHundreds string
		str = fmt.Sprintf("%d", n)
		intOnes, _ := strconv.Atoi(str[2:])
		if strOnes, err = GetOnesForCase(intOnes, upper); err != nil {
			return "", err
		}
		intTens, _ := strconv.Atoi(str[1:2])
		if strTens, err = GetTensForCase(intTens, upper); err != nil {
			return "", err
		}
		intHundreds, _ := strconv.Atoi(str[0:1])
		if strHundreds, err = GetHundredsForCase(intHundreds, upper); err != nil {
			return "", err
		}
		return fmt.Sprintf("%s%s%s%s", strHundreds, strTens, strOnes, UpperNumeralSign), nil
	} else { // > 999
		var str, strOnes, strTens, strHundreds, strThousands string
		str = fmt.Sprintf("%d", n)
		intOnes, _ := strconv.Atoi(str[3:])
		if strOnes, err = GetOnesForCase(intOnes, upper); err != nil {
			return "", err
		}
		intTens, _ := strconv.Atoi(str[2:3])
		if strTens, err = GetTensForCase(intTens, upper); err != nil {
			return "", err
		}
		intHundreds, _ := strconv.Atoi(str[1:2])
		if strHundreds, err = GetHundredsForCase(intHundreds, upper); err != nil {
			return "", err
		}
		intThousands, _ := strconv.Atoi(str[0:1])
		if strThousands, err = GetThousandsForCase(intThousands, upper); err != nil {
			return "", err
		}
		return fmt.Sprintf("%s%s%s%s%s", LowerNumeralSign, strThousands, strHundreds, strTens, strOnes), nil
	}
}
func GetOnesForCase(i int, upper bool) (string, error) {
	if i == 0 {
		return "", nil
	}
	if i > len(UpperCaseOnes) {
		return "", fmt.Errorf("index %d > length of numeral slice", i)
	}
	i--
	if upper {
		return UpperCaseOnes[i], nil
	}
	return LowerCaseOnes[i], nil
}
func GetTensForCase(i int, upper bool) (string, error) {
	if i == 0 {
		return "", nil
	}
	if i > len(UpperCaseTens) {
		return "", fmt.Errorf("index %d > length of numeral slice", i)
	}
	i--
	if upper {
		return UpperCaseTens[i], nil
	}
	return LowerCaseTens[i], nil
}
func GetHundredsForCase(i int, upper bool) (string, error) {
	if i == 0 {
		return "", nil
	}
	if i > len(UpperCaseHundreds) {
		return "", fmt.Errorf("index %d > length of numeral slice", i)
	}
	i--
	if upper {
		return UpperCaseHundreds[i], nil
	}
	return LowerCaseHundreds[i], nil
}
func GetThousandsForCase(i int, upper bool) (string, error) {
	if i == 0 {
		return "", nil
	}
	if i > len(UpperCaseThousands) {
		return "", fmt.Errorf("index %d > length of numeral slice", i)
	}
	i--
	if upper {
		return UpperCaseThousands[i], nil
	}
	return LowerCaseThousands[i], nil
}

// Boundaries returns the index(es) of the start of each boundary character
// of a unicode string. In go, a string is actually a byte array of
// unsigned 8-bit integers (uint8). In go, byte is an alias for uint8.
// The len function returns the number of bytes in the string.
//
// For example:
// s := norm.NFD.String("ἀγάπη")
// indexes := Boundaries(s, norm.NFD)
// fmt.Printf("len(s): %d len(indexes) %d %v\n", len(s), len(indexes), indexes)
//
//		for i, d := range indexes {
//		  if i < len(indexes)-1 {
//		    fmt.Printf("%d %[2]s: %+[2]q\n", d, s[d:indexes[i+1]])
//		  } else {
//		  fmt.Printf("%d %[2]s: %+[2]q\n", d, s[d:])
//		  }
//	}
//
// outputs:
//
//	len(s): 14 len(indexes) 5 [0 4 6 10 12]
//	0 ἀ: "\u03b1\u0313"
//	4 γ: "\u03b3"
//	6 ά: "\u03b1\u0301"
//
// 10 π: "\u03c0"
// 12 η: "\u03b7"
func Boundaries(s string, f norm.Form) (indexes []int) {
	indexes = append(indexes, 0) // 1st is always a boundary
	l := len(s)
	for i := 0; i < l; {
		d := f.NextBoundaryInString(s[i:], true)
		i += d
		if i < l {
			indexes = append(indexes, i)
		}
	}
	return indexes
}
