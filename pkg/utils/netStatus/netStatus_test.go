package netStatus

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/utils/stamp"
	"runtime"
	"testing"
)

func TestCheckOnMac(t *testing.T) {
	timer := stamp.NewStamp("netStatus.CheckOnMac")
	timer.Start()
	networkQuality, ok, err := CheckOnMac()
	fmt.Println(timer.FinishMillis())
	// If we're not on a Mac, CheckOnMac should return nil, false, nil
	if runtime.GOOS != "darwin" {
		if networkQuality != nil || ok || err != nil {
			t.Errorf("Was expecting nil, false, nil on non-Mac system, but got %#v, %v, %v", networkQuality, ok, err)
		}
		return
	}
	if !ok || err != nil {
		t.Errorf("Error running CheckOnMac: %v", err)
	}
}
