package netStatus

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"os/exec"
	"runtime"
)

func CheckGitLabStatus() bool {
	url := "https://gitlab.com"
	return checkInternetConnection(url)
}
func InternetAvailable() bool {
	return CheckInternetStatus()
}
func CheckInternetStatus() bool {
	var ok bool
	url := "https://www.google.com"
	ok = checkInternetConnection(url)
	if !ok {
		ok = CheckGitLabStatus()
	}
	return ok
}
func checkInternetConnection(url string) bool {
	// Make a GET request
	resp, err := http.Get(url)
	if err != nil {
		return false
	}
	defer func(Body io.ReadCloser) {
		err = Body.Close()
		if err != nil {
			// do nothing
		}
	}(resp.Body)

	// Check if the HTTP status code is 200 OK
	if resp.StatusCode == http.StatusOK {
		return true
	}
	return false
}

/*
COMPUTER OUTPUT FIELD DESCRIPTION
     The -c option will produce JSON output with the following fields:

     base_rt                     The calculated idle latency of the test run (in milliseconds).

     dl_flows                    Number of download flows initiated.

     dl_responsiveness           The downlink responsiveness score (in RPM)

                                 (only available when -s is specified).

     dl_throughput               The measured downlink throughput (in bytes per second).

     end_date                    Time when test run was completed (in local time).

     il_h2_req_resp              The idle-latency Request/Response times for HTTP/2 (in milliseconds).

     il_tcp_handshake_443        The idle-latency TCP-handshake times (in milliseconds).

     il_tls_handshake            The idle-latency TLS-handshake times (in milliseconds).

     interface_name              Interface name in which the test ran against.

     lud_foreign_dl_h2_req_resp  Download latency-under-load request/response times for HTTP/2 (in milliseconds).

                                 (only available when -s is specified).

     lud_foreign_dl_tcp_handshake_443
                                 Download latency-under-load for TCP-handshake times (in milliseconds).

                                 (only available when -s is specified).

     lud_foreign_dl_tls_handshake
                                 Download latency-under-load for for TLS-handshake times (in milliseconds).

lud_foreign_h2_req_resp     Combined upload/download latency-under-load request/response times for HTTP/2 (in milliseconds).

                                 (only available when -s is not specified).

     lud_foreign_tcp_handshake_443
                                 Combined upload/download latency-under-load for for TCP-handshake times (in milliseconds).

                                 (only available when -s is not specified).

     lud_foreign_tls_handshake   Combined foreign upload/download latency-under-load for for TLS-handshake times (in milliseconds).

                                 (only available when -s is not specified).

     lud_foreign_ul_h2_req_resp  Foreign upload latency-under-load request/response times for HTTP/2 (in milliseconds).

                                 (only available when -s is specified).

     lud_foreign_ul_tcp_handshake_443
                                 Foreign upload latency-under-load for for TCP-handshake times (in milliseconds).

                                 (only available when -s is specified).

     lud_foreign_ul_tls_handshake
                                 Upload latency-under-load for for TLS-handshake times (in milliseconds).

                                 (only available when -s is specified).

     lud_self_dl_h2_req_resp     Self download latency-under-load request/response times for HTTP/2 (in milliseconds).

                                 (only available when -s is specified).
lud_self_dl_h2_req_resp     Self download latency-under-load request/response times for HTTP/2 (in milliseconds).

                                 (only available when -s is specified).

     lud_self_h2_req_resp        Combined self upload/download latency-under-load request/response times for HTTP/2 (in milliseconds).

                                 (only available when -s is not specified).

     lud_self_ul_h2_req_resp     Self upload latency-under-load request/response times for HTTP/2 (in milliseconds).

                                 (only available when -s is specified).

     os_version                  The version of the OS the test was run on.

     responsiveness              The responsiveness score (in RPM)

                                 (the combined value if -c is not specified).

     start_date                  Time when test run was started (in local time).

     ul_flows                    Number of upload flows created.

     ul_responsiveness           The uplink responsiveness score (in RPM)

                                 (only available when -s is specified).

     ul_throughput               The measured uplink throughput (in bytes per second).
*/

type NetworkQuality struct {
	BaseRtt                   float64 `json:"base_rtt"`
	DlBytesTransferred        int64   `json:"dl_bytes_transferred"`
	DlFlows                   int     `json:"dl_flows"`
	DlThroughput              int     `json:"dl_throughput"`
	EndDate                   string  `json:"end_date"`
	IlH2ReqResp               []int   `json:"il_h2_req_resp"`
	IlTcpHandshake443         []int   `json:"il_tcp_handshake_443"`
	IlTlsHandshake            []int   `json:"il_tls_handshake"`
	InterfaceName             string  `json:"interface_name"`
	LudForeignH2ReqResp       []int   `json:"lud_foreign_h2_req_resp"`
	LudForeignTcpHandshake443 []int   `json:"lud_foreign_tcp_handshake_443"`
	LudForeignTlsHandshake    []int   `json:"lud_foreign_tls_handshake"`
	LudSelfH2ReqResp          []int   `json:"lud_self_h2_req_resp"`
	OsVersion                 string  `json:"os_version"`
	Other                     Other   `json:"other"`
	Responsiveness            float64 `json:"responsiveness"`
	StartDate                 string  `json:"start_date"`
	TestEndpoint              string  `json:"test_endpoint"`
	UlBytesTransferred        int64   `json:"ul_bytes_transferred"`
	UlFlows                   int     `json:"ul_flows"`
	UlThroughput              int     `json:"ul_throughput"`
}

type Other struct {
	ECNValues     map[string]int `json:"ecn_values"`
	L4SEnablement map[string]int `json:"l4s_enablement"`
	ProtocolsSeen map[string]int `json:"protocols_seen"`
	ProxyState    map[string]int `json:"proxy_state"`
}

// CheckOnMac returns the result of running exec.Command("networkQuality", "-c")
// It will return nil, false, nil if the app is not running on macOS (aka Darwin)
// networkQuality can take many seconds or minutes to finish, so
// do not call this in an endless loop.
func CheckOnMac() (networkQuality *NetworkQuality, ok bool, err error) {
	if runtime.GOOS != "darwin" {
		return nil, false, nil
	}
	cmd := exec.Command("networkQuality", "-c")
	var out bytes.Buffer
	cmd.Stdout = &out

	err = cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
	jsonErr := json.Unmarshal(out.Bytes(), &networkQuality)
	if jsonErr != nil {
		return nil, false, jsonErr
	}
	return networkQuality, true, nil
}
