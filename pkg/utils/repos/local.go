package repos

import (
	"bufio"
	"fmt"
	"github.com/liturgiko/doxa/pkg/ages/ares"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"log"
	"os"
	"path/filepath"
	"strings"
)

// Ares2LpFromLocalDir  maps lines from ares files in GitHub repositories
// to LineParts
func Ares2LpFromLocalDir(rootDir string,
	fileSuffix string,
	printProgress bool) <-chan *ares.LineParts {
	out := make(chan *ares.LineParts)
	go func() {
		err := filepath.Walk(ltfile.ToSysPath(rootDir),
			func(path string, info os.FileInfo, err error) error {
				if err != nil {
					return err
				}
				if strings.HasSuffix(info.Name(), ".git") {
					doxlog.Info(fmt.Sprintf("Processing: %s", path[:len(path)-5]))
				}
				if !info.IsDir() && strings.HasSuffix(path, fileSuffix) {
					file, err := os.Open(ltfile.ToSysPath(path))
					if err != nil {
						log.Fatal(err)
					}
					defer file.Close()

					scanner := bufio.NewScanner(file)
					var fileNameParts ares.FilenameParts
					fileNameParts, err = ares.ParseAresFileName(file.Name())
					if err != nil {
						return fmt.Errorf("error getting file name parts from %s: %v", file.Name(), err)
					}
					if printProgress {
						fmt.Printf("\r%-80s", "")
						fmt.Print(fmt.Sprintf("\rProcessing %s_%s_%s_%s.ares", fileNameParts.Topic, fileNameParts.Language, fileNameParts.Country, fileNameParts.Realm))
					}
					var lineCnt int
					inCommentBlock := false

					for scanner.Scan() {

						lineCnt = lineCnt + 1
						line := strings.TrimSpace(scanner.Text())
						line = strings.TrimSpace(line)
						if strings.HasPrefix(line, "/*") {
							inCommentBlock = true
							continue
						}
						if strings.HasPrefix(line, "*/") || strings.HasSuffix(line, "*/") {
							inCommentBlock = false
							continue
						}
						if !inCommentBlock {
							lineParts, err := ares.ParseLine(fileNameParts, line)
							if err != nil {
								doxlog.Errorf("%s: %s: line %d", err, file.Name(), lineCnt)
								continue
							}
							lineParts.LineNbr = lineCnt
							if lineParts.IsAresId || lineParts.IsBlank || lineParts.IsCommentedOut {
								// ignore
							} else {
								out <- &lineParts
							}
						}
					}
				}
				return nil
			})
		if err != nil {
			doxlog.Errorf("%v", err)
		}
		close(out)
	}()
	return out
}
