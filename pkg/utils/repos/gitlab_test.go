package repos

import (
	"fmt"
	"github.com/xanzy/go-gitlab"
	"net/http"
	"os"
	"testing"
)

// Test getting all Gitlab users
func TestGitlabUsersList(t *testing.T) {
	url := "https://gitlab.com/mcolburn"
	items, err := GetUsers(url, os.Getenv("gitlabToken"))

	if err != nil {
		t.Error("error:", err)
	}
	if len(items) < 1 {
		t.Error("error: no items found")
	}
}

// Test getting all Gitlab projects
func TestGitlabProjectsList(t *testing.T) {
	url := "https://gitlab.com"
	items, err := GetProjects(url, os.Getenv("gitlabToken"))
	for _, item := range items {
		fmt.Printf("%v\n", item)
	}
	if err != nil {
		t.Error("error:", err)
	}
	if len(items) < 1 {
		t.Error("error: no items found")
	}
}

// Test getting all Gitlab projects
func TestGitlabProject(t *testing.T) {
	url := "https://gitlab.com"
	pid := "20666084"
	project, err := GetProject(url, pid, os.Getenv("gitlabToken"))

	if err != nil {
		t.Error("error:", err)
	}
	if project == nil {
		t.Error("error", fmt.Errorf("no project found for pid=%s at %s", pid, url))
	}
}
func TestAgain(t *testing.T) {
	url := "https://gitlab.com"
	git, err := gitlab.NewClient(os.Getenv("gitlabToken"), gitlab.WithBaseURL(url+"/api/v4"))
	opt := &gitlab.ListProjectsOptions{Search: gitlab.String("doxa")}
	projects, _, err := git.Projects.ListProjects(opt)
	if err != nil {
		t.Error(err)
	}
	for _, p := range projects {
		fmt.Printf("%v\n", p)
	}
}
func TestListSubgroups(t *testing.T) {
	url := "https://gitlab.com"
	git, err := gitlab.NewClient(os.Getenv("gitlabToken")+"1", gitlab.WithBaseURL(url+"/api/v4"))
	//opt := &gitlab.ListProjectsOptions{Visibility: gitlab.Visibility("all")}
	//opt := &gitlab.ListDescendantGroupsOptions{Search: gitlab.String("doxa")}
	items, res, err := git.Groups.ListDescendantGroups("doxa-mac", nil)
	if err != nil {
		t.Error(err)
	}
	if res.StatusCode != http.StatusOK {
		t.Error(fmt.Sprintf("expected http.StatusOK, got: %d - %s", res.StatusCode, res.Status))
	}
	for _, i := range items {
		fmt.Printf("%v\n", i)
	}
}
