package repos

import (
	"fmt"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/object"
	"os"
	"testing"
)

func TestClone(t *testing.T) {
	repos := []string{
		"https://gitlab.com/doxa-seraphimdedes/assets.git",
		//"https://gitlab.com/doxa-seraphimdedes/templates.git",
	}
	err := CloneConcurrently("/Users/mac002/tempt/doxa-mac-test/subscriptions", repos, false, false, 1)
	if err != nil {
		t.Error("error:", err)
	}
}
func TestGetFileContent(t *testing.T) {
	url := "https://gitlab.com/ocmc/liturgiko/doxa-catalog.git"
	filename := "catalog.json"
	c, err := GetFileContent(url, filename)
	if err != nil {
		t.Error(err)
	}
	fmt.Println(c)
}
func TestChanged(t *testing.T) {
	diff, err := Changed("/Users/mac002/doxa/repos/btx/en_uk_kjv")
	if err != nil {
		t.Error(err)
	}
	fmt.Println(diff.String())
}
func TestUpToDate(t *testing.T) {
	upToDate, err := UpToDate("/Users/mac002/doxa/repos/btx/en_uk_kjv")
	if err != nil {
		t.Error(err)
	}
	fmt.Println(upToDate)
}
func TestRemoteUrl(t *testing.T) {
	url, err := RemoteUrl("/Users/mac002/doxa/repos/btx/en_uk_kjv")
	if err != nil {
		t.Error(err)
	}
	if url == "" {
		t.Error(fmt.Errorf("url is empty"))
	}
}
func TestCreateRemote(t *testing.T) {
	err := CreateRemote("/Users/mac002/doxa/repos/ltx/gr_gr_cog")
	if err != nil {
		t.Error(err)
	}
}
func TestDirPath(t *testing.T) {
	home := "/Users/mac002/canBeRemoved/doxago/git"
	url := "https://github.com/liturgiko/testrepo1.git"
	p, err := DirPath(home, url)
	if err != nil {
		t.Error("error: ", err)
	}
	if p != "repos/liturgiko/testrepo1" {
		t.Error("error: invalid dir path", err)
	}
}
func TestCommit(t *testing.T) {
	home := os.Getenv("dir")
	url := os.Getenv("url")
	err := Commit(home, url, "test commit")
	if err != nil {
		t.Error(err)
	}
}
func TestPush(t *testing.T) {
	home := os.Getenv("dir")
	url := os.Getenv("url")
	err := Commit(home, url, "test commit")
	if err != nil {
		t.Error("commit error:", err)
	}
	err = Push(home, url, os.Getenv("usr"), os.Getenv("pwd"))
	if err != nil {
		t.Error("push error", err)
	}
}
func TestPull(t *testing.T) {
	home := os.Getenv("dir")
	url := os.Getenv("url")
	hash, err := Pull(home, url)
	if err != nil {
		t.Error("pull error", err)
	}
	fmt.Println(hash)
}

func TestFilesToProcess(t *testing.T) {
	home := os.Getenv("dir")
	url := os.Getenv("url")
	if home == "" || url == "" {
		t.Error("home or url environment variable not set")
	} else {
		dir, _ := DirPath(home, url)
		fmt.Println(FilesToProcess(dir))
	}
}
func TestInit(t *testing.T) {
	err := Init("/Users/mac002/doxa/sites/demo", "https://gitlab.com/ocmc/liml/test.git")
	if err != nil {
		fmt.Println(err)
	}
}

func TestCloneTo(t *testing.T) {
	tmpDir, err := os.MkdirTemp(os.TempDir(), "testRepo")
	srcUrl := "https://gitlab.com/doxa-liml/assets.git"
	shallow := true
	showProgress := false
	deleteFirst := false
	defer os.RemoveAll(tmpDir)
	if err != nil {
		t.Errorf("Failed to create temporary directory: %v", err)
		t.Fail()
		return
	}
	err = CloneTo(tmpDir, srcUrl, showProgress, deleteFirst, shallow)
	if err != nil {
		t.Errorf("CloneTo failed with: %v", err)
		t.Fail()
	}
	if shallow {
		//repoUrl, err := url.Parse(srcUrl)
		if err != nil {
			t.Errorf("could not parse url: %v", err)
			t.Fail()
			return
		}
		r, err := git.PlainOpen(tmpDir) //path.Join(tmpDir, strings.TrimSuffix(path.Base(repoUrl.Path), ".git")))
		if err != nil {
			t.Fatalf("couldn't open repo: %v", err)
		}

		ref, err := r.Head()
		if err != nil {
			t.Fatalf("cold not get head: %v", err)
		}

		cIter, err := r.Log(&git.LogOptions{From: ref.Hash()})
		if err != nil {
			t.Fatalf("could not git commit history: %v", err)
		}

		depth := 0
		err = cIter.ForEach(func(c *object.Commit) error {
			depth++
			return nil
		})
		if depth > 1 {
			t.Fail()
			t.Errorf("expected depth 1 but got depth %d", depth)
		}
	}
}
