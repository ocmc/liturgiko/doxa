package repos

import (
	"fmt"
	"testing"
)

func TestReposToFileLines(t *testing.T) {
	repos := []string{"https://gitlab.com/ocmc/test-data/testrepo1.git", "https://gitlab.com/ocmc/test-data/testrepo2.git"}
	nbrLinesExpected := 448
	linesReceived := 0
	for line := range Ares2LpFromGithub(repos, "ares", false) {
		// since Ares2LpFromGithub returns an unbuffered channel, we can't use its length.
		// Hence, the workaround below.
		if line.LineNbr > -1 {
			linesReceived++
		}
	}
	if linesReceived != nbrLinesExpected {
		t.Error(fmt.Sprintf("expected %d lines, received %d", nbrLinesExpected, linesReceived))
	}
}

func TestRepoNotFound(t *testing.T) {
	repos := []string{"https://gitlab.com/ocmc/test-data/testrepo3.git"}
	nbrLinesExpected := 0
	linesReceived := 0
	for line := range Ares2LpFromGithub(repos, "ares", false) {
		// since Ares2LpFromGithub returns an unbuffered channel, we can't use its length.
		// Hence, the workaround below.
		if line.LineNbr > -1 {
			linesReceived++
		}
	}
	if linesReceived != nbrLinesExpected {
		t.Error(fmt.Sprintf("expected %d lines, received %d", nbrLinesExpected, linesReceived))
	}
}

// Test in-memory clone of repo from Github.
func TestGitClone(t *testing.T) {
	repo := "https://gitlab.com/ocmc/test-data/testrepo1.git"
	err := ProcessRepoCommits(repo)
	if err != nil {
		t.Error("error:", err)
	}
}
