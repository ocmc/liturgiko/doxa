package repos

import (
	"fmt"
	"github.com/go-git/go-billy/v5/memfs"
	"github.com/go-git/go-billy/v5/osfs"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/config"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/cache"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/go-git/go-git/v5/plumbing/transport/http"
	"github.com/go-git/go-git/v5/storage/filesystem"
	"github.com/go-git/go-git/v5/storage/memory"
	"github.com/liturgiko/doxa/pkg/dvcs"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"io/ioutil"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"
)

// DirPath converts repository clone url into
// a path into which to clone the repository
func DirPath(parentDir, theUrl string) (string, error) {
	var err error
	Url, err := url.Parse(strings.TrimSpace(theUrl))
	if err != nil {
		return "", err
	}
	result := path.Join(parentDir, Url.Path)
	result = result[:len(result)-4]
	return result, err
}

// CloneRepos Clone sequentially each repository to the parent
// directory specified in path.
// The directory will be created by the Clone function.
// Be aware that the contents of the root directory will
// be deleted if it already exists.
// See func Clone to understand the parameters after urls.
func CloneRepos(path string, urls []string, deleteFirst, progress bool, depth int) error {
	_ = os.RemoveAll(path)
	err := os.MkdirAll(path, os.ModePerm)
	if err != nil {
		return err
	}
	for _, theUrl := range urls {
		u, err := Clone(path, theUrl, deleteFirst, progress, depth)
		fmt.Println(u)
		if err != nil {
			fmt.Println(err)
		}
	}
	return err
}

// CloneConcurrently Clone concurrently each repository to the parent
// directory specified in path.
// The directory will be created by the Clone function.
// Be aware that the contents of the root directory will
// be deleted if it already exists. This func runs nearly
// twice as fast as the CloneRepos func.
// See func Clone for parameters after urls.
func CloneConcurrently(path string, urls []string, deleteFirst, progress bool, depth int) error {
	_ = os.RemoveAll(path)
	err := os.MkdirAll(path, os.ModePerm)
	if err != nil {
		return err
	}
	done := make(chan string, len(urls))
	errch := make(chan error, len(urls))

	for _, theUrl := range urls {
		go func(path, url string) {
			u, err := Clone(path, url, false, false, depth)
			done <- u
			errch <- err
		}(path, theUrl)
	}
	for i := 0; i < len(urls); i++ {
		fmt.Println(<-done)
		if err := <-errch; err != nil {
			fmt.Println(err)
		}
	}
	return err
}

// Clone the repo into the dir.
// If deleteFirst == true, an existing repo dir will be deleted, then the clone will occur.
//
//	If you set deleteFirst = false, then the directory that the clone will create should not exist.
//
// If progress == true, the clone process will write to os.Stdout
// If depth > 0, a shallow clone will occur, only downloading depth-1 past commit histories.
//
//	Depending on how many prior commits and pushes have occurred, setting depth = 1 can
//	significantly reduce the time it takes to clone and the amount of local file storage used.
//
// Read git guidelines on when to use shallow clone.  Do not use if subsequent git operations
// require history.
func Clone(toDir string, repoUrl string, deleteFirst, progress bool, depth int) (string, error) {
	dirPath, err := DirPath(toDir, repoUrl)
	if err != nil {
		return repoUrl, err
	}
	if deleteFirst {
		err = os.RemoveAll(dirPath)
		if err != nil {
			return repoUrl, err
		}
	}
	opt := &git.CloneOptions{}
	opt.URL = repoUrl
	if progress {
		opt.Progress = os.Stdout
	}
	if depth > 0 {
		opt.Depth = depth
	}
	_, err = git.PlainClone(dirPath, false, opt)
	return dirPath, err
}
func GetFileContent(url string, filename string) ([]byte, error) {
	fs := memfs.New()
	_, err := git.Clone(memory.NewStorage(), fs, &git.CloneOptions{
		URL: url,
	})
	if err != nil {
		return nil, err
	}
	f, err := fs.Open(filename)
	if err != nil {
		return nil, err
	}
	content, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}
	return content, nil
}

// Init will initialize git in the dirPath and set the remote to the specified url.
// If the dirPath does not exist, it will be created.
func Init(dirPath string, url string) error {
	if !ltfile.DirExists(dirPath) {
		err := ltfile.CreateDirs(dirPath)
		if err != nil {
			return err
		}
	}
	fs := osfs.New(dirPath)
	dot, err := fs.Chroot(".git")
	if err != nil {
		return err
	}
	storage := filesystem.NewStorage(dot, cache.NewObjectLRUDefault())
	r, err := git.Init(storage, nil)
	if err != nil {
		return err
	}
	// AddPathToMap a new remote, with the default fetch refspec
	_, err = r.CreateRemote(&config.RemoteConfig{
		Name: "origin",
		URLs: []string{url},
	})
	r.CreateBranch(&config.Branch{
		Name:   "master",
		Remote: "origin",
	})
	return err
}
func CloneTo(dirPath string, url string, showProgress bool, deleteFirst bool, shallow bool) error {
	// depth 0 makes it clone full repo: all commits. Depth 1 is a "shallow clone" and only uses latest commit
	depth := 0
	if shallow {
		depth = 1
	}
	if deleteFirst {
		err := os.RemoveAll(dirPath)
		if err != nil {
			return err
		}
	}
	var err error
	if showProgress {
		_, err = git.PlainClone(dirPath, false, &git.CloneOptions{
			URL:      url,
			Progress: os.Stdout,
			Depth:    depth,
		})
	} else {
		_, err = git.PlainClone(dirPath, false, &git.CloneOptions{
			URL:   url,
			Depth: depth,
		})

	}
	return err
}

// CloneLiml clones the liml site into toDir and removes the .git folder, .gitignore, etc.
func CloneLiml(toDir string, verbose bool) error {
	theUrl := "https://gitlab.com/ocmc/liml/site.git"
	if verbose {
		fmt.Printf("Cloning liml site into %s", toDir)
	}
	err := CloneTo(toDir, theUrl, verbose, true, true)
	if err != nil {
		return err
	}
	_ = ltfile.DeleteDirRecursively(path.Join(toDir, ".git"))
	_ = ltfile.DeleteFile(path.Join(toDir, ".gitignore"))
	_ = ltfile.DeleteFile(path.Join(toDir, ".gitlab-ci.yml"))
	if verbose {
		fmt.Printf("Finished cloning demo")
	}
	return err
}

// Commit adds *, then commits, using the msg
func Commit(path, url, msg string) error {
	dirPath, err := DirPath(path, url)
	if err != nil {
		return err
	}
	r, err := git.PlainOpen(dirPath)
	if err != nil {
		return err
	}
	w, err := r.Worktree()
	if err != nil {
		return err
	}
	err = w.AddGlob("*")
	if err != nil {
		return err
	}
	_, err = w.Commit(msg, &git.CommitOptions{
		Author: &object.Signature{
			Name:  "doxa",
			Email: "olw@ocmc.org",
			When:  time.Now(),
		},
	})
	return err
}
func Pull(path, url string) (plumbing.Hash, error) {
	var hash plumbing.Hash
	dirPath, err := DirPath(path, url)
	if err != nil {
		return hash, err
	}
	r, err := git.PlainOpen(dirPath)
	if err != nil {
		return hash, err
	}
	// GetKeyPath the working directory for the repository
	w, err := r.Worktree()
	if err != nil {
		return hash, err
	}
	// Pull the latest changes from the origin remote and merge into the current branch
	err = w.Pull(&git.PullOptions{
		RemoteName: "origin",
		Force:      true,
	})
	if err != nil {
		return hash, err
	}
	// Print the latest commit that was just pulled
	ref, err := r.Head()
	if err != nil {
		return hash, err
	}
	commit, err := r.CommitObject(ref.Hash())
	if err != nil {
		return hash, err
	}
	return commit.Hash, err
}
func Push(path, url, username, password string) error {
	dirPath, err := DirPath(path, url)
	if err != nil {
		return err
	}
	r, err := git.PlainOpen(dirPath)
	if err != nil {
		return err
	}
	err = r.Push(&git.PushOptions{
		Auth: &http.BasicAuth{
			Username: username,
			Password: password,
		},
	})
	return err
}

// FilesToProcess add files of all types except for .git dir and its contents.
// TODO: add filter from gitignore file in root dir
func FilesToProcess(dirPath string) []string {
	var files []string
	filepath.Walk(ltfile.ToSysPath(dirPath),
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if strings.Contains(path, "/.git/") {
				return nil
			}
			if !info.IsDir() {
				files = append(files, info.Name())
			}
			return nil
		})
	return files
}
func UpToDate(path string) (bool, error) {
	r, err := git.PlainOpen(path)
	if err != nil {
		return false, err
	}
	err = r.Fetch(&git.FetchOptions{
		RemoteName: "origin",
	})
	if err != nil {
		msg := fmt.Sprintf("%s", err)
		if strings.Contains(msg, "up-to-date") {
			return true, nil
		}
	}
	return false, nil
}
func CreateRemote(path string) error {
	r, err := git.PlainOpen(path)
	if err != nil {
		return err
	}
	c := new(config.RemoteConfig)
	c.Name = "origin"
	url := fmt.Sprintf("https://gitlab.com/ocmc/liturgiko/doxa-resources/liturgical/gr_gr_cog.git")
	c.URLs = append(c.URLs, url)
	remote, err := r.CreateRemote(c)
	if err != nil {
		return err
	}
	if remote != nil {

	}
	return nil
}

// Changed compares the hash of master to that of origin and returns the changes.
// If no changes are found, it will return nil, nil
func Changed(path string) (object.Changes, error) {
	r, err := git.PlainOpen(path)
	if err != nil {
		return nil, err
	}
	_ = r.Fetch(&git.FetchOptions{
		RemoteName: "origin",
	})
	localRevHash, err := r.ResolveRevision("HEAD")
	if err != nil {
		return nil, err
	}
	originRevHash, err := r.ResolveRevision("refs/remotes/origin/master")
	if err != nil {
		return nil, err
	}
	if localRevHash.String() == originRevHash.String() {
		return nil, nil
	}
	originRevCommit, err := r.CommitObject(*originRevHash)
	if err != nil {
		return nil, err
	}
	localRevCommit, err := r.CommitObject(*localRevHash)
	if err != nil {
		return nil, err
	}
	t1, _ := originRevCommit.Tree() // parent.Tree()
	t2, _ := localRevCommit.Tree()
	return t1.Diff(t2)
}

type GitRepoMissingError struct {
	err  string
	path string
}

func (e *GitRepoMissingError) Error() string {
	return fmt.Sprintf("local git repo %s: %v", e.path, e.err)
}

type GitConfigMissingError struct {
	err  string
	path string
}

func (e *GitConfigMissingError) Error() string {
	return fmt.Sprintf("local git repo %s: %v", e.path, e.err)
}

type GitRemoteMissingError struct {
	err  string
	path string
}

func (e *GitRemoteMissingError) Error() string {
	return fmt.Sprintf("local git repo %s: %v", e.path, e.err)
}

type GitRemoteUrlMissingError struct {
	err  string
	path string
}

func (e *GitRemoteUrlMissingError) Error() string {
	return fmt.Sprintf("local git repo %s: %v", e.path, e.err)
}

// RemoteUrl reads the local repository specified by path and returns the first remote url for origin.
// If the path endpoint is not a git repository, an error is returned.
func RemoteUrl(path string) (string, error) {
	var remote = dvcs.DoxaOrigin
	var remoteUrl string
	r, err := git.PlainOpen(path)
	if err != nil {
		return "", &GitRepoMissingError{
			err:  "not a git repo",
			path: path,
		}
	}
	config, err := r.Config()
	if err != nil {
		return "", &GitConfigMissingError{
			err:  "config not found",
			path: path,
		}
	}
	if config != nil {
		if _, ok := config.Remotes[remote]; !ok {
			return "", &GitRemoteMissingError{
				err:  fmt.Sprintf("remote %s missing in config", remote),
				path: path,
			}
		}
		if len(config.Remotes[remote].URLs) == 0 {
			return "", &GitRemoteUrlMissingError{
				err:  fmt.Sprintf("no urls for remote %s in config", remote),
				path: path,
			}
		}
		remoteUrl = config.Remotes[remote].URLs[0]
	}
	return remoteUrl, nil
}

// UserAuthorized opens the specified local repository
// and attempts a remote fetch
// using the uid and pwd for authentication.
// If the path is bad, an error is returned.
// If the user is not authorized for the requested repository, false and nil are returned.
func UserAuthorized(path string, uid string, pwd string) (bool, error) {
	auth := new(http.BasicAuth)
	auth.Username = uid
	auth.Password = pwd
	repo, err := git.PlainOpen(path)
	if err != nil {
		return false, err
	}
	_ = repo.Fetch(&git.FetchOptions{
		Auth:       auth,
		RemoteName: "origin",
	})
	if err == nil {
		return true, nil
	}
	msg := fmt.Sprintf("%s", err)
	if strings.Contains(msg, "authentication") {
		return false, nil
	} else {
		return false, err
	}
}
