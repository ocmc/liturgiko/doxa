package repos

import (
	"context"
	"errors"
	"fmt"
	"github.com/google/go-github/github"
	"github.com/liturgiko/doxa/pkg/ages/ares"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"golang.org/x/oauth2"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
	"gopkg.in/src-d/go-git.v4/storage/memory"
	"log"
	"net/http"
	"net/url"
	"path"
	"strings"
)

// GetAllGithubRepos get properties for all the repositories for the supplied user name or org at github.
// Token: a GitHub token.
// When you get back the array of repositories, you can access the
// values of repo properties using an asterisk, e.g. *repo.Name to get the repository name.
func GetAllGithubRepos(name string, token string) ([]*github.Repository, error) {
	ctx := context.Background()
	ts := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: token},
	)
	tc := oauth2.NewClient(ctx, ts)

	client := github.NewClient(tc)
	opt := &github.RepositoryListByOrgOptions{
		ListOptions: github.ListOptions{PerPage: 10},
	}
	// get all pages of results
	var allRepos []*github.Repository
	for {
		repos, resp, err := client.Repositories.ListByOrg(ctx, name, opt)
		if err != nil {
			return allRepos, err
		}
		allRepos = append(allRepos, repos...)
		if resp.NextPage == 0 {
			break
		}
		opt.Page = resp.NextPage
	}
	return allRepos, nil
}

// Clone repo into memory and iterate over its commits.
// At this time it simply prints the commits.
// TODO: add code to process commits.

func ProcessRepoCommits(name string) error {
	r, err := git.Clone(memory.NewStorage(), nil, &git.CloneOptions{
		URL: name,
	})
	if err != nil {
		return err
	}
	ref, err := r.Head()
	if err != nil {
		return err
	}

	// ... retrieves the commit history
	cIter, err := r.Log(&git.LogOptions{From: ref.Hash()})
	if err != nil {
		return err
	}
	// ... iterates over the commits, printing them
	err = cIter.ForEach(func(c *object.Commit) error {
		fmt.Println(c)
		return nil
	})
	return err
}

// Ares2LpFromGithub maps lines from ares files in GitHub repositories
// to LineParts
func Ares2LpFromGithub(repos []string,
	fileSuffix string,
	printProgress bool) <-chan *ares.LineParts {
	out := make(chan *ares.LineParts)
	totalRepos := len(repos)
	reposProcessed := 0
	filesSeen := make(map[string]bool)
	go func() {
		for _, repo := range repos {
			reposProcessed++
			doxlog.Infof("Processing %d/%d: %s", reposProcessed, totalRepos, repo)
			// test to verify we have internet connectivity and the repo exists
			_, err := http.Get(repo)
			if err != nil {
				doxlog.Infof("No Internet connection, or bad url: %s, %s", repo, err)
				continue
			}
			// clone the repo into memory
			r, err := git.Clone(memory.NewStorage(), nil, &git.CloneOptions{
				URL:   repo,
				Depth: 1, // we don't need to clone the git history, just need the latest commit.
			})

			if err != nil {
				// try again because it might have been caused by a network delay (http request timeout)
				r, err = git.Clone(memory.NewStorage(), nil, &git.CloneOptions{
					URL: repo,
				})
				if err != nil {
					hostname := repo
					u, err := url.Parse(repo)
					if err == nil {
						hostname = u.Hostname()
					} else {
						log.Fatal(err)
					}
					doxlog.Infof("repo %s not found at %s. If it is no longer exists, remove the clone entry from your config.yaml file. If it does exist in %s, put the correct clone url in config.yaml", repo, hostname, hostname)
					continue
				}
			}

			ref, err := r.Head()
			if err != nil {
				if printProgress {
					doxlog.Infof("%v", err)
				}
				continue
			}

			// ... retrieving the commit object
			commit, err := r.CommitObject(ref.Hash())
			if err != nil {
				doxlog.Errorf("%v", err)
				continue
			}

			// ... retrieve the tree from the commit
			tree, err := commit.Tree()
			if err != nil {
				doxlog.Errorf("%v", err)
				continue
			}
			// ... get the files iterator and print the file
			err = tree.Files().ForEach(func(f *object.File) error {
				if strings.HasSuffix(f.Name, fileSuffix) {
					if filesSeen[f.Name] {
						msg := fmt.Sprintf("duplicate ares file %s", f.Name)
						doxlog.Error(msg)
						return fmt.Errorf(msg)
					} else { // only process a file we have not seen yet
						filesSeen[f.Name] = true
						var fileNameParts ares.FilenameParts
						fileNameParts, err := ares.ParseAresFileName(f.Name)
						if err == nil {
							if printProgress {
								doxlog.Info(fmt.Sprintf("Processing %s_%s_%s_%s.ares", fileNameParts.Topic, fileNameParts.Language, fileNameParts.Country, fileNameParts.Realm))
							}
						}
						var lineCnt int
						inCommentBlock := false

						if err == nil {
							l, _ := f.Lines()
							for _, line := range l {
								lineCnt = lineCnt + 1
								line = strings.TrimSpace(line)
								if strings.HasPrefix(line, "/*") {
									inCommentBlock = true
									continue
								}
								if strings.HasPrefix(line, "*/") || strings.HasSuffix(line, "*/") {
									inCommentBlock = false
									continue
								}
								if !inCommentBlock {
									lineParts, err := ares.ParseLine(fileNameParts, line)
									lineParts.Source = fmt.Sprintf("%s", f.Name)
									base := path.Base("/" + f.Name)
									if err != nil {
										switch fmt.Sprintf("%s", err) {
										case ares.StrErrLineMissingEqualSign:
											err = errors.New(fmt.Sprintf("Missing equal sign: %s line: %d", base, lineCnt))
										case ares.StrErrValueMissingQuote:
											err = errors.New(fmt.Sprintf("Value Missing quote: %s line: %d", base, lineCnt))
										default:
											err = errors.New(fmt.Sprintf("%v: %s line: %d", err, base, lineCnt))
										}
										doxlog.Warnf("%v", err)
										continue
									}
									lineParts.LineNbr = lineCnt
									if lineParts.IsAresId || lineParts.IsBlank || lineParts.IsCommentedOut {
										// ignore
									} else {
										out <- &lineParts
									}
								}
							}
						}
					}
				}
				return err
			})
		}
		close(out)
	}()
	return out
}

func Tsv2LinesFromGitlab(repos []string,
	fileSuffix string,
	printProgress bool) <-chan string {
	out := make(chan string)

	totalRepos := len(repos)
	reposProcessed := 0
	go func(theRepos []string) {
		for _, repo := range theRepos {
			reposProcessed++
			doxlog.Infof("Processing %d/%d: %s", reposProcessed, totalRepos, repo)
			// test to verify we have internet connectivity and the repo exists
			_, err := http.Get(repo)
			if err != nil {
				doxlog.Infof("No Internet connection, or bad url: %s, %s", repo, err)
				continue
			}
			// clone the repo into memory
			r, err := git.Clone(memory.NewStorage(), nil, &git.CloneOptions{
				URL:   repo,
				Depth: 1, // we don't need to clone the git history, just need the latest commit.
			})

			if err != nil {
				// try again because it might have been caused by a network delay (http request timeout)
				r, err = git.Clone(memory.NewStorage(), nil, &git.CloneOptions{
					URL: repo,
				})
				if err != nil {
					hostname := repo
					u, err := url.Parse(repo)
					if err == nil {
						hostname = u.Hostname()
					} else {
						log.Fatal(err)
					}
					doxlog.Infof("repo %s not found at %s. If it is no longer exists, remove the clone entry from your config.yaml file. If it does exist in %s, put the correct clone url in config.yaml", repo, hostname, hostname)
					continue
				}
			}

			ref, err := r.Head()
			if err != nil {
				if printProgress {
					doxlog.Infof("%v", err)
				}
				continue
			}

			// ... retrieving the commit object
			commit, err := r.CommitObject(ref.Hash())
			if err != nil {
				doxlog.Errorf("%v", err)
				continue
			}

			// ... retrieve the tree from the commit
			tree, err := commit.Tree()
			if err != nil {
				doxlog.Errorf("%v", err)
				continue
			}
			// ... get the files iterator and print the file
			err = tree.Files().ForEach(func(f *object.File) error {
				if strings.HasSuffix(f.Name, fileSuffix) {
					var lineCnt int
					l, _ := f.Lines()
					for _, line := range l {
						lineCnt = lineCnt + 1
						out <- line
					}
				}
				return err
			})
		}
		close(out)
	}(repos)
	return out
}
