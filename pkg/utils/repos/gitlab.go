package repos

import (
	"encoding/json"
	"fmt"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/xanzy/go-gitlab"
	"io"
	"log"
	"net/http"
	"time"
)

const GetUserApiUrl = "https://gitlab.com/api/v4/user"
const GetUserGroupsApiUrl = "https://gitlab.com/api/v4/groups?membership=true&min_access_level=30"
const GetUserProjectsApiUrl = "https://gitlab.com/api/v4/projects?membership=true&min_access_level=30"

/**
// GitLab API docs: https://docs.gitlab.com/ee/user/permissions.html
type AccessLevelValue int

// List of available access levels
//
// GitLab API docs: https://docs.gitlab.com/ee/user/permissions.html
const (
	NoPermissions            AccessLevelValue = 0
	MinimalAccessPermissions AccessLevelValue = 5
	GuestPermissions         AccessLevelValue = 10
	ReporterPermissions      AccessLevelValue = 20
	DeveloperPermissions     AccessLevelValue = 30
	MaintainerPermissions    AccessLevelValue = 40
	OwnerPermissions         AccessLevelValue = 50
	AdminPermissions         AccessLevelValue = 60

	// Deprecated: Renamed to MaintainerPermissions in GitLab 11.0.
	MasterPermissions AccessLevelValue = 40
	// Deprecated: Renamed to OwnerPermissions.
	OwnerPermission AccessLevelValue = 50
)
*/

type BasicProject struct {
	ID                   int `json:"id"`
	ApprovalsBeforeMerge int
	CreatedAt            *time.Time `json:"created_at"`
	DefaultBranch        string     `json:"default_branch"`
	Description          string     `json:"description"`
	DoxaPush             bool
	DoxaPull             bool
	DoxaLastPull         *time.Time
	DoxaLastPush         *time.Time
	HTTPURLToRepo        string                 `json:"http_url_to_repo"`
	IssuesEnabled        bool                   `json:"issues_enabled"`
	MergeRequestsEnabled bool                   `json:"merge_requests_enabled"`
	Name                 string                 `json:"name"`
	NameWithNamespace    string                 `json:"name_with_namespace"`
	OpenIssuesCount      int                    `json:"open_issues_count"`
	Owner                *gitlab.User           `json:"owner"`
	Path                 string                 `json:"path"`
	PathWithNamespace    string                 `json:"path_with_namespace"`
	Public               bool                   `json:"public"`
	SSHURLToRepo         string                 `json:"ssh_url_to_repo"`
	Visibility           gitlab.VisibilityValue `json:"visibility"`
}

// Get fetches response body from GitLab using the provided private token
// and gitlab api url.  Use the above url constants.
func Get(token, url string) ([]byte, error) {
	// Create a new HTTP client
	client := &http.Client{}

	// Create the request
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to make request: %v", err)
	}

	// Add the private token to the request header
	req.Header.Add("PRIVATE-TOKEN", token)

	// Perform the request
	resp, err := client.Do(req)
	if err != nil {
		msg := fmt.Sprintf("request failed: %v", err)
		doxlog.Error(msg)
		return nil, fmt.Errorf(msg)
	}
	defer func(Body io.ReadCloser) {
		err = Body.Close()
		if err != nil {
			doxlog.Errorf(err.Error())
		}
	}(resp.Body)

	// Read the response body
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read response body: %v", err)
	}

	// Check for non-200 status code
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("API request failed with status %d: %s", resp.StatusCode, string(body))
	}

	return body, nil
}

// GetUser fetches user information from GitLab using the provided private token
func GetUser(token string) (*gitlab.User, error) {
	// Read the response body
	body, err := Get(token, GetUserApiUrl)
	if err != nil {
		return nil, fmt.Errorf("GetUser error: %v", err)
	}

	// Decode the JSON response into the User struct
	var user gitlab.User
	if err = json.Unmarshal(body, &user); err != nil {
		return nil, fmt.Errorf("failed to unmarshal response: %v", err)
	}

	// Return the user information
	return &user, nil
}
func GetGroupsForUser(token string) ([]*gitlab.Group, error) {
	// Read the response body
	body, err := Get(token, GetUserProjectsApiUrl)
	if err != nil {
		return nil, fmt.Errorf("GetUser error: %v", err)
	}

	// Decode the JSON response into the struct
	var groups []*gitlab.Group
	if err = json.Unmarshal(body, &groups); err != nil {
		return nil, fmt.Errorf("failed to unmarshal response: %v", err)
	}
	// Return the groups information
	return groups, nil
}
func GetProjectsForUser(token string) ([]*gitlab.Project, error) {
	// Read the response body
	body, err := Get(token, GetUserProjectsApiUrl)
	if err != nil {
		return nil, fmt.Errorf("GetUser error: %v", err)
	}

	// Decode the JSON response into the struct
	var projects []*gitlab.Project
	if err = json.Unmarshal(body, &projects); err != nil {
		return nil, fmt.Errorf("failed to unmarshal response: %v", err)
	}
	// Return the projects information
	return projects, nil
}

// GetUsers returns a list of the Gitlab user accounts
// But, the interface requires pagination.
// So, TODO: append projects and return the complete list.
func GetUsers(url string, token string) ([]*gitlab.User, error) {
	git, err := gitlab.NewClient(token, gitlab.WithBaseURL(url+"/api/v4"))
	if err != nil {
		doxlog.Errorf("%v", err)
	}
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}
	users, _, err := git.Users.ListUsers(&gitlab.ListUsersOptions{})

	opt := &gitlab.ListUsersOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: 10,
			Page:    1,
		},
	}
	for {
		// GetKeyPath the first page with projects.
		items, resp, err := git.Users.ListUsers(opt)
		if err != nil {
			log.Fatal(err)
		}

		// Append the users we've found so far.
		for _, item := range items {
			users = append(users, item)
		}

		// Exit the loop when we've seen all pages.
		if resp.CurrentPage >= resp.TotalPages {
			break
		}

		// Update the page number to get the next page.
		opt.Page = resp.NextPage
	}
	return users, err
}

// GetProjects Should return an array of all projects.
// But, the interface requires pagination.
func GetProjects(url string, token string) ([]*gitlab.Project, error) {
	git, err := gitlab.NewClient(token, gitlab.WithBaseURL(url+"/api/v4"))

	if err != nil {
		doxlog.Errorf("%v", err)
	}
	opt := &gitlab.ListProjectsOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: 10,
			Page:    1,
		},
	}
	var projects []*gitlab.Project

	for {
		// GetKeyPath the first page with projects.
		items, resp, err := git.Projects.ListProjects(opt)
		if err != nil {
			log.Fatal(err)
		}

		// List all the projects we've found so far.
		for _, item := range items {
			projects = append(projects, item)
		}

		// Exit the loop when we've seen all pages.
		if resp.CurrentPage >= resp.TotalPages {
			break
		}

		// Update the page number to get the next page.
		opt.Page = resp.NextPage
	}
	return projects, err
}
func GetProject(url string, pid string, token string) (*gitlab.Project, error) {
	git, err := gitlab.NewClient(token, gitlab.WithBaseURL(url+"/api/v4"))

	if err != nil {
		doxlog.Errorf("%v", err)
	}
	opt := new(gitlab.GetProjectOptions)
	stats := true
	opt.Statistics = &stats
	project, resp, err := git.Projects.GetProject(pid, opt)
	fmt.Println(resp)
	return project, err
}
