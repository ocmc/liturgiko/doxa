// Package dlog provides a means for all doxa code
// to share the same file logger simply by importing this package.
// init() opens a file, but does not defer it's closing.
// Instead, main.go creates the function to defer the closing
// of the log file.  Otherwise, after init() executes it closes the file.
// Use log.Println, etc. if just displaying INFO to standard out.
// Use dlog.Info to both display info to standard out, and write to log file.
// Always use dlog.Error and dlog.Warn to report an error or warn the user.
// This package provides formatted variants of dlog.Info, etc., e.g. dlog.Infof.
// This package uses the rolling logs code from https://github.com/goinggo/tracelog/tree/master,
// used under a BSD-style license.  See the license there for details.
// IMPORTANT:  if you make changes to MetaError,
// you need to also make changes to the following:
//
//	func AddMetaData()
//	pkg/server/handlers.go, func handleLogRequest()
//	static/wc/log-viewer.js

package doxlog

import (
	"encoding/json"
	"fmt"
	"github.com/liturgiko/doxa/pkg/enums/goos"
	"github.com/liturgiko/doxa/pkg/utils/clinput"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/spf13/viper"
	"log"
	"log/slog"
	"os"
	"path"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"
)

var (
	Level       *slog.LevelVar
	fileLogger  *slog.Logger
	mu          sync.Mutex
	logDir      string
	fileName    string
	DaysToKeep  int = 7
	LogFile     *os.File
	WriteCliOut bool
	Skip        int // levels in call stack to skip
	MockLog     bool
)

const SkipDefault = 3

type logType int

const (
	rptInfo logType = iota
	rptWarning
	rptError
	rptDebug
	rptPanic
	rptStatus
	rptStatusWarn
)

type MetaError struct {
	Time     string `json:"time"`
	Level    string `json:"level"`
	Msg      string `json:"msg"`
	Function string `json:"function"`
	File     string `json:"file"`
	Line     int    `json:"line"`
}

const MaxLastErrors = 10

var LastErrors []MetaError

func AddMetaError(m MetaError) {
	if len(LastErrors) >= MaxLastErrors {
		LastErrors = LastErrors[1:]
	}
	LastErrors = append(LastErrors, m)
}

var Config = new(ConfigMeta)

type ConfigMeta struct {
	Home           string
	Cloud          bool
	Debug          bool
	Dev            bool
	Project        string
	SubscribeToFrS bool
}

func Init(altPath, altProject string, mockLog bool) {
	if mockLog {
		MockLog = true
		fileLogger = slog.New(slog.NewTextHandler(os.Stdout, nil))
		return
	}
	WriteCliOut = true
	err := initConfig(altPath, altProject)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	Skip = SkipDefault
	currentDate := time.Now()
	dateDirectory := time.Now().Format("2006-01-02")
	dateFile := currentDate.Format("2006-01-02-15-04-05")
	doxaLogDir := path.Join(Config.Home, "logs")
	logDir = path.Join(doxaLogDir, dateDirectory)
	err = ltfile.CreateDirs(logDir)
	if err != nil {
		log.Fatalf("main : Start : Failed to Create log directory : %s : %s\n", logDir, err)
	}
	fileName = strings.Replace(fmt.Sprintf("%s.log", dateFile), " ", "-", -1)
	fileName = path.Join(logDir, fileName)
	LogFile, err = ltfile.Create(fileName)
	if err != nil {
		log.Fatalf("main : Start : Failed to Create log file : %s : %s\n", fileName, err)
	}
	Level = new(slog.LevelVar)
	Level.Set(slog.LevelInfo)
	fileLogger = slog.New(slog.NewJSONHandler(LogFile, &slog.HandlerOptions{Level: Level}))
	log.SetFlags(0)
	// Cleanup any existing directories
	logDirectoryCleanup(logDir, DaysToKeep)
}

// initConfig populates var Config by
// reading information from $HOME/.doxa/config.yaml
// If .doxa does not exist or .doxa/config.yaml does not exist,
// the user will be prompted for information to create it.
// altHome, altProject if not empty, where passed as
// args to the main program by the user. They override the config values.
func initConfig(altHome, altProject string) (err error) {
	fmt.Println("running doxlog.initConfig")
	// config.yaml property keys
	keyHome := "doxahome"
	keyProject := "project"
	keyDev := "dev"
	keyCloud := "cloud"
	keyDebug := "debug"
	keySubscribe := "subscribe"
	ConfigFileName := "config"
	var userHome string
	var ConfigPath string
	runtimeOs := goos.CodeForString(runtime.GOOS)
	if runtimeOs == goos.Linux {
		fmt.Println("linux os detected")
		userHome = "/var/local"
		ConfigPath = "/usr/local/etc/doxa"
		fmt.Printf("linux doxa home in %s\n", userHome)
	} else {
		userHome, err = os.UserHomeDir()
		if err != nil {
			fmt.Printf("unable to determine user home directory: %v\n", err)
			os.Exit(1)
		}
		ConfigPath = path.Join(userHome, ".doxa")
	}
	fmt.Printf("checking to see if %s exists\n", ConfigPath)
	if !ltfile.DirExists(ConfigPath) {
		fmt.Printf("%s does not exist\n", ConfigPath)
		fmt.Printf("creating doxa config directory %s\n", ConfigPath)
		err = ltfile.CreateDirs(ConfigPath)
		if err != nil {
			fmt.Printf("unable to create doxa config directory: %v\n", err)
			os.Exit(1)
		}
	} else {
		fmt.Printf("ok, %s exists\n", ConfigPath)
	}
	cf := path.Join(ConfigPath, fmt.Sprintf("%s.yaml", ConfigFileName))
	if !ltfile.FileExists(cf) {
		fmt.Printf("runtime os is %s\n", runtimeOs)

		// ask the user which directory to use for the workspace
		var rootDir string
		if runtimeOs == goos.Linux {
			rootDir = userHome
		} else {
			rootDir, err = getInstallDir()
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
		}
		Config.Home = path.Join(rootDir, "doxa")
		// ask use for the initial project name:
		fmt.Print("Enter a short name or abbreviation for your first project: ")
		_, err = fmt.Scanln(&Config.Project)
		if err != nil {
			fmt.Println("Error:", err)
			return
		}
		fmt.Printf("Prject name is %s\n", Config.Project)

		configSettings := []string{fmt.Sprintf("%s: %s", keyHome, Config.Home),
			fmt.Sprintf("%s: %v", keyCloud, runtimeOs == goos.Linux),
			fmt.Sprintf("%s: false", keyDev),
			fmt.Sprintf("%s: false", keyDebug),
			fmt.Sprintf("%s: %s", keyProject, Config.Project)}
		err = ltfile.WriteLinesToFile(cf, configSettings)
		if err != nil {
			msg := fmt.Sprintf("error creating config file %s: %v\n", Config.Home, err)
			return fmt.Errorf(msg)
		}
	}
	// now we are certain we have a config file.
	fmt.Printf("Your doxa config file is at %s\n", cf)
	viper.AddConfigPath(ConfigPath)
	viper.SetConfigName(ConfigFileName)
	viper.SetConfigType("yaml")

	if err = viper.ReadInConfig(); err != nil {
		return fmt.Errorf("error reading config.yaml from %s: %v", ConfigPath, err)
	}

	if len(altHome) > 0 {
		Config.Home = altHome
	} else {
		Config.Home = strings.TrimSpace(viper.GetString(keyHome))
		if len(Config.Home) == 0 {
			fmt.Println("./doxa/config.yaml is missing path to doxahome.")
			os.Exit(1)
		}
	}
	Config.Cloud = viper.GetBool(keyCloud)
	Config.Debug = viper.GetBool(keyDebug)
	Config.Dev = viper.GetBool(keyDev)
	Config.SubscribeToFrS = viper.GetBool(keySubscribe)
	if len(altProject) > 0 {
		Config.Project = altProject
	} else {
		Config.Project = strings.TrimSpace(viper.GetString(keyProject))
		if len(Config.Project) == 0 {
			fmt.Printf("config project undefined\n")
			os.Exit(1)
		}
	}
	// make sure doxa home exists
	if !ltfile.DirExists(Config.Home) {
		fmt.Printf("creating doxa home dir %s\n", Config.Home)
		err = ltfile.CreateDirs(Config.Home)
		if err != nil {
			fmt.Printf("fatal error creating directories %s: %v\n", Config.Home, err)
			os.Exit(1)
		}
	}
	return nil
}

// getDirOptions lists the user home directory and any mounted volumes
// and prompts the user to select one for the setup of the doxa home directory.
func getDirOptions(dirPath string) (options []string, err error) {
	dirs, err := ltfile.DirsInDir(dirPath, true)
	if err != nil {
		return nil, err
	}
	var userHomeDir string
	userHomeDir, err = os.UserHomeDir()
	if err != nil {
		return nil, err
	}
	options = append(options, fmt.Sprintf("%d", 1))
	options = append(options, userHomeDir)
	for i, d := range dirs {
		optionPath := path.Join(dirPath, d)
		options = append(options, fmt.Sprintf("%d", i+2))
		options = append(options, fmt.Sprintf("%s", optionPath))
	}
	options = append(options, "q")
	options = append(options, "quit")
	return options, nil
}
func getInstallDir() (installDir string, err error) {
	GoOS := goos.CodeForString(runtime.GOOS)
	if GoOS == goos.Linux {
		return "/var/local", nil
	}
	q := "Select a directory for your doxa workspace:"
	root := ltfile.OsVolumesDir()
	o, err := getDirOptions(root)
	if err != nil {
		return "", err
	}
	selected := string(clinput.GetInput(q, o))
	if selected == "q" {
		fmt.Println("Exiting doxa...")
		os.Exit(0)
	}
	var selectedInt int
	selectedInt, err = strconv.Atoi(selected)
	if err != nil {
		return "", err
	}
	selectedInt = (selectedInt * 2) - 1
	if selectedInt > len(o) {
		return "", fmt.Errorf("error selection > len(options)")
	}
	return o[selectedInt], nil
}
func LogDir() string {
	return logDir
}
func LogFileName() string {
	return fileName
}
func Error(msg string) {
	report(rptError, msg)
}
func Errorf(format string, a ...interface{}) {
	report(rptError, fmt.Sprintf(format, a...))
}
func Panic(msg string) {
	report(rptPanic, msg)
}
func Panicf(format string, a ...interface{}) {
	report(rptPanic, fmt.Sprintf(format, a...))
}

func Info(msg string) {
	report(rptInfo, msg)
}
func Infof(format string, a ...interface{}) {
	report(rptInfo, fmt.Sprintf(format, a...))
}
func Debug(msg string) {
	if Level.Level() != slog.LevelDebug {
		return
	}
	report(rptDebug, msg)
}
func Debugf(format string, a ...interface{}) {
	if Level.Level() != slog.LevelDebug {
		return
	}
	report(rptDebug, fmt.Sprintf(format, a...))
}
func Status(msg string) {
	report(rptStatus, msg)
}
func Statusf(format string, a ...interface{}) {
	report(rptStatus, fmt.Sprintf(format, a...))
}
func StatusWarn(msg string) {
	report(rptStatusWarn, msg)
}
func StatusWarnf(format string, a ...interface{}) {
	report(rptStatusWarn, fmt.Sprintf(format, a...))
}
func Warn(msg string) {
	report(rptWarning, msg)
}
func Warnf(format string, a ...interface{}) {
	report(rptWarning, fmt.Sprintf(format, a...))
}

func report(lType logType, msg string) {
	if fileLogger == nil {
		fmt.Printf("doxlog called without being initialized properly (fileLogger is nil)")
		return
	}
	mu.Lock()
	defer mu.Unlock()
	var prefix, prefixedMsg, function, file string
	var line int
	var ok bool
	msg = strings.ReplaceAll(msg, "\n", "")
	msg = strings.TrimSpace(msg)
	function, file, line, ok = GetCaller()

	switch lType {
	case rptError:
		prefix = "ERROR "
		if ok {
			fileLogger.Error(msg, slog.String("function", function), slog.String("file", file), slog.Int("line", line))
		} else {
			fileLogger.Error(msg)
		}
	case rptPanic:
		prefix = "PANIC "
		if ok {
			fileLogger.Error(msg, slog.String("function", function), slog.String("file", file), slog.Int("line", line))
			AddMetaError(MetaError{"", prefix, msg, function, file, line})
		} else {
			fileLogger.Error(msg)
		}
	case rptDebug:
		prefix = "DEBUG "
		if ok {
			fileLogger.Debug(msg, slog.String("function", function), slog.String("file", file), slog.Int("line", line))
		} else {
			fileLogger.Debug(msg)
		}
	case rptInfo, rptStatus:
		prefix = "INFO "
		fileLogger.Info(msg)
	case rptStatusWarn:
		prefix = "WARN "
		fileLogger.Info(msg)
	case rptWarning:
		prefix = "WARN "
		if ok {
			fileLogger.Warn(msg, slog.String("function", function), slog.String("file", file), slog.Int("line", line))
		} else {
			fileLogger.Warn(msg)
		}
	}
	// write to standard out
	if ok && (lType == rptError || lType == rptPanic || lType == rptWarning) {
		prefixedMsg = fmt.Sprintf("%sfunc %s: %s:%d %s", prefix, function, file, line, msg)
	} else {
		prefixedMsg = fmt.Sprintf("%s %s", prefix, msg)
	}
	if WriteCliOut {
		fmt.Println(prefixedMsg) // log does not work in the doxa cli, so we use fmt
	}
	if lType == rptPanic {
		panic(1)
	}
}

// GetCaller provides the name of the original calling function, file, and line.
// We do this ourselves rather than using slog.HandlerOptions{AddSource: true}
// because it does not provide a way to indicate how many caller levels back
// to get the info for.  Because this package wraps slog.Error, etc.,
// if we do not write our own function, the caller will appear to be
// the Error wrapper, etc., in this package.
func GetCaller() (function, file string, line int, ok bool) {
	// For the sake of log, we need to get
	// the caller file and line number.
	// We set skip to default of 3 to show the real caller's info.
	// Otherwise, log will show this function, as the caller.
	var pc uintptr
	if MockLog {
		pc, file, line, ok = runtime.Caller(Skip + 3)
	} else {
		pc, file, line, ok = runtime.Caller(Skip)
	}
	if ok {
		i := strings.Index(file, "doxa")
		if i > -1 {
			file = file[i+5:]
		}
		f := runtime.FuncForPC(pc)
		function = f.Name()
		if len(function) > 0 {
			parts := strings.Split(function, ".")
			if len(parts) > 1 {
				function = parts[len(parts)-1]
			}
		}
	}
	return function, file, line, ok
}

// logDirectoryCleanup removes directories older than daysToKeep
func logDirectoryCleanup(baseFilePath string, daysToKeep int) {
	// Get a list of existing directories.
	fileInfos, err := os.ReadDir(baseFilePath)
	if err != nil {
		Error(fmt.Sprintf("%s", err))
		return
	}

	// Create the date to compare for directories to remove.
	currentDate := time.Now().UTC()
	compareDate := time.Date(currentDate.Year(), currentDate.Month(), currentDate.Day()-daysToKeep, 0, 0, 0, 0, time.UTC)

	for _, fileInfo := range fileInfos {
		if fileInfo.IsDir() == false {
			continue
		}

		// The directory names look like: YYYY-MM-DD
		var year, month, day int
		parts := strings.Split(fileInfo.Name(), "-")
		if len(parts) == 3 {
			year, err = strconv.Atoi(parts[0])
			if err != nil {
				Error(fmt.Sprintf("Attempting To Convert Directory [%s] :%s", fileInfo.Name(), err))
				continue
			}

			month, err = strconv.Atoi(parts[1])
			if err != nil {
				Error(fmt.Sprintf("Attempting To Convert Directory [%s] :%s", fileInfo.Name(), err))
				continue
			}

			day, err = strconv.Atoi(parts[2])
			if err != nil {
				Error(fmt.Sprintf("Attempting To Convert Directory [%s] :%s", fileInfo.Name(), err))
				continue
			}
		}

		// The directory to check.
		fullFileName := path.Join(baseFilePath, fileInfo.Name())

		// Create a time type from the directory name.
		directoryDate := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC)

		// Compare the dates and convert to days.
		daysOld := int(compareDate.Sub(directoryDate).Hours() / 24)

		if daysOld >= 0 {
			err = os.RemoveAll(fullFileName)
			if err != nil {
				Error(fmt.Sprintf("Attempting To Convert Directory [%s] :%s", fileInfo.Name(), err))
				continue
			}
		}
	}
	return
}
func LogEntries() (entries []*MetaError, err error) {
	var lines []string
	lines, err = ltfile.GetFileLines(LogFile.Name())
	if err != nil {
		msg := fmt.Sprintf("error reading lines from %s: %v", LogFile.Name(), err)
		Error(msg)
		return nil, fmt.Errorf(msg)
	}
	for _, l := range lines {
		if len(l) == 0 {
			continue
		}
		m := new(MetaError)
		err = json.Unmarshal([]byte(l), m)
		if err != nil {
			msg := fmt.Sprintf("error unmarshalling %s: %v", l, err)
			Error(msg)
			return nil, fmt.Errorf(msg)
		}
		entries = append(entries, m)
	}
	return entries, nil
}
