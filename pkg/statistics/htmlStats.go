package statistics

type HtmlStats struct {
	sitePath string
}

func NewHtmlAnalyzer(sitePath string) *HtmlStats {
	return &HtmlStats{
		sitePath: sitePath,
	}
}
func (a *HtmlStats) Analyze() error {
	return nil
}
