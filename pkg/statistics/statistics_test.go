package statistics

import (
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"testing"
)

func TestAnalyzer_Analyze(t *testing.T) {
	pathOut := "/Volumes/macWdb/parse"
	dbPath := "/Volumes/macWdb/parse/liturgical.db"
	ds, err := kvs.NewBoltKVS(dbPath)
	if err != nil {
		t.Error(err)
		return
	}
	var mapper *kvs.KVS
	mapper, err = kvs.NewKVS(ds)
	if err != nil {
		t.Errorf("error creating mapper: %v\n", err)
		return
	}
	if mapper == nil {
		t.Errorf("mapper is nil")
		return
	}
	analyzer := NewAnalyzer(mapper, pathOut)
	var stats *Statistics
	stats, err = analyzer.ParseAllGreek()
	if err != nil {
		t.Errorf(err.Error())
	}
	if stats.CountTokens == 0 {
		t.Errorf("no tokens")
	}
	fmt.Printf("%d records", stats.CountRecords)
	fmt.Printf("%d redirects", stats.CountRedirects)
	fmt.Printf("%d tokens", stats.CountTokens)
	fmt.Printf("%d inflected forms", stats.CountInflectedForms)
	fmt.Printf("%d lemmas", stats.CountLemmas)
}
func TestHtmlStats_Analyze(t *testing.T) {
	sitePath := "/Volumes/macWdb/doxa/projects/doxa-liml/websites/public"
	a := NewHtmlAnalyzer(sitePath)
	if err := a.Analyze(); err != nil {
		t.Errorf("%s", err)
		return
	}
}
