package statistics

import (
	"encoding/json"
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/nlp"
	"github.com/liturgiko/doxa/pkg/nlp/ud"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"path"
	"sort"
	"strings"
	"unicode"
)

type Analyzer struct {
	m         *kvs.KVS
	PathOut   string
	Serialize bool
}
type Statistics struct {
	DbPath                string
	CountEmptyRecords     int
	CountInflectedForms   int
	CountLemmas           int
	CountRecords          int
	CountRecordsWithValue int
	CountRedirects        int
	CountTokens           int
	UniqueTokens          map[string]int
	Lemmas                map[string]int
	RedirectsMap          map[string]int
	ErrorMap              map[string]string
	SortedTokenKeys       []string
}

func NewStatistics(dbPath string) *Statistics {
	stats := new(Statistics)
	stats.DbPath = dbPath
	stats.UniqueTokens = make(map[string]int)
	stats.Lemmas = make(map[string]int)
	stats.RedirectsMap = make(map[string]int)
	stats.ErrorMap = make(map[string]string)
	return stats
}
func (s *Statistics) Add(stats *Statistics) {
	s.CountEmptyRecords += stats.CountEmptyRecords
	s.CountLemmas += stats.CountLemmas
	s.CountRecords += stats.CountRecords
	s.CountRedirects = stats.CountRedirects
	s.CountTokens = stats.CountTokens
	s.CountInflectedForms += stats.CountInflectedForms
	for k, v := range stats.UniqueTokens {
		s.UniqueTokens[k] += v
	}
	for k, v := range stats.Lemmas {
		s.Lemmas[k] += v
	}
	for k, v := range stats.RedirectsMap {
		s.RedirectsMap[k] += v
	}
	s.SortedTokenKeys = append(s.SortedTokenKeys, stats.SortedTokenKeys...)
}
func (s *Statistics) ToJson(prettyPrint bool) (string, error) {
	var jsonBytes []byte
	var err error
	if prettyPrint {
		jsonBytes, err = json.MarshalIndent(s, "", " ")
	} else {
		jsonBytes, err = json.Marshal(s)
	}
	if err != nil {
		return "", fmt.Errorf("error marshaling statistics to json: %w", err)
	}
	return string(jsonBytes), nil
}

func NewAnalyzer(mapper *kvs.KVS, pathOut string) *Analyzer {
	a := new(Analyzer)
	a.m = mapper
	a.PathOut = pathOut
	a.Serialize = len(a.PathOut) > 0
	return a
}
func (a *Analyzer) Matcher(dbPath string) (matcher *kvs.Matcher, err error) {
	matcher = kvs.NewMatcher()
	matcher.KP = kvs.NewKeyPath()
	err = matcher.KP.ParsePath(dbPath)
	if err != nil {
		msg := fmt.Sprintf("error parsing path %s: %v", dbPath, err)
		fmt.Println(msg)
		return nil, err
	}
	matcher.Recursive = true
	matcher.IncludeEmpty = true
	return matcher, nil
}
func (a *Analyzer) Analyze(dbPath string) (*Statistics, error) {
	matcher, err := a.Matcher(dbPath)
	// get matching records
	var recs []*kvs.DbR
	var count int
	recs, count, err = a.m.Db.GetMatching(*matcher)
	if err != nil {
		msg := fmt.Sprintf("error matching records in path %s: %v", dbPath, err)
		fmt.Println(msg)
		return nil, err
	}
	// create our results object
	stats := new(Statistics)
	stats.DbPath = dbPath
	stats.UniqueTokens = make(map[string]int)
	stats.CountRecords = count

	// process each record
	for _, rec := range recs {
		if len(rec.Value) == 0 {
			stats.CountEmptyRecords++
			continue
		}
		if rec.IsRedirect() {
			stats.CountRedirects++
			continue
		}
		var lang string
		if rec.KP.Dirs.Size() > 2 {
			if rec.KP.Dirs.First() == "ltx" {
				lang, err = ltstring.LangFromLibrary(rec.KP.Dirs.Get(1))
				if err != nil {
					msg := fmt.Sprintf("error extracting language from library %s: %v", rec.KP.Dirs.Get(1), err)
					fmt.Println(msg)
					return nil, err
				}
			}
		}
		lp := nlp.NewNLP()
		lp.Count(rec.KP.Path(), rec.Value, lang, false, false, unicode.Greek)
		stats.CountTokens = stats.CountTokens + lp.Frequency.Total
		for _, k := range lp.Frequency.Keys {
			var ok bool
			var v int
			if v, ok = stats.UniqueTokens[k]; !ok {
				// no op
			}
			stats.UniqueTokens[k] = v + 1
		}
	}
	stats.CountInflectedForms = len(stats.UniqueTokens)
	for k, _ := range stats.UniqueTokens {
		stats.SortedTokenKeys = append(stats.SortedTokenKeys, k)
	}
	sort.Strings(stats.SortedTokenKeys)
	stats.CountRecordsWithValue = stats.CountRecords - stats.CountEmptyRecords - stats.CountRedirects
	return stats, nil
	// TODO: includes English words.  Filter out.  Also, Greek not sorted properly.
}

var toSkip = []string{"client", "eu.consecrationliturgygoa", "eu.consecrationliturgygoaAB", "hcchapelbook.parts"}

func skip(topic string) bool {
	for _, s := range toSkip {
		if s == topic {
			return true
		}
	}
	return false
}
func (a *Analyzer) ParseAllGreek() (*Statistics, error) {
	kp := kvs.NewKeyPath()
	kp.Dirs.Push("ltx")
	kp.Dirs.Push("gr_gr_cog")
	paths, err := a.m.Db.DirPaths(*kp)
	if err != nil {
		return nil, err
	}
	masterStats := NewStatistics(kp.Path())

	for _, thePath := range paths {
		var subStats *Statistics
		topic := thePath.Dirs.First()
		if skip(topic) {
			continue
		}
		kp.Dirs.Push(topic)
		subStats, err = a.Parse(kp.Path())
		if err != nil {
			masterStats.ErrorMap[kp.Path()] = err.Error()
			kp.Dirs.Pop()
			continue
		}
		kp.Dirs.Pop()
		if a.Serialize {
			err = serializeStatistics(subStats, fmt.Sprintf("gr_gr_cog_%s", topic), a.PathOut)
			if err != nil {
				msg := fmt.Sprintf("error serializing statistics to json: %v", err)
				fmt.Println(msg)
				return nil, err
			}
		}

		masterStats.Add(subStats)
	}
	if a.Serialize {
		err = serializeStatistics(masterStats, "All", a.PathOut)
		if err != nil {
			msg := fmt.Sprintf("error serializing statistics to json: %v", err)
			fmt.Println(msg)
			return masterStats, err
		}
	}

	return masterStats, nil
}
func serializeStatistics(stats *Statistics, topic, pathOut string) error {
	var statsJson string
	var err error
	statsJson, err = stats.ToJson(true)
	if err != nil {
		msg := fmt.Sprintf("error converting statistics to json: %v", err)
		return fmt.Errorf(msg)
	}
	err = ltfile.WriteFile(path.Join(pathOut, fmt.Sprintf("statistics_%s.json", topic)), statsJson)
	if err != nil {
		msg := fmt.Sprintf("error writing statistics to file: %v", err)
		fmt.Println(msg)
		return fmt.Errorf(msg)
	}
	return nil
}
func split(s string) []string {
	var result []string
	sentences := strings.Split(s, ".")
	for _, sentence := range sentences {
		clauses := strings.Split(sentence, "·")
		for _, clause := range clauses {
			result = append(result, clause)
		}
	}
	return result
}
func (a *Analyzer) Parse(dbPath string) (*Statistics, error) {
	fmt.Printf("processing %s\n", dbPath)
	matcher, err := a.Matcher(dbPath)
	// get matching records
	var recs []*kvs.DbR
	var count int
	recs, count, err = a.m.Db.GetMatching(*matcher)
	if err != nil {
		msg := fmt.Sprintf("error matching records in path %s: %v", dbPath, err)
		fmt.Println(msg)
		return nil, err
	}
	// create our results object
	stats := new(Statistics)
	stats.DbPath = dbPath
	stats.UniqueTokens = make(map[string]int)
	stats.Lemmas = make(map[string]int)
	stats.RedirectsMap = make(map[string]int)
	stats.CountRecords = count

	// process each record
	for _, rec := range recs {
		if len(rec.Value) == 0 {
			stats.CountEmptyRecords++
			continue
		}
		if rec.IsRedirect() {
			stats.CountRedirects++
			stats.RedirectsMap[rec.KP.Path()]++
			continue
		}
		var lang string
		if rec.KP.Dirs.Size() > 2 {
			if rec.KP.Dirs.First() == "ltx" {
				lang, err = ltstring.LangFromLibrary(rec.KP.Dirs.Get(1))
				if err != nil {
					msg := fmt.Sprintf("error extracting language from library %s: %v", rec.KP.Dirs.Get(1), err)
					fmt.Println(msg)
					return nil, err
				}
			}
		}
		var model string
		// TODO: create a lookup map and put it in ud, key = iso lang code
		switch lang {
		case "gr":
			model = ud.Models[1]
		}
		p := new(ud.Pipe)
		var analysis *ud.Analysis
		sentences := split(cleanText(rec.Value))
		for i, sentence := range sentences {
			analysis, err = p.Process(rec.KP.Path(), sentence, model)
			if err != nil {
				if analysis == nil {
					msg := fmt.Sprintf("error processing record %s: %v", rec.KP.Path(), err)
					fmt.Println(msg)
					return nil, err
				}
			}
			for _, s := range analysis.Sentences {
				stats.CountTokens += len(s.Tokens)
				for k, v := range s.MetaData.InflectedWordMap {
					stats.UniqueTokens[k] += v
				}
				for k, v := range s.MetaData.LemmaWordMap {
					stats.Lemmas[k] += v
				}
			}
			if a.Serialize {
				var jsonOutput string
				jsonOutput, err = analysis.ToJson(true)
				if err != nil {
					msg := fmt.Sprintf("error converting analysis to json: %v", err)
					fmt.Println(msg)
					return nil, err
				}
				err = ltfile.WriteFile(path.Join(a.PathOut, rec.KP.ToFileSystemFile(fmt.Sprintf("%02d.json", i+1))), jsonOutput)
				if err != nil {
					msg := fmt.Sprintf("error writing analysis to file: %v", err)
					fmt.Println(msg)
					continue
				}
			}
		}
	}
	stats.CountInflectedForms = len(stats.UniqueTokens)
	stats.CountLemmas = len(stats.Lemmas)
	for k, _ := range stats.UniqueTokens {
		stats.SortedTokenKeys = append(stats.SortedTokenKeys, k)
	}
	sort.Strings(stats.SortedTokenKeys)
	stats.CountRecordsWithValue = stats.CountRecords - stats.CountEmptyRecords - stats.CountRedirects
	if a.Serialize {
		var statsJson string
		statsJson, err = stats.ToJson(true)
		if err != nil {
			msg := fmt.Sprintf("error converting statistics to json: %v", err)
			fmt.Println(msg)
			return stats, err
		}
		err = ltfile.WriteFile(path.Join(a.PathOut, "statistics.json"), statsJson)
		if err != nil {
			msg := fmt.Sprintf("error writing statistics to file: %v", err)
			fmt.Println(msg)
			return stats, err
		}
	}
	return stats, nil
}
func cleanText(text string) string {
	// perseus model does not recognize the following as punctuation,
	// so, we need to put spaces around them.  Otherwise,
	// they are treated as part of the word.
	text = strings.ReplaceAll(text, "«", " « ")
	text = strings.ReplaceAll(text, "»", " » ")
	text = strings.ReplaceAll(text, "(", " ( ")
	text = strings.ReplaceAll(text, ")", " ) ")
	text = strings.ReplaceAll(text, "[", " [ ")
	text = strings.ReplaceAll(text, "]", " ] ")
	text = strings.ReplaceAll(text, "  ", " ") // remove double space
	return text
}
