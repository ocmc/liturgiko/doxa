// Package uiLabels provides a thread-safe way to manage and access UI labels in multiple languages.
//
// Running the tests:
//
// # Run all tests
// go test
//
// # Run with race detection to check for concurrency issues
// go test -race
//
// # Run benchmarks
// go test -bench=.
//
// # Run benchmarks with more iterations for better statistics
// go test -bench=. -benchtime=5s
//
// # Run with verbose output to see each test
// go test -v
//
// # Run with code coverage report
// go test -cover
//
// # Generate HTML coverage report
// go test -coverprofile=coverage.out
// go tool cover -html=coverage.out
package uiLabels

import (
	"reflect"
	"sort"
	"sync"
	"testing"
)

// Test data for multiple languages
var testTranslations = map[string]map[string]string{
	"priest": {
		"en": "Priest",
		"es": "Sacerdote",
		"sw": "Kasisi",
	},
	"deacon": {
		"en": "Deacon",
		"es": "Diácono",
		"sw": "Shemasi",
	},
	"choir": {
		"en": "Choir",
		"es": "Coro",
		"sw": "Kwaya",
	},
	"lChoir": {
		"en": "Left Choir",
		"es": "Coro Izquierdo",
		"sw": "Kwaya ya Kushoto",
	},
	"rChoir": {
		"en": "Right Choir",
		"es": "Coro Derecho",
		"sw": "Kwaya ya Kulia",
	},
}

// TestNewLabels tests the NewLabels constructor
func TestNewLabels(t *testing.T) {
	labels := NewLabels()

	if labels == nil {
		t.Fatal("NewLabels() returned nil")
	}

	if labels.Map == nil {
		t.Fatal("NewLabels() created Labels with nil Map")
	}

	if len(labels.Map) != 0 {
		t.Errorf("Expected empty Map, got Map with %d elements", len(labels.Map))
	}
}

// TestSet tests setting individual translations
func TestSet(t *testing.T) {
	labels := NewLabels()

	// Set a translation
	labels.Set("priest", "en", "Priest")

	// Verify it was set correctly
	translation, exists := labels.Map["priest"]["en"]
	if !exists {
		t.Fatal("Set() failed to add translation")
	}

	if translation != "Priest" {
		t.Errorf("Expected translation to be 'Priest', got '%s'", translation)
	}

	// Test overwriting an existing translation
	labels.Set("priest", "en", "Modified Priest")
	translation, exists = labels.Map["priest"]["en"]
	if !exists {
		t.Fatal("Set() failed to update translation")
	}

	if translation != "Modified Priest" {
		t.Errorf("Expected translation to be 'Modified Priest', got '%s'", translation)
	}
}

// TestSetBulk tests setting multiple translations at once
func TestSetBulk(t *testing.T) {
	labels := NewLabels()

	// Set bulk translations
	labels.SetBulk(testTranslations)

	// Verify all were set correctly
	for abbr, langs := range testTranslations {
		for lang, expectedTranslation := range langs {
			actualTranslation, exists := labels.Map[abbr][lang]
			if !exists {
				t.Errorf("SetBulk() failed to add translation for '%s' in '%s'", abbr, lang)
				continue
			}

			if actualTranslation != expectedTranslation {
				t.Errorf("For '%s' in '%s', expected '%s', got '%s'",
					abbr, lang, expectedTranslation, actualTranslation)
			}
		}
	}

	// Test overwriting with bulk set
	modifiedTranslations := map[string]map[string]string{
		"priest": {
			"en": "Senior Priest",
			"fr": "Prêtre", // New language
		},
	}

	labels.SetBulk(modifiedTranslations)

	// Verify changes
	if translation, exists := labels.Map["priest"]["en"]; !exists || translation != "Senior Priest" {
		t.Errorf("SetBulk() failed to update existing translation")
	}

	if translation, exists := labels.Map["priest"]["fr"]; !exists || translation != "Prêtre" {
		t.Errorf("SetBulk() failed to add new language")
	}

	// Verify existing translations weren't affected
	if translation, exists := labels.Map["priest"]["es"]; !exists || translation != "Sacerdote" {
		t.Errorf("SetBulk() unexpectedly modified unrelated translation")
	}
}

// TestGet tests retrieving translations
func TestGet(t *testing.T) {
	labels := NewLabels()
	labels.SetBulk(testTranslations)

	// Test existing translation
	translation, exists := labels.Get("choir", "sw")
	if !exists {
		t.Fatal("Get() reported non-existent for existing translation")
	}

	if translation != "Kwaya" {
		t.Errorf("Expected 'Kwaya', got '%s'", translation)
	}

	// Test non-existent abbreviation
	_, exists = labels.Get("unknown", "en")
	if exists {
		t.Error("Get() reported existing for non-existent abbreviation")
	}

	// Test non-existent language
	_, exists = labels.Get("deacon", "fr")
	if exists {
		t.Error("Get() reported existing for non-existent language")
	}
}

// TestGetWithFallback tests retrieving translations with fallback
func TestGetWithFallback(t *testing.T) {
	labels := NewLabels()
	labels.SetBulk(testTranslations)

	// Test direct match (no fallback needed)
	translation, exists := labels.GetWithFallback("lChoir", "es", "en")
	if !exists {
		t.Fatal("GetWithFallback() reported non-existent for existing translation")
	}

	if translation != "Coro Izquierdo" {
		t.Errorf("Expected 'Coro Izquierdo', got '%s'", translation)
	}

	// Test fallback
	translation, exists = labels.GetWithFallback("rChoir", "fr", "en")
	if !exists {
		t.Fatal("GetWithFallback() failed to fall back to 'en'")
	}

	if translation != "Right Choir" {
		t.Errorf("Expected fallback to 'Right Choir', got '%s'", translation)
	}

	// Test non-existent with non-existent fallback
	_, exists = labels.GetWithFallback("unknown", "fr", "de")
	if exists {
		t.Error("GetWithFallback() reported existing for non-existent abbreviation and fallback")
	}
}

// TestDelete tests removing translations
func TestDelete(t *testing.T) {
	labels := NewLabels()
	labels.SetBulk(testTranslations)

	// Delete a single translation
	labels.Delete("priest", "es")

	// Verify it was deleted
	_, exists := labels.Get("priest", "es")
	if exists {
		t.Error("Delete() failed to remove translation")
	}

	// Verify other translations for same abbreviation still exist
	_, exists = labels.Get("priest", "en")
	if !exists {
		t.Error("Delete() incorrectly removed unrelated translation")
	}

	// Delete last translation for an abbreviation and check cleanup
	labels.Delete("priest", "en")
	labels.Delete("priest", "sw")

	// The entire abbreviation should be gone
	abbrs := labels.GetAllAbbreviations()
	for _, abbr := range abbrs {
		if abbr == "priest" {
			t.Error("Delete() failed to clean up empty abbreviation")
		}
	}
}

// TestDeleteAbbr tests removing all translations for an abbreviation
func TestDeleteAbbr(t *testing.T) {
	labels := NewLabels()
	labels.SetBulk(testTranslations)

	// Delete all translations for an abbreviation
	labels.DeleteAbbr("deacon")

	// Verify it was deleted
	_, exists := labels.Get("deacon", "en")
	if exists {
		t.Error("DeleteAbbr() failed to remove all translations")
	}

	// Verify other abbreviations weren't affected
	_, exists = labels.Get("choir", "en")
	if !exists {
		t.Error("DeleteAbbr() incorrectly removed unrelated abbreviation")
	}
}

// TestGetLanguages tests retrieving available languages
func TestGetLanguages(t *testing.T) {
	labels := NewLabels()
	labels.SetBulk(testTranslations)

	// Get languages for an abbreviation
	languages := labels.GetLanguages("choir")

	// Verify count
	if len(languages) != 3 {
		t.Errorf("Expected 3 languages, got %d", len(languages))
	}

	// Verify all expected languages are present
	expectedLangs := map[string]bool{"en": true, "es": true, "sw": true}
	for _, lang := range languages {
		if !expectedLangs[lang] {
			t.Errorf("Unexpected language '%s'", lang)
		}
		delete(expectedLangs, lang)
	}

	if len(expectedLangs) > 0 {
		t.Errorf("Missing languages: %v", expectedLangs)
	}

	// Test for non-existent abbreviation
	languages = labels.GetLanguages("unknown")
	if len(languages) != 0 {
		t.Errorf("Expected empty languages slice for non-existent abbreviation, got %v", languages)
	}
}

// TestGetAllAbbreviations tests retrieving all abbreviations
func TestGetAllAbbreviations(t *testing.T) {
	labels := NewLabels()
	labels.SetBulk(testTranslations)

	// Get all abbreviations
	abbrs := labels.GetAllAbbreviations()

	// Verify count
	if len(abbrs) != 5 {
		t.Errorf("Expected 5 abbreviations, got %d", len(abbrs))
	}

	// Verify all expected abbreviations are present
	expectedAbbrs := map[string]bool{
		"priest": true,
		"deacon": true,
		"choir":  true,
		"lChoir": true,
		"rChoir": true,
	}

	for _, abbr := range abbrs {
		if !expectedAbbrs[abbr] {
			t.Errorf("Unexpected abbreviation '%s'", abbr)
		}
		delete(expectedAbbrs, abbr)
	}

	if len(expectedAbbrs) > 0 {
		t.Errorf("Missing abbreviations: %v", expectedAbbrs)
	}
}

// TestConcurrentAccess tests concurrent read/write operations
func TestConcurrentAccess(t *testing.T) {
	labels := NewLabels()
	labels.SetBulk(testTranslations)

	// Number of concurrent operations
	const numReaders = 100
	const numWriters = 5

	var wg sync.WaitGroup

	// Launch readers
	for i := 0; i < numReaders; i++ {
		wg.Add(1)
		go func(id int) {
			defer wg.Done()

			// Choose different abbreviations for better coverage
			abbrs := []string{"priest", "deacon", "choir", "lChoir", "rChoir"}
			langs := []string{"en", "es", "sw"}

			// Perform multiple reads
			for j := 0; j < 10; j++ {
				abbrIndex := (id + j) % len(abbrs)
				langIndex := (id + j) % len(langs)

				_, _ = labels.Get(abbrs[abbrIndex], langs[langIndex])
				_, _ = labels.GetWithFallback(abbrs[abbrIndex], "fr", langs[langIndex])
				_ = labels.GetLanguages(abbrs[abbrIndex])
			}
		}(i)
	}

	// Launch writers
	for i := 0; i < numWriters; i++ {
		wg.Add(1)
		go func(id int) {
			defer wg.Done()

			// Add some new translations
			labels.Set("newAbbr"+string(rune(id+'0')), "en", "New Item "+string(rune(id+'0')))

			// Update existing translations
			if id%2 == 0 {
				labels.Set("priest", "en", "Senior Priest "+string(rune(id+'0')))
			} else {
				labels.Set("deacon", "sw", "Senior Shemasi "+string(rune(id+'0')))
			}

			// Delete some translations
			if id%3 == 0 {
				labels.Delete("choir", "en")
			}
		}(i)
	}

	// One more goroutine to get all abbreviations while changes are happening
	wg.Add(1)
	go func() {
		defer wg.Done()
		for i := 0; i < 10; i++ {
			_ = labels.GetAllAbbreviations()
		}
	}()

	// Wait for all goroutines to finish
	wg.Wait()

	// No explicit assertions here - this test passes if there are no race conditions
	// detected by the race detector (run with -race flag)
}

// TestEmptyLabels tests operations on newly created Labels
func TestEmptyLabels(t *testing.T) {
	labels := NewLabels()

	// Test Get on empty Labels
	_, exists := labels.Get("any", "en")
	if exists {
		t.Error("Get() reported existing for empty Labels")
	}

	// Test GetWithFallback on empty Labels
	_, exists = labels.GetWithFallback("any", "en", "es")
	if exists {
		t.Error("GetWithFallback() reported existing for empty Labels")
	}

	// Test GetLanguages on empty Labels
	langs := labels.GetLanguages("any")
	if len(langs) != 0 {
		t.Errorf("GetLanguages() returned non-empty slice for empty Labels: %v", langs)
	}

	// Test GetAllAbbreviations on empty Labels
	abbrs := labels.GetAllAbbreviations()
	if len(abbrs) != 0 {
		t.Errorf("GetAllAbbreviations() returned non-empty slice for empty Labels: %v", abbrs)
	}

	// Test Delete on empty Labels (shouldn't panic)
	labels.Delete("any", "en")

	// Test DeleteAbbr on empty Labels (shouldn't panic)
	labels.DeleteAbbr("any")
}

// TestEmptyMaps tests operations on partially empty maps
func TestEmptyMaps(t *testing.T) {
	// Test with an empty language map for an existing abbreviation
	labels := NewLabels()

	// Create an empty language map
	labels.Map["emptyLangs"] = make(map[string]string)

	// Test Get on an abbreviation with no languages
	_, exists := labels.Get("emptyLangs", "en")
	if exists {
		t.Error("Get() reported existing for abbreviation with no languages")
	}

	// Test GetWithFallback on an abbreviation with no languages
	_, exists = labels.GetWithFallback("emptyLangs", "en", "es")
	if exists {
		t.Error("GetWithFallback() reported existing for abbreviation with no languages")
	}

	// Test GetLanguages on an abbreviation with no languages
	langs := labels.GetLanguages("emptyLangs")
	if len(langs) != 0 {
		t.Errorf("GetLanguages() returned non-empty slice for abbreviation with no languages: %v", langs)
	}

	// Test Delete on an abbreviation with no languages
	labels.Delete("emptyLangs", "en") // Should not panic

	// Ensure the empty map still exists
	if _, exists := labels.Map["emptyLangs"]; !exists {
		t.Error("Delete() incorrectly removed the empty language map")
	}
}

// TestGetLanguagesSorted verifies that GetLanguages returns sorted results
func TestGetLanguagesSorted(t *testing.T) {
	labels := NewLabels()

	// Add translations in non-alphabetical order
	labels.Set("test", "zh", "测试")
	labels.Set("test", "en", "Test")
	labels.Set("test", "es", "Prueba")
	labels.Set("test", "fr", "Test")
	labels.Set("test", "de", "Test")

	// Get languages
	langs := labels.GetLanguages("test")

	// Create an expected sorted list for comparison
	expected := []string{"de", "en", "es", "fr", "zh"}
	sort.Strings(expected)

	// Sort the actual result to compare
	sort.Strings(langs)

	if !reflect.DeepEqual(langs, expected) {
		t.Errorf("GetLanguages() returned %v, expected %v", langs, expected)
	}
}

// TestGetAllAbbreviationsSorted verifies that GetAllAbbreviations returns sorted results
func TestGetAllAbbreviationsSorted(t *testing.T) {
	labels := NewLabels()

	// Add translations in non-alphabetical order
	labels.Set("zebra", "en", "Zebra")
	labels.Set("apple", "en", "Apple")
	labels.Set("dog", "en", "Dog")
	labels.Set("banana", "en", "Banana")
	labels.Set("cat", "en", "Cat")

	// Get all abbreviations
	abbrs := labels.GetAllAbbreviations()

	// Create an expected sorted list for comparison
	expected := []string{"apple", "banana", "cat", "dog", "zebra"}
	sort.Strings(expected)

	// Sort the actual result to compare
	sort.Strings(abbrs)

	if !reflect.DeepEqual(abbrs, expected) {
		t.Errorf("GetAllAbbreviations() returned %v, expected %v", abbrs, expected)
	}
}

// TestEdgeCases tests various edge cases
func TestEdgeCases(t *testing.T) {
	labels := NewLabels()

	// Test with empty strings as keys
	labels.Set("", "en", "Empty abbreviation")
	labels.Set("empty", "", "Empty language")
	labels.Set("", "", "Both empty")

	// Test retrieval with empty keys
	if val, exists := labels.Get("", "en"); !exists || val != "Empty abbreviation" {
		t.Error("Failed to retrieve with empty abbreviation")
	}

	if val, exists := labels.Get("empty", ""); !exists || val != "Empty language" {
		t.Error("Failed to retrieve with empty language")
	}

	if val, exists := labels.Get("", ""); !exists || val != "Both empty" {
		t.Error("Failed to retrieve with both empty")
	}

	// Test deletion with empty keys
	labels.Delete("", "en")
	if _, exists := labels.Get("", "en"); exists {
		t.Error("Failed to delete with empty abbreviation")
	}

	labels.Delete("empty", "")
	if _, exists := labels.Get("empty", ""); exists {
		t.Error("Failed to delete with empty language")
	}

	// Test DeleteAbbr with empty key
	labels.DeleteAbbr("")
	if _, exists := labels.Get("", ""); exists {
		t.Error("Failed to delete abbreviation with empty key")
	}
}

// TestConcurrentWriteRead tests concurrent write and read operations
func TestConcurrentWriteRead(t *testing.T) {
	labels := NewLabels()

	// Number of concurrent operations
	const numOps = 1000
	var wg sync.WaitGroup

	// Launch writers
	for i := 0; i < numOps; i++ {
		wg.Add(1)
		go func(id int) {
			defer wg.Done()
			// Write operation
			abbr := "concurrent" + string(rune(id%10+'0'))
			lang := "lang" + string(rune(id%5+'0'))
			labels.Set(abbr, lang, "Concurrent Test "+string(rune(id%20+'0')))
		}(i)
	}

	// Launch readers running concurrently with writers
	for i := 0; i < numOps; i++ {
		wg.Add(1)
		go func(id int) {
			defer wg.Done()
			// Read operation - will read some entries that exist and some that don't
			abbr := "concurrent" + string(rune(id%15+'0')) // Some will exist, some won't
			lang := "lang" + string(rune(id%7+'0'))        // Some will exist, some won't
			_, _ = labels.Get(abbr, lang)
		}(i)
	}

	// Wait for all goroutines to finish
	wg.Wait()

	// Verify that the map contains expected entries
	count := 0
	langCount := 0

	for abbr, langs := range labels.Map {
		if len(abbr) >= 10 && abbr[:10] == "concurrent" {
			count++
			langCount += len(langs)
		}
	}

	// We expect at most 10 different abbreviations (concurrent0-9)
	if count > 10 {
		t.Errorf("Expected at most 10 concurrent abbreviations, found %d", count)
	}
}

// TestSetBulkMerge tests that SetBulk properly merges translations
func TestSetBulkMerge(t *testing.T) {
	labels := NewLabels()

	// Initial set of translations
	initial := map[string]map[string]string{
		"test1": {
			"en": "Test One",
			"es": "Prueba Uno",
		},
		"test2": {
			"en": "Test Two",
			"fr": "Test Deux",
		},
	}

	// Update with overlapping and new translations
	update := map[string]map[string]string{
		"test1": {
			"en": "Updated Test One", // Update existing
			"de": "Test Eins",        // Add new language
		},
		"test3": {
			"en": "Test Three", // New abbreviation
		},
	}

	// Apply the sets
	labels.SetBulk(initial)
	labels.SetBulk(update)

	// Verify all expected entries exist with correct values
	testCases := []struct {
		abbr     string
		lang     string
		expected string
		exists   bool
	}{
		{"test1", "en", "Updated Test One", true}, // Updated
		{"test1", "es", "Prueba Uno", true},       // Untouched
		{"test1", "de", "Test Eins", true},        // Added
		{"test2", "en", "Test Two", true},         // Untouched
		{"test2", "fr", "Test Deux", true},        // Untouched
		{"test3", "en", "Test Three", true},       // Added
		{"test3", "es", "", false},                // Doesn't exist
	}

	for _, tc := range testCases {
		actual, exists := labels.Get(tc.abbr, tc.lang)
		if exists != tc.exists {
			t.Errorf("For %s/%s: expected exists=%v, got %v", tc.abbr, tc.lang, tc.exists, exists)
			continue
		}

		if tc.exists && actual != tc.expected {
			t.Errorf("For %s/%s: expected '%s', got '%s'", tc.abbr, tc.lang, tc.expected, actual)
		}
	}
}

// TestSetBulkEmptyMaps tests SetBulk with empty maps
func TestSetBulkEmptyMaps(t *testing.T) {
	labels := NewLabels()

	// Test with completely empty map
	empty := map[string]map[string]string{}
	labels.SetBulk(empty)

	if len(labels.Map) != 0 {
		t.Error("SetBulk with empty map should not change Labels")
	}

	// Test with map containing an empty inner map
	withEmpty := map[string]map[string]string{
		"empty": {},
	}
	labels.SetBulk(withEmpty)

	if _, exists := labels.Map["empty"]; !exists {
		t.Error("SetBulk should create entry for abbreviation even with empty language map")
	}

	if len(labels.Map["empty"]) != 0 {
		t.Error("Language map should be empty")
	}

	// Test with nil inner map (should be handled gracefully)
	withNil := map[string]map[string]string{
		"nil": nil,
	}
	labels.SetBulk(withNil)

	if _, exists := labels.Map["nil"]; exists && labels.Map["nil"] == nil {
		t.Error("SetBulk with nil inner map should initialize it to empty map")
	}
}
