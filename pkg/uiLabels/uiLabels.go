// Package uiLabels provides a thread-safe way to manage and access UI labels
// in multiple languages. It is optimized for read-heavy workloads where
// writes primarily happen during initialization with rare updates afterward.
package uiLabels

import (
	"sync"
)

// Labels represents a thread-safe collection of UI labels in multiple languages.
// It's optimized for frequent reads and infrequent writes.
type Labels struct {
	// RWMutex provides read-write locking mechanism
	// Multiple goroutines can acquire read lock simultaneously
	// Write operations get exclusive access
	mu sync.RWMutex

	// Map stores UI labels where:
	// - First key is the label abbreviation (e.g., "li")
	// - Second key is the ISO language code (e.g., "en", "es")
	// - Value is the translated string
	Map map[string]map[string]string
}

// NewLabels creates and returns a new Labels instance
func NewLabels() *Labels {
	return &Labels{
		Map: make(map[string]map[string]string),
	}
}

// Get retrieves a translated label for the given abbreviation and language code.
// It returns the translated string and a boolean indicating if the label was found.
func (l *Labels) Get(abbr, langCode string) (string, bool) {
	l.mu.RLock()
	defer l.mu.RUnlock()

	if translations, exists := l.Map[abbr]; exists {
		if translation, langExists := translations[langCode]; langExists {
			return translation, true
		}
	}

	return "", false
}

// GetWithFallback retrieves a translated label with fallback to a default language.
// If the requested language is not available, it falls back to the provided default.
func (l *Labels) GetWithFallback(abbr, langCode, fallbackLang string) (string, bool) {
	l.mu.RLock()
	defer l.mu.RUnlock()

	if translations, exists := l.Map[abbr]; exists {
		// Try the requested language first
		if translation, langExists := translations[langCode]; langExists {
			return translation, true
		}

		// Fall back to the fallback language
		if translation, fallbackExists := translations[fallbackLang]; fallbackExists {
			return translation, true
		}
	}

	return "", false
}

// Set adds or updates a translation for a specific abbreviation and language.
func (l *Labels) Set(abbr, langCode, translation string) {
	l.mu.Lock()
	defer l.mu.Unlock()

	if _, exists := l.Map[abbr]; !exists {
		l.Map[abbr] = make(map[string]string)
	}

	l.Map[abbr][langCode] = translation
}

// SetBulk adds or updates multiple translations at once.
// The input is a map of abbreviations to a map of language codes to translations.
func (l *Labels) SetBulk(translations map[string]map[string]string) {
	l.mu.Lock()
	defer l.mu.Unlock()

	for abbr, langMap := range translations {
		if _, exists := l.Map[abbr]; !exists {
			l.Map[abbr] = make(map[string]string)
		}

		for langCode, translation := range langMap {
			l.Map[abbr][langCode] = translation
		}
	}
}

// Delete removes a translation for a specific abbreviation and language.
func (l *Labels) Delete(abbr, langCode string) {
	l.mu.Lock()
	defer l.mu.Unlock()

	if translations, exists := l.Map[abbr]; exists {
		delete(translations, langCode)

		// If no more translations for this abbreviation, clean up
		if len(translations) == 0 {
			delete(l.Map, abbr)
		}
	}
}

// DeleteAbbr removes all translations for a specific abbreviation.
func (l *Labels) DeleteAbbr(abbr string) {
	l.mu.Lock()
	defer l.mu.Unlock()

	delete(l.Map, abbr)
}

// GetLanguages returns all available language codes for a specific abbreviation.
func (l *Labels) GetLanguages(abbr string) []string {
	l.mu.RLock()
	defer l.mu.RUnlock()

	if translations, exists := l.Map[abbr]; exists {
		languages := make([]string, 0, len(translations))

		for langCode := range translations {
			languages = append(languages, langCode)
		}

		return languages
	}

	return []string{}
}

// GetAllAbbreviations returns all available abbreviations.
func (l *Labels) GetAllAbbreviations() []string {
	l.mu.RLock()
	defer l.mu.RUnlock()

	abbreviations := make([]string, 0, len(l.Map))

	for abbr := range l.Map {
		abbreviations = append(abbreviations, abbr)
	}

	return abbreviations
}
