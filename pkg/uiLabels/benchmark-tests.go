// Package uiLabels provides a thread-safe way to manage and access UI labels in multiple languages.
//
// Running the benchmarks:
//
// # Run all benchmarks
// go test -bench=.
//
// # Run specific benchmark
// go test -bench=BenchmarkGet
//
// # Run benchmarks with more iterations for better statistics
// go test -bench=. -benchtime=5s
//
// # See memory allocations during benchmarks
// go test -bench=. -benchmem
//
// # Profile CPU usage
// go test -bench=. -cpuprofile=cpu.out
// go tool pprof cpu.out
//
// # Profile memory usage
// go test -bench=. -memprofile=mem.out
// go tool pprof mem.out
package uiLabels

import (
	"sync"
	"testing"
)

// Test data for benchmark tests
var benchTranslations = map[string]map[string]string{
	"priest": {
		"en": "Priest",
		"es": "Sacerdote",
		"sw": "Kasisi",
	},
	"deacon": {
		"en": "Deacon",
		"es": "Diácono",
		"sw": "Shemasi",
	},
	"choir": {
		"en": "Choir",
		"es": "Coro",
		"sw": "Kwaya",
	},
	"lChoir": {
		"en": "Left Choir",
		"es": "Coro Izquierdo",
		"sw": "Kwaya ya Kushoto",
	},
	"rChoir": {
		"en": "Right Choir",
		"es": "Coro Derecho",
		"sw": "Kwaya ya Kulia",
	},
}

// BenchmarkGet benchmarks the Get method (read operation)
func BenchmarkGet(b *testing.B) {
	labels := NewLabels()
	labels.SetBulk(benchTranslations)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		// Cycle through different abbreviations and languages
		abbr := "priest"
		lang := "en"

		switch i % 5 {
		case 0:
			abbr = "priest"
		case 1:
			abbr = "deacon"
		case 2:
			abbr = "choir"
		case 3:
			abbr = "lChoir"
		case 4:
			abbr = "rChoir"
		}

		switch i % 3 {
		case 0:
			lang = "en"
		case 1:
			lang = "es"
		case 2:
			lang = "sw"
		}

		_, _ = labels.Get(abbr, lang)
	}
}

// BenchmarkGetWithFallback benchmarks the GetWithFallback method
func BenchmarkGetWithFallback(b *testing.B) {
	labels := NewLabels()
	labels.SetBulk(benchTranslations)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		// Cycle through different abbreviations
		abbr := "priest"
		switch i % 5 {
		case 0:
			abbr = "priest"
		case 1:
			abbr = "deacon"
		case 2:
			abbr = "choir"
		case 3:
			abbr = "lChoir"
		case 4:
			abbr = "rChoir"
		}

		// Sometimes use an unavailable language to test fallback
		if i%3 == 0 {
			// This will use fallback
			_, _ = labels.GetWithFallback(abbr, "fr", "en")
		} else {
			// This won't need fallback
			_, _ = labels.GetWithFallback(abbr, "es", "en")
		}
	}
}

// BenchmarkSet benchmarks the Set method (write operation)
func BenchmarkSet(b *testing.B) {
	labels := NewLabels()

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		abbr := "bench" + string(rune(i%10+'0'))
		lang := "lang" + string(rune(i%5+'0'))
		translation := "Translation " + string(rune(i%20+'0'))

		labels.Set(abbr, lang, translation)
	}
}

// BenchmarkConcurrentRead benchmarks concurrent read operations
func BenchmarkConcurrentRead(b *testing.B) {
	labels := NewLabels()
	labels.SetBulk(benchTranslations)

	// Fixed number of goroutines
	const numGoroutines = 100

	b.ResetTimer()

	// Run the benchmark
	b.RunParallel(func(pb *testing.PB) {
		// Each goroutine will execute this loop until pb.Next() returns false
		counter := 0
		for pb.Next() {
			abbr := "priest"
			lang := "en"

			// Mix up the queries
			switch counter % 5 {
			case 0:
				abbr = "priest"
			case 1:
				abbr = "deacon"
			case 2:
				abbr = "choir"
			case 3:
				abbr = "lChoir"
			case 4:
				abbr = "rChoir"
			}

			switch counter % 3 {
			case 0:
				lang = "en"
			case 1:
				lang = "es"
			case 2:
				lang = "sw"
			}

			_, _ = labels.Get(abbr, lang)
			counter++
		}
	})
}

// BenchmarkMixedReadWrite benchmarks a realistic scenario with mostly reads and occasional writes
func BenchmarkMixedReadWrite(b *testing.B) {
	labels := NewLabels()
	labels.SetBulk(benchTranslations)

	var mutex sync.Mutex
	writeCounter := 0

	b.ResetTimer()

	b.RunParallel(func(pb *testing.PB) {
		counter := 0
		for pb.Next() {
			// Mostly reads (95%) with occasional writes (5%)
			if counter%20 == 0 {
				// Acquire counter to ensure we're not all writing at the same moment
				mutex.Lock()
				localWriteCounter := writeCounter
				writeCounter++
				mutex.Unlock()

				// Perform a write operation
				abbr := "write" + string(rune(localWriteCounter%10+'0'))
				lang := "lang" + string(rune(localWriteCounter%5+'0'))
				translation := "Write Translation " + string(rune(localWriteCounter%20+'0'))

				labels.Set(abbr, lang, translation)
			} else {
				// Perform a read operation
				abbr := "priest"
				lang := "en"

				switch counter % 5 {
				case 0:
					abbr = "priest"
				case 1:
					abbr = "deacon"
				case 2:
					abbr = "choir"
				case 3:
					abbr = "lChoir"
				case 4:
					abbr = "rChoir"
				}

				switch counter % 3 {
				case 0:
					lang = "en"
				case 1:
					lang = "es"
				case 2:
					lang = "sw"
				}

				_, _ = labels.Get(abbr, lang)
			}
			counter++
		}
	})
}

// BenchmarkGetLanguages benchmarks the GetLanguages method
func BenchmarkGetLanguages(b *testing.B) {
	labels := NewLabels()
	labels.SetBulk(benchTranslations)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		// Cycle through different abbreviations
		abbr := "priest"
		switch i % 5 {
		case 0:
			abbr = "priest"
		case 1:
			abbr = "deacon"
		case 2:
			abbr = "choir"
		case 3:
			abbr = "lChoir"
		case 4:
			abbr = "rChoir"
		}

		_ = labels.GetLanguages(abbr)
	}
}

// BenchmarkGetAllAbbreviations benchmarks the GetAllAbbreviations method
func BenchmarkGetAllAbbreviations(b *testing.B) {
	labels := NewLabels()
	labels.SetBulk(benchTranslations)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = labels.GetAllAbbreviations()
	}
}
