#!/usr/bin/env zsh

# run_tests.zsh - Comprehensive test runner for uiLabels package
# This script runs tests in a logical sequence from basic to advanced

# Set up color variables for better output
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
NC='\033[0m' # No Color

# Create output directory for test results
OUTPUT_DIR="./test_results"
mkdir -p $OUTPUT_DIR

# Function to print section headers
print_header() {
  printf "\n${BLUE}=======================================================${NC}\n"
  printf "${YELLOW}    $1${NC}\n"
  printf "${BLUE}=======================================================${NC}\n\n"
}

# Function to run a command and check if it succeeded
run_command() {
  local cmd=$1
  local output_file=$2
  local description=$3

  printf "${PURPLE}Running:${NC} %s\n" "$description"
  printf "${BLUE}Command:${NC} %s\n" "$cmd"

  # Execute command and capture both stdout and stderr to file
  if eval "$cmd" > "$output_file" 2>&1; then
    printf "${GREEN}✓ Success${NC}\n\n"
    return 0
  else
    printf "${RED}✗ Failed${NC}\n"
    printf "See %s for details\n\n" "$output_file"
    return 1
  fi
}

# Start testing sequence
print_header "uiLabels Package Test Suite"
printf "Date: %s\n" "$(date)"
printf "Go version: %s\n" "$(go version)"

# Step 1: Basic tests
print_header "1. Basic Tests"
run_command "go test" "$OUTPUT_DIR/basic_tests.log" "Running basic tests"

# Step 2: Tests with verbose output
print_header "2. Verbose Tests"
run_command "go test -v" "$OUTPUT_DIR/verbose_tests.log" "Running tests with verbose output"

# Step 3: Verify improved tests are being run
print_header "3. Verify Improved Tests"
run_command "go test -v -run=TestEmpty\|TestEdge\|TestConcurrentWrite\|TestSetBulk" "$OUTPUT_DIR/improved_tests.log" "Running improved tests specifically"

# Step 3: Race condition tests
print_header "4. Race Condition Tests"
run_command "go test -race" "$OUTPUT_DIR/race_tests.log" "Running tests with race detector"

# Step 4: Code coverage
print_header "5. Code Coverage"
run_command "go test -cover" "$OUTPUT_DIR/coverage_basic.log" "Running basic coverage report"
run_command "go test -coverprofile=$OUTPUT_DIR/coverage.out" "$OUTPUT_DIR/coverage_detailed.log" "Generating detailed coverage data"
run_command "go tool cover -html=$OUTPUT_DIR/coverage.out -o $OUTPUT_DIR/coverage.html" "$OUTPUT_DIR/coverage_html.log" "Generating HTML coverage report"
printf "HTML coverage report available at: %s\n" "$OUTPUT_DIR/coverage.html"

# Step 5: Compare coverage before and after improved tests
print_header "6. Coverage Comparison"
run_command "grep -A 1 'coverage:' $OUTPUT_DIR/coverage_basic.log" "$OUTPUT_DIR/coverage_comparison.log" "Comparing coverage with improved tests"

# Step 5: Basic benchmarks
print_header "7. Basic Benchmarks"
run_command "go test -bench=. -benchtime=1s" "$OUTPUT_DIR/bench_basic.log" "Running basic benchmarks (1s)"

# Step 6: Detailed benchmarks with memory stats
print_header "8. Detailed Benchmarks with Memory Stats"
run_command "go test -bench=. -benchtime=3s -benchmem" "$OUTPUT_DIR/bench_detailed.log" "Running detailed benchmarks with memory stats (3s)"

# Step 7: CPU Profiling
print_header "9. CPU Profiling"
run_command "go test -bench=. -benchtime=2s -cpuprofile=$OUTPUT_DIR/cpu.out" "$OUTPUT_DIR/profile_cpu.log" "Generating CPU profile"
printf "To analyze the CPU profile, run: go tool pprof %s\n" "$OUTPUT_DIR/cpu.out"

# Step 8: Memory Profiling
print_header "10. Memory Profiling"
run_command "go test -bench=. -benchtime=2s -memprofile=$OUTPUT_DIR/mem.out" "$OUTPUT_DIR/profile_mem.log" "Generating memory profile"
printf "To analyze the memory profile, run: go tool pprof %s\n" "$OUTPUT_DIR/mem.out"

# Step 9: Individual benchmark focus on read operations (most important for this package)
print_header "11. Read Performance Focus"
run_command "go test -bench=BenchmarkGet -benchtime=5s -count=5" "$OUTPUT_DIR/bench_get.log" "Focused benchmarking on Get method (5s x 5)"
run_command "go test -bench=BenchmarkConcurrentRead -benchtime=5s -count=5" "$OUTPUT_DIR/bench_concurrent_read.log" "Focused benchmarking on concurrent reads (5s x 5)"

# Step 10: Real-world mixed read/write scenario (important for validation)
print_header "12. Mixed Read/Write Scenario"
run_command "go test -bench=BenchmarkMixedReadWrite -benchtime=5s -count=5" "$OUTPUT_DIR/bench_mixed.log" "Simulating real-world usage with mixed reads/writes (5s x 5)"

# Print summary
print_header "Test Results Summary"
printf "All test results are available in the %s directory\n" "$OUTPUT_DIR"
printf "\nQuick coverage summary:\n"
grep -A 1 "coverage:" "$OUTPUT_DIR/coverage_basic.log" || printf "Coverage information not available\n"

printf "\nQuick benchmark summary for read operations:\n"
grep "BenchmarkGet" "$OUTPUT_DIR/bench_get.log" | tail -n 5 || printf "Benchmark information not available\n"

print_header "Testing Complete"
printf "For visualizing profiles, consider using:\n"
printf "  - go tool pprof -http=:8080 %s\n" "$OUTPUT_DIR/cpu.out"
printf "  - go tool pprof -http=:8080 %s\n" "$OUTPUT_DIR/mem.out"
printf "To view the HTML coverage report, open:\n"
printf "  - %s\n\n" "$OUTPUT_DIR/coverage.html"