package fonts

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"path"
	"strconv"
	"strings"
	"unicode"
)

type Fonts struct {
	RootDir       string
	LangToFontMap map[string]FontSet
}
type FontSet struct {
	Bold              string
	BoldItalic        string
	Italic            string
	Regular           string
	FontPath          string
	KerningOn         bool
	KerningDivisor    float64
	LineBreakAdjuster float64
	MiddleColAdjuster float64
	DateFormat        string
}

func NewFontManager(path string, langToFontMap map[string]FontSet) (*Fonts, error) {
	var fm = new(Fonts)
	if !ltfile.DirExists(path) {
		return fm, fmt.Errorf("font dir %s does not exist", path)
	}
	fm.RootDir = path
	err := fm.load()
	return fm, err
}
func (fm *Fonts) FontSetForLanguage(language string) (FontSet, error) {
	var font FontSet
	var ok bool
	if font, ok = fm.LangToFontMap[language]; ok {
		return font, nil
	}
	return font, fmt.Errorf(
		"%s not found in font map",
		language,
	)
}
func (fm *Fonts) load() error {
	paths, err := ltfile.FileMatcher(fm.RootDir, "ttf", nil)
	fontFileMap := make(map[string]string)
	if err != nil {
		return err
	}
	for _, p := range paths {
		file := path.Base(p)
		fontFileMap[file] = p
	}

	return nil
}

type Hex string

type RGB struct {
	Red   uint8
	Green uint8
	Blue  uint8
}

func (h Hex) toRGB() (RGB, error) {
	return Hex2RGB(h)
}

func Hex2RGB(hex Hex) (RGB, error) {
	var rgb RGB
	values, err := strconv.ParseUint(string(hex), 16, 32)

	if err != nil {
		return RGB{}, err
	}

	rgb = RGB{
		Red:   uint8(values >> 16),
		Green: uint8((values >> 8) & 0xFF),
		Blue:  uint8(values & 0xFF),
	}

	return rgb, nil
}
func ColorName2RGB(name string) (int, int, int) {
	switch name {
	case "aliceblue":
		return 240, 248, 255
	case "antiquewhite":
		return 250, 235, 215
	case "aqua":
		return 0, 255, 255
	case "aquamarine":
		return 127, 255, 212
	case "azure":
		return 240, 255, 255
	case "beige":
		return 245, 245, 220
	case "bisque":
		return 255, 228, 196
	case "black":
		return 0, 0, 0
	case "blanchedalmond":
		return 255, 235, 205
	case "blue":
		return 0, 0, 255
	case "blueviolet":
		return 138, 43, 226
	case "brown":
		return 165, 42, 42
	case "burlywood":
		return 222, 184, 135
	case "cadetblue":
		return 95, 158, 160
	case "chartreuse":
		return 127, 255, 0
	case "chocolate":
		return 210, 105, 30
	case "coral":
		return 255, 127, 80
	case "cornflowerblue":
		return 100, 149, 237
	case "cornsilk":
		return 255, 248, 220
	case "crimson":
		return 220, 20, 60
	case "cyan":
		return 0, 255, 255
	case "darkblue":
		return 0, 0, 139
	case "darkcyan":
		return 0, 139, 139
	case "darkgoldenrod":
		return 184, 134, 11
	case "darkgray":
		return 169, 169, 169
	case "darkgreen":
		return 0, 100, 0
	case "darkgrey":
		return 169, 169, 169
	case "darkkhaki":
		return 189, 183, 107
	case "darkmagenta":
		return 139, 0, 139
	case "darkolivegreen":
		return 85, 107, 47
	case "darkorange":
		return 255, 140, 0
	case "darkorchid":
		return 153, 50, 204
	case "darkred":
		return 139, 0, 0
	case "darksalmon":
		return 233, 150, 122
	case "darkseagreen":
		return 143, 188, 143
	case "darkslateblue":
		return 72, 61, 139
	case "darkslategray":
		return 47, 79, 79
	case "darkslategrey":
		return 47, 79, 79
	case "darkturquoise":
		return 0, 206, 209
	case "darkviolet":
		return 148, 0, 211
	case "deeppink":
		return 255, 20, 147
	case "deepskyblue":
		return 0, 191, 255
	case "dimgray":
		return 105, 105, 105
	case "dimgrey":
		return 105, 105, 105
	case "dodgerblue":
		return 30, 144, 255
	case "firebrick":
		return 178, 34, 34
	case "floralwhite":
		return 255, 250, 240
	case "forestgreen":
		return 34, 139, 34
	case "fuchsia":
		return 255, 0, 255
	case "gainsboro":
		return 220, 220, 220
	case "ghostwhite":
		return 248, 248, 255
	case "gold":
		return 255, 215, 0
	case "goldenrod":
		return 218, 165, 32
	case "gray":
		return 128, 128, 128
	case "green":
		return 0, 128, 0
	case "greenyellow":
		return 173, 255, 47
	case "grey":
		return 128, 128, 128
	case "honeydew":
		return 240, 255, 240
	case "hotpink":
		return 255, 105, 180
	case "indianred":
		return 205, 92, 92
	case "indigo":
		return 75, 0, 130
	case "ivory":
		return 255, 255, 240
	case "khaki":
		return 240, 230, 140
	case "lavender":
		return 230, 230, 250
	case "lavenderblush":
		return 255, 240, 245
	case "lawngreen":
		return 124, 252, 0
	case "lemonchiffon":
		return 255, 250, 205
	case "lightblue":
		return 173, 216, 230
	case "lightcoral":
		return 240, 128, 128
	case "lightcyan":
		return 224, 255, 255
	case "lightgoldenrodyellow":
		return 250, 250, 210
	case "lightgray":
		return 211, 211, 211
	case "lightgreen":
		return 144, 238, 144
	case "lightgrey":
		return 211, 211, 211
	case "lightpink":
		return 255, 182, 193
	case "lightsalmon":
		return 255, 160, 122
	case "lightseagreen":
		return 32, 178, 170
	case "lightskyblue":
		return 135, 206, 250
	case "lightslategray":
		return 119, 136, 153
	case "lightslategrey":
		return 119, 136, 153
	case "lightsteelblue":
		return 176, 196, 222
	case "lightyellow":
		return 255, 255, 224
	case "lime":
		return 0, 255, 0
	case "limegreen":
		return 50, 205, 50
	case "linen":
		return 250, 240, 230
	case "magenta":
		return 255, 0, 255
	case "maroon":
		return 128, 0, 0
	case "mediumaquamarine":
		return 102, 205, 170
	case "mediumblue":
		return 0, 0, 205
	case "mediumorchid":
		return 186, 85, 211
	case "mediumpurple":
		return 147, 112, 219
	case "mediumseagreen":
		return 60, 179, 113
	case "mediumslateblue":
		return 123, 104, 238
	case "mediumspringgreen":
		return 0, 250, 154
	case "mediumturquoise":
		return 72, 209, 204
	case "mediumvioletred":
		return 199, 21, 133
	case "midnightblue":
		return 25, 25, 112
	case "mintcream":
		return 245, 255, 250
	case "mistyrose":
		return 255, 228, 225
	case "moccasin":
		return 255, 228, 181
	case "navajowhite":
		return 255, 222, 173
	case "navy":
		return 0, 0, 128
	case "oldlace":
		return 253, 245, 230
	case "olive":
		return 128, 128, 0
	case "olivedrab":
		return 107, 142, 35
	case "orange":
		return 255, 165, 0
	case "orangered":
		return 255, 69, 0
	case "orchid":
		return 218, 112, 214
	case "palegoldenrod":
		return 238, 232, 170
	case "palegreen":
		return 152, 251, 152
	case "paleturquoise":
		return 175, 238, 238
	case "palevioletred":
		return 219, 112, 147
	case "papayawhip":
		return 255, 239, 213
	case "peachpuff":
		return 255, 218, 185
	case "peru":
		return 205, 133, 63
	case "pink":
		return 255, 192, 203
	case "plum":
		return 221, 160, 221
	case "powderblue":
		return 176, 224, 230
	case "purple":
		return 128, 0, 128
	case "red":
		return 255, 0, 0
	case "rosybrown":
		return 188, 143, 143
	case "royalblue":
		return 65, 105, 225
	case "saddlebrown":
		return 139, 69, 19
	case "salmon":
		return 250, 128, 114
	case "sandybrown":
		return 244, 164, 96
	case "seagreen":
		return 46, 139, 87
	case "seashell":
		return 255, 245, 238
	case "sienna":
		return 160, 82, 45
	case "silver":
		return 192, 192, 192
	case "skyblue":
		return 135, 206, 235
	case "slateblue":
		return 106, 90, 205
	case "slategray":
		return 112, 128, 144
	case "slategrey":
		return 112, 128, 144
	case "snow":
		return 255, 250, 250
	case "springgreen":
		return 0, 255, 127
	case "steelblue":
		return 70, 130, 180
	case "tan":
		return 210, 180, 140
	case "teal":
		return 0, 128, 128
	case "thistle":
		return 216, 191, 216
	case "tomato":
		return 255, 99, 71
	case "turquoise":
		return 64, 224, 208
	case "violet":
		return 238, 130, 238
	case "wheat":
		return 245, 222, 179
	case "white":
		return 255, 255, 255
	case "whitesmoke":
		return 245, 245, 245
	case "yellow":
		return 255, 255, 0
	case "yellowgreen":
		return 154, 205, 50
	default:
		return 0, 0, 0
	}
}

/*
The following added by Michael Colburn
*/

// RelativePoints calculates the point size for a given relativeSize keyword based on the base font size.
// relativeSize is one of: xx-small, x-small, medium, large, x-large, xx-large
// If baseFontSize is 0.0, returns an error
// If relativeSize is not valid, returns an error.
// Note: smaller and larger probably don't work as the user expects.
func RelativePoints(baseFontSize float64, relativeSize string) (float64, error) {
	if baseFontSize == 0.0 {
		return 0.0, fmt.Errorf("baseFontSize 0.0 not allowed")
	}
	if strings.HasSuffix(relativeSize, "px") {
		str := strings.TrimSuffix(relativeSize, "px")
		f, err := strconv.ParseFloat(str, 64)
		if err != nil {
			msg := fmt.Sprintf("invalid pixel number %s", relativeSize)
			doxlog.Errorf(msg)
			return 0.0, fmt.Errorf(msg)
		}
		return PixelsToPoints(f), nil
	}
	switch relativeSize {
	case "xx-small":
		return baseFontSize * 0.60, nil
	case "x-small", "smaller":
		return baseFontSize * 0.75, nil
	case "small":
		return baseFontSize * 0.89, nil
	case "medium":
		return baseFontSize * 1.00, nil // Base size
	case "large":
		return baseFontSize * 1.20, nil
	case "x-large", "larger":
		return baseFontSize * 1.50, nil
	case "xx-large":
		return baseFontSize * 2.00, nil
	case "0":
		return 0, nil
	case "inherit":
		doxlog.Warnf("inherit is not supported in PDF css")
		return baseFontSize, nil
	default:
		msg := fmt.Sprintf("unknown relativeSize %s", relativeSize)
		doxlog.Warnf(msg)
		return baseFontSize, nil //fmt.Errorf(msg)
	}
}
func PointsToMillimeters(points float64) float64 {
	return points * 0.352778
}
func MillimetersToPoints(mm float64) float64 {
	// 1 point = 0.3527777778 millimeters
	return mm / 0.3527777778
}
func PixelsToMillimeters(pixels float64) float64 {
	// you can't really do this, since pixels have to do with screens
	// and the resolution causes variation
	return PointsToMillimeters(PixelsToPoints(pixels))
}
func PixelsToPoints(pixels float64) float64 {
	// you can't really do this, since pixels have to do with screens
	// and the resolution causes variation
	return pixels * 0.75 * 1.5
}
func PointsToPixels(points float64) float64 {
	// you can't really do this for PDF, since pixels have to do with screens
	// and the resolution causes variation
	return points * 1.3333343412075
}
func RangeTable(rune2 rune) *unicode.RangeTable {
	// placeholder func until ready to use the unicode.Is test.
	// The idea is to use it for kerning.
	if unicode.Is(unicode.Adlam, rune2) {
		return unicode.Adlam
	}
	if unicode.Is(unicode.Ahom, rune2) {
		return unicode.Ahom
	}
	if unicode.Is(unicode.Anatolian_Hieroglyphs, rune2) {
		return unicode.Anatolian_Hieroglyphs
	}
	if unicode.Is(unicode.Arabic, rune2) {
		return unicode.Arabic
	}
	if unicode.Is(unicode.Armenian, rune2) {
		return unicode.Armenian
	}
	if unicode.Is(unicode.Avestan, rune2) {
		return unicode.Avestan
	}
	if unicode.Is(unicode.Balinese, rune2) {
		return unicode.Balinese
	}
	if unicode.Is(unicode.Bamum, rune2) {
		return unicode.Bamum
	}
	if unicode.Is(unicode.Bassa_Vah, rune2) {
		return unicode.Bassa_Vah
	}
	if unicode.Is(unicode.Batak, rune2) {
		return unicode.Batak
	}
	if unicode.Is(unicode.Bengali, rune2) {
		return unicode.Bengali
	}
	if unicode.Is(unicode.Bhaiksuki, rune2) {
		return unicode.Bhaiksuki
	}
	if unicode.Is(unicode.Bopomofo, rune2) {
		return unicode.Bopomofo
	}
	if unicode.Is(unicode.Brahmi, rune2) {
		return unicode.Brahmi
	}
	if unicode.Is(unicode.Braille, rune2) {
		return unicode.Braille
	}
	if unicode.Is(unicode.Buginese, rune2) {
		return unicode.Buginese
	}
	if unicode.Is(unicode.Buhid, rune2) {
		return unicode.Buhid
	}
	if unicode.Is(unicode.Canadian_Aboriginal, rune2) {
		return unicode.Canadian_Aboriginal
	}
	if unicode.Is(unicode.Carian, rune2) {
		return unicode.Carian
	}
	if unicode.Is(unicode.Caucasian_Albanian, rune2) {
		return unicode.Caucasian_Albanian
	}
	if unicode.Is(unicode.Chakma, rune2) {
		return unicode.Chakma
	}
	if unicode.Is(unicode.Cham, rune2) {
		return unicode.Cham
	}
	if unicode.Is(unicode.Cherokee, rune2) {
		return unicode.Cherokee
	}
	if unicode.Is(unicode.Chorasmian, rune2) {
		return unicode.Chorasmian
	}
	if unicode.Is(unicode.Common, rune2) {
		return unicode.Common
	}
	if unicode.Is(unicode.Coptic, rune2) {
		return unicode.Coptic
	}
	if unicode.Is(unicode.Cuneiform, rune2) {
		return unicode.Cuneiform
	}
	if unicode.Is(unicode.Cuneiform, rune2) {
		return unicode.Cuneiform
	}
	if unicode.Is(unicode.Cyrillic, rune2) {
		return unicode.Cyrillic
	}
	if unicode.Is(unicode.Deseret, rune2) {
		return unicode.Deseret
	}
	if unicode.Is(unicode.Devanagari, rune2) {
		return unicode.Devanagari
	}
	if unicode.Is(unicode.Dives_Akuru, rune2) {
		return unicode.Dives_Akuru
	}
	if unicode.Is(unicode.Dogra, rune2) {
		return unicode.Dogra
	}
	if unicode.Is(unicode.Duployan, rune2) {
		return unicode.Duployan
	}
	if unicode.Is(unicode.Egyptian_Hieroglyphs, rune2) {
		return unicode.Egyptian_Hieroglyphs
	}
	if unicode.Is(unicode.Elbasan, rune2) {
		return unicode.Elbasan
	}
	if unicode.Is(unicode.Elymaic, rune2) {
		return unicode.Elymaic
	}
	if unicode.Is(unicode.Ethiopic, rune2) {
		return unicode.Ethiopic
	}
	if unicode.Is(unicode.Georgian, rune2) {
		return unicode.Georgian
	}
	if unicode.Is(unicode.Glagolitic, rune2) {
		return unicode.Glagolitic
	}
	if unicode.Is(unicode.Gothic, rune2) {
		return unicode.Gothic
	}
	if unicode.Is(unicode.Grantha, rune2) {
		return unicode.Grantha
	}
	if unicode.Is(unicode.Greek, rune2) {
		return unicode.Greek
	}
	if unicode.Is(unicode.Gujarati, rune2) {
		return unicode.Gujarati
	}
	if unicode.Is(unicode.Gunjala_Gondi, rune2) {
		return unicode.Gunjala_Gondi
	}
	if unicode.Is(unicode.Gurmukhi, rune2) {
		return unicode.Gurmukhi
	}
	if unicode.Is(unicode.Han, rune2) {
		return unicode.Han
	}
	if unicode.Is(unicode.Hangul, rune2) {
		return unicode.Hangul
	}
	if unicode.Is(unicode.Hanifi_Rohingya, rune2) {
		return unicode.Hanifi_Rohingya
	}
	if unicode.Is(unicode.Hanunoo, rune2) {
		return unicode.Hanunoo
	}
	if unicode.Is(unicode.Hatran, rune2) {
		return unicode.Hatran
	}
	if unicode.Is(unicode.Hebrew, rune2) {
		return unicode.Hebrew
	}
	if unicode.Is(unicode.Hiragana, rune2) {
		return unicode.Hiragana
	}
	if unicode.Is(unicode.Imperial_Aramaic, rune2) {
		return unicode.Imperial_Aramaic
	}
	if unicode.Is(unicode.Inherited, rune2) {
		return unicode.Inherited
	}
	if unicode.Is(unicode.Inscriptional_Pahlavi, rune2) {
		return unicode.Inscriptional_Pahlavi
	}
	if unicode.Is(unicode.Inscriptional_Parthian, rune2) {
		return unicode.Inscriptional_Parthian
	}
	if unicode.Is(unicode.Javanese, rune2) {
		return unicode.Javanese
	}
	if unicode.Is(unicode.Kaithi, rune2) {
		return unicode.Kaithi
	}
	if unicode.Is(unicode.Kannada, rune2) {
		return unicode.Kannada
	}
	if unicode.Is(unicode.Katakana, rune2) {
		return unicode.Katakana
	}
	if unicode.Is(unicode.Kayah_Li, rune2) {
		return unicode.Kayah_Li
	}
	if unicode.Is(unicode.Kharoshthi, rune2) {
		return unicode.Kharoshthi
	}
	if unicode.Is(unicode.Khitan_Small_Script, rune2) {
		return unicode.Khitan_Small_Script
	}
	if unicode.Is(unicode.Khmer, rune2) {
		return unicode.Khmer
	}
	if unicode.Is(unicode.Khojki, rune2) {
		return unicode.Khojki
	}
	if unicode.Is(unicode.Khudawadi, rune2) {
		return unicode.Khudawadi
	}
	if unicode.Is(unicode.Lao, rune2) {
		return unicode.Lao
	}
	if unicode.Is(unicode.Latin, rune2) {
		return unicode.Latin
	}
	if unicode.Is(unicode.Lepcha, rune2) {
		return unicode.Lepcha
	}
	if unicode.Is(unicode.Limbu, rune2) {
		return unicode.Limbu
	}
	if unicode.Is(unicode.Linear_A, rune2) {
		return unicode.Linear_A
	}
	if unicode.Is(unicode.Linear_B, rune2) {
		return unicode.Linear_B
	}
	if unicode.Is(unicode.Lisu, rune2) {
		return unicode.Lisu
	}
	if unicode.Is(unicode.Lycian, rune2) {
		return unicode.Lycian
	}
	if unicode.Is(unicode.Lydian, rune2) {
		return unicode.Lydian
	}
	if unicode.Is(unicode.Mahajani, rune2) {
		return unicode.Mahajani
	}
	if unicode.Is(unicode.Makasar, rune2) {
		return unicode.Makasar
	}
	if unicode.Is(unicode.Malayalam, rune2) {
		return unicode.Malayalam
	}
	if unicode.Is(unicode.Mandaic, rune2) {
		return unicode.Mandaic
	}
	if unicode.Is(unicode.Manichaean, rune2) {
		return unicode.Manichaean
	}
	if unicode.Is(unicode.Marchen, rune2) {
		return unicode.Marchen
	}
	if unicode.Is(unicode.Masaram_Gondi, rune2) {
		return unicode.Masaram_Gondi
	}
	if unicode.Is(unicode.Medefaidrin, rune2) {
		return unicode.Medefaidrin
	}
	if unicode.Is(unicode.Meetei_Mayek, rune2) {
		return unicode.Meetei_Mayek
	}
	if unicode.Is(unicode.Mende_Kikakui, rune2) {
		return unicode.Mende_Kikakui
	}
	if unicode.Is(unicode.Meroitic_Cursive, rune2) {
		return unicode.Meroitic_Cursive
	}
	if unicode.Is(unicode.Meroitic_Hieroglyphs, rune2) {
		return unicode.Meroitic_Hieroglyphs
	}
	if unicode.Is(unicode.Miao, rune2) {
		return unicode.Miao
	}
	if unicode.Is(unicode.Modi, rune2) {
		return unicode.Modi
	}
	if unicode.Is(unicode.Mongolian, rune2) {
		return unicode.Mongolian
	}
	if unicode.Is(unicode.Mro, rune2) {
		return unicode.Mro
	}
	if unicode.Is(unicode.Multani, rune2) {
		return unicode.Multani
	}
	if unicode.Is(unicode.Myanmar, rune2) {
		return unicode.Myanmar
	}
	if unicode.Is(unicode.Nabataean, rune2) {
		return unicode.Nabataean
	}
	if unicode.Is(unicode.Nandinagari, rune2) {
		return unicode.Nandinagari
	}
	if unicode.Is(unicode.New_Tai_Lue, rune2) {
		return nil
	}
	if unicode.Is(unicode.Newa, rune2) {
		return nil
	}
	if unicode.Is(unicode.Nko, rune2) {
		return nil
	}
	if unicode.Is(unicode.Nushu, rune2) {
		return nil
	}
	if unicode.Is(unicode.Nyiakeng_Puachue_Hmong, rune2) {
		return nil
	}
	if unicode.Is(unicode.Ogham, rune2) {
		return nil
	}
	if unicode.Is(unicode.Ol_Chiki, rune2) {
		return nil
	}
	if unicode.Is(unicode.Old_Hungarian, rune2) {
		return nil
	}
	if unicode.Is(unicode.Old_Italic, rune2) {
		return nil
	}
	if unicode.Is(unicode.Old_North_Arabian, rune2) {
		return nil
	}
	if unicode.Is(unicode.Old_Permic, rune2) {
		return nil
	}
	if unicode.Is(unicode.Old_Persian, rune2) {
		return nil
	}
	if unicode.Is(unicode.Old_Sogdian, rune2) {
		return nil
	}
	if unicode.Is(unicode.Old_South_Arabian, rune2) {
		return nil
	}
	if unicode.Is(unicode.Old_Turkic, rune2) {
		return nil
	}
	if unicode.Is(unicode.Oriya, rune2) {
		return nil
	}
	if unicode.Is(unicode.Osage, rune2) {
		return nil
	}
	if unicode.Is(unicode.Osmanya, rune2) {
		return nil
	}
	if unicode.Is(unicode.Pahawh_Hmong, rune2) {
		return nil
	}
	if unicode.Is(unicode.Palmyrene, rune2) {
		return nil
	}
	if unicode.Is(unicode.Pau_Cin_Hau, rune2) {
		return nil
	}
	if unicode.Is(unicode.Phags_Pa, rune2) {
		return nil
	}
	if unicode.Is(unicode.Phoenician, rune2) {
		return nil
	}
	if unicode.Is(unicode.Psalter_Pahlavi, rune2) {
		return nil
	}
	if unicode.Is(unicode.Rejang, rune2) {
		return nil
	}
	if unicode.Is(unicode.Runic, rune2) {
		return nil
	}
	if unicode.Is(unicode.Samaritan, rune2) {
		return nil
	}
	if unicode.Is(unicode.Saurashtra, rune2) {
		return nil
	}
	if unicode.Is(unicode.Sharada, rune2) {
		return nil
	}
	if unicode.Is(unicode.Shavian, rune2) {
		return nil
	}
	if unicode.Is(unicode.Siddham, rune2) {
		return nil
	}
	if unicode.Is(unicode.SignWriting, rune2) {
		return nil
	}
	if unicode.Is(unicode.Sinhala, rune2) {
		return nil
	}
	if unicode.Is(unicode.Sogdian, rune2) {
		return nil
	}
	if unicode.Is(unicode.Sora_Sompeng, rune2) {
		return nil
	}
	if unicode.Is(unicode.Soyombo, rune2) {
		return nil
	}
	if unicode.Is(unicode.Sundanese, rune2) {
		return nil
	}
	if unicode.Is(unicode.Syloti_Nagri, rune2) {
		return nil
	}
	if unicode.Is(unicode.Syriac, rune2) {
		return nil
	}
	if unicode.Is(unicode.Tagalog, rune2) {
		return nil
	}
	if unicode.Is(unicode.Tagbanwa, rune2) {
		return nil
	}
	if unicode.Is(unicode.Tai_Le, rune2) {
		return nil
	}
	if unicode.Is(unicode.Tai_Tham, rune2) {
		return nil
	}
	if unicode.Is(unicode.Tai_Viet, rune2) {
		return nil
	}
	if unicode.Is(unicode.Takri, rune2) {
		return nil
	}
	if unicode.Is(unicode.Tamil, rune2) {
		return nil
	}
	if unicode.Is(unicode.Tangut, rune2) {
		return nil
	}
	if unicode.Is(unicode.Telugu, rune2) {
		return nil
	}
	if unicode.Is(unicode.Thaana, rune2) {
		return nil
	}
	if unicode.Is(unicode.Thai, rune2) {
		return nil
	}
	if unicode.Is(unicode.Tibetan, rune2) {
		return nil
	}
	if unicode.Is(unicode.Tifinagh, rune2) {
		return nil
	}
	if unicode.Is(unicode.Tirhuta, rune2) {
		return nil
	}
	if unicode.Is(unicode.Ugaritic, rune2) {
		return nil
	}
	if unicode.Is(unicode.Vai, rune2) {
		return nil
	}
	if unicode.Is(unicode.Wancho, rune2) {
		return nil
	}
	if unicode.Is(unicode.Warang_Citi, rune2) {
		return nil
	}
	if unicode.Is(unicode.Yezidi, rune2) {
		return nil
	}
	if unicode.Is(unicode.Yi, rune2) {
		return nil
	}
	if unicode.Is(unicode.Zanabazar_Square, rune2) {
		return nil
	}
	return nil
}
