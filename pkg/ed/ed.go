package ed

import (
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"os"
	"os/exec"
	"strings"
)

// Edit writes the supplied content to a file defined by path,
// then opens an editor on it.
// It then reads the contents back from the file,
// and deletes the file.
// The editor is the name of the editor program the user
// wants to use.  It must be invokable from the command line.
func Edit(s string, editor string, path string, isTemplate bool) (string, error) {
	v := s
	err := ltfile.WriteFile(path, s)
	if err != nil {
		return s, err
	}
	cmd := exec.Command(editor, ltfile.ToSysPath(path))
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Run()
	v, err = ltfile.GetFileContent(path)
	if err != nil {
		return s, err
	}
	if !isTemplate {
		// get rid of any unwanted control sequences
		v = strings.ReplaceAll(v, "\n", "")
		v = strings.ReplaceAll(v, "\r", "")
		v = strings.ReplaceAll(v, "\t", " ")
	}
	err = ltfile.DeleteFile(path)
	return v, err
}
