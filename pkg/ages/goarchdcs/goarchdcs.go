package goarchdcs

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/liturgiko/doxa/pkg/atempl"
	"github.com/liturgiko/doxa/pkg/enums/books"
	"github.com/liturgiko/doxa/pkg/enums/calendarTypes"
	"github.com/liturgiko/doxa/pkg/ldp"
	"github.com/liturgiko/doxa/pkg/resolver"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/valuemap"
	"io"
	"net/http"
	"net/url"
	"path"
	"sort"
	"strconv"
	"strings"
	"time"
)

type Scraper struct {
	BasePath             string
	ServiceIndexJsonPath string
	WhenExtracted        string
	FilterFromDate       time.Time
	FilterToDate         time.Time
	FilterServiceTypes   []string
	Layouts              map[string]*atempl.TableLayout
	Resolver             resolver.Resolver
	TableLayouts         []*atempl.TableLayout
	ResolvedTKs          *valuemap.ValueMapper
	MetaTemplates        []*MetaTemplate
	Records              map[string]map[string]string
	RecordsTkAcronym     map[string]map[string]*Record // e.g. RecordsTkAcronym["actors:Priest"]["en"] returns Record{Id: "en_us_dedes/actors:Priest", Value: "Priest"}
	Titles               map[string]string
	TopicKeys            map[string]*TkUsage
}
type Record struct {
	Id    string
	Value string
}

// TODO: change layouts to be what the resolver needs

func NewScraper(basePath,
	serviceIndexJsonPath string,
	resolver resolver.Resolver,
	layouts map[string]*atempl.TableLayout,
	genLangs []*atempl.TableLayout,
	fromDate time.Time,
	toDate time.Time,
	titles map[string]string,
	serviceTypes []string) (*Scraper, error) {
	var err error
	s := new(Scraper)
	s.Resolver = resolver
	s.Layouts = layouts
	s.TableLayouts = genLangs
	s.FilterFromDate = fromDate
	s.FilterToDate = toDate
	s.FilterServiceTypes = serviceTypes
	s.Titles = titles
	s.WhenExtracted = time.Now().Format("Monday, 02-Jan-06 15:04:05 MST")
	s.BasePath = basePath
	if strings.HasPrefix(serviceIndexJsonPath, basePath) {
		s.ServiceIndexJsonPath = serviceIndexJsonPath
	} else {
		s.ServiceIndexJsonPath, err = url.JoinPath(basePath, serviceIndexJsonPath)
		if err != nil {
			return nil, err
		}
	}
	s.Records = make(map[string]map[string]string)
	s.TopicKeys = make(map[string]*TkUsage)
	s.RecordsTkAcronym = make(map[string]map[string]*Record)
	s.ResolvedTKs = valuemap.NewValueMapper()
	return s, nil
}

type TkUsage struct {
	TopicKey            string // it is redundant to Topic, Key, but provided for convenience
	Topic               string
	Key                 string
	Source              books.Book // liturgical book from which it comes, e.g. menaion
	NbrServicesOccursIn int
	Occurrences         int       // number of times used in these services
	FirstUseDate        time.Time // date it is first used
	FirstUseType        string    // type of service first used in
	Services            []*ServiceUsage
}

func NewTkUsage(topicKey string) (*TkUsage, error) {
	tk := new(TkUsage)
	parts := strings.Split(topicKey, ":")
	if len(parts) != 2 {
		return nil, fmt.Errorf("could not split %s into 2 parts using : as delimiter", topicKey)
	}
	tk.TopicKey = topicKey
	tk.Topic = parts[0]
	tk.Key = parts[1]
	tk.Source = tk.SetSource()
	return tk, nil
}
func (tku *TkUsage) SortUsage() {
	sort.Slice(tku.Services, func(i, j int) bool {
		return tku.Services[i].FormattedDate() < tku.Services[j].FormattedDate()
	})
}

type ServiceInfo struct {
	BasePath         string
	ServicePath      string
	Url              string
	Date             string
	LDP              *ldp.LDP
	LanguageAcronyms string
	Type             string // e.g. Liturgy, Matins, Vespers, etc.
}

func (si *ServiceInfo) FormattedDate() string {
	return si.LDP.TheDay.Format("2006-01-02")
}

type ServiceUsage struct {
	ServiceInfo
	Occurrences int // number of times used in this service
}

func NewServiceUsage(serviceInfo *ServiceInfo, occurrences int) *ServiceUsage {
	su := new(ServiceUsage)
	su.BasePath = serviceInfo.BasePath
	su.Date = serviceInfo.Date
	su.LanguageAcronyms = serviceInfo.LanguageAcronyms
	su.LDP = serviceInfo.LDP
	su.ServicePath = serviceInfo.ServicePath
	su.Type = serviceInfo.Type
	su.Url = serviceInfo.Url
	su.Occurrences = occurrences
	return su
}
func (su *ServiceUsage) SortKeyDateServiceType() string {
	return fmt.Sprintf("%s%s", su.Date, su.Type)
}
func (s *Scraper) SerializeToTabSeparatedValues(pathOut string) error {
	var rows []string
	for _, u := range s.TopicKeys {
		var firstUseService string
		var found bool
		if firstUseService, found = s.Titles[u.FirstUseType]; !found {
			firstUseService = u.FirstUseType
		}
		sb := strings.Builder{}
		sb.WriteString(fmt.Sprintf("%s\t%s\t%s\t%s\t%s",
			u.FirstUseDate.Format("2006-01-02"),
			u.FirstUseDate.Format("Monday"),
			firstUseService,
			u.Topic,
			u.Key,
		))
		tkv := s.ResolveTopicKey(fmt.Sprintf("%s/%s", u.Topic, u.Key))
		for _, l := range tkv.Values {
			kvp := l.GetKV()
			sb.WriteString(fmt.Sprintf("\t%s", kvp.ID))
			sb.WriteString(fmt.Sprintf("\t%s", kvp.Value))
		}
		sb.WriteString("\n")
		rows = append(rows, sb.String())
		sb.Reset()
	}
	// sort the rows
	sort.Strings(rows)
	// write spreadsheet column headers
	sb := strings.Builder{}
	sb.WriteString("First Use Date\tFirst Use DOW\tFirst Use Service\tTopic\tKey")
	for _, layout := range s.Layouts {
		sb.WriteString(fmt.Sprintf("\t%s - ID", layout.Acronym))
		sb.WriteString(fmt.Sprintf("\t%s - Value", layout.Acronym))
	}
	sb.WriteString("\n")
	// write spreadsheet rows
	for _, row := range rows {
		sb.WriteString(row)
	}
	dirOut, fileOut := path.Split(pathOut)
	fileOut = fmt.Sprintf("%s-%s", strings.ReplaceAll(time.Now().Format(time.RFC3339), ":", "-"), fileOut)
	pathOut = path.Join(dirOut, fileOut)
	ltfile.WriteFile(pathOut, sb.String())
	return nil
}
func (s *Scraper) ResolveTopicKey(topicKey string) *atempl.TKVal {
	if valueIndexes, ok := s.ResolvedTKs.Get(topicKey); ok {
		return valueIndexes
	} else {
		tkVal := s.Resolver.Values(topicKey, s.TableLayouts)
		s.ResolvedTKs.Add(topicKey, tkVal)
		return tkVal
	}
	return nil
}
func (s *Scraper) AddRecords(recMap map[string]map[string]string) error {
	var exists bool
	var langK string
	var langMap map[string]string
	for langK, langMap = range recMap {
		var summaryMap map[string]string
		if summaryMap, exists = s.Records[langK]; !exists {
			s.Records[langK] = langMap
			continue
		}
		for k, v := range langMap {
			if _, exists = summaryMap[k]; !exists {
				summaryMap[k] = v
			}
		}
		s.Records[langK] = summaryMap
	}
	return nil
}
func (s *Scraper) RecordTkUsage(serviceInfo *ServiceInfo, tkMap map[string]int) error {
	for k, v := range tkMap {
		var tkUsage *TkUsage
		var exists bool
		var err error
		if tkUsage, exists = s.TopicKeys[k]; !exists {
			tkUsage, err = NewTkUsage(k)
			if err != nil {
				return err
			}
		}
		tkUsage.NbrServicesOccursIn++
		tkUsage.Occurrences = tkUsage.Occurrences + v
		serviceUsage := NewServiceUsage(serviceInfo, tkUsage.Occurrences)
		tkUsage.Services = append(tkUsage.Services, serviceUsage)
		s.TopicKeys[k] = tkUsage
	}
	return nil
}
func (tku *TkUsage) SetSource() books.Book {
	if strings.HasPrefix(tku.Topic, "le.") {
		if strings.HasPrefix(tku.Topic, "le.ep") {
			return books.Apostolos
		}
		if strings.HasPrefix(tku.Topic, "le.go") {
			return books.Evangelion
		}
		if strings.HasPrefix(tku.Topic, "le.pr") {
			return books.Prophetologion
		}
		if strings.HasPrefix(tku.Topic, "le.ps") {
			return books.Psalter
		}
	}
	if strings.HasPrefix(tku.Topic, "me.") {
		return books.Menaia
	}
	if strings.HasPrefix(tku.Topic, "oc.") {
		return books.Octeochos
	}
	if strings.HasPrefix(tku.Topic, "tr.") {
		return books.Triodion
	}
	if strings.HasPrefix(tku.Topic, "pe.") {
		return books.Pentecostarion
	}
	return books.Euchologion
}
func NewServiceInfo(basePath, servicePath string) (*ServiceInfo, error) {
	var err error
	s := new(ServiceInfo)
	s.BasePath = basePath
	s.ServicePath = servicePath
	s.Url, err = url.JoinPath(s.BasePath, s.ServicePath)
	if err != nil {
		return nil, fmt.Errorf("could not join url paths %s and %s: %v", s.BasePath, s.ServicePath, err)
	}
	var p string
	p, err = url.JoinPath(s.BasePath, servicePath)
	if err != nil {
		return nil, err
	}
	s.LDP, s.Type, s.LanguageAcronyms, err = ParseServicePath(p)
	if err != nil {
		return nil, err
	}
	s.Date = s.FormattedDate()
	return s, nil
}

type MetaTemplate struct {
	BasePath    string
	ServicePath string
	ServiceType string
	LangAcronym string
	LDP         *ldp.LDP
	Records     map[string]map[string]string
	TopicKeys   map[string]int
	MetaRows    []*atempl.MetaRow
}

func NewMetaTemplate() *MetaTemplate {
	m := new(MetaTemplate)
	m.Records = make(map[string]map[string]string)
	m.TopicKeys = make(map[string]int)
	return m
}

func (s *Scraper) GetMetaTemplates() error {
	services, err := s.ServicesFromJsonIndex()
	if err != nil {
		return err
	}
	if services == nil {
		return fmt.Errorf("could not read services json file")
	}
	for _, service := range services {
		if strings.Contains(service, "gr-en") {
			var serviceInfo *ServiceInfo
			serviceInfo, err = NewServiceInfo(s.BasePath, service)
			if err != nil {
				return err
			}
			if !s.isRequestedServiceType(serviceInfo.Type) {
				continue
			}
			if !s.inDateRange(serviceInfo.LDP.TheDay) {
				continue
			}
			var m *MetaTemplate
			m, err = s.GetMetaTemplate(serviceInfo)
			if err != nil {
				return err
			}
			s.MetaTemplates = append(s.MetaTemplates, m)
			s.RecordTkUsage(serviceInfo, m.TopicKeys)
			s.AddRecords(m.Records)
		}
	}
	s.SortResults()
	return nil
}
func (s *Scraper) inDateRange(t time.Time) bool {
	return t.Equal(s.FilterFromDate) || t.Equal(s.FilterToDate) || t.After(s.FilterFromDate) && t.Before(s.FilterToDate)
}
func (s *Scraper) isRequestedServiceType(service string) bool {
	for _, st := range s.FilterServiceTypes {
		if st == "*" || st == service {
			return true
		}
	}
	return false
}
func (s *Scraper) SortResults() {
	for k, v := range s.TopicKeys {
		v.SortUsage()
		v.FirstUseDate = v.Services[0].LDP.TheDay
		v.FirstUseType = v.Services[0].Type
		s.TopicKeys[k] = v
	}
}
func (s *Scraper) GetMetaTemplate(serviceInfo *ServiceInfo) (*MetaTemplate, error) {
	m := NewMetaTemplate()
	m.BasePath = serviceInfo.BasePath
	m.ServicePath = serviceInfo.ServicePath
	var p string
	var err error
	p, err = url.JoinPath(m.BasePath, m.ServicePath)
	if err != nil {
		return nil, err
	}
	m.LDP = serviceInfo.LDP
	m.ServiceType = serviceInfo.Type
	m.LangAcronym = serviceInfo.LanguageAcronyms
	resp, err := http.Get(p)
	if err != nil {
		return nil, fmt.Errorf("GET error: %v", err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("response status error: %s", resp.Status)
	}
	content, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(string(content)))
	if err != nil {
		return nil, err
	}
	// extract all the kvp spans from both sides
	doc.Find("span.kvp").Each(func(i int, s *goquery.Selection) {
		rec := new(atempl.MetaSpan)
		var ok bool
		if rec.TopicKey, ok = s.Attr("data-key"); ok {
		}
		var c int
		var exists bool
		var lang, doxaId, tk string
		lang, doxaId, tk, err = ParseId(rec.TopicKey)
		if err != nil || len(lang) == 0 {
			// ignore for now
			// return // caused by so-called dummy keys
		}
		if c, exists = m.TopicKeys[tk]; !exists {
			// do nothing, just needed to retrieve c
		}
		rec.Literal = s.Text()
		if len(lang) > 0 && len(rec.Literal) > 0 {
			if strings.HasPrefix(rec.Literal, "\n\n") {
				// ignore
			} else {
				m.TopicKeys[tk] = c + 1
				var colMap map[string]string
				if colMap, exists = m.Records[lang]; !exists {
					colMap = make(map[string]string)
					colMap[doxaId] = rec.Literal
					m.Records[lang] = colMap
				} else {
					if _, exists = colMap[doxaId]; !exists {
						colMap[doxaId] = rec.Literal
						m.Records[lang] = colMap
					}
				}
			}
		}
	})
	// extract the formatting information right column.  It will be the same for the left.
	doc.Find("td.rightCell").Each(func(i int, td *goquery.Selection) {
		td.Find("p").Each(func(i int, para *goquery.Selection) {
			mRow := new(atempl.MetaRow)
			var ok bool
			if mRow.Class, ok = para.Attr("class"); !ok {
				mRow.Class = "unknown"
			}
			var metaSpans []*atempl.MetaSpan
			mRow.MetaSpans = GetMetaSpans(para.Children(), metaSpans)
			m.MetaRows = append(m.MetaRows, mRow)
		})
	})
	return m, nil
}

// ParseId for an ALWB ID, e.g. prayers_en_US_goa|pet22, returns language acronym, doxa ID, and topic:key
func ParseId(id string) (string, string, string, error) {
	// prayers_en_US_goa|pet22
	pipeParts := strings.Split(id, "|")
	if len(pipeParts) != 2 {
		return "", "", "", fmt.Errorf("could not split %s using | into two segments", id)
	}
	underscoreParts := strings.Split(pipeParts[0], "_")
	if len(underscoreParts) != 4 {
		return "", "", "", fmt.Errorf("could not split %s using _ into four segments", pipeParts[0])
	}
	return underscoreParts[1], fmt.Sprintf("%s_%s_%s/%s:%s", underscoreParts[1], strings.ToLower(underscoreParts[2]), underscoreParts[3], underscoreParts[0], pipeParts[1]), fmt.Sprintf("%s:%s", underscoreParts[0], pipeParts[1]), nil
}

func GetMetaSpans(s *goquery.Selection, mSpans []*atempl.MetaSpan) []*atempl.MetaSpan {
	mSpan := new(atempl.MetaSpan)
	mSpan.Class, _ = s.Attr("class")
	if mSpan.Class == "kvp" {
		var ok bool
		if mSpan.TopicKey, ok = s.Attr("data-key"); ok {
		}
	}
	children := s.Children()
	if children.Nodes != nil {
		var childrenSpans []*atempl.MetaSpan
		mSpan.ChildSpans = GetMetaSpans(children, childrenSpans)
	}
	mSpans = append(mSpans, mSpan)
	return mSpans
}
func ParseServicePath(p string) (*ldp.LDP, string, string, error) {
	// h/s/2022/11/01/ma/gr-en/index.html
	parts := strings.Split(p, "h/s/")
	if len(parts) != 2 {
		return nil, "", "", fmt.Errorf("could not parse %s into 2 segments using h/s/", p)
	}
	parts = strings.Split(parts[1], "/")
	if len(parts) != 6 {
		return nil, "", "", fmt.Errorf("could not parse %s into 6 segments using /", parts[1])
	}
	var year, month, day int
	var err error
	year, err = strconv.Atoi(parts[0])
	if err != nil {
		return nil, "", "", fmt.Errorf("could not convert %s to int: %v", parts[0], err)
	}
	month, err = strconv.Atoi(parts[1])
	if err != nil {
		return nil, "", "", fmt.Errorf("could not convert %s to int: %v", parts[1], err)
	}
	day, err = strconv.Atoi(parts[2])
	if err != nil {
		return nil, "", "", fmt.Errorf("could not convert %s to int: %v", parts[2], err)
	}
	var LDP ldp.LDP
	LDP, err = ldp.NewLDPYMD(year, month, day, calendarTypes.Gregorian)
	if err != nil {
		return nil, "", "", fmt.Errorf("could not convert %s into LDP: %v", p, err)
	}
	return &LDP, parts[3], parts[4], nil
}

// ServicesFromJsonIndex returns the url paths to each service
// found in the servicesindex.json file on the dcs.goarch.org site.
func (s *Scraper) ServicesFromJsonIndex() ([]string, error) {
	var result []string
	resp, err := http.Get(s.ServiceIndexJsonPath)

	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		bodyBytes, err := io.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}
		json := string(bodyBytes)
		hrefParts := strings.Split(json, "\"href\": \"")
		for _, href := range hrefParts {
			parts := strings.Split(href, "\"")
			if strings.HasPrefix(parts[0], "h/s") {
				result = append(result, parts[0])
			}
		}
	}
	return result, nil
}

type Archive struct {
	Url       string
	Year      string
	MonthFrom string
	MonthTo   string
}

var Archives = []Archive{
	Archive{"https://dcs.goarch.org/goa_2022_0912/dcs", "2022", "09", "12"},
	Archive{"https://dcs.goarch.org/goa_2022_0508/dcs", "2022", "05", "08"},
	Archive{"https://dcs.goarch.org/goa_2022_0104/dcs", "2022", "01", "04"},
	Archive{"https://dcs.goarch.org/goa_2021_0812/dcs", "2021", "08", "12"},
}
