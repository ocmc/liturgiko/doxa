package tags

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"strings"
	"text/scanner"
)

type Tag struct {
	Map map[string]string
}

func NewTag() *Tag {
	t := new(Tag)
	t.Map = make(map[string]string)
	return t
}
func (b *Tag) addDotValue(key, value string) {
	b.Map[key] = fmt.Sprintf(".%s", value)
}
func (b *Tag) addParaValue(key, value string) {
	b.Map[key] = fmt.Sprintf("p.%s", value)
}
func (b *Tag) addSpanValue(key, value string) {
	b.Map[key] = fmt.Sprintf("span.%s", value)
}
func (b *Tag) Load(filepath, filename string) error {
	switch filename {
	case "iTags", "roles":
		// iTags will be converted to span.{classname}
		// An iTag entry looks like this:
		// bl = "emphasis,black"
		// We will convert it to span.black.
		// roles are also converted to span.{classname}
		// A role entry looks like this:
		// desig = "designation"
		// We will convert it to span.designation
		lines, err := ltfile.GetFileLines(filepath)
		if err != nil {
			return fmt.Errorf("%s does not exist: %v", filepath, err)
		}
		for _, line := range lines {
			// skip over the topic name
			if strings.HasPrefix(line, "A_Resource_Whose_Name") {
				continue
			}
			var s scanner.Scanner
			s.Init(strings.NewReader(line))
			s.Whitespace = 1<<':' | 1<<'-' | 1<<'\t' | 1<<' ' // treat these as white space

			currentState := NOP
			var key string

			for tok := s.Scan(); tok != scanner.EOF; tok = s.Scan() {
				token := s.TokenText()

				switch token {
				case "=":
					{
						currentState = GOTEQUAL
					}
				default:
					switch currentState {
					case GOTEQUAL:
						{
							// process the right side of the equal sign.
							// get rid of "emphasis", we just want the classname, e.g.
							//bl = "emphasis,black", we just want "black"
							parts := strings.Split(token, ",")
							if len(parts) == 2 {
								value := parts[1]
								if filename == "roles" {
									b.addParaValue(key, value[:len(value)-1])
								} else {
									b.addSpanValue(key, value[:len(value)-1])
								}
							} else {
								if strings.HasPrefix(token, "\"") {
									token = token[1:]
								}
								if strings.HasSuffix(token, "\"") {
									token = token[0 : len(token)-1]
								}
								if filename == "roles" {
									b.addParaValue(key, token)
								} else {
									b.addSpanValue(key, token)
								}
							}
						}
					default:
						key = token
					}
				}
			}
		}
	}
	return nil
}

const (
	NOP = iota
	GOTEQUAL
)
