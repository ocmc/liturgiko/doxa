package atem2lml

import (
	"fmt"
	"github.com/antlr/antlr4/runtime/Go/antlr/v4"
	"strings"
)

type ParseError struct {
	TemplateID string
	Line       int
	Column     int
	Message    string
}

// String returns an error formatted as line:column:message
func (e *ParseError) String() string {
	return fmt.Sprintf("\n%s %d:%d \n\t%s", e.TemplateID, e.Line, e.Column, e.Message)
}

// StringVerbose returns an error formatted as Template templateID Line number Column number: message
// e.g. Template a/b Line 1 Column 3: mismatched input '==' expecting '='
func (e *ParseError) StringVerbose() string {
	message := strings.ReplaceAll(e.Message, "{", "")
	message = strings.ReplaceAll(message, "}", "")
	message = strings.ReplaceAll(message, "parse error: ", "\n\t")
	return fmt.Sprintf("\nError in Template \n\t%s %d:%d \n\t%s", e.TemplateID, e.Line, e.Column, message)
}

type ErrorListener struct {
	antlr.ErrorListener
	TemplateID string
	Errors     []ParseError
}

func NewErrorListener(templateID string) *ErrorListener {
	l := new(ErrorListener)
	l.TemplateID = templateID
	return l
}
func (l *ErrorListener) AddError(templateID string, line, column int, msg string) {
	theError := ParseError{l.TemplateID, line, column, msg}
	l.Errors = append(l.Errors, theError)
}
func (l *ErrorListener) SyntaxError(recognizer antlr.Recognizer, offendingSymbol interface{}, line, column int, msg string, e antlr.RecognitionException) {
	l.AddError(l.TemplateID, line, column, msg)
}
func (l *ErrorListener) ReportAmbiguity(recognizer antlr.Parser, dfa *antlr.DFA, startIndex, stopIndex int, exact bool, ambigAlts *antlr.BitSet, configs antlr.ATNConfigSet) {
	//	fmt.Println("Unhandled ReportAmbiguity exception")
}
func (l *ErrorListener) ReportAttemptingFullContext(recognizer antlr.Parser, dfa *antlr.DFA, startIndex, stopIndex int, conflictingAlts *antlr.BitSet, configs antlr.ATNConfigSet) {
	//	fmt.Println("Unhandled ReportAttemptingFullContext exception")
}
func (l *ErrorListener) ReportContextSensitivity(recognizer antlr.Parser, dfa *antlr.DFA, startIndex, stopIndex, prediction int, configs antlr.ATNConfigSet) {
	//	fmt.Println("Unhandled ReportContextSensitivity exception")
}
