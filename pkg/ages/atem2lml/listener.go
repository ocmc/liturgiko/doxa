// Package atem2lml converts ALWB atem files to lml template file content.
// In ALWB, imports provide the scope of identifiers,
// but in lml identifiers must be fully qualified.  There are
// no imports in lml (the liturgical markup language).
// The most complex part of the conversion is handling
// atem Section...End-Section and Insert_section...End-Insert.
// In ALWB, templates can have sections that can be referenced
// both internal to the containing template and external.
// The identifier of an Insert_section is resolved using
// imports and the scopes of (nested) sections.
// In LML there is no concept of a section.
// Therefore, each atem section becomes its own LML template.
// And, each atem Insert_section needs to be resolved and
// written in LML as a fully qualified path.
// Multiple atem instances are created, one for the main template
// and one for each section.  The content of each atem is used
// to write an lml file.
package atem2lml

import (
	"fmt"
	"github.com/antlr/antlr4/runtime/Go/antlr/v4"
	"github.com/emirpasic/gods/stacks/arraystack"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/ages/tags"
	"github.com/liturgiko/doxa/pkg/appender"
	"github.com/liturgiko/doxa/pkg/enums/templateTypes"
	"github.com/liturgiko/doxa/pkg/utils/stack"
	atem "gitlab.com/ocmc/liturgiko/lml/alwb/golang/parser"
	"path"
	"regexp"
	"strconv"
	"strings"
)

var debug = true

func checkDebug() {
	if debug {
		fmt.Sprintf("TODO: remove debug func, var, and calls")
	}
}

type AbstractTemplate struct {
	TemplatePathOut string
	TemplateIdPath  string
	Contents        strings.Builder
	SuffixText      strings.Builder
}

type AtemListener struct {
	atem.BaseAtemListener
	Ctx                     *ListenerContext
	AtemMap                 *AtemMapManager
	KeyPrefixMap            map[string]string // Fr. S renamed all actor keys to have ac. in front of them.  This is a key prefix.
	ImportMapAres           map[string]string
	ImportMapAtem           map[string]string
	ITags                   tags.Tag
	KeyMap                  *map[string][]*kvs.KeyPath
	TemplateDataMap         map[string]*TemplateData
	TemplateIndex           *TemplateIndex
	TemplateIdPath          string
	TemplatePathOut         string
	TemplateStatus          string
	TemplateIndexTitleCodes []string
	TemplateType            templateTypes.TemplateType
}

func NewAtemListener(templatePathOut, templateIdPath string, keyMap *map[string][]*kvs.KeyPath, ITags tags.Tag, templateIndex *TemplateIndex, templateDataMap map[string]*TemplateData) (*AtemListener, error) {
	l := new(AtemListener)
	l.AtemMap = NewAtemMapManager(templatePathOut, templateIdPath)
	l.ImportMapAres = make(map[string]string)
	l.ImportMapAtem = make(map[string]string)
	l.ITags = ITags
	l.KeyMap = keyMap
	l.KeyPrefixMap = make(map[string]string)
	l.TemplateDataMap = templateDataMap
	l.TemplateIdPath = templateIdPath
	fileName := path.Base(l.TemplateIdPath)
	if strings.HasPrefix(fileName, "se.") {
		l.TemplateType = templateTypes.Service
	} else if strings.HasPrefix(fileName, "bk.") {
		l.TemplateType = templateTypes.Book
	} else {
		l.TemplateType = templateTypes.Block
	}
	l.SetIndexTitleCodes(l.TemplateIdPath)
	// 2023-04-27 MAC what I wanted to do is to normalize the template filenames,
	// so they can be parsed into titles for the indexer.
	// However, it is complicated because there are previous passes through the ATEM
	// templates that build maps of template names, inserts, etc.  That would
	// also have to be changed.
	//dir := filepath.Dir(l.TemplateIdPath)
	//if l.TemplateType == templateTypes.Book {
	//	l.TemplateIdPath = fmt.Sprintf("%s/bk.%s", dir, strings.Join(l.TemplateIndexTitleCodes, "."))
	//}
	l.TemplateIndex = templateIndex
	l.TemplatePathOut = templatePathOut
	l.TemplateStatus = "na" // ensure a value is set in case status missing in template
	l.Ctx = NewListenerContext(templateIdPath)
	return l, nil
}

var Indent = "  "

func (l *AtemListener) SetIndexTitleCodes(templatePath string) {
	if l.TemplateType == templateTypes.Block {
		return
	}
	var codes []string
	var inBooksFolder bool
	dir := strings.ToLower(path.Dir(templatePath))
	if strings.HasPrefix(dir, "a-templates/blocks/") {
		dir = dir[19:]
	} else if strings.HasPrefix(dir, "a-templates/books/") {
		inBooksFolder = true
		dir = dir[18:]
	}
	base := path.Base(templatePath)
	if strings.HasPrefix(base, "bk.") || strings.HasPrefix(base, "se.") {
		// it better, but be safe
		base = base[3:]
	}
	if strings.HasSuffix(base, ".atem") {
		base = base[:len(base)-5]
	}
	if inBooksFolder {
		if strings.HasPrefix(dir, "apostolos") {
			cd := "apos"
			if strings.HasPrefix(base, "ap.") {
				base = "apos." + base[3:] // ap means apolytikion elsewhere
			}
			if strings.HasPrefix(base, fmt.Sprintf("%s.", cd)) ||
				strings.Contains(base, fmt.Sprintf(".%s.", cd)) ||
				strings.HasSuffix(base, fmt.Sprintf(".%s", cd)) {
				// ignore
			} else {
				codes = append(codes, cd)
			}
		}
		if strings.HasPrefix(dir, "euchologion") {
			cd := "eu"
			if strings.HasPrefix(base, fmt.Sprintf("%s.", cd)) ||
				strings.Contains(base, fmt.Sprintf(".%s.", cd)) ||
				strings.HasSuffix(base, fmt.Sprintf(".%s", cd)) {
				// ignore
			} else {
				codes = append(codes, cd)
			}
		}
		if strings.HasPrefix(dir, "evangelion") {
			cd := "ev"
			if strings.HasPrefix(base, fmt.Sprintf("%s.", cd)) ||
				strings.Contains(base, fmt.Sprintf(".%s.", cd)) ||
				strings.HasSuffix(base, fmt.Sprintf(".%s", cd)) {
				// ignore
			} else {
				codes = append(codes, cd)
			}
		}
		if strings.HasPrefix(dir, "heirmologion") {
			cd := "he"
			if strings.HasPrefix(base, fmt.Sprintf("%s.", cd)) ||
				strings.Contains(base, fmt.Sprintf(".%s.", cd)) ||
				strings.HasSuffix(base, fmt.Sprintf(".%s", cd)) {
				// ignore
			} else {
				codes = append(codes, cd)
			}
		}
		if strings.HasPrefix(dir, "holyweekservices") {
			cd := "hwk"
			if strings.HasPrefix(base, fmt.Sprintf("%s.", cd)) ||
				strings.Contains(base, fmt.Sprintf(".%s.", cd)) ||
				strings.HasSuffix(base, fmt.Sprintf(".%s", cd)) {
				// ignore
			} else {
				codes = append(codes, cd)
			}
		}
		if strings.HasPrefix(dir, "horologion") {
			cd := "ho"
			if strings.HasPrefix(base, fmt.Sprintf("%s.", cd)) ||
				strings.Contains(base, fmt.Sprintf(".%s.", cd)) ||
				strings.HasSuffix(base, fmt.Sprintf(".%s", cd)) {
				// ignore
			} else {
				codes = append(codes, cd)
			}
		}
		if strings.HasPrefix(dir, "menaion") {
			cd := "me"
			if strings.HasPrefix(base, fmt.Sprintf("%s.", cd)) ||
				strings.Contains(base, fmt.Sprintf(".%s.", cd)) ||
				strings.HasSuffix(base, fmt.Sprintf(".%s", cd)) {
				// ignore
			} else {
				codes = append(codes, cd)
			}
		}
		if strings.HasPrefix(dir, "octoechos") {
			cd := "oc"
			if strings.HasPrefix(base, fmt.Sprintf("%s.", cd)) ||
				strings.Contains(base, fmt.Sprintf(".%s.", cd)) ||
				strings.HasSuffix(base, fmt.Sprintf(".%s", cd)) {
				// ignore
			} else {
				codes = append(codes, cd)
			}
		}
		if strings.HasPrefix(dir, "other") {
			cd := "other"
			if strings.HasPrefix(base, fmt.Sprintf("%s.", cd)) ||
				strings.Contains(base, fmt.Sprintf(".%s.", cd)) ||
				strings.HasSuffix(base, fmt.Sprintf(".%s", cd)) {
				// ignore
			} else {
				codes = append(codes, cd)
			}
		}
		if strings.HasPrefix(dir, "paschaservices") {
			cd := "pas"
			if strings.HasPrefix(base, fmt.Sprintf("%s.", cd)) ||
				strings.Contains(base, fmt.Sprintf(".%s.", cd)) ||
				strings.HasSuffix(base, fmt.Sprintf(".%s", cd)) {
				// ignore
			} else {
				codes = append(codes, cd)
			}
		}
		if strings.HasPrefix(dir, "pentecostarion") {
			cd := "pe"
			if strings.HasPrefix(base, fmt.Sprintf("%s.", cd)) ||
				strings.Contains(base, fmt.Sprintf(".%s.", cd)) ||
				strings.HasSuffix(base, fmt.Sprintf(".%s", cd)) {
				// ignore
			} else {
				codes = append(codes, cd)
			}
		}
		if strings.HasPrefix(dir, "psalter") {
			cd := "ps"
			if strings.HasPrefix(base, "psalter") ||
				strings.HasPrefix(base, fmt.Sprintf("%s.", cd)) ||
				strings.Contains(base, fmt.Sprintf(".%s.", cd)) ||
				strings.HasSuffix(base, fmt.Sprintf(".%s", cd)) {
				// ignore
			} else {
				codes = append(codes, cd)
			}
		}
		if strings.HasPrefix(dir, "servicebook") {
			cd := "sb"
			if strings.HasPrefix(base, fmt.Sprintf("%s.", cd)) ||
				strings.Contains(base, fmt.Sprintf(".%s.", cd)) ||
				strings.HasSuffix(base, fmt.Sprintf(".%s", cd)) {
				// ignore
			} else {
				codes = append(codes, cd)
			}
		}
		if strings.HasPrefix(dir, "synaxarion") {
			cd := "sy"
			if strings.HasPrefix(base, fmt.Sprintf("%s.", cd)) ||
				strings.Contains(base, fmt.Sprintf(".%s.", cd)) ||
				strings.HasSuffix(base, fmt.Sprintf(".%s", cd)) {
				// ignore
			} else {
				codes = append(codes, cd)
			}
		}
		if strings.HasPrefix(dir, "triodion") {
			cd := "tr"
			if strings.HasPrefix(base, fmt.Sprintf("%s.", cd)) ||
				strings.Contains(base, fmt.Sprintf(".%s.", cd)) ||
				strings.HasSuffix(base, fmt.Sprintf(".%s", cd)) {
				// ignore
			} else {
				codes = append(codes, cd)
			}
		}
	}
	parts := strings.Split(base, ".")
	if len(parts) == 0 {
		codes = append(codes, base)
	} else {
		for _, p := range parts {
			p = strings.TrimSpace(p)
			if len(p) > 0 {
				if p == "matinsordinary" {
					codes = append(codes, "ord")
					codes = append(codes, "matins")
					continue
				} else if p == "vespersordinary" {
					codes = append(codes, "ord")
					codes = append(codes, "vespers")
					continue
				}
				codes = append(codes, p)
			}
		}
	}
	l.TemplateIndexTitleCodes = codes
}

type ListenerContext struct {
	IndentStack                  *stack.StringStack
	StatementContext             *StatementContext
	SectionStack                 *stack.StringStack
	StatementStack               *arraystack.Stack
	TemplatePathBase             string
	TemplatePathDir              string
	DeferInsertUntilExitTemplate bool
	InsertsForTemplateExit       []string
	InHead                       bool
	PdfSettings                  *appender.Appender
}
type StatementContext struct {
	IndentStack                             *stack.StringStack
	AbstractComponentFound                  bool
	AbstractComponentWriteCloseBraceOnEnter bool
	AnchorLabel                             string
	CloseHtmlOnlyBlock                      bool
	InExists                                bool
	InExistsCase                            bool
	InExistsCaseCount                       int
	InImportBlock                           bool
	InInsertSection                         bool
	InInsertTemplate                        bool
	InPara                                  bool
	InRid                                   bool
	InRoleBlock                             bool
	InTaggedTextBlock                       bool
	InTemplateTitle                         bool
	InsertingAnchorHref                     bool
	InsertingAnchorLabel                    bool
	PrintElse                               bool
}

func (c *ListenerContext) ClearIndent() {
	c.StatementContext.IndentStack = new(stack.StringStack)
}
func (c *ListenerContext) PushIndent() {
	c.IndentStack.Push(Indent)
}
func (c *ListenerContext) PopIndent() {
	c.IndentStack.Pop()
}
func (c *ListenerContext) Indent() string {
	return c.IndentStack.Join("")
}
func (c *ListenerContext) PushStatementContext(saveIndentStack bool) {
	statementContext := new(StatementContext)
	statementContext.IndentStack = new(stack.StringStack)
	if saveIndentStack {
		c.StatementContext.IndentStack = c.IndentStack
		c.IndentStack = new(stack.StringStack)
	}
	c.StatementStack.Push(c.StatementContext)
	c.StatementContext = statementContext
}
func (c *ListenerContext) PopStatementContext() {
	if popped, ok := c.StatementStack.Pop(); ok {
		c.StatementContext = popped.(*StatementContext)
		if c.StatementContext.IndentStack.Size() > 0 {
			c.IndentStack = c.StatementContext.IndentStack
		}
	}
}

func NewListenerContext(templateIdPath string) *ListenerContext {
	c := new(ListenerContext)
	c.TemplatePathDir, c.TemplatePathBase = path.Split(templateIdPath)
	c.SectionStack = new(stack.StringStack)
	c.StatementStack = arraystack.New()
	c.StatementContext = new(StatementContext)
	c.StatementContext.IndentStack = new(stack.StringStack) // used when we need to preserve the current indentStack for a subsequent pop of the context
	c.IndentStack = new(stack.StringStack)                  // this is the one used for indents
	parts := strings.Split(templateIdPath, "/")
	for _, p := range parts {
		c.SectionStack.Push(p)
	}
	c.PdfSettings = appender.NewAppender(Indent)
	return c
}

type AtemMapManager struct {
	Map map[string]*AbstractTemplate
}

func NewAtemMapManager(templatePathOut, templateIdPath string) *AtemMapManager {
	m := new(AtemMapManager)
	m.Map = make(map[string]*AbstractTemplate)
	atem := new(AbstractTemplate)
	atem.TemplatePathOut = templatePathOut
	atem.TemplateIdPath = templateIdPath
	m.Map[templateIdPath] = atem
	return m
}

// WriteContent writes contents to contextual Atem
// The correct Atem to write to is the result
// of joining the values of the listener ctx sectionStack.
// If the atem does not exist, it is created.
func (l *AtemListener) WriteContent(text string) {
	key := l.Ctx.SectionStack.Join("/")
	var atem *AbstractTemplate
	var ok bool
	if atem, ok = l.AtemMap.Map[key]; !ok {
		atem = new(AbstractTemplate)
		path := l.ToBlock(key)
		atem.TemplateIdPath = path
		atem.TemplatePathOut = l.TemplatePathOut
		// if this is a section atem, write initial lines
		if atem.TemplateIdPath != l.TemplateIdPath {
			atem.Contents.WriteString(fmt.Sprintf("id = \"%s\"", atem.TemplateIdPath))
			atem.Contents.WriteString("\ntype = \"block\"")
			atem.Contents.WriteString(fmt.Sprintf("\nstatus = \"%s\"", l.TemplateStatus))
		}
	}
	atem.Contents.WriteString(text)
	l.AtemMap.Map[key] = atem
}
func (l *AtemListener) ToBlock(key string) string {
	parts := strings.Split(key, kvs.SegmentDelimiter)
	//	parts[0] = "Blocks" // TODO: this leaves ages atem sections as blocks in books.  Should they be in blocks?
	fileName := parts[len(parts)-1]
	if !strings.HasPrefix(fileName, "bl.") {
		fileName = "bl." + fileName
	}
	parts[len(parts)-1] = fileName
	return strings.Join(parts, kvs.SegmentDelimiter)
}
func (l *AtemListener) WriteSuffix(text string) {
	key := l.Ctx.SectionStack.Join("/")
	var atem *AbstractTemplate
	var ok bool
	if atem, ok = l.AtemMap.Map[key]; !ok {
		atem = new(AbstractTemplate)
		path := key
		atem.TemplateIdPath = path
		atem.TemplatePathOut = l.TemplatePathOut
	}
	atem.SuffixText.WriteString(text)
	l.AtemMap.Map[key] = atem
}
func (l *AtemListener) WriteSuffixToContent() {
	key := l.Ctx.SectionStack.Join("/")
	if atem, ok := l.AtemMap.Map[key]; ok {
		if atem.SuffixText.Len() > 0 {
			atem.Contents.WriteString(atem.SuffixText.String())
			atem.SuffixText = strings.Builder{}
			l.AtemMap.Map[key] = atem
		}
	}
}
func (l *AtemListener) MainContent() string {
	key := l.TemplateIdPath
	if atem, ok := l.AtemMap.Map[key]; ok {
		return atem.Contents.String()
	}
	return ""
}

type CallStack struct {
	Stack []string
}

func (s *CallStack) Push(i string) {
	s.Stack = append(s.Stack, i)
}
func (s *CallStack) Pop() string {
	var i string
	if len(s.Stack) > 0 {
		i, s.Stack = s.Stack[len(s.Stack)-1], s.Stack[:len(s.Stack)-1]
	}
	return i
}
func (s *CallStack) String() string {
	return fmt.Sprint(s.Stack)
}

// ResolvedId uses the InverseMap and import map to resolve the supplied ID.
// A resolved ID has a fully qualified path without the ltx/gr_gr_cog prefix.
func (l *AtemListener) ResolvedId(id string) (string, error) {
	if strings.Contains(id, "_gr_GR_cog.") {
		id = strings.ReplaceAll(id, "_gr_GR_cog.", "/")
		return id, nil
	}
	if strings.Contains(id, "cover.version") {
		return "pdf.covers:GE.text", nil
		//		return fmt.Sprintf("ematinscovers:E.text"), nil
	}
	if values, ok := (*l.KeyMap)[id]; ok {
		for _, kp := range values {
			if _, ok := l.ImportMapAres[kp.Dirs.Last()]; ok {
				return kp.LmlTopicKey(), nil
			}
		}
		return GetWildcard(id)
	} else {
		// try prefixing the key and see if it exists that way
		for prefix, _ := range l.KeyPrefixMap {
			if values, ok := (*l.KeyMap)[fmt.Sprintf("%s.%s", prefix, id)]; ok {
				for _, kp := range values {
					if _, ok := l.ImportMapAres[kp.Dirs.Last()]; ok {
						return kp.LmlTopicKey(), nil
					}
				}
			}
		}
		return GetWildcard(id)
	}
}

func GetWildcard(id string) (string, error) {
	from := 0
	to := 2
	if strings.HasPrefix(id, "alt1") || strings.HasPrefix(id, "alt2") || strings.HasPrefix(id, "alt3") {
		from = 5
		to = 7
	}

	switch id[from:to] {
	case "da":
		return fmt.Sprintf("da.*%s%s", kvs.IdDelimiter, id), nil
	case "eo":
		return fmt.Sprintf("eo.*%s%s", kvs.IdDelimiter, id), nil
	case "le":
		if strings.HasPrefix(id, "le.go.eo.") {
			return fmt.Sprintf("le.go.eo.*%s%s", kvs.IdDelimiter, id[9:]), nil
		}
		if strings.HasPrefix(id, "le.go.mc.") {
			return fmt.Sprintf("le.go.mc.*%s%s", kvs.IdDelimiter, id[9:]), nil
		}
		if strings.HasPrefix(id, "le.go.me.") {
			return fmt.Sprintf("le.go.me.*%s%s", kvs.IdDelimiter, id[9:]), nil
		}
		if strings.HasPrefix(id, "le.ep.mc.") {
			return fmt.Sprintf("le.ep.mc.*%s%s", kvs.IdDelimiter, id[9:]), nil
		}
		if strings.HasPrefix(id, "le.ep.me.") {
			return fmt.Sprintf("le.ep.me.*%s%s", kvs.IdDelimiter, id[9:]), nil
		}
		forGospel := strings.Contains(id, ".Gospel.")
		forEpistle := strings.Contains(id, ".Epistle.")
		switch id[2:4] {
		case "eo":
			if forEpistle {
				return fmt.Sprintf("le.ep.eo.*%s%s", kvs.IdDelimiter, id), nil
			} else if forGospel {
				return fmt.Sprintf("le.go.eo.*%s%s", kvs.IdDelimiter, id), nil
			} else {
				return "", fmt.Errorf("key %s not found in imported ares files", id)
			}
		case "lu":
			if forEpistle {
				return fmt.Sprintf("le.ep.lu.*%s%s", kvs.IdDelimiter, id), nil
			} else if forGospel {
				return fmt.Sprintf("le.go.lu.*%s%s", kvs.IdDelimiter, id), nil
			} else {
				return "", fmt.Errorf("key %s not found in imported ares files", id)
			}
		case "mc":
			if forEpistle {
				return fmt.Sprintf("le.ep.mc.*%s%s", kvs.IdDelimiter, id), nil
			} else if forGospel {
				return fmt.Sprintf("le.go.mc.*%s%s", kvs.IdDelimiter, id), nil
			} else {
				return "", fmt.Errorf("key %s not found in imported ares files", id)
			}
		case "me":
			if forEpistle {
				return fmt.Sprintf("le.ep.me.*%s%s", kvs.IdDelimiter, id), nil
			} else if forGospel {
				return fmt.Sprintf("le.go.me.*%s%s", kvs.IdDelimiter, id), nil
			} else {
				return "", fmt.Errorf("key %s not found in imported ares files", id)
			}
		case "va":
			if forEpistle {
				return fmt.Sprintf("le.ep.va.*%s%s", kvs.IdDelimiter, id), nil
			} else if forGospel {
				return fmt.Sprintf("le.go.va.*%s%s", kvs.IdDelimiter, id), nil
			} else {
				return "", fmt.Errorf("key %s not found in imported ares files", id)
			}
		case "tr":
			return "", fmt.Errorf("key %s not found in imported ares files", id)
		default:
			return "", fmt.Errorf("key %s not found in imported ares files", id)
		}
		if strings.HasPrefix(id, "lemc") {
			if strings.Contains(id, ".Gospel.") {
				return fmt.Sprintf("le.go.mc.*%s%s", kvs.IdDelimiter, id), nil
			}
		}
	case "me":
		return fmt.Sprintf("me.*%s%s", kvs.IdDelimiter, id), nil
	case "oc":
		return fmt.Sprintf("oc.*%s%s", kvs.IdDelimiter, id), nil
	case "pe":
		return fmt.Sprintf("pe.*%s%s", kvs.IdDelimiter, id), nil
	case "sy":
		return fmt.Sprintf("sy.*%s%s", kvs.IdDelimiter, id), nil
	case "tr":
		return fmt.Sprintf("tr.*%s%s", kvs.IdDelimiter, id), nil
	case "ty":
		return fmt.Sprintf("ty.*%s%s", kvs.IdDelimiter, id), nil
	}
	return "", fmt.Errorf("key %s not found in imported ares files", id)
}
func (l *AtemListener) EnterAbstractComponent(c *atem.AbstractComponentContext) {
	l.Ctx.StatementContext.AbstractComponentFound = true
	//	if l.Ctx.StatementContext.InExistsCase && l.Ctx.StatementContext.AbstractComponentWriteCloseBraceOnEnter {
	if l.Ctx.StatementContext.InExistsCase {
		l.WriteContent(": {")
		l.Ctx.StatementContext.InExistsCase = false
		l.Ctx.StatementContext.PrintElse = true
	}
}
func (l *AtemListener) EnterAbstractDateCase(c *atem.AbstractDateCaseContext) {
}
func (l *AtemListener) EnterAbstractDayCase(c *atem.AbstractDayCaseContext) {
	l.WriteContent(fmt.Sprintf("\n%scase ", l.Ctx.Indent()))
}
func (l *AtemListener) EnterAbstractDayNameCase(c *atem.AbstractDayNameCaseContext) {
}
func (l *AtemListener) EnterActor(c *atem.ActorContext) {
	l.WriteContent(fmt.Sprintf("\n%sp.actor", l.Ctx.Indent()))
}
func (l *AtemListener) EnterAtemModel(c *atem.AtemModelContext) {
	l.WriteContent(fmt.Sprintf("id = \"%s\"", l.TemplateIdPath))
	l.WriteContent(fmt.Sprintf("\ntype = \"%s\"", templateTypes.NameForType(l.TemplateType)))
}
func (l *AtemListener) EnterBlock(c *atem.BlockContext) {
}
func (l *AtemListener) EnterBreakType(c *atem.BreakTypeContext) {
	text := c.GetText()
	switch text {
	case "line":
		l.WriteContent(fmt.Sprintf("\n%sbr.single", l.Ctx.Indent()))
	case "page":
		l.WriteContent(fmt.Sprintf("\n%spageBreak", l.Ctx.Indent()))
	}
	fmt.Sprintf("%s", text)
}
func (l *AtemListener) EnterCommemoration(c *atem.CommemorationContext) {
}
func (l *AtemListener) EnterSetDate(c *atem.SetDateContext) {
	values := c.AllINT()
out:
	for i, v := range values {
		switch i {
		case 0: // month
			month := v.GetText()
			if month == "0" {
				l.WriteContent(fmt.Sprintf("\n%srestoreDate", l.Ctx.Indent()))
				break out
			} else {
				l.WriteContent(fmt.Sprintf("\n%smonth = %s", l.Ctx.Indent(), v.GetText()))
			}
		case 1: // day
			l.WriteContent(fmt.Sprintf("\n%sday = %s", l.Ctx.Indent(), v.GetText()))
		case 2: // year
			l.WriteContent(fmt.Sprintf("\n%syear = %s", l.Ctx.Indent(), v.GetText()))
		}
	}
}
func (l *AtemListener) EnterDateRange(c *atem.DateRangeContext) {
	values := c.AllINT()
	if len(values) == 2 {
		left := fmt.Sprintf("%v", values[0])
		right := fmt.Sprintf("%v", values[1])
		l.WriteContent(fmt.Sprintf("%s thru %s", left, right))
	}
}
func (l *AtemListener) EnterDateSet(c *atem.DateSetContext) {
	integers, err := integerSlice(c.AllINT())
	if err != nil {
		c.GetParser().NotifyErrorListeners(fmt.Sprintf("parse error: %s", err), c.GetStop(), nil)
		return
	}
	sb := strings.Builder{}
	for _, val := range integers {
		if sb.Len() > 0 {
			sb.WriteString(", ")
		}
		sb.WriteString(fmt.Sprintf("%d", val))
	}
	l.WriteContent(sb.String())
}
func (l *AtemListener) EnterDayNameRange(c *atem.DayNameRangeContext) {
	values := c.AllDayOfWeek()
	if len(values) == 2 {
		left := fmt.Sprintf("%v", ShortDow(values[0].GetText()))
		right := fmt.Sprintf("%v", ShortDow(values[1].GetText()))
		l.WriteContent(fmt.Sprintf("%s thru %s", left, right))
	}
}
func ShortDow(dow string) string {
	switch dow {
	case "Sunday":
		return "sun"
	case "Monday":
		return "mon"
	case "Tuesday":
		return "tue"
	case "Wednesday":
		return "wed"
	case "Thursday":
		return "thu"
	case "Friday":
		return "fri"
	case "Saturday":
		return "sat"
	default:
		return dow
	}
}
func (l *AtemListener) EnterDayNameSet(c *atem.DayNameSetContext) {
	values := c.AllDayOfWeek()
	sb := strings.Builder{}
	for _, val := range values {
		if sb.Len() > 0 {
			sb.WriteString(", ")
		}
		sb.WriteString(fmt.Sprintf("%s", ShortDow(val.GetText())))
	}
	l.WriteContent(sb.String())
}
func (l *AtemListener) EnterDayOfWeek(c *atem.DayOfWeekContext) {
}
func (l *AtemListener) EnterDayRange(c *atem.DayRangeContext) {
}
func (l *AtemListener) EnterDaySet(c *atem.DaySetContext) {
}
func (l *AtemListener) EnterDialog(c *atem.DialogContext) {
	l.WriteContent(fmt.Sprintf("\n%sp.dialog", l.Ctx.Indent()))
}
func (l *AtemListener) EnterElementType(c *atem.ElementTypeContext) {
	if l.Ctx.StatementContext.InPara {
		//		l.WriteContent(fmt.Sprintf("%snc", l.Ctx.Indent()))
		l.Ctx.StatementContext.InPara = false
	}
}
func (l *AtemListener) EnterHead(c *atem.HeadContext) {
	l.Ctx.PdfSettings = appender.NewAppender(Indent)
	l.Ctx.PdfSettings.NewLine()
	l.Ctx.PdfSettings.NewLine()
	l.Ctx.PdfSettings.Write("pdfSettings {")
	l.Ctx.PdfSettings.PushIndent()
	firstPageHeader, _ := regexp.MatchString(`\.(ma\d*|ve\d*)$`, l.TemplateIdPath)
	if l.TemplateType == templateTypes.Service && firstPageHeader {
		l.Ctx.PdfSettings.NewLine()
		l.Ctx.PdfSettings.Write("pageHeaderFirst = none")
		l.Ctx.PdfSettings.NewLine()
		l.Ctx.PdfSettings.Write("pageFooterFirst = none")
	}
}
func (l *AtemListener) EnterHeadComponent(c *atem.HeadComponentContext) {
}
func (l *AtemListener) EnterHeaderFooterColumn(c *atem.HeaderFooterColumnContext) {
}
func (l *AtemListener) EnterHeaderFooterColumnCenter(c *atem.HeaderFooterColumnCenterContext) {
	l.Ctx.PdfSettings.Write(fmt.Sprintf(" center"))
}
func (l *AtemListener) EnterHeaderFooterColumnLeft(c *atem.HeaderFooterColumnLeftContext) {
	l.Ctx.PdfSettings.Write(fmt.Sprintf(" left"))
}
func (l *AtemListener) EnterHeaderFooterColumnRight(c *atem.HeaderFooterColumnRightContext) {
	l.Ctx.PdfSettings.Write(fmt.Sprintf(" right"))
}
func (l *AtemListener) EnterHeaderFooterCommemoration(c *atem.HeaderFooterCommemorationContext) {
	l.Ctx.PdfSettings.Write(" nid \"@commemoration\"")
}
func (l *AtemListener) EnterHeaderFooterDate(c *atem.HeaderFooterDateContext) {
	var lang string
	if c.Language() == nil {
		c.GetParser().NotifyErrorListeners("missing language value", c.GetStart(), nil)
		return
	}
	lang = c.Language().GetText()
	switch lang {
	case "L1":
		lang = "1"
	case "L2":
		lang = "2"
	default:
		msg := fmt.Sprintf(fmt.Sprintf("invalid lang value %s", lang))
		c.GetParser().NotifyErrorListeners(fmt.Sprintf("%s", msg), c.GetStart(), nil)
		return
	}
	l.Ctx.PdfSettings.Write(fmt.Sprintf(" @date ver %s", lang))
}
func (l *AtemListener) EnterHeaderFooterFragment(c *atem.HeaderFooterFragmentContext) {
}

// TODO: remove next line
var inHeaderFooterLookup bool

func (l *AtemListener) EnterHeaderFooterLookup(c *atem.HeaderFooterLookupContext) {
	inHeaderFooterLookup = true
	l.Ctx.PdfSettings.Write(fmt.Sprintf(" @lookup"))
	txt := c.GetText()
	if !strings.Contains(txt, "sid") &&
		!strings.Contains(txt, "rid") {
		c.GetParser().NotifyErrorListeners("@Lookup missing sid or rid", c.GetStart(), nil)
		l.Ctx.PdfSettings.Write(` sid "is/missing"`)
	} else {
		fmt.Sprintf("TODO delete me after handling")
	}
}
func (l *AtemListener) EnterHeaderFooterPageNumber(c *atem.HeaderFooterPageNumberContext) {
	l.Ctx.PdfSettings.Write(fmt.Sprintf(" @pageNbr"))
}
func (l *AtemListener) EnterHeaderFooterText(c *atem.HeaderFooterTextContext) {
	text := c.GetStop().GetText()
	if l.Ctx.StatementContext.InTemplateTitle {
		l.WriteContent(fmt.Sprintf("%s\n\n", text))
	} else {
		l.Ctx.PdfSettings.Write(fmt.Sprintf(" nid %s", text))
	}
}
func (l *AtemListener) EnterHeaderFooterTitle(c *atem.HeaderFooterTitleContext) {
}
func (l *AtemListener) EnterHymn(c *atem.HymnContext) {
	l.WriteContent(fmt.Sprintf("\n%sp.hymn", l.Ctx.Indent()))
}
func (l *AtemListener) EnterImportBlock(c *atem.ImportBlockContext) {
	l.Ctx.StatementContext.InImportBlock = true
}
func (l *AtemListener) EnterInsertBreak(c *atem.InsertBreakContext) {
}
func (l *AtemListener) EnterLanguage(c *atem.LanguageContext) {
}
func (l *AtemListener) EnterLdp(c *atem.LdpContext) {
	l.WriteContent(" ldp ")
}
func (l *AtemListener) EnterLdpType(c *atem.LdpTypeContext) {
	if c.All() != nil { // @All_Liturgical_Day_Properties
		l.WriteContent("@allLiturgicalDayProperties ")
	} else if c.DOL() != nil { // @Lukan_Cycle_Elapsed_Days
		l.WriteContent("@lukanCycleElapsedDays ")
	} else if c.DOM() != nil { // @Day_of_Month
		l.WriteContent("@lukanCycleElapsedDays ")
	} else if c.DOP() != nil { // @Day_of_Period
		l.WriteContent("@dayOfPeriod ")
	} else if c.DOWN() != nil { // @Day_of_Week_As_Number
		l.WriteContent("@dayOfWeekAsNumber ")
	} else if c.DOWT() != nil { // @Day_of_Week_As_Text
		l.WriteContent("@dayOfWeekAsText ")
	} else if c.EOW() != nil { // @Eothinon
		l.WriteContent("@eothinon ")
	} else if c.GenDate() != nil { // @Service_Date
		l.WriteContent("@serviceDate ")
	} else if c.GenYear() != nil { // @Service_Year
		l.WriteContent("@serviceYear ")
	} else if c.MCD() != nil { // @Day_of_Movable_Cycle
		l.WriteContent("@dayOfMovableCycle ")
	} else if c.MOW() != nil { // @Mode_of_Week
		l.WriteContent("@modeOfWeek ")
	} else if c.NOP() != nil { // @Name_of_Period
		l.WriteContent("@nameOfPeriod ")
	} else if c.SAEC() != nil { // @Sunday_After_Elevation_Cross_Date
		l.WriteContent("@sundayAfterElevationCrossDate ")
	} else if c.SBT() != nil { // @Sundays_Before_Triodion
		l.WriteContent("@sundaysBeforeTriodion ")
	} else if c.SOL() != nil { // @Lukan_Cycle_Start_Date
		l.WriteContent("@lukanCycleStartDate ")
	} else if c.WDOLC() != nil { // @Lukan_Cycle_Week_Day
		l.WriteContent("@lukanCycleWeekAndDay ")
	} else if c.WOLC() != nil { // @Lukan_Cycle_Week
		l.WriteContent("@lukanCycleWeek ")
	} else {
		c.GetParser().NotifyErrorListeners("unknown ldp @ argument", c.GetStart(), nil)
	}
}
func (l *AtemListener) EnterLookup(c *atem.LookupContext) {
	//	checkDebug()
	if l.Ctx.PdfSettings.Empty() {
		l.WriteContent(" rid ")
	} else {
		l.Ctx.PdfSettings.Write(" rid ")
	}
	txt := c.GetText()
	if len(txt) > 0 {
		if strings.Contains(txt, "@ver") {
			if l.Ctx.PdfSettings.Empty() {
				l.WriteSuffix(" @ver ")
			} else {
				l.Ctx.PdfSettings.Write(" @ver ")
			}
		}
	}
	l.Ctx.StatementContext.InRid = true
}
func (l *AtemListener) EnterDayOverride(c *atem.DayOverrideContext) {
	var i int
	var err error
	beg := c.GetStart().GetText()
	if beg == "@day" {
		l.WriteContent("@day ")
	} else {
		c.GetParser().NotifyErrorListeners(fmt.Sprintf("expected @day got %s", beg), c.GetStart(), nil)
		return
	}
	end := c.GetStop().GetText()
	if strings.HasPrefix(end, "D") {
		if len(end) == 2 {
			wkDayStr := end[1:]
			i, err = strconv.Atoi(wkDayStr)
			if err != nil {
				c.GetParser().NotifyErrorListeners(fmt.Sprintf("error converting %s to int: %v", wkDayStr, err), c.GetStop(), nil)
				return
			}
			if i > 0 && i < 8 {
				l.WriteContent(wkDayStr + " ")
			} else {
				c.GetParser().NotifyErrorListeners(fmt.Sprintf("invalid weekday number %s, should > 1 && < 8", wkDayStr), c.GetStop(), nil)
			}
		} else {
			c.GetParser().NotifyErrorListeners(fmt.Sprintf("invalid weekday number %s, should > D1 && < D8", c.GetStop().GetText()), c.GetStop(), nil)
		}
	} else {
		c.GetParser().NotifyErrorListeners(fmt.Sprintf("invalid weekday number %s, should > D1 && < D8", c.GetStop().GetText()), c.GetStop(), nil)
	}
	_ = fmt.Sprintf("%s %s", beg, end)
}
func (l *AtemListener) EnterModeOverride(c *atem.ModeOverrideContext) {
	var i int
	var err error
	beg := c.GetStart().GetText()
	if beg == "@mode" {
		l.WriteContent("@mode ")
	} else {
		c.GetParser().NotifyErrorListeners(fmt.Sprintf("expected @day got %s", beg), c.GetStart(), nil)
		return
	}
	end := c.GetStop().GetText()
	if strings.HasPrefix(end, "M") {
		if len(end) == 2 {
			modeStr := end[1:]
			i, err = strconv.Atoi(modeStr)
			if err != nil {
				c.GetParser().NotifyErrorListeners(fmt.Sprintf("error converting %s to int: %v", modeStr, err), c.GetStop(), nil)
				return
			}
			if i > 0 && i < 9 {
				l.WriteContent(modeStr + " ")
			} else {
				c.GetParser().NotifyErrorListeners(fmt.Sprintf("invalid mode %s, should > 1 && < 9", modeStr), c.GetStop(), nil)
			}
		} else {
			c.GetParser().NotifyErrorListeners(fmt.Sprintf("invalid mode %s, should > M1 && < M9", c.GetStop().GetText()), c.GetStop(), nil)
		}
	} else {
		c.GetParser().NotifyErrorListeners(fmt.Sprintf("invalid mode %s, should > M1 && < M9", c.GetStop().GetText()), c.GetStop(), nil)
	}
	_ = fmt.Sprintf("%s %s", beg, end)
}

func (l *AtemListener) EnterMcDay(c *atem.McDayContext) {
	value := c.INT().GetText()
	if value == "0" {
		l.WriteContent(fmt.Sprintf("\n%srestoreMovableCycleDay", l.Ctx.Indent()))
	} else {
		l.WriteContent(fmt.Sprintf("\n%smovableCycleDay = %s", l.Ctx.Indent(), value))
	}
}
func (l *AtemListener) EnterMedia(c *atem.MediaContext) {
	l.WriteContent(fmt.Sprintf("\n%smedia", l.Ctx.Indent()))
}
func (l *AtemListener) EnterModeOfWeekSet(c *atem.ModeOfWeekSetContext) {
	values := c.AllModeTypes()
	sb := strings.Builder{}
	for _, val := range values {
		if sb.Len() > 0 {
			sb.WriteString(", ")
		}
		mode := val.GetText()
		sb.WriteString(fmt.Sprintf("%s", mode[1:]))
	}
	l.WriteContent(sb.String())
}
func (l *AtemListener) EnterModeTypes(c *atem.ModeTypesContext) {
}
func (l *AtemListener) EnterMonthName(c *atem.MonthNameContext) {
	txt := strings.ToLower(c.GetText())
	l.WriteContent(fmt.Sprintf("%s ", txt))
}
func (l *AtemListener) EnterPageFooterEven(c *atem.PageFooterEvenContext) {
	l.Ctx.PdfSettings.NewLine()
	l.Ctx.PdfSettings.WriteIndented("pageFooterEven =")
}
func (l *AtemListener) EnterPageFooterOdd(c *atem.PageFooterOddContext) {
	l.Ctx.PdfSettings.NewLine()
	l.Ctx.PdfSettings.WriteIndented("pageFooterOdd =")
}
func (l *AtemListener) EnterPageHeaderEven(c *atem.PageHeaderEvenContext) {
	l.Ctx.PdfSettings.NewLine()
	l.Ctx.PdfSettings.WriteIndented("pageHeaderEven =")
}
func (l *AtemListener) EnterPageHeaderOdd(c *atem.PageHeaderOddContext) {
	l.Ctx.PdfSettings.NewLine()
	l.Ctx.PdfSettings.WriteIndented("pageHeaderOdd =")
}
func (l *AtemListener) EnterPageKeepWithNext(c *atem.PageKeepWithNextContext) {
}
func (l *AtemListener) EnterPageNumber(c *atem.PageNumberContext) {
	value := c.INT().GetText()
	l.Ctx.PdfSettings.NewLine()
	l.Ctx.PdfSettings.WriteIndentedLine(fmt.Sprintf("pageNbr = %s", value))
}

// handleRole examines the role to see if it is a bmc_ or emc_.
// If it is, it opens or closes an htmlOnly wrapper.
// A subtype of bmc_, emc_ is bmc_collapse and emc_collapse.
// Two context bools are used to differentiate them because
// they result in either a statement wrapped as htmlOnly
// or, an entire block of lines being wrapped as htmlOnly.
// if not, it just writes an indented new line.
func (l *AtemListener) handleRole(txt string) {
	if strings.Contains(txt, "rolebmc_") {
		l.WriteContent(fmt.Sprintf("\n%shtmlOnly {", l.Ctx.Indent()))
		l.Ctx.PushIndent()
		l.WriteContent(fmt.Sprintf("\n%s", l.Ctx.Indent()))
		if strings.Contains(txt, "rolebmc_collapse") {
			l.Ctx.StatementContext.CloseHtmlOnlyBlock = true
		}
	} else if strings.Contains(txt, "roleemc_") {
		if strings.Contains(txt, "roleemc_collapse") {
			l.WriteContent(fmt.Sprintf("\n%shtmlOnly {", l.Ctx.Indent()))
			l.Ctx.PushIndent()
		}
		l.WriteContent(fmt.Sprintf("\n%s", l.Ctx.Indent()))
		l.Ctx.StatementContext.CloseHtmlOnlyBlock = true
	} else {
		l.WriteContent(fmt.Sprintf("\n%s", l.Ctx.Indent()))
	}
}

// closeHtmlOnlyBlock completes the wrapping of an htmlOnly block
func (l *AtemListener) closeHtmlOnlyBlock() {
	l.Ctx.StatementContext.CloseHtmlOnlyBlock = false
	l.Ctx.PopIndent()
	l.WriteContent(fmt.Sprintf("\n%s}", l.Ctx.Indent()))
}

func (l *AtemListener) EnterParagraph(c *atem.ParagraphContext) {
	// antlr = paragraph: 'Para' role? elementType* 'End-Para';
	l.Ctx.StatementContext.InPara = true
	txt := c.GetText()
	if strings.Contains(txt, "role") {
		l.handleRole(txt)
	} else {
		l.WriteContent(fmt.Sprintf("\n%sp", l.Ctx.Indent()))
	}
}
func (l *AtemListener) EnterPassThroughHtml(c *atem.PassThroughHtmlContext) {
}
func (l *AtemListener) EnterPreface(c *atem.PrefaceContext) {
	l.WriteContent(fmt.Sprintf("\n%spdfPreface {", l.Ctx.Indent()))
	l.Ctx.PushIndent()
}
func (l *AtemListener) ExitPreface(c *atem.PrefaceContext) {
	l.WriteContent(fmt.Sprintf("\n%spageBreak", l.Ctx.Indent()))
	l.Ctx.PopIndent()
	l.WriteContent(fmt.Sprintf("\n%s}\n\n", l.Ctx.Indent()))
}
func (l *AtemListener) EnterQualifiedName(c *atem.QualifiedNameContext) {
	text := c.GetText()
	if len(text) == 0 {
		msg := fmt.Sprintf("missing qualified value")
		c.GetParser().NotifyErrorListeners(fmt.Sprintf("%s", msg), c.GetStart(), nil)
		return
	}
	if l.Ctx.StatementContext.InInsertSection {
		template := l.TemplateIdPath
		lineNbr := c.GetStart().GetLine()
		var tData *TemplateData
		var found bool
		if tData, found = l.TemplateDataMap[template]; !found {
			err := fmt.Errorf("could not find template %s to look up LML insert path", template)
			c.GetParser().NotifyErrorListeners(fmt.Sprintf("%v", err), c.GetStart(), nil)
			return
		}
		var id string
		lookupKey := fmt.Sprintf("%s:%d", template, lineNbr-1)
		if id, found = tData.InsertMap[lookupKey]; !found {
			err := fmt.Errorf("could not find insert path %s in template %s to look up LML insert path", text, template)
			c.GetParser().NotifyErrorListeners(fmt.Sprintf("%v", err), c.GetStart(), nil)
			return
		}
		dir, file := path.Split(id)
		if !strings.HasPrefix(file, "bl.") {
			file = "bl." + file
			id = path.Join(dir, file)
		}
		insert := fmt.Sprintf("\n%sinsert \"%s\"", l.Ctx.Indent(), id)
		if strings.HasPrefix(id, "a-templates/Pdf_Covers/pdf.cover.links.lookup") {
			l.Ctx.InsertsForTemplateExit = append(l.Ctx.InsertsForTemplateExit, insert)
		} else {
			l.WriteContent(insert)
		}
		l.Ctx.StatementContext.InInsertSection = false
		return
	}
	if l.Ctx.StatementContext.InInsertTemplate {
		ltxId, err := l.TemplateIndex.TemplatePath(text)
		dir, file := path.Split(ltxId)
		if strings.HasPrefix(file, "se.") {
			// ignore
		} else if strings.HasPrefix(file, "bk.") {
			// ignore
		} else {
			if !strings.HasPrefix(file, "bl.") {
				file = "bl." + file
				ltxId = path.Join(dir, file)
			}
		}

		l.Ctx.StatementContext.InInsertTemplate = false
		if err != nil {
			c.GetParser().NotifyErrorListeners(fmt.Sprintf("%v", err), c.GetStart(), nil)
			return
		}
		insert := fmt.Sprintf("\n%sinsert \"%s\"", l.Ctx.Indent(), ltxId)
		if strings.HasPrefix(ltxId, "a-templates/Pdf_Covers/pdf.cover.links.lookup") {
			l.Ctx.InsertsForTemplateExit = append(l.Ctx.InsertsForTemplateExit, insert)
		} else {
			l.WriteContent(insert)
		}
		return
	}
	if l.Ctx.StatementContext.InImportBlock {
		var prefix string
		tempText := text
		var isAres bool
		if strings.Contains(tempText, "_") ||
			tempText == "da" ||
			tempText == "le" ||
			tempText == "me" ||
			tempText == "oc" ||
			tempText == "pe" ||
			tempText == "tr" {
			isAres = true
		}

		if strings.Contains(tempText, "_") {
			parts := strings.Split(text, "_")
			tempText = parts[0]
			if strings.Contains(parts[len(parts)-1], ".") {
				parts = strings.Split(parts[len(parts)-1], ".")
				prefix = strings.Join(parts[1:], ".")
				l.KeyPrefixMap[prefix] = prefix
			}
		}
		if val := l.TemplateIndex.TemplateMap[tempText]; val != nil {
			if isAres {
				l.ImportMapAres[tempText] = tempText
			} else {
				if len(prefix) > 0 {
					l.ImportMapAtem[tempText] = path.Join(val[0], prefix)
				} else {
					l.ImportMapAtem[tempText] = val[0]
				}
			}
		} else {
			l.ImportMapAres[tempText] = tempText
		}
		return
	}
	if l.Ctx.StatementContext.InRoleBlock {
		var newTag string
		var ok bool
		if newTag, ok = l.ITags.Map[text]; !ok {
			c.GetParser().NotifyErrorListeners(fmt.Sprintf("%s not found in system tags", text), c.GetStart(), nil)
			return
		}
		if !l.TemplateIndex.ExistsCssRule(newTag, true, true) {
			c.GetParser().NotifyErrorListeners(fmt.Sprintf("%s (converted from %s) not found in %s or %s", newTag, text, l.TemplateIndex.HtmlStyleSheetPath, l.TemplateIndex.PdfStyleSheetPath), c.GetStart(), nil)
			return
		}
		if text == "cover10" {
			l.Ctx.StatementContext.InsertingAnchorLabel = true
		} else {
			if text == "cover11" {
				l.Ctx.StatementContext.InsertingAnchorHref = true
				l.WriteContent(fmt.Sprintf("p.title a.%s", text))
			} else {
				l.WriteContent(fmt.Sprintf("%s", newTag))
			}
		}
		l.Ctx.StatementContext.InRoleBlock = false
		return
	}
	if l.Ctx.StatementContext.InTaggedTextBlock {
		var newTag string
		var ok bool
		if newTag, ok = l.ITags.Map[text]; !ok {
			c.GetParser().NotifyErrorListeners(fmt.Sprintf("%s not found in system tags", text), c.GetStart(), nil)
		}
		if !l.TemplateIndex.ExistsCssRule(newTag, true, true) {
			c.GetParser().NotifyErrorListeners(fmt.Sprintf("%s (converted from %s) not found in %s or %s", newTag, text, l.TemplateIndex.HtmlStyleSheetPath, l.TemplateIndex.PdfStyleSheetPath), c.GetStart(), nil)
		}
		l.WriteContent(fmt.Sprintf(" ( %s ", newTag))
		l.Ctx.StatementContext.InTaggedTextBlock = false
		return
	}
	if strings.HasSuffix(l.TemplateIdPath, text) {
		return
	} else {
		var resolvedId string
		var err error
		if l.IsLid(text) {
			//TODO: audit
			resolvedId = AtemExtractLid(text)
		} else if strings.Contains(text, "_en_") || strings.Contains(text, "_gr_") { // this is a fully qualified key, e.g. actors_gr_GR_cog_.acPriest
			parts := strings.Split(text, "_")
			nbrParts := len(parts)
			if nbrParts > 3 {
				resolvedId = parts[0] + ":"
				subParts := strings.Split(parts[3], ".")
				resolvedId = resolvedId + strings.Join(subParts[1:], ".") // we just want the key part
				if nbrParts > 4 {
					resolvedId = resolvedId + "_"
					if nbrParts == 5 {
						resolvedId = resolvedId + parts[4]
					} else {
						resolvedId = resolvedId + strings.Join(parts[4:], "_")
					}
				}
			}
		}
		if len(resolvedId) == 0 {
			resolvedId, err = l.ResolvedId(text)
			if err != nil {
				c.GetParser().NotifyErrorListeners(fmt.Sprintf("%v", err), c.GetStart(), nil)
			}
		}
		if l.Ctx.PdfSettings.Empty() {
			if l.Ctx.StatementContext.InsertingAnchorLabel {
				l.Ctx.StatementContext.AnchorLabel = resolvedId
				l.Ctx.StatementContext.InsertingAnchorLabel = false
				return
			}
			l.WriteContent(fmt.Sprintf("\"%s\"", resolvedId))
		} else {
			l.Ctx.PdfSettings.Write(fmt.Sprintf("\"%s\"", resolvedId))
		}
	}
}
func (l *AtemListener) EnterQualifiedNameWithWildCard(c *atem.QualifiedNameWithWildCardContext) {
}
func (l *AtemListener) EnterReading(c *atem.ReadingContext) {
	l.WriteContent(fmt.Sprintf("\n%sp.reading", l.Ctx.Indent()))
}
func (l *AtemListener) EnterResourceText(c *atem.ResourceTextContext) {
	//TODO:we are attempting to filter fully qualified ids. maybe check for underscore and  use ltstring. AlwbToDoxaId
	//So first we have to grab the Id
	//c.GetText() has sid prefix
	//once prepared check for underscores
	// if _ then try AlwbToDoxaId
	// if it is valid output lid

	// filter lids out
	sidStr := " sid "
	if strings.HasPrefix(c.GetText(), "sid") {
		fqid := c.GetText()[3:]
		if l.IsLid(fqid) {
			sidStr = " lid "
		}
	}
	if l.Ctx.StatementContext.InsertingAnchorLabel {
		return
	}
	if l.Ctx.StatementContext.InsertingAnchorHref {
		l.WriteContent(" href ")
	}
	if l.Ctx.PdfSettings.Empty() {
		l.WriteContent(sidStr)
	} else {
		l.Ctx.PdfSettings.Write(sidStr)
	}
	txt := c.GetText()
	if len(txt) > 0 {
		if strings.Contains(txt, "@ver") {
			if l.Ctx.PdfSettings.Empty() {
				l.WriteSuffix(" @ver ")
			} else {
				l.Ctx.PdfSettings.Write(" @ver ")
			}
		}
	}
}

// IsLid determines whether the parser should output sid or lid based on template Id and patterns.
func (l *AtemListener) IsLid(id string) bool {
	if !strings.Contains(l.TemplateIdPath, "cover.credits") {
		return false
	}
	triggerPrefixes := []string{
		"pdf.credits_",
	}
	triggerSuffixes := []string{}
	relTriggerPrefixes := []string{
		"properties_",
	}
	relTriggerSuffixes := []string{
		".designation",
		".copyright1",
		".name",
	}
	forbiddenPrefixes := []string{}
	forbiddenSuffixes := []string{
		".line_break",
	}

	for _, p := range forbiddenPrefixes {
		if strings.HasPrefix(id, p) {
			return false
		}
	}
	for _, s := range forbiddenSuffixes {
		if strings.HasSuffix(id, s) {
			return false
		}
	}
	for _, p := range triggerPrefixes {
		if strings.HasPrefix(id, p) {
			return true
		}
	}
	for _, s := range triggerSuffixes {
		if strings.HasSuffix(id, s) {
			return true
		}
	}
	for _, p := range relTriggerPrefixes {
		if strings.HasPrefix(id, p) {
			for _, s := range relTriggerSuffixes {
				if strings.HasSuffix(id, s) {
					return true
				}
			}
		}
	}
	return false
}

func AtemExtractLid(fqid string) string {
	parts := strings.Split(fqid, "_")
	var topic string
	var key string
	var library string
	if len(parts) < 4 {
		return fqid
	}
	topic = parts[0]
	library = strings.Join(parts[1:len(parts)-1], "_")
	library = strings.ToLower(library)
	protoKey := strings.Split(parts[len(parts)-1], ".")
	library = fmt.Sprintf("%s_%s", library, protoKey[0])
	key = strings.Join(protoKey[1:], ".")
	return fmt.Sprintf("%s%s%s%s%s", library, kvs.SegmentDelimiter, topic, kvs.IdDelimiter, key)
}

func (l *AtemListener) EnterRestoreLocale(c *atem.RestoreLocaleContext) {
}
func (l *AtemListener) EnterRole(c *atem.RoleContext) {
	l.Ctx.StatementContext.InRoleBlock = true
	l.Ctx.StatementContext.InPara = false
}
func (l *AtemListener) EnterRubric(c *atem.RubricContext) {
	l.WriteContent(fmt.Sprintf("\n%sp.rubric",
		l.Ctx.Indent()))
}
func (l *AtemListener) EnterSection(c *atem.SectionContext) {
	text := c.ID().GetText()
	sectionPath := path.Join(l.Ctx.SectionStack.Join("/"),
		fmt.Sprintf("bl.%s", text))
	l.WriteContent(fmt.Sprintf("\n%sinsert \"%s\"",
		l.Ctx.Indent(), sectionPath))
	l.Ctx.SectionStack.Push(text)
	l.Ctx.PushStatementContext(true)
}
func (l *AtemListener) EnterSectionFragment(c *atem.SectionFragmentContext) {
	l.Ctx.StatementContext.InInsertSection = true
}
func (l *AtemListener) EnterSetLocale(c *atem.SetLocaleContext) {
}
func (l *AtemListener) EnterSubTitle(c *atem.SubTitleContext) {
}
func (l *AtemListener) EnterSundaysBeforeTriodionCase(c *atem.SundaysBeforeTriodionCaseContext) {
	nbr := c.INT().GetText()
	l.WriteContent(fmt.Sprintf("\n%scase %s: {", l.Ctx.Indent(), nbr))
	l.Ctx.PushIndent()
}
func (l *AtemListener) EnterTaggedText(c *atem.TaggedTextContext) {
	l.Ctx.StatementContext.InTaggedTextBlock = true
}
func (l *AtemListener) EnterTemplateFragment(c *atem.TemplateFragmentContext) {
	//	l.WriteContent(fmt.Sprintf("\n%sinsert ", l.Ctx.Indent()))
	l.Ctx.StatementContext.InInsertTemplate = true
}
func (l *AtemListener) EnterTemplateStatus(c *atem.TemplateStatusContext) {
}
func (l *AtemListener) EnterTemplateStatuses(c *atem.TemplateStatusesContext) {
	status := strings.TrimSpace(c.GetText())
	if len(status) == 0 {
		status = "na"
	}
	status = strings.ToLower(status)
	switch status {
	case "na":
		status = "na"
	case "draft":
		status = "draft"
	case "review":
		status = "review"
	case "final":
		status = "final"
	default:
		msg := fmt.Sprintf("parse error: unknown status value %s", status)
		c.GetParser().NotifyErrorListeners(msg, c.GetStop(), nil)
	}
	l.WriteContent(fmt.Sprintf("\nstatus = \"%s\"", status))
	switch l.TemplateType {
	case templateTypes.Book:
		if len(l.TemplateIndexTitleCodes) > 0 {
			l.WriteContent(fmt.Sprintf("\nindexTitleCodes = \"%s\"\n", strings.Join(l.TemplateIndexTitleCodes, ", ")))
		}
	case templateTypes.Service:
		// Certain service types should not have a pdf generated.
		noPdf := []string{"h91", "ma2"} // <-- add more here
		if l.TemplateType == templateTypes.Service {
			for _, code := range l.TemplateIndexTitleCodes {
				for _, p := range noPdf {
					if code == p {
						l.WriteContent(fmt.Sprintf("\noutput = \"html\""))
					}
				}
			}
		}
	}
	l.TemplateStatus = status
}
func (l *AtemListener) EnterTemplateTitle(c *atem.TemplateTitleContext) {
	l.Ctx.StatementContext.InTemplateTitle = true
	l.WriteContent(fmt.Sprintf("\n%sindexLastTitleOverride = ", l.Ctx.Indent()))
}
func (l *AtemListener) EnterTitle(c *atem.TitleContext) {
	if l.Ctx.StatementContext.InsertingAnchorLabel {
		//l.WriteContent(fmt.Sprintf("\n%sp.title", l.Ctx.Indent()))
		return
	}
	txt := c.GetText()
	if strings.Contains(txt, "role") {
		l.handleRole(txt)
	} else {
		l.WriteContent(fmt.Sprintf("\n%sp.title", l.Ctx.Indent()))
	}
}
func (l *AtemListener) EnterVerse(c *atem.VerseContext) {
	l.WriteContent(fmt.Sprintf("\n%sp.verse", l.Ctx.Indent()))
}
func (l *AtemListener) EnterVersion(c *atem.VersionContext) {
}
func (l *AtemListener) EnterVersionSwitch(c *atem.VersionSwitchContext) {
	version := c.VersionSwitchType().GetText()
	switch version {
	case "L1":
		version = "version = 1"
	case "L2":
		version = "version = 2"
	case "Both":
		if len(l.Ctx.InsertsForTemplateExit) > 0 {
			for _, i := range l.Ctx.InsertsForTemplateExit {
				l.WriteContent(fmt.Sprintf("\n%s", i))
			}
			l.Ctx.InsertsForTemplateExit = []string{}
		}

		version = "restoreVersion"
	default:
		msg := fmt.Sprintf("parse error: unknown switch type %s", version)
		c.GetParser().NotifyErrorListeners(msg, c.GetStop(), nil)
	}
	l.WriteContent(fmt.Sprintf("\n%s%s", l.Ctx.Indent(), version))
}
func (l *AtemListener) EnterVersionSwitchType(c *atem.VersionSwitchTypeContext) {
}
func (l *AtemListener) EnterWhenDate(c *atem.WhenDateContext) {
	l.Ctx.PushStatementContext(false)
	l.WriteContent(fmt.Sprintf("\n%sswitch date {", l.Ctx.Indent()))
	l.Ctx.PushIndent()
}
func (l *AtemListener) EnterWhenDateCase(c *atem.WhenDateCaseContext) {
	l.WriteContent(fmt.Sprintf("\n%scase ", l.Ctx.Indent()))
	l.Ctx.PushIndent()
}
func (l *AtemListener) EnterWhenDayName(c *atem.WhenDayNameContext) {
	// TODO delete below
	//fmt.Printf("\n%sEnterWhenDayName: before %v", l.Ctx.Indent(), l.Ctx.StatementContext.InExists)
	// TODO end delete
	l.Ctx.PushStatementContext(false)
	// TODO delete below
	//fmt.Printf("\n%sEnterWhenDayName: after %v", l.Ctx.Indent(), l.Ctx.StatementContext.InExists)
	// TODO end delete
	l.WriteContent(fmt.Sprintf("\n%sswitch dayOfWeek {", l.Ctx.Indent()))
	l.Ctx.PushIndent()
}
func (l *AtemListener) EnterWhenDayNameCase(c *atem.WhenDayNameCaseContext) {
	l.WriteContent(fmt.Sprintf("\n%scase ", l.Ctx.Indent()))
	l.Ctx.PushIndent()
}
func (l *AtemListener) EnterWhenExists(c *atem.WhenExistsContext) {
	checkDebug()
	//l.WriteContent(fmt.Sprintf("\n%sif exists rid ", l.Ctx.Indent()))
	//l.Ctx.PushStatementContext(false)
	//l.Ctx.StatementContext.InExists = true
	//l.Ctx.PushIndent()
	l.Ctx.PushStatementContext(false)
	l.WriteContent(fmt.Sprintf("\n%sswitch exists {", l.Ctx.Indent()))
	l.Ctx.PushIndent()
}
func (l *AtemListener) EnterWhenExistsCase(c *atem.WhenExistsCaseContext) {
	checkDebug()
	l.WriteContent(fmt.Sprintf("\n%scase rid ", l.Ctx.Indent()))
	l.Ctx.PushIndent()
	l.Ctx.StatementContext.InExistsCase = true
	//	l.Ctx.StatementContext.AbstractComponentWriteCloseBraceOnEnter = true
	//l.Ctx.StatementContext.InExistsCase = true
	//if l.Ctx.StatementContext.InExists {
	//	if l.Ctx.StatementContext.InExistsCaseCount > 1 {
	//		l.Ctx.PopIndent()
	//		l.WriteContent(fmt.Sprintf("\n%s} else if exists rid ", l.Ctx.Indent()))
	//	}
	//}
	//l.Ctx.StatementContext.InExistsCaseCount++
	//l.Ctx.PushIndent()
}
func (l *AtemListener) EnterWhenLukanCycleDay(c *atem.WhenLukanCycleDayContext) {
	l.Ctx.PushStatementContext(false)
	l.WriteContent(fmt.Sprintf("\n%sswitch lukanCycleDay {", l.Ctx.Indent()))
	l.Ctx.PushIndent()
}
func (l *AtemListener) EnterWhenModeOfWeek(c *atem.WhenModeOfWeekContext) {
	l.Ctx.PushStatementContext(false)
	l.WriteContent(fmt.Sprintf("\n%sswitch modeOfWeek {", l.Ctx.Indent()))
	l.Ctx.PushIndent()
}
func (l *AtemListener) EnterWhenModeOfWeekCase(c *atem.WhenModeOfWeekCaseContext) {
	l.WriteContent(fmt.Sprintf("\n%scase ",
		l.Ctx.Indent()))
	l.Ctx.PushIndent()
}
func (l *AtemListener) EnterWhenMovableCycleDay(c *atem.WhenMovableCycleDayContext) {
	l.Ctx.PushStatementContext(false)
	l.WriteContent(fmt.Sprintf("\n%sswitch movableCycleDay {",
		l.Ctx.Indent()))
	l.Ctx.PushIndent()
}
func (l *AtemListener) EnterWhenOther(c *atem.WhenOtherContext) {
	checkDebug()
	//if l.Ctx.StatementContext.InExists {
	//	l.Ctx.PopIndent()
	//	l.WriteContent(fmt.Sprintf("\n%s} else {",
	//		l.Ctx.Indent()))
	//	l.Ctx.StatementContext.AbstractComponentWriteCloseBraceOnEnter = false
	//	l.Ctx.StatementContext.PrintElse = false
	//} else {
	l.WriteContent(fmt.Sprintf("\n%sdefault: {", l.Ctx.Indent()))
	//	}
	l.Ctx.PushIndent()
}
func (l *AtemListener) EnterWhenPeriodCase(c *atem.WhenPeriodCaseContext) {
	l.Ctx.PushIndent()
}
func (l *AtemListener) EnterWhenSundayAfterElevationOfCrossDay(c *atem.WhenSundayAfterElevationOfCrossDayContext) {
	l.Ctx.PushStatementContext(false)
	l.WriteContent(fmt.Sprintf("\n%sswitch sundayAfterElevationOfCross {", l.Ctx.Indent()))
	l.Ctx.PushIndent()
}
func (l *AtemListener) EnterWhenSundaysBeforeTriodion(c *atem.WhenSundaysBeforeTriodionContext) {
	l.Ctx.PushStatementContext(false)
	l.WriteContent(fmt.Sprintf("\n%sswitch sundaysBeforeTriodion {", l.Ctx.Indent()))
	l.Ctx.PushIndent()
}
func (l *AtemListener) ExitAbstractComponent(c *atem.AbstractComponentContext) {
}
func (l *AtemListener) ExitAbstractDateCase(c *atem.AbstractDateCaseContext) {
	l.WriteContent(fmt.Sprintf(": {"))
}
func (l *AtemListener) ExitAbstractDayCase(c *atem.AbstractDayCaseContext) {
	l.WriteContent(fmt.Sprintf(": {"))
}
func (l *AtemListener) ExitAbstractDayNameCase(c *atem.AbstractDayNameCaseContext) {
	l.WriteContent(fmt.Sprintf(": {"))
}
func (l *AtemListener) ExitActor(c *atem.ActorContext) {
}
func (l *AtemListener) ExitAtemModel(c *atem.AtemModelContext) {
	if len(l.Ctx.InsertsForTemplateExit) > 0 {
		for _, i := range l.Ctx.InsertsForTemplateExit {
			l.WriteContent(fmt.Sprintf("\n%s", i))
		}
		l.Ctx.InsertsForTemplateExit = []string{}
	}
	l.WriteContent("\n")
}
func (l *AtemListener) ExitBlock(c *atem.BlockContext) {
}
func (l *AtemListener) ExitBreakType(c *atem.BreakTypeContext) {
}
func (l *AtemListener) ExitCommemoration(c *atem.CommemorationContext) {
}
func (l *AtemListener) ExitDate(c *atem.SetDateContext) {
}
func (l *AtemListener) ExitDateRange(c *atem.DateRangeContext) {
}
func (l *AtemListener) ExitDateSet(c *atem.DateSetContext) {
}
func (l *AtemListener) ExitDayNameRange(c *atem.DayNameRangeContext) {
}
func (l *AtemListener) ExitDayNameSet(c *atem.DayNameSetContext) {
}
func (l *AtemListener) ExitDayOfWeek(c *atem.DayOfWeekContext) {
}
func (l *AtemListener) ExitDayRange(c *atem.DayRangeContext) {
	values := c.AllINT()
	if len(values) == 2 {
		left := fmt.Sprintf("%v", values[0])
		right := fmt.Sprintf("%v", values[1])
		l.WriteContent(fmt.Sprintf("%s thru %s", left, right))
	}
}
func (l *AtemListener) ExitDaySet(c *atem.DaySetContext) {
	integers, err := integerSlice(c.AllINT())
	if err != nil {
		c.GetParser().NotifyErrorListeners(fmt.Sprintf("parse error: %s", err), c.GetStop(), nil)
		return
	}
	sb := strings.Builder{}
	for _, val := range integers {
		if sb.Len() > 0 {
			sb.WriteString(", ")
		}
		sb.WriteString(fmt.Sprintf("%d", val))
	}
	l.WriteContent(sb.String())
}
func (l *AtemListener) ExitDialog(c *atem.DialogContext) {
}
func (l *AtemListener) ExitElementType(c *atem.ElementTypeContext) {
}
func (l *AtemListener) ExitHead(c *atem.HeadContext) {
	l.Ctx.PdfSettings.WriteLine("}")
	l.WriteContent(l.Ctx.PdfSettings.String())
	l.Ctx.PdfSettings.Reset()
}
func (l *AtemListener) ExitHeadComponent(c *atem.HeadComponentContext) {
}
func (l *AtemListener) ExitHeaderFooterColumn(c *atem.HeaderFooterColumnContext) {
}
func (l *AtemListener) ExitHeaderFooterColumnCenter(c *atem.HeaderFooterColumnCenterContext) {
}
func (l *AtemListener) ExitHeaderFooterColumnLeft(c *atem.HeaderFooterColumnLeftContext) {
}
func (l *AtemListener) ExitHeaderFooterColumnRight(c *atem.HeaderFooterColumnRightContext) {
}
func (l *AtemListener) ExitHeaderFooterCommemoration(c *atem.HeaderFooterCommemorationContext) {
}
func (l *AtemListener) ExitHeaderFooterDate(c *atem.HeaderFooterDateContext) {
}
func (l *AtemListener) ExitHeaderFooterFragment(c *atem.HeaderFooterFragmentContext) {
}
func (l *AtemListener) ExitHeaderFooterLookup(c *atem.HeaderFooterLookupContext) {
	inHeaderFooterLookup = false //TODO: delete
	lang := c.Language().GetText()
	switch lang {
	case "L1":
		lang = "1"
	case "L2":
		lang = "2"
	default:
		msg := fmt.Sprintf(fmt.Sprintf("invalid lang value %s", lang))
		c.GetParser().NotifyErrorListeners(fmt.Sprintf("%s", msg), c.GetStart(), nil)
		return
	}
	if l.Ctx.PdfSettings.Empty() {
		l.WriteContent(fmt.Sprintf(" ver %s", lang))
	} else {
		l.Ctx.PdfSettings.Write(fmt.Sprintf(" ver %s", lang))
	}
}
func (l *AtemListener) ExitHeaderFooterPageNumber(c *atem.HeaderFooterPageNumberContext) {
}
func (l *AtemListener) ExitHeaderFooterText(c *atem.HeaderFooterTextContext) {
}
func (l *AtemListener) ExitHeaderFooterTitle(c *atem.HeaderFooterTitleContext) {
}
func (l *AtemListener) ExitHymn(c *atem.HymnContext) {
}
func (l *AtemListener) ExitImportBlock(c *atem.ImportBlockContext) {
	l.Ctx.StatementContext.InImportBlock = false
}
func (l *AtemListener) ExitInsertBreak(c *atem.InsertBreakContext) {
}
func (l *AtemListener) ExitLanguage(c *atem.LanguageContext) {
}
func (l *AtemListener) ExitLdp(c *atem.LdpContext) {
}
func (l *AtemListener) ExitLdpType(c *atem.LdpTypeContext) {
}
func (l *AtemListener) ExitLookup(c *atem.LookupContext) {
	//	checkDebug()
	l.WriteSuffixToContent()
	l.Ctx.StatementContext.InRid = false
}
func (l *AtemListener) ExitMcDay(c *atem.McDayContext) {
}
func (l *AtemListener) ExitMedia(c *atem.MediaContext) {
}
func (l *AtemListener) ExitModeOfWeekSet(c *atem.ModeOfWeekSetContext) {
	l.WriteContent(": {")
}
func (l *AtemListener) ExitModeTypes(c *atem.ModeTypesContext) {
}
func (l *AtemListener) ExitMonthName(c *atem.MonthNameContext) {
}
func (l *AtemListener) ExitPageFooterEven(c *atem.PageFooterEvenContext) {
}
func (l *AtemListener) ExitPageFooterOdd(c *atem.PageFooterOddContext) {
}
func (l *AtemListener) ExitPageHeaderEven(c *atem.PageHeaderEvenContext) {
}
func (l *AtemListener) ExitPageHeaderOdd(c *atem.PageHeaderOddContext) {
}
func (l *AtemListener) ExitPageKeepWithNext(c *atem.PageKeepWithNextContext) {
}
func (l *AtemListener) ExitPageNumber(c *atem.PageNumberContext) {
}
func (l *AtemListener) ExitParagraph(c *atem.ParagraphContext) {
	l.Ctx.StatementContext.InPara = false
	if l.Ctx.StatementContext.CloseHtmlOnlyBlock {
		l.closeHtmlOnlyBlock()
	}
}
func (l *AtemListener) ExitPassThroughHtml(c *atem.PassThroughHtmlContext) {
}
func (l *AtemListener) ExitQualifiedName(c *atem.QualifiedNameContext) {
}
func (l *AtemListener) ExitQualifiedNameWithWildCard(c *atem.QualifiedNameWithWildCardContext) {
}
func (l *AtemListener) ExitReading(c *atem.ReadingContext) {
}
func (l *AtemListener) ExitResourceText(c *atem.ResourceTextContext) {
	if l.Ctx.StatementContext.InsertingAnchorHref {
		l.WriteContent(fmt.Sprintf(` target _blank label sid "%s"`, l.Ctx.StatementContext.AnchorLabel))
		l.Ctx.StatementContext.InsertingAnchorHref = false
		l.Ctx.StatementContext.InsertingAnchorLabel = false
		l.Ctx.StatementContext.AnchorLabel = ""
	}
	l.WriteSuffixToContent()
}
func (l *AtemListener) ExitRestoreLocale(c *atem.RestoreLocaleContext) {
}
func (l *AtemListener) ExitRole(c *atem.RoleContext) {
	l.Ctx.StatementContext.InRoleBlock = false
}
func (l *AtemListener) ExitRubric(c *atem.RubricContext) {
}
func (l *AtemListener) ExitSection(c *atem.SectionContext) {
	l.WriteContent(" ")
	l.Ctx.SectionStack.Pop()
	l.Ctx.PopStatementContext()
	//	l.Ctx.PopIndent()
}
func (l *AtemListener) ExitSectionFragment(c *atem.SectionFragmentContext) {
}
func (l *AtemListener) ExitSetLocale(c *atem.SetLocaleContext) {
}
func (l *AtemListener) ExitSubTitle(c *atem.SubTitleContext) {
}
func (l *AtemListener) ExitSundaysBeforeTriodionCase(c *atem.SundaysBeforeTriodionCaseContext) {
	l.Ctx.PopIndent()
	l.WriteContent(fmt.Sprintf("\n%s}", l.Ctx.Indent()))
}
func (l *AtemListener) ExitTaggedText(c *atem.TaggedTextContext) {
	l.WriteContent(" ) ")
	l.Ctx.StatementContext.InTaggedTextBlock = false
}
func (l *AtemListener) ExitTemplateFragment(c *atem.TemplateFragmentContext) {
}
func (l *AtemListener) ExitTemplateStatus(c *atem.TemplateStatusContext) {
}
func (l *AtemListener) ExitTemplateStatuses(c *atem.TemplateStatusesContext) {
}
func (l *AtemListener) ExitTemplateTitle(c *atem.TemplateTitleContext) {
	l.Ctx.StatementContext.InTemplateTitle = false
}
func (l *AtemListener) ExitTitle(c *atem.TitleContext) {
	if l.Ctx.StatementContext.CloseHtmlOnlyBlock {
		l.closeHtmlOnlyBlock()
	}
}
func (l *AtemListener) ExitVerse(c *atem.VerseContext) {
}
func (l *AtemListener) ExitVersion(c *atem.VersionContext) {
}
func (l *AtemListener) ExitVersionSwitch(c *atem.VersionSwitchContext) {
}
func (l *AtemListener) ExitVersionSwitchType(c *atem.VersionSwitchTypeContext) {
}
func (l *AtemListener) ExitWhenDate(c *atem.WhenDateContext) {
	l.Ctx.PopStatementContext()
	l.Ctx.PopIndent()
	l.WriteContent(fmt.Sprintf("\n%s}", l.Ctx.Indent()))
}
func (l *AtemListener) ExitWhenDateCase(c *atem.WhenDateCaseContext) {
	l.Ctx.PopIndent()
	l.WriteContent(fmt.Sprintf("\n%s}", l.Ctx.Indent()))
}
func (l *AtemListener) ExitWhenDayName(c *atem.WhenDayNameContext) {
	l.Ctx.PopStatementContext()
	l.Ctx.PopIndent()
	l.WriteContent(fmt.Sprintf("\n%s}", l.Ctx.Indent()))
}
func (l *AtemListener) ExitWhenDayNameCase(c *atem.WhenDayNameCaseContext) {
	l.Ctx.PopIndent()
	l.WriteContent(fmt.Sprintf("\n%s}", l.Ctx.Indent()))
}
func (l *AtemListener) ExitWhenExists(c *atem.WhenExistsContext) {
	checkDebug()
	l.Ctx.PopStatementContext()
	l.Ctx.PopIndent()
	l.WriteContent(fmt.Sprintf("\n%s}", l.Ctx.Indent()))

	//l.Ctx.StatementContext.InExistsCase = false
	//l.Ctx.StatementContext.InExistsCaseCount = 0
	//l.Ctx.PopStatementContext()
	//l.Ctx.PopIndent()
	//l.WriteContent(fmt.Sprintf("\n%s}", l.Ctx.Indent()))
}
func (l *AtemListener) ExitWhenExistsCase(c *atem.WhenExistsCaseContext) {
	checkDebug()
	//if l.Ctx.StatementContext.AbstractComponentFound {
	//	l.Ctx.StatementContext.AbstractComponentFound = false
	//} else {
	//	l.WriteContent(" {")
	//}
	//l.Ctx.PopIndent()
	if l.Ctx.StatementContext.InExistsCase { // there was no abstractComponent
		l.WriteContent(": {")
		l.Ctx.StatementContext.InExistsCase = false
	}
	l.Ctx.PopIndent()
	l.WriteContent(fmt.Sprintf("\n%s}", l.Ctx.Indent()))
}
func (l *AtemListener) ExitWhenLukanCycleDay(c *atem.WhenLukanCycleDayContext) {
	l.Ctx.PopStatementContext()
	l.Ctx.PopIndent()
	l.WriteContent(fmt.Sprintf("\n%s}", l.Ctx.Indent()))
}
func (l *AtemListener) ExitWhenModeOfWeek(c *atem.WhenModeOfWeekContext) {
	l.Ctx.PopStatementContext()
	l.Ctx.PopIndent()
	l.WriteContent(fmt.Sprintf("\n%s}", l.Ctx.Indent()))
}
func (l *AtemListener) ExitWhenModeOfWeekCase(c *atem.WhenModeOfWeekCaseContext) {
	l.Ctx.PopIndent()
	l.WriteContent(fmt.Sprintf("\n%s}", l.Ctx.Indent()))
}
func (l *AtemListener) ExitWhenMovableCycleDay(c *atem.WhenMovableCycleDayContext) {
	l.Ctx.PopStatementContext()
	l.Ctx.PopIndent()
	l.WriteContent(fmt.Sprintf("\n%s}", l.Ctx.Indent()))
}
func (l *AtemListener) ExitWhenOther(c *atem.WhenOtherContext) {
	checkDebug()
	l.Ctx.PopIndent()
	//	if !l.Ctx.StatementContext.InExists {
	l.WriteContent(fmt.Sprintf("\n%s}", l.Ctx.Indent()))
	//	}
}
func (l *AtemListener) ExitWhenPeriodCase(c *atem.WhenPeriodCaseContext) {
	l.Ctx.PopIndent()
	l.WriteContent(fmt.Sprintf("\n%s}", l.Ctx.Indent()))
}
func (l *AtemListener) ExitWhenSundayAfterElevationOfCrossDay(c *atem.WhenSundayAfterElevationOfCrossDayContext) {
	l.Ctx.PopStatementContext()
	l.Ctx.PopIndent()
	l.WriteContent(fmt.Sprintf("\n%s}", l.Ctx.Indent()))
}
func (l *AtemListener) ExitWhenSundaysBeforeTriodion(c *atem.WhenSundaysBeforeTriodionContext) {
	l.Ctx.PopStatementContext()
	l.Ctx.PopIndent()
	l.WriteContent(fmt.Sprintf("\n%s}", l.Ctx.Indent()))
}

// integerSlice converts a slice of TerminalNode into a slice of int.
// The expectation, of course, is that the Terminal Node values are integers though represented as strings.
func integerSlice(nodes []antlr.TerminalNode) ([]int, error) {
	var intSlice []int
	for _, node := range nodes {
		val, err := strconv.Atoi(node.GetText())
		if err != nil {
			return nil, err
		}
		intSlice = append(intSlice, val)
	}
	return intSlice, nil
}
