package atem2lml

import (
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"path"
	"testing"
)

func TestSetCss(t *testing.T) {
	var err error
	testDir := t.Name()
	htmlCssPath := path.Join(testDir, "app.css")
	pdfCssPath := path.Join(testDir, "pdf.css")
	err = ltfile.WriteFile(htmlCssPath, `p.test {}`)
	if err != nil {
		t.Error(err)
		return
	}
	err = ltfile.WriteFile(pdfCssPath, `p.test {}`)
	if err != nil {
		t.Error(err)
		return
	}
	_, err = SetCss(nil, htmlCssPath, pdfCssPath)
	if err != nil {
		t.Error(err)
		return
	}
	var css string
	css, err = ltfile.GetFileContent(htmlCssPath)
	if err != nil {
		t.Error(err)
		return
	}
	if len(css) == 0 {
		t.Error("html css is empty")
	}
	css, err = ltfile.GetFileContent(pdfCssPath)
	if err != nil {
		t.Error(err)
		return
	}
	if len(css) == 0 {
		t.Error("pdf css is empty")
		return
	}
}
