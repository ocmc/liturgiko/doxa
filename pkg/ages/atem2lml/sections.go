package atem2lml

import (
	"fmt"
	"github.com/emirpasic/gods/stacks/arraystack"
	"github.com/emirpasic/gods/trees/btree"
	"github.com/liturgiko/doxa/pkg/css"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/filter"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/stack"
	"path"
	"sort"
	"strings"
	"text/scanner"
)

// constants

var EndInsert = "End-Insert"
var EndSection = "End-Section"
var InsertSection = "Insert_section"
var InsertTemplate = "Insert_template"
var Section = "Section"

// TemplateNode is used as a value in a tree, with the key being the name of the template.
// ID is the one that will be used by Doxa templates that use the liturgical markup language (lml).
// Path gives the path to the atem file.
// Sections is a tree index of the sections found in the template.
type TemplateNode struct {
	ID       string
	Path     string
	Sections *btree.Tree
}

// SectionNode holds the name of the template it occurs in and the path to the section.
// The TemplateName can be used to find the template in the Templates btree.
// The Path can be used as the Doxa path in an LML insert template.
type SectionNode struct {
	TemplateName string
	Path         string
}

// Index creates an index of all *.atem (AGES Template) files occurring recursively within the dirIn.
// It also creates an index of all sections in each template.
// The 1st btree returned is the templates index.  Its key is the template name and value is TemplateNode.
// The 2nd btree returned is the sections index.  Its key is the fully qualified path to the section.  Its value is the template name.
// So, if a key exists in the sections btree, the value can be used to retrieve the corresponding value
// in the templates btree.
// If dirIn does not exist, an error is returned and the trees will be empty.
func Index(dirIn, library string) (*btree.Tree, *btree.Tree, error) {
	paths := arraystack.New()
	internalPaths := arraystack.New()

	// tTree key = template name, value = TemplateNode
	tTree := btree.NewWithStringComparator(3) // order indicates max number of children per node
	// sTree key = template + "." + internal path to section, value = Template Name
	sTree := btree.NewWithStringComparator(3) // order indicates max number of children per node

	files, err := ltfile.FileMatcher(dirIn, "atem", nil)
	if err != nil {
		return tTree, sTree, err
	}
	for _, f := range files {
		var tNode TemplateNode
		tNode.Path = f
		tNode.Sections = btree.NewWithStringComparator(3)
		parts := strings.Split(f, "a-templates/")
		if len(parts) == 2 {
			filename := path.Base(f)
			filename = filename[0 : len(filename)-5]
			currentAGESPath := filename
			currentDoxaPath := library + path.Dir(parts[1]) + pathSeparator + filename
			tNode.ID = currentDoxaPath
			currentState = LineStart
			// read file in as a string
			content, err := ltfile.ReadAll(f)
			if err != nil {
				fmt.Println(err)
			}
			text := string(content)
			// Problem: text/scanner treats hyphen as a word separator.  Change to underscore to get around this.
			text = strings.ReplaceAll(text, "-", "_")

			var s scanner.Scanner
			s.Init(strings.NewReader(text))
			s.Whitespace = 1<<':' | 1<<'-' | 1<<'\t' | 1<<'\n' | 1<<' ' // treat these as white space

			for tok := s.Scan(); tok != scanner.EOF; tok = s.Scan() {
				token := s.TokenText()
				switch token {
				case "\r":
					continue
				case Section:
					currentState = GotSection
				case EndSection:
					if v, ok := paths.Pop(); ok {
						currentDoxaPath = v.(string)
					} else {
						fmt.Println("expected item from stack but stack has no values")
					}
					if v, ok := internalPaths.Pop(); ok {
						currentAGESPath = v.(string)
					} else {
						fmt.Println("expected item from stack but stack has no values")
					}
					currentState = Neutral
				default:
					switch currentState {
					case GotSection:
						internalPaths.Push(currentAGESPath)
						currentAGESPath = currentAGESPath + "." + token
						paths.Push(currentDoxaPath)
						currentDoxaPath = currentDoxaPath + pathSeparator + token
						var sNode SectionNode
						sNode.TemplateName = filename
						sNode.Path = currentDoxaPath
						sTree.Put(currentAGESPath, sNode)
						tNode.Sections.Put(currentAGESPath, currentDoxaPath)
						currentState = Neutral
					}
				}
				tTree.Put(filename, tNode)
			}
		} else {
			fmt.Errorf("Should be using a-templates in path %s\n", f)
		}
	}
	return tTree, sTree, nil
}
func Indexer(dirIn string) (int, int, error) {
	var fileCount, sectionCount int
	var err error
	var files []string
	files, err = ltfile.FileMatcher(dirIn, "atem", nil)
	if err != nil {
		return 0, 0, err
	}
	for _, f := range files {
		fileCount++
		var lines []string
		// read file in as a string
		lines, err = ltfile.GetFileLines(f)
		if err != nil {
			return 0, 0, fmt.Errorf("error reading %s: %v", f, err)
		}
		for _, line := range lines {
			if strings.HasPrefix(strings.TrimSpace(line), InsertSection) {
				parts := strings.Split(line, InsertSection)
				if len(parts) == 2 {
					parts = strings.Split(parts[1], EndInsert)
					if len(parts) == 2 {
						sectionCount++
					} else {
						return 0, 0, fmt.Errorf("file %s, could not split %s using End-Insert", f, line)
					}
				} else {
					return 0, 0, fmt.Errorf("file %s, could not split %s using Insert_section", f, line)
				}
			}
		}
	}
	return fileCount, sectionCount, nil
}

type TemplateIndex struct {
	DuplicateSections     []string
	SectionMetaDataMap    map[string]*SectionMetaData
	SectionMap            map[string]string
	TemplateMap           map[string][]string // the val is a slice because there are some duplicate template names
	TemplateSectionMap    map[string]map[string]string
	DirPath               string
	TemplatePathSeparator string
	AlphaSections         []string
	AlphaTemplates        []string
	TemplateDataMap       map[string]*TemplateData // key = lmlTemplateId
	InsertMap             map[string]string        // key = lmlTemplateId:lineNumber, val = LML insert value
	AtemId2LmlMap         map[string]string
	HtmlStyleSheet        *css.StyleSheet
	HtmlStyleSheetPath    string // path to assets/css/app.css
	PdfStyleSheet         *css.StyleSheet
	PdfStyleSheetPath     string // path to assets/pdf/css/pdf.css
}

func NewTemplateIndex(dirPath, templatePathSeparator, cssFile string, pdfCssFile string) (*TemplateIndex, error) {
	t := new(TemplateIndex)
	t.DirPath = dirPath
	t.TemplatePathSeparator = templatePathSeparator
	t.SectionMetaDataMap = make(map[string]*SectionMetaData)
	t.SectionMap = make(map[string]string)
	t.TemplateMap = make(map[string][]string)
	t.TemplateSectionMap = make(map[string]map[string]string)
	t.TemplateDataMap = make(map[string]*TemplateData)
	t.InsertMap = make(map[string]string)
	t.AtemId2LmlMap = make(map[string]string)
	return SetCss(t, cssFile, pdfCssFile)
}
func SetCss(t *TemplateIndex, cssFile, pdfCssFile string) (*TemplateIndex, error) {
	if t == nil {
		t = new(TemplateIndex)
	}
	var err error
	// HTML css stylesheet
	t.HtmlStyleSheetPath = cssFile
	t.HtmlStyleSheet, err = css.NewStyleSheet("html", t.HtmlStyleSheetPath, false)
	if err != nil {
		return nil, err
	}
	// read alwb.css directly from github as a file
	// and add any rules that it has that are not in the local app.css
	//err = t.HtmlStyleSheet.AddMissingRulesForHtml(css.AlwbStyleSheetPath)
	if err != nil {
		return nil, err
	}
	//err = t.HtmlStyleSheet.SerializeMissingRules()
	//if err != nil {
	//	return nil, err
	//}

	//// PDF css stylesheet
	t.PdfStyleSheetPath = pdfCssFile
	t.PdfStyleSheet, err = css.NewStyleSheet("pdf", t.PdfStyleSheetPath, false)
	if err != nil {
		return nil, err
	}
	// read remote pdf stylesheet directly from github as a file
	// and add any rules that it has that are not in the local pdf stylesheet
	//err = t.PdfStyleSheet.AddMissingRulesForPdf(css.AlwbPdfStyleSheetPath)
	if err != nil {
		return nil, err
	}
	//err = t.PdfStyleSheet.SerializeMissingRules()
	if err != nil {
		return nil, err
	}
	return t, nil
}

// AddSection adds the section to t.SectionMap and t.TemplateSectionMap
func (t *TemplateIndex) AddSection(lineNbr int, template, key string, sectionStack *stack.StringStack) {
	var sectionStackJoin string
	if sectionStack == nil {
		sectionStackJoin = ""
	} else {
		sectionStackJoin = sectionStack.Join("/")
	}
	var sectionMapValue string
	if len(sectionStackJoin) == 0 {
		sectionMapValue = fmt.Sprintf("%s/bl.%s", template, key)
	} else {
		sectionMapValue = fmt.Sprintf("%s/%s/bl.%s", template, sectionStackJoin, key)
	}
	sectionStack.Push(key)
	base := path.Base(template)
	sectionPath := sectionStack.Join(".")
	sectionMapKey := fmt.Sprintf("%s.%s", base, sectionPath)
	lcSectionMapValue := strings.ToLower(sectionMapValue)
	var exists bool
	var dupSection *SectionMetaData
	if dupSection, exists = t.SectionMetaDataMap[lcSectionMapValue]; !exists {
		dupSection = new(SectionMetaData)
		dupSection.AlwbTemplate = template
		dupSection.SectionPath = sectionPath
		dupSection.AlwbInsertPath = fmt.Sprintf("Insert_section %s.%s End-Insert", base, sectionPath)
		dupSection.Lines = append(dupSection.Lines, lineNbr)
		t.SectionMetaDataMap[lcSectionMapValue] = dupSection
	} else { // duplicate
		exists = false // repurpose and use to see if a line number exists
		for _, l := range dupSection.Lines {
			if l == lineNbr {
				exists = true
				break
			}
		}
		if !exists { // this line number has not been recorded yet
			dupSection.Lines = append(dupSection.Lines, lineNbr)
			t.SectionMetaDataMap[sectionMapValue] = dupSection
		}
	}
	t.SectionMap[sectionMapKey] = sectionMapValue
	var tMap map[string]string
	if tMap = t.TemplateSectionMap[template]; tMap == nil {
		tMap = make(map[string]string)
	}
	tMapKey := sectionStack.Join("/")
	tMap[tMapKey] = sectionMapValue
	t.TemplateSectionMap[template] = tMap
	sectionStack.Pop() // so there is no side effect of calling the function
}

// ExistsCssRule checks either the HTML or PDF css map or both, based on checkHtml and checkPdf
// If both are checked, the func returns true if the rule is found in either html or pdf css.
func (t *TemplateIndex) ExistsCssRule(r string, checkHtml, checkPdf bool) bool {
	var exists, existsHtml, existsPdf bool
	if checkHtml {
		existsHtml = t.ExistsCssRuleForHtml(r)
	}
	if checkPdf {
		existsPdf = t.ExistsCssRuleForPdf(r)
	}
	if checkHtml && checkPdf {
		exists = existsHtml || existsPdf
		return exists
	}
	if checkHtml {
		return existsHtml
	}
	if checkPdf {
		return existsHtml
	}
	return exists
}
func (t *TemplateIndex) ExistsCssRuleForHtml(r string) bool {
	if len(r) == 0 {
		return false
	}
	var exists bool
	exists = t.HtmlStyleSheet.ContainsRule(r)
	nakedRule := r[4:]
	if !exists {
		exists = t.HtmlStyleSheet.ContainsRule(nakedRule)
	}
	return exists
}
func (t *TemplateIndex) ExistsCssRuleForPdf(r string) bool {
	if len(r) == 0 {
		return false
	}
	var exists bool
	if t.PdfStyleSheet == nil {
		doxlog.Errorf("PdfStyleSheet is nil")
		return false
	}
	exists = t.PdfStyleSheet.ContainsRule(r)
	nakedRule := r[4:]
	if !exists {
		exists = t.PdfStyleSheet.ContainsRule(nakedRule)
	}
	return exists
}
func (t *TemplateIndex) SetAlphas() {
	t.setAlphaSections()
	t.setAlphaTemplates()
}
func (t *TemplateIndex) setAlphaSections() {
	keys := make([]string, len(t.SectionMap))
	i := 0
	for k := range t.SectionMap {
		keys[i] = k
		i++
	}
	sort.Strings(keys)
	t.AlphaSections = keys
}
func (t *TemplateIndex) setAlphaTemplates() {
	keys := make([]string, len(t.TemplateMap))
	i := 0
	for k := range t.TemplateMap {
		keys[i] = k
		i++
	}
	sort.Strings(keys)
	t.AlphaTemplates = keys
}
func (t *TemplateIndex) TemplatePath(name string) (string, error) {
	if val, ok := t.AtemId2LmlMap[name]; ok {
		return val, nil
	}
	return "", fmt.Errorf("key %s not found in templateIndex", name)
}
func (t *TemplateIndex) TemplateCount() int {
	return len(t.AtemId2LmlMap)
}
func (t *TemplateIndex) PrintTemplateMap() {
	for k, v := range t.TemplateMap {
		fmt.Printf("%s = %s\n", k, v)
	}
}
func (t *TemplateIndex) EmptySections(files []string) ([]ParseError, error) {
	sectionLine := -1
	var err error
	var errors []ParseError
	for _, f := range files {
		var lines []string
		lines, err = ltfile.GetFileLines(f)
		if err != nil {
			return nil, fmt.Errorf("error in pkg/sections.go, EmptySections() opening file %s: %v ", f, err)
		}
		var inCommentBlock bool
		for i, line := range lines {
			line = strings.TrimSpace(line)
			if strings.HasPrefix(line, "//") {
				// ignore.  It is commented out
				continue
			}
			if strings.HasPrefix(line, "*/") { // end of comment block
				inCommentBlock = false
				continue
			} else if strings.HasPrefix(line, "/*") { // we are entering a comment block
				inCommentBlock = true
				continue
			}
			if inCommentBlock {
				continue
			}

			if strings.HasPrefix(line, Section) && strings.Contains(line, EndSection) {
				sectionLine = -1
				continue
			}
			if strings.HasPrefix(line, Section) {
				sectionLine = i
				continue
			}
			if strings.Contains(line, EndSection) {
				if sectionLine+1 == i {
					var parseError ParseError
					parseError.TemplateID = f
					parseError.Line = i
					parseError.Message = fmt.Sprintf("empty section")
					errors = append(errors, parseError)
				} else {
					sectionLine = -1
				}
			}
		}
	}
	return errors, nil
}

type TemplateData struct {
	IdPath     string
	ImportMap  map[string]string // only atem imports, not ares
	InsertMap  map[string]string
	SectionMap map[string]string
}

func NewTemplateData() *TemplateData {
	t := new(TemplateData)
	t.ImportMap = make(map[string]string)
	t.InsertMap = make(map[string]string)
	t.SectionMap = make(map[string]string)
	return t
}

// AddSection adds the section to t.SectionMap and t.TemplateSectionMap
func (t *TemplateData) AddSection(template, key string, sectionStack *stack.StringStack) {
	var sectionStackJoin string
	if sectionStack.Size() == 0 {
		sectionStackJoin = ""
	} else {
		sectionStackJoin = sectionStack.Join("/")
	}
	var sectionMapValue string
	if len(sectionStackJoin) == 0 {
		sectionMapValue = fmt.Sprintf("%s/bl.%s", template, key)
	} else {
		sectionMapValue = fmt.Sprintf("%s/%s/bl.%s", template, sectionStackJoin, key)
	}
	sectionStack.Push(key)
	base := path.Base(template)
	sectionMapKey := fmt.Sprintf("%s/%s", base, sectionStack.Join("/"))
	t.SectionMap[sectionMapKey] = sectionMapValue
	sectionStack.Pop() // so there is no side effect of calling the function
}

func (t *TemplateIndex) ResolveImport(i, leftOver string) (string, string, bool) {
	var ok bool
	var val string
	if val, ok = t.AtemId2LmlMap[i]; ok {
		return val, leftOver, true
	} else {
		// remove the last segment of the import and recursively call self
		parts := strings.Split(i, ".")
		if len(parts) == 1 {
			return "", "", false
		}
		return t.ResolveImport(strings.Join(parts[:len(parts)-1], "."), parts[len(parts)-1])
	}
}

func (t *TemplateIndex) IndexTemplates(filters *filter.Excluder) ([]ParseError, error) {
	var err error
	var errors []ParseError
	var files []string
	var relativeFilePath string
	files, err = ltfile.FileMatcher(t.DirPath, "atem", nil)
	if err != nil {
		return nil, err
	}
	fCount := len(files)
	fmt.Printf("File Count: %d\n", fCount)
	// 1st pass through files to get relative ID paths.
	//     We need this in order to determine whether
	//     an import is for an atem (template) vs an ares file.
	//     Note: the base (filename without the .atem extension)
	//     is the unique ID of an atem template, but not an LML template.
	//     LML template IDs are fully qualified.
	//     For example, se.m10.d04.ma is unique for an atem, but not for LML.
	//     For LML, it must be a-templates/Dated-Services/m10/d04/se.m10.d04.ma
	for _, f := range files {
		if filters.ContainedIn(f) {
			continue
		}
		relativeFilePath, err = t.CanonicalRelativeFilePath(f)
		if err != nil {
			return nil, fmt.Errorf("sections.IndexTemplates error getting relative path for %s: %v", f, err)
		}
		base := path.Base(relativeFilePath)
		t.AtemId2LmlMap[base] = relativeFilePath
	}
	// 2nd pass through files to get import, and section info
	for _, f := range files {
		if filters.ContainedIn(f) {
			continue
		}
		var inCommentBlock bool
		sectionStack := new(stack.StringStack)
		tData := NewTemplateData()
		relativeFilePath, err = t.CanonicalRelativeFilePath(f)
		if err != nil {
			return nil, err
		}
		tData.IdPath = relativeFilePath
		var lines []string
		lines, err = ltfile.GetFileLines(f)
		if err != nil {
			return nil, fmt.Errorf("error reading %s: %v", f, err)
		}
		for i, line := range lines {
			line = strings.TrimSpace(line)
			if strings.Contains(line, "*/") { // end of comment block
				inCommentBlock = false
				continue
			} else if strings.Contains(line, "/*") { // we are entering a comment block
				inCommentBlock = true
				continue
			}
			if strings.HasPrefix(line, "//") { // commented out line
				continue
			}
			if inCommentBlock {
				continue
			}

			parts := strings.Split(line, "//") // get rid of end of line comment, if it has one
			line = strings.TrimSpace(parts[0])
			if strings.HasPrefix(line, "import") {
				if strings.Contains(line, "_gr_GR_") ||
					strings.Contains(line, "_en_US_") ||
					strings.Contains(line, "_en_UK_") {
					continue // this is a sid ares import
				}

				fileImport := line[7 : len(line)-2]
				fileImport = strings.TrimSpace(fileImport)
				if fileImport == "client" ||
					fileImport == "da" ||
					fileImport == "eo" ||
					fileImport == "me" ||
					fileImport == "oc" ||
					fileImport == "pe" ||
					fileImport == "sy" ||
					fileImport == "tr" ||
					fileImport == "ty" ||
					strings.HasPrefix(fileImport, "le.") {
					continue // this is a rid ares import
				}
				if fileImport == "iTags" ||
					fileImport == "bTags" ||
					fileImport == "roles" ||
					fileImport == "rules" {
					continue // these are css class names
				}
				var ok bool
				var val string
				var sectionPart string
				if val, sectionPart, ok = t.ResolveImport(fileImport, ""); ok {
					if len(sectionPart) > 0 {
						if strings.Index(sectionPart, ".") > 1 {
							sectionPart = strings.ReplaceAll(sectionPart, ".", "/")
						}
						tData.ImportMap[fileImport] = fmt.Sprintf("%s/%s", val, sectionPart)
					} else {
						tData.ImportMap[fileImport] = val
					}
				} else {
					// template does not exist, bogus import
					var parseError ParseError
					parseError.TemplateID = relativeFilePath
					parseError.Line = i
					parseError.Message = fmt.Sprintf("imported template %s does not exist", fileImport)
					errors = append(errors, parseError)
				}
			} else if strings.HasPrefix(line, Section) {
				key := line[8:]
				parts = strings.Split(key, "//") // get rid of comment, if it has one
				key = strings.TrimSpace(parts[0])
				var okToPush = true
				if strings.Contains(key, EndSection) {
					parts = strings.Split(key, EndSection)
					key = strings.TrimSpace(parts[0])
					okToPush = false
				}
				tData.AddSection(relativeFilePath, key, sectionStack)
				t.AddSection(i, relativeFilePath, key, sectionStack)
				if okToPush {
					sectionStack.Push(key)
				}
			} else if strings.HasPrefix(line, EndSection) {
				sectionStack.Pop()
			}
		} // end range lines
		doxlog.Debugf("adding %s as key to TemplateDataMap", relativeFilePath)
		t.TemplateDataMap[relativeFilePath] = tData
	}
	// 3rd pass through files to identify and convert inserts to LML paths.
	for _, f := range files {
		var inCommentBlock bool
		if filters.ContainedIn(f) {
			continue
		}
		relativeFilePath, err = t.CanonicalRelativeFilePath(f)
		if err != nil {
			return nil, err
		}
		var tData *TemplateData
		var ok bool
		if tData, ok = t.TemplateDataMap[relativeFilePath]; !ok {
			return nil, fmt.Errorf("sections.IndexTemplates could not find %s in t.TemplateDataMap", relativeFilePath)
		}
		sectionStack := new(stack.StringStack)
		var lines []string
		lines, err = ltfile.GetFileLines(f)
		if err != nil {
			return nil, fmt.Errorf("error reading %s: %v", f, err)
		}
		for i, line := range lines {
			line = strings.TrimSpace(line)
			if strings.Contains(line, "*/") { // end of comment block
				inCommentBlock = false
				continue
			} else if strings.Contains(line, "/*") { // we are entering a comment block
				inCommentBlock = true
				continue
			}
			if strings.HasPrefix(line, "//") { // commented out
				continue
			}
			if inCommentBlock {
				continue
			}
			parts := strings.Split(line, "//") // get rid of comment, if it has one
			line = strings.TrimSpace(parts[0])

			if strings.Contains(strings.TrimSpace(line), InsertSection) {
				key := GetInsertKey(line, InsertSection, EndInsert)
				var lmlPath string
				// convert the insert ATEM path to an LML path
				lmlPath, err = t.AtemInsert2Lml(key, relativeFilePath, sectionStack)
				if err != nil { // could not be resolved to an LML path
					var parseError ParseError
					parseError.TemplateID = relativeFilePath
					parseError.Line = i
					parseError.Message = fmt.Sprintf("insert %s: reference section could not be found: %v", key, err)
					errors = append(errors, parseError)
					continue
				}
				t.InsertMap[lmlPath] = lmlPath
				tData.InsertMap[fmt.Sprintf("%s:%d", relativeFilePath, i)] = lmlPath
			} else if strings.Contains(strings.TrimSpace(line), InsertTemplate) {
				key := GetInsertKey(line, InsertTemplate, EndInsert)
				var val string
				if val, ok = t.AtemId2LmlMap[key]; ok {
					tData.InsertMap[fmt.Sprintf("%s:%d", relativeFilePath, i)] = val
				} else {
					fmt.Sprintf("TODO delete me or handle")
				}
			} else if strings.HasPrefix(line, Section) {
				key := line[8:]
				parts := strings.Split(key, "//") // get rid of comment, if it has one
				key = strings.TrimSpace(parts[0])
				var okToPush = true
				if strings.Contains(key, EndSection) {
					parts = strings.Split(key, EndSection)
					key = strings.TrimSpace(parts[0])
					okToPush = false
				}
				tData.AddSection(relativeFilePath, key, sectionStack)
				t.AddSection(i, relativeFilePath, key, sectionStack)
				if okToPush {
					sectionStack.Push(key)
				}
			} else if strings.HasPrefix(strings.TrimSpace(line), EndSection) {
				sectionStack.Pop()
			}
		}
		t.TemplateDataMap[relativeFilePath] = tData
	}
	t.DuplicateSections = []string{}
	for _, v := range t.SectionMetaDataMap {
		if len(v.Lines) > 1 {
			sb := strings.Builder{}
			sb.WriteString(fmt.Sprintf("\n\t%s // ", v.AlwbInsertPath))
			for _, l := range v.Lines {
				sb.WriteString(fmt.Sprintf(" %d", l))
			}
			t.DuplicateSections = append(t.DuplicateSections, sb.String())
		}
	}
	t.SetAlphas()
	// TODO: document why we serialize the Maps
	err = t.SerializeMaps(doxlog.LogDir())
	var duplicateTemplateErrors []ParseError
	duplicateTemplateErrors = t.GetDuplicateTemplateIds()
	for _, parseError := range duplicateTemplateErrors {
		errors = append(errors, parseError)
	}
	return errors, err
}
func GetInsertKey(line, openSplitter, closeSplitter string) string {
	var key string
	if strings.HasPrefix(line, openSplitter) {
		key = line[len(openSplitter)+1:]
	} else {
		parts := strings.Split(line, openSplitter)
		if len(parts) > 1 {
			key = strings.TrimSpace(parts[1])
		}
	}
	if strings.Contains(key, closeSplitter) {
		parts := strings.Split(key, closeSplitter)
		if len(parts) > 1 {
			key = strings.TrimSpace(parts[0])
		}
	}
	parts := strings.Split(key, "//") // get rid of comment, if it has one
	key = strings.TrimSpace(parts[0])
	return key
}
func (t *TemplateIndex) SerializeMaps(to string) error {
	if t.TemplateMap == nil {
		return fmt.Errorf("error: in pkg/sections SerializeMaps(), t.TemplateMap is nil")
	}
	if t.SectionMap == nil {
		return fmt.Errorf("in sections SerializeMaps(), t.SectionMap is nil")
	}
	out := path.Join(to, "maps", "atemId2Lml.txt")
	err := t.SerializeStringMap(t.AtemId2LmlMap, out)
	if err != nil {
		return err
	}
	out = path.Join(to, "maps", "sections.txt")
	err = t.SerializeStringMap(t.SectionMap, out)
	if err != nil {
		return err
	}
	out = path.Join(to, "maps", "sections")
	return t.SerializeTemplateSectionsMap(out)
}
func (t *TemplateIndex) SerializeTemplateSectionsMap(to string) error {
	if len(t.TemplateSectionMap) == 0 {
		return fmt.Errorf("in pkg/sections SerializeMaps(), t.TemplateSectionMap is empty")
	}
	err := ltfile.DeleteDirRecursively(to)
	if err != nil {
		return err
	}
	var keys []string
	for k, _ := range t.TemplateSectionMap {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	for _, key := range keys {
		out := path.Join(to, fmt.Sprintf("%s.txt", key))
		val := t.TemplateSectionMap[key]
		err = t.SerializeStringMap(val, out)
		if err != nil {
			return fmt.Errorf("sections.SerializeTemplateSectionsMap error processing %s: %v", key, err)
		}
	}

	return nil
}
func (t *TemplateIndex) SerializeStringMap(m map[string]string, to string) error {
	if len(m) == 0 {
		return fmt.Errorf("in pkg/sections SerializeStringMap, map is empty")
	}
	var keys []string
	for k, _ := range m {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	var lines []string
	for _, key := range keys {
		val := m[key]
		lines = append(lines, fmt.Sprintf("%s == %s", key, val))
	}
	if len(lines) == 0 {
		return nil
	}
	err := ltfile.WriteLinesToFile(to, lines)
	if err != nil {
		return fmt.Errorf("error writing %s", to)
	}
	return nil
}
func (t *TemplateIndex) SerializeStringSliceMap(m map[string][]string, to string) error {
	if len(m) == 0 {
		return fmt.Errorf("in pkg/sections SerializeStringSliceMap, map is empty")
	}
	var keys []string
	for k, _ := range m {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	var lines []string
	for _, key := range keys {
		val := m[key]
		lines = append(lines, fmt.Sprintf("%s == %s", key, val))
	}
	if len(lines) == 0 {
		return nil
	}
	err := ltfile.WriteLinesToFile(to, lines)
	if err != nil {
		return fmt.Errorf("error writing %s", to)
	}
	return nil
}
func (t *TemplateIndex) GetDuplicateTemplateIds() []ParseError {
	var errors []ParseError
	for k, v := range t.TemplateMap {
		if len(v) > 1 {
			var parseError ParseError
			parseError.TemplateID = k
			parseError.Line = 1
			parseError.Column = 11
			parseError.Message = fmt.Sprintf("%s occurs as Template ID in %d templates. It should only be declared in one: %v", k, len(v), v)
			errors = append(errors, parseError)
		}
	}
	return errors
}

// AtemInsert2Lml determines the path to the section referenced by an Insert_section.
// First it attempts to find the referenced section within the containing template.
// Then it attempts to find it in an external template by using each imported template in turn.
func (t *TemplateIndex) AtemInsert2Lml(insert, templateId string, scopeStack *stack.StringStack) (string, error) {
	var lmlPath string
	var err error
	// see if it can be resolved from sections within the template
	lmlPath, err = t.AtemInternalInsert2Lml(insert, templateId, scopeStack)
	if err != nil {
		return lmlPath, err
	}
	if len(lmlPath) > 0 {
		return lmlPath, nil
	}
	// see if it can be resolved from external templates
	return t.AtemExternalInsert2Lml(insert, templateId)
}
func (t *TemplateIndex) AtemExternalInsert2Lml(insert, templateId string) (string, error) {
	var found bool
	var lmlPath string
	// first try matching the insert against external templates paths
	// without using the imports.
	if lmlPath, found = t.SectionMap[insert]; found {
		return lmlPath, nil
	}
	// second, try matching the insert using the imports.
	var tData *TemplateData
	if tData, found = t.TemplateDataMap[templateId]; !found {
		return "", fmt.Errorf("sections.AtemExtenalInsert2Lml: could not find template %s in t.TemplateDataMap", templateId)
	}
	if len(tData.ImportMap) > 0 {
		for k, _ := range tData.ImportMap {
			searchPath := fmt.Sprintf("%s.%s", k, insert)
			if lmlPath, found = t.SectionMap[searchPath]; found {
				return lmlPath, nil
			}
		}
	}
	return "", nil
}

// AtemInternalInsert2Lml searches the nested sections internal to the current template
// to see it can match the path with the insert path.  If so, returns the corresponding
// LML path for the insert.
func (t *TemplateIndex) AtemInternalInsert2Lml(insert, templateId string, scopeStack *stack.StringStack) (string, error) {
	c := scopeStack.Copy() // use a copy because we are going to modify it by popping
	j := c.Size()
	base := path.Base(templateId)
	// see if the insert is a fully qualified reference internal to this template
	// with the template ID as the prefix.
	if v, ok := t.SectionMap[insert]; ok {
		return v, nil
	}
	// see if the insert is a fully qualified reference internal to this template
	// without the template ID as the prefix.
	searchPath := fmt.Sprintf("%s.%s", base, insert)
	if v, ok := t.SectionMap[searchPath]; ok {
		return v, nil
	}
	// search for the path within its context.
	for i := 0; i < j; i++ { // keep reducing the path until there is a match
		searchPath = fmt.Sprintf("%s.%s.%s", base, c.Join("."), insert)
		if v, ok := t.SectionMap[searchPath]; ok {
			return v, nil
		}
		c.Pop() // narrow the scope of the search
	}
	return "", nil
}

// MatchingSectionPath searches the section scope from narrowest to broadest, seeking a match for an insert
func (t *TemplateIndex) MatchingSectionPath(insert, templateId string, scopeStack *stack.StringStack, retry bool) (string, error) {
	var val string
	tMapKey := insert
	if strings.Contains(insert, ".") {
		tMapKey = strings.ReplaceAll(insert, ".", "/")
	}
	// see if section is in the current template
	//var tMap map[string]string
	//if tMap = t.TemplateSectionMap[templateId]; tMap == nil {
	//	return "", fmt.Errorf("TemplateIndex.MatchingSectionPath, could not find template %s in t.TemplateSectionMap", templateId) // there was no match
	//}
	//if val = tMap[tMapKey]; val != "" {
	//	return val, nil
	//}
	c := scopeStack.Copy() // use a copy because we are going to modify it by popping
	j := c.Size()
	var tMap map[string]string
	if tMap = t.TemplateSectionMap[templateId]; tMap == nil {
		return "", fmt.Errorf("TemplateIndex.MatchingSectionPath, could not find template %s in t.TemplateSectionMap", templateId) // there was no match
	}
	for i := 0; i < j; i++ { // keep reducing the path until there is a match
		searchPath := fmt.Sprintf("%s/%s", c.Join("/"), tMapKey)
		if len(searchPath) > len(templateId) {
			searchPath = searchPath[len(templateId)+1:]
		}
		if val = tMap[searchPath]; val != "" {
			return val, nil
		}
		//if v, ok := t.SectionMap[searchPath]; ok {
		//	return v, nil
		//}
		c.Pop() // narrow the scope of the search
	}
	// see if section is in an external template
	// TOMORROW - need to
	// 1) check to see if insert has template as part of identifier
	// 2) use each template import to see if can find its tMap.
	//        the imports need to be supplied as a parameter
	//    then use the tMap to find the insert

	for k, v := range t.TemplateMap {
		if k == insert || strings.HasPrefix(insert, fmt.Sprintf("%s.", k)) { // try to match the template name
			if len(v) > 1 {
				// we have a problem
				fmt.Sprintf("TODO: delete me")
			}
			if k == insert {
				val = k
			} else {
				val = insert[len(k)+1:]
			}
			if strings.Contains(val, ".") {
				// convert segments into paths (except for the last one)
				parts := strings.Split(val, ".")
				extraPath := strings.Join(parts[0:len(parts)-1], "/")
				returnVal := fmt.Sprintf("%s/%s/bl.%s", v[0], extraPath, parts[len(parts)-1])
				return returnVal, nil
			}
			return fmt.Sprintf("%s/bl.%s", v[0], val), nil
		}
	}
	if retry {
		return "", fmt.Errorf("in %s, Insert_section %s, could not find referenced template and section", templateId, insert) // there was no match
	} else {
		return t.MatchingSectionPath(insert, templateId, scopeStack, true)
	}
}

// CanonicalRelativeFilePath returns the sub-path starting from
// the root of the templates folder, and converts the filepath
// separator to the canonical (non-windows, forward slash) form.
func (t *TemplateIndex) CanonicalRelativeFilePath(f string) (string, error) {
	parts := strings.Split(f, t.DirPath)
	if len(parts) == 2 {
		rf := parts[1]
		rf = rf[1 : len(rf)-5]
		rf = ltfile.ToUnixPath(rf)
		doxlog.Debugf("%s => \n%s", f, rf)
		return fmt.Sprintf("%s", rf), nil
	} else {
		return "", fmt.Errorf("file path %s not prefixed by %s", f, t.DirPath)
	}
}

type SectionMetaData struct {
	AlwbTemplate   string
	SectionPath    string
	AlwbInsertPath string
	Lines          []int
}
