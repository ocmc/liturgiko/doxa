// Package atem2lml converts ALWB atem files (templates) to LML templates
package atem2lml

import (
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/ages/tags"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/filter"
	"github.com/liturgiko/doxa/pkg/lmlFormatter"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	repos2 "github.com/liturgiko/doxa/pkg/utils/repos"
	"path"
	"sort"
	"strings"
	"sync"
	"time"
)

var ITags = tags.NewTag() // inline tags

func Convert(tempDirIn, dirOut, cssFile string, pdfCssFile string, keyMap *map[string][]*kvs.KeyPath) (error, []ParseError) {
	var allParseErrors []ParseError
	start := time.Now()
	templateDirIn := path.Join(tempDirIn, "AGES-Initiatives", "ages-alwb-templates", "net.ages.liturgical.workbench.templates")
	var reposToClone = []string{"https://github.com/AGES-Initiatives/ages-alwb-templates.git", // templates
		"https://github.com/AGES-Initiatives/ages-alwb-system.git"} // system

	// Clone from github.  Previously cloned files will first be deleted.
	fmt.Println()
	doxlog.Infof("Cloning templates and system library into %s.", tempDirIn)
	if err := repos2.CloneConcurrently(tempDirIn, reposToClone, false, false, 1); err != nil {
		doxlog.Infof("%v", err)
	}
	filters := filter.NewExcluder([]string{"generator.atem",
		"/special-requests/",
		"/tests/",
	})
	doxlog.Info("Indexing templates and sections...")
	templateIndex, err := NewTemplateIndex(templateDirIn, "a-templates/", cssFile, pdfCssFile)
	if err != nil {
		return err, nil
	}
	duplicateTemplates, err := templateIndex.IndexTemplates(filters)
	if err != nil {
		return err, nil
	}
	doxlog.Infof("template count: %d", templateIndex.TemplateCount())
	// report any duplicate sections
	dupsOut := path.Join(doxlog.LogDir(), "bk.test.duplicate.sections.atem")
	if ltfile.FileExists(dupsOut) {
		ltfile.DeleteFile(dupsOut)
	}
	if len(templateIndex.DuplicateSections) > 0 {
		sort.Strings(templateIndex.DuplicateSections)
		sb := strings.Builder{}
		sb.WriteString(fmt.Sprintf("Template bk.test.duplicate.sections\n\nStatus NA\t\t"))
		for _, d := range templateIndex.DuplicateSections {
			sb.WriteString(d)
		}
		sb.WriteString(fmt.Sprintf("\n\nEnd-Template"))
		err = ltfile.WriteFile(dupsOut, sb.String())
		if err != nil {
			return fmt.Errorf("error writing duplicate section atem file %s: %v", dupsOut, err), nil
		}
	}

	// make sure dirOut ends with path delimiter
	pathSeparator := "/"
	if !strings.HasSuffix(dirOut, pathSeparator) {
		dirOut = dirOut + pathSeparator
	}

	// create a map of all ares files found in the cloned repos
	var aresMap = make(map[string]string)
	var files []string
	files, err = ltfile.FileMatcher(tempDirIn, "ares", nil)
	if err != nil {
		return fmt.Errorf("error finding ares files in %s: %v ", tempDirIn, err), nil
	}
	for _, f := range files {
		_, filename := path.Split(f)
		aresMap[filename] = f
	}
	var ok bool
	var iTagsPath string // AGES-Initiatives/ages-alwb-system/net.ages.liturgical.workbench.system/GOARCH_Properties/iTags.ares
	var rolesPath string // AGES-Initiatives/ages-alwb-system/net.ages.liturgical.workbench.system/GOARCH_Properties/roles.ares
	if iTagsPath, ok = aresMap["iTags.ares"]; !ok {
		return fmt.Errorf("could not find iTags.ares file"), nil
	}
	if rolesPath, ok = aresMap["roles.ares"]; !ok {
		return fmt.Errorf("could not find roles.ares file"), nil
	}

	// load the tag ares files: iTags and roles.
	// these are ways in ATEM to pass through a css classname,
	// e.g. for iTags: Title sid ti.BothNow<Tdesig>sid ti.Theotokion</>End-Title
	// e.g. for roles: Para role bmc_ode3_katavasia sid mc.ode3katavasia End-Para
	if err != nil {
		return err, nil
	}
	err = ITags.Load(iTagsPath, "iTags")
	if err != nil {
		return err, nil
	}
	err = ITags.Load(rolesPath, "roles")
	if err != nil {
		return err, nil
	}

	// prepare missingCssClassMap
	// we will use it to consolidate parse errors
	// thrown when an atem refers to a css class
	// not found in assets/css/app.css
	missingCssClassMap := make(map[string]bool)

	files, err = ltfile.FileMatcher(templateDirIn, "atem", nil)
	if err != nil {
		return err, nil
	}
	errorChan := make(chan error, len(files))
	parseErrorChan := make(chan []ParseError, len(files))
	var missingTags []string

	// identify empty sections
	doxlog.Info("Checking atem files for empty sections...")
	var emptySectionErrors []ParseError
	emptySectionErrors, err = templateIndex.EmptySections(files)
	doxlog.Infof("%d empty sections found", len(emptySectionErrors))
	var wg sync.WaitGroup
	for _, f := range files {
		if filters.ContainedIn(f) {
			continue
		}
		doxlog.Infof("Converting %s", f)
		wg.Add(1)
		err = ProcessTemplate(templateDirIn,
			dirOut,
			f,
			keyMap,
			*ITags,
			templateIndex,
			templateIndex.TemplateDataMap,
			parseErrorChan,
			errorChan, &wg)
		if err != nil {
			return err, nil
		}
		select {
		case err = <-errorChan:
			if err != nil {
				return err, nil
			}
		case parseErrors := <-parseErrorChan:
			for _, parseError := range parseErrors {
				if strings.HasSuffix(parseError.Message, "app.css") {
					parts := strings.Split(parseError.Message, " ")
					if _, ok = missingCssClassMap[parts[0]]; !ok {
						allParseErrors = append(allParseErrors, parseError)
						missingTags = append(missingTags, parts[0])
					}
					missingCssClassMap[parts[0]] = true
					continue
				}
				doxlog.Error(fmt.Sprintf("%s", parseError.StringVerbose()))
				allParseErrors = append(allParseErrors, parseError)
			}
		}
	}
	if len(duplicateTemplates) > 0 {
		for _, parseError := range duplicateTemplates {
			doxlog.Error(fmt.Sprintf("%s", parseError.StringVerbose()))
			allParseErrors = append(allParseErrors, parseError)
		}
		doxlog.Infof("Duplicate template IDs were found. See the a2l.log")
	}
	if len(missingTags) > 0 {
		tag := "tags"
		if len(missingTags) == 1 {
			tag = "tag"
		}
		doxlog.Infof("\n%d %s need to be added to %s:", len(missingTags), tag, cssFile)
		sort.Strings(missingTags)
		for _, t := range missingTags {
			doxlog.Infof("\t%s", t)
		}
	}
	if len(emptySectionErrors) > 0 {
		// Fr. Seraphim wants these to be kept, so do not write to log
		//for _, parseError := range emptySectionErrors {
		//	allParseErrors = append(allParseErrors, parseError)
		//}
		//dlog.Infof("\n\n%d empty sections were found. See the a2l.log\nThese resulted in empty LML files being created.", len(emptySectionErrors))
	}
	elapsed := time.Since(start)
	doxlog.Infof("Finished converting atem templates to lml. Took %s", elapsed)
	if len(templateIndex.DuplicateSections) > 0 {
		doxlog.Errorf("There were %d duplicate section paths. For the list, see %s", len(templateIndex.DuplicateSections), dupsOut)
	} else {
		doxlog.Infof("No duplicate section paths were found.")
	}

	return nil, allParseErrors
}
func okToProcess(f string, doNotProcess []string) bool {
	for _, noGo := range doNotProcess {
		if strings.Contains(f, noGo) {
			return false
		}
	}
	return true
}
func ProcessTemplate(templateDirIn, templateDirOut, templatePathIn string,
	keyMap *map[string][]*kvs.KeyPath,
	ITags tags.Tag,
	templateIndex *TemplateIndex,
	templateDataMap map[string]*TemplateData,
	parseErrorChan chan<- []ParseError,
	errorChan chan<- error,
	wg *sync.WaitGroup) error {
	defer wg.Done()
	templateIdPath, err := NormalizeTemplatePath(templatePathIn, templateDirIn)
	if err != nil {
		return err
	}
	content, err := ltfile.GetFileContent(templatePathIn)
	if err != nil {
		return err
	}
	p, err := NewAtemParser(templateDirOut,
		templateIdPath,
		content,
		keyMap,
		ITags,
		templateIndex,
		templateDataMap)
	if err != nil {
		return err
	}
	parseErrors := p.WalkTemplate()
	if len(parseErrors) > 0 {
		parseErrorChan <- parseErrors
	}
	// write files out, the main template and blocks for each section
	for _, atem := range p.Listener.AtemMap.Map {
		dir, file := path.Split(atem.TemplateIdPath)
		if strings.HasPrefix(file, "bk.") ||
			strings.HasPrefix(file, "se.") ||
			strings.HasPrefix(file, "bl.") {
			// ignore
		} else {
			file = "bl." + file
		}
		fileOut := fmt.Sprintf("%s.lml", path.Join(atem.TemplatePathOut, dir, file))
		lines, _ := lmlFormatter.FormatLines(strings.Split(atem.Contents.String(), "\n"))
		err = ltfile.WriteLinesToFile(fileOut, lines)
		if err != nil {
			msg := fmt.Sprintf("error writing %s: %v", fileOut, err)
			doxlog.Error(msg)
			return fmt.Errorf(msg)
		}
	}
	errorChan <- nil
	return nil
}
func NormalizeTemplatePath(templatePathIn, templateDirIn string) (string, error) {
	parts := strings.Split(templatePathIn, templateDirIn)
	if len(parts) != 2 {
		return "", fmt.Errorf("could not split path: %s using %s", templatePathIn, templateDirIn)
	}
	result := parts[1]
	result = strings.ReplaceAll(result, "\\", "/")
	result = strings.ReplaceAll(result, "", "")

	if strings.HasPrefix(result, "/") {
		result = result[1:]
	}
	if strings.HasSuffix(result, ".atem") {
		result = result[0 : len(result)-5]
	}
	return result, nil
}

// Process converts all .atem files (templates) found in dirIn to .lml files, written to dirOut.
// .atem files are AGES templates.  .lml files are Doxa liturgical markup language templates.
// A non-nil error is returned if the path dirIn does not exist.
// Precondition to calling this function: the liturgical database must
// have already been loaded from the .ares files.
func Process(sysDir, dirIn, dirOut, cssFile, pdfCssFile, logDir string) error {
	ti, err := NewTemplateIndex(dirIn, "a-templates/", cssFile, pdfCssFile)
	if err != nil {
		return err
	}

	theFilters := filter.NewExcluder([]string{"generator.atem",
		"/special-requests/",
		"/tests/",
	})

	duplicateTemplates, err := ti.IndexTemplates(theFilters)
	if err != nil {
		return err
	}
	//	ti.PrintTemplateMap()
	doxlog.Infof("template count: %d", ti.TemplateCount())
	if len(duplicateTemplates) > 0 {
		doxlog.Infof("There are duplicate atem files. This should not be.")
		for _, d := range duplicateTemplates {
			doxlog.Infof("\t%v", d)
		}
		return fmt.Errorf("duplicate templates found")
	}
	// make sure dirOut ends with path delimiter
	pathSeparator := "/"
	if !strings.HasSuffix(dirOut, pathSeparator) {
		dirOut = dirOut + pathSeparator
	}
	// empty the lml templates directory and create a new one
	err = ltfile.DeleteDirRecursively(dirOut)
	if err != nil {
		return err
	}
	ltfile.CreateDirs(dirOut)

	// load the tag ares files
	ITags.Load(sysDir, "iTags")
	//BTags.LoadRemote(sysDir, "bTags")
	//RoleTags.LoadRemote(sysDir, "roles")
	//RuleTags.LoadRemote(sysDir, "rules")

	files, err := ltfile.FileMatcher(dirIn, "atem", nil)
	if err != nil {
		return err
	}
	library := "ages/"
	// Create an index of all templates and sections so ParseTemplate can get the path to any sections inserted.
	doxlog.Info("creating index of atem files and sections within files...")
	tTree, sTree, err := Index(dirIn, library)
	if err != nil {
		return fmt.Errorf("error indexing atem files: %d", err)
	}
	doxlog.Infof("Found %d atem files", len(files))
	doxlog.Infof("Found %d templates", tTree.Size())
	doxlog.Infof("Found %d sections\n", sTree.Size())
	doxlog.Info("Converting files...")
	// Process each AGES template and write out the corresponding lml files.
	// Each section will be written out as a separate file.
	for _, f := range files {
		parts := strings.Split(f, "a-templates/")
		if len(parts) == 2 {
			id := library + parts[1][0:len(parts[1])-5]
			if err := ParseTemplate(f, dirOut, id, tTree, sTree); err != nil {
				doxlog.Infof("%v", err)
			}
		} else {
			doxlog.Infof("folder a-templates not in path %s", f)
		}
	}
	// check the insert statements in the converted templates to ensure
	// the template to insert can be found using the argument of the insert.
	var valid bool
	var badInserts []string
	valid, badInserts, err = InsertsAreValid(dirOut)
	if err != nil {
		return err
	}
	if !valid {
		doxlog.Warnf("template not found in the following inserts in the lml templates:")
		for _, b := range badInserts {
			doxlog.Infof("\t%s", b)
		}
	}
	return nil
}
func InsertsAreValid(templatesDir string) (bool, []string, error) {
	var badInserts []string
	files, err := ltfile.FileMatcher(templatesDir, "lml", nil)
	if err != nil {
		return false, nil, fmt.Errorf("could not find lml files in %s", templatesDir)
	}
	var templateMap = make(map[string]bool)
	for _, f := range files {
		key := f[len(templatesDir)+1 : len(f)-4]
		templateMap[key] = true
	}
	for _, f := range files {
		var lines []string
		lines, err = ltfile.GetFileLines(f)
		if err != nil {
			return false, nil, fmt.Errorf("error reading %s: %v", f, err)
		}
		for i, line := range lines {
			line = strings.TrimSpace(line)
			if strings.HasPrefix(line, "insert") {
				newLine := strings.TrimSpace(line[6:])
				if len(newLine) == 0 {
					badInserts = append(badInserts, fmt.Sprintf("no template specified in %s line %d: %s", f, i, line))
					continue
				}
				templateId := newLine[1 : len(newLine)-1]
				v := templateMap[templateId]
				if !v {
					badInserts = append(badInserts, fmt.Sprintf("%s line %d: %s", f, i, line))
				} else {
					fmt.Sprintf("TODO delete me after handling")
				}
			}
		}
	}
	var validated = len(badInserts) == 0
	return validated, badInserts, nil
}
