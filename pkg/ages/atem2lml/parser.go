package atem2lml

import (
	"github.com/antlr/antlr4/runtime/Go/antlr/v4"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/ages/tags"
	atem "gitlab.com/ocmc/liturgiko/lml/alwb/golang/parser"
)

// AtemParser provides access to a lexer and parser for the input stream.
// Use NewAtemParser to get an instance.
type AtemParser struct {
	ErrorListener   *ErrorListener
	ITags           tags.Tag
	Lexer           *atem.AtemLexer
	Listener        *AtemListener
	Parser          *atem.AtemParser
	TemplateContent string
	TemplateIdPath  string
	TemplatePathOut string
}

func NewAtemParser(templateDirOut,
	templateIdPath,
	templateContent string,
	keyMap *map[string][]*kvs.KeyPath,
	ITags tags.Tag,
	templateIndex *TemplateIndex,
	templateDataMap map[string]*TemplateData) (*AtemParser, error) {
	var err error
	l := new(AtemParser)
	l.TemplatePathOut = templateDirOut
	l.TemplateIdPath = templateIdPath
	l.TemplateContent = templateContent
	l.ITags = ITags
	l.Lexer = atem.NewAtemLexer(antlr.NewInputStream(templateContent))
	stream := antlr.NewCommonTokenStream(l.Lexer, antlr.TokenDefaultChannel)
	l.Parser = atem.NewAtemParser(stream)
	l.Listener, err = NewAtemListener(templateDirOut, templateIdPath, keyMap, ITags, templateIndex, templateDataMap)
	if err != nil {
		return nil, err
	}
	l.ErrorListener = NewErrorListener(templateIdPath)
	l.Parser.RemoveErrorListeners()
	l.Parser.AddErrorListener(l.ErrorListener)
	return l, nil
}

// WalkTemplate performs a walk on the template parse tree starting at the root
// and going down recursively with depth-first search.
// On each node, EnterRule is called before recursively walking down into child nodes,
// then ExitRule is called after the recursive call to wind up.
// Returns the Errors found in the input stream by the parser.
// Note that antlr only returns an error of a specific type once.
// So, there may be more errors than are reported by this function.
func (p *AtemParser) WalkTemplate() []ParseError {
	return p.walk(p.Parser.AtemModel())
}

// Performs a walk on the given parse tree starting at the rootd
// and going down recursively with depth-first search.
// At each node, EnterRule is called before recursively
// walking down into child nodes,
// then ExitRule is called after the recursive call to wind up.
// Returns the Errors found in the input stream by the parser.
// Note that antlr only returns an error of a specific type once.
// So, there may be more errors than are reported by this function.
func (p *AtemParser) walk(t antlr.Tree) []ParseError {
	antlr.ParseTreeWalkerDefault.Walk(p.Listener, t)
	return p.ErrorListener.Errors
}

// Tokens provides back information about each identified token. It will not indicate any errors.
func (p *AtemParser) Tokens() []antlr.Token {
	var tokens []antlr.Token
	for {
		t := p.Lexer.NextToken()
		if t.GetTokenType() == antlr.TokenEOF {
			break
		}
		tokens = append(tokens, t)
	}
	return tokens
}
