package atem2lml

import (
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"strings"
	"testing"
)

const cssFile = "./test/app.css"
const pdfCssFile = "./test/pdf.css"

// InverseKeyMap is used to fully qualify sid and rid ids
// since in ALWB they are not, but must be in Doxa.
var InverseKeyMap *map[string][]*kvs.KeyPath

// CreateInverseMap provides a map for test purposes.
// When the a2l command is run to actually convert ALWB atem templates,
// a inverseMap is created by reading the database.  For test purposes,
// we manually populate such a map.
func CreateInverseMap() *map[string][]*kvs.KeyPath {
	theMap := make(map[string][]*kvs.KeyPath)
	k, v := GetKeyPath("actors:Deacon")
	theMap[k] = v
	k, v = GetKeyPath("actors:Priest")
	theMap[k] = v
	k, v = GetKeyPath("actors:Bishop")
	theMap[k] = v
	k, v = GetKeyPath("actors:Choir")
	theMap[k] = v
	k, v = GetKeyPath("actors:People")
	theMap[k] = v
	k, v = GetKeyPath("misc:parenthesis.open")
	theMap[k] = v
	k, v = GetKeyPath("misc:parenthesis.close")
	theMap[k] = v
	k, v = GetKeyPath("prayers:res04")
	theMap[k] = v
	k, v = GetKeyPath("hi.s16:hiCE.Key_0102.text")
	theMap[k] = v
	k, v = GetKeyPath("ho.s03:hoMA.DoxologyVerse02.text")
	theMap[k] = v
	k, v = GetKeyPath("hi.s16:hiCE.Key_0101.rubric")
	theMap[k] = v
	k, v = GetKeyPath("hi.s16:hiCE.Key_0100.title")
	theMap[k] = v
	k, v = GetKeyPath("template.titles:ma.pdf.header")
	theMap[k] = v
	k, v = GetKeyPath("prayers:verse_God.text")
	theMap[k] = v
	k, v = GetKeyPath("me.m10.d26:meMA.Ode3C34.ode")
	_, v2 := GetKeyPath("me.m11.d30:meMA.Ode3C34.ode")
	v = append(v, v2[0])
	theMap[k] = v
	return &theMap
}

func GetKeyPath(path string) (string, []*kvs.KeyPath) {
	var value []*kvs.KeyPath
	kp := kvs.NewKeyPath()
	kp.ParsePath(fmt.Sprintf("ltx/gr_gr_cog/%s", path))
	value = append(value, kp)
	return kp.Key(), value
}

var TemplateMap *TemplateIndex

func CreateTemplateMap() *TemplateIndex {
	t, err := NewTemplateIndex("x/y/a-templates", "a-templates/", cssFile, pdfCssFile)
	if err != nil {
		fmt.Printf("\nerror creating template index: %v\n", err)
	}
	t.TemplateMap["AP"] = []string{"Blocks/AP"}
	t.TemplateMap["pdf.cover.credits"] = []string{"Covers_Pdf/pdf.cover.credits"}

	t.SectionMap["Blocks.AP.both__.endofmatins_theotokion__.mode1_.wednesday"] = "Blocks/AP/both__/endofmatins_theotokion__/mode1_/wednesday"
	t.SectionMap["Blocks.AP.both__.theotokion__.mode1_.wednesday"] = "Blocks/AP/both__/theotokion__/mode1_/wednesday"
	t.SectionMap["Blocks.AP.gloryboth__.endofmatins_theotokion__.mode1_.wednesday"] = "Blocks/AP/gloryboth__/endofmatins_theotokion__/mode1_/wednesday"
	t.SectionMap["Blocks.AP.gloryboth__.theotokion__.mode1_.wednesday"] = "Blocks/AP/gloryboth__/theotokion__/mode1_/wednesday"
	t.SectionMap["Blocks.AP.NM.both__.endofmatins_theotokion__.mode1_.wednesday"] = "Blocks/AP/NM/both__/endofmatins_theotokion__/mode1_/wednesday"
	t.SectionMap["Blocks.AP.NM.both__.theotokion__.mode1_.wednesday"] = "Blocks/AP/NM/both__/theotokion__/mode1_/wednesday"
	t.SectionMap["Blocks.AP.NM.gloryboth__.endofmatins_theotokion__.mode1_/wednesday"] = "Blocks/AP/NM/gloryboth__/endofmatins_theotokion__/mode1_/wednesday"
	t.SectionMap["Blocks.AP.NM.gloryboth__.theotokion__.mode1_.wednesday"] = "Blocks/AP/NM/gloryboth__/theotokion__/mode1_/wednesday"
	t.SectionMap["pdf.cover.links.lookup.vespers"] = "Covers_PDF/pdf.cover.links.lookup/vespers"

	return t
}
func init() {
	InverseKeyMap = CreateInverseMap()
	TemplateMap = CreateTemplateMap()
}
func TestAtemParser001(t *testing.T) {
	input := `Template AP
//	ModifiedFiles NA
	import actors_gr_GR_cog.*
	import da.*
	import hi.s16_gr_GR_cog.*
    import ho.s03_gr_GR_cog.*
	import me.m11.d30_gr_GR_cog.*
	import misc_gr_GR_cog.*
	import prayers_gr_GR_cog.*

	Set_mcDay day 65 End_mcDay
	Actor sid Deacon End-Actor
	Dialog<i>sid parenthesis.open sid res04 sid parenthesis.close</>End-Dialog
	Hymn sid meMA.Ode3C34.ode @ver End-Hymn
	Hymn sid oc.m2.d2_gr_GR_cog.ocMA.ApolTheotokion.text @ver End-Hymn
	Para role prayer sid hiCE.Key_0102.text @ver End-Para
	Reading sid hoMA.DoxologyVerse02.text End-Reading
	Rubric sid hiCE.Key_0101.rubric End-Rubric
	Title <Tdesig>sid hiCE.Key_0100.title</>End-Title
	Verse sid verse_God.text End-Verse
	Switch-Version L2 End-Switch-Version
    Title <Tdesig>sid hiCE.Key_0100.title</>End-Title
	Switch-Version Both End-Switch-Version
End-Template
`
	expectStarts := 119
	expect := `id = "Blocks/bl.AP"
type = "block"
movableCycleDay = 65
p.actor sid "actors:Deacon"
p.dialog .i ( sid "misc:parenthesis.open" sid "prayers/res04" sid "misc/parenthesis.close" ) 
p.hymn sid "me.m11.d30:meMA.Ode3C34.ode" @ver
p.hymn sid "oc.m2.d2:ocMA.ApolTheotokion.text" @ver
p.prayer sid "hi.s16:hiCE.Key_0102.text" @ver
p.reading sid "ho.s03
;hoMA.DoxologyVerse02.text"
p.rubric sid "hi.s16:hiCE.Key_0101.rubric"
p.title span.desig ( sid "hi.s16:hiCE.Key_0100.title" )
p.verse sid "prayers:verse_God.text"
version = 2
p.title span.desig ( sid "hi.s16:hiCE.Key_0100.title" )
restoreVersion
`
	p, err := NewAtemParser("test/out/",
		"Blocks/bl.AP",
		input,
		InverseKeyMap,
		*ITags,
		TemplateMap,
		nil)
	if err != nil {
		t.Errorf("%s", err)
	}
	parseErrors := p.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("%v", parseErrors[0])
	}
	got := p.Listener.MainContent()
	err = GotExpected(expectStarts, expect, got)
	if err != nil {
		t.Errorf("%v", err)
	}
}
func TestAtemParserSections(t *testing.T) {
	input := `Template AP
	ModifiedFiles NA
	import actors_gr_GR_cog.*
	import da.*
	import hi.s16_gr_GR_cog.*
    import ho.s03_gr_GR_cog.*
	import me.m11.d30_gr_GR_cog.*
	import misc_gr_GR_cog.*
	import prayers_gr_GR_cog.*

	Actor sid Deacon End-Actor
	Dialog<i>sid parenthesis.open sid res04 sid parenthesis.close</>End-Dialog
	Section both__ 
		Section endofmatins_theotokion__
			Section modeofday
				Insert_section mode1_.wednesday End-Insert
			End-Section
			Section mode1_
				Section wednesday
					Hymn sid meMA.Ode3C34.ode @ver End-Hymn
					Hymn sid oc.m2.d2_gr_GR_cog.ocMA.ApolTheotokion.text @ver End-Hymn
				End-Section
			End-Section
		End-Section
	End-Section
	Para role prayer sid hiCE.Key_0102.text @ver End-Para
	Reading sid hoMA.DoxologyVerse02.text End-Reading
	Rubric sid hiCE.Key_0101.rubric End-Rubric
	Section gloryboth__ 
		Section endofmatins_theotokion__
			Section modeofday
				Insert_section mode1_.wednesday End-Insert
			End-Section
			Section mode1_
				Section wednesday
					Hymn sid meMA.Ode3C34.ode @ver End-Hymn
					Hymn sid oc.m2.d2_gr_GR_cog.ocMA.ApolTheotokion.text @ver End-Hymn
				End-Section
			End-Section
		End-Section
	End-Section
End-Template
`
	expected := make(map[string]string)
	expected["Blocks/AP"] = `id = "Blocks/AP"
type = "block"
status = "na"
p.actor sid "actors/Deacon"
p.dialog .i ( sid "misc/parenthesis.open" sid "prayers/res04" sid "misc/parenthesis.close" ) 
insert "Blocks/AP/both__"
p.prayer sid "hi.s16/hiCE.Key_0102.text" @ver
p.reading sid "ho.s03/hoMA.DoxologyVerse02.text"
p.rubric sid "hi.s16/hiCE.Key_0101.rubric"
insert "Blocks/AP/gloryboth__"
`
	expected["Blocks/AP/both__"] = `id = "Blocks/AP/both__"
type = "block"
status = "na"
insert "Blocks/AP/both__/endofmatins_theotokion__"
`
	expected["Blocks/AP/both__/endofmatins_theotokion__"] = `id = "Blocks/AP/both__/endofmatins_theotokion__"
type = "block"
status = "na"
insert "Blocks/AP/both__/endofmatins_theotokion__/modeofday"
insert "Blocks/AP/both__/endofmatins_theotokion__/mode1_"
`
	expected["Blocks/AP/both__/endofmatins_theotokion__/modeofday"] = `id = "Blocks/AP/both__/endofmatins_theotokion__/modeofday"
type = "block"
status = "na"
insert "Blocks/AP/both__/endofmatins_theotokion__/mode1_/wednesday"
`
	expected["Blocks/AP/both__/endofmatins_theotokion__/mode1_"] = `id = "Blocks/AP/both__/endofmatins_theotokion__/mode1_"
type = "block"
status = "na"
insert "Blocks/AP/both__/endofmatins_theotokion__/mode1_/wednesday"
`
	expected["Blocks/AP/both__/endofmatins_theotokion__/mode1_/wednesday"] = `id = "Blocks/AP/both__/endofmatins_theotokion__/mode1_/wednesday"
type = "block"
status = "na"
p.hymn sid "me.m11.d30/meMA.Ode3C34.ode" @ver
p.hymn sid "oc.m2.d2/ocMA.ApolTheotokion.text" @ver
`

	expected["Blocks/AP/gloryboth__"] = `id = "Blocks/AP/gloryboth__"
type = "block"
status = "na"
insert "Blocks/AP/gloryboth__/endofmatins_theotokion__"
`
	expected["Blocks/AP/gloryboth__/endofmatins_theotokion__"] = `id = "Blocks/AP/gloryboth__/endofmatins_theotokion__"
type = "block"
status = "na"
insert "Blocks/AP/gloryboth__/endofmatins_theotokion__/modeofday"
insert "Blocks/AP/gloryboth__/endofmatins_theotokion__/mode1_"
`
	expected["Blocks/AP/gloryboth__/endofmatins_theotokion__/modeofday"] = `id = "Blocks/AP/gloryboth__/endofmatins_theotokion__/modeofday"
type = "block"
status = "na"
insert "Blocks/AP/gloryboth__/endofmatins_theotokion__/mode1_/wednesday"
`
	expected["Blocks/AP/gloryboth__/endofmatins_theotokion__/mode1_"] = `id = "Blocks/AP/gloryboth__/endofmatins_theotokion__/mode1_"
type = "block"
status = "na"
insert "Blocks/AP/gloryboth__/endofmatins_theotokion__/mode1_/wednesday"
`
	expected["Blocks/AP/gloryboth__/endofmatins_theotokion__/mode1_/wednesday"] = `id = "Blocks/AP/gloryboth__/endofmatins_theotokion__/mode1_/wednesday"
type = "block"
status = "na"
p.hymn sid "me.m11.d30/meMA.Ode3C34.ode" @ver
p.hymn sid "oc.m2.d2/ocMA.ApolTheotokion.text" @ver
`

	p, err := NewAtemParser("test/out/", "Blocks/AP", input, InverseKeyMap, *ITags, TemplateMap, nil)
	if err != nil {
		t.Errorf("%s", err)
	}
	parseErrors := p.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("%v", parseErrors[0])
	}
	for k, expect := range expected {
		if atem, ok := p.Listener.AtemMap.Map[k]; ok {
			got := atem.Contents.String()
			err = GotExpected(0, expect, got)
			if err != nil {
				t.Errorf("key %s: %v", k, err)
			}
		} else {
			t.Errorf(fmt.Sprintf("expected AtemMap has key %s, but not found", k))
		}
	}

}
func TestAtemVerInsert(t *testing.T) {
	input := `Template AP
Status Final
import actors_gr_GR_cog.*
Actor sid actors_gr_GR_cog.Deacon @ver media-off End-Actor
End-Template
`
	expectStarts := 341
	expect := `id = "Blocks/AP"
type = "block"
status = "final"
p.actor sid "actors:Deacon" @ver
`
	p, err := NewAtemParser("test/out/", "Blocks/AP", input, InverseKeyMap, *ITags, TemplateMap, nil)
	if err != nil {
		t.Errorf("%s", err)
	}
	parseErrors := p.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("%v", parseErrors[0])
	}
	got := p.Listener.MainContent()
	err = GotExpected(expectStarts, expect, got)
	if err != nil {
		t.Errorf("%v", err)
	}
}
func TestAtemParserHead(t *testing.T) {
	input := `Template AP
	import actors_gr_GR_cog.*
	import da.*
	import hi.s16_gr_GR_cog.*
    import ho.s03_gr_GR_cog.*
	import me.m11.d30_gr_GR_cog.*
	import misc_gr_GR_cog.*
	import prayers_gr_GR_cog.*
	import template.titles_gr_GR_cog.*

	Head 
		Set_Date month 11 day 7 year 2014 End_Date
		Template_Title @commemoration End_Title
		Template_Title @text "Matins Ordinary - Ascension" End_Title
		Template_Title @lookup sid ma.pdf.header lang L1 End_Title
		Page_Header_Even
			center @lookup sid ma.pdf.header lang L1 
		End_Page_Header_Even
		Page_Header_Odd
			center @lookup sid ma.pdf.header lang L2
		End_Page_Header_Odd
		Page_Footer_Even
			left @text "Τέλος τοῦ Ὄρθρου"
			center @pageNbr
			right @date lang L2
		End_Page_Footer_Even
		Page_Footer_Odd
			left @text "End of Matins"
			center @pageNbr
			right @date lang L2
		End_Page_Footer_Odd

		Set_Page_Number 1 End_Set_Page_Number
	End_Head
	Actor sid Deacon End-Actor
	Dialog<i>sid parenthesis.open sid res04 sid parenthesis.close</>End-Dialog
	Hymn sid meMA.Ode3C34.ode @ver End-Hymn
	Hymn sid oc.m2.d2_gr_GR_cog.ocMA.ApolTheotokion.text @ver End-Hymn
	Para role prayer sid hiCE.Key_0102.text @ver End-Para
	Reading sid hoMA.DoxologyVerse02.text End-Reading
	Rubric sid hiCE.Key_0101.rubric End-Rubric
	Title<Tdesig>sid hiCE.Key_0100.title</>End-Title
	Break line End_Break
	Verse sid verse_God.text End-Verse
	Set_Date month 11 day 13 End_Date
	Set_Date month 0 day 0 End_Date
	Break page End_Break
End-Template
`
	expectStarts := 341
	expect := `id = "Blocks/AP"
type = "block"
status = "na"
month = 11
day = 7
year = 2014
title = nid "@commemoration"
title = nid "Matins Ordinary - Ascension"
title = @lookup sid "template.titles/ma.pdf.header" ver 1
pageHeaderEven = center @lookup sid "template.titles/ma.pdf.header" ver 1
pageHeaderOdd = center @lookup sid "template.titles/ma.pdf.header" ver 2
pageFooterEven = left nid "Τέλος τοῦ Ὄρθρου" center @pageNbr right @date ver 2
pageFooterOdd = left nid "End of Matins" center @pageNbr right @date ver 2
pageNumber = 1
p.actor sid "actors/Deacon"
p.dialog .i ( sid "misc/parenthesis.open" sid "prayers/res04" sid "misc/parenthesis.close" ) 
p.hymn sid "me.m11.d30/meMA.Ode3C34.ode" @ver
p.hymn sid "oc.m2.d2/ocMA.ApolTheotokion.text" @ver
p.prayer sid "hi.s16/hiCE.Key_0102.text" @ver
p.reading sid "ho.s03/hoMA.DoxologyVerse02.text"
p.rubric sid "hi.s16/hiCE.Key_0101.rubric"
p.title .Tdesig ( sid "hi.s16/hiCE.Key_0100.title" )
br.single
p.verse sid "prayers/verse_God.text"
month = 11
day = 13
restoreDate
pageBreak
`
	p, err := NewAtemParser("test/out/", "Blocks/AP", input, InverseKeyMap, *ITags, TemplateMap, nil)
	if err != nil {
		t.Errorf("%s", err)
	}
	parseErrors := p.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("%v", parseErrors[0])
	}
	got := p.Listener.MainContent()
	err = GotExpected(expectStarts, expect, got)
	if err != nil {
		t.Errorf("%v", err)
	}
}
func TestAtemParserPreface(t *testing.T) {
	input := `Template AP
	ModifiedFiles NA
	import actors_gr_GR_cog.*
	import da.*
	import hi.s16_gr_GR_cog.*
    import ho.s03_gr_GR_cog.*
	import me.m11.d30_gr_GR_cog.*
	import misc_gr_GR_cog.*
	import prayers_gr_GR_cog.*
	import template.titles_gr_GR_cog.*
	import pdf.covers_en_US_goarch.*

	Head 
		Set_Date month 11 day 7 year 2014 End_Date
		Template_Title @commemoration End_Title
		Template_Title @text "Matins Ordinary - Ascension" End_Title
		Template_Title @lookup sid ma.pdf.header lang L1 End_Title
		Page_Header_Even
			center @lookup sid ma.pdf.header lang L1 
		End_Page_Header_Even
		Page_Header_Odd
			center @lookup sid ma.pdf.header lang L2
		End_Page_Header_Odd
		Page_Footer_Even
			left @text "Τέλος τοῦ Ὄρθρου"
			center @pageNbr
			right @date lang L2
		End_Page_Footer_Even
		Page_Footer_Odd
			left @text "End of Matins"
			center @pageNbr
			right @date lang L2
		End_Page_Footer_Odd

		Set_Page_Number 1 End_Set_Page_Number
	End_Head
	Preface Covers
		Insert_section pdf.cover.links.lookup.vespers End-Insert
		Insert_template pdf.cover.credits End-Insert
	End-Preface

	Actor sid Deacon End-Actor
	Dialog<i>sid parenthesis.open sid res04 sid parenthesis.close</>End-Dialog
	Hymn sid meMA.Ode3C34.ode @ver End-Hymn
	Hymn sid oc.m2.d2_gr_GR_cog.ocMA.ApolTheotokion.text @ver End-Hymn
	Para role prayer sid hiCE.Key_0102.text @ver End-Para
	Reading sid hoMA.DoxologyVerse02.text End-Reading
	Rubric sid hiCE.Key_0101.rubric End-Rubric
	Title<Tdesig>sid hiCE.Key_0100.title</>End-Title
	Break line End_Break
	Verse sid verse_God.text End-Verse
	Set_Date month 11 day 13 End_Date
	Set_Date month 0 day 0 End_Date
	Break page End_Break
End-Template
`
	expectStarts := 442
	expect := `id = "Blocks/AP"
type = "block"
status = "na"
month = 11
day = 7
year = 2014
title = nid "@commemoration"
title = nid "Matins Ordinary - Ascension"
title = @lookup sid "template.titles/ma.pdf.header" ver 1
pageHeaderEven = center @lookup sid "template.titles/ma.pdf.header" ver 1
pageHeaderOdd = center @lookup sid "template.titles/ma.pdf.header" ver 2
pageFooterEven = left nid "Τέλος τοῦ Ὄρθρου" center @pageNbr right @date ver 2
pageFooterOdd = left nid "End of Matins" center @pageNbr right @date ver 2
pageNumber = 1
insert "Covers_PDF/pdf.cover.links.lookup/vespers"
insert "Covers_Pdf/pdf.cover.credits"
p.actor sid "actors/Deacon"
p.dialog .i ( sid "misc/parenthesis.open" sid "prayers/res04" sid "misc/parenthesis.close" ) 
p.hymn sid "me.m11.d30/meMA.Ode3C34.ode" @ver
p.hymn sid "oc.m2.d2/ocMA.ApolTheotokion.text" @ver
p.prayer sid "hi.s16/hiCE.Key_0102.text" @ver
p.reading sid "ho.s03/hoMA.DoxologyVerse02.text"
p.rubric sid "hi.s16/hiCE.Key_0101.rubric"
p.title .Tdesig ( sid "hi.s16/hiCE.Key_0100.title" )
br.single
p.verse sid "prayers/verse_God.text"
month = 11
day = 13
restoreDate
pageBreak
`
	p, err := NewAtemParser("test/out/", "Blocks/AP", input, InverseKeyMap, *ITags, TemplateMap, nil)
	if err != nil {
		t.Errorf("%s", err)
	}
	parseErrors := p.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("%v", parseErrors[0])
	}
	got := p.Listener.MainContent()
	err = GotExpected(expectStarts, expect, got)
	if err != nil {
		t.Errorf("%v", err)
	}
}
func TestAtemParserWhenDate(t *testing.T) {
	input := `Template AP
	ModifiedFiles NA
	import actors_gr_GR_cog.*
	import da.*
	import hi.s16_gr_GR_cog.*
    import ho.s03_gr_GR_cog.*
	import me.m11.d30_gr_GR_cog.*
	import misc_gr_GR_cog.*
	import prayers_gr_GR_cog.*

	Actor sid Deacon End-Actor
	Dialog<i>sid parenthesis.open sid res04 sid parenthesis.close</>End-Dialog

	when-date-is 
		Jan 15 thru 31 use: 
			Hymn sid meMA.Ode3C34.ode @ver End-Hymn
		Feb 1, 3, 5 use:
			Hymn sid oc.m2.d2_gr_GR_cog.ocMA.ApolTheotokion.text @ver End-Hymn
		otherwise use:
			Reading sid hoMA.DoxologyVerse02.text End-Reading
	end-when
	Para role prayer sid hiCE.Key_0102.text @ver End-Para
End-Template
`
	expectStarts := 513
	expect := `id = "Blocks/AP"
type = "block"
status = "na"
p.actor sid "actors/Deacon"
p.dialog .i ( sid "misc/parenthesis.open" sid "prayers/res04" sid "misc/parenthesis.close" ) 
switch date {
  case jan 15 thru 31: {
    p.hymn sid "me.m11.d30/meMA.Ode3C34.ode" @ver
  }
  case feb 1, 3, 5: {
    p.hymn sid "oc.m2.d2/ocMA.ApolTheotokion.text" @ver
  }
  default: {
    p.reading sid "ho.s03/hoMA.DoxologyVerse02.text"
  }
}
p.prayer sid "hi.s16/hiCE.Key_0102.text" @ver
`
	p, err := NewAtemParser("test/out/", "Blocks/AP", input, InverseKeyMap, *ITags, TemplateMap, nil)
	if err != nil {
		t.Errorf("%s", err)
	}
	parseErrors := p.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("%v", parseErrors[0])
	}
	got := p.Listener.MainContent()
	err = GotExpected(expectStarts, expect, got)
	if err != nil {
		t.Errorf("%v", err)
	}
}
func TestAtemParserWhenNameOfDay(t *testing.T) {
	input := `Template AP
	ModifiedFiles NA
	import actors_gr_GR_cog.*
	import da.*
	import hi.s16_gr_GR_cog.*
    import ho.s03_gr_GR_cog.*
	import me.m11.d30_gr_GR_cog.*
	import misc_gr_GR_cog.*
	import prayers_gr_GR_cog.*

	Actor sid Deacon End-Actor
	Dialog<i>sid parenthesis.open sid res04 sid parenthesis.close</>End-Dialog

	when-name-of-day-is 
		Monday thru Wednesday use: 
			Hymn sid meMA.Ode3C34.ode @ver End-Hymn
		Thursday, Friday use:
			Hymn sid oc.m2.d2_gr_GR_cog.ocMA.ApolTheotokion.text @ver End-Hymn
		otherwise use:
			Reading sid hoMA.DoxologyVerse02.text End-Reading
	end-when
	Para role prayer sid hiCE.Key_0102.text @ver End-Para
End-Template
`
	expectStarts := 571
	expect := `id = "Blocks/AP"
type = "block"
status = "na"
p.actor sid "actors/Deacon"
p.dialog .i ( sid "misc/parenthesis.open" sid "prayers/res04" sid "misc/parenthesis.close" ) 
switch dayOfWeek {
  case mon thru wed: {
    p.hymn sid "me.m11.d30/meMA.Ode3C34.ode" @ver
  }
  case thu, fri: {
    p.hymn sid "oc.m2.d2/ocMA.ApolTheotokion.text" @ver
  }
  default: {
    p.reading sid "ho.s03/hoMA.DoxologyVerse02.text"
  }
}
p.prayer sid "hi.s16/hiCE.Key_0102.text" @ver
`
	p, err := NewAtemParser("test/out/", "Blocks/AP", input, InverseKeyMap, *ITags, TemplateMap, nil)
	if err != nil {
		t.Errorf("%s", err)
	}
	parseErrors := p.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("%v", parseErrors[0])
	}
	got := p.Listener.MainContent()
	err = GotExpected(expectStarts, expect, got)
	if err != nil {
		t.Errorf("%v", err)
	}
}
func TestAtemParserWhenLukanCycle(t *testing.T) {
	input := `Template AP
	ModifiedFiles NA
	import actors_gr_GR_cog.*
	import da.*
	import hi.s16_gr_GR_cog.*
    import ho.s03_gr_GR_cog.*
	import me.m11.d30_gr_GR_cog.*
	import misc_gr_GR_cog.*
	import prayers_gr_GR_cog.*

	Actor sid Deacon End-Actor
	Dialog<i>sid parenthesis.open sid res04 sid parenthesis.close</>End-Dialog

	when-Lukan-Cycle-Day-is 
		1 thru 30 use: 
			Hymn sid meMA.Ode3C34.ode @ver End-Hymn
		40, 50 use:
			Hymn sid oc.m2.d2_gr_GR_cog.ocMA.ApolTheotokion.text @ver End-Hymn
		otherwise use:
			Reading sid hoMA.DoxologyVerse02.text End-Reading
	end-when
	Para role prayer sid hiCE.Key_0102.text @ver End-Para
End-Template
`
	expectStarts := 629
	expect := `id = "Blocks/AP"
type = "block"
status = "na"
p.actor sid "actors/Deacon"
p.dialog .i ( sid "misc/parenthesis.open" sid "prayers/res04" sid "misc/parenthesis.close" ) 
switch lukanCycleDay {
  case 1 thru 30: {
    p.hymn sid "me.m11.d30/meMA.Ode3C34.ode" @ver
  }
  case 40, 50: {
    p.hymn sid "oc.m2.d2/ocMA.ApolTheotokion.text" @ver
  }
  default: {
    p.reading sid "ho.s03/hoMA.DoxologyVerse02.text"
  }
}
p.prayer sid "hi.s16/hiCE.Key_0102.text" @ver
`
	p, err := NewAtemParser("test/out/", "Blocks/AP", input, InverseKeyMap, *ITags, TemplateMap, nil)
	if err != nil {
		t.Errorf("%s", err)
	}
	parseErrors := p.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("%v", parseErrors[0])
	}
	got := p.Listener.MainContent()
	err = GotExpected(expectStarts, expect, got)
	if err != nil {
		t.Errorf("%v", err)
	}
}
func TestAtemParserWhenModeOfWeek(t *testing.T) {
	input := `Template AP
	ModifiedFiles NA
	import actors_gr_GR_cog.*
	import da.*
	import hi.s16_gr_GR_cog.*
    import ho.s03_gr_GR_cog.*
	import me.m11.d30_gr_GR_cog.*
	import misc_gr_GR_cog.*
	import prayers_gr_GR_cog.*

	Actor sid Deacon End-Actor
	Dialog<i>sid parenthesis.open sid res04 sid parenthesis.close</>End-Dialog

	when-mode-of-week-is 
		M1, M2 use: 
			Hymn sid meMA.Ode3C34.ode @ver End-Hymn
		M3, M4 use:
			Hymn sid oc.m2.d2_gr_GR_cog.ocMA.ApolTheotokion.text @ver End-Hymn
		otherwise use:
			Reading sid hoMA.DoxologyVerse02.text End-Reading
	end-when
	Para role prayer sid hiCE.Key_0102.text @ver End-Para
End-Template
`
	expectStarts := 687
	expect := `id = "Blocks/AP"
type = "block"
status = "na"
p.actor sid "actors/Deacon"
p.dialog .i ( sid "misc/parenthesis.open" sid "prayers/res04" sid "misc/parenthesis.close" ) 
switch modeOfWeek {
  case 1, 2: {
    p.hymn sid "me.m11.d30/meMA.Ode3C34.ode" @ver
  }
  case 3, 4: {
    p.hymn sid "oc.m2.d2/ocMA.ApolTheotokion.text" @ver
  }
  default: {
    p.reading sid "ho.s03/hoMA.DoxologyVerse02.text"
  }
}
p.prayer sid "hi.s16/hiCE.Key_0102.text" @ver
`
	p, err := NewAtemParser("test/out/", "Blocks/AP", input, InverseKeyMap, *ITags, TemplateMap, nil)
	if err != nil {
		t.Errorf("%s", err)
	}
	parseErrors := p.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("%v", parseErrors[0])
	}
	got := p.Listener.MainContent()
	err = GotExpected(expectStarts, expect, got)
	if err != nil {
		t.Errorf("%v", err)
	}
}
func TestAtemParserWhenMovableCycle(t *testing.T) {
	input := `Template AP
	ModifiedFiles NA
	import actors_gr_GR_cog.*
	import da.*
	import hi.s16_gr_GR_cog.*
    import ho.s03_gr_GR_cog.*
	import me.m11.d30_gr_GR_cog.*
	import misc_gr_GR_cog.*
	import prayers_gr_GR_cog.*

	Actor sid Deacon End-Actor
	Dialog<i>sid parenthesis.open sid res04 sid parenthesis.close</>End-Dialog

	when-movable-cycle-day-is 
		1 thru 30 use: 
			Hymn sid meMA.Ode3C34.ode @ver End-Hymn
		40, 50 use:
			Hymn sid oc.m2.d2_gr_GR_cog.ocMA.ApolTheotokion.text @ver End-Hymn
		otherwise use:
			Reading sid hoMA.DoxologyVerse02.text End-Reading
	end-when
	Para role prayer sid hiCE.Key_0102.text @ver End-Para
End-Template
`
	expectStarts := 745
	expect := `id = "Blocks/AP"
type = "block"
status = "na"
p.actor sid "actors/Deacon"
p.dialog .i ( sid "misc/parenthesis.open" sid "prayers/res04" sid "misc/parenthesis.close" ) 
switch movableCycleDay {
  case 1 thru 30: {
    p.hymn sid "me.m11.d30/meMA.Ode3C34.ode" @ver
  }
  case 40, 50: {
    p.hymn sid "oc.m2.d2/ocMA.ApolTheotokion.text" @ver
  }
  default: {
    p.reading sid "ho.s03/hoMA.DoxologyVerse02.text"
  }
}
p.prayer sid "hi.s16/hiCE.Key_0102.text" @ver
`
	p, err := NewAtemParser("test/out/", "Blocks/AP", input, InverseKeyMap, *ITags, TemplateMap, nil)
	if err != nil {
		t.Errorf("%s", err)
	}
	parseErrors := p.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("%v", parseErrors[0])
	}
	got := p.Listener.MainContent()
	err = GotExpected(expectStarts, expect, got)
	if err != nil {
		t.Errorf("%v", err)
	}
}
func TestAtemParserWhenSundaysBeforeTriodion(t *testing.T) {
	input := `Template AP
	ModifiedFiles NA
	import actors_gr_GR_cog.*
	import da.*
	import hi.s16_gr_GR_cog.*
    import ho.s03_gr_GR_cog.*
	import me.m11.d30_gr_GR_cog.*
	import misc_gr_GR_cog.*
	import prayers_gr_GR_cog.*

	Actor sid Deacon End-Actor
	Dialog<i>sid parenthesis.open sid res04 sid parenthesis.close</>End-Dialog

	when-sundays-before-triodion-is 
		1 use: 
			Hymn sid meMA.Ode3C34.ode @ver End-Hymn
		2 use:
			Hymn sid oc.m2.d2_gr_GR_cog.ocMA.ApolTheotokion.text @ver End-Hymn
		otherwise use:
			Reading sid hoMA.DoxologyVerse02.text End-Reading
	end-when
	Para role prayer sid hiCE.Key_0102.text @ver End-Para
End-Template
`
	expectStarts := 803
	expect := `id = "Blocks/AP"
type = "block"
status = "na"
p.actor sid "actors/Deacon"
p.dialog .i ( sid "misc/parenthesis.open" sid "prayers/res04" sid "misc/parenthesis.close" ) 
switch sundaysBeforeTriodion {
  case 1: {
    p.hymn sid "me.m11.d30/meMA.Ode3C34.ode" @ver
  }
  case 2: {
    p.hymn sid "oc.m2.d2/ocMA.ApolTheotokion.text" @ver
  }
  default: {
    p.reading sid "ho.s03/hoMA.DoxologyVerse02.text"
  }
}
p.prayer sid "hi.s16/hiCE.Key_0102.text" @ver
`
	p, err := NewAtemParser("test/out/", "Blocks/AP", input, InverseKeyMap, *ITags, TemplateMap, nil)
	if err != nil {
		t.Errorf("%s", err)
	}
	parseErrors := p.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("%v", parseErrors[0])
	}
	got := p.Listener.MainContent()
	err = GotExpected(expectStarts, expect, got)
	if err != nil {
		t.Errorf("%v", err)
	}
}
func TestAtemParserWhenSundayAfterElevationOfCross(t *testing.T) {
	input := `Template AP
	ModifiedFiles NA
	import actors_gr_GR_cog.*
	import da.*
	import hi.s16_gr_GR_cog.*
    import ho.s03_gr_GR_cog.*
	import me.m11.d30_gr_GR_cog.*
	import misc_gr_GR_cog.*
	import prayers_gr_GR_cog.*

	Actor sid Deacon End-Actor
	Dialog<i>sid parenthesis.open sid res04 sid parenthesis.close</>End-Dialog

	when-Sunday-after-Elevation-of-Cross-is 
		Sep 15 use: 
			Hymn sid meMA.Ode3C34.ode @ver End-Hymn
		Sep 16 use:
			Hymn sid oc.m2.d2_gr_GR_cog.ocMA.ApolTheotokion.text @ver End-Hymn
		otherwise use:
			Reading sid hoMA.DoxologyVerse02.text End-Reading
	end-when
	Para role prayer sid hiCE.Key_0102.text @ver End-Para
End-Template
`
	expectStarts := 861
	expect := `id = "Blocks/AP"
type = "block"
status = "na"
p.actor sid "actors/Deacon"
p.dialog .i ( sid "misc/parenthesis.open" sid "prayers/res04" sid "misc/parenthesis.close" ) 
switch sundayAfterElevationOfCross {
  case sep 15: {
    p.hymn sid "me.m11.d30/meMA.Ode3C34.ode" @ver
  }
  case sep 16: {
    p.hymn sid "oc.m2.d2/ocMA.ApolTheotokion.text" @ver
  }
  default: {
    p.reading sid "ho.s03/hoMA.DoxologyVerse02.text"
  }
}
p.prayer sid "hi.s16/hiCE.Key_0102.text" @ver
`
	p, err := NewAtemParser("test/out/", "Blocks/AP", input, InverseKeyMap, *ITags, TemplateMap, nil)
	if err != nil {
		t.Errorf("%s", err)
	}
	parseErrors := p.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("%v", parseErrors[0])
	}
	got := p.Listener.MainContent()
	err = GotExpected(expectStarts, expect, got)
	if err != nil {
		t.Errorf("%v", err)
	}
}
func TestAtemParserWhenExists(t *testing.T) {
	input := `Template AP
	ModifiedFiles NA
	import actors_gr_GR_cog.*
	import da.*
	import hi.s16_gr_GR_cog.*
    import ho.s03_gr_GR_cog.*
	import me.*
	import misc_gr_GR_cog.*
	import prayers_gr_GR_cog.*

	Actor sid Deacon End-Actor
	Dialog<i>sid parenthesis.open sid res04 sid parenthesis.close</>End-Dialog

	when-exists rid meMA.Ode3C34.ode 
		use: 
			Media rid meMA.Ode3C34.ode End-Media
			Hymn rid meMA.Ode3C34.ode @ver End-Hymn
		otherwise use:
			Media sid hoMA.DoxologyVerse02.text End-Media
			Reading sid hoMA.DoxologyVerse02.text End-Reading
	end-when
	Para role prayer sid hiCE.Key_0102.text @ver End-Para
End-Template
`
	expectStarts := 919
	expect := `id = "Blocks/AP"
type = "block"
status = "na"
p.actor sid "actors/Deacon"
p.dialog .i ( sid "misc/parenthesis.open" sid "prayers/res04" sid "misc/parenthesis.close" ) 
if exists rid "me.*/meMA.Ode3C34.ode" {
	media rid "me.*/meMA.Ode3C34.ode"
    p.hymn rid "me.*/meMA.Ode3C34.ode" @ver
} else {
	media sid "ho.s03/hoMA.DoxologyVerse02.text"
	p.reading sid "ho.s03/hoMA.DoxologyVerse02.text"
}
p.prayer sid "hi.s16/hiCE.Key_0102.text" @ver
`
	p, err := NewAtemParser("test/out/", "Blocks/AP", input, InverseKeyMap, *ITags, TemplateMap, nil)
	if err != nil {
		t.Errorf("%s", err)
	}
	parseErrors := p.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("%v", parseErrors[0])
	}
	got := p.Listener.MainContent()
	err = GotExpected(expectStarts, expect, got)
	if err != nil {
		t.Errorf("%v", err)
	}
}

func TestAtemParserWhenExistsSimple(t *testing.T) {
	input := `Template AP
	Status NA
	import actors_gr_GR_cog.*
	import da.*
	import hi.s16_gr_GR_cog.*
    import ho.s03_gr_GR_cog.*
	import me.*
	import misc_gr_GR_cog.*
	import prayers_gr_GR_cog.*

	when-exists rid meMA.Ode3C34.ode 
		use: 
            Actor sid Priest End-Actor
		otherwise use:
            Actor sid Deacon End-Actor
	end-when
End-Template
`
	expectStarts := 968
	expect := `id = "Blocks/bl.AP"
type = "block"
status = "na"
if exists rid "me.*:meMA.Ode3C34.ode" {
  p.actor sid "actors:Priest"
} else {
  p.actor sid "actors:Deacon"
}
`
	p, err := NewAtemParser("test/out/", "Blocks/AP", input, InverseKeyMap, *ITags, TemplateMap, nil)
	if err != nil {
		t.Errorf("%s", err)
	}
	parseErrors := p.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("%v", parseErrors[0])
	}
	got := p.Listener.MainContent()
	err = GotExpected(expectStarts, expect, got)
	if err != nil {
		t.Errorf("%v", err)
	}
}
func TestAtemParserWhenExistsElseSimple(t *testing.T) {
	input := `Template AP
	Status NA
	import actors_gr_GR_cog.*
	import da.*
	import hi.s16_gr_GR_cog.*
    import ho.s03_gr_GR_cog.*
	import me.*
	import misc_gr_GR_cog.*
	import prayers_gr_GR_cog.*
	Section me1o2t 
		when-exists rid meMA.Ode3C34.ode 
			use: 
				Actor sid Priest End-Actor
			otherwise use:
				Actor sid Deacon End-Actor
		end-when
    End-Section
End-Template
`
	expectStarts := 968
	expect := `id = "Blocks/bl.AP"
type = "block"
status = "na"
if exists rid "me.*:meMA.Ode3C34.ode" {
  p.actor sid "actors:Priest"
} else {
  p.actor sid "actors:Deacon"
}
`
	p, err := NewAtemParser("test/out/", "Blocks/AP", input, InverseKeyMap, *ITags, TemplateMap, nil)
	if err != nil {
		t.Errorf("%s", err)
	}
	parseErrors := p.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("%v", parseErrors[0])
	}
	got := p.Listener.MainContent()
	err = GotExpected(expectStarts, expect, got)
	if err != nil {
		t.Errorf("%v", err)
	}
}
func TestAtemParserHref(t *testing.T) {
	input := `Template AP

Status NA

	import pdf.covers_en_US_goarch.*

	import actors_gr_GR_cog.*
	import miscellanea_gr_GR_cog.*
	import prayers_gr_GR_cog.*
	import rubrical_gr_GR_cog.*
	import template.titles_gr_GR_cog.*
	import titles_gr_GR_cog.*

	import iTags.*
	import bTags.*
	import roles.*
	import rules.*
	Title role cover10 sid Priest End-Title
	Title role cover11 sid Deacon End-Title
End-Template
`
	expectStarts := 968
	expect := `id = "Blocks/AP"
type = "block"
status = "na"
  p.title a.cover11 href sid "actors:Priest" target _blank label sid "actors:Deacon"
`
	p, err := NewAtemParser("test/out/", "Blocks/AP", input, InverseKeyMap, *ITags, TemplateMap, nil)
	if err != nil {
		t.Errorf("%s", err)
	}
	parseErrors := p.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("%v", parseErrors[0])
	}
	got := p.Listener.MainContent()
	err = GotExpected(expectStarts, expect, got)
	if err != nil {
		t.Errorf("%v", err)
	}
}
func TestAtemParserWhenExistsEmbeddedSwitch(t *testing.T) {
	input := `Template AP
Status NA
import actors_gr_GR_cog.*
import da.*
import hi.s16_gr_GR_cog.*
import ho.s03_gr_GR_cog.*
import me.*
import misc_gr_GR_cog.*
import prayers_gr_GR_cog.*

when-exists rid meMA.Ode3C34.ode use:
  when-exists rid meMA.Ode3C34.ode use: 
    Actor sid Deacon End-Actor
  end-when
  when-name-of-day-is 
     Monday, Tuesday, Thursday use:
       when-exists rid meMA.Ode3C34.ode use:
         Actor sid Deacon End-Actor
       end-when
     Wednesday, Friday use:
       when-exists rid meMA.Ode3C34.ode use: 
         Actor sid Deacon End-Actor
       end-when
  end-when
otherwise use:
  when-name-of-day-is 
    Monday, Tuesday, Thursday use:
      when-exists rid meMA.Ode3C34.ode use: 
        Actor sid Deacon End-Actor
      end-when
	Wednesday, Friday use:
      when-exists rid meMA.Ode3C34.ode use: 
        Actor sid Deacon End-Actor
      end-when
  end-when
end-when

End-Template
`
	expectStarts := 1032
	expect := `id = "Blocks/bl.AP"
type = "block"
status = "na"
if exists rid "me.*:meMA.Ode3C34.ode" {
  if exists rid "me.*:meMA.Ode3C34.ode" {
    p.actor sid "actors:Deacon"
  }  
  switch dayOfWeek {
    case mon, tue, thu: {
      if exists rid "me.*:meMA.Ode3C34.ode" {
        p.actor sid "actors:Deacon"
      }  
    }
    case wed, fri: {
      if exists rid "me.*:meMA.Ode3C34.ode" {
        p.actor sid "actors:Deacon"
      }  
    }
  }
} else {
  switch dayOfWeek {
    case mon, tue, thu: {
      if exists rid "me.*:meMA.Ode3C34.ode" {
        p.actor sid "actors:Deacon"
      }  
    }
    case wed, fri: {
      if exists rid "me.*:meMA.Ode3C34.ode" {
        p.actor sid "actors:Deacon"
      }  
    }
  }
}
`
	p, err := NewAtemParser("test/out/", "Blocks/AP", input, InverseKeyMap, *ITags, TemplateMap, nil)
	if err != nil {
		t.Errorf("%s", err)
	}
	parseErrors := p.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("%v", parseErrors[0])
	}
	got := p.Listener.MainContent()
	err = GotExpected(expectStarts, expect, got)
	if err != nil {
		t.Errorf("%v", err)
	}
}
func TestAtemListener_EnterTemplateTitle(t *testing.T) {
	input := `Template AP
Head
  Template_Title @text "Door Opening of the Church" End_Title
End_Head
End-Template
`
	expectStarts := 2
	expect := `id = "Blocks/AP"
type = "block"
indexLastTitleOverride = "Door Opening of the Church"
`
	p, err := NewAtemParser("test/out/", "Blocks/AP", input, InverseKeyMap, *ITags, TemplateMap, nil)
	if err != nil {
		t.Errorf("%s", err)
	}
	parseErrors := p.WalkTemplate()
	if len(parseErrors) > 0 {
		t.Errorf("%v", parseErrors[0])
	}
	got := p.Listener.MainContent()
	err = GotExpected(expectStarts, expect, got)
	if err != nil {
		t.Errorf("%v", err)
	}
}

func GotExpected(expectedStartsLine int, expected, got string) error {
	expectedLines := strings.Split(expected, "\n")
	gotLines := strings.Split(got, "\n")
	s := len(gotLines)
	for i, l := range expectedLines {
		if i < s {
			if strings.TrimSpace(l) != strings.TrimSpace(gotLines[i]) {
				return fmt.Errorf("\nline %d: expected:\n\t%s\ngot:\n\t%s\n", expectedStartsLine+i, l, gotLines[i])
			}
		}
	}
	return nil
}

func TestAtemExtractLid(t *testing.T) {
	teststring := "oc.m1.d2_gr_GR_cog.ocMA.ApolTheotokion.mode"
	expected := "gr_gr_cog/oc.m1.d2:ocMA.ApolTheotokion.mode"
	got := AtemExtractLid(teststring)
	if got != expected {
		t.Errorf("expected %s but got %s instead.", expected, got)
	}
}
