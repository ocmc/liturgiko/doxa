package ares2bolt

import (
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/mappers"
	"github.com/liturgiko/doxa/pkg/utils/repos"
	"sort"
	"strings"
)

func FromGithubFiles(
	rootDir string,
	mapper *kvs.KVS,
	printProgress bool,
	delete bool,
) error {

	// to keep track of keys we have seen
	idMap := make(map[string]bool)
	batchSize := 2000
	// so we can batch up the records
	var valBatch []*kvs.DbR
	doxlog.Info("loading ares")

	// pipeline: each line of each file of each repo is processed as follows...
	// Ares2LpFromLocalDir -> LineParts -> Lp2Lt -> Ltx, which is then written to bolt
	for ltx := range mappers.Lp2Lt(repos.Ares2LpFromLocalDir(rootDir, "ares", printProgress)) {
		if ltx.Library == "__" {
			return fmt.Errorf("bad library: %s", ltx.Library)
		}
		// report non-unique id
		if idMap[ltx.ID] {
			doxlog.Warn(fmt.Sprintf("Duplicate ID: %s", ltx.ID))
		} else { // write db for unique id
			idMap[ltx.ID] = true
			dbr := kvs.NewDbR()
			dbr.KP.Dirs.Push("ltx")
			dbr.KP.Dirs.Push(ltx.Library)
			dbr.KP.Dirs.Push(ltx.Topic)
			dbr.KP.KeyParts.Push(ltx.Key)
			dbr.Value = strings.ReplaceAll(ltx.Value, "'", "''")
			if len(ltx.Redirect) > 0 {
				rDbr := kvs.NewDbR()
				if strings.Contains(ltx.Redirect, ":") {
					ltx.Redirect = strings.Replace(ltx.Redirect, ":", "/", 1)
				}
				parts := strings.Split(ltx.Redirect, "/")
				if len(parts) > 2 {
					rDbr.KP.Dirs.Push("ltx")
					rDbr.KP.Dirs.Push(parts[0])
					rDbr.KP.Dirs.Push(parts[1])
					rDbr.KP.KeyParts.Push(parts[2])
					dbr.Value = fmt.Sprintf("%s%s", kvs.RedirectsTo, rDbr.KP.Path())
				} else {
					return fmt.Errorf("bad redirect: %s", ltx.Redirect)
				}
			}
			valBatch = append(valBatch, dbr)
			if len(valBatch) >= batchSize {
				err := mapper.Db.Batch(valBatch)
				if err != nil {
					return fmt.Errorf("error batching records: %v", err)
				}
				valBatch = nil
			}
		}
	}
	// write the batch if it finished len < batchSize
	if valBatch != nil {
		err := mapper.Db.Batch(valBatch)
		if err != nil {
			return fmt.Errorf("error batching records: %v", err)
		}
	}
	return nil
}
func FromGithubMemory(
	urls []string,
	mapper *kvs.KVS,
	printProgress bool,
	delete bool,
) error {
	// to keep track of keys we have seen
	idMap := make(map[string]bool)
	// to keep track of whether we have deleted the library before loading
	libMap := make(map[string]bool)

	batchSize := 5000

	var optimizedUrls []string

	sort.Strings(urls)

	largeLibraries := []string{"gr-gr-cog.git",
		"en-us-dedes.git",
		"en-us-goadedes.git",
		"en-redirects-goarch.git",
		"gr-redirects-goarch.git",
		"client-enpublic.git",
	}
	// pass 1 -- add the largest libraries to our load list
	for _, u := range urls {
		for _, l := range largeLibraries {
			if strings.HasSuffix(u, l) {
				optimizedUrls = append(optimizedUrls, u)
			}
		}
	}
	// pass 2 -- append all the rest of the urls,
	// excluding the ones from pass 1 above.
outer:
	for _, u := range urls {
		for _, l := range largeLibraries {
			if strings.HasSuffix(u, l) {
				continue outer
			}
		}
		optimizedUrls = append(optimizedUrls, u)
	}

	// so we can batch up the records
	var valBatch []*kvs.DbR

	for ltx := range mappers.Lp2Lt(repos.Ares2LpFromGithub(optimizedUrls, "ares", printProgress)) {
		if delete {
			if _, deleted := libMap[ltx.Library]; !deleted {
				// delete lib if exists
				kp := kvs.NewKeyPath()
				kp.Dirs.Push("ltx")
				kp.Dirs.Push(ltx.Library)
				if mapper.Db.Exists(kp) {
					err := mapper.Db.Delete(*kp)
					if err != nil {
						msg := fmt.Sprintf("error deleting library %s: %v", kp.Path(), err)
						doxlog.Error(msg)
						return fmt.Errorf(msg)
					}
				}
				// record the fact that we have deleted this library prior to loading
				libMap[ltx.Library] = true
			}
		}
		// report non-unique id
		if idMap[ltx.ID] {
			doxlog.Warn(fmt.Sprintf("Duplicate ID: %s", ltx.ID))
		} else { // write db for unique id
			idMap[ltx.ID] = true
			dbr := kvs.NewDbR()
			dbr.KP.Dirs.Push("ltx")
			dbr.KP.Dirs.Push(ltx.Library)
			dbr.KP.Dirs.Push(ltx.Topic)
			dbr.KP.KeyParts.Push(ltx.Key)
			dbr.Value = strings.ReplaceAll(ltx.Value, "'", "''")
			if len(ltx.Redirect) > 0 {
				rDbr := kvs.NewDbR()
				parts := strings.Split(ltx.Redirect, "/")
				if len(parts) == 2 { // see if colon exists
					if strings.Contains(parts[1], ":") {
						subParts := strings.Split(parts[1], ":")
						rDbr.KP.Dirs.Push("ltx")
						rDbr.KP.Dirs.Push(parts[0])
						rDbr.KP.Dirs.Push(subParts[0])
						rDbr.KP.KeyParts.Push(subParts[1])
						dbr.Value = fmt.Sprintf("%s%s", kvs.RedirectsTo, rDbr.KP.Path())
					} else {
						doxlog.Errorf("bad redirect: %s\n", ltx.Redirect)
					}
				} else if len(parts) == 3 {
					rDbr.KP.Dirs.Push("ltx")
					rDbr.KP.Dirs.Push(parts[0])
					rDbr.KP.Dirs.Push(parts[1])
					rDbr.KP.KeyParts.Push(parts[2])
					dbr.Value = fmt.Sprintf("%s%s", kvs.RedirectsTo, rDbr.KP.Path())
				} else {
					doxlog.Errorf("%s has bad redirect: %s\n", dbr.KP.Path(), ltx.Redirect)
				}
			}
			valBatch = append(valBatch, dbr)
			if len(valBatch) >= batchSize {
				err := mapper.Db.Batch(valBatch)
				if err != nil {
					msg := fmt.Sprintf("error batching records: %v", err)
					doxlog.Error(msg)
					return fmt.Errorf(msg)
				}
				valBatch = nil
			}
		}
	}
	// write the batch if it finished len < batchSize
	if valBatch != nil {
		err := mapper.Db.Batch(valBatch)
		if err != nil {
			msg := fmt.Sprintf("error writing remaining batch of records: %v", err)
			doxlog.Error(msg)
			return fmt.Errorf(msg)
		}
	}
	return nil
}
