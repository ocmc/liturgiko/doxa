/*
Package ares Provides utilities for parsing lines from ares files from the AGES Liturgical Workbench (ALWB) system. The acronymn `ares` means, `AGES resource.` The name of an ares file indicates its subject (called a topic) and its version (called a domain).
*/
package ares

import (
	"bufio"
	"errors"
	"fmt"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"log"
	"os"
	"path"
	"strconv"
	"strings"
)

var Logger *log.Logger
var (
	StrErrFileMissingAres      = "ares: file missing .ares extension"
	ErrFileMissingAres         = errors.New(StrErrFileMissingAres)
	StrErrFileMissingTopic     = "ares: filename missing topic or domain"
	ErrFileMissingTopic        = errors.New(StrErrFileMissingTopic)
	StrErrLineMissingEqualSign = "ares: missing equal sign between key and valu"
	ErrLineMissingEqualSign    = errors.New(StrErrLineMissingEqualSign)
	StrErrRedirectInvalid      = "ares: invalid redirect value"
	ErrRedirectInvalid         = errors.New(StrErrRedirectInvalid)
	StrErrValueMissingQuote    = "ares: value missing initial or final quote"
	ErrValueMissingQuote       = errors.New(StrErrValueMissingQuote)
)

func NewErrLineMissingEqualSign(line int) error {
	return errors.New(fmt.Sprintf("line %d ares: missing equal sign between key and value", line))
}
func NewErrValueMissingQuote(line int) error {
	return errors.New(fmt.Sprintf("line %d ares: value missing initial or final quote", line))
}

type LineParts struct {
	Language       string
	Country        string
	Realm          string
	Topic          string
	Key            string
	Value          string
	Redirect       string
	Comment        string
	IsAresId       bool
	IsBlank        bool
	IsCommentedOut bool
	IsRedirect     bool
	HasComment     bool
	HasValue       bool
	LineNbr        int
	Source         string
}

type FilenameParts struct {
	Topic    string
	Language string
	Country  string
	Realm    string
}

// ParseLine Parse the line into the fields of a LineParts struct.
// We include the language, country, realm, and topic,
// so they are available to downstream channels
func ParseLine(filenameParts FilenameParts, line string) (LineParts, error) {
	var result LineParts
	result.Language = filenameParts.Language
	result.Country = filenameParts.Country
	result.Realm = filenameParts.Realm
	result.Topic = filenameParts.Topic
	result.IsAresId = strings.HasPrefix(line, "A_")
	result.IsBlank = len(strings.TrimSpace(line)) < 1
	result.IsCommentedOut = strings.HasPrefix(line, "//")
	if strings.Contains(line, "heHE.AfrastonThavma.notmetered") {
		_ = fmt.Sprintf("TODO Delete me")
	}
	if result.IsBlank || result.IsAresId || result.IsCommentedOut {
		// fall thru
	} else {
		parts := strings.Split(line, "=")
		if len(parts) > 1 {
			result.Key = strings.TrimSpace(parts[0])
			result.Value = strings.TrimSpace(parts[1])
			if strings.Contains(result.Value, "//") {
				valueCommentParts := strings.Split(result.Value, "//")
				if len(valueCommentParts) > 1 {
					if strings.HasSuffix(valueCommentParts[0], "https:") ||
						strings.HasSuffix(valueCommentParts[0], "http:") {
						// ignore
					} else {
						result.Value = strings.TrimSpace(valueCommentParts[0])
						result.HasComment = true
						result.Comment = strings.TrimSpace(strings.Join(valueCommentParts[1:], " "))
					}
				}
			}
			result.IsRedirect = !strings.HasPrefix(result.Value, "\"") && len(result.Value) > 0
			if result.IsRedirect {
				var err error
				redirect := result.Value
				valueParts := strings.Split(result.Value, "_")
				if len(valueParts) < 4 {
					redirect = result.Topic + "_" + result.Language + "_" + result.Country + "_" + result.Realm + "." + redirect
				}
				result.Redirect, err = ToRedirectId(redirect)
				result.Value = ""
				if err != nil {
					return result, err
				}
			} else {
				if strings.HasPrefix(result.Value, "\"") && strings.HasSuffix(result.Value, "\"") {
					result.Value, _ = strconv.Unquote(result.Value)
					result.HasValue = true
				} else {
					if strings.HasPrefix(result.Value, "//") {
						result.HasValue = false
					} else if len(result.Value) == 0 { //this is a key without any value
						result.HasValue = true // but treat it as if it does so will be created
					} else {
						return result, ErrValueMissingQuote
					}
				}
			}
		} else {
			return result, ErrLineMissingEqualSign
		}
	}
	return result, nil
}

// ParseAresFileName Returns from the filename the topic, ISO country code, ISO language code, and realm.
func ParseAresFileName(f string) (FilenameParts, error) {
	var result FilenameParts
	_, filename := path.Split(ltfile.ToUnixPath(f))
	if strings.HasSuffix(filename, ".ares") {
		// work around for olw bug
		if strings.HasSuffix(filename, "KE.oak.ares") {
			filename = strings.Replace(filename, "KE.oak", "KE_oak", 1)
		}
		parts := strings.Split(filename[:len(filename)-5], "_")
		if len(parts) == 4 {
			result.Topic = parts[0]
			result.Language = strings.ToLower(parts[1])
			result.Country = strings.ToLower(parts[2])
			result.Realm = strings.ToLower(parts[3])
		} else {
			return result, ErrFileMissingTopic
		}
	} else {
		return result, ErrFileMissingAres
	}
	return result, nil
}

// LibraryName Returns the library name (Language_Country_Realm) from an ares filename
func LibraryName(f string) (string, error) {
	var result string
	fparts, err := ParseAresFileName(f)
	if err != nil {
		return result, err
	} else {
		result = fparts.Language + "_" + fparts.Country + "_" + fparts.Realm
		return result, nil
	}
}

// ToRedirectId Converts value from this form: properties_en_UK_lash.media.key
// To this form: en_us_lash~properties~media.key
/*
1. Find the position of the first underscore.
2. GetKeyPath a slice from zero to first underscore.
3. GetKeyPath a slice from underscore to period.
4. GetKeyPath a slice from there to end.
*/
func ToRedirectId(value string) (string, error) {
	var lang, country, realm, topic, key string
	value = strings.TrimSpace(value)
	domainStart := strings.Index(value, "_")
	if domainStart == -1 {
		return value, errors.New(fmt.Sprintf(" redirect %s missing domain", value))
	}
	// find where the domain ends
	var domainEnd int
	j := len(value)
	for i := domainStart; i < j; i++ {
		if value[i] == '.' {
			domainEnd = i
			break
		}
	}
	// now set the parts of the id
	topic = value[:domainStart]
	key = value[domainEnd+1:]
	l := len(value)
	if domainStart+1 < 0 {
		fmt.Printf("%s:%s domainStart+1 bad index: l=%d index = %d", topic, key, l, domainStart+1)
	}
	if domainEnd > l {
		fmt.Printf("%s:%s domainEnd+1 bad index: l=%d index = %d", topic, key, l, domainEnd)
	}
	domain := value[domainStart+1 : domainEnd]
	// split the domain into its parts
	domainParts := strings.Split(domain, "_")
	if len(domainParts) == 3 {
		lang = domainParts[0]
		country = strings.ToLower(domainParts[1])
		realm = domainParts[2]
		return ltstring.ToId(lang, country, realm, topic, key), nil
	} else {
		return value, ErrRedirectInvalid
	}
}

// CleanAres cleans Ares files by finding and fixing the following problems:
//
// When finds a value that starts with quote but does not end with one,
// attempts to joint the next line to it, if the next line appears to be the
// broken part (due to a line break)
//
// When finds a duplicate key, compares the values and keeps the key that has a value,
// throwing away the key that does not.
// If both have values, reports the problem in the log.
// If only one has a value, deletes the key that does not have a value.
// Implements F.2019.005

const emptyString = "\"\""

var lineCnt int

var InBase string  // base path of all input files
var OutBase string // base path of all output files

// The following create global lists of definitions and references to other keys found in an
// entire run of any program cleaning a tree of ares files.

type aresReference struct {
	file string
	line int
}
type libraryReferences map[string][]aresReference      // count the number of times this reference is found
var AllReferences = make(map[string]libraryReferences) // all references for all libraries

type aresReferences map[string]string

var AllDefinitions = make(map[string]aresReferences) // all references for all libraries

// If noComment is true, just log the message (if there is a logger)
// Otherwise, add the message to the end of comments for the key
func addMessage(msg string, k string, noComment bool, Logger *log.Logger, commentsForKey map[string]string) {
	if noComment {
		if Logger != nil {
			Logger.Println(fmt.Sprintf("%s %s", k, msg))
		}
	} else {
		commentsForKey[k] += msg
	}
}

func saveDefinition(source string, k string, v string, sourceComments string, noComment bool,
	definitions map[string]string, commentsForKey map[string]string) bool {

	var found bool
	var msg string
	var oldValue string

	oldValue, found = definitions[k]

	commentsForKey[k] += sourceComments // not turned off by noComment

	if found == false {
		// Case 1: no previous definition, value is "" - save value
		// Case 2: no previous definition, value is NOT "" - save value
		definitions[k] = v // new value
	} else if oldValue == emptyString && v == emptyString {
		// Case 3: saved definition and current definition are both "" - discard this definition
		msg = fmt.Sprintf("// file: %s\n\tline %d duplicate empty definition for key %s - discarded\n", source, lineCnt, k)
		addMessage(msg, k, noComment, Logger, commentsForKey)
	} else if oldValue == emptyString && v != emptyString {
		// Case 4: replacing empty definition with real definition
		definitions[k] = v // substitute real value for placeholder
		msg = fmt.Sprintf("// file: %s\n\t line %d empty definition for key %s replaced with %s\n", source, lineCnt, k, v)
		addMessage(msg, k, noComment, Logger, commentsForKey)
	} else if oldValue != emptyString && v == emptyString {
		// Case 5: old definitions is non-empty but new definition is "" - discard new definition
		msg = fmt.Sprintf("//  file: %s\n\tline %d Invalid replacement of value %s for key %s with empty string\n", source, lineCnt, oldValue, k)
		addMessage(msg, k, noComment, Logger, commentsForKey)
	} else if oldValue != emptyString && v != emptyString {
		if v == oldValue {
			// Case 6: both old definitions and new definition are non-empty and the same - discard new definition
			msg = fmt.Sprintf("//  file: %s\n\tline %d duplicate value %s for key %s - discarded\n", source, lineCnt, v, k)
		} else {
			// Case 7: both old definitions and new definition are non-empty - use new definition
			msg = fmt.Sprintf("//  file: %s\n\tline %d Substituting value %s for key %s old value was %s\n", source, lineCnt, v, k, oldValue)
			definitions[k] = v // updating value for key
		}
		addMessage(msg, k, noComment, Logger, commentsForKey)
	}
	return found
}

func alreadyDefined(k string, definitions map[string]string) bool {
	var found bool
	_, found = definitions[k]
	return found
}

func hasComments(k string, commentsForKey map[string]string) bool {
	var found bool
	_, found = commentsForKey[k]
	return found
}
func CleanAresFiles(dirIn, dirOut string, noComment bool, logger *log.Logger) error {
	Logger = logger
	dirIn = ltfile.ResolvePath(dirIn)
	dirOut = ltfile.ResolvePath(dirOut)

	// filter for only files that have a domain, e.g. gr_gr_cog
	var expressions = []string{
		".*_.*_.*",
	}
	files, err := ltfile.FileMatcher(dirIn, "ares", expressions)
	if err != nil {
		return errors.New(fmt.Sprintf("no ares files in %s\n", dirIn))
	}

	for _, f := range files {
		err = CleanAres(f, strings.Replace(f, dirIn, dirOut, 1), noComment)
		if err != nil {
			Logger.Println(err)
		}
	}

	var expectedBadReferences = map[string]string{
		"gr_gr_cog": "dis101",
		"en_en_cog": "testcase9",
	}

	for libName, libRefs := range AllReferences {
		for keyName, references := range libRefs {
			if _, ok := AllDefinitions[libName][keyName]; !ok {
				if value, ok := expectedBadReferences[libName]; !ok || value != keyName {
					if keyName != "A_Resource_Whose_Name" {
						Logger.Printf("In library %v, definition NOT found for key: %q\n", libName, keyName)
						for _, ref := range references {
							Logger.Printf("    key referenced on line %d of %v\n", ref.line, ref.file)
						}
					}
				}
			}
		}
	}
	return nil
}

func CleanAres(in, out string, noComment bool) error {
	var (
		err            error
		parts          []string
		library        string
		line           string
		nextLine       string
		aresKey        string
		aresDef        string
		keys           []string
		comments       string
		definitions    map[string]string
		commentsForKey map[string]string
	)

	definitions = make(map[string]string)
	commentsForKey = make(map[string]string)
	comments = ""

	fileIn, err := os.Open(ltfile.ToSysPath(in))
	if err != nil {
		Logger.Fatal(err)
	}
	defer fileIn.Close()
	scanner := bufio.NewScanner(fileIn)

	theDir := path.Dir(out)
	err = ltfile.CreateDirs(theDir)
	if err != nil {
		Logger.Fatal(err)
	}
	fileOut, err := ltfile.Create(out)
	if err != nil {
		Logger.Fatal(err)
	}
	defer fileOut.Close()
	w := bufio.NewWriter(fileOut)

	library, err = LibraryName(in)
	if err != nil {
		if Logger != nil {
			Logger.Printf("%s %v", in, ErrFileMissingTopic)
		} else {
			Logger.Printf("%s %v", in, ErrFileMissingTopic)
		}
		return nil
	}

	var blockComment bool

	//
	// Phase 1 - scan the file creating a slice of keys to be written and
	// a map of definitions index by the keys
	//
	for scanner.Scan() {
		line = strings.TrimSpace(scanner.Text())
		line = strings.TrimSpace(line)
		lineCnt++

		// Append all blank or comment lines to the 'comments' variable
		// to be associated with the next key
		if len(line) == 0 {
			comments = comments + line + "\n"
			continue
		}
		if strings.HasPrefix(line, "//") {
			comments = comments + line + "\n"
			continue
		}
		if strings.HasPrefix(line, "/*") || blockComment {
			comments = comments + line + "\n"
			blockComment = true
			continue
		}
		if strings.HasPrefix(line, "*/") || strings.HasSuffix(line, "*/") {
			comments = comments + line + "\n"
			blockComment = false
			continue
		}

		if !strings.Contains(line, "=") { // no "=" in the line, just treat it like a comment
			comments = comments + line + "\n"
			continue
		} else { // this is an assignment of some sort
			parts = strings.Split(line, "=")
			aresKey = strings.TrimSpace(parts[0])
			aresDef = strings.TrimSpace(parts[1])
			aresDef = strings.ReplaceAll(aresDef, "\\\"", "%%") // guard escaped quotes
			if len(parts) > 2 {                                 // a few lines have an '=' in a trailing comment
				aresDef = aresDef + " = " + strings.TrimSpace(parts[2])
			}

			if strings.HasPrefix(aresDef, "\"") { // right side starts with quote
				// Special multi-line case with embedded newline
				// if aresDef has no close quote
				if aresDef == "\"" || !strings.HasSuffix(aresDef, "\"") && strings.Count(aresDef, "\"") == 1 {
					for {
						scanner.Scan()
						nextLine = strings.TrimSpace(scanner.Text())
						nextLine = strings.ReplaceAll(nextLine, "\\\"", "%%") // guard escaped quotes
						lineCnt++
						if aresDef != "\"" && nextLine != "\"" {
							aresDef = aresDef + " " + nextLine
						} else {
							aresDef = aresDef + nextLine // no need for extra space at start or end
						}

						if strings.HasSuffix(nextLine, "\"") { // This is the last line of the definition.
							break
						}
					}
				}
				if _, ok := AllDefinitions[library][aresKey]; !ok { // create a new map of definitions
					AllDefinitions[library] = make(map[string]string)
				}
				AllDefinitions[library][aresKey] = aresDef

			} else { // without quotes, this is a reference, not a definition.  Store it for checking later
				if _, ok := AllReferences[library][aresKey]; !ok { // of there is no entry for the key reference
					refs := make(libraryReferences)
					if library != "A_Resource_Whose_Name" {
						AllReferences[library] = refs
					}
				}
				var aReference aresReference
				aReference.file = in[len(InBase)+1:]
				aReference.line = lineCnt
				if library != "A_Resource_Whose_Name" {
					AllReferences[library][aresKey] = append(AllReferences[library][aresKey], aReference)
				}
			}

			if !alreadyDefined(aresKey, definitions) { // add only the first time the key is encountered
				keys = append(keys, aresKey)
			}

			aresDef = strings.ReplaceAll(aresDef, "%%", "\\\"") // restore escaped quotes

			saveDefinition(in, aresKey, aresDef, comments, noComment, definitions, commentsForKey)
			comments = ""
		}
	}

	//
	// phase 2 - using the slice of keys, write out the keys and their definitions
	//
	lineCnt = 0 // reset to zero

	for _, k := range keys {
		if hasComments(k, commentsForKey) { // are there comments for the key to write out first?
			w.WriteString(commentsForKey[k])
		}
		w.WriteString(k + " = " + definitions[k] + "\n")
	}
	if len(comments) > 0 {
		w.WriteString(comments) // These lines were found at the end of the file (have no key).
	}
	w.Flush()
	return err
}

// GetAresErrors returns an array of errors found
// in the specified ares output file.  The errors will
// be one of the error types defined in this package.
func GetAresErrors(out string) *[]error {
	var result []error
	file, err := os.Open(ltfile.ToSysPath(out))
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	var fileNameParts FilenameParts
	fileNameParts, err = ParseAresFileName(file.Name())
	fname := file.Name()
	fname = fname[len(OutBase)+2:]
	if err != nil {
		result = append(result, errors.New(fmt.Sprintf("%s: %v", fname, err)))
	}
	var lineCnt int
	inCommentBlock := false

	seenKey := map[string]bool{}

	for scanner.Scan() {
		lineCnt = lineCnt + 1
		line := strings.TrimSpace(scanner.Text())
		line = strings.TrimSpace(line)
		if strings.HasPrefix(line, "/*") {
			inCommentBlock = true
			continue
		}
		if strings.HasPrefix(line, "*/") || strings.HasSuffix(line, "*/") {
			inCommentBlock = false
			continue
		}
		if !inCommentBlock {
			lineParts, err := ParseLine(fileNameParts, line)
			lineParts.LineNbr = lineCnt
			if len(lineParts.Key) > 0 && seenKey[lineParts.Key] {
				result = append(result, errors.New(fmt.Sprintf("%s Line: %d: duplicate key %s", fname, lineCnt, lineParts.Key)))
			} else {
				seenKey[lineParts.Key] = true
			}
			if err != nil {
				result = append(result, errors.New(fmt.Sprintf("%s line: %d: %s", fname, lineCnt, err)))
			}
		}
	}
	return &result
}
func AddIncipitsToAresFiles(dirIn, dirOut string, logger *log.Logger) error {
	Logger = logger
	dirIn = ltfile.ResolvePath(dirIn)
	dirOut = ltfile.ResolvePath(dirOut)

	// file filters
	var expressions = []string{
		"prayers.*_.*_.*",
		//"me.*.*_.*_.*",
		//"oc.*.*_.*_.*",
		//"pe.*_.*_.*",
		//"tr.*_.*_.*",
		//"gm.*_.*_.*",
		//"eo.*_.*_.*",
		//"da.*_.*_.*",
		//"eu.*_.*_.*",
		//"ho.*_.*_.*",
		//"other.*_.*_.*",
		//"client",
		//"da",
		//"eo",
		//"me",
		//"oc",
		//"pe",
		//"sy",
		//"tr",
		//"ty",
	}
	// add report of lines that are commented out for Fr. S.

	files, err := ltfile.FileMatcher(dirIn, "ares", expressions)
	if err != nil {
		return errors.New(fmt.Sprintf("no ares files in %s\n", dirIn))
	}

	for _, f := range files {
		err = AddIncipitsToAresFile(f, strings.Replace(f, dirIn, dirOut, 1), logger)
		if err != nil {
			Logger.Println(err)
		}
	}
	return nil
}
func AddIncipitsToAresFile(in, out string, logger *log.Logger) error {
	var err error
	prayers := strings.Contains(in, "prayers_")
	fileIn, err := os.Open(ltfile.ToSysPath(in))
	if err != nil {
		logger.Fatal(err)
	}
	defer fileIn.Close()

	scanner := bufio.NewScanner(fileIn)
	var prevLine string
	inComment := false
	inHymnSet := false
	logComments := false

	sb := strings.Builder{}

	i := 0
	in = strings.Replace(in, "/Users/mac002/git/ages/ares/dcs/AGES-Initiatives/", "", 1)
	for scanner.Scan() {
		i++
		var inlineComment string
		line := strings.TrimSpace(scanner.Text())
		if strings.HasPrefix(line, "A_Resource") {
			sb.WriteString(line + "\n")
			prevLine = ""
			inHymnSet = false
			continue
		}
		if len(line) == 0 {
			sb.WriteString(line + "\n")
			prevLine = ""
			inHymnSet = false
			continue
		}
		if strings.HasPrefix(line, "/*") {
			inComment = true
			sb.WriteString(line + "\n")
			prevLine = ""
			if logComments {
				logger.Printf("Comment at line %d in %s\n", i, in)
			}
			continue
		}
		if strings.HasPrefix(line, "*/") {
			inComment = false
			sb.WriteString(line + "\n")
			prevLine = ""
			continue
		}

		if inComment {
			sb.WriteString(line + "\n")
			prevLine = ""
			continue
		}
		if strings.HasPrefix(line, "//") {
			sb.WriteString(line + "\n")
			prevLine = ""
			if logComments {
				logger.Printf("Comment at line %d in %s\n", i, in)
			}
			continue
		}
		line = strings.Replace(line, "= ", "=", 1)
		line = strings.Replace(line, " =", "=", 1)
		parts := strings.Split(line, "=")
		line = strings.Replace(line, "=", " = ", 1)
		if strings.Contains(line, "incipit") {
			if !strings.HasSuffix(line, "incipit") {
				prevLine = line
			}
		}
		if !strings.Contains(prevLine, "incipit") {
			if len(parts) == 2 {
				key := parts[0]
				if strings.HasSuffix(key, ".melody") || strings.HasSuffix(key, ".mode") {
					inHymnSet = true
				} else if prayers {
					inHymnSet = true
				}
				text := parts[1]
				if strings.Contains(text, "//") {
					tParts := strings.Split(text, "//")
					text = tParts[0]
					if len(tParts) == 2 {
						inlineComment = tParts[1]
					}
				}

				if text == "\"\"" {
					text = ""
				} else {
					if len(text) > 2 {
						if text[0] == '"' {
							text = text[1:]
						}
						if text[len(text)-1] == '"' {
							text = text[:len(text)-1]
						}
					}
				}
				if strings.HasSuffix(key, ".text") {
					key = strings.Replace(key, ".text", ".incipit", 1)
				} else {
					if prayers {
						key = key + ".incipit"
					} else {
						sb.WriteString(line + "\n")
						prevLine = ""
						continue
					}
				}
				if strings.HasSuffix(text, ".text") {
					text = strings.Replace(text, ".text", ".incipit", 1)
					if len(inlineComment) > 0 {
						text = fmt.Sprintf("%s // %s", text, inlineComment)
					}
					sb.WriteString(fmt.Sprintf("%s = %s\n", key, text))
				} else {
					if inHymnSet {
						if len(text) == 0 {
							sb.WriteString(fmt.Sprintf("%s = \"\"\n", key))
						} else {
							words := strings.Split(text, " ")
							if prayers {
								if strings.HasPrefix(text, "(") {
									sb.WriteString(line + "\n")
									prevLine = ""
									continue
								} else {
									if len(words) > 5 {
										sb.WriteString(fmt.Sprintf("%s = \"%s %s %s %s %s\"\n", key, words[0], words[1], words[2], words[3], words[4]))
									} else {
										sb.WriteString(fmt.Sprintf("%s = \"%s\"\n", key, text))
									}
								}
							} else {
								if len(words) > 5 {
									sb.WriteString(fmt.Sprintf("%s = \"%s %s %s %s %s\"\n", key, words[0], words[1], words[2], words[3], words[4]))
								}
							}
						}
					}
				}
			} else {
				logger.Printf("Bad key at line %d in %s\n\t%s\n", i, in, line)
			}
		}
		sb.WriteString(line + "\n")
		prevLine = line
	}
	fileIn.Close()

	theDir := path.Dir(out)
	err = ltfile.CreateDirs(theDir)
	if err != nil {
		logger.Fatal(err)
	}

	fileOut, err := ltfile.Create(out)
	if err != nil {
		logger.Fatal(err)
	}
	defer fileOut.Close()
	w := bufio.NewWriter(fileOut)
	w.WriteString(sb.String())
	w.Flush()
	return err
}
