// Package readme provides templates and a method to write a README.MD file
package readme

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/enums/repositoryTypes"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"path"
)

type ReadMe struct {
	Copyright      string
	Description    string
	Title          string
	RepositoryType repositoryTypes.RepoTypes
}

func (r *ReadMe) Exists(dirPath string) bool {
	return ltfile.FileExists(path.Join(dirPath, "README.md"))
}

// Write README to specified path.  The filename README.MD will be added.
func (r *ReadMe) Write(pathOut string) error {
	if len(r.Title) == 0 {
		return fmt.Errorf("must provide a title")
	}
	if len(r.Description) == 0 {
		r.Description = r.StandardDesc(r.RepositoryType)
	}
	// add or modify filename as needed. It must be README.md.
	_, filename := path.Split(pathOut)
	if filename != "README.md" {
		pathOut = path.Join(pathOut, "README.md")
	}
	return ltfile.TextTemplateToFile("README", Template, pathOut, r)
}

const Template = `
# {{.Title}}

{{if .Description}}{{.Description}}{{end}}

{{if .Copyright}}The files are © {{.Copyright}}{{end}}

Use of these files is subject to the terms stated in the LICENSE file.

[Doxa](https://doxa.liml.org) is a liturgical software product from the [Orthodox Christian Mission Center](https://ocmc.org). 

[Doxa install link](https://github.com/liturgiko/doxa/releases)
`

func (r *ReadMe) StandardDesc(repoType repositoryTypes.RepoTypes) string {
	switch repoType {
	case repositoryTypes.Asset:
		return "Website assets (e.g. css, javascript) used by Doxa when generating a liturgical website."
	case repositoryTypes.Btx:
		return "Biblical translation files used by Doxa. The files in this repository are formatted to load into a Doxa database."
	case repositoryTypes.Ltx:
		return "Liturgical translation files used by Doxa when generating a liturgical website. The files in this repository are formatted to load into a Doxa."
	case repositoryTypes.Catalog:
		return "Information about projects being made public for use with Doxa. If a project is not listed here, even if it is a Gitlab public project, do not use it."
	case repositoryTypes.Config:
		return "Configuration files used by Doxa."
	case repositoryTypes.Media:
		return "Specification of the title for menu items and links to audio files or musical scores, used by Doxa when generating a liturgical website."
	case repositoryTypes.Template:
		return "Templates written in the Liturgical Markup Language, used by Doxa when generating a liturgical website."
	case repositoryTypes.WebSite:
		return "website files generated using Doxa for deployment to a web server."
	default:
		return ""
	}
}
