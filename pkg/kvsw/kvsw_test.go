package kvsw

import (
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/inOut"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"path/filepath"
	"testing"
)

func init() {
	doxlog.Init("", "", true)
	doxlog.Info("doxLog initialized")
}
func TestWrapper_ExportAll(t *testing.T) {
	/*
		   directory structure

		     tempDir (aka Doxa home)
		       |- db
		       |- exports
		       |- imports
		       |- resources
			       |- en_us_dedes
			       |- gr_gr_cog

	*/
	tempDir := t.TempDir()
	dbDir := filepath.Join(tempDir, "db")
	exportsDir := filepath.Join(tempDir, "exports")
	resourcesDir := filepath.Join(tempDir, "resources")
	resourcesImportEnUsDedes := filepath.Join(resourcesDir, "en_us_dedes")
	importsDir := filepath.Join(tempDir, "imports")

	// create import files
	testImportDataEn := []string{"ltx/en_us_dedes/actors:ac.Archbishop\tArchBishop",
		"ltx/en_us_dedes/actors:ac.Archdeacon\tArchDeacon",
		"ltx/en_us_dedes/actors:ac.Bishop_Elect\tBishopElect"}
	testImportDataEnActorsModified := []string{"ltx/en_us_dedes/actors:ac.Archbishop\tArchbishop",
		"ltx/en_us_dedes/actors:ac.Archdeacon\tArchDeacon"}
	testImportDataEnPrayers := []string{"ltx/en_us_dedes/prayers:pet1\tLord, have mercy",
		"ltx/en_us_dedes/prayers:pet2\tHoly Trinity, have mercy"}
	testImportDataGr := []string{"ltx/gr_gr_cog/actors:ac.Archbishop\tΑΡΧΙΕΠΙΣΚΟΠΟΣ",
		"ltx/gr_gr_cog/actors:ac.Archdeacon\tΑΡΧΙΔΙΑΚΟΝΟΣ",
		"ltx/gr_gr_cog/actors:ac.Bishop_Elect\tΕΨΗΦΙΣΜΕΝΟΣ ΕΠΙΣΚΟΠΟΣ"}
	enImport := filepath.Join(importsDir, "en_us_dedes", "actors.tsv")
	enResourcesActors := filepath.Join(resourcesDir, "en_us_dedes", "actors.tsv")
	enResourcesPrayers := filepath.Join(resourcesDir, "en_us_dedes", "prayers.tsv")
	grImport := filepath.Join(importsDir, "gr_gr_cog", "prayers.tsv")
	mixedImport := filepath.Join(importsDir, "mixed/actors.tsv")

	expectRecCountBeforeImportFromDir := len(testImportDataEn) + len(testImportDataGr)
	expectRecCountAfterImportFromDir := len(testImportDataGr) + len(testImportDataEnActorsModified) + len(testImportDataEnPrayers)
	// to populate db
	err := ltfile.WriteLinesToFile(enImport, testImportDataEn)
	if err != nil {
		t.Error(err)
		return
	}
	err = ltfile.WriteLinesToFile(grImport, testImportDataGr)
	if err != nil {
		t.Error(err)
		return
	}
	err = ltfile.WriteLinesToFile(mixedImport, append(testImportDataEn, testImportDataGr...))
	if err != nil {
		t.Error(err)
		return
	}
	// to simulate import from resources
	err = ltfile.WriteLinesToFile(enResourcesActors, testImportDataEnActorsModified)
	if err != nil {
		t.Error(err)
		return
	}
	err = ltfile.WriteLinesToFile(enResourcesPrayers, testImportDataEnPrayers)
	if err != nil {
		t.Error(err)
		return
	}

	// to populate db
	// initialize database
	dbPath := filepath.Join(dbDir, "liturgical.db")
	ds, err := kvs.NewBoltKVS(dbPath)
	if err != nil {
		t.Error(err)
		return
	}
	defer func(ds *kvs.BoltKVS) {
		err = ds.Close()
		if err != nil {
			t.Error(err)
		}
	}(ds)
	var theKvs *kvs.KVS
	theKvs, err = kvs.NewKVS(ds)
	if err != nil {
		t.Error(err)
		return
	}
	var w *Wrapper
	w, err = NewWrapper(theKvs)
	if err != nil {
		t.Error(err)
		return
	}
	dump := func(title string, data []string) {
		fmt.Println(title)
		for _, d := range data {
			fmt.Printf("\t%s\n", d)
		}
	}
	printDbRecs := func(title string, recs []*kvs.DbR) {
		fmt.Println(title)
		for _, rec := range recs {
			fmt.Printf("\t%s = %s\n", rec.KP.Path(), rec.Value)
		}
	}
	load := func(dirsPrefix, importPath string) (int, []error) {
		cnt, errors := w.Kvs.Db.Import(dirsPrefix, importPath, inOut.LineParser, false, false, false)
		if len(errors) != 0 {
			t.Errorf("imports failed: %v", errors)
			t.Fail()
		}
		if cnt == 0 {
			t.Errorf("nothing imported from %s", importPath)
			t.Fail()
		}
		return cnt, errors
	}
	// load the database
	_, _ = load("", enImport)
	_, _ = load("", grImport)

	// verify the record count
	var recs []*kvs.DbR
	recs, err = w.GetRecordsMatchingPartialId("ltx", ".*")
	if err != nil {
		t.Error(err)
		return
	}
	if len(recs) != expectRecCountBeforeImportFromDir {
		dump("testImportDataEn", testImportDataEn)
		dump("testImportDataGr", testImportDataGr)
		printDbRecs("database", recs)
		t.Errorf("expected %d records, got %d", expectRecCountBeforeImportFromDir, len(recs))
		t.Fail()
	}
	errors := w.ImportDir("ltx/en_us_dedes", resourcesImportEnUsDedes, exportsDir, true)
	if len(errors) != 0 {
		t.Errorf("imports failed: %v", errors)
		t.Fail()
	}
	// verify that the library was exported so that it could be restored
	// TODO
	// verify we the expected number of records after the import.
	recs, err = w.GetRecordsMatchingPartialId("ltx", ".*")
	if err != nil {
		t.Error(err)
		return
	}
	if len(recs) != expectRecCountAfterImportFromDir {
		dump("testImportDataGr", testImportDataGr)
		dump("testImportDataEnActorsModified", testImportDataEnActorsModified)
		dump("testImportDataEnPrayers", testImportDataEnPrayers)
		printDbRecs("database", recs)

		t.Errorf("expected %d records, got %d", expectRecCountAfterImportFromDir, len(recs))
		t.Fail()
	}
	/*
		expect "ltx/en_us_dedes/actors:ac.Archbishop\tArchBishop" modified to
			"ltx/en_us_dedes/actors:ac.Archbishop\tArchbishop"
		expect "ltx/en_us_dedes/actors:ac.Bishop_Elect\tBishopElect" deleted
	*/
	expectedKeysExist := []string{"ltx/en_us_dedes/actors:ac.Archbishop"}
	expectedKeysDeleted := []string{"ltx/en_us_dedes/actors:ac.Bishop_Elect"}

	for _, k := range expectedKeysExist {
		kp := kvs.NewKeyPath()
		err = kp.ParsePath(k)
		if !w.Kvs.Db.Exists(kp) {
			t.Errorf("failed to find key: %s", k)
		}
	}
	for _, k := range expectedKeysDeleted {
		kp := kvs.NewKeyPath()
		err = kp.ParsePath(k)
		if w.Kvs.Db.Exists(kp) {
			t.Errorf("should not have found key: %s", k)
		}
	}
}
func TestWrapper_WordCount(t *testing.T) {
	dbPath := "/Volumes/ssd2/doxa/backups/liturgical.db"
	ds, err := kvs.NewBoltKVS(dbPath)
	if err != nil {
		t.Error(err)
		return
	}
	var theKvs *kvs.KVS
	theKvs, err = kvs.NewKVS(ds)
	if err != nil {
		t.Error(err)
		return
	}
	var wrapper *Wrapper
	wrapper, err = NewWrapper(theKvs)
	if err != nil {
		t.Error(err)
		return
	}
	var count int
	pattern := `ltx/gr_gr_cog/me.m01.d01:.*\.text`
	count, err = wrapper.WordCount(pattern)
	if err != nil {
		t.Error(err)
		return
	}
	fmt.Printf("Count: %d\n", count)
}
