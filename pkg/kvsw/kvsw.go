// Package kvsw provides a wrapper around a Kvs.
// It contains application specific composition
// of Kvs methods.
// TODO: analyze existing functions and methods in the Kvs.go and boltdb.go and refactor to here as needed.
package kvsw

import (
	"context"
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/inOut"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"github.com/liturgiko/doxa/pkg/utils/stamp"
	"path"
	"sort"
	"strings"
	"sync"
	"time"
)

type Wrapper struct {
	Kvs             *kvs.KVS
	ProjectPushData []string // TODO: change to map
	ExportTimeout   time.Duration
	MaxWorkers      int
}

func NewWrapper(kvs *kvs.KVS) (*Wrapper, error) {
	if kvs == nil {
		msg := "Kvs is nil"
		doxlog.Errorf(msg)
		return nil, fmt.Errorf(msg)
	}
	if kvs.Db == nil {
		msg := "Kvs.Db is nil"
		doxlog.Errorf(msg)
		return nil, fmt.Errorf(msg)
	}
	w := new(Wrapper)
	w.Kvs = kvs
	w.ExportTimeout = 10 * time.Minute // Default timeout
	w.MaxWorkers = 10                  // Default number of concurrent workers
	return w, nil
}
func (w *Wrapper) WordCount(kpPath string) (count int, err error) {
	matcher := kvs.NewMatcher()
	err = matcher.KP.ParsePath(kpPath)
	if err != nil {
		return 0, err
	}
	matcher.Recursive = true
	matcher.IncludeEmpty = false
	err = matcher.UseAsRegEx()
	if err != nil {
		doxlog.Errorf("error parsing regex pattern: %v", err)
		return
	}
	var dbRecs []*kvs.DbR
	dbRecs, err = w.Kvs.Db.GetMatchingIDs(*matcher)
	if err != nil {
		return 0, err
	}
	for _, rec := range dbRecs {
		count += len(strings.Fields(ltstring.ToNnp(rec.Value)))
	}
	return count, nil
}

// Backup writes the database to the specified location (the to parm).
// If timeStamp == true, the db name of the file is prefixed with a timestamp.
// max indicates the number of old copies that can be kept.  Others will be deleted.
func (w *Wrapper) Backup(to string, max int) (out string, err error) {
	if ltfile.DirExists(to) {
		// call ltFile.Max, which checks to see if the number of copies
		// in the backup db directory >= max.  If so, delete oldest,
		// leaving < max new backup files.
		deletedCount, err := ltfile.Max(to, ".db", max)
		if err != nil {
			return "", fmt.Errorf("error cleaning out old database backups: %v", err)
		}
		if deletedCount > 0 {
			doxlog.Infof("deleted %d old backups of database", deletedCount)
		}
	}
	currentTimeAsFilename := fmt.Sprintf("%s.liturgical.db", time.Now().Format("2006-01-02-15-04-05"))
	to = path.Join(to, currentTimeAsFilename)
	return w.Kvs.Db.Backup(to)
}

func (w *Wrapper) BatchStream(ctx context.Context, batchSize int, dbrChan <-chan *kvs.DbR, errChan chan<- error) {
	batch := make([]*kvs.DbR, 0, batchSize)
	batchesProcessed := 0
	recordsProcessed := 0
	doxlog.Infof("starting batch stream")
	batchStamp := stamp.NewStamp("batch stream")
	batchStamp.Start()
	batchStamp.Pause() // immediately pause so it can be in the correct state when processBath is called.

	processBatch := func() {
		if len(batch) > 0 {
			batchesProcessed++
			recordsProcessed += len(batch)
			doxlog.Infof("writing batch of %d records to the database", len(batch))
			batchStamp.Resume()
			err := w.Kvs.Db.Batch(batch)
			batchStamp.Pause()
			if err != nil {
				errChan <- err
			}
			batch = batch[:0] // Clear the batch while keeping capacity
		}
	}

	for dbr := range dbrChan {
		select {
		case <-ctx.Done():
			return
		default:
		}
		if dbr == nil {
			// Process any remaining items in the batch
			processBatch()
			// Signal that no more objects will be sent
			close(errChan)
			return
		}

		batch = append(batch, dbr)

		if len(batch) == batchSize {
			processBatch()
		}
	}

	// Process any remaining items in the batch after the channel is closed
	processBatch()
	doxlog.Infof("BatchStream wrote %d records, grouped in %d batches: %s", recordsProcessed, batchesProcessed, batchStamp.FinishMillis())
}
func (w *Wrapper) DeleteNotInMap(keep []*kvs.DbR) {}
func (w *Wrapper) SetExportFilter(pushList []string) {
	w.ProjectPushData = pushList
}

// ImportDir imports all .tsv files in the specified directory, recursively.
// `dirPrefix` may be "".  If not, it is applied as a filter against each dir path of each
// line in each .tsv file.  If the line's dir path does start with `dirPrefix`, it is excluded.
// `deleteFirst` is only used if len(dirPrefix) > 0. The `dirPrefix` is combined `deleteFirst`
// for deleting a specific directory in the database before importing lines that match the `dirPrefix`.
// This recreates the deleted db dir, and makes it identical to the imports.
// `restore dir` is used to export the db dir records before deleting them from the database.
// This ensures there is a means to restore them if needed.
// TODO: test this method.
// syncPush has three "stages" first it calls ExportAll (Kvs) and then it calls Git push (dvcs) and then this function (Kvs)
func (w *Wrapper) ImportDir(dirPrefix, importDir, restoreDir string, deleteFirst bool) []error {
	if !strings.Contains(importDir, "resources") {
		return nil
	}
	if len(importDir) > 0 {
		kp := kvs.NewKeyPath()
		err := kp.ParsePath(dirPrefix)
		if err != nil {
			msg := fmt.Sprintf("error parsing key path: %v", err)
			doxlog.Errorf(msg)
			return []error{fmt.Errorf(msg)}
		}
		if deleteFirst {
			if w.Kvs.Db.Exists(kp) {
				// export to exports/restore first, for fallback purposes
				err = w.Kvs.Db.Export(kp, "", kvs.ExportAsLine)
				doxlog.Infof("before deleting db dir, exporting %s to %s in case a restore is needed.", importDir, restoreDir)
				err = w.Kvs.Db.Delete(*kp)
				if err != nil {
					msg := fmt.Sprintf("error deleting db dir: %v", err)
					doxlog.Errorf(msg)
					//return []error{fmt.Errorf(msg)}
				}
				doxlog.Infof("before importing, deleted db dir: %s", dirPrefix)
			}
		}
	}

	files, err := ltfile.FileMatcher(importDir, kvs.FileExtension, nil)
	if err != nil {

	}
	if len(files) == 0 {
		msg := fmt.Sprintf("no %s files found in %s", kvs.FileExtension, importDir)
		doxlog.Errorf(msg)
		return []error{fmt.Errorf(msg)}
	}
	sort.Strings(files)
	// parse path to query subscriptions
	var projName string
	var subscriptionConfigs bool
	if strings.Contains(importDir, "subscriptions") {
		subscriptionConfigs = strings.Contains(importDir, "configs")
		halves := strings.Split(importDir, "subscriptions")
		children := strings.Split(halves[1], "/")
		if len(children) > 1 {
			projName = children[1]
		}
	}

	for _, tsvFile := range files {
		var errors []error
		if subscriptionConfigs {
			_, errors = w.Kvs.Db.Import(dirPrefix, tsvFile, func(line, delimiter string, removeQuotes bool, replace *kvs.DirReplace) (*kvs.KeyPath, []byte, error) {
				dirs := strings.Split(line, kvs.SegmentDelimiter)
				if len(dirs) > 1 {
					dirs[1] = projName
				}
				line = strings.Join(dirs, kvs.SegmentDelimiter)
				return inOut.LineParser(line, delimiter, removeQuotes, nil)
			}, false, false, true)
		} else {
			_, errors = w.Kvs.Db.Import(dirPrefix, tsvFile, inOut.LineParser, false, false, true)
		}
		if len(errors) > 0 {
			return errors
		}
	}
	return nil
}

// ExportAll exports the entire database as files.
// Note that if Wrapper.pushList is not nil, `all` means all that is in the pushList.
func (w *Wrapper) ExportAll(pathOut string) error {
	var err error

	err = w.exportConfig(pathOut)
	var parentDirs = []string{"ltx", "media"}
	err = w.exportDirs(pathOut, parentDirs)
	if err != nil {
		doxlog.Error(err.Error())
		return err
	}
	return w.exportConfig(pathOut)
}

func (w *Wrapper) GetRedirectLibrarySet(kpPath string) (librarySet []string, err error) {
	matcher := kvs.NewMatcher()
	err = matcher.KP.ParsePath(kpPath)
	if err != nil {
		return nil, err
	}
	matcher.ValuePattern = kpPath + ".*"
	matcher.Recursive = true
	matcher.IncludeEmpty = false
	err = matcher.UseAsRegEx()
	if err != nil {
		doxlog.Errorf("error parsing regex pattern: %v", err)
		return
	}
	libSet := make(map[string]struct{})
	var dbRecs []*kvs.DbR
	dbRecs, err = w.Kvs.Db.GetMatchingIDs(*matcher)
	if err != nil {
		return nil, err
	}
	for _, rec := range dbRecs {
		rec.SetRedirectFromValue()
		if rec.Redirect != nil && rec.Redirect.Dirs != nil && len(*rec.Redirect.Dirs) > 1 {
			libSet[rec.Redirect.Dirs.Get(1)] = struct{}{}
		}
	}
	for k, _ := range libSet {
		librarySet = append(librarySet, k)
	}
	return librarySet, nil
}

func (w *Wrapper) exportConfig(dbExportDir string) error {
	/*if w.ProjectPushData == nil {
		msg := "error: ProjectPushData is nil"
		doxlog.Errorf(msg)
		return fmt.Errorf(msg)
	}*/
	kp := kvs.NewKeyPath()
	kp.Dirs.Push("configs")
	exportPath := path.Join(dbExportDir, "configs")
	/*if !w.ProjectPushData.IncludesDbPath("configs") {
		return nil
	}*/
	var dirs []string
	var err error
	dirs, err = w.Kvs.Db.DirNames(*kp)
	for _, dir := range dirs {
		exportFilePath := path.Join(exportPath, fmt.Sprintf("%s.tsv", dir))
		err = w.Kvs.Db.Export(kp, exportFilePath, kvs.ExportAsLine)
		if err != nil {
			msg := fmt.Sprintf("error exporting %s: %d", kp.Path(), err)
			doxlog.Error(msg)
			return fmt.Errorf(msg)
		}
	}
	return nil
}

// exportDirs export the subdirectories of the supplied
// top level db directories
func (w *Wrapper) exportDirs(dbExportDir string, parentDirs []string) error {
	for _, parentDir := range parentDirs {
		err := w.exportLibraries(dbExportDir, parentDir)
		if err != nil {
			return err
		}
	}
	return nil
}
func (w *Wrapper) GetRecordsMatchingPartialId(kpPath, regExPattern string) ([]*kvs.DbR, error) {
	matcher := kvs.NewMatcher()
	err := matcher.KP.ParsePath(kpPath)
	if err != nil {
		return nil, fmt.Errorf("error parsing key path %s: %v", kpPath, err)
	}
	matcher.Recursive = true
	matcher.ValuePattern = kpPath + regExPattern
	matcher.NNP = false
	matcher.NFD = false
	err = matcher.UseAsRegEx()
	return w.Kvs.Db.GetMatchingIDs(*matcher)
}
func (w *Wrapper) exportLibraries(exportParentDir, parent string) error {
	var err error
	if w == nil {
		msg := "w *Wrapper is nil"
		doxlog.Errorf(msg)
		return fmt.Errorf(msg)
	}
	if w.Kvs == nil {
		msg := "wrapper Kvs is nil"
		doxlog.Errorf(msg)
		return fmt.Errorf(msg)
	}
	if w.Kvs.Db == nil {
		msg := "wrapper Kvs.Db is nil"
		doxlog.Errorf(msg)
		return fmt.Errorf(msg)
	}
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(parent)
	var libraries []string
	exportPath := path.Join(exportParentDir, parent)
	libraries, err = w.Kvs.Db.DirNames(*kp)
	for _, l := range libraries {
		if w.ProjectPushData != nil {
			// TODO: after changing ProjectPushData to a map, do a lookup.  If it does not exist: continue.
			// that is much simpler logic.
			include := false
			for _, incl := range w.ProjectPushData {
				if strings.Contains(incl, fmt.Sprintf("ltx/%s", l)) {
					include = true
				}
			}
			if !include {
				continue
			}
		}
		kp.Dirs.Push(l)
		var topics []string
		topics, err = w.Kvs.Db.DirNames(*kp)
		for _, t := range topics {
			exportFilePath := path.Join(exportPath, l, t+".tsv")
			kp.Dirs.Push(t)
			err = w.Kvs.Db.Export(kp, exportFilePath, kvs.ExportAsLine)
			if err != nil {
				return fmt.Errorf("error exporting %s: %d", kp.Path(), err)
			}
			kp.Dirs.Pop()
		}
		kp.Dirs.Pop()
	}
	return nil
}
func (w *Wrapper) ExportLibrary(kp kvs.KeyPath, exportPath string) error {
	var topics []string
	var err error
	topics, err = w.Kvs.Db.DirNames(kp)
	if err != nil {
		return fmt.Errorf("error getting topics to export for %s: %v", kp.Path(), err)
	}

	// Create a context with configured timeout to prevent hanging
	ctx, cancel := context.WithTimeout(context.Background(), w.ExportTimeout)
	defer cancel() // Ensure context is cancelled when we're done

	// Create buffered channels using configured worker count
	semaphore := make(chan struct{}, w.MaxWorkers)
	errChan := make(chan error, len(topics))
	doneChan := make(chan struct{})
	var wg sync.WaitGroup

	// Start a cleanup goroutine with its own done channel
	cleanupDone := make(chan struct{})
	go func() {
		defer close(cleanupDone)
		select {
		case <-ctx.Done():
			// Drain semaphore channel
			for i := 0; i < w.MaxWorkers; i++ {
				select {
				case <-semaphore:
				default:
				}
			}
			close(doneChan)
		case <-doneChan:
			return
		}
	}()

	for _, t := range topics {
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
		}

		t := t
		kp := kp.Copy()
		kp.Dirs.Push(t)
		exportFilePath := path.Join(exportPath, t+".tsv")

		wg.Add(1)
		go func(kp kvs.KeyPath, exportTopicPath string) {
			defer wg.Done()

			select {
			case semaphore <- struct{}{}:
			case <-ctx.Done():
				return
			}

			defer func() {
				select {
				case <-semaphore:
				default:
				}
			}()

			if ctx.Err() != nil {
				return
			}

			// Use a timeout for individual export operations
			exportCtx, exportCancel := context.WithTimeout(ctx, 1*time.Minute)
			defer exportCancel()

			done := make(chan error, 1)
			go func() {
				done <- w.Kvs.Db.Export(&kp, exportTopicPath, kvs.ExportAsLine)
			}()

			select {
			case err := <-done:
				if err != nil {
					errChan <- fmt.Errorf("error exporting %s: %v", kp.Path(), err)
					cancel()
				}
			case <-exportCtx.Done():
				errChan <- fmt.Errorf("timeout exporting %s", kp.Path())
				cancel()
			}
		}(*kp, exportFilePath)
	}

	// Wait with timeout
	waitChan := make(chan struct{})
	go func() {
		wg.Wait()
		close(waitChan)
	}()

	select {
	case <-waitChan:
	case <-ctx.Done():
		return ctx.Err()
	}

	close(errChan)

	// Check for errors
	for err := range errChan {
		if err != nil {
			return err
		}
	}

	// Ensure all channels are drained and closed
	close(doneChan)

	// Wait for cleanup goroutine to finish
	<-cleanupDone

	return nil
}
