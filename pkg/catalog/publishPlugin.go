package catalog

import (
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/config"
	"time"
)

type PublishPlugin struct {
	kvs        *kvs.KVS
	db         *kvs.KVS
	kp         *kvs.KeyPath
	paths      *config.Paths
	catMan     *Manager
	exportPath string
}

func (p *PublishPlugin) OnSetup(kp *kvs.KeyPath, sys, db *kvs.KVS, paths *config.Paths, catMan *Manager) bool {
	p.catMan = catMan
	p.kvs = sys
	p.kp = kp
	p.db = db
	p.exportPath = paths.CatalogFile
	p.paths = paths
	return true
}

// ValidPath determines if the current keypath is one that the command can run in
func (p *PublishPlugin) ValidPath(kp *kvs.KeyPath) bool {
	tmpKP := kp.Copy()
	dirName := tmpKP.Dirs.Pop()
	if dirName == config.CatalogDir {
		return true
	}
	return false
}

// Abr returns the abbreviation used to call the command.
func (p *PublishPlugin) Abr() string {
	return "publish"
}

// DescShort returns a short explanation of what the command does
func (p *PublishPlugin) DescShort() string {
	return "generate and publish catalog of available gitlab projects"
}

// Execute is what runs your command
func (p *PublishPlugin) Execute(args []string) {
	inputPath := p.kp.Copy()
	inputPath.Dirs.Pop()
	isInv := false
	p.exportPath = p.paths.CatalogFile
	if len(args) > 1 && args[1] == "inv" {
		isInv = true
		p.exportPath = p.paths.InventoryFile
	}
	if err := p.catMan.PublishCatalog(inputPath, p.exportPath, isInv); err != nil {
		fmt.Printf("Could not publish catalog: %v\n", err)
		return
	}
	p.catMan.StdoutAll()
	time.Sleep(1 * time.Second)
}
