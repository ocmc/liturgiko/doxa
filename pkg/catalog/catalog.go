// Package catalog supports the reading and updating a catalog repo
// The package allows the Doxa GUI to list available Doxa compatible resources for loading into the Doxa database
package catalog

import (
	bytes2 "bytes"
	"crypto/sha1"
	"encoding/json"
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/kvsw"
	"github.com/liturgiko/doxa/pkg/utils/repos"
	"sort"
	"strings"
	"text/template"
	"time"
)

type CatalogList []*struct {
	Name string `json:"name"`
	URL  string `json:"url"`
}

const catalogFileName = "catalog.json"

type EntryType int //TODO: investigate the repositoryTypes enumer
const (
	Library EntryType = iota
	Templates
	Redirects
	MediaMaps
	StyleSheets
)

func (e EntryType) MarshalJSON() ([]byte, error) {
	switch e {
	case Library:
		return []byte("\"library\""), nil
	case Templates:
		return []byte("\"templates\""), nil
	case Redirects:
		return []byte("\"redirects\""), nil
	case MediaMaps:
		return []byte("\"mediamaps\""), nil
	case StyleSheets:
		return []byte("stylesheets"), nil
	default:
		return nil, fmt.Errorf("unknown type")
	}
}

func (e *EntryType) UnmarshalJSON(data []byte) error {
	switch string(data) {
	case "\"library\"":
		*e = Library
	case "\"templates\"":
		*e = Templates
	case "\"redirects\"":
		*e = Redirects
	case "\"mediamaps\"":
		*e = MediaMaps
	case "\"stylesheets\"":
		*e = StyleSheets
	}
	return nil
}

type Catalogs struct {
}

type catalogReference struct {
	Url     string
	Version string
}

// TODO: match schemas
type Catalog struct {
	Name       string
	Desc       string `json:"Desc,omitempty"`
	GID        string `json:"-"`
	LocalPath  string `json:"-"`
	URL        string
	Maintainer string
	Version    string
	Entries    map[string]*Entry `json:"Contents"`
	References []catalogReference
}
type Entry struct {
	Name          string
	Path          string
	Url           string
	License       string
	Desc          string     `json:"Description"`
	Created       *time.Time `json:"DateCreated"`
	Updated       *time.Time `json:"DateUpdated"`
	UpdateMessage string     `json:"UpdateMessage,omitempty"`
	Maintainer    string
	Version       string
	Type          EntryType
	Dependencies  Dependencies
	Subscribed    bool `json:"-"`
}
type Dependencies []string

func (c *Catalog) Add(entry *Entry) {
	c.Entries[entry.Path+"/"+entry.Name] = entry
}
func (c *Catalog) JsonBytes() ([]byte, error) {
	return json.MarshalIndent(c, "", " ")
}
func (c *Catalog) InitializeCatalog(localPath string) error {
	c.LocalPath = localPath
	return nil
}
func (c *Catalog) LoadLocal(localPath string) error {
	c.LocalPath = localPath
	return nil
}
func LoadRemote(url string, isInv bool) (*Catalog, error) {
	fn := catalogFileName
	if isInv {
		fn = "inventory.json"
	}
	return LoadRemoteByFilename(url, fn)
}
func LoadRemoteByFilename(url, filename string) (*Catalog, error) {
	jsonString, err := repos.GetFileContent(url, filename)
	if err != nil {
		if strings.Contains(err.Error(), "authorization") {
			err = fmt.Errorf("not a repository, or authorization failed")
		}
		return nil, err
	}
	var cat *Catalog
	//var bundle *InventoryCatalogJSON
	err = json.Unmarshal(jsonString, &cat)
	if err != nil || cat == nil {
		catalog, err2 := fallbackJson(url, jsonString, err, cat)
		if err2 != nil {
			return catalog, err2
		}
	}
	return cat, nil
}

func fallbackJson(url string, jsonString []byte, err error, cat *Catalog) (*Catalog, error) {
	doxlog.Warnf("Incompatible schema, falling back to schema-specific version")
	jsonString, err = repos.GetFileContent(strings.TrimSuffix(url, ".json")+"v0.json", catalogFileName)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(jsonString, &cat)
	if err != nil {
		return nil, err
	}
	if cat == nil {
		return nil, fmt.Errorf("incompatible schema")
	}
	return nil, nil
}

//storage concerns

// ToMap creates a map of the catalog, Minus the entries
func (c *Catalog) ToMap() (output map[string]string) {
	output = make(map[string]string)
	output["name"] = c.Name
	output["desc"] = c.Desc
	output["gid"] = c.GID
	output["localPath"] = c.LocalPath
	output["url"] = c.URL
	output["maintainer"] = c.Maintainer
	output["version"] = c.Version
	return output
}

// ToMap returns a string to string map of Entry minus dependencies
func (e *Entry) ToMap() (output map[string]string) {
	output = make(map[string]string)
	output["name"] = e.Name
	if e.Created != nil {
		output["created"] = e.Created.String()
	}
	output["desc"] = e.Desc
	output["dir"] = e.Path
	output["license"] = e.License
	if e.Updated != nil {
		output["updated"] = e.Updated.String()
	}
	output["updateMessage"] = e.UpdateMessage
	output["url"] = e.Url
	output["version"] = e.Version
	output["maintainer"] = e.Maintainer
	output["dependencies"] = e.Dependencies.ToString()
	if e.Subscribed {
		output["subscribed"] = "true"
	} else {
		output["subscribed"] = "false"
	}
	return output //Dependencies ignored
}

func (d Dependencies) ToString() string {
	var output []string
	for _, dep := range d {
		output = append(output, dep)
	}
	return strings.Join(output, ", ")
}

func CatalogFromMap(input map[string]string) *Catalog {
	return &Catalog{
		Name:       input["name"],
		Desc:       input["desc"],
		GID:        input["gid"],
		LocalPath:  input["localPath"],
		URL:        input["url"],
		Maintainer: input["maintainer"],
		Version:    input["version"],
	}
}

func EntryFromMap(input map[string]string) *Entry {
	ts, _ := time.Parse("2006-01-02 15:04:05.999999999 -0700 MST", input["created"])
	updated, _ := time.Parse("2006-01-02 15:04:05.999999999 -0700 MST", input["updated"])
	var subscribed bool
	if val, ok := input["subscribed"]; ok {
		subscribed = val == "true" || val == "yes" || val == "on"
	} else if val, ok := input["included"]; ok {
		subscribed = val == "true" || val == "yes" || val == "on"
	}
	return &Entry{
		Name:          input["name"],
		Created:       &ts,
		Desc:          input["desc"],
		Path:          input["dir"],
		License:       input["license"],
		Updated:       &updated,
		UpdateMessage: input["updateMessage"],
		Url:           input["url"],
		Version:       input["version"],
		Dependencies:  DependenciesFromString(input["dependencies"]),
		Maintainer:    input["maintainer"],
		Subscribed:    subscribed,
	}
}

func DependenciesFromString(input string) Dependencies {
	return strings.Split(input, ", ")
}

func (e *Entry) String() string {
	tmpl := template.Must(template.New("entryToString").Parse(`
Name: {{.Name}}
Path: {{.Path}}
URL: {{.Url}}
License: {{.License}}
Description: {{.Desc}}
Maintainer: {{.Maintainer}}
Version: {{.Version}}
`))
	var byt bytes2.Buffer
	err := tmpl.Execute(&byt, e)
	if err != nil {
		return ""
	}
	return byt.String()
}

func EntryFromString(str string) *Entry {
	//split by newline
	lines := strings.Split(str, "\n")
	// Implement the io.Writer interface
	// This will allow the template to write directly to the Entry struct
	e := new(Entry)
	currKey := ""
	currLn := ""
	for _, s := range lines {
		var key, value string
		parts := strings.Split(s, ":")
		if len(parts) < 2 {
			key = currKey
			currLn += "\n" + parts[0]
		} else {
			key, value = parts[0], strings.Join(parts[1:], ":")
			currLn = value
		}
		key = strings.TrimSpace(key)
		value = strings.TrimSpace(value)
		currKey = key
		switch key {
		case "Name":
			e.Name = value
		case "Path":
			e.Path = value
		case "URL":
			e.Url = value
		case "License":
			e.License = value
		case "Description":
			e.Desc = currLn
		case "UpdateMessage":
			e.UpdateMessage = currLn
		case "Maintainer":
			e.Maintainer = value
		case "Version":
			e.Version = value
		case "Type":
			json.Unmarshal([]byte(value), &e.Type)
		case "Dependencies":
			e.Dependencies = strings.Split(strings.TrimSuffix(value, ","), ",")
		default:
			continue
		}
	}
	return e
}

func (e *Entry) TypeFromName() {
	if strings.Contains(e.Path, "media") {
		e.Type = MediaMaps
		return
	}
	if strings.Contains(e.Name, "redirects") {
		e.Type = Redirects
		return
	}
}

func (e *Entry) CalculateDependencies(kp *kvs.KeyPath, sourcesUsed map[string]string, kvs *kvs.KVS) {
	switch e.Type {
	case Library:
		return
	case MediaMaps:
		// lookup ltx counterpart
	case Templates:
		// look at layout manager
		// Note that all langauge-specific libraries used to generate templates are marked as dependencies
	case StyleSheets:
		// dependent on templates? Yes, but templates depend on it
	case Redirects:
		tmp, err := kvsw.NewWrapper(kvs)
		if err != nil {
			return
		}
		depSlice, err := tmp.GetRedirectLibrarySet(strings.TrimPrefix(e.Path, "resources/"))
		if err != nil {
		}
		//TODO: calculate reference

		for _, k := range depSlice {
			if k == "" {
				continue
			}
			e.Dependencies = append(e.Dependencies, k)
		}
	}
}

func (c *Catalog) FilterSubscribed() {
	newEnts := make(map[string]*Entry)
	for key, ent := range c.Entries {
		if ent.Subscribed {
			newEnts[key] = ent
		}
	}
	c.Entries = newEnts
}

func (c *Catalog) PopulateVersionWithHash() {
	var hashBase strings.Builder
	var catakeys []string //hash will change if order changes leading to false positives
	var entnames []string
	var entkeys []string
	//hash all fields of catalog, not just entry hashes, that way if any metadata changes, it is reflected
	meta := c.ToMap()
	ents := c.Entries
	for k, _ := range meta {
		catakeys = append(catakeys, k)
	}
	for k, _ := range ents {
		entnames = append(entnames, k)
	}
	sort.Strings(catakeys)
	sort.Strings(entnames)
	for _, k := range catakeys {
		hashBase.WriteString(meta[k])
	}
	for _, entName := range entnames {
		ent := c.Entries[entName].ToMap()
		if len(entkeys) == 0 {
			for k, _ := range ent {
				entkeys = append(entkeys, k)
			}
			sort.Strings(entkeys)
		}
		for _, k := range entkeys {
			hashBase.WriteString(ent[k])
		}
	}
	hash := sha1.Sum([]byte(hashBase.String()))
	c.Version = fmt.Sprintf("%x", hash)
}
