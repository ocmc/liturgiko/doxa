package catalog

import (
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/sysManager"
	"strings"
)

// this is rather a collection of plugins than a singular one.
// the plugins operate on three levels:
// catalog of catalogs
// catalog
// entry
//
// in the case of catalog of catalogs there are two commands:
// - refresh: retrieves the catalog of catalogs from the remote URL indicated by the source field, and populates sysDB
// - add: adds a third party catalog that will not be updated by refresh (except in the event of a name collision.)
//
// in the case of catalog there are three special commands:
// - refresh: updates that particular catalog on the basis of URL
// - subscribe: subscribes to the entirety of a particular catalog.
// - unsubscribe: everything in the catalog will no longer be updated and is safe to be removes/overwritten
// note that add is not available on this level
//
// in the case of entry there are currently two commands:
// - subscribe: subscribes to that entry and any dependencies
// - unsubscribe: removes entry from subscriptions. contents will no longer be updated
// * refresh is also available, but it still operates on the catalog level
// in the future we should add
// - check: see if this is used by any other libraries as a dependency
// - mend: mark any unsubscribed dependencies as subscribed. This is currently achievable by unsubscribing and resubscribing

type RefreshPlugin struct {
	kvs    *kvs.KVS
	db     *kvs.KVS
	kp     *kvs.KeyPath
	paths  *config.Paths
	catMan *Manager
	level  int //what level are we operating on: 1 = cat of cats, 2 = cat
}

type SubscribePlugin struct {
	kvs    *kvs.KVS
	db     *kvs.KVS
	kp     *kvs.KeyPath
	paths  *config.Paths
	catMan *Manager
}
type UnsubscribePlugin struct {
	kvs    *kvs.KVS
	db     *kvs.KVS
	kp     *kvs.KeyPath
	paths  *config.Paths
	catMan *Manager
}

func (p *RefreshPlugin) OnSetup(kp *kvs.KeyPath, sys, db *kvs.KVS, paths *config.Paths, catMan *Manager) bool {
	p.kp = kp.Copy()
	p.kvs = sys
	p.paths = paths
	p.catMan = catMan
	return true
}

func (p *SubscribePlugin) OnSetup(kp *kvs.KeyPath, sys, db *kvs.KVS, paths *config.Paths, catMan *Manager) bool {
	p.kp = kp.Copy()
	p.kvs = sys
	p.paths = paths
	p.catMan = catMan
	return true
}

func (p *UnsubscribePlugin) OnSetup(kp *kvs.KeyPath, sys, db *kvs.KVS, paths *config.Paths, catMan *Manager) bool {
	p.kp = kp.Copy()
	p.kvs = sys
	p.paths = paths
	p.catMan = catMan
	return true
}

func (p *RefreshPlugin) ValidPath(kp *kvs.KeyPath) bool {
	myKp := kp.Copy()
	if myKp.Dirs.Size() < 1 {
		return false
	}
	if myKp.Dirs.Get(0) == sysManager.Catalogs || myKp.Dirs.Get(0) == "inventories" {
		if myKp.Dirs.Size() == 1 {
			p.level = 1
			return true
		}
		p.level = 2
		return true
	}
	return false
}

func (p *SubscribePlugin) ValidPath(kp *kvs.KeyPath) bool {
	myKp := kp.Copy()
	if myKp.Dirs.Size() < 2 { //catalogs/myCatalog
		return false
	}
	if myKp.Dirs.Get(0) == sysManager.Catalogs || myKp.Dirs.Get(0) == "inventories" {
		return true
	}
	return false
}

func (p *UnsubscribePlugin) ValidPath(kp *kvs.KeyPath) bool {
	myKp := kp.Copy()
	if myKp.Dirs.Size() < 2 { //catalogs/myCatalog
		return false
	}
	if myKp.Dirs.Get(0) == sysManager.Catalogs {
		return true
	}
	return false
}

func (p *RefreshPlugin) Abr() string { return "refresh" }

func (p *SubscribePlugin) Abr() string { return "subscribe" }

func (p *UnsubscribePlugin) Abr() string { return "unsubscribe" }

func (p *RefreshPlugin) DescShort() string {
	return "get the latest version of the catalog from the internet"
}

func (p *SubscribePlugin) DescShort() string { return "mark as subscribed" }

func (p *UnsubscribePlugin) DescShort() string { return "mark as unsubscribed" }

func (p *RefreshPlugin) Execute(args []string) {
	switch p.level {
	case 1:
		p.catMan.PopulateFromList()
	case 2:
		catPath := p.kp.Copy()
		catName := catPath.Dirs.Pop()
		err := p.catMan.RefreshCatalog(catPath, catName)
		if err != nil {
			fmt.Printf("Could not refresh: %v", err)
		}
	}
}

func (p *SubscribePlugin) Execute(args []string) {
	if p.kp.Dirs.Size() < 3 || p.kp.Dirs.Size() == 4 {
		return
	}
	if p.kp.Dirs.Size() == 3 {
		catPat := p.kp.Copy()
		catVer := catPat.Dirs.Pop()
		catName := catPat.Dirs.Pop()
		base := catPat.Copy()
		root := base.Dirs.Get(0)
		isInv := root == "inventories"
		base.Dirs.Clear()
		base.Dirs.Push(root)
		//entries, err := p.catMan.MatchingEntries(base, catName, catVer, ".*")
		cat := p.catMan.GetCatalogFromDb(base, catName, catVer)
		if cat == nil {
			fmt.Printf("Could not find a match for `%s`", args[1])
			return
		}
		for _, ent := range cat.Entries {
			p.catMan.MarkSubscribed(ent.Path, catName, isInv, nil)
		}
		return
	}
	parts := p.kp.Copy()
	cat := parts.Dirs.Get(1)
	lib := parts.Dirs.Slice()[4:]
	p.catMan.MarkSubscribed(strings.Join(lib, "/"), cat, p.kp.Dirs.Get(0) == "inventories", make(map[string]*struct{}))
}

func (p *UnsubscribePlugin) Execute(args []string) {
	if p.kp.Dirs.Size() < 2 || p.kp.Dirs.Size() == 3 {
		return
	}
	if p.kp.Dirs.Size() == 2 {
		catPat := p.kp.Copy()
		catVer := catPat.Dirs.Pop()
		catName := catPat.Dirs.Pop()
		entries, err := p.catMan.MatchingEntries(p.catMan.CatalogCollectionKP(), catName, catVer, ".*")
		if err != nil {
			fmt.Printf("Could not find a match for `%s`: %v", args[1], err)
			return
		}
		for _, ent := range entries {
			//TODO: remove catalogs/catalog/entries from the path
			p.catMan.MarkUnsubscribed(ent.Path)
		}
		return
	}
	parts := p.kp.Copy()
	lib := parts.Dirs.Slice()[3:]
	p.catMan.MarkUnsubscribed(strings.Join(lib, "/"))
}
