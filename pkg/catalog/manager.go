package catalog

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/asaskevich/EventBus"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/dbPaths"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/dvcs"
	"github.com/liturgiko/doxa/pkg/enums/properties"
	"github.com/liturgiko/doxa/pkg/sysManager"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/repos"
	"path"
	"strings"
	"sync"
	"time"
)

const (
	EntryName     = "name"
	Entries       = "entries"
	Subscriptions = "subscriptions"
	Inventory     = "catalog"
	VersionField  = "version"
)

type Manager struct {
	kvs             *kvs.KVS
	currentProject  string
	Bus             EventBus.Bus
	gitReady        bool //is git ready to push?
	GitlabHost      string
	gitman          *dvcs.DVCS
	LastUpdateCheck time.Time
	Catalock        sync.RWMutex
}

func NewManager(sysDb *kvs.KVS, b EventBus.Bus, dvcs *dvcs.DVCS, gitlabHost string) (m *Manager, err error) {
	if sysDb == nil {
		return nil, errors.New("sysDb is nil")
	}
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(sysManager.Selected)
	kp.KeyParts.Push(sysManager.SelectedProject)
	rec, err := sysDb.Db.Get(kp)
	if err != nil {
		return nil, err
	}
	m = new(Manager)
	m.Bus = b
	m.currentProject = rec.Value
	dbPaths.SYS.SetProject(rec.Value)
	m.kvs = sysDb
	m.GitlabHost = gitlabHost
	m.gitman = dvcs
	m.PopulateFromList()
	//make a local catalog if it doesn't exist
	rebuildInventory(m)
	m.Bus.SubscribeAsync("gitlab:tokenSet", func(token string) { m.gitReady = true }, true)
	m.Bus.SubscribeAsync("gitlab:tokenRevoked", func() { m.gitReady = false }, true)
	go m.refreshCatalogs(dbPaths.SYS.CatalogsPath())
	go m.refreshCatalogs(dbPaths.SYS.InventoriesPath())
	return m, nil
}

func (m *Manager) refreshCatalogs(kp *kvs.KeyPath) {
	catNames, err := m.kvs.Db.DirNames(*kp)
	if err != nil {
		return
	}
	for _, cat := range catNames {
		m.RefreshCatalog(kp.Copy(), cat)
	}
}

func (m *Manager) MarkUpdated() {
	subs := m.GetSubscriptionsRedirectPaths()
	if subs == nil {
		return
	}
	safetyNet := make(map[string]*struct{})
	for _, s := range subs {
		upfront := dbPaths.CachedKPs.ExtractCatalogKP(s, m.kvs)
		chopper := upfront.Dirs.Size() - 1
		catalog := s.Dirs.Get(chopper)
		chopper++
		ver := s.Dirs.Get(chopper)
		chopper++
		lib := s.Dirs.SubSlice(chopper+1, s.Dirs.Size())
		m.MarkSubscribedAtVersion(path.Join(lib...), catalog, ver, hasKpDirsPrefix(upfront, dbPaths.SYS.InventoriesPath()), safetyNet)
	}
	go m.refreshCatalogs(dbPaths.SYS.CatalogsPath())
	go m.refreshCatalogs(dbPaths.SYS.InventoriesPath())
}

// RefreshCatalogs refreshes all catalogs and inventories.
// It differs from PopulateFromList() in that the latter exclusively loads approved catalogs
// It differs from MarkUpdated() in that it has no effect on subscriptions
func (m *Manager) RefreshCatalogs() {
	var wg sync.WaitGroup
	go func() {
		m.refreshCatalogs(dbPaths.SYS.CatalogsPath())
		wg.Add(1)
	}()
	go func() {
		m.refreshCatalogs(dbPaths.SYS.InventoriesPath())
		wg.Add(1)
	}()
	wg.Wait()
}

type InventoryCatalogJSON struct {
	Inventory *Catalog
	Catalog   *Catalog
	Versions  map[string]string //version to hash
}

func (i *InventoryCatalogJSON) PopulateVersions(sys *kvs.KVS) *InventoryCatalogJSON {
	i.Versions = make(map[string]string)
	list, _ := sys.Db.DirNames(*dbPaths.SYS.LocalCatalogPath())
	if list == nil {
		return i
	}
	for _, name := range list {
		if name == "latest" {
			continue
		}
		path := dbPaths.SYS.LocalCatalogPath()
		path.Dirs.Push(name)
		path.KeyParts.Push("hash")
		kr, err := sys.Db.Get(path)
		if err != nil {
			continue
		}
		i.Versions[name] = kr.Value
	}
	return i
}

func (i *InventoryCatalogJSON) WriteCatalog(cat *Catalog) *InventoryCatalogJSON {
	i.Catalog = cat
	return i
}

func (i *InventoryCatalogJSON) WriteInventory(inv *Catalog) *InventoryCatalogJSON {
	i.Inventory = inv
	return i
}

func (i *InventoryCatalogJSON) FilterExcluded() *InventoryCatalogJSON {
	//start with inventory
	if i.Inventory != nil {
		for k, ent := range i.Inventory.Entries {
			if ent.Subscribed {
				continue
			}
			delete(i.Inventory.Entries, k)
		}
	}
	if i.Catalog != nil {
		for k, ent := range i.Catalog.Entries {
			if ent.Subscribed {
				continue
			}
			delete(i.Catalog.Entries, k)
		}
	}
	return i
}

func (m *Manager) GetInventory() *InventoryCatalogJSON {
	return new(InventoryCatalogJSON).PopulateVersions(m.kvs).WriteInventory(
		m.GetCatalogFromDb(dbPaths.SYS.CurrentProjectPath(),
			"catalog", "latest")).WriteCatalog(
		m.GetCatalogFromDb(dbPaths.SYS.CurrentProjectPath(),
			"catalog", m.LastPublishedVersion())).FilterExcluded()
}

func (m *Manager) LastPublishedVersion() string {
	kr, err := m.kvs.Db.Get(dbPaths.SYS.LocalCatalogLastPublished())
	if err != nil {
		return ""
	}
	return kr.Value
}

func trimUrlPrefix(str string) string {
	scheme := "https://"
	if str == scheme {
		return ""
	}
	if strings.HasPrefix(str, scheme) {
		return trimUrlPrefix(strings.TrimPrefix(str, scheme))
	}
	return str
}

func rebuildInventory(m *Manager) {
	localCatalogKP := kvs.NewKeyPath()
	localCatalogKP.Dirs.Push(sysManager.Projects)
	localCatalogKP.Dirs.Push(m.currentProject)
	localCatalogKP.Dirs.Push(Inventory)
	localCatalogKP.Dirs.Push("latest")
	if !m.kvs.Db.Exists(localCatalogKP) {
		m.kvs.Db.MakeDir(localCatalogKP)
		urlDbR := kvs.NewDbR()
		urlDbR.KP = localCatalogKP.Copy()
		urlDbR.KP.KeyParts.Push("url")
		urlDbR.Value = fmt.Sprintf("https://%s/%s/catalog.git", trimUrlPrefix(m.GitlabHost), m.currentProject)
		m.kvs.Db.Put(urlDbR)

		urlDbR.KP.KeyParts.Pop()
		urlDbR.KP.KeyParts.Push("name")
		urlDbR.Value = m.currentProject
		m.kvs.Db.Put(urlDbR)
	}
	for k, _ := range new(Catalog).ToMap() {
		localCatalogKP.KeyParts.Push(k)
		if !m.kvs.Db.Exists(localCatalogKP) {
			entRec := kvs.NewDbR()
			entRec.KP = localCatalogKP.Copy()
			m.kvs.Db.Put(entRec)
		}
		localCatalogKP.KeyParts.Pop()
	}
	localCatalogKP.Dirs.Push(Entries)
	if !m.kvs.Db.Exists(localCatalogKP) {
		m.kvs.Db.MakeDir(localCatalogKP)
	}
	localCatalogKP.Dirs.Pop()
}

const CatalogOfCatalogsUrl = "https://gitlab.com/ocmc/doxa/doxa.today/catalogs.git"
const CatalogOfCatalogsFile = "catalogs.json"

func (m *Manager) HasSubscription() bool {
	return m.currentProject != ""
}
func (m *Manager) SubscriptionUpdateAvailable() bool {
	if !m.HasSubscription() {
		return false
	}
	sub := m.GetPrimarySubscriptionCatalog()
	if sub == nil {
		return false
	}
	m.PopulateFromList()
	return sub.Version != m.LatestVersion(sub.Name)
}
func (m *Manager) PopulateFromList() {
	/*cur := m.GetPrimarySubscriptionCatalog()
	curName := ""
	if cur != nil {
		curName = cur.Name
	}*/

	jsonString, err := repos.GetFileContent(CatalogOfCatalogsUrl, CatalogOfCatalogsFile)
	if err != nil {
		return
	}
	var cofCs struct {
		Catalogs CatalogList `json:"catalogs"`
	}
	err = json.Unmarshal(jsonString, &cofCs)
	if cofCs.Catalogs == nil {
		return
	}
	for _, catRef := range cofCs.Catalogs {
		/*curName = */ m.cacheCat(catRef)
	}
	m.LastUpdateCheck = time.Now()
	/*if curName != "" {
		c, err := LoadRemote(cur.URL)
		if err != nil {
			doxlog.Warnf("Could not update custom catalog: %v", err)
			return
		}
		m.SaveCatalog(m.CatalogCollectionKP(), c)
	}*/
}

func (m *Manager) cacheCat(catRef *struct {
	Name string `json:"name"`
	URL  string `json:"url"`
} /*, curName string*/) string {
	c, err := LoadRemote(catRef.URL, false)
	i, err2 := LoadRemote(catRef.URL, true)
	isInv := false
	if err != nil {
		if err2 != nil {
			return ""
		}
		c = i
	}
	c.Name = catRef.Name
	/*if curName == catRef.Name {
		curName = ""
	}*/
	c.URL = catRef.URL
	if c.Version == "" {
		doxlog.Errorf("Catalog at %s has no version. Could not save", c.URL)
	}
	path := dbPaths.SYS.CatalogsPath()
	if isInv {
		path = dbPaths.SYS.InventoriesPath()
	}
	m.SaveCatalog(path, c)
	return catRef.Name //curName
}

func (m *Manager) RefreshCatalog(kp *kvs.KeyPath, cat string) error {
	c := m.GetCatalogFromDb(kp.Copy(), cat, "")
	if c == nil {
		err := fmt.Errorf("nil catalog at %s", kp.Path())
		doxlog.Errorf("Could not refresh catalog: %v", err)
		return err
	}
	c2, err := LoadRemote(c.URL, hasKpDirsPrefix(dbPaths.SYS.InventoriesPath(), kp))
	if err != nil {
		//try it with inventory
		doxlog.Errorf("could not refresh catalog %s: %v", c.Name, err)
		return err
	}
	m.SaveCatalog(kp, c2)
	return nil
}

func (m *Manager) AvailableCatalogNames() []string {
	if !m.kvs.Db.Exists(m.CatalogCollectionKP()) {
		m.PopulateFromList()
	}
	cats, err := m.kvs.Db.DirNames(*m.CatalogCollectionKP())
	if err != nil {
		doxlog.Errorf("Could not get catalogs from catalog of catalogs: %v", err)
		return nil
	}
	return cats
}

func (m *Manager) CatalogExists(catalogName string) bool {
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(sysManager.Catalogs)
	kp.Dirs.Push(catalogName)
	return m.kvs.Db.Exists(kp)
}
func (m *Manager) VersionExists(catalogName, version string) bool {
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(sysManager.Catalogs)
	kp.Dirs.Push(catalogName)
	if version == "" {
		version = "latest"
	}
	kp.Dirs.Push(version)
	return m.kvs.Db.Exists(kp)
}

func (m *Manager) GetPrimarySubscriptionCatalog() *Catalog {
	//load templates
	kp := m.LocalProjectKP().Copy()
	kp.Dirs.Push(Subscriptions)
	kp.KeyParts.Push("templates")
	if m.kvs.Db.Exists(kp) {
		rec, err := m.kvs.Db.Get(kp)
		if err != nil {
			doxlog.Errorf("Could not get templates entry: %v", err)
			return nil
		}
		err = rec.SetRedirectFromValue()
		if err != nil {
			doxlog.Errorf("Could not set redirect from value: %v", err)
			return nil
		}
		catName := rec.Redirect.Dirs.Get(1)
		verName := rec.Redirect.Dirs.Get(2)

		baseN := rec.Redirect.Dirs.Get(0)
		baseKp := kvs.NewKeyPath()
		baseKp.Dirs.Push(baseN)
		return m.GetCatalogFromDb(baseKp, catName, verName)
	} else {
		kp.KeyParts.Pop()
		contents, _, numRecs, err := m.kvs.Db.Keys(kp)
		if err != nil {
			doxlog.Errorf("Could not infer subscription information: %v, using ltx scan approach", err)
			return nil
		}
		if numRecs == 0 {
			//look for ltx. if not present, give up
			kp.KeyParts.Push(config.ResourcesDir)
			kp.KeyParts.Push("ltx")
			if !m.kvs.Db.Exists(kp) {
				// they are not subscribed to anything
				return nil
			}
			contents, _, _, err = m.kvs.Db.Keys(kp)
			if err != nil {
				doxlog.Errorf("Could not infer subscription informarion: %v", err)
				return nil
			}
			rec, err := m.kvs.Db.Get(contents[0])
			if err != nil {
				doxlog.Errorf("could not infer subscription information: %v", err)
				return nil
			}
			err = rec.SetRedirectFromValue()
			if err != nil {
				doxlog.Errorf("could not infer subscription information: %v", err)
				return nil
			}
			catName := rec.Redirect.Dirs.Get(1)
			catVer := rec.Redirect.Dirs.Get(2)
			baseN := rec.Redirect.Dirs.Get(0)
			baseKp := kvs.NewKeyPath()
			baseKp.Dirs.Push(baseN)
			return m.GetCatalogFromDb(baseKp, catName, catVer)
		}
		rec, err := m.kvs.Db.Get(contents[0])
		if err != nil {
			doxlog.Errorf("could not infer subscription information: %v", err)
			return nil
		}
		err = rec.SetRedirectFromValue()
		if err != nil {
			doxlog.Errorf("could not infer subscription information: %v", err)
			return nil
		}
		catName := rec.Redirect.Dirs.Get(1)
		catVer := rec.Redirect.Dirs.Get(2)
		baseN := rec.Redirect.Dirs.Get(0)
		baseKp := kvs.NewKeyPath()
		baseKp.Dirs.Push(baseN)
		return m.GetCatalogFromDb(baseKp, catName, catVer)
	}
}
func (m *Manager) SaveCatalogContents(kp *kvs.KeyPath, catalog *Catalog) {
	var err error //it will all end in tears, I know it
	for k, v := range catalog.ToMap() {
		kp.KeyParts.Push(k)
		rec := kvs.NewDbR()
		rec.KP = kp
		rec.Value = v
		err = m.kvs.Db.Put(rec)
		if err != nil {
			doxlog.Errorf("%v", err)
		}
		kp.KeyParts.Pop()
	}
	kp.Dirs.Push(Entries)
	for _, ent := range catalog.Entries {
		m.AddLibraryToKP(ent.Path, kp)
		m.kvs.Db.MakeDir(kp)
		for k, v := range ent.ToMap() {
			kp.KeyParts.Push(k)
			rec := kvs.NewDbR()
			rec.KP = kp
			rec.Value = v
			err = m.kvs.Db.Put(rec)
			if err != nil {
				doxlog.Errorf("%v", err)
			}
			kp.KeyParts.Pop()
		}
		m.RemoveLibraryFromKP(ent.Path, kp)
	}
}

func (m *Manager) SaveCatalog(kp *kvs.KeyPath, catalog *Catalog) {
	// get catalog path
	// write to that path
	if kp.Path() == m.LocalProjectKP().Path() {
		kp.Dirs.Push(Inventory)
	} else {
		kp.Dirs.Push(catalog.Name)
	}
	kp.Dirs.Push("latest")
	err := m.kvs.Db.MakeDir(kp)
	if err != nil {
		doxlog.Errorf("Could not make directory for catalog '%s': %v", catalog.Name, err)
	}
	m.SaveCatalogContents(kp, catalog)
}

func (m *Manager) ArchiveCurrentVersion(catalog *Catalog, inventory bool) {
	if catalog.Version == "" {
		return
	}
	kp := dbPaths.SYS.CatalogsPath()
	if inventory {
		kp = dbPaths.SYS.InventoriesPath()
	}
	kp.Dirs.Push(catalog.Name)
	if inventory {
		catalog.Version = "using"
	}
	kp.Dirs.Push(catalog.Version)
	err := m.kvs.Db.MakeDir(kp)
	if err != nil {
		doxlog.Errorf("could not archive version '%s' of catalog '%s': %v", catalog.Version, catalog.Name, err)
		return
	}
	m.SaveCatalogContents(kp, catalog)
}

func (m *Manager) CurrentIsArchived(catalog *Catalog, inventory bool) bool {
	kp := dbPaths.SYS.CatalogsPath()
	if inventory {
		kp = dbPaths.SYS.InventoriesPath()
	}
	kp.Dirs.Push(catalog.Name)
	kp.Dirs.Push(catalog.Version)
	return m.kvs.Db.Exists(kp)
}

/*
Root: a
-> b

*/

func (m *Manager) AddLibraryToKP(lib string, kp *kvs.KeyPath) {
	libp := kvs.NewKeyPath()
	libp.ParsePath(lib)
	kp.AppendPath(libp)
}

func (m *Manager) RemoveLibraryFromKP(lib string, kp *kvs.KeyPath) {
	parts := strings.Split(lib, "/")
	for _, _ = range parts {
		kp.Dirs.Pop()
	}
}

func (m *Manager) GetSubscribedVersion(lib string) (string, error) {
	kp := m.LocalProjectKP()
	kp.Dirs.Push(Subscriptions)
	libp := kvs.NewKeyPath()
	err := libp.ParsePath(lib)
	if err != nil {
		return "", fmt.Errorf("could not parse path '%s': %w", lib, err)
	}
	kp.AppendPath(libp)
	target, err := m.kvs.Db.Get(kp)
	if err != nil {
		return "", fmt.Errorf("could not find '%s': %w", lib, err)
	}
	if target.IsRedirect() {
		err = target.SetRedirectFromValue()
		if err != nil {
			return "", fmt.Errorf("could not determine version for '%s': %w", lib, err)
		}
		m.RemoveLibraryFromKP(lib, target.Redirect)
		return target.Redirect.Dirs.Pop(), nil
	}
	return "", fmt.Errorf("entry is not redirect")
}

func (m *Manager) MarkSubscribed(lib, catalog string, inventory bool, internal map[string]*struct{}) {
	m.MarkSubscribedAtVersion(lib, catalog, "latest", inventory, internal)
}
func (m *Manager) MarkSubscribedAtVersion(lib, catalog, version string, inventory bool, internal map[string]*struct{}) {
	combo := fmt.Sprintf("%s-%s", catalog, lib)
	if internal == nil {
		internal = make(map[string]*struct{})
	}
	if internal[lib] != nil {
		if lib != "templates" || internal[combo] != nil {
			return
		}
	}
	internal[lib] = &struct{}{}
	var baseKP *kvs.KeyPath
	var currentCat *Catalog
	if inventory {
		baseKP = dbPaths.SYS.InventoriesPath()
	} else {
		baseKP = dbPaths.SYS.CatalogsPath()
	}
	currentCat = m.GetCatalogFromDb(baseKP.Copy(), catalog, version)
	if currentCat == nil || currentCat.Name == "" {
		if len(internal) > 0 { //we are a dependency
			if inventory {
				baseKP = dbPaths.SYS.CatalogsPath()
			} else {
				baseKP = dbPaths.SYS.InventoriesPath()
			}
			currentCat = m.GetCatalogFromDb(baseKP.Copy(), catalog, version)
			if currentCat == nil || currentCat.Name == "" {
				doxlog.Errorf("Could not find catalog %s at version %s", catalog, version)
				return
			}
			inventory = !inventory
		}
	}
	if !m.CurrentIsArchived(currentCat, inventory) {
		m.ArchiveCurrentVersion(currentCat, inventory)
	}
	ent := m.GetEntryFromDb(baseKP.Copy(), lib, catalog, currentCat.Version)
	if ent == nil {
		if lib == "" {
			return
		}
		ents, err := m.MatchingEntries(baseKP, catalog, currentCat.Version, lib)
		if err != nil {
			doxlog.Errorf("Couldn't find `%s` in catalog `%s`: %v", lib, catalog, err)
			return
		}
		for _, ent := range ents {
			if ent == nil || ent.Path == lib {
				continue
			}
			m.MarkSubscribed(ent.Path, catalog, false, internal)
			m.Bus.Publish("subscriptions:set", catalog+":"+lib)
		}
		return
	}
	if len(ent.Dependencies) > 0 {
		//install dependencies
		//TODO: support cross-catalog dependencies in catalog:library format as per spec
		for _, dep := range ent.Dependencies {
			if strings.Contains(dep, ":") {
				parts := strings.Split(dep, ":")
				if len(parts) > 1 {
					if !m.Subscribed(parts[1]) {
						m.MarkSubscribed(parts[1], parts[0], inventory, internal)
					}
				}
			} else {
				if !m.Subscribed(dep) {
					m.MarkSubscribed(dep, catalog, inventory, internal)
				}
			}
		}
	}
	if lib == config.TemplatesDir {
		config.SM.SetValueByType(properties.SiteBuildTemplatesFallbackEnabled, "true")
		config.SM.SetValueByType(properties.SiteBuildTemplatesFallbackDir, catalog)
	}
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(sysManager.Projects)
	kp.Dirs.Push(m.currentProject)
	kp.Dirs.Push(Subscriptions)
	m.kvs.Db.MakeDir(kp)
	m.AddLibraryToKP(lib, kp)
	kp.KeyParts.Push(kp.Dirs.Pop())
	if lib == "templates" && internal["templates"] != nil {
		kp.KeyParts.Pop()
		kp.KeyParts.Push(combo)
	}
	libPath := m.GetEntryPath(baseKP, lib, catalog, currentCat.Version)
	if libPath == nil {
		return //TODO: error
	}
	libPath.KeyParts.Push(EntryName)
	//err := m.kvs.Db.MakeRedirects(libPath, kp)
	redirect := kvs.NewDbR()
	redirect.KP = kp.Copy()
	redirect.Value = libPath.ToRedirectId()
	err := redirect.SetRedirectFromValue()
	if err != nil {

	}
	err = m.kvs.Db.Put(redirect)

	if err != nil {
		doxlog.Errorf("Could not mark '%s' as subscribed: %v", lib, err)
	}
}

func (m *Manager) MarkUnsubscribed(lib string) {
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(sysManager.Projects)
	kp.Dirs.Push(m.currentProject)
	kp.Dirs.Push(Subscriptions)
	m.AddLibraryToKP(lib, kp)
	kp.KeyParts.Push(kp.Dirs.Pop())
	err := m.kvs.Db.Delete(*kp)
	if err != nil {
		doxlog.Errorf("Could not mark '%s' as unsubscribed: %v", lib, err)
	}
}

// TODO: audit
func (m *Manager) ClearSubscriptions() {
	victim := m.LocalProjectKP().Copy()
	victim.Dirs.Push(Subscriptions)
	m.kvs.Db.Delete(*victim)
	m.kvs.Db.MakeDir(victim)
	config.SM.SetValueByType(properties.SiteBuildTemplatesFallbackEnabled, "false")
	config.SM.SetValueByType(properties.SiteBuildTemplatesFallbackDir, "")
}

func (m *Manager) Subscriptions() (ents []*Entry) {
	matcha := kvs.NewMatcher()
	matcha.Recursive = true
	matcha.KP.Dirs.Push(sysManager.Projects)
	matcha.KP.Dirs.Push(m.currentProject)
	matcha.KP.Dirs.Push(Subscriptions)
	matcha.ValuePattern = matcha.KP.Path()
	// always call UseAsRegEx after setting everything else
	err := matcha.UseAsRegEx()
	if err != nil {
		//TODO handle error
		return nil
	}
	everything, err := m.kvs.Db.GetMatchingIDs(*matcha)
	if err != nil {
		return nil //TODO
	}
	for _, rec := range everything {
		err := rec.SetRedirectFromValue()
		if err != nil {
			doxlog.Warnf("Could not parse redirect `%s`: %v", rec, err)
		}
		entPath := rec.Redirect.Copy()
		if rec.Redirect == nil {
			continue
		}
		catPath := entPath.Copy()
		entPath.KeyParts.Clear()
		entPath.KeyParts.Push("dir")
		entDir, err := m.kvs.Db.Get(entPath)
		if err != nil {
			doxlog.Warnf("could not get '%s': %v", rec.Value, err)
			continue
		}
		m.RemoveLibraryFromKP(entDir.Value, catPath)
		catPath.Dirs.Pop() //remove "entries"
		//catPath.Dirs.Pop() //remove VersionField
		catPath.KeyParts.Clear()
		catPath.KeyParts.Push(EntryName) //TODO: should be catalog name
		catName, err := m.kvs.Db.Get(catPath)
		if err != nil {
			//if there is an error getting the catalog, we want the value to be an empty string
			catName = kvs.NewDbR()
		}
		catPath.KeyParts.Clear()
		catPath.KeyParts.Push(VersionField)
		catVer, err := m.kvs.Db.Get(catPath)
		if err != nil {
			catVer = kvs.NewDbR()
			catVer.Value = "latest"
		}
		baseKpDirName := catPath.Dirs.Get(0) //potential out of bounds handled in Get()
		baseKp := kvs.NewKeyPath()
		baseKp.Dirs.Push(baseKpDirName)
		ents = append(ents, m.GetEntryFromDb(baseKp, entDir.Value, catName.Value, catVer.Value))
	}
	return ents
}

func (m *Manager) Subscribed(lib string) bool {
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(sysManager.Projects)
	kp.Dirs.Push(m.currentProject)
	kp.Dirs.Push(Subscriptions)
	m.AddLibraryToKP(lib, kp)
	return m.kvs.Db.Exists(kp)
}

func (m *Manager) RemoveCatalog(path *kvs.KeyPath, cat, version string) error {
	kp := path.Copy()
	kp.Dirs.Push(cat)
	if version != "" {
		kp.Dirs.Push(version)
	}
	return m.kvs.Db.Delete(*kp)
}

func (m *Manager) LatestVersion(cat string) string {
	latest := m.GetCatalogFromDb(m.CatalogCollectionKP(), cat, "latest")
	return latest.Version
}

// GetEntryPath returns the path to a specific library within a catalog.
// note that the version refers to the catalog version, not the entry version
func (m *Manager) GetEntryPath(kp *kvs.KeyPath, libName, catalog, version string) *kvs.KeyPath {
	if catalog == "" {
		kp = m.GetCatalogPath("none", false)
		kp.Dirs.Pop()
		matcha := kvs.NewMatcher()
		matcha.KP = kp.Copy()
		matcha.ValuePattern = libName
		matcha.Recursive = true
		err := matcha.UseAsRegEx()
		if err != nil {
			doxlog.Errorf("Could not get key path for '%s': %v", libName, err)
		}

		results, err := m.kvs.Db.GetMatchingIDs(*matcha)
		if err != nil {
			doxlog.Errorf("could not find matchin path for '%s', '%v'", libName, err)
		}
		if results == nil || len(results) < 1 {
			return nil //not found
		}
		//TODO: is this best practice?
		return results[0].KP
	}
	if kp.Path() != m.LocalProjectKP().Path() {
		if kp.Dirs.Get(0) != dbPaths.SYS.InventoriesPath().Dirs.Get(0) {
			kp = m.GetCatalogPath(catalog, true)
		} else {
			kp.Dirs.Push(catalog)
		}
	} else {
		kp.Dirs.Push(Inventory)
	}
	if kp == nil {
		return nil
	}
	if version == "" {
		version = "latest"
	}
	kp.Dirs.Push(version)
	kp.Dirs.Push(Entries)
	m.AddLibraryToKP(libName, kp)
	if m.kvs.Db.Exists(kp) {
		return kp
	}
	return nil
}

// GetCatalogPath return a kvs.KeyPath object
func (m *Manager) GetCatalogPath(name string, mustExist bool) (path *kvs.KeyPath) {
	//build the to-catalog portion
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(sysManager.Catalogs)
	kp.Dirs.Push(name)
	if m.kvs.Db.Exists(kp) || !mustExist {
		return kp
	}
	return nil
}

// EntryExists looks for a library in the database.
// if catalog is an empty string it will itterate over all installed catalogs
func (m *Manager) EntryExists(kp *kvs.KeyPath, lib, catalog, version string) bool {
	if m.GetEntryPath(kp, lib, catalog, version) != nil {
		return true
	}
	return false
}

func (m *Manager) AllEntries(kp *kvs.KeyPath) ([]*kvs.DbR, error) {
	matcha := kvs.NewMatcher()
	matcha.KP = kp.Copy()
	matcha.ValuePattern = kp.Path()
	matcha.Recursive = true
	matcha.RegEx = true
	err := matcha.UseAsRegEx()
	if err != nil {
		return nil, err
	}
	return m.kvs.Db.GetMatchingIDs(*matcha)
}

// TODO: leverage dbPaths to avoid matcher
func (m *Manager) MatchingEntries(kp *kvs.KeyPath, catalog, version, pattern string) ([]*Entry, error) {
	topKp := kp.Copy()
	if kp.Path() == m.LocalProjectKP().Path() {
		kp.Dirs.Push(Inventory)
	} else {
		kp = m.GetCatalogPath(catalog, true)
	}
	if kp == nil {
		return nil, errors.New("not found")
	}
	kp.Dirs.Push(Entries)
	matcha := kvs.NewMatcher()
	matcha.KP = kp.Copy()
	matcha.ValuePattern = fmt.Sprintf("%s%s.*%s", kp.Path(), kvs.SegmentDelimiter, pattern)
	matcha.Recursive = true
	matcha.RegEx = true
	err := matcha.UseAsRegEx()
	if err != nil {
		return nil, err
	}
	recs, err := m.kvs.Db.GetMatchingIDs(*matcha)
	if err != nil {
		return nil, err
	}
	var paths []string
	for _, rec := range recs {
		if rec.KP.KeyParts.Pop() == "dir" {
			paths = append(paths, rec.Value)
		}
	}
	var results []*Entry
	for _, path := range paths {
		results = append(results, m.GetEntryFromDb(topKp, path, catalog, version))
	}
	return results, nil
}

// TODO: we can replace this
func (m *Manager) AllEntryNames(catalogPath *kvs.KeyPath) []string {
	results, err := m.AllEntries(catalogPath)
	if err != nil {
		return nil
	}
	resultsMap := map[string]struct{}{}
	for _, ent := range results {
		ent.KP.KeyParts.Clear()
		ent.KP.KeyParts.Push("dir")
		val, err := m.kvs.Db.Get(ent.KP)
		if err != nil || val == nil {
			continue
		}
		resultsMap[val.Value] = struct{}{}
	}
	var resultsSlice []string
	for k, _ := range resultsMap {
		resultsSlice = append(resultsSlice, k)
	}
	return resultsSlice
}

func (m *Manager) GetEntryFromDb(kp *kvs.KeyPath, lib, cat, version string) (ent *Entry) {
	if kp == nil {
		kp = m.CatalogCollectionKP()
	}
	if version == "" {
		version = "latest"
	}
	if !m.EntryExists(kp.Copy(), lib, cat, version) {
		return nil
	}
	if cat == "" {
		tmpKp := m.GetEntryPath(kp, lib, cat, version)
		tmpKp.Dirs.Pop()
		tmpKp.Dirs.Pop()
		cat = tmpKp.Dirs.Pop()
	}
	//kp = m.CatalogCollectionKP()
	//kp.Dirs.Push(cat)
	//kp.Dirs.Push(Entries)
	//m.AddLibraryToKP(lib, kp)
	kp = m.GetEntryPath(kp.Copy(), lib, cat, version)
	if kp == nil {
		return nil
	}
	//convert it to map so we can save time

	winningMap := new(Entry).ToMap()
	for k, _ := range winningMap {
		kp.KeyParts.Push(k)
		dbR, err := m.kvs.Db.Get(kp)
		if err != nil {
			kp.KeyParts.Pop()
			continue
		}
		winningMap[k] = dbR.Value
		kp.KeyParts.Pop()
	}
	return EntryFromMap(winningMap)
}

func (m *Manager) GetEntryFromKP(kp *kvs.KeyPath) (ent *Entry) {
	if kp == nil {
		return nil
	}
	if !m.kvs.Db.Exists(kp) {
		return nil
	}
	kp.KeyParts.Clear()
	winningMap := new(Entry).ToMap()
	for k, _ := range winningMap {
		kp.KeyParts.Push(k)
		dbR, err := m.kvs.Db.Get(kp)
		if err != nil {
			kp.KeyParts.Pop()
			continue
		}
		winningMap[k] = dbR.Value
		kp.KeyParts.Pop()
	}
	return EntryFromMap(winningMap)
}

func (m *Manager) GetCatalogFromDb(kp *kvs.KeyPath, cat, version string) (output *Catalog) {
	if kp == nil {
		kp = m.CatalogCollectionKP()
	}
	locationDir := kp.Copy()
	kp.Dirs.Push(cat)
	//if cat != Inventory {
	if version == "" {
		version = "latest"
	}
	kp.Dirs.Push(version)
	//}
	output = new(Catalog)
	winningMap := output.ToMap()
	for k, _ := range winningMap {
		kp.KeyParts.Push(k)
		dbR, err := m.kvs.Db.Get(kp)
		if err != nil {
			doxlog.Warnf("could not populate field '%s': %v", k, err)
			continue
		}
		winningMap[k] = dbR.Value
		kp.KeyParts.Pop()
	}
	output = CatalogFromMap(winningMap)
	output.Entries = map[string]*Entry{}
	//get database records: matcher
	//kp.Dirs.Push(Entries)
	hitList := m.AllEntryNames(kp)
	for _, ent := range hitList {
		mapEnt := m.GetEntryFromDb(locationDir, ent, cat, version)
		if mapEnt == nil {
			continue
		}
		output.Entries[mapEnt.Path] = mapEnt
	}
	return output
}

func (m *Manager) PreviousSubscribedVersions(catalog string) []string {
	kp := m.CatalogCollectionKP()
	kp.Dirs.Push(catalog)
	versions, err := m.kvs.Db.DirNames(*kp)
	if err != nil {
		doxlog.Errorf("could not get previous versions for catalog %s, %v", catalog, err)
	}
	return versions
}

func (m *Manager) CatalogCollectionKP() *kvs.KeyPath {
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(sysManager.Catalogs)
	return kp
}

func (m *Manager) LocalProjectKP() *kvs.KeyPath {
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(sysManager.Projects)
	kp.Dirs.Push(m.currentProject)
	return kp
}

func (m *Manager) GetCatalogs() []*Catalog {
	var cats []*Catalog
	kp := m.GetCatalogPath("none", false)
	kp.Dirs.Pop()
	names, err := m.kvs.Db.DirNames(*kp)
	if err != nil {

	}
	for _, cat := range names {
		cats = append(cats, m.GetCatalogFromDb(m.CatalogCollectionKP(), cat, ""))
	}
	return cats
}
func (m *Manager) SaveCatalogToSubscribableList(cat *Catalog) {
	m.SaveCatalog(m.CatalogCollectionKP(), cat)
}

func (m *Manager) SaveLocalProjectCatalog(cat *Catalog) {
	m.SaveCatalog(m.LocalProjectKP(), cat)
}

func (m *Manager) ListIncludedInLocalCatalog() []string {
	kp := m.LocalProjectKP().Copy()
	var included []string
	kp.Dirs.Push(Inventory)
	kp.Dirs.Push(Entries)
	raw, err := m.AllEntries(kp)
	if err != nil {
		return []string{}
	}
	kp.KeyParts.Push("subscribed")
	for _, lib := range raw {
		if lib.KP.Copy().KeyParts.Pop() != "subscribed" {
			continue
		}
		if lib.Value == "true" {
			pathKP := lib.KP.Copy()
			pathKP.KeyParts.Pop()
			pathKP.KeyParts.Push("dir")
			tmp, err := m.kvs.Db.Get(pathKP)
			if err != nil {
				continue
			}
			included = append(included, tmp.Value)
		}
	}
	return included
}

func (m *Manager) PublishCatalog(catalogPath *kvs.KeyPath, exportPath string, isInventory bool) error {

	cat := m.GetCatalogFromDb(catalogPath, Inventory, "latest")
	if cat == nil {
		return fmt.Errorf("no catalog to publish!")
	}
	cat.FilterSubscribed()

	// Dependency calculation!!!
	var externalSources map[string]string
	//itterate through each of them
	for _, lib := range cat.Entries {
		if !lib.Subscribed {
			continue
		}
		lib.TypeFromName()
		lib.CalculateDependencies(catalogPath, externalSources, m.kvs)
	}
	bundle := m.GetInventory()
	var raw []byte
	var err error
	if !isInventory {
		raw, err = cat.JsonBytes() //json.Marshal(m.GetInventory())
		raw2, _ := json.Marshal(bundle.Versions)
		ltfile.WriteFile(exportPath+".versions", string(raw2))
	} else {
		raw, err = bundle.Inventory.JsonBytes()
	}

	if err != nil {
		return fmt.Errorf("could not export catalog: %v", err)
	}
	err = ltfile.WriteFile(exportPath, string(raw))
	if !m.gitReady {
		m.Bus.Publish("gitlab:tokenNeeded")
		m.Bus.SubscribeAsync("gitlab:tokenSet",
			m.pushCatalog, false)
		return nil
	}
	if m == nil {
		//this should never happen under any circumstance, but somehow it did, so just in case
		return fmt.Errorf("method called on nil object")
	}
	if m.gitman == nil {
		return fmt.Errorf("git manager is nil")
	}
	if m.gitman.Gitlab == nil {
		return fmt.Errorf("git manager has no gitlab object")
	}
	m.pushCatalog(m.gitman.Gitlab.RemoteToken, cat.Version)
	return nil
}

func (m *Manager) pushCatalog(token, version string) {
	hash, err := m.gitman.Git.CommitAllWithHash(config.DoxaPaths.CatalogPath, "catalog published using doxa.tools")
	if err != nil {
		doxlog.Errorf("could not commit %s: %v", config.CatalogDir, err)
		return
	}
	m.CacheCatalogRelease(version, hash)
	err = m.gitman.Git.Push(config.DoxaPaths.CatalogPath, "doxatools", token)
	if err != nil {
		doxlog.Errorf("Could not publish catalog (push failed): %v", err)
		return
	}
	m.Bus.Unsubscribe("gitlab:tokenSet", m.pushCatalog)
	go m.Bus.Publish("catalog:pushed")
}

func (m *Manager) CacheCatalogRelease(version, hash string) {
	//read version
	start := dbPaths.SYS.LocalCatalogPath()
	start.Dirs.Push(version)
	start.KeyParts.Push("hash")
	kr := kvs.NewDbR()
	kr.KP = start
	kr.Value = hash
	m.kvs.Db.Put(kr)
	dr := kvs.NewDbR()
	dr.KP = dbPaths.SYS.LocalCatalogLastPublished()
	dr.Value = version
	m.kvs.Db.Put(dr)
}

func _printCatalogPushed() {
	fmt.Println("Catalog successfully published")
}
func (m *Manager) StdoutAll() {
	m.Bus.SubscribeOnce("catalog:pushed", _printCatalogPushed)
}

func (m *Manager) MuteAll() {
	m.Bus.Unsubscribe("catalog:pushed", _printCatalogPushed)
}

func (m *Manager) UpdateVersionHash(lib, hash string) error {
	kp := m.LocalProjectKP()
	kp.Dirs.Push(Inventory)
	kp.Dirs.Push("latest")
	kp.Dirs.Push(Entries)
	m.AddLibraryToKP(lib, kp)
	kp.KeyParts.Push(VersionField)
	kr := kvs.NewDbR()
	kr.KP = kp.Copy()
	kr.Value = hash
	return m.kvs.Db.Put(kr)
}

func (m *Manager) UpdateInventoryVersionHash() error {
	cat := m.GetCatalogFromDb(m.LocalProjectKP(), Inventory, "")
	if cat == nil {
		return fmt.Errorf("could not get inventory from sysDb")
	}
	kp := m.LocalProjectKP()
	kp.Dirs.Push("latest")
	kp.Dirs.Push(Inventory)
	kp.KeyParts.Push(VersionField)
	kr := kvs.NewDbR()
	kr.KP = kp.Copy()
	cat.PopulateVersionWithHash()
	kr.Value = cat.Version
	return m.kvs.Db.Put(kr)
}

func (m *Manager) RebuildInventory(liturgical *kvs.KVS) error {
	omit := m.Subscriptions()
	omitMap := make(map[string]bool)
	for _, ent := range omit {
		if ent == nil || ent.Name == "" {
			doxlog.Errorf("nil or empty subscription name")
			continue
		}
		omitMap[ent.Name] = true
	}
	/*cat := m.GetPrimarySubscriptionCatalog()
	if cat != nil {
		omitMap[cat.Name] = true
	}*/

	domesticCat := m.GetCatalogFromDb(m.LocalProjectKP(), Inventory, "")
	if domesticCat == nil {
		domesticCat = new(Catalog)
		domesticCat.Entries = make(map[string]*Entry)
	}
	now := time.Now()
	// get all ltx and media
	kp := kvs.NewKeyPath()
	kp.Dirs.Push(config.LtxDir)
	ltx, err := liturgical.Db.DirNames(*kp)
	if err == nil {
		for _, lib := range ltx {
			if omitMap[lib] {
				continue
			}
			libP := path.Join(config.ResourcesDir, config.LtxDir, lib)
			if ent, alreadyDone := domesticCat.Entries[libP]; alreadyDone {
				ent.Updated = &now
				continue
			}
			domesticCat.Entries[libP] = &Entry{
				Name:          lib,
				Path:          libP,
				Url:           fmt.Sprintf("https://%s/%s/%s.git", trimUrlPrefix(m.GitlabHost), m.currentProject, libP),
				License:       "",
				Desc:          "",
				Created:       &now,
				Updated:       nil,
				UpdateMessage: "",
				Maintainer:    "",
				Version:       "",
				Type:          Library,
				Dependencies:  nil,
				Subscribed:    true, //if marked subscribed it is included
			}
		}
	}
	kp.Dirs.Pop()
	kp.Dirs.Push(config.MediaDir)
	media, err := liturgical.Db.DirNames(*kp)
	if err == nil {
		for _, lib := range media {
			if omitMap[lib] {
				continue
			}
			libP := path.Join(config.ResourcesDir, config.MediaDir, lib)
			if ent, alreadyDone := domesticCat.Entries[libP]; alreadyDone {
				ent.Updated = &now
				continue
			}
			domesticCat.Entries[libP] = &Entry{
				Name:          lib,
				Path:          libP,
				Url:           fmt.Sprintf("https://%s/%s/%s.git", m.GitlabHost, m.currentProject, libP),
				License:       "",
				Desc:          "",
				Created:       &now,
				Updated:       nil,
				UpdateMessage: "",
				Maintainer:    "",
				Version:       "",
				Type:          Library,
				Dependencies:  nil,
				Subscribed:    true, //if marked subscribed it is included
			}
		}
	}
	if _, existeth := domesticCat.Entries[config.TemplatesDir]; !existeth {
		domesticCat.Entries[config.TemplatesDir] = &Entry{
			Name:          config.TemplatesDir,
			Path:          config.TemplatesDir,
			Url:           fmt.Sprintf("https://%s/%s/%s.git", m.GitlabHost, m.currentProject, config.TemplatesDir),
			License:       "",
			Desc:          "",
			Created:       &now,
			Updated:       nil,
			UpdateMessage: "",
			Maintainer:    "",
			Version:       "",
			Type:          Templates,
			Dependencies:  nil,  //TODO: populate
			Subscribed:    true, //if marked subscribed it is included
		}
	} else {
		domesticCat.Entries[config.TemplatesDir].Updated = &now
	}
	if _, existeth := domesticCat.Entries[config.AssetsDir]; !existeth {
		domesticCat.Entries[config.AssetsDir] = &Entry{
			Name:          config.AssetsDir,
			Path:          config.AssetsDir,
			Url:           fmt.Sprintf("https://%s/%s/%s.git", m.GitlabHost, m.currentProject, config.AssetsDir),
			License:       "",
			Desc:          "",
			Created:       &now,
			Updated:       nil,
			UpdateMessage: "",
			Maintainer:    "",
			Version:       "",
			Type:          StyleSheets,
			Dependencies:  nil,  //TODO: populate
			Subscribed:    true, //if marked subscribed it is included
		}
	} else {
		domesticCat.Entries[config.AssetsDir].Updated = &now
	}

	libP := path.Join(config.ResourcesDir, config.ConfigsDir)
	if _, existeth := domesticCat.Entries[libP]; !existeth {
		domesticCat.Entries[libP] = &Entry{
			Name:          config.ConfigsDir,
			Path:          libP,
			Url:           fmt.Sprintf("https://%s/%s/%s.git", m.GitlabHost, m.currentProject, libP),
			License:       "",
			Desc:          "",
			Created:       &now,
			Updated:       nil,
			UpdateMessage: "",
			Maintainer:    "",
			Version:       "",
			Type:          Library,
			Dependencies:  nil,
			Subscribed:    true, //if marked subscribed it is included
		}
	} else {
		domesticCat.Entries[libP].Updated = &now
	}
	domesticCat.Name = m.currentProject
	if domesticCat.URL == "" {
		domesticCat.URL = fmt.Sprintf("https://%s/%s/catalog.git", m.GitlabHost, m.currentProject)
	}
	m.SaveLocalProjectCatalog(domesticCat)
	return nil
}

func (m *Manager) GetInventoryNames() ([]string, error) {
	return m.kvs.Db.DirNames(*dbPaths.SYS.InventoriesPath())
}

func (m *Manager) GetAvailCatalogNames() ([]string, error) {
	return m.kvs.Db.DirNames(*dbPaths.SYS.CatalogsPath())
}

func (m *Manager) GetSubscriptionsInCatalog(cat string, isInv bool) (ents []*Entry) {
	matcha := kvs.NewMatcher()
	matcha.Recursive = true
	matcha.KP = dbPaths.SYS.SubscriptionPath()
	matcha.ValuePattern = matcha.KP.Path()
	// always call UseAsRegEx after setting everything else
	err := matcha.UseAsRegEx()
	if err != nil {
		doxlog.Errorf("could not convert path %s to regex", matcha.ValuePattern)
		return nil
	}
	everything, err := m.kvs.Db.GetMatchingIDs(*matcha)
	if err != nil {
		return nil //TODO
	}
	for _, rec := range everything {
		err := rec.SetRedirectFromValue()
		if rec.Redirect == nil || !strings.Contains(rec.Redirect.Path(), cat) {
			continue
		}
		if rec.Redirect.Dirs.Get(0) == dbPaths.SYS.InventoriesPath().Dirs.Get(0) {
			if !isInv {
				continue
			}
		} else {
			if isInv {
				continue
			}
		}
		if err != nil {
			doxlog.Warnf("Could not parse redirect `%s`: %v", rec.Value, err)
		}
		entPath := rec.Redirect.Copy()
		if rec.Redirect == nil {
			continue
		}
		catPath := entPath.Copy()
		entPath.KeyParts.Clear()
		entPath.KeyParts.Push("dir")
		entDir, err := m.kvs.Db.Get(entPath)
		if err != nil {
			doxlog.Warnf("could not get '%s': %v", rec.Value, err)
			continue
		}
		m.RemoveLibraryFromKP(entDir.Value, catPath)
		catPath.Dirs.Pop() //remove "entries"
		//catPath.Dirs.Pop() //remove VersionField
		catPath.KeyParts.Clear()
		catPath.KeyParts.Push(EntryName) //TODO: should be catalog name
		catName, err := m.kvs.Db.Get(catPath)
		if err != nil {
			//if there is an error getting the catalog, we want the value to be an empty string
			catName = kvs.NewDbR()
		}
		catPath.KeyParts.Clear()
		catPath.KeyParts.Push(VersionField)
		catVer, err := m.kvs.Db.Get(catPath)
		if err != nil {
			catVer = kvs.NewDbR()
			catVer.Value = "latest"
		}
		rootKp := dbPaths.SYS.CatalogsPath()
		if isInv {
			rootKp = dbPaths.SYS.InventoriesPath()
		}
		ents = append(ents, m.GetEntryFromDb(rootKp, entDir.Value, catName.Value, catVer.Value))
	}
	return ents
}

func (m *Manager) SubscriptionKPs() []*kvs.KeyPath {
	kp := dbPaths.SYS.SubscriptionPath()
	tldirs, _, _, err := m.kvs.Db.Keys(kp)
	if err != nil {
		tldirs = make([]*kvs.KeyPath, 0)
	}
	templatesKP := kp.Copy()
	templatesKP.KeyParts.Push(config.TemplatesDir)
	assetsKp := kp.Copy()
	assetsKp.KeyParts.Push(config.AssetsDir)

	kp.Dirs.Push(config.ResourcesDir)
	configs := kp.Copy()
	configs.KeyParts.Push(config.ConfigsDir)
	ltx := kp.Copy()
	ltx.Dirs.Push(config.LtxDir)
	media := kp.Copy()
	media.Dirs.Push(config.MediaDir)

	ltxLibs, _, _, _ := m.kvs.Db.Keys(ltx)
	if ltxLibs == nil {
		ltxLibs = make([]*kvs.KeyPath, 0)
	}

	mediaLibs, _, _, _ := m.kvs.Db.Keys(media)
	if mediaLibs == nil {
		mediaLibs = make([]*kvs.KeyPath, 0)
	}

	slice := []*kvs.KeyPath{templatesKP, assetsKp, configs}
	slice = append(slice, ltxLibs...)
	slice = append(slice, mediaLibs...)
	for _, candidate := range tldirs {
		if strings.HasSuffix(candidate.KeyParts.Last(), config.TemplatesDir) {
			slice = append(slice, candidate)
		}
	}
	return slice
}

func (m *Manager) CatalogsWithSubscriptions(prefix *kvs.KeyPath) (cats []*Catalog) {
	subs := m.SubscriptionKPs()
	catMap := make(map[string]struct{})
	for _, sub := range subs {
		kr, err := m.kvs.Db.Get(sub)
		if err != nil {
			continue
		}
		kr.SetRedirectFromValue()
		if !strings.HasPrefix(kr.Redirect.Path(), prefix.Path()) {
			continue
		}
		if kr.Redirect.Dirs.Size() < prefix.Dirs.Size()+2 {
			continue
		}
		cat := kr.Redirect.Dirs.Get(prefix.Dirs.Size())
		ver := kr.Redirect.Dirs.Get(prefix.Dirs.Size() + 1)
		if _, exists := catMap[cat]; !exists {
			catMap[cat] = struct{}{}
			cats = append(cats, m.GetCatalogFromDb(prefix.Copy(), cat, ver))
		}
	}
	return cats
}

func (m *Manager) GetSubscriptionsRedirectPaths() []*kvs.KeyPath {
	subs := m.SubscriptionKPs()
	solved := make([]*kvs.KeyPath, 0, len(subs))
	for _, sub := range subs {
		kr, err := m.kvs.Db.Get(sub)
		if err != nil {
			continue
		}
		err = kr.SetRedirectFromValue()
		if err != nil {
			continue
		}
		solved = append(solved, kr.Redirect)
	}
	return solved
}

// SubscribeAll marks all entries in the specified catalog as subscribed
func (m *Manager) SubscribeAll(kp *kvs.KeyPath, catalog, version string) {
	// Pre-populate maps to optimize dependency handling
	internal := make(map[string]*struct{})
	dependenciesMap := make(map[string][]string) // map[libraryPath][]dependencyPaths

	// Get all entries from catalog
	cat := m.GetCatalogFromDb(kp, catalog, version)
	isInventory := hasKpDirsPrefix(kp, dbPaths.SYS.InventoriesPath())
	if cat == nil || cat.Name == "" {
		isInventory = !isInventory
		otherPath := dbPaths.SYS.InventoriesPath()
		if !isInventory {
			otherPath = dbPaths.SYS.CatalogsPath()
		}
		cat = m.GetCatalogFromDb(otherPath, catalog, version)
		if cat == nil || cat.Name == "" {
			return //TODO error
		}
	}

	// First pass: mark entries as subscribed and collect dependencies
	for _, entry := range cat.Entries {
		combo := fmt.Sprintf("%s-%s", catalog, entry.Path)
		if internal[entry.Path] != nil {
			if entry.Path != "templates" || internal[combo] != nil {
				continue
			}
		}
		internal[entry.Path] = &struct{}{}

		// Get the entry path and create subscription
		kp := kvs.NewKeyPath()
		kp.Dirs.Push(sysManager.Projects)
		kp.Dirs.Push(m.currentProject)
		kp.Dirs.Push(Subscriptions)
		m.AddLibraryToKP(entry.Path, kp)
		kp.KeyParts.Push(kp.Dirs.Pop())
		if entry.Path == "templates" && internal["templates"] != nil {
			kp.KeyParts.Pop()
			kp.KeyParts.Push(combo)
		}

		// Get the library path
		var baseKP *kvs.KeyPath
		if isInventory {
			baseKP = dbPaths.SYS.InventoriesPath()
		} else {
			baseKP = dbPaths.SYS.CatalogsPath()
		}
		libPath := m.GetEntryPath(baseKP, entry.Path, catalog, version)
		if libPath == nil {
			continue
		}
		libPath.KeyParts.Push(EntryName)

		// Create and store redirect
		redirect := kvs.NewDbR()
		redirect.KP = kp.Copy()
		redirect.Value = libPath.ToRedirectId()
		err := redirect.SetRedirectFromValue()
		if err != nil {
			continue
		}
		err = m.kvs.Db.Put(redirect)
		if err != nil {
			continue
		}

		// Store dependencies for later processing
		if len(entry.Dependencies) > 0 {
			dependenciesMap[entry.Path] = entry.Dependencies
		}

		// Handle templates specifically
		if entry.Path == config.TemplatesDir {
			config.SM.SetValueByType(properties.SiteBuildTemplatesFallbackEnabled, "true")
			config.SM.SetValueByType(properties.SiteBuildTemplatesFallbackDir, catalog)
		}
	}

	// Second pass: handle dependencies
	for _, deps := range dependenciesMap {
		for _, dep := range deps {
			if strings.Contains(dep, ":") {
				parts := strings.Split(dep, ":")
				if len(parts) > 1 {
					depCatalog, depLib := parts[0], parts[1]
					if internal[depLib] == nil && !m.Subscribed(depLib) {
						m.MarkSubscribed(depLib, depCatalog, isInventory, internal)
					}
				}
			} else {
				if internal[dep] == nil && !m.Subscribed(dep) {
					m.MarkSubscribed(dep, catalog, isInventory, internal)
				}
			}
		}
	}
}

// UnsubscribeAll unsubscribes from all entries in the specified catalog
func (m *Manager) UnsubscribeAll(kp *kvs.KeyPath, catalog string) {
	// Get all subscription redirects
	redirects := m.GetSubscriptionsRedirectPaths()
	if redirects == nil {
		return
	}

	// Build catalog path to match against
	catalogKP := kp.Copy()
	catalogKP.Dirs.Push(catalog)

	// Unsubscribe matching entries
	for _, redirect := range redirects {
		// Skip if redirect doesn't match catalog path
		if !hasKpDirsPrefix(redirect, catalogKP) {
			continue
		}
		// Get entry path from redirect
		redirect.KeyParts.Clear()
		redirect.KeyParts.Push("dir")
		entryRec, err := m.kvs.Db.Get(redirect)
		if err != nil {
			continue
		}
		m.MarkUnsubscribed(entryRec.Value)
	}
}

func (m *Manager) GetSubscriptionFromCatalog(kp *kvs.KeyPath, cat, ver string) []*Entry {
	allSubs := m.GetSubscriptionsRedirectPaths()
	solved := make([]*Entry, 0, len(allSubs))
	wkp := kp.Copy()
	wkp.Dirs.Push(cat)
	//kp.Dirs.Push(ver)
	for _, contender := range allSubs {
		contender.KeyParts.Clear()
		if hasKpDirsPrefix(contender, kp) {
			if contender.Dirs.Size() > wkp.Dirs.Size() {
				contender.Dirs.Set(wkp.Dirs.Size(), ver)
			}
			legitimate := m.GetEntryFromKP(contender)
			if legitimate == nil {
				continue
			}
			solved = append(solved, legitimate)
		}
	}
	return solved
}

// this could be part of the KeyPath library
func hasKpDirsPrefix(kp, suffix *kvs.KeyPath) bool {
	//first things first
	if suffix.Dirs.Size() > kp.Dirs.Size() {
		return false
	}
	for i := 0; i < suffix.Dirs.Size(); i++ {
		if kp.Dirs.Get(i) != suffix.Dirs.Get(i) {
			return false
		}
	}
	return true
}

// GetUpdates returns a slice of catalogs, but unlike normal catalogs, these only contain entries that need updating
func (m *Manager) GetUpdates() []*Catalog {
	subKps := m.GetSubscriptionsRedirectPaths()
	if subKps == nil {
		return nil
	}
	catMap := make(map[string]*Catalog)
	for _, kp := range subKps {
		if kp == nil {
			continue
		}
		installed := m.GetEntryFromKP(kp.Copy())
		if installed == nil {
			continue
		}
		latestKP := dbPaths.CachedKPs.ExtractCatalogKP(kp, m.kvs)
		if latestKP == nil {
			continue
		}
		mapkey := latestKP.Path()
		latestKP.Dirs.Push("latest")
		for _, dir := range kp.Dirs.SubSlice(latestKP.Dirs.Size(), kp.Dirs.Size()) {
			latestKP.Dirs.Push(dir)
		}
		latest := m.GetEntryFromKP(latestKP)
		if latest == nil {
			continue
		}
		if installed.Version == latest.Version {
			continue
		}
		if _, exists := catMap[mapkey]; !exists {
			catMap[mapkey] = new(Catalog)
			catMap[mapkey].Name = mapkey
			catMap[mapkey].Entries = make(map[string]*Entry)
		}
		catMap[mapkey].Entries[installed.Path] = latest //installed
	}
	catSlice := make([]*Catalog, len(catMap))
	i := 0
	for _, c := range catMap {
		catSlice[i] = c
		i++
	}
	return catSlice
}
