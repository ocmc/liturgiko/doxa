package catalog

import (
	"errors"
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"net/url"
	"strings"
	"time"
)

type EdPlugin struct {
	kvs         *kvs.KVS
	catMan      *Manager
	db          *kvs.KVS
	catalogMode bool
}

func dirFromKp(kp *kvs.KeyPath) string {
	entriesIndex := -1
	for i := 0; i < kp.Dirs.Size(); i++ {
		if kp.Dirs.Get(i) == "entries" {
			entriesIndex = i
			break
		}
	}

	// If "entries" is found, return the substring from "entries" to the end
	if entriesIndex != -1 {
		return strings.Join(kp.Dirs.SubSlice(entriesIndex+1, kp.Dirs.Size()), "/")
	}
	return kp.Path()
}

func (ed *EdPlugin) Get(kp *kvs.KeyPath) (*kvs.DbR, error) {
	if ed.kvs == nil {
		return nil, errors.New("EdPlugin has not been initialized. use the SetKVS method to initialize")
	}
	if kp.Dirs.Size() == 1 {
		if kp.Dirs.Copy().Pop() == "catalogs" {
			if kp.KeyParts.Size() == 0 {
				faux := kvs.NewDbR()
				faux.KP = kp.Copy()
				faux.Value = ""
				return faux, nil
			}
		}
	}
	//see if we are in catalog
	if kp.Copy().Dirs.Pop() == "catalog" {
		//load their subscriptions, as they cannot be listed in catalog
		omit := ed.catMan.Subscriptions()
		omitMap := make(map[string]struct{})
		for _, ent := range omit {
			omitMap[ent.Name] = struct{}{}
		}

		//load the list of libraries in
		avail := []string{"templates", "assets"}
		//fetch the libraries
		libKp := kvs.NewKeyPath()
		libKp.Dirs.Push("ltx")
		ltxOpts, err := ed.db.Db.DirNames(*libKp)
		if err != nil {
			return nil, err
		}
		libKp.Dirs.Pop()
		libKp.Dirs.Push("media")
		mediaOpts, err := ed.db.Db.DirNames(*libKp)
		if err != nil {
			return nil, err
		}
		for _, lib := range ltxOpts {
			if _, pleaseOmit := omitMap[lib]; !pleaseOmit {
				avail = append(avail, fmt.Sprintf("%s%s%s%s%s", "resources", kvs.SegmentDelimiter, "ltx", kvs.SegmentDelimiter, lib))
			}
		}
		for _, lib := range mediaOpts {
			if _, pleaseOmit := omitMap[lib]; !pleaseOmit {
				avail = append(avail, fmt.Sprintf("%s%s%s%s%s", "resources", kvs.SegmentDelimiter, "media", kvs.SegmentDelimiter, lib))
			}
		}
		avail = append(avail, fmt.Sprintf("%s%s%s", "resources", kvs.SegmentDelimiter, "configs"))

		finalCut := []string{"INCLUDED:"}
		tmpMap := make(map[string]struct{})
		for _, inc := range ed.catMan.ListIncludedInLocalCatalog() {
			tmpMap[inc] = struct{}{}
			finalCut = append(finalCut, inc)
		}
		finalCut = append(finalCut, "\nEXCLUDED:")

		for _, ent := range avail {
			if _, exists := tmpMap[ent]; exists {
				continue
			}
			finalCut = append(finalCut, ent)
		}

		faux := kvs.NewDbR()
		faux.Value = strings.Join(finalCut, "\n")
		faux.KP = kp
		return faux, nil

	}
	//make a faux record
	faux := kvs.NewDbR()
	winningMap := new(Entry).ToMap()
	for k, _ := range winningMap {
		kp.KeyParts.Push(k)
		if ed.kvs.Db.Exists(kp) {
			dbR, err := ed.kvs.Db.Get(kp)
			if err != nil {
				continue
			}
			winningMap[k] = dbR.Value
		} else {
			// context sensitive suggestions
			switch k {
			case "name":
				winningMap[k] = kp.Copy().Dirs.Pop()
			case "path", "dir":
				winningMap[k] = dirFromKp(kp.Copy())
			case "url":
				urlPath, err := url.JoinPath(ed.catMan.currentProject, dirFromKp(kp.Copy()))
				if err != nil {
					winningMap[k] = ""
					continue
				}
				suggestURL := fmt.Sprintf("https://gitlab.com/%s.git", urlPath)
				winningMap[k] = suggestURL
			default:
				winningMap[k] = ""
			}
		}
		kp.KeyParts.Pop()
	}
	ent := EntryFromMap(winningMap)
	faux.Value = ent.String()
	faux.KP = kp.Copy()
	return faux, nil
}

// ValidPath indicates that the plugin applies in the given path
func (ed *EdPlugin) ValidPath(kp *kvs.KeyPath) bool {
	tmpKP := kp.Copy()
	if tmpKP.Dirs.Size() == 1 {
		if tmpKP.Dirs.Copy().Pop() == "catalogs" {
			if tmpKP.KeyParts.Size() == 0 {
				return true
			}
		}
	}
	if tmpKP.KeyParts.Size() > 0 {
		return false
	}
	if tmpKP.Dirs.Pop() == "catalog" {
		return true
	}
	if tmpKP.Dirs.Pop() == "entries" {
		return true
	}
	//temporary workaround for resources/ltx/lib
	tmpKP.Dirs.Pop()
	if tmpKP.Dirs.Pop() == "entries" {
		return true
	}
	return false
}

// AfterEdit makes any modifications to input string and returns resulting output string, and wether the record can be saved
func (ed *EdPlugin) AfterEdit(str string) (string, bool) {
	return str, true
}

// Put is used instead of kvs.Put
func (ed *EdPlugin) Put(rec *kvs.DbR) error {
	if rec.KP.Dirs.Size() == 1 {
		//TODO: inventory support
		if rec.KP.Dirs.Copy().Pop() == "catalogs" {
			if rec.KP.KeyParts.Size() == 0 {
				// install the catalog
				rec.Value = strings.TrimSuffix(rec.Value, "\n")
				rec.Value = strings.TrimSuffix(rec.Value, " ")
				c, err := LoadRemote(rec.Value, false)
				if err != nil {
					fmt.Printf("Could not load catalog. %v\n", err)
					return err
				}
				c.Name = strings.TrimSuffix(strings.TrimSuffix(strings.TrimPrefix(c.Name, "https://gitlab.com/"), ".git"), "/catalog")
				c.URL = rec.Value
				ed.catMan.SaveCatalog(ed.catMan.CatalogCollectionKP(), c)
				return nil
			}
		}
	}
	if rec.KP.Copy().Dirs.Pop() == "catalog" {
		// grab list of currently included
		previousList := ed.catMan.ListIncludedInLocalCatalog()
		removeMap := make(map[string]bool)
		for _, ent := range previousList {
			removeMap[ent] = true
		}
		included := strings.Split(rec.Value, "\n")
		//for each mkdir
		for _, lib := range included {
			if lib == "INCLUDED:" || lib == "" {
				continue
			}
			if lib == "EXCLUDED:" {
				break
			}
			if _, doomedToExile := removeMap[lib]; doomedToExile {
				removeMap[lib] = false
			}
			parentKP := rec.KP.Copy()
			catalogName := parentKP.Dirs.Pop()
			ent := ed.catMan.GetEntryFromDb(parentKP, lib, catalogName, "")
			if ent == nil {
				ent = new(Entry)
			}
			base := kvs.NewKeyPath()
			base.ParsePath(fmt.Sprintf("%s%sentries%s%s", rec.KP.Path(), kvs.SegmentDelimiter, kvs.SegmentDelimiter, lib))
			ed.kvs.Db.MakeDir(base)
			entDbR := kvs.NewDbR()
			entDbR.KP = base
			entMap := ent.ToMap()
			for k, _ := range entMap {
				if entMap[k] != "" {
					if k != "subscribed" {
						continue
					}
				}
				switch k {
				case "path", "dir":
					entDbR.Value = dirFromKp(entDbR.KP.Copy())
				case "name":
					entDbR.Value = entDbR.KP.Copy().Dirs.Pop()
				case "url":
					urlPath, _ := url.JoinPath(ed.catMan.currentProject, dirFromKp(entDbR.KP.Copy()))
					entDbR.Value = fmt.Sprintf("https://gitlab.com/%s.git", urlPath)
				case "created":
					entDbR.Value = time.Now().String()
				case "subscribed":
					entDbR.Value = "true"
				default:
					continue
				}
				entDbR.KP.KeyParts.Push(k)
				ed.kvs.Db.Put(entDbR)
				entDbR.KP.KeyParts.Pop()
			}
		}
		for k, exile := range removeMap {
			if exile {
				record := kvs.NewDbR()
				record.KP = rec.KP.Copy()
				record.KP.Dirs.Push(Entries)
				xtra := strings.Split(k, kvs.SegmentDelimiter)
				for _, dir := range xtra {
					record.KP.Dirs.Push(dir)
				}
				record.KP.KeyParts.Push("subscribed")
				record.Value = "false"
				ed.kvs.Db.Put(record)
			}
		}
		//for each fill values
		return nil
	}
	//the value of rec should be a string
	ent := EntryFromString(rec.Value)
	if ent == nil {
		return errors.New("could not save, invalid format")
	}

	for k, v := range ent.ToMap() {
		rec.KP.KeyParts.Push(k)
		if !ed.kvs.Db.Exists(rec.KP) {
			if k == "created" {
				v = time.Now().String()
			}
		}
		rec.Value = v
		err := ed.kvs.Db.Put(rec)
		if err != nil {
			fmt.Printf("error saving '%s': %v\n", k, err)
		}
		rec.KP.KeyParts.Pop()
	}

	return nil
}

func (ed *EdPlugin) PreserveWhitespace() bool {
	return true
}

func (ed *EdPlugin) SetKVS(k *kvs.KVS) {
	ed.kvs = k
}
func (ed *EdPlugin) SetLiturgicalDb(k *kvs.KVS) {
	ed.db = k
}

func (ed *EdPlugin) SetCatalogManager(m *Manager) {
	ed.catMan = m
}
