package catalog

import (
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"os"
	"path/filepath"
	"testing"
)

var tmpDir string
var manager *Manager

func init() {
	doxlog.Init("", "", true)
	doxlog.Info("doxLog initialized")
}

func setup() {
	var err error
	tmpDir, err = os.MkdirTemp("", "catalogManager-")
	if err != nil {
		doxlog.Errorf(err.Error())
		return
	}
	dbPath := filepath.Join(tmpDir, ".doxa", ".sys.db")

	// open the database
	dsSystem, err := kvs.NewBoltKVS(dbPath)
	if err != nil {
		doxlog.Errorf(fmt.Sprintf("error creating database %s", dbPath), err)
		return
	}
	// create the database mapper
	var sysKvs *kvs.KVS
	sysKvs, err = kvs.NewKVS(dsSystem)
	if err != nil {
		doxlog.Errorf("error creating mapper for system database: %v\n", err)
		return
	}
	if sysKvs == nil {
		doxlog.Errorf("system kvs is nil")
		return
	}
	kp := kvs.NewKeyPath()
	kp.Dirs.Push("selected")
	err = sysKvs.Db.MakeDir(kp)
	if err != nil {
		doxlog.Errorf("Could not create: %v", err)
	}
	kp.KeyParts.Push("project")
	dbR := kvs.NewDbR()
	dbR.KP = kp.Copy()
	dbR.Value = "testproj"
	sysKvs.Db.Put(dbR)
	manager, err = NewManager(sysKvs)
	if err != nil {
		doxlog.Errorf("Could not create manager: %v", err)
		return
	}
	// create the context System database Manager
	manager, err = NewManager(sysKvs)
	if err != nil {
		doxlog.Errorf("error creating sys manager: %v", err)
		return
	}
	if manager == nil {
		doxlog.Errorf("sysManager is nil")
	}
}
func teardown() {
	err := manager.kvs.Db.Close()
	if err != nil {
		doxlog.Error(err.Error())
	}

	// Remove the temporary directory
	//err = os.RemoveAll(tmpDir)
	if err != nil {
		doxlog.Errorf(err.Error())
	}
}

func TestCatalog_JsonString(t *testing.T) {
	var c Catalog
	e := new(Entry)
	e.Path = "ltx"
	e.Desc = "English language Liturgical translations by Fr Xavier"
	e.Url = "github.com/dude"
	e.License = "Eclipse 2"
	c.Add(e)
	b := new(Entry)
	b.Path = "btx"
	b.Desc = "English language Bible, King James Version"
	b.Url = "github.com/kjv"
	b.License = "Eclipse 2"
	c.Add(b)
	s, err := c.JsonBytes()
	if err != nil {
		t.Error(err)
	}
	fmt.Printf("%s\n", s)
}
func TestCatalog_LoadRemote(t *testing.T) {
	url := "https://gitlab.com/doxa-seraphimdedes/catalog.git"
	c, err := LoadRemote(url, false)
	if err != nil {
		t.Error(err)
		return
	}
	var entries []byte
	entries, err = c.JsonBytes()
	if err != nil {
		t.Error(err)
		return
	}
	if len(entries) == 0 {
		t.Error(fmt.Errorf("expected len(entries) > 0, got 0"))
	}
}

func TestManager_SaveCatalog(t *testing.T) {
	setup()
	c := new(Catalog)
	c.Entries = map[string]*Entry{}
	c.URL = "https://example.com/test/catalog.json"
	c.Name = "testing"
	c.Maintainer = "John Doe <name@example.com>"
	c.Desc = "this is a test catalog, with test entries"

	ent1 := Entry{
		Name: "resources/ltx/en_us_test",
		Desc: "A test ltx library",
		Path: "resources/ltx",
		Url:  "https://example.com/testing/resources/ltx/en_us_test.git",
	}

	c.Add(&ent1)
	c.Add(&Entry{
		Name: "resources/media/en_us_test",
		Desc: "a test media library",
		Path: "resources/media",
		Url:  "https://example.com/testing/resources/media/en_us_test.git",
	})

	c.Add(&Entry{
		Name:         "templates",
		Desc:         "a test templates library",
		Path:         "templates",
		Url:          "https://example.com/testing/templates.git",
		Dependencies: Dependencies{ent1.Name},
	})
	manager.SaveCatalog(c)

	//now get a record...
	newC := manager.GetCatalogFromDb("testing")
	m1 := c.ToMap()
	m2 := newC.ToMap()
	for k, expecV := range m1 {
		if m2[k] != expecV {
			t.Errorf("expected '%s' to be '%s' but got '%s'", k, expecV, m2[k])
		}
	}
	for k, _ := range m2 {
		if _, exists := m1[k]; !exists {
			t.Errorf("Abnormal key: %s", k)
		}
	}
	teardown()
}

func TestManager_GetEntryPath(t *testing.T) {
	setup()
	e := new(Entry)
	e.Name = "test"
	e.Desc = "test entry"
	e.Url = "https://example.com"
	e.Path = "resources/ltx"
	c := new(Catalog)
	c.Entries = map[string]*Entry{}
	c.Name = "correct"
	c.Add(e)
	fakeC := new(Catalog)
	fakeC.Name = "fake"
	fakerC := new(Catalog)
	fakerC.Name = "faker"
	fakestC := new(Catalog)
	fakestC.Name = "fakest"
	manager.SaveCatalog(c)
	manager.SaveCatalog(fakeC)
	manager.SaveCatalog(fakerC)
	manager.SaveCatalog(fakestC)
	//first make path with definite catalog
	//then look for it without
	actualPath := manager.GetEntryPath("test", "")
	expectedPath := manager.GetEntryPath("test", "")
	if expectedPath.Path() != actualPath.Path() {
		t.Errorf("Expected '%s' but got '%s'", expectedPath.Path(), actualPath.Path())
	}
	teardown()
}

func TestManager_Subscriptions(t *testing.T) {
	setup()
	manager.Subscriptions()
	teardown()
}
