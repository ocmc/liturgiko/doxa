// Package resolver provides an interface with methods to check if a TopicKey exists, get Values for all specified generation libraries and the Version acronyms.
// It also provides a MockResolver that can be used in place of a database resolver.  That allows test cases to work without the need to access an actual database.
// It also provides DbResolver, which is a resolver that works against the ltk table.

package resolver

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/martian/log"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/atempl"
	"github.com/liturgiko/doxa/pkg/css"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/enums/dbStatus"
	"github.com/liturgiko/doxa/pkg/keyring"
	"github.com/liturgiko/doxa/pkg/ltm"
	"github.com/liturgiko/doxa/pkg/media"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"path"
	"sort"
	"strings"
	"sync"
)

// Resolver provides a methods for obtaining database setValues.
type Resolver interface {
	// The Close method tells the resolver to close its database connection.
	Close() error
	CSS(cssPath string) (*css.StyleSheet, error)
	// The Exists method returns true if there is a record exists in the database for the specified ID.
	Exists(library, topic, key string) (bool, error)
	ExistsTK(id string) (bool, error)
	Get(id string) (*kvs.DbR, error)
	GetShowUnresolvedTopicKey() bool
	MediaLinks(topicKey string, genLangs []*atempl.TableLayout, reverseMediaLookup *sync.Map) *atempl.TKVal
	Reset()
	SetShowUnresolvedTopicKey(to bool)
	SetTemplateMapper(templateResolver ltm.LTM)
	// The Template method gets the content of a template.
	Template(id, root string) (string, error)
	// The Values method gets the value of each combination of GenLib and topic/key.
	Values(topicKey string, genLangs []*atempl.TableLayout) *atempl.TKVal
	// The Versions method gets the value of each combination of GenLib and the topic/key 'properties/version.designation'
	Versions() map[string]string
}

// MockResolver implements the LMLListener.Resolver interface, so it can simulate resolving setValues by reading a database.
type MockResolver struct {
	TKMap                  map[string]bool   // used by the mock Exists method
	ResolvedValues         map[string]string // used by the mock Values and Versions methods
	ShowUnresolvedTopicKey bool
}

func (r *MockResolver) GetShowUnresolvedTopicKey() bool {
	return r.ShowUnresolvedTopicKey
}
func (r *MockResolver) SetShowUnresolvedTopicKey(to bool) {
	r.ShowUnresolvedTopicKey = to
}
func (r *MockResolver) AddTK(tk string) {
	r.TKMap[tk] = true
}
func (r *MockResolver) AddValue(id, value string) {
	r.ResolvedValues[id] = value
}
func (r *MockResolver) Close() error { return nil }
func (r *MockResolver) Reset()       {}
func (r *MockResolver) Exists(library, topic, key string) (bool, error) {
	if _, ok := r.ResolvedValues[fmt.Sprintf("%s%s%s%s%s", library, kvs.SegmentDelimiter, topic, kvs.IdDelimiter, key)]; ok {
		return true, nil
	} else {
		return false, nil
	}
}
func (r *MockResolver) ExistsTK(topicKey string) (bool, error) {
	tk, err := ltstring.TopicKeyFromId(topicKey)
	if err != nil {
		if exists := r.TKMap[topicKey]; exists {
			return true, nil
		} else {
			return false, err
		}
	}
	if exists := r.TKMap[tk]; exists {
		return true, nil
	} else {
		return false, nil
	}
	return r.TKMap[tk], nil
}

func (r *MockResolver) Get(id string) (*kvs.DbR, error) {
	return nil, nil
}

// Not implemented

func (r *MockResolver) MediaLinks(topicKey string, genLangs []*atempl.TableLayout, mediaReverseLookup *sync.Map) *atempl.TKVal {
	tkv := new(atempl.TKVal)
	l := new(atempl.TableLayout)
	g := new(atempl.GenLib)
	g.Primary = "gr_gr_cog"
	l.GospelLibs = append(l.GospelLibs, g)
	l.ProphetLibs = append(l.ProphetLibs, g)
	l.PsalterLibs = append(l.PsalterLibs, g)
	l.EpistleLibs = append(l.EpistleLibs, g)
	l.LiturgicalLibs = append(l.LiturgicalLibs, g)
	m := new(media.Media)
	l.MediaLibs = append(l.MediaLibs, m)

	tkv.Values = append(tkv.Values, l)
	return tkv
}
func (r *MockResolver) Values(topicKey string, genLangs []*atempl.TableLayout) *atempl.TKVal {
	var langs []*atempl.TableLayout
	for _, genLang := range genLangs {
		genLangCopy := genLang.Copy()
		for i, genLib := range genLangCopy.LiturgicalLibs {
			// determine whether we will use the primary or a fallback, and get the value
			for _, library := range genLib.All() {
				ltkv := r.Value(library, topicKey)
				if len(ltkv.Value) > 0 {
					genLangCopy.LiturgicalLibs[i].Value = ltkv.Value
					genLangCopy.LiturgicalLibs[i].IDUsed = ltkv.ID()
					genLangCopy.LiturgicalLibs[i].LibUsed = ltkv.UsedLibrary
					break
				}
			}
		}
		langs = append(langs, genLangCopy)
	}
	tkVal := new(atempl.TKVal)
	tkVal.Values = langs
	return tkVal
}
func (r *MockResolver) SetTemplateMapper(templateResolver ltm.LTM) {
	// do nothing for mock
}
func (r *MockResolver) Template(id, root string) (string, error) {
	if template, ok := r.ResolvedValues[id]; ok {
		return template, nil
	} else {
		return "", errors.New(fmt.Sprintf("not found: template %s", id))
	}
}
func (r *MockResolver) CSS(cssPath string) (*css.StyleSheet, error) {
	if content, ok := r.ResolvedValues[cssPath]; ok {
		styleSheet, _ := css.NewStyleSheet(cssPath, "", false)
		errors := styleSheet.Parse(content)
		if len(errors) > 0 {
			return nil, fmt.Errorf("%v", errors)
		}

		return styleSheet, nil
	} else {
		return nil, errors.New(fmt.Sprintf("not found: css %s", cssPath))
	}
}

func (r *MockResolver) Value(library, topicKey string) atempl.LTKVal {
	var ltkValue atempl.LTKVal
	ltkValue.UsedLibrary = library
	ltkValue.TopicKey = topicKey
	ltkValue.Value = r.ResolvedValues[ltkValue.ID()]
	return ltkValue
}

// Versions creates a map of requested libraries with the version (acronym) for each library
func (r *MockResolver) Versions() map[string]string {
	var mock = map[string]string{}
	return mock
}

// DbResolver implements the Resolver interface by providing the required methods, which access a database mapper.
// ResolvedValues caches setValues that have previously been read from the database.
// ResolvedIDs caches full IDs and topic/keys that have been previously verified to exist in the database
// ResolvedContent caches the content of templates that have previously been read.
// Caching reduces the processing time of templates by 66%.
type DbResolver struct {
	Mapper                 *kvs.KVS
	ResolvedMedia          sync.Map // holds previously retrieved media-map information for library/topic/key
	ResolvedValues         sync.Map // holds setValues that have been previously read
	ResolvedIDs            sync.Map // holds full IDs and topic/keys that we have previously verified
	ResolvedContent        sync.Map // holds content of templates previously read
	Mutex                  *sync.Mutex
	PdfViewer              string
	AudioPathPrefix        string
	PdfPathPrefix          string
	TemplateMapper         ltm.LTM
	ShowUnresolvedTopicKey bool
	KeyRing                *keyring.KeyRing
}

// NewDbResolver returns a DbResolver with all maps initialized
func NewDbResolver(mapper *kvs.KVS,
	keyRing *keyring.KeyRing,
	templateMapper ltm.LTM,
	showUnresolvedTopicKey bool) *DbResolver {
	r := new(DbResolver)
	r.KeyRing = keyRing
	r.ShowUnresolvedTopicKey = showUnresolvedTopicKey
	r.Mapper = mapper
	r.Mutex = &sync.Mutex{}
	r.TemplateMapper = templateMapper
	return r
}
func syncMapToMap(sm *sync.Map) (m map[string]interface{}) {
	m = make(map[string]interface{})
	sm.Range(func(k, v interface{}) bool {
		key, keyOk := k.(string)
		if !keyOk {
			return false
		}
		m[key] = v
		return true
	})
	return m
}
func (r *DbResolver) MediaToJson(filename string) {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()
	// convert sync.Map to map
	regularMap := syncMapToMap(&r.ResolvedMedia)
	// serialize to json
	jsonBytes, err := json.Marshal(regularMap)
	if err != nil {
		doxlog.Errorf("Error serializing media map to JSON: %v", err)
		return
	}
	err = ltfile.WriteFile(filename, string(jsonBytes))
	if err != nil {
		doxlog.Errorf("Error serializing json media map to %s: %v", filename, err)
	}
}
func (r *DbResolver) Get(id string) (*kvs.DbR, error) {
	kp := kvs.NewKeyPath()
	err := kp.ParsePath(id)
	if err != nil {
		msg := fmt.Sprintf("Error getting %s: %v", id, err)
		doxlog.Error(msg)
		return nil, fmt.Errorf(msg)
	}
	var rec *kvs.DbR
	rec, err = r.Mapper.Db.Get(kp)

	if err != nil {
		msg := fmt.Sprintf("Error getting %s: %v", id, err)
		doxlog.Error(msg)
		return nil, err
	}
	return rec, nil
}

func (r *DbResolver) GetShowUnresolvedTopicKey() bool {
	return r.ShowUnresolvedTopicKey
}
func (r *DbResolver) SetShowUnresolvedTopicKey(to bool) {
	r.ShowUnresolvedTopicKey = to
}

// Close closes the database in the Mapper. This also closes it for the CssMapper
// since they both point to the same database.
func (r *DbResolver) Close() error { return r.Mapper.Db.Close() }

func (r *DbResolver) Reset() {
	eraseSyncMap(&r.ResolvedValues)
	eraseSyncMap(&r.ResolvedMedia)
	eraseSyncMap(&r.ResolvedIDs)
	eraseSyncMap(&r.ResolvedContent)
}
func eraseSyncMap(m *sync.Map) {
	m.Range(func(key interface{}, value interface{}) bool {
		m.Delete(key)
		return true
	})
}

func (r *DbResolver) Exists(library, topic, key string) (bool, error) {
	id := fmt.Sprintf("%s%s%s%s%s", library, kvs.SegmentDelimiter, topic, kvs.IdDelimiter, key)
	val, ok := r.ResolvedIDs.Load(id)
	if ok {
		exists := val.(bool)
		return exists, nil
	} else {
		kp := kvs.NewKeyPath()
		kp.Dirs.Push("ltx")
		kp.Dirs.Push(library)
		kp.Dirs.Push(topic)
		kp.KeyParts.Push(key)
		exists := r.Mapper.Db.Exists(kp)
		r.ResolvedIDs.Store(id, exists) // could be false
		return exists, nil
	}
}

// ExistsTK checks to see if a particular topic and key occur in the database for any record.
// This is useful for resolving rids (relative IDs).
func (r *DbResolver) ExistsTK(topicKey string) (bool, error) {
	if r.KeyRing != nil {
		if r.KeyRing.Exists(topicKey) {
			return true, nil
		} else {
			return false, nil
		}
	}
	// since keyring was nil, search the database
	topic, key, err := ltstring.ToTK(topicKey)
	if err != nil {
		return false, err
	}
	val, ok := r.ResolvedIDs.Load(topicKey)
	if ok {
		exists := val.(bool)
		return exists, nil
	} else {
		matcher := kvs.NewMatcher()
		matcher.KP.Dirs.Push("ltx")
		matcher.KP.Dirs.Push(".*")
		matcher.KP.Dirs.Push(topic)
		matcher.KP.KeyParts.Push(key)
		matcher.Recursive = true
		matcher.WholeWord = true
		matcher.UseAsRegEx()
		exists, err := r.Mapper.Db.ExistsMatching(*matcher)
		r.ResolvedIDs.Store(topicKey, exists) // could be set to false
		return exists, err
	}
}
func (r *DbResolver) Template(id, root string) (string, error) {
	if len(root) == 0 {
		// TODO: implement db read of template table
		return "", nil
	}
	if !strings.HasSuffix(id, ".lml") {
		id = id + ".lml"
	}
	val, ok := r.ResolvedContent.Load(id)
	if ok {
		content, _ := val.(string)
		return content, nil
	}
	var err error
	content, err := r.TemplateMapper.GetContent(id)
	r.ResolvedContent.Store(id, content)
	return content, err
}

func (r *DbResolver) CSS(cssPath string) (*css.StyleSheet, error) {
	// TODO: why does this function exist in the DbResolver?
	base := path.Base(cssPath)
	styleSheet, err := css.NewStyleSheet(base, cssPath, false)
	if err != nil {
		return nil, err
	}
	return styleSheet, nil
}

// MediaLinks returns the setValues for each specified primary library.
func (r *DbResolver) MediaLinks(topicKey string, genLangs []*atempl.TableLayout, reverseMediaResolver *sync.Map) *atempl.TKVal {
	var langs []*atempl.TableLayout
	mediaManager := media.NewMediaManager(r.Mapper, &r.KeyRing.MediaReverseLookup)
	r.Mutex.Lock()
	for _, genLang := range genLangs {
		genLangCopy := genLang.Copy()
		for _, lib := range genLang.LiturgicalLibs {
			primary := lib.Primary
			var fallback string
			if len(lib.FallBacks) > 0 {
				fallback = lib.FallBacks[0]
			}
			parts := strings.Split(topicKey, kvs.IdDelimiter)
			if len(parts) != 2 {
				return nil
			}
			topic := parts[0]
			key := parts[1]
			if strings.HasSuffix(key, ".text") {
				key = key[:len(key)-5]
			} else { // only have media for keys ending in .text
				return nil
			}
			hasFallback := len(fallback) > 0
			var primaryFound bool
			var primaryMediaLinks *media.Media
			var primaryErr error
			var fallbackMediaLinks *media.Media
			var fallbackErr error
			primaryFound, primaryMediaLinks, primaryErr = mediaManager.GetMedia(primary, topic, key)
			if hasFallback {
				if primaryErr == nil && primaryFound && primaryMediaLinks != nil {
					genLangCopy.AddMediaLib(primaryMediaLinks)
				} else {
					_, fallbackMediaLinks, fallbackErr = mediaManager.GetMedia(fallback, topic, key)
					if fallbackErr == nil && fallbackMediaLinks != nil {
						genLangCopy.AddMediaLib(fallbackMediaLinks)
					}
				}
			} else {
				if primaryErr == nil && primaryMediaLinks != nil {
					genLangCopy.AddMediaLib(primaryMediaLinks)
				}
			}
		}
		langs = append(langs, genLangCopy)
	}
	tkVal := new(atempl.TKVal)
	tkVal.Values = langs
	r.Mutex.Unlock()
	return tkVal
}

// Values returns the setValues for each specified primary library.  If a primary does not have a value, attempts are made to get the value using the fallBack libraries.
// The len(genLibs) should equal the number of columns on a generated book or service.
func (r *DbResolver) Values(topicKey string, genLangs []*atempl.TableLayout) *atempl.TKVal {
	var langs []*atempl.TableLayout
	var libraries = make(map[string]string)
	var hasValueWithText bool

	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	for _, genLang := range genLangs {
		genLangCopy := genLang.Copy()
		if strings.HasPrefix(topicKey, "le.go.") {
			genLangCopy.GospelLibs = r.setValues(topicKey, genLangCopy.GospelLibs)
		} else if strings.HasPrefix(topicKey, "le.ep.") {
			genLangCopy.EpistleLibs = r.setValues(topicKey, genLangCopy.EpistleLibs)
		} else if strings.HasPrefix(topicKey, "le.pr.") {
			genLangCopy.ProphetLibs = r.setValues(topicKey, genLangCopy.ProphetLibs)
		} else if strings.HasPrefix(topicKey, "ps.") {
			genLangCopy.PsalterLibs = r.setValues(topicKey, genLangCopy.PsalterLibs)
		} else {
			genLangCopy.LiturgicalLibs = r.setValues(topicKey, genLangCopy.LiturgicalLibs)
		}
		langs = append(langs, genLangCopy)
	}
	tkVal := new(atempl.TKVal)
	tkVal.Values = langs
	tkVal.HasNonEmptyValue = hasValueWithText
	var theLibraries []string
	for k, v := range libraries {
		theLibraries = append(theLibraries, k)
		theLibraries = append(theLibraries, v)
	}
	sort.Strings(theLibraries)
	tkVal.Libraries = strings.Join(theLibraries, ", ")

	return tkVal
}
func (r *DbResolver) setValues(topicKey string, genLibCopy []*atempl.GenLib) []*atempl.GenLib {
	var libraries = make(map[string]string)
	var hasValueWithText bool
	for i, genLib := range genLibCopy {
		// determine whether we will use the primary or a fallback, and get the value
		readStatus := dbStatus.NA
		var trace string
		for j, library := range genLib.All() {
			if readStatus == dbStatus.OK || len(trace) > 0 {
				continue
			}
			kp := kvs.NewKeyPath()
			kp.Dirs.Push("ltx")
			kp.Dirs.Push(library)
			parts := strings.Split(topicKey, kvs.IdDelimiter)
			if len(parts) == 2 {
				kp.Dirs.Push(parts[0])
				kp.KeyParts.Push(parts[1])
			}
			var idUsed, langUsed, libraryUsed, value string
			// see if we already have this value
			val, ok := r.ResolvedValues.Load(kp.Path())
			if ok {
				resolvedValue := val.(*ResolvedValue)
				idUsed = resolvedValue.IDUsed
				libraryUsed = resolvedValue.LibUsed
				value = resolvedValue.Value
				trace = resolvedValue.Redirects
				readStatus = resolvedValue.Status
			} else { // we have not previously read the value, so get it from the database
				rec, redirects, _ := r.Mapper.Db.GetResolved(kp)
				// set trace
				if len(redirects) > 1 {
					trace = strings.Join(redirects, " > ")
					if j > 0 {
						var primaryPath = genLib.Primary
						primaryKp := kp.Copy()
						if kp.Dirs.Size() > 1 {
							primaryKp.Dirs.Set(1, genLib.Primary)
							primaryPath = primaryKp.Path()
						}
						trace = fmt.Sprintf("primary %s > fallback %s", primaryPath, trace)
					}
				}
				if rec == nil {
					readStatus = dbStatus.NotFound
					if len(redirects) > 0 {
						idUsed = redirects[len(redirects)-1]
						usedKp := kvs.NewKeyPath()
						err := usedKp.ParsePath(idUsed)
						if err == nil {
							if len(idUsed) > 0 && usedKp.Dirs.Size() > 1 {
								libraryUsed = usedKp.Dirs.Get(1)
								libraries[library] = libraryUsed
							}
						}
					} else {
						idUsed = kp.Path()
						if len(idUsed) > 0 && kp.Dirs.Size() > 1 {
							libraryUsed = kp.Dirs.Get(1)
							libraries[library] = libraryUsed
						}
					}
				} else {
					value = rec.Value
					if len(rec.Value) == 0 {
						readStatus = dbStatus.NoValue
					} else {
						readStatus = dbStatus.OK
					}
					// set idUsed, libraryUsed
					if rec.Redirect == nil {
						idUsed = rec.KP.Path()
						if len(idUsed) > 0 && rec.KP.Dirs.Size() > 1 {
							libraryUsed = rec.KP.Dirs.Get(1)
							libraries[library] = libraryUsed
						}
					} else {
						idUsed = rec.Redirect.Path()
						if len(idUsed) > 0 && rec.Redirect.Dirs.Size() > 1 {
							libraryUsed = rec.Redirect.Dirs.Get(1)
							libraries[library] = libraryUsed
						}
					}
				}
			}
			var err error
			langUsed, err = ltstring.LangFromLibrary(library)
			if err != nil {
				log.Errorf("could not extract language code from %s", libraryUsed)
			}
			if len(value) > 0 {
				genLibCopy[i].Value = value
				genLibCopy[i].TopicKeyRequested = topicKey
				genLibCopy[i].IDRequested = kp.Path()
				genLibCopy[i].IDUsed = idUsed
				genLibCopy[i].LibUsed = idUsed
				genLibCopy[i].Redirects = trace
				genLibCopy[i].Status = readStatus
				resolvedValue := new(ResolvedValue)
				resolvedValue.Status = readStatus
				resolvedValue.TopicKeyRequested = topicKey
				resolvedValue.IDRequested = kp.Path()
				resolvedValue.LangUsed = langUsed
				resolvedValue.LibUsed = libraryUsed
				resolvedValue.IDUsed = idUsed
				resolvedValue.Value = value
				resolvedValue.Redirects = trace
				if !hasValueWithText {
					hasValueWithText = true
				}
				r.ResolvedValues.Store(kp.Path(), resolvedValue)
				break
			} else if r.ShowUnresolvedTopicKey {
				genLibCopy[i].Value = value
				genLibCopy[i].TopicKeyRequested = topicKey
				genLibCopy[i].IDRequested = kp.Path()
				genLibCopy[i].IDUsed = idUsed
				genLibCopy[i].LibUsed = idUsed
				genLibCopy[i].LangUsed = langUsed
				genLibCopy[i].Redirects = trace
				genLibCopy[i].Status = readStatus
				resolvedValue := new(ResolvedValue)
				resolvedValue.Status = readStatus
				resolvedValue.Primary = genLibCopy[i].Primary
				if len(genLibCopy[i].FallBacks) > 0 {
					resolvedValue.Fallback = genLibCopy[i].FallBacks[0]
				}
				resolvedValue.TopicKeyRequested = topicKey
				resolvedValue.IDRequested = kp.Path()
				resolvedValue.LibUsed = idUsed // kp.Dirs.Get(1)
				resolvedValue.LangUsed = langUsed
				resolvedValue.IDUsed = idUsed // kp.Path()
				resolvedValue.Value = fmt.Sprintf("missing: %s", topicKey)
				resolvedValue.Redirects = trace
				r.ResolvedValues.Store(kp.Path(), resolvedValue)
			}
		}
	}
	return genLibCopy
}
func (r *DbResolver) SetTemplateMapper(templateMapper ltm.LTM) {
	r.TemplateMapper = templateMapper
}

// Versions creates a map of requested libraries with the version (acronym) for each library
func (r *DbResolver) Versions() map[string]string {
	theMap := map[string]string{}
	kp := kvs.NewKeyPath()
	kp.Dirs.Push("ltx")
	libs, err := r.Mapper.Db.DirNames(*kp)
	if err != nil {
		log.Errorf("could not load map of version names")
		return theMap
	}
	for _, library := range libs {
		kp := kvs.NewKeyPath()
		kp.Dirs.Push("ltx")
		kp.Dirs.Push(library)
		kp.Dirs.Push("properties")
		kp.KeyParts.Push("version.designation")
		rec, _ := r.Mapper.Db.Get(kp)
		var value string
		if rec != nil {
			value = rec.Value
		}
		if len(value) > 0 {
			theMap[library] = value
		}
	}
	return theMap
}

type ResolvedValue struct {
	TopicKeyRequested string
	IDRequested       string
	IDUsed            string
	LangUsed          string
	LibUsed           string
	Primary           string
	Fallback          string
	Redirects         string
	Status            dbStatus.DbStatus
	Value             string
}
