package api

import (
	"context"
	"fmt"
	"github.com/liturgiko/doxa/app"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/dvcs"
	"github.com/liturgiko/doxa/pkg/syncsrvc"
	"github.com/liturgiko/doxa/pkg/users"
	picoWidget "github.com/liturgiko/doxa/templates/webApp/structs/widget"
	"html/template"
	"net/url"
	"time"
)

type RwSync struct {
}

type RwRepoHealth int

const (
	RwRepoNoErrors RwRepoHealth = iota
	RwRepoWarnings
	RwRepoErrors
	RwComplete
)

type RwSyncInfo struct {
	URL      string    // the path to the gitlab project group
	LastSync time.Time //
	InSync   bool
}

type RwRepoList []string
type rwSyncService struct {
	LastSync time.Time
	Started  time.Time
	State    SyncState
	ErrorMsg string
}

func NewRwSyncService() RwSyncService {
	return new(rwSyncService)
}
func (rw *rwSyncService) getTitleAndDesc() (title, desc string) {
	switch rw.State {
	case NotSyncing:
		title = "Ready to Upload to Gitlab"
		desc = "Hit start when you're ready"
		if hasGroup, resp, err := rw.GitlabGroupExists(nil, ""); !hasGroup {
			if err != nil {
				doxlog.Errorf("Could not detect gitlab group: (%v) %v", err, resp)
			}
			title = "Group doesn't exist"
			desc = "The gitlab group does not exist, or you do not have authorization to access it"
			rw.State = ErrNoSuchGroup
		}
	case SyncInProgress:
		title = "Upload to Gitlab in progress"
		desc = "Now uploading your libraries"
	case NeedsToken:
		title = "Authorization Needed"
		desc = "You need to unlock your token to access gitlab features like upload"
	case CompletedNoErrs:
		title = "Finished uploading to Gitlab"
		desc = ""
	case Terminated:
		title = "Upload Failed"
		desc = rw.ErrorMsg
	default:
		title = "How did we get here?"
		desc = "We didn't expect you to ever see this, and it shouldn't happen. So please take a screenshot, make a bug report, and refresh the page to see if it fixes it"
	}
	return
}

func (rw *rwSyncService) GetWidget(ctx context.Context, actionPrefix string, id string, userID string) *picoWidget.Form {
	fb := rw.PopulateFeedback()
	t, d := rw.getTitleAndDesc()
	rwForm := picoWidget.NewForm(id, t)
	rwForm.TitleIcon = "bi-cloud-upload"
	rwForm.Class += " fs-2"
	rwForm.Description = d
	switch rw.State {
	case NotSyncing:
		lastSyncTime := time.Since(rw.LastSync)
		var enArche time.Time
		if lastSyncTime < time.Since(enArche) {
			LastSynced := picoWidget.NewInputField()
			LastSynced.Value = timestampToHumanReadable(lastSyncTime)
			LastSynced.ReadOnly = true
			LastSynced.Label = "Last upload"
			rwForm.Fields = append(rwForm.Fields, LastSynced)
		}
		act := picoWidget.NewInputField()
		act.Type = "submit"
		act.Value = "Start"
		rwForm.AddAction(act)
		rwForm.FormMethod = POST
		rwForm.FormAction, _ = url.JoinPath(actionPrefix, "start")
		//last synced
		//action start
	case SyncInProgress:
		progBar := picoWidget.NewProgressBar()
		progBar.Max = fb.NumTotal
		progBar.Value = fb.NumCompleted
		progBar.HtmxFields = []template.HTMLAttr{
			`hx-get="/api/htmx/rw/pollProgress"`,
			`hx-trigger="every 1s"`,
			`hx-swap="outerHTML"`,
		}
		rwForm.Fields = append(rwForm.Fields, progBar)
	case NeedsToken:
		act := picoWidget.NewInputField()
		act.Type = "submit"
		act.ID = "action-auth"
		act.Value = "Open Unlock Token"
		rwForm.FormMethod = GET
		rwForm.FormAction = "/protectToken"
		rwForm.AddAction(act)
		rwForm.TitleIcon = "bi-lock"
		return rwForm
	case CompletedNoErrs:
		act := picoWidget.NewInputField()
		act.Type = "submit"
		act.Value = "Clear"
		scroller := picoWidget.NewScrollArea()
		scroller.Vh = 57
		scroller.Contents = fb.ToElements()
		rwForm.Fields = append(rwForm.Fields, scroller)
		rwForm.AddAction(act)
		rwForm.FormMethod = POST
		rwForm.FormAction, _ = url.JoinPath(actionPrefix, "reset")
	case Terminated:
		valBiding := picoWidget.NewLabel()
		valBiding.Contents = rw.ErrorMsg
		act := picoWidget.NewInputField()
		act.Type = "submit"
		act.Value = "Dismiss"
		rwForm.Fields = append(rwForm.Fields, valBiding, act)
	}

	glLink := picoWidget.NewInputField()
	glLink.Type = "button"
	glLink.Class = "secondary"
	glLink.Value = "View in gitlab"
	link := fmt.Sprintf("https://gitlab.com/%s", app.Ctx.Project)
	if rw.State == ErrNoSuchGroup {
		glLink.Value = "Create group in gitlab"
		glLink.Class = "primary"
		link = "https://gitlab.com/groups/new"
	}
	glLink.HtmxFields = append(glLink.HtmxFields, template.HTMLAttr(fmt.Sprintf("onclick=\"window.location.href='%s'\"", link)))
	rwForm.AddAction(glLink)
	return rwForm
}

func (rw *rwSyncService) ResetPage(ctx context.Context, sessionID string) (resp RequestResponse, err error) {
	switch rw.State {
	case NotSyncing:
	case SyncInProgress:
	case NeedsToken:
		if app.Ctx.UserManager.HasToken(users.LocalUser) {
			rw.State = NotSyncing
		}
	default:
		rw.State = NotSyncing
	}
	return RequestResponse{RequestStatus: OK}, nil
}

func (rw *rwSyncService) PopulateFeedback() SyncFeedback {
	var fb SyncFeedback
	if !rw.hasToken() {
		return SyncFeedback{}
	}
	rw.LastSync = app.Ctx.SyncManager.RwSyncer.(*syncsrvc.ReadWriteSyncer).LastSync
	for _, rem := range app.Ctx.SyncManager.RwSyncer.GetAllFromMap() {
		fb.NumTotal++
		inf := SyncItemInfo{}
		if rw.Started.Before(rem.LastSynced) {
			fb.NumCompleted++
			inf.Health = SyncComplete
		}
		if rem.HasIssuesOfType(syncsrvc.Warning) {
			inf.Health = SyncWarnings
		}
		if rem.HasIssuesOfType(syncsrvc.Error) {
			inf.Health = SyncErrors
		}
		inf.Name = rem.RepoName
		for _, i := range rem.Issues {
			inf.Messages = append(inf.Messages, i)
		}
		fb.Items = append(fb.Items, inf)
	}
	if rw.State == CompletedNoErrs || rw.State == Terminated {
		fb.Done = true
	}
	return fb
}

func (rw *rwSyncService) Details(ctx context.Context, sessionID string) (SyncFeedback, RequestResponse, error) {
	return rw.PopulateFeedback(), RequestResponse{RequestStatus: OK}, nil
}

func (rw *rwSyncService) Info(ctx context.Context, sessionID string) (RwSyncInfo, RequestResponse, error) {
	//TODO implement me
	panic("implement me")
}

func (rw *rwSyncService) Repos(ctx context.Context, sessionID string) (RwRepoList, RequestResponse, error) {
	fb := rw.PopulateFeedback()
	var rl []string
	for _, r := range fb.Items {
		rl = append(rl, r.Name)
	}
	return rl, RequestResponse{RequestStatus: OK}, nil
}

func (rw *rwSyncService) Sync(ctx context.Context, sessionID string, repoId string) (RequestResponse, error) {
	//TODO implement me
	panic("implement me")
}

func (rw *rwSyncService) SyncAll(ctx context.Context, sessionID string) (RequestResponse, error) {
	if !rw.hasToken() {
		return RequestResponse{RequestStatus: Error, RequestMessages: []string{"Token needed"}}, fmt.Errorf("needs token")
	}
	if exists, resp, err := rw.GitlabGroupExists(ctx, sessionID); !exists {
		return resp, err
	}
	rw.Started = time.Now()
	go func() {
		err := app.Ctx.SyncManager.RwSyncer.SyncAll()
		if err != nil {
			rw.State = Terminated
			doxlog.Errorf("%v", err)
			rw.ErrorMsg = err.Error()
			return
		}
		rw.State = CompletedNoErrs
	}()
	rw.State = SyncInProgress
	rw.LastSync = app.Ctx.SyncManager.RwSyncer.(*syncsrvc.ReadWriteSyncer).LastSync
	return RequestResponse{RequestStatus: OK}, nil
}

func (rw *rwSyncService) hasToken() bool {
	if !app.Ctx.UserManager.HasToken(users.LocalUser) {
		rw.State = NeedsToken
		return false
	}
	if tok, err := app.Ctx.UserManager.GetToken(users.LocalUser); tok == "" || err != nil {
		rw.State = NeedsToken
		return false
	}
	if rw.State == NeedsToken {
		rw.State = NotSyncing
	}
	return true
}

func (rw *rwSyncService) getState() SyncState {
	return rw.State
}

func (rw *rwSyncService) GitlabGroupExists(ctx context.Context, sessionID string) (bool, RequestResponse, error) {
	if !rw.hasToken() {
		return false, RequestResponse{RequestStatus: Error, RequestMessages: []string{"Token needed"}}, fmt.Errorf("needs token")
	}

	token, err := app.Ctx.UserManager.GetToken(users.LocalUser)
	if err != nil {
		return false, RequestResponse{RequestStatus: Error, RequestMessages: []string{err.Error()}}, err
	}

	groupID, err := new(dvcs.DVCS).GetProjectGroup(token, app.Ctx.Project, app.Ctx.GitlabApi)
	if err != nil {
		if err.Error() == "could not find gitlab group" {
			return false, RequestResponse{RequestStatus: OK}, nil
		}
		return false, RequestResponse{RequestStatus: Error, RequestMessages: []string{err.Error()}}, err
	}

	return groupID != 0, RequestResponse{RequestStatus: OK}, nil
}
