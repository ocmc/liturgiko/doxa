package api

import (
	"context"
	"fmt"
	"github.com/liturgiko/doxa/app"
	"github.com/liturgiko/doxa/pkg/catalog"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/dbPaths"
	"github.com/liturgiko/doxa/pkg/doxlog"
	picoWidget "github.com/liturgiko/doxa/templates/webApp/structs/widget"
	"html/template"
	"path"
	"sort"
	"strings"
	"time"
)

type Subscription struct {
}
type subscriptionService struct {
	catMan        *catalog.Manager
	fallbackPaths []string
	uiState       struct {
		catalogSelected   string
		librarySelected   string
		libraryIsSelected bool //used to handle selection state
		catalogIndices    map[string]int
		libraryIndices    map[string]int
		isInventory       bool
		sortedCatalogs    []string
		sortedLibraries   []string
	}
}

type SubscriptionServiceAddCustomCatalog struct {
	Name string
}

type SubscriptionServiceInfo struct {
}

type SubscriptionSelectionData struct {
	Inventories []CatalogSelectionContents
	Catalogs    []CatalogSelectionContents
}

type CatalogSelectionContents struct {
	Name     string
	Contents []struct {
		Name   string
		Action SubscriptionSelectionAction
	}
}
type SubscriptionSelectionAction int

const (
	SelectSubscribe SubscriptionSelectionAction = iota
	SelectUnsubscribe
)

func NewSubscriptionService() SubscriptionService {
	ss := new(subscriptionService)
	ss.catMan = app.Ctx.CatalogManager
	ss.uiState.libraryIndices = make(map[string]int)
	ss.uiState.catalogIndices = make(map[string]int)
	return ss
}

type SubscribeUISelection struct {
	Name   string
	ListID string
}

func (ss *subscriptionService) GetForm(ctx context.Context, actionPrefix, id, userID string, isInventory bool, selection SubscribeUISelection) *picoWidget.SubscriptionManager {
	sm := picoWidget.NewSubscriptionManager()
	sm.ID = id
	sm.Title = "Subscriptions"
	sm.Description = "Manage your subscriptions"

	// Handle selection updates
	if selection.Name != "" {
		switch selection.ListID {
		case SSUICatalogListId, SSUIInventoryListID:
			ss.uiState.libraryIsSelected = false
			ss.uiState.librarySelected = ""
			ss.uiState.catalogSelected = selection.Name
		case SSUIEntryListID, SSUIInventoryEntryListID:
			ss.uiState.libraryIsSelected = true
			ss.uiState.librarySelected = selection.Name
		}
	}

	// Get all catalog and inventory names
	var groups []picoWidget.Group
	seenNames := make(map[string]bool)
	allNames := make([]string, 0)

	// Get catalogs
	names, _ := ss.catMan.GetAvailCatalogNames()
	for _, name := range names {
		allNames = append(allNames, name)
	}

	// Get inventories
	invNames, _ := ss.catMan.GetInventoryNames()
	for _, name := range invNames {
		allNames = append(allNames, name)
	}

	// Sort all names
	sort.Strings(allNames)

	// Create groups in sorted order
	for _, name := range allNames {
		var cat *catalog.Catalog
		// Try catalogs first
		cat = ss.catMan.GetCatalogFromDb(dbPaths.SYS.CatalogsPath(), name, "latest")
		if cat == nil || cat.Name == "" {
			// Try inventories
			cat = ss.catMan.GetCatalogFromDb(dbPaths.SYS.InventoriesPath(), name, "latest")
		}
		if cat != nil && !seenNames[cat.Name] {
			seenNames[cat.Name] = true
			groups = append(groups, picoWidget.Group{
				Name:        cat.Name,
				Maintainer:  cat.Maintainer,
				Description: cat.Desc,
			})
		}
	}
	sm.SetGroups(groups)

	// If a catalog is selected, get its projects
	if ss.uiState.catalogSelected != "" {
		// Try inventories first, then catalogs
		var cat *catalog.Catalog
		cat = ss.catMan.GetCatalogFromDb(dbPaths.SYS.InventoriesPath(), ss.uiState.catalogSelected, "latest")
		if cat == nil || cat.Name == "" {
			cat = ss.catMan.GetCatalogFromDb(dbPaths.SYS.CatalogsPath(), ss.uiState.catalogSelected, "latest")
		}

		if cat != nil && len(cat.Entries) > 0 {
			var projects []picoWidget.Project
			// Get subscriptions from both catalogs and inventories
			subbedCat := ss.catMan.GetSubscriptionsInCatalog(ss.uiState.catalogSelected, false)
			subbedInv := ss.catMan.GetSubscriptionsInCatalog(ss.uiState.catalogSelected, true)

			// Track both stable and unstable subscriptions
			stableMap := make(map[string]bool)
			unstableMap := make(map[string]bool)

			// Catalog subscriptions are stable
			for _, ent := range subbedCat {
				if ent != nil {
					stableMap[ent.Path] = true
				}
			}

			// Inventory subscriptions are unstable
			for _, ent := range subbedInv {
				if ent != nil {
					unstableMap[ent.Path] = true
				}
			}

			// Create sorted slice of entries
			sortedEntries := make([]*catalog.Entry, 0, len(cat.Entries))
			for _, entry := range cat.Entries {
				if entry != nil {
					sortedEntries = append(sortedEntries, entry)
				}
			}
			// Sort entries by Path
			sort.Slice(sortedEntries, func(i, j int) bool {
				return sortedEntries[i].Path < sortedEntries[j].Path
			})

			// Use sorted entries to create projects
			for _, entry := range sortedEntries {
				projects = append(projects, picoWidget.Project{
					Name:        entry.Path,
					DbName:      strings.TrimPrefix(entry.Path, "resources/"),
					Description: entry.Desc,
					Subscribed:  stableMap[entry.Path] || unstableMap[entry.Path],
					Stable:      stableMap[entry.Path],
					Unstable:    unstableMap[entry.Path],
				})
			}
			sm.SetProjects(ss.uiState.catalogSelected, projects)
		}
	}

	return sm
}

func (ss *subscriptionService) LegacyGetForm(ctx context.Context, actionPrefix, id, userID string, isInventory bool, selection SubscribeUISelection) *picoWidget.Generic {
	ss.uiState.isInventory = isInventory
	if selection.Name != "" {
		switch selection.ListID {
		case SSUICatalogListId:
			ss.uiState.libraryIsSelected = false
			ss.uiState.librarySelected = "" //reset library selection
			ss.uiState.catalogSelected = selection.Name
		case SSUIInventoryListID:
			ss.uiState.libraryIsSelected = false
			ss.uiState.librarySelected = "" //reset library selection
			ss.uiState.catalogSelected = selection.Name
		case SSUIEntryListID:
			ss.uiState.libraryIsSelected = true
			ss.uiState.librarySelected = selection.Name
			//we don't reset catalog
		case SSUIInventoryEntryListID:
			ss.uiState.libraryIsSelected = true
			ss.uiState.librarySelected = selection.Name
			//we don't reset catalog
		default:
			doxlog.Errorf("UI error, unknown list '%s'", selection.ListID)
		}
	}
	if ss.uiState.catalogSelected == "" {
		ss.uiState.catalogSelected = "doxa-seraphimdedes"
	}

	wrapper := picoWidget.NewGeneric()
	wrapper.Title = "Change your subscription"
	wrapper.ID = "modifyCatalogSubscription"
	if isInventory {
		wrapper.ID = "modifyInventorySubscription"
	}
	/*backB := picoWidget.NewAnchor()
	backB.Role = "button"
	backB.Class = "bi bi-arrow-left-circle"
	backBlabel := picoWidget.NewLabel()
	backBlabel.Contents = "Back to Install"
	backB.Contents = []picoWidget.Element{backBlabel}
	backB.HtmxFields = []template.HTMLAttr{
		`hx-get="api/htmx/ro/syncUI"`, //TODO: add this
		`hx-swap="outerHTML"`,
		`hx-target="#modifySubscription"`,
	}

	switchG := picoWidget.NewFormInputGroup()
	buttonL := picoWidget.NewLabel()
	buttonL.Contents = "Under construction"
	buttonL.Class = "bi bi-shield-exclamation"
	locationL := picoWidget.NewLabel()
	locationL.Contents = "Published"
	locationL.Class = "bi bi-shield-check outline"

	switchB := picoWidget.NewAnchor()
	switchBPath := `hx-get="api/htmx/subscriptions/subscribeInventoriesUI"`
	switchB.Contents = []picoWidget.Element{buttonL}
	switchG.Contents = []picoWidget.Element{backB, locationL, switchB}
	if isInventory {
		switchBPath = `hx-get="api/htmx/subscriptions/subscribeCatalogsUI"`
		buttonL.Contents = "Published"
		buttonL.Class = "bi bi-shield-check"
		locationL.Contents = "Under Construction"
		locationL.Class = "bi bi-shield-exclamation outline"
	}
	locationL.HtmxFields = []template.HTMLAttr{
		`role="button"`,
	}
	switchB.Role = "button"

	switchB.HtmxFields = []template.HTMLAttr{
		template.HTMLAttr(switchBPath),
		`hx-swap="outerHTML"`,
		`hx-target="#modifySubscription"`,
	}*/

	libWrapper := picoWidget.NewScrollArea()
	libWrapper.Contents = []picoWidget.Element{ss.GetEntryList(wrapper.ID, isInventory)}
	libWrapper.Vh = 50

	//gg := picoWidget.NewFormInputGroup()
	//gg.Contents = []picoWidget.Element{backB, switchB}
	doubleColumnView := picoWidget.NewGridLayout()
	doubleColumnView.Contents = []picoWidget.Element{ss.GetCatalogList(wrapper.ID, isInventory), libWrapper}
	doubleColumnView.HtmxFields = []template.HTMLAttr{
		`style="grid-template-columns: auto 1fr;"`,
	}
	wrapper.Contents = []picoWidget.Element{ /*backB, switchG,*/ doubleColumnView}
	if ss.uiState.librarySelected == "templates" && ss.uiState.catalogSelected != "" {
		//ss.SetFallbackTemplates()
		setB := picoWidget.NewInputField()
		setB.Type = "button"
		setB.Class = "secondary"
		setB.Value = "Set As Fallback"
		setB.HtmxFields = []template.HTMLAttr{
			`hx-post="/api/htmx/subscriptions/updateFallback"`,
			`onclick='this.style="display:none;"'`,
		}
		wrapper.Contents = append(wrapper.Contents, setB)
	}
	wrapper.Title = "Released Catalogs"
	wrapper.Description = "TODO"
	if isInventory {
		wrapper.Title = "Under Construction"
		wrapper.Description = "TODO"
	}
	return wrapper
}

const SSUICatalogListId = "availCatalogs"
const SSUIInventoryListID = "inventoryList"
const SSUIEntryListID = "entryList"
const SSUIInventoryEntryListID = "inventoryEntryList"

func (ss *subscriptionService) GetCatalogList(shellId string, inventory bool) *picoWidget.DynamicList {

	cataList := picoWidget.NewDynamicList()
	cataList.ListEndpoint = "api/htmx/subscriptions/update"
	cataList.ID = SSUICatalogListId
	if inventory {
		cataList.ID = SSUIInventoryListID
	}

	cataList.Contents = ss.genCatalogListContents(cataList.ID, shellId, inventory)
	cataList.HasActions = true
	infAction := new(picoWidget.ListItemInfoAction)
	infAction.SetEndpoint("/api/htmx/subscriptions/info", GET)
	acts := make(picoWidget.ActionSet, 0, 1)
	acts = append(acts, infAction)
	cataList.SetActions(acts)
	cataList.AddModal = ss.getAddModal(true, cataList.ID)
	cataList.Permissions.AddItems = true
	if ss.uiState.catalogSelected != "" {
		cataList.Selected = ss.uiState.catalogIndices[ss.uiState.catalogSelected]
	}
	//cataList.Permissions.AddItemsExceptions = []bool{true, true, true, false}
	cataList.Finalize()
	return cataList
}

func (ss *subscriptionService) GetEntryList(shellId string, inventory bool) *picoWidget.DynamicList {
	entList := picoWidget.NewDynamicList()
	entList.ListEndpoint = "api/htmx/subscriptions/update"
	entList.ID = SSUIEntryListID
	if inventory {
		entList.ID = SSUIInventoryEntryListID
	}
	cont, sel := ss.genEntryListContents(entList.ID, shellId, inventory)
	entList.Contents = cont
	entList.Selection = sel
	entList.HasActions = true
	entList.Checklist = true
	infAction := new(picoWidget.ListItemInfoAction)
	infAction.SetEndpoint("/api/htmx/subscriptions/info", GET)
	acts := make(picoWidget.ActionSet, 0, 1)
	acts = append(acts, infAction)
	if ss.getFallbackCatalog() == ss.uiState.catalogSelected && ss.uiState.catalogSelected != "" {
		acts = append(acts, new(fallbackCatalogAction))
	}
	entList.SetActions(acts)
	entList.Permissions.AddItems = false
	if ss.uiState.libraryIsSelected && ss.uiState.librarySelected != "" {
		entList.Selected = ss.uiState.libraryIndices[ss.uiState.librarySelected]
	}
	//cataList.Permissions.AddItemsExceptions = []bool{true, true, true, false}
	entList.Finalize()
	return entList
}
func (ss *subscriptionService) genEntryListContents(listId, shellId string, inventory bool) ([]picoWidget.Element, []bool) {
	subMap := make(map[string]bool)
	entMap := make(map[string]*catalog.Entry)
	subbed := ss.catMan.GetSubscriptionsInCatalog(ss.uiState.catalogSelected, inventory)
	if subbed == nil {
		subbed = make([]*catalog.Entry, 0)
	}
	for _, ent := range subbed {
		if ent == nil {
			continue
		}
		subMap[ent.Path] = true
	}
	catpat := dbPaths.SYS.CatalogsPath()
	if inventory {
		catpat = dbPaths.SYS.InventoriesPath()
	}
	if ss.uiState.catalogSelected == "" {
		return make([]picoWidget.Element, 0), make([]bool, 0) //we do not do this
	}
	selCat := ss.catMan.GetCatalogFromDb(catpat, ss.uiState.catalogSelected, "latest")
	if selCat == nil || len(selCat.Entries) == 0 {
		return make([]picoWidget.Element, 0), make([]bool, 0)
	}

	entNames := make([]string, 0, len(selCat.Entries))
	for ent, deets := range selCat.Entries {
		if deets == nil {
			continue
		}
		entMap[deets.Path] = deets
		entNames = append(entNames, ent)
	}

	collection := make([]picoWidget.Element, len(entNames))
	selection := make([]bool, len(entNames))
	sort.Strings(entNames) //do this for ease of navigation, but also to ensure consistent mapping
	for i, nomos := range entNames {
		selection[i] = subMap[nomos]
		ss.uiState.libraryIndices[nomos] = i
		tmp := picoWidget.NewLabel()
		if shortName, isThere := entMap[nomos]; isThere {
			tmp.Contents = shortName.Name
			if strings.Contains(nomos, "media") {
				tmp.Contents += " (media)"
			}
		}
		endpoint := `hx-get="api/htmx/subscriptions/subscribeCatalogsUI"`
		if inventory {
			endpoint = `hx-get="api/htmx/subscriptions/subscribeInventoriesUI"`
		}
		tmp.HtmxFields = []template.HTMLAttr{
			`hx-trigger='click'`,
			template.HTMLAttr(endpoint),
			template.HTMLAttr(fmt.Sprintf(`hx-target="#%s"`, shellId)),
			template.HTMLAttr(fmt.Sprintf(`hx-headers='{"DL-Selected": "%s", "DL-ID": "%s"}'`, nomos, listId)),
		}
		tmp.ID = strings.ReplaceAll(nomos, "/", "-")
		collection[i] = tmp

	}
	return collection, selection
}

func (ss *subscriptionService) GetSelectedUICatalog(ctx context.Context, userID string) (catalog string, inventory bool) {
	return ss.uiState.catalogSelected, ss.uiState.isInventory
}

func (ss *subscriptionService) genCatalogListContents(listId, shellId string, inventory bool) []picoWidget.Element {
	var catNames []string
	if inventory {
		catNames, _ = ss.catMan.GetInventoryNames()
	} else {
		catNames, _ = ss.catMan.GetAvailCatalogNames()
	}
	if catNames == nil {
		//empty list
		return make([]picoWidget.Element, 0)
	}
	collection := make([]picoWidget.Element, len(catNames))
	sort.Strings(catNames) //do this for ease of navigation, but also to ensure consistent mapping
	for i, nomos := range catNames {
		ss.uiState.catalogIndices[nomos] = i
		tmp := picoWidget.NewLabel()
		tmp.Contents = nomos

		endpoint := `hx-get="api/htmx/subscriptions/subscribeCatalogsUI""`
		if inventory {
			endpoint = `hx-get="api/htmx/subscriptions/subscribeInventoriesUI"`
		}
		tmp.HtmxFields = []template.HTMLAttr{
			`hx-trigger='click'`,
			template.HTMLAttr(endpoint),
			template.HTMLAttr(fmt.Sprintf(`hx-target="#%s"`, shellId)),
			template.HTMLAttr(fmt.Sprintf(`hx-headers='{"DL-Selected": "%s", "DL-ID": "%s"}'`, nomos, listId)),
		}
		tmp.ID = nomos
		collection[i] = tmp
	}
	return collection
}

func (ss *subscriptionService) getAddModal(inventory bool, targetListID string) picoWidget.GenericModal {
	idStr := "importCatalogModal"
	affordanceIndicator := "Catalog URL to Add"
	if inventory {
		idStr = "addInventoryModal"
		affordanceIndicator = "inventory URL to Add"
	}
	return picoWidget.GenericModal{
		ID:    idStr,
		Title: "Add item",
		Content: []picoWidget.Element{
			//FormMethod: api.POST,
			&picoWidget.InputField{
				ID:          "picoModalGenericAddCatalog",
				ElementType: new(picoWidget.InputField).ET(),
				Type:        "Text",
				Name:        "URL", //TODO:
				AriaLabel:   affordanceIndicator,
				Label:       affordanceIndicator,
			},
			&picoWidget.InputField{
				ElementType: new(picoWidget.InputField).ET(),
				Type:        "submit",
				//	Name:        "Add",
				Value: "Add Catalog",
				HtmxFields: []template.HTMLAttr{
					`hx-post="api/htmx/subscriptions/update"`,
					//input field URL will be in respose, not header
					template.HTMLAttr(fmt.Sprintf(`hx-target="#%s"`, targetListID)),
					template.HTMLAttr(fmt.Sprintf(`hx-headers='js:{"DL-Add": document.getElementById("picoModalGenericAddCatalog").value, "DL-ID": "%s"}'`, targetListID)),
					`hx-trigger="click prevent"`,
				},
			},
		},
	}
}

func (ss *subscriptionService) GetCatalogInfo(ctx context.Context, sessionID, catName, listId string, isInventory bool) picoWidget.Element {
	//get the catalog
	catHome := dbPaths.SYS.CatalogsPath()
	catStyle := "catalog"
	cat := ss.catMan.GetCatalogFromDb(catHome, catName, "latest")
	if cat == nil || cat.Name == "" {
		//try inventory
		cat = ss.catMan.GetCatalogFromDb(dbPaths.SYS.InventoriesPath(), catName, "latest")
		if cat == nil || cat.Name == "" {
			doxlog.Errorf("Could not find %s '%s'", catStyle, catName)
			return nil
		}
	}
	nomos := picoWidget.NewInputField()
	nomos.Value = cat.Name
	nomos.Label = "Name"
	nomos.ReadOnly = true

	desc := picoWidget.NewInputField()
	desc.Value = cat.Desc
	desc.Label = "Description"
	desc.Type = "textarea"
	desc.ReadOnly = true

	maint := picoWidget.NewInputField()
	maint.Value = cat.Maintainer
	maint.Label = "maintained by"
	maint.ReadOnly = true

	url := picoWidget.NewInputField()
	url.Value = "Copy Link"
	url.Type = "button"
	url.Class = "secondary"
	url.HtmxFields = []template.HTMLAttr{
		template.HTMLAttr(fmt.Sprintf(`onclick="navigator.clipboard.writeText(\"%s\")"`, cat.URL)),
		template.HTMLAttr(fmt.Sprintf(`title="%s"`, cat.URL)),
	}

	ver := picoWidget.NewInputField()
	ver.Label = "Available version"
	ver.Value = cat.Version
	ver.ReadOnly = true

	backB := picoWidget.NewInputField()
	backB.Type = "submit"
	backB.Value = "Done"
	backB.HtmxFields = []template.HTMLAttr{
		`hx-swap="outerHTML"`,
		`hx-target="#subscriptionManagerWidget"`,
		`hx-get="api/htmx/subscriptions/groups"`,
	}

	dynamiteB := picoWidget.NewInputField()
	dynamiteB.Type = "button"
	dynamiteB.Class = "secondary"
	dynamiteB.Value = "Delete"
	dynamiteB.HtmxFields = []template.HTMLAttr{
		`hx-confirm="are you sure you want to delete this catalog?"`,
		template.HTMLAttr(`hx-delete="api/htmx/subscriptions/delete/` + catName + `"`),
	}
	wrapper := picoWidget.NewScrollArea()
	wrapper.Vh = 50
	wrapper.Contents = []picoWidget.Element{
		nomos, desc, maint, ver, url,
	}
	actionB := picoWidget.NewGridLayout()
	actionB.Contents = []picoWidget.Element{
		backB, dynamiteB,
	}
	wrapper2 := picoWidget.NewUnorderedList()
	wrapper2.Contents = []picoWidget.Element{wrapper, actionB}
	return wrapper2
}

func (ss *subscriptionService) GetEntryInfo(ctx context.Context, sessionID, catName, entName, listId string, isInventory bool) picoWidget.Element {
	//get the catalog
	catHome := dbPaths.SYS.CatalogsPath()
	catStyle := "catalog"
	if isInventory {
		catStyle = "inventory"
		catHome = dbPaths.SYS.InventoriesPath()
	}
	ent := ss.catMan.GetEntryFromDb(catHome, entName, catName, "latest")
	if ent == nil {
		doxlog.Errorf("Could not find entry '%s' in %s '%s'", entName, catStyle, catName)
		return nil
	}
	nomos := picoWidget.NewInputField()
	nomos.Value = ent.Name
	nomos.Label = "Name"
	nomos.ReadOnly = true

	entPath := picoWidget.NewInputField()
	entPath.Value = ent.Path
	entPath.Label = "path"
	entPath.ReadOnly = true

	legal := picoWidget.NewInputField()
	legal.Value = ent.License
	if legal.Value == "" {
		legal.Value = "Unspecified"
	}
	legal.Label = "License type"
	legal.ReadOnly = true

	desc := picoWidget.NewInputField()
	desc.Value = ent.Desc
	desc.Label = "Description"
	desc.Type = "textarea"
	desc.ReadOnly = true

	genesis := picoWidget.NewInputField()
	genesis.Value = ent.Created.Format(time.ANSIC)
	genesis.Label = "Created on"
	genesis.ReadOnly = true

	lastChange := picoWidget.NewInputField()
	lastChange.Value = ent.Updated.Format(time.ANSIC)
	lastChange.Label = "Last Updated"
	lastChange.ReadOnly = true

	maint := picoWidget.NewInputField()
	maint.Value = ent.Maintainer
	maint.Label = "maintained by"
	maint.ReadOnly = true

	url := picoWidget.NewInputField()
	url.Value = "Copy Link"
	url.Type = "button"
	url.Class = "secondary"
	url.HtmxFields = []template.HTMLAttr{
		template.HTMLAttr(fmt.Sprintf(`onclick="navigator.clipboard.writeText(\"%s\")"`, ent.Url)),
		template.HTMLAttr(fmt.Sprintf(`title="%s"`, ent.Url)),
	}

	ver := picoWidget.NewInputField()
	ver.Label = "version hash"
	ver.Value = ent.Version
	ver.ReadOnly = true

	backB := picoWidget.NewInputField()
	backB.Type = "submit"
	backB.Value = "Done"
	backB.HtmxFields = []template.HTMLAttr{
		`hx-swap="outerHTML"`,
		`hx-target="#subscriptionManagerWidget"`,
		template.HTMLAttr(`hx-get="api/htmx/subscriptions/groups/` + catName + `"`),
	}

	wrapper := picoWidget.NewScrollArea()
	wrapper.Vh = 50
	wrapper.Contents = []picoWidget.Element{
		nomos, entPath, desc, genesis, lastChange, legal, maint, ver, url,
	}
	wrapper2 := picoWidget.NewUnorderedList()
	wrapper2.ID = listId
	wrapper2.Contents = []picoWidget.Element{wrapper, backB}
	return wrapper2
}

func (ss *subscriptionService) GetCatalogsTable(ctx context.Context, actionPrefix, id, userID string) *picoWidget.Table {
	return picoWidget.GetTestTable(false, false, false, false)
}

// normalForm is a deticated funciton so we can change it in the future
func normalForm(inputUrl string) string {
	chunks := strings.Split(strings.TrimPrefix(strings.TrimPrefix(inputUrl, "http://"), "https://"), "/")
	if len(chunks) < 2 {
		return inputUrl //TODO: error?
	}
	//host is chunk 1, group is chunk 2, and catalog.git is on the end
	return fmt.Sprintf("https://%s/%s/catalog.git", chunks[0], chunks[1])
}

func (ss *subscriptionService) downloadCatalog(url string, isInv bool, errIn error) (*catalog.Catalog, error) {
	//if it has .git go for it
	cat, err := catalog.LoadRemote(url, isInv)
	if err != nil {
		if normalForm(url) == url {
			if errIn == nil {
				errIn = fmt.Errorf("none")
			}
			return nil, fmt.Errorf("could not find catalog '%s': %v (additional errors: %w)", url, err, errIn)
		}
		return ss.downloadCatalog(normalForm(url), isInv, err)
	}
	return cat, nil
}

func (ss *subscriptionService) AddCustomCatalog(ctx context.Context, sessionID, url string) (data SubscriptionServiceAddCustomCatalog, resp RequestResponse, err error) {
	myHappyCatalog, err := ss.downloadCatalog(url, false, nil)
	if err != nil {
		resp.RequestStatus = Error
		resp.RequestMessages = append(resp.RequestMessages, err.Error())
		return SubscriptionServiceAddCustomCatalog{}, resp, err
	}
	ss.catMan.SaveCatalogToSubscribableList(myHappyCatalog)
	data.Name = myHappyCatalog.Name
	return data, resp, err
}
func (ss *subscriptionService) AddCustomInventory(ctx context.Context, sessionID, url string) (data SubscriptionServiceAddCustomCatalog, resp RequestResponse, err error) {
	myHappyCatalog, err := ss.downloadCatalog(url, true, nil)
	if err != nil {
		resp.RequestStatus = Error
		resp.RequestMessages = append(resp.RequestMessages, err.Error())
		return SubscriptionServiceAddCustomCatalog{}, resp, err
	}
	ss.catMan.SaveCatalog(dbPaths.SYS.InventoriesPath(), myHappyCatalog)
	data.Name = myHappyCatalog.Name
	return data, resp, err
}

func (ss *subscriptionService) Catalogs(ctx context.Context, sessionID string) ([]*Catalog, RequestResponse, error) {
	results := ss.catMan.GetCatalogs()
	converted := make([]*Catalog, 0, len(results))
	for _, realCat := range results {
		fakeCat := Catalog{
			Name:        realCat.Name,
			Version:     realCat.Version,
			Url:         realCat.URL,
			Maintainer:  realCat.Maintainer,
			Description: realCat.Desc,
		}
		converted = append(converted, &fakeCat)
	}
	return converted, RequestResponse{RequestStatus: OK}, nil
}
func (ss *subscriptionService) RemoveCustomCatalog(ctx context.Context, sessionID, customCatalogId string) (RequestResponse, error) {
	err := ss.catMan.RemoveCatalog(dbPaths.SYS.CatalogsPath(), customCatalogId, "")
	if err != nil {
		return RequestResponse{RequestStatus: Error, RequestMessages: []string{err.Error()}}, err
	}
	return RequestResponse{RequestStatus: OK}, nil
}
func (ss *subscriptionService) RemoveCustomInventory(ctx context.Context, sessionID, customCatalogId string) (RequestResponse, error) {
	err := ss.catMan.RemoveCatalog(dbPaths.SYS.InventoriesPath(), customCatalogId, "")
	if err != nil {
		return RequestResponse{RequestStatus: Error, RequestMessages: []string{err.Error()}}, err
	}
	return RequestResponse{RequestStatus: OK}, nil
}
func (ss *subscriptionService) Info(ctx context.Context, sessionID string) (SubscriptionServiceInfo, RequestResponse, error) {
	if app.Ctx.InternetAvailable {

	}
	cat := ss.catMan.GetPrimarySubscriptionCatalog()
	//TODO: we can access the fields
	if cat == nil {
		return SubscriptionServiceInfo{}, RequestResponse{RequestStatus: Error, RequestMessages: []string{"Not subscribed to anything"}}, fmt.Errorf("catalog subscription is nil")
	}
	return SubscriptionServiceInfo{}, RequestResponse{RequestStatus: OK}, nil
}
func (ss *subscriptionService) PreviousVersions(ctx context.Context, sessionID string) ([]string, RequestResponse, error) {
	cat := ss.catMan.GetPrimarySubscriptionCatalog()
	if cat == nil {
		return nil, RequestResponse{RequestStatus: Error, RequestMessages: []string{"Not subscribed to anything"}}, fmt.Errorf("catalog subscription is nil")
	}
	return ss.catMan.PreviousSubscribedVersions(cat.Name), RequestResponse{RequestStatus: OK}, nil
}
func (ss *subscriptionService) RevertToVersion(ctx context.Context, sessionID, versionId string) (RequestResponse, error) {
	//get the current catalog
	cat := ss.catMan.GetPrimarySubscriptionCatalog()
	if cat == nil {
		return RequestResponse{RequestStatus: Error, RequestMessages: []string{"Not subscribed to anything"}}, fmt.Errorf("catalog subscription is nil")
	}
	//TODO: download version if it doesn't exist locally
	tgtCat := ss.catMan.GetCatalogFromDb(ss.catMan.CatalogCollectionKP(), cat.Name, versionId)
	if tgtCat == nil {
		return RequestResponse{RequestStatus: Error, RequestMessages: []string{"Could not find that version of the catalog"}}, fmt.Errorf("can't find that version of catalog")
	}

	//now unsubscribe from the current and subscribe to target
	for _, ent := range cat.Entries {
		ss.catMan.MarkUnsubscribed(ent.Path)
	}
	for _, ent := range tgtCat.Entries {
		ss.catMan.MarkSubscribedAtVersion(ent.Path, tgtCat.Name, versionId, false, nil)
	}
	return RequestResponse{RequestStatus: OK}, nil
}
func (ss *subscriptionService) Subscribe(ctx context.Context, sessionID, catalogId string, isInv bool) (RequestResponse, error) {
	pat := dbPaths.SYS.CatalogsPath()
	if isInv {
		pat = dbPaths.SYS.InventoriesPath()
	}
	cat := ss.catMan.GetCatalogFromDb(pat, catalogId, "latest")
	if cat == nil {
		return RequestResponse{RequestStatus: Error, RequestMessages: []string{"Could not retrieve catalog"}}, fmt.Errorf("could not retrieve catalog")
	}
	for _, ent := range cat.Entries {
		if ent == nil {
			continue
		}
		ss.catMan.MarkSubscribed(ent.Path, cat.Name, isInv, nil)
		app.Ctx.SysManager.StoreCurrentProjectSettingByKey("ro", "catalog", cat.Name)
		app.Ctx.SysManager.StoreCurrentProjectSettingByKey("ro", "version", cat.Version)
	}
	return RequestResponse{RequestStatus: OK}, nil
}

func (ss *subscriptionService) ChangeSubscription(ctx context.Context, sessionID string, data SubscriptionSelectionData) (RequestResponse, error) {
	efficientMap := make(map[string]*struct{})
	ss.batchSubscribe(data.Inventories, efficientMap, true)
	ss.batchSubscribe(data.Catalogs, efficientMap, false)
	return RequestResponse{RequestStatus: OK}, nil
}

func (ss *subscriptionService) batchSubscribe(data []CatalogSelectionContents, efficientMap map[string]*struct{}, isInv bool) {
	for _, inv := range data {
		if inv.Contents == nil {
			continue
		}
		for _, ent := range inv.Contents {
			if ent.Name == "all" {
				if ent.Action == SelectSubscribe {
					kp := dbPaths.SYS.CatalogsPath()
					if isInv {
						kp = dbPaths.SYS.InventoriesPath()
					}
					ss.catMan.SubscribeAll(kp, inv.Name, "latest")
					go ss.SetFallbackTemplates(isInv)
				} else if ent.Action == SelectUnsubscribe {
					kp := dbPaths.SYS.CatalogsPath()
					if isInv {
						kp = dbPaths.SYS.InventoriesPath()
					}
					ss.catMan.UnsubscribeAll(kp, inv.Name)
				}
				continue
			}

			if ent.Action == SelectSubscribe {
				ss.catMan.MarkSubscribed(ent.Name, inv.Name, isInv, efficientMap)
				if ent.Name == "templates" {
					if len(ss.fallbackPaths) == 0 {
						go ss.SetFallbackTemplates(isInv)
					}
				}
			} else if ent.Action == SelectUnsubscribe {
				if ent.Name == "templates" {
					ent.Name = fmt.Sprintf("%s-%s", inv.Name, ent.Name)
				}
				ss.catMan.MarkUnsubscribed(ent.Name)
			}
		}
	}
}

func (ss *subscriptionService) Unsubscribe(ctx context.Context, sessionID string) (RequestResponse, error) {
	if ss.uiState.catalogSelected == "" {
		return RequestResponse{RequestStatus: InputError}, fmt.Errorf("no selection")
	}
	catPat := dbPaths.SYS.CatalogsPath()
	if ss.uiState.isInventory {
		catPat = dbPaths.SYS.InventoriesPath()
	}
	cat := ss.catMan.GetCatalogFromDb(catPat, ss.uiState.catalogSelected, "latest")
	if cat == nil || cat.Entries == nil {
		return RequestResponse{RequestStatus: Error, RequestMessages: []string{"Not subscribed to anything"}}, fmt.Errorf("catalog subscription is nil")
	}
	for _, ent := range cat.Entries {
		if ent == nil {
			continue
		}
		if ent.Name == "templates" {
			ent.Name = cat.Name + "-templates"
		}
		ss.catMan.MarkUnsubscribed(ent.Path)
	}
	return RequestResponse{RequestStatus: OK}, nil
}

func (ss *subscriptionService) SetFallbackTemplates(inventory bool) {
	//TODO: user customizable list?
	//determine catalog selected
	if ss.uiState.catalogSelected == "" {
		return
	}
	all := addDependencyPathToStack(app.Ctx.CatalogManager, ss.uiState.catalogSelected, make(map[string]bool), inventory)
	solved := make([]string, len(all))
	for i, inst := range all {
		solved[i] = path.Join(app.Ctx.Paths.SubscriptionsPath, inst, config.TemplatesDir)
	}
	//solved gets copied in reverse order
	ss.fallbackPaths = make([]string, len(solved))
	j := 0
	for i := len(solved) - 1; i >= 0; i-- {
		ss.fallbackPaths[j] = solved[i]
		j++
	}
	go app.Ctx.SysManager.StoreSliceAtPath(dbPaths.SYS.FallbackPriorityPath(), ss.fallbackPaths)
}

func addDependencyPathToStack(catMan *catalog.Manager, cat string, catMap map[string]bool, isInv bool) []string {
	kp := dbPaths.SYS.CatalogsPath()
	if isInv {
		kp = dbPaths.SYS.InventoriesPath()
	}
	cat = strings.TrimSuffix(cat, ":templates")
	templ := app.Ctx.CatalogManager.GetEntryFromDb(kp, config.TemplatesDir, cat, "latest")
	if templ == nil {
		kp = dbPaths.SYS.InventoriesPath()
		if isInv {
			kp = dbPaths.SYS.CatalogsPath()
		}
		templ = app.Ctx.CatalogManager.GetEntryFromDb(kp, config.TemplatesDir, cat, "latest")
		if templ == nil {
			return nil
		}
	}

	retVal := make([]string, 0)
	retVal = append(retVal, cat)
	catMap[cat] = true

	for _, dep := range templ.Dependencies {
		if catMap[dep] {
			continue
		}
		retVal = append(retVal, addDependencyPathToStack(catMan, dep, catMap, isInv)...)
	}
	return retVal
}

func (ss *subscriptionService) getFallbackCatalog() string {
	stack := app.Ctx.SysManager.LoadSliceFromPath(dbPaths.SYS.FallbackPriorityPath())
	if len(stack) < 1 {
		return ""
	}
	return path.Base(path.Dir(stack[0]))
}

type fallbackCatalogAction struct {
	itemId string
}

func (p *fallbackCatalogAction) BsIcon() string {
	if p.itemId == "templates" {
		return "bi-layers"
	}
	return ""
}

func (p *fallbackCatalogAction) Class() string {
	return ""
}

func (p *fallbackCatalogAction) HtmxFields() []template.HTMLAttr {
	return []template.HTMLAttr{
		`title="This is your fallback"`,
	}
}

func (p *fallbackCatalogAction) SetEndpoint(endpoint, method string) {
}

func (p *fallbackCatalogAction) SetListContext(listId, itemId string) {
	p.itemId = itemId
}

func (p *fallbackCatalogAction) Copy() picoWidget.Action {
	return new(fallbackCatalogAction)
}

func (ss *subscriptionService) GetFallbackPaths() []string {
	ss.fallbackPaths = app.Ctx.SysManager.LoadSliceFromPath(dbPaths.SYS.FallbackPriorityPath())
	return ss.fallbackPaths
}

func (ss *subscriptionService) SetFallbackPaths(newPaths []string) {
	ss.fallbackPaths = newPaths
	go app.Ctx.SysManager.StoreSliceAtPath(dbPaths.SYS.FallbackPriorityPath(), newPaths)
}

func (ss *subscriptionService) GetFallbackPriorityStatus(node string) bool {
	for _, path := range ss.fallbackPaths {
		if strings.Contains(path, node) {
			return true
		}
	}
	return false
}

func (ss *subscriptionService) SetFallbackPriorityStatus(node string, status bool) {
	if status {
		// Add node to fallback paths if not already present
		found := false
		for _, path := range ss.fallbackPaths {
			if strings.Contains(path, node) {
				found = true
				break
			}
		}
		if !found {
			ss.fallbackPaths = append(ss.fallbackPaths, path.Join(app.Ctx.Paths.SubscriptionsPath, node, config.TemplatesDir))
		}
	} else {
		// Remove node from fallback paths
		newPaths := make([]string, 0)
		for _, path := range ss.fallbackPaths {
			if !strings.Contains(path, node) {
				newPaths = append(newPaths, path)
			}
		}
		ss.fallbackPaths = newPaths
	}
	go app.Ctx.SysManager.StoreSliceAtPath(dbPaths.SYS.FallbackPriorityPath(), ss.fallbackPaths)
}

func (ss *subscriptionService) RefreshCatalogs() (RequestResponse, error) {
	ss.catMan.RefreshCatalogs()
	return RequestResponse{RequestStatus: OK}, nil
}
