package api

import (
	"context"
	"crypto/sha256"
	"fmt"
	"github.com/liturgiko/doxa/app"
	"github.com/liturgiko/doxa/pkg/dvcs"
	"github.com/liturgiko/doxa/pkg/users"
	picoWidget "github.com/liturgiko/doxa/templates/webApp/structs/widget"
	"html/template"
	"strings"
)

type tokenService struct {
	State             TokenServiceEncryptionStatus
	tokenErrorMessage string
}

type TokenServiceEncryptionStatus byte
type TokenServiceValidationStatus byte

const (
	TokenNeeded TokenServiceEncryptionStatus = iota
	PasswordNeeded
	Decrypted
	DecryptionError
	TokenError //used for expired token
)
const (
	ValidToken TokenServiceValidationStatus = iota
	InvalidToken
	ExpiredToken
	RevokedToken
	UnknownError TokenServiceValidationStatus = 0xFF
)

const (
	SetToken     FormAction = "set"
	DecryptToken FormAction = "decrypt"
	EncryptToken FormAction = "encrypt"
)

func (s TokenServiceEncryptionStatus) String() string {
	switch s {
	case Decrypted:
		return "unlocked"
	case TokenNeeded:
		return "missing token"
	case PasswordNeeded:
		return "need password"
	case DecryptionError:
		return "unlocking error"
	case TokenError:
		return "token no longer valid"
	}
	return "unknown"
}

func NewTokenService() TokenService {
	return &tokenService{}
}

// GET methods

func (s *tokenService) GetWidget(ctx context.Context, actionPrefix, id, userID string) *picoWidget.Form {
	s.State, _, _ = s.GetTokenStatus(ctx, userID)
	t, d := s.getTitleAndDesc()
	f := picoWidget.NewForm(id, t)
	f.FormMethod = s.getMethod()
	f.FormAction = fmt.Sprintf("%s%s", actionPrefix, s.getNextAction())
	f.Description = d
	f.Fields = s.getFields(id)
	f.Actions = s.getActions(id)
	return f
}

func (s *tokenService) GetTokenStatus(ctx context.Context, userID string) (TokenServiceEncryptionStatus, RequestResponse, error) {
	if userID == "" {
		userID = users.LocalUser
	}
	if s.State == DecryptionError || s.State == TokenError {
		return s.State, RequestResponse{RequestStatus: Error, RequestMessages: []string{"Failed to retrieve token", "see log for more details"}}, fmt.Errorf("could not decrypt token. see log for more details")
	}
	hasPassword := app.Ctx.UserManager.Vault.IsInKeyRing(userID)
	hasToken := app.Ctx.UserManager.HasToken(userID)
	if hasPassword && hasToken {
		/* just because there is a password, don't mean it's the right one */
		_, err := app.Ctx.UserManager.GetToken(userID)
		if err != nil {
			return DecryptionError, RequestResponse{RequestStatus: Error}, err
		}
		return Decrypted, RequestResponse{RequestStatus: OK}, nil
	}
	if hasToken {
		return PasswordNeeded, RequestResponse{RequestStatus: OK, RequestMessages: []string{"password needed"}}, nil
	}
	return TokenNeeded, RequestResponse{RequestStatus: OK, RequestMessages: []string{"token required"}}, nil
}

// PUT methods

func (s *tokenService) SetToken(ctx context.Context, userId string, key, token string) (response RequestResponse, err error) {
	if userId == "" {
		userId = users.LocalUser
	}
	app.Ctx.UserManager.Vault.AddKey(userId, sha256.Sum256([]byte(key)))
	app.Ctx.Bus.Publish("gitlab:tokenSet", token)
	err = app.Ctx.UserManager.SetToken(userId, token)
	if err != nil {
		return RequestResponse{RequestStatus: Error, RequestMessages: []string{err.Error()}}, err
	}
	s.State = Decrypted
	return RequestResponse{RequestStatus: OK}, nil
}

// POST methods

func (s *tokenService) DecryptToken(ctx context.Context, userId string, key string) (response RequestResponse, err error) {
	if userId == "" {
		userId = users.LocalUser
	}
	app.Ctx.UserManager.Vault.AddKey(userId, sha256.Sum256([]byte(key)))
	token, err := app.Ctx.UserManager.GetToken(userId)
	if err != nil || token == "" {
		app.Ctx.UserManager.Vault.RevokeKey(userId)
		s.State = DecryptionError
		return RequestResponse{RequestStatus: InputError}, nil //err
	}
	if tkErr := dvcs.TokenError(token, app.Ctx.GitlabApi); tkErr != nil {
		s.State = TokenError
		s.tokenErrorMessage = tkErr.Error()
		return RequestResponse{RequestStatus: Error, RequestMessages: []string{tkErr.Error()}}, tkErr
	}
	s.State = Decrypted
	app.Ctx.Bus.Publish("gitlab:tokenSet", token)
	return RequestResponse{RequestStatus: OK}, nil
}

func (s *tokenService) EncryptToken(ctx context.Context, userId string) (response RequestResponse, err error) {
	if userId == "" {
		userId = users.LocalUser
	}
	app.Ctx.UserManager.Vault.RevokeKey(userId)
	app.Ctx.UserManager.IsDecrypted = false
	go app.Ctx.Bus.Publish("gitlab:tokenRevoked")
	s.State = PasswordNeeded
	response.RequestStatus = OK
	return response, nil
}

func (s *tokenService) UpdateToken(ctx context.Context, userId string, key, token string) (RequestResponse, error) {
	if userId == "" {
		userId = users.LocalUser
	}
	app.Ctx.UserManager.Vault.AddKey(userId, sha256.Sum256([]byte(key)))
	err := app.Ctx.UserManager.SetToken(userId, token)
	if err != nil {
		return RequestResponse{RequestStatus: Error}, err
	}
	s.State = Decrypted
	return RequestResponse{RequestStatus: OK}, nil
}

func (s *tokenService) ValidateToken(ctx context.Context, userId string, token string) (validity TokenServiceValidationStatus, resp RequestResponse, err error) {
	valid, reason, err := app.Ctx.UserManager.ValidateToken(token, app.Ctx.GitlabApi)
	if err != nil {
		return UnknownError, RequestResponse{RequestStatus: Error}, err
	}
	if valid {
		return ValidToken, RequestResponse{RequestStatus: OK}, nil
	}
	switch reason {
	case users.ValidToken:
		return ValidToken, RequestResponse{RequestStatus: OK}, nil
	case users.InvalidToken:
		return InvalidToken, RequestResponse{RequestStatus: OK}, nil
	case users.ExpiredToken:
		return ExpiredToken, RequestResponse{RequestStatus: OK}, nil
	case users.RevokedToken:
		return RevokedToken, RequestResponse{RequestStatus: OK}, nil
	case users.Unknown:
		return UnknownError, RequestResponse{RequestStatus: Error}, err
	}
	return UnknownError, RequestResponse{RequestStatus: Error}, err
}

// DELETE methods

func (s *tokenService) DeleteToken(ctx context.Context, userId string, key string) (response RequestResponse, err error) {
	if userId == "" {
		userId = users.LocalUser
	}
	err = app.Ctx.UserManager.RemoveToken(userId)
	//if this is incorrect it will be reset by GetTokenStatus
	s.State = TokenNeeded
	if err != nil {
		return RequestResponse{RequestStatus: Error}, err
	}
	return RequestResponse{RequestStatus: OK}, nil
}

// Form helper methods

func (s *tokenService) getMethod() string {
	switch s.State {
	case TokenNeeded, TokenError:
		return PUT
	case PasswordNeeded, DecryptionError:
		return POST
	case Decrypted:
		return POST
	default:
		return GET //should never reach here
	}
}

func (s *tokenService) getNextAction() FormAction {
	switch s.State {
	case TokenNeeded, TokenError:
		return SetToken
	case PasswordNeeded, DecryptionError:
		return DecryptToken
	case Decrypted:
		return EncryptToken
	default:
		return ""
	}
}

// func (s tokenService) setState(ctx context.Context, id, userID string) {
// }
func (s *tokenService) getTitleAndDesc() (title, desc string) {
	switch s.State {
	case Decrypted:
		title = "Unlocked"
		desc = "Your token is unlocked, which allows DOXA to use it to access your Gitlab Project Group. This allows DOXA to back up your project files and manage subgroups and repositories.  If you do not want DOXA to have access to your token, you can do so by clicking the 'Lock Token' button.  If you want to delete your token, click the 'Delete Token' button. After it is deleted, you can provide it again to DOXA in the future to be stored securely in the DOXA vault. "
	case TokenNeeded:
		title = "Authorization Needed"
		desc = "If you want DOXA to have access to your Gitlab Project Group, you need to authorize it. To do so, enter your token and a password, then click the 'Authorize DOXA' button."
	case DecryptionError, PasswordNeeded:
		title = "Password Needed"
		desc = "Your Gitlab access token is currently locked, and cannot be used by DOXA.  To unlock it, enter your password and click the 'Unlock Token' button. This will allow DOXA to use your token to access your Gitlab Project Group."
	case TokenError:
		title = "Token Rejected"
		desc = fmt.Sprintf("Gitlab did not accept your token (%s)", s.tokenErrorMessage)
		if strings.Contains(s.tokenErrorMessage, "expired") {
			title = "Token Expired"
			desc = "Your token expired after the last time you allowed DOXA to use it.  If you want DOXA to have access to your project's Gitlab group, you must get a new token, copy it, and enter it below."
		} else if strings.Contains(s.tokenErrorMessage, "revoked") {
			title = "Token Revoked"
			desc = "Somebody (hopefully you) revoked your gitlab token. You need to enter a new token, or re-authorize your previous one"
		}
	default:
		title = "Authorization Needed"
		desc = "If you want DOXA to have access to your Gitlab Project Group, you need to authorize it. Enter your token and a password, then click the 'Authorize DOXA' button."
	}
	return
}
func (s *tokenService) getFields(idSuffix string) (fields []picoWidget.Element) {

	switch s.State {
	case Decrypted:
	case TokenNeeded, TokenError:
		fields = append(fields, s.getTokenField(idSuffix, "", true, false))
		fields = append(fields, s.getPasswordField(idSuffix, "", true, false))
	case PasswordNeeded, DecryptionError:
		fields = append(fields, s.getPasswordField(idSuffix, "", true, false))
	default:
		fields = append(fields, s.getTokenField(idSuffix, "", true, false))
		fields = append(fields, s.getPasswordField(idSuffix, "", true, false))
	}
	return
}
func (s *tokenService) getActions(id string) (fields []*picoWidget.InputField) {
	actionsId := fmt.Sprintf("%s-actions", id)
	switch s.State {
	case Decrypted:
		fields = append(fields, s.getActionField(actionsId, "Lock Token"), s.getDeleteField(actionsId))
	case TokenNeeded, TokenError:
		fields = append(fields, s.getActionField(actionsId, "Authorize DOXA"))
	case PasswordNeeded, DecryptionError:
		fields = append(fields, s.getActionField(actionsId, "Unlock Token"), s.getDeleteField(actionsId))
	default:
		fields = append(fields, s.getActionField(actionsId, "Authorize DOXA"))
	}
	return
}
func (s *tokenService) getActionField(id, value string) (a *picoWidget.InputField) {
	a = &picoWidget.InputField{
		ID:                 fmt.Sprintf("action-%s", id),
		Class:              "",
		Type:               "submit",
		Name:               "",
		PlaceHolder:        "",
		Required:           false,
		Disabled:           false,
		Value:              value,
		AriaDescribedBy:    "",
		AriaLabel:          "",
		AriaInvalid:        false,
		Helper:             picoWidget.Helper{},
		ValidationFeedback: nil,
		ShowLoading:        false,
		LoadingText:        "",
	}
	return
}

func (s *tokenService) getDeleteField(id string) *picoWidget.InputField {
	if id == "" {
		id = "tokenService-delete-field"
	}
	return &picoWidget.InputField{
		ID:    id,
		Type:  "button",
		Class: "secondary button",
		HtmxFields: []template.HTMLAttr{
			`hx-delete="/api/htmx/token/delete"`,
			`hx-confirm="Click OK to delete your token. It will no longer be stored on your computer. You can add your token again in the future. Click Cancel to cancel the delete operation."`,
			`hx-target="#formMessages"`,
		},
		Value: "Delete Token",
	}
}

func (s *tokenService) getTokenField(idSuffix, value string, required, disabled bool) (f *picoWidget.InputField) {
	id := fmt.Sprintf("field-token-%s", idSuffix)
	// input for token
	helperId := fmt.Sprintf("%s-helper", id)
	validationId := fmt.Sprintf("%s-validator", id)
	f = &picoWidget.InputField{
		ID:              id,
		Type:            picoWidget.WidgetInputTypePassword,
		Label:           "Gitlab Token",
		Name:            "token",
		PlaceHolder:     "token",
		Required:        required,
		Disabled:        disabled,
		Value:           value,
		AriaDescribedBy: fmt.Sprintf("%s %s", helperId, validationId),
		AriaLabel:       "Gitlab token to allow DOXA access to your Gitlab Group",
		Helper: picoWidget.Helper{
			ID:   helperId,
			Text: "DOXA saves your token in an locked vault on your computer. DOXA uses your token to manage subgroups and repositories in your Gitlab Project Group and to back up project files.",
		},
		ValidationFeedback: nil,
		HtmxFields: []template.HTMLAttr{
			`hx-post = "api/htmx/token/validate"`,
			`hx-trigger = "blur"`,
			`hx-target = "closest label"`,
			`hx-swap = "outerHTML"`,
		},
	}
	f.ElementType = f.ET()
	return
}
func (s *tokenService) getPasswordField(idSuffix, value string, required, disabled bool) (f *picoWidget.InputField) {
	fieldId := fmt.Sprintf("field-password-%s", idSuffix)
	// input for token
	helperId := fmt.Sprintf("%s-helper", fieldId)
	validationId := fmt.Sprintf("%s-validator", fieldId)
	var validationFeedback *picoWidget.ValidationFeedback
	if s.State == DecryptionError {
		validationFeedback = &picoWidget.ValidationFeedback{
			ID:        validationId,
			Message:   "Could not unlock token with password you entered. Please try again.",
			IsInvalid: true,
		}
	}
	f = &picoWidget.InputField{
		Type:            picoWidget.WidgetInputTypePassword,
		Label:           "Password",
		Name:            "password",
		PlaceHolder:     "password",
		Required:        required,
		Disabled:        disabled,
		Value:           value,
		AriaDescribedBy: fmt.Sprintf("%s %s", helperId, validationId),
		AriaLabel:       "password used to lock or unlock your Gitlab token",
		Helper: picoWidget.Helper{
			ID:   helperId,
			Text: "DOXA does not save your password.  Each time DOXA starts, you have to provide your password for DOXA to use as the key to unlock your token. The technical terms for lock and unlock are 'encrypt' and 'decrypt'.",
		},
		ValidationFeedback: validationFeedback,
	}
	f.ElementType = f.ET()
	return
}
