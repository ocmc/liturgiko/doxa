package api

import (
	"github.com/liturgiko/doxa/app"
)

var Manager StatusServiceManager

type StatusServiceManager struct {
}

func NewStatusServiceManager() *StatusServiceManager {
	return &StatusServiceManager{}
}

func (m *StatusServiceManager) InternetOk() bool {
	return app.Ctx.InternetAvailable //netStatus.InternetAvailable()
}
