package api

import (
	"github.com/liturgiko/doxa/app"
	"github.com/liturgiko/doxa/pkg/users"
	"os"
	"testing"
)

func Test_rwSyncService_Details(t *testing.T) {
	// create the object
	rw := NewRwSyncService()
	// populate status
	stat, resp, err := rw.Details(nil, "")
	if resp.RequestStatus != OK {
		t.Fatalf("%v", err)
	}
	if rw.(*rwSyncService).State != RwNeedsToken {
		t.Fatalf("Token needed, but got %d instead", rw.(*rwSyncService).State)
	}
	//TODO how to handle this
	if stat.NumTotal > 0 {

	}
	// should be: needs token
	// prompt for token
	tok := os.Getenv("gitlab-token")
	app.Ctx.Bus.Publish("gitlab:tokenSet", tok)
	app.Ctx.UserManager.SetToken(users.LocalUser, tok)
	stat, resp, err = rw.Details(nil, "")
	if resp.RequestStatus != OK {
		t.Fatalf("%v", err)
	}
	if rw.(*rwSyncService).State != RwNotSyncing {
		t.Fatalf("expected RwNotSyncing, but got %d instead", rw.(*rwSyncService).State)
	}
	//
	// start sync
	//resp, err = rw.SyncAll(nil, "")
	if resp.RequestStatus != OK {
		t.Fatalf("%v", err)
	}
	stat, resp, err = rw.Details(nil, "")
	if resp.RequestStatus != OK {
		t.Fatalf("%v", err)
	}
	if rw.(*rwSyncService).State != RwSyncInProgress {
		t.Fatalf("expected RwSyncInProgress, but got %d instead", rw.(*rwSyncService).State)
	}
	// populate status
	//
}
