package api

import (
	"context"
	"errors"
	"sync"
	"time"
)

type Session struct {
	ID           string
	LanguageCode string
	UserID       string
	CreatedAt    time.Time
	ExpiresAt    time.Time
}
type sessionService struct {
	sessions map[string]*Session
	mu       sync.RWMutex
}

func NewSessionService() SessionService {
	return &sessionService{
		sessions: make(map[string]*Session),
	}
}

func (s *sessionService) Create(ctx context.Context, userID string, duration time.Duration) (*Session, error) {
	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	default:
	}

	s.mu.Lock()
	defer s.mu.Unlock()

	session := &Session{
		ID:        generateUniqueID(), // Implement this function to generate a unique ID
		UserID:    userID,
		CreatedAt: time.Now(),
		ExpiresAt: time.Now().Add(duration),
	}

	s.sessions[session.ID] = session
	return session, nil
}

func (s *sessionService) Get(ctx context.Context, sessionID string) (*Session, error) {
	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	default:
	}

	s.mu.RLock()
	defer s.mu.RUnlock()

	session, exists := s.sessions[sessionID]
	if !exists {
		return nil, errors.New("session not found")
	}

	if time.Now().After(session.ExpiresAt) {
		delete(s.sessions, sessionID)
		return nil, errors.New("session expired")
	}

	return session, nil
}

func (s *sessionService) Delete(ctx context.Context, sessionID string) error {
	select {
	case <-ctx.Done():
		return ctx.Err()
	default:
	}

	s.mu.Lock()
	defer s.mu.Unlock()

	delete(s.sessions, sessionID)
	return nil
}

func (s *sessionService) DeleteExpired(ctx context.Context) error {
	select {
	case <-ctx.Done():
		return ctx.Err()
	default:
	}

	s.mu.Lock()
	defer s.mu.Unlock()

	now := time.Now()
	for id, session := range s.sessions {
		if now.After(session.ExpiresAt) {
			delete(s.sessions, id)
		}
	}

	return nil
}

func generateUniqueID() string {
	// Implement a function to generate a unique ID
	// This could use a UUID library or any other method to ensure uniqueness
	return "unique-id" // Placeholder
}
