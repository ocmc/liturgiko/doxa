package api

import (
	"embed"
	"errors"
	"flag"
	"fmt"
	"github.com/asaskevich/EventBus"
	"github.com/liturgiko/doxa/app"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/catalog"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/dvcs"
	"github.com/liturgiko/doxa/pkg/enums/goos"
	"github.com/liturgiko/doxa/pkg/enums/properties"
	"github.com/liturgiko/doxa/pkg/keyring"
	"github.com/liturgiko/doxa/pkg/layouts"
	"github.com/liturgiko/doxa/pkg/site"
	"github.com/liturgiko/doxa/pkg/syncsrvc"
	"github.com/liturgiko/doxa/pkg/sysManager"
	"github.com/liturgiko/doxa/pkg/users"
	"github.com/liturgiko/doxa/pkg/vault"
	"github.com/xanzy/go-gitlab"
	"math/rand"
	"net/http"
	"os"
	"path"
	"runtime"
	"strings"
	"sync"
	"testing"
	"time"
)

//The following are from main.go:

// VERSION For a new release, set version number and list issues addressed, found at https://gitlab.com/ocmc/liturgiko/doxa/-/issues
// also do the same in xcmd/releaser/release.yaml
var VERSION = "v0.130.18" //

var siteBuilder *site.Builder
var GLORY string
var MediaEnabled bool
var MediaUrlAudio string
var MediaUrlPdf string
var PdfViewer string
var RecEditor string
var SysDbPath string

func TestMain(m *testing.M) {
	var (
		skipInit = flag.Bool("basic", false, "skip doxa initialization")
		//startServers   = flag.Bool("servers", false, "start servers")
		//startCLI       = flag.Bool("cli", false, "start doxa shell")
		useCurrentProj = flag.Bool("use-current", true, "use the current doxa-project instead of creating a new one")
		altHome        = "" //os.MkdirTemp()
		altProject     = ""
	)

	var err error

	//setup
	if !*skipInit {
		//first make an alt home directory for ease of setup
		if !*useCurrentProj {
			//altHome, err = os.MkdirTemp(".", "doxa-tmphome-*")
			//defer os.Remove(altHome)
		}
		defer os.Remove(SysDbPath)
		doxlog.Init(altHome, altProject, false)
		defer func() {
			err = doxlog.LogFile.Close()
			if err != nil {
				fmt.Printf("error closing log: %v", err)
			}
		}()
		InitApp()
		defer app.CloseDatabase()
		setGlobals()
		layouts.Init()
		err = LoadKeyRing(app.Ctx.ConfigName)
		if err != nil {
			doxlog.Errorf("%s", err)
		}
		siteBuilder, err = site.NewBuilder(VERSION,
			false,
			app.Ctx.Kvs,
			app.Ctx.Realm,
			MediaUrlAudio, // TODO: what about media enabled?
			MediaUrlPdf,
			PdfViewer,
			app.Ctx.Project,
			config.SM,
			embed.FS{},
			embed.FS{},
			nil,   // will be set by the server
			false, // will be set by the server
			app.Ctx.Keyring,
			&app.Ctx.BuildSyncMutex,
			app.Ctx.Bus,
		)

	}
	TESTING_RESULT := m.Run()
	if !*skipInit {

	}
	//teardown
	os.Exit(TESTING_RESULT)
}

func setGlobals() {
	app.Ctx.PortApp = "8080" //config.SM.StringProps[properties.SysServerPortsDoxa]
	app.Ctx.PortSitePublic = config.SM.StringProps[properties.SysServerPortsPublicSite]
	app.Ctx.PortSiteTest = config.SM.StringProps[properties.SysServerPortsTestSite]
	app.Ctx.PortApi = config.SM.StringProps[properties.SysServerPortsApi]
	app.Ctx.PortMedia = config.SM.StringProps[properties.SysServerPortsMedia]
	app.Ctx.PortMessage = config.SM.StringProps[properties.SysServerPortsMessages]
	app.Ctx.Paths.MediaDirPath = config.SM.StringProps[properties.SiteMediaDir]
	// TODO: the media urls being passed to the servers means
	// they can't be dynamically changed once a server starts.
	// This is problematic.
	MediaEnabled = config.SM.BoolProps[properties.SiteMediaEnabled]
	// Media URL Audio
	MediaUrlAudio = config.SM.StringProps[properties.SiteMediaAudioUrl]
	if len(MediaUrlAudio) == 0 {
		MediaUrlAudio = "http://localhost"
	}
	if strings.Contains(MediaUrlAudio, "localhost") {
		MediaUrlAudio = MediaUrlAudio + ":" + app.Ctx.PortMedia
	}
	// media URL PDF
	MediaUrlPdf = config.SM.StringProps[properties.SiteMediaPdfUrl]
	if len(MediaUrlPdf) == 0 {
		MediaUrlPdf = "http://localhost"
	}
	if strings.Contains(MediaUrlPdf, "localhost") {
		MediaUrlPdf = MediaUrlPdf + ":" + app.Ctx.PortMedia
	}

	PdfViewer = config.SM.StringProps[properties.SiteMediaPdfViewer]

	// glory languages
	gloryLangs := config.SM.StringSliceProps[properties.SysGloryLanguages]
	gloryIndex := config.SM.IntProps[properties.SysGloryIndex]
	// convert to be zero based for use with the glory slice.
	if gloryIndex > 0 {
		gloryIndex--
	}
	if gloryIndex > len(gloryLangs) {
		fmt.Println("The glory index is greater than the number of languages.  The index starts with 0.")
	} else {
		if gloryLangs != nil {
			GLORY = gloryLangs[gloryIndex]
		}
	}
	RecEditor = config.SM.StringProps[properties.SysShellDbEditor]

	app.Ctx.GoOS = goos.CodeForString(runtime.GOOS)
	doxlog.Infof("runtime OS is %s", runtime.GOOS)
}

func LoadKeyRing(configName string) error {
	titlesPath := path.Join("configs", configName, properties.SiteBuildPagesTitles.Data().Path)
	kr := keyring.NewKeyRing(app.Ctx.Kvs, titlesPath)
	doxlog.Info("Building keyring")
	err := kr.Build()
	if err != nil {
		msg := fmt.Sprintf("error creating keyring: %v", err)
		doxlog.Error(msg)
		return fmt.Errorf(msg)
	}
	app.Ctx.Keyring = kr
	app.Ctx.SyncManager.SetKeyring(kr)
	return nil
}
func InitApp() {
	// set the port numbers
	app.Ctx.PortApp = "8080"        // overwritten by config setting in database
	app.Ctx.PortSiteTest = "8085"   // overwritten by config setting in database
	app.Ctx.PortSitePublic = "8086" // overwritten by config setting in database
	app.Ctx.PortMedia = "8095"      // overwritten by config setting in database
	app.Ctx.PortMessage = "9000"    // overwritten by config setting in database
	inDockerStr := os.Getenv("IN_DOCKER")
	if inDockerStr == "true" {
		app.Ctx.Docker = true
		doxlog.Info("running in docker")
	}
	// shutdown any open instance of doxa.
	// we can only have a single process accessing the database.
	shutdown()
	app.Ctx.Bus = EventBus.New()
	initDoxaPaths()
	//checkSeed()
	app.Ctx.Bus.Subscribe("gitlab:tokenSet", func(token string) {
		if app.Ctx.UserManager != nil {
			prevToken, _ := app.Ctx.UserManager.GetToken(users.LocalUser)
			if prevToken != token {
				if app.Ctx.UserManager.SetToken(users.LocalUser, token) == nil {
				}
			}
			app.Ctx.UserManager.IsDecrypted = true
		}
	})
	app.Ctx.OnlineInterval = 5 * time.Minute
	app.Ctx.OfflineInterval = 1 * time.Minute
	app.StartInternetChecker()
	// create the vault manager
	vaultManager, err := vault.NewManager(app.Ctx.Paths.VaultPath)
	if err != nil {
		doxlog.Error(fmt.Sprintf("error opening vault: %v", err))
	}
	// create the map of data stores
	app.Ctx.DataStores = kvs.NewStores()

	// create the sys manager
	err = initSysDb()
	if err != nil {
		doxlog.Error(fmt.Sprintf("error creating system database: %v", err))
	}
	// create the user manager
	app.Ctx.UserManager, err = users.NewManager(app.Ctx.Paths.VaultPath, vaultManager)
	if err != nil {
		doxlog.Error(fmt.Sprintf("error creating user manager: %v", err))
	}
	err = app.InitDatabase()
	PopulateDb()
	err = initializeHelpers()
	if err != nil {
		doxlog.Panic(err.Error())
	}

	myDvcs, err := dvcs.NewDvcsClientLocalOnly("assets", app.Ctx.Paths.ProjectDirPath)
	if err != nil {
		doxlog.Panic(err.Error())
	}
	//deleteObsoleteProperties()
	//err = initializeHelpers()
	//if err != nil {
	//	doxlog.Panic(err.Error())
	//

	myDvcs, err = dvcs.NewDvcsClientLocalOnly("assets", app.Ctx.Paths.ProjectDirPath)
	if err != nil {
		doxlog.Errorf("Could not create local git client: %v", err)
		doxlog.Info("NewDvcsClientLocalOnly failed. attempting Manual creation. Sync will be disabled")
		myDvcs = new(dvcs.DVCS)
		myDvcs.Git = new(dvcs.Git)
		myDvcs.Gitlab = new(dvcs.Gitlab) //this one needs more to prevent abend.
		myDvcs.Gitlab.Mu = &sync.Mutex{}
	}
	app.Ctx.Bus.SubscribeAsync("gitlab:tokenSet", func(token string) {
		if !app.Ctx.InternetAvailable {
			var netwatcher func()
			netwatcher = func() {
				initGitlab(token, myDvcs)
				app.Ctx.Bus.Unsubscribe("net:available", netwatcher)
			}
			app.Ctx.Bus.SubscribeAsync("net:available", netwatcher, false)
			return //TODO: listen for internet to become available
		}
		initGitlab(token, myDvcs)

	}, false)

	ds := app.Ctx.DataStores.Get(config.SystemDbName)
	app.Ctx.SyncManager = syncsrvc.NewManager(&syncsrvc.ManagerParms{
		Ds:           app.Ctx.Wrapper,
		Dvcs:         myDvcs,
		Keyring:      app.Ctx.Keyring,
		Paths:        app.Ctx.Paths,
		Pm:           app.Ctx.PM,
		Token:        "",
		RoAutoSyncOn: app.Ctx.RoAutoSyncRequestedInConfig,
		RwAutoSyncOn: app.Ctx.RwAutoSyncRequestedInConfig,
		SysDs:        ds.Kvs,
		LtxDs:        app.Ctx.Kvs,
		Bus:          app.Ctx.Bus,
		BuilderMutex: &app.Ctx.BuildSyncMutex,
	})
	//initialize catalogManager
	app.Ctx.CatalogManager, err = catalog.NewManager(app.Ctx.DataStores.Get(config.SystemDbName).Kvs, app.Ctx.Bus, myDvcs)
	if err != nil {
		doxlog.Errorf("Could not create catalog manager: %v", err)
	} else {
		//Ctx.CatCmd = cli.NewCatCmd(Ctx.CatalogManager)
		cata := app.Ctx.CatalogManager.GetPrimarySubscriptionCatalog()
		if cata != nil {
			if cata.Name != "" {
				config.SM.SetValueByType(properties.SiteBuildTemplatesFallbackDir, cata.Name)
				config.SM.SetValueByType(properties.SiteBuildTemplatesFallbackEnabled, "true")
			}
		}
	}
}

func shutdown() {
	doxlog.Info("Shutting down any previous local instance...")
	_, err := http.Get(fmt.Sprintf("http://127.0.0.1:%s/quit", app.Ctx.PortApp))
	if err != nil {
		doxlog.Info("No local instance running.")
	} else {
		doxlog.Info("Local instance found and stopped.")
	}
	time.Sleep(2 * time.Second)
}

func initDoxaPaths() {
	app.Ctx.UserHome, _ = os.MkdirTemp(".", "testing*")
	err := config.InitDoxaPaths("gitlab.com", app.Ctx.UserHome, app.Ctx.DoxaHome, app.Ctx.Project)
	if err != nil {
		doxlog.Panic(fmt.Sprintf("error setting doxa paths: %v", err))
	}
	app.Ctx.Paths = config.DoxaPaths
}
func initSysDb() error {
	shellDir, err := os.MkdirTemp(app.Ctx.UserHome, "sys*")
	SysDbPath = path.Join(shellDir, fmt.Sprintf("sys-%d.db", rand.Int()))
	// open the database
	dsSystem, err := kvs.NewBoltKVS(SysDbPath)
	if err != nil { // should not happen, but just in case
		if strings.Contains(err.Error(), "timeout") {
			return fmt.Errorf("system database is already open")
		}
	}
	app.OverrideSysDb(dsSystem)
	// create the database mapper
	var sysKvs *kvs.KVS
	sysKvs, err = kvs.NewKVS(dsSystem)
	if err != nil {
		doxlog.Panicf("error creating mapper for system database: %v\n", err)
	}
	if sysKvs == nil {
		doxlog.Panicf("system kvs is nil")
	}
	// add current project to database
	curProjRec := kvs.NewDbR()
	curProjRec.KP.Dirs.Push(sysManager.Selected)
	sysKvs.Db.MakeDir(curProjRec.KP)
	curProjRec.KP.KeyParts.Push(sysManager.SelectedProject)
	curProjRec.Value = app.Ctx.Project
	sysKvs.Db.Put(curProjRec)

	app.Ctx.DataStores.Add(&kvs.Store{
		Name:        config.SystemDbName,
		Description: "Doxa system data for specific user and machine",
		Path:        SysDbPath,
		Prompt:      "sysDb",
		Kvs:         sysKvs,
	})
	// create the context System database Manager
	paths := new(sysManager.Paths)
	paths.KvsPath = SysDbPath
	paths.DoxaHome = app.Ctx.DoxaHome
	paths.DoxaProjectGroupsPath = path.Join(app.Ctx.DoxaHome, config.ProjectsDir)
	parms := sysManager.Parms{
		Kvs:     sysKvs,
		Paths:   paths,
		InCloud: app.Ctx.Cloud,
	}
	app.Ctx.SysManager, err = sysManager.NewSysManager(&parms)
	if err != nil {
		msg := fmt.Sprintf("error creating sys manager: %v", err)
		doxlog.Error(msg)
		return errors.New(msg)
	}
	return nil
}

func initGitlab(token string, myDvcs *dvcs.DVCS) {
	tmpClient, err := gitlab.NewClient(token, gitlab.WithBaseURL("https://gitlab.com/api/v4"))
	if err != nil {
		doxlog.Errorf("Could not create gitlab client: %v", err)
		return
	}
	groups, _, err := tmpClient.Groups.SearchGroup(app.Ctx.Project)
	for _, group := range groups {
		if group.Name == app.Ctx.Project {

			myDvcs.Gitlab, err = dvcs.NewGitlabClient(token, group.ID, app.Ctx.Paths.ProjectDirPath, true)
			if err != nil {
				doxlog.Errorf("Could not create gitlab client: %v", err)
				return
			}
			doxlog.Infof("Gitlab client created succesfully")
			return
		}
	}
	doxlog.Errorf("Could not find group %s in your gitlab groups. Make sure group has been created and token is correct", app.Ctx.Project)
}

func initializeHelpers() error {
	var err error
	app.Ctx.PM = kvs.NewPropertyManager(app.Ctx.Kvs)

	app.Ctx.PM.KP.Clear()
	app.Ctx.PM.KP.Dirs.Push("configs")
	app.Ctx.PM.KP.Dirs.Push("use")
	var configName string

	// if there is no configuration Name found, use the default
	if configName, err = app.Ctx.PM.GetString("value"); err != nil {
		configName = "default"
		err = app.Ctx.PM.SetString("desc", "You may have multiple configurations, but only one can be used at a time.  Indicate here the configuration to use.")
		if err != nil {
			return err
		}
		err = app.Ctx.PM.SetString("value", configName)
		if err != nil {
			return err
		}
	}
	app.Ctx.ConfigName = configName
	config.InitSettingsManager(configName, app.Ctx.Paths.ExportPath, app.Ctx.Paths.ImportPath, app.Ctx.Kvs, app.Ctx.PM, app.Ctx.Dev) // why both PM and SM? Because the settings manager uses the property manager.
	app.Ctx.PM.KP.Dirs.Pop()

	err = config.SM.SyncDbWithPropertiesMap()
	if err != nil {
		doxlog.Errorf(err.Error())
	}

	// Read in the configuration settings (properties).
	// Missing settings are automatically created using defaults
	// defined in pkg/enums/properties.
	// First, let the user know whether we are initializing
	// or simply reading the settings.
	app.Ctx.PM.KP.Dirs.Push(configName)
	if !app.Ctx.Kvs.Db.Exists(app.Ctx.PM.KP) {
		doxlog.Infof("Config `%s` does not exist.", configName)
		doxlog.Info("Initializing configuration using default values.")
	} else {
		doxlog.Infof("Reading %s configuration settings from the database.", configName)
	}
	if err = config.SM.ReadConfiguration(); err != nil {
		doxlog.Panicf("Could not read configurations.  Did developer forget to run `go-generate` for pkg/enums/properties? %s", err)
	}
	app.Ctx.TTY = config.SM.BoolProps[properties.SysShellTty]
	app.Ctx.Wrapper.SetExportFilter(app.Ctx.ProjectPushData.ToStrSlice())
	return nil
}

func PopulateDb() {
	kp := kvs.NewKeyPath()
	kp.Dirs.Push("ltx")
	app.Ctx.Kvs.Db.MakeDir(kp)
	kp.Dirs.Push("default")
	kp.Dirs.Push("topic")
	app.Ctx.Kvs.Db.MakeDir(kp)
	kp.KeyParts.Push("key")
	rec := kvs.NewDbR()
	rec.KP = kp.Copy()
	rec.Value = "value"
	app.Ctx.Kvs.Db.Put(rec)
}
