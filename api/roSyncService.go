package api

import (
	"context"
	"fmt"
	"github.com/liturgiko/doxa/app"
	"github.com/liturgiko/doxa/pkg/catalog"
	"github.com/liturgiko/doxa/pkg/dbPaths"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/syncsrvc"
	picoWidget "github.com/liturgiko/doxa/templates/webApp/structs/widget"
	"html/template"
	"math"
	"path"
	"sort"
	"strings"
	"sync"
	"time"
)

type roSafeState struct {
	internal SyncState
	mutex    sync.RWMutex
}

func (s *roSafeState) Update(newState SyncState) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	s.internal = newState
}

func (s *roSafeState) Check() SyncState {
	s.mutex.RLock()
	defer s.mutex.RUnlock()
	return s.internal
}

type roSyncService struct {
	syncMan *syncsrvc.Manager
	catMan  *catalog.Manager
	State   roSafeState
	uiState struct {
		catalogSelected   string
		librarySelected   string
		libraryIsSelected bool //used to handle selection state
		catalogIndices    map[string]int
		libraryIndices    map[string]int
		isInventory       bool
	}
	ErrorMsg string
}
type RoSyncInfo struct {
	CatalogName      string // Gitlab Group Name, e.g. doxa-seraphimdedes of subscribed catalog
	VersionAvailable string
	VersionInstalled string
	Syncing          bool   // Running, Not Running
	UpdateLast       string // e.g. 3 weeks ago
}
type RoSyncOnce struct{}
type RoSyncAll struct {
	MsgChan chan string
}

// TODO: do we need a labels service for api
const updateLabel = "update"
const listCatalogSubscriptions = "list catalogs"

func NewRoSyncService() RoSyncService {
	implement := new(roSyncService)
	implement.syncMan = app.Ctx.SyncManager
	implement.catMan = app.Ctx.CatalogManager
	implement.uiState.catalogIndices = make(map[string]int)
	implement.uiState.libraryIndices = make(map[string]int)
	return implement
}

type RoSyncServiceState struct {
	NotSubscribed   bool
	SyncState       SyncState
	UpdateAvailable bool
}

func (ro *roSyncService) getState() RoSyncServiceState {
	if len(ro.catMan.SubscriptionKPs()) == 0 {
		return RoSyncServiceState{NotSubscribed: true}
	}
	stateObj := RoSyncServiceState{}

	currentState := ro.State.Check()
	inSync := app.Ctx.SyncManager.RoSyncer.(*syncsrvc.ReadOnlySyncer).InSync

	if currentState != SyncRequested {
		stateObj.SyncState = NotSyncing
	} else {
		stateObj.SyncState = SyncRequested
	}
	// First check if we're in a completed state
	if currentState == CompletedNoErrs {
		stateObj.SyncState = CompletedNoErrs
		return stateObj
	}

	// Then check current sync status
	if inSync {
		if app.Ctx.InternetAvailable {
			stateObj.SyncState = SyncInProgress
			ro.State.Update(SyncInProgress)
		} else {
			stateObj.SyncState = NoInternet
			ro.State.Update(NoInternet)
		}
		return stateObj
	}

	if currentState == SyncRequested {
		return stateObj
	}
	// If we're not syncing and not completed, we're in NotSyncing state
	if currentState != CompletedNoErrs {
		stateObj.SyncState = NotSyncing
		ro.State.Update(NotSyncing)
	}

	updates, _, _ := ro.GetAvailableUpdates()
	stateObj.UpdateAvailable = len(updates) > 0
	return stateObj
}

func (ro *roSyncService) getTitleAndDesc() (title, desc string) {
	state := ro.getState()
	switch state.SyncState { // Note: getState() already returns a safe copy
	case NotSyncing:
		if state.UpdateAvailable {
			title = "Update Available"
			desc = "New subscriptions are ready to download"
		} else {
			title = "Subscriptions Up To Date"
			desc = "Your subscriptions are current"
		}
	case SyncInProgress:
		title = "Download in progress"
		desc = "Now downloading your subscriptions"
	case NoInternet:
		title = "Waiting for Connection"
		desc = "Download will continue when internet is available"
	case Cancelling:
		title = "Cancelling Download"
		desc = "Finishing current operations"
	case CompletedNoErrs:
		title = "Download Complete"
		desc = "All subscriptions are up to date"
	case Terminated:
		title = "Download Failed"
		desc = "Please check the error messages below"
	default:
		title = "Unexpected State"
		desc = "Please refresh the page"
	}
	return
}

func (ro *roSyncService) ClearInfo() {
	ro.State.Update(NotSyncing)
}

func (ro *roSyncService) GetForm(ctx context.Context, actionPrefix, id, userID string) *picoWidget.Form {
	if app.Ctx.SyncManager == nil || app.Ctx.SyncManager.RoSyncer == nil {
		doxlog.Errorf("RoSyncManager is nil")
		return picoWidget.NewForm("ro", "Startup error")
	}
	//app.Ctx.SyncManager.RoSyncer.(*syncsrvc.ReadOnlySyncer).LoadActionList()
	/****
	No subscription
	Subscribed, up to date
	Subscribed, update available

	Sync running
	Sync not running
	*/
	/* MEGA state
	Not Subscribed, Subscribed, Syncing (implied subscribed)
	Child states:
		Not Subscribed -> Childless
		Subscribed 	-> up to date
					-> update available
		Syncing -> in progress
				-> cancelation in progress?
				-> waiting for internet

	*/

	info, _, _ := ro.Info(ctx, "")
	state := ro.getState()
	if state.SyncState == SyncRequested {
		state.SyncState = SyncInProgress
	}
	f := picoWidget.NewForm(id /*+" doxa-widget-wide"*/, "") //TODO: re-enable wide once update status and selection supported
	f.FormMethod = POST
	f.FormAction = path.Join(actionPrefix, "startSync")
	f.ID = id
	/*
		if state.NotSubscribed {
			f.Title = "No subscription"
			f.Description = "You are currently not subscribed to any catalog"
			f.Actions = append(f.Actions, &picoWidget.InputField{Type: "Button", Value: "Open Subscriptions",
				HtmxFields: []template.HTMLAttr{
					`hx-get="api/htmx/subscriptions/subscribeCatalogsUI"`,
					`hx-swap="outerHTML"`,
					template.HTMLAttr(fmt.Sprintf(`hx-target="#%s"`, id)),
				}})
			return f
		} else {
			field := new(picoWidget.InputField)*/
	title, desc := ro.getTitleAndDesc()
	f.Title = title
	f.TitleIcon = "bi-cloud-check"
	if state.UpdateAvailable {
		f.TitleIcon = "bi-cloud-download"
	}
	f.Description = desc

	if state.SyncState == SyncInProgress {
		progBar := picoWidget.NewProgressBar()
		progBar.Max = len(app.Ctx.SyncManager.RoSyncer.GetAllFromMap())
		if progBar.Max == 0 {
			progBar.Max = 1 //compensate for amp not being loaded yet
		}
		progBar.Value = ro.getCompletedCount()
		progBar.HtmxFields = []template.HTMLAttr{
			`hx-get="/api/htmx/ro/pollProgress"`,
			`hx-trigger="every 1s"`,
			`hx-swap="outerHTML"`,
		}
		f.Fields = append(f.Fields, progBar)
	} else if state.SyncState == CompletedNoErrs {
		scroller := picoWidget.NewScrollArea()
		scroller.Vh = 57
		scroller.Contents = ro.convertToSyncFeedback().ToElements()
		f.Fields = append(f.Fields, scroller)

		clearBtn := picoWidget.NewInputField()
		clearBtn.Type = "button"
		clearBtn.Class = "Secondary"
		clearBtn.Value = "Clear"
		clearBtn.HtmxFields = []template.HTMLAttr{
			`hx-post="api/htmx/ro/reset"`,
		}
		f.AddAction(clearBtn)
	}
	//f.Fields = //make([]*picoWidget.InputField, 0)

	//TODO: fix this
	//f.Fields = append(f.Fields, ro.GetListView())

	/*f.Fields = append(f.Fields, &picoWidget.InputField{
		ElementType: field.ET(),
		Type:        "text",
		Label:       "Subscribed To:",
		ReadOnly:    true,
		Value:       info.CatalogName,
		AriaLabel:   "The catalog you are subscribed to",
	})

	f.Fields = append(f.Fields, &picoWidget.InputField{
		ElementType: field.ET(),
		Type:        "text",
		Label:       "Installed:",
		ReadOnly:    true,
		Value:       info.VersionInstalled,
		AriaLabel:   "The catalog you are subscribed to",
	})

	availText := "You are on the latest version"
	if state.UpdateAvailable {
		availText = info.VersionAvailable
	}
	f.Fields = append(f.Fields, &picoWidget.InputField{
		ElementType: field.ET(),
		Type:        "text",
		Label:       "Available:",
		Name:        "available",
		Helper: picoWidget.Helper{
			Text: "Last checked " + timestampToHumanReadable(time.Since(app.Ctx.CatalogManager.LastUpdateCheck)),
		},
		ReadOnly:  true,
		Value:     availText,
		AriaLabel: "The latest version",
		HtmxFields: []template.HTMLAttr{
			`hx-get="/api/htmx/ro/pollAvailableVersion"`,
			`hx-trigger="every 5s"`,
			`hx-target="closest label"`,
			`hx-swap="outerHTML"`,
		},
	})*/
	// Get available updates
	updates, _, _ := ro.GetAvailableUpdates()
	numUpdates := len(updates)

	// Create updates field
	var availUpdates picoWidget.Element
	if numUpdates == 0 {
		field := picoWidget.NewInputField()
		field.Type = "text"
		field.Label = "Available Updates:"
		field.ReadOnly = true
		field.Value = "No updates available"
		availUpdates = field
	} else {
		mainDetails := picoWidget.NewDetails()
		summary := "1 update available"
		if numUpdates > 1 {
			summary = fmt.Sprintf("%d updates available", numUpdates)
		}
		mainDetails.Summary = summary
		mainDetails.Accordion = true
		mainDetails.IsButton = true
		mainDetails.SummaryClass = "contrast"
		// Create FlexTable for updates
		table := picoWidget.NewFlexTable()
		//table.Class = "grid"
		table.ID = "availUpdateList"

		// Set column widths
		table.FlexAttrs = []picoWidget.FlexAttrs{
			{Grow: true, Basis: "50%"}, // Repository column
			{Grow: true, Basis: "40%"}, // Catalog column
			{Grow: true, Basis: "10%"}, // Status icon column
		}

		for _, update := range updates {
			row := picoWidget.NewFlexTR()

			// Repository cell
			repoLabel := picoWidget.NewLabel()
			repoLabel.Contents = strings.TrimPrefix(update.DbPath, "resources/")

			// Catalog cell with link
			catLink := picoWidget.NewAnchor()
			catLink.Contents = []picoWidget.Element{picoWidget.NewLabel()}
			catLink.Contents[0].(*picoWidget.Label).Contents = update.Catalog
			catLink.Href = "/subscriptionManager/" + update.Catalog

			// Status icon cell
			stabilityIcon := picoWidget.NewLabel()
			if update.IsInventory {
				stabilityIcon.Class = "bi bi-shield-exclamation"
			} else {
				stabilityIcon.Class = "bi bi-shield-check"
			}

			row.Contents = []picoWidget.Element{repoLabel, catLink, stabilityIcon}
			table.Contents = append(table.Contents, row)
		}

		// Create scroll area to contain the table
		scroller := picoWidget.NewScrollArea()
		scroller.Vh = 50
		scroller.Shrink = true
		scroller.Contents = []picoWidget.Element{table}

		mainDetails.Contents = []picoWidget.Element{scroller}
		availUpdates = mainDetails
	}
	f.Fields = append(f.Fields, availUpdates)

	lastUpdate := info.UpdateLast
	hxFields := []template.HTMLAttr{
		`hx-get="/api/htmx/ro/pollUpdated"`,
		`hx-trigger="every 5s"`,
		`hx-target="closest label"`,
		`hx-swap="outerHTML"`,
	} /**/
	if state.SyncState == SyncInProgress {
		lastUpdate = fmt.Sprintf("Sync started %s", info.UpdateLast)
		if state.SyncState == NoInternet {
			lastUpdate += ", Waiting for internet."
		}
		//<div hx-get="/check-status" hx-trigger="load delay:5s">
		hxFields = append(hxFields, `hx-headers='{"X-Sync-Ongoing": "true"}'`)
	} else if !state.UpdateAvailable {
		hxFields = append(hxFields, `hx-headers='{"X-Up-To-Date": "true"}'`)
	}
	f.Fields = append(f.Fields, &picoWidget.InputField{
		ElementType: new(picoWidget.InputField).ET(),
		Type:        "text",
		Label:       "Last Download:",
		Name:        "last-updated",
		ReadOnly:    true,
		Value:       lastUpdate,
		AriaLabel:   "The catalog you are subscribed to",
		HtmxFields:  hxFields,
		//HtmxFields: hxFields,
	})
	/*
		actionName := "Reinstall"
		if state.UpdateAvailable {
			actionName = "Update"
		}
		if state.SyncState == NotRunning {
			f.Actions = append(f.Actions, &picoWidget.InputField{
				Type:  "submit",
				Value: actionName,
			})
			f.Actions = append(f.Actions, &picoWidget.InputField{
				Type:  "button",
				Class: "secondary",
				Value: "Unsubscribe",
				ID:    "unsubscribe-btn",
			})
		} else {
			f.Actions = append(f.Actions, &picoWidget.InputField{
				Type:  "button",
				Class: "secondary",
				Value: "Cancel",
				HtmxFields: []template.HTMLAttr{
					`hx-post="api/htmx/ro/cancelSync"`,
				},
			})
		}
	}*/
	/*f.Actions = append(f.Actions, &picoWidget.InputField{
		Type:  "button",
		Class: "secondary",
		Value: "Manage subscriptions",
		HtmxFields: []template.HTMLAttr{
			`hx-get="api/htmx/subscriptions/subscribeCatalogsUI"`,
			`hx-swap="outerHTML"`,
			template.HTMLAttr(fmt.Sprintf(`hx-target="#%s"`, id)),
		},
	})*/
	actionName := "Download subscriptions"
	if state.UpdateAvailable {
		actionName = "Update subscriptions"
	}
	if state.SyncState == NotSyncing {
		f.Actions = append(f.Actions, &picoWidget.InputField{
			Type:  "submit",
			Value: actionName,
		})
	}
	if state.SyncState == SyncInProgress {
		//DEBUG clause TODO
		_ = func(fields []picoWidget.Element) *picoWidget.ProgressBar {
			for _, e := range fields {
				if pb := e.(*picoWidget.ProgressBar); pb != nil {
					return pb
				}
			}
			return nil
		}(f.Fields)
	}
	return f
}

func (ro *roSyncService) Cancel(ctx context.Context, sessionID string) (responseStatus RequestResponse, err error) {
	ro.syncMan.RoSyncer.SyncAutoOff()
	return RequestResponse{RequestStatus: OK}, nil
} // Get

func (ro *roSyncService) Info(ctx context.Context, sessionID string) (responseData RoSyncInfo, responseStatus RequestResponse, err error) {
	//TODO: session support
	inf := RoSyncInfo{}
	//cat := ro.catMan.GetPrimarySubscriptionCatalog()
	//if cat == nil {
	//	cat = new(catalog.Catalog)
	//	cat.Name = "none"
	//}
	//inf.CatalogName = cat.Name
	elapsed := time.Since(ro.syncMan.RoSyncer.(*syncsrvc.ReadOnlySyncer).LastSync)
	var enArche time.Time
	if elapsed < time.Since(enArche) {
		inf.UpdateLast = timestampToHumanReadable(elapsed)
	}
	inf.Syncing = ro.syncMan.RoSyncer.(*syncsrvc.ReadOnlySyncer).InSync

	//inf.UpdateLast = ro.syncMan.RoSyncer.(*syncsrvc.ReadOnlySyncer).LastSync.String()
	//inf.VersionAvailable = ro.catMan.LatestVersion(cat.Name)
	//inf.VersionInstalled = cat.Version

	resp := RequestResponse{}
	resp.PermittedActions = make([]Action, 0)
	resp.RequestMessages = make([]string, 0)
	/*online := app.Ctx.InternetAvailable
	updateAvail := app.Ctx.SubscriptionUpdateAvailable
	if cat.Name == "none" {
		resp.PermittedActions = append(resp.PermittedActions, Action{Label: listCatalogSubscriptions})
		resp.RequestMessages = append(resp.RequestMessages, "You are not currently subscribed to any catalog")
	} else if updateAvail && online {
		resp.PermittedActions = append(resp.PermittedActions, Action{Label: updateLabel})
	} else if !online {
		resp.RequestMessages = append(resp.RequestMessages, "Internet not available so can't check for updates")
	}*/
	resp.RequestStatus = OK
	return inf, resp, nil
}

func timestampToHumanReadable(elapsed time.Duration) string {
	if elapsed > time.Hour {
		hours := math.Round(float64(elapsed / time.Hour))
		if hours > 24 {
			days := math.Round(hours / 24)
			return fmt.Sprintf("%d days ago", int(days))
		} else {
			return fmt.Sprintf("%d hours ago", int(hours))
		}
	} else if elapsed > time.Minute {
		minutes := math.Round(float64(elapsed / time.Minute))
		return fmt.Sprintf("%d minutes ago", int(minutes))
	} else {
		seconds := math.Round(float64(elapsed / time.Second))
		return fmt.Sprintf("%d seconds ago", int(seconds))
	}
} // Get
func (ro *roSyncService) Sync(ctx context.Context, sessionID string, repoName string) (responseData RoSyncOnce, responseStatus RequestResponse, err error) {
	return RoSyncOnce{}, RequestResponse{}, err
} // Post?
func (ro *roSyncService) SyncAll(ctx context.Context, sessionID string) (responseData RoSyncAll, responseStatus RequestResponse, err error) {
	ro.State.Update(SyncRequested)
	dat := RoSyncAll{}
	dat.MsgChan = ro.syncMan.RoSyncer.(*syncsrvc.ReadOnlySyncer).GetMsgChannel()
	go func() {
		err := ro.syncMan.RoSyncer.SyncAll()
		if err == nil {
			ro.State.Update(CompletedNoErrs)
		} else {
			doxlog.Errorf("RoSyncer.SyncAll failed: %v", err)
			ro.ErrorMsg = err.Error()
			ro.State.Update(Terminated)
		}
		ro.syncMan.RoSyncer.(*syncsrvc.ReadOnlySyncer).BuildFallbackTemplates(app.Ctx.SysManager.LoadSliceFromPath(dbPaths.SYS.FallbackPriorityPath()))
	}()
	return dat, RequestResponse{RequestStatus: OK}, nil
} // Post?

type inventoryIcon struct {
	IsInv bool
}

func (i *inventoryIcon) BsIcon() string {
	if i.IsInv {
		return "bi-cone-striped" //TODO: replace with man digging
	}
	return ""
}

func (i *inventoryIcon) Class() string { return "" }

func (i *inventoryIcon) HtmxFields() []template.HTMLAttr { return make([]template.HTMLAttr, 0) }

func (i *inventoryIcon) SetEndpoint(endpoint, method string) {}

func (i *inventoryIcon) SetListContext(listId, itemId string) {
	if strings.HasPrefix(itemId, "inv-") {
		i.IsInv = true
	}
}

func (i *inventoryIcon) Copy() picoWidget.Action {
	return new(inventoryIcon)
}

type RepoUpdateStatus int

const (
	RepoUpToDate RepoUpdateStatus = iota
	RepoHasErrors
	RepoHasWarns
	RepoUpdateInProgress
	RepoUpdateAvailable
)

type updateIndicator struct {
	UpdateStatus *map[string]RepoUpdateStatus
	id           string
}

func (u *updateIndicator) BsIcon() string {
	switch (*u.UpdateStatus)[u.id] {
	case RepoUpToDate:
		return "bi-cloud-check"
	case RepoHasErrors:
		return "bi-exclamation-octagon-fill"
	case RepoHasWarns:
		return "bi-exclamation-triangle-fill"
	case RepoUpdateInProgress:
		return "bi-arrow-repeat"
	case RepoUpdateAvailable:
		return "bi-cloud-arrow-down-fill"
	}
	return "bi-exclamation-octagon"
}

func (u *updateIndicator) Class() string { return "" }

func (u *updateIndicator) HtmxFields() []template.HTMLAttr { return make([]template.HTMLAttr, 0) }

func (u *updateIndicator) SetEndpoint(endpoint, method string) {}

func (u *updateIndicator) SetListContext(listId, itemId string) { u.id = itemId }

func (u *updateIndicator) Copy() picoWidget.Action {
	nu := new(updateIndicator)
	nu.UpdateStatus = u.UpdateStatus
	return nu
}

const ROUICatalogListId = "subscribedCatalogs"

func (ro *roSyncService) GetCatalogList(shellId string) *picoWidget.DynamicList {

	updateMap := ro.getCatalogUpdateStatus()
	cataList := picoWidget.NewDynamicList()
	cataList.ListEndpoint = "api/htmx/subscriptions/update"
	cataList.ID = ROUICatalogListId

	cataList.Contents = ro.genCatalogListContents(cataList.ID, shellId)
	cataList.HasActions = true
	isInv := new(inventoryIcon)
	needsUpdate := new(updateIndicator)
	needsUpdate.UpdateStatus = &updateMap
	cataList.SetActions(picoWidget.ActionSet{needsUpdate, isInv})
	cataList.Permissions.AddItems = false
	cataList.Checklist = false
	if ro.uiState.catalogSelected != "" {
		cataList.Selected = ro.uiState.catalogIndices[ro.uiState.catalogSelected]
	}
	//cataList.Permissions.AddItemsExceptions = []bool{true, true, true, false}
	cataList.Finalize()
	return cataList
}

func (ro *roSyncService) getCatalogUpdateStatus() map[string]RepoUpdateStatus {
	ret := make(map[string]RepoUpdateStatus)
	state := ro.getState()
	// first off, get the catalogs
	cats := ro.catMan.CatalogsWithSubscriptions(dbPaths.SYS.CatalogsPath())
	for _, cat := range cats {
		//get the latest
		latest := ro.catMan.GetCatalogFromDb(dbPaths.SYS.CatalogsPath(), cat.Name, "latest")
		if latest == nil {
			continue
		}
		if latest.Version != cat.Version {
			if state.SyncState == SyncInProgress {
				ret[cat.Name] = RepoUpdateInProgress
			} else {
				ret[cat.Name] = RepoUpdateAvailable
			}
		} else {
			ret[cat.Name] = RepoUpToDate
		}
	}
	// next we do the inventories
	return ret
}

func (ro *roSyncService) genCatalogListContents(listId, shellId string) []picoWidget.Element {
	cats := ro.catMan.CatalogsWithSubscriptions(dbPaths.SYS.CatalogsPath())
	if cats == nil {
		cats = make([]*catalog.Catalog, 0)
	}
	catNames := make([]string, len(cats))
	for i, cat := range cats {
		catNames[i] = cat.Name
	}
	invens := ro.catMan.CatalogsWithSubscriptions(dbPaths.SYS.InventoriesPath())
	if invens == nil {
		invens = make([]*catalog.Catalog, 0)
	}
	invenNames := make([]string, len(invens))
	for i, inv := range invens {
		invenNames[i] = "inv-" + inv.Name
	}
	collection := make([]picoWidget.Element, len(catNames)+len(invenNames))
	sort.Strings(catNames)   //do this for ease of navigation, but also to ensure consistent mapping
	sort.Strings(invenNames) //do this for ease of navigation, but also to ensure consistent mapping
	master := make([]string, 0, len(collection))
	master = append(master, catNames...)
	master = append(master, invenNames...)
	for i, nomos := range master {
		ro.uiState.catalogIndices[nomos] = i
		tmp := picoWidget.NewLabel()
		tmp.Contents = strings.TrimPrefix(nomos, "inv-")

		endpoint := `hx-get="api/htmx/ro/syncUI""`
		tmp.HtmxFields = []template.HTMLAttr{
			`hx-trigger='click'`,
			template.HTMLAttr(endpoint),
			template.HTMLAttr(fmt.Sprintf(`hx-target="#%s"`, shellId)),
			template.HTMLAttr(fmt.Sprintf(`hx-headers='{"DL-Selected": "%s", "DL-ID": "%s"}'`, nomos, listId)),
		}
		tmp.ID = nomos
		collection[i] = tmp
	}
	return collection
}

const ROUIEntryListID = "entryList"

func (ro *roSyncService) GetEntryList(shellId string) *picoWidget.DynamicList {
	isInv := strings.HasPrefix(ro.uiState.catalogSelected, "inv-")
	catName := strings.TrimPrefix(ro.uiState.catalogSelected, "inv-")
	entList := picoWidget.NewDynamicList()
	entList.ID = ROUIEntryListID
	cont, sel := ro.genEntryListContents(entList.ID, shellId, isInv)
	entList.Contents = cont
	entList.Selection = sel
	entList.HasActions = true
	entList.Checklist = false
	infAction := new(picoWidget.ListItemInfoAction)
	infAction.SetEndpoint("/api/htmx/ro/info", GET)
	updateStatus := new(updateIndicator)
	needs := ro.getEntriesUpdateStatus(catName, isInv)
	updateStatus.UpdateStatus = &needs
	entList.SetActions(picoWidget.ActionSet{infAction, updateStatus})
	entList.Permissions.AddItems = false
	if ro.uiState.libraryIsSelected && ro.uiState.librarySelected != "" {
		entList.Selected = ro.uiState.libraryIndices[ro.uiState.librarySelected]
	}
	entList.Finalize()
	return entList
}
func (ro *roSyncService) genEntryListContents(listId, shellId string, inventory bool) ([]picoWidget.Element, []bool) {
	subMap := make(map[string]bool)
	entMap := make(map[string]*catalog.Entry)
	catName := ro.uiState.catalogSelected
	if inventory {
		catName = strings.TrimPrefix(catName, "inv-")
	}
	subbed := ro.catMan.GetSubscriptionsInCatalog(catName, inventory)
	if subbed == nil {
		subbed = make([]*catalog.Entry, 0)
	}
	for _, ent := range subbed {
		subMap[ent.Path] = true
	}
	catpat := dbPaths.SYS.CatalogsPath()
	if inventory {
		catpat = dbPaths.SYS.InventoriesPath()
	}
	if ro.uiState.catalogSelected == "" {
		return make([]picoWidget.Element, 0), make([]bool, 0) //we do not do this
	}
	selCat := ro.catMan.GetCatalogFromDb(catpat, ro.uiState.catalogSelected, "latest")
	if selCat == nil || len(selCat.Entries) == 0 {
		return make([]picoWidget.Element, 0), make([]bool, 0)
	}

	entNames := make([]string, 0, len(selCat.Entries))
	for ent, deets := range selCat.Entries {
		entMap[deets.Path] = deets
		entNames = append(entNames, ent)
	}

	collection := make([]picoWidget.Element, len(entNames))
	collectionCap := len(collection)
	skipCount := 0
	selection := make([]bool, len(entNames))
	sort.Strings(entNames) //do this for ease of navigation, but also to ensure consistent mapping
	for i, nomos := range entNames {
		if _, exists := subMap[nomos]; !exists {
			collectionCap--
			skipCount++
			continue
		}
		selection[i-skipCount] = subMap[nomos]
		ro.uiState.libraryIndices[nomos] = i
		tmp := picoWidget.NewLabel()
		if shortName, isThere := entMap[nomos]; isThere {
			tmp.Contents = shortName.Name
			if strings.Contains(nomos, "media") {
				tmp.Contents += " (media)"
			}
		}
		endpoint := `hx-get="api/htmx/ro/syncUI"`
		tmp.HtmxFields = []template.HTMLAttr{
			`hx-trigger='click'`,
			template.HTMLAttr(endpoint),
			template.HTMLAttr(fmt.Sprintf(`hx-target="#%s"`, shellId)),
			template.HTMLAttr(fmt.Sprintf(`hx-headers='{"DL-Selected": "%s", "DL-ID": "%s"}'`, nomos, listId)),
		}
		tmp.ID = strings.ReplaceAll(nomos, "/", "-")
		collection[i-skipCount] = tmp

	}
	return collection[:collectionCap], selection[:collectionCap]
}

func (ro *roSyncService) getEntriesUpdateStatus(catName string, isInv bool) map[string]RepoUpdateStatus {
	mappa := make(map[string]RepoUpdateStatus)
	allSubs := ro.catMan.Subscriptions()
	if len(allSubs) == 0 {
		return mappa
	}
	subMap := make(map[string]*catalog.Entry, len(allSubs))
	for _, sub := range allSubs {
		if sub == nil {
			continue
		}
		subMap[sub.Path] = sub
	}
	state := ro.getState()
	switch state.SyncState {
	case NotSyncing:
	case SyncInProgress:
	}
	if !isInv {
		latestVers := ro.catMan.GetSubscriptionFromCatalog(dbPaths.SYS.CatalogsPath(), catName, "latest")
		if latestVers == nil {
			return mappa
		}
		for _, sub := range latestVers {
			if sub == nil {
				continue
			}
			if _, exists := subMap[sub.Path]; !exists {
				continue
			}
			//if legacy, present := subMap[sub.Path]; present {
			if repo, exists := app.Ctx.SyncManager.RoSyncer.GetFromMap(sub.Path); exists {
				if sub.Version != subMap[sub.Path].Version {
					switch state.SyncState {
					case NotSyncing:
						mappa[sub.Path] = RepoUpdateAvailable
					case SyncInProgress:
						mappa[sub.Path] = RepoUpdateInProgress
					case NoInternet:
						mappa[sub.Path] = RepoHasWarns
					case Cancelling:
						mappa[sub.Path] = RepoUpdateAvailable
					}
				} else {
					mappa[sub.Path] = RepoUpToDate
					if repo != nil && repo.HasIssues() {
						mappa[sub.Path] = RepoHasWarns
						if repo.HasIssuesOfType(syncsrvc.Error) {
							mappa[sub.Path] = RepoHasErrors
						}
					}
				}
			}
		}
	}
	//}
	//TODO: process inventories
	return mappa
}

const ROUIListComboID = "roListCombo"

func (ro *roSyncService) GetListView() picoWidget.Element {
	e := picoWidget.NewGridLayout()
	e.ID = ROUIListComboID
	libWrapper := picoWidget.NewScrollArea()
	libWrapper.Contents = []picoWidget.Element{ro.GetEntryList("ro")}
	libWrapper.Vh = 50
	e.Contents = []picoWidget.Element{
		ro.GetCatalogList("ro"),
		libWrapper,
	}
	return e
}

func (ro *roSyncService) GetCatalogInfoForm(cat string) picoWidget.Element {
	return picoWidget.NewScrollArea()
}

func (ro *roSyncService) GetEntryInfoForm(entname string) picoWidget.Element {
	combo := picoWidget.NewUnorderedList()
	scrollWrapper := picoWidget.NewScrollArea()
	libName := picoWidget.NewInputField()
	lastSync := picoWidget.NewInputField()
	backB := picoWidget.NewInputField()

	//TODO: this could be problematic. better to use a helper function in case a libname does contain -
	entname = strings.ReplaceAll(entname, "-", "/")
	//look for entry in syncher rmm
	if ent, isReal := app.Ctx.SyncManager.RwSyncer.GetFromMap(entname); isReal {
		libName.Type = "text"
		libName.ReadOnly = true
		libName.Value = ent.RepoName

		var enArche time.Time
		if time.Since(ent.LastSynced) < time.Since(enArche) {
			lastSync.Type = "text"
			lastSync.ReadOnly = true
			lastSync.Value = timestampToHumanReadable(time.Since(ent.LastSynced))
			scrollWrapper.Contents = []picoWidget.Element{libName, lastSync}
		}
	}

	backB.Type = "submit"
	backB.Value = "Done"
	backB.HtmxFields = []template.HTMLAttr{
		`hx-get="api/htmx/ro/syncUI"`, //TODO: add this
		`hx-swap="outerHTML"`,
		template.HTMLAttr(fmt.Sprintf(`hx-target="#%s", hx-headers='{"DL-List": "%s"}'`, ROUIEntryListID, ROUIEntryListID)),
	}
	combo.Contents = []picoWidget.Element{scrollWrapper, backB}
	return combo
}

func (ro *roSyncService) UpdateSelection(listId, mandate string) {
	switch listId {
	case ROUIEntryListID:
		//TODO: this will actually have to be revised
		mandate = strings.ReplaceAll(mandate, "-", "/")
		ro.uiState.librarySelected = mandate
	case ROUICatalogListId:
		ro.uiState.catalogSelected = mandate
	}
}

type AvailableUpdates []struct {
	DbPath      string
	Catalog     string
	URL         string
	IsInventory bool
}

func (ro *roSyncService) getCompletedCount() int {
	completed := 0
	for _, remote := range app.Ctx.SyncManager.RoSyncer.GetAllFromMap() {
		if remote.LastSynced.After(app.Ctx.SyncManager.RoSyncer.(*syncsrvc.ReadOnlySyncer).LastSync) {
			completed++
		}
	}
	return completed
}

func (ro *roSyncService) hasCompletedSync() bool {
	syncer := app.Ctx.SyncManager.RoSyncer.(*syncsrvc.ReadOnlySyncer)
	return !syncer.InSync && !syncer.LastSync.IsZero()
}

func (ro *roSyncService) ResetPage(ctx context.Context, sessionID string) (RequestResponse, error) {
	syncer := app.Ctx.SyncManager.RoSyncer.(*syncsrvc.ReadOnlySyncer)
	syncer.LastSync = time.Time{} // Reset last sync time
	ro.State.Update(NotSyncing)   // Reset to idle state using thread-safe method
	return RequestResponse{RequestStatus: OK}, nil
}

func (ro *roSyncService) convertToSyncFeedback() *SyncFeedback {
	remotes := app.Ctx.SyncManager.RoSyncer.GetAllFromMap()
	fb := &SyncFeedback{
		NumTotal: len(remotes),
		Done:     !app.Ctx.SyncManager.RoSyncer.(*syncsrvc.ReadOnlySyncer).InSync,
	}

	if fb.Done {
		ro.State.Update(CompletedNoErrs)
	}

	for _, remote := range remotes {
		item := SyncItemInfo{
			Name: remote.RepoName,
		}

		if len(remote.Issues) > 0 {
			item.Health = SyncErrors
			fb.NumWithErrs++
			for _, issue := range remote.Issues {
				item.Messages = append(item.Messages, issue)
			}
		} else if remote.LastSynced.After(app.Ctx.SyncManager.RoSyncer.(*syncsrvc.ReadOnlySyncer).LastSync) {
			item.Health = SyncComplete
			fb.NumCompleted++
		}

		fb.Items = append(fb.Items, item)
	}

	return fb
}

func (ro *roSyncService) GetAvailableUpdates() (AvailableUpdates, RequestResponse, error) {
	var updates AvailableUpdates
	response := RequestResponse{
		PermittedActions: []Action{}, // Populate with actions if needed
		RequestStatus:    OK,
		RequestMessages:  []string{},
	}

	// Step 1: Get catalogs that need updates
	catalogs := ro.catMan.GetUpdates()
	if catalogs == nil {
		response.RequestStatus = InputError
		response.RequestMessages = append(response.RequestMessages, "No updates available.")
		return updates, response, nil
	}

	// Step 2: Process each catalog to find available updates
	for _, catalog := range catalogs {
		if catalog == nil {
			continue
		}

		// Iterate over each entry in the catalog
		for _, entry := range catalog.Entries {
			if entry == nil {
				continue
			}

			// Since GetUpdates already checks versions,
			// we can directly create an AvailableUpdate.
			updates = append(updates, struct {
				DbPath      string
				Catalog     string
				URL         string
				IsInventory bool
			}{
				DbPath:      entry.Path,
				Catalog:     path.Base(catalog.Name),
				URL:         entry.Url,
				IsInventory: strings.HasPrefix(catalog.Name, dbPaths.SYS.InventoriesPath().Path()),
			})
		}
	}

	return updates, response, nil
}

func (ro *roSyncService) GetProgressBar() (*picoWidget.ProgressBar, bool) {

	state := ro.getState()

	// Create progress bar
	bar := picoWidget.NewProgressBar()
	bar.Max = len(app.Ctx.SyncManager.RoSyncer.GetAllFromMap())
	bar.Value = ro.getCompletedCount()

	// If sync is complete or terminated, trigger page refresh
	if state.SyncState != SyncInProgress {
		return nil, true
	}

	// Set up HTMX polling if still in progress
	bar.HtmxFields = []template.HTMLAttr{
		`hx-get="/api/htmx/ro/pollProgress"`,
		`hx-trigger="every 1s"`,
		`hx-swap="outerHTML"`,
	}
	return bar, false
}
