package api

import (
	"context"
	"fmt"
	"github.com/liturgiko/doxa/app"
	"github.com/liturgiko/doxa/pkg/catalog"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/dbPaths"
	"github.com/liturgiko/doxa/pkg/users"
	picoWidget "github.com/liturgiko/doxa/templates/webApp/structs/widget"
	"html/template"
	"path"
	"runtime"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

// used to toggle tracing
const debugSrvc = false

func tracef(format string, args ...interface{}) {
	if !debugSrvc {
		return
	}
	_, _, line, _ := runtime.Caller(1)
	fmt.Printf("[catalogSrvc:%d] "+format+"\n", append([]interface{}{line}, args...)...)
}

type Catalog struct {
	Name        string
	Version     string
	Url         string
	Maintainer  string
	Description string
}

const CUIID = "catalog-form"

type catalogService struct {
	maintainersMutex sync.RWMutex
	Maintainers      []string
	uiState          struct {
		isInventory   bool
		stagedVersion string
		includedMutex sync.RWMutex
		included      map[string]bool
	}
}

func NewCatalogService() CatalogService {
	tracef("Initializing new catalog service")
	c := new(catalogService)

	// Initialize inclusion map from local catalog
	cat := app.Ctx.CatalogManager.GetCatalogFromDb(dbPaths.SYS.CurrentProjectPath(), "catalog", "latest")
	if cat != nil {
		c.setInclusionMap(cat)
	} else {
		c.uiState.included = make(map[string]bool)
	}

	if countStr, exists := app.Ctx.SysManager.LoadCurrentProjectSettingByKey("maintainers", "count"); exists {
		count, err := strconv.Atoi(countStr)
		if err != nil {
			return c
		}
		c.Maintainers = make([]string, count)
		for i := 0; i < count; i++ {
			setting, exists := app.Ctx.SysManager.LoadCurrentProjectSettingByKey("maintainers", fmt.Sprintf("%d", i))
			if !exists {
				continue
			}
			c.Maintainers[i] = setting
		}
	}
	return c
}

// TODO: Add bounds checking for maximum number of entries (e.g. const maxEntries = 1000)
func (c *catalogService) GetEntryList(ctx context.Context, sessionID string) *picoWidget.DynamicList {
	tracef("Getting entry list")
	cat := app.Ctx.CatalogManager.GetCatalogFromDb(dbPaths.SYS.CurrentProjectPath(), "catalog", "latest")
	if cat == nil || cat.Entries == nil {
		return nil
	}
	entList := picoWidget.NewDynamicList()
	entList.ID = "catalog-entries-list"
	entList.Checklist = true
	entList.ListEndpoint = "api/htmx/catalog/setInclusion"
	// Build the entry list
	for _, ent := range cat.Entries {
		lab := picoWidget.NewLabel()
		lab.ID = strings.ReplaceAll(ent.Path, "/", "-")
		lab.Contents = ent.Name
		if strings.Contains(ent.Path, "media") {
			lab.Contents += " (media)"
		}
		entList.Contents = append(entList.Contents, lab)
	}
	sort.Slice(entList.Contents, func(i, j int) bool {
		a := entList.Contents[i].(*picoWidget.Label)
		b := entList.Contents[j].(*picoWidget.Label)
		return a != nil && b != nil && a.Contents < b.Contents
	})
	for _, labW := range entList.Contents {
		lab := labW.(*picoWidget.Label)
		if lab == nil {
			continue //we are only interested in labelled entries
		}
		c.uiState.includedMutex.RLock()
		entList.Selection = append(entList.Selection, c.uiState.included[lab.ID])
		c.uiState.includedMutex.RUnlock()
	}

	entList.SetActions(picoWidget.ActionSet{new(editEntryAction)})
	return entList
}

func (c *catalogService) GetForm(ctx context.Context, sessionID string) *picoWidget.Form {
	tracef("GetForm called with sessionID: %s", sessionID)
	//get entry names from catalog
	tracef("Calling GetEntryList")
	entList := c.GetEntryList(ctx, sessionID)
	if entList == nil {
		tracef("EntryList is nil, falling back to GetCatalogForm")
		return c.GetCatalogForm(ctx, sessionID)
	}
	tracef("Got EntryList with %d contents", len(entList.Contents))

	tracef("Creating edit button")
	editB := picoWidget.NewInputField()
	editB.Value = "Done"
	editB.Type = "button"
	editB.HtmxFields = []template.HTMLAttr{
		`hx-get="api/htmx/catalog/getMeta"`,
		`hx-target="closest article"`,
		`hx-swap=outerHTML`,
		//template.HTMLAttr(fmt.Sprintf(`hx-target="#%s"`, CUIID)),
	}
	refreshB := picoWidget.NewInputField()
	refreshB.Value = "Rescan"
	refreshB.Type = "button"
	refreshB.Class = "secondary"
	refreshB.HtmxFields = []template.HTMLAttr{
		`hx-post="api/htmx/catalog/rebuild"`,
	}
	sg := picoWidget.NewScrollArea()
	sg.Vh = 50
	sg.Contents = []picoWidget.Element{entList}
	tracef("Creating form with scroll area")
	f := picoWidget.NewForm("", "Manage Catalog Entries")
	f.Description = "Select which entries to include in your catalog. Check the boxes next to entries you want to include, and use the gear icon to edit entry details."
	f.ID = CUIID
	f.Fields = []picoWidget.Element{sg}
	f.Actions = []*picoWidget.InputField{editB, refreshB}
	tracef("Returning completed form with ID: %s", f.ID)
	return f
}

func (c *catalogService) GetEntryForm(ctx context.Context, sessionID, entryID string) *picoWidget.Form {
	tracef("Getting entry form for %s", entryID)
	ent := app.Ctx.CatalogManager.GetEntryFromDb(dbPaths.SYS.CurrentProjectPath(), entryID, "catalog", "latest")
	if ent == nil {
		ent = new(catalog.Entry)
		ent.Name = path.Base(entryID)
		ent.Path = entryID
	}
	nameF := picoWidget.NewInputField()
	nameF.Value = ent.Name
	nameF.ReadOnly = true
	nameF.Label = "Entry"

	pathF := picoWidget.NewInputField()
	pathF.Value = ent.Path
	pathF.ReadOnly = true
	pathF.Name = "EntryID"
	pathF.Label = "Relative Path"

	descF := picoWidget.NewTextArea()
	descF.Value = template.HTMLAttr(ent.Desc)
	descF.Name = "Description"
	descF.Label = "Description"

	maintF := picoWidget.NewSelectInput()
	maintF.Value = ent.Maintainer
	maintF.AllowCustomInput = true

	c.maintainersMutex.RLock()
	maintF.Options = make([]string, len(c.Maintainers))
	copy(maintF.Options, c.Maintainers)
	c.maintainersMutex.RUnlock()
	maintF.Label = "Maintained By"
	maintF.Name = "Maintainer"
	maintF.ID = "catalog-maintainer"

	legalF := picoWidget.NewInputField()
	legalF.Value = ent.License
	legalF.Name = "License"
	legalF.Label = "License"

	umF := picoWidget.NewInputField()
	umF.Value = ent.UpdateMessage
	umF.Name = "UpdateMessage"
	umF.Label = "Update Message"

	depF := picoWidget.NewTextArea()
	valStr := ""
	for _, d := range ent.Dependencies {
		valStr += d + "\n"
	}
	depF.Value = template.HTMLAttr(valStr)
	depF.Name = "Dependencies"
	depF.Label = "Dependencies (Advanced)"
	/*typeF := picoWidget.NewSelectInput()
	typeF.Label = "Entry Type"
	typeF.Name = "EntryType"
	typeF.Value = entryTypeToString(ent.Type)
	typeF.Options = []string{
		"Liturgical Text",
		"Redirects Library",
		"Media Maps",
		"Templates",
		"Stylesheets &c",
	}*/

	nvmB := picoWidget.NewInputField()
	nvmB.Type = "button"
	nvmB.Class = "secondary"
	nvmB.Value = "Cancel"
	nvmB.HtmxFields = []template.HTMLAttr{
		`hx-get="api/htmx/catalog/listEntries"`,
		`hx-target="closest article"`,
		`hx-swap=outerHTML`,
	}

	lockinB := picoWidget.NewInputField()
	lockinB.Type = "submit"
	lockinB.Value = "Save Changes"

	f := picoWidget.NewForm("catalog-form-c", "Settings for "+ent.Name)
	f.Description = "Configure the details for this catalog entry. You can set its description, maintainer, license information, and specify any dependencies it may have."
	f.Fields = []picoWidget.Element{
		nameF, pathF, descF /*typeF,*/, maintF, legalF, umF, depF,
	}

	f.Actions = []*picoWidget.InputField{
		nvmB, lockinB,
	}
	f.ID = CUIID
	f.HtmxFields = []template.HTMLAttr{
		`hx-post="/api/htmx/catalog/update"`,
		`hx-headers='{"UpdateTarget": "entry"}'`,
		`hx-target="closest article"`,
		`hx-swap=outerHTML`,
	}
	f.Title = "Entry Settings"
	return f
}

func (c *catalogService) GetCatalogForm(ctx context.Context, sessionID string) *picoWidget.Form {
	tracef("Getting catalog form")
	//get the catalog
	cat := app.Ctx.CatalogManager.GetCatalogFromDb(dbPaths.SYS.CurrentProjectPath(), "catalog", "latest")
	if cat == nil {
		cat = new(catalog.Catalog)
		cat.Name = app.Ctx.Project
	}
	nameF := picoWidget.NewInputField()
	nameF.Value = cat.Name
	nameF.ReadOnly = true
	nameF.Label = "Project"

	descF := picoWidget.NewTextArea()
	descF.Value = template.HTMLAttr(cat.Desc)
	descF.Name = "Description"
	descF.Label = "Description"

	verF := picoWidget.NewInputField()
	verF.Value = cat.Version
	verF.Name = "Version"
	verF.Label = "Version"

	maintF := picoWidget.NewSelectInput()
	maintF.Value = cat.Maintainer
	maintF.AllowCustomInput = true

	c.maintainersMutex.RLock()
	maintF.Options = make([]string, len(c.Maintainers))
	copy(maintF.Options, c.Maintainers)
	c.maintainersMutex.RUnlock()
	maintF.Label = "Maintained By"
	maintF.Name = "Maintainer"
	maintF.ID = "catalog-maintainer"

	entB := picoWidget.NewInputField()
	entB.Value = "Manage Entries"
	entB.Class = "secondary"
	entB.Type = "button"
	entB.HtmxFields = []template.HTMLAttr{
		`hx-get="api/htmx/catalog/listEntries"`,
		`hx-target="closest article"`,
		`hx-swap=outerHTML`,
	}

	saveB := picoWidget.NewInputField()
	saveB.Type = "submit"
	saveB.Value = "Save Catalog"

	publishB := picoWidget.NewInputField()
	publishB.Type = "button"
	publishB.Class = "secondary"
	publishB.Value = "Publish Catalog"
	if !app.Ctx.UserManager.Vault.IsInKeyRing(users.LocalUser) {
		publishB.Disabled = true
	}
	publishB.HtmxFields = []template.HTMLAttr{
		`hx-post="/api/htmx/catalog/publish"`,
	}

	f := picoWidget.NewForm("catalog-form-c", "Catalog Settings")
	f.Description = "Configure the main settings for your catalog. Here you can set the catalog description, version, and maintainer. Use 'Manage Entries' to configure individual entries."
	f.Fields = []picoWidget.Element{nameF, descF, verF, maintF}
	f.Actions = []*picoWidget.InputField{
		entB, saveB, publishB,
	}
	f.ID = CUIID
	f.HtmxFields = []template.HTMLAttr{
		`hx-post="/api/htmx/catalog/update"`,
		`hx-headers='{"UpdateTarget":"meta"}'`,
		`hx-target="closest article"`,
		`hx-swap=outerHTML`,
		//template.HTMLAttr(fmt.Sprintf(`hx-target="#%s"`, CUIID)),
	}
	// Update inclusion map in background after sorting
	go func() {
		c.setInclusionMap(cat)
	}()
	return f
}

// TODO: Add bounds checking for:
// - Maximum description length (e.g. const maxDescriptionLength = 1000)
// - Maximum number of dependencies (e.g. const maxDependencies = 50)
// - Maximum length of update message (e.g. const maxUpdateMessageLength = 200)
// - Maximum length of license text (e.g. const maxLicenseLength = 100)
func (c *catalogService) UpdateEntry(ctx context.Context, sessionID string, update CatalogEntryUpdate) {
	tracef("Updating entry %s", update.EntryID)
	cat := app.Ctx.CatalogManager.GetCatalogFromDb(dbPaths.SYS.CurrentProjectPath(), "catalog", "latest")
	ent := app.Ctx.CatalogManager.GetEntryFromDb(dbPaths.SYS.CurrentProjectPath(), update.EntryID, "catalog", "latest")
	if ent == nil {
		ent = new(catalog.Entry)
		now := time.Now()
		ent.Created = &now
	}
	if cat == nil {
		cat = new(catalog.Catalog)
	}
	ent.Desc = update.Description
	ent.Maintainer = update.Maintainer
	go c.AddMaintainer(ctx, sessionID, update.Maintainer)
	//ent.Type = entryTypeFromString(update.EntryType)
	ent.License = update.License
	ent.UpdateMessage = update.UpdateMessage
	if cat.Entries == nil {
		cat.Entries = make(map[string]*catalog.Entry)
	}
	ent.Dependencies = strings.Split(update.Dependencies, "\n")
	cat.Entries[ent.Name] = ent
	app.Ctx.CatalogManager.SaveLocalProjectCatalog(cat)
}

func entryTypeFromString(entryType string) catalog.EntryType {
	switch entryType {
	case "Liturgical Text":
		return catalog.Library
	case "Redirects Library":
		return catalog.Redirects
	case "Media Maps":
		return catalog.MediaMaps
	case "Templates":
		return catalog.Templates
	case "Stylesheets &c":
		return catalog.StyleSheets
	default:
		return catalog.StyleSheets
	}
}

func entryTypeToString(typ catalog.EntryType) string {
	switch typ {
	case catalog.Library:
		return "Liturgical Text"
	case catalog.Redirects:
		return "Redirects Library"
	case catalog.MediaMaps:
		return "Media Maps"
	case catalog.Templates:
		return "Templates"
	case catalog.StyleSheets:
		return "Stylesheets &c"
	default:
		return ""
	}
}

// TODO: Add bounds checking for:
// - Maximum description length (e.g. const maxCatalogDescLength = 2000)
// - Maximum version string length (e.g. const maxVersionLength = 50)
func (c *catalogService) UpdateCatalogDesc(ctx context.Context, sessionID string, update CatalogMetaUpdate) {
	tracef("Updating catalog description, version: %s", update.Version)
	localCat := app.Ctx.CatalogManager.GetCatalogFromDb(dbPaths.SYS.CurrentProjectPath(), "catalog", "latest")
	localCat.Name = app.Ctx.Project
	localCat.Desc = update.Description
	localCat.Maintainer = update.Maintainer
	localCat.Version = update.Version
	app.Ctx.CatalogManager.SaveLocalProjectCatalog(localCat)
}

func (c *catalogService) UpdateOperationalContext(ctx context.Context, sessionID string, inventory bool) {
	c.uiState.isInventory = inventory
}

func (c *catalogService) Publish(ctx context.Context, sessionID string) error {
	tracef("Publishing catalog, isInventory: %v", c.uiState.isInventory)
	if c.uiState.isInventory {
		return app.Ctx.CatalogManager.PublishCatalog(dbPaths.SYS.InventoryPath(), path.Join(app.Ctx.Paths.CatalogPath, config.InventoryFile), true)
	}
	return app.Ctx.CatalogManager.PublishCatalog(dbPaths.SYS.CurrentProjectPath(), path.Join(app.Ctx.Paths.CatalogPath, config.CatalogFile), false)
}

func (c *catalogService) updateInclusionMap(entryID string, included bool) {
	c.uiState.includedMutex.Lock()
	defer c.uiState.includedMutex.Unlock()
	c.uiState.included[strings.ReplaceAll(entryID, "/", "-")] = included
}

func (c *catalogService) setInclusionMap(cat *catalog.Catalog) {
	c.uiState.includedMutex.Lock()
	defer c.uiState.includedMutex.Unlock()

	c.uiState.included = make(map[string]bool)
	for _, ent := range cat.Entries {
		c.uiState.included[strings.ReplaceAll(ent.Path, "/", "-")] = ent.Subscribed
	}
}

// TODO: Add bounds checking for:
// - Maximum number of maintainers (e.g. const maxMaintainers = 100)
// - Maximum length of maintainer name (e.g. const maxMaintainerNameLength = 50)
func (c *catalogService) AddMaintainer(ctx context.Context, sessionID, maintainer string) {
	// First check with read lock
	c.maintainersMutex.RLock()
	for _, m := range c.Maintainers {
		if m == maintainer {
			c.maintainersMutex.RUnlock()
			return
		}
	}
	c.maintainersMutex.RUnlock()

	// If we need to add, get write lock
	c.maintainersMutex.Lock()
	defer c.maintainersMutex.Unlock()

	// Double-check in case another goroutine added while we were switching locks
	for _, m := range c.Maintainers {
		if m == maintainer {
			return
		}
	}

	c.Maintainers = append(c.Maintainers, maintainer)
	app.Ctx.SysManager.StoreCurrentProjectSettingByKey("maintainers", "count", fmt.Sprintf("%d", len(c.Maintainers)))
	app.Ctx.SysManager.StoreCurrentProjectSettingByKey("maintainers", fmt.Sprintf("%d", len(c.Maintainers)-1), maintainer)
}

func (c *catalogService) ExcludeEntry(ctx context.Context, sessionID string, entryID string) (RequestResponse, error) {
	tracef("Excluding entry: %s", entryID)
	response := RequestResponse{
		RequestStatus:   Error,
		RequestMessages: []string{},
	}

	cat := app.Ctx.CatalogManager.GetCatalogFromDb(dbPaths.SYS.CurrentProjectPath(), "catalog", "latest")
	if cat == nil || cat.Entries == nil {
		response.RequestMessages = append(response.RequestMessages, "Catalog not found")
		return response, fmt.Errorf("catalog not found")
	}

	if entryID == "all" {
		// Exclude all entries
		for _, entry := range cat.Entries {
			entry.Subscribed = false
			c.updateInclusionMap(entry.Path, false)
		}
		go func() {
			app.Ctx.CatalogManager.SaveLocalProjectCatalog(cat)
		}()
		response.RequestStatus = OK
		response.RequestMessages = append(response.RequestMessages, "Successfully excluded all entries")
	} else {
		entry, exists := cat.Entries[entryID]
		if !exists {
			response.RequestStatus = InputError
			response.RequestMessages = append(response.RequestMessages, fmt.Sprintf("Entry %s not found", entryID))
			return response, fmt.Errorf("entry %s not found", entryID)
		}

		entry.Subscribed = false
		go app.Ctx.CatalogManager.SaveLocalProjectCatalog(cat)
		c.updateInclusionMap(entryID, false)

		response.RequestStatus = OK
		response.RequestMessages = append(response.RequestMessages, fmt.Sprintf("Successfully excluded entry: %s", entryID))
	}
	response.PermittedActions = []Action{
		{
			Name:     "view_catalog",
			Label:    "View Catalog",
			EndPoint: "/api/htmx/catalog/listEntries",
			Type:     "get",
			Enabled:  true,
		},
	}

	return response, nil
}

func (c *catalogService) IncludeEntry(ctx context.Context, sessionID string, entryID string) (RequestResponse, error) {
	tracef("Including entry: %s", entryID)
	response := RequestResponse{
		RequestStatus:   Error,
		RequestMessages: []string{},
	}

	cat := app.Ctx.CatalogManager.GetCatalogFromDb(dbPaths.SYS.CurrentProjectPath(), "catalog", "latest")
	if cat == nil {
		cat = new(catalog.Catalog)
		cat.Entries = make(map[string]*catalog.Entry)
	}
	if cat.Entries == nil {
		cat.Entries = make(map[string]*catalog.Entry)
	}

	if entryID == "all" {
		// Include all entries
		for _, entry := range cat.Entries {
			entry.Subscribed = true
			c.updateInclusionMap(entry.Path, true)
		}
		go app.Ctx.CatalogManager.SaveLocalProjectCatalog(cat)
		response.RequestStatus = OK
		response.RequestMessages = append(response.RequestMessages, "Successfully included all entries")
	} else {
		entry, exists := cat.Entries[entryID]
		if !exists {
			// Create new entry if it doesn't exist
			entry = &catalog.Entry{
				Name: path.Base(entryID),
				Path: entryID,
			}
			now := time.Now()
			entry.Created = &now
			cat.Entries[entryID] = entry
		}

		entry.Subscribed = true
		go app.Ctx.CatalogManager.SaveLocalProjectCatalog(cat)
		c.updateInclusionMap(entryID, true)
		response.RequestStatus = OK
		response.RequestMessages = append(response.RequestMessages, fmt.Sprintf("Successfully included entry: %s", entryID))
	}
	response.PermittedActions = []Action{
		{
			Name:     "view_catalog",
			Label:    "View Catalog",
			EndPoint: "/api/htmx/catalog/listEntries",
			Type:     "get",
			Enabled:  true,
		},
	}

	return response, nil
}

func (c *catalogService) Rebuild() (RequestResponse, error) {
	err := app.Ctx.CatalogManager.RebuildInventory(app.Ctx.Kvs)
	if err != nil {
		return RequestResponse{RequestStatus: Error}, err
	}
	return RequestResponse{RequestStatus: OK}, nil
}

type CatalogEntryUpdate struct {
	EntryID       string
	License       string
	Description   string
	UpdateMessage string
	Maintainer    string
	Dependencies  string
	EntryType     string //dropdown menu
}

type CatalogMetaUpdate struct {
	Description string
	Version     string
	Maintainer  string
}

type editEntryAction struct {
	entID    string
	endpoint string
	method   string
}

func (e *editEntryAction) BsIcon() string {
	return "bi-gear"
}

func (e *editEntryAction) Class() string {
	return ""
}

func (e *editEntryAction) HtmxFields() []template.HTMLAttr {
	return []template.HTMLAttr{
		`hx-trigger="click"`,
		`hx-get="/api/htmx/catalog/getEntryForm"`,
		template.HTMLAttr(fmt.Sprintf(`hx-headers='{"DL-Target":"%s"}'`, e.entID)),
		`hx-target="closest article"`,
		`hx-swap=outerHTML`,
	}
}

func (e *editEntryAction) SetEndpoint(endpoint, method string) {
	e.endpoint = endpoint
	e.method = method
}

func (e *editEntryAction) SetListContext(listId, itemId string) {
	e.entID = itemId
}

func (e *editEntryAction) Copy() picoWidget.Action {
	e2 := new(editEntryAction)
	e2.method = e.method
	e2.endpoint = e.endpoint
	return e2
}
