package api

import (
	"context"
	"fmt"
	"github.com/liturgiko/doxa/app"
	"github.com/liturgiko/doxa/pkg/users"
	"reflect"
	"testing"
)

func TestNewTokenService(t *testing.T) {
	// in it's current formulation there is nothing that could go wrong
}

func Test_tokenService_CreateToken(t *testing.T) {
	api := new(API)
	api.TokenService = NewTokenService()
	//first make sure there isn't a token already
	stat, resp, err := api.TokenService.GetTokenStatus(nil, "")
	fmt.Println(stat, resp, err)
	if stat != TokenNeeded {
		t.Fatalf("token already set")
	}
	if err != nil {
		t.Fatalf("%v", err)
	}
	resp, err = api.TokenService.SetToken(nil, "", "password", "glpat-testing")
	fmt.Println(resp, err)
	if resp.RequestStatus != OK {
		t.Fatalf("create token failed: internal err")
	}
	stat, resp, err = api.TokenService.GetTokenStatus(nil, "")
	if stat != Decrypted {
		t.Fatalf("token did not persist")
	}

}

func Test_tokenService_DecryptToken(t *testing.T) {
	type args struct {
		ctx    context.Context
		userId string
		key    string
	}
	tests := []struct {
		name         string
		args         args
		wantResponse RequestResponse
		wantErr      bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := tokenService{}
			gotResponse, err := u.DecryptToken(tt.args.ctx, tt.args.userId, tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("DecryptToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotResponse, tt.wantResponse) {
				t.Errorf("DecryptToken() gotResponse = %v, want %v", gotResponse, tt.wantResponse)
			}
		})
	}
}

func Test_tokenService_DeleteToken(t *testing.T) {
	type args struct {
		ctx    context.Context
		userId string
		key    string
	}
	tests := []struct {
		name         string
		args         args
		wantResponse RequestResponse
		wantErr      bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := tokenService{}
			gotResponse, err := u.DeleteToken(tt.args.ctx, tt.args.userId, tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("DeleteToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotResponse, tt.wantResponse) {
				t.Errorf("DeleteToken() gotResponse = %v, want %v", gotResponse, tt.wantResponse)
			}
		})
	}
}

func Test_tokenService_GetTokenStatus(t *testing.T) {
	api := new(API)
	api.TokenService = NewTokenService()
	//first, we should need the token
	status, _, err := api.TokenService.GetTokenStatus(nil, "")
	fmt.Println(status, err)
	if err != nil {
		t.Fatalf("Got error %v", err)
	}
	if status != TokenNeeded {
		t.Fatalf("Expected status to be TokenNeeded (%d) but got status %d", TokenNeeded, status)
	}
	//we need a token for this phase of the testing
	//let's set a token
	resp, err := api.TokenService.SetToken(nil, "", "password", "glpat-debugtoken")
	fmt.Println(resp, err)
	if err != nil {
		t.Fatalf("Error creating token: %v", err)
	}
	/* we need to remove the key for testing purposes. in the absence of a dedicated api method, we can call backend */
	app.Ctx.UserManager.Vault.RevokeKey(users.LocalUser)
	status, _, err = api.TokenService.GetTokenStatus(nil, "")
	fmt.Println(status, err)
	if err != nil {
		t.Fatalf("Got error %v", err)
	}
	if status != PasswordNeeded {
		t.Fatalf("Expected status to be PasswordNeeded (%d) but got status %d", PasswordNeeded, status)
	}
	/* ok, now try to decrypt with the incorrect password */
	resp, err = api.TokenService.DecryptToken(nil, "", "password123")
	fmt.Println(resp, err)
	if err == nil {
		t.Fatalf("Expected Incorrect password error!")
	}
	//now see what we get as a status
	status, _, err = api.TokenService.GetTokenStatus(nil, "")
	fmt.Println(status, err)
	if err != nil {
		//		t.Fatalf("Got error %v", err)
	}
	/* ok, now try to decrypt with the correct password */
	resp, err = api.TokenService.DecryptToken(nil, "", "password")
	fmt.Println(resp, err)
	if err != nil {
		t.Fatalf("Error creating token: %v", err)
	}
	status, _, err = api.TokenService.GetTokenStatus(nil, "")
	fmt.Println(status, err)
	if err != nil {
		t.Fatalf("Got error %v", err)
	}
	if status != Decrypted {
		t.Fatalf("Expected status to be Decrypted (%d) but got status %d", Decrypted, status)
	}

}

func Test_tokenService_UpdateToken(t *testing.T) {
	firstToken := "first token"
	secondToken := "right token"

	api := new(API)
	api.TokenService = NewTokenService()

	status, _, err := api.TokenService.GetTokenStatus(nil, "")
	if status != TokenNeeded {
		t.Fatalf("Expected status to be %d but got %d. improper setup. aborting.", TokenNeeded, status)
	}
	if err != nil {
		t.Fatalf("Could not check status: %v", err)
	}
	resp, err := api.TokenService.SetToken(nil, "", "password", firstToken)
	fmt.Println(resp, err)
	if err != nil || resp.RequestStatus != OK {
		t.Fatalf("Token could not be created: %d %v", resp.RequestStatus, err)
	}
	status, _, err = api.TokenService.GetTokenStatus(nil, "")
	if status != Decrypted {
		t.Fatalf("Expected status to be %d but got %d.", TokenNeeded, status)
	}
	if err != nil {
		t.Fatalf("Could not check status: %v", err)
	}
	resp, err = api.TokenService.UpdateToken(nil, "", "new password", secondToken)
	fmt.Println(resp, err)
	if err != nil || resp.RequestStatus != OK {
		t.Fatalf("Token could not be updated: %d %v", resp.RequestStatus, err)
	}

}

func Test_tokenService_ValidateToken(t *testing.T) {
	type args struct {
		ctx    context.Context
		userId string
		token  string
	}
	tests := []struct {
		name         string
		args         args
		wantValidity TokenServiceValidationStatus
		wantResp     RequestResponse
		wantErr      bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := tokenService{}
			gotValidity, gotResp, err := u.ValidateToken(tt.args.ctx, tt.args.userId, tt.args.token)
			if (err != nil) != tt.wantErr {
				t.Errorf("ValidateToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotValidity != tt.wantValidity {
				t.Errorf("ValidateToken() gotValidity = %v, want %v", gotValidity, tt.wantValidity)
			}
			if !reflect.DeepEqual(gotResp, tt.wantResp) {
				t.Errorf("ValidateToken() gotResp = %v, want %v", gotResp, tt.wantResp)
			}
		})
	}
}
