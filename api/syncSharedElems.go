package api

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/syncsrvc"
	picoWidget "github.com/liturgiko/doxa/templates/webApp/structs/widget"
	"sort"
)

type SyncHealth int

const (
	SyncComplete SyncHealth = iota
	SyncWarnings
	SyncErrors
)

type SyncFeedback struct {
	Messages     []FeedbackMessage
	Done         bool
	NumTotal     int
	NumCompleted int
	NumWithErrs  int
	Items        []SyncItemInfo
}

type FeedbackMessage struct {
	Level   FeedbackClassification
	Payload string
}

type FeedbackClassification int

const (
	LevelInfo FeedbackClassification = iota
	LevelWarning
	LevelError
)

type SyncItemInfo struct {
	Name     string
	Health   SyncHealth
	Messages []*syncsrvc.Issue
}

func (fb *SyncFeedback) ToElements() []picoWidget.Element {
	errorElems := make([]picoWidget.Element, 0)
	normalElems := make([]picoWidget.Element, 0)

	for i, item := range fb.Items {
		if len(item.Messages) == 0 {
			stat := picoWidget.NewInputField()
			stat.Value = item.Name
			stat.ReadOnly = true
			switch item.Health {
			case SyncComplete:
				stat.ValidationFeedback = &picoWidget.ValidationFeedback{IsInvalid: false}
				normalElems = append(normalElems, stat)
			case SyncErrors:
				stat.ValidationFeedback = &picoWidget.ValidationFeedback{IsInvalid: true}
				errorElems = append(errorElems, stat)
			default:
				normalElems = append(normalElems, stat)
			}
			continue
		}
		deets := picoWidget.NewDetails()
		deets.Summary = item.Name
		switch item.Health {
		case SyncComplete:
			deets.ValidationFeedback = &picoWidget.ValidationFeedback{
				IsInvalid: false,
			}
		case SyncErrors:
			deets.ValidationFeedback = &picoWidget.ValidationFeedback{
				IsInvalid: true,
			}
		default:
		}
		for j, msg := range item.Messages {
			em := picoWidget.NewInputField()
			em.Type = "text"
			em.ReadOnly = true
			switch msg.Level {
			case syncsrvc.Warning:
				em.Value = msg.String()
			case syncsrvc.Error:
				em.ValidationFeedback = &picoWidget.ValidationFeedback{
					ID:        fmt.Sprintf("err-%d.%d", i, j),
					Class:     "Error",
					Message:   msg.Desc,
					IsInvalid: true,
				}
				em.Value = fmt.Sprintf("%s (%d)", msg.Code, msg.Count)
			}
			deets.Contents = append(deets.Contents, em)
		}
		if item.Health == SyncErrors {
			errorElems = append(errorElems, deets)
		} else {
			normalElems = append(normalElems, deets)
		}
	}

	sort.Slice(errorElems, func(i, j int) bool {
		var nameI, nameJ string
		switch v := errorElems[i].(type) {
		case *picoWidget.InputField:
			nameI = v.Value
		case *picoWidget.Details:
			nameI = v.Summary
		}
		switch v := errorElems[j].(type) {
		case *picoWidget.InputField:
			nameJ = v.Value
		case *picoWidget.Details:
			nameJ = v.Summary
		}
		return nameI < nameJ
	})

	sort.Slice(normalElems, func(i, j int) bool {
		var nameI, nameJ string
		switch v := normalElems[i].(type) {
		case *picoWidget.InputField:
			nameI = v.Value
		case *picoWidget.Details:
			nameI = v.Summary
		}
		switch v := normalElems[j].(type) {
		case *picoWidget.InputField:
			nameJ = v.Value
		case *picoWidget.Details:
			nameJ = v.Summary
		}
		return nameI < nameJ
	})

	elems := make([]picoWidget.Element, 0, len(errorElems)+len(normalElems))
	elems = append(elems, errorElems...)
	elems = append(elems, normalElems...)
	return elems
}

type SyncState int

const (
	NotSyncing    SyncState = iota
	SyncRequested           //we know we want to sync but are not sure if we are syncing yet
	SyncInProgress
	NeedsToken
	CompletedNoErrs
	Terminated
	NoInternet
	Cancelling
	ErrNoSuchGroup
)
