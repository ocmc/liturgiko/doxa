package api

import (
	"context"
	picoWidget "github.com/liturgiko/doxa/templates/webApp/structs/widget"
	"time"
)

type FormAction string // each service implementation defines it actions.

const (
	GET    string = "GET"
	PUT    string = "PUT"
	POST   string = "POST"
	DELETE string = "DELETE"
)

type CatalogService interface {
	GetForm(ctx context.Context, sessionID string) *picoWidget.Form
	GetEntryList(ctx context.Context, sessionID string) *picoWidget.DynamicList
	GetEntryForm(ctx context.Context, sessionID, entryID string) *picoWidget.Form
	GetCatalogForm(ctx context.Context, sessionID string) *picoWidget.Form
	UpdateEntry(ctx context.Context, sessionID string, update CatalogEntryUpdate)
	UpdateCatalogDesc(ctx context.Context, sessionID string, update CatalogMetaUpdate)
	UpdateOperationalContext(ctx context.Context, sessionID string, inventory bool)
	AddMaintainer(ctx context.Context, sessionID, maintainer string)

	Publish(ctx context.Context, sessionID string) error
	IncludeEntry(ctx context.Context, sessionID string, entryID string) (RequestResponse, error)
	ExcludeEntry(ctx context.Context, sessionID string, entryID string) (RequestResponse, error)
	Rebuild() (RequestResponse, error)
}
type DbBackupService interface {
	Backup(ctx context.Context, sessionID string) error
	Copy(ctx context.Context, destination, sessionID, BackUpId string) error
	Delete(ctx context.Context, sessionID, BackUpId string) error
	Info(ctx context.Context, sessionID string) error
	List(ctx context.Context, sessionID string) error
}
type RoSyncService interface {
	//UI methods
	GetForm(ctx context.Context, actionPrefix, id, userID string) *picoWidget.Form
	UpdateSelection(listId, mandate string)
	GetEntryList(shellId string) *picoWidget.DynamicList
	GetCatalogList(shellId string) *picoWidget.DynamicList
	GetEntryInfoForm(entname string) picoWidget.Element
	GetProgressBar() (*picoWidget.ProgressBar, bool)
	ClearInfo()

	//API methods
	Cancel(ctx context.Context, sessionID string) (responseStatus RequestResponse, err error)                                         // Get
	Info(ctx context.Context, sessionID string) (responseData RoSyncInfo, responseStatus RequestResponse, err error)                  // Get
	Sync(ctx context.Context, sessionID string, repoName string) (responseData RoSyncOnce, responseStatus RequestResponse, err error) // Post?
	SyncAll(ctx context.Context, sessionID string) (responseData RoSyncAll, responseStatus RequestResponse, err error)                // Post?
	GetAvailableUpdates() (AvailableUpdates, RequestResponse, error)
}
type RwSyncService interface {
	GetWidget(ctx context.Context, actionPrefix string, id string, userID string) *picoWidget.Form
	Details(ctx context.Context, sessionID string) (SyncFeedback, RequestResponse, error) // Get
	Info(ctx context.Context, sessionID string) (RwSyncInfo, RequestResponse, error)      // Status (running, not running)
	Repos(ctx context.Context, sessionID string) (RwRepoList, RequestResponse, error)
	Sync(ctx context.Context, sessionID string, repoId string) (RequestResponse, error)
	SyncAll(ctx context.Context, sessionID string) (RequestResponse, error)
	ResetPage(ctx context.Context, sessionID string) (RequestResponse, error)
	GitlabGroupExists(ctx context.Context, sessionID string) (bool, RequestResponse, error)
}
type SessionService interface {
	Create(ctx context.Context, userID string, duration time.Duration) (*Session, error)
	Get(ctx context.Context, sessionID string) (*Session, error)
	Delete(ctx context.Context, sessionID string) error
	DeleteExpired(ctx context.Context) error
}
type StatusService interface {
	InternetOk() bool
}
type SubscriptionService interface {
	GetForm(ctx context.Context, actionPrefix, id, userID string, isInventory bool, selection SubscribeUISelection) *picoWidget.SubscriptionManager
	GetSelectedUICatalog(ctx context.Context, userID string) (catalog string, inventory bool)
	GetCatalogList(shellId string, inventory bool) *picoWidget.DynamicList
	GetEntryList(shellId string, inventory bool) *picoWidget.DynamicList
	GetCatalogsTable(ctx context.Context, actionPrefix, id, userID string) *picoWidget.Table
	GetCatalogInfo(ctx context.Context, sessionID, catName, listId string, isInventory bool) picoWidget.Element
	GetEntryInfo(ctx context.Context, sessionID, catName, entName, listId string, isInventory bool) picoWidget.Element

	AddCustomCatalog(ctx context.Context, sessionID, url string) (SubscriptionServiceAddCustomCatalog, RequestResponse, error)
	AddCustomInventory(ctx context.Context, sessionID, url string) (SubscriptionServiceAddCustomCatalog, RequestResponse, error)
	Catalogs(ctx context.Context, sessionID string) ([]*Catalog, RequestResponse, error)
	RemoveCustomCatalog(ctx context.Context, sessionID, customCatalogId string) (RequestResponse, error)
	RemoveCustomInventory(ctx context.Context, sessionID, customCatalogId string) (RequestResponse, error)
	Info(ctx context.Context, sessionID string) (SubscriptionServiceInfo, RequestResponse, error)
	PreviousVersions(ctx context.Context, sessionID string) ([]string, RequestResponse, error)
	RevertToVersion(ctx context.Context, sessionID, versionId string) (RequestResponse, error)
	ChangeSubscription(ctx context.Context, sessionID string, data SubscriptionSelectionData) (RequestResponse, error)
	Subscribe(ctx context.Context, sessionID, catalogId string, isInv bool) (RequestResponse, error)
	Unsubscribe(ctx context.Context, sessionID string) (RequestResponse, error)
	SetFallbackTemplates(inventory bool)
	GetFallbackPaths() []string
	SetFallbackPaths(newPaths []string)
	GetFallbackPriorityStatus(node string) bool
	SetFallbackPriorityStatus(node string, status bool)
	RefreshCatalogs() (RequestResponse, error)
}
type UserService interface {
	//GET

	GetUser(ctx context.Context, sessionID string, id string) (string, RequestResponse, error)
	//PUT

	CreateUser(ctx context.Context, user string) (status RequestResponse, err error)
	// ... other methods
}

type TokenService interface {
	//GROUND RULES:
	// 1. A service has one and only one form type associated with it
	// 2. A service has a set of states that determine form contents
	// 3. A form field can result in calls to the service, and updates to the field in response
	// 4. The methods of a service are to be grouped into GET, PUT, POST, and DELETE
	// 		Method	Purpose												Idempotent	Safe
	//		GET		Retrieve data										Yes			Yes
	//		POST	Submit data to create or modify resources			No			No
	//		PUT		Replace all current representations of a resource	Yes			No
	//		DELETE	Remove a specified resource							Yes			No

	// GET

	GetWidget(ctx context.Context, actionPrefix, id, userID string) *picoWidget.Form

	GetTokenStatus(ctx context.Context, userID string) (TokenServiceEncryptionStatus, RequestResponse, error)

	// PUT

	SetToken(ctx context.Context, userId string, key, token string) (response RequestResponse, err error)
	UpdateToken(ctx context.Context, userId string, key, token string) (RequestResponse, error)

	// POST

	ValidateToken(ctx context.Context, userId string, token string) (TokenServiceValidationStatus, RequestResponse, error)
	DecryptToken(ctx context.Context, userId string, key string) (response RequestResponse, err error)
	EncryptToken(ctx context.Context, userId string) (response RequestResponse, err error)

	// DELETE

	DeleteToken(ctx context.Context, userId string, key string) (response RequestResponse, err error)
	// ... other methods
}

type API struct {
	Catalog      CatalogService
	DbBackup     DbBackupService
	RoSync       RoSyncService
	RwSync       RwSyncService
	Sessions     SessionService
	Status       StatusService
	Subscription SubscriptionService
	Users        UserService
	TokenService TokenService
}

func NewApi() *API {
	return &API{
		Catalog:      NewCatalogService(),
		DbBackup:     nil,
		RoSync:       NewRoSyncService(),
		RwSync:       NewRwSyncService(),
		Sessions:     nil,
		Status:       NewStatusServiceManager(),
		Subscription: NewSubscriptionService(),
		TokenService: NewTokenService(),
		Users:        nil,
	}
}

// Shared structs

type RequestResponse struct {
	PermittedActions []Action           // e.g. for GUI, each becomes a button
	RequestStatus    ResponseStatusCode // Short, e.g. OK
	RequestMessages  []string           // Any messages about the status
}
type Action struct {
	Name     string
	Label    string
	EndPoint string //
	Type     string // for http, e.g. get, put, post, delete
	Enabled  bool   // for GUI button
}

type ResponseStatusCode int

const (
	OK ResponseStatusCode = iota
	InputError
	Error
)

func (r ResponseStatusCode) String() string {
	switch r {
	case OK:
		return "OK"
	case Error:
		return "ERROR"
	case InputError:
		return "INPUT ERROR"
	default:
		return "UNKNOWN"
	}
}
