package picoWidget

import (
	"fmt"
	"html/template"
	"strings"
)

// Structs for Pico Form Semantic Field

// default class names

const (
	WidgetClass                     = "container doxa-widget"
	WidgetTitleClass                = "doxa-widget-title"
	WidgetLabelClass                = "doxa-widget-label"
	WidgetFormButtonClass           = "doxa-widget-form-button"
	WidgetFormButtonHelperTextClass = "doxa-widget-form-button-helper-text"
	WidgetInputTypeText             = "text"
	WidgetInputTypeEmail            = "email"
	WidgetInputTypeNumber           = "number"
	WidgetInputTypeTel              = "tel"
	WidgetInputTypePassword         = "password"
)

// Interface subtypes
const (
	ntMenuItem    = "menuItem"
	ntSimpleMenu  = "simpleMenu"
	ntSubMenu     = "subMenu"
	wtAccordion   = "accordion"
	wtForm        = "form"
	wtTable       = "table"
	wtStringSlice = "stringSlice"
	etInput       = "input"
)

// Widget exists simply to allow web components of different kinds to exist together in a list.
// If a new widget struct is added in this package, it must implement the interface.
// Widget structs are used in template bodyMainSectionGridArticleWidget.
// The WT() func should be called to get the WidgetType for use in the template.
type Widget interface {
	WT() string
	//Finalize()
}

// Element should really only have four implementations, Air, Fire, Water, and Dirt. Maybe Aether
type Element interface {
	ET() string
	GetID() string
}
type Menu interface {
	MT() string
}

type NavItem interface {
	NT() string
}

/*
Relationships
--> one and only one
-->> one to many
Body --> Main
Main -->> Section -->> Grid -->> Article (aka Widget) -->> Field (aka InputPut)

	--> InputField
*/

// IndexHtmlData is populated and passed into template index.
type IndexHtmlData struct {
	UrlPath           template.HTMLAttr
	Navbar            *Nav   // used to create the navbar
	Title             string // used in the html head title tag
	Body              *Body
	NeedsModalConfirm bool
	WidgetTest        Widget // option.  Use for testing.
}

func NewIndexHtmlData(title string, nav *Nav, needsModalConfirm bool) *IndexHtmlData {
	i := new(IndexHtmlData)
	i.Title = title
	i.Navbar = nav
	i.Body = new(Body)
	i.Body.Main = new(Main)
	i.NeedsModalConfirm = needsModalConfirm
	return i
}

type Nav struct {
	ID         string
	Class      string
	WidgetType string
	Home       NavItem
	Items      []NavItem
}

type MenuItem struct {
	ID     string
	Class  string
	Href   template.HTMLAttr
	Target string
	Label  string
}

func (s SimpleMenu) NT() string { return ntSimpleMenu }

type SimpleMenu struct {
	WidgetType string
	Item       MenuItem
}

func (s SubMenu) NT() string { return ntSubMenu }

type SubMenu struct {
	ID         string
	Class      string
	WidgetType string
	Summary    string     // label to appear at main menu level
	Direction  string     // pico css rtl | ltr   (right to left | left to right
	Items      []MenuItem // submenu items
}

type Body struct {
	//Heading *Heading
	Main *Main
	//Footer  *Footer
}

type Main struct {
	ID          string
	Class       string
	Heading     string
	Description string
	Help        template.HTML
	Sections    []*Section
}

type Section struct {
	ID    string
	Class string
	Grids []*Grid // each grid displays as a row.
	// the widgets in a grid display as columns, side-by-side
}

type Grid struct {
	ID      string
	Class   string
	Widgets []Widget
}

// WT implements the Widget interface
// and provides the literal string
// to use in the if statements in
// template bodyMainSectionGridArticleWidget.
func (f *Form) WT() string { return wtForm }

type Form struct {
	ID          string
	Class       string // the css class for the form container, e.g. "container doxa-widget"
	WidgetType  string
	Title       string // Title of the widget displayed in the div.doxa-widget-title
	TitleIcon   string // TitleIcon is a bootstrap icon that can precede title.
	Description string
	FormAction  string
	FormMethod  string
	Fields      []Element     // Form fieldset
	Actions     []*InputField // Form action input, e.g. button.  1st should be marked primary, others secondary
	//HXAttributes []Attrib
	HtmxFields []template.HTMLAttr
}

func (a *Accordion) WT() string { return wtAccordion }

type Accordion struct {
	ID             string
	Class          string // the css class for the form container, e.g. "container doxa-widget"
	WidgetType     string
	Title          string   //
	TitleIcon      string   // TitleIcon is a bootstrap icon that can precede title
	Description    string   //
	Summary        []string // e.g. text.  When clicked toggles accordion
	Details        string
	InitializeOpen bool
}

func (g *Generic) WT() string { return "generic" }

type Generic struct {
	ID             string
	Class          string
	WidgetType     string
	Title          string
	TitleIcon      string // TitleIcon is a bootstrap icon that can precede title
	Description    string
	ContainerClass string
	Contents       []Element
}

// WT implements the Widget interface
// and provides the literal string
// to use in the if statements in
// template bodyMainSectionGridArticleWidget.
func (t Table) WT() string { return wtTable }

type Table struct {
	ID                         string
	Class                      string
	WidgetType                 string
	Title                      string
	TitleIcon                  string // TitleIcon is a bootstrap icon that can precede title
	Description                string
	ContainerClass             string
	CanPage                    bool
	CanSelectRow               bool
	CanFilter                  bool
	IdColumnHidden             bool
	BtnHideFiltersLabel        string
	BtnShowFiltersLabel        string
	BtnSortSelectedBottomLabel string
	BtnSortSelectedTopLabel    string
	BtnShowSelectedLabel       string
	BtnShowAllLabel            string
	BtnSubmitLabel             string
	PaginationClass            string
	ToggleFiltersClass         string
	RowsPerPageClass           string
	SelectHeaderName           string
	IncludeSelectRowNone       bool
	SelectRowNone              string
	SelectRowNoneOtherTds      string
	PrevButtonClass            string
	NextButtonClass            string
	ToggleFilters              string
	RowsPerPage                string
	Headers                    []string
	Rows                       []Row
	PreviousButton             string
	NextButton                 string
	ColumnConfigs              []ColumnConfig
}

type ColumnConfig struct {
	Width     string
	Alignment string
}

type Row struct {
	MainData   []string
	DetailData []DetailItem
	UniqueID   string
	Selected   bool
}

type DetailItem struct {
	Label string
	Value interface{}
}

func (s *StringSlice) WT() string { return wtStringSlice }

type StringSlice struct {
	Strings []string
}

// Test Data Functions

func GetTestAccordion(id int) *Accordion {
	a := new(Accordion)
	a = &Accordion{
		ID:             fmt.Sprintf("tstAccordion-%d", id),
		Class:          "",
		WidgetType:     a.WT(),
		Title:          "Accordion as a Widget",
		Description:    "This widget has a Summary and Details.",
		Summary:        []string{"Click for more info about", "A", "B"},
		Details:        "Click above to hide this.",
		InitializeOpen: false,
	}
	return a
}

func GetTestTable(canPage, canSelectRow, canFilter, includeDetails bool) *Table {
	var cssClass string
	if includeDetails {
		cssClass = "striped hover"
	} else {
		cssClass = "striped"
	}
	table := &Table{
		ID:    "myTable",
		Class: cssClass,
		//WidgetType:         "table",
		ContainerClass:     "table-container",
		CanPage:            canPage,
		CanSelectRow:       canSelectRow,
		CanFilter:          canFilter,
		PaginationClass:    "pagination",
		ToggleFiltersClass: "secondary",
		RowsPerPageClass:   "outline",
		SelectHeaderName:   "Subscribe",
		PrevButtonClass:    "outline",
		NextButtonClass:    "outline",
		ToggleFilters:      "Toggle Filters",
		RowsPerPage:        "10",
		Headers:            []string{"Name", "Age", "City"},
		PreviousButton:     "Previous",
		NextButton:         "Next",
		ColumnConfigs: []ColumnConfig{
			{Width: "40%", Alignment: "left"},
			{Width: "20%", Alignment: "center"},
			{Width: "40%", Alignment: "left"},
		},
	}
	table.WidgetType = table.WT()
	if canFilter {
		table.BtnShowFiltersLabel = "Hide Filters"
		table.BtnShowFiltersLabel = "Show Filters"
	}
	if canSelectRow {
		table.BtnSortSelectedBottomLabel = "Sort Selected Bottom"
		table.BtnSortSelectedTopLabel = "Sort Selected Top"
		table.BtnShowSelectedLabel = "Show Selected"
		table.BtnShowAllLabel = "Show All"
		table.BtnSubmitLabel = "Submit Selection"
	}

	table.Rows = []Row{}

	additionalRows := [][]string{
		{"None", "n/a", "n/a"},
		{"Alice", "28", "New York"},
		{"Bob", "35", "San Francisco"},
		{"Charlie", "42", "London"},
		{"David", "31", "Tokyo"},
		{"Eve", "39", "Paris"},
		{"Frank", "45", "Berlin"},
		{"Grace", "33", "Sydney"},
		{"Henry", "29", "Toronto"},
		{"Ivy", "37", "Singapore"},
		{"Jack", "41", "Dubai"},
		{"Kate", "36", "Mumbai"},
		{"Liam", "30", "Dublin"},
	}

	for _, rowData := range additionalRows {
		r := Row{
			MainData: rowData,
			UniqueID: strings.ToLower(rowData[0]),
			Selected: false,
		}
		if includeDetails {
			r.DetailData = []DetailItem{
				{Label: "Email", Value: strings.ToLower(rowData[0]) + "@example.com"},
				{Label: "Phone", Value: "(000) 000-0000"},
				{Label: "Address", Value: "123 Main St, " + rowData[2]},
			}
		}
		table.Rows = append(table.Rows, r)
	}

	return table
}

// AddRow Method to add a new row to the table
func (t Table) AddRow(mainData []string, detailData []DetailItem, uniqueID string) {
	t.Rows = append(t.Rows, Row{
		MainData:   mainData,
		DetailData: detailData,
		UniqueID:   uniqueID,
		Selected:   false,
	})
}
func (i *InputField) ET() string {
	return etInput
}
func (i *InputField) GetID() string {
	return i.ID
}

type InputField struct {
	ID                 string
	Class              string
	ElementType        string
	Type               string // text, password,etc. see https://picocss.com/docs/forms/input
	Label              string // appears above the input
	Name               string // is the key in a form key-value pair as seen by REST API
	PlaceHolder        string // prompts the user regarding what to enter
	Required           bool   // To indicate if the input is required
	Disabled           bool   // To disable the input if needed
	ReadOnly           bool
	Value              string // Default value for the input field
	AriaDescribedBy    string // Must match helper ID.
	AriaLabel          string // ARIA label for accessibility
	AriaInvalid        bool   // To indicate if the input value is invalid
	Helper             Helper
	ValidationFeedback *ValidationFeedback
	ShowLoading        bool                // Flag to show a loading indicator
	LoadingText        string              // Text for the loading indicator
	HtmxFields         []template.HTMLAttr //used for custom endpoints
	ConfirmModal       *ConfirmModal       // used with hx-confirm
}

func (t *TextArea) ET() string {
	return "textarea"
}
func (t *TextArea) GetID() string {
	return t.ID
}

type TextArea struct {
	ID                 string
	Class              string
	ElementType        string
	Type               string // text, password,etc. see https://picocss.com/docs/forms/input
	Label              string // appears above the input
	Name               string // is the key in a form key-value pair as seen by REST API
	PlaceHolder        string // prompts the user regarding what to enter
	Required           bool   // To indicate if the input is required
	Disabled           bool   // To disable the input if needed
	ReadOnly           bool
	Value              template.HTMLAttr // Default value for the input field
	AriaDescribedBy    string            // Must match helper ID.
	AriaLabel          string            // ARIA label for accessibility
	AriaInvalid        bool              // To indicate if the input value is invalid
	Helper             Helper
	ValidationFeedback *ValidationFeedback
	ShowLoading        bool                // Flag to show a loading indicator
	LoadingText        string              // Text for the loading indicator
	HtmxFields         []template.HTMLAttr //used for custom endpoints
	ConfirmModal       *ConfirmModal       // used with hx-confirm
}

func (s *SelectInput) ET() string {
	return "formSelect"
}
func (s *SelectInput) GetID() string {
	return s.ID
}

type SelectInput struct {
	ID                 string
	Class              string
	ElementType        string
	Type               string // text, password,etc. see https://picocss.com/docs/forms/input
	Label              string // appears above the input
	Name               string // is the key in a form key-value pair as seen by REST API
	PlaceHolder        string // prompts the user regarding what to enter
	Required           bool   // To indicate if the input is required
	Disabled           bool   // To disable the input if needed
	ReadOnly           bool
	Value              string // Default value for the input field
	AriaDescribedBy    string // Must match helper ID.
	AriaLabel          string // ARIA label for accessibility
	AriaInvalid        bool   // To indicate if the input value is invalid
	Helper             Helper
	ValidationFeedback *ValidationFeedback
	ShowLoading        bool                // Flag to show a loading indicator
	LoadingText        string              // Text for the loading indicator
	HtmxFields         []template.HTMLAttr //used for custom endpoints
	ConfirmModal       *ConfirmModal       // used with hx-confirm

	Options          []string
	AllowCustomInput bool
}

type Helper struct {
	ID    string // used by aria-described-by
	Class string
	Text  string // General helper text
}
type ConfirmModal struct {
	ID          string
	Class       string
	Title       string
	TitleIcon   string // TitleIcon is a bootstrap icon that can precede title
	WidgetType  string
	Message     string
	CancelText  string
	ConfirmText string
}
type ValidationFeedback struct {
	ID        string
	Class     string
	Message   string // Validation feedback message
	IsInvalid bool   // Flag to indicate if the message is an error message
}

// Methods

func (m *Main) AddSection(section *Section) {
	m.Sections = append(m.Sections, section)
}
func (s *Section) AddGrid(grid *Grid) {
	s.Grids = append(s.Grids, grid)
}
func (g *Grid) AddWidget(e Widget) {
	g.Widgets = append(g.Widgets, e)
}
func (g *Grid) AddWidgets(widgets ...Widget) {
	for _, w := range widgets {
		g.Widgets = append(g.Widgets, w)
	}
}
func (f *Form) AddField(field Element) {
	f.Fields = append(f.Fields, field)
}
func (f *Form) AddAction(action *InputField) {
	f.Actions = append(f.Actions, action)
}
func (i *InputField) AddHelper(id, text string) {
	i.Helper = Helper{
		ID:   id,
		Text: text,
	}
}
func (i *InputField) AddValidationFeedback(id, message string, isInvalid bool) {
	i.ValidationFeedback = &ValidationFeedback{
		ID:        id,
		Message:   message,
		IsInvalid: isInvalid,
	}
}

// Constructors

func NewBody() *Body {
	return &Body{}
}
func NewMain(heading, description string) *Main {
	p := &Main{}
	p.Heading = heading
	p.Description = description
	return p
}
func NewSection() *Section {
	return &Section{}
}
func NewGrid() *Grid {
	return &Grid{}
}
func NewForm(containerClass, title string) *Form {
	f := &Form{}
	f.Class = containerClass
	f.WidgetType = "form"
	f.Title = title
	return f
}
func NewGeneric() *Generic {
	g := new(Generic)
	g.WidgetType = g.WT()
	return g
}
func NewTable(containerClass string) *Table {
	t := &Table{}
	t.Class = containerClass
	t.WidgetType = "table"
	return t
}
func NewInputField() *InputField {
	i := new(InputField)
	i.ElementType = i.ET()
	return i
}

func NewTextArea() *TextArea {
	t := new(TextArea)
	t.ElementType = t.ET()
	return t
}

func NewSelectInput() *SelectInput {
	s := new(SelectInput)
	s.ElementType = s.ET()
	return s
}
func (c ConfirmModal) WT() string { return "modalConfirm" }

func NewConfirmModal() ConfirmModal {
	m := ConfirmModal{
		ID:          "confirmModal",
		Title:       "Confirm Action",
		Message:     "Are you sure you want to proceed?",
		CancelText:  "Cancel",
		ConfirmText: "Confirm",
	}
	m.WidgetType = m.WT()
	return m
}

func (p *ProgressBar) ET() string {
	return "progress"
}
func (p *ProgressBar) GetID() string { return p.ID }

type ProgressBar struct {
	Max             int
	Value           int
	ID              string
	Class           string
	ElementType     string
	Type            string // text, password,etc. see https://picocss.com/docs/forms/input
	AriaDescribedBy string // Must match helper ID.
	AriaLabel       string // ARIA label for accessibility
	Helper          Helper
	HtmxFields      []template.HTMLAttr //used for custom endpoints
}

func NewProgressBar() *ProgressBar {
	p := new(ProgressBar)
	p.ElementType = p.ET()
	return p
}
func GetPrototypeNav() *Nav {
	return &Nav{
		ID:         "nav",
		Class:      "nav",
		WidgetType: "nav",
		Home: SimpleMenu{
			WidgetType: ntSimpleMenu,
			Item: MenuItem{
				ID:     "niHome",
				Class:  "a",
				Href:   "/",
				Target: "_blank",
				Label:  "DOXA Tools",
			},
		},
		Items: []NavItem{
			SimpleMenu{
				WidgetType: ntSimpleMenu,
				Item: MenuItem{
					ID:     "niTable",
					Class:  "a",
					Href:   "table",
					Target: "_blank",
					Label:  "Table",
				},
			},
			SubMenu{
				ID:         "smTable",
				Class:      "sub-menu",
				WidgetType: ntSubMenu,
				Summary:    "Widgets",
				Direction:  "ltr",
				Items: []MenuItem{
					MenuItem{
						ID:    "miAccordion",
						Class: "",
						Href:  "accordion",
						Label: "Accordion",
					},
					MenuItem{
						ID:    "miTable",
						Class: "",
						Href:  "table",
						Label: "Table",
					},
				},
			},
		},
	}
}
func GetProtoTypeSectionWithTable() *Section {
	s := &Section{
		ID:    "section",
		Class: "section",
		Grids: []*Grid{
			NewGrid(),
		},
	}
	s.Grids[0].Widgets = append(s.Grids[0].Widgets, GetTestTable(true, true, true, true))
	return s
}

func (d *Details) ET() string    { return "details" }
func (d *Details) GetID() string { return d.ID }

type Details struct {
	Accordion          bool
	AriaDescribedBy    string
	AriaLabel          string
	Class              string
	ElementType        string
	ID                 string
	IsButton           bool
	Label              string
	Name               string
	Summary            string
	SummaryClass       string
	Contents           []Element
	Helper             Helper
	HtmxFields         []template.HTMLAttr
	ValidationFeedback *ValidationFeedback
	ShowLoading        bool
	LoadingText        string
}

func NewDetails() *Details {
	deets := new(Details)
	deets.ElementType = deets.ET()
	return deets
}

func (dl *DynamicList) ET() string {
	return "dynamicList"
}

func (dl *DynamicList) GetID() string {
	return dl.ID
}

func (dl *DynamicList) Finalize() {
	if dl.Checklist {
		if dl.Selection == nil {
			dl.Selection = make([]bool, len(dl.Contents))
		}
		for len(dl.Selection) < len(dl.Contents) {
			dl.Selection = append(dl.Selection, false)
		}
	}
	if dl.Permissions.AddItems {
		dl.Permissions.AddItemsExceptions = make([]bool, len(dl.Contents))
	}
}

type DynamicList struct {
	ID           string
	Class        string
	ElementType  string
	Checklist    bool
	Selection    []bool //used with checkbox
	HasActions   bool
	Actions      []ActionSet
	Contents     []Element
	Selected     int
	ListEndpoint template.HTMLAttr //unlike htmxfields this will be passed to all list elements
	Permissions  DynamicListPermissions
	HtmxFields   []template.HTMLAttr //used for custom endpoints
	ConfirmModal *ConfirmModal       // used with hx-confirm
	AddModal     GenericModal
}

type ActionSet []Action

type Action interface {
	BsIcon() string
	Class() string
	HtmxFields() []template.HTMLAttr
	SetEndpoint(endpoint, method string)
	SetListContext(listId, itemId string)
	Copy() Action
}

type ListItemInfoAction struct {
	endpoint string
	method   string
	listId   string
	itemId   string
}

func (li *ListItemInfoAction) BsIcon() string {
	return "bi-info-circle"
}

func (li *ListItemInfoAction) Class() string {
	return ""
}

func (li *ListItemInfoAction) HtmxFields() []template.HTMLAttr {
	f1 := fmt.Sprintf(`hx-%s="%s"`, li.method, li.endpoint)
	f2 := `hx-trigger="click"`
	f3 := fmt.Sprintf(`hx-headers='{"DL-Info":"%s","DL-ID":"%s"}' hx-target="#%s"`, li.itemId, li.listId, li.listId)
	return []template.HTMLAttr{
		template.HTMLAttr(f1),
		template.HTMLAttr(f2),
		template.HTMLAttr(f3),
	}
}

func (li *ListItemInfoAction) Copy() Action {
	li2 := new(ListItemInfoAction)
	li2.method = li.method
	li2.itemId = li.itemId
	li2.endpoint = li.endpoint
	li2.listId = li.listId
	return li2
}

func (li *ListItemInfoAction) SetEndpoint(endpoint, method string) {
	li.endpoint = endpoint
	li.method = method
}

func (li *ListItemInfoAction) SetListContext(listId, itemId string) {
	li.listId = listId
	li.itemId = itemId
}

func NewDynamicList() *DynamicList {
	dl := new(DynamicList)
	dl.ElementType = dl.ET()
	dl.Selected = -1 //no selection
	return dl
}

type DynamicListPermissions struct {
	AddItems           bool
	EditItem           bool
	AddItemsExceptions []bool //indices of items that cannot be removed
}

// SetActions applies selected set across all elements in the list
func (dl *DynamicList) SetActions(set ActionSet) {
	dl.HasActions = true
	dl.Actions = make([]ActionSet, len(dl.Contents))
	for i, li := range dl.Contents {
		if li == nil {
			continue
		}
		thisSet := make(ActionSet, len(set))
		for i, ac := range set {
			newAc := ac.Copy()
			newAc.SetListContext(dl.ID, li.GetID())
			thisSet[i] = newAc
		}
		dl.Actions[i] = thisSet
	}
}

func (l *Label) ET() string {
	return "label"
}

func (l *Label) GetID() string {
	return l.ID
}

func (l *Label) Finalize() {

}

type Label struct {
	ID          string
	Class       string
	ElementType string
	Contents    string
	HtmxFields  []template.HTMLAttr
}

func NewLabel() *Label {
	l := new(Label)
	l.ElementType = l.ET()
	return l
}

func (gl *GridLayout) ET() string {
	return "gridLayout"
}

func (gl *GridLayout) GetID() string {
	return gl.ID
}

func (gl *GridLayout) Finalize() {
}

type GridLayout struct {
	ID          string
	Class       string
	ElementType string
	Contents    []Element
	HtmxFields  []template.HTMLAttr
}

func NewGridLayout() *GridLayout {
	gl := new(GridLayout)
	gl.ElementType = gl.ET()
	return gl
}

func (fig *FormInputGroup) ET() string {
	return "formInputGroup"
}

func (fig *FormInputGroup) GetID() string {
	return fig.ID
}

func (fig *FormInputGroup) Finalize() {

}

type FormInputGroup struct {
	ID          string
	Class       string
	ElementType string
	Contents    []Element
	HtmxFields  []template.HTMLAttr
}

func NewFormInputGroup() *FormInputGroup {
	fig := new(FormInputGroup)
	fig.ElementType = fig.ET()
	return fig
}

func (s *ScrollArea) ET() string {
	return "scroll"
}

func (s *ScrollArea) GetID() string {
	return "" //this should probably have a real ID
}

type ScrollArea struct {
	ElementType string
	Contents    []Element
	Vh          int
	Shrink      bool
	HtmxFields  []template.HTMLAttr
}

func NewScrollArea() *ScrollArea {
	s := new(ScrollArea)
	s.ElementType = s.ET()
	return s
}

func (ul *UnorderedList) ET() string {
	return "ul"
}

func (ul *UnorderedList) GetID() string {
	return ""
}

type UnorderedList struct {
	ID          string
	ElementType string
	Contents    []Element
}

func NewUnorderedList() *UnorderedList {
	ul := new(UnorderedList)
	ul.ElementType = ul.ET()
	return ul
}

func GetTestList() *Generic {
	f := NewGeneric()
	g := NewGridLayout()
	cataList := NewDynamicList()
	itemA := NewLabel()
	itemB := NewLabel()
	itemC := NewLabel()
	itemD := NewLabel()
	itemA.Contents = "doxa-seraphimdedes"
	itemB.Contents = "doxa-eac"
	itemC.Contents = "doxa-esdcs"
	itemD.Contents = "doxa-test"
	cataList.Contents = []Element{
		itemA,
		itemB,
		itemC,
		itemD,
	}
	cataList.Selected = 0
	cataList.Permissions.AddItems = true
	cataList.Permissions.AddItemsExceptions = []bool{true, true, true, false}
	cataList.ListEndpoint = `/updateList`
	cataList.Finalize()

	cataList.AddModal = GenericModal{
		ID:    "TestAddModal",
		Title: "Add item",
		Content: []Element{
			//FormMethod: api.POST,
			&InputField{
				ID:          "picoModalGenericAddCatalog",
				ElementType: new(InputField).ET(),
				Type:        "Text",
				Name:        "URL", //TODO:
				AriaLabel:   "Catalog URL",
			},
			&InputField{
				ElementType: new(InputField).ET(),
				Type:        "submit",
				Name:        "Add",
				Value:       "Add Catalog",
				HtmxFields: []template.HTMLAttr{
					`hx-post="/updateList"`,
					`hx-on:htmx:configRequest='event.detail.headers["DL-Add"] = "test"'`, //TODO: this doesn't work, but the input field NAME is in response
					`hx-target="#testlist"`,
				},
			},
		},
	}

	libList := NewDynamicList()
	lib1 := NewLabel()
	lib2 := NewLabel()
	lib3 := NewLabel()
	lib4 := NewLabel()
	lib5 := NewLabel()
	lib6 := NewLabel()
	lib7 := NewLabel()

	lib1.Contents = "assets"
	lib2.Contents = "configs"
	lib3.Contents = "en_redirects_goarch"
	lib4.Contents = "en_us_dedes"
	lib5.Contents = "en_us_goa"
	lib6.Contents = "en_us_saas"
	lib7.Contents = "templates"
	libList.Contents = []Element{lib1, lib2, lib3, lib4, lib5, lib6, lib7}
	libList.Checklist = true
	libList.Finalize()
	cataList.HasActions = true
	cataList.ID = "testlist"
	infAction := new(ListItemInfoAction)
	infAction.SetEndpoint("/getDLInfo", "get")
	acts := make(ActionSet, 0, 1)
	acts = append(acts, infAction)
	cataList.SetActions(acts)
	//g2 := NewGridLayout()
	//g2.Contents = []Element{cataList, libList}
	//g.Contents = []Element{g2, third}
	g.Contents = []Element{cataList, libList}
	f.Contents = []Element{g}
	f.ID = "testform"
	return f
}

type GenericModal struct {
	ID      string
	Title   string
	Content []Element
}

func (a *Anchor) ET() string {
	return "anchor"
}

func (a *Anchor) GetID() string {
	return a.ID
}

type Anchor struct {
	ElementType string
	Href        string
	ID          string
	Role        string
	Class       string
	HtmxFields  []template.HTMLAttr
	Contents    []Element
}

func NewAnchor() *Anchor {
	a := new(Anchor)
	a.ElementType = a.ET()
	return a
}

func (ft *FlexTable) ET() string {
	return "flexTable"
}

func (ft *FlexTable) GetID() string {
	return ft.ID
}

type FlexAttrs struct {
	Grow   bool
	Shrink bool
	Basis  string
}

type FlexTable struct {
	ID          string
	Class       string
	ElementType string
	Contents    []Element // Will contain FlexTR elements
	HtmxFields  []template.HTMLAttr
	FlexAttrs   []FlexAttrs
}

func NewFlexTable() *FlexTable {
	ft := new(FlexTable)
	ft.ElementType = ft.ET()
	ft.FlexAttrs = make([]FlexAttrs, 0)
	return ft
}

func (tr *FlexTR) ET() string {
	return "flexTR"
}

func (tr *FlexTR) GetID() string {
	return tr.ID
}

type FlexTR struct {
	ID          string
	Class       string
	ElementType string
	Contents    []Element // Will contain the "cells" as Elements
	HtmxFields  []template.HTMLAttr
}

func NewFlexTR() *FlexTR {
	tr := new(FlexTR)
	tr.ElementType = tr.ET()
	return tr
}

// SubscriptionManager implements the Widget interface for managing subscriptions
type SubscriptionManager struct {
	ID           string
	Class        string
	WidgetType   string
	Title        string
	TitleIcon    string
	Description  string
	GroupName    string    // Current group being viewed
	RepoName     string    // Current repo being viewed
	ShowGroups   bool      // Whether to show the groups table
	ShowProjects bool      // Whether to show the projects table
	Groups       []Group   // List of GitLab groups
	Projects     []Project // List of repos in current group
}

// Group represents a GitLab group in the subscription manager
type Group struct {
	Name        string
	Maintainer  string
	Description string
}

// Project represents a GitLab project/repo in the subscription manager
type Project struct {
	Name        string
	DbName      string
	Language    string
	Country     string
	Description string
	Subscribed  bool
	Stable      bool
	Unstable    bool
}

func (sm *SubscriptionManager) WT() string {
	return "subscriptionManager"
}

// NewSubscriptionManager creates a new SubscriptionManager widget
func NewSubscriptionManager() *SubscriptionManager {
	sm := &SubscriptionManager{
		WidgetType: "subscriptionManager",
		Class:      WidgetClass,
		ShowGroups: true, // Start by showing groups
	}
	return sm
}

// SetGroups updates the widget to show the groups view
func (sm *SubscriptionManager) SetGroups(groups []Group) {
	sm.Groups = groups
	sm.ShowGroups = true
	sm.ShowProjects = false
	sm.GroupName = ""
	sm.RepoName = ""
}

// SetProjects updates the widget to show projects for a specific group
func (sm *SubscriptionManager) SetProjects(groupName string, projects []Project) {
	sm.Projects = projects
	sm.ShowGroups = false
	sm.ShowProjects = true
	sm.GroupName = groupName
	sm.RepoName = ""
}

// ShowProjectDetails updates the widget to show detailed info for a specific project
func (sm *SubscriptionManager) ShowProjectDetails(groupName, repoName string, project Project) {
	sm.ShowGroups = false
	sm.ShowProjects = false
	sm.GroupName = groupName
	sm.RepoName = repoName
}

// Reset returns the widget to its initial state
func (sm *SubscriptionManager) Reset() {
	sm.ShowGroups = true
	sm.ShowProjects = false
	sm.GroupName = ""
	sm.RepoName = ""
	sm.Groups = nil
	sm.Projects = nil
}

// GetHtmxHeaders returns HTMX headers for the current widget state
func (sm *SubscriptionManager) GetHtmxHeaders() []template.HTMLAttr {
	headers := make([]template.HTMLAttr, 0)
	if sm.GroupName != "" {
		headers = append(headers, template.HTMLAttr(`hx-headers='{"Group-Name": "`+sm.GroupName+`"}'`))
	}

	if sm.RepoName != "" {
		headers = append(headers, template.HTMLAttr(`hx-headers='{"Repo-Name": "`+sm.RepoName+`"}'`))
	}
	return headers
}
