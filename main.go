/*******************************************************************************
 * Copyright © 2019-2025 Michael A. Colburn, Copyright © 2025 LIML Professional Services, LLC. Copyright © The Orthodox Christian Missionary Center (OCMC)
 *
 * This program is provided under the terms of the Eclipse Public License v. 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Developer: Michael A. Colburn
 *
 * Contributors:
 *    Johnpaul Humphrey since 2025
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

// This is the entry point for the Doxa application.
// It starts a shell that provides a cli (command line interface).
// Various commands are available for the building
// and publishing of Eastern Orthodox Liturgical texts.
// It also starts a web application.
// Available commands are enumerated in pkg/enums/commands/commands.go
package main

import (
	"bufio"
	"crypto/sha256"
	"embed"
	"fmt"
	"github.com/c-bata/go-prompt"
	"github.com/fatih/color"
	"github.com/liturgiko/doxa/api"
	"github.com/liturgiko/doxa/app"
	"github.com/liturgiko/doxa/cli"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/catalog"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/enums/commands"
	"github.com/liturgiko/doxa/pkg/enums/goos"
	"github.com/liturgiko/doxa/pkg/enums/properties"
	"github.com/liturgiko/doxa/pkg/enums/versionStatuses"
	"github.com/liturgiko/doxa/pkg/inOut"
	"github.com/liturgiko/doxa/pkg/keyring"
	"github.com/liturgiko/doxa/pkg/layouts"
	"github.com/liturgiko/doxa/pkg/meta_template"
	"github.com/liturgiko/doxa/pkg/site"
	"github.com/liturgiko/doxa/pkg/syncsrvc"
	"github.com/liturgiko/doxa/pkg/updater"
	"github.com/liturgiko/doxa/pkg/users"
	"github.com/liturgiko/doxa/pkg/utils/clinput"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/repos"
	"github.com/liturgiko/doxa/servers"
	"log/slog"
	"os"
	"os/user"
	"path"
	"runtime"
	"strconv"
	"strings"
	"time"
)

// VERSION For a new release, set version number and list issues addressed, found at https://gitlab.com/ocmc/liturgiko/doxa/-/issues
// also do the same in xcmd/releaser/release.yaml
var VERSION = "v0.131.1" //

// TODO: finish work on creating patches for new releases
//
//go:embed patch/genSite
var embeddedGenSitePatch embed.FS // contents to patch user's generated website for a new release

//go:embed patch/scripts
var embeddedScriptsPatch embed.FS // contents to patch user's generated website for a new release

//go:embed templates/genSite/*
var embeddedGenSiteTemplates embed.FS

//go:embed templates/webApp/*
var embeddedAppTemplates embed.FS

//go:embed static/*
var embeddedAppStaticFiles embed.FS

var siteBuilder *site.Builder

var DoxaApi *api.API

func main() {
	var err error
	// check for command line args passed into main
	cliRequestDebug, // enable debug level for logging
		cliRequestNewDb,                     // create an empty database
		cliRequestRebuildDb,                 // rebuild existing database
		cliRequestSyncEnabled,               // turn sync on
		cliRequestSyncSim,                   // turn sync simulation on and turn sync on
		noShell,                             // no interactive shell
		noUpgrade,                           // ignore new version if available
		altHome,                             // override .doxa/config.yaml doxahome value
		altProject := checkMainArgs(os.Args) // override project value in config.yaml
	// initialize the logger
	doxlog.Init(altHome, altProject, false)
	// and defer the closing of the log file
	defer func() {
		err = doxlog.LogFile.Close()
		if err != nil {
			fmt.Printf("error closing log: %v", err)
		}
	}()
	doxlog.Infof("--noShell == %v", noShell)
	doxlog.Infof("--noUpgrade == %v", noUpgrade)

	// app.Init opens the database file.
	app.Init(cliRequestSyncEnabled, cliRequestSyncSim, &embeddedScriptsPatch)
	// To keep it open, we defer the close here in main.
	defer func() {
		app.CloseDatabase()
	}()
	if app.Ctx.Debug || cliRequestDebug {
		doxlog.Level.Set(slog.LevelDebug)
	}
	doxlog.Infof("log %s", doxlog.Level.String())
	// Note: see package doxlog.  Use doxlog to
	// both display to standard out and the logfile.
	// Use slog only for info to be written to standard out.
	// Use slog.Info to avoid cluttering the log with unnecessary entries.
	doxlog.Info("Starting...")                           // useful to have in logfile
	slog.Info("this will take a few seconds....")        // we don't need this in logfile
	doxlog.Infof("version: %s", VERSION)                 // important to have in logfile
	doxlog.Infof("home directory: %s", app.Ctx.DoxaHome) // etc.
	doxlog.Infof("project directory: %s", app.Ctx.Paths.ProjectDirPath)
	if cliRequestNewDb {
		err = newDb()
	}
	if cliRequestRebuildDb {
		err = rebuildDb()
		if err != nil {
			doxlog.Error(err.Error())
		}
	}

	// write the Doxa README.md file, only if we are executing in a bin directory (the end user's machine)
	readmePath := app.Ctx.Paths.HomePath
	err = ltfile.CreateDirs(readmePath)
	if err != nil {
		doxlog.Error(fmt.Sprintf("error creating path to README.md: %v", err))
	} else {
		readmePath = path.Join(readmePath, "README.md")
		doxlog.Infof("writing %s", readmePath)
		err = ltfile.WriteFile(readmePath, DoxaReadMe)
		if err != nil {
			doxlog.Error(fmt.Sprintf("error writing %s: %v", readmePath, err))
		}
	}
	// check for a newer version of doxa
	if noUpgrade {
		doxlog.Infof("--noUpgrade flag was passed, so doxa will not check for updates.")
	} else if !app.Ctx.Docker {
		doxlog.Infof("Checking for updates to doxa...")
		// if a newer version exists, and the user wants to upgrade to it,
		// the current binary will be copied from doxa/bin to doxa/bin/.previous/.doxa
		// and the latest version will be downloaded and installed to doxa/bin.
		// doxa will then restart.
		app.Ctx.DoxaUpdater, err = updater.NewDoxaUpdater(VERSION)
		if err != nil {
			doxlog.Errorf("error getting NewDoxaUpdater: %v", err)
		} else {
			// delete bin/tmp if not successfully removed during the last update
			if ltfile.DirExists(app.Ctx.DoxaUpdater.Paths.TempDownload) {
				_ = ltfile.DeleteDirRecursively(app.Ctx.DoxaUpdater.Paths.TempDownload)
			}
			if err != nil {
				msg := fmt.Sprintf("%v", err)
				if strings.HasPrefix(msg, "GET error") {
					doxlog.Error("Could not read doxa update information.  Are you connected to the Internet?")
				} else {
					doxlog.Error(fmt.Sprintf("%s", err))
				}
			} else {
				switch app.Ctx.DoxaUpdater.VersionStatus() {
				case versionStatuses.BehindLatest:
					doxlog.Infof("You have doxa %s. The latest version is %s.", app.Ctx.DoxaUpdater.LocalDoxaVersion, app.Ctx.DoxaUpdater.ReleaseInfo.ReleaseId)
					if len(app.Ctx.DoxaUpdater.ReleaseInfo.ReleaseInfo) > 0 {
						doxlog.Infof("In the new version:\n\t%s", app.Ctx.DoxaUpdater.ReleaseInfo.ReleaseInfo)
					}
					err = app.Ctx.DoxaUpdater.Update()
					if err != nil {
						doxlog.Infof("error updating doxa: %v", err)
					}
				default:
					doxlog.Infof("A newer version is not available.")
				} // doxa_0.2.31_Darwin_arm64.tar.gz
				if err != nil {
					err = app.Ctx.DoxaUpdater.Restore()
					if err != nil {
						doxlog.Errorf("%s", err)
					}
				} else {
					app.Ctx.DoxaUpdater.AddDoxaBinPath()
				}
			}
		}
	}
	// For conciseness and readability, set some globals using the property values
	doxlog.Infof("Setting Globals")
	setGlobals()

	DoxaApi = api.NewApi()

	// Now, you can use `api` for whatever you need...

	// create a Database Manager
	doxlog.Infof("Creating cli database manager")
	app.Ctx.DbM = cli.NewDbM(app.Ctx.PortApp, app.Ctx.DataStores, config.DbName, config.SM, config.DoxaPaths, app.Ctx.CatalogManager)
	//add plugins to database manager. right now, we only have one
	app.Ctx.DbM.EdPlugins = append(app.Ctx.DbM.EdPlugins, new(catalog.EdPlugin)) //TODO: do we need an install method?
	app.Ctx.DbM.CliPlugins = append(app.Ctx.DbM.CliPlugins, new(catalog.PublishPlugin))
	app.Ctx.DbM.CliPlugins = append(app.Ctx.DbM.CliPlugins, new(catalog.RefreshPlugin))
	app.Ctx.DbM.CliPlugins = append(app.Ctx.DbM.CliPlugins, new(catalog.SubscribePlugin))
	app.Ctx.DbM.CliPlugins = append(app.Ctx.DbM.CliPlugins, new(catalog.UnsubscribePlugin))

	// set the editor to use when the user wants to edit a record within the shell
	doxlog.Infof("Setting record editor")
	app.Ctx.DbM.RecEditor = RecEditor

	// if this is a new database, we need to populate it
	// with biblical text, liturgical text, and the media map.
	doxlog.Infof("checking to see if this is a new, empty database")
	kp := kvs.NewKeyPath()
	kp.Dirs.Push("ltx")
	if !app.Ctx.Kvs.Db.Exists(kp) {
		doxlog.Info("the database is empty")
		err = downloadImportFiles(app.Ctx.Paths.ImportPath)
		if err != nil {
			doxlog.Errorf("error downloading database import files: %v", err)
			os.Exit(1)
		}
		var hadErrors bool
		var totalRecords int
		exportedFiles, _ := ltfile.FileMatcher(app.Ctx.Paths.ImportPath, "tsv", nil)
		for _, ex := range exportedFiles {
			count, errors := app.Ctx.Kvs.Db.Import("", ex, inOut.LineParser, true, false, true)
			if len(errors) > 0 {
				hadErrors = true
				for _, e := range errors {
					doxlog.Errorf("%s: %v", ex, e)
				}
			}
			totalRecords = totalRecords + count
		}
		if hadErrors {
			doxlog.Errorf("%d records imported, but, there were errors-- check the log", totalRecords)
		} else {
			doxlog.Infof("%d records imported")
			doxlog.Info("finished loading database")
		}
	} else {
		doxlog.Infof("database already has data")
	}

	// make sure we have an assets directory with contents
	doxlog.Infof("checking to see if project assets directory has contents")
	if !ltfile.FileExists(path.Join(app.Ctx.Paths.AssetsPath, "css", "app.css")) {
		doxlog.Infof("assets directory does not exist, so creating it")
		err = downloadAssets(app.Ctx.Paths.AssetsPath)
		if err != nil {
			doxlog.Errorf("error cloning assets: %v", err)
			os.Exit(1)
		}
	} else {
		doxlog.Infof("ok, assets directory exists and has contents")
	}

	doxlog.Info("Copying any missing asset files...")
	placesToCopyTo := []string{app.Ctx.Paths.AssetsPath}
	if ltfile.DirExists(path.Join(app.Ctx.Paths.SiteGenPublicPath, "css")) {
		placesToCopyTo = append(placesToCopyTo, app.Ctx.Paths.SiteGenPublicPath)
	}
	if ltfile.DirExists(path.Join(app.Ctx.Paths.SiteGenTestPath, "css")) {
		placesToCopyTo = append(placesToCopyTo, app.Ctx.Paths.SiteGenTestPath)
	}
	err = ltfile.CopyEmbedded(embeddedGenSitePatch, "patch/genSite/assets", placesToCopyTo)
	if err != nil {
		doxlog.Infof("error copying missing asset files: %v", err)
	}
	// if there are no templates for the project,
	// initialize them.
	doxlog.Infof("checking that templates exist")
	if app.Ctx == nil {
		doxlog.Panic("app.Ctx == nil")
	} else if app.Ctx.Paths == nil {
		doxlog.Panic("app.Ctx.Paths == nil")
	} else if len(app.Ctx.Paths.SubscriptionsPath) == 0 {
		doxlog.Panic("len(app.Ctx.Paths.SubscriptionsPath == 0")
	} else if len(app.Ctx.Paths.PrimaryTemplatesPath) == 0 {
		doxlog.Panic("len(app.Ctx.Paths.SubscriptionsPath == 0")
	}

	// start up the synchronizer, which will do git push and pulls
	// app.Ctx.SyncManager.RoSyncer.SyncAutoOn(15 * time.Second)
	// app.Ctx.SyncManager.RwSyncer.SyncAutoOn(15 * time.Second)
	// subscriptions.Subscriptions() //TODO: replace

	// initialize the layout manager
	doxlog.Infof("initializing layout manager")
	layouts.Init()

	// load the keyring
	err = LoadKeyRing(app.Ctx.ConfigName)
	if err != nil {
		doxlog.Errorf("%s", err)
	}

	StartSync()
	// set up the site builder
	// TODO: remove.  MAC--this is what properly sets up the template fallback directory.
	doxlog.Infof("creating site builder")
	siteBuilder, err = site.NewBuilder(VERSION,
		false,
		app.Ctx.Kvs,
		app.Ctx.Realm,
		MediaUrlAudio, // TODO: what about media enabled?
		MediaUrlPdf,
		PdfViewer,
		app.Ctx.Project,
		config.SM,
		embeddedGenSitePatch,
		embeddedGenSiteTemplates,
		nil,   // will be set by the server
		false, // will be set by the server
		app.Ctx.Keyring,
		&app.Ctx.BuildSyncMutex,
		app.Ctx.Bus,
	)
	if err != nil {
		doxlog.Panicf("error creating siteBuilder: %s", err)
	}
	if siteBuilder.Config.TemplatesFallbackEnabled {
		doxlog.Info("Template fallback is enabled.")
		doxlog.Infof("Primary templates folder is %s", app.Ctx.Paths.PrimaryTemplatesPath)
		doxlog.Infof("Fallback templates folder is %s", app.Ctx.Paths.FallbackTemplatesPath)
		if !ltfile.DirExists(app.Ctx.Paths.FallbackTemplatesPath) {
			err = ltfile.CreateDirs(app.Ctx.Paths.FallbackTemplatesPath)
			if err != nil {
				doxlog.Errorf("error creating fallback template directory: %s", err)
				return
			}
		}
	}
	if err != nil {
		doxlog.Panicf("error initializing site builder, please report: %s", err)
	}

	// see if user wants meta-templates to be created
	if config.SM.BoolProps[properties.SysRemoteMetaTemplateCreate] {
		go meta_template.CreateMetaTemplates(config.SM.StringSliceProps[properties.SysRemoteMetaTemplateUrls], app.Ctx.Paths.MetaTemplatesPath)
	}
	// set up a call-back mechanism for the web server to tell func main() to quit.
	// it is called if 1) another instance starts or 2) the user clicks on App.Exit in a browser.
	var fp func()
	fp = Quit // see function Quit() defined below

	doxlog.Info("starting http servers")
	if app.Ctx.Docker {
		go servers.ServeGeneratedPublicSite(app.Ctx.Paths.SiteGenPublicPath,
			config.SM.StringProps[properties.SiteBuildPublishPublicUrl],
			app.Ctx.PortSitePublic,
			MediaUrlPdf,
			PdfViewer)
		go servers.ServeGeneratedTestSite(app.Ctx.Paths.SiteGenTestPath,
			config.SM.StringProps[properties.SiteBuildPublishPublicUrl],
			app.Ctx.PortSiteTest,
			MediaUrlPdf,
			PdfViewer)

		servers.Serve(VERSION,
			DoxaApi,
			app.Ctx.Kvs,
			app.Ctx.Keyring,
			config.SM,
			app.Ctx.SyncManager,
			app.Ctx.UserManager,
			app.Ctx.Paths.DbPath,
			app.Ctx.Paths.Project,
			"",
			app.Ctx.Paths.ProjectDirPath,
			"",
			app.Ctx.PortApp,
			app.Ctx.PortSitePublic,
			app.Ctx.PortSiteTest,
			app.Ctx.PortMedia,
			app.Ctx.PortMessage,
			app.Ctx.Cloud,
			app.Ctx.Debug,
			siteBuilder,
			embeddedAppTemplates,
			embeddedAppStaticFiles,
			fp)
	} else {
		// start the http servers
		go func() {
			go servers.Serve(VERSION,
				DoxaApi,
				app.Ctx.Kvs,
				app.Ctx.Keyring,
				config.SM,
				app.Ctx.SyncManager,
				app.Ctx.UserManager,
				app.Ctx.Paths.DbPath,
				app.Ctx.Paths.Project,
				"",
				app.Ctx.Paths.ProjectDirPath,
				"",
				app.Ctx.PortApp,
				app.Ctx.PortSitePublic,
				app.Ctx.PortSiteTest,
				app.Ctx.PortMedia,
				app.Ctx.PortMessage,
				app.Ctx.Cloud,
				app.Ctx.Debug,
				siteBuilder,
				embeddedAppTemplates,
				embeddedAppStaticFiles,
				fp)
			go servers.ServeGeneratedPublicSite(app.Ctx.Paths.SiteGenPublicPath,
				config.SM.StringProps[properties.SiteBuildPublishPublicUrl],
				app.Ctx.PortSitePublic,
				MediaUrlPdf,
				PdfViewer)
			go servers.ServeGeneratedTestSite(app.Ctx.Paths.SiteGenTestPath,
				config.SM.StringProps[properties.SiteBuildPublishPublicUrl],
				app.Ctx.PortSiteTest,
				MediaUrlPdf,
				PdfViewer)
			if MediaEnabled {
				if strings.Contains(MediaUrlPdf, "localhost") {
					servers.ServeMedia(app.Ctx.Paths.MediaDirPath,
						config.SM.StringProps[properties.SiteBuildPublishPublicUrl],
						app.Ctx.PortMedia)
				}
			}
		}()
		// sleep a bit to allow the app server to start and display its messages
		time.Sleep(1 * time.Second)
		if noShell {
			doxlog.Info("--noShell flag set so skipping shell")
			doxlog.Info("doxa will terminate when REST endpoint /quit is called")
		} else {
			doxlog.Info("Starting doxa shell...")
			fmt.Println("Use the `exit` command to exit this program or `help` for more information.")
		}
		doxlog.WriteCliOut = false

		// If there is any OS specific things to do,
		// we can use context.GoOS.
		// e.g.,
		// switch context.GoOS {
		//    case goos.Darwin
		// }

		if noShell {
			for {
				// deliberate endless loop
				// doxa will terminate when
				// REST endpoint /quit is called
			}
		} else if app.Ctx.TTY { // tty does not use the go-prompt library
			// set up a simple read, eval, print loop (repl)
			if len(os.Args) > 0 {
				sb := strings.Builder{}
				for _, a := range os.Args {
					sb.WriteString(a)
					sb.WriteString(" ")
				}
				reader := bufio.NewReader(os.Stdin)

				for {
					pt := LivePrefixState.LivePrefix
					if len(pt) == 0 {
						pt = "doxa> "
					}
					promptMsg := color.HiYellowString(pt)
					fmt.Print(promptMsg)
					text, _ := reader.ReadString('\n')
					// convert CRLF to LF
					text = strings.Replace(text, "\n", "", -1)
					executor(text)
				}
			}
		} else { // use the go-prompt library
			// create the command prompt and run it
			p := prompt.New(
				executor,
				completer,
				prompt.OptionPrefix("doxa> "),
				prompt.OptionLivePrefix(changeLivePrefix),
				prompt.OptionPrefixTextColor(prompt.Yellow),
				prompt.OptionCompletionWordSeparator(" "),
			)
			p.Run()
		}
	}
}

func checkMainArgs(args []string) (debug, newDb, rebuildDb, syncEnabled, syncSim, noShell, noUpgrade bool, altHome, altProject string) {
	if len(args) > 1 {
		for i := 1; i < len(args); i++ {
			switch args[i] {
			case "-d", "--debug", "debug", "-debug":
				debug = true
			case "-h", "--help", "help", "-help":
				showCommandLineOptions()
				os.Exit(0)
			case "-n", "--new", "new", "-new":
				newDb = true
			case "-ns", "--noShell", "noShell", "-noShell":
				noShell = true
			case "-nu", "--noUpgrade", "noUpgrade", "-noUpgrade":
				noUpgrade = true
			case "-p", "--project", "project", "-project":
				if len(args) > i {
					i++
					altProject = strings.TrimSpace(args[i])
				} else {
					fmt.Println("you did not provide a project name")
					continue
				}
				if strings.HasPrefix(altProject, "-") ||
					len(altProject) == 0 {
					fmt.Printf("invalid project name %s\n", altProject)
					altProject = ""
					continue
				}
			case "-r", "--rebuild", "rebuild", "-rebuild":
				rebuildDb = true
			case "-s", "--sync", "sync", "-sync":
				if len(args) > i+1 {
					i++
					syncSwitch := strings.TrimSpace(args[i])
					switch syncSwitch {
					case "y", "yes", "true", "t", "on":
						syncEnabled = true
					case "n", "no", "false", "f", "off":
						syncEnabled = false
					case "sim", "simulate":
						syncEnabled = true
						syncSim = true
					default:
						syncEnabled = false
					}
				} else {
					fmt.Println("sync flag (-s, --sync, sync) missing parameter (y, yes, true, t, on, n, no, false, f, off): e.g, `-s on` or `-s off`")
					fmt.Printf("\tor, simulate: e.g, `-s simulate` or `-s simulate,`, which also has the effect of y, true, t, on")
					os.Exit(1)
				}
			case "-v", "--version", "version", "-version":
				fmt.Println(VERSION)
				os.Exit(0)
			case "-w", "--workspace", "workspace", "-workspace": // aka doxa home
				if len(args) > i {
					i++
					altHome = strings.TrimSpace(args[i])
				} else {
					fmt.Println("you did not provide a path to the doxa directory")
					return
				}
				if strings.HasPrefix(altHome, "-") ||
					len(altHome) == 0 {
					fmt.Printf("invalid command %s\n", altHome)
					altHome = ""
					continue
				}
			default:
				fmt.Printf("unknown flag %s\n", args[i])
				showCommandLineOptions()
				return
			}
		}
	}
	return
}
func showCommandLineOptions() {
	fmt.Println("command line options are:")
	fmt.Println("-d | --debug | debug - run doxa in debug mode")
	fmt.Println("-n | --new | new - new database")
	fmt.Println("-h | --help | help - show doxa command line options")
	fmt.Println("-p  {project name} | --project  {project name} | project {project name}")
	if app.Ctx != nil && app.Ctx.Paths != nil {
		fmt.Printf("\t- overrides project from %s\n", app.Ctx.Paths.DoxaConfigPath)
	} else {
		fmt.Println("\t- overrides project from config")
	}
	fmt.Println("-r | --rebuild | rebuild - rebuild the database")
	fmt.Println("-s | --seed | seed - key for encryption/decryption")
	fmt.Println("-v, --version, version - show doxa version")
	fmt.Println("-w, --workspace, workspace - alternative path to doxa home")
}
func newDb() (err error) {
	var backupPath string
	backupPath, err = app.BackupDb()
	if err != nil {
		return err
	}
	doxlog.Infof("database backed up to %s", backupPath)
	app.CloseDatabase()
	err = ltfile.DeleteFile(app.Ctx.Paths.DbPath)
	if err != nil {
		msg := fmt.Sprintf("error deleting database: %v", err)
		doxlog.Errorf(msg)
		return fmt.Errorf(msg)
	}
	app.ReOpenDatabase()
	return nil
}
func rebuildDb() (err error) {
	backupExportsDir := path.Join(app.Ctx.Paths.BackupPath, "exports")
	var backupPath string
	backupPath, err = app.BackupDb()
	if err != nil {
		return err
	}
	// make sure we have exported files
	var exportedFiles []string
	exportedFiles, err = ltfile.FileMatcher(backupExportsDir, "tsv", nil)
	if err != nil {
		msg := fmt.Sprintf("exported database to %s, but an error occurred reading them back: %v", backupExportsDir, err)
		doxlog.Error(msg)
		return fmt.Errorf(msg)
	}
	if len(exportedFiles) == 0 {
		msg := fmt.Sprintf("exported database to %s, but there are no files there", backupExportsDir)
		doxlog.Error(msg)
		return fmt.Errorf(msg)
	}
	// we are ok to continue, so...
	doxlog.Info("closing database")
	app.CloseDatabase()
	doxlog.Info("deleting database")
	err = ltfile.DeleteFile(app.Ctx.Paths.DbPath)
	if err != nil {
		doxlog.Panicf("error removing database: %v", err)
	}
	doxlog.Info("creating a new database")
	app.ReOpenDatabase()
	var totalRecords int
	var hadErrors bool
	doxlog.Infof("importing %d files", len(exportedFiles))
	for _, ex := range exportedFiles {
		count, errors := app.Ctx.Kvs.Db.Import("", ex, inOut.LineParser, true, false, true)
		if len(errors) > 0 {
			hadErrors = true
			for _, e := range errors {
				doxlog.Errorf("%s: %v", ex, e)
			}
		}
		totalRecords = totalRecords + count
	}
	if hadErrors {
		doxlog.Errorf("%d records imported, but, there were errors-- check the log", totalRecords)
		doxlog.Warnf("If you have problems with the rebuilt database, you can close doxa, then copy the original database from %s, rename it liturgical.db, then restart doxa.", backupPath)
		//doxlog.Info("Restoring old database")
		//app.CloseDatabase()
		//err = ltfile.CopyFile(backupPath, filepath.Join(app.Ctx.Paths.DbPath))
		//if err != nil {
		//	doxlog.Panicf("error restoring database from %s: %v", backupPath, err)
		//}
		//app.ReOpenDatabase()
		//doxlog.Info("successfully restored original database")
	} else {
		doxlog.Infof("%d records imported", totalRecords)
		doxlog.Info("finished rebuilding database")
	}
	return nil
}
func downloadAssets(assetsPath string) error {
	url := "https://gitlab.com/doxa-liml/assets.git"
	doxlog.Infof("Cloning demonstration website asset files from %s", url)
	fmt.Println("Depending on your internet connection speed, this could take 15 minutes or more...")
	err := repos.CloneTo(assetsPath, url, true, true, true)
	if err != nil {
		return fmt.Errorf("error cloning demo project from %s into %s: %v", url, assetsPath, err)
	}
	// delete the .git file so it doesn't interfere with rw
	err = ltfile.DeleteDirRecursively(path.Join(assetsPath, ".git"))
	if err != nil {
		doxlog.Errorf("Could not delete .git folder from assets. '%v' this may interfere with some operations.", err)
	}
	return nil
}
func downloadImportFiles(assetsPath string) error {
	url := "https://gitlab.com/doxa-liml/resources/init.git"
	doxlog.Infof("Cloning demonstration website database import files from %s", url)
	fmt.Println("Depending on your internet connection speed, this could take 15 minutes or more...")
	err := repos.CloneTo(assetsPath, url, true, true, true)
	if err != nil {
		return fmt.Errorf("error cloning demo project from %s into %s: %v", url, assetsPath, err)
	}
	return nil
}
func Quit() {
	GiveGlory()
	os.Exit(0)
}
func subscriptionChoice() bool {
	q := "Do you want to use Fr. Seraphim Dedes templates? (Recommended)"
	var o = []string{
		"n", "no",
		"y", "yes",
	}
	switch clinput.GetInput(q, o) {
	case 'n':
		return false
	case 'y':
		return true
	default:
		fmt.Println("Invalid selection...")
	}
	return false
}

func LoadKeyRing(configName string) error {
	titlesPath := path.Join("configs", configName, properties.SiteBuildPagesTitles.Data().Path)
	kr := keyring.NewKeyRing(app.Ctx.Kvs, titlesPath)
	doxlog.Info("Building keyring")
	err := kr.Build()
	if err != nil {
		msg := fmt.Sprintf("error creating keyring: %v", err)
		doxlog.Error(msg)
		return fmt.Errorf(msg)
	}
	app.Ctx.Keyring = kr
	app.Ctx.SyncManager.SetKeyring(kr)
	return nil
}

func setTimeFromKeyValue(topic, key string, thing *time.Duration) {
	if freq, exists := app.Ctx.SysManager.LoadCurrentProjectSettingByKey(topic, key); exists {
		num, err := time.ParseDuration(freq)
		if err != nil {
			return
		}
		*thing = num
	}
}

// StartSync will start syncher if requested by the user via the autoSync on command
func StartSync() {
	if app.Ctx.SysManager != nil {
		if app.Ctx.SyncManager != nil {
			//SUBSCRIPTIONS SECTION
			if subSync, exists := app.Ctx.SysManager.LoadCurrentProjectSettingByKey("subscriptionSync", "auto"); exists {
				if subSync == "on" {
					app.Ctx.SyncManager.RoSyncer.SyncAutoOn(10 * time.Second)
				}
			}
			setTimeFromKeyValue("subscriptionSync", "onlineWait", &app.Ctx.SyncManager.RoSyncer.(*syncsrvc.ReadOnlySyncer).OnlineWaitDuration)
			setTimeFromKeyValue("subscriptionSync", "offlineWait", &app.Ctx.SyncManager.RoSyncer.(*syncsrvc.ReadOnlySyncer).OffLineWaitDuration)
			//RW SECTION

			//We'll also do the internet here while we;re at it

			setTimeFromKeyValue("netstat", "offlineInterval", &app.Ctx.OfflineInterval)
			setTimeFromKeyValue("netstat", "onlineInterval", &app.Ctx.OnlineInterval)
		}
	}
}

var GLORY string
var MediaEnabled bool
var MediaUrlAudio string
var MediaUrlPdf string
var PdfViewer string
var RecEditor string

var LivePrefixState struct {
	LivePrefix string
	IsEnable   bool
}

type Cmd struct {
	which       commands.Command
	parent      commands.Command
	flags       []string
	hungryFlags map[string]string
	args        []string
}

func ParseCmd(args []string) *Cmd {
	cmd := new(Cmd)
	//first one is parent command
	switch args[0] {
	case commands.RO.Abr():
		cmd.parent = commands.RO
	}
	if len(args) < 2 {
		return cmd
	}
	cmd.hungryFlags = make(map[string]string)
	flagMap := make(map[string]int)
	flagMap["-h"] = 0
	flagMap["--help"] = 0
	flagMap["-l"] = 1
	flagMap["--library"] = 1
	switch args[1] {
	case commands.ROConfig.Abr():
		cmd.which = commands.ROConfig
		flagMap["-d"] = 0
		flagMap["--default"] = 0
	case commands.ROInfo.Abr():
		cmd.which = commands.ROInfo
		flagMap["-v"] = 0

	case commands.ROList.Abr():
		cmd.which = commands.ROList
		flagMap["-I"] = 0
		flagMap["--exclude-installed"] = 0
		flagMap["-i"] = 0
		flagMap["--installed-only"] = 0
		flagMap["-q"] = 1
		flagMap["--query"] = 1

	case commands.ROGet.Abr():
		cmd.which = commands.ROGet

		flagMap["-n"] = 0
		flagMap["--no-thanks"] = 0
		flagMap["-v"] = 1
		flagMap["--version"] = 1
		flagMap["-y"] = 0
		flagMap["--yes-sir"] = 0

	case commands.ROUnsubscribe.Abr():
		cmd.which = commands.ROUnsubscribe
	case commands.ROUpdates.Abr():
		cmd.which = commands.ROUpdates
	case commands.ROSubscribe.Abr():
		cmd.which = commands.ROSubscribe
	default:
		return cmd
	}
	leftOver := args[2:]
	//for i, arg := range leftOver {
	for len(leftOver) > 0 {
		arg := leftOver[0]
		leftOver = leftOver[1:]
		if strings.HasPrefix(arg, "--") {
			if count, valid := flagMap[arg]; valid {
				if count == 0 {
					cmd.flags = append(cmd.flags, arg)
				} else {
					if len(leftOver) < 1 {
						fmt.Printf("flag `%s` requires at least one argument\n", arg)
						return nil
					}
					cmd.hungryFlags[arg] = leftOver[0]
					leftOver = leftOver[1:]
					continue
				}
			} else {
				fmt.Printf("Invalid flag `%s` valid flags are:\n", arg)
				for k, _ := range flagMap {
					fmt.Println(k)
				}
			}
			continue
		} else if strings.HasPrefix(arg, "-") {
			if len(arg) >= 2 {
				for _, item := range arg[1:] {
					if count, valid := flagMap["-"+string(item)]; valid {
						if count == 0 {
							cmd.flags = append(cmd.flags, "-"+string(item))
						} else {
							if len(leftOver) < 1 {
								fmt.Printf("flag `%s` requires at least one argument\n", string(item))
								return nil
							}
							cmd.hungryFlags["-"+string(item)] = leftOver[0]
							leftOver = leftOver[1:]
							continue
						}
					} else {
						fmt.Printf("Invalid flag `%s` valid flags are:\n", arg)
						for k, _ := range flagMap {
							fmt.Println(k)
						}
						return nil
					}
				}
			}
			continue
		} else {
			cmd.args = append(cmd.args, arg)
		}
	}
	return cmd
}

func executor(in string) {
	lmlOutDir := app.Ctx.Paths.PrimaryTemplatesPath
	fallbackEnabled := config.SM.BoolProps[properties.SiteBuildTemplatesFallbackEnabled]
	if fallbackEnabled {
		lmlOutDir = app.Ctx.Paths.FallbackTemplatesPath
	}
	if in == "" {
		return
	}
	args := strings.Split(in, " ")
	switch args[0] {
	case commands.A2L.Abr():
		cli.A2L(lmlOutDir, app.Ctx.Project,
			app.Ctx.DbM.Mapper,
			&app.Ctx.Keyring.MediaReverseLookup,
		)
	case commands.AGES.Abr():
		app.Ctx.Command = commands.AGES
		if len(args) > 1 {
			if args[1] == "a2l" {
				cli.A2L(
					lmlOutDir,
					app.Ctx.Project,
					app.Ctx.DbM.Mapper,
					&app.Ctx.Keyring.MediaReverseLookup,
				)
			}
		}
	case commands.Build.Abr(), commands.BuildIndex.Abr():
		siteBuilder.RelayUsesStandardOut = true
		cli.Build(args, siteBuilder)
		siteBuilder.RelayUsesStandardOut = false
	case commands.Database.Abr():
		if app.Ctx.Command != commands.Database {
			app.Ctx.DbM.ResetContext()
		}
		app.Ctx.Command = commands.Database
	case commands.Eform.Abr():
		app.Ctx.Command = commands.Eform
	case commands.Exit.Abr(), commands.Quit.Abr():
		fmt.Println("Bye!")
		Quit()
	case commands.Help.Abr(), "?":
		cli.Help(app.Ctx.Command, app.Ctx.Suggestions)
	case commands.Home.Abr():
		app.Ctx.Command = commands.Home
	case commands.Internet.Abr():
		app.Ctx.Command = commands.Internet
		if app.Ctx.InternetAvailable {
			fmt.Println("Internet available")
		} else {
			fmt.Println("Internet not available")
		}
	case commands.Interval.Abr():
		if len(args) < 2 {
			fmt.Printf("checks internet status every %s when online and every %s when offline.\n", app.Ctx.OnlineInterval.String(), app.Ctx.OfflineInterval.String())
			return
		}
		var interval, onlineI, offlineI time.Duration
		offlineI = app.Ctx.OfflineInterval
		onlineI = app.Ctx.OnlineInterval
		if len(args) == 3 {
			amount, err := strconv.ParseInt(args[2], 10, 64)
			if err != nil {
				interval, err = time.ParseDuration(args[2])
				if err != nil {
					return
				}
			} else {
				interval = time.Duration(amount) * time.Minute
			}
			onlineI = interval
			offlineI = interval
		}
		switch args[1] {
		case "--online":
			app.Ctx.OnlineInterval = onlineI
			fmt.Printf("checks internet status every %s when online.", app.Ctx.OnlineInterval.String())
		case "--offline":
			app.Ctx.OfflineInterval = offlineI
			fmt.Printf("checks internet status every %s when offline.", app.Ctx.OfflineInterval.String())
		default:
			amount, err := strconv.ParseInt(args[1], 10, 64)
			if err != nil {
				interval, err = time.ParseDuration(args[1])
				if err != nil {
					return
				}
			}
			interval = time.Duration(amount) * time.Minute
			app.Ctx.OfflineInterval = offlineI
			app.Ctx.OnlineInterval = onlineI
		}
		app.Ctx.SysManager.StoreCurrentProjectSettingByKey("netstat", "onlineInterval", app.Ctx.OnlineInterval.String())
		app.Ctx.SysManager.StoreCurrentProjectSettingByKey("netstat", "offlineInterval", app.Ctx.OfflineInterval.String())
	case commands.KbM.Abr():
		app.Ctx.Command = commands.KbM
		fmt.Println("Welcome to the Knowledge-Base Manager")
	case commands.Ldp.Abr():
		app.Ctx.Command = commands.Ldp
		ldp(args)
	case commands.Load.Abr():
		var reloadKeyRing bool
		reloadKeyRing = cli.Load(app.Ctx.Kvs, &app.Ctx.Keyring.MediaReverseLookup, app.Ctx.Project, false, false, config.SM)
		if reloadKeyRing {
			doxlog.Info("Rebuilding keyring...")
			err := LoadKeyRing(app.Ctx.ConfigName)
			if err != nil {
				msg := fmt.Sprintf("error loading keyring: %v", err)
				doxlog.Panic(msg)
			}
			doxlog.Info("Finished rebuilding keyring...")
		}
	case commands.Login.Abr():
		app.Ctx.Bus.Publish("gitlab:tokenNeeded")
		//a little time
		time.Sleep(70 * time.Millisecond)
	case commands.Logout.Abr():
		app.Ctx.UserManager.Vault.RevokeKey(users.LocalUser)
		app.Ctx.UserManager.IsDecrypted = false
		app.Ctx.Bus.Publish("gitlab:tokenRevoked")
		fmt.Println("You are now logged out")
	case commands.NetSim.Abr():
		if len(args) > 1 {
			if args[1] == "off" || args[1] == "down" || args[1] == "offline" {
				app.Ctx.SyncManager.SetOfflineSimulation(true)
			} else if args[1] == "on" || args[1] == "online" || args[1] == "up" {
				app.Ctx.SyncManager.SetOfflineSimulation(false)
			}
		}
	case commands.Restore.Abr():
		err := app.Ctx.DoxaUpdater.Restore()
		if err != nil {
			doxlog.Errorf(fmt.Sprintf("error restoring doxa to previous version: %v", err))
		} else {
			app.Ctx.DoxaUpdater.AddDoxaBinPath()
		}
	case commands.RO.Abr():
		cmd := ParseCmd(args)
		if cmd == nil {
			fmt.Println("invalid syntax")
			return
		}
		switch cmd.which {
		case commands.ROSubscribe:
			if len(cmd.args) == 0 {
				fmt.Println(`Usage: ro subscribe <catalog> [-l library]

subscribe to a particular catalog. if the -l flag is specified, subscribe to a specific library within that catalog`)
				return
			}
			library := ""
			for flag, arg := range cmd.hungryFlags {
				switch flag {
				case "-l", "--library":
					library = arg
				default:
					fmt.Printf("warning: flag %s is not yet supported.\n", flag)
				}
			}
			catName := cmd.args[0]
			cat := app.Ctx.CatalogManager.GetCatalogFromDb(nil, catName, "latest")
			libs := make([]string, 0)
			if library == "" {
				if cat.Entries != nil {
					cur := app.Ctx.CatalogManager.GetPrimarySubscriptionCatalog()
					if cur != nil {
						if cat.Name != cur.Name {
							if clinput.GetInput(fmt.Sprintf("you are already subscribed to %s, change your subscription to %s?", cur.Name, cat.Name), []string{"y", "yes", "n", "no"}) != 'y' {
								fmt.Println("Cancelled")
								return
							}
						}
					}
				}
				for k, _ := range cat.Entries {
					libs = append(libs, k)
				}
			} else {
				libs = append(libs, library)
			}
			for _, lib := range libs {
				app.Ctx.CatalogManager.MarkSubscribed(lib, catName, false, make(map[string]*struct{}))
				if lib == config.TemplatesDir {
					//update paths for templates
					config.DoxaPaths.FallbackTemplatesPath = path.Join(config.DoxaPaths.SubscriptionsPath, config.TemplatesDir)
					app.Ctx.Paths.FallbackTemplatesPath = config.DoxaPaths.FallbackTemplatesPath
					config.SM.SetValueByType(properties.SiteBuildTemplatesFallbackDir, catName)
					config.SM.SetValueByType(properties.SiteBuildTemplatesFallbackEnabled, "true")
					app.Ctx.Bus.Publish("subscriptions:set", fmt.Sprintf("%s:%s", catName, lib))
				}
			}
			app.Ctx.SysManager.StoreCurrentProjectSettingByKey("ro", "catalog", cat.Name)
			app.Ctx.SysManager.StoreCurrentProjectSettingByKey("ro", "version", cat.Version)
			if library == "" {
				fmt.Printf("Subscribed to %s\n", catName)
				return
			}
			fmt.Printf("Subscribed to %s from %s\n", library, catName)
		case commands.ROUnsubscribe:
			target := app.Ctx.CatalogManager.GetPrimarySubscriptionCatalog()
			if target == nil {
				return
			}
			if len(cmd.args) > 0 {
				target = app.Ctx.CatalogManager.GetCatalogFromDb(nil, cmd.args[1], "latest")
				if target == nil {
					return
				}
			}
			if sel, exists := cmd.hungryFlags["-l"]; exists {
				app.Ctx.CatalogManager.MarkUnsubscribed(sel)
				fmt.Println("You are no longer subscribed to " + sel)
				return
			}
			app.Ctx.CatalogManager.ClearSubscriptions()
			fmt.Println("You are no longer subscribed to " + target.Name)
		case commands.ROUpdates:
			if DoxaApi == nil {
				fmt.Println("error: the api has not yet been initialized")
				return
			}
			if DoxaApi.RoSync == nil {
				fmt.Println("error: the api was initialized without roSync!")
				return
			}
			payload, resp, err := DoxaApi.RoSync.GetAvailableUpdates()
			if err != nil {
				fmt.Fprintf(os.Stderr, "Error fetching available updates: %v\n", err)
				os.Exit(1)
			}

			if resp.RequestStatus != api.OK {
				fmt.Println("Request status:", resp.RequestStatus)
				for _, msg := range resp.RequestMessages {
					fmt.Println(msg)
				}
				if resp.RequestStatus == api.InputError {
					fmt.Println("No updates available.")
					return
				}
			}

			if len(payload) == 0 {
				fmt.Println("No updates available.")
				return
			}

			fmt.Println("Available updates:")
			for _, update := range payload {
				inventoryFlag := ""
				if update.IsInventory {
					inventoryFlag = " [I]"
				}
				dbName := path.Base(update.DbPath)
				fmt.Printf("%s\t%s\t%s%s\n", dbName, update.DbPath, update.Catalog, inventoryFlag)
			}

			fmt.Printf("\nTotal updates available: %d\n", len(payload))
		case commands.ROGet:
			if lib, exists := cmd.hungryFlags["--library"]; exists {
				cmd.hungryFlags["-l"] = lib
			}
			if lib, exists := cmd.hungryFlags["-l"]; exists {
				fmt.Println("Updating " + lib)
				go func() {
					err := app.Ctx.SyncManager.RoSyncer.Sync(lib)
					if err != nil {
						doxlog.Errorf(fmt.Sprintf("error running ro sync: %v", err))
					}
				}()
				return
			}
			fmt.Println("Updating subscriptions")
			go app.Ctx.SyncManager.RoSyncer.SyncAll()
		case commands.ROList:
			if len(cmd.flags) > 0 {
				for _, flag := range cmd.flags {
					if flag == "-h" || flag == "--help" {
						fmt.Println(`USAGE: ro list [catalog]
If run without arguments, lists available catalogs.
If catalog is specified, lists items inside specified catalog.`)
						return
					}
				}
			}
			if len(cmd.args) == 0 {
				fmt.Println("Available catalogs:")
				for _, cn := range app.Ctx.CatalogManager.AvailableCatalogNames() {
					fmt.Println(" * " + cn)
				}
				mySub := app.Ctx.CatalogManager.GetPrimarySubscriptionCatalog()
				mySubName := "(none)"
				if mySub != nil {
					mySubName = mySub.Name
				}
				fmt.Printf("\nyou are currently subscribed to %s\n", mySubName)
				return
			}
			selectedCat := app.Ctx.CatalogManager.GetCatalogFromDb(nil, cmd.args[0], "latest")
			if selectedCat == nil {
				fmt.Printf("could not find catalog '%s'\n", cmd.args[0])
				return
			}
			if selectedCat.Entries == nil {
				fmt.Printf("catalog %s has no entries!\n", selectedCat.Name)
			}
			for _, ent := range selectedCat.Entries {
				fmt.Println(ent.Path)
			}
		case commands.ROConfig:
			if len(cmd.flags) > 0 {
				for _, flag := range cmd.flags {
					if flag == "-h" || flag == "--help" {
						fmt.Println(`USAGE: ro config [setting] [value]
when run without any arguments, displays current settings.
when setting is specified, displays the value of that setting.
when value is specified, sets the setting to that value.`)
						return
					}
				}
			}
			roSync := app.Ctx.SyncManager.RoSyncer.(*syncsrvc.ReadOnlySyncer)
			if len(cmd.args) == 0 {
				if len(cmd.flags) == 0 {
					fmt.Printf("auto:\t%v\nonlineWait:\t%s\nofflineWait:\t%s\n", roSync.AutoOn, roSync.OnlineWaitDuration.String(), roSync.OffLineWaitDuration.String())
				}
				return
			}
			if len(cmd.args) == 1 {
				switch cmd.args[0] {
				case "auto":
					fmt.Printf("auto:\t%v\n", roSync.AutoOn)
				case "onlineWait":
					fmt.Printf("onlineWait:\t%s\n", roSync.OnlineWaitDuration.String())
				case "offlineWait":
					fmt.Printf("offlineWait:\t%s\n", roSync.OffLineWaitDuration.String())
				}
				return
			}
			if len(cmd.args) == 2 {
				switch cmd.args[0] {
				case "auto":
					switch cmd.args[1] {
					case "on", "yes", "true", "enable":
						app.Ctx.SysManager.StoreCurrentProjectSettingByKey("subscriptionSync", "auto", "on")
						roSync.SyncAutoOn(1 * time.Second)
						roSync.AutoOn = true
					case "off", "no", "false", "disable":
						app.Ctx.SysManager.StoreCurrentProjectSettingByKey("subscriptionSync", "auto", "off")
						roSync.SyncAutoOff()
						roSync.AutoOn = false
					}
					fmt.Printf("auto:\t%v\n", roSync.AutoOn)
				case "onlineWait":
					//parse the input
					dur, err := time.ParseDuration(cmd.args[1])
					if err != nil {
						fmt.Printf("%v\n", err)
						return
					}
					roSync.OnlineWaitDuration = dur
					app.Ctx.SysManager.StoreCurrentProjectSettingByKey("subscriptionSync", "onlineWait", dur.String())
					fmt.Printf("onlineWait:\t%s\n", roSync.OnlineWaitDuration.String())
				case "offlineWait":
					dur, err := time.ParseDuration(cmd.args[1])
					if err != nil {
						fmt.Printf("%v\n", err)
						return
					}
					roSync.OffLineWaitDuration = dur
					app.Ctx.SysManager.StoreCurrentProjectSettingByKey("subscriptionSync", "offlineWait", dur.String())
					fmt.Printf("offlineWait:\t%s\n", roSync.OffLineWaitDuration.String())
				}
				return
			}
		case commands.ROInfo:
			if len(cmd.flags) > 0 {
				for _, flag := range cmd.flags {
					if flag == "-h" || flag == "--help" {
						fmt.Println(`USAGE: ro info [catalog] [-l library]
when run without any arguments displays information on current status of ro.
when catalog specified, displays catalog-specific information. 
when -l is specified, display information about the specified library if it is in the catalog`)
						return
					}
					if flag == "-v" {
						ents := app.Ctx.SyncManager.RoSyncer.GetAllFromMap()
						for _, ent := range ents {
							if ent.HasIssues() {
								fmt.Printf("in %s:\n", ent.FsPath)
								fmt.Println(ent.IssuesToString())
							}
						}
					}
				}
			}
			if len(cmd.args) == 0 {
				fmt.Println(app.Ctx.SyncManager.RoSyncer.Status())
				bigCata := app.Ctx.CatalogManager.GetPrimarySubscriptionCatalog()
				if bigCata != nil {
					fmt.Printf("You are currently subscribed to %s\n", bigCata.Name)
				} else {
					fmt.Printf("You are not currently subscribed to any catalog. use %s %s to subscribe\n", commands.RO.Abr(), commands.ROSubscribe.Abr())
				}
				return
			}
			selCat := app.Ctx.CatalogManager.GetCatalogFromDb(nil, cmd.args[0], "latest")
			if selCat == nil {
				fmt.Printf("Could not find catalog %v\n", cmd.args[0])
				return
			}
			if lib, exists := cmd.hungryFlags["--library"]; exists {
				cmd.hungryFlags["-l"] = lib
			}
			if lib, exists := cmd.hungryFlags["-l"]; exists {
				//TODO: version support
				selLib := app.Ctx.CatalogManager.GetEntryFromDb(nil, lib, selCat.Name, "")
				if selLib == nil {
					fmt.Printf("could not find %s in catalog %s\n", lib, selCat.Name)
					return
				}
				fmt.Printf("Name: %s\nPath: %s\nUrl: %s\nMaintainer: %s\nLicense: %s\nVersion: %s\n%s\n", selLib.Name, selLib.Path, selLib.Url, selLib.Maintainer, selLib.License, selLib.Version, selLib.Desc)
				return
			}
			fmt.Printf("Name: %s\nUrl: %s\nMaintainer: %s\nVersion: %s\n%s\n", selCat.Name, selCat.URL, selCat.Maintainer, selCat.Version, selCat.Desc)
		default:
			fmt.Println(`USAGE: ro <command>
Available Commands:
	config		adjust ro settings
	update		download files from the subscribed catalog
	info		view the name of the subscribed catalog and last update
	list		list available catalogs
	subscribe	subscribe to a catalog
	unsubscribe	unsubscribe from the subscribed catalog`)
		}
	case commands.Change.Abr():
		manual := `Usage: change <password|token>

Change authentication credentials.

Options:
	password	Change the password used to access the token
	token		Change the GitLab token used for publishing`
		if len(args) < 2 {
			fmt.Println(manual)
			return
		}
		switch args[1] {
		case "password":
			if !app.Ctx.UserManager.IsDecrypted {
				fmt.Println("You are not logged in. Enter your old password to set a new one. If you forgot your old password, re-enter your gitlab token instead.")
				fmt.Printf("Password: ")
				input, err := clinput.GetPassword(false)
				if err != nil {
					fmt.Printf("%v\n", err)
				}
				if strings.HasPrefix(input, "glpat-") {
					//it's a token
					app.ResetTokenAndPassword(input)
					return
				} else if input == "" {
					return
				} else {
					app.Ctx.UserManager.Vault.AddKey(users.LocalUser, sha256.Sum256([]byte(input)))
					if _, err := app.Ctx.UserManager.GetToken(users.LocalUser); err != nil {
						fmt.Println("Incorrect password")
						return
					}
					app.Ctx.UserManager.IsDecrypted = true
				}
			}
			fmt.Printf("New Password: ")
			pwd, err := clinput.GetPassword(true)
			if err != nil {
				fmt.Printf("%v", err)
				return
			}
			app.Ctx.UserManager.Vault.AddKey(users.LocalUser, sha256.Sum256([]byte(pwd)))
			app.Ctx.UserManager.IsDecrypted = true
			fmt.Println("Password changed")
		case "token":
			if !app.Ctx.UserManager.IsDecrypted {
				app.ResetTokenAndPassword("")
				return
			}
			fmt.Printf("Gitlab Token: ")
			token, err := clinput.GetPassword(false)
			if err != nil {
				fmt.Printf("%v\n", err)
				return
			}
			fmt.Println("Token changed")
			app.Ctx.Bus.Publish("gitlab:tokenSet", token)
		default:
			fmt.Println(manual)
		}

	case commands.Server.Abr():
		doxlog.Infof("doxa app is running at http://127.0.0.1:%v\n", config.SM.StringProps[properties.SysServerPortsDoxa])
		doxlog.Infof("doxa api is running at http://127.0.0.1:%v\n", config.SM.StringProps[properties.SysServerPortsApi])
		doxlog.Infof("Generated public site is at http://127.0.0.1:%v\n", config.SM.StringProps[properties.SysServerPortsPublicSite])
		doxlog.Infof("Generated test site is at http://127.0.0.1:%v\n", config.SM.StringProps[properties.SysServerPortsTestSite])
		doxlog.Info("Click on a link to open in your browser.")
		doxlog.Infof("You can change the port numbers in the database at: %s%s\n", config.SM.ConfigPath, properties.SysServerPortsDoxa.Data().Path)
	case commands.SyncUserMaintainedOn.Abr(), commands.SyncSubscriptionsOn.Abr():
		var syncSrvc syncsrvc.SyncerInterface
		switch app.Ctx.Command {
		case commands.SyncUserMaintained:
			syncSrvc = app.Ctx.SyncManager.RwSyncer
		default:
			return
		}
		syncSrvc.SyncAutoOn(10 * time.Second)
		fmt.Println(syncSrvc.Status())
	case commands.SyncUserMaintainedOff.Abr(), commands.SyncSubscriptionsOff.Abr():
		var syncSrvc syncsrvc.SyncerInterface
		switch app.Ctx.Command {
		case commands.SyncUserMaintained:
			syncSrvc = app.Ctx.SyncManager.RwSyncer
		default:
			return
		}
		syncSrvc.SyncAutoOff()
		fmt.Println(syncSrvc.Status())
	case commands.SyncSubscriptionsNow.Abr(), commands.SyncUserMaintainedNow.Abr():
		var syncSrvc syncsrvc.SyncerInterface
		fmt.Println("Synchronizing in background while you continue working. use syncStatus to check")
		switch app.Ctx.Command {
		case commands.SyncUserMaintained:
			syncSrvc = app.Ctx.SyncManager.RwSyncer
		default:
			return
		}
		go func() {
			err := syncSrvc.SyncAll()
			if err != nil {
				fmt.Printf("[sync] %v\n", err)
				doxlog.Errorf("%v\n", err)
			}
		}()
	case commands.SyncStatus.Abr():
		fmt.Println(app.Ctx.SyncManager.RwSyncer.Status())
	case commands.SyncUserMaintained.Abr():
		app.Ctx.Command = commands.SyncUserMaintained
		if len(args) > 1 {
			switch args[1] {
			case "off":
				app.Ctx.SyncManager.RwSyncer.SyncAutoOff()
				fmt.Println(app.Ctx.SyncManager.RwSyncer.Status())
			case "on":
				app.Ctx.SyncManager.RwSyncer.SyncAutoOn(10 * time.Second)
				fmt.Println(app.Ctx.SyncManager.RwSyncer.Status())
			case "now": // TODO
				fmt.Println(app.Ctx.SyncManager.RwSyncer.Status())
			default:
				fmt.Println(app.Ctx.SyncManager.RwSyncer.Status())
			}
		}
	case commands.Version.Abr():
		fmt.Println(VERSION)
	default:
		switch app.Ctx.Command {
		case commands.ROConfig: //TODO: fix
			if len(args) < 2 {
				switch args[0] {
				}
				return
			}
			switch args[0] {
			case "autoSync":
				switch args[1] {
				case "off":
					app.Ctx.SysManager.StoreCurrentProjectSettingByKey("subscriptionSync", "auto", "off")
					app.Ctx.SyncManager.RoSyncer.SyncAutoOff()
					fmt.Println(app.Ctx.SyncManager.RoSyncer.Status())
				case "on":
					app.Ctx.SysManager.StoreCurrentProjectSettingByKey("subscriptionSync", "auto", "on")
					app.Ctx.SyncManager.RoSyncer.SyncAutoOn(10 * time.Second)
					fmt.Println(app.Ctx.SyncManager.RoSyncer.Status())
				}
			case "syncFrequency":
				unit := 'm'
				if args[1] == "-offline" {
					if len(args) < 3 {
						fmt.Printf("Interval between internet checks when offline is %f minutes.\n", app.Ctx.SyncManager.RoSyncer.(*syncsrvc.ReadOnlySyncer).OffLineWaitDuration.Minutes())
						return
					}
					if strings.HasSuffix(args[2], "s") {
						unit = 's'
						args[2] = strings.TrimSuffix(args[2], "s")
					}
					if strings.HasSuffix(args[2], "m") {
						unit = 'm'
						args[2] = strings.TrimSuffix(args[2], "m")
					}
					freq, err := strconv.ParseInt(args[2], 10, 64)
					if err != nil {
						fmt.Printf("Couldn't understand input: %v\ninput should be given in minutes\n", err)
						return
					}
					dur := time.Duration(freq)
					switch unit {
					case 'm':
						dur *= time.Minute
					case 's':
						dur *= time.Second
					default:
						fmt.Println("Invalid unit specified. Must specify m or s")
						return
					}
					app.Ctx.SyncManager.RoSyncer.(*syncsrvc.ReadOnlySyncer).OffLineWaitDuration = dur
					app.Ctx.SysManager.StoreCurrentProjectSettingByKey("subscriptionSync", "offlineWait", dur.String())
					return
				}

				if strings.HasSuffix(args[1], "h") {
					unit = 'h'
					args[1] = strings.TrimSuffix(args[1], "h")
				}
				if strings.HasSuffix(args[1], "m") {
					unit = 'm'
					args[1] = strings.TrimSuffix(args[1], "m")
				}
				freq, err := strconv.ParseInt(args[1], 10, 16)
				if err != nil {
					fmt.Printf("Couldn't understand input: %v\ninput should be given in minutes\n", err)
					return
				}
				dur := time.Duration(freq)
				switch unit {
				case 'h':
					dur *= time.Hour
				case 'm':
					dur *= time.Minute
				default:
					fmt.Println("Invalid unit specified. Must specify h or m")
					return
				}
				app.Ctx.SyncManager.RoSyncer.(*syncsrvc.ReadOnlySyncer).OnlineWaitDuration = dur
				app.Ctx.SysManager.StoreCurrentProjectSettingByKey("subscriptionSync", "onlineWait", dur.String())
				return
			}

		case commands.Database:
			app.Ctx.DbM.Executor(in)
		case commands.Ldp:
			ldp(args)
			//default:
			//	fmt.Println("I did not understand you.")
		}
	}
	prefix := app.Ctx.Command.Abr() + "> "
	if app.Ctx.Command == commands.Database {
		if dbMPrefix, ok := app.Ctx.DbM.LivePrefix(); ok {
			prefix = dbMPrefix
		}
	}
	LivePrefixState.LivePrefix = prefix
	LivePrefixState.IsEnable = true

}

// completer provides the command line autocompletion prompts.
// The prompts provided depend on the value of context.Command.
func completer(in prompt.Document) []prompt.Suggest {
	var s []prompt.Suggest
	command := app.Ctx.Command
	if len(in.Text) > 0 {
		currentCmdSlice := strings.Fields(in.Text)
		if len(currentCmdSlice) > 0 {
			currentCmd := currentCmdSlice[0]
			for _, c := range commands.CommandValues() {
				if currentCmd == c.Abr() {
					command = c
					break
				}
			}
		}
	}

	switch command {
	case commands.AGES:
		s = []prompt.Suggest{
			{Text: commands.A2L.Abr(), Description: commands.A2L.DescShort()},
			{Text: commands.Load.Abr(), Description: commands.Load.DescShort()},
		}
	case commands.Database:
		s = app.Ctx.DbM.Suggestions
	case commands.Internet:
		s = []prompt.Suggest{
			{Text: commands.Exit.Abr(), Description: commands.Exit.DescShort()},
			{Text: commands.Help.Abr(), Description: commands.Help.DescShort()},
			{Text: commands.Home.Abr(), Description: commands.Home.DescShort()},
			{Text: commands.Interval.Abr(), Description: "interval between checking availability"},
			{Text: commands.Interval.Abr() + " n", Description: "e.g., interval 15, sets both offline and online intervals"},
			{Text: commands.Interval.Abr() + " --offline n", Description: "e.g., interval --offline 15, if Internet not available, wait 15 minutes between checking again"},
			{Text: commands.Interval.Abr() + " --online n", Description: "e.g., interval --online 5, when Internet is available, wait 15 minutes between checking again"},
			{Text: commands.Status.Abr(), Description: commands.Status.DescShort() + "Internet status"},
		}
	case commands.Ldp:
		s = []prompt.Suggest{
			{Text: commands.All.Abr(), Description: commands.All.DescShort()},
			{Text: commands.Day.Abr(), Description: commands.Day.DescShort()},
			{Text: commands.Eval.Abr(), Description: commands.Eval.DescShort()},
			{Text: commands.EvalLukanCycleDay.Abr(), Description: commands.EvalLukanCycleDay.DescShort()},
			{Text: commands.EvalModeOfWeek.Abr(), Description: commands.EvalModeOfWeek.DescShort()},
			{Text: commands.EvalMovableCycleDay.Abr(), Description: commands.EvalMovableCycleDay.DescShort()},
			{Text: commands.EvalDauOfDay.Abr(), Description: commands.EvalDauOfDay.DescShort()},
			{Text: commands.EvalSundayAfterElevationOfCross.Abr(), Description: commands.EvalSundayAfterElevationOfCross.DescShort()},
			{Text: commands.EvalSundaysBeforeTriodion.Abr(), Description: commands.EvalSundaysBeforeTriodion.DescShort()},
			{Text: commands.Exit.Abr(), Description: commands.Exit.DescShort()},
			{Text: commands.Feasts.Abr(), Description: commands.Feasts.DescShort()},
			{Text: commands.Help.Abr(), Description: commands.Help.DescShort()},
			{Text: commands.Home.Abr(), Description: commands.Home.DescShort()},
			{Text: commands.Model.Abr(), Description: commands.Model.DescShort()},
			{Text: commands.Month.Abr(), Description: commands.Month.DescShort()},
			{Text: commands.Year.Abr(), Description: commands.Year.DescShort()},
		}
	case commands.RO:
		s = []prompt.Suggest{
			{Text: commands.ROConfig.Abr(), Description: commands.ROConfig.DescShort()},
			{Text: commands.ROSubscribe.Abr(), Description: commands.ROSubscribe.DescShort()},
			{Text: commands.ROUnsubscribe.Abr(), Description: commands.ROUnsubscribe.DescShort()},
			{Text: commands.ROUpdates.Abr(), Description: commands.ROUpdates.DescShort()},
			{Text: commands.ROList.Abr(), Description: commands.ROList.DescShort()},
			{Text: commands.ROInfo.Abr(), Description: commands.ROInfo.DescShort()},
			{Text: commands.ROGet.Abr(), Description: commands.ROGet.DescShort()},
		}
		chunks := strings.Fields(in.Text)
		if len(chunks) > 1 {
			switch chunks[1] {
			case commands.ROUnsubscribe.Abr():
				s = []prompt.Suggest{}
				if len(chunks) == 3 && (chunks[2] == "-l" || chunks[2] == "--library") {
					//get catalog name
					cata := app.Ctx.CatalogManager.GetPrimarySubscriptionCatalog()
					if cata != nil && cata.Entries != nil {
						for _, ent := range cata.Entries {
							s = append(s, prompt.Suggest{Text: ent.Path, Description: ent.Desc})
						}
					}
				}
			case commands.ROSubscribe.Abr():
				s = []prompt.Suggest{}
				if strings.HasPrefix(chunks[len(chunks)-1], "-") {
					s = []prompt.Suggest{
						{Text: "-l", Description: "choose specific library"},
						{Text: "-h", Description: "get help"},
					}
				}
				if len(chunks) == 2 || (len(chunks) == 4 && (chunks[2] == "-l" || chunks[2] == "--library")) {
					for _, n := range app.Ctx.CatalogManager.AvailableCatalogNames() {
						s = append(s, prompt.Suggest{Text: n})
					}
				}
				if len(chunks) == 4 && (chunks[3] == "-l" || chunks[3] == "--library") {
					//get catalog name
					selCatName := chunks[2]
					if app.Ctx.CatalogManager.CatalogExists(selCatName) {
						cata := app.Ctx.CatalogManager.GetCatalogFromDb(nil, selCatName, "latest")
						if cata != nil && cata.Entries != nil {
							for _, ent := range cata.Entries {
								s = append(s, prompt.Suggest{Text: ent.Path, Description: ent.Desc})
							}
						}
					}
				}
				if strings.HasPrefix(chunks[len(chunks)-1], "--") {
					s = []prompt.Suggest{
						{Text: "--library", Description: "choose specific library"},
						{Text: "--help", Description: "get help"},
					}
				}
			case commands.ROGet.Abr():
				if strings.HasPrefix(chunks[len(chunks)-1], "-") {
					s = []prompt.Suggest{
						{Text: "-l", Description: "choose specific library"},
						{Text: "-h", Description: "get help"},
					}
				}
				if chunks[len(chunks)-2] == "-l" || chunks[len(chunks)-2] == "--library" {
					for _, n := range app.Ctx.CatalogManager.Subscriptions() {
						s = append(s, prompt.Suggest{Text: n.Path})
					}
				}
				if strings.HasPrefix(chunks[len(chunks)-1], "--") {
					s = []prompt.Suggest{
						{Text: "--library", Description: "choose specific library"},
						{Text: "--help", Description: "get help"},
					}
				}
			case commands.ROList.Abr():
				s = []prompt.Suggest{
					{"-h", "get help"},
				}
				for _, n := range app.Ctx.CatalogManager.AvailableCatalogNames() {
					s = append(s, prompt.Suggest{Text: n})
				}
			case commands.ROInfo.Abr():
				s = []prompt.Suggest{
					{Text: "-l", Description: "choose specific library"},
					{Text: "-h", Description: "get help"},
				}
				for _, n := range app.Ctx.CatalogManager.AvailableCatalogNames() {
					s = append(s, prompt.Suggest{Text: n})
				}
				if strings.HasPrefix(chunks[len(chunks)-1], "--") {
					s = []prompt.Suggest{
						{Text: "--library", Description: "choose specific library"},
						{Text: "--help", Description: "get help"},
					}
				}
			case commands.ROConfig.Abr():
				s = []prompt.Suggest{
					{Text: "auto", Description: "enable/disable automatic updates"},
					{Text: "onlineWait", Description: "delay between updates when online"},
					{Text: "offlineWait", Description: "delay between attempts when offline"},
					{Text: "-h", Description: "get help"},
				}
				if len(chunks) > 2 {
					roSync := app.Ctx.SyncManager.RoSyncer.(*syncsrvc.ReadOnlySyncer)
					switch chunks[2] {
					case "auto":
						s = []prompt.Suggest{
							{Text: "true", Description: "enable automatic updates"},
							{Text: "false", Description: "disable automatic updates"},
						}
					case "onlineWait":
						s = []prompt.Suggest{
							{Text: roSync.OnlineWaitDuration.String(), Description: "current value"},
						}
					case "offlineWait":
						s = []prompt.Suggest{
							{Text: roSync.OffLineWaitDuration.String(), Description: "current value"},
						}
					}
				}
			}
		}
	case commands.Server:
		s = []prompt.Suggest{
			{Text: commands.Exit.Abr(), Description: commands.Exit.DescShort()},
			{Text: commands.Help.Abr(), Description: commands.Help.DescShort()},
			{Text: commands.Home.Abr(), Description: commands.Home.DescShort()},
			{Text: commands.Start.Abr(), Description: commands.Start.DescShort()},
			{Text: commands.Status.Abr(), Description: commands.Status.DescShort() + "the server"},
			{Text: commands.Stop.Abr(), Description: commands.Stop.DescShort()},
		}
	case commands.SyncUserMaintained:
		s = []prompt.Suggest{
			{Text: commands.Help.Abr(), Description: commands.Help.DescShort()},
			{Text: commands.Home.Abr(), Description: commands.Home.DescShort()},
			{Text: commands.SyncUserMaintainedOff.Abr(), Description: commands.SyncUserMaintainedOff.DescShort()},
			{Text: commands.SyncUserMaintainedOn.Abr(), Description: commands.SyncUserMaintainedOn.DescShort()},
			{Text: commands.SyncUserMaintainedNow.Abr(), Description: commands.SyncUserMaintainedNow.DescShort()},
			{Text: commands.SyncUserMaintainedSim.Abr(), Description: commands.SyncUserMaintainedSim.DescShort()},
			{Text: commands.SyncStatus.Abr(), Description: commands.SyncStatus.DescShort() + "changes to your site"},
		}
	case commands.Version:
		s = []prompt.Suggest{
			{Text: commands.Exit.Abr(), Description: commands.Exit.DescShort()},
			{Text: commands.Help.Abr(), Description: commands.Help.DescShort()},
			{Text: commands.Home.Abr(), Description: commands.Home.DescShort()},
			{Text: commands.Start.Abr(), Description: commands.Start.DescShort()},
			{Text: commands.Status.Abr(), Description: commands.Status.DescShort() + "the server"},
			{Text: commands.Stop.Abr(), Description: commands.Stop.DescShort()},
			{Text: commands.Version.Abr(), Description: commands.Version.DescShort()},
		}
	default:
		s = []prompt.Suggest{
			{Text: commands.AGES.Abr(), Description: commands.AGES.DescShort()},
			{Text: commands.Build.Abr(), Description: commands.Build.DescShort()},
			{Text: commands.BuildIndex.Abr(), Description: commands.BuildIndex.DescShort()},
			{Text: commands.Database.Abr(), Description: commands.Database.DescShort()},
			{Text: commands.Exit.Abr(), Description: commands.Exit.DescShort()},
			{Text: commands.Help.Abr(), Description: commands.Help.DescShort()},
			{Text: commands.Internet.Abr(), Description: commands.Internet.DescShort()},
			{Text: commands.Ldp.Abr(), Description: commands.Ldp.DescShort()},
			{Text: commands.Login.Abr(), Description: commands.Login.DescShort()},
			{Text: commands.Logout.Abr(), Description: commands.Logout.DescShort()},
			{Text: commands.NetSim.Abr(), Description: commands.NetSim.DescShort()},
			{Text: commands.RO.Abr(), Description: commands.RO.DescShort()},
			{Text: commands.Change.Abr() + " password", Description: "reset password for gitlab token access"},
			{Text: commands.Change.Abr() + " token", Description: "reset gitlab token"},
			{Text: commands.Restore.Abr(), Description: commands.Restore.DescShort()},
			{Text: commands.Server.Abr(), Description: commands.Server.DescShort()},
			{Text: commands.Version.Abr(), Description: commands.Version.DescShort()},
		}
	}
	app.Ctx.Suggestions = s
	return prompt.FilterHasPrefix(s, in.GetWordBeforeCursor(), true)
}

func changeLivePrefix() (string, bool) {
	return LivePrefixState.LivePrefix, LivePrefixState.IsEnable
}

func setGlobals() {
	app.Ctx.PortApp = "8080" //config.SM.StringProps[properties.SysServerPortsDoxa]
	app.Ctx.PortSitePublic = config.SM.StringProps[properties.SysServerPortsPublicSite]
	app.Ctx.PortSiteTest = config.SM.StringProps[properties.SysServerPortsTestSite]
	app.Ctx.PortApi = config.SM.StringProps[properties.SysServerPortsApi]
	app.Ctx.PortMedia = config.SM.StringProps[properties.SysServerPortsMedia]
	app.Ctx.PortMessage = config.SM.StringProps[properties.SysServerPortsMessages]
	app.Ctx.Paths.MediaDirPath = config.SM.StringProps[properties.SiteMediaDir]
	// TODO: the media urls being passed to the servers means
	// they can't be dynamically changed once a server starts.
	// This is problematic.
	MediaEnabled = config.SM.BoolProps[properties.SiteMediaEnabled]
	// Media URL Audio
	MediaUrlAudio = config.SM.StringProps[properties.SiteMediaAudioUrl]
	if len(MediaUrlAudio) == 0 {
		MediaUrlAudio = "http://localhost"
	}
	if strings.Contains(MediaUrlAudio, "localhost") {
		MediaUrlAudio = MediaUrlAudio + ":" + app.Ctx.PortMedia
	}
	// media URL PDF
	MediaUrlPdf = config.SM.StringProps[properties.SiteMediaPdfUrl]
	if len(MediaUrlPdf) == 0 {
		MediaUrlPdf = "http://localhost"
	}
	if strings.Contains(MediaUrlPdf, "localhost") {
		MediaUrlPdf = MediaUrlPdf + ":" + app.Ctx.PortMedia
	}

	PdfViewer = config.SM.StringProps[properties.SiteMediaPdfViewer]

	// glory languages
	gloryLangs := config.SM.StringSliceProps[properties.SysGloryLanguages]
	gloryIndex := config.SM.IntProps[properties.SysGloryIndex]
	// convert to be zero based for use with the glory slice.
	if gloryIndex > 0 {
		gloryIndex--
	}
	if gloryIndex > len(gloryLangs) {
		fmt.Println("The glory index is greater than the number of languages.  The index starts with 0.")
	} else {
		if gloryLangs != nil {
			GLORY = gloryLangs[gloryIndex]
		}
	}
	RecEditor = config.SM.StringProps[properties.SysShellDbEditor]

	app.Ctx.GoOS = goos.CodeForString(runtime.GOOS)
	doxlog.Infof("runtime OS is %s", runtime.GOOS)
}
func GiveGlory() {
	fmt.Println(GLORY)
}

// userHomeDir returns the home directory of the current user
func userHomeDir() string {
	usr, err := user.Current()
	if err != nil {
		msg := fmt.Sprintf("Error getting current user: %v\n", err)
		doxlog.Panic(msg)
	}
	return usr.HomeDir
}

func ldp(args []string) {
	var year, month, day int
	var all, feasts, julian bool
	var model = "gr_gr_cog"
	var eval string
	var err error
	start := 1
	j := len(args)
	// check to see if we are inside the ldp prompt.
	// if so, we process flags from index = 0 instead of 1.
	if j >= 1 {
		if args[0] != commands.Ldp.Abr() {
			start = 0
		}
	}
	for i := start; i < j; i++ {
		switch args[i] {
		case commands.All.Abr():
			all = true
		case commands.Day.Abr():
			if len(args) > i+1 {
				i++
				day, err = strconv.Atoi(args[i])
				if err != nil {
					fmt.Printf("invalid day %d: %v\n", day, err)
					return
				}
				if day < 1 || day > 31 {
					fmt.Printf("invalid day %d: must be >= 1 and <= 31.\n", day)
					return
				}
			} else {
				fmt.Printf("if you use the day flag, you must provide a day, e.g. day 10\n")
				return
			}
		case commands.Eval.Abr():
			if len(args) > i+1 {
				i++
				var ex []string
				// read in the rest of the arguments to form an expression for evaluation
				for i < j {
					ex = append(ex, args[i])
					i++
				}
				eval = strings.Join(ex, " ")
			} else {
				fmt.Printf("if you use the eval flag, you must provide an expression, e.g. eval \"DayOfWeek == 1\"\n")
			}
		case commands.Feasts.Abr():
			feasts = true
		case commands.Julian.Abr():
			julian = true
		case commands.Model.Abr():
			model = args[i]
		case commands.Month.Abr():
			if len(args) > i+1 {
				i++
				month, err = strconv.Atoi(args[i])
				if err != nil {
					fmt.Printf("invalid month %d: %v\n", month, err)
					return
				}
				if month < 1 || month > 12 {
					fmt.Printf("invalid month %d: must be >= 1 and <= 12.\n", month)
					return
				}
			} else {
				fmt.Printf("if you use the month flag, you must provide a month, e.g. month 10\n")
				return
			}
		case commands.Year.Abr():
			if len(args) > i+1 {
				i++
				year, err = strconv.Atoi(args[i])
				if err != nil {
					fmt.Printf("invalid year %d: %v\n", year, err)
					return
				}
			} else {
				fmt.Printf("if you use the year flag, you must provide a year, e.g. year 2020\n")
				return
			}
		default:
			if feasts { // assume the current arg is a year
				year, err = strconv.Atoi(args[i])
				if err != nil {
					fmt.Printf("invalid year %d: %v\n", year, err)
					return
				}
				if year < 2000 {
					fmt.Printf("did you mean: feasts year %d?\n", year)
					return
				}
			}
		}
	}
	cli.ShowLdp(app.Ctx.Kvs,
		year,
		month,
		day,
		all,
		feasts,
		julian,
		model,
		eval,
		siteBuilder.TemplateMapper, app.Ctx.Keyring)
}

var DoxaReadMe = `
# Doxa

Doxa provides tools for researchers, translators, and publishers of the Eastern Orthodox Christian liturgical texts.
Doxa is written in the go programming language and is offered as a free service to the pan-Orthodox Christian community by the Orthodox Christian Mission Center (

## Updating Doxa to a New Version

When Doxa starts, it checks to see if there is a new version available.  If so, it will ask you whether you want to upgrade to the new version.  If you say yes, it will save your previous version of Doxa.  After it updates, you will need to restart Doxa.  If Doxa crashes, you can restore the pervious version by using this command in a terminal window:

cp .previous/doxa ./doxa

If the new version of Doxa starts fine, but you want to restore the previous version, in the Doxa command line interface you can run this command:

restore

## Directory Structure

*bin* - has the executable file for Doxa

*projects* - has folders for each website you are working on.

*subscriptions* - contains files from other people's projects that you are using in your own project(s).

## Directory Structure of a Project

*assets* - has files to be copied into your generated DCS website.

*backups* - has backup copies of your database.

*clipboard* - has text you have copied in Doxa

*db* - has your database file.

*exports* - has tab separated value files (.csv) of database records you have exported.

*imports* - has tab separated value files (.csv)  of records you wish to import into the database.

*logs* - has files that record events that have happened while you are running Doxa, such as errors that have occurred.

*templates* - has Liturgical Markup Language (.lml) files you have created that specify the content and format of a liturgical book or service.

*websites* - has DCS websites you have generated using Doxa.


`
