#!/bin/sh
if [ -z "$1" ]
then
  echo "missing version number, e.g., v0.2.31"
else
  echo releasing $1
  go mod tidy
  go mod vendor
  git add -A
  git commit -m "$1"
  git push origin
  git tag -a $1 -m "$1"
  git push origin $1
fi

