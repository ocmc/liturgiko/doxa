# dario.cat/mergo v1.0.0
## explicit; go 1.13
dario.cat/mergo
# github.com/01walid/goarabic v0.0.1
## explicit
github.com/01walid/goarabic
# github.com/Microsoft/go-winio v0.6.1
## explicit; go 1.17
github.com/Microsoft/go-winio
github.com/Microsoft/go-winio/internal/fs
github.com/Microsoft/go-winio/internal/socket
github.com/Microsoft/go-winio/internal/stringbuffer
github.com/Microsoft/go-winio/pkg/guid
# github.com/ProtonMail/go-crypto v1.0.0
## explicit; go 1.13
github.com/ProtonMail/go-crypto/bitcurves
github.com/ProtonMail/go-crypto/brainpool
github.com/ProtonMail/go-crypto/eax
github.com/ProtonMail/go-crypto/internal/byteutil
github.com/ProtonMail/go-crypto/ocb
github.com/ProtonMail/go-crypto/openpgp
github.com/ProtonMail/go-crypto/openpgp/aes/keywrap
github.com/ProtonMail/go-crypto/openpgp/armor
github.com/ProtonMail/go-crypto/openpgp/ecdh
github.com/ProtonMail/go-crypto/openpgp/ecdsa
github.com/ProtonMail/go-crypto/openpgp/eddsa
github.com/ProtonMail/go-crypto/openpgp/elgamal
github.com/ProtonMail/go-crypto/openpgp/errors
github.com/ProtonMail/go-crypto/openpgp/internal/algorithm
github.com/ProtonMail/go-crypto/openpgp/internal/ecc
github.com/ProtonMail/go-crypto/openpgp/internal/encoding
github.com/ProtonMail/go-crypto/openpgp/packet
github.com/ProtonMail/go-crypto/openpgp/s2k
# github.com/PuerkitoBio/goquery v1.8.1
## explicit; go 1.13
github.com/PuerkitoBio/goquery
# github.com/a-h/templ v0.2.778
## explicit; go 1.21
github.com/a-h/templ
github.com/a-h/templ/runtime
github.com/a-h/templ/safehtml
# github.com/adrg/strutil v0.3.1
## explicit; go 1.14
github.com/adrg/strutil
github.com/adrg/strutil/internal/mathutil
github.com/adrg/strutil/internal/ngram
github.com/adrg/strutil/internal/stringutil
github.com/adrg/strutil/metrics
# github.com/andybalholm/cascadia v1.3.1
## explicit; go 1.16
github.com/andybalholm/cascadia
# github.com/antlr/antlr4/runtime/Go/antlr/v4 v4.0.0-20230219212500-1f9a474cc2dc
## explicit; go 1.18
github.com/antlr/antlr4/runtime/Go/antlr/v4
# github.com/antlr4-go/antlr/v4 v4.13.0
## explicit; go 1.20
github.com/antlr4-go/antlr/v4
# github.com/asaskevich/EventBus v0.0.0-20200907212545-49d423059eef
## explicit
github.com/asaskevich/EventBus
# github.com/aws/aws-sdk-go v1.47.9
## explicit; go 1.19
github.com/aws/aws-sdk-go/aws
github.com/aws/aws-sdk-go/aws/arn
github.com/aws/aws-sdk-go/aws/auth/bearer
github.com/aws/aws-sdk-go/aws/awserr
github.com/aws/aws-sdk-go/aws/awsutil
github.com/aws/aws-sdk-go/aws/client
github.com/aws/aws-sdk-go/aws/client/metadata
github.com/aws/aws-sdk-go/aws/corehandlers
github.com/aws/aws-sdk-go/aws/credentials
github.com/aws/aws-sdk-go/aws/credentials/ec2rolecreds
github.com/aws/aws-sdk-go/aws/credentials/endpointcreds
github.com/aws/aws-sdk-go/aws/credentials/processcreds
github.com/aws/aws-sdk-go/aws/credentials/ssocreds
github.com/aws/aws-sdk-go/aws/credentials/stscreds
github.com/aws/aws-sdk-go/aws/csm
github.com/aws/aws-sdk-go/aws/defaults
github.com/aws/aws-sdk-go/aws/ec2metadata
github.com/aws/aws-sdk-go/aws/endpoints
github.com/aws/aws-sdk-go/aws/request
github.com/aws/aws-sdk-go/aws/session
github.com/aws/aws-sdk-go/aws/signer/v4
github.com/aws/aws-sdk-go/internal/context
github.com/aws/aws-sdk-go/internal/ini
github.com/aws/aws-sdk-go/internal/s3shared
github.com/aws/aws-sdk-go/internal/s3shared/arn
github.com/aws/aws-sdk-go/internal/s3shared/s3err
github.com/aws/aws-sdk-go/internal/sdkio
github.com/aws/aws-sdk-go/internal/sdkmath
github.com/aws/aws-sdk-go/internal/sdkrand
github.com/aws/aws-sdk-go/internal/sdkuri
github.com/aws/aws-sdk-go/internal/shareddefaults
github.com/aws/aws-sdk-go/internal/strings
github.com/aws/aws-sdk-go/internal/sync/singleflight
github.com/aws/aws-sdk-go/private/checksum
github.com/aws/aws-sdk-go/private/protocol
github.com/aws/aws-sdk-go/private/protocol/eventstream
github.com/aws/aws-sdk-go/private/protocol/eventstream/eventstreamapi
github.com/aws/aws-sdk-go/private/protocol/json/jsonutil
github.com/aws/aws-sdk-go/private/protocol/jsonrpc
github.com/aws/aws-sdk-go/private/protocol/query
github.com/aws/aws-sdk-go/private/protocol/query/queryutil
github.com/aws/aws-sdk-go/private/protocol/rest
github.com/aws/aws-sdk-go/private/protocol/restjson
github.com/aws/aws-sdk-go/private/protocol/restxml
github.com/aws/aws-sdk-go/private/protocol/xml/xmlutil
github.com/aws/aws-sdk-go/service/s3
github.com/aws/aws-sdk-go/service/sso
github.com/aws/aws-sdk-go/service/sso/ssoiface
github.com/aws/aws-sdk-go/service/ssooidc
github.com/aws/aws-sdk-go/service/sts
github.com/aws/aws-sdk-go/service/sts/stsiface
# github.com/biter777/countries v1.5.6
## explicit; go 1.13
github.com/biter777/countries
# github.com/c-bata/go-prompt v0.2.3
## explicit
github.com/c-bata/go-prompt
# github.com/cenkalti/backoff/v4 v4.3.0
## explicit; go 1.18
github.com/cenkalti/backoff/v4
# github.com/cespare/xxhash/v2 v2.3.0
## explicit; go 1.11
github.com/cespare/xxhash/v2
# github.com/cloudflare/circl v1.3.7
## explicit; go 1.19
github.com/cloudflare/circl/dh/x25519
github.com/cloudflare/circl/dh/x448
github.com/cloudflare/circl/ecc/goldilocks
github.com/cloudflare/circl/internal/conv
github.com/cloudflare/circl/internal/sha3
github.com/cloudflare/circl/math
github.com/cloudflare/circl/math/fp25519
github.com/cloudflare/circl/math/fp448
github.com/cloudflare/circl/math/mlsbset
github.com/cloudflare/circl/sign
github.com/cloudflare/circl/sign/ed25519
github.com/cloudflare/circl/sign/ed448
# github.com/cyphar/filepath-securejoin v0.2.4
## explicit; go 1.13
github.com/cyphar/filepath-securejoin
# github.com/deckarep/golang-set/v2 v2.3.0
## explicit; go 1.18
github.com/deckarep/golang-set/v2
# github.com/emirpasic/gods v1.18.1
## explicit; go 1.2
github.com/emirpasic/gods/containers
github.com/emirpasic/gods/lists
github.com/emirpasic/gods/lists/arraylist
github.com/emirpasic/gods/stacks
github.com/emirpasic/gods/stacks/arraystack
github.com/emirpasic/gods/trees
github.com/emirpasic/gods/trees/binaryheap
github.com/emirpasic/gods/trees/btree
github.com/emirpasic/gods/utils
# github.com/emvi/iso-639-1 v1.0.1
## explicit
github.com/emvi/iso-639-1
# github.com/fatih/color v1.16.0
## explicit; go 1.17
github.com/fatih/color
# github.com/fsnotify/fsnotify v1.7.0
## explicit; go 1.17
github.com/fsnotify/fsnotify
# github.com/go-git/gcfg v1.5.1-0.20230307220236-3a3c6141e376
## explicit; go 1.13
github.com/go-git/gcfg
github.com/go-git/gcfg/scanner
github.com/go-git/gcfg/token
github.com/go-git/gcfg/types
# github.com/go-git/go-billy/v5 v5.5.0
## explicit; go 1.19
github.com/go-git/go-billy/v5
github.com/go-git/go-billy/v5/helper/chroot
github.com/go-git/go-billy/v5/helper/polyfill
github.com/go-git/go-billy/v5/memfs
github.com/go-git/go-billy/v5/osfs
github.com/go-git/go-billy/v5/util
# github.com/go-git/go-git/v5 v5.12.0
## explicit; go 1.19
github.com/go-git/go-git/v5
github.com/go-git/go-git/v5/config
github.com/go-git/go-git/v5/internal/path_util
github.com/go-git/go-git/v5/internal/revision
github.com/go-git/go-git/v5/internal/url
github.com/go-git/go-git/v5/plumbing
github.com/go-git/go-git/v5/plumbing/cache
github.com/go-git/go-git/v5/plumbing/color
github.com/go-git/go-git/v5/plumbing/filemode
github.com/go-git/go-git/v5/plumbing/format/config
github.com/go-git/go-git/v5/plumbing/format/diff
github.com/go-git/go-git/v5/plumbing/format/gitignore
github.com/go-git/go-git/v5/plumbing/format/idxfile
github.com/go-git/go-git/v5/plumbing/format/index
github.com/go-git/go-git/v5/plumbing/format/objfile
github.com/go-git/go-git/v5/plumbing/format/packfile
github.com/go-git/go-git/v5/plumbing/format/pktline
github.com/go-git/go-git/v5/plumbing/hash
github.com/go-git/go-git/v5/plumbing/object
github.com/go-git/go-git/v5/plumbing/protocol/packp
github.com/go-git/go-git/v5/plumbing/protocol/packp/capability
github.com/go-git/go-git/v5/plumbing/protocol/packp/sideband
github.com/go-git/go-git/v5/plumbing/revlist
github.com/go-git/go-git/v5/plumbing/storer
github.com/go-git/go-git/v5/plumbing/transport
github.com/go-git/go-git/v5/plumbing/transport/client
github.com/go-git/go-git/v5/plumbing/transport/file
github.com/go-git/go-git/v5/plumbing/transport/git
github.com/go-git/go-git/v5/plumbing/transport/http
github.com/go-git/go-git/v5/plumbing/transport/internal/common
github.com/go-git/go-git/v5/plumbing/transport/server
github.com/go-git/go-git/v5/plumbing/transport/ssh
github.com/go-git/go-git/v5/storage
github.com/go-git/go-git/v5/storage/filesystem
github.com/go-git/go-git/v5/storage/filesystem/dotgit
github.com/go-git/go-git/v5/storage/memory
github.com/go-git/go-git/v5/utils/binary
github.com/go-git/go-git/v5/utils/diff
github.com/go-git/go-git/v5/utils/ioutil
github.com/go-git/go-git/v5/utils/merkletrie
github.com/go-git/go-git/v5/utils/merkletrie/filesystem
github.com/go-git/go-git/v5/utils/merkletrie/index
github.com/go-git/go-git/v5/utils/merkletrie/internal/frame
github.com/go-git/go-git/v5/utils/merkletrie/noder
github.com/go-git/go-git/v5/utils/sync
github.com/go-git/go-git/v5/utils/trace
# github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da
## explicit
github.com/golang/groupcache/lru
# github.com/golang/protobuf v1.5.3
## explicit; go 1.9
github.com/golang/protobuf/proto
# github.com/google/go-github v17.0.0+incompatible
## explicit
github.com/google/go-github/github
# github.com/google/go-querystring v1.1.0
## explicit; go 1.10
github.com/google/go-querystring/query
# github.com/google/martian v2.1.1-0.20190517191504-25dcb96d9e51+incompatible
## explicit
github.com/google/martian/log
# github.com/google/uuid v1.1.2
## explicit
github.com/google/uuid
# github.com/gorilla/mux v1.8.1
## explicit; go 1.20
github.com/gorilla/mux
# github.com/gorilla/securecookie v1.1.2
## explicit; go 1.20
github.com/gorilla/securecookie
# github.com/gorilla/sessions v1.2.2
## explicit; go 1.20
github.com/gorilla/sessions
# github.com/gorilla/websocket v1.5.0
## explicit; go 1.12
github.com/gorilla/websocket
# github.com/hashicorp/go-cleanhttp v0.5.2
## explicit; go 1.13
github.com/hashicorp/go-cleanhttp
# github.com/hashicorp/go-retryablehttp v0.7.1
## explicit; go 1.13
github.com/hashicorp/go-retryablehttp
# github.com/hashicorp/hcl v1.0.0
## explicit
github.com/hashicorp/hcl
github.com/hashicorp/hcl/hcl/ast
github.com/hashicorp/hcl/hcl/parser
github.com/hashicorp/hcl/hcl/printer
github.com/hashicorp/hcl/hcl/scanner
github.com/hashicorp/hcl/hcl/strconv
github.com/hashicorp/hcl/hcl/token
github.com/hashicorp/hcl/json/parser
github.com/hashicorp/hcl/json/scanner
github.com/hashicorp/hcl/json/token
# github.com/hbollon/go-edlib v1.6.0
## explicit; go 1.13
github.com/hbollon/go-edlib
github.com/hbollon/go-edlib/internal/orderedmap
github.com/hbollon/go-edlib/internal/utils
# github.com/jbenet/go-context v0.0.0-20150711004518-d14ea06fba99
## explicit
github.com/jbenet/go-context/io
# github.com/jmespath/go-jmespath v0.4.0
## explicit; go 1.14
github.com/jmespath/go-jmespath
# github.com/kevinburke/ssh_config v1.2.0
## explicit
github.com/kevinburke/ssh_config
# github.com/klauspost/lctime v0.1.0
## explicit
github.com/klauspost/lctime
github.com/klauspost/lctime/internal/locales
# github.com/magiconair/properties v1.8.5
## explicit; go 1.13
github.com/magiconair/properties
# github.com/mattn/go-colorable v0.1.13
## explicit; go 1.15
github.com/mattn/go-colorable
# github.com/mattn/go-isatty v0.0.20
## explicit; go 1.15
github.com/mattn/go-isatty
# github.com/mattn/go-runewidth v0.0.15
## explicit; go 1.9
github.com/mattn/go-runewidth
# github.com/mattn/go-tty v0.0.3
## explicit; go 1.14
github.com/mattn/go-tty
# github.com/mitchellh/go-homedir v1.1.0
## explicit
github.com/mitchellh/go-homedir
# github.com/mitchellh/mapstructure v1.4.1
## explicit; go 1.14
github.com/mitchellh/mapstructure
# github.com/nuvi/go-conllu v1.1.0
## explicit; go 1.14
github.com/nuvi/go-conllu
# github.com/pelletier/go-toml v1.9.3
## explicit; go 1.12
github.com/pelletier/go-toml
# github.com/pjbgf/sha1cd v0.3.0
## explicit; go 1.19
github.com/pjbgf/sha1cd
github.com/pjbgf/sha1cd/internal
github.com/pjbgf/sha1cd/ubc
# github.com/pkg/term v0.0.0-20190109203006-aa71e9d9e942
## explicit
github.com/pkg/term/termios
# github.com/rivo/uniseg v0.4.7
## explicit; go 1.18
github.com/rivo/uniseg
# github.com/sergi/go-diff v1.3.2-0.20230802210424-5b0b94c5c0d3
## explicit; go 1.13
github.com/sergi/go-diff/diffmatchpatch
# github.com/skeema/knownhosts v1.2.2
## explicit; go 1.17
github.com/skeema/knownhosts
# github.com/smartystreets/assertions v1.0.0
## explicit; go 1.12
# github.com/speedata/hyphenation v1.0.1
## explicit; go 1.16
github.com/speedata/hyphenation
# github.com/spf13/afero v1.6.0
## explicit; go 1.13
github.com/spf13/afero
github.com/spf13/afero/mem
# github.com/spf13/cast v1.3.1
## explicit
github.com/spf13/cast
# github.com/spf13/jwalterweatherman v1.1.0
## explicit
github.com/spf13/jwalterweatherman
# github.com/spf13/pflag v1.0.5
## explicit; go 1.12
github.com/spf13/pflag
# github.com/spf13/viper v1.8.1
## explicit; go 1.12
github.com/spf13/viper
# github.com/src-d/gcfg v1.4.0
## explicit
github.com/src-d/gcfg
github.com/src-d/gcfg/scanner
github.com/src-d/gcfg/token
github.com/src-d/gcfg/types
# github.com/subosito/gotenv v1.2.0
## explicit
github.com/subosito/gotenv
# github.com/tdewolff/parse/v2 v2.4.3
## explicit; go 1.13
github.com/tdewolff/parse/v2
github.com/tdewolff/parse/v2/buffer
github.com/tdewolff/parse/v2/css
# github.com/tmaxmax/go-sse v0.4.2
## explicit; go 1.16
github.com/tmaxmax/go-sse
github.com/tmaxmax/go-sse/internal/parser
# github.com/walle/targz v0.0.0-20140417120357-57fe4206da5a
## explicit
github.com/walle/targz
# github.com/xanzy/go-gitlab v0.73.1
## explicit; go 1.18
github.com/xanzy/go-gitlab
# github.com/xanzy/ssh-agent v0.3.3
## explicit; go 1.16
github.com/xanzy/ssh-agent
# gitlab.com/ocmc/liturgiko/lml/alwb/golang v1.0.7
## explicit; go 1.17
gitlab.com/ocmc/liturgiko/lml/alwb/golang/parser
# gitlab.com/ocmc/liturgiko/lml/golang v1.0.70
## explicit; go 1.19
gitlab.com/ocmc/liturgiko/lml/golang/parser
# go.etcd.io/bbolt v1.3.7
## explicit; go 1.17
go.etcd.io/bbolt
# golang.org/x/crypto v0.26.0
## explicit; go 1.20
golang.org/x/crypto/argon2
golang.org/x/crypto/blake2b
golang.org/x/crypto/blowfish
golang.org/x/crypto/cast5
golang.org/x/crypto/chacha20
golang.org/x/crypto/curve25519
golang.org/x/crypto/hkdf
golang.org/x/crypto/internal/alias
golang.org/x/crypto/internal/poly1305
golang.org/x/crypto/openpgp
golang.org/x/crypto/openpgp/armor
golang.org/x/crypto/openpgp/elgamal
golang.org/x/crypto/openpgp/errors
golang.org/x/crypto/openpgp/packet
golang.org/x/crypto/openpgp/s2k
golang.org/x/crypto/sha3
golang.org/x/crypto/ssh
golang.org/x/crypto/ssh/agent
golang.org/x/crypto/ssh/internal/bcrypt_pbkdf
golang.org/x/crypto/ssh/knownhosts
golang.org/x/crypto/ssh/terminal
# golang.org/x/exp v0.0.0-20240222234643-814bf88cf225
## explicit; go 1.20
golang.org/x/exp/constraints
golang.org/x/exp/slices
# golang.org/x/mod v0.20.0
## explicit; go 1.18
golang.org/x/mod/semver
# golang.org/x/net v0.28.0
## explicit; go 1.18
golang.org/x/net/context
golang.org/x/net/html
golang.org/x/net/html/atom
golang.org/x/net/internal/socks
golang.org/x/net/proxy
# golang.org/x/oauth2 v0.14.0
## explicit; go 1.18
golang.org/x/oauth2
golang.org/x/oauth2/internal
# golang.org/x/sync v0.8.0
## explicit; go 1.18
golang.org/x/sync/errgroup
# golang.org/x/sys v0.23.0
## explicit; go 1.18
golang.org/x/sys/cpu
golang.org/x/sys/execabs
golang.org/x/sys/plan9
golang.org/x/sys/unix
golang.org/x/sys/windows
# golang.org/x/term v0.23.0
## explicit; go 1.18
golang.org/x/term
# golang.org/x/text v0.17.0
## explicit; go 1.18
golang.org/x/text/feature/plural
golang.org/x/text/internal
golang.org/x/text/internal/catmsg
golang.org/x/text/internal/format
golang.org/x/text/internal/language
golang.org/x/text/internal/language/compact
golang.org/x/text/internal/number
golang.org/x/text/internal/stringset
golang.org/x/text/internal/tag
golang.org/x/text/language
golang.org/x/text/message
golang.org/x/text/message/catalog
golang.org/x/text/runes
golang.org/x/text/transform
golang.org/x/text/unicode/norm
# golang.org/x/time v0.0.0-20220722155302-e5dcc9cfc0b9
## explicit
golang.org/x/time/rate
# golang.org/x/tools v0.24.0
## explicit; go 1.19
golang.org/x/tools/cmd/stringer
golang.org/x/tools/go/gcexportdata
golang.org/x/tools/go/packages
golang.org/x/tools/go/types/objectpath
golang.org/x/tools/internal/aliases
golang.org/x/tools/internal/event
golang.org/x/tools/internal/event/core
golang.org/x/tools/internal/event/keys
golang.org/x/tools/internal/event/label
golang.org/x/tools/internal/gcimporter
golang.org/x/tools/internal/gocommand
golang.org/x/tools/internal/packagesinternal
golang.org/x/tools/internal/pkgbits
golang.org/x/tools/internal/stdlib
golang.org/x/tools/internal/tokeninternal
golang.org/x/tools/internal/typesinternal
golang.org/x/tools/internal/versions
# google.golang.org/appengine v1.6.7
## explicit; go 1.11
google.golang.org/appengine/internal
google.golang.org/appengine/internal/base
google.golang.org/appengine/internal/datastore
google.golang.org/appengine/internal/log
google.golang.org/appengine/internal/remote_api
google.golang.org/appengine/internal/urlfetch
google.golang.org/appengine/urlfetch
# google.golang.org/protobuf v1.31.0
## explicit; go 1.11
google.golang.org/protobuf/encoding/prototext
google.golang.org/protobuf/encoding/protowire
google.golang.org/protobuf/internal/descfmt
google.golang.org/protobuf/internal/descopts
google.golang.org/protobuf/internal/detrand
google.golang.org/protobuf/internal/encoding/defval
google.golang.org/protobuf/internal/encoding/messageset
google.golang.org/protobuf/internal/encoding/tag
google.golang.org/protobuf/internal/encoding/text
google.golang.org/protobuf/internal/errors
google.golang.org/protobuf/internal/filedesc
google.golang.org/protobuf/internal/filetype
google.golang.org/protobuf/internal/flags
google.golang.org/protobuf/internal/genid
google.golang.org/protobuf/internal/impl
google.golang.org/protobuf/internal/order
google.golang.org/protobuf/internal/pragma
google.golang.org/protobuf/internal/set
google.golang.org/protobuf/internal/strs
google.golang.org/protobuf/internal/version
google.golang.org/protobuf/proto
google.golang.org/protobuf/reflect/protodesc
google.golang.org/protobuf/reflect/protoreflect
google.golang.org/protobuf/reflect/protoregistry
google.golang.org/protobuf/runtime/protoiface
google.golang.org/protobuf/runtime/protoimpl
google.golang.org/protobuf/types/descriptorpb
# gopkg.in/ini.v1 v1.62.0
## explicit
gopkg.in/ini.v1
# gopkg.in/src-d/go-billy.v4 v4.3.2
## explicit
gopkg.in/src-d/go-billy.v4
gopkg.in/src-d/go-billy.v4/helper/chroot
gopkg.in/src-d/go-billy.v4/helper/polyfill
gopkg.in/src-d/go-billy.v4/osfs
gopkg.in/src-d/go-billy.v4/util
# gopkg.in/src-d/go-git.v4 v4.13.1
## explicit
gopkg.in/src-d/go-git.v4
gopkg.in/src-d/go-git.v4/config
gopkg.in/src-d/go-git.v4/internal/revision
gopkg.in/src-d/go-git.v4/internal/url
gopkg.in/src-d/go-git.v4/plumbing
gopkg.in/src-d/go-git.v4/plumbing/cache
gopkg.in/src-d/go-git.v4/plumbing/filemode
gopkg.in/src-d/go-git.v4/plumbing/format/config
gopkg.in/src-d/go-git.v4/plumbing/format/diff
gopkg.in/src-d/go-git.v4/plumbing/format/gitignore
gopkg.in/src-d/go-git.v4/plumbing/format/idxfile
gopkg.in/src-d/go-git.v4/plumbing/format/index
gopkg.in/src-d/go-git.v4/plumbing/format/objfile
gopkg.in/src-d/go-git.v4/plumbing/format/packfile
gopkg.in/src-d/go-git.v4/plumbing/format/pktline
gopkg.in/src-d/go-git.v4/plumbing/object
gopkg.in/src-d/go-git.v4/plumbing/protocol/packp
gopkg.in/src-d/go-git.v4/plumbing/protocol/packp/capability
gopkg.in/src-d/go-git.v4/plumbing/protocol/packp/sideband
gopkg.in/src-d/go-git.v4/plumbing/revlist
gopkg.in/src-d/go-git.v4/plumbing/storer
gopkg.in/src-d/go-git.v4/plumbing/transport
gopkg.in/src-d/go-git.v4/plumbing/transport/client
gopkg.in/src-d/go-git.v4/plumbing/transport/file
gopkg.in/src-d/go-git.v4/plumbing/transport/git
gopkg.in/src-d/go-git.v4/plumbing/transport/http
gopkg.in/src-d/go-git.v4/plumbing/transport/internal/common
gopkg.in/src-d/go-git.v4/plumbing/transport/server
gopkg.in/src-d/go-git.v4/plumbing/transport/ssh
gopkg.in/src-d/go-git.v4/storage
gopkg.in/src-d/go-git.v4/storage/filesystem
gopkg.in/src-d/go-git.v4/storage/filesystem/dotgit
gopkg.in/src-d/go-git.v4/storage/memory
gopkg.in/src-d/go-git.v4/utils/binary
gopkg.in/src-d/go-git.v4/utils/diff
gopkg.in/src-d/go-git.v4/utils/ioutil
gopkg.in/src-d/go-git.v4/utils/merkletrie
gopkg.in/src-d/go-git.v4/utils/merkletrie/filesystem
gopkg.in/src-d/go-git.v4/utils/merkletrie/index
gopkg.in/src-d/go-git.v4/utils/merkletrie/internal/frame
gopkg.in/src-d/go-git.v4/utils/merkletrie/noder
# gopkg.in/warnings.v0 v0.1.2
## explicit
gopkg.in/warnings.v0
# gopkg.in/yaml.v2 v2.4.0
## explicit; go 1.15
gopkg.in/yaml.v2
