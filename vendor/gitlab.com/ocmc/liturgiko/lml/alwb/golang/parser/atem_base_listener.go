// Code generated from java-escape by ANTLR 4.11.1. DO NOT EDIT.

package parser // Atem

import "github.com/antlr/antlr4/runtime/Go/antlr/v4"

// BaseAtemListener is a complete listener for a parse tree produced by AtemParser.
type BaseAtemListener struct{}

var _ AtemListener = &BaseAtemListener{}

// VisitTerminal is called when a terminal node is visited.
func (s *BaseAtemListener) VisitTerminal(node antlr.TerminalNode) {}

// VisitErrorNode is called when an error node is visited.
func (s *BaseAtemListener) VisitErrorNode(node antlr.ErrorNode) {}

// EnterEveryRule is called when any rule is entered.
func (s *BaseAtemListener) EnterEveryRule(ctx antlr.ParserRuleContext) {}

// ExitEveryRule is called when any rule is exited.
func (s *BaseAtemListener) ExitEveryRule(ctx antlr.ParserRuleContext) {}

// EnterAtemModel is called when production atemModel is entered.
func (s *BaseAtemListener) EnterAtemModel(ctx *AtemModelContext) {}

// ExitAtemModel is called when production atemModel is exited.
func (s *BaseAtemListener) ExitAtemModel(ctx *AtemModelContext) {}

// EnterHead is called when production head is entered.
func (s *BaseAtemListener) EnterHead(ctx *HeadContext) {}

// ExitHead is called when production head is exited.
func (s *BaseAtemListener) ExitHead(ctx *HeadContext) {}

// EnterHeadComponent is called when production headComponent is entered.
func (s *BaseAtemListener) EnterHeadComponent(ctx *HeadComponentContext) {}

// ExitHeadComponent is called when production headComponent is exited.
func (s *BaseAtemListener) ExitHeadComponent(ctx *HeadComponentContext) {}

// EnterCommemoration is called when production commemoration is entered.
func (s *BaseAtemListener) EnterCommemoration(ctx *CommemorationContext) {}

// ExitCommemoration is called when production commemoration is exited.
func (s *BaseAtemListener) ExitCommemoration(ctx *CommemorationContext) {}

// EnterPreface is called when production preface is entered.
func (s *BaseAtemListener) EnterPreface(ctx *PrefaceContext) {}

// ExitPreface is called when production preface is exited.
func (s *BaseAtemListener) ExitPreface(ctx *PrefaceContext) {}

// EnterPrefaceElementType is called when production prefaceElementType is entered.
func (s *BaseAtemListener) EnterPrefaceElementType(ctx *PrefaceElementTypeContext) {}

// ExitPrefaceElementType is called when production prefaceElementType is exited.
func (s *BaseAtemListener) ExitPrefaceElementType(ctx *PrefaceElementTypeContext) {}

// EnterHeaderFooterFragment is called when production headerFooterFragment is entered.
func (s *BaseAtemListener) EnterHeaderFooterFragment(ctx *HeaderFooterFragmentContext) {}

// ExitHeaderFooterFragment is called when production headerFooterFragment is exited.
func (s *BaseAtemListener) ExitHeaderFooterFragment(ctx *HeaderFooterFragmentContext) {}

// EnterHeaderFooterText is called when production headerFooterText is entered.
func (s *BaseAtemListener) EnterHeaderFooterText(ctx *HeaderFooterTextContext) {}

// ExitHeaderFooterText is called when production headerFooterText is exited.
func (s *BaseAtemListener) ExitHeaderFooterText(ctx *HeaderFooterTextContext) {}

// EnterHeaderFooterDate is called when production headerFooterDate is entered.
func (s *BaseAtemListener) EnterHeaderFooterDate(ctx *HeaderFooterDateContext) {}

// ExitHeaderFooterDate is called when production headerFooterDate is exited.
func (s *BaseAtemListener) ExitHeaderFooterDate(ctx *HeaderFooterDateContext) {}

// EnterHeaderFooterPageNumber is called when production headerFooterPageNumber is entered.
func (s *BaseAtemListener) EnterHeaderFooterPageNumber(ctx *HeaderFooterPageNumberContext) {}

// ExitHeaderFooterPageNumber is called when production headerFooterPageNumber is exited.
func (s *BaseAtemListener) ExitHeaderFooterPageNumber(ctx *HeaderFooterPageNumberContext) {}

// EnterHeaderFooterLookup is called when production headerFooterLookup is entered.
func (s *BaseAtemListener) EnterHeaderFooterLookup(ctx *HeaderFooterLookupContext) {}

// ExitHeaderFooterLookup is called when production headerFooterLookup is exited.
func (s *BaseAtemListener) ExitHeaderFooterLookup(ctx *HeaderFooterLookupContext) {}

// EnterHeaderFooterTitle is called when production headerFooterTitle is entered.
func (s *BaseAtemListener) EnterHeaderFooterTitle(ctx *HeaderFooterTitleContext) {}

// ExitHeaderFooterTitle is called when production headerFooterTitle is exited.
func (s *BaseAtemListener) ExitHeaderFooterTitle(ctx *HeaderFooterTitleContext) {}

// EnterHeaderFooterCommemoration is called when production headerFooterCommemoration is entered.
func (s *BaseAtemListener) EnterHeaderFooterCommemoration(ctx *HeaderFooterCommemorationContext) {}

// ExitHeaderFooterCommemoration is called when production headerFooterCommemoration is exited.
func (s *BaseAtemListener) ExitHeaderFooterCommemoration(ctx *HeaderFooterCommemorationContext) {}

// EnterSetDate is called when production setDate is entered.
func (s *BaseAtemListener) EnterSetDate(ctx *SetDateContext) {}

// ExitSetDate is called when production setDate is exited.
func (s *BaseAtemListener) ExitSetDate(ctx *SetDateContext) {}

// EnterMcDay is called when production mcDay is entered.
func (s *BaseAtemListener) EnterMcDay(ctx *McDayContext) {}

// ExitMcDay is called when production mcDay is exited.
func (s *BaseAtemListener) ExitMcDay(ctx *McDayContext) {}

// EnterTemplateTitle is called when production templateTitle is entered.
func (s *BaseAtemListener) EnterTemplateTitle(ctx *TemplateTitleContext) {}

// ExitTemplateTitle is called when production templateTitle is exited.
func (s *BaseAtemListener) ExitTemplateTitle(ctx *TemplateTitleContext) {}

// EnterPageKeepWithNext is called when production pageKeepWithNext is entered.
func (s *BaseAtemListener) EnterPageKeepWithNext(ctx *PageKeepWithNextContext) {}

// ExitPageKeepWithNext is called when production pageKeepWithNext is exited.
func (s *BaseAtemListener) ExitPageKeepWithNext(ctx *PageKeepWithNextContext) {}

// EnterPageHeaderEven is called when production pageHeaderEven is entered.
func (s *BaseAtemListener) EnterPageHeaderEven(ctx *PageHeaderEvenContext) {}

// ExitPageHeaderEven is called when production pageHeaderEven is exited.
func (s *BaseAtemListener) ExitPageHeaderEven(ctx *PageHeaderEvenContext) {}

// EnterPageHeaderOdd is called when production pageHeaderOdd is entered.
func (s *BaseAtemListener) EnterPageHeaderOdd(ctx *PageHeaderOddContext) {}

// ExitPageHeaderOdd is called when production pageHeaderOdd is exited.
func (s *BaseAtemListener) ExitPageHeaderOdd(ctx *PageHeaderOddContext) {}

// EnterPageFooterEven is called when production pageFooterEven is entered.
func (s *BaseAtemListener) EnterPageFooterEven(ctx *PageFooterEvenContext) {}

// ExitPageFooterEven is called when production pageFooterEven is exited.
func (s *BaseAtemListener) ExitPageFooterEven(ctx *PageFooterEvenContext) {}

// EnterPageFooterOdd is called when production pageFooterOdd is entered.
func (s *BaseAtemListener) EnterPageFooterOdd(ctx *PageFooterOddContext) {}

// ExitPageFooterOdd is called when production pageFooterOdd is exited.
func (s *BaseAtemListener) ExitPageFooterOdd(ctx *PageFooterOddContext) {}

// EnterHeaderFooterColumn is called when production headerFooterColumn is entered.
func (s *BaseAtemListener) EnterHeaderFooterColumn(ctx *HeaderFooterColumnContext) {}

// ExitHeaderFooterColumn is called when production headerFooterColumn is exited.
func (s *BaseAtemListener) ExitHeaderFooterColumn(ctx *HeaderFooterColumnContext) {}

// EnterHeaderFooterColumnLeft is called when production headerFooterColumnLeft is entered.
func (s *BaseAtemListener) EnterHeaderFooterColumnLeft(ctx *HeaderFooterColumnLeftContext) {}

// ExitHeaderFooterColumnLeft is called when production headerFooterColumnLeft is exited.
func (s *BaseAtemListener) ExitHeaderFooterColumnLeft(ctx *HeaderFooterColumnLeftContext) {}

// EnterHeaderFooterColumnCenter is called when production headerFooterColumnCenter is entered.
func (s *BaseAtemListener) EnterHeaderFooterColumnCenter(ctx *HeaderFooterColumnCenterContext) {}

// ExitHeaderFooterColumnCenter is called when production headerFooterColumnCenter is exited.
func (s *BaseAtemListener) ExitHeaderFooterColumnCenter(ctx *HeaderFooterColumnCenterContext) {}

// EnterHeaderFooterColumnRight is called when production headerFooterColumnRight is entered.
func (s *BaseAtemListener) EnterHeaderFooterColumnRight(ctx *HeaderFooterColumnRightContext) {}

// ExitHeaderFooterColumnRight is called when production headerFooterColumnRight is exited.
func (s *BaseAtemListener) ExitHeaderFooterColumnRight(ctx *HeaderFooterColumnRightContext) {}

// EnterPageNumber is called when production pageNumber is entered.
func (s *BaseAtemListener) EnterPageNumber(ctx *PageNumberContext) {}

// ExitPageNumber is called when production pageNumber is exited.
func (s *BaseAtemListener) ExitPageNumber(ctx *PageNumberContext) {}

// EnterAbstractComponent is called when production abstractComponent is entered.
func (s *BaseAtemListener) EnterAbstractComponent(ctx *AbstractComponentContext) {}

// ExitAbstractComponent is called when production abstractComponent is exited.
func (s *BaseAtemListener) ExitAbstractComponent(ctx *AbstractComponentContext) {}

// EnterActor is called when production actor is entered.
func (s *BaseAtemListener) EnterActor(ctx *ActorContext) {}

// ExitActor is called when production actor is exited.
func (s *BaseAtemListener) ExitActor(ctx *ActorContext) {}

// EnterBlock is called when production block is entered.
func (s *BaseAtemListener) EnterBlock(ctx *BlockContext) {}

// ExitBlock is called when production block is exited.
func (s *BaseAtemListener) ExitBlock(ctx *BlockContext) {}

// EnterDialog is called when production dialog is entered.
func (s *BaseAtemListener) EnterDialog(ctx *DialogContext) {}

// ExitDialog is called when production dialog is exited.
func (s *BaseAtemListener) ExitDialog(ctx *DialogContext) {}

// EnterHymn is called when production hymn is entered.
func (s *BaseAtemListener) EnterHymn(ctx *HymnContext) {}

// ExitHymn is called when production hymn is exited.
func (s *BaseAtemListener) ExitHymn(ctx *HymnContext) {}

// EnterMedia is called when production media is entered.
func (s *BaseAtemListener) EnterMedia(ctx *MediaContext) {}

// ExitMedia is called when production media is exited.
func (s *BaseAtemListener) ExitMedia(ctx *MediaContext) {}

// EnterParagraph is called when production paragraph is entered.
func (s *BaseAtemListener) EnterParagraph(ctx *ParagraphContext) {}

// ExitParagraph is called when production paragraph is exited.
func (s *BaseAtemListener) ExitParagraph(ctx *ParagraphContext) {}

// EnterPassThroughHtml is called when production passThroughHtml is entered.
func (s *BaseAtemListener) EnterPassThroughHtml(ctx *PassThroughHtmlContext) {}

// ExitPassThroughHtml is called when production passThroughHtml is exited.
func (s *BaseAtemListener) ExitPassThroughHtml(ctx *PassThroughHtmlContext) {}

// EnterReading is called when production reading is entered.
func (s *BaseAtemListener) EnterReading(ctx *ReadingContext) {}

// ExitReading is called when production reading is exited.
func (s *BaseAtemListener) ExitReading(ctx *ReadingContext) {}

// EnterRestoreLocale is called when production restoreLocale is entered.
func (s *BaseAtemListener) EnterRestoreLocale(ctx *RestoreLocaleContext) {}

// ExitRestoreLocale is called when production restoreLocale is exited.
func (s *BaseAtemListener) ExitRestoreLocale(ctx *RestoreLocaleContext) {}

// EnterRubric is called when production rubric is entered.
func (s *BaseAtemListener) EnterRubric(ctx *RubricContext) {}

// ExitRubric is called when production rubric is exited.
func (s *BaseAtemListener) ExitRubric(ctx *RubricContext) {}

// EnterSection is called when production section is entered.
func (s *BaseAtemListener) EnterSection(ctx *SectionContext) {}

// ExitSection is called when production section is exited.
func (s *BaseAtemListener) ExitSection(ctx *SectionContext) {}

// EnterSetLocale is called when production setLocale is entered.
func (s *BaseAtemListener) EnterSetLocale(ctx *SetLocaleContext) {}

// ExitSetLocale is called when production setLocale is exited.
func (s *BaseAtemListener) ExitSetLocale(ctx *SetLocaleContext) {}

// EnterSubTitle is called when production subTitle is entered.
func (s *BaseAtemListener) EnterSubTitle(ctx *SubTitleContext) {}

// ExitSubTitle is called when production subTitle is exited.
func (s *BaseAtemListener) ExitSubTitle(ctx *SubTitleContext) {}

// EnterTitle is called when production title is entered.
func (s *BaseAtemListener) EnterTitle(ctx *TitleContext) {}

// ExitTitle is called when production title is exited.
func (s *BaseAtemListener) ExitTitle(ctx *TitleContext) {}

// EnterVerse is called when production verse is entered.
func (s *BaseAtemListener) EnterVerse(ctx *VerseContext) {}

// ExitVerse is called when production verse is exited.
func (s *BaseAtemListener) ExitVerse(ctx *VerseContext) {}

// EnterVersion is called when production version is entered.
func (s *BaseAtemListener) EnterVersion(ctx *VersionContext) {}

// ExitVersion is called when production version is exited.
func (s *BaseAtemListener) ExitVersion(ctx *VersionContext) {}

// EnterVersionSwitch is called when production versionSwitch is entered.
func (s *BaseAtemListener) EnterVersionSwitch(ctx *VersionSwitchContext) {}

// ExitVersionSwitch is called when production versionSwitch is exited.
func (s *BaseAtemListener) ExitVersionSwitch(ctx *VersionSwitchContext) {}

// EnterVersionSwitchType is called when production versionSwitchType is entered.
func (s *BaseAtemListener) EnterVersionSwitchType(ctx *VersionSwitchTypeContext) {}

// ExitVersionSwitchType is called when production versionSwitchType is exited.
func (s *BaseAtemListener) ExitVersionSwitchType(ctx *VersionSwitchTypeContext) {}

// EnterWhenDate is called when production whenDate is entered.
func (s *BaseAtemListener) EnterWhenDate(ctx *WhenDateContext) {}

// ExitWhenDate is called when production whenDate is exited.
func (s *BaseAtemListener) ExitWhenDate(ctx *WhenDateContext) {}

// EnterWhenDateCase is called when production whenDateCase is entered.
func (s *BaseAtemListener) EnterWhenDateCase(ctx *WhenDateCaseContext) {}

// ExitWhenDateCase is called when production whenDateCase is exited.
func (s *BaseAtemListener) ExitWhenDateCase(ctx *WhenDateCaseContext) {}

// EnterWhenOther is called when production whenOther is entered.
func (s *BaseAtemListener) EnterWhenOther(ctx *WhenOtherContext) {}

// ExitWhenOther is called when production whenOther is exited.
func (s *BaseAtemListener) ExitWhenOther(ctx *WhenOtherContext) {}

// EnterWhenPeriodCase is called when production whenPeriodCase is entered.
func (s *BaseAtemListener) EnterWhenPeriodCase(ctx *WhenPeriodCaseContext) {}

// ExitWhenPeriodCase is called when production whenPeriodCase is exited.
func (s *BaseAtemListener) ExitWhenPeriodCase(ctx *WhenPeriodCaseContext) {}

// EnterAbstractDayCase is called when production abstractDayCase is entered.
func (s *BaseAtemListener) EnterAbstractDayCase(ctx *AbstractDayCaseContext) {}

// ExitAbstractDayCase is called when production abstractDayCase is exited.
func (s *BaseAtemListener) ExitAbstractDayCase(ctx *AbstractDayCaseContext) {}

// EnterDayRange is called when production dayRange is entered.
func (s *BaseAtemListener) EnterDayRange(ctx *DayRangeContext) {}

// ExitDayRange is called when production dayRange is exited.
func (s *BaseAtemListener) ExitDayRange(ctx *DayRangeContext) {}

// EnterDaySet is called when production daySet is entered.
func (s *BaseAtemListener) EnterDaySet(ctx *DaySetContext) {}

// ExitDaySet is called when production daySet is exited.
func (s *BaseAtemListener) ExitDaySet(ctx *DaySetContext) {}

// EnterWhenMovableCycleDay is called when production whenMovableCycleDay is entered.
func (s *BaseAtemListener) EnterWhenMovableCycleDay(ctx *WhenMovableCycleDayContext) {}

// ExitWhenMovableCycleDay is called when production whenMovableCycleDay is exited.
func (s *BaseAtemListener) ExitWhenMovableCycleDay(ctx *WhenMovableCycleDayContext) {}

// EnterAbstractDateCase is called when production abstractDateCase is entered.
func (s *BaseAtemListener) EnterAbstractDateCase(ctx *AbstractDateCaseContext) {}

// ExitAbstractDateCase is called when production abstractDateCase is exited.
func (s *BaseAtemListener) ExitAbstractDateCase(ctx *AbstractDateCaseContext) {}

// EnterDateRange is called when production dateRange is entered.
func (s *BaseAtemListener) EnterDateRange(ctx *DateRangeContext) {}

// ExitDateRange is called when production dateRange is exited.
func (s *BaseAtemListener) ExitDateRange(ctx *DateRangeContext) {}

// EnterDateSet is called when production dateSet is entered.
func (s *BaseAtemListener) EnterDateSet(ctx *DateSetContext) {}

// ExitDateSet is called when production dateSet is exited.
func (s *BaseAtemListener) ExitDateSet(ctx *DateSetContext) {}

// EnterWhenDayName is called when production whenDayName is entered.
func (s *BaseAtemListener) EnterWhenDayName(ctx *WhenDayNameContext) {}

// ExitWhenDayName is called when production whenDayName is exited.
func (s *BaseAtemListener) ExitWhenDayName(ctx *WhenDayNameContext) {}

// EnterWhenDayNameCase is called when production whenDayNameCase is entered.
func (s *BaseAtemListener) EnterWhenDayNameCase(ctx *WhenDayNameCaseContext) {}

// ExitWhenDayNameCase is called when production whenDayNameCase is exited.
func (s *BaseAtemListener) ExitWhenDayNameCase(ctx *WhenDayNameCaseContext) {}

// EnterAbstractDayNameCase is called when production abstractDayNameCase is entered.
func (s *BaseAtemListener) EnterAbstractDayNameCase(ctx *AbstractDayNameCaseContext) {}

// ExitAbstractDayNameCase is called when production abstractDayNameCase is exited.
func (s *BaseAtemListener) ExitAbstractDayNameCase(ctx *AbstractDayNameCaseContext) {}

// EnterDayNameRange is called when production dayNameRange is entered.
func (s *BaseAtemListener) EnterDayNameRange(ctx *DayNameRangeContext) {}

// ExitDayNameRange is called when production dayNameRange is exited.
func (s *BaseAtemListener) ExitDayNameRange(ctx *DayNameRangeContext) {}

// EnterDayNameSet is called when production dayNameSet is entered.
func (s *BaseAtemListener) EnterDayNameSet(ctx *DayNameSetContext) {}

// ExitDayNameSet is called when production dayNameSet is exited.
func (s *BaseAtemListener) ExitDayNameSet(ctx *DayNameSetContext) {}

// EnterWhenExists is called when production whenExists is entered.
func (s *BaseAtemListener) EnterWhenExists(ctx *WhenExistsContext) {}

// ExitWhenExists is called when production whenExists is exited.
func (s *BaseAtemListener) ExitWhenExists(ctx *WhenExistsContext) {}

// EnterWhenExistsCase is called when production whenExistsCase is entered.
func (s *BaseAtemListener) EnterWhenExistsCase(ctx *WhenExistsCaseContext) {}

// ExitWhenExistsCase is called when production whenExistsCase is exited.
func (s *BaseAtemListener) ExitWhenExistsCase(ctx *WhenExistsCaseContext) {}

// EnterWhenModeOfWeek is called when production whenModeOfWeek is entered.
func (s *BaseAtemListener) EnterWhenModeOfWeek(ctx *WhenModeOfWeekContext) {}

// ExitWhenModeOfWeek is called when production whenModeOfWeek is exited.
func (s *BaseAtemListener) ExitWhenModeOfWeek(ctx *WhenModeOfWeekContext) {}

// EnterWhenModeOfWeekCase is called when production whenModeOfWeekCase is entered.
func (s *BaseAtemListener) EnterWhenModeOfWeekCase(ctx *WhenModeOfWeekCaseContext) {}

// ExitWhenModeOfWeekCase is called when production whenModeOfWeekCase is exited.
func (s *BaseAtemListener) ExitWhenModeOfWeekCase(ctx *WhenModeOfWeekCaseContext) {}

// EnterModeOfWeekSet is called when production modeOfWeekSet is entered.
func (s *BaseAtemListener) EnterModeOfWeekSet(ctx *ModeOfWeekSetContext) {}

// ExitModeOfWeekSet is called when production modeOfWeekSet is exited.
func (s *BaseAtemListener) ExitModeOfWeekSet(ctx *ModeOfWeekSetContext) {}

// EnterWhenLukanCycleDay is called when production whenLukanCycleDay is entered.
func (s *BaseAtemListener) EnterWhenLukanCycleDay(ctx *WhenLukanCycleDayContext) {}

// ExitWhenLukanCycleDay is called when production whenLukanCycleDay is exited.
func (s *BaseAtemListener) ExitWhenLukanCycleDay(ctx *WhenLukanCycleDayContext) {}

// EnterWhenSundayAfterElevationOfCrossDay is called when production whenSundayAfterElevationOfCrossDay is entered.
func (s *BaseAtemListener) EnterWhenSundayAfterElevationOfCrossDay(ctx *WhenSundayAfterElevationOfCrossDayContext) {
}

// ExitWhenSundayAfterElevationOfCrossDay is called when production whenSundayAfterElevationOfCrossDay is exited.
func (s *BaseAtemListener) ExitWhenSundayAfterElevationOfCrossDay(ctx *WhenSundayAfterElevationOfCrossDayContext) {
}

// EnterWhenSundaysBeforeTriodion is called when production whenSundaysBeforeTriodion is entered.
func (s *BaseAtemListener) EnterWhenSundaysBeforeTriodion(ctx *WhenSundaysBeforeTriodionContext) {}

// ExitWhenSundaysBeforeTriodion is called when production whenSundaysBeforeTriodion is exited.
func (s *BaseAtemListener) ExitWhenSundaysBeforeTriodion(ctx *WhenSundaysBeforeTriodionContext) {}

// EnterSundaysBeforeTriodionCase is called when production sundaysBeforeTriodionCase is entered.
func (s *BaseAtemListener) EnterSundaysBeforeTriodionCase(ctx *SundaysBeforeTriodionCaseContext) {}

// ExitSundaysBeforeTriodionCase is called when production sundaysBeforeTriodionCase is exited.
func (s *BaseAtemListener) ExitSundaysBeforeTriodionCase(ctx *SundaysBeforeTriodionCaseContext) {}

// EnterDayOverride is called when production dayOverride is entered.
func (s *BaseAtemListener) EnterDayOverride(ctx *DayOverrideContext) {}

// ExitDayOverride is called when production dayOverride is exited.
func (s *BaseAtemListener) ExitDayOverride(ctx *DayOverrideContext) {}

// EnterElementType is called when production elementType is entered.
func (s *BaseAtemListener) EnterElementType(ctx *ElementTypeContext) {}

// ExitElementType is called when production elementType is exited.
func (s *BaseAtemListener) ExitElementType(ctx *ElementTypeContext) {}

// EnterResourceText is called when production resourceText is entered.
func (s *BaseAtemListener) EnterResourceText(ctx *ResourceTextContext) {}

// ExitResourceText is called when production resourceText is exited.
func (s *BaseAtemListener) ExitResourceText(ctx *ResourceTextContext) {}

// EnterTaggedText is called when production taggedText is entered.
func (s *BaseAtemListener) EnterTaggedText(ctx *TaggedTextContext) {}

// ExitTaggedText is called when production taggedText is exited.
func (s *BaseAtemListener) ExitTaggedText(ctx *TaggedTextContext) {}

// EnterLookup is called when production lookup is entered.
func (s *BaseAtemListener) EnterLookup(ctx *LookupContext) {}

// ExitLookup is called when production lookup is exited.
func (s *BaseAtemListener) ExitLookup(ctx *LookupContext) {}

// EnterLdp is called when production ldp is entered.
func (s *BaseAtemListener) EnterLdp(ctx *LdpContext) {}

// ExitLdp is called when production ldp is exited.
func (s *BaseAtemListener) ExitLdp(ctx *LdpContext) {}

// EnterInsertBreak is called when production insertBreak is entered.
func (s *BaseAtemListener) EnterInsertBreak(ctx *InsertBreakContext) {}

// ExitInsertBreak is called when production insertBreak is exited.
func (s *BaseAtemListener) ExitInsertBreak(ctx *InsertBreakContext) {}

// EnterImportBlock is called when production importBlock is entered.
func (s *BaseAtemListener) EnterImportBlock(ctx *ImportBlockContext) {}

// ExitImportBlock is called when production importBlock is exited.
func (s *BaseAtemListener) ExitImportBlock(ctx *ImportBlockContext) {}

// EnterModeOverride is called when production modeOverride is entered.
func (s *BaseAtemListener) EnterModeOverride(ctx *ModeOverrideContext) {}

// ExitModeOverride is called when production modeOverride is exited.
func (s *BaseAtemListener) ExitModeOverride(ctx *ModeOverrideContext) {}

// EnterRole is called when production role is entered.
func (s *BaseAtemListener) EnterRole(ctx *RoleContext) {}

// ExitRole is called when production role is exited.
func (s *BaseAtemListener) ExitRole(ctx *RoleContext) {}

// EnterSectionFragment is called when production sectionFragment is entered.
func (s *BaseAtemListener) EnterSectionFragment(ctx *SectionFragmentContext) {}

// ExitSectionFragment is called when production sectionFragment is exited.
func (s *BaseAtemListener) ExitSectionFragment(ctx *SectionFragmentContext) {}

// EnterTemplateFragment is called when production templateFragment is entered.
func (s *BaseAtemListener) EnterTemplateFragment(ctx *TemplateFragmentContext) {}

// ExitTemplateFragment is called when production templateFragment is exited.
func (s *BaseAtemListener) ExitTemplateFragment(ctx *TemplateFragmentContext) {}

// EnterTemplateStatus is called when production templateStatus is entered.
func (s *BaseAtemListener) EnterTemplateStatus(ctx *TemplateStatusContext) {}

// ExitTemplateStatus is called when production templateStatus is exited.
func (s *BaseAtemListener) ExitTemplateStatus(ctx *TemplateStatusContext) {}

// EnterQualifiedName is called when production qualifiedName is entered.
func (s *BaseAtemListener) EnterQualifiedName(ctx *QualifiedNameContext) {}

// ExitQualifiedName is called when production qualifiedName is exited.
func (s *BaseAtemListener) ExitQualifiedName(ctx *QualifiedNameContext) {}

// EnterQualifiedNameWithWildCard is called when production qualifiedNameWithWildCard is entered.
func (s *BaseAtemListener) EnterQualifiedNameWithWildCard(ctx *QualifiedNameWithWildCardContext) {}

// ExitQualifiedNameWithWildCard is called when production qualifiedNameWithWildCard is exited.
func (s *BaseAtemListener) ExitQualifiedNameWithWildCard(ctx *QualifiedNameWithWildCardContext) {}

// EnterLdpType is called when production ldpType is entered.
func (s *BaseAtemListener) EnterLdpType(ctx *LdpTypeContext) {}

// ExitLdpType is called when production ldpType is exited.
func (s *BaseAtemListener) ExitLdpType(ctx *LdpTypeContext) {}

// EnterBreakType is called when production breakType is entered.
func (s *BaseAtemListener) EnterBreakType(ctx *BreakTypeContext) {}

// ExitBreakType is called when production breakType is exited.
func (s *BaseAtemListener) ExitBreakType(ctx *BreakTypeContext) {}

// EnterLanguage is called when production language is entered.
func (s *BaseAtemListener) EnterLanguage(ctx *LanguageContext) {}

// ExitLanguage is called when production language is exited.
func (s *BaseAtemListener) ExitLanguage(ctx *LanguageContext) {}

// EnterDayOfWeek is called when production dayOfWeek is entered.
func (s *BaseAtemListener) EnterDayOfWeek(ctx *DayOfWeekContext) {}

// ExitDayOfWeek is called when production dayOfWeek is exited.
func (s *BaseAtemListener) ExitDayOfWeek(ctx *DayOfWeekContext) {}

// EnterModeTypes is called when production modeTypes is entered.
func (s *BaseAtemListener) EnterModeTypes(ctx *ModeTypesContext) {}

// ExitModeTypes is called when production modeTypes is exited.
func (s *BaseAtemListener) ExitModeTypes(ctx *ModeTypesContext) {}

// EnterMonthName is called when production monthName is entered.
func (s *BaseAtemListener) EnterMonthName(ctx *MonthNameContext) {}

// ExitMonthName is called when production monthName is exited.
func (s *BaseAtemListener) ExitMonthName(ctx *MonthNameContext) {}

// EnterTemplateStatuses is called when production templateStatuses is entered.
func (s *BaseAtemListener) EnterTemplateStatuses(ctx *TemplateStatusesContext) {}

// ExitTemplateStatuses is called when production templateStatuses is exited.
func (s *BaseAtemListener) ExitTemplateStatuses(ctx *TemplateStatusesContext) {}
