# Development Mode for Web Components

This feature allows you to edit web components and see changes instantly without restarting the Go application.

## Setup

### 1. Install Bun.js

If you haven't already installed Bun.js, do so with:

```bash
curl -fsSL https://bun.sh/install | bash
```

### 2. Run in Development Mode

```bash
# Make the script executable if not already
chmod +x dev.sh

# Run in development mode
./dev.sh
```

## How It Works

1. The `dev.sh` script starts a Bun.js development server on port 3000 that serves static files from the `static` directory
2. It then builds and runs the Go application with `DOXA_DEV_MODE=true`
3. When in development mode, the Go application proxies all requests for static files to the Bun dev server instead of using the embedded files
4. This allows you to edit files in the `static/wc` directory and immediately see the changes without restarting the Go app

## Benefits

- Instant feedback when developing web components
- No need to restart the Go application after each change
- Development remains simple - no complex build systems or npm dependencies
- Production builds still use embedded files for deployment

## Troubleshooting

- If you see errors about the dev server, make sure Bun is installed correctly
- If changes aren't showing up, try doing a hard refresh in your browser (Ctrl+F5 or Cmd+Shift+R)
- Check the application logs for any errors related to the dev server