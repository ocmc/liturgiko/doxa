# DOXA Database ID System

This document explains the database ID system in DOXA, including the KeyPath structure and the hierarchical organization of different data types.

## Overview

DOXA uses a hierarchical key-value database system with the following structure:

- **KeyPath**: The primary structure representing database IDs with two components:
  - `Dirs`: Directory segments (similar to filesystem folders)
  - `KeyParts`: Key segments that identify specific records

- **Delimiters**:
  - `:` (colon) - Separates directory path from key parts
  - `/` (slash) - Separates segments within directories or key parts

- **Example ID**: `ltx/en_us_dedes/actors:Priest` where:
  - `ltx/en_us_dedes/actors` is the directory path
  - `Priest` is the key part

- **Dual Meaning of "Key"**:
  1. Traditional database meaning: The full identifier to retrieve a value
  2. DOXA user meaning: Hierarchical structure using filesystem path metaphor

## Key Implementation Details

The `KeyPath` structure (defined in `db/kvs/kvs.go`) has these key components:

```go
type KeyPath struct {
    Dirs     *stack.StringStack
    KeyParts *stack.StringStack
}
```

Important methods:
- `Path()` - Returns the full path with both directories and key parts
- `DirPath()` - Returns just the directory path
- `Key()` - Returns just the key parts
- `ParsePath()` - Parses a string into directory and key parts

## Database Root Structure

The root of the DOXA database always contains these primary buckets (directories):
- `btx` (Biblical Text)
- `configs`
- `keyring`
- `ltx` (Liturgical Text)
- `media`

## Bucket-Specific Hierarchies

### Biblical Text (btx) Hierarchy

Example: `btx/en_uk_kjv/act/c001:001`

- **Segment 1**: Bucket name (`btx` for Biblical Text)
- **Segment 2**: Library identifier with three underscore-delimited parts:
  - Part 1: ISO language code (e.g., `en` for English)
  - Part 2: ISO country code (e.g., `uk` for United Kingdom)
  - Part 3: User-defined realm/version (e.g., `kjv` for King James Version)
- **Segment 3**: Biblical book abbreviation (e.g., `act` for Acts)
- **Segment 4**: Chapter indicator:
  - Prefixed with letter `c` (for "Chapter")
  - Followed by a 3-digit number with zero padding
  - Example: `c001` for Chapter 1
- **KeyParts**: Verse number after the colon delimiter:
  - 3-digit number with zero padding
  - Example: `:001` for Verse 1

So the complete ID `btx/en_uk_kjv/act/c001:001` represents "English (UK) King James Version, Book of Acts, Chapter 1, Verse 1".

### Configuration Settings (configs) Hierarchy

Examples: 
- `configs/default/site/build/assets/copy.assets:value`
- `configs/default/site/build/frames/score/paragraphs/enabled:desc`

Structure:
- **Segment 1**: Bucket name `configs` for configuration settings
- **Segments 2..n**: Variable-length category hierarchy using forward slashes
  - Can have an arbitrary number of path segments until the keyparts delimiter (colon)
  - Organizes settings in a logical hierarchical structure
- **KeyParts**: After the colon delimiter, contains the actual configuration key

Important distinction: `configs` demonstrates how DOXA IDs can have flexible path depth before reaching the key part.

### Liturgical Text (ltx) Hierarchy

Example: `ltx/en_us_goadedes/actors:ac.Priest`

Structure:
- **Segment 1**: Bucket name `ltx` for Liturgical text
- **Segment 2**: Library identifier (similar structure to btx)
  - Example: `en_us_goadedes` - translations by Fr. Seraphim Dedes in US English for the Greek Archdiocese of America
- **Segment 3**: Topic - a group name for related keypaths
  - Example: `actors` - relating to clerical roles
- **KeyParts**: After the colon delimiter
  - Example: `ac.Priest` - a specific clerical role

Important: For `ltx` records, segments have specific terminology:
- Segment 1: Bucket name
- Segment 2: Library
- Segment 3: Topic
- After colon: Key

The full ID retrieves the English word used by this library to translate the Greek word for Priest.

## Usage in Database Explorer

The database explorer web component (`static/wc/db-explorer.js`) provides a user interface for browsing this hierarchical structure. The ID search functionality allows users to search for specific record IDs within the current directory context.

Key features related to IDs:
- Breadcrumb navigation represents the current directory path
- ID search shows the current path context to help users understand search scope
- Various methods for working with IDs (copy, edit, delete)
- Special handling for liturgical IDs (SID, RID) through the KeyPath methods

## Implementation Notes

- BoltDB provides the actual implementation of buckets (as directories) and key-value pairs
- The system supports redirects, allowing one ID to point to another
- Special handling for liturgical text IDs through KeyPath methods like `LmlSid()` and `LmlRid()`