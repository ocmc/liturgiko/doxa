# Pandoc Setup for Arabic with LuaHBTeX

This guide provides a detailed setup for using Pandoc with LuaHBTeX for optimal Arabic text rendering.

## Basic Setup

First, create a custom template file named `arabic-template.tex`:

```bash
pandoc -D latex > arabic-template.tex
```

Then edit this template to include the necessary packages and settings for Arabic with LuaHBTeX. Here's how to modify it:

```tex
% Replace the existing configuration with these settings at the appropriate places
\documentclass[$if(fontsize)$$fontsize$,$endif$$if(lang)$$babel-lang$,$endif$$if(papersize)$$papersize$paper,$endif$$for(classoption)$$classoption$$sep$,$endfor$]{$documentclass$}

% Load fontspec for font handling
\usepackage{fontspec}

% Load luaotfload with HarfBuzz renderer
\usepackage[harfbuzz]{luaotfload}

% Load bidi for bidirectional text support
\usepackage{bidi}

% Load arabluatex for Arabic support
\usepackage{arabluatex}

% Set the default font to one with good Arabic support
\setmainfont{Amiri}[
  Script=Arabic,
  Renderer=HarfBuzz
]

% Set RTL as the main direction
\setRTL

% For mixing LTR and RTL content
\usepackage{microtype}
\SetTracking{encoding=*}{80}
```

Now, use this command to convert your Markdown with Arabic content:

```bash
pandoc -f markdown -t pdf --pdf-engine=lualatex --template=arabic-template.tex \
  --variable mainfont="Amiri" \
  --variable documentclass=article \
  --variable dir=rtl \
  input.md -o output.pdf
```

## Advanced Configuration Options

You can enhance the setup with these options:

### For Better Arabic Justification

```tex
% Add to your template
\usepackage{arabtex}
\usepackage{utf8}
\setcode{utf8}
```

### For Specific Arabic Font Features

```tex
\setmainfont{Amiri}[
  Script=Arabic,
  Renderer=HarfBuzz,
  Extension=.ttf,
  UprightFont=*-Regular,
  BoldFont=*-Bold,
  ItalicFont=*-Italic,
  BoldItalicFont=*-BoldItalic,
  SizeFeatures={
    {Size=-10, Font=*},
    {Size=10-, Font=*}
  },
  CharacterVariant={1,3},
  Contextuals=Alternate
]
```

### Create a Metadata Block in Your Markdown File

```yaml
---
title: "عنوان المستند"
author: "اسم المؤلف"
date: "التاريخ"
dir: rtl
lang: ar
mainfont: Amiri
header-includes:
  - \usepackage{arabluatex}
  - \usepackage[harfbuzz]{luaotfload}
  - \setRTL
---
```

## Testing the Setup

Create a test file with mixed Arabic and English content:

```markdown
---
title: "اختبار النص العربي"
author: "Test Author"
date: "2025-03-08"
dir: rtl
lang: ar
---

# اختبار النص العربي

هذا مثال للنص العربي مع لواهبتكس. يجب أن يظهر النص بشكل صحيح مع تشكيل مناسب.

## English Section

This is an example of mixed content with English text.

## قسم عربي آخر

نص عربي آخر للتأكد من عمل التنسيق بشكل صحيح، مع الحركات والتشكيل: بِسْمِ اللّٰهِ الرَّحْمٰنِ الرَّحِيمِ
```

## Troubleshooting Tips

1. **Font issues**: Make sure Amiri (or your chosen font) is installed on your system
2. **Package errors**: You may need to install additional TeX packages:
   ```bash
   tlmgr install fontspec luaotfload bidi arabluatex arabtex microtype
   ```
3. **Rendering issues**: Try different font rendering options or different fonts that support Arabic well, such as:
   - Scheherazade
   - Noto Naskh Arabic
   - Arabic Typesetting
   - Traditional Arabic
   - Simplified Arabic
4. **Complex layouts**: For very complex layouts, consider creating a direct LaTeX document

## Why LuaHBTeX Works Well for Arabic

The LuaHBTeX engine with HarfBuzz rendering provides excellent support for complex Arabic script features:

- Proper letter shaping and contextual forms
- Correct positioning of vowel marks (tashkeel)
- Ligatures specific to Arabic typography
- Proper handling of bidirectional text
- Kashida justification
- Support for various Arabic calligraphic styles

## Additional Resources

- [Overleaf guide on Arabic in LaTeX](https://www.overleaf.com/learn/latex/Arabic)
- [arabluatex package documentation](https://ctan.org/pkg/arabluatex)
- [luaotfload package documentation](https://ctan.org/pkg/luaotfload)
- [Amiri Font Project](https://www.amirifont.org/)
- [Arabic Typography in TeX and Digital Typography](https://tug.org/TUGboat/tb27-2/tb87benatia.pdf)

## Complete Command-Line Example

```bash
# Create a template
pandoc -D latex > arabic-template.tex

# Edit the template as described above

# Convert markdown to PDF with all settings
pandoc -f markdown -t pdf \
  --pdf-engine=lualatex \
  --template=arabic-template.tex \
  --variable mainfont="Amiri" \
  --variable documentclass=article \
  --variable dir=rtl \
  --variable lang=ar \
  --variable fontsize=12pt \
  --variable papersize=a4 \
  --variable geometry="margin=1in" \
  input.md -o output.pdf
```
