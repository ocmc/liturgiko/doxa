# Git Workflow for DOXA Tools

This document outlines the git branching strategy, release management, and development workflow for DOXA Tools.

## Repository Information

- Project URL: [https://gitlab.com/ocmc/liturgiko/doxa](https://gitlab.com/ocmc/liturgiko/doxa)
- Tags URL: [https://gitlab.com/ocmc/liturgiko/doxa/-/tags](https://gitlab.com/ocmc/liturgiko/doxa/-/tags)
- Releases URL: [https://gitlab.com/ocmc/liturgiko/doxa/-/releases](https://gitlab.com/ocmc/liturgiko/doxa/-/releases)
- Latest Release Tag: `v0.130.20` (at time of writing)

## Semantic Versioning

DOXA Tools uses semantic versioning (SemVer) for release numbering. The version number format is: `vMAJOR.MINOR.PATCH`

- **MAJOR**: Incremented for incompatible API changes
- **MINOR**: Incremented for new functionality that is backward-compatible
- **PATCH**: Incremented for backward-compatible bug fixes

The first public release will be `v1.0.0`. Prior to that, we are using `v0.x.y` to indicate pre-release development versions.

## Branch Structure

### Main Branch

The `main` branch represents the current stable release of DOXA Tools. When we complete a release, we merge it into the `main` branch and tag it with the appropriate version number.

### Release Branches

Release branches are used to prepare and stabilize code for an upcoming release. They are named with the prefix `r/` followed by the version number:

```
r/v0.131.01
r/v0.130.21
```

When a release is merged into `main`, we create a new release branch for the next planned release. All development work for the next release is branched from this release branch.

## Software Action Requests (SARs)

Instead of "issues," we use **Software Action Requests (SARs)** to track development tasks. SARs are managed in ClickUp and can be one of the following types:

- **Feature Request**: Request for entirely new functionality
- **Change Request**: Request to modify existing functionality
- **Problem Report**: Report of a defect or issue in the existing codebase
- **Task Request**: Request to perform research or investigation that may lead to future SARs

### SAR Naming Convention

SARs follow this naming pattern:
```
SAR-[Number]-[Brief-Description]
```

Examples:
- `SAR-30-matcher-id-search`
- `SAR-124-fix-inventories`

## Development Workflow

1. **Create SAR Branch**: 
   - To work on a SAR, create a new branch from the current release branch
   - Example: To work on `SAR-30-matcher-id-search`, switch to release branch `r/v0.131.01` and then create a branch named `SAR-30-matcher-id-search`

2. **Development Work**:
   - Make frequent commits in the SAR branch
   - Push regularly to the GitLab project to maintain a backup of your work
   - Complete all unit testing within the SAR branch

3. **Code Integration**:
   - When development and unit testing are complete, merge the SAR branch into the release branch
   - Example: Merge `SAR-30-matcher-id-search` into `r/v0.131.01`

4. **Release Testing**:
   - In the release branch, perform regression testing and integration testing
   - Fix any issues that arise during testing

5. **Release Deployment**:
   - When all SARs for the release are complete and testing is successful, merge the release branch into the `main` branch
   - Tag the commit on `main` with the appropriate version number (e.g., `v0.131.01`)
   - Create a release in GitLab associated with this tag

## Example Workflow

1. Current release is `v0.130.20` on the `main` branch
2. Release branch `r/v0.131.01` created for next release
3. Developer creates branch `SAR-30-matcher-id-search` from `r/v0.131.01`
4. Developer completes work and testing on `SAR-30-matcher-id-search`
5. `SAR-30-matcher-id-search` is merged into `r/v0.131.01`
6. After all SARs are completed and tested, `r/v0.131.01` is merged into `main`
7. The commit is tagged as `v0.131.01` in the `main` branch
8. A new release branch is created for the next release (e.g., `r/v0.131.02` or `r/v0.132.01`)

## Best Practices

1. Always branch from the appropriate release branch, not from `main`
2. Make regular, small commits with clear commit messages
3. Push your branches to GitLab regularly
4. Complete all unit testing before merging a SAR branch into a release branch
5. Never merge directly from a SAR branch to `main`
