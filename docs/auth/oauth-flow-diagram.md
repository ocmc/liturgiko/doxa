# GitLab OAuth Authentication Flow

This document illustrates the OAuth 2.0 authentication flow between the user, our application, and GitLab.

## OAuth Flow Sequence Diagram

```mermaid
sequenceDiagram
    participant User
    participant App as Go Application
    participant Browser
    participant GitLab as GitLab OAuth Server
    participant GitLabAPI as GitLab API

    %% Initial Setup
    Note over User, GitLab: One-time Setup
    User->>GitLab: Log in to GitLab
    User->>GitLab: Register OAuth application
    GitLab-->>User: Provide Client ID and Secret
    User->>App: Enter Client ID and Secret
    App->>App: Store credentials securely

    %% Authentication Flow
    Note over User, GitLabAPI: Authentication Flow
    User->>App: Launch application
    App->>App: Check for valid session
    
    alt No valid session
        App->>App: Generate state parameter
        App->>Browser: Open browser with GitLab auth URL
        Browser->>GitLab: Request authorization (/oauth/authorize)
        GitLab->>Browser: Display login page (if not logged in)
        User->>GitLab: Enter GitLab credentials
        GitLab->>Browser: Display authorization consent
        User->>GitLab: Approve authorization
        GitLab->>Browser: Redirect to callback URL with auth code
        Browser->>App: Pass auth code to application
        App->>GitLab: Exchange code for access token (/oauth/token)
        GitLab-->>App: Return access token, refresh token
        App->>App: Store tokens securely
    else Valid session exists
        App->>App: Use existing access token
    end

    %% Using the API
    User->>App: Request GitLab data
    App->>App: Check token validity
    
    alt Token expired
        App->>GitLab: Request token refresh
        GitLab-->>App: Provide new access token
        App->>App: Update stored token
    end
    
    App->>GitLabAPI: Make API request with token
    GitLabAPI-->>App: Return requested data
    App-->>User: Display data

    %% Token Revocation (Optional)
    opt User revokes access
        User->>GitLab: Visit applications page
        User->>GitLab: Revoke application access
        Note over App, GitLabAPI: Future API requests will fail
        App->>GitLabAPI: API request with revoked token
        GitLabAPI-->>App: Return 401 Unauthorized
        App-->>User: Prompt to re-authenticate
    end
```

## Flow Description

### One-time Setup
1. User logs into GitLab and registers a new OAuth application
2. GitLab provides Client ID and Secret for the application
3. User enters these credentials into the Go application
4. Application securely stores these credentials

### Authentication Flow
1. When the user launches the application, it checks for a valid session
2. If no valid session exists:
   - Application generates a secure state parameter to prevent CSRF attacks
   - Application opens the browser with the GitLab authorization URL
   - User logs into GitLab (if not already logged in)
   - User approves the authorization request
   - GitLab redirects back to the application with an authorization code
   - Application exchanges the code for access and refresh tokens
   - Application securely stores the tokens

### Using the API
1. When the user requests data from GitLab, the application checks if the token is valid
2. If the token has expired, the application uses the refresh token to get a new access token
3. The application makes API requests to GitLab using the access token
4. The application displays the returned data to the user

### Token Revocation
1. At any time, the user can revoke access by visiting GitLab's applications page
2. If a token is revoked, future API requests will fail
3. The application detects the failure and prompts the user to re-authenticate

## Technical Implementation Details

- **State Parameter**: A cryptographically secure random string used to prevent CSRF attacks
- **Access Token**: Short-lived token (typically 2 hours) used to access the GitLab API
- **Refresh Token**: Long-lived token used to obtain new access tokens without re-authentication
- **Secure Storage**: Tokens stored either in the system keychain or encrypted on disk

## Benefits of OAuth vs Personal Access Tokens

1. **Better Security**: Access tokens expire automatically
2. **Improved User Experience**: No manual token copying required
3. **Granular Permissions**: Specific scopes can be requested
4. **Revocation**: Easy access revocation without affecting other applications
