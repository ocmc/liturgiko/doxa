package main

import (
	"encoding/json"
	"fmt"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"os"
)

type Site struct {
	Name        string
	URL         string
	Account     string
	AppendDCS   bool
	Description string
}

func main() {
	// TODO: need an online way to manage this.
	// writes a json file containing information about available DCS websites
	var sites = []Site{
		{"GOA DCS",
			"https://dcs.goarch.org",
			"goa",
			true,
			"DCS for the Greek Orthodox Archdiocese of America",
		},
		{"Demo GOA DCS",
			"https://demo.liml.org",
			"",
			false,
			"Demonstration GOA DCS generated using Doxa",
		},
		{"Liturgia Digital",
			"https://liturgia.mx",
			"",
			false,
			"Liturgia Digital es un sitio web del Sacro Arzobispado Ortodoxo Griego de México (Patriarcado Ecuménico).",
		},
	}
	content, err := json.MarshalIndent(sites, "", " ")
	if err != nil {
		fmt.Printf("error marshalling sites: %v", err)
		os.Exit(2)
	}
	err = ltfile.WriteFile("./xcmd/registry/registered.json", string(content))
	if err != nil {
		fmt.Printf("error writing json: %v", err)
		os.Exit(2)
	}
}
