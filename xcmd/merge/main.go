package main

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"os"
	"strings"
)

func main() {
	var file1 string = "/Volumes/macWdb/temp/swh_merge/swh_ke_oak.tsv"
	var file2 string = "/Volumes/macWdb/temp/swh_merge/before_olw_updates_swh_ke_oak.tsv"

	var linesFromF1 []string
	var linesFromF2 []string

	var err error

	var map1 map[string]string
	var map2 map[string]string
	map1 = make(map[string]string)
	map2 = make(map[string]string)

	var adds []string
	var deletes []string
	var changes []string

	linesFromF1, err = ltfile.GetFileLines(file1)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
	for _, l := range linesFromF1 {
		parts := strings.Split(l, "\t")
		if len(parts) != 2 {
			fmt.Printf("%s does not split into 2", l)
			continue
		}
		map1[parts[0]] = parts[1]
	}
	linesFromF2, err = ltfile.GetFileLines(file2)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
	for _, l := range linesFromF2 {
		parts := strings.Split(l, "\t")
		if len(parts) != 2 {
			fmt.Printf("%s does not split into 2", l)
			continue
		}
		map2[parts[0]] = parts[1]
	}
	for k, v := range map1 {
		var ok bool
		var v2 string
		if v2, ok = map2[k]; !ok {
			deletes = append(deletes, fmt.Sprintf("%s\t%s", k, v))
			continue
		}
		if strings.Compare(v, v2) != 0 {
			changes = append(changes, fmt.Sprintf("%s\t%s", k, v2))
		}
	}
	for k, v := range map2 {
		var ok bool
		if _, ok = map1[k]; !ok {
			adds = append(adds, fmt.Sprintf("%s\t%s", k, v))
			continue
		}
	}
	fmt.Println("Adds")
	for _, v := range adds {
		fmt.Println(v)
	}
	fmt.Println("Changes")
	for _, v := range changes {
		fmt.Println(v)
	}
	fmt.Println("Deletes")
	for _, v := range deletes {
		fmt.Println(v)
	}
}
