package main

import (
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"path/filepath"
	"strings"
)

var newRedirectLib string = "swh_redirects_oak"
var toLib string = "swh_ke_oak"

func main() {
	/**
	This utility processes an export of en_us_public,
	and writes it out as the specified library
	*/
	enUsPublicPath := "/Volumes/macWdb/doxa/projects/doxa-alwb/exports/ltx/en_us_public.tsv"
	fileOut := filepath.Join("/Volumes/macWdb/doxa/projects/doxa-alwb/imports/swh_redirects_oak.tsv")
	var newLines []string
	lines, err := ltfile.GetFileLines(enUsPublicPath)
	if err != nil {
		fmt.Println(err)
		return
	}
	for i, l := range lines {
		var n string
		n, err = newLine(l)
		if err != nil {
			fmt.Printf("line %d: %v\n", i, err)
			continue
		}
		newLines = append(newLines, n)
	}
	err = ltfile.WriteLinesToFile(fileOut, newLines)
	if err != nil {
		fmt.Println(err)
		return
	}
}
func newLine(l string) (n string, err error) {
	parts := strings.Split(l, "\t")
	if len(parts) != 2 {
		return "", fmt.Errorf("does not split into two using tab")
	}
	kp := kvs.NewKeyPath()
	err = kp.ParsePath(parts[0])
	if err != nil {
		return "", fmt.Errorf("error parsing path %s: %v", parts[0], err)
	}
	err = kp.Dirs.Set(1, newRedirectLib)
	if err != nil {
		return "", fmt.Errorf("error replacing %s with %s: %v", kp.Dirs.Get(1), toLib)
	}
	// see if value is redirect
	if !strings.HasPrefix(parts[1], "@") {
		return fmt.Sprintf("%s\t%s", kp.Path(), parts[1]), nil
	}
	kpRedirect := kvs.NewKeyPath()
	err = kpRedirect.ParsePath(parts[1][1:])
	if err != nil {
		return "", fmt.Errorf("error parsing path %s: %v", parts[0], err)
	}
	err = kpRedirect.Dirs.Set(1, toLib)
	if err != nil {
		return "", fmt.Errorf("error replacing %s with %s: %v", kpRedirect.Dirs.Get(1), toLib)
	}
	return fmt.Sprintf("%s\t@%s", kp.Path(), kpRedirect.Path()), nil
}
