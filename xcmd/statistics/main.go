package main

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
)

func main() {
	srcDir := "pkg" // go
	files, err := ltfile.FileMatcher(srcDir, "go", nil)
	//srcDir := "static/wc" // go
	//files, err := ltfile.FileMatcher(srcDir, "js", nil)
	if err != nil {
		fmt.Println(err)
		return
	}
	var fileCount, lineCount int
	var minLines, maxLines int
	var lines []string
	for _, f := range files {
		fileCount++
		lines, err = ltfile.GetFileLines(f)
		if err != nil {
			fmt.Println(err)
			return
		}
		l := len(lines)
		if l > maxLines {
			maxLines = l
		}
		if l < minLines || minLines == 0 {
			minLines = l
		}
		lineCount += l
	}
	fmt.Printf("%d files, %d lines\nmin %d max %d ave %d\n", fileCount, lineCount, minLines, maxLines, lineCount/fileCount)
}
