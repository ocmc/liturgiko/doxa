package main

import (
	"context"
	"fmt"
	"html/template"
	"net/http"
	"sync"
	"time"

	"github.com/google/uuid"
)

type ProcessManager struct {
	processes map[string]context.CancelFunc
	mu        sync.Mutex
}

func NewProcessManager() *ProcessManager {
	return &ProcessManager{
		processes: make(map[string]context.CancelFunc),
	}
}

func (pm *ProcessManager) StartProcess(ctx context.Context) string {
	processID := uuid.New().String()
	ctx, cancel := context.WithCancel(ctx)

	pm.mu.Lock()
	pm.processes[processID] = cancel
	pm.mu.Unlock()

	go runLongProcess(ctx, processID)

	return processID
}

func (pm *ProcessManager) CancelProcess(processID string) bool {
	pm.mu.Lock()
	defer pm.mu.Unlock()

	if cancel, exists := pm.processes[processID]; exists {
		cancel()                        // Trigger cancellation
		delete(pm.processes, processID) // Remove from ProcessManager after explicit cancellation
		fmt.Printf("Cancellation requested for process %s\n", processID)
		return true
	}
	return false
}

func (pm *ProcessManager) GetProcesses() []string {
	pm.mu.Lock()
	defer pm.mu.Unlock()

	var ids []string
	for id := range pm.processes {
		ids = append(ids, id)
	}
	return ids
}

func runLongProcess(ctx context.Context, processID string) {
	fmt.Printf("Main process %s started\n", processID)

	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		defer wg.Done()
		runSubProcess(ctx, processID)
	}()

	startTime := time.Now()
	for time.Since(startTime) < 5*time.Minute {
		select {
		case <-ctx.Done():
			fmt.Printf("Main process %s cancelled\n", processID)
			wg.Wait() // Wait for sub-process to finish
			return
		default:
			fmt.Printf("Main process %s: Running for %v\n", processID, time.Since(startTime))
			time.Sleep(10 * time.Second)
		}
	}

	wg.Wait()
	fmt.Printf("Main process %s completed\n", processID)
}

func runSubProcess(ctx context.Context, processID string) {
	fmt.Printf("Sub-process for %s started\n", processID)

	startTime := time.Now()
	for time.Since(startTime) < 5*time.Minute {
		select {
		case <-ctx.Done():
			fmt.Printf("Sub-process for %s cancelled\n", processID)
			return
		default:
			fmt.Printf("Sub-process for %s: Running for %v\n", processID, time.Since(startTime))
			time.Sleep(15 * time.Second)
		}
	}

	fmt.Printf("Sub-process for %s completed\n", processID)
}

// Define the template as a constant string in Go.
const indexHTML = `
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Process Manager</title>
  <link rel="stylesheet" href="https://unpkg.com/@picocss/pico@latest/css/pico.min.css">
  <script src="https://unpkg.com/htmx.org@1.9.2"></script>
</head>
<body>

  <main class="container">
    <h1>Process Manager</h1>

    <!-- Button to start a new process -->
    <button hx-post="/start" hx-target="#process-list" hx-swap="beforeend">Start New Process</button>

    <!-- List of running processes -->
    <h2>Running Processes:</h2>
    <ul id="process-list" hx-get="/processes" hx-trigger="load"></ul>

    <!-- Selected Process and Cancel Button -->
    <div id="selected-process"></div>

  </main>

</body>
</html>
`

func main() {
	pm := NewProcessManager()

	tmpl := template.Must(template.New("index").Parse(indexHTML))

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		tmpl.Execute(w, nil)
	})

	http.HandleFunc("/start", func(w http.ResponseWriter, r *http.Request) {
		processID := pm.StartProcess(r.Context())

		// Append new process ID to the list without replacing existing content.
		fmt.Fprintf(w, `<li hx-get="/select?id=%s">%s</li>`, processID, processID)
	})

	http.HandleFunc("/cancel", func(w http.ResponseWriter, r *http.Request) {
		processID := r.URL.Query().Get("id")

		if pm.CancelProcess(processID) {
			// After cancelling a process, return an updated list of processes.
			processes := pm.GetProcesses()

			// Dynamically generate the updated list of processes.
			fmt.Fprint(w, "<ul>")
			for _, id := range processes {
				fmt.Fprintf(w, `<li><a href="#" hx-get="/select?id=%s">%s</a></li>`, id, id)
			}
			fmt.Fprint(w, "</ul>")
			return
		}

		http.Error(w, "Process not found", http.StatusNotFound)
	})

	http.HandleFunc("/processes", func(w http.ResponseWriter, r *http.Request) {
		processes := pm.GetProcesses()

		// Dynamically generate the list of processes.
		fmt.Fprint(w, "<ul>")
		for _, id := range processes {
			fmt.Fprintf(w, `<li><a href="#" hx-get="/select?id=%s">%s</a></li>`, id, id)
		}
		fmt.Fprint(w, "</ul>")
	})

	// Handle selecting a specific ID and showing it near the cancel button.
	http.HandleFunc("/select", func(w http.ResponseWriter, r *http.Request) {
		id := r.URL.Query().Get("id")
		fmt.Fprintf(w, `<div>Selected Process ID: <strong>%s</strong></div>
                        <button hx-get="/cancel?id=%s" hx-target="#process-list" hx-swap="outerHTML">Cancel Process</button>`, id, id)
	})

	fmt.Println("Server starting on :8080")
	http.ListenAndServe(":8080", nil)
}
