package main

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"os"
	"sort"
	"strings"
)

func main() {
	// attempts to determine topic-key renames by comparing values
	var file1 string = "/Volumes/macWdb/doxa/projects/doxa-liml/exports/ltx/from_olw_before_frs_fixes_swh_ke_oak.tsv"
	var file2 string = "/Volumes/macWdb/doxa/projects/doxa-liml/exports/ltx/after_fr_seraphim_swh_ke_oak.tsv"

	var linesFromF1 []string
	var linesFromF2 []string

	var err error

	var map1 map[string]string
	var map2 map[string]string
	map1 = make(map[string]string)
	map2 = make(map[string]string)

	type FromTo struct {
		From string
		To   string
	}
	var changes []*FromTo

	linesFromF1, err = ltfile.GetFileLines(file1)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
	// load map 1
	for _, l := range linesFromF1 {
		parts := strings.Split(l, "\t")
		if len(parts) != 2 {
			fmt.Printf("%s does not split into 2", l)
			continue
		}
		k := strings.TrimSpace(parts[0])
		v := strings.TrimSpace(parts[1])
		if len(v) > 0 {
			var ok bool
			var x string
			if x, ok = map1[v]; ok {
				_ = fmt.Sprintf("TODO DELETE ME %s", x)
			}
			map1[v] = k
		}
	}
	linesFromF2, err = ltfile.GetFileLines(file2)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
	for _, l := range linesFromF2 {
		parts := strings.Split(l, "\t")
		if len(parts) != 2 {
			fmt.Printf("%s does not split into 2", l)
			continue
		}
		k := strings.TrimSpace(parts[0])
		v := strings.TrimSpace(parts[1])

		if len(v) > 0 {
			var ok bool
			var x string
			if x, ok = map2[v]; ok {
				_ = fmt.Sprintf("TODO DELETE ME %s", x)
			}
			map2[v] = k
		}
	}
	for v, k := range map1 {
		if v == "WATU" {
			_ = fmt.Sprintf("TODO DELETE ME")
		}
		var ok bool
		var k2 string
		if k2, ok = map2[v]; ok {
			c := new(FromTo)
			c.From = k
			c.To = k2
			changes = append(changes, c)
			continue
		}
	}
	sort.Slice(changes, func(i, j int) bool {
		return changes[i].From < changes[j].From
	})
	fmt.Println("Changes")
	for _, v := range changes {
		fmt.Printf("%s -> %s\n", v.From, v.To)
	}
}
