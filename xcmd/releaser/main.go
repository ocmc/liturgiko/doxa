// package man creates a Doxa release on github
// Must have installed github cli.
// Update doxa main.go VERSION.
// Update release.sh with release info
package main

import (
	"crypto/sha256"
	"fmt"
	"github.com/go-git/go-billy/v5/memfs"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/transport/http"
	"github.com/go-git/go-git/v5/storage/memory"
	"github.com/liturgiko/doxa/pkg/updater"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/spf13/viper"
	"golang.org/x/net/context"
	"golang.org/x/sync/errgroup"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

var checksumFile = "checksum.txt"
var configName = "release"
var configFile = configName + ".yaml"
var distDir = "../../dist"
var osTypes = []string{"darwin", "windows"}
var archTypes = []string{"amd64", "arm64"}
var Release *updater.ReleaseInfo
var githubRepoUrl string
var githubReleaseUrl string
var addresses []string

// main creates a new release to the GitHub repository.
// Be sure to set the environment variable GH_TOKEN
func main() {
	var test bool
	if len(os.Args) > 1 {
		test = os.Args[1] == "-test"
	}
	// read in the config file
	viper.SetConfigName("release") // name of config file (without extension)
	viper.SetConfigType("yaml")    // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath(".")
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		panic(fmt.Errorf("fatal error config file: %w", err))
	}
	// read in the config settings
	desc := viper.GetString("Description")
	if len(desc) == 0 {
		fmt.Printf("missing Description in %s\n", configFile)
		os.Exit(1)
	}
	releaseId := viper.GetString("ReleaseId")
	if len(releaseId) == 0 {
		fmt.Printf("missing ReleaseId in %s\n", configFile)
		os.Exit(1)
	}
	if !strings.HasPrefix(releaseId, "v") {
		fmt.Printf("bad releaseId format: %s must start with v\n", releaseId)
		os.Exit(1)
	}
	parts := strings.Split(releaseId, ".")
	if len(parts) != 3 {
		fmt.Printf("bad releaseId format: %s must have format vx.x.x\n", releaseId)
		os.Exit(1)
	}
	releaseInfo := viper.GetString("ReleaseInfo")
	if len(releaseInfo) == 0 {
		fmt.Printf("missing ReleaseInfo in %s\n", configFile)
		os.Exit(1)
	}

	// GitHub configuration
	githubReleaseUrl = viper.GetString("GithubReleaseUrl")
	if len(githubReleaseUrl) == 0 {
		fmt.Printf("missing GithubReleaseUrl in %s\n", configFile)
		os.Exit(1)
	}
	githubRepoUrl = viper.GetString("GithubRepoUrl")
	if len(githubRepoUrl) == 0 {
		fmt.Printf("missing GithubRepoUrl in %s\n", configFile)
		os.Exit(1)
	}
	pushReleaseJson := viper.GetBool("PushReleaseJson")
	fmt.Printf("PushReleaseJson == %v\n", pushReleaseJson)

	addresses = viper.GetStringSlice("addresses")

	// initialize the release struct
	Release = updater.NewReleaseInfo()
	Release.Description = desc
	Release.ReleaseId = releaseId
	Release.ReleaseInfo = releaseInfo

	createOutputs(Release.ReleaseId)

	if err = CreateDistDirs(); err != nil {
		fmt.Printf("%v\n", err)
		os.Exit(1)
	}
	// BuildBinaries will also create the archive and compute a checksum for each entry in outputs
	if err = BuildBinaries(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	// CreateChecksumFile iterates the slice of outputs and writes out a checksum.txt file
	if err = CreateChecksumFile(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	// Populate ReleaseBinary map
	if err = PopulateReleaseBinaryMap(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	if test {
		fmt.Println("-test flag set, so exiting before creating Github release")
		return
	}
	// Create GitHub release
	fmt.Println("Creating release in Github")
	if err = CreateGhRelease(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	// Create release.json and push it
	fmt.Println("Creating release.json and pushing it")
	var jsonStr string
	jsonStr, err = Release.ToJson()
	if err != nil {
		fmt.Println(err)
	}
	if pushReleaseJson {
		err = PushReleaseInfo(Release.ReleaseId, githubRepoUrl, "release.json", jsonStr)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	} else {
		fmt.Println("release.json written to dist folder.  Was not pushed to GitHub.")
		ltfile.WriteFile("release.json", jsonStr)
	}
	// tag and push the Doxa code
	err = PushDoxa()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
func PushDoxa() error {
	wd, err := os.Getwd()
	if err != nil {
		return err
	}
	if !ltfile.FileExists("./release.sh") {
		return fmt.Errorf("release.sh not found in %s", wd)
	}
	fmt.Printf("Tagging and pushing Doxa code from %s\n", wd)
	args := []string{Release.ReleaseId}
	cmd := exec.Command("./release.sh", args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {
		return err
	}
	return nil

}
func PushReleaseInfo(version, repoUrl, filename, content string) error {
	// see https://ish-ar.io/tutorial-go-git/
	fs := memfs.New()
	token := os.Getenv("GH_TOKEN")
	if len(token) == 0 {
		return fmt.Errorf("error - environment property GH_TOKEN not set")
	}
	// Authentication
	auth := &http.BasicAuth{
		Username: "doxa",
		Password: token,
	}

	var r *git.Repository
	var err error
	r, err = git.Clone(memory.NewStorage(), fs, &git.CloneOptions{
		URL:  repoUrl,
		Auth: auth,
	})
	if err != nil {
		return fmt.Errorf("error cloning repo %s: %v", repoUrl, err)
	}
	w, err := r.Worktree()
	if err != nil {
		return fmt.Errorf("could not get worktree for cloned release repo")
	}
	// Create new file
	filePath := filename
	newFile, err := fs.Create(filePath)
	if err != nil {
		return fmt.Errorf("could not write file %s to in-memory file system", filePath)
	}
	newFile.Write([]byte(content))
	newFile.Close()

	// Run git status before adding the file to the worktree
	fmt.Println(w.Status())

	// git add $filePath
	w.Add(filePath)

	// Run git status after the file has been added to the worktree
	fmt.Println(w.Status())

	// git commit -m $message
	w.Commit(version, &git.CommitOptions{})

	//Push the code to the remote
	err = r.Push(&git.PushOptions{
		RemoteName: "origin",
		Auth:       auth,
	})
	if err != nil {
		return fmt.Errorf("could not push file %s to %s", filePath, repoUrl)
	}
	fmt.Println("Remote updated.", filePath)
	return nil
}
func PopulateReleaseBinaryMap() error {
	for _, o := range outputs {
		var rb updater.ReleaseBinary
		rb.ARCH = o.GOARCH
		rb.OS = o.GOOS
		rb.ArchiveName = o.Path()
		var err error
		var sha256, archiveFile string
		if sha256, archiveFile, err = o.CheckSumParts(); err != nil {
			return err
		}
		rb.SHA256 = sha256
		rb.ArchiveName = archiveFile
		rb.URL = fmt.Sprintf("https://github.com/liturgiko/doxa/releases/download/%s/%s", Release.ReleaseId, rb.ArchiveName)
		Release.BinaryMap[fmt.Sprintf("%s/%s", o.GOOS, o.GOARCH)] = rb
	}
	return nil
}
func CreateChecksumFile() error {
	os.Chdir(distDir)
	wd, err := os.Getwd()
	if err != nil {
		return fmt.Errorf("error geting working directory: %v", err)
	}
	var checksums []string
	for _, f := range outputs {
		checksums = append(checksums, f.CheckSum)
	}
	err = ltfile.WriteLinesToFile(filepath.Join(wd, checksumFile), checksums)
	if err != nil {
		return err
	}
	return nil
}
func createOutputs(releaseId string) {
	for _, osType := range osTypes {
		for _, archType := range archTypes {
			out := new(output)
			out.GOOS = osType
			out.GOARCH = archType
			out.ReleaseId = releaseId
			outputs = append(outputs, out)
		}
	}
}

var outputs []*output

type output struct {
	CheckSum        string
	ArchiveFileName string
	GOARCH          string
	GOOS            string
	ReleaseId       string
}

func (o *output) CheckSumParts() (string, string, error) {
	parts := strings.Split(o.CheckSum, " - ")
	if len(parts) != 2 {
		return "", "", fmt.Errorf("%s checksum %s not properly formated", o.ArchiveFileName, o.CheckSum)
	}
	return strings.TrimSpace(parts[0]), strings.TrimSpace(parts[1]), nil
}
func (o *output) CreateArchive() error {
	fmt.Println("")
	wd, err := os.Getwd()
	if err != nil {
		return fmt.Errorf("error getting working directory")
	}
	os.Chdir(filepath.Join(wd, "dist"))
	wd, err = os.Getwd()
	if err != nil {
		return fmt.Errorf("error getting working directory")
	}
	in := filepath.Join(o.Path())
	var out string
	var theCmd *exec.Cmd
	if strings.ToLower(o.GOOS) == "windows" {
		out = fmt.Sprintf("%s.zip", in)
		// zip -r archive_name.zip folder_to_compress
		theCmd = exec.Command("zip", "-r", out, in)
	} else {
		out = fmt.Sprintf("%s.tar.gz", in)
		// tar -czvf doxa_"$1"_Darwin_amd64.tar.gz doxa_"$1"_Darwin_amd64/
		theCmd = exec.Command("tar", "-czvf", out, in)
	}
	theCmd.Stdout = os.Stdout
	theCmd.Stderr = os.Stderr
	if err = theCmd.Run(); err != nil {
		return fmt.Errorf("error archiving %s: %v", in, err)
	}
	o.ArchiveFileName = filepath.Base(out)
	o.CheckSum, err = ltfile.Checksum(out)
	if err != nil {
		return err
	}
	return nil
}
func Sha256(path string) string {
	f, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	h := sha256.New()
	if _, err := io.Copy(h, f); err != nil {
		log.Fatal(err)
	}
	return fmt.Sprintf("%x", h.Sum(nil))
}
func Checksum(path string) string {
	return fmt.Sprintf("%s - %s", Sha256(path), filepath.Base(path))
}

func (o *output) CreateBinary(wd string) error {
	out := filepath.Join(wd, "dist", o.Path())
	envArgs := o.Env()
	cmd := exec.Command("go", "build", "-o", out)
	fmt.Printf("\toutput to %s\n", out)
	cmd.Env = os.Environ()
	cmd.Env = append(cmd.Env, envArgs...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	fmt.Println(cmd.String())
	if err := cmd.Run(); err != nil {
		return fmt.Errorf("error building %s: %v", out, err)
	}
	if err := o.CreateArchive(); err != nil {
		return err
	}
	return nil
}
func (o *output) Path() string {
	return fmt.Sprintf("doxa_%s_%s_%s", o.ReleaseId, o.GOOS, o.GOARCH)
}
func (o *output) Env() []string {
	return []string{fmt.Sprintf("GOARCH=%s", o.GOARCH), fmt.Sprintf("GOOS=%s", o.GOOS)}
}
func BuildBinaries() error {
	os.Chdir("../../")
	wd, err := os.Getwd()
	if err != nil {
		return fmt.Errorf("error getting working directory")
	}
	ctx := context.Background()
	g, ctx := errgroup.WithContext(ctx)
	fmt.Printf("building main.go in %s\n", wd)
	for _, o := range outputs {
		o := o // have to shadow o, otherwise it outputs the same GOOS and GOARCH
		g.Go(func() error {
			return o.CreateBinary(wd)
		})
	}
	if err = g.Wait(); err != nil {
		return err
	}
	return nil
}
func CreateDistDirs() error {
	var err error
	ltfile.DeleteDirRecursively(distDir)
	if err = ltfile.CreateDir(distDir); err != nil {
		return fmt.Errorf("error creating %s: %v", distDir, err)
	}
	for _, output := range outputs {
		if err = ltfile.CreateDir(filepath.Join(distDir, output.Path())); err != nil {
			return fmt.Errorf("error creating %s: %v\n", output.Path(), err)
		}
	}
	return nil
}

// ReleaseInfo holds the unmarshalled json release information.
type ReleaseInfo struct {
	Description string
	ReleaseId   string
	ReleaseInfo string
	BinaryMap   map[string]ReleaseBinary
}

// ReleaseBinary holds the unmarshalled json information for an available binary
type ReleaseBinary struct {
	OS     string
	ARCH   string
	URL    string
	SHA256 string
}

func CreateGhRelease() error {
	os.Chdir("../")
	wd, err := os.Getwd()
	if err != nil {
		return fmt.Errorf("error getting working directory: %v", err)
	}
	fmt.Sprintf("%s", wd)
	args := []string{"release", "-R", githubRepoUrl, "create", Release.ReleaseId, filepath.Join("dist", checksumFile)}
	sep := string(filepath.Separator)
	for _, o := range outputs {
		args = append(args, fmt.Sprintf("dist%s%s", sep, o.ArchiveFileName))
	}
	args = append(args, "-n", GetNotes(), "-p", "-t", fmt.Sprintf("DOXA %s", Release.ReleaseId))
	// gh release create v0.0.1 {<file1> <file2> etc.} -F release/notes.md
	cmd := exec.Command("gh", args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {
		return err
	}
	return nil
}
func GetNotes() string {
	sb := strings.Builder{}
	sb.WriteString(tmplPreamble)
	if len(addresses) == 0 {
		sb.WriteString("\nNA")
	} else {
		for _, a := range addresses {
			sb.WriteString(fmt.Sprintf("\n%s", a))
		}
	}
	sb.WriteString("\n")
	sb.WriteString(notesInstructions)
	return sb.String()
}

const tmplPreamble = `
DOXA is a desktop application that provides tools for the creation of Digital Orthodox Akolouthia for use in Eastern Orthodox Christian services. It is downloaded from here.

WARNING: do not download and install DOXA unless you are working directly with the programmers.

THIS IS A PRE-BETA TEST VERSION.

This release:
`
const notesInstructions = `
Binaries are available for Linux, Mac, and Windows.  Note that the binary you download must match both your OS and architecture.  Darwin means Mac OS.  Use AMD unless you have a machine that uses ARM, such as the Mac M series (e.g., M1 chip).  

BEWARE: The Linux binary has not been tested yet on a Linux installation.

## Important Note ##
You only need to follow the instructions here when installing DOXA for the first time.  After that, Doxa will automatically update itself to new versions.

## Generic Installation ##

Beware that the trademark name DOXA is uppercase, but on your computer, the executable file will be lowercase, as will the doxa directory for your work files.

To install DOXA, do the following:

1. Expand the assets (see below).
2. Identify the file you need based on the combination of your operating system darwin (Mac OSX) or windows, and architecture, amd64 or arm64. Note: on a Mac, ARM is the M series chip, e.g. M1.
3. Download the file and expand it. If you know about checksums, the information is available in the checksums.txt file.  Compressed files are provided as both tar.gz and zip.
4. If your home directory does not have a doxa directory, create it, using lowercase doxa as the name.
5. If your doxa directory does not have a bin directory, create it.
6. Copy the doxa program file to {your home directory}/doxa/bin.

Now follow the specific directions for your operating system (i.e. Mac vs Windows).

Note that the first time DOXA runs, it will download and extract a sample liturgical website.  This can take a few minutes. Each time you run Doxa, it will check to see if a newer version is available and if so it will ask you if you want to install it.  

## OS Specific Instructions ##

### On a Mac: ###
1. In Finder, right-click doxa/bin/doxa
2. Click on Open.
3. You will see a message that the developer could not be verified. Click on Open. DOXA will then start up in a terminal window and open a browser tab.
4. When the DOXA browser app opens, click on App, then Quit.
5. Open a terminal window.
6. Type pwd
8. Press the Enter key.
9. Copy or remember what it displays, which will be your home directory
10. Type nano .zshrc
11. Press the Enter key.
12. Add this line to the bottom of the file:
13. export PATH=$PATH:"{your home directory}/doxa/bin"
but replace {your home directory} with what was displayed after the pwd command in the steps 13-15 above.
14. Press the control-o key
15. Type y
16. Press the control-x key
17. Type source .zshrc
18. Type 
        which doxa 
     and press the Enter key. It should show {your home directory}/doxa/bin/doxa. 
19. Once which doxa is displaying correctly the path to doxa/bin correctly, you can always start DOXA by opening a terminal window and typing:
doxa
then press the Enter key.

### On Windows ###

1. Expand the assets folder below.
2. Click on the file you want to download.
3. Open the folder.  If you download a zip, it should extract the file automatically.  If you download a .tar.gz, you will need to use a utility such as 7-Zip to extract it.
4. Copy the doxa executable (.exe) file to your home directory\doxa\bin.
5. Double-click doxa\bin\doxa
6. If a pop up window appears that says, "Windows protected your PC", click on More info, then <em>Run anyway<em>.
7. DOXA will start up in a terminal, then open the DOXA web interface in your browser.
8.  The first time Doxa runs, it will download and extract a sample liturgical website.  This can take a few minutes depending on the speed of your computer.
10. Each time you run DOXA, it will check to see if a newer version is available and if so it will ask you if you want to install it.  Please note that when doxa upgrades itself, it will then automatically quit.  You need to then restart it, and it will be running the new version.
12. For convenience, add the doxa\bin folder to your PATH as follows...
     1. Open the Start Search, type in “env”, and choose “Edit the system environment variables”
     2. Click  “Edit the system environment variables…” button.
     3. Click on “Environment Variables”
     4. In  section in the lower half, find the row with “Path” in the first column. Select it and click edit.
     4. The “Edit environment variable” UI will appear.
     5. Click “New” and type in {your home directory\doxa\bin}, e.g. <em>C:\Users\dave\doxa\bin<em>
     6. Click OK
     7. Click OK
     8. Click OK.
     9. Open Power Shell and type <em>doxa<em>.  If the PATH was set right, DOXA will start up.

Each time DOXA runs, it will check to see if a more recent version is available. If so, it will ask you if you want to install it. After a new version is installed, restart DOXA.
`
