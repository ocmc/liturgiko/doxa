# releaser/main.go

Does the following:

1. Creates binaries for targeted OS and Arch
2. Creates archive files of binaries
3. Creates a checksum.txt file for the archives
4. Updates the remote GitHub and/or GitLab release repositories with a release.json file
5. Creates the release in the GitHub and/or GitLab repositories

## Setup and Usage

You must do the following:

1. Update the release number in doxa/main.go
2. Update the information in release.yaml including:
   - Release description
   - Release ID (version)
   - GitHub and GitLab repository URLs
   - GitLab project ID
   - Release flags (whether to push to GitHub, GitLab, etc.)

3. Set environment variables for authentication:
   - For GitHub: `export GH_TOKEN={github_token_value}`
   - For GitLab: `export GITLAB_TOKEN={gitlab_token_value}`

4. Then, in the releaser directory, run: `go run ./main.go`

## Configuration Options

The release.yaml file supports the following configuration:

```yaml
Description: Description of the release
ReleaseId: v0.x.y
ReleaseInfo: brief info about the release
GithubReleaseUrl: https://github.com/org/repo/releases/download
GithubRepoUrl: https://github.com/org/repo.git
GitlabReleaseUrl: https://gitlab.com/org/group/project/-/releases
GitlabRepoUrl: https://gitlab.com/org/group/project.git
GitlabProjectId: 12345678
PushReleaseJson: true
PushReleaseToGithub: true
PushReleaseToGitlab: true
Addresses:
  - List of items addressed in this release
```

## Testing Releases

For testing purposes, you can:

1. Use the `-test` flag to perform all operations except creating the actual releases:
   ```
   go run ./main.go -test
   ```

2. Configure selective publishing by toggling these flags in release.yaml:
   - `PushReleaseToGithub: false` - Skip GitHub release creation
   - `PushReleaseToGitlab: true` - Only create GitLab release

3. Delete test releases:
   - GitLab: Navigate to the project's Releases page and use the Delete button
   - GitHub: Use `gh release delete <tag-name>` command

## Requirements

- GitHub CLI (`gh`) must be installed for GitHub releases
- go-gitlab library for GitLab integration
- Appropriate authentication tokens with release creation permissions