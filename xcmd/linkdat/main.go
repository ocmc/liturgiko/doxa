package main

import (
	"encoding/json"
	"fmt"
	conllu "github.com/nuvi/go-conllu"
	"io"
	"log"
	"net/http"
	"net/url"
	"strings"
)

// makeUDPipeAPICall makes an HTTP GET request to the UDPipe API with specified parameters.
func makeUDPipeAPICall(data string) {
	baseURL := "http://lindat.mff.cuni.cz/services/udpipe/api/process"
	model := "ancient_greek-perseus-ud-2.12-230717"

	// Construct the URL with query parameters
	queryParams := url.Values{}
	queryParams.Add("model", model)
	queryParams.Add("tokenizer", "")
	queryParams.Add("tagger", "")
	queryParams.Add("parser", "")
	queryParams.Add("data", data)

	// Encode the parameters
	encodedParams := queryParams.Encode()
	fullURL := strings.Join([]string{baseURL, encodedParams}, "?")

	// Make the GET request
	resp, err := http.Get(fullURL)
	if err != nil {
		log.Fatalf("Error making the GET request: %v", err)
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			// TODO
		}
	}(resp.Body)

	// Read and handle the response
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatalf("Error reading the response body: %v", err)
	}
	var result Result
	// Unmarshal the JSON string into the Person struct
	err = json.Unmarshal([]byte(body), &result)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	lines := strings.Split(result.Result, "\n")
	for _, l := range lines {
		fmt.Println(l)
	}
	/*
		# generator = UDPipe 2, https://lindat.mff.cuni.cz/services/udpipe
		# udpipe_model = ancient_greek-perseus-ud-2.12-230717
		# udpipe_model_licence = CC BY-NC-SA
		# newdoc
		# newpar
		# sent_id = 1
		# text = ὁ Θεὸς ἡμῶν, σῶσον τὸν λαόν σου.
		1       ὁ       ὁ       DET     l-s---mn-       Case=Nom|Gender=Masc|Number=Sing        2       det     __
		2       Θεὸς    Θεὸς    NOUN    n-s---mn-       Case=Nom|Gender=Masc|Number=Sing        5       nsubj   __
		3       ἡμῶν    ἐγώ     PRON    p-p---mg-       Case=Gen|Gender=Masc|Number=Plur        2       nmod    _SpaceAfter=No
		4       ,       ,       PUNCT   u--------       _       2       punct   _       _
		5       σῶσον   σῴζω    VERB    v2sama---       Mood=Imp|Number=Sing|Person=2|Tense=Past|VerbForm=Fin|Voice=Act   0       root    _       _
		6       τὸν     ὁ       DET     l-s---ma-       Case=Acc|Gender=Masc|Number=Sing        7       det     __
		7       λαόν    λαός    NOUN    n-s---ma-       Case=Acc|Gender=Masc|Number=Sing        5       obj     __
		8       σου     σύ      PRON    p-s---mg-       Case=Gen|Gender=Masc|Number=Sing        7       nmod    _SpaceAfter=No
		9       .       .       PUNCT   u--------       _       5       punct   _       SpaceAfter=No


	*/
	sentences, err := conllu.Parse(strings.NewReader(result.Result))
	if err != nil {
		log.Fatal(err)
	}

	for _, sentence := range sentences {
		for _, token := range sentence.Tokens {
			fmt.Println(token)
		}
		fmt.Println()
	}
	// Here you would handle the response body as needed, e.g., unmarshal JSON
	log.Printf("Response: %s", body)
}

func main() {
	// Example usage of the function
	makeUDPipeAPICall("ὁ Θεὸς ἡμῶν, σῶσον τὸν λαόν σου.")
}

type Result struct {
	Model            string   `json:"model"`
	Acknowledgements []string `json:"acknowledgements"`
	Result           string   `json:"result"`
}
