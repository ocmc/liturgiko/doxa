package archiver

import "fmt"

func Run(homePath string) error {
	if homePath != "" {
		fmt.Printf("\nCreating project archive. Doxa home path:%s\n", homePath)
	} else {
		return fmt.Errorf("\nCannot create project archive because doxa home path is empty\n")
	}
	return nil
}
