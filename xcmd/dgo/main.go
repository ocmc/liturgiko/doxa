package main

import (
	"fmt"
	"github.com/c-bata/go-prompt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/utils/clinput"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/xcmd/dgo/pkg/archiver"
	"os"
	"path"
	"strconv"
	"strings"
)

const (
	fHome    = "-home"
	fProject = "-project"
)

type paths struct {
	UserHome    string
	DotDoxa     string
	DoxaHome    string
	ProjectsDir string
	ProjectDir  string
	SysDbPath   string
}

var UserPaths *paths
var Projects []string
var DataStores *kvs.Stores

func main() {
	setPaths()
	err := loadStores()
	if err != nil {
		fmt.Printf("%+v\n", err)
	}
	fmt.Printf("\nDoxa Utilities\n\n")
	p := prompt.New(
		executor,
		completer,
		prompt.OptionPrefix("dgo> "),
	)
	executor("help") // Run the help command when the shell starts
	p.Run()
}
func setUserProjectPath(projectPath string) {
	UserPaths.ProjectsDir = path.Join(UserPaths.ProjectsDir, "projects", projectPath)
}
func setPaths() {
	var err error
	UserPaths = new(paths)
	UserPaths.UserHome, err = os.UserHomeDir()
	UserPaths.UserHome = ltfile.ToUnixPath(UserPaths.UserHome)
	if err != nil {
		fmt.Println(err)
	}
	UserPaths.DotDoxa = path.Join(UserPaths.UserHome, ".doxa")
	UserPaths.DoxaHome = path.Join(UserPaths.UserHome, "doxa")
	if !ltfile.DirExists(UserPaths.DoxaHome) {
		UserPaths.DoxaHome = ""
		return
	} else {
		UserPaths.ProjectsDir = path.Join(UserPaths.DoxaHome, "projects")
		if !ltfile.DirExists(UserPaths.ProjectsDir) {
			UserPaths.ProjectsDir = ""
			return
		}
		Projects, err = ltfile.DirsInDir(UserPaths.ProjectsDir, true)
		if err != nil {
			fmt.Printf("\nCould not find projects in %s\n", UserPaths.ProjectsDir)
			return
		}
		UserPaths.SysDbPath = ".doxa/sys/.sys.db"
	}
}
func getProjectSelection() {
	var project string
	var err error
	var i int
	q := "Select project:"
	var o []string
	for i, project = range Projects {
		o = append(o, fmt.Sprintf("%d", i), project)
	}
	o = append(o, "q", "quit")
	input := clinput.GetInput(q, o)
	switch input {
	case 'q':
		os.Exit(0)
	default:
		i, err = strconv.Atoi(string(input))
		if err != nil {
			if len(Projects) > i {

			} else {
				setUserProjectPath(Projects[i])
			}
		}
	}
}
func loadStores() error {
	DataStores = kvs.NewStores()
	var dbPaths []string
	var err error
	if dbPaths, err = getDbPaths(); err != nil {
		return err
	}

	for _, dbPath := range dbPaths {
		// open the Doxa database
		var db *kvs.BoltKVS
		db, err = kvs.NewBoltKVS(dbPath)
		if err != nil { // should not happen, but just in case
			if strings.Contains(err.Error(), "timeout") {
				fmt.Printf("error opening db %s: %v", dbPath, err)
				continue
			}
		}
		// create the database mapper
		var Kvs *kvs.KVS
		Kvs, err = kvs.NewKVS(db)
		if err != nil {
			fmt.Printf("error creating mapper for %s: %v\n", dbPath, err)
			continue
		}
		if Kvs == nil {
			fmt.Printf("Kvs for %s is nil", dbPath)
		}
		subPath := dbPath
		if strings.HasPrefix(subPath, UserPaths.DotDoxa) {
			subPath = strings.TrimPrefix(subPath, UserPaths.UserHome)
		} else if strings.HasPrefix(subPath, UserPaths.ProjectsDir) {
			subPath = strings.TrimPrefix(subPath, UserPaths.ProjectsDir)
		}
		DataStores.Add(&kvs.Store{
			Name:        subPath,
			Description: "",
			Path:        dbPath,
			Prompt:      subPath,
			Kvs:         Kvs,
		})
	}
	return nil
}
func getDbPaths() (dbPaths []string, err error) {
	dbPaths, err = ltfile.FileMatcher(UserPaths.DotDoxa, "db", nil)
	if err != nil {
		return nil, fmt.Errorf("error finding .db files in %s: %v", UserPaths.DotDoxa, err)
	}
	var more []string
	more, err = ltfile.FileMatcher(UserPaths.ProjectsDir, "db", nil)
	if err != nil {
		return nil, fmt.Errorf("error finding .db files in %s: %v", UserPaths.DoxaHome, err)
	}
	if len(more) > 0 {
		dbPaths = append(dbPaths, more...)
	}
	return
}
func executor(in string) {
	in = strings.TrimSpace(in)
	var err error

	switch in {
	case "exit":
		fmt.Println("Exiting shell")
		os.Exit(0)
	case "help":
		fmt.Println("Available commands:")
		fmt.Println("\tarchive: Create an archive of all critical directories within project.")
		fmt.Println("\t\tfHome <path>: Doxa home path")
		fmt.Println("\tdb: Run the db explorer")
		fmt.Println("\texit: Exit the shell")
	case "paths":
		fmt.Println("User home:", UserPaths.UserHome)
		fmt.Println("Doxa home:", UserPaths.DoxaHome)
		fmt.Println("Projects dir:", UserPaths.ProjectsDir)
		fmt.Println("Project dir:", UserPaths.ProjectDir)
	case "db":
	default:
		blocks := strings.Split(in, " ")
		switch blocks[0] {
		case "archive":
			projectPath := ""
			for i := 1; i < len(blocks); i++ {
				switch blocks[i] {
				case fProject:
					if i+1 < len(blocks) {
						projectPath = blocks[i+1]
						i++
						setUserProjectPath(projectPath)
					} else {
						getProjectSelection()
					}
				default:
					fmt.Println("Unknown option", blocks[i])
				}
			}
			if UserPaths.ProjectDir != "" {
				getProjectSelection()
			}
			if UserPaths.ProjectDir != "" {
				err = archiver.Run(UserPaths.ProjectDir)
				if err != nil {
					fmt.Println(err)
				}
			}
		default:
			fmt.Println("Unknown command", blocks[0])
		}
	}
}

func completer(in prompt.Document) []prompt.Suggest {
	suggestions := []prompt.Suggest{
		{Text: "archive", Description: "Create an archive of all critical folders and files within project"},
		{Text: "exit", Description: "Exit the shell"},
		{Text: "help", Description: "Show available commands"},
		{Text: "paths", Description: "Show doxa paths"},
		{Text: "db", Description: "Run db explorer"},
		{Text: "restore", Description: "restore critical folders and files for a project"},
	}
	return prompt.FilterHasPrefix(suggestions, in.GetWordBeforeCursor(), true)
}
