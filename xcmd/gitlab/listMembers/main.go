package main

import (
	"fmt"
	"log"
	"os"

	"github.com/xanzy/go-gitlab"
)

func main() {
	// Read your GitLab token from an environment variable.
	token := os.Getenv("GITLAB_TOKEN")
	if token == "" {
		log.Fatal("GITLAB_TOKEN environment variable not set")
	}

	// Create a new GitLab client.
	client, err := gitlab.NewClient(token)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	// Map from member username to a slice of project clone URLs.
	memberProjects := make(map[string][]string)

	// List groups owned by your account.
	ownedGroups, err := listAllOwnedGroups(client)
	if err != nil {
		log.Fatalf("Failed to list owned groups: %v", err)
	}

	// Process each owned group recursively.
	for _, group := range ownedGroups {
		err = processGroup(client, group, memberProjects)
		if err != nil {
			log.Printf("Error processing group %s: %v", group.FullPath, err)
		}
	}

	// Print the result.
	for member, projects := range memberProjects {
		fmt.Printf("Member: %s\n", member)
		for _, cloneURL := range projects {
			fmt.Printf("  %s\n", cloneURL)
		}
	}
}

// listAllOwnedGroups returns all groups owned by the current user.
func listAllOwnedGroups(client *gitlab.Client) ([]*gitlab.Group, error) {
	var allGroups []*gitlab.Group
	opt := &gitlab.ListGroupsOptions{
		Owned: gitlab.Bool(true),
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	}

	for {
		groups, resp, err := client.Groups.ListGroups(opt)
		if err != nil {
			return nil, err
		}
		allGroups = append(allGroups, groups...)
		if resp.CurrentPage >= resp.TotalPages {
			break
		}
		opt.Page = resp.NextPage
	}
	return allGroups, nil
}

// processGroup processes a group by:
//   - listing its projects and, for each project, adding member info,
//   - recursively processing all subgroups.
func processGroup(client *gitlab.Client, group *gitlab.Group, memberProjects map[string][]string) error {
	// Process projects in the current group.
	projects, err := listAllGroupProjects(client, group.ID)
	if err != nil {
		return err
	}
	for _, project := range projects {
		cloneURL := project.HTTPURLToRepo // using HTTP clone URL; change as needed

		// Get the project members (including inherited ones).
		members, err := listAllProjectMembers(client, project.ID)
		if err != nil {
			log.Printf("Failed to list members for project %s: %v", project.PathWithNamespace, err)
			continue
		}
		for _, member := range members {
			username := member.Username
			if !contains(memberProjects[username], cloneURL) {
				memberProjects[username] = append(memberProjects[username], cloneURL)
			}
		}
	}

	// Recursively process any subgroups.
	subgroups, err := listAllSubgroups(client, group.ID)
	if err != nil {
		return err
	}
	for _, subgroup := range subgroups {
		err = processGroup(client, subgroup, memberProjects)
		if err != nil {
			log.Printf("Error processing subgroup %s: %v", subgroup.FullPath, err)
		}
	}

	return nil
}

// listAllGroupProjects lists all projects for a given group.
func listAllGroupProjects(client *gitlab.Client, groupID int) ([]*gitlab.Project, error) {
	var allProjects []*gitlab.Project
	opt := &gitlab.ListGroupProjectsOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	}

	for {
		projects, resp, err := client.Groups.ListGroupProjects(groupID, opt)
		if err != nil {
			return nil, err
		}
		allProjects = append(allProjects, projects...)
		if resp.CurrentPage >= resp.TotalPages {
			break
		}
		opt.Page = resp.NextPage
	}
	return allProjects, nil
}

// listAllProjectMembers lists all members of a project (including inherited members)
// using the /projects/:id/members/all endpoint.
func listAllProjectMembers(client *gitlab.Client, projectID int) ([]*gitlab.ProjectMember, error) {
	var allMembers []*gitlab.ProjectMember
	opt := &gitlab.ListProjectMembersOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	}

	for {
		members, resp, err := client.ProjectMembers.ListAllProjectMembers(projectID, opt)
		if err != nil {
			return nil, err
		}
		allMembers = append(allMembers, members...)
		if resp.CurrentPage >= resp.TotalPages {
			break
		}
		opt.Page = resp.NextPage
	}
	return allMembers, nil
}

// listAllSubgroups lists all direct subgroups of a group.
func listAllSubgroups(client *gitlab.Client, groupID int) ([]*gitlab.Group, error) {
	var allSubgroups []*gitlab.Group
	opt := &gitlab.ListSubGroupsOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	}

	for {
		subgroups, resp, err := client.Groups.ListSubGroups(groupID, opt)
		if err != nil {
			return nil, err
		}
		allSubgroups = append(allSubgroups, subgroups...)
		if resp.CurrentPage >= resp.TotalPages {
			break
		}
		opt.Page = resp.NextPage
	}
	return allSubgroups, nil
}

// contains returns true if slice contains the given string.
func contains(slice []string, str string) bool {
	for _, s := range slice {
		if s == str {
			return true
		}
	}
	return false
}
