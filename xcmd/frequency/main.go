package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"sort"
	"strings"
)

func main() {
	// Replace 'your_file_path_here.txt' with the path to your text file
	filePath := "en_us_chrys_li.txt"
	stats := &Stats{}
	err := stats.CountWordsInFile(filePath)
	if err != nil {
		fmt.Println("Error counting words:", err)
		return
	}
	stats.SortKeys()

	err = stats.Write("output.txt")
	if err != nil {
		fmt.Println("Error writing to file:", err)
		return
	}
}

type Stats struct {
	WordCount  map[string]int
	SortedKeys []string
}

func (s *Stats) Write(filename string) error {
	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()
	for _, key := range s.SortedKeys {
		_, err = fmt.Fprintf(file, "%s: %d\n", key, s.WordCount[key])
		if err != nil {
			return err
		}
	}
	return nil
}
func (s *Stats) SortKeys() {
	s.SortedKeys = make([]string, 0, len(s.WordCount))
	for key := range s.WordCount {
		s.SortedKeys = append(s.SortedKeys, key)
	}
	sort.Strings(s.SortedKeys)
}
func (s *Stats) CountWordsInFile(filePath string) error {
	// Open the file
	file, err := os.Open(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	// Create a new Scanner for the file
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanWords)

	// Regular expression to match words (alphanumeric characters)
	wordRegex := regexp.MustCompile(`^\w+$`)

	// Map to store word counts
	s.WordCount = make(map[string]int)

	// Scan all words in the file
	for scanner.Scan() {
		word := scanner.Text()
		word = strings.ToLower(word) // Convert to lowercase

		// Check if the scanned text is a word
		if wordRegex.MatchString(word) {
			s.WordCount[word]++
		}
	}

	if err := scanner.Err(); err != nil {
		return err
	}

	return nil
}
