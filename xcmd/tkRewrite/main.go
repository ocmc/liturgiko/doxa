package main

import (
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/nlp/ed"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"os"
	"path"
	"strings"
)

/**
The purpose of this program is to compare all
the topic-keys in a given library against
the en_us_public library and attempt to
detect ones that have been renamed by Fr Seraphim.
The goal is to use this information to rename topic-keys
in libraries that use the older names for topic-keys
so that they use the same names as Fr. S.
*/

func main() {
	// paths
	exports := "/volumes/macWdb/doxa/projects/doxa-alwb/exports/ltx"
	//imports := "/volumes/macWdb/doxa/projects/doxa-alwb/imports"

	// libraries
	public := path.Join(exports, "en_us_public.tsv") // library with renames
	target := path.Join(exports, "swh_ke_oak.tsv")   // library without renames

	// load the public map.
	// this has the recent topic:key values from Fr Seraphim
	publicMap, err := ToMap(public)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("%d\n", len(publicMap))

	// load the target map
	targetMap, err := ToMap(target)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("%d\n", len(targetMap))

	// for each topic and set of keys
	// create a map where the topic is the
	// key and the values are tuples.
	// The tuples contain the target key
	// and what the algorithm suggests
	// is the result of it being renamed
	// by Fr. Seraphim.
	// If it was not renamed, the two keys
	// will be identical.
	// If they are identical, we ignore them.
	changesMap := make(map[string][]*ed.Tuple)
	for k, v := range publicMap {
		var ok bool
		var targetKeys []string
		if targetKeys, ok = targetMap[k]; ok {
			var candidates []*ed.Tuple
			candidates, err = ed.Match(targetKeys, v)
			if err != nil {
				fmt.Printf("%s: %v\n", k, err)
				os.Exit(1)
			}
			var changes []*ed.Tuple
			for _, t := range candidates {
				if t.Changed() {
					t.Similarity = ed.Similarity2(t.S1, t.S2)
					changes = append(changes, t)
				}
			}
			if len(changes) > 0 {
				changesMap[k] = changes
			}
		}
	}
	fmt.Printf("%d", len(changesMap))
	// write the possible changes to a tab delimited file
	// for examination
	var tupleLines = []string{"From\tTo\tSimilarity"}
	for t, v := range changesMap {
		for _, k := range v {
			tupleLines = append(tupleLines, fmt.Sprintf("%s:%s\t%s:%s\t%f", t, k.S1, t, k.S2, k.Similarity))
		}
	}
	err = ltfile.WriteLinesToFile(path.Join(exports, "tkChanges.tsv"), tupleLines)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
func ToMap(p string) (theMap map[string][]string, err error) {
	theMap = make(map[string][]string)
	lines, err := ltfile.GetFileLines(p)
	if err != nil {
		fmt.Println(err)
		return nil, fmt.Errorf("error getting lines from %s: %v", p, err)
	}
	for _, l := range lines {
		var t, k string
		var keys []string
		t, k, err = tk(l)
		if err != nil {
			return nil, fmt.Errorf("%s: %v", l, err)
		}
		keys, _ = theMap[t]
		keys = append(keys, k)
		theMap[t] = keys
	}
	return theMap, err
}
func tk(l string) (t, k string, err error) {
	parts := strings.Split(l, "\t")
	if len(parts) != 2 {
		if len(parts) == 1 {
			return "", "", fmt.Errorf("can't split")
		}
		parts[1] = strings.Join(parts[1:], " ")
	}
	kp := kvs.NewKeyPath()
	err = kp.ParsePath(parts[0])
	if err != nil {
		return "", "", fmt.Errorf("error parsing path %s: %v", parts[0], err)
	}
	return kp.Dirs.Last(), kp.KeyParts.First(), nil
}
