package main

import (
	"fmt"
	"golang.org/x/text/unicode/norm"
	"regexp"
)

const (
	IgnoreCase = `(?i)`
	AnyUnicode = `\p{L}(\p{M})*`
	AnyAlpha   = `\x{03B1}(\p{M})*`
	AnyEpsilon = `\x{03B5}(\p{M})*`
	AnyEta     = `\x{03B7}(\p{M})*`
	AnyIota    = `\x{03B9}(\p{M})*`
	AnyUpsilon = `\x{03C5}(\p{M})*`
	AnyOmicron = `\x{03BF}(\p{M})*`
	AnyRho     = `\x{03C1}(\p{M})*`
	AnyOmega   = `\x{03C9}(\p{M})*`
)

func main() {
	//	r := regexp.MustCompile(AnyUnicode) // any unicode and composing
	//r := regexp.MustCompile(AnyAlpha) // any unicode and composing
	//r := regexp.MustCompile(AnyUpsilon) // any unicode and composing
	//r := regexp.MustCompile(AnyEpsilon) // any unicode and composing
	//r := regexp.MustCompile(AnyEta) // any unicode and composing
	//r := regexp.MustCompile(AnyOmicron) // any unicode and composing
	//r := regexp.MustCompile(AnyRho) // any unicode and composing
	//	r := regexp.MustCompile(AnyOmega) // any unicode and composing
	r := regexp.MustCompile(`τ` + AnyOmega) // any unicode and composing
	//r := regexp.MustCompile(AnyIota) // any unicode and composing
	//	r := regexp.MustCompile(`\x{03B1}(\p{M})*`)
	//	r := regexp.MustCompile(`(?:\x{03B1}\p{M})+`)
	//	r := regexp.MustCompile(`[\x{1F00}-\x{1F07}]`)
	//	s := norm.NFD.String("ἀγάπηαᾶ")
	//	s := norm.NFD.String("ἈἀαᾶὶῖἰίἴυύὐεἐἐὲἔΕηἠήἦοὀόὸὁρῤῶὼὤῳ")
	s := norm.NFD.String("Μετεμορφώθης ἐν τῷ ὄρει Χριστὲ ὁ Θεός, δείξας τοῖς Μαθηταῖς σου τὴν δόξαν σου, καθὼς ἠδύναντο. Λάμψον καὶ ἡμῖν τοῖς ἁμαρτωλοῖς, τὸ φῶς σου τὸ ἀΐδιον, πρεσβείαις τῆς Θεοτόκου, φωτοδότα δόξα σοι.")
	//	s := norm.NFD.String("Ἀδελφοί, ἡ ἀγάπη μακροθυμεῖ, χρηστεύεται· ἡ ἀγάπη, οὐ ζηλοῖ,…")
	matches := r.FindAllString(s, -1)
	for _, m := range matches {
		fmt.Printf("%s\n", m)
	}
}
