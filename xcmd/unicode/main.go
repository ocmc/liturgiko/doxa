package main

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"github.com/liturgiko/doxa/pkg/utils/queryw"
	"golang.org/x/text/unicode/norm"
	"os"
	"sort"
	"strings"
	"text/template"
	"unicode"
)

func main() {
	theUrl := "https://www.compart.com/en/unicode/block/U+1F00"
	// Request the HTML page.
	/**
	<a href="/en/unicode/U+1F01" class="content-item card">
		<p class="uid">U+1F01</p>
		<div class="text">ἁ</div>
	    <div class="name">
	      <p>Greek Small Letter Alpha with Dasia</p>
	    </div>
	</a>
	*/
	theSelector := "a.content-item"
	sel, err := queryw.Children(theUrl, theSelector)
	if err != nil {
		return
	}
	var letters UniMeta
	var general IndexMap
	general = make(map[SetKey][]int)
	var graphemeMap = make(map[string]int)

	sel.Each(func(i int, s *goquery.Selection) {
		uid := s.Find(".uid").Text()
		text := s.Find(".text").Text()
		name := s.Find(".name").Text()
		gu := new(GreekUnicode)
		gu.UniCode = uid
		gu.Grapheme = norm.NFC.String(text)
		gu.Base = ltstring.ToNnp(text)
		gu.IsVowel = unicode.IsLetter(rune(gu.Base[0])) && strings.Contains("αειουωη", gu.Base)
		gu.IsConsonant = unicode.IsLetter(rune(gu.Base[0])) && !gu.IsVowel
		gu.Description = name
		gu.IsLower = strings.Contains(name, "Small Letter")
		gu.IsUpper = strings.Contains(name, "Capital Letter")
		gu.HasDiacritics = strings.Contains(name, "Letter") && strings.Contains(name, "with")
		gu.HasVaria = strings.Contains(name, "Varia")
		gu.HasPerispomeni = strings.Contains(name, "Perispomeni")
		gu.HasYpogegrammeni = strings.Contains(name, "Ypogegrammeni")
		gu.HasProsgegrammeni = strings.Contains(name, "Prosgegrammeni")
		gu.HasVrachy = strings.Contains(name, "Vrachy")
		gu.HasMacron = strings.Contains(name, "Macron")
		gu.HasDialytika = strings.Contains(name, "Dialytika")
		gu.HasPsili = strings.Contains(name, "Psili")
		gu.HasDasia = strings.Contains(name, "Dasia")
		gu.HasOxia = strings.Contains(name, "Oxia")
		gu.HasDiacritics = gu.HasVaria ||
			gu.HasPerispomeni ||
			gu.HasYpogegrammeni ||
			gu.HasProsgegrammeni ||
			gu.HasVrachy ||
			gu.HasMacron ||
			gu.HasDialytika ||
			gu.HasPsili ||
			gu.HasDasia ||
			gu.HasOxia
		letters = append(letters, gu)
		graphemeMap[gu.Grapheme] = i
	})
	sort.Sort(letters)
	byUnicode := make(map[string]int)
	byLetter := make(map[string]int)
	for i, l := range letters {
		byUnicode[l.UniCode] = i
		byLetter[l.Grapheme] = i
		parts := strings.Split(l.Description, " with ")
		if len(parts) == 2 {
			d := parts[1]
			d = strings.TrimSpace(strings.ReplaceAll(d, " and ", ""))
			var v SetKey
			switch d {
			case "Dasia":
				v = Dasia
			case "DasiaOxia":
				v = DasiaOxia
			case "DasiaOxiaProsgegrammeni":
				v = DasiaOxiaProsgegrammeni
			case "DasiaOxiaYpogegrammeni":
				v = DasiaOxiaYpogegrammeni
			case "DasiaPerispomeni":
				v = DasiaPerispomeni
			case "DasiaPerispomeniProsgegrammeni":
				v = DasiaPerispomeniProsgegrammeni
			case "DasiaPerispomeniYpogegrammeni":
				v = DasiaPerispomeniYpogegrammeni
			case "DasiaProsgegrammeni":
				v = DasiaProsgegrammeni
			case "DasiaVaria":
				v = DasiaVaria
			case "DasiaVariaProsgegrammeni":
				v = DasiaVariaProsgegrammeni
			case "DasiaVariaYpogegrammeni":
				v = DasiaVariaYpogegrammeni
			case "DasiaYpogegrammeni":
				v = DasiaYpogegrammeni
			case "DialytikaOxia":
				v = DialytikaOxia
			case "DialytikaPerispomeni":
				v = DialytikaPerispomeni
			case "DialytikaVaria":
				v = DialytikaVaria
			case "Macron":
				v = Macron
			case "Oxia":
				v = Oxia
			case "OxiaYpogegrammeni":
				v = OxiaYpogegrammeni
			case "Perispomeni":
				v = Perispomeni
			case "PerispomeniYpogegrammeni":
				v = PerispomeniYpogegrammeni
			case "Prosgegrammeni":
				v = Prosgegrammeni
			case "Psili":
				v = Psili
			case "PsiliOxia":
				v = PsiliOxia
			case "PsiliOxiaProsgegrammeni":
				v = PsiliOxiaProsgegrammeni
			case "PsiliOxiaYpogegrammeni":
				v = PsiliOxiaYpogegrammeni
			case "PsiliPerispomeni":
				v = PsiliPerispomeni
			case "PsiliPerispomeniProsgegrammeni":
				v = PsiliPerispomeni
			case "PsiliPerispomeniYpogegrammeni":
				v = PsiliPerispomeni
			case "PsiliProsgegrammeni":
				v = PsiliProsgegrammeni
			case "PsiliVaria":
				v = PsiliVaria
			case "PsiliVariaProsgegrammeni":
				v = PsiliVariaProsgegrammeni
			case "PsiliVariaYpogegrammeni":
				v = PsiliVariaYpogegrammeni
			case "PsiliYpogegrammeni":
				v = PsiliYpogegrammeni
			case "Varia":
				v = Varia
			case "VariaYpogegrammeni":
				v = VariaYpogegrammeni
			case "Vrachy":
				v = Vrachy
			case "Ypogegrammeni":
				v = Ypogegrammeni
			}
			general.Add(v, i)
			general.Add(Diacritics, i)
			switch l.Base {
			case "α":
				general.Add(Alpha, i)
				if l.IsUpper {
					general.Add(AlphaUpper, i)
				} else if l.IsLower {
					general.Add(AlphaLower, i)
				}
				if l.HasDiacritics {
					general.Add(AlphaDiacritics, i)
				}
			case "ε":
				general.Add(Epsilon, i)
				if l.IsUpper {
					general.Add(EpsilonUpper, i)
				} else if l.IsLower {
					general.Add(EpsilonLower, i)
				}
				if l.HasDiacritics {
					general.Add(EpsilonDiacritics, i)
				}
			case "η":
				general.Add(Eta, i)
				if l.IsUpper {
					general.Add(EtaUpper, i)
				} else if l.IsLower {
					general.Add(EtaLower, i)
				}
				if l.HasDiacritics {
					general.Add(EtaDiacritics, i)
				}
			case "ι":
				general.Add(Iota, i)
				if l.IsUpper {
					general.Add(IotaUpper, i)
				} else if l.IsLower {
					general.Add(IotaLower, i)
				}
				if l.HasDiacritics {
					general.Add(IotaDiacritics, i)
				}
			case "ο":
				general.Add(Omicron, i)
				if l.IsUpper {
					general.Add(OmicronUpper, i)
				} else if l.IsLower {
					general.Add(OmicronLower, i)
				}
				if l.HasDiacritics {
					general.Add(OmicronDiacritics, i)
				}
			case "υ":
				general.Add(Upsilon, i)
				if l.IsUpper {
					general.Add(UpsilonUpper, i)
				} else if l.IsLower {
					general.Add(UpsilonLower, i)
				}
				if l.HasDiacritics {
					general.Add(UpsilonDiacritics, i)
				}
			case "ω":
				general.Add(Omega, i)
				if l.IsUpper {
					general.Add(OmegaUpper, i)
				} else if l.IsLower {
					general.Add(OmegaLower, i)
				}
				if l.HasDiacritics {
					general.Add(OmegaDiacritics, i)
				}
			case "ρ":
				general.Add(Rho, i)
				if l.IsUpper {
					general.Add(RhoUpper, i)
				} else if l.IsLower {
					general.Add(RhoLower, i)
				}
				if l.HasDiacritics {
					general.Add(RhoDiacritics, i)
				}
			}
			if l.IsConsonant {
				general.Add(Consonants, i)
			} else if l.IsVowel {
				general.Add(Vowels, i)
			}
		}
	}
	// TODO: add byUnicode and byLetter to template data
	u := letters[byUnicode["U+1FFC"]]
	g := letters[byLetter["ῼ"]]
	fmt.Printf("u.UniCode: %s\n", u.UniCode)
	fmt.Printf("g.UniCode: %s\n", g.UniCode)

	var tData TemplateData
	tData.Letters = letters
	tData.General = general
	tData.Graphemes = graphemeMap

	t1 := template.New("t1")
	t1, err = t1.Parse(UniTemplate)
	if err != nil {
		panic(err)
	}
	file, err := os.Create("./greek.go")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			fmt.Println(err)
		}
	}(file)
	err = t1.Execute(file, tData)
	if err != nil {
		fmt.Println(err)
		return
	}
}

type TemplateData struct {
	Letters    UniMeta
	General    map[SetKey][]int
	Graphemes  map[string]int
	Consonants []string
	Vowels     []string
	Alpha      []string
	Epsilon    []string
	Eta        []string
	Iota       []string
	Upsilon    []string
	Rho        []string
	Omega      []string
}
type IndexMap map[SetKey][]int

func (i IndexMap) Add(k SetKey, v int) {
	var indexes []int
	var found bool
	if indexes, found = i[k]; !found {
	}
	indexes = append(indexes, v)
	i[k] = indexes
}

var SetNames = []string{"Alpha",
	"AlphaDiacritics",
	"AlphaLower",
	"AlphaUpper",
	"Consonants",
	"Dasia",
	"DasiaOxia",
	"DasiaOxiaProsgegrammeni",
	"DasiaOxiaYpogegrammeni",
	"DasiaPerispomeni",
	"DasiaPerispomeniProsgegrammeni",
	"DasiaPerispomeniYpogegrammeni",
	"DasiaProsgegrammeni",
	"DasiaVaria",
	"DasiaVariaProsgegrammeni",
	"DasiaVariaYpogegrammeni",
	"DasiaYpogegrammeni",
	"Diacritics",
	"DialytikaOxia",
	"DialytikaPerispomeni",
	"DialytikaVaria",
	"Epsilon",
	"EpsilonDiacritics",
	"EpsilonLower",
	"EpsilonUpper",
	"Eta",
	"EtaDiacritics",
	"EtaLower",
	"EtaUpper",
	"Iota",
	"IotaDiacritics",
	"IotaLower",
	"IotaUpper",
	"Macron",
	"Omega",
	"OmegaDiacritics",
	"OmegaLower",
	"OmegaUpper",
	"Omicron",
	"OmicronDiacritics",
	"OmicronLower",
	"OmicronUpper",
	"Oxia",
	"OxiaYpogegrammeni",
	"Perispomeni",
	"PerispomeniYpogegrammeni",
	"Prosgegrammeni",
	"Psili",
	"PsiliOxia",
	"PsiliOxiaProsgegrammeni",
	"PsiliOxiaYpogegrammeni",
	"PsiliPerispomeni",
	"PsiliPerispomeniProsgegrammeni",
	"PsiliPerispomeniYpogegrammeni",
	"PsiliProsgegrammeni",
	"PsiliVaria",
	"PsiliVariaProsgegrammeni",
	"PsiliVariaYpogegrammeni",
	"PsiliYpogegrammeni",
	"Rho",
	"RhoDiacritics",
	"RhoLower",
	"RhoUpper",
	"Upsilon",
	"UpsilonDiacritics",
	"UpsilonLower",
	"UpsilonUpper",
	"Varia",
	"VariaYpogegrammeni",
	"Vowels",
	"Vrachy",
	"Ypogegrammeni",
}

type SetKey int

const (
	Alpha SetKey = iota
	AlphaDiacritics
	AlphaLower
	AlphaUpper
	Consonants
	Dasia
	DasiaOxia
	DasiaOxiaProsgegrammeni
	DasiaOxiaYpogegrammeni
	DasiaPerispomeni
	DasiaPerispomeniProsgegrammeni
	DasiaPerispomeniYpogegrammeni
	DasiaProsgegrammeni
	DasiaVaria
	DasiaVariaProsgegrammeni
	DasiaVariaYpogegrammeni
	DasiaYpogegrammeni
	Diacritics
	DialytikaOxia
	DialytikaPerispomeni
	DialytikaVaria
	Epsilon
	EpsilonDiacritics
	EpsilonLower
	EpsilonUpper
	Eta
	EtaDiacritics
	EtaLower
	EtaUpper
	Iota
	IotaDiacritics
	IotaLower
	IotaUpper
	Macron
	Omega
	OmegaDiacritics
	OmegaLower
	OmegaUpper
	Omicron
	OmicronDiacritics
	OmicronLower
	OmicronUpper
	Oxia
	OxiaYpogegrammeni
	Perispomeni
	PerispomeniYpogegrammeni
	Prosgegrammeni
	Psili
	PsiliOxia
	PsiliOxiaProsgegrammeni
	PsiliOxiaYpogegrammeni
	PsiliPerispomeni
	PsiliPerispomeniProsgegrammeni
	PsiliPerispomeniYpogegrammeni
	PsiliProsgegrammeni
	PsiliVaria
	PsiliVariaProsgegrammeni
	PsiliVariaYpogegrammeni
	PsiliYpogegrammeni
	Rho
	RhoDiacritics
	RhoLower
	RhoUpper
	Upsilon
	UpsilonDiacritics
	UpsilonLower
	UpsilonUpper
	Varia
	VariaYpogegrammeni
	Vowels
	Vrachy
	Ypogegrammeni
)

func Name(s SetKey) string {
	switch s {
	case Alpha:
		return "Alpha"
	case AlphaDiacritics:
		return "AlphaDiacritics"
	case AlphaLower:
		return "AlphaLower"
	case AlphaUpper:
		return "AlphaUpper"
	case Consonants:
		return "Consonants"
	case Dasia:
		return "Dasia"
	case DasiaOxia:
		return "DasiaOxia"
	case DasiaOxiaProsgegrammeni:
		return "DasiaOxiaProsgegrammeni"
	case DasiaOxiaYpogegrammeni:
		return "DasiaOxiaYpogegrammeni"
	case DasiaPerispomeni:
		return "DasiaPerispomeni"
	case DasiaPerispomeniProsgegrammeni:
		return "DasiaPerispomeniProsgegrammeni"
	case DasiaPerispomeniYpogegrammeni:
		return "DasiaPerispomeniYpogegrammeni"
	case DasiaProsgegrammeni:
		return "DasiaProsgegrammeni"
	case DasiaVaria:
		return "DasiaVaria"
	case DasiaVariaProsgegrammeni:
		return "DasiaVariaProsgegrammeni"
	case DasiaVariaYpogegrammeni:
		return "DasiaVariaYpogegrammeni"
	case DasiaYpogegrammeni:
		return "DasiaYpogegrammeni"
	case Diacritics:
		return "Diacritics"
	case DialytikaOxia:
		return "DialytikaOxia"
	case DialytikaPerispomeni:
		return "DialytikaPerispomeni"
	case DialytikaVaria:
		return "DialytikaVaria"
	case Epsilon:
		return "Epsilon"
	case EpsilonDiacritics:
		return "EpsilonDiacritics"
	case EpsilonLower:
		return "EpsilonLower"
	case EpsilonUpper:
		return "EpsilonUpper"
	case Eta:
		return "Eta"
	case EtaDiacritics:
		return "EtaDiacritics"
	case EtaLower:
		return "EtaLower"
	case EtaUpper:
		return "EtaUpper"
	case Iota:
		return "Iota"
	case IotaDiacritics:
		return "IotaDiacritics"
	case IotaLower:
		return "IotaLower"
	case IotaUpper:
		return "IotaUpper"
	case Macron:
		return "Macron"
	case Omega:
		return "Omega"
	case OmegaDiacritics:
		return "OmegaDiacritics"
	case OmegaLower:
		return "OmegaLower"
	case OmegaUpper:
		return "OmegaUpper"
	case Omicron:
		return "Omicron"
	case OmicronDiacritics:
		return "OmicronDiacritics"
	case OmicronLower:
		return "OmicronLower"
	case OmicronUpper:
		return "OmicronUpper"
	case Oxia:
		return "Oxia"
	case OxiaYpogegrammeni:
		return "OxiaYpogegrammeni"
	case Perispomeni:
		return "Perispomeni"
	case PerispomeniYpogegrammeni:
		return "PerispomeniYpogegrammeni"
	case Prosgegrammeni:
		return "Prosgegrammeni"
	case Psili:
		return "Psili"
	case PsiliOxia:
		return "PsiliOxia"
	case PsiliOxiaProsgegrammeni:
		return "PsiliOxiaProsgegrammeni"
	case PsiliOxiaYpogegrammeni:
		return "PsiliOxiaYpogegrammeni"
	case PsiliPerispomeni:
		return "PsiliPerispomeni"
	case PsiliPerispomeniProsgegrammeni:
		return "PsiliPerispomeniProsgegrammeni"
	case PsiliPerispomeniYpogegrammeni:
		return "PsiliPerispomeniYpogegrammeni"
	case PsiliProsgegrammeni:
		return "PsiliProsgegrammeni"
	case PsiliVaria:
		return "PsiliVaria"
	case PsiliVariaProsgegrammeni:
		return "PsiliVariaProsgegrammeni"
	case PsiliVariaYpogegrammeni:
		return "PsiliVariaYpogegrammeni"
	case PsiliYpogegrammeni:
		return "PsiliYpogegrammeni"
	case Rho:
		return "Rho"
	case RhoDiacritics:
		return "RhoDiacritics"
	case RhoLower:
		return "RhoLower"
	case RhoUpper:
		return "RhoUpper"
	case Upsilon:
		return "Upsilon"
	case UpsilonDiacritics:
		return "UpsilonDiacritics"
	case UpsilonLower:
		return "UpsilonLower"
	case UpsilonUpper:
		return "UpsilonUpper"
	case Varia:
		return "Varia"
	case VariaYpogegrammeni:
		return "VariaYpogegrammeni"
	case Vowels:
		return "Vowels"
	case Vrachy:
		return "Vrachy"
	case Ypogegrammeni:
		return "Ypogegrammeni"
	default:
		return ""
	}
}

type UniMeta []*GreekUnicode

func (e UniMeta) Len() int {
	return len(e)
}

func (e UniMeta) Less(i, j int) bool {
	return e[i].UniCode > e[j].UniCode
}

func (e UniMeta) Swap(i, j int) {
	e[i], e[j] = e[j], e[i]
}

type GreekUnicode struct {
	UniCode           string
	Grapheme          string
	Base              string
	IsConsonant       bool
	IsPunctuation     bool
	IsVowel           bool
	IsLower           bool
	IsUpper           bool
	HasDiacritics     bool
	HasVaria          bool
	HasPerispomeni    bool
	HasYpogegrammeni  bool
	HasProsgegrammeni bool
	HasVrachy         bool
	HasMacron         bool
	HasDialytika      bool
	HasPsili          bool
	HasDasia          bool
	HasOxia           bool
	Description       string
}

/*
   1. var CodePoints []GreekUnicode
   2. lookup maps
      var ByUnicode map[string]int  <- value is index into CodePoints
      var ByLetter map[string]int <-- value is index into CodePoints
*/

const UniTemplate = `package main

type GreekUnicode struct {
	UniCode           string
	Grapheme          string
	Base              string
	IsConsonant       bool
	IsPunctuation     bool
	IsVowel           bool
	IsLower           bool
	IsUpper           bool
	HasDiacritics     bool
	HasVaria          bool
	HasPerispomeni    bool
	HasYpogegrammeni  bool
	HasProsgegrammeni bool
	HasVrachy         bool
	HasMacron         bool
	HasDialytika      bool
	HasPsili          bool
	HasDasia          bool
	HasOxia           bool
	Description       string
}
// LetterMap Contains attributes for each code point from 
// Unicode block U+1F00.  Extracted from https://www.compart.com/en/unicode/block/U+1F00
// Use this map in conjunction with 
var LetterMap = map[int]GreekUnicode{
{{ range $k, $v := .Letters -}}
   {{$k}}: GreekUnicode{
     UniCode: "{{$v.UniCode}}",
     Grapheme: "{{$v.Grapheme}}", 
     Base: "{{$v.Base}}", 
     IsConsonant: {{$v.IsConsonant}}, 
     IsPunctuation: {{$v.IsPunctuation}}, 
     IsVowel: {{$v.IsVowel}}, 
     IsLower: {{$v.IsLower}}, 
     IsUpper: {{$v.IsUpper}}, 
     HasDiacritics: {{$v.HasDiacritics}}, 
     HasVaria: {{$v.HasVaria}}, 
     HasPerispomeni: {{$v.HasPerispomeni}}, 
     HasYpogegrammeni: {{$v.HasYpogegrammeni}}, 
     HasProsgegrammeni: {{$v.HasProsgegrammeni}}, 
     HasVrachy: {{$v.HasVrachy}}, 
     HasMacron: {{$v.HasMacron}}, 
     HasDialytika: {{$v.HasDialytika}}, 
     HasPsili: {{$v.HasPsili}}, 
     HasDasia: {{$v.HasDasia}}, 
     HasOxia: {{$v.HasOxia}}, 
     Description: "{{$v.Description}}",
   },
{{ end }}
}
// SetMap provides keys to use in the LetterMap.  
// The keys for SetMap are int constants of type SetKey.
// The values are sets of letters, e.g. 
// the set of all consonants, or set of all vowels.
// For example, to find all entries in LetterMap that are
// consonants: 
// indexes := LookupMap[Consonants]
// for _, i := range indexes {
//    c := LetterMap[i]
// }
var SetMap = map[SetKey][]int{ {{ range $k, $v := .General -}}
  {{$k}}: []int{ {{ range $v }} {{.}}, {{ end }} },
{{ end }}
}

// GraphemeMap The keys for GraphemeMap are unicode graphemes, 
// that is, the letter as it displays for end users.
// The keys in this map are in unicode normal form c.
// Normalize your key before using it with the map, e.g.
// k := "ά" // your key
// k = norm.NFC.String(k) // unicode normal form c
// i := GraphemeMap[k] // lookup the int key
// l := LetterMap[i] // use the int key to get the Letter info.
var GraphemeMap = map[string]int{ {{ range $k, $v := .Graphemes -}}
  "{{$k}}": {{$v}},
{{ end }}
}

// SetKey int constants as keys for the SetMap
type SetKey int
const (
	Alpha SetKey = iota
	AlphaDiacritics
	AlphaLower
	AlphaUpper
	Consonants
	Dasia
	DasiaOxia
	DasiaOxiaProsgegrammeni
	DasiaOxiaYpogegrammeni
	DasiaPerispomeni
	DasiaPerispomeniProsgegrammeni
	DasiaPerispomeniYpogegrammeni
	DasiaProsgegrammeni
	DasiaVaria
	DasiaVariaProsgegrammeni
	DasiaVariaYpogegrammeni
	DasiaYpogegrammeni
	Diacritics
	DialytikaOxia
	DialytikaPerispomeni
	DialytikaVaria
	Epsilon
	EpsilonDiacritics
	EpsilonLower
	EpsilonUpper
	Eta
	EtaDiacritics
	EtaLower
	EtaUpper
	Iota
	IotaDiacritics
	IotaLower
	IotaUpper
	Macron
	Omega
	OmegaDiacritics
	OmegaLower
	OmegaUpper
	Omicron
	OmicronDiacritics
	OmicronLower
	OmicronUpper
	Oxia
	OxiaYpogegrammeni
	Perispomeni
	PerispomeniYpogegrammeni
	Prosgegrammeni
	Psili
	PsiliOxia
	PsiliOxiaProsgegrammeni
	PsiliOxiaYpogegrammeni
	PsiliPerispomeni
	PsiliPerispomeniProsgegrammeni
	PsiliPerispomeniYpogegrammeni
	PsiliProsgegrammeni
	PsiliVaria
	PsiliVariaProsgegrammeni
	PsiliVariaYpogegrammeni
	PsiliYpogegrammeni
	Rho
	RhoDiacritics
	RhoLower
	RhoUpper
	Upsilon
	UpsilonDiacritics
	UpsilonLower
	UpsilonUpper
	Varia
	VariaYpogegrammeni
	Vowels
	Vrachy
	Ypogegrammeni
)

func Name(s SetKey) string {
	switch s {
	case Alpha:
		return "Alpha"
	case AlphaDiacritics:
		return "AlphaDiacritics"
	case AlphaLower:
		return "AlphaLower"
	case AlphaUpper:
		return "AlphaUpper"
	case Consonants:
		return "Consonants"
	case Dasia:
		return "Dasia"
	case DasiaOxia:
		return "DasiaOxia"
	case DasiaOxiaProsgegrammeni:
		return "DasiaOxiaProsgegrammeni"
	case DasiaOxiaYpogegrammeni:
		return "DasiaOxiaYpogegrammeni"
	case DasiaPerispomeni:
		return "DasiaPerispomeni"
	case DasiaPerispomeniProsgegrammeni:
		return "DasiaPerispomeniProsgegrammeni"
	case DasiaPerispomeniYpogegrammeni:
		return "DasiaPerispomeniYpogegrammeni"
	case DasiaProsgegrammeni:
		return "DasiaProsgegrammeni"
	case DasiaVaria:
		return "DasiaVaria"
	case DasiaVariaProsgegrammeni:
		return "DasiaVariaProsgegrammeni"
	case DasiaVariaYpogegrammeni:
		return "DasiaVariaYpogegrammeni"
	case DasiaYpogegrammeni:
		return "DasiaYpogegrammeni"
	case Diacritics:
		return "Diacritics"
	case DialytikaOxia:
		return "DialytikaOxia"
	case DialytikaPerispomeni:
		return "DialytikaPerispomeni"
	case DialytikaVaria:
		return "DialytikaVaria"
	case Epsilon:
		return "Epsilon"
	case EpsilonDiacritics:
		return "EpsilonDiacritics"
	case EpsilonLower:
		return "EpsilonLower"
	case EpsilonUpper:
		return "EpsilonUpper"
	case Eta:
		return "Eta"
	case EtaDiacritics:
		return "EtaDiacritics"
	case EtaLower:
		return "EtaLower"
	case EtaUpper:
		return "EtaUpper"
	case Iota:
		return "Iota"
	case IotaDiacritics:
		return "IotaDiacritics"
	case IotaLower:
		return "IotaLower"
	case IotaUpper:
		return "IotaUpper"
	case Macron:
		return "Macron"
	case Omega:
		return "Omega"
	case OmegaDiacritics:
		return "OmegaDiacritics"
	case OmegaLower:
		return "OmegaLower"
	case OmegaUpper:
		return "OmegaUpper"
	case Omicron:
		return "Omicron"
	case OmicronDiacritics:
		return "OmicronDiacritics"
	case OmicronLower:
		return "OmicronLower"
	case OmicronUpper:
		return "OmicronUpper"
	case Oxia:
		return "Oxia"
	case OxiaYpogegrammeni:
		return "OxiaYpogegrammeni"
	case Perispomeni:
		return "Perispomeni"
	case PerispomeniYpogegrammeni:
		return "PerispomeniYpogegrammeni"
	case Prosgegrammeni:
		return "Prosgegrammeni"
	case Psili:
		return "Psili"
	case PsiliOxia:
		return "PsiliOxia"
	case PsiliOxiaProsgegrammeni:
		return "PsiliOxiaProsgegrammeni"
	case PsiliOxiaYpogegrammeni:
		return "PsiliOxiaYpogegrammeni"
	case PsiliPerispomeni:
		return "PsiliPerispomeni"
	case PsiliPerispomeniProsgegrammeni:
		return "PsiliPerispomeniProsgegrammeni"
	case PsiliPerispomeniYpogegrammeni:
		return "PsiliPerispomeniYpogegrammeni"
	case PsiliProsgegrammeni:
		return "PsiliProsgegrammeni"
	case PsiliVaria:
		return "PsiliVaria"
	case PsiliVariaProsgegrammeni:
		return "PsiliVariaProsgegrammeni"
	case PsiliVariaYpogegrammeni:
		return "PsiliVariaYpogegrammeni"
	case PsiliYpogegrammeni:
		return "PsiliYpogegrammeni"
	case Rho:
		return "Rho"
	case RhoDiacritics:
		return "RhoDiacritics"
	case RhoLower:
		return "RhoLower"
	case RhoUpper:
		return "RhoUpper"
	case Upsilon:
		return "Upsilon"
	case UpsilonDiacritics:
		return "UpsilonDiacritics"
	case UpsilonLower:
		return "UpsilonLower"
	case UpsilonUpper:
		return "UpsilonUpper"
	case Varia:
		return "Varia"
	case VariaYpogegrammeni:
		return "VariaYpogegrammeni"
	case Vowels:
		return "Vowels"
	case Vrachy:
		return "Vrachy"
	case Ypogegrammeni:
		return "Ypogegrammeni"
	default:
		return ""
	}
}

`
