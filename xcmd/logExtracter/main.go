package main

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"os"
	"strings"
)

func main() {
	logPath := "/Users/mac002/doxa/projects/doxa-alwb/logs/generation.log"
	var noShowFilters []string
	//	noShowFilters := []string{""}
	var showFilters []string
	//	showFilters := []string{"not exist in database"}
	lines, err := ltfile.GetFileLines(logPath)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	/*
		2023/04/20 10:46:06 logging.go:36:
		/Users/mac002/doxa/projects/doxa-alwb/templates/a-templates/Dated-CompareServices/m06/d10/se.m06.d10.ma2.lml 18:4
		        parse error: {a-templates/Movable-Cycle/mcMaps/mc.map.ma2/bl.Instance01 93 4 parse error: {a-templates/Movable-Cycle/mc2-Pentecostarion/d126/se.pe.d126.ma2/bl.Instance01 12 0 parse error: {a-templates/Movable-Cycle/mc2-Pentecostarion/d126/se.pe.d126.ma2/Instance01/bl.options 7 0 parse error: {a-templates/Movable-Cycle/mc2-Pentecostarion/d126/se.pe.d126.ma2/Instance01/options/bl.block06_kontakionposition1 8 18 mismatched input '120' expecting MODE_INTEGERS}}}}

	*/
outer:
	for _, line := range lines {
		line = strings.ReplaceAll(line, "\t", "")
		parts := strings.Split(line, "{")
		if len(parts) == 1 {
			continue
		}
		joinStartIndex := -1
		for i, p := range parts {
			if strings.HasSuffix(strings.TrimSpace(p), "expecting") {
				joinStartIndex = i
			}
		}
		var last string
		if joinStartIndex > -1 {
			last = strings.Join(parts[joinStartIndex:len(parts)-1], " ")
		} else {
			last = parts[len(parts)-1]
		}
		last = strings.ReplaceAll(last, "}", "")
		//		fmt.Println(last)
		for _, noShowFilter := range noShowFilters {
			if strings.Contains(last, noShowFilter) {
				continue outer
			}
		}
		if len(showFilters) > 0 {
			for _, showFilter := range showFilters {
				if strings.Contains(last, showFilter) {
					ep := Parse(last)
					fmt.Printf("%s %s\n", ep.template, ep.key)
				}
			}
		} else {
			ep := Parse(last)
			fmt.Printf("%s %s\n", ep.template, ep.key)
		}
	}
}

type ErrorParts struct {
	template string
	key      string
}

func Parse(s string) *ErrorParts {
	ep := new(ErrorParts)
	i := strings.Index(s, " ")
	ep.template = s[:i]
	ep.template = strings.ReplaceAll(ep.template, "/bl.", "/")
	parts := strings.Split(s, "key '")
	if len(parts) > 1 {
		ep.key = parts[1]
	}
	parts = strings.Split(ep.key, "' does not exist in database")
	ep.key = parts[0]
	parts = strings.Split(ep.key, ":")
	if len(parts) > 1 {
		ep.key = fmt.Sprintf("sid %s", parts[1])
	}
	return ep
}
