document.addEventListener('DOMContentLoaded', function() {
    // Function to initialize the table
    function initializeTable(tableContainer) {
        console.log('Initializing table...');
        console.log('Table container:', tableContainer);

        // const table = tableContainer.querySelector('table');
        const headers = tableContainer.querySelectorAll('thead tr:first-child th');
        const filterInputs = tableContainer.querySelectorAll('.filter-row input');
        const tableBody = tableContainer.querySelector('tbody');
        const hasSelectionColumns = tableContainer.querySelector('.selectionColumn') !== null;

        console.log('Table elements:', {
            tableId: tableContainer.id,
            headerCount: headers.length,
            filterInputCount: filterInputs.length
        });

        const directions = Array.from(headers).map(() => '');
        let rowPairs = [];

        // Modified row pairing logic
        tableBody.querySelectorAll('tr.expandable-row').forEach(row => {
            const content = row.nextElementSibling;
            if (content && content.classList.contains('expandable-content')) {
                rowPairs.push([row, content]);
                row.classList.add('has-content');
            } else {
                rowPairs.push([row, null]);
            }
        });

        console.log('Initial rowPairs count:', rowPairs.length);

        const toggleExpand = (row) => {
            if (!row.classList.contains('has-content')) return;
            const content = row.nextElementSibling;
            if (content && content.classList.contains('expandable-content')) {
                content.classList.toggle('active');
                row.classList.toggle('expanded');
            }
        };

        const attachRowListeners = () => {
            rowPairs.forEach(([row, _]) => {
                row.removeEventListener('click', rowClickHandler);
                if (row.classList.contains('has-content')) {
                    row.addEventListener('click', rowClickHandler);
                }
            });
        };

        const rowClickHandler = function(evt) {
            // Ignore clicks on the radio or check box icon
            if (evt.target.type === 'radio' || evt.target.type === 'checkbox'){
                return;
            }
            evt.preventDefault();
            toggleExpand(this);
        };

        const attachInputListeners = () => {
            const inputs = tableContainer.querySelectorAll('input[type="radio"], input[type="checkbox"]');
            inputs.forEach(input => {
                input.addEventListener('change', (event) => {
                    event.stopPropagation(); // Prevent the event from bubbling up to the row

                    // If it's a checkbox, you might want to handle multiple selections
                    if (input.type === 'checkbox') {
                        const checkedBoxes = tableContainer.querySelectorAll('input[type="checkbox"]:checked');
                        console.log(`${checkedBoxes.length} checkboxes selected`);
                    } else {
                        console.log(`Radio button selected: ${input.value}`);
                    }

                    // Optionally, you can submit the form automatically when an input is selected
                    // input.closest('form').submit();
                });
            });
        };

        const sortSelectedRows = (direction) => {
            rowPairs.sort(([rowA], [rowB]) => {
                const inputA = rowA.querySelector('input[type="radio"], input[type="checkbox"]');
                const inputB = rowB.querySelector('input[type="radio"], input[type="checkbox"]');
                const isSelectedA = inputA && inputA.checked;
                const isSelectedB = inputB && inputB.checked;

                if (isSelectedA === isSelectedB) return 0;
                if (direction === 'asc') {
                    return isSelectedA ? -1 : 1;
                } else {
                    return isSelectedA ? 1 : -1;
                }
            });

            while (tableBody.firstChild) {
                tableBody.removeChild(tableBody.firstChild);
            }

            rowPairs.forEach(([row, content]) => {
                tableBody.appendChild(row);
                if (content) {
                    tableBody.appendChild(content);
                }
            });

            attachRowListeners();
        };

        const sortColumn = (index) => {
            const direction = directions[index] || 'asc';
            const multiplier = (direction === 'asc') ? 1 : -1;

            if (index === 0 && hasSelectionColumns) {
                sortSelectedRows(direction);
            } else {
                rowPairs.sort(([rowA], [rowB]) => {
                    const cellA = rowA.querySelectorAll('td')[index].textContent.trim();
                    const cellB = rowB.querySelectorAll('td')[index].textContent.trim();

                    const valueA = isNaN(cellA) ? cellA : parseFloat(cellA);
                    const valueB = isNaN(cellB) ? cellB : parseFloat(cellB);

                    switch (true) {
                        case valueA > valueB: return 1 * multiplier;
                        case valueA < valueB: return -1 * multiplier;
                        case valueA === valueB: return 0;
                    }
                });

                while (tableBody.firstChild) {
                    tableBody.removeChild(tableBody.firstChild);
                }

                rowPairs.forEach(([row, content]) => {
                    tableBody.appendChild(row);
                    if (content) {
                        tableBody.appendChild(content);
                    }
                });
            }

            directions[index] = direction === 'asc' ? 'desc' : 'asc';

            headers.forEach(header => header.classList.remove('asc', 'desc'));
            headers[index].classList.add(directions[index]);

            attachRowListeners();
        };

        const filterTable = (showOnlySelected = false) => {
            console.log('Filtering table');
            const filterValues = Array.from(filterInputs).map(input => input.value.toLowerCase());
            console.log('Filter values:', filterValues);

            let visibleCount = 0;
            rowPairs.forEach(([row, content], index) => {
                const cells = row.querySelectorAll('td');
                const input = row.querySelector('input[type="radio"], input[type="checkbox"]');
                const isSelected = input && input.checked;

                const shouldShow = (showOnlySelected ? isSelected : true) && filterValues.every((filter, filterIndex) => {
                    if (filter === '') return true; // Skip empty filters

                    // Adjust cell index if there's a selection column
                    let cellIndex = filterIndex;
                    if (hasSelectionColumns) {
                        cellIndex++;
                    }

                    if (cellIndex >= cells.length) return true; // Skip if cell doesn't exist

                    const cellText = cells[cellIndex].textContent.trim().toLowerCase();
                    const includes = cellText.includes(filter);
                    console.log(`Row ${index}, Cell ${cellIndex}: "${cellText}" includes "${filter}": ${includes}`);
                    return includes;
                });

                console.log(`Row ${index} visibility:`, shouldShow);
                row.style.display = shouldShow ? '' : 'none';
                if (content) {
                    content.style.display = shouldShow ? '' : 'none';
                }
                if (shouldShow) visibleCount++;
            });
            console.log('Visible rows after filtering:', visibleCount);
        };

        headers.forEach((header, index) => {
            header.addEventListener('click', () => {
                sortColumn(index);
            });
        });

        filterInputs.forEach((input, index) => {
            input.addEventListener('input', () => {
                console.log(`Filter input ${index} changed:`, input.value);
                filterTable();
            });
        });

        attachInputListeners();
        attachRowListeners();

        // Button event listeners
        const buttons = {
            sortSelectedTop: tableContainer.querySelector('#sortSelectedTop'),
            sortSelectedBottom: tableContainer.querySelector('#sortSelectedBottom'),
            showOnlySelected: tableContainer.querySelector('#showOnlySelected'),
            showAll: tableContainer.querySelector('#showAll'),
            toggleFilters: tableContainer.querySelector('#toggleFilters')
        };

        if (buttons.sortSelectedTop) buttons.sortSelectedTop.addEventListener('click', () => sortSelectedRows('asc'));
        if (buttons.sortSelectedBottom) buttons.sortSelectedBottom.addEventListener('click', () => sortSelectedRows('desc'));
        if (buttons.showOnlySelected) buttons.showOnlySelected.addEventListener('click', () => filterTable(true));
        if (buttons.showAll) buttons.showAll.addEventListener('click', () => filterTable(false));

        // Toggle filters visibility
        if (buttons.toggleFilters && filterInputs.length > 0) {
            const filterRow = table.querySelector('.filter-row');
            filterRow.style.display = 'none'; // Initially hide the filter row

            buttons.toggleFilters.addEventListener('click', () => {
                const isHidden = filterRow.style.display === 'none';
                filterRow.style.display = isHidden ? '' : 'none';
                buttons.toggleFilters.textContent = isHidden ? 'Hide Filters' : 'Show Filters';

                if (isHidden) {
                    filterInputs.forEach(input => input.value = ''); // Clear filters when showing
                    filterTable(); // Reapply (empty) filters to show all rows
                }

                // Adjust table height if needed
                if (typeof adjustTableHeight === 'function') {
                    adjustTableHeight();
                }
            });
        }

        // Adjust table height based on window size
        function adjustTableHeight() {
            const windowHeight = window.innerHeight;
            const tableTop = tableContainer.getBoundingClientRect().top;
            const newHeight = windowHeight - tableTop - 20; // 20px buffer
            tableContainer.style.height = `${newHeight}px`;
        }

        // Call on load and on window resize
        adjustTableHeight();
        window.addEventListener('resize', adjustTableHeight);
    }

    // Initialize all tables on the page
    document.querySelectorAll('.table-container').forEach(initializeTable);
});
