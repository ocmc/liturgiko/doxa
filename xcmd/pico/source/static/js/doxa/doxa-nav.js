document.addEventListener("click", (e) => {
    let targetElement = e.target;
    let isDropdown = false;

    while (targetElement) {
        if (targetElement.classList && targetElement.classList.contains("dropdown-menu")) {
            isDropdown = true;
            break;
        }
        targetElement = targetElement.parentNode;
    }

    const dropdowns = document.querySelectorAll(".dropdown-content");
    for (let i = 0; i < dropdowns.length; i++) {
        const dropdown = dropdowns[i];
        if (!dropdown.parentElement.contains(e.target)) {
            dropdown.style.display = "none";
        }
    }

    if (isDropdown) {
        const dropdownContent = targetElement.querySelector(".dropdown-content");
        if (dropdownContent.style.display === "block") {
            dropdownContent.style.display = "none";
        } else {
            dropdownContent.style.display = "block";
        }
    }
});
document.addEventListener('DOMContentLoaded', function() {
    let themeToggle = document.getElementById('theme-toggle');

    function toggleTheme() {
        const currentTheme = document.documentElement.getAttribute('data-theme');
        const newTheme = currentTheme === 'dark' ? 'light' : 'dark';
        document.documentElement.setAttribute('data-theme', newTheme);
        localStorage.setItem('theme', newTheme);
        updateIcon(newTheme);
    }

// Apply the saved theme on page load
    document.addEventListener('DOMContentLoaded', function() {
        const savedTheme = localStorage.getItem('theme') || 'light';
        document.documentElement.setAttribute('data-theme', savedTheme);
        updateIcon(savedTheme);
    });
    function updateIcon(theme) {
        const themeIcon = document.getElementById('theme-icon');
        if (theme === 'dark') {
            themeIcon.className = 'bi bi-brightness-high';
        } else {
            themeIcon.className = 'bi bi-moon';
        }
    }

    if (themeToggle) {
        themeToggle.addEventListener('click', function(event) {
            event.preventDefault();
            toggleTheme();
        });

        updateIcon(document.documentElement.getAttribute('data-theme') || 'light');
    }

    const messageIndicator = document.getElementById('message-indicator');
    const messageModal = document.getElementById('message-modal');
    const alertIndicator = document.getElementById('alert-indicator');
    const alertModal = document.getElementById('alert-modal');
    const tokenIndicator = document.getElementById('token-indicator');
    const tokenModal = document.getElementById('token-modal');


    function setupModal(indicator, modal, modalName) {
        if (indicator && modal) {
            indicator.addEventListener('click', (e) => {
                e.preventDefault();
                if(indicator.querySelector('i.bi-unlock') !== null){
                    // do not show modal in this case
                    return
                }
                try {
                    modal.showModal();
                    document.documentElement.classList.add(`${modalName}-is-open`, `${modalName}-is-opening`);
                } catch (error) {
                    console.error(`Error opening ${modalName} modal:`, error);
                }
            });

            const closeButtons = document.querySelectorAll(`[data-target="${modalName}-modal"]`);
            closeButtons.forEach(button => {
                button.addEventListener('click', (e) => {
                    e.preventDefault();
                    document.documentElement.classList.add(`${modalName}-is-closing`);
                    setTimeout(() => {
                        modal.close();
                        document.documentElement.classList.remove(`${modalName}-is-open`, `${modalName}-is-opening`, `${modalName}-is-closing`);
                    }, 300);
                });
            });
        }
    }

    setupModal(tokenIndicator, tokenModal, 'token');
    setupModal(messageIndicator, messageModal, 'message');
    setupModal(alertIndicator, alertModal, 'alert');});