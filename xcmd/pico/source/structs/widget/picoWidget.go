package picoWidget

import (
	"html/template"
	"strings"
)

// Structs for Pico Form Semantic Field

// default class names

const (
	WidgetClass                     = "container doxa-widget"
	WidgetTitleClass                = "doxa-widget-title"
	WidgetLabelClass                = "doxa-widget-label"
	WidgetFormButtonClass           = "doxa-widget-form-button"
	WidgetFormButtonHelperTextClass = "doxa-widget-form-button-helper-text"
	WidgetInputTypeText             = "text"
	WidgetInputTypeEmail            = "email"
	WidgetInputTypeNumber           = "number"
	WidgetInputTypeTel              = "tel"
	WidgetInputTypePassword         = "password"
)

// Widget exists simply to allow web components of different kinds to exist together in a list.
// If a new widget struct is added in this package, it must implement the interface.
// Widget structs are used in template bodyMainSectionGridArticleWidget.
// The WT() func should be called to get the WidgetType for use in the template.
type Widget interface {
	WT() string
}

type Element interface {
	ET() string
}

/*
Relationships
--> one and only one
-->> one to many
Body --> Main
Main -->> Section -->> Grid -->> Article (aka Widget) -->> Field (aka InputPut)

	--> InputField
*/

type Body struct {
	//Heading *Heading
	Main *Main
	//Footer  *Footer
}

type Main struct {
	ID          string
	Class       string
	Heading     string
	Description string
	Sections    []*Section
}

type Section struct {
	ID    string
	Class string
	Grids []*Grid // each grid displays as a row.
	// the widgets in a grid display as columns, side-by-side
}

type Grid struct {
	ID      string
	Class   string
	Widgets []Widget
}

// WT implements the Widget interface
// and provides the literal string
// to use in the if statements in
// template bodyMainSectionGridArticleWidget.
func (f *Form) WT() string { return "form" }

type Form struct {
	ID          string
	Class       string // the css class for the form container, e.g. "container doxa-widget"
	WidgetType  string
	Title       string // Title of the widget displayed in the div.doxa-widget-title
	Description string
	FormAction  string
	FormMethod  string
	Fields      []Element     // Form fieldset
	Actions     []*InputField // Form action input, e.g. button.  1st should be marked primary, others secondary
	//HXAttributes []Attrib
}

// WT implements the Widget interface
// and provides the literal string
// to use in the if statements in
// template bodyMainSectionGridArticleWidget.
func (t Table) WT() string { return "table" }

type Table struct {
	ID                         string
	Class                      string
	WidgetType                 string
	Title                      string
	Description                string
	ContainerClass             string
	CanPage                    bool
	CanSelectRow               bool
	CanFilter                  bool
	IdColumnHidden             bool
	BtnHideFiltersLabel        string
	BtnShowFiltersLabel        string
	BtnSortSelectedBottomLabel string
	BtnSortSelectedTopLabel    string
	BtnShowSelectedLabel       string
	BtnShowAllLabel            string
	BtnSubmitLabel             string
	PaginationClass            string
	ToggleFiltersClass         string
	RowsPerPageClass           string
	SelectHeaderName           string
	IncludeSelectRowNone       bool
	SelectRowNone              string
	SelectRowNoneOtherTds      string
	PrevButtonClass            string
	NextButtonClass            string
	ToggleFilters              string
	RowsPerPage                string
	Headers                    []string
	Rows                       []Row
	PreviousButton             string
	NextButton                 string
	ColumnConfigs              []ColumnConfig
}

type ColumnConfig struct {
	Width     string
	Alignment string
}

type Row struct {
	MainData   []string
	DetailData []DetailItem
	UniqueID   string
	Selected   bool
}

type DetailItem struct {
	Label string
	Value interface{}
}

func (t Table) WID() {}

func GetTestTable(canPage, canSelectRow, canFilter, includeDetails bool) *Table {
	var cssClass string
	if includeDetails {
		cssClass = "striped hover"
	} else {
		cssClass = "striped"
	}
	table := &Table{
		ID:    "myTable",
		Class: cssClass,
		//WidgetType:         "table",
		ContainerClass:     "table-container",
		CanPage:            canPage,
		CanSelectRow:       canSelectRow,
		CanFilter:          canFilter,
		PaginationClass:    "pagination",
		ToggleFiltersClass: "secondary",
		RowsPerPageClass:   "outline",
		SelectHeaderName:   "Subscribe",
		PrevButtonClass:    "outline",
		NextButtonClass:    "outline",
		ToggleFilters:      "Toggle Filters",
		RowsPerPage:        "10",
		Headers:            []string{"Name", "Age", "City"},
		PreviousButton:     "Previous",
		NextButton:         "Next",
		ColumnConfigs: []ColumnConfig{
			{Width: "40%", Alignment: "left"},
			{Width: "20%", Alignment: "center"},
			{Width: "40%", Alignment: "left"},
		},
	}
	table.WidgetType = table.WT()
	if canFilter {
		table.BtnShowFiltersLabel = "Hide Filters"
		table.BtnShowFiltersLabel = "Show Filters"
	}
	if canSelectRow {
		table.BtnSortSelectedBottomLabel = "Sort Selected Bottom"
		table.BtnSortSelectedTopLabel = "Sort Selected Top"
		table.BtnShowSelectedLabel = "Show Selected"
		table.BtnShowAllLabel = "Show All"
		table.BtnSubmitLabel = "Submit Selection"
	}

	table.Rows = []Row{}

	additionalRows := [][]string{
		{"None", "n/a", "n/a"},
		{"Alice", "28", "New York"},
		{"Bob", "35", "San Francisco"},
		{"Charlie", "42", "London"},
		{"David", "31", "Tokyo"},
		{"Eve", "39", "Paris"},
		{"Frank", "45", "Berlin"},
		{"Grace", "33", "Sydney"},
		{"Henry", "29", "Toronto"},
		{"Ivy", "37", "Singapore"},
		{"Jack", "41", "Dubai"},
		{"Kate", "36", "Mumbai"},
		{"Liam", "30", "Dublin"},
	}

	for _, rowData := range additionalRows {
		r := Row{
			MainData: rowData,
			UniqueID: strings.ToLower(rowData[0]),
			Selected: false,
		}
		if includeDetails {
			r.DetailData = []DetailItem{
				{Label: "Email", Value: strings.ToLower(rowData[0]) + "@example.com"},
				{Label: "Phone", Value: "(000) 000-0000"},
				{Label: "Address", Value: "123 Main St, " + rowData[2]},
			}
		}
		table.Rows = append(table.Rows, r)
	}

	return table
}

// AddRow Method to add a new row to the table
func (t Table) AddRow(mainData []string, detailData []DetailItem, uniqueID string) {
	t.Rows = append(t.Rows, Row{
		MainData:   mainData,
		DetailData: detailData,
		UniqueID:   uniqueID,
		Selected:   false,
	})
}
func (i *InputField) ET() string {
	return "input"
}

type InputField struct {
	ID                 string
	Class              string
	ElementType        string
	Type               string // text, password,etc. see https://picocss.com/docs/forms/input
	Label              string // appears above the input
	Name               string // is the key in a form key-value pair as seen by REST API
	PlaceHolder        string // prompts the user regarding what to enter
	Required           bool   // To indicate if the input is required
	Disabled           bool   // To disable the input if needed
	ReadOnly           bool
	Value              string // Default value for the input field
	AriaDescribedBy    string // Must match helper ID.
	AriaLabel          string // ARIA label for accessibility
	AriaInvalid        bool   // To indicate if the input value is invalid
	Helper             Helper
	ValidationFeedback *ValidationFeedback
	ShowLoading        bool                // Flag to show a loading indicator
	LoadingText        string              // Text for the loading indicator
	HtmxFields         []template.HTMLAttr //used for custom endpoints
	ConfirmModal       *ConfirmModal       // used with hx-confirm
}

type Helper struct {
	ID    string // used by aria-described-by
	Class string
	Text  string // General helper text
}
type ConfirmModal struct {
	ID          string
	Title       string
	Message     string
	CancelText  string
	ConfirmText string
}
type ValidationFeedback struct {
	ID        string
	Class     string
	Message   string // Validation feedback message
	IsInvalid bool   // Flag to indicate if the message is an error message
}

// Methods

func (m *Main) AddSection(section *Section) {
	m.Sections = append(m.Sections, section)
}
func (s *Section) AddGrid(grid *Grid) {
	s.Grids = append(s.Grids, grid)
}
func (g *Grid) AddWidget(e Widget) {
	g.Widgets = append(g.Widgets, e)
}
func (f *Form) AddField(field Element) {
	f.Fields = append(f.Fields, field)
}
func (f *Form) AddAction(action *InputField) {
	f.Actions = append(f.Actions, action)
}
func (i *InputField) AddHelper(id, text string) {
	i.Helper = Helper{
		ID:   id,
		Text: text,
	}
}
func (i *InputField) AddValidationFeedback(id, message string, isInvalid bool) {
	i.ValidationFeedback = &ValidationFeedback{
		ID:        id,
		Message:   message,
		IsInvalid: isInvalid,
	}
}

// Constructors

func NewPage() *Body {
	return &Body{}
}
func NewMain(heading, description string) *Main {
	p := &Main{}
	p.Heading = heading
	p.Description = description
	return p
}
func NewSection() *Section {
	return &Section{}
}
func NewGrid() *Grid {
	return &Grid{}
}
func NewForm(containerClass, title string) *Form {
	f := &Form{}
	f.Class = containerClass
	f.WidgetType = "form"
	f.Title = title
	return f
}
func NewTable(containerClass string) *Table {
	t := &Table{}
	t.Class = containerClass
	t.WidgetType = "table"
	return t
}
func NewInputField() *InputField {
	i := new(InputField)
	i.ElementType = i.ET()
	return i
}

func (p *ProgressBar) ET() string {
	return "progress"
}

type ProgressBar struct {
	Max             int
	Value           int
	ID              string
	Class           string
	ElementType     string
	Type            string // text, password,etc. see https://picocss.com/docs/forms/input
	AriaDescribedBy string // Must match helper ID.
	AriaLabel       string // ARIA label for accessibility
	Helper          Helper
	HtmxFields      []template.HTMLAttr //used for custom endpoints
}

func NewProgressBar() *ProgressBar {
	p := new(ProgressBar)
	p.ElementType = p.ET()
	return p
}
