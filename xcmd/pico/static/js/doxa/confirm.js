(function() {
    htmx.on('htmx:confirm', function(evt) {
        // Prevent the default confirmation dialog
        evt.preventDefault();
        let message = evt.detail.question;

        const modal = document.getElementById('confirmModalDialog');
        const messageElement = document.getElementById('confirmModalMessage');
        const confirmButton = document.getElementById('confirmModalConfirmButton');
        const cancelButton = document.getElementById('confirmModalCancelButton');

        messageElement.textContent = message;

        const handleConfirm = () => {
            modal.close();
            cleanup();
            evt.detail.issueRequest(true);  // Proceed with the request
        };

        const handleCancel = () => {
            modal.close();
            cleanup();
        };

        const cleanup = () => {
            confirmButton.removeEventListener('click', handleConfirm);
            cancelButton.removeEventListener('click', handleCancel);
        };

        confirmButton.addEventListener('click', handleConfirm);
        cancelButton.addEventListener('click', handleCancel);

        modal.showModal();

        // Return false to prevent HTMX from showing the native confirm dialog
        return false;
    });
})();
