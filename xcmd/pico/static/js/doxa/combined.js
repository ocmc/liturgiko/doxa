/* doxa-modal-confirm.js */
/*
Example usage:
	return &picoWidget.InputField{
		ID:    id,
		Type:  "button",
		Class: "secondary button",
		HtmxFields: []template.HTMLAttr{
			`hx-delete="/api/htmx/token/delete"`,
			`hx-confirm="Are you sure? This cannot be undone. If you delete your Gitlab Token, you will have to manually handle git and Gitlab for your project."`,
			`hx-target="#formMessages"`,
		},
		ConfirmModal: &picoWidget.ConfirmModal{
			ID:          "tokenService-delete-modal",
			Title:       "Are you sure?",
			Message:     "This cannot be undone. If you delete your Gitlab Token, you will have to re-enter it before doxa can interact with your Gitlab Group. Features like Backup and Publish Catalog will not be available.",
			CancelText:  "Cancel",
			ConfirmText: "Delete Token",
		},
		Value: "Delete Token",
	}

Problem: when delete is clicked and the modal confirm is displayed, and its delete button is clicked, the native browser confirm modal appears on top of it.
         See https://htmx.org/examples/confirm/
 */

document.addEventListener('DOMContentLoaded', function() {
    (function () {
        htmx.on('htmx:confirm', function (evt) {

            // Get the target element that triggered the event
            const target = evt.target;

            // Check if the event target has an 'hx-confirm' attribute
            if (! (target.hasAttribute('hx-confirm') && target.hasAttribute('data-confirm-title'))) {
                // If not, proceed without showing the modal
                evt.detail.issueRequest(true);
                return;
            }

            // Prevent the default confirmation dialog
            evt.preventDefault();

            // Retrieve values from data attributes
            const title = target.getAttribute('data-confirm-title') || 'Are you sure?';
            const message = target.getAttribute('data-confirm-message') || 'This action cannot be undone.';
            const cancelText = target.getAttribute('data-confirm-cancel') || 'Cancel';
            const confirmText = target.getAttribute('data-confirm-confirm') || 'Confirm';

            // Get modal elements
            const modal = document.getElementById('confirmModalDialog');
            const titleElement = document.getElementById('confirmModalTitle');
            const messageElement = document.getElementById('confirmModalMessage');
            const confirmButton = document.getElementById('confirmModalConfirmButton');
            const cancelButton = document.getElementById('confirmModalCancelButton');

            // Update modal content with values from data attributes
            titleElement.textContent = title;
            messageElement.textContent = message;
            confirmButton.textContent = confirmText;
            cancelButton.textContent = cancelText;

            const close = () => {
                evt.preventDefault();
                evt.stopPropagation();
                modal.close();
                cleanup();
            }
            // Define confirm and cancel handlers
            const handleConfirm = () => {
                evt.preventDefault();
                evt.stopPropagation();
                close();
                evt.detail.issueRequest(true);  // Proceed with the request
            };

            const handleCancel = () => {
                close();
            };

            const cleanup = () => {
                confirmButton.removeEventListener('click', handleConfirm);
                cancelButton.removeEventListener('click', handleCancel);
            };

            // Attach event listeners for buttons
            confirmButton.addEventListener('click', handleConfirm);
            cancelButton.addEventListener('click', handleCancel);

            // Show the modal
            modal.showModal();

            // Return false to prevent HTMX from showing native confirm dialog
            return false;
        });
    })();
});


/* doxa-nav.js */
document.addEventListener("click", (e) => {
    let targetElement = e.target;
    let isDropdown = false;

    while (targetElement) {
        if (targetElement.classList && targetElement.classList.contains("dropdown-menu")) {
            isDropdown = true;
            break;
        }
        targetElement = targetElement.parentNode;
    }

    const dropdowns = document.querySelectorAll(".dropdown-content");
    for (let i = 0; i < dropdowns.length; i++) {
        const dropdown = dropdowns[i];
        if (!dropdown.parentElement.contains(e.target)) {
            dropdown.style.display = "none";
        }
    }

    if (isDropdown) {
        const dropdownContent = targetElement.querySelector(".dropdown-content");
        if (dropdownContent.style.display === "block") {
            dropdownContent.style.display = "none";
        } else {
            dropdownContent.style.display = "block";
        }
    }
});
document.addEventListener('DOMContentLoaded', function() {
    let themeToggle = document.getElementById('theme-toggle');

    function toggleTheme() {
        const currentTheme = document.documentElement.getAttribute('data-theme');
        const newTheme = currentTheme === 'dark' ? 'light' : 'dark';
        document.documentElement.setAttribute('data-theme', newTheme);
        localStorage.setItem('theme', newTheme);
        updateIcon(newTheme);
    }

// Apply the saved theme on page load
    document.addEventListener('DOMContentLoaded', function() {
        const savedTheme = localStorage.getItem('theme') || 'light';
        document.documentElement.setAttribute('data-theme', savedTheme);
        updateIcon(savedTheme);
    });
    function updateIcon(theme) {
        const themeIcon = document.getElementById('theme-icon');
        if (theme === 'dark') {
            themeIcon.className = 'bi bi-brightness-high';
        } else {
            themeIcon.className = 'bi bi-moon';
        }
    }

    if (themeToggle) {
        themeToggle.addEventListener('click', function(event) {
            event.preventDefault();
            toggleTheme();
        });

        updateIcon(document.documentElement.getAttribute('data-theme') || 'light');
    }

    const messageIndicator = document.getElementById('message-indicator');
    const messageModal = document.getElementById('message-modal');
    const alertIndicator = document.getElementById('alert-indicator');
    const alertModal = document.getElementById('alert-modal');
    const tokenIndicator = document.getElementById('token-indicator');
    const tokenModal = document.getElementById('token-modal');


    function setupModal(indicator, modal, modalName) {
        if (indicator && modal) {
            indicator.addEventListener('click', (e) => {
                e.preventDefault();
                if(indicator.querySelector('i.bi-unlock') !== null){
                    // do not show modal in this case
                    return
                }
                try {
                    modal.showModal();
                    document.documentElement.classList.add(`${modalName}-is-open`, `${modalName}-is-opening`);
                } catch (error) {
                    console.error(`Error opening ${modalName} modal:`, error);
                }
            });

            const closeButtons = document.querySelectorAll(`[data-target="${modalName}-modal"]`);
            closeButtons.forEach(button => {
                button.addEventListener('click', (e) => {
                    e.preventDefault();
                    document.documentElement.classList.add(`${modalName}-is-closing`);
                    setTimeout(() => {
                        modal.close();
                        document.documentElement.classList.remove(`${modalName}-is-open`, `${modalName}-is-opening`, `${modalName}-is-closing`);
                    }, 300);
                });
            });
        }
    }

    setupModal(tokenIndicator, tokenModal, 'token');
    setupModal(messageIndicator, messageModal, 'message');
    setupModal(alertIndicator, alertModal, 'alert');});

/* doxa-pico-table.js */
document.addEventListener('DOMContentLoaded', function() {
    // Function to initialize the table
    function initializeTable(tableContainer) {
        const table = tableContainer.querySelector('table');
        const headers = table.querySelectorAll('thead tr:first-child th');
        const filterInputs = table.querySelectorAll('.filter-row input');
        const tableBody = table.querySelector('tbody');
        const hasSelectionColumns = table.querySelector('.selectionColumn') !== null;

        console.log('Table elements:', {
            tableId: table.id,
            headerCount: headers.length,
            filterInputCount: filterInputs.length
        });

        const directions = Array.from(headers).map(() => '');
        let rowPairs = [];

        // Modified row pairing logic
        tableBody.querySelectorAll('tr.expandable-row').forEach(row => {
            const content = row.nextElementSibling;
            if (content && content.classList.contains('expandable-content')) {
                rowPairs.push([row, content]);
                row.classList.add('has-content');
            } else {
                rowPairs.push([row, null]);
            }
        });

        console.log('Initial rowPairs count:', rowPairs.length);

        const toggleExpand = (row) => {
            if (!row.classList.contains('has-content')) return;
            const content = row.nextElementSibling;
            if (content && content.classList.contains('expandable-content')) {
                content.classList.toggle('active');
                row.classList.toggle('expanded');
            }
        };

        const attachRowListeners = () => {
            rowPairs.forEach(([row, _]) => {
                row.removeEventListener('click', rowClickHandler);
                if (row.classList.contains('has-content')) {
                    row.addEventListener('click', rowClickHandler);
                }
            });
        };

        const rowClickHandler = function(evt) {
            // Ignore clicks on the radio or check box icon
            if (evt.target.type === 'radio' || evt.target.type === 'checkbox'){
                return;
            }
            evt.preventDefault();
            toggleExpand(this);
        };

        const attachInputListeners = () => {
            const inputs = table.querySelectorAll('input[type="radio"], input[type="checkbox"]');
            inputs.forEach(input => {
                input.addEventListener('change', (event) => {
                    event.stopPropagation(); // Prevent the event from bubbling up to the row

                    // If it's a checkbox, you might want to handle multiple selections
                    if (input.type === 'checkbox') {
                        const checkedBoxes = table.querySelectorAll('input[type="checkbox"]:checked');
                        console.log(`${checkedBoxes.length} checkboxes selected`);
                    } else {
                        console.log(`Radio button selected: ${input.value}`);
                    }

                    // Optionally, you can submit the form automatically when an input is selected
                    // input.closest('form').submit();
                });
            });
        };

        const sortSelectedRows = (direction) => {
            rowPairs.sort(([rowA], [rowB]) => {
                const inputA = rowA.querySelector('input[type="radio"], input[type="checkbox"]');
                const inputB = rowB.querySelector('input[type="radio"], input[type="checkbox"]');
                const isSelectedA = inputA && inputA.checked;
                const isSelectedB = inputB && inputB.checked;

                if (isSelectedA === isSelectedB) return 0;
                if (direction === 'asc') {
                    return isSelectedA ? -1 : 1;
                } else {
                    return isSelectedA ? 1 : -1;
                }
            });

            while (tableBody.firstChild) {
                tableBody.removeChild(tableBody.firstChild);
            }

            rowPairs.forEach(([row, content]) => {
                tableBody.appendChild(row);
                if (content) {
                    tableBody.appendChild(content);
                }
            });

            attachRowListeners();
        };

        const sortColumn = (index) => {
            const direction = directions[index] || 'asc';
            const multiplier = (direction === 'asc') ? 1 : -1;

            if (index === 0 && hasSelectionColumns) {
                sortSelectedRows(direction);
            } else {
                rowPairs.sort(([rowA], [rowB]) => {
                    const cellA = rowA.querySelectorAll('td')[index].textContent.trim();
                    const cellB = rowB.querySelectorAll('td')[index].textContent.trim();

                    const valueA = isNaN(cellA) ? cellA : parseFloat(cellA);
                    const valueB = isNaN(cellB) ? cellB : parseFloat(cellB);

                    switch (true) {
                        case valueA > valueB: return 1 * multiplier;
                        case valueA < valueB: return -1 * multiplier;
                        case valueA === valueB: return 0;
                    }
                });

                while (tableBody.firstChild) {
                    tableBody.removeChild(tableBody.firstChild);
                }

                rowPairs.forEach(([row, content]) => {
                    tableBody.appendChild(row);
                    if (content) {
                        tableBody.appendChild(content);
                    }
                });
            }

            directions[index] = direction === 'asc' ? 'desc' : 'asc';

            headers.forEach(header => header.classList.remove('asc', 'desc'));
            headers[index].classList.add(directions[index]);

            attachRowListeners();
        };

        const filterTable = (showOnlySelected = false) => {
            console.log('Filtering table');
            const filterValues = Array.from(filterInputs).map(input => input.value.toLowerCase());
            console.log('Filter values:', filterValues);

            let visibleCount = 0;
            rowPairs.forEach(([row, content], index) => {
                const cells = row.querySelectorAll('td');
                const input = row.querySelector('input[type="radio"], input[type="checkbox"]');
                const isSelected = input && input.checked;

                const shouldShow = (showOnlySelected ? isSelected : true) && filterValues.every((filter, filterIndex) => {
                    if (filter === '') return true; // Skip empty filters

                    // Adjust cell index if there's a selection column
                    let cellIndex = filterIndex;
                    if (hasSelectionColumns) {
                        cellIndex++;
                    }

                    if (cellIndex >= cells.length) return true; // Skip if cell doesn't exist

                    const cellText = cells[cellIndex].textContent.trim().toLowerCase();
                    const includes = cellText.includes(filter);
                    console.log(`Row ${index}, Cell ${cellIndex}: "${cellText}" includes "${filter}": ${includes}`);
                    return includes;
                });

                console.log(`Row ${index} visibility:`, shouldShow);
                row.style.display = shouldShow ? '' : 'none';
                if (content) {
                    content.style.display = shouldShow ? '' : 'none';
                }
                if (shouldShow) visibleCount++;
            });
            console.log('Visible rows after filtering:', visibleCount);
        };

        headers.forEach((header, index) => {
            header.addEventListener('click', () => {
                sortColumn(index);
            });
        });

        filterInputs.forEach((input, index) => {
            input.addEventListener('input', () => {
                console.log(`Filter input ${index} changed:`, input.value);
                filterTable();
            });
        });

        attachInputListeners();
        attachRowListeners();

        // Button event listeners
        const buttons = {
            sortSelectedTop: tableContainer.querySelector('#sortSelectedTop'),
            sortSelectedBottom: tableContainer.querySelector('#sortSelectedBottom'),
            showOnlySelected: tableContainer.querySelector('#showOnlySelected'),
            showAll: tableContainer.querySelector('#showAll'),
            toggleFilters: tableContainer.querySelector('#toggleFilters')
        };

        if (buttons.sortSelectedTop) buttons.sortSelectedTop.addEventListener('click', () => sortSelectedRows('asc'));
        if (buttons.sortSelectedBottom) buttons.sortSelectedBottom.addEventListener('click', () => sortSelectedRows('desc'));
        if (buttons.showOnlySelected) buttons.showOnlySelected.addEventListener('click', () => filterTable(true));
        if (buttons.showAll) buttons.showAll.addEventListener('click', () => filterTable(false));

        // Toggle filters visibility
        if (buttons.toggleFilters && filterInputs.length > 0) {
            const filterRow = table.querySelector('.filter-row');
            filterRow.style.display = 'none'; // Initially hide the filter row

            buttons.toggleFilters.addEventListener('click', () => {
                const isHidden = filterRow.style.display === 'none';
                filterRow.style.display = isHidden ? '' : 'none';
                buttons.toggleFilters.textContent = isHidden ? 'Hide Filters' : 'Show Filters';

                if (isHidden) {
                    filterInputs.forEach(input => input.value = ''); // Clear filters when showing
                    filterTable(); // Reapply (empty) filters to show all rows
                }

                // Adjust table height if needed
                if (typeof adjustTableHeight === 'function') {
                    adjustTableHeight();
                }
            });
        }

        // Adjust table height based on window size
        function adjustTableHeight() {
            const windowHeight = window.innerHeight;
            const tableTop = tableContainer.getBoundingClientRect().top;
            const newHeight = windowHeight - tableTop - 20; // 20px buffer
            tableContainer.style.height = `${newHeight}px`;
        }

        // Call on load and on window resize
        adjustTableHeight();
        window.addEventListener('resize', adjustTableHeight);
    }

    // Initialize all tables on the page
    document.querySelectorAll('.table-container').forEach(initializeTable);
});


