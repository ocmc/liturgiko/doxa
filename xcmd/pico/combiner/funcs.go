package main

import (
	"bufio"
	"fmt"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"io"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strings"
)

func CombineTemplates(templatesDir, comboOut string) error {
	// Get all .gohtml files in the directory
	files, err := filepath.Glob(filepath.Join(ltfile.ToSysPath(templatesDir), "*.gohtml"))
	if err != nil {
		return fmt.Errorf("error reading directory: %v", err)
	}

	// Sort files by filename length
	sort.Slice(files, func(i, j int) bool {
		return len(files[i]) < len(files[j])
	})

	// Create output file
	output, err := os.Create(comboOut)
	if err != nil {
		return fmt.Errorf("error creating output file: %v", err)
	}
	defer output.Close()

	// Process each file
	for _, file := range files {
		// Read file contents
		content, err := os.ReadFile(file)
		if err != nil {
			return fmt.Errorf("error reading file %s: %v", file, err)
		}

		// Write relative filepath as comment
		relPath, err := filepath.Rel(templatesDir, file)
		if err != nil {
			return fmt.Errorf("error getting relative path for %s: %v", file, err)
		}
		if _, err := fmt.Fprintf(output, "<!-- %s -->\n", relPath); err != nil {
			return fmt.Errorf("error writing comment to output file: %v", err)
		}

		// Write file contents
		if _, err := output.Write(content); err != nil {
			return fmt.Errorf("error writing content to output file: %v", err)
		}

		// Add newline for separation
		if _, err := fmt.Fprintln(output); err != nil {
			return fmt.Errorf("error writing newline to output file: %v", err)
		}
	}

	return nil
}

func SplitTemplates(comboIn, splitDirOut string) error {
	// Create the output directory if it doesn't exist
	if err := os.MkdirAll(splitDirOut, 0755); err != nil {
		return fmt.Errorf("error creating output directory: %v", err)
	}

	// Open the combined file
	file, err := os.Open(comboIn)
	if err != nil {
		return fmt.Errorf("error opening combined file: %v", err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var currentTemplate strings.Builder
	var currentFilePath string

	for scanner.Scan() {
		line := scanner.Text()

		if strings.HasPrefix(line, "<!--") && strings.HasSuffix(line, "-->") {
			// If we have a previous template, write it to a file
			if currentTemplate.Len() > 0 && currentFilePath != "" {
				if err := writeToFile(currentFilePath, currentTemplate.String(), splitDirOut); err != nil {
					return err
				}
				currentTemplate.Reset()
			}
			// Extract the file path from the comment
			currentFilePath = strings.TrimSpace(strings.TrimSuffix(strings.TrimPrefix(line, "<!--"), "-->"))
		} else {
			// Add the line to the current template
			currentTemplate.WriteString(line)
			currentTemplate.WriteString("\n")
		}
	}

	// Write the last template
	if currentTemplate.Len() > 0 && currentFilePath != "" {
		if err := writeToFile(currentFilePath, currentTemplate.String(), splitDirOut); err != nil {
			return err
		}
	}

	if err := scanner.Err(); err != nil {
		return fmt.Errorf("error reading combined file: %v", err)
	}

	return nil
}

func writeToFile(relPath, content, baseDir string) error {
	fullPath := filepath.Join(baseDir, relPath)
	dir := filepath.Dir(fullPath)

	// Create the directory if it doesn't exist
	if err := os.MkdirAll(dir, 0755); err != nil {
		return fmt.Errorf("error creating directory for %s: %v", fullPath, err)
	}

	// Write the content to the file
	if err := os.WriteFile(fullPath, []byte(content), 0644); err != nil {
		return fmt.Errorf("error writing file %s: %v", fullPath, err)
	}

	return nil
}
func copyGoFile(pathIn, pathOut, packageFrom, packageTo string) error {
	lines, err := ltfile.GetFileLines(pathIn)
	if err != nil {
		return fmt.Errorf("error reading file %s: %v", pathIn, err)
	}
	lines[0] = fmt.Sprintf("package %s", packageTo)
	err = ltfile.WriteLinesToFile(pathOut, lines)
	return nil
}
func Combine(suffix, dirIn, fileOut string) error {
	var combinedCSS strings.Builder
	if !strings.HasPrefix(suffix, ".") {
		suffix = "." + suffix
	}
	err := filepath.Walk(ltfile.ToSysPath(dirIn), func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() && strings.HasSuffix(info.Name(), suffix) {
			relPath, err := filepath.Rel(dirIn, path)
			if err != nil {
				return fmt.Errorf("error getting relative path for %s: %v", path, err)
			}

			content, err := os.ReadFile(path)
			if err != nil {
				return fmt.Errorf("error reading file %s: %v", path, err)
			}

			combinedCSS.WriteString(fmt.Sprintf("/* %s */\n", relPath))
			combinedCSS.Write(content)
			combinedCSS.WriteString("\n\n")
		}
		return nil
	})

	if err != nil {
		return fmt.Errorf("error walking through directory: %v", err)
	}
	d, f := filepath.Split(fileOut)
	if err = writeToFile(f, combinedCSS.String(), d); err != nil {
		return err
	}

	return nil
}

func CombineCss(dirIn []string, fileOut string) error {
	var combinedCSS strings.Builder
	var err error
	for _, f := range dirIn {
		err = filepath.Walk(ltfile.ToSysPath(f), func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if !info.IsDir() && strings.HasSuffix(info.Name(), ".css") {
				relPath, err := filepath.Rel(f, path)
				if err != nil {
					return fmt.Errorf("error getting relative path for %s: %v", path, err)
				}

				content, err := os.ReadFile(path)
				if err != nil {
					return fmt.Errorf("error reading file %s: %v", path, err)
				}

				combinedCSS.WriteString(fmt.Sprintf("/* %s */\n", relPath))
				combinedCSS.Write(content)
				combinedCSS.WriteString("\n\n")
			}
			return nil
		})

	}

	if err != nil {
		return fmt.Errorf("error walking through directory: %v", err)
	}
	d, f := path.Split(fileOut)
	if err = writeToFile(f, combinedCSS.String(), d); err != nil {
		return err
	}

	return nil
}

// copyFile copies a file from src to dst. If dst does not exist, it will be created.
func copyFile(src, dst string) error {
	// Open the source file
	sourceFile, err := os.Open(src)
	if err != nil {
		return err
	}
	defer sourceFile.Close()

	// Create the destination file
	destinationFile, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer destinationFile.Close()

	// Copy the content from source to destination
	_, err = io.Copy(destinationFile, sourceFile)
	if err != nil {
		return err
	}

	// Flush any remaining data to disk
	err = destinationFile.Sync()
	if err != nil {
		return err
	}

	return nil
}
