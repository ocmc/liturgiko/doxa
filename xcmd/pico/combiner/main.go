package main

import (
	"fmt"
	"path/filepath"
)

func main() {
	protoDirOut := "xcmd/pico"

	// structs
	structsFrom := "templates/webApp/structs/widget/picoWidget.go"
	structsTo := filepath.Join(protoDirOut, "structs.go")
	packageFrom := "picoWidgets"
	packageTo := "main"

	// css
	cssDir := []string{"static/css/doxa"}
	comboCssOut := filepath.Join(protoDirOut, "static/css/doxa/combined.css")

	// js
	jsDir := "static/js/doxa"
	comboJsOut := filepath.Join(protoDirOut, "static/js/doxa/combined.js")

	// templates
	templatesDir := "templates/webApp/pico/a_index"
	comboOut := filepath.Join(protoDirOut, "templates/doxa-combined.gohtml")
	splitOut := "xcmd/pico/combiner/templates"

	// combine css
	err := CombineCss(cssDir, comboCssOut)
	if err != nil {
		fmt.Printf("Error combining css files: %v\n", err)
		return
	}
	fmt.Printf("Combined css written to %s\n", comboCssOut)

	// combine js
	err = Combine(".js", jsDir, comboJsOut)
	if err != nil {
		fmt.Printf("Error combining css files: %v\n", err)
		return
	}
	fmt.Printf("Combined js written to %s\n", comboJsOut)
	// combine templates
	err = CombineTemplates(templatesDir, comboOut)
	if err != nil {
		fmt.Printf("Error combining templates: %v\n", err)
		return
	}

	// split templates
	fmt.Printf("Combined template split out and written to %s\n", splitOut)

	err = SplitTemplates(comboOut, splitOut)
	if err != nil {
		fmt.Printf("Error spliting templates: %v\n", err)
		return
	}
	fmt.Printf("SplitTemplates templates written to %s\n", comboOut)

	// copy structs
	err = copyGoFile(structsFrom, structsTo, packageFrom, packageTo)
	if err != nil {
		fmt.Printf("Error copying structs: %v\n", err)
		return
	}
	fmt.Printf("Copied %s to %s\n", structsFrom, structsTo)
}
