package main

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/tmplfunc"

	"html/template"
	"log"
	"net/http"
	"path/filepath"
	"runtime"
)

func main() {
	// Get the directory of the current source file
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		log.Fatal("No caller information")
	}
	sourceDir := filepath.Dir(filename)
	log.Printf("Source directory: %s", sourceDir)

	// Get template functions
	// Get all template files in the templates directodry
	templatePattern := filepath.Join(sourceDir, "templates", "*.gohtml")
	tf := tmplfunc.New()
	templates, err := template.New("").Funcs(tf.Map()).ParseGlob(templatePattern)
	if err != nil {
		log.Fatalf("Error parsing templates: %v", err)
	}

	// Serve static files with custom file server
	staticDir := filepath.Join(sourceDir, "static")
	log.Printf("Serving static files from: %s", staticDir)

	fs := &customFileServer{http.FileServer(http.Dir(staticDir))}
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	// Handle the root route
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		p := NewIndexHtmlData("DOXA UI Prototype", GetPrototypeNav(), false)
		err := templates.ExecuteTemplate(w, "index", p)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	})

	// Handle the modal confirm route
	http.HandleFunc("/confirm", func(w http.ResponseWriter, r *http.Request) {
		p := NewIndexHtmlData("Confirm", GetPrototypeNav(), false)
		err := templates.ExecuteTemplate(w, "index", p)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	})

	// Handle the accordion route
	http.HandleFunc("/accordion", func(w http.ResponseWriter, r *http.Request) {
		p := NewIndexHtmlData("Accordion", GetPrototypeNav(), false)
		section := new(Section)
		grid := NewGrid()
		grid.AddWidget(GetTestAccordion(1))
		section.AddGrid(grid)
		p.Body.Main.AddSection(section)
		err := templates.ExecuteTemplate(w, "index", p)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	})

	// Handle the table route
	http.HandleFunc("/table", func(w http.ResponseWriter, r *http.Request) {
		p := NewIndexHtmlData("Table", GetPrototypeNav(), false)
		p.Body.Main.AddSection(GetProtoTypeSectionWithTable())
		p.WidgetTest = GetTestTable(false, false, true, true)
		err := templates.ExecuteTemplate(w, "index", p)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	})

	http.HandleFunc("/dynamicList", func(w http.ResponseWriter, r *http.Request) {
		p := NewIndexHtmlData("DynamicList", GetPrototypeNav(), false)
		//p.Body.Main.Class = "container-fluid"
		section := new(Section)
		grid := NewGrid()
		tl := GetTestList()
		tl.Class = "container-fluid"
		grid.AddWidget(tl)
		f := NewGeneric() //("", "doxa-seraphimdedes")
		l := NewLabel()
		l.Contents = "this is where info about the catalog or library will go"
		f.Class = "container-fluid"
		f.Contents = append(f.Contents, l)
		//grid.AddWidget(f)
		section.AddGrid(grid)
		p.Body.Main.AddSection(section)
		err := templates.ExecuteTemplate(w, "index", p)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	})

	http.HandleFunc("/updateList", func(w http.ResponseWriter, r *http.Request) {
		for k, v := range r.Header {
			fmt.Printf("%s:\t%s\n", k, v)
		}
		wdgt := GetTestList()
		w.Header().Set("HX-Refresh", "false")
		err := templates.ExecuteTemplate(w, "Element", wdgt.Contents[0].(*GridLayout).Contents[0])
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	})

	http.HandleFunc("/getDLInfo", func(w http.ResponseWriter, r *http.Request) {
		for k, v := range r.Header {
			fmt.Printf("%s:\t%s\n", k, v)
		}
		s := NewScrollArea()
		l := NewLabel()
		l.Contents = fmt.Sprintf("Info about '%s' goes here", r.Header.Get("DL-Info"))
		b := NewInputField()
		b.Type = "submit"
		b.HtmxFields = []template.HTMLAttr{
			`hx-post="/updateList"`,
			template.HTMLAttr(fmt.Sprintf(`hx-target="#%s"`, r.Header.Get("DL-ID"))),
			template.HTMLAttr(fmt.Sprintf(`hx-headers='{"DL-ID":"%s"}'`, r.Header.Get("DL-ID"))),
		}
		s.Contents = []Element{l, b}
		w.Header().Set("HX-Refresh", "false")
		err := templates.ExecuteTemplate(w, "Element", s)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	})
	//// Handle the delete action (simulated)
	//http.HandleFunc("/api/user/123", func(w http.ResponseWriter, r *http.Request) {
	//	if r.Method != http.MethodDelete {
	//		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	//		return
	//	}
	//	w.Write([]byte("User deleted successfully"))
	//})
	//http.HandleFunc("/api/selected", func(w http.ResponseWriter, r *http.Request) {
	//	// Ensure the request is a POST method
	//	if r.Method != http.MethodPost {
	//		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	//		return
	//	}
	//
	//	// Parse the form data
	//	err := r.ParseForm()
	//	if err != nil {
	//		http.Error(w, "Error parsing form data", http.StatusBadRequest)
	//		return
	//	}
	//
	//	// Get the selected value
	//	selectedValue := r.FormValue("row-select")
	//
	//	// Create the message
	//	message := fmt.Sprintf("Selected row: %s", selectedValue)
	//
	//	// Set the content type to text/html
	//	w.Header().Set("Content-Type", "text/html")
	//
	//	// Write the message as the response
	//	fmt.Fprint(w, message)
	//})

	// Start HTTP server
	log.Println("Starting server on :8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
