package main

import (
	"log"
	"net/http"
	"strings"
)

type customFileServer struct {
	http.Handler
}

func (fs *customFileServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Printf("Serving static file: %s", r.URL.Path)

	switch {
	case strings.HasSuffix(r.URL.Path, ".js"):
		w.Header().Set("Content-Type", "application/javascript")
	case strings.HasSuffix(r.URL.Path, ".css"):
		w.Header().Set("Content-Type", "text/css")
	case strings.HasSuffix(r.URL.Path, ".html"):
		w.Header().Set("Content-Type", "text/html")
	}

	fs.Handler.ServeHTTP(w, r)
}
