package main

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strings"
	"text/tabwriter"
)

const templatesPath = "templates/webApp/pico"

var hasCollisions bool

type Templates struct {
	Name     string
	Collides bool
	Paths    []string
}

func findTemplates(dir string) (map[string]Templates, error) {
	templates := make(map[string]Templates)
	defineRegex := regexp.MustCompile(`{{define\s+"([^"]+)"}}`)

	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() && strings.HasSuffix(strings.ToLower(path), ".gohtml") {
			content, err := os.ReadFile(path)
			if err != nil {
				return err
			}

			matches := defineRegex.FindAllSubmatch(content, -1)
			for _, match := range matches {
				name := string(match[1])
				if t, exists := templates[name]; exists {
					t.Paths = append(t.Paths, path)
					t.Collides = true
					hasCollisions = true
					templates[name] = t
				} else {
					templates[name] = Templates{
						Name:  name,
						Paths: []string{path},
					}
				}
			}
		}
		return nil
	})

	if err != nil {
		return nil, err
	}

	return templates, nil
}

func main() {
	dir := templatesPath // Change this to the directory you want to search
	templates, err := findTemplates(dir)
	if err != nil {
		fmt.Printf("Error finding templates: %v\n", err)
		return
	}

	// Create a sorted slice of template names
	var names []string
	for name := range templates {
		names = append(names, name)
	}
	sort.Strings(names)

	// Create a new tabwriter
	w := tabwriter.NewWriter(os.Stdout, 0, 0, 2, ' ', 0)

	// Print table header
	fmt.Fprintln(w, "Template Name\tCollides\tPaths")
	fmt.Fprintln(w, "-------------\t--------\t-----")

	// Print table rows in sorted order
	for _, name := range names {
		t := templates[name]
		paths := strings.Join(t.Paths, ", ")
		fmt.Fprintf(w, "%s\t%v\t%s\n", name, t.Collides, paths)
	}
	if hasCollisions {
		fmt.Fprintf(w, "\nWARNING: There are templates with the same name.\n")
	} else {
		fmt.Fprintf(w, "\nNo collisions found.\n")
	}

	// Flush the tabwriter to output
	w.Flush()
}
