package main

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
)

func main() {
	srcDirs := map[string]string{
		"static/css/doxa":          "xcmd/pico/source/static/css/doxa",
		"static/js/doxa":           "xcmd/pico/source/static/js/doxa",
		"templates/webApp/pico":    "xcmd/pico/source/templates",
		"templates/webApp/structs": "xcmd/pico/source/structs",
	}

	for src, dst := range srcDirs {
		if err := copyDirectory(src, dst); err != nil {
			fmt.Printf("Error copying %s to %s: %v\n", src, dst, err)
			continue
		}
		fmt.Printf("Successfully copied %s to %s\n", src, dst)
	}
}

// copyFile copies a single file from src to dst. If the destination file already exists, it will be overwritten.
func copyFile(src, dst string) error {
	sourceFile, err := os.Open(src)
	if err != nil {
		return err
	}
	defer sourceFile.Close()

	dstDir := filepath.Dir(dst)
	if err := os.MkdirAll(dstDir, os.ModePerm); err != nil {
		return err
	}

	destinationFile, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer destinationFile.Close()

	_, err = io.Copy(destinationFile, sourceFile)
	return err
}

// copyDirectory recursively copies a directory tree, attempting to preserve permissions.
func copyDirectory(srcDir, dstDir string) error {
	return filepath.Walk(srcDir, func(srcPath string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		// Calculate the destination path based on the source path.
		relPath, err := filepath.Rel(srcDir, srcPath)
		if err != nil {
			return err
		}
		dstPath := filepath.Join(dstDir, relPath)

		if info.IsDir() {
			return os.MkdirAll(dstPath, info.Mode())
		}

		err = copyFile(srcPath, dstPath)
		if err != nil {
			return err
		}
		return os.Chmod(dstPath, info.Mode())
	})
}
