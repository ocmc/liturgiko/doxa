package main

import (
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"golang.org/x/text/unicode/norm"
	"os"
	"regexp"
	"strings"
)

func main() {
	//	fileIn := "/Users/mac002/doxa/projects/doxa-alwb/exports/ltx/gr_gr_cog.tsv"
	fileIn := "/Users/mac002/doxa/projects/doxa-alwb/exports/ltx/gr_gr_cog/eu.lichrysbasil.tsv"
	lines, err := ltfile.GetFileLines(fileIn)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	externalPattern := "ἀγάπη"
	pattern := "(?i)Ἀγάπη"
	wholeWord := true
	useExternalPattern := false
	if useExternalPattern {
		pattern = externalPattern
	}
	fmt.Printf("NFD: %s\n", ltstring.ToNnd(pattern))
	//	fmt.Printf("NFKD: %s\n", norm.NFKD.String(pattern))

	fmt.Printf("ext: %U\nmac: %U\nnfc: %U\n", []rune(externalPattern), []rune(pattern), []rune(norm.NFC.String(pattern)))
	normalizePattern := true
	if normalizePattern {
		pattern = norm.NFC.String(pattern)
	}
	if wholeWord {
		pattern = fmt.Sprintf("%s(%s)%s", kvs.WordBoundary, pattern, kvs.WordBoundary)
	}
	valueRegEx, err := regexp.Compile(pattern)

	var containsCount int
	var matchCount int
	var nfcMatchCount int
	for _, l := range lines {
		if valueRegEx.Match([]byte(l)) {
			matchCount++
		}
		if valueRegEx.Match([]byte(norm.NFC.String(l))) {
			nfcMatchCount++
		}
		if strings.Contains(pattern, l) {
			containsCount++
		}
	}
	fmt.Printf("Pattern: %s\nWhole word? %v\nRegEx Matches: %d\nNFC Matches: %d\nContains: %d\n", pattern, wholeWord, matchCount, nfcMatchCount, containsCount)
}
