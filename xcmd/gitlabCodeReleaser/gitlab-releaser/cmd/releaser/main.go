package main

import (
	"archive/tar"
	"archive/zip"
	"bytes"
	"compress/gzip"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/fs"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"
)

// GitLab API URLs
const (
	apiBaseURL = "https://gitlab.com/api/v4"
)

// ReleaseRequest represents the payload for creating a release
type ReleaseRequest struct {
	Name        string `json:"name"`
	TagName     string `json:"tag_name"`
	Description string `json:"description"`
	Ref         string `json:"ref"`
}

// ReleaseAssetLink represents a link to include in the release
type ReleaseAssetLink struct {
	Name string `json:"name"`
	URL  string `json:"url"`
}

// AssetLinksRequest represents the payload for adding asset links to a release
type AssetLinksRequest struct {
	Links []ReleaseAssetLink `json:"links"`
}

// Configuration contains the application settings
type Configuration struct {
	ProjectID          string
	Token              string
	ReleaseName        string
	TagName            string
	Description        string
	Ref                string
	AssetLinksFilePath string
	AssetsDir          string
	SourceDir          string
	ExcludePaths       []string
	CreateArchive      bool
	ArchiveFormat      string
}

func main() {
	// Parse command-line arguments
	config := parseFlags()

	// Check if required fields are set
	if config.ProjectID == "" || config.Token == "" || config.TagName == "" {
		log.Fatal("Project ID, token, and tag name are required")
	}

	// Create filtered release archive if requested
	if config.CreateArchive {
		archivePath, err := createFilteredArchive(config)
		if err != nil {
			log.Fatalf("Failed to create filtered archive: %v", err)
		}
		fmt.Printf("Created filtered release archive at: %s\n", archivePath)
		
		// Add the archive to assets for upload if assets directory is set
		if config.AssetsDir != "" {
			// Copy the archive to the assets directory
			destPath := filepath.Join(config.AssetsDir, filepath.Base(archivePath))
			input, err := os.ReadFile(archivePath)
			if err != nil {
				log.Fatalf("Failed to read archive file: %v", err)
			}
			err = os.WriteFile(destPath, input, 0644)
			if err != nil {
				log.Fatalf("Failed to copy archive to assets directory: %v", err)
			}
			fmt.Printf("Copied archive to assets directory: %s\n", destPath)
		}
	}

	// Create the release
	releaseResponse, err := createRelease(config)
	if err != nil {
		log.Fatalf("Failed to create release: %v", err)
	}

	fmt.Printf("Release created successfully: %s\n", releaseResponse)

	// Add asset links if specified
	if config.AssetLinksFilePath != "" {
		err = addAssetLinks(config)
		if err != nil {
			log.Fatalf("Failed to add asset links: %v", err)
		}
		fmt.Println("Asset links added successfully")
	}

	// Upload assets if directory is specified
	if config.AssetsDir != "" {
		err = uploadAssets(config)
		if err != nil {
			log.Fatalf("Failed to upload assets: %v", err)
		}
		fmt.Println("Assets uploaded successfully")
	}

	fmt.Println("Release process completed successfully")
}

func parseFlags() Configuration {
	var config Configuration
	var excludePathsStr string

	flag.StringVar(&config.ProjectID, "project", "", "GitLab project ID (required)")
	flag.StringVar(&config.Token, "token", "", "GitLab personal access token (required)")
	flag.StringVar(&config.ReleaseName, "name", "", "Release name (optional, defaults to tag name)")
	flag.StringVar(&config.TagName, "tag", "", "Tag name (required)")
	flag.StringVar(&config.Description, "desc", "", "Release description (optional)")
	flag.StringVar(&config.Ref, "ref", "main", "Git reference (branch/commit) for the release (optional, defaults to 'main')")
	flag.StringVar(&config.AssetLinksFilePath, "links", "", "Path to JSON file containing asset links (optional)")
	flag.StringVar(&config.AssetsDir, "assets", "", "Directory containing assets to upload (optional)")
	flag.StringVar(&config.SourceDir, "source", ".", "Source directory for creating filtered archive (optional, defaults to current directory)")
	flag.StringVar(&excludePathsStr, "exclude", "", "Comma-separated list of paths to exclude (e.g., 'logs/,utils/')")
	flag.BoolVar(&config.CreateArchive, "create-archive", false, "Create a filtered archive of the source code")
	flag.StringVar(&config.ArchiveFormat, "archive-format", "zip", "Archive format (zip or tar.gz)")

	// Check for environment variables if not set via flags
	flag.Parse()

	if config.Token == "" {
		config.Token = os.Getenv("GITLAB_TOKEN")
	}

	if config.ProjectID == "" {
		config.ProjectID = os.Getenv("GITLAB_PROJECT_ID")
	}

	// Parse exclude paths if provided
	if excludePathsStr != "" {
		config.ExcludePaths = strings.Split(excludePathsStr, ",")
		// Trim whitespace
		for i := range config.ExcludePaths {
			config.ExcludePaths[i] = strings.TrimSpace(config.ExcludePaths[i])
		}
	}

	// If release name not specified, use tag name
	if config.ReleaseName == "" && config.TagName != "" {
		config.ReleaseName = "Release " + config.TagName
	}

	return config
}

func createRelease(config Configuration) (string, error) {
	url := fmt.Sprintf("%s/projects/%s/releases", apiBaseURL, config.ProjectID)

	releaseReq := ReleaseRequest{
		Name:        config.ReleaseName,
		TagName:     config.TagName,
		Description: config.Description,
		Ref:         config.Ref,
	}

	reqBody, err := json.Marshal(releaseReq)
	if err != nil {
		return "", err
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(reqBody))
	if err != nil {
		return "", err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("PRIVATE-TOKEN", config.Token)

	client := &http.Client{Timeout: 30 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		body, _ := io.ReadAll(resp.Body)
		return "", fmt.Errorf("API error: %s - %s", resp.Status, string(body))
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(body), nil
}

func addAssetLinks(config Configuration) error {
	// Read asset links file
	data, err := os.ReadFile(config.AssetLinksFilePath)
	if err != nil {
		return fmt.Errorf("failed to read asset links file: %v", err)
	}

	var assetLinks AssetLinksRequest
	if err := json.Unmarshal(data, &assetLinks); err != nil {
		return fmt.Errorf("failed to parse asset links JSON: %v", err)
	}

	url := fmt.Sprintf("%s/projects/%s/releases/%s/assets/links", apiBaseURL, config.ProjectID, config.TagName)

	// Add each asset link
	for _, link := range assetLinks.Links {
		linkData, err := json.Marshal(link)
		if err != nil {
			return err
		}

		req, err := http.NewRequest("POST", url, bytes.NewBuffer(linkData))
		if err != nil {
			return err
		}

		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("PRIVATE-TOKEN", config.Token)

		client := &http.Client{Timeout: 30 * time.Second}
		resp, err := client.Do(req)
		if err != nil {
			return err
		}
		defer resp.Body.Close()

		if resp.StatusCode >= 400 {
			body, _ := io.ReadAll(resp.Body)
			return fmt.Errorf("API error adding link %s: %s - %s", link.Name, resp.Status, string(body))
		}
	}

	return nil
}

func uploadAssets(config Configuration) error {
	// Read the assets directory
	files, err := os.ReadDir(config.AssetsDir)
	if err != nil {
		return fmt.Errorf("failed to read assets directory: %v", err)
	}

	// Upload each asset
	for _, file := range files {
		if file.IsDir() {
			continue // Skip directories
		}

		filePath := filepath.Join(config.AssetsDir, file.Name())
		err := uploadSingleAsset(config, filePath, file.Name())
		if err != nil {
			return fmt.Errorf("failed to upload asset %s: %v", file.Name(), err)
		}
		fmt.Printf("Uploaded asset: %s\n", file.Name())
	}

	return nil
}

// createFilteredArchive creates an archive of the source code while excluding specified paths
func createFilteredArchive(config Configuration) (string, error) {
	var archivePath string
	if config.ArchiveFormat == "tar.gz" {
		archivePath = fmt.Sprintf("%s-%s.tar.gz", filepath.Base(config.SourceDir), config.TagName)
		return createTarGzArchive(config, archivePath)
	} else {
		// Default to zip
		archivePath = fmt.Sprintf("%s-%s.zip", filepath.Base(config.SourceDir), config.TagName)
		return createZipArchive(config, archivePath)
	}
}

// shouldExclude checks if a path should be excluded based on the configuration
func shouldExclude(path string, config Configuration) bool {
	// Convert to slashed path for consistent comparison
	slashedPath := filepath.ToSlash(path)
	
	for _, excludePath := range config.ExcludePaths {
		// Convert exclude path to slashed path
		slashedExclude := filepath.ToSlash(excludePath)
		
		// If the exclude path ends with a slash, it's a directory exclusion
		if strings.HasSuffix(slashedExclude, "/") {
			if strings.HasPrefix(slashedPath, slashedExclude) || strings.HasPrefix(slashedPath, slashedExclude[:len(slashedExclude)-1]) {
				return true
			}
		} else {
			// Otherwise it's a file or specific path exclusion
			if slashedPath == slashedExclude || strings.HasPrefix(slashedPath, slashedExclude+"/") {
				return true
			}
		}
	}
	
	return false
}

// createZipArchive creates a ZIP archive of the source directory excluding specified paths
func createZipArchive(config Configuration, archivePath string) (string, error) {
	// Create a new zip file
	zipFile, err := os.Create(archivePath)
	if err != nil {
		return "", fmt.Errorf("failed to create zip file: %v", err)
	}
	defer zipFile.Close()

	// Create a new zip writer
	zipWriter := zip.NewWriter(zipFile)
	defer zipWriter.Close()

	// Walk through the source directory
	err = filepath.Walk(config.SourceDir, func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			return err
		}

		// Skip .git directory
		if info.IsDir() && info.Name() == ".git" {
			return filepath.SkipDir
		}

		// Get the relative path
		relPath, err := filepath.Rel(config.SourceDir, path)
		if err != nil {
			return fmt.Errorf("failed to get relative path: %v", err)
		}

		// Skip the root directory itself
		if relPath == "." {
			return nil
		}

		// Check if this path should be excluded
		if shouldExclude(relPath, config) {
			if info.IsDir() {
				return filepath.SkipDir
			}
			return nil
		}

		// Create a new zip header
		header, err := zip.FileInfoHeader(info)
		if err != nil {
			return fmt.Errorf("failed to create zip header: %v", err)
		}

		// Update the name to use relative path
		header.Name = relPath

		// Use Unix-style path separators
		header.Name = filepath.ToSlash(header.Name)

		// Set compression
		header.Method = zip.Deflate

		// Skip directories in the zip file - they're created implicitly
		if info.IsDir() {
			return nil
		}

		// Create a writer for this file
		writer, err := zipWriter.CreateHeader(header)
		if err != nil {
			return fmt.Errorf("failed to create zip file entry: %v", err)
		}

		// If this is a file, copy its contents
		file, err := os.Open(path)
		if err != nil {
			return fmt.Errorf("failed to open file: %v", err)
		}
		defer file.Close()

		_, err = io.Copy(writer, file)
		if err != nil {
			return fmt.Errorf("failed to write file to zip: %v", err)
		}

		return nil
	})

	if err != nil {
		return "", fmt.Errorf("failed to create zip archive: %v", err)
	}

	return archivePath, nil
}

// createTarGzArchive creates a tar.gz archive of the source directory excluding specified paths
func createTarGzArchive(config Configuration, archivePath string) (string, error) {
	// Create a new tar.gz file
	tarFile, err := os.Create(archivePath)
	if err != nil {
		return "", fmt.Errorf("failed to create tar.gz file: %v", err)
	}
	defer tarFile.Close()

	// Create a gzip writer
	gzWriter := gzip.NewWriter(tarFile)
	defer gzWriter.Close()

	// Create a tar writer
	tarWriter := tar.NewWriter(gzWriter)
	defer tarWriter.Close()

	// Walk through the source directory
	err = filepath.Walk(config.SourceDir, func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			return err
		}

		// Skip .git directory
		if info.IsDir() && info.Name() == ".git" {
			return filepath.SkipDir
		}

		// Get the relative path
		relPath, err := filepath.Rel(config.SourceDir, path)
		if err != nil {
			return fmt.Errorf("failed to get relative path: %v", err)
		}

		// Skip the root directory itself
		if relPath == "." {
			return nil
		}

		// Check if this path should be excluded
		if shouldExclude(relPath, config) {
			if info.IsDir() {
				return filepath.SkipDir
			}
			return nil
		}

		// Use Unix-style path separators
		relPath = filepath.ToSlash(relPath)

		// Skip directories in the tar file - they're created implicitly when needed
		if info.IsDir() {
			return nil
		}

		// Create a tar header
		header, err := tar.FileInfoHeader(info, "")
		if err != nil {
			return fmt.Errorf("failed to create tar header: %v", err)
		}

		// Update the name to use relative path
		header.Name = relPath

		// Write the header
		if err := tarWriter.WriteHeader(header); err != nil {
			return fmt.Errorf("failed to write tar header: %v", err)
		}

		// If this is a file, copy its contents
		if !info.IsDir() {
			file, err := os.Open(path)
			if err != nil {
				return fmt.Errorf("failed to open file: %v", err)
			}
			defer file.Close()

			_, err = io.Copy(tarWriter, file)
			if err != nil {
				return fmt.Errorf("failed to write file to tar: %v", err)
			}
		}

		return nil
	})

	if err != nil {
		return "", fmt.Errorf("failed to create tar.gz archive: %v", err)
	}

	return archivePath, nil
}

func uploadSingleAsset(config Configuration, filePath, fileName string) error {
	// Open the file
	file, err := os.Open(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	// Get the file info to determine size
	fileInfo, err := file.Stat()
	if err != nil {
		return err
	}

	// Create a new upload request
	url := fmt.Sprintf("%s/projects/%s/uploads", apiBaseURL, config.ProjectID)

	// Create a buffer to store the multipart form data
	var requestBody bytes.Buffer

	// Create a new multipart writer
	multipartWriter := http.MultipartWriter(&requestBody)

	// Add the file to the multipart form
	part, err := multipartWriter.CreateFormFile("file", fileName)
	if err != nil {
		return err
	}

	// Copy the file content to the form
	_, err = io.Copy(part, file)
	if err != nil {
		return err
	}

	// Close the multipart writer to set its boundaries
	if err := multipartWriter.Close(); err != nil {
		return err
	}

	// Create a new HTTP request
	req, err := http.NewRequest("POST", url, &requestBody)
	if err != nil {
		return err
	}

	// Set the appropriate headers
	req.Header.Set("Content-Type", multipartWriter.FormDataContentType())
	req.Header.Set("PRIVATE-TOKEN", config.Token)

	// Send the request
	client := &http.Client{Timeout: 60 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		body, _ := io.ReadAll(resp.Body)
		return fmt.Errorf("API error uploading file: %s - %s", resp.Status, string(body))
	}

	// Parse the response to get the upload URL
	var uploadResponse struct {
		URL string `json:"url"`
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if err := json.Unmarshal(body, &uploadResponse); err != nil {
		return err
	}

	// Now add this as a link to the release
	linkURL := fmt.Sprintf("%s/projects/%s/releases/%s/assets/links", apiBaseURL, config.ProjectID, config.TagName)
	
	// Create the link data
	linkData := map[string]string{
		"name": fileName,
		"url":  strings.TrimPrefix(uploadResponse.URL, "/"),
	}

	linkBytes, err := json.Marshal(linkData)
	if err != nil {
		return err
	}

	linkReq, err := http.NewRequest("POST", linkURL, bytes.NewBuffer(linkBytes))
	if err != nil {
		return err
	}

	linkReq.Header.Set("Content-Type", "application/json")
	linkReq.Header.Set("PRIVATE-TOKEN", config.Token)

	linkResp, err := client.Do(linkReq)
	if err != nil {
		return err
	}
	defer linkResp.Body.Close()

	if linkResp.StatusCode >= 400 {
		body, _ := io.ReadAll(linkResp.Body)
		return fmt.Errorf("API error linking uploaded file: %s - %s", linkResp.Status, string(body))
	}

	return nil
}
