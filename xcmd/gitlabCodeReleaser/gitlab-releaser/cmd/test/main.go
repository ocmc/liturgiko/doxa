package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"github.com/yourusername/gitlab-releaser/pkg/api"
	"github.com/yourusername/gitlab-releaser/pkg/archive"
	"github.com/yourusername/gitlab-releaser/pkg/config"
)

func main() {
	// Enable mock mode for GitLab API calls
	api.EnableMockMode()
	
	// Parse command-line arguments
	cfg := config.ParseFlags()
	
	// Add test-specific flags
	var verbose bool
	flag.BoolVar(&verbose, "verbose", false, "Enable verbose output for testing")
	flag.Parse()
	
	if verbose {
		log.Println("Running in test mode with mock GitLab API calls")
	}

	// Set token to test token if not provided
	if cfg.Token == "" {
		cfg.Token = "test-token"
	}
	
	// Set project ID to test ID if not provided
	if cfg.ProjectID == "" {
		cfg.ProjectID = "test-project"
	}

	// Check if required fields are set
	if !cfg.Validate() {
		log.Fatal("Project ID, token, and tag name are required")
	}

	// Create filtered release archive if requested
	if cfg.CreateArchive {
		log.Println("Creating filtered archive...")
		archivePath, err := archive.CreateFilteredArchive(cfg)
		if err != nil {
			log.Fatalf("Failed to create filtered archive: %v", err)
		}
		fmt.Printf("Created filtered release archive at: %s\n", archivePath)
		
		// Test the archive contents
		validateArchive(archivePath, cfg.ExcludePaths, verbose)
		
		// Add the archive to assets for upload if assets directory is set
		if cfg.AssetsDir != "" {
			// Create assets directory if it doesn't exist
			if err := os.MkdirAll(cfg.AssetsDir, 0755); err != nil {
				log.Fatalf("Failed to create assets directory: %v", err)
			}
			
			// Copy the archive to the assets directory
			destPath := filepath.Join(cfg.AssetsDir, filepath.Base(archivePath))
			input, err := os.ReadFile(archivePath)
			if err != nil {
				log.Fatalf("Failed to read archive file: %v", err)
			}
			err = os.WriteFile(destPath, input, 0644)
			if err != nil {
				log.Fatalf("Failed to copy archive to assets directory: %v", err)
			}
			fmt.Printf("Copied archive to assets directory: %s\n", destPath)
		}
	}

	// Mock creating the release
	log.Println("Testing release creation...")
	releaseResponse, err := api.CreateRelease(cfg)
	if err != nil {
		log.Fatalf("Failed to create release: %v", err)
	}

	fmt.Printf("Mock release created successfully: %s\n", releaseResponse)

	// Mock adding asset links if specified
	if cfg.AssetLinksFilePath != "" {
		log.Println("Testing asset links...")
		err = api.AddAssetLinks(cfg)
		if err != nil {
			log.Fatalf("Failed to add asset links: %v", err)
		}
		fmt.Println("Asset links test passed")
	}

	// Mock uploading assets if directory is specified
	if cfg.AssetsDir != "" {
		log.Println("Testing asset upload...")
		err = api.UploadAssets(cfg)
		if err != nil {
			log.Fatalf("Failed to upload assets: %v", err)
		}
		fmt.Println("Asset upload test passed")
	}

	fmt.Println("All tests completed successfully!")
}

// validateArchive checks if the excluded paths are actually excluded from the archive
func validateArchive(archivePath string, excludePaths []string, verbose bool) {
	// This is a placeholder for actual validation logic
	// In a real implementation, you would extract the archive and check its contents
	if verbose {
		fmt.Println("Archive validation would happen here")
		fmt.Println("Excluded paths:", excludePaths)
	}
}
