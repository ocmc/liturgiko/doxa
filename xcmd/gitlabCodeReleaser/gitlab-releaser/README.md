# GitLab Releaser

A comprehensive tool for creating GitLab releases with support for excluding specific directories.

## Features

- Create GitLab releases with tags
- Generate filtered archives that exclude specified directories (e.g., logs, utility code)
- Add asset links to releases
- Upload assets directly to GitLab
- Automatic changelog generation
- Full automation through a convenient bash script

## Directory Structure

```
gitlab-releaser/
├── .gitignore                        # Git ignore file
├── bin/                              # Compiled binaries
│   ├── gitlab-release                # Main binary
│   └── gitlab-release-test           # Test binary
├── cmd/                              # Command applications
│   ├── releaser/
│   │   └── main.go                   # Main entry point for the tool
│   └── test/
│       └── main.go                   # Test command entry point
├── go.mod                            # Go module file
├── go.sum                            # Go dependencies checksums
├── pkg/                              # Shared packages
│   ├── api/
│   │   ├── gitlab.go                 # GitLab API interaction code
│   │   └── mock.go                   # Mock API for testing
│   ├── archive/
│   │   ├── tar.go                    # TAR.GZ archive handling
│   │   └── zip.go                    # ZIP archive handling
│   └── config/
│       └── config.go                 # Configuration parsing
├── README.md                         # Documentation
└── scripts/
    ├── gitlab-release.sh             # Main bash script
    └── test-releaser.sh              # Testing bash script
```

## Installation

1. Clone the repository:
```bash
git clone https://github.com/yourusername/gitlab-releaser.git
cd gitlab-releaser
```

2. Build the tool:
```bash
go build -o bin/gitlab-release cmd/releaser/main.go
```

3. Make the script executable:
```bash
chmod +x scripts/gitlab-release.sh
```

## Usage

### Using the Go Tool Directly

```bash
./bin/gitlab-release \
  -project="12345" \
  -token="your_gitlab_token" \
  -tag="v1.0.0" \
  -desc="Release version 1.0.0" \
  -create-archive \
  -exclude="logs/,utils/" \
  -archive-format="zip"
```

### Using the Bash Script

```bash
./scripts/gitlab-release.sh v1.0.0 12345 your_gitlab_token "logs/,utils/"
```

## Example Use Case

Let's say we have a Go project with the following structure:

```
my-go-project/
├── cmd/
│   └── app/
│       └── main.go        # Main application entry point
├── internal/
│   └── service/
│       └── service.go     # Core business logic
├── pkg/
│   └── models/
│       └── models.go      # Data models
├── logs/                  # Directory we want to exclude
│   └── debug.log
├── utils/                 # Directory we want to exclude
│   └── scripts/
│       └── cleanup.sh
├── go.mod
└── go.sum
```

To create a release excluding the logs and utils directories:

```bash
# Copy the releaser to your project
cp ~/gitlab-releaser/bin/gitlab-release .
cp ~/gitlab-releaser/scripts/gitlab-release.sh .

# Make the script executable
chmod +x gitlab-release.sh

# Create a GitLab release
./gitlab-release.sh v1.2.0 12345 your_gitlab_token "logs/,utils/"
```

This will:
1. Create a filtered ZIP archive (`my-go-project-v1.2.0.zip`) without the logs/ and utils/ directories
2. Create a GitLab release with the tag v1.2.0
3. Upload the filtered archive as a release asset
4. Generate a changelog based on Git commits and upload it

## Environment Variables

You can use environment variables instead of command-line arguments:

```bash
export GITLAB_TOKEN="your_gitlab_token"
export GITLAB_PROJECT_ID="12345"
./gitlab-release.sh v1.0.0
```

## Integration with CI/CD

Add this to your `.gitlab-ci.yml` to automate releases on tags:

```yaml
release:
  stage: deploy
  script:
    - ./gitlab-release.sh $CI_COMMIT_TAG $CI_PROJECT_ID $GITLAB_TOKEN "logs/,utils/"
  only:
    - tags
```

## Testing

The GitLab Releaser includes a comprehensive testing system to verify functionality without making actual GitLab API calls.

### Using the Test Script

The simplest way to test is using the included bash script:

```bash
./scripts/test-releaser.sh
```

This script:
- Creates a test directory with sample files
- Sets up directories to exclude (logs/, utils/)
- Tests archive creation with directory exclusion
- Verifies that excluded directories are actually excluded
- Tests changelog generation

### Using the Test Binary

For more controlled testing:

```bash
# Build the test binary
go build -o bin/gitlab-release-test cmd/test/main.go

# Run a test with verbose output
./bin/gitlab-release-test \
  -tag="v0.0.0-test" \
  -create-archive \
  -exclude="logs/,utils/" \
  -verbose
```

### Using Environment Variables

You can also use the regular binary in test mode:

```bash
# Enable test mode
export GITLAB_TEST_MODE=true

# Use the regular binary with a test token
./bin/gitlab-release \
  -project="test-project" \
  -token="test-token" \
  -tag="v0.0.0-test" \
  -create-archive \
  -exclude="logs/,utils/"
```

## License

[MIT License](LICENSE)
