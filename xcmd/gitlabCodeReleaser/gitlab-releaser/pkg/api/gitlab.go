package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/yourusername/gitlab-releaser/pkg/config"
)

const (
	// apiBaseURL is the base URL for GitLab API v4
	apiBaseURL = "https://gitlab.com/api/v4"
)

// ReleaseRequest represents the payload for creating a release
type ReleaseRequest struct {
	Name        string `json:"name"`
	TagName     string `json:"tag_name"`
	Description string `json:"description"`
	Ref         string `json:"ref"`
}

// ReleaseAssetLink represents a link to include in the release
type ReleaseAssetLink struct {
	Name string `json:"name"`
	URL  string `json:"url"`
}

// AssetLinksRequest represents the payload for adding asset links to a release
type AssetLinksRequest struct {
	Links []ReleaseAssetLink `json:"links"`
}

// CreateRelease creates a new GitLab release
func CreateRelease(cfg *config.Configuration) (string, error) {
	url := fmt.Sprintf("%s/projects/%s/releases", apiBaseURL, cfg.ProjectID)

	releaseReq := ReleaseRequest{
		Name:        cfg.ReleaseName,
		TagName:     cfg.TagName,
		Description: cfg.Description,
		Ref:         cfg.Ref,
	}

	reqBody, err := json.Marshal(releaseReq)
	if err != nil {
		return "", err
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(reqBody))
	if err != nil {
		return "", err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("PRIVATE-TOKEN", cfg.Token)

	client := &http.Client{Timeout: 30 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		body, _ := io.ReadAll(resp.Body)
		return "", fmt.Errorf("API error: %s - %s", resp.Status, string(body))
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(body), nil
}

// AddAssetLinks adds asset links to a GitLab release
func AddAssetLinks(cfg *config.Configuration) error {
	// Read asset links file
	data, err := os.ReadFile(cfg.AssetLinksFilePath)
	if err != nil {
		return fmt.Errorf("failed to read asset links file: %v", err)
	}

	var assetLinks AssetLinksRequest
	if err := json.Unmarshal(data, &assetLinks); err != nil {
		return fmt.Errorf("failed to parse asset links JSON: %v", err)
	}

	url := fmt.Sprintf("%s/projects/%s/releases/%s/assets/links", apiBaseURL, cfg.ProjectID, cfg.TagName)

	// Add each asset link
	for _, link := range assetLinks.Links {
		linkData, err := json.Marshal(link)
		if err != nil {
			return err
		}

		req, err := http.NewRequest("POST", url, bytes.NewBuffer(linkData))
		if err != nil {
			return err
		}

		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("PRIVATE-TOKEN", cfg.Token)

		client := &http.Client{Timeout: 30 * time.Second}
		resp, err := client.Do(req)
		if err != nil {
			return err
		}
		defer resp.Body.Close()

		if resp.StatusCode >= 400 {
			body, _ := io.ReadAll(resp.Body)
			return fmt.Errorf("API error adding link %s: %s - %s", link.Name, resp.Status, string(body))
		}
	}

	return nil
}

// UploadAssets uploads assets to GitLab and attaches them to the release
func UploadAssets(cfg *config.Configuration) error {
	// Read the assets directory
	files, err := os.ReadDir(cfg.AssetsDir)
	if err != nil {
		return fmt.Errorf("failed to read assets directory: %v", err)
	}

	// Upload each asset
	for _, file := range files {
		if file.IsDir() {
			continue // Skip directories
		}

		filePath := filepath.Join(cfg.AssetsDir, file.Name())
		err := UploadSingleAsset(cfg, filePath, file.Name())
		if err != nil {
			return fmt.Errorf("failed to upload asset %s: %v", file.Name(), err)
		}
		fmt.Printf("Uploaded asset: %s\n", file.Name())
	}

	return nil
}

// UploadSingleAsset uploads a single asset to GitLab and attaches it to the release
func UploadSingleAsset(cfg *config.Configuration, filePath, fileName string) error {
	// Open the file
	file, err := os.Open(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	// Get the file info to determine size
	fileInfo, err := file.Stat()
	if err != nil {
		return err
	}

	// Create a new upload request
	url := fmt.Sprintf("%s/projects/%s/uploads", apiBaseURL, cfg.ProjectID)

	// Create a buffer to store the multipart form data
	var requestBody bytes.Buffer

	// Create a new multipart writer
	multipartWriter := http.NewWriter(&requestBody)

	// Add the file to the multipart form
	part, err := multipartWriter.CreateFormFile("file", fileName)
	if err != nil {
		return err
	}

	// Copy the file content to the form
	_, err = io.Copy(part, file)
	if err != nil {
		return err
	}

	// Close the multipart writer to set its boundaries
	if err := multipartWriter.Close(); err != nil {
		return err
	}

	// Create a new HTTP request
	req, err := http.NewRequest("POST", url, &requestBody)
	if err != nil {
		return err
	}

	// Set the appropriate headers
	req.Header.Set("Content-Type", multipartWriter.FormDataContentType())
	req.Header.Set("PRIVATE-TOKEN", cfg.Token)

	// Send the request
	client := &http.Client{Timeout: 60 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		body, _ := io.ReadAll(resp.Body)
		return fmt.Errorf("API error uploading file: %s - %s", resp.Status, string(body))
	}

	// Parse the response to get the upload URL
	var uploadResponse struct {
		URL string `json:"url"`
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if err := json.Unmarshal(body, &uploadResponse); err != nil {
		return err
	}

	// Now add this as a link to the release
	linkURL := fmt.Sprintf("%s/projects/%s/releases/%s/assets/links", apiBaseURL, cfg.ProjectID, cfg.TagName)
	
	// Create the link data
	linkData := map[string]string{
		"name": fileName,
		"url":  strings.TrimPrefix(uploadResponse.URL, "/"),
	}

	linkBytes, err := json.Marshal(linkData)
	if err != nil {
		return err
	}

	linkReq, err := http.NewRequest("POST", linkURL, bytes.NewBuffer(linkBytes))
	if err != nil {
		return err
	}

	linkReq.Header.Set("Content-Type", "application/json")
	linkReq.Header.Set("PRIVATE-TOKEN", cfg.Token)

	linkResp, err := client.Do(linkReq)
	if err != nil {
		return err
	}
	defer linkResp.Body.Close()

	if linkResp.StatusCode >= 400 {
		body, _ := io.ReadAll(linkResp.Body)
		return fmt.Errorf("API error linking uploaded file: %s - %s", linkResp.Status, string(body))
	}

	return nil
}
