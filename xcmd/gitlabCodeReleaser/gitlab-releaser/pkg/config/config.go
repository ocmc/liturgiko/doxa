package config

import (
	"flag"
	"os"
	"strings"
)

// Configuration contains the application settings
type Configuration struct {
	ProjectID          string
	Token              string
	ReleaseName        string
	TagName            string
	Description        string
	Ref                string
	AssetLinksFilePath string
	AssetsDir          string
	SourceDir          string
	ExcludePaths       []string
	CreateArchive      bool
	ArchiveFormat      string
}

// ParseFlags parses command-line arguments and environment variables
func ParseFlags() *Configuration {
	var config Configuration
	var excludePathsStr string

	flag.StringVar(&config.ProjectID, "project", "", "GitLab project ID (required)")
	flag.StringVar(&config.Token, "token", "", "GitLab personal access token (required)")
	flag.StringVar(&config.ReleaseName, "name", "", "Release name (optional, defaults to tag name)")
	flag.StringVar(&config.TagName, "tag", "", "Tag name (required)")
	flag.StringVar(&config.Description, "desc", "", "Release description (optional)")
	flag.StringVar(&config.Ref, "ref", "main", "Git reference (branch/commit) for the release (optional, defaults to 'main')")
	flag.StringVar(&config.AssetLinksFilePath, "links", "", "Path to JSON file containing asset links (optional)")
	flag.StringVar(&config.AssetsDir, "assets", "", "Directory containing assets to upload (optional)")
	flag.StringVar(&config.SourceDir, "source", ".", "Source directory for creating filtered archive (optional, defaults to current directory)")
	flag.StringVar(&excludePathsStr, "exclude", "", "Comma-separated list of paths to exclude (e.g., 'logs/,utils/')")
	flag.BoolVar(&config.CreateArchive, "create-archive", false, "Create a filtered archive of the source code")
	flag.StringVar(&config.ArchiveFormat, "archive-format", "zip", "Archive format (zip or tar.gz)")

	// Check for environment variables if not set via flags
	flag.Parse()

	if config.Token == "" {
		config.Token = os.Getenv("GITLAB_TOKEN")
	}

	if config.ProjectID == "" {
		config.ProjectID = os.Getenv("GITLAB_PROJECT_ID")
	}

	// Parse exclude paths if provided
	if excludePathsStr != "" {
		config.ExcludePaths = strings.Split(excludePathsStr, ",")
		// Trim whitespace
		for i := range config.ExcludePaths {
			config.ExcludePaths[i] = strings.TrimSpace(config.ExcludePaths[i])
		}
	}

	// If release name not specified, use tag name
	if config.ReleaseName == "" && config.TagName != "" {
		config.ReleaseName = "Release " + config.TagName
	}

	return &config
}

// Validate checks if the configuration is valid
func (c *Configuration) Validate() bool {
	return c.ProjectID != "" && c.Token != "" && c.TagName != ""
}
