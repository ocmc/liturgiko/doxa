package archive

import (
	"archive/zip"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"

	"github.com/yourusername/gitlab-releaser/pkg/config"
)

// CreateZipArchive creates a ZIP archive of the source directory excluding specified paths
func CreateZipArchive(cfg *config.Configuration, archivePath string) (string, error) {
	// Create a new zip file
	zipFile, err := os.Create(archivePath)
	if err != nil {
		return "", fmt.Errorf("failed to create zip file: %v", err)
	}
	defer zipFile.Close()

	// Create a new zip writer
	zipWriter := zip.NewWriter(zipFile)
	defer zipWriter.Close()

	// Walk through the source directory
	err = filepath.Walk(cfg.SourceDir, func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			return err
		}

		// Skip .git directory
		if info.IsDir() && info.Name() == ".git" {
			return filepath.SkipDir
		}

		// Get the relative path
		relPath, err := filepath.Rel(cfg.SourceDir, path)
		if err != nil {
			return fmt.Errorf("failed to get relative path: %v", err)
		}

		// Skip the root directory itself
		if relPath == "." {
			return nil
		}

		// Check if this path should be excluded
		if shouldExclude(relPath, cfg) {
			if info.IsDir() {
				return filepath.SkipDir
			}
			return nil
		}

		// Create a new zip header
		header, err := zip.FileInfoHeader(info)
		if err != nil {
			return fmt.Errorf("failed to create zip header: %v", err)
		}

		// Update the name to use relative path
		header.Name = relPath

		// Use Unix-style path separators
		header.Name = filepath.ToSlash(header.Name)

		// Set compression
		header.Method = zip.Deflate

		// Skip directories in the zip file - they're created implicitly
		if info.IsDir() {
			return nil
		}

		// Create a writer for this file
		writer, err := zipWriter.CreateHeader(header)
		if err != nil {
			return fmt.Errorf("failed to create zip file entry: %v", err)
		}

		// If this is a file, copy its contents
		file, err := os.Open(path)
		if err != nil {
			return fmt.Errorf("failed to open file: %v", err)
		}
		defer file.Close()

		_, err = io.Copy(writer, file)
		if err != nil {
			return fmt.Errorf("failed to write file to zip: %v", err)
		}

		return nil
	})

	if err != nil {
		return "", fmt.Errorf("failed to create zip archive: %v", err)
	}

	return archivePath, nil
}
