package archive

import (
	"archive/tar"
	"compress/gzip"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"strings"

	"github.com/yourusername/gitlab-releaser/pkg/config"
)

// CreateTarGzArchive creates a tar.gz archive of the source directory excluding specified paths
func CreateTarGzArchive(cfg *config.Configuration, archivePath string) (string, error) {
	// Create a new tar.gz file
	tarFile, err := os.Create(archivePath)
	if err != nil {
		return "", fmt.Errorf("failed to create tar.gz file: %v", err)
	}
	defer tarFile.Close()

	// Create a gzip writer
	gzWriter := gzip.NewWriter(tarFile)
	defer gzWriter.Close()

	// Create a tar writer
	tarWriter := tar.NewWriter(gzWriter)
	defer tarWriter.Close()

	// Walk through the source directory
	err = filepath.Walk(cfg.SourceDir, func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			return err
		}

		// Skip .git directory
		if info.IsDir() && info.Name() == ".git" {
			return filepath.SkipDir
		}

		// Get the relative path
		relPath, err := filepath.Rel(cfg.SourceDir, path)
		if err != nil {
			return fmt.Errorf("failed to get relative path: %v", err)
		}

		// Skip the root directory itself
		if relPath == "." {
			return nil
		}

		// Check if this path should be excluded
		if shouldExclude(relPath, cfg) {
			if info.IsDir() {
				return filepath.SkipDir
			}
			return nil
		}

		// Use Unix-style path separators
		relPath = filepath.ToSlash(relPath)

		// Skip directories in the tar file - they're created implicitly when needed
		if info.IsDir() {
			return nil
		}

		// Create a tar header
		header, err := tar.FileInfoHeader(info, "")
		if err != nil {
			return fmt.Errorf("failed to create tar header: %v", err)
		}

		// Update the name to use relative path
		header.Name = relPath

		// Write the header
		if err := tarWriter.WriteHeader(header); err != nil {
			return fmt.Errorf("failed to write tar header: %v", err)
		}

		// If this is a file, copy its contents
		if !info.IsDir() {
			file, err := os.Open(path)
			if err != nil {
				return fmt.Errorf("failed to open file: %v", err)
			}
			defer file.Close()

			_, err = io.Copy(tarWriter, file)
			if err != nil {
				return fmt.Errorf("failed to write file to tar: %v", err)
			}
		}

		return nil
	})

	if err != nil {
		return "", fmt.Errorf("failed to create tar.gz archive: %v", err)
	}

	return archivePath, nil
}

// shouldExclude checks if a path should be excluded based on the configuration
func shouldExclude(path string, cfg *config.Configuration) bool {
	// Convert to slashed path for consistent comparison
	slashedPath := filepath.ToSlash(path)
	
	for _, excludePath := range cfg.ExcludePaths {
		// Convert exclude path to slashed path
		slashedExclude := filepath.ToSlash(excludePath)
		
		// If the exclude path ends with a slash, it's a directory exclusion
		if strings.HasSuffix(slashedExclude, "/") {
			if strings.HasPrefix(slashedPath, slashedExclude) || strings.HasPrefix(slashedPath, slashedExclude[:len(slashedExclude)-1]) {
				return true
			}
		} else {
			// Otherwise it's a file or specific path exclusion
			if slashedPath == slashedExclude || strings.HasPrefix(slashedPath, slashedExclude+"/") {
				return true
			}
		}
	}
	
	return false
}

// CreateFilteredArchive creates an archive of the source code while excluding specified paths
func CreateFilteredArchive(cfg *config.Configuration) (string, error) {
	var archivePath string
	if cfg.ArchiveFormat == "tar.gz" {
		archivePath = fmt.Sprintf("%s-%s.tar.gz", filepath.Base(cfg.SourceDir), cfg.TagName)
		return CreateTarGzArchive(cfg, archivePath)
	} else {
		// Default to zip
		archivePath = fmt.Sprintf("%s-%s.zip", filepath.Base(cfg.SourceDir), cfg.TagName)
		return CreateZipArchive(cfg, archivePath)
	}
}
