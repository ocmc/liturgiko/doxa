#!/bin/bash
# test-releaser.sh - Test the GitLab Releaser without creating actual GitLab releases
# Usage: ./test-releaser.sh

set -e  # Exit on any error

# Test configuration
TEST_DIR="test-releaser-$(date +%s)"
TEST_TAG="v0.0.0-test"
TEST_PROJECT_ID="0000000"
TEST_TOKEN="test-token"
EXCLUDE_PATHS="logs/,utils/"

# Create test directory structure
echo "Creating test directory structure in '$TEST_DIR'..."
mkdir -p "$TEST_DIR/src/cmd/app"
mkdir -p "$TEST_DIR/src/internal/service"
mkdir -p "$TEST_DIR/src/pkg/models"
mkdir -p "$TEST_DIR/src/logs"
mkdir -p "$TEST_DIR/src/utils/scripts"

# Create test files
echo 'package main' > "$TEST_DIR/src/cmd/app/main.go"
echo 'func main() {}' >> "$TEST_DIR/src/cmd/app/main.go"

echo 'package service' > "$TEST_DIR/src/internal/service/service.go"
echo 'func DoSomething() {}' >> "$TEST_DIR/src/internal/service/service.go"

echo 'package models' > "$TEST_DIR/src/pkg/models/models.go"
echo 'type User struct { ID int }' >> "$TEST_DIR/src/pkg/models/models.go"

echo 'DEBUG: Test log entry' > "$TEST_DIR/src/logs/debug.log"
echo '#!/bin/bash' > "$TEST_DIR/src/utils/scripts/cleanup.sh"
echo 'echo "Cleanup complete"' >> "$TEST_DIR/src/utils/scripts/cleanup.sh"

# Create go.mod
echo 'module test.project' > "$TEST_DIR/src/go.mod"
echo 'go 1.19' >> "$TEST_DIR/src/go.mod"

# Initialize Git repository for changelog testing
cd "$TEST_DIR/src"
git init
git config --local user.email "test@example.com"
git config --local user.name "Test User"
git add .
git commit -m "Initial commit"
git tag -a "$TEST_TAG" -m "Test tag"

# Check if the GitLab Releaser exists
RELEASE_TOOL="../gitlab-release"
if [ ! -f "$RELEASE_TOOL" ]; then
    echo "Building GitLab release tool..."
    cd ../..
    
    # Check where we are in the directory structure
    if [ -d "cmd/releaser" ]; then
        # We're in the project root
        go build -o "$TEST_DIR/gitlab-release" ./cmd/releaser
    elif [ -f "cmd/releaser/main.go" ]; then
        # Source available but not in root
        go build -o "$TEST_DIR/gitlab-release" ./cmd/releaser/main.go
    elif [ -f "main.go" ]; then
        # Single file mode
        go build -o "$TEST_DIR/gitlab-release" main.go
    else
        echo "Error: Could not find source code to build the release tool"
        exit 1
    fi
    
    cd "$TEST_DIR/src"
    RELEASE_TOOL="../gitlab-release"
fi

echo "===== Testing Archive Generation ====="
# Mock GitLab API calls (the tool will attempt to make API calls even in test mode)
function mock_api() {
    echo "MOCK: Would send API request to GitLab"
    return 0
}

# Test creating filtered archive
echo "Testing archive creation with excluded paths..."
"$RELEASE_TOOL" \
    -project="$TEST_PROJECT_ID" \
    -token="$TEST_TOKEN" \
    -tag="$TEST_TAG" \
    -create-archive \
    -exclude="$EXCLUDE_PATHS" || true

# Verify archive was created
ARCHIVE_NAME="../src-$TEST_TAG.zip"
if [ -f "$ARCHIVE_NAME" ]; then
    echo "✅ Archive created successfully: $ARCHIVE_NAME"
    
    # List contents to verify exclusions
    echo "Verifying archive contents..."
    if command -v unzip &> /dev/null; then
        # Create a directory for extraction
        EXTRACT_DIR="../extract-test"
        mkdir -p "$EXTRACT_DIR"
        
        # Extract the archive
        unzip -q "$ARCHIVE_NAME" -d "$EXTRACT_DIR"
        
        # Check if excluded directories exist in the archive
        if [ -d "$EXTRACT_DIR/logs" ] || [ -d "$EXTRACT_DIR/utils" ]; then
            echo "❌ Test failed: Excluded directories were found in the archive"
            if [ -d "$EXTRACT_DIR/logs" ]; then
                echo "   - logs/ directory was not excluded"
            fi
            if [ -d "$EXTRACT_DIR/utils" ]; then
                echo "   - utils/ directory was not excluded"
            fi
        else
            echo "✅ Test passed: Excluded directories were not found in the archive"
            
            # Verify that included directories exist
            if [ -d "$EXTRACT_DIR/cmd" ] && [ -d "$EXTRACT_DIR/internal" ] && [ -d "$EXTRACT_DIR/pkg" ]; then
                echo "✅ Test passed: Included directories were found in the archive"
            else
                echo "❌ Test failed: Some included directories were missing from the archive"
            fi
        fi
    else
        echo "⚠️ Warning: 'unzip' command not found. Cannot verify archive contents."
    fi
else
    echo "❌ Test failed: Archive was not created"
fi

# Generate a changelog
echo "Testing changelog generation..."
ASSETS_DIR="../assets-$TEST_TAG"
mkdir -p "$ASSETS_DIR"
CHANGELOG_FILE="$ASSETS_DIR/CHANGELOG-$TEST_TAG.md"

echo "# Changelog for $TEST_TAG" > "$CHANGELOG_FILE"
echo "" >> "$CHANGELOG_FILE"
echo "Changes in this release:" >> "$CHANGELOG_FILE"
git log --pretty=format:"* %s" >> "$CHANGELOG_FILE"
echo "" >> "$CHANGELOG_FILE"
echo "Generated on $(date)" >> "$CHANGELOG_FILE"

if [ -f "$CHANGELOG_FILE" ]; then
    echo "✅ Changelog generated successfully: $CHANGELOG_FILE"
else
    echo "❌ Test failed: Changelog was not generated"
fi

# Clean up
echo "===== Test Summary ====="
echo "Test directory: $TEST_DIR"
echo "Archive: $ARCHIVE_NAME"
echo "Changelog: $CHANGELOG_FILE"

echo "Run the following command to clean up the test environment:"
echo "rm -rf $TEST_DIR"
