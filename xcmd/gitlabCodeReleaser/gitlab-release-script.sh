#!/bin/bash
# gitlab-release.sh - Script to create GitLab releases with directory exclusions
# Usage: ./gitlab-release.sh <tag> <project_id> <token> [exclude_paths]

set -e  # Exit on any error

# Parse command line arguments
TAG="$1"
PROJECT_ID="$2"
TOKEN="$3"
EXCLUDE_PATHS="${4:-logs/,utils/}"  # Default exclusions if not provided

# Validate required arguments
if [ -z "$TAG" ] || [ -z "$PROJECT_ID" ] || [ -z "$TOKEN" ]; then
    echo "Error: Missing required arguments"
    echo "Usage: ./gitlab-release.sh <tag> <project_id> <token> [exclude_paths]"
    echo "Example: ./gitlab-release.sh v1.2.0 12345 glpat-XXXXX logs/,utils/"
    exit 1
fi

# Prepare directories
RELEASE_DIR="release-$TAG"
ASSETS_DIR="assets-$TAG"
SOURCE_DIR="."
RELEASE_DESCRIPTION="Release $TAG created on $(date)"
RELEASE_NAME="Release $TAG"

# Create directories if they don't exist
mkdir -p "$ASSETS_DIR"

echo "===== GitLab Release Process for $TAG ====="
echo "Project ID: $PROJECT_ID"
echo "Excluding: $EXCLUDE_PATHS"

# Check if the Go release tool exists
RELEASE_TOOL="./gitlab-release"
if [ ! -f "$RELEASE_TOOL" ]; then
    echo "Building GitLab release tool..."
    go build -o "$RELEASE_TOOL" main.go
    chmod +x "$RELEASE_TOOL"
fi

# 1. Create a filtered archive
echo ""
echo "Step 1: Creating filtered source archive..."
"$RELEASE_TOOL" \
    -project="$PROJECT_ID" \
    -token="$TOKEN" \
    -tag="$TAG" \
    -create-archive \
    -exclude="$EXCLUDE_PATHS" \
    -archive-format="zip"

# Move the archive to the assets directory
ARCHIVE_NAME="$(basename "$SOURCE_DIR")-$TAG.zip"
if [ -f "$ARCHIVE_NAME" ]; then
    mv "$ARCHIVE_NAME" "$ASSETS_DIR/"
    echo "Created archive: $ASSETS_DIR/$ARCHIVE_NAME"
else
    echo "Warning: Archive file not found!"
fi

# 2. Create the release in GitLab
echo ""
echo "Step 2: Creating GitLab release..."
"$RELEASE_TOOL" \
    -project="$PROJECT_ID" \
    -token="$TOKEN" \
    -tag="$TAG" \
    -name="$RELEASE_NAME" \
    -desc="$RELEASE_DESCRIPTION" \
    -ref="main"

# 3. Upload the assets
echo ""
echo "Step 3: Uploading release assets..."
"$RELEASE_TOOL" \
    -project="$PROJECT_ID" \
    -token="$TOKEN" \
    -tag="$TAG" \
    -assets="$ASSETS_DIR"

# 4. Create a changelog file if git log is available
if command -v git &> /dev/null && [ -d ".git" ]; then
    echo ""
    echo "Step 4: Generating changelog..."
    CHANGELOG_FILE="$ASSETS_DIR/CHANGELOG-$TAG.md"
    
    # Get the previous tag
    PREV_TAG=$(git describe --tags --abbrev=0 HEAD^ 2>/dev/null || echo "")
    
    echo "# Changelog for $TAG" > "$CHANGELOG_FILE"
    echo "" >> "$CHANGELOG_FILE"
    
    if [ -n "$PREV_TAG" ]; then
        echo "Changes since $PREV_TAG:" >> "$CHANGELOG_FILE"
        git log --pretty=format:"* %s" "$PREV_TAG..HEAD" >> "$CHANGELOG_FILE"
    else
        echo "Changes in this release:" >> "$CHANGELOG_FILE"
        git log --pretty=format:"* %s" >> "$CHANGELOG_FILE"
    fi
    
    echo "" >> "$CHANGELOG_FILE"
    echo "" >> "$CHANGELOG_FILE"
    echo "Generated on $(date)" >> "$CHANGELOG_FILE"
    
    # Upload the changelog
    echo "Uploading changelog..."
    "$RELEASE_TOOL" \
        -project="$PROJECT_ID" \
        -token="$TOKEN" \
        -tag="$TAG" \
        -assets="$ASSETS_DIR"
fi

echo ""
echo "===== Release process completed successfully ====="
echo "Release: $TAG"
echo "Project: $PROJECT_ID"
echo "Assets: $ASSETS_DIR"
echo ""
echo "You can view your release at:"
echo "https://gitlab.com/projects/$PROJECT_ID/releases"
