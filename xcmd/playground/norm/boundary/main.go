package main

import (
	"fmt"
	"golang.org/x/text/unicode/norm"
	"strconv"
)

func main() {
	runeLiteral := '\u1f00'
	fmt.Printf("string(rune) rune literal %s %q\n", string(runeLiteral), strconv.QuoteRuneToASCII(runeLiteral))
	quoted := strconv.QuoteRuneToASCII('ἀ') // quoted = "'\u554a'"
	unquoted := quoted[1 : len(quoted)-1]   // unquoted = "\u554a"
	fmt.Println(unquoted)
	//you := "ἀγάπη"
	//YOU := strings.ToUpper(you)
	//fmt.Println(YOU)
	//acute := "'\u0301'"
	//r, err := strconv.Unquote(acute)
	//fmt.Printf("%s, %v\n", r, err)
	s := norm.NFD.String("α")
	fmt.Printf("%T\n", s[0])

	u := []rune(s)
	fmt.Println(u)
	//s := norm.NFD.String("ἀ")
	indexes := Boundaries(s, norm.NFD)
	fmt.Printf("len(s): %d len(indexes) %d %v\n", len(s), len(indexes), indexes)
	for i, d := range indexes {
		if i < len(indexes)-1 {
			fmt.Printf("%d %[2]s: %+[2]q\n", d, s[d:indexes[i+1]])
		} else {
			fmt.Printf("%d %[2]s: %+[2]q\n", d, s[d:])
		}
	}
}
func Boundaries(s string, f norm.Form) (indexes []int) {
	indexes = append(indexes, 0) // 1st is always a boundary
	l := len(s)
	for i := 0; i < l; {
		d := f.NextBoundaryInString(s[i:], true)
		i += d
		if i < l {
			indexes = append(indexes, i)
		}
	}
	return indexes
}
