package main

import (
	"fmt"
	"golang.org/x/text/unicode/norm"
	"regexp"
)

const (
	IgnoreCase = `(?i)`
	AnyUnicode = `\p{L}(\p{M})*`
	AnyAlpha   = `\x{03B1}(\p{M})*`
	AnyEpsilon = `\x{03B5}(\p{M})*`
	AnyEta     = `\x{03B7}(\p{M})*`
	AnyIota    = `\x{03B9}(\p{M})*`
	AnyUpsilon = `\x{03C5}(\p{M})*`
	AnyOmicron = `\x{03BF}(\p{M})*`
	AnyRho     = `\x{03C1}(\p{M})*`
	AnyOmega   = `\x{03C9}(\p{M})*`
)

func main() {
	//	r := regexp.MustCompile(AnyUnicode) // any unicode and composing
	//	r := regexp.MustCompile(AnyAlpha)   // any unicode and composing
	//r := regexp.MustCompile(AnyUpsilon) // any unicode and composing
	//r := regexp.MustCompile(AnyEpsilon) // any unicode and composing
	//r := regexp.MustCompile(AnyEta) // any unicode and composing
	//r := regexp.MustCompile(AnyOmicron) // any unicode and composing
	//r := regexp.MustCompile(AnyRho) // any unicode and composing
	//r := regexp.MustCompile(AnyOmega) // any unicode and composing
	r := regexp.MustCompile("" + AnyAlpha) // any unicode and composing
	//	r := regexp.MustCompile(`\x{03B1}(\p{M})*`)
	//	r := regexp.MustCompile(`(?:\x{03B1}\p{M})+`)
	//	r := regexp.MustCompile(`[\x{1F00}-\x{1F07}]`)
	//	s := norm.NFD.String("ἀγάπηαᾶ")
	s := norm.NFD.String("ἈἀαᾶὶῖἰίἴυύὐεἐἐὲἔΕηἠήἦοὀόὸὁρῤῶὼὤῳ")
	matches := r.FindAllString(s, -1)
	for _, m := range matches {
		fmt.Printf("%s\n", m)
	}
	indexes := r.FindAllStringIndex(s, -1)
	fmt.Printf("%s\n%v\n", s, indexes)
	s = norm.NFC.String("ἈἀαᾶὶῖἰίἴυύὐεἐἐὲἔΕηἠήἦοὀόὸὁρῤῶὼὤῳ")
	matches = r.FindAllString(s, -1)
	for _, m := range matches {
		fmt.Printf("%s\n", m)
	}
	indexes = r.FindAllStringIndex(s, -1)
	fmt.Printf("%s\n%v\n", s, indexes)
}
