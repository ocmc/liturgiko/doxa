package main

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"unicode/utf8"
)

func main() {
	s := ltstring.ToUnicodeNFD(`ῷ`)
	fmt.Printf("len(%s) == %d\n", s, len(s))
	fmt.Println("hex: ")
	for i := 0; i < len(s); i++ {
		fmt.Printf("%s %x %U\n", string(s[i]), s[i], []rune(s))
	}
	fmt.Println()
	fmt.Printf("utf8.RuneCountInString(%s) == %d\n", s, utf8.RuneCountInString(s))
	s2 := "ὁ Θεὸς ἠγάπησεν ἡμᾶς"
	fmt.Printf("len(%s) == %d\n", s2, len(s2))
	fmt.Printf("utf8.RuneCountInString(%s) == %d\n", s2, utf8.RuneCountInString(s2))
	s2c := ltstring.ToUnicodeNFC(s2)
	fmt.Printf("nfc len(%s) == %d\n", s2c, len(s2c))
	fmt.Printf("nfc utf8.RuneCountInString(%s) == %d\n", s2c, utf8.RuneCountInString(s2c))
	s2d := ltstring.ToUnicodeNFD(s2)
	fmt.Printf("nfd len(%s) == %d\n", s2d, len(s2d))
	fmt.Printf("nfd utf8.RuneCountInString(%s) == %d\n", s2d, utf8.RuneCountInString(s2d))
}
