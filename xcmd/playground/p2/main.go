package main

import (
	"fmt"
	"golang.org/x/text/unicode/norm"
)

func main() {
	// purpose: experiment with polytonic greek and the go norm package
	data := []string{
		norm.NFD.String("ω"),
		norm.NFD.String("ῷ"),
	}
	for _, s := range data {
		fmt.Printf("%v len(%s): %d U: %U 1stBoundary: %d\n", []byte(s), s, len(s), []rune(s), norm.NFD.FirstBoundaryInString(s))
	}
}
