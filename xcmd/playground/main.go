package main

import (
	"fmt"
	"strings"
)

func main() {
	// Assuming the Markdown table is converted to a 2D slice of strings
	table := [][]string{
		{"eu", "baptism", "html", "en", "0"},
		{"eu", "baptism", "html", "gr-en", "1"},
		{"eu", "baptism", "pdf", "en-gr", "2"},
		{"eu", "baptism", "pdf", "en", "3"},
		{"eu", "li", "chrys", "html", "en", "4"},
		{"eu", "li", "chrys", "html", "gr-en", "5"},
		{"eu", "li", "chrys", "pdf", "en-gr", "6"},
		{"eu", "li", "chrys", "pdf", "en", "7"},
		{"eu", "li", "basil", "html", "en", "8"},
		{"eu", "li", "basil", "html", "gr-en", "9"},
		{"eu", "li", "basil", "pdf", "en-gr", "10"},
		{"eu", "li", "basil", "pdf", "en", "11"},
	}
	// TODO: convert the []string into [][]string
	// The 2nd dimension is the slice of TitleCodes.
	// Append to the TitleCodes slice and index from the first dimension
	tree := buildTree(table)
	printTree(tree, 0)
}

type TreeNode struct {
	Value    string
	Children []*TreeNode
}

func addNode(root *TreeNode, value string) *TreeNode {
	// Check if the node already exists among the children
	for _, child := range root.Children {
		if child.Value == value {
			return child
		}
	}

	// If the node does not exist, create a new one and add it to the children
	newNode := &TreeNode{Value: value}
	root.Children = append(root.Children, newNode)
	return newNode
}

func buildTree(table [][]string) *TreeNode {
	root := &TreeNode{Value: "root"} // Starting point of the tree

	for _, row := range table {
		currentNode := root
		for _, value := range row {
			currentNode = addNode(currentNode, value)
		}
	}

	return root
}

func printTree(node *TreeNode, level int) {
	if node == nil {
		return
	}

	fmt.Printf("%s%s\n", " "+strings.Repeat("-", level*2), node.Value)
	for _, child := range node.Children {
		printTree(child, level+1)
	}
}
