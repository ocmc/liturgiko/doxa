# DOXA Tools Development Guide

## Build Commands

- Build: `go build -o bin/doxa .`
- Run: `./bin/doxa`
- Run tests: `go test ./...`
- Run specific test: `go test github.com/liturgiko/doxa/pkg/concord -run TestSortedKeys`
- Debug mode: `./bin/doxa --debug`

## Code Style Guidelines

- Uses Go modules (go.mod)
- Import order: standard library → third-party → internal
- Naming: camelCase for variables, PascalCase for exported functions/types
- Error handling: propagate errors upward, use descriptive error messages
- Logging: use pkg/doxlog for structured logging
- Testing: include test files with _test.go suffix
- File organization: follow Go standard project layout

## Coding Standards

- Follow Go's official style guide and effective Go principles
- Maximum line length of 100 characters
- Use meaningful variable/function names that reflect purpose
- Document all exported functions, types, and packages
- Include context in error messages (e.g., "loading config: file not found")
- Prefer early returns over nested conditionals
- Group related declarations and keep functions focused on single responsibility
- Write unit tests for all new functionality
- Maintain backward compatibility where possible
- Include appropriate error recovery mechanisms
- Reference ticket numbers in comments for complex implementations
- Avoid package-level variables when possible
- Check for nil before dereferencing pointers

## Web Component Standards

All web components in the `static/wc` directory should follow these standards to ensure consistency, maintainability, and accessibility:

### Component Documentation

- Each component JS file must begin with a documentation header:

#### Basic Header Documentation

```javascript
/**
 * @tagname data-list
 * @description Displays a paginated list of data items with filter and sort capabilities. Handles large datasets efficiently with virtual scrolling.
 *
 * @inputs {Attribute} data-source - API endpoint or JSON data array for the list items
 * @inputs {Attribute} page-size - Number of items to display per page (default: 20)
 * @outputs {Event} item-selected - Fires when a list item is selected with detail: {item: Object}
 * @api {GET} /api/items - Fetches items when data-source is a URL
 * @watches {Attribute} filter - Re-filters data when this attribute changes
 */
class DataList extends HTMLElement {
   // Implementation details follow...
}
```

### Naming and Structure
- HTML element names must be kebab-case (e.g., `data-table`, `input-with-dropdown`)
- Component class names must be PascalCase (e.g., `DataTable`, `InputWithDropdown`)
- Every element in the component must have a unique ID that follows the pattern: `[component-name]-[element-function]-[id]`
- All public methods and properties should be documented with JSDoc comments

### Component Functionality

- Components should be self-contained with clearly defined boundaries
- Component interface should be primarily attribute-based
- Use CustomEvent with bubbles:true and composed:true for crossing shadow DOM boundaries
- Components should properly clean up resources in disconnectedCallback()
- Registered observed attributes should be minimal and purposeful

### Styling

- Use shadow DOM for component style encapsulation
- Provide reasonable default styles that work in light and dark modes
- Use CSS variables for themeable properties
- Include responsive design considerations for mobile devices

### Accessibility

- All interactive elements must have appropriate ARIA attributes
- Components should be keyboard navigable where applicable
- Color contrast should meet WCAG AA standards
- Error states and messages should be properly communicated to assistive technologies

### Performance
- Avoid expensive operations in frequently called lifecycle methods
- Defer network operations until needed
- Use attribute change detection instead of polling
- Batch DOM updates when making multiple changes

### Testing
- Each component should have basic unit tests for key functionality
- Include tests for accessibility compliance
- Test with all supported browsers for compatibility

### Component Documentation Examples
Each component file should include practical usage examples that demonstrate the component's features and configurations. This helps both developers and AI assistants understand how to properly implement the component.

#### Basic Header Documentation

```javascript
/**
 * @tagname data-list
 * @description Displays a paginated list of data items with filter and sort capabilities. Handles large datasets efficiently with virtual scrolling.
 * 
 * @inputs {Attribute} data-source - API endpoint or JSON data array for the list items
 * @inputs {Attribute} page-size - Number of items to display per page (default: 20)
 * @outputs {Event} item-selected - Fires when a list item is selected with detail: {item: Object}
 * @api {GET} /api/items - Fetches items when data-source is a URL
 * @watches {Attribute} filter - Re-filters data when this attribute changes
 */
class DataList extends HTMLElement {
  // Implementation details follow...
}
```

#### Usage Examples Section
Each component file should also include a section with usage examples like this:

```javascript
/**
 * @examples
 * 
 * Basic usage:
 * ~~~html
 * <data-list 
 *   id="user-data-list"
 *   data-source="/api/users" 
 *   page-size="10">
 * </data-list>
 * ~~~
 *
 * With event handling:
 * ~~~javascript
 * const dataList = document.getElementById('user-data-list');
 * dataList.addEventListener('item-selected', (event) => {
 *   console.log('Selected item:', event.detail.item);
 *   // Perform actions with the selected item
 * });
 * ~~~
 *
 * Advanced configuration:
 * ~~~html
 * <data-list
 *   id="advanced-data-list"
 *   data-source="/api/products"
 *   page-size="25"
 *   filter="category:electronics"
 *   sort-by="price"
 *   sort-direction="desc">
 * </data-list>
 * ~~~
 *
 */
```

## Documentation Standards
- Document all major functions with clear purpose, parameters and return values
- Include usage examples for complex interfaces
- Maintain accurate documentation when changing code behavior
- Document known limitations or edge cases

## Markdown Documentation Guidelines

### Instructions for Handling Nested Code Blocks in Markdown

When writing markdown that includes nested code blocks, always follow these guidelines to ensure compatibility with various markdown parsers, particularly in IDEs like JetBrains GoLand.

### Core Rule: Differentiate Nested Code Block Delimiters

Always use different fence characters for different nesting levels of code blocks:

- Use triple backticks (```) for the outermost code blocks
- Use triple tildes (~~~) for code blocks nested within other code blocks

### Implementation Guidelines

#### 1. For Standard Code Blocks (Outermost Level)

Always use triple backticks with an optional language identifier:

```javascript
function example() {
  console.log("This is standard code");
}
```

#### 2. For Nested Code Blocks (Within Documentation or Other Code)

Use triple tildes with an optional language identifier:

```javascript
/**
 * @examples
 * 
 * Basic usage:
 * ~~~html
 * <data-list 
 *   id="user-data-list"
 *   data-source="/api/users" 
 *   page-size="10">
 * </data-list>
 * ~~~
 * 
 * Advanced example:
 * ~~~javascript
 * const list = document.querySelector('#data-list');
 * list.addEventListener('change', (e) => {
 *   console.log(e.detail);
 * });
 * ~~~
 */
class ExampleComponent {
  // Implementation
}
```

#### 3. For Multiple Levels of Nesting (Rare Cases)

If you need more than two levels of nesting (uncommon but possible):

- Level 1 (outermost): Triple backticks (```)
- Level 2: Triple tildes (~~~)
- Level 3: Triple asterisks (***) or indent with four spaces

#### 4. For JSDoc Examples

When writing JSDoc documentation that includes code examples, always use the tilde format for the example code blocks:

```javascript
/**
 * Returns the sum of two numbers
 * 
 * @param {number} a First number
 * @param {number} b Second number
 * @returns {number} Sum of a and b
 * 
 * @example
 * ~~~javascript
 * const result = add(2, 3);
 * console.log(result); // 5
 * ~~~
 */
function add(a, b) {
  return a + b;
}
```

### Special Cases

#### Markdown Within HTML

When embedding code blocks within HTML in markdown:

```html
<details>
<summary>View code example</summary>

~~~javascript
function nestedExample() {
  return "This works in most parsers";
}
~~~

</details>
```

#### Tables Containing Code

When including code blocks within markdown tables, use inline code (single backticks) where possible, or reference a separate code block.

### Compatibility Note

This approach ensures maximum compatibility with:
- JetBrains IDEs (GoLand, WebStorm, IntelliJ)
- GitHub Flavored Markdown
- Most documentation generators
- Popular markdown preview extensions

## Project Features
- Orthodox liturgical tools written in Go
- Supports template-based generation of liturgical texts
- Includes database functionality, web server, and CLI
- Multi-language support for liturgical texts
- Synchronization capabilities for collaborative work

## Git Workflow

### SAR Naming Convention
- SAR stands for "Software Action Request" and is used for issue tracking
- All development work must be associated with a SAR number

### Branch Naming
- Branch names MUST start with `SAR-` followed by the issue number, then a hyphen and a brief description
- Example: `SAR-123-fix-login-issue`
- Do NOT use prefixes like feature/ or bugfix/ as previously instructed

### Commit Messages
- IMPORTANT: All commit messages MUST start with the SAR identifier (e.g., `SAR-123 Fix login form validation`)
- This prefix triggers integration with other tools like Slack and ClickUp
- The SAR- prefix is required for automated tracking and visibility across tools
- Format: `SAR-XXX Brief description of what changed`
- For multi-line commit messages, ensure the first line begins with the SAR identifier

### Other Guidelines
- Create PRs against master branch
- Keep commits focused and logical (single responsibility)
- Write descriptive commit messages with clear explanations

## AI Session Documentation Guidelines

This project uses a standardized approach for documenting AI assistance sessions.

### Important Files
- `AI_README.md`: This file - Development standards and guidelines
- `AI_LOG.md`: Records AI assistance sessions in concise format (auto-generated, read-only)
- `ai-logs/*.md`: Detailed AI session logs (read-only after generation)

### Protection of AI-Generated Files
All AI-generated files are protected to prevent accidental modifications:
1. Files include warning headers indicating they are auto-generated
2. Files are set to read-only permissions (chmod 444) after generation
3. To make changes, you must:
   - For individual logs: Create a new log file or temporarily make writable with `chmod 644 file.md`
   - For AI_LOG.md: Use the `update_ai_log.sh` script which will regenerate the file

### Documentation Process

When working with Claude or other AI tools:

1. **Check ai-logs directory**:
   Ensure the `ai-logs` directory exists at the project root. If not, create it:
   ```bash
   mkdir -p ai-logs
   ```

2. **Create detailed session log**:
   Create a file in the `ai-logs` directory with format: `YYYY-MM-DD-HHMM-SAR-XX-feature-name.md`

   Content structure:
   ```markdown
   # Conversation Summary: SAR-XX Feature Name

   **Developer:** Your Name (your.email@example.com)

   **AI:**
   - Organization: Anthropic
   - Tool name: Claude Code
   - Model identifier: claude-3-7-sonnet-20250219
   
   **Session Cost:**
   - Total cost: $X.XX
   - Total duration: Xm XX.Xs

   ## Concise Summary
   One or two sentences summarizing what was accomplished in the session (this will be used in AI_LOG.md).

   ## Key Accomplishments
   1. **Task area 1:**
      - Detail point
      - Detail point

   2. **Task area 2:**
      - Detail point
      - Detail point

   ## Current Status
   - Current work items

   ## Next Steps
   1. Future consideration
   2. Future consideration

   ## Files Worked With
   - `path/to/file.ext` - Brief description
   - `path/to/file2.ext` - Brief description
   ```

3. **Update AI_LOG.md**:

   Run the update_ai_log.sh script with your log file path as a parameter:
   ```bash
   ./update_ai_log.sh ai-logs/YYYY-MM-DD-HHMM-SAR-XX-feature-name.md
   ```

   This will automatically add a new line at the top of the file with:
   ```markdown
   [YYYY-MM-DD-HHMM-SAR-XX-feature-name](ai-logs/YYYY-MM-DD-HHMM-SAR-XX-feature-name.md): Your Name (your.email@example.com): Tool name: Model identifier: Copy the exact text from the "Concise Summary" section of the detailed log file.
   ```

   Each entry will be a single line with the following components separated by colons:
   - The filename as a markdown link to the detailed log
   - Developer name and email from git config
   - AI tool name
   - Complete AI model identifier
   - The concise summary text

   IMPORTANT: Always add new entries at the top of the file, maintaining reverse chronological order (newest first).

4. **Link format**:
   - Always use relative paths for links between files
   - Format: `[Full details](ai-logs/YYYY-MM-DD-HHMM-SAR-XX-feature-name.md)`

5. **Include git committer info**:
   - Always include developer name and email exactly as shown in git config
   - Format: `**Developer:** Name (email@example.com)`

6. **Include AI info**:
   - Always include organization, tool name, and complete model identifier
   - Format:
   ```
   **AI:**
   - Organization: Anthropic
   - Tool name: Claude Code
   - Model identifier: claude-3-7-sonnet-20250219
   ```

7. **Include session cost information**:
   - Before concluding the AI session, ask the user: "Would you like to include session cost information in the log file?"
   - If yes, wait for the user to run the `/cost` command and provide the output
   - Add the cost information in the format:
   ```
   **Session Cost:**
   - Total cost: $X.XX
   - Total duration: Xm XX.Xs
   ```

## Project Documentation

The following documentation is available in the repository:

| Document                             | Description |
|--------------------------------------|-------------|
| [Database ID System](docs/db/id.md)  | Explains the DOXA hierarchical key-value database system, including KeyPath structure and organization of different data types |
| ------------------------------------ |-------------|