package cli

import (
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/ages/ares/ares2bolt"
	agesAtem "github.com/liturgiko/doxa/pkg/ages/atem2lml"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/media"
	"github.com/liturgiko/doxa/pkg/utils/clinput"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/stack"
	"path"
	"strings"
	"sync"
)

// A2L converts AGES atem files to Doxa LML files
func A2L(lmlOutDir, projectDir string,
	mapper *kvs.KVS, mediaReverseLookup *sync.Map) {
	doxlog.WriteCliOut = true
	defer func() {
		doxlog.WriteCliOut = false
	}()
	if mapper == nil {
		doxlog.Error("mapper is nil")
		return
	}
	if config.DoxaPaths == nil {
		doxlog.Error("config.DoxaPaths is nil")
		return
	}
	tempInDir := config.DoxaPaths.TempDirAlwbPath
	cssFile := config.DoxaPaths.HtmlCss
	pdfCssFile := config.DoxaPaths.PdfCss

	fmt.Printf("%s\n", lmlOutDir)
	if !ltfile.DirExists(tempInDir) {
		err := ltfile.CreateDirs(tempInDir)
		if err != nil {
			doxlog.Errorf("%v", err)
			return
		}
	}
	if ltfile.DirExists(lmlOutDir) {
		err := ltfile.DeleteAllDirsExcept(lmlOutDir, []string{".git", projectDir})
		if err != nil {
			fmt.Printf("error removing empty directories from %s: %v", lmlOutDir, err)
		}
	} else {
		err := ltfile.CreateDirs(lmlOutDir)
		if err != nil {
			doxlog.Errorf("error creating dirs for %s: %v", lmlOutDir, err)
			return
		}
	}
	doxlog.Infof("The converted templates will be written to: %s", lmlOutDir)
	doxlog.Infof("Extracting key information from Doxa database for 'ltx/gr_gr_cog")
	// we will use the keyMap as a means to get the full directory path to each record
	// when we convert the atem templates to lml templates.
	// We need to populate the keymap to be ready for each library
	// used as an import in an atem template file.
	// There are three used to build the keymap:
	// en_redirects_goarch, gr_redirects_goarch, and gr_gr_cog.
	// kp is the key path object we will use.
	kp := kvs.NewKeyPath()
	kp.Dirs.Push("ltx")
	kp.Dirs.Push("gr_gr_cog") // this is supposed to be the source
	keyMap, err := mapper.Db.InverseKeyMap(kp)
	if err != nil {
		doxlog.Errorf("could not get database keys for gr_gr_cog: %v\n", err)
		return
	}
	doxlog.Infof("%d record keys found\n", len(*keyMap))

	addLibs := []string{"gr_redirects_goarch",
		"en_redirects_goarch",
		"en_us_goarch", // removing this results in mcTag not found errors
		"en_us_public", // TODO: Issue 420 on hold, which removes this.  Waiting for Fr S to stop importing en_US_public into atem templates.
	}
	for _, lib := range addLibs {
		moreKp := kvs.NewKeyPath()
		moreKp.Dirs.Push("ltx")
		moreKp.Dirs.Push(lib)
		goaKeyMap, err := mapper.Db.InverseKeyMap(moreKp)
		if err != nil {
			doxlog.Errorf("could not get database keys for %s: %v\n", lib, err)
			continue
		}
		for _, v := range *goaKeyMap {
			for _, e := range v {
				var ok bool
				var values []*kvs.KeyPath
				if values, ok = (*keyMap)[e.KeyParts.First()]; ok {
				}
				values = append(values, e)
				(*keyMap)[e.KeyParts.First()] = values
			}
		}
	}
	// load pdf_credits and pdf_titles from github raw files
	if err = LoadPdfAres(mapper); err != nil {
		msg := fmt.Sprintf("error loading pdf_credits and pdf_titles: %v", err)
		doxlog.Error(msg)
		return
	}

	err, parseErrors := agesAtem.Convert(tempInDir, lmlOutDir, cssFile, pdfCssFile, keyMap)
	if err != nil {
		doxlog.Errorf("%v", err)
		return
	}
	var logFileLines []string
	var logFile = path.Join(doxlog.LogDir(), "a2l.log")
	if len(parseErrors) > 0 {
		doxlog.Warn("There were errors parsing the atem templates:")
		for _, pe := range parseErrors {
			if strings.Contains(pe.Message, "empty section") {
				continue
			}
			logFileLines = append(logFileLines, pe.StringVerbose())
		}
		doxlog.Warnf("Total errors = %d.\n", len(parseErrors))
	}
	// check the insert statements in the converted templates to ensure
	// the template to insert can be found using the argument of the insert.
	var valid bool
	var badInserts []string
	valid, badInserts, err = agesAtem.InsertsAreValid(lmlOutDir)
	if err != nil {
		doxlog.Errorf("error verifying inserts in %s: %v", lmlOutDir, err)
	}
	if !valid {
		logFileLines = append(logFileLines, fmt.Sprintf("\n\ntemplate not found for the following inserts in the lml templates:"))
		for _, b := range badInserts {
			logFileLines = append(logFileLines, fmt.Sprintf("\t%s", b))
		}
	}
	if ltfile.FileExists(logFile) {
		err = ltfile.DeleteFile(logFile)
		if err != nil {
			doxlog.Errorf("%v", err)
		}
	}
	if len(logFileLines) > 0 {
		doxlog.Infof("See the log file %s for details about the errors.\n", logFile)
		err = ltfile.WriteLinesToFile(logFile, logFileLines)
		if err != nil {
			doxlog.Errorf("%v", err)
		}
	}
	doxlog.Infof("\nThe converted templates are in %s\n", lmlOutDir)

	doxlog.Info("Loading media-map from templates directory...")
	m := media.NewMediaManager(mapper, mediaReverseLookup)
	var filePaths stack.StringStack
	mediaFolder := path.Join(tempInDir, "AGES-Initiatives", "ages-alwb-templates", "net.ages.liturgical.workbench.templates", "media-maps")
	filePaths, err = ltfile.FileMatcher(mediaFolder, "ares", nil)
	if err != nil {
		doxlog.Errorf("error reading media files from %s: %v", mediaFolder, err)
		return
	}
	if filePaths == nil {
		doxlog.Errorf("no media files found in %s", mediaFolder)
		return
	}

	for _, p := range filePaths {
		if strings.HasSuffix(p, "_old.ares") {
			continue
		}
		doxlog.Infof("loading %s\n", p)
		err = m.LoadFromAresMediaFile(p)
		if err != nil {
			doxlog.Errorf("error loading media maps: %v\n", err)
		} else {
			doxlog.Infof("Deleted existing database records for for %s and reloaded them.\n", p)
		}
	}
	return
}

// LoadPdfAres reads from github two ares files: one for pdf covers, the other pdf credits
// and writes them to the database under the path ltx/en_us_goarchs
func LoadPdfAres(mapper *kvs.KVS) error {
	pdfCoversUrl := "https://raw.githubusercontent.com/AGES-Initiatives/ages-alwb-templates/master/net.ages.liturgical.workbench.templates/b-preferences/goarch/pdf.covers_en_US_goarch.ares"
	pdfCreditsUrl := "https://raw.githubusercontent.com/AGES-Initiatives/ages-alwb-templates/master/net.ages.liturgical.workbench.templates/b-preferences/goarch/pdf.credits_en_US_goarch.ares"
	websiteIndexTitles := "https://raw.githubusercontent.com/AGES-Initiatives/ages-alwb-templates/master/net.ages.liturgical.workbench.templates/b-preferences/goarch/website.index.titles_en_US_goarch.ares"
	websitePrefTitles := "https://raw.githubusercontent.com/AGES-Initiatives/ages-alwb-templates/master/net.ages.liturgical.workbench.templates/b-preferences/goarch/website.preferences.titles_en_US_goarch.ares"
	pdfCoversContents, err := ltfile.ContentsFromUrl(pdfCoversUrl)
	if err != nil {
		msg := fmt.Sprintf("error gettng contents of %s: %v", pdfCoversUrl, err)
		doxlog.Errorf(msg)
		return fmt.Errorf("%s", msg)
	}
	pdfCreditsContent, err := ltfile.ContentsFromUrl(pdfCreditsUrl)
	if err != nil {
		msg := fmt.Sprintf("error gettng contents of %s: %v", pdfCoversUrl, err)
		doxlog.Error(msg)
		return fmt.Errorf("%s", msg)
	}

	websiteIndexTitlesContent, err := ltfile.ContentsFromUrl(websiteIndexTitles)
	if err != nil {
		msg := fmt.Sprintf("error gettng contents of %s: %v", websiteIndexTitles, err)
		doxlog.Error(msg)
		return fmt.Errorf("%s", msg)
	}

	websitePrefTitlesContent, err := ltfile.ContentsFromUrl(websitePrefTitles)
	if err != nil {
		msg := fmt.Sprintf("error gettng contents of %s: %v", websitePrefTitles, err)
		doxlog.Error(msg)
		return fmt.Errorf("%s", msg)
	}

	// create a temporary directory for the ares files to be written to file system
	dir, err := ltfile.MkdirTemp("", "example")
	if err != nil {
		msg := fmt.Sprintf("error creating temporary director for ares file to be written to file system: %v", err)
		doxlog.Error(msg)
		return fmt.Errorf("%s", msg)
	}
	defer func() {
		err = ltfile.DeleteDirRecursively(dir)
		if err != nil {
			msg := fmt.Sprintf("error removing %s: %v", dir, err)
			doxlog.Error(msg)
		}
	}()

	// write the pdf covers to the temp dir
	file := path.Join(dir, "pdf.covers_en_US_goarch.ares")
	if err = ltfile.WriteFile(file, pdfCoversContents); err != nil {
		msg := fmt.Sprintf("error writing pdf covers to temp directory: %v", err)
		doxlog.Error(msg)
		return fmt.Errorf("%s", msg)
	}
	// write the pdf credits to the temp dir
	file = path.Join(dir, "pdf.credits_en_US_goarch.ares")
	if err = ltfile.WriteFile(file, pdfCreditsContent); err != nil {
		msg := fmt.Sprintf("error writing pdf credits to temp directory: %v", err)
		doxlog.Error(msg)
		return fmt.Errorf("%s", msg)
	}
	// write the website index titles to the temp dir
	file = path.Join(dir, "website.index.titles_en_US_goarch.ares")
	if err = ltfile.WriteFile(file, websiteIndexTitlesContent); err != nil {
		msg := fmt.Sprintf("error writing website index titles to temp directory: %v", err)
		doxlog.Error(msg)
		return fmt.Errorf("%s", msg)
	}
	// write the website preference titles to the temp dir
	file = path.Join(dir, "website.preferences.titles_en_US_goarch.ares")
	if err = ltfile.WriteFile(file, websitePrefTitlesContent); err != nil {
		msg := fmt.Sprintf("error writing website pref titles to temp directory: %v", err)
		doxlog.Error(msg)
		return fmt.Errorf("%s", msg)
	}

	err = ares2bolt.FromGithubFiles(dir, mapper, true, false)
	if err != nil {
		doxlog.Errorf("%s", err)
		return err
	}
	fromKp := kvs.NewKeyPath()
	fromKp.Dirs.Push("ltx")
	fromKp.Dirs.Push("en_us_goarch")
	fromKp.Dirs.Push("pdf.covers")
	toKp := kvs.NewKeyPath()
	toKp.Dirs.Push("ltx")
	toKp.Dirs.Push("en_us_public")
	err = mapper.Db.MakeRedirects(fromKp, toKp)
	if err != nil {
		doxlog.Errorf("%s", err)
		return err
	}
	err = fromKp.Dirs.Set(2, "pdf.credits")
	if err != nil {
		doxlog.Errorf("%s", err)
		return err
	}
	err = mapper.Db.MakeRedirects(fromKp, toKp)
	if err != nil {
		doxlog.Errorf("%s", err)
		return err
	}
	err = fromKp.Dirs.Set(2, "website.index.titles")
	if err != nil {
		doxlog.Errorf("%s", err)
		return err
	}
	err = mapper.Db.MakeRedirects(fromKp, toKp)
	if err != nil {
		doxlog.Errorf("%s", err)
		return err
	}
	err = fromKp.Dirs.Set(2, "website.preferences.titles")
	if err != nil {
		doxlog.Errorf("%s", err)
		return err
	}
	err = mapper.Db.MakeRedirects(fromKp, toKp)
	if err != nil {
		doxlog.Errorf("%s", err)
		return err
	}
	return nil
}
func setLmlDir(lmlDir, projectTemplatesDir, subscriptionTemplatesDir string) (useProjectTemplatesDir bool, useSubscriptionTemplatesDir bool, quit bool) {
	var o []string
	fmt.Println()
	q := "Where do you want the converted templates to be placed?"
	if len(subscriptionTemplatesDir) == 0 {
		o = []string{
			"1", fmt.Sprintf("%s", lmlDir),
			"2", fmt.Sprintf("%s", projectTemplatesDir),
			"q", "quit the command",
		}
	} else {
		o = []string{
			"1", fmt.Sprintf("%s (requires manual copying to use in Doxa)", lmlDir),
			"2", fmt.Sprintf("%s (replaces existing)", projectTemplatesDir),
			"3", fmt.Sprintf("%s", subscriptionTemplatesDir),
			"q", "quit the command",
		}
	}
	switch clinput.GetInput(q, o) {
	case '1':
		return false, false, false
	case '2':
		return true, false, false
	case '3':
		return false, true, false
	case 'q':
		return false, false, true
	default:
		fmt.Println("Invalid selection...")
	}
	return false, false, true
}
