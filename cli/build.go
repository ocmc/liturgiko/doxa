/*******************************************************************************
 * Copyright © 2019-2024 Michael A. Colburn, Copyright © 2024 LIML Professional Services, LLC.
 *
 * This program is provided under the terms of the Eclipse Public License v. 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Developer: Michael A. Colburn
 *
 * Contributors:
 *    Johnpaul Humphrey 2024
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package cli

import (
	"fmt"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/parser"
	"github.com/liturgiko/doxa/pkg/site"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"github.com/liturgiko/doxa/pkg/utils/stamp"
)

func Build(args []string, siteBuilder *site.Builder) {
	doxlog.WriteCliOut = true
	defer func() {
		doxlog.WriteCliOut = false
	}()
	timeStamp := stamp.NewStamp("site build")
	siteBuilder.Relay(fmt.Sprintf(timeStamp.Start()))

	var buildPublic bool
	var indexOnly bool

	for _, arg := range args {
		switch arg {
		case "-public", "-p":
			buildPublic = true
		case "-index", "-i":
			indexOnly = true
		}
	}
	// generate the site
	var parseErrors []parser.ParseError
	var buildError error

	if !indexOnly { // call the siteBuilder.BuildConfig function to build the site
		doxlog.Infof("\nBuilding website %s...\nThe site is set in the %s file.", siteBuilder.Realm, config.DoxaPaths.DoxaConfigPath)
		buildError, parseErrors = siteBuilder.Build(buildPublic, false)
		siteBuilder.Relay(fmt.Sprintf("\n\nNote: the first template(s) take longer to process than the subsequent ones.  \nThis is because values read from the database are cached in memory. \nSubsequent templates use values from the cache in memory, \nwhich speeds up their processing.\n\n"))
	}
	if indexOnly || siteBuilder.Config.IndexForNav {
		siteBuilder.Relay("Creating index.html, help.html, dcs.html, blank.html, etc. and index files")
		err := siteBuilder.WriteAuxPages()
		if err != nil {
			siteBuilder.Relay(fmt.Sprintf("%v", err))
		}
	} else {
		fmt.Println("Config option /site/build/postgen/index/forNav is set to false. If you want to run the site navigation indexer, use the build -index flag.")
	}
	siteBuilder.Relay(fmt.Sprintf("%v", timeStamp.Finish()))
	if buildError != nil {
		buildError = ltstring.Error(buildError)
		siteBuilder.Relay(fmt.Sprintf("There was an error building the site..."))
		doxlog.Errorf("%v", buildError)
		siteBuilder.Relay(fmt.Sprintf("%v", buildError))
	}
	// 2023/04/06 Don't report error details.  Instead, we are reporting which templates have errors.
	siteBuilder.Relay(fmt.Sprintf("A total of %d errors occurred across all templates.", len(parseErrors)))
	return
}
