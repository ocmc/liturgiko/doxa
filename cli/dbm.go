package cli

import (
	"bufio"
	"encoding/json"
	"fmt"
	"github.com/c-bata/go-prompt"
	"github.com/fatih/color"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/atempl"
	"github.com/liturgiko/doxa/pkg/catalog"
	"github.com/liturgiko/doxa/pkg/concord"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/dbmPlugin"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/ed"
	"github.com/liturgiko/doxa/pkg/enums/goos"
	"github.com/liturgiko/doxa/pkg/enums/properties"
	"github.com/liturgiko/doxa/pkg/generators/pdf"
	"github.com/liturgiko/doxa/pkg/inOut"
	"github.com/liturgiko/doxa/pkg/ltm"
	"github.com/liturgiko/doxa/pkg/models"
	"github.com/liturgiko/doxa/pkg/nlp"
	"github.com/liturgiko/doxa/pkg/utils/clinput"
	"github.com/liturgiko/doxa/pkg/utils/ltfile"
	"github.com/liturgiko/doxa/pkg/utils/ltstring"
	"golang.org/x/crypto/ssh/terminal"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
	"os"
	"os/user"
	"path"
	"runtime"
	"sort"
	"strconv"
	"strings"
	"time"
)

// DbM - Database Manager
type DbM struct {
	clipboard       *Clipboard
	conc            concord.Concordance
	context         *Context
	previousContext *Context
	idMap           *IDMap
	Mapper          *kvs.KVS
	SM              *config.SettingsManager
	paths           *config.Paths
	settings        *Settings
	Commands        []prompt.Suggest
	Suggestions     []prompt.Suggest
	FrequencyData   []*nlp.FrequencyData
	Port            string
	RecEditor       string
	Stores          *kvs.Stores
	CurrentStore    *kvs.Store
	EditPath        string
	EdPlugins       []dbmPlugin.WithEdit
	CliPlugins      []dbmPlugin.Command
	CatalogManager  *catalog.Manager
}

func NewDbM(port string, stores *kvs.Stores, initialStore string, settingsManager *config.SettingsManager, paths *config.Paths, catMan *catalog.Manager) *DbM {
	dbm := new(DbM)
	dbm.Port = port // for displaying api
	dbm.Stores = stores
	store := stores.Get(initialStore)
	dbm.CurrentStore = store
	dbm.Mapper = store.Kvs
	dbm.paths = paths
	dbm.settings = NewSettings()
	dbm.context = NewContext()
	dbm.SM = settingsManager
	dbm.CatalogManager = catMan

	// load settings from config file
	dbm.SetConfig()

	dbm.context.SetTerminalHW()
	dbm.setSuggestions()
	dbm.RecEditor = GetOsEditor()
	GoOS := goos.CodeForString(runtime.GOOS)
	if GoOS == goos.Linux {
		dbm.EditPath = "/var/local/doxtmp.txt"
	} else {
		usr, err := user.Current()
		if err != nil {
			fmt.Printf("dbm.NewDbm() could not get user: %v\n", err)
		} else {
			dbm.EditPath = path.Join(usr.HomeDir, "doxatmp.txt")
		}
	}
	dbm.clipboard = NewClipboard()
	return dbm
}

var pathDelimiter = kvs.IdDelimiter
var segmentDelimiter = kvs.SegmentDelimiter

func (dbm *DbM) AddResultsToClipboard() {
	for _, key := range dbm.context.Keys {
		rec, _, err := dbm.Mapper.Db.GetResolved(key)
		if err == nil {
			dbm.clipboard.AddRecord(rec)
		} else {
			fmt.Printf("error adding %s: %v", key.Path(), err)
		}
	}
}

type Clipboard struct {
	RecordMap map[string]*kvs.DbR
}

func NewClipboard() *Clipboard {
	c := new(Clipboard)
	c.RecordMap = make(map[string]*kvs.DbR)
	return c
}
func (c *Clipboard) AddRecord(rec *kvs.DbR) {
	c.RecordMap[rec.KP.Path()] = rec
}
func (c *Clipboard) Clear() {
	c.RecordMap = make(map[string]*kvs.DbR)
}
func (c *Clipboard) Empty() bool {
	return len(c.RecordMap) == 0
}
func (c *Clipboard) List() {
	if len(c.RecordMap) == 0 {
		fmt.Println("Clipboard empty")
	} else {
		for key, rec := range c.RecordMap {
			fmt.Printf("%s %s\n", key, rec.Value)
		}
	}
}

type Padding struct {
	P1 int `json:"number"`
	P2 int `json:"id"`
	P3 int `json:"value"`
}
type Settings struct {
	Exact                  bool          `json:".exact"`
	Hints                  bool          `json:".hints"`
	HyphenPunctuationChars string        `json:".hyphenPunct"`
	HyphenScansionChars    string        `json:".hyphenScan"`
	HyphenKeepPunctuation  bool          `json:".hyphenNoPunct"`
	HyphenKeepScansion     bool          `json:".hyphenNoScan"`
	IDlike                 *kvs.KeyPath  `json:".idlike"`
	Padding                Padding       `json:".padding"`
	ShortPrompt            bool          `json:".shortprompt"`
	ShowEmpty              bool          `json:".showempty"`
	ShowSystem             bool          `json:".showsystem"`
	Sort                   concord.Order `json:".sort"`
	Width                  int           `json:".width"`
	WholeWord              bool          `json:".wholeword"`
}

func NewSettings() *Settings {
	newSettings := new(Settings)
	newSettings.IDlike = kvs.NewKeyPath()
	return newSettings
}

// Context holds information about the current context.
// There is also a previousContext, used for reverting the context if there is an error.
// Because of this, if you add or remove fields to the Context struct, be sure to
// update the copyContext() and revertContext() methods.
type Context struct {
	KP            *kvs.KeyPath   // current KeyPath values
	Matcher       *kvs.Matcher   // properties for matching records during a find
	Path          string         // current path, e.g. gr_gr_cog/actors/Priest
	Keys          []*kvs.KeyPath // most recently obtained keys.  directory keys end with forward slash
	NbrDirKeys    int            // number of directories
	NbrRecordKeys int            // number of record keys
	THeight       int            // height of the user's terminal window
	TWidth        int            // Width of the user's terminal window
}

func NewContext() *Context {
	c := new(Context)
	c.KP = kvs.NewKeyPath()
	c.Matcher = kvs.NewMatcher()
	return c
}

func (c *Context) DBPath() string {
	return c.KP.Path()
}

// SetTerminalHW set the width and height of the terminal.
func (c *Context) SetTerminalHW() {
	fd := int(os.Stdout.Fd())
	tWidth, tHeight, err := terminal.GetSize(fd)
	if err != nil {
		c.THeight = 12
		c.TWidth = 80
	} else {
		c.THeight = tHeight
		c.TWidth = tWidth
	}

}
func (c *Context) InRecord() bool {
	return !c.KP.KeyParts.Empty()
}

// SetConfig reads the database for configuration settings used by the db command
func (dbm *DbM) SetConfig() {
	dbm.settings.HyphenPunctuationChars = dbm.SM.StringProps[properties.SysShellDbHyphenPunctuationChars]
	dbm.settings.HyphenKeepPunctuation = dbm.SM.BoolProps[properties.SysShellDbHyphenKeepPunct]
	dbm.settings.HyphenScansionChars = dbm.SM.StringProps[properties.SysShellDbHyphenScansionChars]
	dbm.settings.HyphenKeepScansion = dbm.SM.BoolProps[properties.SysShellDbHyphenKeepScansion]
	dbm.settings.Exact = true // TODO: add regex support as found in online search of Greek.  Remove references to nnp.
	//	dbm.settings.Exact = dbm.SM.BoolProps[properties.SysShellDbFindExact]
	dbm.settings.WholeWord = dbm.SM.BoolProps[properties.SysShellDbFindWholeword]
	dbm.settings.ShowEmpty = dbm.SM.BoolProps[properties.SysShellDbFindEmpty]
	dbm.settings.ShortPrompt = dbm.SM.BoolProps[properties.SysShellDbShortPrompt]
	dbm.settings.ShowSystem = dbm.SM.BoolProps[properties.SysShellDbShowHidden]
	dbm.settings.Hints = dbm.SM.BoolProps[properties.SysShellDbShowHints]

	dbm.settings.Padding.P1 = dbm.SM.IntProps[properties.SysShellDbConcordancePaddingP1]
	if dbm.settings.Padding.P1 == 0 {
		dbm.settings.Padding.P1 = 4
	}
	dbm.settings.Padding.P2 = dbm.SM.IntProps[properties.SysShellDbConcordancePaddingP2]
	if dbm.settings.Padding.P2 == 0 {
		dbm.settings.Padding.P2 = 50
	}
	dbm.settings.Padding.P3 = dbm.SM.IntProps[properties.SysShellDbConcordancePaddingP3]

	sort := strings.ToLower(dbm.SM.StringProps[properties.SysShellDbConcordanceSort])

	switch sort {
	case "id":
		dbm.settings.Sort = concord.SortId
	case "left":
		dbm.settings.Sort = concord.SortLeft
	default:
		dbm.settings.Sort = concord.SortRight
	}
	dbm.settings.Width = dbm.SM.IntProps[properties.SysShellDbConcordanceWidth]
	if dbm.settings.Width == 0 {
		dbm.settings.Width = 30
	}
}
func (dbm *DbM) SetPath() {
	p := dbm.context.KP.Path()
	if p == ":" {
		dbm.context.Path = ""
		return
	}
	dbm.context.Path = p
}

// Like sets Matcher based on context KeyPath
func (dbm *DbM) Like() {
	dbm.context.Matcher.KP = dbm.context.KP
}

// IDMap provides the map held by this type a string index into an int index to access the context.Keys slice
type IDMap struct {
	Map map[string]int
}

func NewIDMap() *IDMap {
	m := new(IDMap)
	m.Map = make(map[string]int)
	return m
}

// ResetIdMap Resets the map so it is empty
func (dbm *DbM) ResetIdMap() {
	dbm.context.Keys = nil
	dbm.idMap.Map = make(map[string]int)
}

// AddPathToMap adds a path to the map.
func (dbm *DbM) AddPathToMap(kp *kvs.KeyPath) {
	dbm.context.Keys = append(dbm.context.Keys, kp)
	dbm.idMap.Map[kp.Path()] = len(dbm.context.Keys)
	if len(dbm.context.Keys) != len(dbm.idMap.Map) {
		fmt.Printf("error: len(context.Keys) != len(im.Map)\n")
	}
}

// GetKeyPath the KeyPath pointed to by the key
func (dbm *DbM) GetKeyPath(key string) *kvs.KeyPath {
	if i, ok := dbm.idMap.Map[key]; ok {
		if len(dbm.context.Keys) > i {
			return dbm.context.Keys[i-1]
		}
	}
	return nil
}

func (dbm *DbM) LivePrefix() (string, bool) {
	if dbm.context.Path == "" {
		return fmt.Sprintf("%s> ", dbm.CurrentStore.Prompt), true
	}
	var path string
	if dbm.settings.ShortPrompt {
		if dbm.context.InRecord() {
			path = ":" + dbm.context.KP.Key()
		} else {
			path = dbm.context.KP.Dirs.Last()
		}
	} else {
		path = dbm.context.Path
	}
	return path + "> ", true
}

// Executor executes the Commands issued by the user
func (dbm *DbM) Executor(in string) {
	doxlog.WriteCliOut = true
	defer func() {
		doxlog.WriteCliOut = false
	}()
	in = strings.TrimSpace(in)
	var method string
	args := strings.Split(in, " ")
	switch args[0] {
	case "cd":
		method = args[0]
		dbm.changeDirectory(args)
	case "cmp":
		method = args[0]
		dbm.compareValues()
	case "clip":
		method = args[0]
		dbm.clip(args)
	case "ed":
		method = args[0]
		dbm.edit()
	case "exit":
		fmt.Println("Bye!")
		os.Exit(0)
	case "export":
		method = args[0]
		dbm.export()
	case "find":
		method = args[0]
		dbm.find(args)
	case "findID":
		method = args[0]
		dbm.findID(args)
	case "findNear":
		method = args[0]
		dbm.findNear(args)
	case "freq":
		method = args[0]
		dbm.WordListFrequencyCount(args)
	case "help":
		method = args[0]
		dbm.showHelp()
	case "hyphen":
		method = args[0]
		dbm.hyphenate(args)
	case "import":
		method = args[0]
		dbm.importFromFile()
	case "lml":
		method = args[0]
		dbm.showTKAsLML()
	case "ls":
		method = args[0]
		dbm.list(args)
	case "lsDb":
		method = args[0]
		dbm.listDataStores()
	case "lsf":
		method = args[0]
		dbm.listFinds()
	case "mkdir":
		method = args[0]
		dbm.makeDirectory(args)
	case "mv", "move":
		method = args[0]
		dbm.move(args)
	case "pwd":
		method = args[0]
		dbm.pwd()
	case "redirects":
		method = args[0]
		dbm.listRedirects()
	case "rm", "del":
		method = args[0]
		dbm.removeRecord(args)
	case "rmdir": // no equivalent Windows command, since del works with both files and dirs?
		method = args[0]
		dbm.removeDirectory(args)
	case "put":
		method = args[0]
		dbm.putRecord(args)
	case "set":
		method = args[0]
		dbm.setRecord(args)
	case "swap":
		method = args[0]
		dbm.swap(args)
	case "switch":
		method = args[0]
		dbm.switchDataStores(method)
	case "tree":
		method = args[0]
		dbm.tree(args)
	case "url":
		method = args[0]
		dbm.Url()
	case ".context":
		{
			method = ".context"
			var jsonData []byte
			jsonData, err := json.Marshal(dbm.context)
			if err != nil {
				doxlog.Errorf("%v", err)
			} else {
				fmt.Println(string(jsonData))
			}
		}
	case ".exact":
		{
			method = ".exact"
			fmt.Println(".exact always on until nnp removed from code")
			//if len(args) > 1 {
			//	dbm.settings.Exact = strings.Contains(strings.ToLower(args[1]), "on")
			//	if dbm.settings.Exact {
			//		fmt.Println("on")
			//	} else {
			//		fmt.Println("off")
			//	}
			//} else {
			//	if dbm.settings.Exact {
			//		fmt.Println("Exact match for find is on. To switch off: .exact off")
			//	} else {
			//		fmt.Println("Exact match for find is off. To switch on: .exact on")
			//	}
			//}
		}
	case ".hints":
		{
			method = ".hints"
			if len(args) > 1 {
				dbm.settings.Hints = strings.Contains(strings.ToLower(args[1]), "on")
			} else {
				if dbm.settings.Hints {
					fmt.Println("Hints are on. To switch off: .hints off")
				} else {
					fmt.Println("Exact match for find is off. To switch on: .hints on")
				}
			}
		}
	case ".idlike":
		{
			method = ".idlike"
			dbm.settings.IDlike = kvs.NewKeyPath()
			if len(args) > 1 {
				parts := strings.Split(args[1], segmentDelimiter)
				for _, p := range parts {
					dbm.settings.IDlike.Dirs.Push(p)
				}
			} else {
				fmt.Printf(".idlike off\n")
			}
		}
	case ".jump":
		method = ".jump"
		if len(args) > 1 {
			switch args[1] {
			case "catalog":
				currProj := dbm.CatalogManager.LocalProjectKP()
				currProj.Dirs.Push("catalog")
				dbm.switchDataStores(config.SystemDbName)
				dbm.setContextKP(currProj.Path())
			case "catalogs":
				kp := kvs.NewKeyPath()
				kp.Dirs.Push("catalogs")
				dbm.switchDataStores(config.SystemDbName)
				dbm.setContextKP(kp.Path())

			case "subscriptions":
				currProj := dbm.CatalogManager.LocalProjectKP()
				currProj.Dirs.Push("subscriptions")
				dbm.switchDataStores(config.SystemDbName)
				dbm.setContextKP(currProj.Path())
			case "sysDb":
				dbm.switchDataStores(config.SystemDbName)
				dbm.context.Path = dbm.resetPath()
			// TODO: the following endpoints are primarily for debugging purposes
			case "dsd":
				dbm.switchDataStores(config.SystemDbName)
				dbm.setContextKP("catalogs/doxa-seraphimdedes/latest")
			case "dsderg":
				dbm.switchDataStores(config.SystemDbName)
				dbm.setContextKP("catalogs/doxa-seraphimdedes/latest/entries/resources/ltx/en_redirects_goarch")
			case "jth":
				dbm.switchDataStores(config.SystemDbName)
				dbm.setContextKP("inventories/doxa-jhumphrey/latest")
			case "jtht":
				dbm.switchDataStores(config.SystemDbName)
				dbm.setContextKP("inventories/doxa-jhumphrey/latest/entries/templates:version")
			}
		} else {
			fmt.Println("Usage: .jump <location>\nlocation can be: catalog, catalogs, subscriptions, or sysDb")
		}
	case ".keys":
		os := runtime.GOOS
		switch os {
		case "windows":
			fmt.Println("not available")
		case "darwin":
			fmt.Println("ctrl-a move to beginning of line")
			fmt.Println("ctrl-e move to end of line")
			fmt.Println("ctrl-j delete line")
			fmt.Println("ctrl-w delete word to left of cursor")
		case "linux":
			fmt.Println("not available")
		}
		return
	case ".padding":
		{
			method = ".padding"
			switch len(args) {
			case 1:
				{
					fmt.Println("number |id                        |value")
					fmt.Println("2433   |en_us_net/ps/psa44.v8.text| anointed you with the oil of joy elevating you above your co")
					fmt.Println("The `Padding` command controls the Padding of the three parts of a concordance line shown as the result of the find command. The first Padding is for the result number. The second and third paddings are for the id and value). Padding values can be negative, in which case they are left aligned.  Positive values are right aligned.")
					fmt.Printf("The current values are: %v\n", dbm.settings.Padding)
				}
			case 2:
				{
					dbm.setPadding(1, args[1])
				}
			case 3:
				{
					dbm.setPadding(1, args[1])
					dbm.setPadding(2, args[2])
				}
			case 4:
				{
					dbm.setPadding(1, args[1])
					dbm.setPadding(2, args[2])
					dbm.setPadding(3, args[3])
				}
			}
		}
	case ".showempty":
		{
			method = ".showempty"
			if len(args) > 1 {
				dbm.settings.ShowEmpty = strings.Contains(strings.ToLower(args[1]), "on")
				if dbm.settings.ShowEmpty {
					fmt.Println("on")
				} else {
					fmt.Println("off")
				}
			} else {
				if dbm.settings.ShowEmpty {
					fmt.Println(".showempty is on. The `find` command will include records with an empty value. To switch off: .showempty off")
				} else {
					fmt.Println(".showempty is off. The `find` command will exclude records with an empty value. To switch on: .showempty on")
				}
			}
		}
	case ".showSystem":
		{
			method = ".showSystem"
			if len(args) > 1 {
				dbm.settings.ShowSystem = strings.Contains(strings.ToLower(args[1]), "on")
			} else {
				if dbm.settings.ShowSystem {
					fmt.Println("Show system directories is on. To switch off: .showSystem off")
				} else {
					fmt.Println("Show system directories is off. To switch on: .showSystem on")
				}
			}
			dbm.setSuggestions()
		}
	case ".shortprompt":
		{
			method = ".shortpromt"
			if len(args) > 1 {
				dbm.settings.ShortPrompt = strings.Contains(strings.ToLower(args[1]), "on")
			} else {
				if dbm.settings.ShortPrompt {
					fmt.Println("Short prompt is on. To switch off: .shortprompt off")
				} else {
					fmt.Println("Short prompt is off. To switch on: .shortprompt on")
				}
			}
			dbm.setSuggestions()
		}
	case ".width":
		{
			method = ".width"
			switch len(args) {
			case 2:
				{
					i, err := strconv.Atoi(args[1])
					if err != nil {
						fmt.Println(err)
					} else {
						dbm.settings.Width = i
					}
				}
			default:
				{
					fmt.Println("`Width` sets the Width of left and right parts of the concordance lines shown using the `find` command.")
					fmt.Printf("Width = %d\n", dbm.settings.Width)
				}
			}
		}
	case ".wholeword":
		{
			method = ".wholeword"
			if len(args) > 1 {
				dbm.settings.WholeWord = strings.Contains(strings.ToLower(args[1]), "on")
				if dbm.settings.WholeWord {
					fmt.Println("on")
				} else {
					fmt.Println("off")
				}
			} else {
				if dbm.settings.WholeWord {
					fmt.Println(".wholeword is on. The `find` command will include records whose value contains the whole word. To switch off: .wholeword off")
				} else {
					fmt.Println(".wholeword is off. The `find` command will exclude records whose value does not contain the whole word. To switch on: .wholeword on")
				}
			}
		}
	case ".settings":
		{
			method = ".settings"
			var jsonData []byte
			jsonData, err := json.Marshal(dbm.settings)
			if err != nil {
				doxlog.Errorf("%v", err)
			}
			fmt.Println(string(jsonData))
		}
	case ".sort":
		{
			method = ".sort"
			if len(args) > 1 {
				order := strings.ToLower(args[1])
				switch order {
				case "id":
					dbm.settings.Sort = concord.SortId
					fmt.Println("Sort by id is on")
				case "left":
					dbm.settings.Sort = concord.SortLeft
					fmt.Println("Sort by left is on")
				case "right":
					dbm.settings.Sort = concord.SortRight
					fmt.Println("Sort by right is on")
				default:
					fmt.Println("Invalid sort option. Use: .sort id or .sort left or .sort right")
				}
			} else {
				fmt.Printf("%s. To change: .sort id or .sort left or .sort right\n", dbm.settings.Sort.String())
			}
		}
	default:
		//is the user calling a plugin?
		for _, plugin := range dbm.CliPlugins {
			if plugin.Abr() == args[0] {
				if plugin.ValidPath(dbm.context.KP.Copy()) {
					method = args[0]
					if plugin.OnSetup(dbm.context.KP.Copy(), dbm.Mapper, dbm.Stores.Get(config.DbName).Kvs, dbm.paths, dbm.CatalogManager) {
						plugin.Execute(args)
					}
					break
				}
			}
		}
	}
	if method != "" {
		return
	}

	fmt.Println("Sorry, I don't understand.")
}

func (dbm *DbM) Completer(in prompt.Document) []prompt.Suggest {
	w := in.GetWordBeforeCursor()
	if w == "" {
		return []prompt.Suggest{}
	}
	return prompt.FilterHasPrefix(dbm.Suggestions, w, true)
}

func (dbm *DbM) JumpToSysDb(path string) {
	dbm.switchDataStores(config.SystemDbName)
	dbm.setContextKP(path)
}

func (dbm *DbM) ResetContext() {
	dbm.switchDataStores(config.DbName)
	dbm.context.Path = dbm.resetPath()
}

// moves up the path hierarchy
func (dbm *DbM) popPath(levels string) string {
	l := len(strings.Split(levels, segmentDelimiter))
	for i := 0; i < l; i++ {
		if dbm.context.KP.KeyParts.Size() > 0 {
			dbm.context.KP.KeyParts.Pop()
		} else {
			if dbm.context.KP.Dirs.Size() > 0 {
				dbm.context.KP.Dirs.Pop()
			}
		}
	}
	dbm.context.Path = dbm.context.KP.Path()
	if dbm.context.Path == ":" {
		dbm.context.Path = ""
	}
	return dbm.context.Path
}

// Moves deeper down the path hierarchy
func (dbm *DbM) pushPath(segment string) string {
	if dbm.context.InRecord() {
		fmt.Printf("This is a record.  You can't go deeper.")
		return ""
	} else {
		tmpKP := dbm.context.KP.Copy()
		tmpKP.Dirs.Push(segment)
		kp := dbm.GetKeyPath(tmpKP.Path())
		if kp == nil {
			if strings.HasSuffix(segment, "/") {
				dbm.context.KP.Dirs.Push(segment)
			} else {
				dbm.context.KP.KeyParts.Push(segment)
			}
		} else {
			dbm.context.KP = kp
		}
	}
	dbm.context.Path = dbm.context.KP.Path()
	return dbm.context.Path
}

// Sets the values for the popup window that shows the users matching strings
func (dbm *DbM) setSuggestions() {
	dbm.SetPath()
	dbm.Suggestions = []prompt.Suggest{}
	dbm.appendCommands()
	keys, b, k, err := dbm.Mapper.Db.Keys(dbm.context.KP)
	if err != nil {
		p := message.NewPrinter(language.English)
		p.Printf("error getting keys: %v\n", err)
		return
	}
	dbm.context.NbrDirKeys = b
	dbm.context.NbrRecordKeys = k
	dbm.idMap = NewIDMap()
	dbm.context.Keys = nil
	for _, key := range keys {
		last, dir := key.Last()
		desc := kvs.DirDescription(last)
		// filter out hidden directories unless user wants them
		hidden := dir && kvs.IsSystem(last)
		if hidden {
			if !dbm.settings.ShowSystem {
				continue
			}
		}
		if dir {
			last = last + "/"
		}
		dbm.AddPathToMap(key)
		prompt := prompt.Suggest{Text: last, Description: desc}
		dbm.Suggestions = append(dbm.Suggestions, prompt)
	}
	if !dbm.context.InRecord() {
		dbm.reportCount()
	}
}

// add Commands to Suggestions
func (dbm *DbM) appendCommands() {
	dbm.Commands = []prompt.Suggest{
		// Command
		{"cd", "CHANGE path, e.g. cd gr_gr_cog/actors or cd gr_gr_cog/actors/Priest"},
		{"cd ..", "CHANGE path up one level"},
		{"cd ../..", "CHANGE path up two levels"},
		{"cd -", "CHANGE path to previous one"},
		{"cd ~", "CHANGE path to root"},
		{"cd {number}", "After a find or ls, numbers can be used to change path, e.g. cd 244"},
		{"clip", "save record(s) to clipboard"},
		{"clip clear", "clear contents of the clipboard"},
		{"clip ls", "list contents of the clipboard"},
		{"clip > ", "save clipboard to specified file, e.g. clip > hymns.pdf, clip > hymns.txt"},
		{"cmp", "compare values for this topic/key. Must be in a record."},
		{"cp", "COPY contents. Works for directories and records."},
		{"doxa", "return to doxa home prompt"},
		{"ed", "EDIT current record value. Must be in a record."},
		{"exit", "Exit Doxa"},
		{"export", "export records recursively in current directory"},
		{"find", "FIND records with specified value"},
		{"findID", "FIND record ID matching pattern, e.g. ltx/gr_gr_cog/.*/.*.text"},
		{"findNear word1 word2", "FIND records with word1 followed by 1..6 words, then word2."},
		{"findNear word1 word2 x y", "FIND records with word1 followed by x..y words, then word2."},
		{"freq", "frequency count of all words in current directory or path"},
		{"freq -a", "frequency count, order by frequency, ascending."},
		{"freq -d", "frequency count, order by frequency, descending."},
		{"freq -ex", "frequency count, export file"},
		{"freq -in", "frequency count, include numbers."},
		{"hyphen", "hyphenate the words in the current record"},
		{"hyphen -d word", "debug hyphenation patterns for the word"},
		{"import", "import records from file"},
		{"lml", "Display topic/key in the format required for the Liturgical Markup Language."},
		{"ls", "LIST contents. Directory names end with a forward slash. Record keys do not."},
		{"lsDb", "LIST open databases. The prompt indicates the one currently open"},
		{"lsf", "LIST finds. Display concordance of records found using the find command."},
		{"mkdir", "MAKE DIRECTORY. To make a record, use the put command."},
		{"mv", "MOVE (rename) matching id to new id"},
		{"put", "PUT a new record in the current directory."},
		{"pwd", "PWD print working directory. Useful when .shortprompt is on."},
		{"redirects", "REDIRECTS lists records that redirect to the current directory or record."},
		{"rm", "REMOVE record"},
		{"rmdir", "REMOVE directory. Must be empty."},
		{"rmdir {dir name} -force", "force REMOVE directory and all its content, e.g. rmdir props -force"},
		{"set redirect", "SET redirect for current record. Must be 3 levels deep."},
		{"set value", "SET value for current record. Must be in a record."},
		{"swap", "SWAP value of item x and y of a list within in a value directory, e.g. swap 3 1"},
		{"switch", "SWITCH database."},
		{"tree", "TREE prints a tree representation of the directories. Flags can be combined."},
		{"tree -ex e", "TREE -ex e exclude empty records with empty values. Can combine, e.g. -ex et"},
		{"tree -ex r", "TREE -ex r exclude records that are redirects. Can combine, e.g. -ex re"},
		{"tree -ex t", "TREE -ex t exclude records with text"},
		{"tree -L 3", "TREE -L shows 3 levels of directories. Omit to show all."},
		{"tree -v", "TREE -v show value of records."},
		{"tree -v 10", "TREE -v 10 shows 1st 10 characters of record value. -v shows all."},
		{"url", "URL displays url to access a liturgical text using the Doxa web api."},
		{".context", "Shows the current context, i.e. the current path"},
		{".exact off", "If EXACT off, find is insensitive to case and accents and punctuation."},
		{".exact on", "If EXACT on, find will match case and accents."},
		{".hints off", "If HINTS off, no explanations will be given about what you can do next."},
		{".hints on", "If HINTS on, explanations of what you can do next will be displayed."},
		{".idlike", "Sets pattern of id for subsequent finds, e.g. .idlike ltx/en.* matches ltx ids starting with en"},
		{".jump", "go to a specific shortcut"},
		{".jump catalog", "go to your local catalog"},
		{".jump catalogs", "go to the catalog of catalogs"},
		{".jump subscriptions", "go to your current project subscriptions"},
		{".keys", "Show available keys for moving the cursor or deleting text"},
		{".padding", "Sets the padding for concordance line parts."},
		{".settings", "Display the values of the settings"},
		{".shortprompt off", "When off, the full directory path shows in the prompt."},
		{".shortprompt on", "When on, the current directory or record key shows in the prompt."},
		{".showempty off", "Don't show records where both the value and redirect are empty"},
		{".showempty on", "Show records where both the value and redirect are empty"},
		{".showSystem off", "If showSystem off, you won't see system related directories."},
		{".showSystem on", "If showSystem on, you can view system related directories."},
		{".sort id", "Results of Find will be sorted by record ID"},
		{".sort left", "Results of Find will be sorted by left part of concordance line"},
		{".sort right", "Results of Find will be sorted by right part of concordance line"},
		{".width", "Sets the width of left and right sides of concordance line"},
		{".wholeword off", "Show records where word partially matches"},
		{".wholeword on", "Show records where whole word matches"},
	}

	//add context specific commands
	//make sure to sort in alphabetical order

	for _, command := range dbm.Commands {
		dbm.Suggestions = append(dbm.Suggestions, command)
	}
	for _, plugin := range dbm.CliPlugins {
		if plugin.ValidPath(dbm.context.KP.Copy()) {
			dbm.Suggestions = append(dbm.Suggestions, prompt.Suggest{Text: plugin.Abr(), Description: plugin.DescShort()})
		}
	}
}

// Sets the path to be empty
func (dbm *DbM) resetPath() string {
	dbm.context.KP = kvs.NewKeyPath()
	dbm.context.Path = ""
	dbm.context.Keys = nil
	dbm.context.NbrDirKeys = 0
	dbm.context.NbrRecordKeys = 0
	dbm.ResetIdMap()
	return dbm.context.Path
}

// Converts a path into the pop and push parts
// e.g. ../../actors/Priest has ../../ as the pop parts and actors/Priest as the push parts
func cdParts(p string) (string, string) {
	var cdU = strings.Builder{}
	var cdD = strings.Builder{}
	parts := strings.Split(p, segmentDelimiter)
	for _, part := range parts {
		if len(part) == 0 {
			continue
		}
		if strings.HasPrefix(part, "..") || part == "~" {
			cdU.WriteString(part)
			cdU.WriteString(segmentDelimiter)
		} else {
			cdD.WriteString(part)
			cdD.WriteString(segmentDelimiter)
		}
	}
	u := cdU.String()
	if len(u) > 0 {
		u = u[:len(u)-1]
	}
	d := cdD.String()
	if len(d) > 0 {
		d = d[:len(d)-1]
	}
	return u, d
}

// populates the IDMap with redirects
func (dbm *DbM) loadRedirects(redirects []*kvs.DbR, isDir bool) {
	var displayed int
	size := len(redirects)
	dbm.ResetIdMap()
	for i, rec := range redirects {
		dbm.context.SetTerminalHW()
		cnt := i + 1
		to := "-> " + rec.Value[1:]
		fmt.Printf("%4d %s %s\n", cnt, rec.KP.Path(), to)
		dbm.AddPathToMap(rec.KP)
		// chunk the display into what fits terminal height
		// and provide user a means to quit or continue
		displayed++
		if displayed >= dbm.context.THeight-3 {
			quit, recNbr := dbm.Quit(cnt, size, "")
			if quit {
				if recNbr > -1 {
					dbm.Executor(fmt.Sprintf("cd %d", recNbr))
				}
				break
			}
			displayed = 0
		}
	}
	cnt := len(redirects)
	noun := "redirects"
	if cnt == 1 {
		noun = "redirect"
	}
	what := "record"
	if isDir {
		what = "directory"
	}
	fmt.Printf("\n%d %s to this %s.\n", cnt, noun, what)
}

// listRedirects finds and lists any records that redirect to the current record
// or directory, based on the context.
func (dbm *DbM) listRedirects() {
	if dbm.context.KP.IsRecord() {
		if redirects, err := dbm.Mapper.Db.GetRedirectsToRecord(dbm.context.KP); err != nil {
			fmt.Printf("error getting redirects for %s: %v", dbm.context.KP.Path(), err)
		} else {
			dbm.loadRedirects(redirects, false)
		}
	} else {
		if redirects, err := dbm.Mapper.Db.GetRedirectsToDir(dbm.context.KP); err != nil {
			fmt.Printf("error getting redirects for %s: %v", dbm.context.KP.Path(), err)
		} else {
			dbm.loadRedirects(redirects, true)
		}
	}
}

// Gets the number of records that exist.  It is based on the depth of the path.
func (dbm *DbM) reportCount() {
	if len(dbm.context.Path) > 0 {
		dirs := "directories"
		keys := "keys"
		if dbm.context.NbrDirKeys == 1 {
			dirs = "directory"
		}
		if dbm.context.NbrRecordKeys == 1 {
			keys = "key"
		}
		fmt.Printf("%d %s %d %s\n", dbm.context.NbrDirKeys, dirs, dbm.context.NbrRecordKeys, keys)
	}
}

// Copies the current context so it can be reverted if an error occurs
func (dbm *DbM) copyContext() {
	dbm.previousContext = NewContext()
	dbm.previousContext.NbrRecordKeys = dbm.context.NbrRecordKeys
	dbm.previousContext.NbrDirKeys = dbm.context.NbrDirKeys
	dbm.previousContext.Path = dbm.context.Path
	dbm.previousContext.THeight = dbm.context.THeight
	dbm.previousContext.TWidth = dbm.context.TWidth
	if dbm.context.KP != nil {
		dbm.previousContext.KP = dbm.context.KP.Copy()
	}
	if dbm.context.Matcher != nil {
		dbm.previousContext.Matcher = dbm.context.Matcher.Copy() // deep copy
	}
}

// Revert the context to the values in previousContext
func (dbm *DbM) revertContext() {
	if dbm.previousContext.KP != nil {
		dbm.context = NewContext()
		dbm.context.NbrRecordKeys = dbm.previousContext.NbrRecordKeys
		dbm.context.NbrDirKeys = dbm.previousContext.NbrDirKeys
		dbm.context.Path = dbm.previousContext.Path
		dbm.context.THeight = dbm.previousContext.THeight
		dbm.context.TWidth = dbm.previousContext.TWidth
		dbm.context.KP = dbm.previousContext.KP.Copy()
		dbm.SetPath()
	}
	if dbm.previousContext.Matcher != nil {
		dbm.context.Matcher = dbm.previousContext.Matcher.Copy() // deep copy
	}
}

// Show the value for the record with the path.
func (dbm *DbM) showValue() {
	rec, redirects, err := dbm.Mapper.Db.GetResolved(dbm.context.KP)
	if err != nil {
		fmt.Println(err)
	} else {
		if rec != nil {
			dbm.ResetIdMap()
			if rec.IsRedirect() {
				if len(redirects) > 0 {
					fmt.Fprintf(color.Output, "Redirects to:")
					l := len(redirects)
					for i, r := range redirects {
						if i == 0 {
							continue
						}
						rkp := kvs.NewKeyPath()
						err := rkp.ParsePath(r)
						if err != nil {
							fmt.Printf("error parsing ID %s: %v", r, err)
						}
						if i == l-1 {
							fmt.Fprintf(color.Output, "\n\t%s => %s\n", r, color.HiCyanString(rec.Value))
						} else {
							fmt.Printf("\n\t%s =>", r)
						}
					}
					dbm.AddPathToMap(rec.Redirect)
				} else {
					path := rec.Redirect.Path()
					fmt.Fprintf(color.Output, "Redirects to: \n1 %s =>\n%s\n", path, color.HiCyanString(rec.Value))
					dbm.AddPathToMap(rec.Redirect)
				}
			} else {
				// layouts are a json string, so convert to a user-friendly output
				// TODO: if store more values as json strings, write a more generic way to handle this
				thePath := rec.KP.ToDirPath()
				if strings.HasSuffix(thePath, "site/build/layouts/values") {
					if err != nil {
						fmt.Fprintf(color.Output, "%s\n", color.HiCyanString(rec.Value))
					} else {
						var layout *atempl.TableLayout
						err = json.Unmarshal([]byte(rec.Value), &layout)
						if err != nil {
							fmt.Fprintf(color.Output, "%s\n", color.HiCyanString(rec.Value))
						} else {
							var indent []byte
							indent, err = json.MarshalIndent(layout, "", " ")
							parts := strings.Split(string(indent), "\n")
							if len(parts) > 0 {
								for _, p := range parts {
									fmt.Fprintf(color.Output, "%s\n", color.HiCyanString(p))
								}
							} else {
								fmt.Fprintf(color.Output, "%s\n", color.HiCyanString(rec.Value))
							}
						}
					}
				} else {
					fmt.Fprintf(color.Output, "%s\n", color.HiCyanString(rec.Value))
				}
				dbm.AddPathToMap(rec.KP)
			}
		}
	}
}

// escape quotes
func escape(value string) string {
	value = strconv.Quote(value)
	// a side effect of strconv.Quote is it wraps the entire string in quotes,
	// which we do not want.  So, remove them.
	value = value[1 : len(value)-1]
	return value
}

// converts value to int and stores it in dbm.settings.Padding for indicated index of 1 to 3
func (dbm *DbM) setPadding(index int, value string) {
	i, err := strconv.Atoi(value)
	if err != nil {
		fmt.Println(err)
		return
	}
	switch index {
	case 1:
		dbm.settings.Padding.P1 = i
	case 2:
		dbm.settings.Padding.P2 = i
	case 3:
		dbm.settings.Padding.P3 = i
	default:
		fmt.Printf("Invalid index %d passed to func setPadding\n", index)
	}
}
func isNumber(s string) bool {
	if _, err := strconv.Atoi(s); err == nil {
		return true
	} else {
		return false
	}
}

// set the context KeyPath
func (dbm *DbM) setContextKP(path string) {
	kp := kvs.NewKeyPath()
	err := kp.ParsePath(path)
	if err != nil {
		fmt.Printf("error setting context KeyPath: %v\n", err)
		return
	}
	dbm.context.KP = kp
	dbm.SetPath()
}

// changes the virtual directory based on parameters supplied
func (dbm *DbM) changeDirectory(blocks []string) {
	if len(blocks) == 2 {
		if blocks[1] == "-" { // user requested a return to previous path
			if len(dbm.context.Path) == 0 && dbm.previousContext == nil { // because we are at the root directory
				return
			}
			current := dbm.context
			dbm.revertContext()
			dbm.previousContext = current
			return
		}
	}
	dbm.copyContext() // so it can be reverted if we encounter an error
	if len(blocks) < 2 {
		dbm.context.Path = ""
	} else if blocks[1] == "~" {
		dbm.context.Path = dbm.resetPath()
	} else if len(blocks) > 1 && isNumber(blocks[1]) {
		index, err := strconv.Atoi(blocks[1])
		if err != nil {
			fmt.Println(err)
		} else {
			if index > 0 && len(dbm.context.Keys) >= index {
				dbm.context.KP = dbm.context.Keys[index-1]
				dbm.SetPath()
			} else {
				fmt.Printf("%s not found\n", blocks[1])
				return
			}
		}
	} else {
		up, dirs := cdParts(blocks[1])
		if len(up) > 0 {
			if up == "~" {
				if dbm.context.KP.Dirs.Size() == 0 {
					fmt.Println("You can't go higher.")
				} else {
					dbm.context.Path = dbm.resetPath()
				}
			} else {
				if dbm.context.KP.Dirs.Size() == 0 {
					fmt.Println("You can't go higher.")
				} else {
					dbm.context.Path = dbm.popPath(up)
				}
			}
		}
		if len(dirs) > 0 {
			if dbm.context.KP.KeyParts.Size() > 0 {
				fmt.Println("You can't go deeper.")
				dbm.revertContext()
			} else {
				// first try using s[0] as a directory.
				// if the idMap does not have it,
				// try using s[0] as a record key.
				s := strings.Split(dirs, pathDelimiter)
				dbm.context.KP.Dirs.Push(s[0])
				if _, ok := dbm.idMap.Map[dbm.context.KP.Path()]; ok {
					dbm.context.Path = dbm.context.KP.Path()
				} else { // try as record key
					dbm.context.KP.Dirs.Pop()
					dbm.context.KP.KeyParts.Push(s[0])
					if _, ok := dbm.idMap.Map[dbm.context.KP.Path()]; !ok {
						fmt.Printf("%s not found\n", s[0])
						dbm.revertContext()
						dbm.setSuggestions()
						return
					}
					dbm.context.Path = dbm.context.KP.Path()
				}
			}
		}
		if dbm.context.InRecord() {
			if dbm.Mapper.Db.Exists(dbm.context.KP) {
				dbm.setSuggestions()
				dbm.showValue()
				if dbm.settings.Hints {
					var topic string
					key := dbm.context.KP.SegmentString(dbm.context.KP.KeyParts)
					if !dbm.context.KP.Dirs.Empty() {
						topic = dbm.context.KP.Dirs.Last()
					}
					fmt.Printf("Hint: cmp to compare values for records with topic/key = %s/%s\n", topic, key)
				}
				return
			} else {
				dbm.revertContext()
				fmt.Printf("%s not found\n", blocks[1])
			}
		}
	}
	dbm.setSuggestions()
}

// If the context is 3 levels deep, i.e. library/topic/key, displays value of all records with the same topic/key
func (dbm *DbM) compareValues() {
	if dbm.context.KP.KeyParts.Empty() {
		fmt.Println("You must be at the key level to use this command.")
	} else {
		dbm.context.Matcher = kvs.NewMatcher()
		dbm.context.Matcher.KP = dbm.context.KP.Copy()
		dbm.context.Matcher = dbm.context.Matcher.TopicKeyMatcher()
		recs, _, err := dbm.Mapper.Db.GetMatching(*dbm.context.Matcher)
		if err != nil {
			fmt.Println(err)
			return
		}
		dbm.ResetIdMap()
		dbm.context.SetTerminalHW()
		for i, rec := range recs {
			path := rec.KP.Path()
			dbm.AddPathToMap(rec.KP)
			left := fmt.Sprintf("%4d %s\n", i+1, path)
			leftMargin := 4             // len(left) + 4
			width := dbm.context.TWidth // - leftMargin
			v := ltstring.WrappedLines(rec.Value, leftMargin, width, 0)
			fmt.Fprintf(color.Output, "%s    %s\n", left, color.HiCyanString(v))
		}
		if dbm.settings.Hints {
			l := len(dbm.idMap.Map)
			fmt.Printf("\nHint: cd {number} to change directory, e.g. cd %d to change to %s\n", l, dbm.context.Keys[l-1].Path())
		}
	}
}

// WordListFrequencyCount reads all records in the current context
// and displays a list with frequency counts.
func (dbm *DbM) WordListFrequencyCount(args []string) {

	var sortFrequencyAscending bool
	var sortFrequencyDescending bool
	var includeNumbers bool
	var includePunctuation bool
	var export bool

	for _, arg := range args {
		switch arg {
		case "freq":
		case "-a":
			sortFrequencyAscending = true
		case "-d":
			sortFrequencyDescending = true
		case "-ex":
			export = true
		case "-in":
			includeNumbers = true
		default:
			fmt.Printf("unknown flag %s\n", arg)
			return
		}
	}

	dbm.context.Matcher = kvs.NewMatcher()
	dbm.context.Matcher.KP = dbm.context.KP.Copy()
	if !dbm.settings.Exact { // use nnp
		dbm.context.Matcher.KP.Dirs.Set(0, dbm.context.Matcher.KP.Dirs.First()+"-nnp")
	}
	dbm.context.Matcher.Recursive = true

	var recs []*kvs.DbR
	var err error

	if len(dbm.context.Matcher.KP.Key()) > 0 {
		rec, err := dbm.Mapper.Db.Get(dbm.context.Matcher.KP)
		if err != nil {
			fmt.Println(err)
			return
		}
		recs = append(recs, rec)
	} else {
		// TODO: use count for frequency, it is the return value current ignored below
		recs, _, err = dbm.Mapper.Db.GetMatching(*dbm.context.Matcher)
		if err != nil {
			fmt.Println(err)
			return
		}
	}
	if len(recs) > 0 {
		Nlp := nlp.NewNLP()
		Nlp.HyphenDir = config.DoxaPaths.HyphenPath
		Nlp.PunctuationChars = dbm.settings.HyphenPunctuationChars
		Nlp.ScansionChars = dbm.settings.HyphenScansionChars

		for _, rec := range recs {
			if !rec.IsRedirect() {
				// TODO: extract lang from path
				Nlp.Count(rec.KP.Path(), rec.Value, "en", includeNumbers, includePunctuation, nil)
			}
		}
		dbm.FrequencyData = Nlp.Frequency.FrequencyDataSlice()

		if sortFrequencyAscending {
			sort.Sort(nlp.FreqSorterAscending(dbm.FrequencyData))
		} else if sortFrequencyDescending {
			sort.Sort(nlp.FreqSorterDescending(dbm.FrequencyData))
		} else {
			sort.Sort(nlp.TokenSorter(dbm.FrequencyData))
		}
		// export
		dirPath := dbm.context.KP.DirPath()
		if len(dirPath) == 0 {
			dirPath = "doxaDatabase"
		}
		if export {
			var filePath string
			if dbm.settings.Exact {
				filePath = path.Join(config.DoxaPaths.ExportPath, "frequency", dirPath+".csv")
			} else {
				filePath = path.Join(config.DoxaPaths.ExportPath, "frequency", dirPath+".normalized.csv")
			}
			err = nlp.Export(filePath, dbm.FrequencyData)
			if err == nil {
				fmt.Printf("frequencies exported to %s\n", filePath)
			} else {
				fmt.Printf("error exporting frequencies to %s\n", filePath)
			}
			return
		}
		// display line by line
		var displayed int
		size := len(dbm.FrequencyData)
		for i, fd := range dbm.FrequencyData {
			dbm.context.SetTerminalHW()
			if len(recs) == 1 {
				fmt.Printf("%d %s : %d\n", i+1, fd.Token, fd.Frequency)
			} else {
				if len(fd.FirstFivePaths) > 1 {
					fmt.Printf("%d %s : %d, e.g. %s\n", i+1, fd.Token, fd.Frequency, fd.FirstFivePaths[0])
				} else {
					fmt.Printf("%d %s : %d, in %s\n", i+1, fd.Token, fd.Frequency, fd.FirstFivePaths[0])
				}
			}
			displayed++
			if displayed >= dbm.context.THeight-3 {
				quit, recNbr := dbm.Quit(i+1, size, "")
				recNbr--
				if quit {
					if recNbr > -1 {
						if len(dbm.FrequencyData[recNbr].FirstFivePaths) > 1 {
							dbm.Executor(fmt.Sprintf("find %s", dbm.FrequencyData[recNbr].Token))
						} else {
							path := strings.ReplaceAll(dbm.FrequencyData[recNbr].FirstFivePaths[0], "-nnp", "")
							err = dbm.context.KP.ParsePath(path)
							if err != nil {
								fmt.Printf("freq call to find: %v", err)
							}
							dbm.SetPath()
							return
						}
					}
					break
				}
				displayed = 0
			}
		}
	}
}
func (dbm *DbM) hyphenate(args []string) {
	// initialize an nlp
	Nlp := nlp.NewNLP()
	Nlp.HyphenDir = config.DoxaPaths.HyphenPath
	Nlp.KeepPunctuation = dbm.settings.HyphenKeepPunctuation
	Nlp.KeepScansion = dbm.settings.HyphenKeepScansion
	Nlp.PunctuationChars = dbm.settings.HyphenPunctuationChars
	Nlp.ScansionChars = dbm.settings.HyphenScansionChars

	if len(args) > 1 {
		if args[1] == "-d" {
			if len(args) > 2 {
				h, err := Nlp.DebugHyphenate("", args[2], "en")
				if err != nil {
					fmt.Println(err)
				} else {
					fmt.Print(h)
				}
			} else {
				fmt.Println("hyphen -d works on a single word, e.g. hyphen -d hypothesis")
			}
			return
		}

	}
	if dbm.context.KP.KeyParts.Empty() {
		fmt.Println("You must be at the key level to use this command.")
	} else {
		rec, _, err := dbm.Mapper.Db.GetResolved(dbm.context.KP)
		if err != nil {
			fmt.Println(err)
		} else {
			hyphenated, err := Nlp.Hyphenate(
				rec.KP.Path(),
				rec.Value,
				"en")
			if err != nil {
				fmt.Println(err)
				return
			}
			fmt.Println(hyphenated)
		}
	}
}
func (dbm *DbM) find(blocks []string) {
	if dbm.context.KP.Dirs.Empty() {
		fmt.Println("You can't use the find command from the root directory. cd into a sub-directory first.")
		return
	}
	if dbm.context.InRecord() {
		fmt.Println("You can't use the find command inside a record.")
		return
	}
	dbm.conc.Map = make(map[string]concord.ConcordanceLine)
	tWidth, tHeight, err := terminal.GetSize(0)
	if err != nil {
		fmt.Println(err)
	} else {
		dbm.context.THeight = tHeight
		dbm.context.TWidth = tWidth
	}
	var firstKeyWord string

	if len(blocks) == 1 {
		fmt.Println("You must tell me what value to find")
	} else {
		var recs []*kvs.DbR
		var err error
		dbm.context.Matcher = kvs.NewMatcher()
		dbm.context.Matcher.ForConcordance = true
		if dbm.settings.IDlike.Dirs.Empty() {
			dbm.context.Matcher.KP = dbm.context.KP.Copy()
		} else {
			dbm.context.Matcher.KP = dbm.settings.IDlike.Copy()
		}
		dbm.context.Matcher.ValuePattern = strings.Join(blocks[1:], " ") // value
		dbm.context.Matcher.Recursive = true
		dbm.context.Matcher.WholeWord = dbm.settings.WholeWord

		// TODO: 1) if we set (?i) here, don't do it in db-explorer.js.  2) figure out how to get
		// boldDb matcher to work by on-the-fly conversion of record value to nnp version.
		if !dbm.settings.Exact {
			// add regex case-insensitive flag
			dbm.context.Matcher.ValuePattern = "(?i)" + dbm.context.Matcher.ValuePattern
		}
		err = dbm.context.Matcher.UseAsRegEx()
		if err != nil {
			fmt.Printf("bad regex: %v", err)
		}
		recs, _, err = dbm.Mapper.Db.GetMatching(*dbm.context.Matcher)
		dbm.context.Matcher.ClearValueRegEx() // for its next use
		if err != nil {
			fmt.Println(err)
		} else {
			for _, rec := range recs {
				line := rec.Value
				// There is a bug because rec.Indexes which is computed using NNP, but the line was
				// switched to the non-NNP value. See line 446 in boltdb.go and figure it out.
				// Maybe set a bool to indicate it is for the command line and
				// don't swap the non-NNP for the nnp.
				for _, i := range rec.Indexes {
					var key string
					if len(i) == 2 {
						// if the search pattern contained .*, we can get the entire record value back as the key.
						// so, in such cases, use the firstKeyWord as the key.
						if len(firstKeyWord) > 0 {
							key = firstKeyWord
						} else {
							key = line[i[0]:i[1]]
						}
						dbm.conc.Line(fmt.Sprintf("%*s", dbm.settings.Padding.P2, rec.KP.Path()), line, key, dbm.settings.Width, dbm.settings.Sort)
					}
				}
			}
			if len(dbm.conc.Map) > 0 {
				dbm.listFinds()
			}
		}
	}
}
func (dbm *DbM) findNear(blocks []string) {
	if dbm.context.KP.Dirs.Empty() {
		fmt.Println("You can't use the findNear command from the root directory. cd into a sub-directory first.")
		return
	}
	if dbm.context.InRecord() {
		fmt.Println("You can't use the findNear command inside a record.")
		return
	}

	dbm.conc.Map = make(map[string]concord.ConcordanceLine)
	tWidth, tHeight, err := terminal.GetSize(0)
	if err != nil {
		fmt.Println(err)
	} else {
		dbm.context.THeight = tHeight
		dbm.context.TWidth = tWidth
	}
	var w1, w2 string
	min := 1
	max := 6

	switch len(blocks) {
	case 3:
		w1 = blocks[1]
		w2 = blocks[2]
	case 5:
		w1 = blocks[1]
		w2 = blocks[2]
		min, err = strconv.Atoi(blocks[3])
		if err != nil {
			fmt.Printf("bad start number %s: %v\n", blocks[3], err)
			return
		}
		max, err = strconv.Atoi(blocks[4])
		if err != nil {
			fmt.Printf("bad ending number %s: %v\n", blocks[4], err)
			return
		}
	default:
		fmt.Println("You must tell me what to find, e.g. findNear joy angel 1 6")
		return
	}
	if len(blocks) == 1 {
		fmt.Println("You must tell me what to find, e.g. findNear joy angel 1 6")
	} else {
		// Below is a regEx way to find 2 words near each other.
		// But, it does not work with Greek.
		//	value = fmt.Sprintf("\\b%s\\W+(?:\\w+\\W+){%d,%d}?%s\\b", w1, min, max, w2)
		value := fmt.Sprintf("%s.*?%s", w1, w2)
		var recs []*kvs.DbR
		var err error
		dbm.context.Matcher = kvs.NewMatcher()
		if dbm.settings.IDlike.Dirs.Empty() {
			dbm.context.Matcher.KP = dbm.context.KP.Copy()
		} else {
			dbm.context.Matcher.KP = dbm.settings.IDlike.Copy()
		}
		dbm.context.Matcher.ValuePattern = value
		dbm.context.Matcher.Recursive = true

		if !dbm.settings.Exact {
			// add regex case-insensitive flag
			dbm.context.Matcher.ValuePattern = "(?i)" + value
		}
		err = dbm.context.Matcher.UseAsRegEx()
		if err != nil {
			fmt.Printf("bad regex: %v", err)
		}
		recs, _, err = dbm.Mapper.Db.GetMatching(*dbm.context.Matcher)
		if err != nil {
			fmt.Println(err)
		} else {
			for _, rec := range recs {
				line := rec.Value
				for _, i := range rec.Indexes {
					var key string
					if len(i) == 2 {
						key = line[i[0]:i[1]]
						var inRange bool
						if dbm.settings.WholeWord {
							dbm.inRange(line, w1, w2, min, max)
						} else {
							inRange = dbm.inRange(key, w1, w2, min, max)
						}
						if inRange {
							dbm.conc.Line(fmt.Sprintf("%*s", dbm.settings.Padding.P2, rec.KP.Path()), line, key, dbm.settings.Width, dbm.settings.Sort)
						}
					}
				}
			}
			if len(dbm.conc.Map) > 0 {
				dbm.listFinds()
			}
		}
	}
}

// we use a brute force but crude
// check to see if w2 occurs with min...max words of w1
func (dbm *DbM) inRange(s, w1, w2 string, min, max int) bool {
	i := strings.Index(s, w1)
	if i < 0 {
		return false
	}
	parts := strings.Split(s[i:], " ")
	for i, p := range parts {
		if len(p) < 1 {
			continue
		}
		if dbm.settings.WholeWord {
			if p == w2 {
				return i >= min && i <= max
			}
		} else {
			if strings.Contains(p, w2) {
				return i >= min && i <= max
			}
		}
	}
	return false
}
func (dbm *DbM) findID(blocks []string) {
	if dbm.context.InRecord() {
		fmt.Println("You can't use the findID command inside a record.")
		return
	}

	switch len(blocks) {
	case 1:
		fmt.Println("missing pattern for findID, e.g. .*/actors/.*")
	case 2:
		dbm.context.Matcher = kvs.NewMatcher()
		contextPath := dbm.context.Path
		var path string
		if len(contextPath) > 0 {
			path = contextPath + "/"
		}
		//		path = path + blocks[1]
		dbm.context.Matcher.KP.ParsePath(path)
		dbm.context.Matcher.ValuePattern = path + blocks[1]
		fmt.Println(dbm.context.Matcher.ValuePattern)
		dbm.context.Matcher.UseAsRegEx()
		dbm.context.Matcher.Recursive = true
		dbm.context.Matcher.IncludeEmpty = dbm.settings.ShowEmpty
		ids, err := dbm.Mapper.Db.GetMatchingIDs(*dbm.context.Matcher)
		if err != nil {
			fmt.Println(err)
			return
		}
		if ids == nil {
			fmt.Printf("no record ID matches %s\n", blocks[1])
		} else {
			var displayed int
			dbm.ResetIdMap()
			size := len(ids)
			for i, id := range ids {
				dbm.context.SetTerminalHW()
				dbm.AddPathToMap(id.KP)
				fmt.Printf("%4d %s\n", i+1, id.KP.Path())
				// chunk the display into what fits terminal height
				// and provide user a means to quit or continue
				displayed++
				if displayed >= dbm.context.THeight-3 {
					quit, recNbr := dbm.Quit(i+1, size, "")
					if quit {
						if recNbr > -1 {
							dbm.Executor(fmt.Sprintf("cd %d", recNbr))
						}
						break
					}

					displayed = 0
				}
			}
		}
	default:
		fmt.Println("pattern for findID must not have spaces")
	}
}
func (dbm *DbM) showHelp() {
	for _, command := range dbm.Commands {
		fmt.Printf("%s:\t%s\n", command.Text, command.Description)
	}
	if dbm.settings.Hints {
		fmt.Println("")
		fmt.Println("* indicates a command that is not available yet.")
		fmt.Println("Commands that start with a dot are for the dbm.settings.")
		fmt.Println("The settings affect the way the other commands behave.")
		fmt.Printf("As you type, suggestions will appear as a pop up.\nTab to set focus on the pop up.\nYou can scroll up or down using the arrow keys.\nUse the <Enter> key to select an item in the pop up.\n")
		fmt.Println("You can also use the up and down keys to scroll through previously issued commands.")
		fmt.Println("An ID has 2 parts: the directory (path) and a key.  An example is:")
		fmt.Printf("\tltx/gr_gr_cog/variables.info:Theotokos.title\n")
		fmt.Printf("ltx = liturgical text. It is a directory and indicates the type of records it contains.\n")
		fmt.Printf("gr_gr_cog is a directory and is the `library`.\n")
		fmt.Printf("variables.info is a directory and is the `topic`.\n")
		fmt.Printf("Directory names are separated by a forward slash `/`.\n")
		fmt.Printf("The directory path and key of a record are separated by a colon `:`\n")
		fmt.Printf("Theotokos.title is the record key.  You can tell because it is preceeded by a colon `:`\n")
		fmt.Printf("Edit the config.yaml file cmd.shell settings to configure the shell to your liking.\n")
	}
}

// show Topic-Key as LML Topic-Key. One use of the shell is when the user is creating templates and is trying to find
// a topic-Key.  This function will display the topic-key in the correct format
// for the liturgical markup language. Note that LML does not use libraries in the template.
// The libraries are prefixed to LML topic-keys when books/services are generated.
func (dbm *DbM) showTKAsLML() {
	if dbm.context.InRecord() {
		msg := fmt.Sprintf(`"%s/%s"`, dbm.context.KP.Dirs.Last(), dbm.context.KP.KeyParts.Join(segmentDelimiter))
		fmt.Println(msg)
	} else {
		fmt.Println("You must be inside the record to use this command")
	}
}

// list directories and records. If in a record, also show value.
func (dbm *DbM) list(blocks []string) {
	dbm.setSuggestions() // rebuild contextual list of dirs and recs.
	if dbm.context.InRecord() {
		dbm.showValue()
	} else {
		var displayed int
		size := len(dbm.context.Keys)
		for i, id := range dbm.context.Keys {
			dbm.context.SetTerminalHW()
			last, dir := id.Last()
			if dir {
				last = last + "/"
			}
			fmt.Printf("%4d %s\n", i+1, last)
			// chunk the display into what fits terminal height
			// and provide user a means to quit or continue
			displayed++
			if displayed >= dbm.context.THeight-3 {
				quit, recNbr := dbm.Quit(i+1, size, "")
				if quit {
					if recNbr > -1 {
						dbm.Executor(fmt.Sprintf("cd %d", recNbr))
					}
					break
				}
				displayed = 0
			}
		}
	}
}
func (dbm *DbM) switchDataStores(name string) {
	if s := dbm.Stores.Get(name); s != nil {
		dbm.Mapper = s.Kvs
		return
	}
	q := "enter the database number to switch to:"
	var d []string
	stores := dbm.Stores.SliceSortedByPrompt()
	for i, s := range stores {
		d = append(d, fmt.Sprintf("%d", i+1)) // add 1 so it is user-friendly
		d = append(d, fmt.Sprintf("%s\t- %s %s", s.Prompt, s.Description, s.Path))
	}
	d = append(d, "q", "quit")
	choice := string(clinput.GetInput(q, d))
	if choice == "q" {
		return
	}
	index, err := strconv.Atoi(choice)
	if err != nil {
		fmt.Println("Invalid choice")
		return
	}
	index-- // To be user-friendly we had added 1 to the index.  See above.
	dbm.CurrentStore = stores[index]
	dbm.Mapper = stores[index].Kvs
	dbm.context = NewContext()
	dbm.list([]string{""})
	return
}

func (dbm *DbM) listDataStores() {
	for _, s := range dbm.Stores.SliceSortedByPrompt() {
		fmt.Printf("%s\t- %s %s\n", s.Prompt, s.Description, s.Path)
	}
}
func (dbm *DbM) listFinds() {
	if len(dbm.conc.Map) == 0 {
		fmt.Println("The concordance is empty. Run the find command first.")
		return
	}
	dbm.ResetIdMap()
	var displayed int
	size := len(dbm.conc.Map)
	cnt := 0
	for i, res := range concord.SortedKeys(dbm.conc.Map, dbm.settings.Sort) {
		dbm.context.SetTerminalHW()
		cnt++
		kp := kvs.NewKeyPath()
		id := strings.TrimSpace(dbm.conc.Map[res].ID)
		err := kp.ParsePath(id)
		if err != nil {
			fmt.Printf("error parsing %s: %v ", id, err)
		}
		first := kp.Dirs.First()
		if !dbm.settings.Exact && strings.HasSuffix(first, "-nnp") {
			kp.Dirs.Set(0, first[:len(first)-4])
		}
		dbm.AddPathToMap(kp)

		if len(strings.TrimSpace(dbm.conc.Map[res].ID)) > 50 {
			parts := strings.Split(dbm.conc.Map[res].ID, pathDelimiter)
			if len(parts) == 2 {
				fmt.Fprintf(color.Output, "%*d |%s%s\n", dbm.settings.Padding.P1, i+1, parts[0], pathDelimiter)
				fmt.Fprintf(color.Output, "     | %*s | %-s %s %s\n", dbm.settings.Padding.P2, parts[1], dbm.conc.Map[res].Left, color.HiCyanString(dbm.conc.Map[res].Key), dbm.conc.Map[res].Right)
				displayed++
			} else {
				fmt.Fprintf(color.Output, "%*d | %*s | %-s %s %s\n", dbm.settings.Padding.P1, i+1, dbm.settings.Padding.P2, dbm.conc.Map[res].ID, dbm.conc.Map[res].Left, color.HiCyanString(dbm.conc.Map[res].Key), dbm.conc.Map[res].Right)
			}
		} else {
			fmt.Fprintf(color.Output, "%*d | %*s | %-s %s %s\n", dbm.settings.Padding.P1, i+1, dbm.settings.Padding.P2, dbm.conc.Map[res].ID, dbm.conc.Map[res].Left, color.HiCyanString(dbm.conc.Map[res].Key), dbm.conc.Map[res].Right)
		}
		// chunk the display into what fits terminal height
		// and provide user a means to quit or continue
		displayed++
		if displayed >= dbm.context.THeight-3 {
			quit, recNbr := dbm.Quit(cnt, size, "")
			if quit {
				if recNbr > -1 {
					dbm.Executor(fmt.Sprintf("cd %d", recNbr))
				}
				break
			}
			displayed = 0
		}

	}
	// let user know the sort order used
	var sortMsg string
	switch dbm.settings.Sort {
	case concord.SortId:
		sortMsg = "ID"
	case concord.SortLeft:
		sortMsg = "word(s) left of keyword"
	case concord.SortRight:
		sortMsg = "word(s) right of keyword"
	}
	fmt.Printf("%d records found, sorted by %s, ", size, sortMsg)
	if dbm.settings.Exact {
		fmt.Printf(".exact on, ")
	} else {
		fmt.Printf(".exact off, ")
	}
	if dbm.settings.IDlike.Dirs.Size() > 0 {
		fmt.Printf(".idlike = %s, so current path was not used. To turn off: idlike %%", dbm.settings.IDlike.Dirs.Join(segmentDelimiter))
	}
	if dbm.settings.WholeWord {
		fmt.Printf(" .wholeword on")
	} else {
		fmt.Printf(" .wholeword off")
	}
	fmt.Println("")
	if dbm.settings.Hints {
		fmt.Printf("Hint: use cd {number} to view a record listed above, e.g. cd %d\n", cnt)
	}
}

// makeDirectory creates the directory specified in blocks[1]
func (dbm *DbM) makeDirectory(blocks []string) {
	if dbm.context.InRecord() {
		fmt.Println("Can't make a directory. You are in a record.")
		return
	}
	if len(blocks) > 1 {
		dbm.copyContext()
		parts := strings.Split(blocks[1], segmentDelimiter)
		for _, p := range parts {
			p = strings.TrimSpace(p)
			if len(p) > 0 {
				dbm.context.KP.Dirs.Push(p)
			}
		}
		err := dbm.Mapper.Db.MakeDir(dbm.context.KP)
		if err != nil {
			fmt.Printf("error creating directory %s: %v\n", blocks[1], err)
		}
		dbm.revertContext()
		dbm.setSuggestions()
	}
}
func (dbm *DbM) putRecord(blocks []string) {
	if len(blocks) < 2 {
		fmt.Printf("Can't put record.  No key provided.")
		return
	}
	if dbm.context.KP.IsRecord() {
		fmt.Printf("Can't put record %s. Already in record %s. Did you mean to use the set command?\n", blocks[1], dbm.context.Path)
		return
	}
	dbm.copyContext()
	dbm.context.KP.KeyParts.Push(blocks[1])
	dbr := kvs.NewDbR()
	dbr.KP = dbm.context.KP.Copy()
	err := dbm.Mapper.Db.Put(dbr)
	if err != nil {
		fmt.Printf("Error creating record %s: %v\n", blocks[1], err)
		dbm.revertContext()
		return
	}
	//	revertContext()
	dbm.setSuggestions()
	//	changeDirectory(blocks)
	fmt.Println("To set the value of the record, use the set command.")

}
func (dbm *DbM) removeRecord(blocks []string) {
	if len(blocks) < 2 {
		fmt.Println("can't remove record.  No key provided.")
		return
	}
	var force bool
	if len(blocks) > 2 {
		if blocks[2] == "-force" {
			force = true
		} else {
			fmt.Printf("unknown option %s\n", blocks[2])
			return
		}
	}
	dbm.copyContext()
	if isNumber(blocks[1]) {
		index, err := strconv.Atoi(blocks[1])
		if err != nil {
			fmt.Println(err)
		} else {
			if index >= 0 && len(dbm.context.Keys) > index-1 {
				dbm.context.KP = dbm.context.Keys[index-1]
				if dbm.context.KP.KeyParts.Empty() {
					fmt.Printf("can't delete a directory using the rm command.  Use rmdir.\n")
					dbm.revertContext()
					dbm.setSuggestions()
					return
				}
			} else {
				fmt.Printf("record %s not found\n", blocks[1])
				if dbm.settings.Hints {
					fmt.Printf("Did you mean to use the remove directory command, i.e., rmdir %s\n", blocks[1])
				}
			}
		}
	} else {
		dbm.context.KP.KeyParts.Push(blocks[1])
		if _, ok := dbm.idMap.Map[dbm.context.KP.Path()]; !ok {
			fmt.Printf("record %s not found.\n", blocks[1])
			if dbm.settings.Hints {
				fmt.Printf("Did you mean to use the remove directory command, i.e., rmdir %s\n", blocks[1])
			}
			dbm.revertContext()
			dbm.setSuggestions()
			return
		}
	}
	if !dbm.context.InRecord() {
		fmt.Printf("%s is not a record.  Can't use rm command.\n", dbm.context.KP.Path())
		dbm.revertContext()
		dbm.setSuggestions()
		return
	}
	if !dbm.Mapper.Db.Exists(dbm.context.KP) {
		fmt.Printf("can't remove record. %s does not exist.\n", dbm.context.KP.Path())
		dbm.revertContext()
		dbm.setSuggestions()
		return
	}
	if usedBy, _ := dbm.Mapper.Db.GetRedirectsToRecord(dbm.context.KP); len(usedBy) > 0 {
		dbm.loadRedirects(usedBy, false)
		cnt := len(usedBy)
		np := "records listed above that redirect"
		if cnt == 1 {
			np = "record listed above that redirects"
		}
		if force {
			fmt.Printf("Record %s has %d %s to it. Because you used the -force flag, it will be deleted, but you need to fix the records pointing to it.\n", dbm.context.KP.Path(), cnt, np)
		} else {
			fmt.Printf("Record %s has %d %s to it.\nEdit the records to redirect to another record,\n then you can delete %s.\n Or, use the -force flag.", dbm.context.KP.Path(), cnt, np, dbm.context.KP.Path())
			dbm.revertContext()
			dbm.setSuggestions()
			return
		}
	}
	err := dbm.Mapper.Db.Delete(*dbm.context.KP)
	if err != nil {
		fmt.Printf("error deleting %s: %v\n", dbm.context.KP.Path(), err)
	}
	dbm.revertContext()
	dbm.setSuggestions()
}

// removeDirectory removes the directory specified in blocks[1]
func (dbm *DbM) removeDirectory(blocks []string) {
	if len(blocks) > 1 {
		var force bool
		var adminOverride bool
		for i := 1; i < len(blocks); i++ {
			switch blocks[i] {
			case "-force":
				force = true
			case "-admin":
				adminOverride = true
			}
		}
		// drop trailing forward slash if dir name has it
		if strings.HasSuffix(blocks[1], "/") {
			blocks[1] = blocks[1][:len(blocks[1])-1]
		}
		dbm.copyContext()
		if isNumber(blocks[1]) {
			index, err := strconv.Atoi(blocks[1])
			if err != nil {
				fmt.Println(err)
				dbm.revertContext()
			} else {
				if index >= 0 && len(dbm.context.Keys) > index-1 {
					dbm.context.KP = dbm.context.Keys[index-1]
				} else {
					fmt.Printf("%s not found\n", blocks[1])
					dbm.revertContext()
					return
				}
			}
		} else {
			dbm.context.KP.Dirs.Push(blocks[1])
			if _, ok := dbm.idMap.Map[dbm.context.KP.Path()]; !ok {
				fmt.Printf("%s not found\n", blocks[1])
				dbm.revertContext()
				return
			}
		}
		if kvs.IsSystem(dbm.context.KP.Dirs.First()) && !adminOverride {
			fmt.Printf("You are in a system directory, %s.  You can't manually delete it or subdirectories in it.\n", dbm.context.KP.Dirs.First())
			dbm.revertContext()
			return
		}
		if dbm.Mapper.Db.Exists(dbm.context.KP) {
			_, dirs, recs, _ := dbm.Mapper.Db.Keys(dbm.context.KP)
			if dirs > 0 || recs > 0 {
				fmt.Println("Directory is not empty.")
			}
			fmt.Println("Checking to see if any records redirect to records in this directory...")
			usedBy, _ := dbm.Mapper.Db.GetRedirectsToDir(dbm.context.KP)
			if len(usedBy) > 0 {
				dbm.loadRedirects(usedBy, true)
				cnt := len(usedBy)
				np := "records listed above redirect"
				if cnt == 1 {
					np = "record listed above redirects"
				}
				fmt.Printf("Directory %s %d %s to it.\nEdit the records to redirect to another record,\n then you can delete %s.\n", dbm.context.KP.Path(), cnt, np, dbm.context.KP.Path())
			}
			if force {
				fmt.Println("Deleting directory...")
				err := dbm.Mapper.Db.Delete(*dbm.context.KP)
				if err != nil {
					fmt.Printf("error deleting directory %s: %v\n", blocks[1], err)
				}
			} else {
				fmt.Println("Because you did not use the -force flag and this directory is not empty or has redirects to records in it, you may not delete it.")
			}
		} else {
			fmt.Printf("directory %s does not exist\n", blocks[1])
		}
		dbm.revertContext()
		dbm.setSuggestions()
	}
}

// For the record whose id matches the context, sets the specified column
// which can be comment, redirect, or value.
func (dbm *DbM) setRecord(blocks []string) {
	if !dbm.context.InRecord() {
		fmt.Printf("can't set. Not in record.\n")
		return
	}
	dbr := kvs.NewDbR()
	dbr.KP = dbm.context.KP.Copy()

	switch len(blocks) {
	case 0:
		fmt.Printf("can't set record.  No command\n")
		return
	case 1:
		fmt.Printf("can't set. You must indicate whether this is a redirect or value.\n")
		fmt.Printf("e.g., set redirect, or set value.\n")
		return
	case 2: // set value to empty string.  Already instantiated as such.
	default:
		dbr.Value = strings.Join(blocks[2:], " ")
	}
	switch blocks[1] {
	case "redirect":
		if len(dbr.Value) > 0 {
			kp := kvs.NewKeyPath()
			kp.ParsePath(dbr.Value)
			if !dbm.Mapper.Db.Exists(kp) {
				fmt.Printf("can't redirect to %s: does not exist.\n", dbr.Value)
				parts := strings.Split(dbr.Value, pathDelimiter)
				if len(parts) == 1 {
					parts = strings.Split(dbr.Value, segmentDelimiter)
					j := len(parts) - 1
					sb := strings.Builder{}
					for i := 0; i < j; i++ {
						if sb.Len() > 0 {
							sb.WriteString(segmentDelimiter)
						}
						sb.WriteString(parts[i])
					}
					sb.WriteString(pathDelimiter)
					sb.WriteString(parts[len(parts)-1])
					fmt.Printf("Did you mean: %s\n", sb.String())
				}
				return
			}
			if !strings.HasPrefix(dbr.Value, "@") {
				dbr.Value = "@" + dbr.Value
			}
		}
	case "value": // nothing more to do, yet
	default:
		fmt.Printf("I don't understand. Did you mean: set value %s\n", blocks[1])
		return
	}
	err := dbm.Mapper.Db.Put(dbr)
	if err != nil {
		fmt.Printf("error setting record to %s: %v\n", dbr.Value, err)
		return
	}
	// if the record that was updated is a configuration setting
	// update the dbm config settings and settings manager settings.
	if dbr.KP.Dirs.First() == "configs" {
		dbm.SM.UpdateConfiguration(dbr.KP.DirPath())
		dbm.SetConfig()
	}
}
func (dbm *DbM) swap(blocks []string) {
	if dbm.context.KP.Dirs.Last() != "values" {
		fmt.Println("The swap command only works inside a list of values with record keys that are numbers")
		return
	}
	if len(dbm.context.Keys) < 2 {
		fmt.Println("Not enough keys to do a swap.")
		return
	}
	if len(blocks) != 3 {
		fmt.Println("You must indicate the items to swap, e.g. swap 4 1")
		return
	}
	from := blocks[1]
	var fromIndex int
	var fromDbr *kvs.DbR

	to := blocks[2]
	var toIndex int
	var toDbr *kvs.DbR
	var err error

	if fromIndex, err = strconv.Atoi(from); err != nil {
		fmt.Printf("%s is not a number\n", from)
		return
	}
	if toIndex, err = strconv.Atoi(to); err != nil {
		fmt.Printf("%s is not a number\n", to)
		return
	}
	// decrement the indexes because the keys slice is zero based and the record keys are 1 based
	fromIndex--
	toIndex--
	l := len(dbm.context.Keys)
	if fromIndex < 0 || fromIndex > l {
		fmt.Printf("invalid key %s\n", from)
		return
	}
	if toIndex < 0 || toIndex > l {
		fmt.Printf("invalid key %s\n", to)
		return
	}
	fromDbr, err = dbm.Mapper.Db.Get(dbm.context.Keys[fromIndex])
	if err != nil {
		fmt.Println(err)
	}
	toDbr, err = dbm.Mapper.Db.Get(dbm.context.Keys[toIndex])
	if err != nil {
		fmt.Println(err)
	}
	fromValue := fromDbr.Value
	toValue := toDbr.Value
	fromDbr.Value = toValue
	toDbr.Value = fromValue
	err = dbm.Mapper.Db.Put(fromDbr)
	if err != nil {
		fmt.Println(err)
	}
	err = dbm.Mapper.Db.Put(toDbr)
	if err != nil {
		fmt.Println(err)
	}
}
func (dbm *DbM) pwd() {
	fmt.Println(dbm.context.Path)
}

func (dbm *DbM) copy(args []string) {
	fmt.Printf("Command not implemented yet: %s\n", args)
	// will call
	// dbm.Mapper.Db.Copy()
}

type recordEdit struct {
	Dbm *DbM
}

func (re *recordEdit) Get(kp *kvs.KeyPath) (*kvs.DbR, error) {
	return re.Dbm.Mapper.Db.Get(re.Dbm.context.KP)
}

func (re *recordEdit) ValidPath(kp *kvs.KeyPath) bool {
	return re.Dbm.context.InRecord()
}

// AfterEdit makes any modifications to input string and returns resulting output string
func (re *recordEdit) AfterEdit(str string) (string, bool) {
	return str, true
}

// Put in this implementation is just a wrapper. other plugins might be more complex
func (re *recordEdit) Put(rec *kvs.DbR) error {
	return re.Dbm.Mapper.Db.Put(rec)
}

func (re *recordEdit) PreserveWhitespace() bool {
	return re.Dbm.context.KP.Dirs.First() == "templates"
}

func (re *recordEdit) SetKVS(k *kvs.KVS) {
	return
}

func (re *recordEdit) SetLiturgicalDb(k *kvs.KVS) {}

func (re *recordEdit) SetCatalogManager(m *catalog.Manager) {}

func (dbm *DbM) edit() {
	dbm.EdPlugins = append(dbm.EdPlugins, &recordEdit{Dbm: dbm})
	//if !dbm.context.InRecord() {
	proceed := false
	var editPlugin dbmPlugin.WithEdit
	for _, plug := range dbm.EdPlugins {
		if edPlug, applicable := plug.(dbmPlugin.WithEdit); applicable {
			edPlug.SetKVS(dbm.Mapper)
			edPlug.SetLiturgicalDb(dbm.Stores.Get(config.DbName).Kvs)
			edPlug.SetCatalogManager(dbm.CatalogManager)
			if edPlug.ValidPath(dbm.context.KP) {
				proceed = true
				editPlugin = edPlug
				break
			}
		}
	}
	//	}
	if !proceed {
		fmt.Println("edit command only available within a record")
		return
	}
	if editPlugin == nil {
		return
	}
	rec, err := editPlugin.Get(dbm.context.KP) //dbm.Mapper.Db.Get(dbm.context.KP)
	if err != nil {
		fmt.Println(err)
		return
	}
	if len(dbm.RecEditor) == 0 {
		dbm.RecEditor = GetOsEditor()
	}
	//isTemplate := dbm.context.KP.Dirs.First() == "templates"

	val, err := ed.Edit(rec.Value, dbm.RecEditor, dbm.EditPath, editPlugin.PreserveWhitespace() /*isTemplate*/)
	if val != rec.Value {
		rec.Value = val
		//err = dbm.Mapper.Db.Put(rec)
		if val, proceed = editPlugin.AfterEdit(val); proceed {
			err = editPlugin.Put(rec)
			if err == nil {
				fmt.Println(val)
			} else {
				fmt.Println(err)
			}
		}
	}
}

func (dbm *DbM) export() {
	dirPath := dbm.context.KP.DirPath()
	if len(dirPath) == 0 {
		dirPath = "doxaDatabase"
	}
	var filePath string
	if dbm.context.KP.Dirs.First() == "templates" {
		filePath = path.Join(config.DoxaPaths.ExportPath, "templates")
	} else {
		filePath = path.Join(config.DoxaPaths.ExportPath, dirPath+"."+kvs.FileExtension)
	}
	fmt.Printf("Exporting records in %s...\n", dirPath)
	err := dbm.Mapper.Db.Export(dbm.context.KP, filePath, kvs.ExportAsLine)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("Records exported to %s\n", filePath)
	}
}

// importFromFile loads the database from a file tab separated values.
// The file must be in the doxa/imports directory
// The function lists all files with a .tsv extension and
// allows the user to indicate which one to import from.
// Two basic tab formats are supported:
// id\tvalue
// dir\tdir\tdir\tkey\tvalue
// For the latter, the last field is treated as the value
// and the second to last as the key.
// The other (prior) fields are treated as dirs.
func (dbm *DbM) importFromFile() {
	//	var fileIn string
	var files []string
	var err error

	if files, err = ltfile.FileMatcher(config.DoxaPaths.ImportPath, kvs.FileExtension, nil); err != nil {
		doxlog.Errorf("%v", err)
		return
	}
	// MacNote: uncomment the next 7 lines to enable import from templates.
	// The generator will generate from templates in the db, but the lml editor does not.
	// The api handlers for lml will need to be modified to read from either the db or file system.
	// Until that change is made, the lines following the next one are commented out.
	exportedTemplatesPath := path.Join(config.DoxaPaths.ExportPath, "templates")
	if ltfile.DirExists(exportedTemplatesPath) {
		files = append(files, exportedTemplatesPath)
	}
	if ltfile.DirExists(config.DoxaPaths.PrimaryTemplatesPath) {
		files = append(files, config.DoxaPaths.PrimaryTemplatesPath)
	}
	// TODO: template import needs to be handled elsewhere.
	fmt.Println("Here are the available files.")
	fmt.Println("Enter the number of the file to import:")
	fmt.Println("")
	for i, f := range files {
		index := i + 1
		fmt.Printf("%v) %s\n", index, f)
	}
	var index int
	scanner := bufio.NewScanner(os.Stdin)
	if scanner.Scan() {
		input := scanner.Text()
		if index, err = strconv.Atoi(input); err != nil {
			return
		}
		if index > len(files) {
			fmt.Println("Invalid selection")
			return
		}
		index = index - 1
		if files[index] == exportedTemplatesPath {
			ltdm := ltm.NewLTDM(dbm.Mapper, "templates", "templates", "templates", dbm.paths.ExportPath)
			count, loadErr := ltdm.LoadContentFromFs(exportedTemplatesPath)
			if loadErr != nil {
				fmt.Printf("error loading %s: %v", exportedTemplatesPath, loadErr)
			}
			fmt.Printf("%d templates imported.\n", count)
			return
		} else if files[index] == dbm.paths.PrimaryTemplatesPath {
			ltdm := ltm.NewLTDM(dbm.Mapper, "templates", "templates", "templates", dbm.paths.PrimaryTemplatesPath)
			count, loadErr := ltdm.LoadContentFromFs(dbm.paths.PrimaryTemplatesPath)
			if loadErr != nil {
				fmt.Printf("error loading %s: %v\n", dbm.paths.PrimaryTemplatesPath, loadErr)
			}
			fmt.Printf("%d templates imported.\n", count)
			return
		}
		var removeQuotes = true // TODO: ask user for value
		var allOrNone = true    // TODO: ask user for value
		var skipObsoleteConfigs = true
		fmt.Printf("Importing records from %s...\n", files[index])
		count, errors := dbm.Mapper.Db.Import("", files[index], inOut.LineParser, removeQuotes, allOrNone, skipObsoleteConfigs)
		if len(errors) > 0 {
			for _, err := range errors {
				fmt.Printf("\n%v\n", err)
			}
			fmt.Println("")
		} else {
			p := message.NewPrinter(language.English)
			p.Printf("%d records imported.\n", count)
			fmt.Println("Reloading configuration from database...")
			dbm.setSuggestions()
			err := dbm.SM.ReadConfiguration()
			if err != nil {
				fmt.Println("error reloading configuration after import")
			}
		}
	}
}
func (dbm *DbM) clip(args []string) {
	switch len(args) {
	case 1:
		if dbm.context.InRecord() {
			dbm.AddResultsToClipboard()
		}
		fmt.Println("Saved to clipboard")
	case 2:
		switch args[1] {
		case "clear":
			dbm.clipboard.Clear()
			fmt.Println("Clipboard cleared")
		case "ls":
			dbm.clipboard.List()
		case ">":
			dbm.clip2text("")
		default:
			fmt.Println("Sorry, I did not understand you.")
		}
	case 3:
		if strings.HasSuffix(args[2], ".pdf") {
			dbm.clip2pdf(args[2])
		} else if strings.HasSuffix(args[2], ".txt") {
			dbm.clip2text(args[2])
		} else {
			fmt.Println("Invalid file format.  Only .pdf or .txt are supported.")
		}
	default:
		fmt.Println("Sorry, I did not understand you.")
	}
}
func (dbm *DbM) clip2pdf(filename string) {
	if dbm.clipboard.Empty() {
		if dbm.context.InRecord() {
			dbm.AddResultsToClipboard()
		}
	}
	filePath := config.DoxaPaths.ClipboardPath
	if len(filename) == 0 {
		filePath = path.Join(config.DoxaPaths.ClipboardPath, GetFilenameDate("pdf"))
	} else {
		filePath = path.Join(config.DoxaPaths.ClipboardPath, filename)
	}
	buildConfig := models.NewBuildConfig("", dbm.SM)
	buildConfig.AssetsPath = dbm.paths.AssetsPath
	buildConfig.SitePath = dbm.paths.SiteGenPath
	buildConfig.LoadFontMap()
	err := pdf.WriteRecords(filePath, dbm.clipboard.RecordMap, buildConfig.PdfFontMap)
	if err != nil {
		fmt.Printf("error writing clipboard to PDF: %v\n", err)
		return
	} else {
		fmt.Printf("Clipboard saved to %s\n", filePath)
	}
}
func (dbm *DbM) clip2text(filename string) {
	if dbm.clipboard.Empty() {
		if dbm.context.InRecord() {
			dbm.AddResultsToClipboard()
		}
	}
	filePath := config.DoxaPaths.ClipboardPath
	if len(filename) == 0 {
		filePath = path.Join(config.DoxaPaths.ClipboardPath, GetFilenameDate("txt"))
	} else {
		filePath = path.Join(config.DoxaPaths.ClipboardPath, filename)
	}
	var lines []string
	for _, rec := range dbm.clipboard.RecordMap {
		lines = append(lines, fmt.Sprintf("%s = %s", rec.KP.Path(), rec.Value))
	}
	err := ltfile.WriteLinesToFile(filePath, lines)
	if err != nil {
		fmt.Printf("error saving clipboard: %v\n", err)
	} else {
		fmt.Printf("Clipboard saved to %s\n", filePath)
	}
}

func GetFilenameDate(suffix string) string {
	// Use layout string for time format.
	const layout = "2006-01-02"
	// Place now in the string.
	t := time.Now()
	return fmt.Sprintf("doxa-%s-%02d-%02d-%02d.%s", t.Format(layout), t.Hour(), t.Minute(), t.Second(), suffix)
}
func (dbm *DbM) move(args []string) {
	fmt.Printf("Command not implemented yet: %s\n", args)
	// will call
	// dbm.Mapper.Db.Move()
}

func (dbm *DbM) tree(blocks []string) {
	if dbm.context.InRecord() {
		fmt.Println("You can't use the tree command inside a record. Move up a level first.")
		return
	}
	depth := 3
	var width int
	var dirOnly bool
	var err error
	var excludeRedirect, excludeEmpty, excludeText bool
	for i, arg := range blocks {
		switch arg {
		case "-d":
			dirOnly = true
		case "-ex":
			if len(blocks) > i+1 {
				exclusions := blocks[i+1]
				if strings.Contains(exclusions, "r") {
					excludeRedirect = true
				}
				if strings.Contains(exclusions, "e") {
					excludeEmpty = true
				}
				if strings.Contains(exclusions, "t") {
					excludeText = true
				}
			}
		case "-L":
			if len(blocks) > i+1 {
				if !strings.HasPrefix(blocks[i+1], "-") {
					depth, err = strconv.Atoi(blocks[i+1])
					if err != nil {
						fmt.Printf("invalid value for -L: %v", err)
						return
					}
				}
			}
		case "-v":
			width = -1
			if len(blocks) > i+1 {
				if !strings.HasPrefix(blocks[i+1], "-") {
					width, err = strconv.Atoi(blocks[i+1])
					if err != nil {
						fmt.Printf("invalid value for -v: %v", err)
						return
					}
				}
			}
		}
	}
	depth = depth + dbm.context.KP.Dirs.Size()
	tree, err := dbm.Mapper.Db.Tree(dbm.context.KP,
		dirOnly,
		dbm.settings.ShowSystem,
		excludeRedirect,
		excludeEmpty,
		excludeText,
		depth,
		width)
	if err != nil {
		fmt.Printf("error creating tree: %v", err)
		return
	}
	// calculate the index padding required for this size of a tree
	iPad := tree.Size()
	if iPad == 0 {
		fmt.Println("No directories or records found")
		return
	}
	strPad := strconv.Itoa(iPad)
	iPad = len(strPad)

	dbm.ResetIdMap()

	var i int // index for cd by number
	var displayed int
	var offset int

	var size = tree.Size()
	it := tree.Iterator()
	for it.Next() {
		i++
		dbm.context.SetTerminalHW()
		_, value := it.Key(), it.Value()
		val := value.(*kvs.TreeValue)
		reps := val.KP.Dirs.Size() * 4
		if i == 1 {
			offset = val.KP.Dirs.Size() * 4
		}
		if reps == 0 {
			reps = 4
		}
		l := val.Leaf
		if val.KP.KeyParts.Empty() {
			l = l + "/"
		} else { // add indent for a record key
			reps = reps + 4
		}
		if reps >= offset {
			reps = reps - offset
		}
		n := fmt.Sprintf("%*d", iPad, i)
		left := fmt.Sprintf("%*s%s", iPad, " ", strings.Repeat(" ", reps))
		if i == 1 { // print the root (i.e. a dot)
			fmt.Printf("%s%s\n", left, " .")
		}
		b := elbow
		// print the dir or record
		fmt.Fprintf(color.Output, "%s%s%s%s", n, left, color.HiCyanString(b), l)

		var v string
		if len(val.Value) > 0 { // print the value
			// the following lines check to see if the value
			// exceeds the terminal width.
			// If so, it is broken in to multiple indented lines,
			// so each part lines under the equal sign
			leftMargin := len(left) + len(n) + 4
			width := dbm.context.TWidth - leftMargin
			v = ltstring.WrappedLines(val.Value, leftMargin, width, len(val.KP.Key()))
			fmt.Fprintf(color.Output, "%s\n", color.HiCyanString(v))
		} else {
			fmt.Println()
		}
		// add KeyPath to ID map so user can cd using a number.
		dbm.AddPathToMap(val.KP)
		// chunk the display into what fits terminal height
		// and provide user a means to quit or continue
		displayed++
		if displayed >= dbm.context.THeight-3 {
			quit, recNbr := dbm.Quit(i, size, val.KP.Dirs.Join("/"))
			if quit {
				if recNbr > -1 {
					dbm.Executor(fmt.Sprintf("cd %d", recNbr))
				}
				break
			}

			displayed = 0
		}
	}
}

const (
	elbow = "└── "
	line  = " ─── "
	pipe  = "│ "
	tBone = "├── "
)

// Quit is used to page information being displayed within a for loop.
// It displays information and instructions to continue or quit,
// and returns true if the user wants to break out of the loop.
// shown is the number of items displayed so far.
// total is the total number of items available for display.
// context is the context of the last displayed item, e.g. its directory path.
func (dbm *DbM) Quit(shown, total int, context string) (bool, int) {
	instructions := "Press <return> to continue, q<return> to quit, {number}<return> to view"
	shownMsg := color.HiGreenString(fmt.Sprintf("%d", shown))
	totalMsg := color.HiGreenString(fmt.Sprintf("%d", total))
	contextMsg := color.HiGreenString(context)
	if len(context) > 0 {
		if shown > 0 {
			fmt.Fprintf(color.Output, ": Showing %s of %s. Context: %s. %s", shownMsg, totalMsg, contextMsg, instructions)
		} else {
			fmt.Fprintf(color.Output, ": Showing %s. %s", contextMsg, instructions)
		}
	} else {
		if shown > 0 {
			fmt.Fprintf(color.Output, ": Showing %s of %s. %s", shownMsg, totalMsg, instructions)
		} else {
			fmt.Printf(": %s\n", instructions)
		}
	}
	fmt.Println()
	scanner := bufio.NewScanner(os.Stdin)
	showRecNbr := -1
	var err error
	for scanner.Scan() {
		txt := scanner.Text()
		switch txt {
		case "":
			return false, showRecNbr
		case "q":
			return true, showRecNbr
		default:
			if showRecNbr, err = strconv.Atoi(txt); err == nil {
				return true, showRecNbr
			}
		}
	}
	return false, showRecNbr
}
func (dbm *DbM) Url() {
	if dbm.context.InRecord() {
		fmt.Println("Use the url below to access the record using a browser:")
		fmt.Println(dbm.context.KP.ApiID("localhost", dbm.Port))
		fmt.Println("Use the url below to compare this version and others using a browser:")
		fmt.Println(dbm.context.KP.ApiTopic("localhost", dbm.Port))
	} else if dbm.context.KP.Dirs.First() != "ltx " {
		fmt.Println("You can't use the url command unless you are in the ltx root and in a record.")
	} else {
		fmt.Println("You can't use the url command unless you are in a record.")
	}
}
func GetOsEditor() string {
	switch goos.CodeForString(runtime.GOOS) {
	case goos.Darwin, goos.Linux:
		return "vi"
	case goos.Windows:
		return "notepad"
	default:
		return ""
	}
}
