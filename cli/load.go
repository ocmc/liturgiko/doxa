// Copyright © 2019 The Orthodox Christian Mission Center (ocmc.org)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cli

import (
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/ages/ares/ares2bolt"
	"github.com/liturgiko/doxa/pkg/bible"
	"github.com/liturgiko/doxa/pkg/config"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/media"
	"github.com/liturgiko/doxa/pkg/utils/clinput"
	"github.com/liturgiko/doxa/pkg/utils/oslw"
	"github.com/liturgiko/doxa/pkg/utils/repos"
	"github.com/liturgiko/doxa/pkg/utils/stack"
	"github.com/liturgiko/doxa/pkg/utils/stamp"
	"path"
	"sync"
)

var dbFilename = "liturgical.db"

// vars for 'load' flags
var (
	loadAres   bool
	loadBible  bool
	loadJson   bool
	loadMedia  bool
	loadCvs    bool
	loadTexRes bool
	loadTexTmp bool
	ares       = "ares"
	bibles     = "Bible versions"
	cvs        = "cvs"
	mediaMaps  = "media maps"
	texRes     = "OslwResourceLine Resources"
	texTmp     = "OslwResourceLine Templates"
	whatJson   = "json"
)

// vars for 'from' flags
var (
	fromDir    bool
	fromGithub bool
	fromGitlab bool
	ocmc       bool
)

// vars for 'to' flags
var (
	toBolt bool
	toSql  bool
	toNeo  bool
)
var quit bool
var deleteFirst bool
var detailedProgress bool

// TODO: remove sql. how to handle delete first?

func Load(
	mapper *kvs.KVS,
	mediaReverseLookup *sync.Map,
	site string,
	useLiturgicalDefaults bool,
	useBibleDefaults bool,
	sm *config.SettingsManager) (reloadKeyRing bool) {
	doxlog.WriteCliOut = true
	defer func() {
		doxlog.WriteCliOut = false
	}()
	quit = false
	loadAres = false
	loadBible = false
	fromGithub = false
	toBolt = false
	detailedProgress = false
	ocmc = false

	if useLiturgicalDefaults {
		loadAres = true
		fromGithub = true
		toBolt = true
		ocmc = true
	} else if useBibleDefaults {
		loadBible = true
		fromGithub = true
		toBolt = true
		ocmc = true
	} else {
		setParameters()
		if quit {
			return false
		}
	}
	timeStamp := stamp.NewStamp("load")
	timeStamp.Start()
	msg := fmt.Sprintf("Loading %s from %s to %s", loadWhat(), loadFrom(), loadTo())
	doxlog.Info(msg)

	aresPath := path.Join(config.DoxaPaths.ResourcesPath, "ares")

	if toBolt {
		dbFilename = config.DoxaPaths.DbPath
		doxlog.Info(fmt.Sprintf("DB file is: %s", dbFilename))
	}
	// TODO: ask user whether to delete first or not
	deleteFirst = true

	// based on the parameters, determine which function to call
	switch {
	case loadAres:
		switch {
		case fromDir:
			msg = fmt.Sprintf("Reading from directory %s", aresPath)
			doxlog.Info(msg)
			switch {
			case toBolt:
				err := ares2bolt.FromGithubFiles(aresPath, mapper, detailedProgress, deleteFirst)
				if err != nil {
					doxlog.Errorf("%v", err)
					return
				}
				doxlog.Info("Finished loading ares files")
				doxlog.Infof("Check %s to see if there were errors.", doxlog.LogFileName())
				reloadKeyRing = true
			case toNeo:
			}
		case fromGithub:
			switch {
			case toBolt:
				var theLiturgicalRepos []string
				var err error
				theLiturgicalRepos, err = sm.GetSysSyncLtxUrls(ocmc)
				if err != nil {
					doxlog.Errorf("%v", err)
					return
				}
				err = ares2bolt.FromGithubMemory(theLiturgicalRepos, mapper, detailedProgress, deleteFirst)
				if err != nil {
					doxlog.Errorf("%v", err)
					return
				}
				doxlog.Infof("Finished loading ares files into %s.", dbFilename)
				doxlog.Infof("Check %s to see if there were errors.", config.DoxaPaths.LogPath)
				reloadKeyRing = true
			default:
				doxlog.Error("What to load 'into' is not defined.  Exiting.")
				return
			}
		case fromGitlab:
		default:
			doxlog.Error("What to load from is not defined.  Exiting.")
			return
		}
	case loadBible:
		loader, err := bible.NewLoader(mapper, "txt", "~", 5)
		if err != nil {
			doxlog.Errorf("%v", err)
			return
		}
		var theBiblicalRepos []string
		theBiblicalRepos, err = sm.GetSysSynchBtxUrls(ocmc)
		if err != nil {
			doxlog.Errorf("%v", err)
			return
		}

		err = loader.Load(theBiblicalRepos, detailedProgress)
		if err != nil {
			doxlog.Errorf("%v", err)
			return
		}
	case loadCvs:
	case loadJson:
	case loadMedia:
		// should be in a directory downloaded from https://github.com/AGES-Initiatives/ages-alwb-templates/tree/master/net.ages.liturgical.workbench.templates
		m := media.NewMediaManager(mapper, mediaReverseLookup)
		var err error
		var filePaths stack.StringStack
		// BigMac: big problem.  You should read filePaths from the properties manager.
		// Also, why is it multiple files?  Why not just the directory?
		if filePaths, err = sm.GetSiteMediaMapFiles(site); err != nil {
			doxlog.Errorf("Error reading media map files for %s: %v\n", site, err)
			return
		}
		for _, p := range filePaths {
			doxlog.Infof("loading %s\n", p)
			err = m.LoadFromAresMediaFile(p)
			if err != nil {
				doxlog.Errorf("error loading media maps: %v\n", err)
				continue
			} else {
				doxlog.Infof("Deleted existing database records for for %s and reloaded them.\n", p)
			}
		}
	case loadTexRes: // OSLW LaTeX
		oslwPath := path.Join(config.DoxaPaths.ResourcesPath, "oslw")
		switch {
		case fromDir:
			msg = fmt.Sprintf("Reading from directory %s", oslwPath)
			doxlog.Info(msg)
			switch {
			case toNeo:
			case toSql:
				err := oslw.Res2Kvs(oslwPath, dbFilename)
				if err != nil {
					doxlog.Errorf("%v", err)
					return
				}
				doxlog.Infof("\nFinished loading OslwResourceLine resources into %s.", dbFilename)
				doxlog.Infof("\nCheck %s to see if there were errors.\n", config.DoxaPaths.LogPath)
			}
		case fromGithub:
			oslwCloneUrl := "https://github.com/OCMC-Translation-Projects/service.book.ke.oak.git"
			_, err := repos.Clone(oslwPath, oslwCloneUrl, true, false, 1)
			if err != nil {
				doxlog.Error(fmt.Sprintf("Clone %s: %v", oslwCloneUrl, err))
				return
			}
			switch {
			case toBolt:
				// TODO Need list of oslw repos
				var oslwRepos []string
				err = ares2bolt.FromGithubMemory(oslwRepos, mapper, detailedProgress, deleteFirst)
				if err != nil {
					doxlog.Errorf("%v", err)
					return
				}
				doxlog.Infof("\nFinished loading ares files into %s.", dbFilename)
				doxlog.Infof("\nCheck %s to see if there were errors.\n", config.DoxaPaths.LogPath)
			default:
				doxlog.Warnf("What to load into is not defined.  Exiting.")
				return
			}
		default:
			doxlog.Warn("What to load from is not defined.  Exiting.")
			return
		}
	default:
		doxlog.Warn("What to load is not defined.  Exiting.")
		return
	}
	doxlog.Info(timeStamp.Finish())
	return
}

func setParameters() {
	setWhat()
	if quit {
		return
	}
	includeOcmc()
	if quit {
		return
	}
	setFrom(loadWhat())
	if quit {
		return
	}
	toBolt = true
	// we only load to Bolt, so skip.
	// Leave commented out in case we
	// have another option in the future.
	//setTo()
	//if quit {
	//	return
	//}
	setDetailedProgress()
	if quit {
		return
	}
}

func setWhat() {
	q := "load what?"
	var o = []string{
		"a", "liturgical texts (from ares)",
		"b", "biblical texts",
		"m", "media-map (from ares)",
		"q", "quit the command",
	}
	loadAres = false
	loadBible = false
	loadMedia = false
	quit = false
	switch clinput.GetInput(q, o) {
	case 'a':
		loadAres = true
	case 'b':
		loadBible = true
	case 'm':
		loadMedia = true
	case 'q':
		quit = true
	default:
		fmt.Println("Invalid selection...")
	}
}
func setFrom(what string) {
	q := fmt.Sprintf("load %s from what?", what)
	var o = []string{
		"a", "dir (directory on your computer)",
	}
	switch {
	case loadAres, loadBible:
		o = append(o, "b")
		o = append(o, "github / gitlab")
	}
	o = append(o, "q")
	o = append(o, "quit the command")

	fromDir = false
	fromGithub = false
	quit = false

	switch clinput.GetInput(q, o) {
	case 'a':
		fromDir = true
	case 'b':
		fromGithub = true
	case 'q':
		quit = true
	default:
		fmt.Println("Invalid selection...")
	}
}
func setTo() {
	q := "load into:"
	var o = []string{
		"a", "database",
		"q", "quit the command",
	}
	switch clinput.GetInput(q, o) {
	case 'a':
		toBolt = true
	case 'q':
		quit = true
	default:
		fmt.Println("Invalid selection...")
	}
}

func includeOcmc() {
	q := "Include OCMC sources:"
	var o = []string{
		"n", "no",
		"y", "yes",
		"q", "quit the command",
	}
	switch clinput.GetInput(q, o) {
	case 'n':
		ocmc = false
	case 'y':
		ocmc = true
	default:
		fmt.Println("Invalid selection...")
	}
}
func loadWhat() string {
	switch {
	case loadAres:
		return ares
	case loadBible:
		return bibles
	case loadCvs:
		return cvs
	case loadJson:
		return whatJson
	case loadMedia:
		return mediaMaps
	case loadTexRes:
		return texRes
	case loadTexTmp:
		return texTmp
	default:
		return "unknown"
	}
}
func loadFrom() string {
	switch {
	case fromDir:
		return "dir"
	case fromGithub:
		return "github"
	case fromGitlab:
		return "gitlab"
	default:
		return "unknown"
	}
}
func loadTo() string {
	switch {
	case toBolt:
		return "boltDB database"
	case toSql:
		return "sqlite database"
	case toNeo:
		return "neo4j database"
	default:
		return "unknown"
	}
}
func setDetailedProgress() {
	q := "show detailed information while loading?"
	var o = []string{
		"y", "yes",
		"n", "no",
		"q", "quit the command",
	}
	switch clinput.GetInput(q, o) {
	case 'n':
		detailedProgress = false
	case 'y':
		detailedProgress = true
	case 'q':
		quit = true
	default:
		fmt.Println("Invalid selection...")
	}
}
