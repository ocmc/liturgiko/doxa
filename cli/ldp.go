package cli

import (
	"encoding/json"
	"fmt"
	"github.com/liturgiko/doxa/db/kvs"
	"github.com/liturgiko/doxa/pkg/doxlog"
	"github.com/liturgiko/doxa/pkg/enums/calendarTypes"
	"github.com/liturgiko/doxa/pkg/enums/expressionTypes"
	"github.com/liturgiko/doxa/pkg/keyring"
	"github.com/liturgiko/doxa/pkg/ldp"
	"github.com/liturgiko/doxa/pkg/ltm"
	"github.com/liturgiko/doxa/pkg/parser"
	"github.com/liturgiko/doxa/pkg/resolver"
	"strings"
	"time"
)

// ShowLdp displays the liturgical day properties for a date.
// All parameters can be set to zero value, in which case
// the current date is used.
func ShowLdp(mapper *kvs.KVS,
	year,
	month,
	day int,
	all, // show all liturgical properties
	feasts, // show the feasts for the year
	julian bool, // use julian calendar instead of default (gregorian)
	model, // to use for evaluating a rid, defaults to "gr_gr_cog"
	eval string, // expression to be evaluated
	templateMapper ltm.LTM,
	keyRing *keyring.KeyRing) {
	doxlog.WriteCliOut = true
	defer func() {
		doxlog.WriteCliOut = false
	}()
	var calendarType = calendarTypes.Gregorian
	if julian {
		calendarType = calendarTypes.Julian
	}
	now := time.Now()
	if year == 0 {
		year = now.Year()
	}
	if month == 0 {
		month = int(now.Month())
	}
	if day == 0 {
		day = now.Day()
	}
	l, err := ldp.NewLDPYMD(year, month, day, calendarType)
	if err != nil {
		doxlog.Errorf("%v", err)
		return
	}
	if len(eval) > 0 {

		ltkResolver := resolver.NewDbResolver(mapper, keyRing, templateMapper, false)
		lml, err := parser.NewLMLExpressionParser(eval, year, month, day, calendarType, ltkResolver)
		if err != nil {
			fmt.Println(err)
		}
		lml.Listener.Ctx.Model = model
		_, parseErrors := lml.WalkExpression()
		if len(parseErrors) > 0 {
			verbose := parseErrors[0].StringVerbose()
			if lml.Listener.Ctx.ExpressionType == expressionTypes.Exists && strings.Contains(verbose, "not found") {
				fmt.Printf("\nOn %s, the expression:\n", lml.Listener.Ctx.LDP.FormattedLiturgicalDate())
				fmt.Printf("\n\t%s for model %s", eval, model)
				fmt.Printf("\n\nIs %v\n\n", lml.Listener.Ctx.ExpressionTrue)
			} else {
				for _, parseError := range parseErrors {
					fmt.Println(parseError.StringVerbose())
				}
			}
		} else {
			fmt.Printf("\nOn %s, the expression:\n", lml.Listener.Ctx.LDP.FormattedLiturgicalDate())
			fmt.Printf("\n\t%s", eval)
			fmt.Printf("\n\nIs %v\n\n", lml.Listener.Ctx.ExpressionTrue)
		}
	} else if all {
		atemJson, _ := json.MarshalIndent(l, "", " ")
		fmt.Println(string(atemJson))
	} else {
		fmt.Println(l.String())
		if feasts {
			fmt.Println(l.Feasts())
		}
	}
}
