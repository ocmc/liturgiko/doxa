package cli 

import (
	"fmt"
	"github.com/c-bata/go-prompt"
	"github.com/liturgiko/doxa/pkg/enums/commands"
)

func Help(command commands.Command, suggestions []prompt.Suggest) {
	mode := command.Abr()
	if command == commands.Home {
		mode = "doxa home"
	}
	fmt.Printf("Commands available in the %s mode are:\n", mode)
	for _, s := range suggestions {
		fmt.Printf("%12v : %s\n", s.Text, s.Description)
	}
	switch command {
	case commands.Database:
		dbHelp()
	case commands.Ldp:
		ldpHelp()
	case commands.Server:
		serverHelp()
	default:
		doxaHelp()
	}
}
func dbHelp() {
	fmt.Println("db help")
}
func doxaHelp() {
//	fmt.Println("generic help")
}
func ldpHelp() {
	fmt.Println("The ldp mode allows you to view the liturgical properties for specific dates.")
	fmt.Println("This is useful for these reasons:")
	fmt.Println("\t1) If you want to know the information.")
	fmt.Println("\t2) For viewing the liturgical properties for a date used in a service template.")
	fmt.Println("You can use the ldp eval command to evaluate an expression you want to use in a template.")
	fmt.Println("Examples:")
	fmt.Printf("\teval  Exists rid \"me.*/meDismissal\"\n")
	fmt.Printf("\teval  ModeOfWeek == 2\n")
	fmt.Printf("eval works on the current date, unless you precede it with another date, e.g.:\n")
	fmt.Printf("\tmonth 1 day 6 eval  Exists rid \"me.*/meDismissal\"\n")
	fmt.Printf("By default the ldp command uses the Gregorian calendar.\n")
	fmt.Printf("\tTo use the Julian calendar, add the word `julian`, e.g. feasts julian\n")
}
func serverHelp() {
	fmt.Println("server help")
}
