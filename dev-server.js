// Simple dev server for static files using Bun
// This allows you to see changes to web components immediately without restarting the Go app

import { serve } from 'bun';

const PORT = 3000;
const STATIC_DIR = './static';

const server = serve({
  port: PORT,
  fetch(req) {
    const url = new URL(req.url);
    // Allow CORS for development
    const headers = {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, OPTIONS',
      'Access-Control-Allow-Headers': '*',
      'Cache-Control': 'no-store'
    };

    if (req.method === 'OPTIONS') {
      return new Response(null, { headers });
    }

    // Serve files from the static directory
    let path = url.pathname;
    if (path === '/') {
      path = '/index.html';
    }

    try {
      const file = Bun.file(`${STATIC_DIR}${path}`);
      return new Response(file, { headers });
    } catch (error) {
      return new Response('Not found', { status: 404, headers });
    }
  },
});

console.log(`Dev server running at http://localhost:${PORT}`);
console.log(`Serving static files from ${STATIC_DIR}`);